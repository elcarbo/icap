<?php
// init
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
include APPLICATION_PATH . '/modules/default/models/Exporter.php';
include APPLICATION_PATH . '/modules/default/models/ExcelStreamer.php';

//initialize database
$config = new Zend_Config_ini(APPLICATION_PATH . '/configs/application.ini', 'production'); 
Zend_Registry::set('db', $config->database);
$objExporter = new Mainsim_Model_Exporter(APPLICATION_PATH . '/../export/temp/', APPLICATION_PATH . '/../export/');
$objExporter->createSheetTool(urldecode($argv[1]), str_replace("£$?", " ", $argv[2]), $argv[3]);

?>
