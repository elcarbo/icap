<?php


$PTR=json_decode($_POST["param"],true);
$url="file.xml";         // LANGUAGE file name

echo json_encode(xmltojson($url,$PTR));








function xmltojson($url, $ptr) {   // $ptr =[ { module:"wizard" }, { argument:"Configurazione" } ]
  $xml=file_get_contents($url);
  $xml = str_replace(array("\n", "\r", "\t"), '', $xml);
  $xml = trim(str_replace('"', "'", $xml));
  
  $axml = json_decode(json_encode( (array) simplexml_load_string($xml) ),1);
    
  $page=1;        
  $n=count($ptr);    
  if($n) {
    $lastp=$ptr[$n-1];
    foreach($lastp as $k=>$v){ 
      if($k=="page") { $page=$v; array_pop($ptr); }
  }}            
                                   
  $x=navXml($axml,$ptr); $y=array();
  if( isset($x["page"]) ) {
    if(isAssoc($x["page"])) $y[]=$x["page"]; else $y=$x["page"];
    return array( "data"=>$y, "page"=>$page, "tag"=>"page");         // page nodes
  } 
  
  foreach($x as $k=>$v){
    if($k!="@attributes"){
        
      if(isAssoc($x[$k])) { 
        $y[]=$x[$k]["@attributes"]["label"];
      } else {
        for($r=0;$r<count($x[$k]);$r++) {
           $y[]=$x[$k][$r]["@attributes"]["label"];                             // list nodes
        }
      } 
      
        return array( "data"=>$y, "page"=>0, "tag"=>$k ); 
    }
  }
  
  return array("data"=>array(), "page"=>-1, "error"=>$x );                       // error
}

function navXml($x,$p){                               
  $n=count($p);
  for($r=0;$r<$n;$r++) { $x=GetSub($x,$p[$r]);  }
  return $x;
}

function GetSub($x,$i){
  foreach($i as $k=>$v){ $key=$k; $val=$v; }
  
  if(isAssoc($x[$key])) return $x[$key];
  
  $n=count($x[$key]); 
  for($r=0;$r<$n;$r++){
     if( $x[$key][$r]["@attributes"]["label"]==$val ) return $x[$key][$r];
  }          
  return $x;  
}

function isAssoc($a){
  return array_keys($a) !== range(0, count($a) - 1);
}

?>