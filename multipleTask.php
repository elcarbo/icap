<?php

require __DIR__.'/public/config.php';
/*class cronThread extends Thread
{
    private $url;
    public function __construct($url) {        
        $this->url = $url;                
    }
    
    public function run()
    {
        /*if($this->url == 'a') {
            sleep(2);
            echo 'script a'.PHP_EOL;
        }
        else {
            echo 'script b'.PHP_EOL;
        }
        try {  
            $argv[1] = $this->url;
            require  "./crontab.php";            
        }catch(Exception $e){echo 'lol '.$e->getMessage();}
    }
}*/
$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
$db = Zend_Db::factory($config->database);
$select = new Zend_Db_Select($db);
$res = $select->from(["t1"=>"database_lists"],"f_url")
    ->join(['t2'=>'t_creation_date'],"f_code = t2.f_id",[])
    ->where("f_phase_id = 1")->where("f_installation_type IN ('free','pro')")
    ->query()->fetchAll();

foreach($res as $site) {
    shell_exec("php ".__DIR__.DIRECTORY_SEPARATOR."crontab.php {$site['f_url']}");
}
/*
$cron[0] = new cronThread("a");
    $cron[0]->start();die;
for($i = 0;$i < 4;$i++) {
    $cron[$i] = new cronThread("a");
    $cron[$i]->start();
    if(!($i%3)){$cron[$i]->join();}
}
$cron[$i] = new cronThread("b");
$cron[$i]->start();
*/