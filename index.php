<?php
// security fix
//header('X-Frame-Options: SAMEORIGIN');

/*
0   non supportato
>0  supportato
2   ipad iphone, iPod
*/
function suppBrowser(){

$u=$_SERVER['HTTP_USER_AGENT']; $b=""; $r=1;
$bs=array("Firefox","Chrome","Safari","Opera","iPhone","iPod","iPad","MSIE","Windows");

for($i=0;$i<count($bs);$i++)  if(preg_match('/'.$bs[$i].'/i',$u)) { $b=$bs[$i]; break; }

if($b){
    $k = array('Version', $b, 'other');
    $p = '#(?<browser>'.join('|',$k).')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if(!preg_match_all($p, $u, $m)) {} 
    $i=count($m['browser']);
    if($i!=1) { if(strripos($u,"Version")<strripos($u,$b)) $v=$m['version'][0];    
    else $v= $m['version'][1]; } else $v= $m['version'][0];
    if($v!=null && $v!="") $v=(float) $v;
} else $r=0;

if($b=="MSIE" && $v<9) return 0;
if($b=="Firefox" && $v<3) return 0;

// mobile
if(strstr(strtolower($u), 'mobile') || strstr(strtolower($u), 'android')) $r=3;
if($b=="iPad" || $b=="iPhone" || $b=="iPod") $r=2;
 

return $r;
} 

$BRW=suppBrowser();

//ripulisco l'array $_POST

function iterateArray($arr)
{
    foreach($arr as &$line) {
        if(!is_array($line)) {
            $line = rawurldecode($line);
        }
        else {
            $line = iterateArray($line);
        }
    }
    return $arr;
}


foreach($_POST as $key => &$line) {
    if(!is_array($line)) {
        $_POST[$key] = rawurldecode($line);
    }
    else {
        $_POST[$key] = iterateArray($line);
    }
}

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(300);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/./application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';
require_once APPLICATION_PATH.'/configs/constants.php';
require_once APPLICATION_PATH.'/configs/lang/en_GB.php';
require_once APPLICATION_PATH.'/configs/lang/it_IT.php';
require_once APPLICATION_PATH.'/configs/lang/fr_FR.php';
require_once APPLICATION_PATH.'/configs/lang/es_ES.php';

require_once('library/fpdf/fpdf.php');
require_once('library/fpdf/time_sheet.php');
require_once('library/fpdf/mainsim_pdf.php');
require_once('library/phpExcel/PHPExcel.php');

session_write_close();
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$front = Zend_Controller_Front::getInstance();
$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
if($config->multiDatabase == 'On' && $config->staging == 'On'){        
    
    if($_SERVER['REQUEST_URI'] != '/'){
    $expRU = explode('/',$_SERVER['REQUEST_URI']);    
}
    else{
        $expRU = explode('.',$_SERVER['SERVER_NAME']);
    }
    //$front->setBaseUrl('/'.$expRU[0]);
}
$front->setControllerDirectory(array(
    'default'=>APPLICATION_PATH.'/modules/default/controllers',
    'admin'=>APPLICATION_PATH.'/modules/admin/controllers'    
));

//script for special function
require_once APPLICATION_PATH.'/configs/script_special_functions.php';
require_once APPLICATION_PATH.'/configs/errorHandler.php';

set_error_handler('mainsimErrorHandler');
$application->bootstrap()
            ->run();
$userData = Zend_Auth::getInstance()->getIdentity();
//if(!$userData->switch_desktop){
//    if($BRW>1  && !isset($_GET['ns']) ) { header("location:".BASEURL."/mobile2/index.html"); exit(); } // mobile
//}
if(!$BRW && (!isset($_POST['page_browser_support']) && !isset($_GET['ns']))  ) { header("location:".BASEURL."/page_browser_support/index.html"); exit(); }
