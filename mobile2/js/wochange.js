// save WO history
/*

t_creation_date_history
t_workorders_history
t_custom_fields_history


- update localDB
- if online ajax send WOs
  resp: if(released WO or delete history)
- rebuilt temp_grid_wo (visibility!)



Set(par)

par=[

{ f_code:fc, f_phase:x  }


]

OnOffline.STATUS  0=off   1=on

*/

  /* indexCode={  f_code:[ 
                              id_t_workorders,          0
                              id_t_crreation,           1
                              id_t_custom,              2
                              id_t_workorders_parent,   3
                            ], ... }
  */



WOchange={  TB:["t_creation_date","t_workorders","t_custom_fields","t_workorders_parent"],           
             aRels:[], callsync:0,                       

changeWF:function(){
with(WOchange){

  var v,r,a,tm, par=[]; 
  a=mobileWO.ackSEL;
  v=$('#idsel_wf').val();
  
  if(!v || !a.length) return;
  tm=time();
  var ll=f_geocoords();
  for(r=0;r<a.length;r++) par.push({ "f_code":a[r], "f_phase_id":v, "f_timestamp":tm, "fc_latitude":ll.lat, "fc_longitude":ll.lon }); 
  Set(par);
  
  mobileWO.ackSEL=[];
  mobileWO.footer_disabled();
}},


Set:function(par){  // par array f_code and value to update or create new WO
with(WOchange){
 
  var a,j,i,r,fidcode,fid,fc,n,jj={},k;
  
for(i=0;i<par.length;i++){    
  fc=par[i]["f_code"];
 
  for(r=0;r<TB.length;r++){
  
    fidcode=r?"f_code":"f_id";
    j=Dbl.query("SELECT "+TB[r]+" GET * WHERE "+fidcode+"="+fc);
    jj={"f_code":fc};
    
  if(!par[i].new_WO){ 
    for(k in j[0]) {
      if(k=="f_id") continue;
      jj[k]=j[0][k];
    }
    if(r!=3) Dbl.query("INSERT "+TB[r]+"_history",jj);   // no t_workorders_parent
  }

    for(k in j[0]) { if(typeof(par[i][k])!="undefined") j[0][k]=par[i][k]; }   // update row with last value in par
    Dbl.query("UPDATE "+TB[r]+" WHERE "+fidcode+"="+fc,j[0]);
  }
 
}


mobileWO.f_refreshWO();
ifUpdMainsim(0);
}},




// event from return ONLINE
f_retOnlineUpd:function(){
  if(OnOffline.STATUS) WOchange.ifUpdMainsim(0);
},


// verify if exist history  // return -1 if no history  or no new WO 
ifUpdMainsim:function(m){   //  m:  0:from changeWF || from offline    1:from sync 
with(WOchange){      
              
  var d,c=Dbl.query("SELECT t_creation_date_history count(*) WHERE 1");
  d=Dbl.query("SELECT t_creation_date GET f_id WHERE f_id<0 AND f_type='WORKORDERS' ", "NUM");    // new WO no new wares(documents)
  
  if(!c && !d.length) return -1;
 
  if(!OnOffline.STATUS) return false;
  
  if(m==1) callsync=1;   

 // prepare unpack wo 
  var rr,t,r,n,f,fic,he,k, upk={}, afc=[], ifc="";
 
 with(Dbl){ 
  for(r=0;r<TB.length;r++){
  
  if(r!=3){                           // no t_workorders_parent
     t=TB[r]+"_history";
     he=db[t].field;
     upk[t]=query("SELECT "+t+" * WHERE 1 ORDER BY f_code","NUM");
     upk[t].splice(0,0,he);
 
     // get distinct(f_code)
     if(!ifc){
      for(n=1;n<upk[t].length;n++){
        f=upk[t][n][1];  // f_code
        if(afc.indexOf(f)==-1) afc.push(f);
      }
 
      if(d.length){ 
        for(n=0;n<d.length;n++) {
          if(afc.indexOf(d[n][0])==-1) afc.push(d[n][0]);
        }
      }     
      ifc=afc.join(",");
     }
     
       // remove older element with f_code >0  (already exist in history of mainsim)-------------------------------
    var nn=upk[t].length-1, ic=he.indexOf("f_code"), it=he.indexOf("f_timestamp"),
        of=0, ft, frst=1,
        oft=0, nft=0;
    
    for(n=nn; n>0; n--){
      
      f=upk[t][n][ic];    // f_code
      ft=upk[t][n][it];  // f_timestamp
      
        if(of<0 || (!of && f<0) ) break;
        if(!nft) { nft=n; oft=ft; }
        if(of==f && ft<oft) { nft=n; oft=ft; }
     
      if(of!=f || n==1){
        if(!frst || (frst && n==1) ) { upk[t].splice(nft,1); nft=n; oft=ft; }
      }  
      
      frst=0;
      of=f;
    } // ---------------------------------------------------------------------------------------------------------  
    
  } 
 
     fic=r?"f_code":"f_id";
     t=TB[r];
     he=db[t].field; 
     upk[t]=query("SELECT "+t+" * WHERE "+fic+" IN ("+ifc+")","NUM");
     upk[t].splice(0,0,he);
  }
 
   // add cross t_ware_wo  
   t="t_ware_wo";
   he=db[t].field; 
   upk[t]=query("SELECT "+t+" * WHERE f_ware_id>0 AND f_wo_id IN ("+ifc+")","NUM");    // f_ware_id>0  new documents are <0
   upk[t].splice(0,0,he);  
    
    // if add t_pair_cross
    var vupd=[];
    try{  vupd=Dbl.GETVAR("v_upd_paircross"); 
    }catch(e){ alert("no var v_upd_paircross initialized!"); } 
 
    if(vupd.length){
       t="t_pair_cross";
       he=db[t].field; 
       upk[t]=query("SELECT "+t+" * WHERE f_group_code IN ("+vupd.join(",")+") AND f_code_main IN ("+ifc+")","NUM");
       upk[t].splice(0,0,he);
    }
        
  

  // if update documents     // create code list ware to generate, tables to update and send base64 files image
  var VD={ "codelist":[], "tables":{}, "data64":{}, "wo_code_list":{} };
  vupd={};       
  try{ vupd=Dbl.GETVAR("v_upd_docs"); }catch(e){ alert("no v_upd_docs initialized!"); }   
      
  if( isFull(vupd) ) { 
 
    VD=f_documents_prepare(VD); 
                       
    //alert("if\n"+JSON.stringify(vupd)); 
                       
    VD["wo_code_list"]=vupd;     
  }                       

      
} // Dbl   


  // alert(JSON.stringify(VD) )
  // alert( JSON.stringify(upk) )       
        
  $.ajax({
        url: staticUrl + '/mobile-workorders/edit?ns=1',
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   "2.0",    
            params:  {"codes_list":afc, "tables":upk, "doc_codes_list":VD.codelist, "doc_wo_deleted":VD.wo_code_list, "doc_tables":VD.tables, "doc_data_b64":VD.data64  }
        },
        success:function(resp){  

            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return;   }
            
            if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
            
            with(jsn){
              if(result) {  // action in params
                
                for(r=0;r<TB.length;r++){
                 if(r!=3) Dbl.query("TRUNCATE "+TB[r]+"_history");     // delete history 
                }
                
                // reset update t_pair_cross and t_upd_docs
                Dbl.SETVAR("v_upd_paircross",[]);
                Dbl.SETVAR("v_upd_docs",{});
                

                // update negative WO f_code
                if(params==null) params={};
                if(params["new_code_list"]){
                  var k,v,r,pr,qad,fidcode, Ncl=params["new_code_list"];
                  
                    for(rr=0;rr<Ncl.length;rr++){
                       k=Ncl[rr]["f_code_old"];
                       v=Ncl[rr]["f_code_new"];
                      pr=Ncl[rr]["fc_progress"]; 
                       
                       for(r=0;r<TB.length;r++){
                          fidcode=r?"f_code":"f_id";
                          qad=""; if(TB[r]=="t_creation_date") qad=", fc_progress="+pr;
                          Dbl.query("UPDATE "+TB[r]+" SET "+fidcode+"="+v+qad+" WHERE "+fidcode+"="+k);
                          if(r!=3) Dbl.query("UPDATE "+TB[r]+"_history SET f_code="+v+" WHERE f_code="+k);
                       }
                       Dbl.query("UPDATE t_locked SET f_code="+v+" WHERE f_code="+k);
                       Dbl.query("UPDATE temp_grid_wo SET f_code="+v+" WHERE f_code="+k);
                       Dbl.query("UPDATE t_ware_wo SET f_wo_id="+v+" WHERE f_wo_id="+k);
                    }  
                    
                    
                // update negative DOC wares    
                if(params["doc_codes_list"]){
                    Ncl=params["doc_codes_list"];   
                    f_updateDoc(Ncl);
                }    

                 mobileWO.f_refreshWO();  
                }

                if(aRels.length) f_ReleaseWO();   // release after updating modify
                if(params["code_list"]){
                  var afcdel=params["code_list"];
                  if(afcdel.length) { f_removeWO(afcdel); mobileWO.f_refreshWO(); }  // unlocked after modify
                }
                  if(callsync) { mobileWO.f_CallSync(); callsync=0; }
              }
            }                                               
        },
        
        error:function(){
           $('#idmsg_alert').html("Trasmission error 04!");
           $.mobile.changePage("#idpopupalert");
           aRels=[];   
        }  
    });    
  return false;  
}},


f_updateDoc:function(ncl){   // ncl = { -timestamp:new_f_code, ...  }
with(WOchange){

  var r,n,d,t,v,tb,p,k; 
  tb=["t_creation_date","t_wares","t_wares_parent","t_custom_fields","t_ware_wo"];
  
  for(r=0;r<tb.length;r++){
    p=r?1:0;
    t=tb[r]; k=0;
    d=Dbl.db[t].data;
    for(n=0;n<d.length;n++){  
      if(!d[n]) continue;
      v=d[n][p];
      if(typeof(ncl[v+""])!="undefined") {
        d[n][p]=parseInt(ncl[v]+"");
        k++;
      } 
    }
    if(k) Dbl.save_tab(t);
  }

}},

f_documents_prepare:function(VD){   // VD={ "codelist":[], "tables":{}, "data64":{}, wo_code_list:{} };
with(WOchange){
    
    var r,f,fc,fw,a,da=[],sda,he,dd=[],ext="jpg";
 
with(Dbl){  
 
    t="t_creation_date";
    he=db[t].field;
    dd=query("SELECT "+t+" * WHERE f_id<0 AND f_category IN ('DOCUMENT','FILES AND LINKS')","NUM");
    if(!dd.length) return VD;
    VD.tables[t]=dd;       // documents  && <0
    a=VD.tables[t];
    
    for(r=0;r<a.length;r++) { 
      f=a[r][0]; da.push(f); 
      ext=(a[r][6].split(".")).pop();
      try{VD.data64[f]=parent.retB64(-f);}catch(e){}                                              // remove if in app
      try{ VD.data64[f]= window.JavascriptInterface.GetImage64(-f,ext) ;   }catch(e){           /* fAlert("w307"); */ }  
      
      VD.codelist.push(f); 
    } 
    sda=da.join(",");
    VD.tables[t].splice(0,0,he);    
        
    t="t_ware_wo";
    he=db[t].field;
    VD.tables[t]=query("SELECT "+t+" * WHERE f_ware_id IN ("+sda+")","NUM"); 
    VD.tables[t].splice(0,0,he);
    
    t="t_wares";
    he=db[t].field;
    VD.tables[t]=query("SELECT "+t+" * WHERE f_code IN ("+sda+")","NUM"); 
    VD.tables[t].splice(0,0,he);    
    
    t="t_wares_parent";
    he=db[t].field;
    VD.tables[t]=query("SELECT "+t+" * WHERE f_code IN ("+sda+")","NUM"); 
    VD.tables[t].splice(0,0,he);    
    
    t="t_custom_fields";
    he=db[t].field;
    VD.tables[t]=query("SELECT "+t+" * WHERE f_code IN ("+sda+")","NUM"); 
    VD.tables[t].splice(0,0,he);    
  }
     
    return VD;
}},




f_removeWO:function(afd){
with(WOchange){
  var r,v;
      v=afd.join();            
      with(Dbl){
        query("DELETE t_creation_date WHERE f_id IN ("+v+")");
        query("DELETE t_workorders WHERE f_code IN ("+v+")");
        query("DELETE t_custom_fields WHERE f_code IN ("+v+")");
        query("DELETE t_workorders_parent WHERE f_code IN ("+v+")");
        query("DELETE t_locked WHERE f_code IN ("+v+")");
        query("DELETE t_ware_wo WHERE f_wo_id IN ("+v+")");
        query("DELETE t_pair_cross WHERE f_code_main IN ("+v+")");
        
        // wo relations delete
        query("DELETE t_wo_rel WHERE f_code IN ("+v+")");
      }  
}},




 
f_btnReleaseWO:function(){
with(WOchange){
  var r,a=mobileWO.ackSEL; 
  if(!a.length) return;

  // if offline
  if(!f_test_online( LANG("Connection is required to complete this action") )) return; 
  for(r=0;r<a.length;r++) aRels.push(a[r]);
  if(ifUpdMainsim()==-1 && aRels.length) f_ReleaseWO();
}},


f_ReleaseWO:function(){
with(WOchange){
  
  var v,r;
  $.ajax({
        url: staticUrl + "/mobile-workorders/release?ns=1",           //'/mobile-sync/check-db-structure',
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   2.0,     
            params:  {"f_codes":aRels}
        },
        success:function(resp){  

            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return;   }
            
              if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
            
              if(jsn.result) {  // action in params
                  f_removeWO(aRels);  
                  aRels=[]; 
                  mobileWO.f_refreshWO();           
              }                                                 
        },
        error:function(){
           $('#idmsg_alert').html("Trasmission error 05!");
           $.mobile.changePage("#idpopupalert"); 
           aRels=[];   
        }  
    });

}},




f_btnLockWO:function(){
with(WOchange){

  if(!f_test_online( LANG("Connection is required to complete this action") )) return; 
  
  var r,a,n,nl, ar=[], v,al=[], msg="";
  
  nl=mobileWO.ackSEL.length;
  if(!nl) return;
   
  a=Dbl.query("SELECT t_locked f_code WHERE f_code IN ("+mobileWO.ackSEL.join(',')+")");
  if(a.length) {
    if(a.length==nl) { mobileWO.f_refreshWO();  return; }
    for(r=0;r<a.length;r++) ar.push[a[r].f_code];  
  } 
   
  for(r=0;r<nl;r++) {
    v=mobileWO.ackSEL[r];
    if(ar.indexOf(v)==-1) al.push(v); 
  }
    
    
  // call to lock  
  $.ajax({
        url: staticUrl + "/mobile-workorders/lock?ns=1",        
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   2.0,     
            params:  {"code_list":al}
        },
        success:function(resp){  

            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return;   }
              if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
            
              if(jsn.result) {  // action in params
                  a=jsn.params["code_list"];
                  
                  n=a.length;
                  
                  if(n!=nl) msg=LANG("Some work orders are not lockable!");
                  if(!n) msg=LANG("Work orders are not lockable!");
                  
                  if(n!=nl) { 
                    NOBACK=0;
                    $('#idmsg_alert').html(msg);
                    $.mobile.changePage("#idpopupalert");
                  }
                    
                  if(n){  
                      ar=[];  
                      for(r=0;r<n;r++) ar.push( { f_code:a[r], f_user_id:mobileWO.user_id } );  
                      Dbl.query("INSERT t_locked",ar);
                  }  
                    
                  mobileWO.f_refreshWO();             
              }                                                 
        },
        error:function(){
           $('#idmsg_alert').html("Trasmission error 06!");
           $.mobile.changePage("#idpopupalert");
        }  
    });  
  

}},





f_btnUnlockWO:function(){
with(WOchange){

  if(!f_test_online( LANG("Connection is required to complete this action") )) return; 

  var r,a,n,nl, ar=[], msg="";
  
  nl=mobileWO.ackSEL.length;
  if(!nl) return;

  a=Dbl.query("SELECT t_locked f_code WHERE f_code IN ("+mobileWO.ackSEL.join(',')+")");
  if(!a.length) return;
  
  for(r=0;r<a.length;r++) ar.push(a[r].f_code);  // ar = list of f_code to unlock
  nl=ar.length;
  
  // call to unlock  
  $.ajax({
        url: staticUrl + "/mobile-workorders/unlock?ns=1",        
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   2.0,     
            params:  {"code_list":ar}
        },
        success:function(resp){  

            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return;   }
              if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
            
              if(jsn.result) {  // action in params
                  a=jsn.params["code_list"];
                  n=a.length;
 

                  if(n!=nl) { 
                    NOBACK=0;
                    $('#idmsg_alert').html( LANG("Some work orders are not unlockable!") );
                    $.mobile.changePage("#idpopupalert");
                  }
                    
                  if(n) Dbl.query("DELETE t_locked WHERE f_code IN ("+a.join(',')+")");
 
                  mobileWO.f_refreshWO();             
              }                                                 
        },
        error:function(){
           $('#idmsg_alert').html("Trasmission error 07!");
            $.mobile.changePage("#idpopupalert"); 
        }  
    });

}},



 


f_test_online:function(msg){
  if(!msg) msg=LANG("Connection is required to complete this action");
    if(!OnOffline.STATUS){
      NOBACK=0;
      $('#idmsg_alert').html(msg);
      $.mobile.changePage("#idpopupalert");
      return false;
     }
  return true;
},




time:function(){
  var dt=0,t=Math.floor(new Date().getTime()/1000);
  dt=Dbl.GETVAR("deltatime");
  return t+dt;
},


isFull:function(a){
  for(var k in a) return true; 
  return false;
}

} //