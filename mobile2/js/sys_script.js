/* system scripts

onload | onedit | onsave

LD_

ED_

SV_
SVC_check_


oldVal / newVal 
{
  field_name : value

}



f_field_name

in
[field1, field2, ...]

out
error: message



*/




sysScript={

  oldVal:{},
  newVal:{},
  
  fcode:0,
  wfid:0,
  phase_in:0,
  phase_out:0,
  type_id:0,
  isplanned:0,
  
  nowLD:0,        
  nowSV:0,





 //-----------------------------------------------------------------------------
// run system script onload edit 
LD_script:function(){
with(sysScript){

  getOnLoadPar();
  
  
   
  
}},


 


 //-----------------------------------------------------------------------------
// run script in edit onchange events 
ED_script:function(fnm,n,v){   // fnm=field name     n=index in UIF[n]   v=val
with(sysScript){

  var status;
  
  
  if(fnm=="f_start_date" || fnm=="f_end_date"){
    ED_StartEnd_date("f_start_date", "f_end_date", fnm);
  } 
   
  //console.log(fnm+" | "+n+" | "+v+" | ");
  
  
  
}},


 //-----------------------------------------------------------------------------
// run script when save  

SVC_check_script:function(){
with(sysScript){

  getOnSavePar();  
 
  var msg="";
  
  // planned start end date
  msg=SVC_StartEnd_date("f_start_date", "f_end_date");  
  if(msg!="") return msg;




  return msg;
}},


//--- custom script in settings ---//


SV_script:function(){
with(sysScript){

  getOnSavePar();  
 
  SV_fc_owner_name();   // picklist owner
  
 
  resetPar();
}},




/*
SetTimeToPhase:function(O,N,F){  // N=0 no change phase
with(Editpg){
    var r,f,tt, ph=parseInt($('#idsel_wfedit').val());
    if(PHID==O && ph==N){  // PHID=old | phase=new
      for(r=0;r<UIF.length;r++){                  
        f=UIF[r]["f_field_name"];
        if(f==F){ 
          tt=WOchange.time();
          mScript.jsmf("id_"+F).value=tms_to_date(tt,1);
          return true;  
    }}}
  return false;  
}},
*/




getOnLoadPar:function(){
with(sysScript){
  fcode=Editpg.FC;
  type_id=Editpg.EL_TYPE_ID;
  wfid=Editpg.WFID;
  phase_in=Editpg.PHID;
  phase_out=0; 
  isplanned=Editpg.PLANNED;
  nowLD=WOchange.time(); 
  oldVal=getVal();  
}},


getOnSavePar:function(){
with(sysScript){
  phase_out=parseInt($('#idsel_wfedit').val()); 
  nowSV=WOchange.time(); 
  newVal=getVal();  
}},


resetPar:function(){
with(sysScript){
  fcode=0;
  type_id=0;
  wfid=0;
  phase_in=0;
  phase_out=0;
  oldVal={}; 
  newVal={}; 
}},


fieldExist:function(f){           // f= array|string of field name  
  if(typeof(f)=="string") f=[f];
  for(var r=0;r<f.length;r++){
    if(typeof(sysScript.oldVal[f[r]])=="undefined") return false;
  } 
  return true;
},


getVal:function(f){   // f=field name     if !f  get All
with(Editpg){
  var v,r,n,fnm,ft,o,val={};
  for(n=0;n<UIF.length;n++){
     
     fnm=UIF[n]["f_field_name"];

    if(f && f!=fnm) continue; // get single field
      v="";
      ft=UIF[n]["f_field_type"];

     if(ft!="image") { 
       if(ft=="checkbox" || ft=="radio") {     
          o=UIF[n]["f_options"]; 
          if(o==null || o=="null") continue;
          if(o) o=JSON.parse(o); else o=[];
            sep="";
            for(r=0;r<o.length;r++){  
             if(document.getElementById('id_'+fnm+r).checked) { v+=sep+o[r].value; sep=","; }
            }        
        } else v=document.getElementById('id_'+fnm).value;
  
      if(ft=="datetime" || ft=="date") v=date_to_tms(v);                // check format
      if(ft=="time") v=time_to_tms(v);
    }    
    val[fnm]=v; 
    if(f) return v;
  }
  return val;
}},


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// script list

//-------------------------------------------------------------------------------------------------------
// onSave 
// picklist owner write selected in cross ware_wo   fc_owner_name
SV_fc_owner_name:function(){  
with(sysScript){

  var r,a,p,fid=0,v,vl;
  vl=newVal["fc_owner_name"];
  v=getUserWareId(vl);
  
  // if not new WO => check row in t_ware_wo with wo
  if(fcode){      
    p=f_get_userlist();  // list of user_id   
    a=[];
    if(p) a=Dbl.query("SELECT t_ware_wo GET f_id WHERE f_wo_id="+fcode+" AND f_ware_id IN ("+p+")","N"); 
    if(a.length) fid=a[0][0];
  }
  
  // updade or insert row in t_ware_wo
  if(fid){  
    if(!v) Dbl.query("DELETE t_ware_wo WHERE f_id="+fid);  
    else {   
      Dbl.query("UPDATE t_ware_wo SET f_ware_id='"+v+"' WHERE f_id="+fid);  
    }
  } else {
    if(v){
      a={ "f_ware_id":v,	"f_wo_id":fcode,	"f_timestamp":WOchange.time() };
      Dbl.query("INSERT t_ware_wo",a); 
    }   
  }
}},

getUserWareId:function(v){  
  var r,m, D=Dbl.db["t_users"].data, n=D.length;
  for(r=0;r<n;r++) {
    if(!D[r]) continue;
    m=D[r][3]+" "+D[r][4];
    if(m==v) return D[r][1];
  }
  return 0;
},

f_get_userlist:function(v){  
  var r,a,p,sep="";
  p=Dbl.GETVAR("user_ids");
  if(!p){
    a=Dbl.query("SELECT t_users GET f_code WHERE 1","N");
    p="";
    for(r=0;r<a.length;r++) { p+=sep+a[r][0]; sep=","; } 
    Dbl.SETVAR("user_ids",p);
  }
  return p;
},


//-------------------------------------------------------------------------------------------------------
// Start date < End date 

ED_StartEnd_date:function(f_st,f_en,fnm){  
with(sysScript){
  if(!fieldExist([f_st,f_en]) ) return true; 
  var v1=parseInt(getVal(f_st)), 
      v2=parseInt(getVal(f_en)); 
  if(v1 && v2){
    if(v1<=v2) return true;   
    $('#id_'+fnm).val(""); 
    setTimeout(function(){ fAlert(LANG("Warning: end cannot come before start!")); },50); 
  } 
  return false; 
}},



SVC_StartEnd_date:function(f_st,f_en,fnm){  
with(sysScript){
  if(!fieldExist([f_st,f_en]) ) return ""; 
  
  var v1=parseInt(newVal[f_st]), 
      v2=parseInt(newVal[f_en]);
       
  if(v1 && v2 && v1>v2) return "Warning: end cannot come before start!";     // start > end
  if(!v1 && v2) return "Warning: end cannot come without start!";             // only ending date
 
  return ""; 
}},


//-------------------------------------------------------------------------------------------------------



} // end  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////