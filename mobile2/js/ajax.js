/* ajax

methods:
Ajax.sendAjaxPost( fileserver, post_value, fnc_name );    
Ajax.sendAjaxGet( fileserver, fnc_name );

properties:
Disable   
fnc_offline
fnc_check

*/


Ajax = { Disable:0,              // 1: stop Ajax call 
         fnc_offline:"",        // function run if can not reach server
         fnc_check:"",         // function run when arrive response from server 
         Na:[],               // processi attivi   1 impegnato    0 libero
         jx:[],              // array di oggetti ajax
         Rst:{},
 
sendAjaxPost:function(fileserver, post_value, fnc, encode) {
     with(Ajax){
         if(Disable) return false;
         if(!fnc) fnc = "Ajax.result";
         var i=searchId(), FNC;
         jx[i]=new XMLHttpRequest();

         Rst[i]=0; 
         FNC="Ajax.Frisp"+i;

         eval(FNC+"=function(){ "+
            "if(Ajax.jx["+i+"].readyState==3) Ajax.Rst["+i+"]=1; "+
            "if(Ajax.jx["+i+"].readyState==4) { if(!Ajax.Rst["+i+"]) { " + (fnc_offline?(fnc_offline+"(\""+fileserver+"\",\""+fnc+"\");"):"") + " } "+
            "if(Ajax.jx["+i+"].status==200){ "+                                    
            "var risp=Ajax.jx["+i+"].responseText; " + (fnc_check?(fnc_check+"(); "):"") +fnc+"(risp); Ajax.Na["+i+"]=0; } } }"); 
         
         jx[i].open("post",fileserver,true);
         jx[i].onreadystatechange=eval(FNC);
         jx[i].setRequestHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
         
         if(encode) {
            var val = post_value.split("="), v = val.shift();
                post_value = v+"="+encodeURIComponent(val.join("="));  // in php rawurldecode
         }
         
         jx[i].send(post_value);
     }
     return true;
},


sendAjaxGet:function(fileserver,nomefunz){
     with(Ajax){
         if(Disable) return false;
         if(!fnc) fnc = "Ajax.none";
         var i=searchId();
         jx[i]=new XMLHttpRequest();
         var FNC="Ajax.Frisp"+i;
          
         eval(FNC+"=function(){ if(Ajax.jx["+i+"].readyState==4) if(Ajax.jx["+i+"].status==200){"+
              "  var risp=Ajax.jx["+i+"].responseText;  "+fnc+"(risp); Ajax.Na["+i+"]=0;  }}");          
            
         jx[i].open("get",fileserver,true);
         jx[i].onreadystatechange=eval( FNC );
         jx[i].send(null); 
         }
     return true;    
}, 


searchId:function(){
     with(Ajax){
         var n=Na.length, i=-1;
         for(var r=0;r<n;r++) if(!Na[r]) {i=r;break;}
         if(i<0) {Na.push(1);return n;}
         Na[i]=1;    
         return i;
         }         
},


result:function (o) {
     if(o) {
        o = JSON.parse(o);
        if(moGlb.isset(o.message)) moGlb.alert.show(moGlb.langTranslate(o.message));
		    else if(moGlb.isset(o.error)) moGlb.alert.show(moGlb.langTranslate(o.error));
        else if(typeof o == "string" && o.indexOf('SQLSTATE') >= 0) moGlb.alert.show(moGlb.langTranslate("SQL error: check your bookmark fields"));
        moGlb.loader.hide();
    }
}   
 
} // fine Ajax