// OnOffline STATUS

/*
Properties:
            
STATUS:       0=offline   1=Online
fileServer:   path of file in the server
evtListener:  array of string names of the function to run when status change 
timeTest:     time between check online status (default 8000 => 5 sec)  



Example:
OnOffline.Start( { fileServer: ,
                evtListener:["f_status_ico","f_change_status"]
});
*/
 
 
 
$(document).bind("mobileinit", function(){
  //apply overrides here
  $.mobile.defaultPageTransition="none";
  $.mobile.defaultDialogTransition="none";
  
}); 
 
 

OnOffline={ STATUS:0,
         N:1, count:0, 
         fileServer:"",
         evtListener:[],
         timeTest:8000, DT:250, 

Start:function(props){
with(OnOffline){

  for(var i in props) OnOffline[i]=props[i];
  if(!fileServer) { alert("error: test Online file missing"); return; }
  evtChange();
  OnOffline.Test();
}},


Test:function(){
with(OnOffline){
  var oN=N; count++;
  
  try { document.getElementById("id_n_polling").innerHTML=count; }catch(e){}
 
  var L,r,ac=[], a=Dbl.query("SELECT t_creation_date GET f_id WHERE 1","NUM");
  for(r=0;r<a.length;r++) ac.push(a[r][0]);
 
  L=f_geocoords();
 
  $.ajax({
        url: fileServer,                          
        type: "post",
        data: {
            count:     count,
            num:       N,
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            userAdmin: mobileWO.user_admin,
            version:   2.0,
            params:{ code_list:ac, latitude:L.lat, longitude:L.lon } 
        },
        success:function(resp){   
        
          try{ var jsn=JSON.parse(resp); }catch(e){  return; }
            
          if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
        
          mobileWO.f_upd_counter(jsn["params"]);
          
          var os=STATUS;
          N++; STATUS=1; if(!os) evtChange();                                          
        }
  }) 
 
  setTimeout("OnOffline.Out("+oN+")", timeTest-DT);
  setTimeout("OnOffline.Test()",timeTest);
}},


Out:function(oN){
with(OnOffline){
  if(oN==N) { var os=STATUS;
     STATUS=0; if(os) evtChange(); }
}},

 

evtChange:function(){
with(OnOffline){
  for(var r=0;r<evtListener.length;r++){
    try{ eval(evtListener[r]+"("+STATUS+")"); } catch(e){} }
}}

}

