/* asset

FC = f_code Workorder


  gridAS=[
{ f_id:1000, f_title:"test asset1", f_description:"Test1 descrizione asset in WO1.", status:0 },   // status: 0=ok  1=added  2=deleted exist   3=delete added
...
]


tempTask[f_idasset]=[

  { ... },
  ...
]


*/

//------------------------------------------

QRBAR={


f_readQrBarCode:function(){
with(QRBAR){

  // only wizard
  if(ONWIZ) return false;

  // alert("read qr-barcode");
  //NFC.Get("12345");
}}


}

//------------------------------------------------------------------------------------------------------------
// nfc

 

NFC={  nfcField:"nfc", nfcTable:"t_wares_parent",              //nfcField:"f_id", nfcTable:"t_creation_date",
       nfcVal:"",
       Fcode:0, L:"",
       nfcStatus:0,
       fWO:[],

Get:function(val){
with(NFC){

  // only wizard
  if(ONWIZ) return false;
 
  if(nfcStatus) return false;
  nfcStatus=1;
  nfcVal=val;
 
  // Fcode offline  from NFC
  var ff=(nfcTable=="t_creation_date" || nfcTable=="t_pair_cross")?"f_id":"f_code";
  var a=Dbl.query("SELECT "+nfcTable+" GET "+ff+" WHERE "+nfcField+"='"+nfcVal+"'");              
  if(a.length) Fcode=a[0][ff]; else Fcode="";

  L=location.href;

  // in EDIT?
  if(L.indexOf("index.html#idwoasset")!=-1) {
    //fcode=Editpg.FC;
    nfc_edit_search();
    return; 
  }
  // EDIT: from form edit WO or add Asset
  if(L.indexOf("index.html#idwoedit")!=-1 || L.indexOf("index.html#idsearchAS")!=-1) { 
    fcode=Editpg.FC;
    mobileAS.f_viewAssetListWO(fcode);
    nfc_wait_edit_search(fcode,0);
    return;
  }   
    
    
  if(!Fcode && !OnOffline.STATUS) { nfc_error(LANG("Offline NFC code not found")); return; } 
  
  
  if(L.indexOf("index.html#idvieweditAS")!=-1) {
   nfc_error(LANG("Exit from asset tab"));
   return;
 }  
     
  // popup 
  NOBACK=0;
  $.mobile.changePage("#idpopupNFC"); 
  $(document).delegate("#idpopupNFC", "pagehide", NFC.Close );
}},


//---------------------------------------------------------------------------------
// N F C    E D I T

nfc_wait_edit_search:function(f,t){
with(NFC){
  if(!t) t=0;
  if(!mobileAS.inloading) {  nfc_edit_search(); return; }
  t++;
  if(t>20) { nfc_error(LANG("NFC timeout error!")); return; }
  setTimeout("NFC.nfc_wait_edit_search("+f+","+t+")",500);
}},


nfc_edit_search:function(){
with(NFC){
  $(".ui-input-search .ui-input-text").val( "" ); 
  $(".ui-input-search .ui-input-text").trigger("change"); 
  setTimeout("NFC.f_wait_search()",150);
}},

nfc_edit_continue:function(md){
with(NFC){
  
  // online?
  if(!OnOffline.STATUS) { nfcStatus=0; return; }
  
  if(!md){  // md==0  in EDIT      md==1  no edit && no Fcode
      
      // if there are 1 or more results or if periodic or offline ... no ADD
      var s=document.getElementById("idlistrowswoasset").innerHTML;
      var sfnd="ui-first-child ui-last-child"; // se trovati 1 o più elementi
      if(s.indexOf(sfnd)>-1 || Editpg.PLANNED) { nfcStatus=0; return; }
      
      mobileWO.f_show_loading();
  }  

  // online try to ADD asset to WO
      $.ajax({
        url: staticUrl + '/mobile-wares/check-asset-nfc?ns=1',
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   2.0,
            params:{ nfc_value:nfcVal  }    
        },
        success:function(resp){   
        
            // hide loading
            mobileWO.f_hide_loading(); 
            
            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; } 
            if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
            
            // status NFC_ASSET_FOUND  | NFC_ASSET_NOT_FOUND
            //params = { asset_code:f_code }
            // alert("Codice: "+resp )
            
            nfcStatus=0;
            if(!jsn.params) {
              if(md) nfc_error(LANG("Asset not found!"));
              return;
            }
            
            Fcode=jsn.params.asset_code;

            if(!md){  if(!Fcode) return;
              NOBACK=0;   
              mobileWO.fncConfirm="NFC.AddAssetListEdit('"+jsn.params.asset_title+"','"+jsn.params.asset_description+"')";
              $('#idmsg_confirm').html( LANG("Add asset to list?") );
              $.mobile.changePage("#idpopupconfirm"); 
            } else {
              
                if(!Fcode) {
                    nfc_error(LANG("Asset not found!"));
                    return;
                }
              f_ViewFcodeAsset();
            }
                                     
        },
        error:function(){ 
          if(OnOffline.STATUS){
            mobileWO.f_hide_loading(); 
            $('#idmsg_alert').html("Trasmission error nfc11!");
            $.mobile.changePage("#idpopupalert"); 
          } 
        }  
    });

 
}},



AddAssetListEdit:function(title,description){  // Fcode
with(NFC){
  with(mobileAS) { 
    TempAsset[Fcode]={ f_title:title , f_description:description };
    aSELAS.push(Fcode); 
    f_addSelAS();        
  } 
}},






f_wait_search:function(){
with(NFC){
  var f="...";
  if(Fcode) f=Fcode;
  $(".ui-input-search .ui-input-text").val(f); 
  $(".ui-input-search .ui-input-text").trigger("change");
  nfc_edit_continue(0);
}},
//---------------------------------------------------------------------------------


 

// BUTTONS popup
f_checkNFC:function(m){
with(NFC){
  mobileWO.f_show_loading();
  NOBACK=1;
  var u=m?"NFC.f_checkcreateAsset()":"NFC.f_checkcreateWO()";  
  setTimeout(u,200);
}},




// 0 Check / create WO 
f_checkcreateWO:function(){
with(NFC){

  mobileWO.f_hide_loading();
  $.mobile.changePage("#idwolist");
  
    // search offline
  if(Fcode){ // search offline
 
      var a,r;
      fWO=[];
      
      // search children in  t_wares_parent
      af=childSearch(Fcode);
      a=Dbl.query("SELECT t_ware_wo GET f_wo_id WHERE f_ware_id IN ("+(af.join(","))+")" );   // search wo

      if(a.length){ for(r=0;r<a.length;r++) fWO.push(a[r].f_wo_id); }       
  } 

  // search & sync WORKORDERS in mainsim ...
  if(OnOffline.STATUS){
  
      mobileWO.f_show_loading();
      
          $.ajax({
            url: staticUrl + '/mobile-workorders/check-nfc?ns=1',
            type: "post",
            data: {
                userId:    mobileWO.user_id,
                sessionId: mobileWO.session_id,
                version:   2.0,
                params:{ nfc_value:nfcVal  }    
            },
            success:function(resp){   
                try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; } 
                if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
                
                // params = { code_list:[f_code,...], asset_code:0 | f_code (of nfc)    }
 
 
                // search if some WO already exist offline
                var v,r,i,n=0,a=[],wocod=jsn.params["code_list"], 
                    acod=jsn.params["asset_code"];
                    
                      if(!acod) {   // nfc code not found
                          nfc_error(LANG("Asset not found!"));
                          return;
                      }
                      
                      Fcode=acod; 
                      Close(); 
    
                      if(wocod.length){
                        // remove fWO from wocod
                        for(r=0;r<fWO.length;r++){
                          i=wocod.indexOf(fWO[r]+"");
                          if(i!=-1) { wocod[i]=0; n++; }   
                        }
 
                        if(n==wocod.length) wocod=[];
                        else {
                        
       //------------- remove if check cross ware wo for children         
                          // add fWO wo in localDB
 
                           a=Dbl.query("SELECT t_creation_date GET f_id WHERE f_id IN ("+wocod.join(",")+")","NUM");
                      
                          for(r=0;r<a.length;r++){
                            v=a[r][0];
                            if(fWO.indexOf(v)==-1) fWO.push(v);
                            i=wocod.indexOf(a[r][0]);
                            wocod[i]=0; n++;
                          }
      //---------------             
                          
                          
                          //alert(wocod.join(",")+"\n"+fWO.join(",")+"\n"+a.length )
                          
                          // if SYNCHRONIZE WO
                          if(wocod.length>n){     // n = n. of WO in localDB
                            Close();
                            NOBACK=0;
                            mobileWO.fncConfirm="NFC.synchWO('"+wocod.join(",")+"')";
                            $('#idmsg_confirm').html( LANG("Do you want to download more work orders with selected asset?") );
                            $.mobile.changePage("#idpopupconfirm");
                            
                            if(fWO.length) viewWOresults();  
                            return;  
                          }
 
                        }
       
                      } else { // no workorders found  -->  CREATE NEW WO width asset?
                        NOBACK=0;
                        mobileWO.fncConfirm="NFC.createWO('"+jsn.params.asset_title+"','"+jsn.params.asset_description+"')";
                        if(fWO.length) mobileWO.nofncConfirm="NFC.viewWOresults()";
                        $('#idmsg_confirm').html( LANG("Do you want to create new work order with selected asset?") );
                        $.mobile.changePage("#idpopupconfirm");

                        return;
                      }
                
                viewWOresults();
    
                // hide loading
                mobileWO.f_hide_loading();                                  
            },
            error:function(){ 
              if(OnOffline.STATUS){
                mobileWO.f_hide_loading(); 
                Close();
                $('#idmsg_alert').html("Trasmission error nfc02!");
                $.mobile.changePage("#idpopupalert"); 
              } 
            }  
        });
   } else {
   
      // view WO results
      viewWOresults();      
   } 

 
  nfcStatus=0;
}},

// recursive search of asset
childSearch:function(f){
with(NFC){
  var q,r,i,a=[],af=[];
  a.push(f);
  q=Dbl.query("SELECT t_wares_parent GET f_code WHERE f_parent_code="+f,"NUM");
  for(r=0;r<q.length;r++) {
    af=childSearch(q[r][0]);
    for(i=0;i<af.length;i++) a.push(af[i]);
  }
  return a;
}},



// view WO results
viewWOresults:function(){
with(NFC){
  
  if(!fWO.length) { nfc_error(LANG("Code not found!")); return; }

      // filter list wo!
      if(!mobileWO.First){
        mobileWO.f_gridWO();   
        mobileWO.f_summaryView();  
      }
      mobileWO.NFCFilter="f_code IN ("+(fWO.join(","))+")";
      mobileWO.f_draw_wo();   
}},




createWO:function(title,description){
with(NFC){
 
  with(mobileAS) { 
    TempAsset[Fcode]={ f_title:title , f_description:description };
    aSELAS.push(Fcode); 
    f_createWOfromAS();   
  } 

}},




synchWO:function(wo){  
with(NFC){

  var awo=[],r,a=wo.split(",");
  for(r=0;r<a.length;r++) {
    v=parseInt(a[r]);
    if(v) awo.push(v);
  }
  
  //alert("sync WO! "+awo.join()); return;

  mobileWO.f_show_loading();
  
      $.ajax({
        url: staticUrl + '/mobile-sync/sync-add?ns=1',
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   2.0,
            params:{ code_list:awo }    
        },
        success:function(resp){   
            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; } 
            if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
 
        //alert(JSON.stringify(jsn.params) )
        //return
         
            // params.db     params.wo_list
            wo=jsn.params.wo_list;
            if(!wo || !wo.length ) {
              nfc_error(LANG("No work orders available!"));
              return;
            }
            
            //  insert wo in table
            f_insertwo_tables(jsn.params.db);
            
            // update wo_relation
            if(jsn["par_wo_relations"]) mobileWO.updateWoRel(jsn.par_wo_relations);
             
            for(r=0;r<wo.length;r++) fWO.push(parseInt(wo[r]));
            
       //     alert(fWO.join(","));
            
            mobileWO.First=0;
            viewWOresults();
 
            // hide loading
            mobileWO.f_hide_loading();                                  
        },
        error:function(){ 
          if(OnOffline.STATUS){
            mobileWO.f_hide_loading(); 
            $('#idmsg_alert').html("Trasmission error nfc 03!");
            $.mobile.changePage("#idpopupalert"); 
          } 
        }  
    });

}},





f_insertwo_tables:function(p){
with(NFC){
  var r,jn,i,k,afi,ff;  
  with(Dbl){
    for(k in p){    
      jn=p[k];
      if(!jn.length) continue;
      
      // check if exist?
      afi=[];
      for(r=0;r<jn.length;r++) afi[r]=jn[r][0]; // indexes of codes
      
      if(k!="t_ware_wo"){  
        ff=(k=="t_creation_date" || k=="t_pair_cross")?"f_id":"f_code";
        query("DELETE "+k+" WHERE "+ff+" IN ("+afi.join(",")+")");
      }
      query("INSERT "+k,jn); 
  }} 

}},


//-----------------------------------------------------------------------------
// 1 Check / [create] Asset
f_checkcreateAsset:function(){
with(NFC){
 
  if(!Fcode) nfc_edit_continue(1);
  else f_ViewFcodeAsset();

  mobileAS.f_run_moduleAS();
  $(".ui-input-search .ui-input-text").val(""); 
  $(".ui-input-search .ui-input-text").trigger("change");
}},

  
f_ViewFcodeAsset:function(){
with(NFC){
  mobileWO.f_hide_loading();
  setTimeout("NFC.f_SearchFcodeAsset()",150);
}},   
  
f_SearchFcodeAsset:function(){
with(NFC){
 
  $(".ui-input-search .ui-input-text").val(Fcode); 
  $(".ui-input-search .ui-input-text").trigger("change");
  
   // EditAS.viewLoadAS(Fcode,2);
     
  mobileAS.f_call_readAS(Fcode,0,0); 
  nfcStatus=0;
}}, 






nfc_error:function(er){
with(NFC){
  if(!er) er=LANG("NFC error!");
  $('#idmsg_alert').html(er);
  NOBACK=0;
  $.mobile.changePage("#idpopupalert");
  nfcStatus=0;
}},


Close:function(){
with(NFC){
// $.mobile.changePage("#idhomepage");
  nfcStatus=0;
}}


} // end NFC









 





//------------------------------------------------------------------------------------------------------------



mobileAS={ M:"mobileAS", FC:0, oFC:-99999,
           gridAS:[], 
           Lim:0,Offset:30,CountAS:0,Mode:0, Page:"", exitPage:"", searc:0,
           aSELAS:[], iFIL:0, FILTER:[],
           TempAsset:{}, isTask:0, lisTask:-1,    // accordion
           TaskSEL:[], TskMTH:0, TskData:0,
           tempTask:{},
           tempDocs:[], buildDoc:0,
           inloading:0, tempAtchs:[],

f_viewAssetListWO:function(fc){
with(mobileAS){

  inloading=1;
  FC=fc;
  mobileWO.f_show_loading();
  setTimeout("mobileAS.f_ActviewAssetListWO()",200); 
}},

  
SetTaskMethod:function(){   // TaskMethods: 0 auto  1 old   2 new    TskMTH: 0=periodic  1=inspection & periodic
with(mobileAS){    
  TskData=( (Dbl.db["t_pair_cross"].field).indexOf("fc_task_quality_good")==-1 )?0:1;    // detect:   0 old   1 new 
  if(!TskData) TskMTH=0;
  else {
    if(!TaskMethods) TskMTH=1;
    else {
      TskMTH=TaskMethods-1;
      if(!TskMTH && Editpg.EL_TYPE_ID!=4) TskMTH=1;   // inspection with new view
  }}
}},
  
 
 
optionSelTask:function(){
with(mobileAS){ 


  // select task
  var s="",seltsk, NtO;
 
  if(TskMTH) {
  
   seltsk=[
    "Choose status",
    "Change selected in Not performed",
    "All Not performed",
    "Change selected in Performed",
    "All Performed",
    "Change selected in Not executable",
    "All Not executable",   
    "Change selected in Performed Good",
    "All Performed Good",
    "Change selected in Performed Acceptable",
    "All Performed Acceptable",
    "Change selected in Performed Not acceptable",
    "All Performed Not acceptable"
   ];
   
   NtO=NOTaskOpt_quality;
  
  } else {
  
   seltsk=[
    "Choose status",
    "Change selected in Not performed",
    "All Not performed",
    "Change selected in Positive",
    "All Positive",
    "Change selected in Negative",
    "All Negative",
    "Change selected in Not executable",
    "All Not executable"
   ];
   
   NtO=NOTaskOpt;
  
  }
  
  for(r=0;r<seltsk.length;r++) { 
    if(NtO.indexOf(r)!=-1) continue;
    s+="<option value='"+r+"'>"+LANG(seltsk[r])+"</option>";
  }
  document.getElementById("idsel_tsk").innerHTML=s;

}},
  
  
f_ActviewAssetListWO:function(){
with(mobileAS){

  isTask=(Editpg.PLANNED)?1:0;  // periodic / inspection
  lisTask=-1;
  
  SetTaskMethod();  // TaskMethods | TskMTH | TskData
  optionSelTask();


  // read query gridAS  
  var ac=[], r,a;

  if(oFC!=FC){
    tempTask={};
    gridAS=[];
    a=[];
    a=Dbl.query("SELECT t_ware_wo GET f_ware_id WHERE f_wo_id="+FC);
    
    if(a.length){
      for(r=0;r<a.length;r++) { ac.push(a[r].f_ware_id);  }
      gridAS=Dbl.query("SELECT t_creation_date GET f_id, f_title, f_description WHERE f_category='ASSET' AND f_id IN ("+ac.join(",")+")");
    }
  }

  oFC=FC;

  mobileWO.f_hide_loading();
  $.mobile.changePage("#idwoasset" );
  
  var adisp="block", stask=isTask? (LANG("/Task")) :"";
  if(isTask || !FooterBtnHome_asset) adisp="none";
  document.getElementById("idwoassetheadAdd").style.display=adisp;
  
  
  var lnw="",FCP=mobileWO.fcodeTOprogress[FC];
  if(!FCP) { FCP=FC; 
  } else lnw=LANG(" (WO ") + Editpg.f_zero(FCP,5)+")";
   
  
  $('#idwoassettitle').html( LANG("Asset")+stask + lnw);  
  f_upd_viewAssetListWO();  
  
  inloading=0;
}},




f_upd_viewAssetListWO:function(){
with(mobileAS){
  mobileWO.f_show_loading();
  setTimeout("mobileAS.f_Actupd_viewAssetListWO()",100);
}},


f_Actupd_viewAssetListWO:function(){
with(mobileAS){
  TaskSEL=[];
  var str=f_show_listAS();
  $('#idlistrowswoasset').html(str);
  $('#idlistrowswoasset').listview('refresh'); 
  
  mobileWO.f_hide_loading();
}},




f_show_listAS:function(){
with(mobileAS){ 

  var r,ri,v,str="",rsl={},st1,st2;
 
 for(r=0;r<gridAS.length;r++) with(gridAS[r]){
 
  v=gridAS[r].status; if(!v) v=0;
  if(v&2) continue;   // if deleted
 

  // count task results for this asset and this WO
  if(isTask) {          
    if(typeof(tempTask[f_id])=="undefined") {               
      tempTask[f_id]=Dbl.query("SELECT t_pair_cross * WHERE f_group_code="+f_id+" AND f_code_main="+FC);       // +" order by f_title"     fc_task_issue_not_performed,	fc_task_issue_positive,	fc_task_issue_negative,	fc_task_issue_not_executable       
    }                                                                                                                             // fc_task_issue_performed,  fc_task_issue_not_executable,	fc_task_issue_not_performed, fc_task_quality_good, 	fc_task_quality_acceptable, 	fc_task_quality_not_acceptable
            
    // count result
    rsl=getCountTaskAS(tempTask[f_id]);  
  }
 
  str+="<li data-theme='b' style='overflow:hidden;'>"+
       "<a onclick='EditAS.viewLoadAS("+f_id+",0);'>"+
           "<img src='"+(isTask?"img/bigcheck2.png":"img/trash-2.png")+"' onclick=' "+(isTask?"mobileAS.f_checktaskAS(event,"+f_id+",1,0,-1);":"mobileAS.f_confirm_deleteAS(event,"+f_id+","+r+");")+" return false;'>"+
           "<p style='float:left;'>"+ 
            "<p style='line-height:18px;'><b>"+f_title+"</b>"+
           "</p>"+
            "<p style='color:#000;margin-bottom:-6px;'>"+f_description+"&nbsp;</p>";
            
          if(isTask) { 
            st1=""; st2="";
            if(!rsl.np) st1="style='color:#85AA28;'"; 
            if(rsl.n) st2="style='color:#C00;'";
            
      if(TskMTH){
        str+="<p style='margin:6px 0 -14px 0;font:bold 11px opensansR;color:#444;'><span "+st1+">NP:["+rsl.np+"]</span> - <span "+st2+">P:["+rsl.pp+"]</span> - NE:["+rsl.ne+"]</p>";
      }else{      
        str+="<p style='margin:6px 0 -14px 0;font:bold 11px opensansR;color:#444;'><span "+st1+">NP:["+rsl.np+"]</span> - P:["+rsl.p+"] - <span "+st2+">N:["+rsl.n+"]</span> - NE:["+rsl.ne+"]</p>"; 
      }
          } 
           
      str+="</p>"+ 
           "<p class='ui-li-aside' style='width:80px;margin-top:-3px;'><strong>"+f_id+"</strong></p>"+
        "</a>";
        
        if(isTask) {           
            var vtsk="";
            vtsk=" onclick='mobileAS.f_openclose_Task("+f_id+");' data-icon='"+( (lisTask==f_id)?'minus':'plus' )+"' "; 
            
            str+="<a href='#' "+vtsk+" data-theme='c' data-shadow='false' data-iconshadow='false'>"+LANG("Show tasks")+"</a>"; 
            
           // view task 
           if(lisTask==f_id) {
              var Tka=tempTask[f_id];
              str+="</li>";
                for(ri=0;ri<Tka.length;ri++){
                    str+="<li data-theme='b' style='overflow:hidden;padding-left:32px;'>"+
                         "<a onclick='mobileAS.f_Edit_Task("+Tka[ri].f_id+","+f_id+","+ri+")'>"+     // ,\""+f_title+"\"
                             "<img src='img/bigcheck2.png' onclick='mobileAS.f_checktaskAS(event,"+Tka[ri].f_id+",0,"+f_id+","+ri+"); return false;'>"+
                             "<p>"+ 
                                "<p style='line-height:18px;'><b>"+Tka[ri].f_title+"</b>"+
                               "</p>"+
                                "<p style='color:#000;margin-bottom:-2px;'>"+LANG(getResultTask(Tka[ri]))+"&nbsp;</p>"+
                               "</p>"+ 
                            "</a></li>";
                } // 
           }                 
          continue;  
        }               
      str+="</li>";
 }

  return str;
}},



f_openclose_Task:function(fc){
with(mobileAS){ 
  if(lisTask==fc) lisTask=-1; else lisTask=fc;
  f_upd_viewAssetListWO();
}},




getCountTaskAS:function(a){   // a = tempTask[f_id_asset]
with(mobileAS){ 

  var r,k,i, z={ np:0, pp:0, p:0, n:0, p_a:0, ne:0 };  //  	fc_task_issue 
  
  for(r=0;r<a.length;r++){   
    k=0;
    if(a[r].fc_task_issue_positive) k++, z.p++;   
    if(a[r].fc_task_issue_negative) k++, z.n++; 
    if(a[r].fc_task_issue_not_executable) k++, z.ne++;
    
  if(a[r].fc_task_issue_performed){  i=0; k++; z.pp++;
    if(a[r].fc_task_quality_good) z.p++,i=1;   
    if(a[r].fc_task_quality_not_acceptable) z.n++,i=1; 
    if(a[r].fc_task_quality_acceptable) z.p_a++;
    
    if(!TskMTH) { 
      if(!i) { z.p++; z.p_a=0; }       //  TskMTH=0  &  TskData=1     
    }
    
  }   
     
    if(a[r].fc_task_issue_not_performed || !k) z.np++;
  }
  return z;
}},



getResultTask:function(a){   // status of single task
with(mobileAS){

  var k=0,n,z=0;  
    if(a.fc_task_issue_positive) k++, n="Positive";   
    if(a.fc_task_issue_negative) k++, n="Negative"; 
      
    if(a.fc_task_issue_performed){ k++; n="Performed", z="Positive"; 
      if(a.fc_task_quality_good) n="Performed - Good", z="Positive";  
      if(a.fc_task_quality_acceptable) n="Performed - Acceptable", z="Positive";; 
      if(a.fc_task_quality_not_acceptable) n="Performed - Not acceptable", z="Negative";  
      
      if(!TskMTH && TskData) n=z;     
    }  
  
    if(a.fc_task_issue_not_executable) k++, n="Not executable"; 
    if(a.fc_task_issue_not_performed || !k) n="Not performed";        
 
  return n;
}},




setTaskResult:function(){   // v:  0-8  ||  0-12
with(mobileAS){

  var k,n,d,vv,j,r,rslt,v,f_id;
                
  v=parseInt($("#idsel_tsk").val());
  if(!v) return false;
  
  
  // set selected / all
  vv=v+((v&1)?1:0);
  rslt=(vv/2)-1; 
 
  if(v&1){  // selected
      for(r=0;r<TaskSEL.length;r++){
        
        d=TaskSEL[r][2];       // d:  -1 asset selection else posiz of task in array tempTask[f_id]
        f_id=TaskSEL[r][1];
 
        //alert(r+" | "+TaskSEL[r][2]+" | "+f_id)
 
            if(d<0) {
              f_id=TaskSEL[r][0];
              for(n=0;n<tempTask[f_id].length;n++){
                ClearSetRes(tempTask[f_id][n],rslt);
              }
            } else { 
                ClearSetRes(tempTask[f_id][d],rslt); 
            }
      
      }
  
  } else { // all
  
    for(k in tempTask){
      for(n=0;n<tempTask[k].length;n++){
          ClearSetRes(tempTask[k][n],rslt);
      }
    }
  }
  
  
  // set to choose status
  j=$("select#idsel_tsk");
  j[0].selectedIndex = 0;
  j.selectmenu("refresh");
  
  
  // redraw asset/task
  f_upd_viewAssetListWO();
}},



ClearSetRes:function(a,i){
with(mobileAS){

  var r,av,ai;  
  
    av=["fc_task_issue_not_performed","fc_task_issue_positive","fc_task_issue_negative","fc_task_issue_not_executable"];
    ai=["not_performed","positive","negative","not_executable"];
    
    av1=["fc_task_issue_not_performed","fc_task_issue_performed","fc_task_issue_not_executable", "fc_task_quality_good", "fc_task_quality_acceptable", 	"fc_task_quality_not_acceptable"];
   
   
  
  if(!TskMTH){ // old

    if(!TskData){  
    
      for(r=0;r<4;r++) a[ av[r] ]=0;
      a[ av[i] ]=1;
      a["fc_task_issue"]=ai[i];  // fc_task_issue
    
    } else { // new data
    
      for(r=0;r<6;r++) a[ av1[r] ]=0;
      if(i==0) a["fc_task_issue_not_performed"]=1;
      if(i==1) { a["fc_task_issue_performed"]=1; a["fc_task_quality_good"]=1; }
      if(i==2) { a["fc_task_issue_performed"]=1; a["fc_task_quality_not_acceptable"]=1; }
      if(i==3) a["fc_task_issue_not_executable"]=1;  
    }
  
  
  } else { // new

    for(r=0;r<6;r++) a[ av1[r] ]=0;
    if(i>2) { 
      a[ av1[1] ]=1; // performed
      a[ av1[i] ]=1;     
    } else a[ av1[i] ]=1;
  }

}},



f_checktaskAS:function(e,fid_task, isprnt, fid_asset,pos){ cancelEvent(e);
with(mobileAS){

  var s, i,n,j=e.target || e.srcElement;
  s=j.src;
  i=(s.indexOf("check2")==-1)?2:3;  
  j.src="img/bigcheck"+i+".png";
 
  if(i==3) TaskSEL.push( [fid_task,fid_asset,pos]  );
  else {  
        n=isTskChecked(fid_task);
        if(n!=-1) TaskSEL.splice(n,1);
  }
   
}},

 
isTskChecked:function(fid){
with(mobileAS){ 
  for(var r=0;r<TaskSEL.length;r++){
    if(TaskSEL[r][0]==fid) return r;
  }  
  return -1;
}}, 
 
 


f_Edit_Task:function(fid_task, fid_asset,i){ 
with(mobileAS){  
    
  Calendar.PGClose="#idtaskEditAS"; // back page for calendar
  
  $.mobile.changePage("#idtaskEditAS");
  EditTSK.viewForm(fid_task,fid_asset,i);
}},






f_confirm_deleteAS:function(e,f,r){ cancelEvent(e);
with(mobileAS){
  NOBACK=0;
  $('#idmsg_confirm').html( LANG("Remove asset?") );
  mobileWO.fncConfirm="mobileAS.f_delete_woasset("+f+","+r+")";
  $.mobile.changePage("#idpopupconfirm");
   
}},





f_delete_woasset:function(f,r){
with(mobileAS){  
  gridAS[r].status |= 2;
  f_upd_viewAssetListWO();
}},
 

// search asset

f_keyupAS:function(e,j,page){
with(mobileAS){

  Page=page;

  if(!e) e=window.event;
  var v, k=(e.which)?e.which:e.keyCode;

  // verify invalid characters

  if(k==13){  // enter
    v=j.value;
    f_call_readAS(v,0,0);
  }
}},



f_call_readAS:function(v,lim,mode){
with(mobileAS){ 
  
    if(!v) { f_upd_listsearchAS([],""); return; }
    
    aSELAS=[]; // deselect all
    
    Lim=lim;
    Mode=mode;
    
    mobileWO.f_show_loading();
 
if(!FILTER.length) FILTER=["f_title","f_description","f_code"];
TempAsset={};


// alert(v+" | "+FILTER+" | "+Lim+" | "+Offset)
 

    $.ajax({    
            url: staticUrl + '/mobile-wares/search-ware?ns=1',
            type: "post",
            data: {
                userId:    mobileWO.user_id,
                sessionId: mobileWO.session_id,
                version:   2.0,
                params:{ mode:Mode, search:v, filter:FILTER, limit:Lim, offset:Offset, type_id:1 }    
            },
            success:function(resp){   
            
                try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; } 
                if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
                 
                CountAS=jsn.params["total_count"]; 
                f_upd_listsearchAS(jsn.params["asset_list"],v);
                
                mobileWO.f_hide_loading();
                                                  
            },
            error:function(){ 
                $('#idmsg_alert').html("Trasmission error 08!");
                $.mobile.changePage("#idpopupalert");  
            }  
        });


}},



f_upd_listsearchAS:function(a,v){   // a=[ [ f_code, f_title, f_description, nchild ], ... ]
with(mobileAS){ 
  var r,s="";
  TempAsset={};
  if(!a) a=[];
  for(r=0;r<a.length;r++){
  
    TempAsset[ a[r][0] ]={ f_title:a[r][1] , f_description:a[r][2] };
    
    s+="<li data-theme='b' data-icon='false' style='overflow:hidden;'>"+
       "<a href='#' onclick='EditAS.viewLoadAS("+a[r][0]+","+(Page?1:0)+")'>"+
           "<img id='idckAS"+a[r][0]+"' src='img/bigcheck2.png' onclick='mobileAS.f_check_onoffAS(event,"+a[r][0]+"); return false;'>"+
           "<p style='float:left;'>"+ 
            "<p style='line-height:22px;'><b>"+a[r][1]+"</b>"+
           "</p>"+
            "<p style='color:#000;margin-bottom:-12px;'>"+a[r][2]+"&nbsp;</p>"+
           "</p>"+ 
           "<p class='ui-li-aside' style='width:80px;margin-top:-3px;'><strong>"+a[r][0]+"</strong></p>"+
        "</a>"+
        "<a href='#' onclick='"+(a[r][3]?"mobileAS.f_call_readAS("+a[r][0]+",0,1)":"")+"' data-theme='c' "+(a[r][3]?"data-icon='plus'":"")+" data-shadow='false' data-iconshadow='false'>"+a[r][3]+LANG(" Assets children")+"</a>"+
        "</li>";     
  }

  var sep=Mode?"":'"';
  
  if(v && (Lim+Offset)<CountAS) s+="<li data-theme='b' data-icon='arrow-d'><a "+
                              "onclick='mobileAS.f_call_readAS("+sep+v+sep+","+(Lim+Offset)+","+Mode+")' style='font-size:16px;'>"+LANG("Show more assets")+"</a></li>";



try{
  $('#idlistASsearch'+Page).html(s);
  $('#idlistASsearch'+Page).listview('refresh');
}catch(e){ alert(e); }

  // scrolltop
  $.mobile.silentScroll(0);
}},



f_check_onoffAS:function(e,fc){  cancelEvent(e);  
with(mobileAS){ 

 var i,n,s, j=e.target || e.srcElement;
  s=j.src;
  i=(s.indexOf("check2")==-1)?0:1;  
  j.src="img/bigcheck"+(i+2)+".png";
 
  if(i) aSELAS.push(fc);
  else {  
        n=aSELAS.indexOf(fc);
        if(n!=-1) aSELAS.splice(n,1);
  }
 
}},




f_run_moduleAS:function(){
with(mobileAS){

  if(!FooterBtnHome_asset) return;

  $.mobile.changePage("#idmoduleAS");
  Page="_mod";
  f_buildPanekfAS();
  
}},




f_buildPanekfAS:function(){  
with(mobileAS){

  var s=""; 
      
    if(Page){
       s+="<li style='margin-right:9px;'><a href='#idhomepage'>"+LANG("Home")+"</a></li>"+
          "<li style='margin-right:9px;'><a href='#' onclick='mobileAS.f_createWOfromAS()'>"+LANG("Create new work order")+"</a></li>";
    } else s+="<li style='margin-right:9px;'><a href='#idwoasset'>"+LANG("Back to asset list")+"</a></li>";
          
      s+="<li data-icon='"+(iFIL?"minus":"plus")+"' data-theme='d'><a  href='#' onclick='mobileAS.f_openclosefilterAS()'>"+LANG("Filter by")+"</a></li>";
          
        if(iFIL) s+="<li><fieldset data-role='controlgroup'>"+
          "<input type='radio' name='filterAS' id='filterAS-1' value='all' checked='checked' data-theme='b' onclick='mobileAS.f_radioFilterAS(0)' />"+
         	"<label for='filterAS-1'>"+LANG("All")+"</label>"+
         	"<input type='radio' name='filterAS' id='filterAS-2' value='f_title' data-theme='b' onclick='mobileAS.f_radioFilterAS(1)' />"+
         	"<label for='filterAS-2'>"+LANG("Title")+"</label>"+
         	"<input type='radio' name='filterAS' id='filterAS-3' value='f_description' data-theme='b' onclick='mobileAS.f_radioFilterAS(2)' />"+
         	"<label for='filterAS-3'>"+LANG("Description")+"</label>"+
         	"<input type='radio' name='filterAS' id='filterAS-4' value='f_code' data-theme='b' onclick='mobileAS.f_radioFilterAS(3)' />"+
         	"<label for='filterAS-4'>"+LANG("Code")+"</label>"+
          "</fieldset></li>";
                   
          
  FILTER=[];
  document.getElementById("idfilterAS_mod").innerHTML=LANG("All");        

try{ 
  $('#idlistAS'+Page).html(s);
  $('#idlistAS'+Page).trigger('create'); 
  $('#idlistAS'+Page).listview('refresh'); 
} catch(e){}


}},


f_openclosefilterAS:function(){  
with(mobileAS){

  iFIL=iFIL?0:1;
  f_buildPanekfAS();
}},



f_radioFilterAS:function(i){
with(mobileAS){
  
  var j=document.getElementById("idfilterAS"+Page),
      a=[ ["f_title","f_description","f_code"], ["f_title"], ["f_description"], ["f_code"] ];
  FILTER=a[i];
  
  if(!i) j.innerHTML=LANG("All");
  else j.innerHTML=a[i][0];
  
  f_closePanelAS();
}},




f_createWOfromAS:function(i){
with(mobileAS){

  // only one selected asset
  if(!aSELAS.length) {  
    NOBACK=0;
    $('#idmsg_alert').html( LANG("Select one or more assets") );
    $.mobile.changePage("#idpopupalert");
    return; 
  }

  var listas=Dbl.CloneA(aSELAS);
  EditAS.read_ware_AS( listas, 1, "mobileAS.f_createWOwithAS()");

}},

f_createWOwithAS:function(i){
with(mobileAS){
  var jlis=aSELAS.join(",");
  Editpg.viewForm(0,0,jlis); 
}},



f_closePanelAS:function(){
try{
  $("#idpanelAS_mod").panel("close"); 
} catch(e){}
},



f_back_woedit:function(){
with(mobileAS){
  $.mobile.changePage("#idwoedit");
  
  // idcounterAS
  var c=0;
  for(r=0;r<gridAS.length;r++) {
    if(gridAS[r].status&2) continue; 
    c++;
  }
  $('#idcounterAS').html(c);
}},



f_addAS:function(){
with(mobileAS){
  aSELAS=[];
  Lim=0;
  CountAS=0;
  Page="";   searc=1;
  $.mobile.changePage("#idsearchAS");
  
  document.getElementById("id_search_inputAS").value="";
  $('#idlistASsearch').html("");
  $('#idlistASsearch').listview('refresh');  
  iFIL=0;
  f_buildPanekfAS();
}},



f_addSelAS:function(i){   //  0 add    1=cancel
with(mobileAS){

  searc=0;
  if(!i){
  
      var r, f, ft,fd,n,listas=[];
      
      if(aSELAS.length){
        for(r=0;r<aSELAS.length;r++){
            f=aSELAS[r];
    
            if( f_ifingridAS(f) ) continue;
    
            // add in gridAS with status 1        
            ft=TempAsset[f].f_title;
            fd=TempAsset[f].f_description;
    
            gridAS.push( { f_id:f, f_title:ft, f_description:fd, status:1 }  ); 
        } 
        
        // load assets in local_db  // check if asset already loaded
        listas=Dbl.CloneA(aSELAS);
        EditAS.read_ware_AS( listas, 1, "");
        
      }
 
  }
  
  // ajax for loading asset table from mainsim
  
  $.mobile.changePage("#idwoasset");
  if(!i) f_upd_viewAssetListWO();
}},


f_ifingridAS:function(f){             // status: 0=ok  1=added  2=deleted exist   3=delete added
with(mobileAS){
  for(var r=0;r<gridAS.length;r++){
    if(f==gridAS[r].f_id) { 
      if(typeof(gridAS[r].status)=="undefined") gridAS[r].status=0;
      gridAS[r].status&=1;                                              // mask 
      return true; }
  }
  return false;
}},






//---------------------------------------------------------------------------------
/* Documents       

buildDoc      0 | 1     0=first execution
fc_doc_attach = 1392635936_1|Essex.pdf          ----

tempDocs:[

{ fcode:(t_wares  <0 -timestamp ),  timestamp , msim_name , oname, status }

f_code        t_wares code    < 0 (-timestamp) temp immage
tmsuser      fc_doc_attach   (timestamp_userId)
msim_name    name in t_creation_date
oname        original name in fc_doc_attach

status:      0=picture temp     1=from db local o mainsim    2=deleted (from mainsim)


]

*/

           
f_Edit_Documents:function(fc){      
with(mobileAS){
          
  var b=0;        
  try{ if(parent.inApp()) b=1; }catch(e){}                      
  if(IMEI) b=1;
  if(!b) $("#iddocs_Addbtn").hide();                               // !IMEI &&   (!IMEI)   hide Add if not in App
 
  $.mobile.changePage("#iddocs_page");
 
  
  if(buildDoc) return;
  buildDoc=1;
  
  var r,f, a,aa=[],ab=[],ac=[],ad=[], 
      str="", dcs=Editpg.AlistDC;
  
  
  tempDocs=[]; 
 
  if(dcs.length){
    // search documents in ware_wo and then in t_creation_date / t_wares
    ad=Dbl.query("SELECT t_ware_wo GET f_ware_id WHERE f_wo_id="+fc+" AND f_ware_id IN ("+dcs.join(",")+")");     // f_code (t_ware_wo)
    if(ad.length) {
    
      n=ad.length;
      for(r=0;r<n;r++) ac.push(ad[r].f_ware_id);                                                            // ac[]  f_code ware document
         
      a=Dbl.query("SELECT t_wares GET f_code, fc_doc_attach WHERE f_code IN ("+ac.join(",")+")");           // aa[f]  fc_doc_attach (t_wares)
      for(r=0;r<n;r++) aa[ a[r].f_code ]=a[r].fc_doc_attach;                                                 
      
      a=Dbl.query("SELECT t_creation_date GET f_id,f_title WHERE f_id IN ("+ac.join(",")+")");              // ab[f]  f_title   (t_creation_date)
      for(r=0;r<n;r++) { 
        ab[ a[r].f_id ]=a[r].f_title; 
      }                                                     
                                                                  
      for(r=0;r<n;r++) {                                                                                    
        f=ac[r];
        a=aa[f]; if(!a) a="|"; a=a.split("|");
                                                                                                          
        tempDocs.push( { f_code:f, tmsuser:a[0], oname:a[1], msim_name:ab[f], status:1 } );           
      }                                                                                                     
 
    }    
  } 

  f_drw_docs(fc);
}},



f_drw_docs:function(fc){
with(mobileAS){  
  if(!fc) fc=FC;
  
  // attachmentUrl = C:\wamp\www\mainsim3\attachments
  
  var sau="",pau="attachments", au=attachmentUrl.split("attachments");
  if(au.length>1) {
    au.shift();
    sau=au.join("attachments");
    pau+=(sau.replace(/\\/g,"/")); 
  }
  
  var r, n=0, fn, hr="",oc="",str="", ext="", pth=staticUrl + pau + "/doc/"; 
  
  for(r=0;r<tempDocs.length;r++) with(tempDocs[r]){ 
    if(status==2) continue;
    n++;
    if(f_code>0) {
    
      if(!oname) oname="";
      ext=(oname.split(".")).pop();   
      
      var pext=ext?".":"";
      
      hr=pth+f_code+"/"+tmsuser+"_doc"+pext+ext;     
      oc="onclick='mobileAS.f_viewDoc("+r+",\""+hr+"\",1)'";  
      
    }else{ 
    
      oc="onclick='mobileAS.f_viewDoc("+r+","+fc+",0)'";
    }
    
    fn=oname;
    if(!fn) fn=msim_name;
    
    str+="<li data-theme='b' ><a href='#' "+oc+"  >"+
          "<img src='img/trash-1.png' onclick='mobileAS.f_confirm_deleteDC(event,"+r+","+fc+"); return false;'>"+
          "<p style='float:left;font-size:16px;margin-top:8px;'>"+fn+"</p></a></li>";  
  }
  
  $('#idlistdocs').html(str);
  $('#idlistdocs').listview('refresh');
  
  $('#idcounterDC').html(n);
}},


f_viewDoc:function(i,fc,m,K){ // m=0  =>   i = index in tempDocs   fc = f_code WO                
with(mobileAS){             // m=1  =>   fc=url   
   
  var ext="jpg", f=tempDocs[i].f_code;
  
  if(!tempDocs[i].oname) tempDocs[i].oname="";
  ext=((tempDocs[i].oname).split(".")).pop();
  
  if(m){
    NOBACK=0;
    document.getElementById("iddocs_frameviewer_doc").src="";
    $.mobile.changePage("#iddocs_frameviewer");


      if(ext!="jpg" && ext!="png" && MAINURL) {
        fc=MAINURL + ( (fc.split("..")).pop() );
        fc="http://docs.google.com/viewer?embedded=true&url="+fc; 
          
        document.getElementById("iddocs_frameviewer_doc").src=fc; 
        return; 
      } 
 
 
 // attachments / multi      // http://docs.google.com/viewer?embedded=true&url=www.tonenge.com/attachments/doc/8649/14321978086250_4_8649_doc.pdf 
 //alert(fc)
       
    // check if documents exist in attachments   
    $.ajax({
        url: staticUrl + "/mobile-wares/check-document?ns=1",
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   2.0,
            params:{  path:fc }   
        },
        success:function(resp){
          if(resp!="ok"){
            fc="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAAAoCAYAAAA2cfJIAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAB00lEQVR42mL8//8/wygYuQAggJhGg2BkA4AAGk0AIxwABNBoAhjhACCARhPACAcAATSaAEY4AAig0QQwwgFAAI0mgBEOAAJoNAGMcAAQQKMJYIQDgAAaTQAjHAAE0GgCGOEAIIBGE8AIBwABNJoARjgACKDRBDDCAUAAsVDTMEZGRrxzy////2eEqYOxsfEHK4D5b6DdSs3wAwggFmo7jhiHDMbIJhSIQyWRkgoAAmi0ChjhACCAWAZjbkOvTohRC1JDSA8ueZg4riIenzw+O7H5E1mMUndTAwAEEMtgTJXk1HGE9ODjwyIBlx245KlRF5PjbmqGNUAAMQ32yEeOAHLbFeSaSQt3UsPd1AxvgABioUUEDpWG3yhgYAAIoAHpBVDanRwF1AMAATQo2wCjpQX9AEAADZlu4GipQBsAEEBMgzH3o0c2pYMwg8VMchqI2OygZokJEECDtgqgdt+XlP42sfYRMhObPKWJgNqJGSCAGEe3ho1sABBAo0PBIxwABNBoAhjhACCARhPACAcAATSaAEY4AAig0QQwwgFAAI0mgBEOAAJoNAGMcAAQQKMJYIQDgAAaTQAjHAAE0GgCGOEAIIBGE8AIBwABNJoARjgACDAA6VIcXFzurykAAAAASUVORK5CYII=";
          } 
          
           document.getElementById("iddocs_frameviewer_doc").src=fc;                                                        
        },
        error:function(){
           $('#idmsg_alert').html("Trasmission error 22");
           $.mobile.changePage("#idpopupalert");  
        }  
    });



 
  }else{

    if(f<0) {     
      NOBACK=0;
      $.mobile.changePage("#iddocs_viewer"); 
         
      try{ 
        var B=parent.retB64(-f);
        document.getElementById("iddocs_viewer_img").src= "data:image/"+ext+"-icon;base64,"+B;
      }catch(e){} 
      
      try{ document.getElementById("iddocs_viewer_img").src= "data:image/"+ext+"-icon;base64,"+window.JavascriptInterface.GetImage64(-f,ext); }catch(e){ /* fAlert("1316"); */ } 
    }
  }

}},






f_confirm_deleteDC:function(e,r,fc){   cancelEvent(e);
with(mobileAS){
  NOBACK=0;
  $('#idmsg_confirm').html( LANG("Remove attachment?") );
  mobileWO.fncConfirm="mobileAS.f_delete_doc("+r+","+fc+")";
  $.mobile.changePage("#idpopupconfirm"); 
}},

f_delete_doc:function(r,fc){  
with(mobileAS){

 try{ window.JavascriptInterface.DelImage(tempDocs[r].tmsuser); }catch(e){ /* fAlert("1334");*/ }
   
 if(!tempDocs[r].status) tempDocs.splice(r,1);
 else tempDocs[r].status=2;
 
 f_drw_docs(fc);
}},



addAttach:function(fc){ 
with(mobileAS){
  
  try{ parent.get_Pict();  } catch(e){}                                            // test -> call to setImageID  
  try{ window.JavascriptInterface.OpenCamera(); }catch(e){ /* fAlert("1348");*/ }
}}


} // end documents






//---------------------------------------------------------------------------------

function setImageID(aid,ext){  // timespamp of picture
with(mobileAS){

  var nm="picture_";

  if(!ext) ext="jpg";
  if(ext=="png") nm="signature_";
  
  if(aid){
    var pext=ext?".":"";
    tempDocs.push( { f_code:-aid, tmsuser:aid+"_"+mobileWO.user_id, oname:nm+aid+pext+ext, msim_name:-aid, status:0 } );     
  } 
  f_drw_docs();
}}
 




//---------------------------------------------------------------------------------------------------------------------------
// view edit asset

EditAS={ FC:0, UIF:[], tmp:[],


viewLoadAS:function(fc,md){  // md 0 WO   1 Module  2 to Home from nfc
with(EditAS){

  tmp=[fc,md];
  mobileWO.f_show_loading();
  setTimeout("EditAS.ActviewLoadAS()",200);
}},


ActviewLoadAS:function(){
with(EditAS){

  var fc=tmp[0], md=tmp[1];

  var a;  
  a=Dbl.query("SELECT t_creation_date GET * WHERE f_id="+fc);
 
  if(a.length) viewForm(fc,md);
  else {
      // call to read asset field of asset temps
      read_ware_AS([fc], 0, "EditAS.viewForm("+fc+","+md+")");
  }
    
  mobileWO.f_hide_loading();
}},

  

viewForm:function(fc,md){
with(EditAS){

   //alert(fc+" | "+md+" | "+mobileAS.Page)

  mobileAS.exitPage=md?"#idmoduleAS":"#idwoasset"; 
  if(md==2) mobileAS.exitPage="#idhomepage";

  var r,n,s,a,v,rw,ext,t,f,fidcode,fnm,uif,
      opt="", EL_TYPE="WARES", EL_TYPE_ID=1;
 
  FC=parseInt(fc); 
  f="Asset ("+(FC?Editpg.f_zero(FC):"")+")";

  // view in header
  $('#ideditASheader').html(f);

// find fields in t_ui_fields
  uif=Dbl.query("SELECT t_ui_fields GET * WHERE f_type='"+EL_TYPE+"' AND f_type_id IN (0,"+EL_TYPE_ID+")");    // 0 ALL type_id
  UIF=[];
 

  // find value
  for(r=0;r<uif.length;r++){  
    if(uif[r]["f_visibility"] & mobileWO.user_level){
      
      fnm=uif[r]["f_field_name"];
      t=uif[r]["f_table_ref"];
      
        if(FC){ 
          fidcode=(t=="t_creation_date" || t=="t_pair_cross")?"f_id":"f_code";
          a=Dbl.query("SELECT "+t+" GET "+fnm+" WHERE "+fidcode+"="+FC, "NUM");
          if(!a.length) { alert("value not found: "+t+" | "+fnm); continue; }
          v=a[0][0];    
        } else {
          n=Dbl.AtoN(t,fnm);
          v=Dbl.db[t].blank[n]; 
        }
        
      uif[r]["f_value"]=v; 
      UIF.push(uif[r]);
  }}

  $.mobile.changePage("#idvieweditAS");

  // innerHTML  form fields
  s=Editpg.f_build_form(UIF,"AS");

    //---------------------------------------------------------------------------
  // file and link
  s+="<br><ul id='idlistAttachs' data-role='listview' data-theme='b' data-split-icon='false' data-inset='false'>"+
        "<li onclick='EditAS.f_Edit_Attachs("+FC+")'>"+LANG("Files and links")+"<span id='idcounterATCH' data-count-theme='a' class='ui-li-count'></span></li>"+
     "</ul><br>&nbsp;";  



  // update/create page
  $('#idneweditAS').html(s);      
  $('#idvieweditAS').trigger('create'); 
  
  for(r=0;r<UIF.length;r++){
    fnm=UIF[r]["f_field_name"];
    if(UIF[r]["f_mandatory"]) $('#id_AS'+fnm).parent().css("border-color","#A1C24D"); 
  }
}},





f_backPage:function(pg){      
with(EditAS){

  if(!pg) pg="#idvieweditAS";
  $.mobile.changePage(pg);
  setTimeout( function(){ NOBACK=1; },500);

}},



f_Edit_Attachs:function(fc){      
with(EditAS){
 
  NOBACK=1; 
  $.mobile.changePage("#idatchs_page");
 
  tempAtchs=[];
 
    $.ajax({
        url: staticUrl + "/mobile-wares/read-attach?ns=1",
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   2.0,
            params:{ "f_code":fc }   
        },
        success:function(resp){
               
          try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; } 
 
              
          var ja=jsn.attach, ta={},f,a,ac;
          tempAtchs=[];
               
          var sau="",pau="attachments", au=attachmentUrl.split("attachments");
            
          if(au.length>1) {
              au.shift();
              sau=au.join("attachments");
              pau+=(sau.replace(/\\/g,"/")); 
          }
            
          var r, n=0, fn, hr="",oc="",str="", ext="", pth=staticUrl + pau + "/doc/"; 
            
          for(r=0;r<ja.length;r++) { 
              
             a=ja[r]["fc_doc_attach"];
             if(!a) a="";
             f=a.split("|");   
 
            if(!f[0]) f[0]="";
            if(!f[1]) f[1]="noname";
              
            if(!ja[r]["f_title"]) ja[r]["f_title"]="noname";  
              
              ta={ "f_code":ja[r]["f_code_ware_slave"], "tmsuser":f[0], "oname":f[1], "msim_name":ja[r]["f_title"], status:1 };           // tmsuser:a[0], oname:a[1], msim_name:ab[f], status:1 } );                             
              tempAtchs.push(ta);
                                                        
            
          n++;

                ext=(ta.oname.split(".")).pop();
                var pext=ext?".":"";
                
                hr=pth+ta.f_code+"/"+ta.tmsuser+"_doc"+pext+ext;     
                oc="onclick='EditAS.f_viewDoc("+r+",\""+hr+"\",1)'";  

   
          fn=ta.oname;
          if(!fn) fn=ta.msim_name;
              
              str+="<li data-theme='b' ><a href='#' "+oc+"  >"+
                    "<p style='float:left;font-size:16px;margin-top:8px;'>"+fn+"</p></a></li>";  
          }
            
            
      
            
          $('#idlistatchs').html(str);
          $('#idlistatchs').listview('refresh');
              
                                                          
        },
        error:function(){
           $('#idmsg_alert').html("Trasmission error attach");
           $.mobile.changePage("#idpopupalert");  
        }  
    });
 
}},


                    


f_viewDoc:function(i,fc,m){ //  m=1     i = index in tempAtchs  fc=url   
   
  var ext="jpg", f=tempAtchs[i].f_code;
  if(!tempAtchs[i].oname) tempAtchs[i].oname="";
  ext=((tempAtchs[i].oname).split(".")).pop();
                                     
  
 // alert(fc+"\n"+ext+"\n"+f)         // ../attachments/doc/81193/14659080089502_2_81193_doc.jpg
                                           
    NOBACK=0;
    document.getElementById("idatchs_frameviewer_doc").src="";
    $.mobile.changePage("#idatchs_frameviewer");


      if(ext!="jpg" && ext!="png" && MAINURL) {
        fc=MAINURL + ( (fc.split("..")).pop() );
        fc="http://docs.google.com/viewer?embedded=true&url="+fc; 
          
        document.getElementById("idatchs_frameviewer_doc").src=fc; 
        return; 
      } 
 
 
 // attachments / multi      // http://docs.google.com/viewer?embedded=true&url=www.tonenge.com/attachments/doc/8649/14321978086250_4_8649_doc.pdf 
 //alert(fc)
       
    // check if documents exist in attachments   
    $.ajax({
        url: staticUrl + "/mobile-wares/check-document?ns=1",
        type: "post",
        data: {
            userId:    mobileWO.user_id,
            sessionId: mobileWO.session_id,
            version:   2.0,
            params:{  path:fc }   
        },
        success:function(resp){
          if(resp!="ok"){
            fc="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAAAoCAYAAAA2cfJIAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAB00lEQVR42mL8//8/wygYuQAggJhGg2BkA4AAGk0AIxwABNBoAhjhACCARhPACAcAATSaAEY4AAig0QQwwgFAAI0mgBEOAAJoNAGMcAAQQKMJYIQDgAAaTQAjHAAE0GgCGOEAIIBGE8AIBwABNJoARjgACKDRBDDCAUAAsVDTMEZGRrxzy////2eEqYOxsfEHK4D5b6DdSs3wAwggFmo7jhiHDMbIJhSIQyWRkgoAAmi0ChjhACCAWAZjbkOvTohRC1JDSA8ueZg4riIenzw+O7H5E1mMUndTAwAEEMtgTJXk1HGE9ODjwyIBlx245KlRF5PjbmqGNUAAMQ32yEeOAHLbFeSaSQt3UsPd1AxvgABioUUEDpWG3yhgYAAIoAHpBVDanRwF1AMAATQo2wCjpQX9AEAADZlu4GipQBsAEEBMgzH3o0c2pYMwg8VMchqI2OygZokJEECDtgqgdt+XlP42sfYRMhObPKWJgNqJGSCAGEe3ho1sABBAo0PBIxwABNBoAhjhACCARhPACAcAATSaAEY4AAig0QQwwgFAAI0mgBEOAAJoNAGMcAAQQKMJYIQDgAAaTQAjHAAE0GgCGOEAIIBGE8AIBwABNJoARjgACDAA6VIcXFzurykAAAAASUVORK5CYII=";
          } 
          
           document.getElementById("idatchs_frameviewer_doc").src=fc;                                                        
        },
        error:function(){
           $('#idmsg_alert').html("Trasmission error 22");
           $.mobile.changePage("#idpopupalert");  
        }  
    });

 

},










                    

/* 
  afc= array f_code  
  tst: 0 don't verify if codes exist in local    
  fnc = function to call after loading
*/
read_ware_AS:function(afc, tst, fnc){   
with(EditAS){

  var a,r,f,n;
  
  if(tst){ 
    a=Dbl.query("SELECT t_creation_date GET f_id WHERE f_id IN ("+afc.join(',')+")");
    for(r=0;r<a.length;r++){
      n=afc.indexOf(a[r].f_id);
      afc.splice(n,1);
    }  
  }

  if(!afc.length) { if(fnc) eval(fnc);  return; }

  mobileWO.f_show_loading();

    $.ajax({    
            url: staticUrl + '/mobile-wares/read-wares?ns=1',
            type: "post",
            data: {
                userId:    mobileWO.user_id,
                sessionId: mobileWO.session_id,
                version:   2.0,
                params:{ code_list:afc }   
            },
            success:function(resp){   
                try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; } 
                if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
                 
                // jsn.params  
                insert_ware_AS(jsn.params);
                if(fnc) eval(fnc);
                 
                mobileWO.f_hide_loading(); 
                                                  
            },
            error:function(){ 
                $('#idmsg_alert').html("Trasmission error 09!");
                $.mobile.changePage("#idpopupalert");  
            }  
    });

}},


// t_creation_date
// t_wares
// t_ware_parent    // with parent unto root


insert_ware_AS:function(p){ 
  var r,n,i,tabname;  
    with(Dbl){
      for(r in p){    
        tabname=r;
        if(!p[r].length) continue;
        query("INSERT "+tabname, p[r]);   
   }}    
},



f_closeEdit:function(){
with(EditAS){
 
  
  rf=mobileAS.exitPage;   
  
  
  //alert(rf+"\n"+mobileAS.searc+ " " + typeof(mobileAS.searc) )
  
  
  if(mobileAS.searc) rf="#idsearchAS";  else mobileAS.searc=0;     
  
  
   
  $.mobile.changePage(rf);
}} 







} // end edit asset







//---------------------------------------------------------------------------------------------------------------------------
// view edit task

EditTSK={ FC:0, UIF:[], V:{},


viewForm:function(fc,fa,i){
with(EditTSK){

  var r,n,k,kk,s,a,v,rw,ext,t,f,fidcode,fnm,uif,
      opt="", EL_TYPE="TASKS", EL_TYPE_ID=0;
 
  FC=parseInt(fc); 
  f=" ("+FC+")";

  // view in header
  $('#idtaskeditASheader').html(f);

// find fields in t_ui_fields
  uif=Dbl.query("SELECT t_ui_fields GET * WHERE f_type='"+EL_TYPE+"' AND f_type_id="+EL_TYPE_ID);    // 0 ALL type_id
  UIF=[];
 
 
  // find value
  V=mobileAS.tempTask[fa][i];
 
 
 // title
 for(r=0;r<uif.length;r++){  
    fnm=uif[r]["f_field_name"]; 
    if(fnm=="f_title") {  
      uif[r]["f_value"]=V[fnm]; 
      UIF.push(uif[r]);
      break;
 }}
 
 
 // issue task
  k=0;  // not performed default
 
 if(!mobileAS.TskMTH){   // old periodic
 
  // radio
  if(!mobileAS.TskData){ 
    if(V.fc_task_issue_positive) k=1;   
    if(V.fc_task_issue_negative) k=2; 
    if(V.fc_task_issue_not_executable) k=3; 
    
  }else{
  
    if(V.fc_task_issue_not_executable) k=3;
  
    if(V.fc_task_issue_performed){
      k=1;
      if(V.fc_task_quality_good) k=1;  
      if(V.fc_task_quality_acceptable) k=1; 
      if(V.fc_task_quality_not_acceptable) k=2; 
    }
  }
 
  UIF.push({  f_field_type:"radio",	f_field_name:"fstatusNPE",	f_label:LANG("Check status"),	
              f_options:'[{"value":0,"label":"'+LANG("Not performed (np)")+'"},{"value":1,"label":"'+LANG("Positive (p)")+'"},{"value":2,"label":"'+LANG("Negative (n)")+'"},{"value":"3","label":"'+LANG("Not executable (ne)")+'"}]',
             	f_visibility:"-1",	f_editability:0,	f_mandatory:0,	f_selector_visibility:"",	f_table_ref:"",	f_check_exp:"", f_value:k });
 
 
 } else {  // new periodic + inspection
 
  // 2 select 
  kk=0; kkr=0;
  if(V.fc_task_issue_not_executable) k=2;
  
  if(V.fc_task_issue_performed){
    k=1, kkr=-1;
    if(V.fc_task_quality_good) kk=1;  
    if(V.fc_task_quality_acceptable) kk=2; 
    if(V.fc_task_quality_not_acceptable) kk=3;  
  }

   
  UIF.push({  f_field_type:"select",	f_field_name:"fstatusNPE",	f_label:LANG("Issue"),	
              f_options:'[{"value":0,"label":"'+LANG("Not performed (np)")+'"},{"value":1,"label":"'+LANG("Performed (p)")+'"},{"value":2,"label":"'+LANG("Not executable (ne)")+'"}]',
             	f_visibility:"-1",	f_editability:-1,	f_mandatory:0,	f_selector_visibility:"",	f_table_ref:"",	f_check_exp:"", f_value:k, f_onchange:'EditTSK.f_chg_tsk_issue(this);' });
   
 
  UIF.push({  f_field_type:"select",	f_field_name:"qualityNPE",	f_label:LANG("Quality"),	
              f_options:'[{"value":0,"label":"'+LANG("Choose")+'"},{"value":1,"label":"'+LANG("Good")+'"},{"value":2,"label":"'+LANG("Acceptable")+'"},{"value":3,"label":"'+LANG("Not acceptable")+'"}]',
             	f_visibility:"-1",	f_editability:kkr,	f_mandatory:0,	f_selector_visibility:"",	f_table_ref:"",	f_check_exp:"", f_value:kk }); 
 }
 
 
  // other fields
  for(r=0;r<uif.length;r++){    
    fnm=uif[r]["f_field_name"];  
    if(fnm=="fc_task_issue_not_performed" || fnm=="fc_task_issue_positive" || fnm=="fc_task_issue_negative" || fnm=="fc_task_issue_not_executable" || fnm=="f_title") continue;
    if(uif[r]["f_visibility"] & mobileWO.user_level){    
      uif[r]["f_value"]=V[fnm]; 
      UIF.push(uif[r]);
  }}

  

  // innerHTML  form fields
  s=Editpg.f_build_form(UIF,"TSK");
  
  
  
  // t_task_readonly fields   
   try{ 
  
    var lk,Af, mtt=mobileAS.tempTask[fa][i].f_code_cross,  
        afcro=Dbl.query("SELECT t_task_readonly GET * WHERE f_code="+mtt);
        
    if(afcro.length) {
      Af=afcro[0];
      for(k in Af){ if(k=="f_code") continue;
      lk=k.replace(/_/g, ' ');
      s+="<div style='margin-top:-8px;margin-bottom:-16px;'>"+
         "<label for='id_tskrdo"+k+"' style='margin-bottom:-8px;font-size:14px;font-weight:bold;'>"+lk+"</label>"+
         "<textarea id='id_tskrdo"+k+"' readonly>"+Af[k]+"</textarea>"+
         "<br></div>"; 
    }}
  
    }catch(e){}


  // update/create page
  $('#idtaskformeditAS').html(s);      
  $('#idtaskEditAS').trigger('create'); 
  
  for(r=0;r<UIF.length;r++){
    fnm=UIF[r]["f_field_name"];
    if(UIF[r]["f_mandatory"]) $('#id_AS'+fnm).parent().css("border-color","#A1C24D"); 
  }
}},



f_chg_tsk_issue:function(isj){ 
with(EditTSK){

  var r,k=parseInt(isj.value), qj=document.getElementById("id_TSKqualityNPE"),
      opt="", aopt=["Choose","Good","Acceptable","Not acceptable"];
      
  for(r=0;r<4;r++) opt+="<option value='"+r+"'>"+LANG(aopt[r])+"</option>"; 
  $('#id_TSKqualityNPE').empty().append(opt).selectmenu('refresh');
  
  
  if(k==1) {
    $("#id_TSKqualityNPE").selectmenu('enable');;
  } else {
    $("#id_TSKqualityNPE").selectmenu('disable');
  }
  
}},





f_saveEdTask:function(){ 
with(EditTSK){

  var r,n,v,m,fnm,ft,er=[],vj,
      av=["fc_task_issue_not_performed","fc_task_issue_positive","fc_task_issue_negative","fc_task_issue_not_executable"],
      av0=["fc_task_issue_not_performed","fc_task_issue_performed","fc_task_issue_not_executable"],
      av1=["", "fc_task_quality_good", "fc_task_quality_acceptable", 	"fc_task_quality_not_acceptable"];
  
  
  for(n=0;n<UIF.length;n++){   
   fnm=UIF[n]["f_field_name"];  
   
   if( (mobileAS.TskMTH && mobileAS.TskData) || (fnm!="fstatusNPE" && fnm!="qualityNPE") ) {  // test other fields
  
        ft=UIF[n]["f_field_type"];
        if(ft=="image") continue; 
        
        v=$('#id_TSK'+fnm).val();
        m=UIF[n]["f_mandatory"];
        
        $('#id_TSK'+fnm).parent().css("border-color",m?"#A1C24D":"#60828D");                // reset border color
    
        if(ft=="datetime" || ft=="date") { v=Editpg.date_to_tms(v); if(v===false) { er.push(fnm); continue; } }
        if(ft=="time" || ft=="date") { v=Editpg.time_to_tms(v); if(v===false) { er.push(fnm); continue; }  }
        
        if(typeof(v)=="undefined") { v=""; }
        if(m && v=="") { er.push(fnm); continue; }
        
        if( !moCheck.check(v,UIF[n]["f_check_exp"]) ) er.push(fnm); 
        
        UIF[n]["f_value"]=v;
   }
 
  } // end for
 
  
  if(er.length){ 
     NOBACK=0; 
     $('#idmsg_alert').html( LANG("Check fields before saving") );
     $.mobile.changePage("#idpopupalert");  
     for(n=0;n<er.length;n++){ $('#id_TSK'+er[n]).parent().css("border-color","#FF0000"); }    
    return false;
  }
  
   
  // update value in V
  for(n=0;n<UIF.length;n++){  
    fnm=UIF[n]["f_field_name"];
    
      if(fnm=="fstatusNPE" || fnm=="qualityNPE"){
      
        if(!mobileAS.TskMTH){ // old
        
          
            for(r=0;r<4;r++) { 
              vj=document.getElementById("id_TSK"+fnm+r); 
              v=(vj.checked)?1:0; 
              
              if(!mobileAS.TskData) {
                V[ av[r] ]=v; 
              } else {
                
                if(!r) for(var rq=1;rq<4;rq++) V[ av1[rq] ]=0; // reset quality  
                
                if(!r || r==3) { 
                  V[ av[r] ]=v; if(v) V["fc_task_issue_performed"]=0;  
                
                } else {
                  if(r==1 && v) { 
                    V["fc_task_issue_performed"]=1;          // positive 
                    V["fc_task_quality_good"]=1;   
                  }
                  if(r==2 && v) { 
                    V["fc_task_issue_performed"]=1;          // negative 
                    V["fc_task_quality_not_acceptable"]=1;   
                  }
                }
              }
            }
           
          
          
        } else {                              // new
           v=$('#id_TSK'+fnm).val();
           if(fnm=="fstatusNPE"){
            for(r=0;r<3;r++) V[ av0[r] ]=(v==r)?1:0;   // issue
           } else {
            for(r=1;r<4;r++) V[ av1[r] ]=(v==r)?1:0;   // quality
           }
        } 
    } else V[fnm]= UIF[n]["f_value"];   
    
  }
  
  // timestamp
  V["f_timestamp"]=WOchange.time();
   
  f_closetask();
  
  // redraw list task updated
  mobileAS.f_upd_viewAssetListWO(); 
  
}},


f_closetask:function(){
  Calendar.PGClose="";
  $.mobile.changePage("#idwoasset" );   // , { transition: "slide", reverse:true }
}
 

} // end EditTSK











// delete wares not used after sync (download or upload)    // Clean pair_cross ????
function cleaningWARE(){
with(Dbl){

  return
  if(db["t_creation_date"].data.length < 32) return; 
 
  var r,V=[],D,a,b=[],s;
 
  try { D=db["temp_grid_wo"].data; }catch(e){ return; }
  for(r=0;r<D.length;r++){
    if(D[r]==null) continue;
    V.push(D[r][2]);          // f_codes WO downloaded
  }
  a=query("SELECT t_ware_wo f_ware_id WHERE f_wo_id IN ("+V.join(",")+")","NUM");  
  for(r=0;r<a.length;r++) { b.push(a[r][0]); V.push(a[r][0]); }
  if(b.length) s=" NOTIN ("+b.join(",")+")";

  query("DELETE t_creation_date WHERE f_id NOTIN ("+V.join(",")+")");
  query("DELETE t_custom_fields WHERE " + ( s?("f_code"+s) : "1" ) );
  query("DELETE t_wares WHERE " + ( s?("f_code"+s) : "1" ) );  
  query("DELETE t_wares_parent WHERE " + ( s?("f_code"+s) : "1" ) ); 
}}








