/* calendar date time picker

mode:  1 date   2 time   3 datetime


*/


Calendar={ Mode:3, Val:{}, Idf:"", Str:"", ww:80, VV:[0,0,0,0,0],
           J:null, fnt:"font:bold 13px opensansR,Arial;", 
           W:0, PGClose:"", FNM:"", NN:0, V:0, IK:"",
 month:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
 

View:function(mode,ik,label,fnm,n){
with(Calendar){
  
  Mode=mode; 
  Idf="id_"+ik+fnm;
  FNM=fnm;
  NN=n;
  IK=ik;
  
  var v=document.getElementById(Idf).value;
  if(!v) value=parseInt((new Date().getTime())/1000);  // now
  else value=Editpg.date_to_tms(v);
  
  
  Val=f_tms_date(value);
  J=document.getElementById("id_dtm_content");
 
  W=jsW();
  var nw=parseInt((W-40)/6);
  if(nw<ww) ww=nw; 


  Str="";
  if(Mode&1) DrawDate();
  if(Mode&2) DrawTime();

  J.innerHTML=Str;

  document.getElementById("id_dtm_label").innerHTML=label;
  $.mobile.changePage("#id_pagedatetime", { transition: "slide"});
 
}},


jsW:function(){
if (window.innerWidth!=window.undefined) return window.innerWidth;
if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth;
if (document.body) return document.body.clientWidth;
},

DrawDate:function(){
with(Calendar){

  Str+="<div style='position:absolute;left:10px;top:60px;"+fnt+"'>"+LANG("Date:")+"</div>"+
       "<div>"+
       f_Sel(Val.DD,10,80,2,1,31,1,0)+" "+
       f_Sel(Val.MM,20+ww*2,80,2,1,12,1,1)+" "+
       f_Sel(Val.YY,30+ww*4,80,2,1970,2038,1,2)+
       "</div>";

}},





DrawTime:function(){
with(Calendar){
  
  var tt=(Mode&1)?228:60;
  Str+="<div style='position:absolute;left:10px;top:"+tt+"px;"+fnt+"'>"+LANG("Time:")+"</div>"+
       "<div>"+
       f_Sel(Val.hh,10,tt+20,2,0,23,1,3)+" "+
       f_Sel(Val.mm,20+ww*2,tt+20,2,0,45,15,4)+" "+
       "</div>";



}},



f_Sel:function(v,x,y,cf,mi,mx,stp,idd){
with(Calendar){

  if(!stp) stp=1;
  var rv,s,st="style='position:absolute;left:"+x+"px;width:"+(ww*cf)+"px;height:50px;text-align:center;background-color:#D0D0D0;overflow:hidden;";

  rv=(mx!=12)?Editpg.f_zero(v,cf):LANG(month[v-1]);
  

  s="<div "+st+"top:"+y+"px;' onclick='Calendar.f_incdec("+stp+","+mi+","+mx+","+idd+","+cf+")'>"+
    "<img src='img/add.png' style='margin:5px;'></div>"+
    "<div id='id_tms_sel"+idd+"' "+st+"top:"+(y+45)+"px;font:28px opensansR,Arial;background-color:#FFF;'>"+rv+"</div>"+
    "<div "+st+"top:"+(y+80)+"px;' onclick='Calendar.f_incdec("+(stp*-1)+","+mi+","+mx+","+idd+","+cf+")'>"+
    "<img src='img/sub.png' style='margin:5px;' ></div>";

  VV[idd]=v;

  return s;
}},



f_incdec:function(stp,mi,mx,idd,cf){
with(Calendar){

  var rv, v=VV[idd];
  v+=stp; 
  if(v>mx) v=mi;
  if(v<mi) v=mx;
  VV[idd]=v;
  
  rv=(mx!=12)?Editpg.f_zero(v,cf):LANG(month[v-1]);
  document.getElementById("id_tms_sel"+idd).innerHTML=rv;  
}},



Set:function(){
with(Calendar){

  var t,dta=new Date(VV[2],VV[1]-1,VV[0],VV[3],VV[4],0);
  t=parseInt((dta.getTime())/1000);

  //alert(t+" | "+Editpg.tms_to_date(t,1) )
  V=Editpg.tms_to_date(t,1);
  document.getElementById(Idf).value=V;
  V=(Mode==2)?Editpg.time_to_tms(V):Editpg.date_to_tms(V);
  Close(1);
}},


Clear:function(){
with(Calendar){
  V=0;
  document.getElementById(Idf).value="";
  Close(1);
}},


Close:function(i){
with(Calendar){
  
  // script onchange
  if(i && !IK) sysScript.ED_script(FNM,NN,V);
  
  if(!PGClose) PGClose="#idwoedit";
  $.mobile.changePage(PGClose); 
}},



f_tms_date:function(v){

  if(v<=86400) {
    var mm=parseInt((v%3600)/60);
    mm=(parseInt(mm/15))*15;
    
    return { DD:0,MM:0,YY:0, hh:parseInt(v/3600), mm:mm };
  }
  
  var dt=new Date(v*1000), mm=parseInt(dt.getMinutes()); 
  mm=(parseInt(mm/15))*15;

  return { DD:parseInt(dt.getDate()), MM:parseInt(dt.getMonth())+1, YY:parseInt(dt.getFullYear()), hh:parseInt(dt.getHours()), mm:mm };
}



} // end