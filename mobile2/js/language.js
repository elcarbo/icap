// language en -> ita

// login.html
/*
"Browser does not support html5!",
"Username",
"Password",
"Login",
"Login message",
"Ok",
"Cancel",
*/



langwords=[


// index.html
"Action denied!",
"Start desktop version",
"Always start with desktop version",
"Settings",
"Logout",
"Welcome",
"Work orders",
"Filter by:",
"All",
"Element not found",
"Workflow",
"Save",
"Alert",
"Confirm",
"Yes",
"Reset wizard",
"Change phase",
"Back",
"Add",
"Cancel",
"Back to asset list",
"Select asset",
"Filter items...",
"Fullscreen",


// wo.js
"Summary WO",
"Upload selected WO",
"Lock selected",
"Unlock selected",
"Search",
"New work order",
"Select/unselect all",
"Summary",
"Category",
"Priority",
"Are you sure you want to logout?",
"Requested",
"Requested",
"Approval",
"Execution",
"Completion",
"Suspention",
"Closing",
"Deleting",
"Cloned",

// wochange.js
"Some work orders are not lockable!",
"Work orders are not lockable!",
"Some work orders are not unlockable!",
"Connection is required to complete this action",


// edit.js
"New WO",
"Edit (WO ",
" (DD/MM/YYYY)",
" (hh:min)",
" (DD/MM/YYYY - hh:min)",
"Check fields before saving",
"Work order relations",
"WO relations",

// asset.js
"Show tasks",
"Remove asset?",
"Show more assets",
"Create new work order",
"Back to asset list",
"Filter by",
"Title",
"Description",
"Code",
"Select one or more assets",
"Check status",
"Not performed (np)",
"Positive (p)",
"Negative (n)",
"Not executable (ne)",
"Choose",


// select & checkbox
"Choose status",
"Change selected in Not performed",
"All Not performed",
"Change selected in Positive",
"All Positive",
"Change selected in Negative",
"All Negative",
"Change selected in Not executable",
"All Not executable",
"Not performed",
"Negative",
"Positive",
"Not executable",
"Change selected in Not performed",
"All Not performed",
"Change selected in Performed",
"All Performed", 
"Change selected in Performed Good",
"All Performed Good",
"Change selected in Performed Acceptable",
"All Performed Acceptable",
"Change selected in Performed Not acceptable",
"All Performed Not acceptable",


// other
"Read barcode / qrcode",
"Download work orders",

// nfc
"Actions:",
"Check/create WO",
"Check asset",
"Offline NFC code not found",
"Exit from asset tab",
"NFC timeout error!",
"Add asset to list?",
"Asset not found!",
"Do you want to download more work orders with selected asset?",
"Do you want to create new work order with selected asset?",
"Code not found!",
"NFC error!",
"No work order available!",
"Enter code:",
"Set",
"Not performed",
"Corrective",
"Planned maintenance",
"Emergency",
"On condition",
"Planned Inspection",
"Meter",

"Files and links",

"Remove attachment?",
"Image",
"Compile all tasks before save the work order!",


"Add signature",
"Work order",
"Date",
"Parent",
"Children",
"Brothers",

"CORRECTIVE",
"PLANNED",
"INSPECTION",
"ON CONDITION",
"METER READING",
"EMERGENCY",
"STANDARD WORKORDER",

"Issue",
"Quality",
"Good",
"Acceptable",
"Not acceptable",
"Not executable",
"Performed",
"Performed - Good",
"Performed - Acceptable",
"Performed - Not acceptable",

"Performed (p)",
"Not performed (np)",

"Clear",
"Date:",
"Time:",
"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec",

// wizard
"Next",
"Find",
"Do you confirm the creation of the work order?",
"Exit",
"Request from wizard",
"Create new request",

"Change selected in Performed Acceptable",
"All Performed Acceptable",
"Change selected in Performed Not acceptable",,
"All Performed Not acceptable",
"Change selected in Performed",


// system script
"Warning: end cannot come before start!",
"Warning: end cannot come without start!"      // new


];