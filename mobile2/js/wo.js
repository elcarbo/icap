// mobile WORKORDERS


// CONFIG default
SETTINGS={};


NoAccessMsg="";             // DEPRECATED  =>  UNDER_MAINTENANCE           //  if != "" mobile is locked and show message in string   

MAINURL="";                // to show pdf attachment  es: "test.mainsim.com/ciana"   
MOBILE_DESKTOP_LOGIN=0;    // 0 do noto show button and setting to switch desktop|mobile version
NOTaskOpt=[];              // 5,6 => remove from select negative options
NOTaskOpt_quality=[];
usrWizLev=0;               // 0 disabled
usrNewWoLev=-1;            // -1 All  0 disabled  new WO button in panel
usrSignLev=0;              // 0 disable signature in document
DisWoPhaseChangeType=[];   // array of type disabled in change phase wo multiple
MaxOffWoWiz=3;             // Max number of Wizard Request Offline
FooterBtnHome_asset=0;     // show buttons home
FooterBtnHome_wizard=0; 
DateWoList="";              // default=""  date in WO list   "table:fieldlist" 
NphaseTaskChecked=0;        // N. of phase where task must be checked (closed is always checked)
TaskMethods=0;              // task rappresentation  0=auto   1=periodic   2=inspection and periodic   
 
attachmentUrl=""; 
 
mScript={ Save:function(typeid){} }   // Script al save WO


ONWIZ=0;                   // 1 = only wizard     
NTRE01=0; 
 
function WizWinObj(j){ mobileWO.wizj=j;  }
 
mobileWO = { M:"mobileWO",
             indexCode:{}, RFT:{},
             gridWO:[],GROUP:[],
             FILTER:[], SORT:"", filter:[], NFCFilter:"",
             wizj:null,
             fcodeTOprogress:{},                 //  f_code to fc_progress
             First:0,                            // never execute
    session_id:"",            // user params in database vars
    user_id:0,
    user_name:"",
    user_type:0,
    first_name:"",
    last_name:"",
    user_mail:"",
    user_phone:"",
    user_level:0,
    user_admin:0, 
    id_watch:null, Lat:0,Lon:0, 
        
             iSUM:-1, iCAT:-1, iPRI:-1,
             ackSEL:[],
             arrPDR:[],
             fncConfirm:"", 
             nofncConfirm:"", 
             
    noTruncateTable:["t_ui_fields","t_workorders_types","t_workflows","t_wf_phases","t_wf_groups","t_wf_exits","t_users"],
                 
//-----------------------------------------
// geo position

GSS:0,

geo_start_stop:function(){
with(mobileWO){ 
  if(GSS) geo_suspend();
  else geo_start();

}},

geo_start:function(){
with(mobileWO){  
  id_watch=navigator.geolocation.watchPosition(geo_success);
}},


geo_success:function(position){
with(mobileWO){ 
    Lat=position.coords.latitude, Lon=position.coords.longitude;
    GSS=1;
}},
   
geo_suspend:function(){
with(mobileWO){ 
    navigator.geolocation.clearWatch(id_watch);
    id_watch=null, Lat=0, Lon=0;
    GSS=0;
}},                 
                 

//--------------------------------------------------------------------------------------------------------
// SYNC




f_user_start:function(){  // start home page if user exist and verify database structure
with(mobileWO){ 
 
  try{ var i=isTouch;  }catch(e){ isTouch=0; } 

  // check support
  if(!window.localStorage) { 
    alert('Browser does not support html5!');
    location.href="login.html";
    return false; 
  }

  var conn=Dbl.connect_db("db_mainsim_mobile");
  if(!conn) f_exit();
  
  var lo=location.href, alo; 
  
  alo=lo.split("#");
  if(alo.length>1) { location.href=alo[0]; return; }
  
  
  // verify account exist
  with(Dbl){
      session_id=GETVAR("session_id");
      user_id=GETVAR("user_id");
      user_level=GETVAR("user_level");
      user_name=GETVAR("f_username");
      user_type=GETVAR("f_usertype");
      first_name=GETVAR("f_name");
      last_name=GETVAR("f_surname");
      user_mail=GETVAR("user_mail"); 
      user_phone=GETVAR("user_phone");  
      user_admin=GETVAR("f_admin");
      IMEI=GETVAR("imei");   
  }
  if(!session_id || !user_id) f_exit();
 
  return true;
}},



f_settings:function(){
with(mobileWO){

  SETTINGS=Dbl.GETVAR("settings");
  if(!SETTINGS) SETTINGS={};

  
  var set1j=document.getElementById("id_span_setting1");
  if(!IMEI) document.getElementById("idbtnfullscreen").style.display="block";


MAINURL=f_set_to_glb("mobile_main_url","");                                  // to show pdf attachment  es: "test.mainsim.com/ciana"   
MOBILE_DESKTOP_LOGIN=parseInt(f_set_to_glb("mobile_to_desktop",0));                    // 0 do noto show button and setting to switch desktop|mobile version
NOTaskOpt=f_set_to_glb("mobile_no_task_options",[]);                         // 5,6 => remove from select negative options
NOTaskOpt_quality=f_set_to_glb("mobile_no_task_options_quality",[]);
usrWizLev=f_set_to_glb("mobile_user_wizard_level",1);                                 // 0 disabled
usrNewWoLev=parseInt(f_set_to_glb("mobile_new_wo_button_level",-1));                            // -1 All  0 disabled  new WO button in panel
usrSignLev=parseInt(f_set_to_glb("mobile_signature_level",-1));                                  // 0 disable signature in document
DisWoPhaseChangeType=f_set_to_glb("mobile_disable_types_multi_change_phase_wo",[]);   // array of type disabled in change phase wo multiple
MaxOffWoWiz=f_set_to_glb("mobile_max_wizard_offline_wo",3);                           // Max number of Wizard Request Offline
FooterBtnHome_asset=parseInt(f_set_to_glb("mobile_footer_home_asset",1));                      // 1=show  0=hide   buttons home
FooterBtnHome_wizard=parseInt(f_set_to_glb("mobile_footer_home_wizard",1)); 
DateWoList=f_set_to_glb("mobile_date_wo_list","");                                    // default=""  date in WO list   "table:fieldlist" 
NphaseTaskChecked=f_set_to_glb("mobile_nphase_task_checked",0);                       // N. of phase where task must be checked (closed is always checked)
TaskMethods=parseInt(f_set_to_glb("mobile_taskview_method",0));                                                // task rappresentation  0=auto   1=periodic   2=inspection and periodic   

attachmentUrl=f_set_to_glb("attachment_url","");   

//alert("settings: "+typeof(SETTINGS["mobile_scripts"]) );

if(parseInt(SETTINGS["fc_usr_mobile_autoswitch_desktop"]) && (MOBILE_DESKTOP_LOGIN & user_level) ) { f_switch(); return; }

if(FooterBtnHome_wizard) document.getElementById("id_FooterBtnHome_wizard").style.color="#FFF";
if(FooterBtnHome_asset) document.getElementById("id_FooterBtnHome_asset").style.color="#FFF";
 
if(typeof(SETTINGS["mobile_scripts"])=="undefined" || !SETTINGS["mobile_scripts"]){
  mScript={ Save:function(typeid){} };
} else {

  var msc="mScript="+SETTINGS["mobile_scripts"];   // Base64.decode()
 // alert(msc)
  eval(msc);

}




// if user with only wizard
if(user_level>-1 && (user_level & usrWizLev) && FooterBtnHome_wizard) {
  ONWIZ=1;
  document.getElementById("idhomewiz").style.display="none";
  f_runWizard();  
}

// signature button
if(user_level & usrSignLev){  
  document.getElementById("idsigndispl").style.display="block";
}


 

  if( (MOBILE_DESKTOP_LOGIN & user_level)  && !IMEI){
      
    set1j.style.display="block";

    document.getElementById("idbtndeskmobile").style.display="block";
    document.getElementById("check_setting1").checked=((parseInt(SETTINGS["fc_usr_mobile_autoswitch_desktop"]))?true:false);
            
            
  } else set1j.style.display="none";
              
}},


f_set_to_glb:function(k,v){
with(mobileWO){
  if(typeof(SETTINGS[k])=="undefined") return v;
  return SETTINGS[k];
}},


f_MobileDesktop:function(){
with(mobileWO){

  var v,j=document.getElementById("check_setting1");
  v=j.checked?1:0;
  SETTINGS["fc_usr_mobile_autoswitch_desktop"]=v;
  
  if(!OnOffline.STATUS) {
    $('#idmsg_alert').html( LANG("Connection is required to complete this action") );
     $.mobile.changePage("#idpopupalert");
    return;
  }

  $.ajax({
        url: staticUrl + "/mobile-login/switch-setting?ns=1",
        type: "post",
        data: {
            userId:    user_id,
            sessionId: session_id,
            version:   2.0,
            params:{ fc_usr_mobile_autoswitch_desktop:v }   
        },
        success:function(resp){
          if(resp!="ok"){
           alert("error: "+resp);
          }                                                          
        },
        error:function(){
           $('#idmsg_alert').html("Trasmission error 10");
           $.mobile.changePage("#idpopupalert");  
        }  
    });

}},



f_switch:function(){  
with(mobileWO){

  // verify IMEI and no WO downloaded
  try{ 
    if(IMEI) {
      $('#idmsg_alert').html(LANG("Action denied!"));
      $.mobile.changePage("#idpopupalert");
      return; 
    }
    var nwo=Dbl.query("SELECT temp_grid_wo count(*) WHERE 1")
    if(nwo){
     $('#idmsg_alert').html("Release work order!");
     $.mobile.changePage("#idpopupalert");
     return;
    }
  }catch(e){ return; }


  $.ajax({
        url: staticUrl + "/mobile-login/switch-desktop?ns=1",
        type: "post",
        data: {
            userId:    user_id,
            sessionId: session_id,
            version:   2.0,
            params:{}   
        },
        success:function(resp){
          if(resp=="ok"){
            Dbl.reset_db("db_mainsim_mobile");    // change with truncate tables WO,Asset, ecc...
            Dbl.db={};
            location.href="../";
          }                                                          
        },
        error:function(){
           $('#idmsg_alert').html("Trasmission error 09!");
           $.mobile.changePage("#idpopupalert");  
        }  
    });
}},


f_load_structureDB:function(){  
with(mobileWO){
 
  // if load/reload database structure
  var k,stg,STG={}, dbL=1;
  if(typeof(Dbl.db["t_creation_date"])!="undefined") dbL=0;
 
  try {   
      if(!dbL) {
          // build menu wo
          mobileWO.f_update_menuwo(-1);

          // summary
          mobileWO.f_summaryView();
          
          // lang
          f_update_lang(); 
          
          // restore SETTINGS
          f_settings();
        return;
      }
    
    } catch(e) {}
  

  
  // view loading popup
  f_show_loading();
  
      $.ajax({
        url: staticUrl + '/mobile-sync/check-db-structure?ns=1',
        type: "post",
        data: {
            userId:    user_id,
            sessionId: session_id,
            version:   2.0,
            params:{ dbLoad:dbL, "langwords":langwords }    
        },
        success:function(resp){   
            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; } 
            if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
            
            if(!jsn.params) jsn.params={}
 
            // general settings
            if(jsn.params.settings){
              
              stg=jsn.params.settings;

              if(parseInt(stg["UNDER_MAINTENANCE"])) { f_exit(); return; }
 
        //      var astg=["fc_usr_mobile_autoswitch_desktop"];
              
            //  for(k in stg) if(astg.indexOf(k)>-1) STG[k]=stg[k];
  
              Dbl.SETVAR("settings",stg);
            }
          f_settings();
     
     if(jsn.params.priority){
        Dbl.SETVAR("priority",jsn.params.priority); 
     } else {                                           // test
         jsn.params.priority={
          "0":["None","#999"],
          "1":["Normal","#222"],
          "2":["Urgent","#D60"],
          "3":["Immediate","#D00"]
        }
    }   
     Dbl.SETVAR("priority",jsn.params.priority);
     
     
            
            if(jsn.status=='LOAD_STRUCTURE_DB'){            // rebuild database
                f_drop_all_tables();                       // drop all tables (no vars) 

                // create & insert table
                f_create_insert_tables(jsn.params.db); 

                // lang substitution
                f_update_lang(jsn.params.lang);   // langwords
                         
            } else {
            
               f_update_lang();   
               f_refreshWO();
               
            }   
        
    // build menu wo
    mobileWO.f_update_menuwo(-1);

    // summary
    mobileWO.f_summaryView();
 
            // hide loading
            f_hide_loading();                                  
        },
        error:function(){ 
        
            NTRE01++;
            if(NTRE01<4){
            
              setTimeout( function(){ mobileWO.f_load_structureDB(); }, 2000 );

            } else { 
                
              f_logout();
      
           } 
          
        }  
    });
   
}},







f_update_lang:function(lang){
  if(!lang) {
    var l=Dbl.GETVAR("langwords");
    if(l) langwords=l; else return; 
  }else{
    langwords=lang;
    // write in localstorage
    Dbl.SETVAR("langwords",langwords);
    
  }
mobileWO.langIndexUpd();
},

langIndexUpd:function(){

  var r,tx,alng=[ 
  "idfilterwo",          
  "idbtnwfsave",
  "idbtnwfsaveedit",
  "idbtntsksave",
  "idfilterAS",
  "idfilterAS_mod",
  "idmsg_NFC_title",
  "iddoc_header",
  "idatchs_header",
  "idsetting_header"
  ];
   
  for(r=0;r<50;r++) alng.push("idlang"+r);
    
  for(r=0;r<alng.length;r++){ 
    tx=document.getElementById(alng[r]).innerHTML;
    document.getElementById(alng[r]).innerHTML=LANG(tx);
  }
},




f_refreshWO:function(){
with(mobileWO){  
  f_gridWO();     // build grid workorders and show
  f_summaryView();
  f_draw_wo(); 
}}, 



 
f_sync:function(){
with(mobileWO){ 
  
f_show_loading();  
  
try{  
  // verify online
  if(!WOchange.f_test_online(LANG("Connection is required to complete this action"))) return;
  
  // verify if there are WO to update autoSYNC 
  var m=WOchange.ifUpdMainsim(1);
 if(m==-1) f_CallSync();

}catch(e){ alert("Download error: "+e);  }
  
  
  
}},  
  
f_CallSync:function(){
with(mobileWO){   
  
  var r,ac=[], a=Dbl.query("SELECT t_creation_date GET f_id WHERE 1","NUM");
  for(r=0;r<a.length;r++) ac.push(a[r][0]);
 
  // call sync   
  $.ajax({
        url:  staticUrl + '/mobile-sync/sync?ns=1',
        type: "post",
        data: {
            userId:    user_id,
            sessionId: session_id,
            userAdmin: user_admin,
            version:   2.0,
            params:{ code_list:ac }    
        },
        success:function(resp){   
        
            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; }

            if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
            
            if(jsn.result) unPack(jsn.params); 
            if(jsn["par_wo_relations"]) updateWoRel(jsn.par_wo_relations); 

            cleaningWARE();  // delete ware not in cross between WO & Ware

            // hide loading
            f_hide_loading();                                  
        },
        error:function(ee){ 
            f_hide_loading(); 
            $('#idmsg_alert').html("Trasmission error 02!");
            $.mobile.changePage("#idpopupalert");      
        }  
    });
   
  closePanels();
}},




unPack:function(pack){
with(mobileWO){

  var r,t,d; 
  f_truncate_all_tables(noTruncateTable);

  for(t in pack){ 
    Dbl.query("TRUNCATE "+t); 
    d=pack[t];
    if(!d.length) continue;
    Dbl.query("INSERT "+t,d);
  }

  // f_ref_fieldTab_wo();  // ref field to table to find custom fields
 
  f_gridWO();     // build workorders grid list in table temp_grid_wo
  Dbl.save_db();

    // build summary
  f_summaryView();
  f_draw_wo();   
}},



updateWoRel:function(wr){
  if(wr.length) Dbl.query("INSERT t_wo_rel",wr);
},



//---------------------------------------------------------------------------------------------------------
// ODL
 

f_check_onoff:function(e,fc){  cancelEvent(e);
with(mobileWO){

  var i,n,s, j=e.target || e.srcElement;
  s=j.src;
  i=(s.indexOf("check0")==-1)?0:1;  
  j.src="img/bigcheck"+i+".png";
 
  if(i) ackSEL.push(fc);
  else {  
        n=ackSEL.indexOf(fc);
        if(n!=-1) ackSEL.splice(n,1);
  }
  footer_disabled();
}},








f_create_odl:function(){
with(mobileWO){
  Editpg.viewForm(0); 
}},



/* build workorders list from Dbl

temp_grid_wo:{  
      f_type_id:1,
      type: "WO" 
      f_code:12, 
      f_parent_code:0,
      fc_progress:1,
      f_priority:0, 
      f_user_id: 1
      f_user: "Alfredo", 
      f_creation_date:  "28/07/2013", 
      f_title:  "Figlio Guasto B", 
      f_description:"Descrizione del guasto", 
      f_wf_id: 1
      f_phase_id:"execution",
      f_group,
      f_group_id
      f_lock    
}      
      
 indexCode={  f_code:[index_two, index_tcr, index_tcu], ... }
    
 RFT[field]=table;    
*/

f_gridWO:function(){
with(mobileWO){
   
   var a,d,r,n,n0,n1,n2,n3,fc,k,I,gf,f,i,fu, ki, grph,
       rw,tb=[],typ=[],usn=[],alk=[],gWO=[], tb_dwl="", fl_dwl="", create_dwl=""; 
       
   if(DateWoList) {              // table:field
     a=DateWoList.split(":");
     tb_dwl=a[0];
     fl_dwl=a[1]; 
     create_dwl=", "+fl_dwl+" int";
   }     
   
   
First=1;    
RFT={};
indexCode={};
  
  with(Dbl){ 
    // create indexCode
    tb[0]="t_workorders"; 
    n0=AtoN(tb[0],"f_code"); 
    
    tb[1]="t_creation_date"; 
    n1=AtoN(tb[1],"f_id"); 
    
    tb[2]="t_custom_fields"; 
    n2=AtoN(tb[2],"f_code"); 
    
    tb[3]="t_workorders_parent"; 
    n3=AtoN(tb[3],"f_code"); 
   
      d=db[tb[0]].data;  
      //for(r=0;r<d.length;r++) indexCode[ d[r][n0] ]=[r,0,0];
      for(r=0;r<d.length;r++) { if(d[r]==null) continue; indexCode[ d[r][n0] ]=[r,0,0,0]; }
  
  
      d=db[tb[1]].data;  
      for(r=0;r<d.length;r++){ if(d[r]==null) continue;
        fc=d[r][n1];
        if(typeof(indexCode[fc])=="undefined") continue;
        indexCode[fc][1]=r;
      }  
      
      d=db[tb[2]].data;  
      for(r=0;r<d.length;r++){ if(d[r]==null) continue;
        fc=d[r][n2];
        if(typeof(indexCode[fc])=="undefined") continue;
        indexCode[fc][2]=r;
      } 
      
      d=db[tb[3]].data;  
      for(r=0;r<d.length;r++){ if(d[r]==null) continue;
        fc=d[r][n3];
        if(typeof(indexCode[fc])=="undefined") continue;
       // if(typeof(indexCode[fc][3])=="undefined") indexCode[fc][3]=[];          // no parent multiplo
        indexCode[fc][3]=r;
      } 
       
  a=query("SELECT t_locked GET f_code WHERE 1","NUM");
  for(r=0;r<a.length;r++) alk.push(a[r][0]);     
  
 
  // type name
  a=query("SELECT t_workorders_types GET f_id,f_type WHERE 1","NUM");
  for(r=0;r<a.length;r++) typ[a[r][0]]=a[r][1]; 

  // user name
  // a=query("SELECT t_users GET f_code,fc_usr_usn WHERE 1","NUM");
  // for(r=0;r<a.length;r++) usn[a[r][0]]=a[r][1];
  
  a=query("SELECT t_wf_groups GET f_id,f_group WHERE 1","NUM");   // 1 opening || 6 closing  || 8 = cloned
  for(r=0;r<a.length;r++) GROUP[a[r][0]]=a[r][1]; 
  
  /* indexCode={  f_code:[ 
                              id_tworkorders,   0
                              id_tcrreation,    1
                              id_tcustom,       2
                              id_parent_code,   3
                            ], ... }
  */  
  gf={"f_parent_code":3,"f_type_id":0,"fc_progress":1,"f_priority":0,"f_user_id":0,"f_creation_date":1,"f_title":1,"f_description":1,"f_wf_id":1,"f_phase_id":1};
  
  n=0;
  for(k in indexCode){ rw={};
      I=indexCode[k];
      ki=parseInt(k);
      rw["f_code"]=ki;
 
        // view if editability
        edy=db["t_creation_date"].data[ I[1] ][AtoN("t_creation_date","f_editability")];
        if( !(user_level & edy) && !user_admin) continue;
        
      for(f in gf){ i=gf[f];
          
          rw[f]=db[tb[i]].data[I[i]][AtoN(tb[i],f)];   // row { key:value } 
          
          if(f=="f_type_id") rw["type"]=typ[rw[f]];
          if(f=="f_user_id") { 
            // fu=usn[rw[f]]; if(!fu) fu=""; 
            rw["f_user"]=""; 
          }
                  
      }

  // f_lock
  rw["f_lock"]=(alk.indexOf(ki)!=-1)?1:0; 
  
  if(DateWoList){
    if(ki>0) {    
      dwl=query("SELECT "+tb_dwl+" GET "+fl_dwl+" WHERE "+((tb_dwl=="t_creation_date")?"f_id":"f_code")+"="+ki,"NUM");
      rw[fl_dwl]=dwl[0][0];
    }else rw[fl_dwl]=0;
  }
  
  gWO.push(rw);   
  n++;
  }
  
  // find f_group/f_group_id
  for(r=0;r<gWO.length;r++){ 
    
    grph=fGroupPhaseWO(gWO[r]);
    i=grph[0];
    f=grph[1];
    
    gWO[r]["f_group_id"]=i;
    gWO[r]["f_group"]=GROUP[i];
    gWO[r]["f_phase_name"]=f;
  }
 
  query("TRUNCATE temp_grid_wo");
  
  query("CREATE_TABLE temp_grid_wo (f_type_id int, type string, f_code int, f_parent_code int, fc_progress int, f_priority int,"+
        "f_user_id int, f_user string, f_creation_date int, f_title string, f_description string, f_wf_id int, f_phase_id int, f_group string, f_group_id int, f_lock int"+ create_dwl +", f_phase_name string)");
 
 
 
  if(gWO.length) { 
    query("INSERT temp_grid_wo", gWO);
    query("DELETE temp_grid_wo WHERE f_group_id IN (6,7)");    // do not show closing or deleting WF group
  }
 
 } // end Dbl

}},






fGroupPhaseWO:function(g){  // g=row { .... }

  var a,grp,phn, wf=g["f_wf_id"], fs=g["f_phase_id"]; 
  
    a=Dbl.query("SELECT t_wf_phases GET f_group_id, f_name WHERE f_number="+fs+" AND f_wf_id="+wf, "NUM"); 
    if(!a.length) { alert("error phase: no grouping"); return [1,1];  }
  
  grp=parseInt(a[0][0]);
  phn=a[0][1];
  
  return [grp,phn];
},

//---------------------------------------------------------------------------------------------------------
// VIEW



f_draw_wo:function(){
with(mobileWO){

  var r,flt="1",sep="";

  // if(!SORT) SORT=" ORDER BY f_code asc";
  
  if(!NFCFilter){
    if(FILTER.length) { flt="";
      for(r=0;r<FILTER.length;r++){
        flt+=sep+FILTER[r].value;
        sep=" AND ";
    }}
  }else{
    flt=NFCFilter;  // result search by qr and bar code and nfc...
  }
  
  f_viewfilter();
  gridWO=Dbl.query("SELECT temp_grid_wo GET * WHERE "+flt+SORT);
 
  ackSEL=[];  
  var str=f_show_listWO();
  
try{
  $('#idlistrows').html(str);
} catch(e){}
try{
  $('#idlistrows').listview('refresh');
} catch(e){}

}},





f_openclose_Row:function(p){ // open/close hierarchy WO
with(mobileWO){

  var z=arrPDR.indexOf(p); 
    if(z==-1) arrPDR.push(p);   // array row opened
    else arrPDR.splice(z,1);
  f_draw_wo();
}},




f_show_listWO:function(){
with(mobileWO){ 

  var r,col,fpc=0,won,str="",a,fl_dwl="",vd,p;
  
  fpc=Dbl.query("SELECT temp_grid_wo count(*) WHERE fc_progress=0 AND f_code>0");  // use fc_progress if all !=0

  if(DateWoList) {              // table_name:field_name
     a=DateWoList.split(":");
     fl_dwl=a[1]; 
  } 

 for(r=0;r<gridWO.length;r++) with(gridWO[r]){


  
  p=Dbl.GETVAR("priority");
  if(typeof(p[f_priority])=="undefined") col="#000";
  else col=p[f_priority][1];
 

  // save fc_progress relation with f_code
  if(fpc) won=f_code; else { won=Editpg.f_zero(fc_progress,5);  fcodeTOprogress[f_code]=fc_progress; }
  
  vd=f_creation_date;
  if(DateWoList) vd=gridWO[r][fl_dwl];
 
  str+="<li data-icon='false' data-theme='b'>"+
       "<a href='#' onclick='Editpg.viewForm("+f_code+","+(f_lock?0:1)+")'>"+
       "<img id='idck"+f_code+"' src='img/bigcheck0.png' onclick='"+M+".f_check_onoff(event,"+f_code+")'>"+
       "<p style='float:left;'>"+ 
         "<p style='line-height:18px;'><b>"+LANG("WO n.")+" "+won+" ("+LANG(type)+")</b>"+
         
         "<br>"+f_user+" "+ (vd?(fdate("d/m/Y - H:i",vd)):"")+"</p>"+
         
          "<h2 style='color:"+col+";margin-top:-4px;'>"+f_title+"</h2>"+
          "<p style='color:"+col+";margin-bottom:-2px;'>"+f_description+"&nbsp;</p>"+
       "</p>"+       
          "<p class='ui-li-aside' style='width:30%;margin-top:-3px;'><strong>"+LANG(f_phase_name)+
          "</strong><br><span style='font-size:11px;'>("+LANG(f_group)+")</span>"+(f_lock?"":"<br><img src='img/unlock-3.png'>")+"</p>"+          
        "</a>"+
    "</li>";
 
  }

  footer_disabled();
  return str;
}},



footer_disabled:function(){
with(mobileWO){

  var ab,rmv=1,wf=0,opt,n,phs=[],x,D,cc={},per=0,tp;
  
 
  if(ackSEL.length) {   // verify if workflows are equal 

    ab=Dbl.query("SELECT temp_grid_wo GET f_wf_id, f_phase_id, f_type_id WHERE f_code IN ("+ackSEL.join(",")+")","NUM");
    
    wf=ab[0][0];  phs.push(ab[0][1]);
    rmv=0;  

    tp=ab[0][2];
    if(tp==4) per=1;
    if(DisWoPhaseChangeType.indexOf(tp)!=-1) rmv=1;
    else {    
      for(r=1;r<ab.length;r++) {  
        tp=ab[r][2];
        if(tp==4) per=1;                              // 1: is periodic type
        if(ab[r][0]!=wf || DisWoPhaseChangeType.indexOf(tp)!=-1) { rmv=1; break; }    // if disable type || !=WF
        if(phs.indexOf(ab[r][1])==-1) phs.push(ab[r][1]); 
      } 
    }                  
  }  
   
  opt="<option value=''>"+LANG("Change phase")+"</option>"; 
  
try{             
  if(!rmv) {
          D=Dbl.db["t_wf_exits"].data;
          n=phs.length;
           
          for(r=0;r<D.length;r++){
            if(D[r]==null) continue;
            if(D[r][1]==wf &&  phs.indexOf(D[r][2])!=-1)  { 
              x=D[r][4];
              if(typeof(cc[x])=="undefined") cc[x]=0;
              cc[x]++; 
            }   
          }         
          r=0;
          for(x in cc) {
            if(cc[x]==n) {
              ab=Dbl.query("SELECT t_wf_phases GET f_name,f_group_id WHERE f_wf_id="+wf+" AND f_number="+x,"NUM");  
              if(per && ab[0][1]==6) continue;  
              opt+="<option value='"+x+"'>"+ab[0][0]+"</option>"; 
              r++; 
          }}    
               
          if(r){          
              $('#idsel_wf').empty().append(opt).selectmenu('refresh');
              $("#idbtnwfsave").removeAttr("disabled").button('refresh'); 
              $('#idsel_wf').selectmenu('enable');
          } else rmv=1;
  }
   
    if(rmv){ // disabled           
           $('#idsel_wf').empty().append(opt).selectmenu('refresh');
           $("#idbtnwfsave").attr("disabled", "disabled").button('refresh');
           $('#idsel_wf').selectmenu('disable');
     
   } 
}catch(e){}

}},




f_summaryView:function(){  
with(mobileWO){

var str0="<li data-role='list-divider'>"+LANG("Summary WO")+"</li>";

str0+=f_summarylst(0);

try{
$("#idlist1").html(str0);
} catch(e){}
try{
$("#idlist1").listview('refresh');
} catch(e){}

}},


f_summarylst:function(md,m){  // md=1 panel  0=home
with(mobileWO){

if(!m) m="";
var cc=[0,0,0,0,0,0,0,0,0,0], ag=[], a,r,gg,cv,str="",cgr;
   
    // home
    str="<li onclick='mobileWO.panelAct(\"\",0,\"\",1)'><a href='#'>"+LANG("All")+" <span class='ui-li-count'>00</span></a></li>";

with(Dbl){ 
  if(typeof(db["t_wf_groups"])!="undefined" && 
     typeof(db["temp_grid_wo"])!="undefined" &&
     typeof(db["t_wf_groups"]["data"])!="undefined"){   
  ag=query("SELECT t_wf_groups GET * WHERE f_visibility!=0");
    
  // counters:
  a=query("SELECT temp_grid_wo GET f_group_id WHERE 1","NUM");
  for(r=0;r<a.length;r++) cc[a[r][0]]++; 
  cv=a.length; if(cv<10) cv="0"+cv;
  str="<li onclick='mobileWO.panelAct(\"\","+md+",\"\",1)' "+m+"><a href='#'>"+LANG("All")+" <span class='ui-li-count'>"+cv+"</span></a></li>";
 
    for(r=0;r<ag.length;r++) {    //if(ag[r].f_visibility & user_level)     
      gg=ag[r].f_id;
      cv=cc[gg]; 
      if(!cv) continue;
      if(cv<10) cv="0"+cv;
      cgr=toCapitalize(ag[r].f_group);
      str+="<li onclick='mobileWO.panelAct(\"f_group_id="+gg+"\","+md+",\""+cgr+"\",1)' "+m+">"+
           "<a href='#'>"+LANG(cgr)+"<span class='ui-li-count'>"+cv+"</span></a></li>";          
    }
}}

return str;
}},



f_summaryPanel:function(m){  
with(mobileWO){
  var str1=f_summarylst(1,m);
  return str1;
}},


panelAct:function(f,md,ff,typ){   // f!="" FILTER     md=1 from panel  ff=filter name  typ: 1=summary 2=category  3=priority
with(mobileWO){

  var r,mm=1;

  SORT="";
  if(f) {  for(r=0;r<FILTER.length;r++){
              if(FILTER[r].type==typ) { mm=0; FILTER[r].value=f; filter[r]=ff; break; }
            } 
    if(mm) { FILTER.push( { value:f, type:typ  } ); filter.push(ff); }
  
  } else { FILTER=[]; filter=[]; NFCFilter=""; }
 
 try{
  if(md) closePanels(); 
  else $.mobile.changePage("#idwolist");
 } catch(e){} 
  
  f_draw_wo();
}},



f_categoryPanel:function(m){  
with(mobileWO){

var r,s="",a,i,cc, ctg={},ctn;

with(Dbl){
 
  if(typeof(db["temp_grid_wo"])!="undefined"){  
   
  a=query("SELECT temp_grid_wo GET f_type_id,type WHERE 1");
  for(r=0;r<a.length;r++) { 
    i=a[r].f_type_id;
    if(typeof(ctg[i])=="undefined") ctg[i]={ name:a[r].type, counter:0 };
    ctg[i].counter++; 
  }   

  for(r in ctg){
    cc=ctg[r].counter;
    if(cc<10) cc="0"+cc;
    ctn=toCapitalize(ctg[r].name);
    s+="<li onclick='mobileWO.panelAct(\"f_type_id="+r+"\",1,\""+ctn+"\",2)' "+m+"><a href='#' >"+LANG(ctn)+" <span class='ui-li-count' >"+cc+"</span></a></li>";      
  } 
  }
}  

return s;
}},



f_priorityPanel:function(m){  
with(mobileWO){

var r,s="",a,i,cc, ctg={},pri=[],cpr;
 
with(Dbl){

  pri=GETVAR("priority");     //  old =>  query("SELECT t_ui_fields GET f_options WHERE f_type='WORKORDERS' AND f_field_name='f_priority'","NUM");
  if(!pri) return "";
  if(typeof(db["temp_grid_wo"])!="undefined"){  
   
  a=query("SELECT temp_grid_wo GET f_priority WHERE 1");
  for(r=0;r<a.length;r++) { 
    i=a[r].f_priority;
    if(typeof(ctg[i])=="undefined") ctg[i]=0;
    ctg[i]++; 
  }   

  for(i in pri){        
    cc=ctg[i];
    if(!cc) cc=0;
    if(cc<10) cc="0"+cc;
    cpr=toCapitalize(pri[i][0]);   
    s+="<li onclick='mobileWO.panelAct(\"f_priority="+i+"\",1,\""+cpr+"\",3)' "+m+"><a href='#' >"+LANG(cpr)+" <span class='ui-li-count' >"+cc+"</span></a></li>";      
  } 
  }
}  

return s;
}},



f_viewfilter:function(){  
with(mobileWO){

var s=LANG("All");

if(filter.length) s=filter.join(", ");
if(NFCFilter) s=(filter.length)?(s+", NFC"):"NFC";

try{ $("#idfilterwo").html(s); } catch(e){}

}},



f_show_loading:function() {	
			$.mobile.loading( 'show', {
					text: LANG("loading..."),
					textVisible: true,
					theme: "d",
					textonly: false,
					html: ""
			});
},

f_hide_loading:function() {
			$.mobile.loading( 'hide' );
},


fnc_confirm:function(){
with(mobileWO){
  if(fncConfirm) eval(fncConfirm);
  fncConfirm="";
  NOBACK=1;
}},

nofnc_confirm:function(){
with(mobileWO){ 
  if(nofncConfirm) eval(nofncConfirm);
  nofncConfirm="";
  NOBACK=1;
}},

//---------------------------------------------------------------------------------------------------------
// MENU


f_update_menuwo:function(i){  
with(mobileWO){

  var s, m="style='margin-right:9px;'";
  
  if(i==1) iSUM*=-1, iCAT=-1, iPRI=-1;
  if(i==2) iCAT*=-1, iPRI=-1, iSUM=-1;
  if(i==3) iPRI*=-1, iSUM=-1, iCAT=-1;
   
  s="<li data-icon='home' "+m+"><a href='#idhomepage' onclick='"+M+".f_summaryView()'>"+"Home"+"</a></li>"+
  "<li onclick='mobileWO.panelAct(\"\",1,\"\",1)' "+m+"><a href='#'>"+LANG("All")+"</a></li>"+
  "<li data-icon='refresh' "+m+"><a href='#' onclick='"+M+".f_sync()'>"+LANG("Download work orders")+"</a></li>"+
  "<li data-icon='false' "+m+"><a href='#' onclick='WOchange.f_btnReleaseWO(); "+M+".closePanels();'>"+LANG("Upload selected WO")+"</a></li>";
  
  // QR BAR CODE if in app 
  var b=0; try{ if(parent.inApp()) b=1; }catch(e){}                      
  if(IMEI || b) s+="<li data-icon='false' "+m+"><a href='#' onclick='QRBAR.f_readQrBarCode();'>"+LANG("Read barcode / qrcode")+"</a></li>";
  
  // lock / unlock user administrator
  if(user_admin) {
    s+="<li data-icon='false' "+m+"><a href='#' onclick='WOchange.f_btnLockWO(); "+M+".closePanels();'>"+LANG("Lock selected")+"</a></li>"+
       "<li data-icon='false' "+m+"><a href='#' onclick='WOchange.f_btnUnlockWO(); "+M+".closePanels();'>"+LANG("Unlock selected")+"</a></li>"; 
  }
  
  s+="<li data-icon='search' "+m+"><a href='#' onclick='"+M+".f_search_toggle()'>"+LANG("Search")+"</a></li>";
  
  // new workorder
  if(usrNewWoLev & user_level)
    s+="<li data-icon='edit' "+m+"><a href='#' onclick='"+M+".f_create_odl()'>"+LANG("New work order")+"</a></li>";
  
  
  // tag code
  s+="<li data-icon='grid' "+m+"><a href='#' onclick='"+M+".f_inputTagCode()'>"+LANG("Tag code")+"</a></li>";
  
  
  s+="<li data-icon='false' "+m+"><a href='#' onclick='"+M+".f_selectAll()'>"+LANG("Select/unselect all")+"</a></li>"+
  "<li data-icon='"+((iSUM>0)?"minus":"plus")+"' "+m+" data-theme='d'><a  href='#' onclick='"+M+".f_update_menuwo(1)'>"+LANG("Summary")+"</a></li>";
  
  // Summary
  if(iSUM>0) s+=f_summaryPanel(m);
  
  
  s+="<li data-icon='"+((iCAT>0)?"minus":"plus")+"' "+m+" data-theme='d'><a  href='#' onclick='"+M+".f_update_menuwo(2)'>"+LANG("Category")+"</a></li>";
  
  // Category
  if(iCAT>0) s+=f_categoryPanel(m);
  
  
  s+="<li data-icon='"+((iPRI>0)?"minus":"plus")+"' "+m+" data-theme='d'><a href='#' onclick='"+M+".f_update_menuwo(3)'>"+LANG("Priority")+"</a></li>";
  
  // Priority
  if(iPRI>0) s+=f_priorityPanel(m);
  
  if(user_level==-1) s+="<li data-icon='false' "+m+"><a href='#' onclick='Debug.Open(); "+M+".closePanels()'>Debug</a></li>";                            //--------------------------->  debug
  
try{ 
  $('#idmenuwo').html(s);
} catch(e){}

try{
  if(i>-1) $('#idmenuwo').listview('refresh'); 
} catch(e){}


}},



f_search_toggle:function(){   // Search view/hide
 var i=$('#idlistrows').listview('option', 'filter');
 if(i) $("#idlistrows").prev("form.ui-listview-filter").toggle();
 else {
   $('#idlistrows').listview('option', 'filter', true);
   $('#idlistrows').trigger("listviewcreate");
 } 
 mobileWO.closePanels();
},




f_selectAll:function(){   // select/deselect list of WO
with(mobileWO){
  var j,r,fc, sall=0;   // sall:   0 deselect   1 select
  
  // check if already all selected
  for(r=0;r<gridWO.length;r++){
      fc=gridWO[r].f_code;
      j=document.getElementById("idck"+fc);
      if(j) { if(ackSEL.indexOf(fc)==-1) { sall=1; break; }  }
  }
  ackSEL=[];
  for(r=0;r<gridWO.length;r++){
      fc=gridWO[r].f_code;
      j=document.getElementById("idck"+fc);
      if(j) {          
        if(sall) { j.src="img/bigcheck1.png"; ackSEL.push(fc); }
        else j.src="img/bigcheck0.png"; 
      }
  } 
  closePanels();         // close panel
  footer_disabled();     // disabled  change WF  and save in footer
}},




closePanels:function(){  
try{
  $("#idpanelmenu" ).panel("close");   
} catch(e){  }

try{
  $("#idpanelhome").panel("close"); 
} catch(e){}
},






f_inputTagCode:function(){  
with(mobileWO){
 
  NOBACK=0;
  try{
    document.getElementById("id_nfc_value").value="";
    $.mobile.changePage("#idInppopupNFC"); 
  } catch(e){}
 
}},

f_SearchTagCode:function(){  
  
  NOBACK=1; 
  NFC.Get(document.getElementById("id_nfc_value").value);
 
},



//-------------------------------------------------------------------------------------------------
// ONLINE / OFFLINE

f_status_ico:function(v){
    var j=document.getElementById("idONLINE_home");
    if(j) j.src="img/online_status"+v+".png";
    
    j=document.getElementById("idONLINE_wo");
    if(j) j.src="img/online_status"+v+".png";
    
    j=document.getElementById("idONLINE_edit");
    if(j) j.src="img/online_status"+v+".png";   
    
    j=document.getElementById("idONLINE_wiz");
    if(j) j.src="img/online_status"+v+".png";  
    
    j=document.getElementById("idONLINE_AS");
    if(j) j.src="img/online_status"+v+".png";  
    
    j=document.getElementById("idONLINE_AS_mod");
    if(j) j.src="img/online_status"+v+".png";     
},




f_cache_request_send:function(){

  if(!OnOffline.STATUS) return;

    var par, p=Dbl.GETVAR("cache_request");
    if(!p.length) return false; 
    par=p.shift();
    
    Ajax.sendAjaxPost("../workorder/wizard?ns=1&resp=1", "params="+par, "mobileWO.RetWizEnd",0);  
},

RetWizEnd:function(a){

  if(a!="OK") { /*return false;*/ }
  
  var par, p=Dbl.GETVAR("cache_request");
  par=p.shift();
  Dbl.SETVAR("cache_request",p);
  
  setTimeout("mobileWO.f_cache_request_send()",2000);
},






f_upd_counter:function(par){

    var v=par["counter"];
    if(typeof(v)=="undefined") v="";

    var j=document.getElementById("idOLcounter_home");
    if(j) j.innerHTML=v;
    
    j=document.getElementById("idOLcounter_wo");
    if(j) j.innerHTML=v;
    
},




//------------------------------------------------------------------------------------------------
// DATABASE


f_truncate_all_tables:function(notabs){
  var t;
  if(!notabs) notabs=[];
  notabs.push("vars");
  with(Dbl){
    for(t in db){ 
      if(notabs.indexOf(t)!=-1) continue;
      query("TRUNCATE "+t);
  }}
},


f_drop_all_tables:function(notabs){
  var t;
  if(!notabs) notabs=[];
  notabs.push("vars");
  with(Dbl){
    for(t in db){ 
      if(notabs.indexOf(t)!=-1) continue;
      query("DROP_TABLE "+t);
  }}
},


f_create_insert_tables:function(p){
  var r,n,i;  
  with(Dbl){
    for(r=0;r<p.length;r++){    
      query(p[r].structure);
        if(!p[r].data) continue;
        if(!p[r].data.length) continue;
      query("INSERT "+last_tab, p[r].data); 
    }
  
  // create wo relations table
  query("CREATE_TABLE t_wo_rel (f_code int, f_parents string, f_childrens string, f_brothers string)");
  }    
},


// RFT:{}    field => table
f_ref_fieldTab_wo:function(){
with(mobileWO){

  var tabs=["t_creation_date","t_workorders","t_custom_fields"];   // Workorder table

  RFT={};
  var r,i,t,V;  
  with(Dbl){
    for(i=0;i<tabs.length;i++){ 
      t=tabs[i]; V=db[t]["field"];
      for(r=0;r<V.length;r++) RFT[V[r]]=t;   
  }}    
}},


f_confirm_create_wizard:function(){
with(mobileWO){
  NOBACK=0;
  fncConfirm="mobileWO.f_call_actend()";
  $('#idmsg_confirm').html( LANG("Do you confirm the creation of the work order?") );
  $.mobile.changePage("#idpopupconfirm");
}},

f_call_actend:function(j,m){
with(mobileWO){
  if(m) JFWIZ=j;
  else  JFWIZ.Wizard.ActEnd();
}},

//------------------------------------------------------------------------------------------------
// LOGOUT / END

f_logout:function(){   // truncate WO and delete user account var
with(mobileWO){

    var md=1,n=0;
    
    // if offline
     if(!WOchange.f_test_online( LANG("Connection is required to complete this action")  )) return;
     
    // check that workorders tab is empty  
    n=Dbl.query("SELECT t_workorders count(*) WHERE 1");
    md=(n===false)?0:1;

/*    
    // if there are WO
    if(n){  
      NOBACK=0;
      fncConfirm="mobileWO.f_call_logout()";
      $('#idmsg_confirm').html( LANG("Are you sure you want to logout?") );
      $.mobile.changePage("#idpopupconfirm");
      return;
    } 
 */ 
      
    // if empty & no error  
    if(md){ 
      NOBACK=0;
      $('#idmsg_confirm').html( LANG("Are you sure you want to logout?") );
      fncConfirm="mobileWO.f_call_logout()";
      $.mobile.changePage("#idpopupconfirm");
      return;
    }
    
    // logout if error WO query
    f_call_logout();
}},

f_call_logout:function(){
with(mobileWO){
    
    $.ajax({
        url:  staticUrl + "/mobile-login/logout?ns=1",
        type: "post",
        data: {
            userId:    user_id,
            sessionId: session_id,
            version:   2.0,
            params:{}    
        },
        success:function(resp){  
            
            try{ var jsn=JSON.parse(resp); }catch(e){ alert(resp); return; }
            
            //if(jsn.status=="SESSION_EXPIRED") { mobileWO.f_exit(); return; }
    
            mobileWO.f_exit();  
                                                  
        },
        error:function(){ 
           $('#idmsg_alert').html("Trasmission error 03!");
           $.mobile.changePage("#idpopupalert");  
        }  
    }); 

}},  
 

f_exit:function(){
  Dbl.reset_db("db_mainsim_mobile");    // change with truncate tables WO,Asset, ecc...
  Dbl.db={};
  location.href="login.html";
},

f_force_exit:function(){
  Dbl.reset_db("db_mainsim_mobile");    // Drop database and exit!
  mobileWO.f_exit();
},


f_runWizard:function(){
with(mobileWO){
 
  if(!FooterBtnHome_wizard) return;
  
f_resWizMobile(); 
$("#idfrmwiz").attr('src',"../new_wizard/wiz.html"); 
$.mobile.changePage("#idwizardpage");

  if(!isTouch) window.onresize=function(){ mobileWO.f_resWizMobile();  }  // problem with text in input/textarea
  
}},


f_resWizMobile:function(){
  var th=46,hfw=0; 
  if(!isTouch) { $("#dividfrmwiz").css("top","40px" ); th=40; }
  hfw=jsH();
  if(hfw) document.getElementById("idfrmwiz").style.height=(hfw-th)+"px";
},



mobile_wiz_reset:function(){
with(mobileWO){
  if(wizj) wizj.Wizard.freset();
  $("#idpanelmenuwiz").panel("close");
}}


} //



function fAlert(m){
  if(!m) m=LANG("Alert");
  NOBACK=0;
  $('#idmsg_alert').html(m);  
  $.mobile.changePage("#idpopupalert");
}

function fretAlert(){
  setTimeout( function(){ NOBACK=1; },500);
}
