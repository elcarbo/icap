// edit page wo / wares


//-------------------------------------
/*
function create_test_ui(){
with(Dbl){
  
  query("CREATE_TABLE t_ui_fields (f_id inc, f_type string, f_type_id int, f_field_type string, f_field_name string, f_label string, f_options string, f_visibility int -1, f_editability int 0,"+
        "f_mandatory int, f_selector_visibility string, f_table_ref string, f_check_exp string)");
 
  query("TRUNCATE t_ui_fields");
 
  var jt=[
  
    { f_type:"WORKORDERS", f_type_id:1, f_field_type:"input", f_field_name:"f_title", f_label:"Title", f_table_ref:"t_creation_date"  },
    { f_type:"WORKORDERS", f_type_id:1, f_field_type:"textarea", f_field_name:"f_description", f_label:"Description", f_table_ref:"t_creation_date"  },
    { f_type:"WORKORDERS", f_type_id:1, f_field_type:"select", f_field_name:"f_priority", f_label:"Priority", f_table_ref:"t_workorders", 
      f_options:JSON.stringify( [{value:1, label:'normal'},{value:2, label:'urgent'},{value:3, label:'immediate'}] )  }
  ];
 
  query("INSERT t_ui_fields", jt);
 
}}
*/
//-------------------------------------

Editpg={ 
  jsf:[], WFID:0,PHID:0, UIF:[],       // UIF  ui fields for WORKORDERS
  FC:0, FCP:0, EL_TYPE:"WORKORDERS", EL_TYPE_ID:1, PLANNED:0, aplnd:[4,5,6],
  withassets:"", tmp:[], 
  AlistAS:[], AlistDC:[],

viewForm:function(fc,ulk, wa){   // ulk: 0 save   1 no save
with(Editpg){

  tmp=[fc,ulk,wa];
  mobileWO.f_show_loading();
  setTimeout("Editpg.ActviewForm()",200); 
 
}},

 
ActviewForm:function(){
with(Editpg){
 
  var fc=tmp[0], ulk=tmp[1], wa=tmp[2];
  

 //   try{
 
  var r,n,s,a,v,rw,ext,t,f,l,fidcode,fnm,uif, opt="";
 
  // gridAS=[ { f_id:1000, f_title:"test asset1", f_description:"Test1 descrizione asset in WO1.", status:0 },... ]
  if(wa) { withassets=wa;
    mobileAS.gridAS=Dbl.query("SELECT t_creation_date GET f_id, f_title, f_description WHERE f_category='ASSET' AND f_id IN ("+withassets+")");
    for(r=0;r<mobileAS.gridAS.length;r++) mobileAS.gridAS[r].status=1;
    mobileAS.Page="";
    
  } else withassets=""; // if new WO with asset
 
// alert(mobileAS.gridAS.length)
  mobileAS.oFC=wa?0:-99999; // reset temp asset
 

  EL_TYPE_ID=1; 
  FC=parseInt(fc); 
  
  FCP=mobileWO.fcodeTOprogress[FC];
  if(!FCP) FCP=FC;
  
  if(!FC) f=LANG("New WO"); 
  else { f=LANG("Edit (WO ") + f_zero(FCP,5)+")";
         
         a=Dbl.query("SELECT temp_grid_wo GET * WHERE f_code="+FC);
         rw=a[0];
         EL_TYPE_ID=rw.f_type_id;
  }
  
  // if periodic or inspection
  PLANNED=(aplnd.indexOf(EL_TYPE_ID)!=-1)?1:0;
  
  // view in header
  $('#ideditwoheader').html(f);

  
  // find fields in t_ui_fields
  uif=Dbl.query("SELECT t_ui_fields GET * WHERE f_type='"+EL_TYPE+"' AND f_type_id IN (0,"+EL_TYPE_ID+")");    // 0 ALL type_id
  UIF=[];
 

  WFID=0; PHID=0;
  if(FC){
    WFID=parseInt(rw.f_wf_id);
    PHID=parseInt(rw.f_phase_id);  
    
    a=Dbl.query("SELECT t_wf_phases GET f_editability WHERE f_wf_id="+WFID+" AND f_number="+PHID, "NUM");
    if(a.length){
      
       l=parseInt(a[0][0]);
       if( ! (l & mobileWO.user_level) )  ulk|=1;
    }
  }
    

  // find value
  for(r=0;r<uif.length;r++){  
   
    fnm=uif[r]["f_field_name"];
    t=uif[r]["f_table_ref"];
      
    uif[r]["f_visibility"]=comp_Visibility(uif[r]);       // 0 no display but exist for script at save!
  
        if(FC){ 
          fidcode=(t=="t_creation_date" || t=="t_pair_cross")?"f_id":"f_code";
          a=Dbl.query("SELECT "+t+" GET "+fnm+" WHERE "+fidcode+"="+FC, "NUM");
          if(!a.length) { alert("value not found: "+t+" | "+fnm); continue; }
          v=a[0][0];    
        } else {
          n=Dbl.AtoN(t,fnm);
          v=Dbl.db[t].blank[n]; 
        }
        
      uif[r]["f_value"]=v; 
      UIF.push(uif[r]);
  }
  
   // change page
  $('#idnewedit').html(""); 
  $.mobile.changePage("#idwoedit");           
  $('#idsel_wfedit').selectmenu('disable');
  
  
   
  
  // save
  if(!ulk) $("#idbtnwfsaveedit").removeAttr("disabled").button('refresh');
  else $("#idbtnwfsaveedit").attr("disabled","disabled").button('refresh');
  
  
  
  // select exits workflow if edit
  if(FC){
    ext=Dbl.query("SELECT t_wf_exits GET f_description, f_exit WHERE f_wf_id="+WFID+" AND f_phase_number="+PHID+" AND f_visibility&"+mobileWO.user_level+" ORDER BY f_order");
    opt="<option value=0>"+LANG("Change phase")+"</option>";
    for(r=0;r<ext.length;r++) { 
     opt+="<option value='"+ext[r].f_exit+"'>"+LANG(ext[r].f_description)+"</option>"; }
      
    $('#idsel_wfedit').empty().append(opt).selectmenu('refresh');
    $('#idsel_wfedit').selectmenu('enable');
    
  } else { // new

    $('#idsel_wfedit').empty().append(opt).selectmenu('refresh');
  }

  // innerHTML  form fields
  s=f_build_form(UIF);
  
  // tabs
  if(!withassets) mobileAS.gridAS=[];
  s+=f_build_tabs();

  // update/create page
  $('#idnewedit').html(s);      
  $('#idwoedit').trigger('create'); 
  
  for(r=0;r<UIF.length;r++){
    fnm=UIF[r]["f_field_name"];
    if(UIF[r]["f_mandatory"]) {
      if(UIF[r]["f_field_type"]=="textarea") $('#id_'+fnm).css("border-color","#A1C24D");
      else $('#id_'+fnm).parent().css("border-color","#A1C24D"); 
    }
  }
   
      
  // onload system script---------------------
  sysScript.LD_script(); 
            
}},








f_readO:function(d, dwf){  // d=f_editability    return readonly or ''   0=readonly 
  if(!d) rdo="readonly";
  else {
    if(d==-1) rdo="";
    else rdo=(d & mobileWO.user_level)?"readonly":""; 
  }
 
  if(!rdo) {
    var v=Editpg.test_WF_PH(dwf);
    rdo=v?"readonly":"";
  } 
  return rdo;
},


comp_Mandatory:function(a){
with(Editpg){

  var vf= a["f_mandatory_wf"],
      vs=a["f_mandatory"];         //  & mobileWO.user_level)?1:0
  
  if(vs || !WFID || !vf) return vs;
  return test_WF_PH(vf);
}},

comp_Visibility:function(a){
with(Editpg){
  var vf=a["f_visibility_wf"],
      vs=(a["f_visibility"] & mobileWO.user_level)?1:0; 
  
  if(!vs || !WFID || !vf) return vs;
 
  return test_WF_PH(vf);
}},


test_WF_PH:function(vf){   // test x WF_phase     vf:  2_1,3_4,3_4
with(Editpg){
  if(!vf) vf="";
  var r, avf,lvf;
  vf=vf.replace(/ /g,"");
  lvf=vf.split(",");
  
  for(r=0;r<lvf.length;r++){
    avf=lvf[r].split("_");
    if(avf.length!=2) continue;
    if(avf[0]==WFID && avf[1]==PHID) return 1;
  }  
  return 0;
}},






f_build_form:function(uif,ik){
with(Editpg){
  
  var n=0,k,s="",typ,v,r,n,o,slc,d,rdo,sinp,sdt,onc=0,ong="";
  if(!ik) ik="",onc=1;
  
  for(n=0;n<uif.length;n++){    

    k=uif[n]["f_field_name"];
    typ=uif[n]["f_field_type"];  // input, textarea, select, radio, ...
 
  if(typ=="image") continue;  
    
    
    
    d=uif[n]["f_editability"]; 
    rdo=f_readO(d, uif[n]["f_editability_wf"]);
 
    ong=(onc && !rdo) ? "onchange='sysScript.ED_script(\""+k+"\","+n+",this.value)'" : "";
 
 
    v=uif[n]["f_value"];
 
    if(v==null || v=="null") v="";
    
    o=uif[n]["f_options"]; 
    if(o==null || o=="null") continue;
    
    s+="<div style='margin-top:-8px;margin-bottom:-16px;"+( (uif[n]["f_visibility"])?"":"display:none;" )+"'>";    //  data-role='fieldcontain'

    if(o) o=JSON.parse(o); else o=[];
 
    sdt=""; sinp="";  

     // extra
     if(typ=="extra"){ sinp="<input name='id_"+ik+k+"' id='id_"+ik+k+"' value=\""+f_noquote(v)+"\" type='text' readonly />"; }

 
      // input
      if(typ=="input") sinp="<input name='id_"+ik+k+"' id='id_"+ik+k+"' value=\""+f_noquote(v)+"\" type='text' "+rdo+" "+ong+" />";
      if(typ=="date") { sinp="<input name='id_"+ik+k+"' id='id_"+ik+k+"' value='"+tms_to_date(v,0)+"' type='text' readonly "+(rdo?"":("onclick='Calendar.View(1,\""+ik+"\",\""+uif[n]["f_label"]+"\",\""+k+"\","+n+");'") )+" />"; sdt=LANG(" (DD/MM/YYYY)");  }
      if(typ=="time") { sinp="<input name='id_"+ik+k+"' id='id_"+ik+k+"' value='"+tms_to_time(v)+"' type='text' readonly "+(rdo?"":("onclick='Calendar.View(2,\""+ik+"\",\""+uif[n]["f_label"]+"\",\""+k+"\","+n+");'") )+" />"; sdt=LANG(" (hh:min)");  }
      if(typ=="datetime") { sinp="<input name='id_"+ik+k+"' id='id_"+ik+k+"' value='"+tms_to_date(v,1)+"' type='text' readonly "+(rdo?"":("onclick='Calendar.View(3,\""+ik+"\",\""+uif[n]["f_label"]+"\",\""+k+"\","+n+");'") )+" />"; sdt=LANG(" (DD/MM/YYYY - hh:min)");  }
    
    // label
    if(typ!="radio" && typ!="checkbox") { 
        if(sdt) sdt="<span style='font-size:11px;font-weight:normal;'>"+sdt+"</span>";
        s+="<label for='id_"+ik+k+"' style='margin-bottom:-8px;font-size:14px;font-weight:bold;'>"+uif[n]["f_label"]+sdt+"</label>"+sinp; 
    } 

      // textarea
      if(typ=="textarea") s+="<textarea name='id_"+ik+k+"' id='id_"+ik+k+"' "+rdo+" "+ong+" style='height:92px;font-size:12px;line-height:13px;'>"+v+"</textarea>";

      // select
      if(typ=="select") {
        if(rdo) rdo="disabled='true'";
       
       var ochg=""; if(uif[n].f_onchange) { ochg="onchange='"+uif[n].f_onchange+"'";  }
    
        s+="<select name='id_"+ik+k+"' id='id_"+ik+k+"' data-native-menu='true' data-theme='b' "+rdo+" "+ochg+" "+ong+">"; 
        for(r=0;r<o.length;r++){
          slc=(o[r].value==v)?"selected":"";
          s+="<option value='"+o[r].value+"' "+slc+">"+o[r].label+"</option>";      
        }    
        s+="</select>";
      }

    // single checkbox
    if(typ=="checkbox") {
     s+="<fieldset data-role='controlgroup' data-theme='b'>"+
          "<legend>"+uif[n]["f_label"]+"</legend>";   
       for(r=0;r<o.length;r++){  
         slc=(o[r].value==v)?"checked='checked'":"";
     	   s+="<input type='checkbox' name='id_"+ik+k+"' id='id_"+ik+k+r+"' value='"+o[r].value+"' "+slc+" data-theme='b' "+rdo+" "+ong+" />"+
     	   "<label for='id_"+ik+k+r+"'>"+o[r].label+"</label>";
       }
       s+="</fieldset>";
    }
    
    // radio
    if(typ=="radio") {
       s+="<fieldset data-role='controlgroup' data-theme='b'>"+
          "<legend>"+uif[n]["f_label"]+"</legend>";       
       for(r=0;r<o.length;r++){  
         slc=(o[r].value==v)?"checked='checked'":"";
     	   s+="<input type='radio' name='id_"+ik+k+"' id='id_"+ik+k+r+"' value='"+o[r].value+"'  "+slc+" data-theme='b' "+rdo+" "+ong+" />"+
     	   "<label for='id_"+ik+k+r+"'>"+o[r].label+"</label>";
       }
       s+="</fieldset>";
    }
    // image
    if(typ=="image") {
      s+="<input name='id_"+ik+k+"' id='id_"+ik+k+"' value='"+v+"' type='file' "+rdo+" />";
    }
 
   s+="<br></div>";   
  } //
   
  return s;
}},



f_build_tabs:function(){
with(Editpg){
  
  // counter asset
  var r,ncount=0, ndocs=0,b,s;
  
  AlistAS=[], AlistDC=[]; 
  
  mobileAS.buildDoc=0;
 
  // assets
  b=Dbl.query("SELECT t_creation_date f_id WHERE f_category='ASSET'");     // 'ASSET'
  for(r=0;r<b.length;r++) AlistAS.push(b[r].f_id);
 
 
  if(withassets) ncount=mobileAS.gridAS.length;
  else {
    if(AlistAS.length){
      s="SELECT t_ware_wo count(*) WHERE f_wo_id="+FC+" AND f_ware_id IN ("+AlistAS.join(",")+")";
      ncount=Dbl.query(s);      
    }
  }
  
  // documents per WO
  b=Dbl.query("SELECT t_creation_date f_id WHERE f_category IN ('DOCUMENT','FILES AND LINKS')"); 
  for(r=0;r<b.length;r++) AlistDC.push(b[r].f_id);
 
  if(AlistDC.length) ndocs=Dbl.query("SELECT t_ware_wo GET count(*) WHERE f_wo_id="+FC+" AND f_ware_id IN ("+AlistDC.join(",")+")");
  
 
  $("#idfootertask").css("display", (PLANNED?"block":"none") );  // abilita footer
  
  s="<br><ul id='idlistAsset' data-role='listview' data-theme='b' data-split-icon='false' data-inset='false'>"+
        "<li onclick='mobileAS.f_viewAssetListWO("+FC+")'>"+LANG("Asset")+( PLANNED?(LANG(" / Task")):"" )+"<span id='idcounterAS' data-count-theme='a' class='ui-li-count'>"+ncount+"</span></li>"+
        "<li onclick='mobileAS.f_Edit_Documents("+FC+")'>"+LANG("Files and links")+"<span id='idcounterDC' data-count-theme='a' class='ui-li-count'>"+ndocs+"</span></li>"+
        "<li onclick='Editpg.f_WOrelation("+FC+")'>"+LANG("Work order relations")+"</li>"+
    //    "<li >Resource</li>"+
     "</ul><br>&nbsp;";  
  return s;
}},





f_WOrelation:function(fc){
with(Editpg){
  
  // read value in t_wo_rel
  var s,a,v=["","",""];
  
  a=Dbl.query("SELECT t_wo_rel * WHERE f_code="+fc,"NUM");
  if(a.length) v=[ a[0][1], a[0][2], a[0][3] ];
   

  s="<label for='id_worel_parent'>"+LANG("Parent")+"</label>"+
    "<textarea name='id_worel_parent' id='id_worel_parent' readonly>"+v[0]+"</textarea>"+    

    "<label for='id_worel_children'>"+LANG("Children")+"</label>"+
    "<textarea name='id_worel_children' id='id_worel_children' readonly>"+v[1]+"</textarea>"+ 
    
    "<label for='id_worel_brothers'>"+LANG("Brothers")+"</label>"+
    "<textarea name='id_worel_parent' id='id_worel_parent' readonly>"+v[2]+"</textarea>"; 

   $('#id_worel_form').html(s);
   $('#id_worel_form').trigger('create'); 


   $.mobile.changePage("#idwo_relations");
}},





f_EditSave:function(){
with(Editpg){

  try{ 
    
    var msg=mScript.Save(EL_TYPE_ID); 
    if(msg) {  fAlert(msg); return false; }
    
  }catch(e){ alert(e+" no script") }

  // system script check
  msg=sysScript.SVC_check_script();
  if(msg) { fAlert(msg); return false; }

  mobileWO.f_show_loading();
  setTimeout("Editpg.f_actSave()",50);
}},



f_actSave:function(){
with(Editpg){

  var r,rr,n,o,v,m,k, V,jn,fidcode, min=0, phase, fex=0, wf, fnm, ft, par, a, er=[],sep;
 
   // if periodic or inspection (4,5) and task aren't sets and WF phase change => exit 
   if(PLANNED){ 
    phase=parseInt($('#idsel_wfedit').val());
    
    if(phase){
      // close phase 
      a=Dbl.query("SELECT t_wf_phases GET f_number WHERE f_wf_id="+WFID+" AND f_group_id=6","NUM");
      if(a.length) fex=parseInt(a[0][0]);
      
      //alert(WFID+" | ph: "+phase+"  cloase: "+fex); 
      
      var isNTC=false;
      if(NphaseTaskChecked){                                // NphaseTaskChecked es. 9_2,2_3
       
        NphaseTaskChecked=NphaseTaskChecked.replace(/ /g,"");
   
        var g,gp, agp=(NphaseTaskChecked+"").split(",");
        for(g=0;g<agp.length;g++) {
          gp=agp[g].split("_");
          if(gp.length==1) gp.unshift(WFID);
          if(gp[0]==WFID && gp[1]==phase) { isNTC=true; break; } 
        }
      }
        
      // alert("a) "+isNTC+"\n"+gp[0]+" | "+WFID+"\n"+gp[1]+" | "+phase)
        
      if(phase==fex || isNTC) {  // task must be compiled in exit and in NphaseTaskChecked
        
        if(mobileAS.gridAS.length){
        
              V=mobileAS.tempTask;
              for(k in V){ 
                for(r=0;r<V[k].length;r++){
                    v=0;
                    if(V[k][r].fc_task_issue_positive) v++;   
                    if(V[k][r].fc_task_issue_negative) v++; 
                    if(V[k][r].fc_task_issue_not_executable) v++; 
                    
                    if(V[k][r].fc_task_issue_performed) v++;  

                    if(V[k][r].fc_task_issue_not_performed || !v) {
                      NOBACK=0;
                      $('#idmsg_alert').html( LANG("Compile all tasks before save the work order!") );
                      $.mobile.changePage("#idpopupalert");
                      return;
              }}}
        
        } else {

 
        mobileAS.SetTaskMethod();

          if(mobileAS.TskData){
            a=Dbl.query("SELECT t_pair_cross GET fc_task_issue_performed, fc_task_issue_not_executable WHERE f_code_main="+FC,"NUM");
          } else { 
            a=Dbl.query("SELECT t_pair_cross GET fc_task_issue_positive, fc_task_issue_negative, fc_task_issue_not_executable WHERE f_code_main="+FC,"NUM");
          }
          
          for(r=0;r<a.length;r++){ 
          
            if(a[r][0]) continue;   
            if(a[r][1]) continue;
            if(!mobileAS.TskData && a[r][2]) continue;
                NOBACK=0;
                $('#idmsg_alert').html( LANG("Compile all tasks before closing Workorder!") );
                $.mobile.changePage("#idpopupalert");
                return; 
          }
        }   
      }}
   } // end if periodic




  // system scripts 
  // onsave system script---------------------
  sysScript.SV_script(); 
  
  
   
  // mandatory / verify moCheck.check(v,c);  
  for(n=0;n<UIF.length;n++){
    
    if(f_readO(UIF[n]["f_editability"], UIF[n]["f_editability_wf"])) continue;
    
    fnm=UIF[n]["f_field_name"];
    ft=UIF[n]["f_field_type"];
    
    if(ft=="image") continue; 
    if(ft=="checkbox" || ft=="radio")  continue;

    v=$('#id_'+fnm).val();
    
    m=comp_Mandatory(UIF[n]);
    
    if(ft=="textarea") $('#id_'+fnm).css("border-color",m?"#A1C24D":"#60828D");
    else $('#id_'+fnm).parent().css("border-color",m?"#A1C24D":"#60828D");                // reset border color

    if(ft=="datetime" || ft=="date") { v=date_to_tms(v); if(v===false) { er.push(fnm); continue; } }
    if(ft=="time") { v=time_to_tms(v); if(v===false) { er.push(fnm); continue; }  }
    
    if(typeof(v)=="undefined") { v=""; }
    if(m && v=="") { er.push(fnm); continue; }
    
    if(v) { if( !moCheck.check(v,UIF[n]["f_check_exp"]) ) er.push(fnm); }
  } 
   
  if(er.length){ 
     NOBACK=0; 
     $('#idmsg_alert').html( LANG("Check fields before saving") );
     $.mobile.changePage("#idpopupalert");  
     
     for(n=0;n<er.length;n++){ 
      if( (document.getElementById('id_'+er[n]).tagName).toLowerCase() =="textarea") $('#id_'+er[n]).css("border-color","#FF0000"); 
      else $('#id_'+er[n]).parent().css("border-color","#FF0000"); 
     }    
    return;
  } 
  
Dbl.AUTOSAVE=0; 
   
  var tm=WOchange.time(), ll=f_geocoords();
  par=[{ f_code:FC, f_timestamp:tm, new_WO:0, fc_latitude:ll.lat, fc_longitude:ll.lon } ];
   
  // if new WO search negative f_code
  if(!FC) {  
    V=Dbl.db["t_creation_date"].data;
    for(n=0;n<V.length;n++){
      if(V[n]==null || V[n]<-1000000000) continue;   // <1000000000 is temp ware
      if(V[n][0]<min ) min=V[n][0];
    }
    FC=min-1;  // new f_code
    
    // insert default rows in TB:(t_creation_date, t_workorders, t_custom_fields,t_workorders_parent)                                       
    V=WOchange.TB;
    
    for(r=0;r<V.length;r++){   
      fidcode=r?"f_code":"f_id";
      jn={};
      jn[fidcode]=FC;
      Dbl.query("INSERT "+V[r],jn);
    }
    
    par[0]["new_WO"]=1;
    par[0]["f_code"]=FC;
    
    // t_locked
     Dbl.query("INSERT t_locked",{f_code:FC, f_user_id:mobileWO.user_id, f_timestamp:tm }); 
  } 
 
  

  //get value 
  for(n=0;n<UIF.length;n++){
  
    fnm=UIF[n]["f_field_name"];
    ft=UIF[n]["f_field_type"];
    
    if(ft=="checkbox" || ft=="radio") {
      v="";      
      o=UIF[n]["f_options"]; 
      if(o==null || o=="null") continue;
      if(o) o=JSON.parse(o); else o=[];
        sep="";
        for(rr=0;rr<o.length;rr++){  
         if(document.getElementById('id_'+fnm+rr).checked) { v+=sep+o[rr].value; sep=","; }
        }
     if(o.length<2) v=+v;
    } else v=$('#id_'+fnm).val();

    if(ft=="datetime" || ft=="date") v=date_to_tms(v);                // check format
    if(ft=="time") v=time_to_tms(v);
    par[0][fnm]=v   // assign value
  }
   
  // coords
  par[0]["fc_latitude"]=ll.lat, par[0]["fc_longitude"]=ll.lon; 
   
   
  // workflow
  if(FC>0){
    phase=parseInt($('#idsel_wfedit').val());
    if(phase) par[0]["f_phase_id"]=phase; 
    
    par[0]["fc_editor_user_name"]=mobileWO.user_name;
    par[0]["fc_editor_user_mail"]=mobileWO.user_mail; 
    
  } else {
    // wf num & phase start
    // get from t_workorders_types   CORRECTIVE   
    a=Dbl.query("SELECT t_workorders_types GET f_id, f_wf_id, f_wf_phase WHERE f_type='CORRECTIVE'","NUM");
    if(!a) { Dbl.AUTOSAVE=1; return; }
    
    par[0]["f_type_id"]=a[0][0]; 
    par[0]["f_wf_id"]=a[0][1]; 
    par[0]["f_phase_id"]=a[0][2];
           
    par[0]["f_type"]="WORKORDERS";
    par[0]["f_category"]="CORRECTIVE";
  
  if(par[0]["new_WO"])  par[0]["f_creation_date"]=tm;  // new wo creation date
  
    par[0]["f_timestamp"]=tm;
    par[0]["f_main_table"]="t_workorders";      
     
    par[0]["f_visibility"]=-1;    
    par[0]["f_editability"]=-1;
    
    par[0]["f_parent_code"]=0;
    par[0]["f_active"]=1;    
    
    par[0]["fc_editor_user_name"]=mobileWO.user_name;
    par[0]["fc_editor_user_mail"]=mobileWO.user_mail;                                       
  }
  
  
  //  par[0]["f_creation_user"]=mobileWO.user_id;
    par[0]["f_user_id"]=mobileWO.user_id; 
  
  
    // CROSS
  // update asset wo list
  var gA=mobileAS.gridAS, Acd,Ast, jA=[];
  if(gA.length){
  
    if(AlistAS.length) Dbl.query("DELETE t_ware_wo WHERE f_wo_id="+FC+" AND f_ware_id IN ("+AlistAS.join(",")+")");    // delete cross Asset with WO   need to be rebuilt
  
        for(r=0;r<gA.length;r++){
          Ast=gA[r].status; if(typeof(Ast)=="undefined") Ast=0;
          Acd=gA[r].f_id;
          if(Ast<2) jA.push( { f_ware_id:Acd, f_wo_id:FC, f_timestamp:tm } );
        }
  
    if(jA.length) Dbl.query("INSERT t_ware_wo", jA);

    // if periodic update paircross
    if(PLANNED){
      V=mobileAS.tempTask;
        var vupd=[];
        try{  vupd=Dbl.GETVAR("v_upd_paircross"); 
        }catch(e){ vupd=[]; alert("no var v_upd_paircross initialized!"); } 
      if(vupd==null) vupd=[];
      
    // index on f_id of t_pair_cross
    var r,i,p,VD,idx={},vid,ir;
    
    VD=Dbl.db["t_pair_cross"].data;
    
    for(r=0;r<VD.length;r++) { if(VD[r]) idx[ VD[r][0] ]=r;  }

    for(k in V){

        if(vupd.indexOf(k)==-1) vupd.push(k);

    //  alert("asset: "+k+" | n. task: "+V[k].length);
      
        for(r=0;r<V[k].length;r++){
          vid=V[k][r].f_id;
          if(vid) { ir=idx[vid];
             for(i in V[k][r]){      
                p=Dbl.AtoN("t_pair_cross",i);  // position field in data row
                VD[ir][p]=V[k][r][i];                                               //  Dbl.query("UPDATE t_pair_cross WHERE f_id="+V[k][r].f_id, V[k][r]);       // update TASKS       
             }            
          }   
        } 
      }

      Dbl.SETVAR("v_upd_paircross",vupd);
    }

  } //


  
  // update documents
  var gD=mobileAS.tempDocs, Dst,fD, stts,pszdel, 
      jDlt=[], jDel=[], jD=[], jDcd=[], jDwa=[], jDwap=[], jDcf=[];
  
 
  if(gD.length){
    stts=0;
    vupd={}; 
    try{ vupd=Dbl.GETVAR("v_upd_docs"); }catch(e){ vupd={}; alert("no var v_upd_docs initialized!"); }  // WO with docs to update in mainsim db
    if(vupd==null) vupd={};
    
    for(r=0;r<gD.length;r++){
      Dst=parseInt(gD[r].status);   // 0 new 1 old exist  2=delete
      fD=gD[r].f_code;
      
      // delete
      if(Dst==2) { jDlt.push(fD); if(fD>0) { stts=1; jDel.push(fD); }  }
      
      // insert
      if(!Dst){ 
        // if new
        if(fD<0) {
          // cross t_ware_wo
          jD.push( { f_ware_id:fD, f_wo_id:FC, f_timestamp:tm } );
        
          // creation date  
          jDcd.push( { f_id:fD, 
                       f_type:"WARES", 
                       f_category:"FILES AND LINKS", 
                       f_creation_date:tm, 
                  //     f_creation_user:mobileWO.user_id, 
                       f_title:gD[r].oname,
                       f_wf_id:9,
                       f_phase_id:1,
                       f_visibility:-1,
                       f_editability:-1,
                       f_timestamp:tm,
                       fc_creation_user_name:mobileWO.user_name
          });
          
          // t_wares 
          jDwa.push({  f_code:fD, 
                       f_type_id:5,
                       f_timestamp:tm,
                       f_user_id:mobileWO.user_id,
                       fc_doc_attach:(-fD)+"_"+mobileWO.user_id+"|"+gD[r].oname              
          });
 
          // t_wares_parent
          jDwap.push({ f_code:fD, 
                       f_parent_code:0,
                       f_active:1,		
                       f_timestamp:tm                     
          });   
          
          // t_custom_fields
          jDcf.push({ f_code:fD,
                      f_timestamp:tm,
                      f_main_table:"t_wares", 
                      f_main_table_type:5                   
          });   
          
           
        }
        
      }     
    }
    
    // to delete
    if(jDlt.length) {  
      Dbl.query("DELETE t_ware_wo WHERE f_wo_id="+FC+" AND f_ware_id IN ("+jDlt.join(",")+")");    
       
          Dbl.query("DELETE t_wares WHERE f_code IN ("+jDlt.join(",")+")"); 
          Dbl.query("DELETE t_wares_parent WHERE f_code IN ("+jDlt.join(",")+")");
          Dbl.query("DELETE t_custom_fields WHERE f_code IN ("+jDlt.join(",")+")"); 
          Dbl.query("DELETE t_creation_date WHERE f_id IN ("+jDlt.join(",")+")");   
         
    }
    
    // to insert
    if(jD.length) {  stts=1;
      Dbl.query("INSERT t_ware_wo",jD);   
      Dbl.query("INSERT t_creation_date",jDcd); 
      Dbl.query("INSERT t_wares",jDwa); 
      Dbl.query("INSERT t_wares_parent",jDwap); 
      Dbl.query("INSERT t_custom_fields",jDcf);     
    }

    // if update documents
    if(stts) { 
 
      if( typeof(vupd[FC])=="undefined" ) vupd[FC+""]=[]; 
      for(r=0;r<jDel.length;r++) {
        pszdel=vupd[FC+""].indexOf(jDel[r]);
        if(pszdel!=-1) vupd[FC+""].splice(pszdel,1);  
      }
      
      for(r=0;r<jD.length;r++)  vupd[FC+""].push(jD[r]);    
      
      
      Dbl.SETVAR("v_upd_docs",vupd); 
    }
   }
 
  mobileAS.tempDocs=[];


  WOchange.Set(par);
  $.mobile.changePage("#idwolist");
  
  Dbl.AUTOSAVE=1;
  Dbl.save_db(); 
  
  // update summary
  mobileWO.f_update_menuwo(-1);
  try{ $('#idmenuwo').listview('refresh'); } catch(e){}
  
}},


 



//----------------------------------------------------------------------------------------------------------------------------------------
// functions

f_zero:function(v,n){ if(!n) n=5;
  var r,s=v+"", i=s.length;
  for(r=i;r<n;r++) s="0"+s;
  return s;
},



tms_to_date:function(vv,m){
  var dt,s; 
  vv=parseInt(vv);
  if(!vv) return ""; 
  
  dt=new Date(vv*1000); 
  
  var DD = dt.getDate(),
      MM = parseInt(dt.getMonth())+1,
      YY = dt.getFullYear(),
      hh=0,mm=0;
  
  if(m){
      hh=dt.getHours(),
      mm=dt.getMinutes();
  }
  
  with(Editpg){
    DD=f_zero(DD,2);
    MM=f_zero(MM,2);
    if(m){
      hh=f_zero(hh,2);
      mm=f_zero(mm,2);
    }}
  
  s=DD+"/"+MM+"/"+YY;
  if(m) s+=" - "+hh+":"+mm;

return s; 
},




tms_to_time:function(vv,m){

var hh,mm; 
vv=parseInt(vv);
if(!vv || vv>=86400) return ""; 

hh=parseInt(vv/3600);
mm=parseInt((vv%3600)/60);

return Editpg.f_zero(hh,2)+":"+Editpg.f_zero(mm,2);
},




date_to_tms:function(s){
with(Editpg){
  
  if(!s) return 0;
  
  var a,sd,sh,DD,MM,YY,hh=0,mm=0,dta;
  a=s.split(" - ");
  
  // time hh:mm
  if(a.length==2){
    sh=a[1].split(":");
    if(sh.length!=2) return false;
    hh=parseInt(sh[0]);
    mm=parseInt(sh[1]);
  }
  
  // date
  sd=a[0].split("/");
  if(sd.length!=3) return false;
  DD=parseInt(sd[0]);
  MM=parseInt(sd[1])-1;
  YY=parseInt(sd[2]);

  dta=new Date(YY,MM,DD,hh,mm,0);

return parseInt((dta.getTime())/1000);
}},


time_to_tms:function(s){
  var hh,mm,a=s.split(":");
  if(a.length!=2) return false; 
  hh=parseInt(a[0]),mm=parseInt(a[1]);
  if(!hh) hh=0;
  if(!mm) mm=0;
return hh*3600+mm*60;
}



} // end Editpg




/*


UIF=[
  field_name:{ type:input,  value:...,  props:...    }
]
 
  Editpg.viewForm()

 <label for='date'>Date:</label>
    <input name='date' id='date' value='' type='date'>
    
    
    <label for='time'>time:</label>
    <input name='time' id='time' value='' type='time'>
    
    <label for='datetime'>datetime:</label>
    <input name='datetime' id='datetime' value='' type='datetime'>
    
    <label for='file'>File:</label>
    <input name='file' id='file' value='' type='file'>  

*/





//----------------------------------------------------------

Signature={ canvasj:null, cancelj:null, saveclearj:null, sboxj:null,
            drw:0, ftl:"", WON:"", Dta:"", SIGN:0,


Start:function(){
with(Signature){
  SIGN=1;
  $.mobile.changePage("#idsign_page");
  
  var r, s,sc,woinfo, fw=jsW(), fh=jsH(),  sj=document.getElementById("idsign_page");
  
  ftl=""; WON="";
  sc="<img id='idsigncancel' src='img/cancel32.png' style='position:absolute;right:10px;top:10px;cursor:pointer;' onclick='Signature.f_close()'>";
  
  // info workorder N. - f_title - data  AAAA/MM/DD hh:mm  iso 8601
  with(Editpg){
    
    for(r=0;r<UIF.length;r++) {  if(UIF[r]["f_field_name"]=="f_title"){ ftl=UIF[r]["f_label"]+": "+UIF[r]["f_value"]; break;  }   }
    
    WON=LANG("Work order")+" n. "+f_zero(FCP,5); 
    Dta=LANG("Date")+": "+nowDate();
    
    woinfo="<div style='position:absolute;left:10px;top:10px;font:13px opensansR,Arial;color:#888;'>"+WON+
    "<br>"+ftl+"<br>"+Dta+"</div>"+
    "<div style='position:absolute;left:10px;right:10px;bottom:30%;height:1px;overflow:hidden;background-color:#aaa;'></div>";
  }
  
  s=woinfo+
    "<canvas id='idsign' style='position:absolute;left:0px;top:0px;' width='"+fw+"' height='"+fh+"'></canvas>"+
    sc+"<div id='idsignbox' style='position:absolute;left:50%;bottom:10px;margin-left:-68px;width:136px;height:56px;border:1px solid #444;"+
    "background-color:#FFF;opacity:0.7;border-radius:6px;display:none;'></div>"+
    "<img id='idsignclear' src='img/clear32.png' style='position:absolute;left:50%;bottom:22px;margin-left:-48px;cursor:pointer;display:none;' onclick='Signature.f_clear()'>"+
    "<img id='idsignsave' src='img/save32.png' style='position:absolute;left:50%;bottom:22px;margin-left:16px;cursor:pointer;display:none;' onclick='Signature.f_save()'>";
  
  //window.onorientationchange=function(){ Signature.Start(); }
  window.onresize=function(){ if(SIGN) Signature.Start(); }
  
  if(fw<fh) { 
    sj.innerHTML="<div style='position:absolute;width:100%;top:50%;margin-top:-64px;text-align:center;font:bold 48px opensansR,Arial;color:#888;'>"+
    LANG("Rotate")+"<br><img src='img/rotate64.png' /></div>"+sc;  
    return;          
  }
  
  sj.innerHTML=s;
  
  canvasj=document.getElementById("idsign");
  cancelj=document.getElementById("idsigncancel");
  
  clearj=document.getElementById("idsignclear");
  savej=document.getElementById("idsignsave");
  sboxj=document.getElementById("idsignbox");
  

  var prop={
  
    dotSize:1,
    minWidth:0.5,
    maxWidth:1.6,
    backgroundColor:"rgba(0,0,0,0)",
    penColor:"black",
    velocityFilterWeight:0.7,
    
    onBegin:Signature.strokeBegin,   // Callback when stroke begin
    onEnd:strokeEnd                 // Callback when stroke end
  }

  drw=0;
  signaturePad = new SignaturePad(canvasj, prop); 

}},

strokeBegin:function(){
with(Signature){
  drw=1;
  cancelj.style.display="none";
  clearj.style.display="none";
  savej.style.display="none";
  sboxj.style.display="none";
}},


strokeEnd:function(){
with(Signature){

  drw=0;
  setTimeout("Signature.SignEnd()",800);
}},

SignEnd:function(){
with(Signature){

  if(drw) return;
  cancelj.style.display="block";
  clearj.style.display="block";
  savej.style.display="block";
  sboxj.style.display="block";
  
}},


f_close:function(){
  Signature.SIGN=0;
  $.mobile.changePage("#iddocs_page");
},


f_clear:function(){
with(Signature){
  signaturePad.clear();
  drw=0;
}},




f_save:function(){
with(Signature){

  // if sign is empty
  var Emp=signaturePad.isEmpty(); if(Emp) return;
  
  // insert info WO
  var cj=canvasj.getContext("2d");
  with(cj){
    font="13px opensansR,Arial"; textBaseline="top"; textAlign="left";
    fillStyle="#888";
    fillText(WON,10,12);
    fillText(ftl,10,28);
    fillText(Dta,10,44);
  }  
  
  var d=signaturePad.toDataURL();   // base 64 image png  to send app
 
  try {   
    parent.SaveSignature(d,"png");    // send b64   =>   mobileAS.setImageID(i,ext)   (i=timestamp of signature)
  }catch(e){}
 
 
  try {   
    window.JavascriptInterface.SaveSignature(d,"png");    // send b64   =>   mobileAS.setImageID(i,ext)   (i=timestamp of signature)
  }catch(e){}
 
 
  f_close();
}}


} // end signature




// date


function nowDate(){

var dt = new Date(),   
    hh=dt.getHours(),
    mm=dt.getMinutes(),
    ss=dt.getSeconds(),  
    DD = dt.getDate(),
    MM = parseInt(dt.getMonth()) +1,
    YY = dt.getFullYear(),
    d=[YY,MM,DD,hh,mm,ss], r;

for(r=1;r<d.length;r++) d[r]=Editpg.f_zero(d[r],2);

return d[0]+"/"+d[1]+"/"+d[2]+" "+d[3]+":"+d[4];
}

function f_noquote(str) {
  return (str + '').replace(/"/g, '“');
}


function f_geocoords() {
 
 // geolocation in fields   // t_creation_date     fc_latitude   fc_longitude
 var s,lt=0,ln=0,a;
 
 try{
  s=window.JavascriptInterface.GetLatLngCoords();
  if(s) {
    a=s.split(",");
    lt=parseFloat(a[0]);
    ln=parseFloat(a[1]);
  } 
 }catch(e){
    lt=parseFloat(mobileWO.Lat);
    ln=parseFloat(mobileWO.Lon);
 }


  return {lat:lt, lon:ln };
}