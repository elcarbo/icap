<?php

class Selector
{
    private $db;
    
    public function __construct() {}
    
    public function insertSelectors($selectors,$custom_fields,$f_type_id = 0) 
    {           
        $this->db = Zend_Registry::get('db');
        $time = Zend_Registry::get('f_timestamp');
        
        $select = new Zend_Db_Select($this->db);
        
        $sel_cols = utilities::$default_table_cols;
        $creation_cols = utilities::$creation_table_cols;
        
        $time_list = array(
            "fc_wo_starting_date",
            "fc_wo_ending_date",
            "fc_offer_date",
            "fc_sch_end_date",
            "fc_contract_starting_date",
            "fc_contract_ending_date"
        );
        //verifico se esiste il primo elemento di categoria
        if(!$f_type_id) {
            $f_type_id = substr($selectors[0]['f_type_id'], 0,  strpos($selectors[0]['f_type_id'], " :"));
        }        
        try {
            $res_type = $select->from("t_selectors_types")
                    ->where("f_id = ?",$f_type_id)->query()->fetchAll();                            
        }catch(Exception $e) {                
            utilities::reporQuerytError("t_creation_date",$e->getMessage(),$select->__toString());die;
        };  
        
        $type_id_info = $res_type[0];
                
        try {
            $select->reset();
            $res_cat = $select->from(array("t1"=>"t_selectors_parent"))
                    ->join(array("t2"=>"t_selectors"),"t1.f_code = t2.f_code",array())
                    ->where("f_type_id = $f_type_id")->where("f_parent_code = 0")->query()->fetchAll();            
        }catch(Exception $e){                
            utilities::reportQueryError("t_selectors_parent",$e->getMessage(),$select->__toString());die;
        };
        $f_order = 0;
        if(empty($res_cat)) {            
            $select->reset();
            try {
                $res_types = $select->from("t_selectors_types")->where("f_id = ?",$f_type_id)->query()->fetchAll();
            }catch(Exception $e) {
                utilities::reportQueryError("t_selectors_types", $e->getMessage(), $select->__toString());
                die;
            }

            if(empty($res_types)) {
                try {
                    $f_type = str_replace("$f_type_id : ","",$selectors[0]['f_type_id']);
                    $f_type = strtoupper($f_type);
                    $new_type = array('f_id'=>$f_type_id,'f_type'=>  $f_type,'f_wf_id'=>9,'f_wf_phase'=>1);
                    $this->db->insert("t_selectors_types",$new_type);
                    $type_id_info = $new_type;
                }catch(Exception $e) {
                    $dump = Zend_Debug::dump($new_type,null,false);
                    utilities::reportQueryError("t_selectors_types", $e->getMessage(),$dump );
                    die;
                }
            }
            
            $select->reset();                
            $creation = array(
                'f_creation_date'=>$time,
                'f_order'=>$f_order,
                'f_type'=>"SELECTORS",
                'f_category'=>  $type_id_info['f_type'],
                'f_creation_user'=>1,
                'f_title'=>$type_id_info['f_type'],
                'f_description'=>'',
                'f_editability'=>-1,
                'f_visibility'=>-1,
                'f_wf_id'=>9,
                'f_phase_id'=>1                
            );
            $this->db->beginTransaction();
            try {
                $this->db->insert("t_creation_date",$creation);
                $f_code = $this->db->lastInsertId();
            }catch(Exception $e) {                                    
                $this->db->rollBack();                       
                $message_dump = Zend_Debug::dump($selectors[0],null,false);                    
                utilities::reportInsertError("t_creation_date",$e->getMessage(),$message_dump);die;                    
            }           
            $this->db->commit();
            $sel_val = array();
            $sel_val['f_code'] = $f_code;                            
            $sel_val['f_order'] = $f_order;
            $sel_val['f_type_id'] = $type_id_info['f_id'];            
            $sel_val['f_timestamp'] = $time;            
            $sel_val['f_priority'] = 1;

            try {
                foreach($sel_val as &$line_val) {
                    $line_val = is_null($line_val)?'':$line_val;
                    $line_val = utf8_decode($line_val);
                }
                $this->db->insert("t_selectors",$sel_val);
            }catch(Exception $e) {                      
                $message_dump = Zend_Debug::dump($selectors[0],null,false);                    
                utilities::reportQueryError("t_selectors",$e->getMessage(),$message_dump);die();
            }
            //carico il custom field
            $custom = array(                
                'f_timestamp'=>$time,
                'f_code'=>$f_code,                
            );
            try {
                foreach($custom as &$line) {
                    $line = is_null($line)?'':$line;
                    $line = utf8_decode($line);
                }
                $custom['f_main_table'] = 't_selectors';
                $custom['f_main_table_type'] = $f_type_id;
                $this->db->insert("t_custom_fields",$custom);            
            }catch(Exception $e) {
                $message_dump = Zend_Debug::dump($selectors[0],null,false);                     
                utilities::reportInsertError("t_custom_fields",$e->getMessage(),$message_dump);die();
            }

            //carico il parent
            $parent = array(
                'f_active'=>1,
                'f_timestamp'=>$time,
                'f_code'=>$f_code,                
                'f_parent_code'=>0
            );
            try {                    
                $this->db->insert("t_selectors_parent",$parent);            
            }catch(Exception $e) {        
                $message_dump = Zend_Debug::dump($selectors[0],null,false);                     
                utilities::reportInsertError("t_selectors_parent",$e->getMessage(),$message_dump);die();
            }                
        }        
        
        $f_order = $f_order == 0?Zend_Registry::get('f_order'):1;        
        $select->reset();
        try {
            $res = $select->from("t_creation_date",array("f_order"))->where("f_category = ?",strtoupper($explode_type[1]))
                ->order("f_order DESC")->limit(1)
                ->query()->fetchAll();
        }catch(Exception $e) {
            utilities::reportQueryError("t_creation_date",$e->getMessage(),$select->__toString());die;
        };

        if(!empty($res)) {                
            $f_order = $res[0]['f_order'];
            $f_order++;
        }    
        
        foreach($selectors as $sel) {            
            $sel_val = $sel;
            
            $keys = array_keys($sel_val);
            foreach($keys as $line) {
                if(is_null($sel_val[$line])) {
                    $sel_val[$line] = '';                    
                }
                if(!in_array($line, $sel_cols)){
                    unset($sel_val[$line]);
                }                        
            }
                 $time = time();
            //creo il creation date
            $creation = array(
                'f_creation_date'=>$time,
                'f_order'=>$f_order,
                'f_type'=>"SELECTORS",
                'f_category'=>  $type_id_info['f_type'],
                'f_creation_user'=>1,
                'f_wf_id'=>9,
                'f_phase_id'=>1,
                'f_visibility'=>-1,
                'f_editability'=>-1,
                'f_title'=>!isset($sel['f_title'])?'':utf8_decode($sel['f_title']),
                'f_description'=>!isset($sel['f_description'])?'':utf8_decode($sel['f_description']),
            );
            $f_order++;
            $this->db->beginTransaction();
            try {
                $this->db->insert("t_creation_date",$creation);
                $f_code = $this->db->lastInsertId();
            }catch(Exception $e) {                
                $this->db->rollBack(); 
                $message_dump = Zend_Debug::dump($sel,null,true);                
                utilities::reportInsertError("t_creation_date",$e->getMessage(),$message_dump);die;                
            }           
            $this->db->commit();
            $sel_val['f_code'] = $f_code;
            unset($sel_val['f_parent_code']);
            $sel_val['f_order'] = $f_order;
            $sel_val['f_type_id'] = $f_type_id;            
            $sel_val['f_timestamp'] = $time;                        
            $sel_val['f_priority'] = 1;
            $sel_val['fc_imp_code_sel_excel'] = $sel['f_code'];
                        
            try {
                foreach($sel_val as &$line_val) {
                    $line_val = is_null($line_val)?'':$line_val;
                    $line_val = utf8_decode($line_val);
                }                
                $this->db->insert("t_selectors",$sel_val);
            }catch(Exception $e) {         
                $message_dump = Zend_Debug::dump($sel,null,true);                
                utilities::reportQueryError("t_selectors",$e->getMessage(),$message_dump);die();
            }
            //carico il custom field
            $custom = array(                
                'f_timestamp'=>$time,
                'f_code'=>$f_code,                                
                'fc_imp_parent_code'=>$sel['f_parent_code']
            );
            
            foreach($sel as $key => $line) {
                if(!array_key_exists($key, $sel_val) && !in_array($key, $creation_cols) ) { 
                    $not_custom = true;
                    $index_custom = -1;
                    $field = utilities::clean_col_name($key == 'f_parent_code' ? 'fc_imp_parent_code' : $key);
                    //verifico se trovo il custom field orginale fra quelli passati dall'excel.
                    foreach($custom_fields as $key_custom => $line_custom) {
                        if($key == $line_custom['f_title'] || $key == $line_custom['f_label']) {                            
                            $not_custom = false;
                            $index_custom = $key_custom;
                            $field = $line_custom['f_title'];
                            break;
                        }
                    }
                    //verifico che sia coerente con il custom fields passato                    
                    if(!$not_custom) {
                        $type_custom = substr(str_replace(" : ","",$custom_fields[$index_custom]['f_type']),1);
                        switch($type_custom) {
                            case 'BOOLEAN' :$custom[$field] = (int)(bool)$line;                                
                                break;
                            case 'INT' : $custom[$field] = (int)$line;                                
                                break;
                            case 'DATE' : 
                                if(is_int($type_custom)){
                                    $custom[$field] = (int)$line;
                                }
                                else {
                                    $custom[$field] = (int)strtotime($line);
                                }                                
                                break;                           
                            default : $custom[$field] = $line;
                                break;
                        }
                    }
                    else {
                        $custom[$field] = $line;
                    }
                    if(in_array($field,$time_list)){                        
                        if(is_int($type_custom)){
                            $custom[$field] = (int)$custom[$field];
                        }
                        else {
                            $custom[$field] = (int)strtotime($custom[$field]);
                        }  
                    }
                }
            }
            
            try {
                foreach($custom as &$line) {
                   $line = is_null($line)?'':$line;
                   $line = utf8_decode($line);
                }
                $custom['f_main_table'] = 't_selectors';
                $custom['f_main_table_type'] = $f_type_id;
                $this->db->insert( "t_custom_fields",$custom);            
            }catch(Exception $e) {  
                $message_dump = Zend_Debug::dump($sel,null,false);
                utilities::reportInsertError("t_custom_fields",$e->getMessage(),$message_dump);die();
            }                            
        }           
        $this->db->closeConnection();
    }
    
    public function fatherSelector($f_id) 
    {        
        $this->db = Zend_Registry::get('db');
        $time = Zend_Registry::get('f_timestamp');
        
        $select = new Zend_Db_Select($this->db); 
        try {
            $selectors = $select->from(array("t1"=>"t_custom_fields"),array('f_code','fc_imp_parent_code'))                    
                    ->join(array("t2"=>"t_selectors"),"t1.f_code = t2.f_code",array())
                    ->where("f_type_id = {$f_id}")->where("t1.f_code not in (select f_code from t_selectors_parent where f_code = t1.f_code)")
                    ->query()->fetchAll();
        }catch(Exception $e) {            
            utilities::reportQueryError("t_selectors",$e->getMessage(),$select->__toString());die;
        };
                
        if(!empty($selectors)) {
            //cerco la root del tipo
            $select->reset();
            try {
                $root = $select->from(array("t1"=>"t_selectors_parent"))
                    ->join(array("t2"=>"t_selectors"),"t1.f_code = t2.f_code",array())   
                    ->where("f_parent_code = 0")
                    ->where("f_type_id = $f_id")->query()->fetchAll();
            }catch(Exception $e) {                
                utilities::reportQueryError("t_selectors_parent",$e->getMessage(),$select->__toString());die;
            }
            $root_code = !empty($root) ? $root[0]['f_code'] : 0;
        }
        //caricati i selettori, ricomincio il giro per creare le parentele
        $tot_a = count($selectors);
        for($a = 0;$a < $tot_a;$a++){
        //foreach($selectors as $sel) {
            $sel = $selectors[$a];
            $select->reset();
            $f_code = $sel['f_code'];
            $f_parent_code_excel = $sel['fc_imp_parent_code'];                
            $f_parent_code_excel = explode(',',$sel['fc_imp_parent_code']);
            if(count($f_parent_code_excel) == 1) {
                $f_parent_code_excel = explode('.',$sel['fc_imp_parent_code']);
            }
            foreach($f_parent_code_excel as $line_f_parent_code_excel) {
                $f_parent_code = $root_code;
                if($line_f_parent_code_excel != "" && !is_null($line_f_parent_code_excel)) {
                    try{
                        $res_custom = $select->from("t_selectors")
                            ->where("fc_imp_code_sel_excel = ?",$line_f_parent_code_excel)
                            ->where("f_type_id = $f_id")->query()->fetchAll();
                    }catch(Exception $e){                    
                        utilities::reportQueryError("t_custom_fields",$e->getMessage(),$select->__toString());die;
                    };
                    if(!empty($res_custom)) {
                        $custom = $res_custom[0];
                        $f_parent_code = $custom['f_code'];     
                    }
                }

                $parent = array(
                    'f_parent_code'=>$f_parent_code,
                    'f_code'=>$f_code,
                    'f_active'=>1,
                    'f_timestamp'=>$time
                );         

                try {
                    $this->db->insert("t_selectors_parent",$parent);
                }catch(Exception $e) {
                    $message_dump = Zend_Debug::dump($parent,null,false);                    
                    utilities::reportInsertError("t_selectors_parent",$e->getMessage(),$message_dump);die();                
                }    
            }
        }        
        
        //search check if parent code = 0 is truely 0 or a parent code not found
        $select->reset();
        try {
            $res_parent = $select->from(array("t1"=>"t_selectors_parent"),array("f_code"))
                ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array("fc_imp_parent_code"))
                ->join(array("t3"=>"t_selectors"),"t2.f_code = t3.f_code",array("fc_imp_code_sel_excel"))
                ->where("t1.f_active = 1")->where("f_type_id = ?",$f_id)
                ->where("f_parent_code = 0")
                ->query()->fetchAll();
        }catch(Exception $e) {
            utilities::reportQueryError("t_selectors_parent",$e->getMessage(),$select->__toString());die;
        }
        $tot_c = count($res_parent);
        for($c = 0; $c < $tot_c; ++$c) {
            $line = $res_parent[$c];            
            if(!$line['fc_imp_parent_code']) continue;
            
            //if fc_imp_parent_code != 0, search father
            $parent_array = array();
            if(strpos($line['fc_imp_parent_code'],",")!== false) {
                $parent_array = explode(",",$line['fc_imp_parent_code']);
            }
            else {
                $parent_array = explode(".",$line['fc_imp_parent_code']);
            }
            foreach($parent_array as $line_array) {
                if($line_array == '') continue;
                $select->reset();
                try {
                    $res_search_parent_code = $select->from("t_selectors",array("f_code"))
                        ->where("fc_imp_code_sel_excel = ?",$line_array)
                        ->where("f_type_id = ?",$f_id)->query()->fetchAll();
                }catch(Exception $e) {
                    utilities::reportQueryError("t_custom_fields",$e->getMessage(),$select->__toString());die;
                }
                //if not exist, continue
                if(empty($res_search_parent_code)) continue;
                
                //change parent code
                try {
                    $this->db->update("t_selectors_parent",array("f_parent_code"=>$res_search_parent_code[0]['f_code']),"f_code = {$line['f_code']} and f_active = 1");
                }catch(Exception $e) {
                    utilities::reportError("Something went wrong during an update : ".$e->getMessage());die;
                }
            }
        }        
        $this->db->closeConnection();
    }
    
    /**
     * Create cross between selectors (and parent) and passed element (wares/workorders)
     * @param type $line cross to insert
     * @param type $table table to extract system code (wares/workorders)
     */    
    public function crossSelectors($crosses = array(),$table = 't_wares') 
    {           
        $this->db = Zend_Registry::get('db');
        $time = Zend_Registry::get('f_timestamp');
        $select = new Zend_Db_Select($this->db);                            
        //field of excel code in table
        $field = $table == 't_wares'?'fc_imp_code_wares_excel':'fc_imp_code_wo_excel';
        // fields of excel 
        $field_code_excel = $table == 't_wares'?'f_code_wares':'f_code_workorders'; // field used as code in excel
        $field_type_excel = $table == 't_wares'?'f_type_id_wares':'f_type_id_workorders'; // field used as type in excel
        // fields to insert data in cross table
        $table_cross = $table == 't_wares'?'t_selector_ware':'t_selector_wo';
        $field_cross = $table == 't_wares'?'f_ware_id':'f_wo_id';

        $sel = array(); //list of selectors
        $oth = array(); // list of wares/wos
        $tot_a = count($crosses);
        for($a = 0;$a < $tot_a;++$a) {
            if($crosses[$a]['f_type_id_selector'] == '' || $crosses[$a]['f_code_selector'] == ''
                || $crosses[$a][$field_code_excel] == '' || $crosses[$a][$field_type_excel] == '') continue;
            $type_sel = substr($crosses[$a]['f_type_id_selector'], 0,  strpos($crosses[$a]['f_type_id_selector'], " :"));
            $type_oth = substr($crosses[$a][$field_type_excel], 0,  strpos($crosses[$a][$field_type_excel], " :"));
            $sel[$type_sel]["'{$crosses[$a]['f_code_selector']}'"] = "'{$crosses[$a]['f_code_selector']}'";
            $oth[$type_oth]["'{$crosses[$a][$field_code_excel]}'"] = "'{$crosses[$a][$field_code_excel]}'";                        
        }
        
        $type_sel_keys = array_keys($sel);
        $type_oth_keys = array_keys($oth);        
        //get true codes of selectors
        $tot_b = count($type_sel_keys);
        for($b = 0;$b < $tot_b;++$b) {
            $key = $type_sel_keys[$b];            
            $sel_list = implode(',',$sel[$key]);
            if(empty($sel_list)) continue;                        
            $sel[$key] = array();
            $select->reset();
            try {
                $res_true_sel = $select->from("t_selectors",array("f_code","fc_imp_code_sel_excel"))
                    ->where("f_type_id = ?",$key)->where("fc_imp_code_sel_excel IN ($sel_list)")
                    ->query()->fetchAll();
            }catch(Exception $e) {
                utilities::reportQueryError("t_selectors", $e->getMessage(), $select->__toString());
            }
            $tot_c = count($res_true_sel);
            for($c = 0;$c < $tot_c;++$c) {
                $sel[$key][$res_true_sel[$c]['fc_imp_code_sel_excel']] = (int)$res_true_sel[$c]['f_code'];
            }
        }
        
        //get true codes of wos/wares
        $tot_b = count($type_oth_keys);
        for($b = 0;$b < $tot_b;++$b) {
            $key = $type_oth_keys[$b];
            $oth_list = implode(',',$oth[$key]);
            if(empty($oth_list)) continue;            
            $oth[$key] = array();
            $select->reset();
            try {
            $res_true_oth = $select->from($table,array("f_code",$field))
                    ->where("f_type_id = ?",$key)->where("$field IN ($oth_list)")
                    ->query()->fetchAll();
            }catch(Exception $e) {
                utilities::reportQueryError($table, $e->getMessage(), $select->__toString());
            }
            $tot_c = count($res_true_oth);
            for($c = 0;$c < $tot_c;++$c) {
                $oth[$key][$res_true_oth[$c][$field]] = (int)$res_true_oth[$c]['f_code'];
            }
        }
                
        //insert cross
        $tot_b = count($crosses);
        for($b = 0;$b < $tot_b;++$b) {
            $line = $crosses[$b];
            if($line['f_type_id_selector'] == '' || $line['f_code_selector'] == ''
                || $line[$field_code_excel] == '' || $line[$field_type_excel] == '') continue;
            $type_sel = substr($line['f_type_id_selector'], 0,  strpos($line['f_type_id_selector'], " :"));
            $type_oth = substr($line[$field_type_excel], 0,  strpos($line[$field_type_excel], " :"));
            if(!isset($sel[$type_sel][$line['f_code_selector']]) || !isset($oth[$type_oth][$line[$field_code_excel]])) continue;
            $fcodesel = $sel[$type_sel][$line['f_code_selector']];
            $fcodeoth = $oth[$type_oth][$line[$field_code_excel]];
            $select->reset();
            $check = $select->from($table_cross,array("num"=>"count(*)"))->where("$field_cross = ?",$fcodeoth)
                    ->where("f_selector_id = ?",$fcodesel)->query()->fetch();
            if($check['num'] > 0) continue;
            //insert cross
            $this->db->insert($table_cross,array(
                $field_cross=>$fcodeoth,
                'f_selector_id'=>$fcodesel,            
                'f_timestamp'=>  $time,
                'f_nesting_level'=>1
            ));                        
        }                
    }
            
}