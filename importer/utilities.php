<?php

class utilities
{
    
    public static $array_wf_phases = array();
    public static $array_uniques = array();    
    public static $fc_progress = 1;
    public static $creation_table_cols = array();
    public static $default_table_cols = array();
    public static $custom_table_cols = array();    
    public static $user_table_cols = array();
    public static $wizard_table_cols = array();
    public static $selectors_parent = array();    
    
    /**
     *Method to create selector parent list to 
     * link cross selector wares 
     */
    public static function createSelectorsParent()
    {        
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);
        $res_types = $select->from("t_selectors_types","f_id")->query()->fetchAll();
        foreach($res_types as $type) {
            $select->reset();
            $res_codes = $select->from("t_selectors","f_code")->where("f_type_id = ?",$type['f_id'])->query()->fetchAll();
            foreach($res_codes as $code) {                
                self::$selectors_parent[$type['f_id']][$code['f_code']] = self::findSelectorParent($code['f_code'],array());
            }
        }
        
    }
    
    private function findSelectorParent($f_code_sel = 0,$fathers = array())
    {           
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);      
        //check parents
        $select->reset();    
        try {
            $res_parents = $select->from("t_selectors_parent","f_parent_code")->where("f_code = ?",$f_code_sel)
                    ->where("f_active = 1")->where("f_parent_code != 0")->query()->fetchAll();        
        }catch(Exception $e) {        
            self::reportQueryError("t_selectors_parent", $e->getMessage(), $select->__toString());die;
        }
        foreach($res_parents as $par) {
            if(!in_array($par['f_parent_code'], $fathers) && $par['f_parent_code'] != $f_code_sel) {
                $fathers[] = $par['f_parent_code'];
                $fathers = self::findSelectorParent($par['f_parent_code'],$fathers);
            }
        }    
        $db->closeConnection();        
        return $fathers;
    }
    
    
    /**
     * Method to compile wf/phases
     * @param type $tab
     * @return type 
     */
    public static function setWfPhases()
    {   
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);
        $res = $select->from("t_wf_phases",array('f_wf_id','f_number'))->order("f_wf_id")->order("f_number")->query()->fetchAll();
        foreach($res as $line) {
            self::$array_wf_phases[$line['f_wf_id']][] = $line['f_number'];
        }        
    }
    
    public static function setDefaultTableCols($table = '') {
        if($table != '') {
            self::$default_table_cols = self::get_tab_cols($table);
        }
    }
    public static function setCreationTableCols() {        
        self::$creation_table_cols = self::get_tab_cols("t_creation_date");        
    }
    public static function setCustomTableCols() {        
        self::$custom_table_cols = self::get_tab_cols("t_custom_fields");        
    }
    
    /**
     * Set default columns of table t_users 
     */
    public static function setUserTableCols() 
    {
        self::$user_table_cols = self::get_tab_cols("t_users");
    }
    
    /**
     * Set default columns of table t_wizard
     */
    public static function setWizardTableCols() {        
        self::$wizard_table_cols = self::get_tab_cols("t_wizard");        
    }
    
    public static function checkUniquesArray($field,$data = array(),$position = 0) 
    {   
        if(empty($data) || empty($field)) return;     
        $array_uniques = array();
        $tot_a = count($data);
        for($a = 0;$a < $tot_a;++$a) {            
            $val = $data[$a][$field];
            if($field == 'f_code' && is_float($val)) {
                $val = "'$val'";
            }
            if(in_array($val,$array_uniques)){
                self::reportError("Error occurred at line ".($position+$a)." : $field must be unique and not empty");
            }
            $array_uniques[] = $val;
        }
    }
    
    /**
     *Set order to insert in mainsim for specific type
     * @param type $table : table interested (t_wares,t_workorders or t_selectors)
     * @param type $f_type_id : type id for the order
     */
    public static function setCreationDateOrder($table,$f_type_id) 
    {        
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);
        //get name of type
        $res_name_type = $select->from("{$table}_types","f_type")->where("f_id = $f_type_id")->query()->fetchAll();
        $type = strtoupper($res_name_type[0]['f_type']);
        $select->reset();
        $res_f_order = $select->from("t_creation_date",array("f_order"=>"MAX(f_order)"))
                ->where("f_type = ?",  strtoupper($table))->where("f_category = ?",$type)->query()->fetchAll();        
        $order = (int)$res_f_order[0]['f_order'];           
        $order = $order?$order:1;
        Zend_Registry::set("f_order",$order);        
    }
    
    public static function get_tab_cols($tab) 
    {           
        $db = Zend_Registry::get('db');
        Zend_Db_Table::setDefaultAdapter($db);
        $table = new Zend_Db_Table($tab);
        $columns_raw = $table->info('cols');       
        $columns = array();
        foreach($columns_raw as $line) {
            $columns[] = strtolower($line);
        }
        $db->closeConnection();
        return $columns;
    }
    
    
    /**
     * Create fc_progress      
     */
    public static function setFirstFcProgress($mdl_name) 
    {        
        $db = Zend_Registry::get('db');
        //search type to check                        
        $sel_prog = new Zend_Db_Select($db);
        //get f_code
        try {            
            $res_tg = $sel_prog->from("t_ui_object_instances",array("f_properties"))
                ->where("f_instance_name = ?",$mdl_name)->query()->fetchAll();
        }catch(Exception $e) {
            self::reportQueryError("t_ui_object_instances",$e->getMessage(),$sel_prog->__toString());
            echo json_encode(array("response"=>'ko'));die;
        }     
        self::$fc_progress = 1;
        if(!empty($res_tg)) {            
            $prop = json_decode(self::chg($res_tg[0]['f_properties']),true);
            $table = $prop['table'];
            $f_type_prog = $prop['ftype'];                            
            $sel_prog->reset();
            $res_prog = $sel_prog->from(array("t1"=>"t_custom_fields"),array('num'=>'count(*)'))
                    ->join(array("t2"=>$table),"t1.f_code = t2.f_code",array())
                    ->where("t2.f_type_id IN ($f_type_prog)")->query()->fetchAll();
            self::$fc_progress = $res_prog[0]['num']?(int)$res_prog[0]['num']:1;            
        }        
    }
    
    /**
     * Report general error during import
     * @param type $message
     * @param type $id 
     */
    public static function reportError($message,$delete_all = true)
    {        
        
        $id = Zend_Registry::get('f_id');        
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);        
        if($delete_all && $id) {                    
            $res = $select->from("t_queue","f_rollback")->where("f_id = ?",$id)->query()->fetchAll();                        
            if(!empty($res)) {
                $rollback = unserialize($res[0]['f_rollback']);                                     
                if(is_array($rollback)) self::deleteDataInserted($rollback);            
            }
        }        
        //insert a message error in t_queue and t_logs                
        $db->update("t_queue",array("f_message"=>$message,'f_active'=>-1),"f_id = $id");        
        $db->insert("t_logs",array("f_log"=>$message,'f_timestamp'=>time(),'f_type_log'=>0));                        
        echo json_encode(array("response"=>"ko",'message'=>self::chg($message)));die;
    }
    
    /**
     * Report specific query error during import
     * @param type $table : table where verify error
     * @param type $error : string of error
     * @param type $id : id of request to import
     */
    public static function reportQueryError($table,$error,$query,$delete_all = true)
    {        
        $id = Zend_Registry::get('f_id');        
        $db = Zend_Registry::get('db');
        //insert a message error in t_queue
        $message = "<div>Something went wrong during execution of a query in $table .<br /> This is the db error generated : ".
                $error."<br />and this is the query : ".$query."</div> ";       
        $select = new Zend_Db_Select($db);
        if($delete_all && $id) {            
            $res = $select->from("t_queue","f_rollback")->where("f_id = ?",$id)->query()->fetchAll();
            if(!empty($res)) {
                $rollback = unserialize($res[0]['f_rollback']);                
                if(is_array($rollback)) self::deleteDataInserted($rollback);            
            }
        }  
        //update t_queue and insert log row
        $db->update("t_queue",array("f_message"=>$message,'f_active'=>-1),"f_id = $id");
        $db->insert("t_logs",array("f_log"=>$message,'f_timestamp'=>time(),'f_type_log'=>0));        
         echo json_encode(array("response"=>"ko",'message'=>self::chg($message)));die;
    }
    
    /**
     * Report specific insert error
     * @param type $table
     * @param type $error
     * @param type $insert_data
     * @param type $id 
     */
    public static function reportInsertError($table,$error,$insert_data,$delete_all = true)
    {
        $id = Zend_Registry::get('f_id');        
        $db = Zend_Registry::get('db');
        //insert a message error in t_queue
        $message = "<div>Something went wrong during inserting in $table .<br /> This is the db error generated : $error <br />
            and this is the insert data : $insert_data </div> ";        
        $select = new Zend_Db_Select($db);
        if($delete_all && $id) {            
            $res = $select->from("t_queue","f_rollback")->where("f_id = ?",$id)->query()->fetchAll();
            if(!empty($res)) {
                $rollback = unserialize($res[0]['f_rollback']);
                if(is_array($rollback)) self::deleteDataInserted($rollback);            
            }
        }  
        //update t_queue and insert log row
        $db->update("t_queue",array("f_message"=>$message,'f_active'=>-1),"f_id = $id");
        $db->insert("t_logs",array("f_log"=>$message,'f_timestamp'=>time(),'f_type_log'=>0));        
         echo json_encode(array("response"=>"ko",'message'=>self::chg($message)));die;
    }
    
    /**
     * during delete, clear all ui field
     * @param type $searched 
     */
    /*
    public static function clearUi($searched)
    {           
        $db = Zend_Registry::get('db');
        $sel = new Zend_Db_Select($db);      
        try {
            //find every module for remove ui        
            $sel->reset();
            $res_modules = $sel->from("t_systems",array("fc_ui_fields",'f_code','fc_ui_type'))
                    ->join("t_creation_date","t_systems.f_code = t_creation_date.f_id",array("f_instance_name"=>"f_title"))
                    ->join("t_selector_system","t_selector_system.f_system_id = t_systems.f_code",array('f_selector_id'))                    
                    ->where("f_selector_id = 13")->where("(f_title like '%{$searched}_edit%' OR f_title like '%{$searched}_pp_edit%' )")
                    ->query()->fetchAll();            
            foreach($res_modules as &$line_mod) {            
                $sel->reset();            
                $create_tg = substr($line_mod['f_instance_name'],0,strpos($line_mod['f_instance_name'],"edit_")).'tg';
                try {
                    $res_pair = $sel->from("t_systems",array('f_id'))
                        ->join("t_creation_date","t_systems.f_code = t_creation_date.f_id",array())    
                        ->where("f_title = ?",$create_tg)
                        ->where("fc_ui_type like '%moTreeGrid_light_check_pair%'")->query()->fetchAll();
                }catch(Exception $e) {              
                    echo $e->getMessage();
                    $db->insert("t_logs",array("f_log"=>$e->getMessage(),'f_timestamp'=>time(),'f_type_log'=>0));die;
                };
                if(!empty($res_pair)) continue;                 
                //decode the properties and get the list of component            
                $fields = explode(',',$line_mod['fc_ui_fields']);                
                $accep_fields = array();
                foreach($fields as $field) {
                    $sel->reset();
                    $res_ui = $sel->from("t_systems",array('fc_ui_bind'))
                            ->join("t_creation_date","t_systems.f_code = t_creation_date.f_id",array("f_id",'f_title'))
                            ->where("f_title = ?",$field)->where("f_type = 'SYSTEM'")->query()->fetch();
                    if(empty($res_ui)) continue;                    
                    //check if ui is binded with system column or custom                
                    if(strpos($res_ui['fc_ui_bind'],'f_',0) !== 0 &&  strpos($res_ui['fc_ui_bind'],'fc_',0) !== 0) {                        
                        $sel->reset();
                        $children_ui = array();
                        $children_ui = $sel->from("t_systems_parent",array("f_code"))
                                ->where("f_active = 1")->where("f_parent_code = ?",$res_ui['f_id'])
                                ->query()->fetchAll();
                        $children_ui[] = array('f_code'=>$res_ui['f_id']);                        
                        $tot_c = count($children_ui);                        
                        for($c = 0;$c < $tot_c;++$c) {                            
                            try {                                
                                $db->delete("t_selector_system","f_system_id = {$children_ui[$c]['f_code']} ");                    
                                $db->delete("t_systems_parent","f_code = {$children_ui[$c]['f_code']} ");
                                $db->delete("t_systems","f_code = {$children_ui[$c]['f_code']} ");
                                $db->delete("t_custom_fields","f_code = {$children_ui[$c]['f_code']} ");
                                $db->delete("t_creation_date","f_id = {$children_ui[$c]['f_code']} ");                                
                            }catch(Exception $e) {                                
                                echo $e->getMessage();
                                utilities::reportError($e->getMessage());
                            }
                        }                    
                    }
                    else {
                        $accep_fields[] = $field;
                    }                
                }                
                $accep_fields = implode(',',$accep_fields);
                $db->update("t_systems",array("fc_ui_fields"=>$accep_fields),"f_code = '{$line_mod['f_code']}'");
            } 
        }catch(Exception $e) {
            echo $e->getMessage();
            utilities::reportError($e->getMessage());
        }
    }*/
    public static function clearUi($searched,$db = null)
    {           
        $db = is_null($db)?Zend_Registry::get('db'):$db;
        $sel = new Zend_Db_Select($db);        
        //find every module for remove ui        
        $sel->reset();
        $res_modules = $sel->from("t_ui_object_instances",array("f_instance_name","f_properties"))
                ->where("f_type_id = 9")->where("(f_instance_name like '%{$searched}_edit%' OR f_instance_name like '%{$searched}_pp_edit%' )")
                ->where("f_properties like '%moModuleComponent%'")->query()->fetchAll();        
        foreach($res_modules as &$line_mod) {            
            $sel->reset();
            
            $create_tg = substr($line_mod['f_instance_name'],0,strpos($line_mod['f_instance_name'],"edit_")).'tg';
            try {
                $res_pair = $sel->from("t_ui_object_instances",array('f_code'))
                    ->where("f_type_id = 9")->where("f_instance_name = ?",$create_tg)
                    ->where("f_properties like '%moTreeGrid_light_check_pair%'")->query()->fetchAll();
            }catch(Exception $e) {                                
                $db->insert("t_logs",array("f_log"=>$e->getMessage(),'f_timestamp'=>time(),'f_type_log'=>0));die;
            };
            if(!empty($res_pair)) continue; 
            
            //decode the properties and get the list of component
            $mod_prop = json_decode($line_mod['f_properties'],true);            
            $fields = explode(',',$mod_prop['fields']);
            $accep_fields = array();
            foreach($fields as $field) {
                $sel->reset();
                $res_ui = $sel->from("t_ui_object_instances",array("f_code","f_instance_name","f_properties"))
                        ->where("f_instance_name = ?",$field)->query()->fetchAll();
                if(empty($res_ui)) continue;
                //check if ui is binded with system column or custom
                $ui_prop = json_decode($res_ui[0]['f_properties'],true);
                if(strpos($ui_prop['bind'],'f_',0) !== 0 &&  strpos($ui_prop['bind'],'fc_',0) !== 0) {
                    $db->delete("t_ui_object_instances","f_instance_name = '{$res_ui[0]['f_instance_name']}' ");                    
                }
                else {
                    $accep_fields[] = $field;
                }                
            }            
            $mod_prop['fields'] = implode(',',$accep_fields);            
            $enc = json_encode($mod_prop);                        
            $db->update("t_ui_object_instances",array("f_properties"=>$enc),"f_instance_name = '{$line_mod['f_instance_name']}'");
        }                
    }
    
    public static function clearBm($searched)
    {   
        $db = Zend_Registry::get('db');
        $sel = new Zend_Db_Select($db);
        $module = "mdl_{$searched}_tg";
        $res_bm_name = $sel->from(array("t1"=>"t_creation_date"),array("f_id"))
            ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code",array())                                
            ->where("f_type_id = 3")->where("fc_bkm_module = ?",$module)
            ->where("fc_bkm_user_id = 0")->query()->fetchAll();                
        $tot_bm = count($res_bm_name);        
        for($bm = 0;$bm < $tot_bm;++$bm) {
            //find true bm
            $sel->reset();
            $res_bm_fields = $sel->from("t_bookmarks",array("f_id","fc_bkm_bind"))
                ->where("f_code = ?",$res_bm_name[$bm]['f_id'])
                ->where("(fc_bkm_bind not like 'fc_%' AND fc_bkm_bind not like 'f_%')")
                ->query()->fetchAll();  
            $tot_fields = count($res_bm_fields);
            for($b = 0;$b < $tot_fields;++$b) {                
                $db->delete("t_bookmarks","f_id = {$res_bm_fields[$b]['f_id']}");
            }              
        }           
    }
    
    public static function deleteBookmarks($db,$module = '') 
    {
        $select = new Zend_Db_Select($db);        
        $delete_condition = "";
        $delete_condition_creation = "";
        $delete_condition_creation_history = "";        
        $select->from(array("t1"=>"t_creation_date"),"f_id")
                ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code",array())
                ->where("fc_bkm_user_id != 0")                
                ->where("f_category = 'BOOKMARK'");
        
        if($module != '') {
            $select->where("fc_bkm_module = ?",$module);                        
        }        
        $res_code = $select->query()->fetchAll();          
        $codes = array();
        $tot_code = count($res_code);
        for($c = 0;$c < $tot_code;++$c) {
            $codes[] = $res_code[$c]['f_id'];
        }
        if(!empty($codes)) {
            $codes = implode(',',$codes);                    
            $delete_condition = $codes;
            $delete_condition_creation = "f_id IN ($codes)";
            $delete_condition_creation_history = "f_code IN ($codes)";
        }
        else return;
        $db->delete("t_systems_parent","f_code IN ($delete_condition)");                                
        $db->delete("t_custom_fields_history","f_code IN ($delete_condition $user)");            
        $db->delete("t_custom_fields","f_code IN ($delete_condition)");
        $db->delete("t_selector_system","f_system_id IN ($delete_condition)");
        $db->delete("t_wares_systems","f_system_id IN ($delete_condition)");            
        $db->delete("t_bookmarks","f_code IN ($delete_condition)");        
        $db->delete("t_systems_history","f_code IN ($delete_condition)");
        $db->delete("t_systems","f_code IN ($delete_condition)");        
        $db->delete("t_locked","f_code IN ($delete_condition)");        
        $db->delete("t_creation_date_history","f_type = 'SYSTEM' AND $delete_condition_creation_history");      
        $db->delete("t_creation_date","f_type = 'SYSTEM' AND $delete_condition_creation");
    }
    
        
    public static function clearCustom() 
    {        
        $db = Zend_Registry::get('db');
        $custom_cols = self::get_tab_cols("t_custom_fields");        
        foreach($custom_cols as $line) {
            if((strpos($line,"fc_") === false && strpos($line,"f_") === false)
               || (strpos($line,"fc_") != 0 || strpos($line,"f_") != 0) ) {                
                try {
                    $db->query("alter table t_custom_fields drop column {$line}");                
                    $db->query("alter table t_custom_fields_history drop column {$line}");
                }catch(Exception $e) {};
            }
        }        
    }
    
    /**
     *  get sheets from update id and delete every data insert
     */
    public static function deleteDataInserted($roll_back_list = array())
    {        
        $db = Zend_Registry::get('db');
        $config = Zend_Registry::get('db_config');
        $conf = $config->toArray();    
        $is_mysql = true;
        if(stripos($conf['database']['adapter'], 'sqlsrv') !== false) {
            $is_mysql = false;
        }
    
        if(is_array($roll_back_list)) {
            try {
                foreach($roll_back_list as $table => $id) {
                    $db->delete($table,"f_id > $id");
                    if($table == 't_creation_date') {
                        $pos = $id+1;
                        if($is_mysql) {
                            $db->query("alter table t_creation_date AUTO_INCREMENT = $pos");
                        }
                        else {
                            try{
                                $db->query("DBCC CHECKIDENT (t_creation_date, RESEED, $pos)");
                            }catch(Exception $e){}
                        }
                    }
                }        
            }catch(Exception $e) {                       
                self::reportError($e->getMessage(), false);            
            }        
        }        
    }
    
    public static function chg($str) 
    {
        $cp1252_map = array ("\xc2\x80" => "\xe2\x82\xac",
        "\xc2\x82" => "\xe2\x80\x9a",
        "\xc2\x83" => "\xc6\x92",    
        "\xc2\x84" => "\xe2\x80\x9e",
        "\xc2\x85" => "\xe2\x80\xa6",
        "\xc2\x86" => "\xe2\x80\xa0",
        "\xc2\x87" => "\xe2\x80\xa1",
        "\xc2\x88" => "\xcb\x86",
        "\xc2\x89" => "\xe2\x80\xb0",
        "\xc2\x8a" => "\xc5\xa0",
        "\xc2\x8b" => "\xe2\x80\xb9",
        "\xc2\x8c" => "\xc5\x92",
        "\xc2\x8e" => "\xc5\xbd",
        "\xc2\x91" => "\xe2\x80\x98",
        "\xc2\x92" => "\xe2\x80\x99",
        "\xc2\x93" => "\xe2\x80\x9c",
        "\xc2\x94" => "\xe2\x80\x9d",
        "\xc2\x95" => "\xe2\x80\xa2",
        "\xc2\x96" => "\xe2\x80\x93",
        "\xc2\x97" => "\xe2\x80\x94",
        "\xc2\x98" => "\xcb\x9c",
        "\xc2\x99" => "\xe2\x84\xa2",
        "\xc2\x9a" => "\xc5\xa1",
        "\xc2\x9b" => "\xe2\x80\xba",
        "\xc2\x9c" => "\xc5\x93",
        "\xc2\x9e" => "\xc5\xbe",
        "\xc2\x9f" => "\xc5\xb8");
        return strtr ( utf8_encode ( $str ), $cp1252_map );
    }
    
    /**
     * create new column in t_custom_fields and t_custom_fields history
     * @param string $col_name : name of column
     * @param array $col_data : array with information about column
     */
    public static function createCustomCol($col_name,$col_data) 
    {
        $db = Zend_Registry::get('db');
        //get adapter to create right command
        $config = Zend_Registry::get('db_config');
        $config = $config->toArray();
        $pdo = $config['database']['adapter'];
        $null = stripos($pdo,"mysql")!== false?'DEFAULT NULL':'NULL';
        // don't create column start at 'f_' or 'fc_'
        if(strpos($col_name,"fc_") === 0 ||  strpos($col_name,"f_") === 0) return;        
        
        $type_column = "TEXT";                        
        $type_custom = substr(str_replace(" : ","",$col_data['f_type']),1);
        switch($type_custom) {
            case 'VARCHAR':
                if(!isset($col_data['f_column_length']) || empty($col_data['f_column_length'])) {
                    self::reportError("The length of field $col_name must be setted");
                }
                $length = (int)$col_data['f_column_length'];
                $type_column = "VARCHAR({$length}) ";
                if(stripos($pdo,"mysql")!== false && $length > 256 ) {
                    $type_column = "TEXT";
                }
                break;
            case 'TEXT' : 
                $type_column = stripos($pdo,"mysql")!== false?"TEXT":"VARCHAR(MAX)";
                break;
            case 'BOOLEAN' :                
            case 'INT' :                
            case 'DATE' :
                $type_column = "INT";
                break;    
            case "DOUBLE" : 
                $type_column = stripos($pdo,"mysql")!== false?"DOUBLE":"FLOAT";
                break;
        }                            
        //create column in t_custom_fields and t_custom_fields history
        try {
            $db->query("ALTER TABLE t_custom_fields ADD $col_name $type_column  $null");
        }catch(Exception $e) {                
            self::reportQueryError("t_custom_fields",$e->getMessage(),"ALTER TABLE t_custom_fields ADD $col_name $type_column $null");            
        };
        try {
            $db->query("ALTER TABLE t_custom_fields_history ADD $col_name $type_column  $null");
        }catch(Exception $e) {
            self::reportQueryError("t_custom_fields",$e->getMessage(),"ALTER TABLE t_custom_fields_history ADD $col_name $type_column $null");            
        };        
    }
    
    /**
     * write in a custom fields selectors connect with a wares
     * @param type $table set if type id passed is a wares or wo
     * @param type $f_type_id 
     */
    public static function selWareWoField($table,$f_type_id)
    {        
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);
        $cross_table = $table == 't_wares'?'t_selector_ware':'t_selector_wo';
        $cross_field = $table == 't_wares'?'f_ware_id':'f_wo_id';
        $res = $select->from(array("t1"=>"t_selectors"),array("f_title","f_type_id"))
                ->join(array("t2"=>$cross_table),"t1.f_code = t2.f_selector_id",array())
                ->join(array("t3"=>$table),"t2.$cross_field = t3.f_code",array('f_code'))                
                ->where("t3.f_type_id = $f_type_id")->where("t2.f_nesting_level = 1")                
                ->query()->fetchAll();
        $tot_a = count($res);
        $sel_fields = array();
        for($a = 0; $a < $tot_a;++$a) {
            $line = $res[$a];
            $select->reset();
            $res_parent = $select->from(array("t1"=>"t_selectors"),array("f_title"))
                    ->join(array("t2"=>$cross_table),"t1.f_code = t2.f_selector_id",array())
                    ->where("t2.$cross_field = {$line['f_code']}")
                    ->where("t2.f_nesting_level = 0")->where("t1.f_type_id = ?",$line['f_type_id'])
                            ->group("t1.f_code")->order("t1.f_code DESC")
                    ->query()->fetchAll();           
             $tot_b = count($res_parent);       
             for($b = 0;$b < $tot_b;++$b) {
                 $sel_fields[$line['f_code']][$line['f_title']][] = $res_parent[$b]['f_title'];
             }             
        }
        
        foreach($sel_fields as $key => $value) {
            $hy = array();
            foreach($value as $sel => $sel_parent) {
                $hy[] = $sel.' ('.implode(', ',$sel_parent).')';
            }
            $tot_sels = implode('; ',$hy);            
            $db->update("t_custom_fields",array("fc_selector"=>$tot_sels),"f_code = $key");
        }
    }
    
    
    /**
     * exec check inside ui object
     * @param type $v value
     * @param type $c condition
     * @return int 1 = ok, 0 = ko
     */
    public static function check($v,$c,$col = '',$table = '',$data = array(),$row = 0,$f_type_id = 0,$value_check = '')
    {           
        $risp=1; 
        $arrc=explode(" ",$c);  
        $na=count($arrc);   // ,cc,pp,app,ss,s,pi,pf,vl;

        $vl=strlen($v); 

        for($r=0;$r<$na;$r++){
            $cc=$arrc[$r]; 
            if($cc=="mail")  $risp=self::checkMail($v);
            if($cc=="int")   $risp=self::checkInt($v);
            if($cc=="unique")   $risp=self::checkUnique($v,$col,$table,$f_type_id);
            if($cc=="uniqueCode") return self::checkUniqueCode($v,$col,$table,$f_type_id);
            if($cc=="wf_exist") $risp=self::checkWf($v);
            if($cc=="phase_exist") $risp=self::checkWfPhase($v,$data[$row]['f_wf_id']);
            if($cc=='valid_date') $risp=self::checkValidDate($v);
            if($cc=='empty_field') $risp=self::checkEmpty($v);
            if($cc=='f_type') $risp=self::checkType($v);
            if($cc=='maxLength') $risp=self::maxLength($v,$value_check);
            if($cc=='minLength') $risp=self::minLength($v,$value_check);
            if($cc=='inList') $risp=self::inList($v,$value_check);
            if($cc=='notInList') $risp=self::notInList($v,$value_check);
            
            $app=explode("_",$cc); 
            //if value exist, check condition
            if($vl){
                if(substr($cc,0,5)=="nchar") {
                    if(count($app)>2) {
                        $pi=(int) ($app[1]);
                        $pf=(int) ($app[2]);
                        if($vl<$pi || $vl>$pf) $risp=0;
                    } else {
                        if($vl != ((int) ($app[1])) ) $risp=0;
                    } 
                }

                if(substr($cc,0,3)=="not") {
                    $ss=$app[1];
                    if(count($app)>2)  $ss.=$app[2]+"_";
                    for($n=0;$n<strlen($ss);$n++) {
                        $s=substr($ss,$n,1); 
                        if(strpos($v,$s)!==false) {
                            $risp=0;
                            break;
                        }
                    }
                }

                if(substr($cc,0,5)=="range") {
                    if(!is_numeric($v)) $risp=0;
                    else {
                        $v=(double) $v;

                        if($app[1]=="m" && $app[2]=="M") {  }
                        else {
                            $i=$app[1]; $f=$app[2]; $mM=1;

                            if($i=="m") { $f=(double) $f;  
                                if($v>$f) $risp=0;
                                $mM=0;
                            } else if($f=="M") { $i=(double) $i;
                                if($v<$i) $risp=0; 
                                $mM=0; 
                            }

                            if($mM) { $f=(double) $f; $i=(double) $i;
                                if($v<$i || $v>$f) $risp=0;  }
                        }
                    }
                }

                if( substr($cc,0,7)=="decimal") {   
                    if(!is_numeric($v)) $risp=0;
                    else { 
                        $pp=explode(".",$v);
                        $s=count($pp);                            
                        if($s && strlen($pp[1])>$app[1]) $risp=0; 
                    }
                }
                if(strpos($cc,"regexp")!== false) {
                    $risp = 0;
                    if(preg_match_all($app[1],$v)) {
                        $risp = 1;
                    }
                }
            } 
            if(!$risp) return 0;
        } //
        return 1;
    }


    /**
     * check if passed value is an int
     * @param type $v = value
     * @return int 1 = ok, 0= ko
     */
    private static function checkInt($v)
    {
        if($v=="" || is_long($v)) return 1;
        $rsp=1; 
        for($r=0;$r<strlen($v);$r++){
            $c=substr($v,$r,1); $o=ord($c);
            if($o<48 || $o>57) return 0;
        }
        return 1;
    }
    
    /**
     * check if the value is unique inside the system
     * @param type $v value to check
     * @param type $col colum to check
     * @param type $table table to check
     * @param type $data other data passed from excel
     */
    private static function checkUnique($v,$col,$table,$f_type_id)
    {           
        //if $v is empty, return false;        
        if(empty($v) || is_null($v)) return false;
                
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);                
        
        $tab_cols = self::get_tab_cols($table);
        $db_col = in_array($col,$tab_cols)?"t1.$col":"t2.$col";        
        //now check inside the system             
        try {            
            $res_unique = $select->from(array("t1"=>$table),array('num'=>'count(*)'))                    
                    ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array())
                    ->where("$db_col = $v")->where("f_type_id = ?",$f_type_id)->query()->fetchAll();          
        }catch(Exception $e) {return false;}
        
        if($res_unique['num'] > 0) return false;
        
        return true;
    }
    
    
    /**
     * check if the value is unique inside the system
     * @param type $v value to check
     * @param type $col colum to check
     * @param type $table table to check
     * @param type $data other data passed from excel
     */
    private static function checkUniqueCode($codes_list,$col,$table,$f_type_id)
    {           
        //if $v is empty, return false;        
        if(empty($codes_list) || is_null($codes_list)) return false;        
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);        
        //now check inside the system             
        try {            
            $res_unique = $select->from(array("t1"=>$table),array($col))
                ->where("$col IN ($codes_list)")->where("f_type_id = ?",$f_type_id)
                ->query()->fetchAll();                   
        }catch(Exception $e) {die($e->getMessage());return false;}        
        if(!empty($res_unique)) return $res_unique[0][$col];                
        return false;
    }

    /**
     * check if is a valid mail
     * @param type $email 
     * @return boolean 
     */
    private static function checkMail($email) {
        // First, we check that there's one @ symbol, and that the lengths are right
        if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
            // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
            return false;
        }
        // Split it into sections to make life easier
        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_{|}~-][A-Za-z0-9!#$%&'*+\/=?^_{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
                return false;
            }
        }
        if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
            $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
                if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static function checkWf($wf)
    {        
        $wf = (int)$wf;        
        if($wf == '' || array_key_exists($wf, self::$array_wf_phases)) return true;
        return false;
    }
    
    public static function checkWfPhase($phase = '',$wf = '')
    {
        $wf = (int)$wf;
        if($phase == '' && $wf == '') return true;
        if(!array_key_exists($wf, self::$array_wf_phases)) return false;
        if(!in_array($phase, self::$array_wf_phases[$wf])) return false;
        return true;        
    }
    
    public static function checkValidDate($v)
    {     
        if($v == '') return true;
        $ts = strtotime($v);
        return !$ts?false:true;
    }
    
    /**
     * create father asset
     * @param type $type_id 
     */    
    public static function createFathers($table,$type_id)
    {   
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);
        $timestamp = Zend_Registry::isRegistered("f_timestamp")?Zend_Registry::get("f_timestamp"):time();
        $col_code = '';        
        $cols = array('f_code','fc_imp_parent_code');        
        if($table == 't_wares') {
            $col_code = 'fc_imp_code_wares_excel';            
        }
        elseif($table == 't_workorders') {
            $col_code = 'fc_imp_code_wo_excel';            
        }
        else{
            $col_code = 'fc_imp_code_sel_excel';            
        }
        
        if($table == 't_wares' && $type_id == 9) {
            $cols[] = 'fc_imp_parent_code_wizard';            
        }
        
        $select = new Zend_Db_Select($db);         
         try {
            //object without father
            $objects = $select->from(array("t1"=>"t_custom_fields"),$cols)
                    ->join(array("t3"=>$table),"t1.f_code = t3.f_code",array($col_code))                                        
                    ->where("t3.f_type_id = {$type_id}")->where("t3.f_code not in (select f_code from {$table}_parent)")->query()->fetchAll();            
        }catch(Exception $e) {            
            utilities::reportQueryError("t_custom_fields",$e->getMessage(),$select->__toString());            
            echo json_encode(array("response"=>'ko'));die;
        };
                
        $tot_a = count($objects);
        for($a = 0;$a < $tot_a;$a++){        
            $obj = $objects[$a];
            $f_code = $obj['f_code'];
            $f_parent_code_excel = $table == 't_wares' && $type_id == 9?$obj['fc_imp_parent_code_wizard']:$obj['fc_imp_parent_code'];                            
            //search original f_code in t_custom_fields
            $f_parent_code = 0;
            if($f_parent_code_excel != "" ) {                    
                $parent_array = array();
                if(strpos($f_parent_code_excel,",")!== false) {
                    $parent_array = explode(",",$f_parent_code_excel);
                }
                else {
                    $parent_array = explode(".",$f_parent_code_excel);
                }                
                foreach($parent_array as $line_array) {                    
                    if($line_array == '') continue;
                    
                    $select->reset();
                    try {
                        $res_custom = $select->from($table,"f_code")->where("$col_code = ?",$line_array)
                            ->where("f_type_id = ?",$type_id)->query()->fetch();
                    }
                    catch(Exception $e) {                                                
                        utilities::reportQueryError($table,$e->getMessage(),$select->__toString());
                        echo json_encode(array("response"=>'ko'));die;
                    };
                    $f_parent_code = 0;                    
                    if(!empty($res_custom) && $res_custom['f_code'] != $f_code){
                        $f_parent_code = $res_custom['f_code'];                         
                    }
                    $parent = array(
                        'f_parent_code'=>$f_parent_code,
                        'f_code'=>$f_code,
                        'f_active'=>1,
                        'f_timestamp'=>$timestamp         
                    );     
                    try {
                        $db->insert("{$table}_parent",$parent);
                    }
                    catch(Exception $e) {                               
                        $dump = Zend_Debug::dump($parent,null,false);
                        utilities::reportInsertError("{$table}_parent",$e->getMessage(),$dump);
                        echo json_encode(array("response"=>'ko'));die;
                    }
                }
            }            
            else {
                $parent = array(
                    'f_parent_code'=>$f_parent_code,
                    'f_code'=>$f_code,
                    'f_active'=>1,
                    'f_timestamp'=>time()                
                );       
                try {
                    $db->insert("{$table}_parent",$parent);
                }catch(Exception $e) {                    
                    $dump = Zend_Debug::dump($parent,null,false);
                    utilities::reportInsertError("{$table}_parent", $e->getMessage(), $dump);
                    echo json_encode(array("response"=>'ko'));die;
                }
            }            
        }        
        
        if($table == 't_wares' && $type_id == 9) return;
        //search check if parent code = 0 is truely 0 or a parent code not found        
        $select->reset();
        try {
            $res_parent = $select->from(array("t1"=>"{$table}_parent"),array())
                ->join(array("t3"=>$table),"t1.f_code = t3.f_code",array($col_code))
                ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",$cols)
                ->where("t1.f_active = 1")->where("f_type_id = ?",$type_id)
                ->where("f_parent_code = 0")->query()->fetchAll();
        }catch(Exception $e) {            
            utilities::reportQueryError("{$table}_parent",$e->getMessage(),$select->__toString());
            echo json_encode(array("response"=>'ko'));die;
        }
        $tot_c = count($res_parent);
        for($c = 0; $c < $tot_c; ++$c) {
            $line = $res_parent[$c];            
            if(!$line['fc_imp_parent_code']) continue;

            //if fc_imp_parent_code != 0, search father
            $parent_array = array();
            if(strpos($line['fc_imp_parent_code'],",")!== false) {
                $parent_array = explode(",",$line['fc_imp_parent_code']);
            }
            else {
                $parent_array = explode(".",$line['fc_imp_parent_code']);
            }
            foreach($parent_array as $line_array) {
                if($line_array == '') continue;
                $select->reset();
                try {
                    $res_search_parent_code = $select->from($table,array("f_code"))
                        ->where("$col_code = ?",$line_array)->where("f_type_id = ?",$type_id)
                        ->query()->fetchAll();
                }catch(Exception $e) {                    
                    utilities::reportQueryError($table,$e->getMessage(),$select->__toString());die;
                }
                //if not exist, continue
                if(empty($res_search_parent_code)) continue;                
                //change parent code
                try {
                    $db->update("{$table}_parent",array("f_parent_code"=>$res_search_parent_code[0]['f_code']),"f_code = {$line['f_code']} and f_active = 1");
                }catch(Exception $e) {                    
                    utilities::reportError("Something went wrong during an update : ".$e->getMessage());die;
                }
            }
        }                
    }
    
    public static function checkEmpty($v) 
    {
        $v = trim($v);
        return  $v == '' || is_null($v)? false:true;
    }
    
    public static function checkType($v)
    {
        $res = false;
        $exp_v = explode(' : ',$v);
        if(count($exp_v) == 2) {
            if(ctype_digit($exp_v[0])) $res = true;
        }        
        return $res;
    }
    
    public static function clean_col_name($name)
    {
        $black_list = array(" "=>"_","'"=>"_","("=>"",
            ")"=>"","="=>"_","/"=>"","-"=>"","%"=>"",
            "Â°"=>"");     
        $f = $name;
        foreach($black_list as $key => $value) {
            $f = str_replace($key,$value,$f);
        }
        return $f;
    }
    
    /**
     * Get list of parents code
     * @param string $table : name of father table (ex : t_wares_parent)
     * @param int $f_code
     * @param array $fathers     
     * @return array : list of parents 
     */
    public static function getParentCode($table = '',$f_code = 0,$fathers = array())
    {    
        $db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($db);      
        if(!is_array($f_code)) {
            $f_code = array($f_code);
        }
        $f_code_list = implode(',',$f_code);
        $new_code = array();
        //check parents
        $select->reset();            
        $select->from($table,"f_parent_code")->where("f_code IN ($f_code_list)")->where("f_active = 1")->where("f_parent_code != 0");         
        if(!empty($fathers)) {
            $exclude = implode(',',$fathers);
            $select->where("f_parent_code not in ($exclude)");
        }
        $res_parents = $select->query()->fetchAll();
        $tot_a = count($res_parents);        
        for($a = 0;$a < $tot_a;++$a){            
            $fathers[] = $res_parents[$a]['f_parent_code'];
            $new_code[] = $res_parents[$a]['f_parent_code'];            
        }    
        if(!empty($new_code)) {
            $fathers = self::getParentCode($table,$new_code,$fathers);
        }
        return $fathers;
    }
    
    /**
     * check max length of passed value
     * @param string $value string to check
     * @param int $length max length allow
     * @return boolean true if is less than length passed, otherwise false
     */
    private function maxLength($value,$length)
    {
        if(empty($value) || empty($length)) return true; //don't apply check
        if(strlen($value) > $length) return false; // too long
        return true;// it's ok
    }
    
    /**
     * check min length of passed value
     * @param string $value string to check
     * @param int $length min length allow
     * @return boolean true if is over length passed, otherwise false
     */
    private function minLength($value,$length)
    {
        if(empty($value) || empty($length)) return true; //don't apply check
        if(strlen($value) < $length) return false; // too short
        return true;// it's ok
    }
    
    /**
     * check if passed value is inside passed list
     * @param string $value value to check
     * @param string $list list of allow values separated by comma
     * @return boolean true if ok, otherwise false
     */
    private static function inList($value,$list)  
    {
        if(empty($value) || empty($list)) return true; //don't apply check
        $list = explode(',',$list); //change string in array
        if(!in_array($value, $list)) return false; //not in list
        return true; // ok
    }
    
    /**
     * check if passed value is not inside passed list
     * @param string $value value to check
     * @param string $list list of values separated by comma
     * @return boolean true if not in list, otherwise false
     */
    private static function notInList($value,$list)  
    {
        if(empty($value) || empty($list)) return true; //don't apply check
        $list = explode(',',$list); //change string in array
        if(in_array($value, $list)) return false; //not in list
        return true; // ok
    }
    
    /**
     * Insert slice of excel
     * @param type $sheet_name
     * @param type $check_list
     * @param type $table
     * @param type $f_type_id
     * @param type $mdl
     * @param type $custom
     * @param type $objects 
     */
    public static function manageTrunk($sheet_name,$check_list,$table,$f_type_id,$mdl,$custom,$position,$objects)            
    {
        $db = Zend_Registry::get('db');
        //initialize method to import
        $wares = new Wares();
        $selector = new Selector();
        $wo = new Workorders();        
        $mp = new mp_class();
        //create array with unique field
        if(!in_array($sheet_name,array("t_cross_wares_selectors","t_cross_wares_wares","t_cross_workorders_wares","t_cross_workorders_selectors","t_engagement_meter","t_engagement"))) {
            foreach($check_list[$sheet_name] as $key_check => $value_check) {                
                if($value_check['check'] == 'unique' || $value_check['check'] == 'uniqueCode') {                      
                    self::checkUniquesArray($key_check,$objects,$position);
                }
            }      
        }
        
        
        if($table != '') {                    
            //check unique codes
            $tot_b = count($objects);
            $codes_array_check = array();
            $col = 'fc_imp_code_wares_excel';
            if($table == 't_workorders') {
                $col = 'fc_imp_code_wo_excel';
            }
            elseif($table == 't_selectors') {
                $col = 'fc_imp_code_sel_excel';
            }
            
            for($b = 0;$b < $tot_b;++$b) {                                
                $codes_array_check[$b] = $db->quote($objects[$b]['f_code']);
            }
            
            $unique_check = implode(',',$codes_array_check);             
            $duplicate = self::check($unique_check, 'uniqueCode', $col,$table , $objects,0,$f_type_id); //get code duplicate                                    
            if($duplicate !== false) {                
                $b = array_search($duplicate, $codes_array_check);
                $message = $check_list[$sheet]['f_code']['message'];
                self::reportError("Error occurred at line ".($position+$b).' : '.$message);                
            }            
        }      
        
        if(($table == 't_wares' || $table == 't_workorders' || $table == 't_selectors') ) {
            self::setCreationDateOrder($table,$f_type_id);
            self::setFirstFcProgress($mdl);
        }
        
        if($mdl == '' && !empty($table) && !empty($f_type_id)) {
            self::setCreationDateOrder($table,$f_type_id);
        }       
        //check rules
        if(strpos($sheet_name,"t_cross") === false && ($sheet_name != 't_wares_wizard') ) {            
            $tot_b = count($objects);           
            for($b = 0; $b < $tot_b;++$b) {               
                $line = $objects[$b];
                if(strpos($sheet_name,"t_engagement") === false) {
                    foreach($line as $key_check => $value) {
                        if(isset($check_list[$sheet_name][$key_check]) && $key_check != 'f_code') {                    
                            $col = $key_check;                                                                                 
                            if($table != '' && $f_type_id !=0 ){                                 
                                if(!self::check($value, $check_list[$sheet_name][$key_check]['check'], $col,$table , $objects,$b,$f_type_id,$check_list[$sheet_name][$key_check]['check_value'])) {                
                                    $message = isset($check_list[$sheet_name][$key_check]['message'])?$check_list[$sheet_name][$key_check]['message']:'error raised during the controll of the field '.$key_check;                        
                                    self::reportError("Error occurred at line ".($pos_excel+$b).' : '.$message);
                                }                
                            }
                        }
                        if(isset($validation[$key_check])) {
                            if(!in_array($value,$validation[$key_check]['f_value_allow'])) {                
                                $message = "The value inside the column '$key_check' doesn't respect validation table.";
                                self::reportError("Error occurred at line ".($pos_excel+$b).' : '.$message);                                
                            }
                        }
                    }        
                }
                //insert checked element in specific table
                if($table == "t_wares" ) {                    
                    $wares->insertWares($objects[$b],$custom,$f_type_id);                                        
                }            
                elseif($table == "t_workorders") {            
                    $wo->insertWorkorder($objects[$b],$custom,$f_type_id);
                }    
                elseif($sheet_name == "t_pair_cross") {
                    $wo->insertPairCross($objects[$b]);
                }
                elseif($sheet_name == "t_engagement" ) {                               
                    $t_engagement = $wo->createEngagement($objects[$b]); //FUNZIONA                            
                    if(!empty($t_engagement)) {
                        $mp->periodiGenerator($t_engagement);//FUNZIONA                                                                
                        $wo->pm_frequency($t_engagement['f_code']);                    
                        $wo->addNextDueDate($t_engagement['f_code']);// set due date and subsequent due date
                    }
                }
                elseif($sheet_name == "t_engagement_meter") {                
                    $t_engagement_meter = $wo->createEngagement($objects[$b],11); //FUNZIONA       
                    if(!empty($t_engagement_meter)){
                        $mp->periodiGenerator($t_engagement_meter);//FUNZIONA    
                    }
                }            
            }
        }        
        if($sheet_name == 't_wares_wizard') {
            $wares->createWizard($objects);
        }
        //specific manage for selector        
        if($table == "t_selectors") {                      
            $selector->insertSelectors($objects,$custom,$f_type_id);
        }      
        elseif(strpos($sheet_name,"t_cross") !== false){
            switch($sheet_name) {
                case 't_cross_wares_selectors' : //cross between wares and selectors                                                            
                    $selector->crossSelectors($objects,'t_wares');
                    break;
                case 't_cross_workorders_selectors' : //cross between wares and selectors                                    
                    $selector->crossSelectors($objects,'t_workorders');
                    break;
                case 't_cross_workorders_wares' ://cross between wares and workorders                         
                    $wo->crossWaresWorkorders($objects);
                    break;
                case 't_cross_wares_wares' : //cross between wares and wares
                    $wares->crossAllWares($objects);
                    break;                    
            }
        }
    }
    
    
    public static function createRollBack($sheet,$table,$db) 
    {
        $id = Zend_Registry::get('f_id');        
        $select = new Zend_Db_Select($db);        
        //f_id of start in case of rollback
        $f_id_starts = array();
        //this condition are only to catch the max f_id in case of rollback
        if(strpos($sheet,"t_cross_") !== false){ // simple cross
            switch($sheet) {
                case 't_cross_wares_selectors' : 
                    $res_max_id = $select->from("t_selector_ware",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_selector_ware"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_wares_wares' : 
                    $res_max_id = $select->from("t_wares_relations",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_wares_relations"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_workorders_wares' : 
                    $res_max_id = $select->from("t_ware_wo",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_ware_wo"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_workorders_selectors' : 
                    $res_max_id = $select->from("t_selector_wo",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_selector_wo"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
            }
        }
        //t_pair_cross, t_engagement and t_engagement_meter are saved in t_pair_cross table
        elseif($sheet == 't_pair_cross' || $sheet == 't_engagement' || $sheet == 't_engagement_meter') {
            $res_max_id = $select->from("t_pair_cross",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_pair_cross"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
            //catch max f_id of t_periodics in case of engagement
            if($sheet == 't_engagement' || $sheet == 't_engagement_meter') {
                $select->reset();
                $res_max_id = $select->from("t_periodics",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                $f_id_starts["t_periodics"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
            }
        }

        //if not empty table, compile ids relative table,table_parent,creation_date and custom fields
        if(!empty($table)) {        
            //do the same operation for father and custom fields
            $select->reset();        
            $res_max_id = $select->from("{$table}_parent",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["{$table}_parent"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 

            $select->reset();
            $res_max_id = $select->from($table,array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts[$table] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 


            $select->reset();        
            $res_max_id = $select->from("t_custom_fields",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_custom_fields"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 

            $select->reset();        
            $res_max_id = $select->from("t_creation_date",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_creation_date"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
        }
        //close existing connection (will be re-open when finish system)
        $compress_roolback = serialize($f_id_starts);
        $db->update("t_queue",array("f_rollback"=>$compress_roolback),"f_id = $id");
    }
    
    
    public static function createCustomList($path,$iterator,$sheets)
    {
        $key_sheet = array();
        $pos = array_search("Custom", $sheets);             
        $position = 1;
        $inc = 1000;      
        $custom = array();
        $file = $iterator->IterateExcel($path,$position,$inc);
        $rowIterator = $file->setActiveSheetIndex($pos)->getRowIterator();            
        $iterator->rowIteratorImport($rowIterator,$key_sheet,true);        
        $keys["Custom"] = $key_sheet;
        //extract and parse all custom fields    
        do {    
            $not_end = true;
            $file = $iterator->IterateExcel($path,$position,$inc,"Custom");
            $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
            $custom = $iterator->rowIteratorImport($rowIterator,$keys["Custom"],false); 
            if(count($custom)<$inc-1) $not_end = false;                  
            $position+=$inc;    
            $file->disconnectWorksheets();
            unset($file);
        }while($not_end);

        //change key of array with the f_title inside
        $tot_a = count($custom);
        $custom_copy = array();
        for($a = 0;$a < $tot_a;++$a) {
            if(!isset($custom[$a]['f_title']) || empty($custom[$a]['f_title'])) continue;
            $k = strtolower($custom[$a]['f_title']);
            $custom_copy[$k] = $custom[$a];
            $custom_copy[$k]['f_title'] = strtolower($custom[$a]['f_title']);            
        }        
        return $custom_copy;
    }
    
    public static function createValidationList($path,$iterator,$sheets)
    {
        $key_sheet = array();
        $position = 1;
        $inc = 1000;   
        $pos = array_search("Validation Tables", $sheets);        
        $validation = array();
        $file = $iterator->IterateExcel($path,1,1);
        $rowIterator = $file->setActiveSheetIndex($pos)->getRowIterator();    
        $iterator->rowIteratorImport($rowIterator,$key_sheet,true);
        $keys["Validation Tables"] = $key_sheet;        
        do {    
            $not_end = true;
            $file = $iterator->IterateExcel($path,$position,$inc,"Validation Tables");
            $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
            $validation = $iterator->rowIteratorImport($rowIterator,$keys["Validation Tables"],false); 
            if(count($validation)<$inc-1) $not_end = false;                  
            $position+=$inc;    
            $file->disconnectWorksheets();
            unset($file);
        }while($not_end);

        //change key of array with the f_title inside
        $tot_a = count($validation);
        $validation_copy = array();
        for($a = 0;$a < $tot_a;++$a) {        
            if(!isset($validation[$a]['f_category']) || empty($validation[$a]['f_category'])) continue;        
            $validation_copy[$validation[$a]['f_category']]['f_value_allow'][] = $validation[$a]['f_value'];
            $validation_copy[$validation[$a]['f_category']]['f_ui_category'][] = array(
                'label'=>$validation[$a]['f_label'],
                'value'=>$validation[$a]['f_value'],
                'selected'=>substr($validation[$a]['f_selected'],0,1)
            );
        }    
        return $validation_copy;
    }
}