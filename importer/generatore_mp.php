<?php
ini_set("display_errors","On");

require_once 'constants.php';


new Zend_Application_Module_Autoloader(array(
    'namespace' => 'Mainsim_',
    'basePath'  => APPLICATION_PATH .'/modules/default',
    'resourceTypes' => array ()));

$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
$db_config_array = $config->toArray();
$db = $db_config_array['database']['params']['dbname'];      


date_default_timezone_set("Europe/Paris");
$imported = false;
if(isset($_POST['db'])) {
    $start_date = 0;
    $end_date = time();    
    $number = 0;
    if(!empty($_POST['start_date'])) {
        $start_date = date('Y',strtotime($_POST['start_date'])) != '1970' ?strtotime($_POST['start_date']):0;
    }    
    if(!empty($_POST['end_date'])) {
        $end_date = date('Y',strtotime($_POST['end_date'])) != '1970'?strtotime($_POST['end_date']):time();
    }    
    if($_POST['number_of_creation']) {
        $number = (int)$_POST['number_of_creation'];        
    }
    $reader = new Mainsim_Model_PMReader($db);
    $number_created = $reader->readPeriodics($start_date,$end_date,$number,true);
    $imported = true;
}

?>
<html>
    <head>
        <title>Importer</title>        
        <script type="text/javascript" src="jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="default.css" />
    </head>
    <body>     
        <div><a href="index.php"><< Back to main menu</a></div>
        <?php if(!$imported) :?>
            <form action="generatore_mp.php" method="POST">
                <div style="text-align:center">Here you can generate past workorders from periodic generators.</div>
                <div style="text-align:center; margin-bottom : 35px;">
                    Insert a starting and an ending date. Empty fields means all periodics generation.
                </div> 
                <div style="text-align:center">
                    <p>
                        <label for="start_date">Start Date</label>                    
                        <input type="text" name="start_date" /><label for="start_date"> (d-m-Y ex : 01-02-2012)</label><br /><br />
                    </p>
                    <p>
                        <label for="end_date">End Date</label>
                        <input type="text" name="end_date" /><label for="end_date"> (d-m-Y ex : 01-02-2012)</label><br /><br />
                    </p>
                    <p>
                        <label for="limit">Number of periodic generated</label>
                        <input type="text" name="number_of_creation" /><label for="limit"> (if empty, system will generate every periodic finded)</label><br />
                    </p>
                </div>
                <div id="choose_action" style="margin-top : 20px;" >
                    <div style="text-align:center">
                        <input type ="submit" value="Generate" /> <br />
                        <input type ="hidden" value="<?php echo $db;?>" name="db"/>
                    </div>
                    <br clear ="all" />
                </div>
            </form>
        <?php else : ?>
            <div style="margin-top : 50px;text-align : center"><?php echo $number_created; ?> periodic workorders generated! <a href="index.php">Click here</a> to come back home</div>                
        <?php endif; ?>
        <div id="logo">&nbsp;</div>
        <div id="credits">
            for any problem e-mail to <a href="mailto:info@unp.it">UNPlugged</a>
        </div>
    </body>
</html>
    