<?php

class Wares
{
    private $db;
        
    public function __construct() {}
    
    
    public function insertWares($ware,$custom_fields,$f_type_id = 0) 
    {           
        $this->db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($this->db);     
        $f_order = Zend_Registry::isRegistered('f_order')?Zend_Registry::get('f_order'):1;
        $time = Zend_Registry::get('f_timestamp');
        
        try {            
            $i = 0;
            $wares_cols = utilities::$default_table_cols;   
            $creation_cols = utilities::$creation_table_cols;
            $custom_cols = utilities::$custom_table_cols;            
            $special_cols = array();
            if(empty($ware['f_type_id']) && !$f_type_id) return;       
            if(!empty($ware['f_type_id'])) {
                $exp_type = explode(' : ',$ware['f_type_id']);
                $f_type_id = $exp_type[0];
            }
            
            if($f_type_id == 9) {
                $special_cols = utilities::$wizard_table_cols;
            }
            elseif($f_type_id == 16) {
                $special_cols = utilities::$user_table_cols;
            }
            $type_id_info = array();            
            $time_list = array(
                "f_start_date",
                "fc_wo_starting_date",
                "fc_wo_ending_date",
                "fc_offer_date",
                "fc_sch_end_date",
                "fc_contract_starting_date",
                "fc_contract_ending_date"
            );

            if(empty($ware)) return;

            try {
                $res_type = $select->from("t_wares_types")
                        ->where("f_id = ?",$f_type_id)->query()->fetchAll();                            
            }catch(Exception $e) {                
                utilities::reporQuerytError("t_wares_types",$e->getMessage(),$select->__toString());die;
            };  
            $type_id_info = $res_type[0];
            $select->reset();

            $select->reset();                        
            $ware_val = $ware;
            //elimino le colonne che non c'entrano con gli asset
            $keys = array_keys($ware_val);
            foreach($keys as $line) {
                if(is_null($ware_val[$line])) {
                    $ware_val[$line] = '';                    
                }
                if(!in_array($line, $wares_cols)){                    
                    unset($ware_val[$line]);
                    if(!in_array($line,$custom_cols) && !in_array($line, $creation_cols) && !in_array($line,$special_cols) && $line != 'f_parent_code') {
                        unset($ware[$line]);
                    }
                }                        
            }           
            
            //create f_code
            $creation = array(
                'f_creation_date'=>!isset($ware['f_creation_date']) || empty($ware['f_creation_date'])?$time:strtotime($ware['f_creation_date']),
                'f_order'=>$f_order,
                'f_type'=>"WARES",
                'f_category'=>  strtoupper($type_id_info['f_type']),
                'f_creation_user'=>1,
                'f_title'=>isset($ware['f_title'])?utf8_decode($ware['f_title']):'',
                'f_description'=>isset($ware['f_description'])?utf8_decode($ware['f_description']):'',
                'f_visibility'=>isset($ware['f_visibility'])  && $ware['f_visibility']!=''?$ware['f_visibility']:-1,
                'f_editability'=>isset($ware['f_editability']) && $ware['f_editability']!=''?$ware['f_editability']:-1,
                'f_wf_id'=>isset($ware['f_wf_id']) && $ware['f_wf_id']!=''?$ware['f_wf_id']:$type_id_info['f_wf_id'],
                'f_phase_id'=>isset($ware['f_phase_id']) && $ware['f_phase_id']!=''?$ware['f_phase_id']:$type_id_info['f_wf_phase'],
                'f_timestamp'=>!isset($ware['f_timestamp']) || empty($ware['f_timestamp'])?$time:strtotime($ware['f_timestamp']),
                'fc_progress'=>  utilities::$fc_progress
            );
            utilities::$fc_progress++;
            if($creation['f_creation_date'] == 0) {
                $creation['f_creation_date'] = $time;
            }
            if($creation['f_timestamp'] == 0) {
               $creation['f_timestamp'] =  $time;
            }
            
            $f_order++;
            
            try {
                $this->db->insert("t_creation_date",$creation);
                $f_code = $this->db->lastInsertId();
            }catch(Exception $e) {   
                $message_dump  = Zend_Debug::dump($creation,null,false);
                utilities::reportInsertError("t_creation_date",$e->getMessage(),$message_dump);die;                 
            }
            
            $ware_val['f_code'] = $f_code;            
            $ware_val['f_order'] = isset($ware['f_order'])  && $ware['f_order']!=''?$ware['f_order']:$f_order;
            $ware_val['f_type_id'] = $f_type_id;            
            $ware_val['f_timestamp'] = $time;            
            $ware_val['f_user_id'] = 1;            
            $ware_val['f_priority'] = isset($ware['f_priority'])  && $ware['f_priority']!=''?$ware['f_priority']:0;                                                                
            $ware_val['f_start_date'] = isset($ware['f_start_date']) && !empty($ware['f_start_date'])?strtotime($ware['f_start_date']):$time;                                                
            $ware_val['fc_imp_code_wares_excel'] = $ware['f_code'];

            foreach($ware_val as &$line) {
                $line = is_null($line)?'':$line;
                $line = utf8_decode($line);
            }
            
            try {                
                $this->db->insert("t_wares",$ware_val);                 
            }catch(Exception $e) {                
                utilities::reportInsertError("t_wares",$e->getMessage(),$message_dump);die;                
            }
            
            //carico il custom field
            $custom = array(                
                'f_timestamp'=>$time,
                'f_code'=>$f_code                
            );            
            foreach($ware as $key => $line) {
                if(!array_key_exists($key, $ware_val) && !in_array($key,$creation_cols)) {                    
                    $field = utilities::clean_col_name($key == 'f_parent_code' ? 'fc_imp_parent_code' : $key);
                    //verifico se trovo il custom field orginale fra quelli passati dall'excel.
                    if(array_key_exists($field, $custom_fields)) {
                        $type_custom = substr(str_replace(" : ","",$custom_fields[$field]['f_type']),1);
                        switch($type_custom) {
                            case 'BOOLEAN' :$custom[$field] = (int)(bool)$line;                                
                                break;
                            case 'INT' : $custom[$field] = (int)$line;                                
                                break;
                            case 'DATE' : 
                                if(is_int($type_custom)){
                                    $custom[$field] = (int)$line;
                                }
                                else {
                                    $custom[$field] = (int)strtotime($line);
                                }
                                break;  
                            case 'VARCHAR' :
                                $max_lenght = ( (int)$custom_fields[$field]['f_column_length']);
                                if(strlen($line) > $max_lenght) {
                                    utilities::reportError("Error : string $line for field $field is too long. Maximum characters allow: $max_lenght");
                                }
                                $custom[$field] = $line;
                                break;
                            default : $custom[$field] = $line;
                                break;
                        }
                    }
                    else {
                        $custom[$field] = $line;
                    }
                    if(in_array($field,$time_list)) $custom[$field] = strtotime($custom[$field]);                                        
                }
            }       
            try {
                $users_custom = array();
                $wiz_custom = array();
                foreach($custom as $key => &$line) {
                    $line = is_null($line)?'':$line;
                    $line = utf8_decode($line);
                    if($key != 'f_code' && $f_type_id == 9 && in_array($key,$special_cols)) {
                        $wiz_custom[$key] = $line;
                        unset($custom[$key]);
                    }
                    elseif($key != 'f_code' && $f_type_id == 16 && in_array($key,$special_cols)) {
                        $users_custom[$key] = $line;
                        unset($custom[$key]);
                    }
                }
                
                $custom['f_main_table'] = 't_wares';
                $custom['f_main_table_type'] = $f_type_id;
                $this->db->insert("t_custom_fields",$custom);
                //for special table                
                if($f_type_id == 16) {
                    $users_custom['f_code'] = $f_code;
                    $this->db->insert("t_users",$users_custom);
                }
            }catch(Exception $e) {           
                $message_dump = Zend_Debug::dump($custom,null,false);
                utilities::reportInsertError("t_custom_fields",$e->getMessage(),$message_dump);die;
            }                             
        }catch(Exception $e) {            
            utilities::reportError("unknown error : ".$e->getMessage());die;
        }
        $i++;                                              
        Zend_Registry::set('f_order',$f_order);
    }
    
    /**
     * Insert wizard
     * @param type $rows 
     */
    public function createWizard($rows) 
    {
        $this->db = Zend_Registry::get('db');                
        $tot = count($rows);
        for($a = 0;$a < $tot;++$a) {
            $row = $rows[$a];
            $row['f_order'] = 0;            
            $this->db->insert("t_wizard",$row);
        }        
    }
    
    /**
     * @deprecated 
     */
    public function __setWizardCustom()
    {        
        $this->db = Zend_Registry::get('db');        
        $select = new Zend_Db_Select($this->db);
        try {
            $res = $select->from(array("t1"=>"t_wizard"),array('f_code','fc_wizard_valfield'))
                ->join(array("t2"=>"t_wares"),"t1.f_code = t2.f_code",array())->where("t2.f_type_id = 9")->query()->fetchAll();
        }catch(Exception $e) {
            utilities::reportQueryError("t_wizard", $e->getMessage(), $select->__toString());
            die;
        }
        foreach($res as $line){
            if(strpos($line['fc_wizard_valfield'],"t_wares")!==false) {
                $f_code_asset = str_replace("'","",$line['fc_wizard_valfield']);
                $f_code_asset = str_replace("t_wares:","",$f_code_asset);
                //cerco se esiste l'asset
                $select->reset();   
                try {
                    $res_asset = $select->from("t_wares",array("f_code"))->where("fc_imp_code_wares_excel = '$f_code_asset'")->where("f_type_id = 1")->query()->fetchAll();                
                }catch(Exception $e) {
                    utilities::reportQueryError("t_wares", $e->getMessage(), $select->__toString());
                    die;
                }
                if(!empty($res_asset)) {                    
                    $f_code = $res_asset[0];         
                    try {
                        $this->db->update("t_wizard",array("fc_wizard_valfield"=>"'t_wares:\'{$f_code['f_code']}\''"),"f_code = {$line['f_code']}");
                    }catch(Exception $e) {
                        utilities::reportError("<div>Something went wrong during the update in t_wizard: ".$e->getMessage());die;
                    }
                }
            }
        }                
    }
    
    /**
     *  Cerco tutti i padri di ogni wares e lo inserisco nel campo fc_asset_hierarchy 
     */
    public function createParentField($f_type_id = 0)
    {           
        if(!$f_type_id) return;        
        $this->db = Zend_Registry::get('db');
        $select = new Zend_Db_Select($this->db);
        
        //cerco tutti i padri dell'elemento
        try {
            $res_f_codes = $select->from(array('t1'=>"t_wares"),array('f_code'))                    
                ->where("f_type_id = $f_type_id")->query()->fetchAll();
        }catch(Exception $e) {
            utilities::reportQueryError("t_wares", $e->getMessage(), $select->__toString());die;
        }        
        foreach($res_f_codes as $f_code) {
            $parents_title = array();
            $parents = array();
            $new_code = array($f_code['f_code']);                        
            do {
                $not_end = false;
                $select->reset();                   
                try {
                    $res_parent = $select->from(array("t1"=>"t_wares_parent"),array('f_parent_code'))                            
                            ->join(array("t2"=>"t_wares"),"t2.f_code = t1.f_parent_code",array())
                            ->join(array("t_creation_date"),"t2.f_code = t_creation_date.f_id",array("f_title"))
                        ->where("t1.f_active = 1")->where("t1.f_code IN (".implode(',',$new_code).')')
                        ->query()->fetchAll();
                }catch(Exception $e) {
                    utilities::reportQueryError("t_wares_parent", $e->getMessage(), $select->__toString());
                    die;
                }
                if(!empty($res_parent)) {                    
                    $new_code = array();
                    foreach($res_parent as $line_par) {
                        $new_code[] = $line_par['f_parent_code'];
                        if(!in_array($line_par, $parents) && $line_par['f_parent_code'] != 0){
                            $parents[] = $line_par['f_parent_code'];
                            $parents_title[] = $line_par['f_title'];
                        }
                    }
                    $not_end = true;
                }
            }while($not_end);                                
            
            $select->reset();
            
            if(!empty($parents)) {                                
                try {
                    $parents_title = implode(', ',$parents_title);    
                    $this->db->update("t_creation_date",array('fc_hierarchy'=>$parents_title),"f_id = {$f_code['f_code']}");                                                    
                }catch(Exception $e) {
                    utilities::reportQueryError("t_creation_date", $e->getMessage(), $select->__toString());
                    die;
                }
            }
        }                
    }    
    
    /*
     * Cross all wares with other wares
     * in t_wares_relations table
     */    			        
    public function crossAllWares($crosses = array())
    {
        $this->db = Zend_Registry::get('db'); 
        $time = Zend_Registry::get('f_timestamp');
        
        $select = new Zend_Db_Select($this->db);        
        $wa_master = array();
        $wa_slave = array();
        $tot_a = count($crosses);
        for($a = 0;$a < $tot_a;++$a) {
            if($crosses[$a]['f_code_wares_master'] == '' || $crosses[$a]['f_type_id_wares_master'] == '' || 
                    $crosses[$a]['f_code_wares_slave'] == '' || $crosses[$a]['f_type_id_wares_slave'] == '') continue;            
            $type_wa_master = substr($crosses[$a]['f_type_id_wares_master'], 0,  strpos($crosses[$a]['f_type_id_wares_master'], " :"));
            $type_wa_slave = substr($crosses[$a]['f_type_id_wares_slave'], 0,  strpos($crosses[$a]['f_type_id_wares_slave'], " :"));
            $wa_master[$type_wa_master]["'{$crosses[$a]['f_code_wares_master']}'"] = "'{$crosses[$a]['f_code_wares_master']}'";
            $wa_slave[$type_wa_slave]["'{$crosses[$a]['f_code_wares_slave']}'"] = "'{$crosses[$a]['f_code_wares_slave']}'";                        
        }
        
        //get true codes of wares master
        $type_wa_master_keys = array_keys($wa_master);
        $tot_b = count($type_wa_master_keys);
        for($b = 0;$b < $tot_b;++$b) {
            $key = $type_wa_master_keys[$b];            
            $wa_master_list = implode(',',$wa_master[$key]);
            if(empty($wa_master_list)) continue;                        
            $wa_master[$key] = array();
            $select->reset();
            $res_true_wa_master = $select->from("t_wares",array("f_code","fc_imp_code_wares_excel"))
                    ->where("f_type_id = ?",$key)->where("fc_imp_code_wares_excel IN ($wa_master_list)")
                    ->query()->fetchAll();            
            $tot_c = count($res_true_wa_master);
            for($c = 0;$c < $tot_c;++$c) {
                $wa_master[$key][$res_true_wa_master[$c]['fc_imp_code_wares_excel']] = (int)$res_true_wa_master[$c]['f_code'];
            }
        }
        
        //get true codes of wos
        $type_wa_slave_keys = array_keys($wa_slave);        
        $tot_b = count($type_wa_slave_keys);
        for($b = 0;$b < $tot_b;++$b) {
            $key = $type_wa_slave_keys[$b];
            $wa_slave_list = implode(',',$wa_slave[$key]);
            if(empty($wa_slave_list)) continue;            
            $wa_slave[$key] = array();
            $select->reset();
            $res_true_wa_slave = $select->from("t_wares",array("f_code","fc_imp_code_wares_excel"))
                    ->where("f_type_id = ?",$key)->where("fc_imp_code_wares_excel IN ($wa_slave_list)")
                    ->query()->fetchAll();            
            $tot_c = count($res_true_wa_slave);
            for($c = 0;$c < $tot_c;++$c) {
                $wa_slave[$key][$res_true_wa_slave[$c]['fc_imp_code_wares_excel']] = (int)$res_true_wa_slave[$c]['f_code'];
            }            
        }
        
        //insert cross (master and slave)
        $tot_b = count($crosses);
        for($b = 0;$b < $tot_b;++$b) {
            $line = $crosses[$b];
            if($line['f_code_wares_master'] == '' || $line['f_type_id_wares_master'] == '' || $line['f_code_wares_slave'] == '' || $line['f_type_id_wares_slave'] == '') continue;
            $type_wa_master = substr($line['f_type_id_wares_master'], 0,  strpos($line['f_type_id_wares_master'], " :"));
            $type_wa_slave = substr($line['f_type_id_wares_slave'], 0,  strpos($line['f_type_id_wares_slave'], " :"));
            if(!isset($wa_master[$type_wa_master][$line['f_code_wares_master']]) || !isset($wa_slave[$type_wa_slave][$line['f_code_wares_slave']])) continue;
            $fcodewam = $wa_master[$type_wa_master][$line['f_code_wares_master']];
            $fcodewas = $wa_slave[$type_wa_slave][$line['f_code_wares_slave']];
            $select->reset();
            $check = $select->from("t_wares_relations",array("num"=>"count(*)"))->where("f_code_ware_master = ?",$fcodewam)
                    ->where("f_code_ware_slave = ?",$fcodewas)->query()->fetch();
            if($check['num'] == 0) {
                //insert cross
                $this->db->insert("t_wares_relations",array(
                    'f_code_ware_master'=>$fcodewam,
                    'f_code_ware_slave'=>$fcodewas,
                    'f_type_id_master'=>$type_wa_master,
                    'f_type_id_slave'=>$type_wa_slave,                    
                    'f_timestamp'=>$time,                    
                ));     
            }
            //insert revert cross
            $select->reset();
            $check = $select->from("t_wares_relations",array("num"=>"count(*)"))->where("f_code_ware_master = ?",$fcodewas)
                    ->where("f_code_ware_slave = ?",$fcodewam)->query()->fetch();
            if($check['num'] == 0) {
                //insert cross
                $this->db->insert("t_wares_relations",array(
                    'f_code_ware_master'=>$fcodewas,
                    'f_code_ware_slave'=>$fcodewam,
                    'f_type_id_master'=>$type_wa_slave,
                    'f_type_id_slave'=>$type_wa_master,                    
                    'f_timestamp'=>$time,                    
                ));     
            }
        }        
    }
        
    
    public function cryptPwd() 
    {        
        $this->db = Zend_Registry::get('db');
        
        $select = new Zend_Db_Select($this->db);
        $res = $select->from("t_users",array("f_code","fc_usr_pwd"))
                ->join("t_wares","t_users.f_code = t_wares.f_code",array())                
                ->where("t_wares.f_type_id = 16")
                ->where("t_wares.f_code > 4")
                ->query()->fetchAll();        
        $tot = count($res);
        for($a = 0;$a < $tot;++$a) {
            $line = $res[$a];
            $pwd = $this->crypt(md5($line['fc_usr_pwd']));try {
                $this->db->update("t_users",array("fc_usr_pwd"=>$pwd),"f_code = {$line['f_code']}");
            }catch (Exception $e) {
                utilities::reportError("Something went wrong: ".$e->getMessage());die;
            }
        }
    }
    
    private function crypt($pv)
    {
        $xc0=substr($pv,0,2);
        $xc1=substr($pv,2,6);
        $xc2=substr($pv,8,16);
        $xc3=substr($pv,24,8);
        $pv = $xc1.$xc3.$xc0.$xc2;
    
        $xc0=substr($pv,0,5);
        $xc1=ord(substr($pv,5,1));
        $xc1++; if($xc1>57 && $xc1<90) $xc1=52;
        if($xc1>95) $xc1-=49;
        $xc2=substr($pv,6,26);          
        return $xc2.$xc1.$xc0;
    }        
}