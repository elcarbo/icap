<?php

//initialize constants
ini_set("display_errors","On");
defined('BASE_PATH') || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

require_once BASE_PATH.'/constants.php';
require_once BASE_PATH.'/utilities.php';
    
/*
 * File che richiamato da crontab, 
 * avvia i file previsti
 */

function get_tab_cols($tab,$db) 
{       
    Zend_Db_Table::setDefaultAdapter($db);
    $table = new Zend_Db_Table($tab);
    $columns = $table->info('cols');        
    return $columns;
}

//old db connection
$db_old = Zend_Db::factory("PDO_MYSQL",array(
    'dbname'=>'mainsim3_fiorani_old',
    'host'=>'192.168.0.12',
    'username'=>'root',
    'password'=>'1881'
));

//new db connection
$db_new = Zend_Db::factory("PDO_MYSQL",array(
    'dbname'=>'mainsim3_fiorani',
    'host'=>'192.168.0.12',
    'username'=>'root',
    'password'=>'1881'
));

//dichiarazione nuove colonne
$creation_new = get_tab_cols("t_creation_date", $db_new);
$wares_new = get_tab_cols("t_wares", $db_new);
$wo_new = get_tab_cols("t_workorders", $db_new);
$selector_new = get_tab_cols("t_selectors", $db_new);
$custom_new = get_tab_cols("t_custom_fields", $db_new);
$user_new = get_tab_cols("t_users", $db_new);
$wizard_new = get_tab_cols("t_wizard", $db_new);

//obj select vecchio/nuovo db
$sel_old = new Zend_Db_Select($db_old);
$sel_new = new Zend_Db_Select($db_new);

//transfert wares
$i = 0;
$inc = 2000;
if(false) {
do {
    $not_end = true;
    $sel_old->reset();
    $res_old = $sel_old->from(array("t1"=>"t_creation_date"))
        ->join(array("t2"=>"t_wares"),"t1.f_id = t2.f_code")
        ->join(array("t3"=>"t_custom_fields"),"t1.f_id = t3.f_code")
        //->where("t2.f_active = 1")->where("t3.f_active = 1")
        ->where("t1.f_id > 8")->limit($inc,$i)->query()->fetchAll();
    // numero elementi estratti
    $tot_a = count($res_old);
    if(isset($res_old[0])) {
        //estrazione intestazione righe
        $key_row = array_keys($res_old[0]);
        $tot_b = count($key_row);
    }
    for($a = 0;$a < $tot_a;++$a) {
        //array con info totali
        $new_row = array(
            't_creation_date'=>array(),
            't_wares'=>array(),
            't_custom_fields'=>array(),
            't_users'=>array(),
            't_wizard'=>array()
        );  
        //ciclo la riga e divido le colonne nelle nuove tabelle
        for($b = 0;$b < $tot_b;++$b) {
            if($key_row[$b] == 'f_id') continue;
            $key = $key_row[$b];
            if($key == "fc_asset_hierarchy") {
                $key = "fc_hierarchy";
            }
            //metto il vecchio campo nella t_creation_date
            if(in_array($key,$creation_new)) {
                $new_row['t_creation_date'][$key] = $res_old[$a][$key_row[$b]];
            }
            //metto il vecchio campo nella t_wares
            elseif(in_array($key_row[$b],$wares_new)) {
                $new_row['t_wares'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }
            //metto il vecchio campo nella t_custom_fields
            elseif(in_array($key_row[$b],$custom_new)) {
                $new_row['t_custom_fields'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }          
            //metto il vecchio campo nella t_users
            elseif(in_array($key_row[$b],$user_new)) {
                $new_row['t_users'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }            
            //metto il vecchio campo nella t_wizard
            elseif(in_array($key_row[$b],$wizard_new)) {
                $new_row['t_wizard'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }            
        }
        //insert creation_date per ottenere il nuovo f_code
        //estraggo f_timestamp per associarlo alle altre tabelle
        $timestamp = $new_row['t_creation_date']['f_timestamp'];
        $db_new->insert("t_creation_date", $new_row['t_creation_date']);
        $f_code = $db_new->lastInsertId();
        $new_row['t_wares']['f_code'] = $f_code;
        $new_row['t_wares']['f_timestamp'] = $timestamp;
        $new_row['t_custom_fields']['f_code'] = $f_code;
        $new_row['t_custom_fields']['f_timestamp'] = $timestamp;
        $db_new->insert("t_wares", $new_row['t_wares']);
        $db_new->insert("t_custom_fields", $new_row['t_custom_fields']);  
        
        $sel_old->reset();
        $res_doc_att = $sel_old->from('t_attachments')->where('f_code = ?', $res_old[$a]['f_code'])
                ->query()->fetchAll();
        if(!empty($res_doc_att)){            
            foreach ($res_doc_att as $at_value) {
                
                $av = $at_value;
                $f_path = realpath(APPLICATION_PATH.'/../attachments/'.$av['f_type'].'/');
                unset($av['f_id']);
                $av['f_code'] = $f_code;
                $path_code = realpath($f_path.'/'.$f_code);
                if(!is_dir($path_code)){
                    //creo la cartella col nome 'f_code'
                    $folder = mkdir($path_code, 0777);
                }
                //inserisco il file nella cartella
                $file_transf = $av['f_data'];
                file_put_contents($path_code."/".$av['f_session_id'].'.'.$av['f_file_ext'], $file_transf);
                if(!file_exists($path_code."/".$av['f_session_id'].'.'.$av['f_file_ext']))  { //controllo che il file sia stato effettivamente creato                 
                    echo 'Error file transfer';
                }
            }
        }
        
        /*if($new_row['t_wares']['f_type_id'] == 9) {
            if(empty($new_row['t_wizard'])) { //exist user table
                $sel_old->reset();
                $res_wiz_old = $sel_old->from("t_wizard")->where("f_code = ?",$res_old[$a]['f_code'])->query()->fetch();
                foreach($res_wiz_old as $key_wiz => $value_wiz_old) {
                    if($key_wiz == 'f_id' || $key_wiz == 'f_code') continue;
                    $new_row['t_wizard'][$key_wiz] = $value_wiz_old;
                }
            }
            $new_row['t_wizard']['f_code'] = $f_code;
            $db_new->insert("t_wizard",$new_row['t_wizard']);
        }*/
        if($new_row['t_wares']['f_type_id'] == 16) {
            if(empty($new_row['t_users'])) { //exist user table
                $sel_old->reset();
                $res_user_old = $sel_old->from("t_users")->where("f_code = ?",$res_old[$a]['f_code'])->query()->fetch();
                foreach($res_user_old as $key_user => $value_user_old) {
                    if($key_user == 'f_id' || $key_user == 'f_code') continue;
                    $new_row['t_users'][$key_user] = $value_user_old;
                }
            }
            $new_row['t_users']['f_code'] = $f_code;
            $db_new->insert("t_users",$new_row['t_users']);
        }        
        file_put_contents("migration.txt", $res_old[$a]['f_code'].":".$f_code.PHP_EOL,FILE_APPEND);        
    }
    
    if(count($res_old) < $inc) $not_end = false;
    echo "Wares : $i".PHP_EOL;
    $i+=$inc;
}while($not_end);

echo 'Wares Completati '.PHP_EOL;


//transfert workorders
$i = 0;
$inc = 2000;
do {
    $not_end = true;
    $sel_old->reset();
    $res_old = $sel_old->from(array("t1"=>"t_creation_date"))
        ->join(array("t2"=>"t_workorders"),"t1.f_id = t2.f_code")
        ->join(array("t3"=>"t_custom_fields"),"t1.f_id = t3.f_code")
        //->where("t2.f_active = 1")->where("t3.f_active = 1")
        ->where("t1.f_id > 8")->limit($inc,$i)->query()->fetchAll();
    
    $tot_a = count($res_old);
    if(isset($res_old[0])) {
        $key_row = array_keys($res_old[0]);
        $tot_b = count($key_row);
    }
    for($a = 0;$a < $tot_a;++$a) {
        $new_row = array(
            't_creation_date'=>array(),
            't_workorders'=>array(),
            't_custom_fields'=>array()
        );                
        for($b = 0;$b < $tot_b;++$b) {
            if($key_row[$b] == 'f_id') continue;
            $key = $key_row[$b];
            if($key == "fc_asset_hierarchy") {
                $key = "fc_wo_asset";
            }
            if(in_array($key,$creation_new)) {
                $new_row['t_creation_date'][$key] = $res_old[$a][$key_row[$b]];
            }
            elseif(in_array($key_row[$b],$wo_new)) {
                $new_row['t_workorders'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }
            elseif(in_array($key_row[$b],$custom_new)) {
                $new_row['t_custom_fields'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }            
        }
        //insert creation_date
        $timestamp = $new_row['t_creation_date']['f_timestamp'];
        $db_new->insert("t_creation_date", $new_row['t_creation_date']);
        $f_code = $db_new->lastInsertId();
        $new_row['t_workorders']['f_code'] = $f_code;
        $new_row['t_workorders']['f_timestamp'] = $timestamp;
        $new_row['t_custom_fields']['f_code'] = $f_code;
        $new_row['t_custom_fields']['f_timestamp'] = $timestamp;
        $db_new->insert("t_workorders", $new_row['t_workorders']);
        $db_new->insert("t_custom_fields", $new_row['t_custom_fields']);        
        file_put_contents("migration.txt", $res_old[$a]['f_code'].":".$f_code.PHP_EOL,FILE_APPEND); 
        
        $sel_old->reset();
        $res_doc_att = $sel_old->from('t_attachments')->where('f_code = ?', $res_old[$a]['f_code'])
                ->query()->fetchAll();
        if(!empty($res_doc_att)){
            foreach ($res_doc_att as $at_value) {
                $av = $at_value;
                $f_path = realpath(APPLICATION_PATH.'/../attachments/'.$av['f_type'].'/');

                unset($av['f_id']);
                $av['f_code'] = $f_code;
                $path_code = realpath($f_path.'/'.$f_code);
                if(!is_dir($path_code)){
                    //creo la cartella col nome 'f_code'
                    $folder = mkdir($path_code, 0777);
                }
                //inserisco il file nella cartella
                $file_transf = $av['f_data'];
                file_put_contents($path_code."/".$av['f_session_id'].'.'.$av['f_file_ext'], $file_transf);
                if(!file_exists($path_code."/".$av['f_session_id'].'.'.$av['f_file_ext']))  { //controllo che il file sia stato effettivamente creato                 
                    echo 'Error file transfer';
                }
            }
        }
    }
    
    if(count($res_old) < $inc) $not_end = false;
    echo "Wo : $i".PHP_EOL;
    $i+=$inc;
}while($not_end);

echo 'Workorders Completati '.PHP_EOL;


//transfert selectors
$i = 0;
$inc = 2000;
do {
    $not_end = true;
    $sel_old->reset();
    $res_old = $sel_old->from(array("t1"=>"t_creation_date"))
        ->join(array("t2"=>"t_selectors"),"t1.f_id = t2.f_code")
        ->join(array("t3"=>"t_custom_fields"),"t1.f_id = t3.f_code")
        //->where("t2.f_active = 1")->where("t3.f_active = 1")
        ->where("t1.f_id > 8")->limit($inc,$i)->query()->fetchAll();
    
    $tot_a = count($res_old);
    if(isset($res_old[0])) {
        $key_row = array_keys($res_old[0]);
        $tot_b = count($key_row);
    }
    for($a = 0;$a < $tot_a;++$a) {
        $new_row = array(
            't_creation_date'=>array(),
            't_selectors'=>array(),
            't_custom_fields'=>array()
        );                
        for($b = 0;$b < $tot_b;++$b) {
            if($key_row[$b] == 'f_id') continue;
            if(in_array($key_row[$b],$creation_new)) {
                $new_row['t_creation_date'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }
            elseif(in_array($key_row[$b],$selector_new)) {
                $new_row['t_selectors'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }
            elseif(in_array($key_row[$b],$custom_new)) {
                $new_row['t_custom_fields'][$key_row[$b]] = $res_old[$a][$key_row[$b]];
            }            
        }
        //insert creation_date
        $timestamp = $new_row['t_creation_date']['f_timestamp'];
        $db_new->insert("t_creation_date", $new_row['t_creation_date']);
        $f_code = $db_new->lastInsertId();
        $new_row['t_selectors']['f_code'] = $f_code;
        $new_row['t_selectors']['f_timestamp'] = $timestamp;
        $new_row['t_custom_fields']['f_code'] = $f_code;
        $new_row['t_custom_fields']['f_timestamp'] = $timestamp;
        $db_new->insert("t_selectors", $new_row['t_selectors']);
        $db_new->insert("t_custom_fields", $new_row['t_custom_fields']);        
        file_put_contents("migration.txt", $res_old[$a]['f_code'].":".$f_code.PHP_EOL,FILE_APPEND);        
    }
    
    if(count($res_old) < $inc) $not_end = false;
    echo "Selectors : $i".PHP_EOL;
    $i+=$inc;
}while($not_end);

echo 'Missione compiuta'.PHP_EOL;

$file = file_get_contents("migration.txt");
$exp_file = explode(PHP_EOL,$file);
$list = array();
$tot_a = count($exp_file);
for($a = 0;$a < $tot_a;++$a) {
    $line = explode(':',$exp_file[$a]);
    $list[$line[0]] = $line[1];
}

//creo i parent
$old_codes = array_keys($list);
$tot_a = count($old_codes);
$time = time();
for($a = 0;$a < $tot_a;++$a) {
    $old_code = $old_codes[$a];
    $new_code = $list[$old_code];
    $sel_old->reset();
    $res_type = $sel_old->from("t_creation_date","f_type")->where("f_id = ?",$old_code)->query()->fetch();
    if(empty($res_type['f_type'])) continue;
    $table = "t_".strtolower($res_type['f_type'])."_parent";
    $sel_old->reset();
    $res_parents = $sel_old->from($table,"f_parent_code")->where("f_code = ?",$old_code)->where("f_active = 1")
            ->query()->fetchAll();
    $tot_b = count($res_parents);
    for($b = 0;$b < $tot_b;++$b) {
        $parent_code = $res_parents[$b]['f_parent_code'];
        if($parent_code != 0) {
            $parent_code = $list[$parent_code];
        }
        $db_new->insert($table,array(
            "f_active"=>1,
            "f_parent_code"=>$parent_code,
            "f_code"=>$new_code,
            "f_timestamp"=>$time
        ));
    }    
}
echo "Parent creati";
}


$file = file_get_contents("migration.txt");
$exp_file = explode(PHP_EOL,$file);
$list = array();
$tot_a = count($exp_file);
for($a = 0;$a < $tot_a;++$a) {
    $line = explode(':',$exp_file[$a]);
    $list[$line[0]] = $line[1];
}

$res = $db_new->query("select t1.f_id,f_code_main,f_group_code,f_code_cross from t_pair_cross as t1
    join t_wares as t2 on t1.f_code_cross = t2.f_code
    join t_workorders as t3 on t1.f_code_main = t3.f_code
    where t2.f_type_id = 10 and t3.f_type_id = 4"
)->fetchAll();

foreach($res as $line) {
    if(isset($list[$line['f_group_code']])) {
        $db_new->update("t_pair_cross",array("f_group_code"=>$list[$line['f_group_code']]),"f_id = {$line['f_id']}");
    }
}



die('ok');
//creo le cross con i selettori
$cross = array(
    /*"t_selector_ware",
    "t_selector_wo",
    "t_wares_relations",
    "t_wo_relations",*/
    "t_pair_cross",
    //"t_ware_wo"
);

$pair_field_converter = array(
    'fc_wo_rsc_cost'=>'fc_rsc_cost',
    'fc_wo_rsc_since'=>'fc_rsc_since',
    'fc_wo_rsc_till'=>'fc_rsc_till',
    'fc_wo_inv_cost'=>'fc_material_cost',
    'fc_wo_tool_since'=>'fc_tool_since',
    'fc_wo_tool_till'=>'fc_tool_till',
    'fc_wo_tool_cost'=>'fc_tool_cost',
    'fc_wo_task_issue'=>'fc_task_issue',
    'fc_wo_task_notes'=>'fc_task_notes',    
    'fc_wo_task_issue_not_performed'=>'fc_task_issue_not_performed',
    'fc_wo_task_issue_positive'=>'fc_task_issue_positive',
    'fc_wo_task_issue_negative'=>'fc_task_issue_negative',
    'fc_wo_task_issue_not_executable'=>'fc_task_issue_not_executable',
    'fc_wo_task_duration_hours'=>'fc_task_duration_hours',
    'fc_wo_vendor_cost'=>'fc_vendor_cost',
    'fc_wo_vendor_date'=>'fc_vendor_date',
    'fc_wo_asset_downtime_from'=>'fc_asset_downtime_since',
    'fc_wo_asset_downtime_to'=>'fc_asset_downtime_till',
    'fc_asset_planned_downtime_pm'=>'fc_asset_planned_downtime',
    'fc_pm_inv_cost'=>'fc_material_cost',
    'fc_pm_inv_moved_quantity'=>'fc_material_moved_quantity',
    'fc_wo_inv_moved_quantity'=>'fc_material_moved_quantity',
    'fc_wo_inv_moved_quantity'=>'fc_material_moved_quantity',
    'fc_inv_moving_date'=>'fc_material_moving_date',
    'fc_inv_moved_quantity'=>'fc_material_moved_quantity',
    'fc_inv_cost'=>'fc_material_cost',
);
$black_pair_list = array(
    'fc_wo_task_breakdown',
    'f_code','f_id','f_active'
);
foreach($cross as $table) {
    echo $table.PHP_EOL;
    $i = 0;
    $inc = 30000;
    $col1 = "";
    $col2 = "";
    switch($table) {
        case "t_selector_ware":
            $col1 = "f_selector_id";
            $col2 = "f_ware_id";
            break;
        case "t_selector_wo":
            $col1 = "f_selector_id";
            $col2 = "f_wo_id";
            break;
        case "t_wares_relations":
            $col1 = "f_code_ware_master";
            $col2 = "f_code_ware_slave";
            break;
        case "t_ware_wo":
            $col1 = "f_ware_id";
            $col2 = "f_wo_id";
            break;
        case "t_wo_relations":
            $col1 = "f_code_wo_master";
            $col2 = "f_code_wo_slave";
            break;
        case "t_pair_cross":
            $col1 = "f_code_main";
            $col2 = "f_code_cross";
            break;
    }
    do {
        $not_end = true;
        $sel_old->reset();
        $res_trunk = $sel_old->from($table)->limit($inc,$i)->query()->fetchAll();
        $tot_a = count($res_trunk);
        if($tot_a < $inc) $not_end = false;
        for($a = 0;$a < $tot_a;++$a) {
            $new_rec = $res_trunk[$a];            
            foreach($new_rec as $key_rec => &$value_rec) {
                if(in_array($key_rec, $black_pair_list)){
                    unset($new_rec[$key_rec]);
                }
                elseif(array_key_exists($key_rec, $pair_field_converter)) {
                    $new_rec[$pair_field_converter[$key_rec]] = $value_rec;
                    unset($new_rec[$key_rec]);
                }
            }
            $new_rec[$col1] = $list[$new_rec[$col1]];
            if($new_rec[$col2] != -1) {
                $new_rec[$col2] = $list[$new_rec[$col2]];
            }
            elseif(isset($new_rec['f_group_code']) && !empty($new_rec['f_group_code'])) {
                $new_rec['f_group_code'] = $list[$new_rec['f_group_code']];
            }
            // unset($new_rec['f_id']);unset($new_rec['f_active']);unset($new_rec['f_code']);
            if(empty($new_rec[$col1]) || empty($new_rec[$col2]) ) continue;
            
            $db_new->insert($table,$new_rec);
        }  
        echo $i.PHP_EOL;
        $i+=$inc;
    }while($not_end);    
}
die;
//trasporto UI
/*$sel_old->reset();
$res_ui_old = $sel_old->from("t_ui_object_instances",array("f_type_id","f_instance_name","f_properties"))
        ->where("f_type_id in (5,6,7,8,12)")->where("f_instance_name not like '%\_pp\_%'")->query()->fetchAll();
$tot_a = count($res_ui_old);
for($a = 0;$a < $tot_a;++$a) {
    $sel_new->reset();
    $res_check = $sel_new->from("t_ui_object_instances")->where("f_instance_name = ?",$res_ui_old[$a]['f_instance_name'])
            ->query()->fetch();
    if(empty($res_check)) {
        $db_new->insert("t_ui_object_instances",$res_ui_old[$a]);
    }
}
*/

//traposto periodiche
$res_periodics_old = $sel_old->from("t_periodics")->query()->fetchAll();
$tot_a = count($res_periodics_old);

for($a = 0;$a < $tot_a;++$a) {
    $old_code = $res_periodics_old[$a]['f_code'];    
    $periodic = $res_periodics_old[$a];
    if(!array_key_exists($old_code, $list)) continue
    $sel_old->reset();
    if(isset($periodic['f_active']) && $periodic['f_active'] == 0) { continue;}    
    $periodic['f_code'] = $list[$old_code];
    unset($periodic['f_id']);unset($periodic['f_active']);    
    $db_new->insert("t_periodics",$periodic);    
}
echo "Periodic creati";
die;
//trasporto UI
$sel_old->reset();
$res_ui_old = $sel_old->from("t_ui_object_instances",array("f_type_id","f_instance_name","f_properties"))->where("f_type_id = 9")
        ->where("f_instance_name like '%\_edit\_c1'")->where("f_instance_name not like '%\_pp\_%'")->query()->fetchAll();
$tot_a = count($res_ui_old);
for($a = 0;$a < $tot_a;++$a) {
    echo $res_ui_old[$a]['f_instance_name'].PHP_EOL;
    $prop = json_decode($res_ui_old[$a]['f_properties'],true);
    if(!is_array($prop)) die($res_ui_old[$a]['f_instance_name'].'non va');
    $fields = explode(',',$prop['fields']);
    //check new and add ui
    $sel_new->reset();    
    $res_ui_new = $sel_new->from("t_ui_object_instances")->where("f_instance_name = ?",$res_ui_old[$a]['f_instance_name'])->query()->fetch();
    $prop_new = json_decode($res_ui_new['f_properties'],true);    
    if(!is_array($prop_new)) die($res_ui_new['f_instance_name'].'non va');
    $fields_new = explode(',',$prop_new['fields']);
    $tot_b = count($fields);
    for($b = 0;$b < $tot_b;++$b) {
        $searched = trim($fields[$b]);
        if(!in_array($searched, $fields_new)) {
            $fields_new[] = $searched;
        }
    }
    $prop_new['fields'] = implode(',',$fields_new);
    $prop_new = json_encode($prop_new);
    $db_new->update("t_ui_object_instances",array("f_properties"=>$prop_new),"f_instance_name = '{$res_ui_new['f_instance_name']}'");    
} 