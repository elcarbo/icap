<?php
require_once ('constants.php');

$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');

$conn = Zend_Db::factory($config->database);          
$select = new Zend_Db_Select($conn);
$response = array();
if(strpos($_POST['table'],"cross") === false ){
    $res_types = $select->from("{$_POST['table']}_types")->query()->fetchAll();
    


    if(!empty($res_types)) {
        foreach($res_types as $line) {
            $response[] = $line['f_type'];
        }    
    }
}
elseif($_POST['table'] == 'cross') {
    $response = array(
        't_ware_wo',
        't_wares_relations',
        't_selector_ware',
        't_selector_wo',
        't_wo_relations',
        't_pair_cross',
        't_periodics'
    );
}
$conn->closeConnection();
echo json_encode($response);