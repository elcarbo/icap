<?php
function import() {
    $time = time();
    try {
        ini_set("display_errors","On");
        defined('BASE_PATH')
        || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

        require_once BASE_PATH.'/constants.php';
        //Zend_session::writeClose();            
        require_once(APPLICATION_PATH.'/../library/phpExcel/PHPExcel.php');
        include (BASE_PATH.'/utilities.php');
        include (BASE_PATH.'/chunkReadFilter.php');
        include (BASE_PATH.'/Iterator.php');
        include (BASE_PATH.'/wares.php');
        include (BASE_PATH.'/workorders.php');
        include (BASE_PATH.'/ui.php');
        include (BASE_PATH.'/selector.php');
        include (BASE_PATH.'/bookmark.php');
        include (BASE_PATH.'/scheduler/mp_class.php');

        date_default_timezone_set("Europe/Paris");

        $dir = opendir(BASE_PATH.'/uploads');
        $project_xls = "";
        //initialize database
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        Zend_Registry::set('db_config',$config);
        $db = Zend_Db::factory($config->database);
        $select = new Zend_Db_Select($db);
        Zend_Registry::set('db',$db);

        while($entryName = readdir($dir)) {
            if($entryName == '.' || $entryName == '..') continue;
            if(strrpos($entryName, ".xls") !== false) {        
                $project_xls = $entryName;         
                //controllo su db che ci sia il file richiesto e la white list
                $select->reset();
                $res_xls = $select->from("t_queue")->where("f_file_name = '$project_xls'")->where("f_active = 1")->query()->fetchAll();        
                if(empty($res_xls)){$project_xls = ""; continue;}
                $xls = $res_xls[0];
                if(empty($xls['f_sheets'])){$project_xls = ""; continue; }                        
                $white_list = explode(',',$xls['f_sheets']);    
                $f_id = $xls['f_id'];            
                //prendo in consegna la richiesta            
                $db->update("t_queue",array('f_active'=>0),"f_id = {$f_id}");                    
                break;
            }    
        }
        closedir($dir);

        if(empty($project_xls)) {
            echo ("No File found at ".date('d-m-Y H:i:s').PHP_EOL);
            return;
        }
        Zend_Registry::set("f_id",$f_id);
        Zend_Registry::set("f_timestamp",time());

        $db->closeConnection();
        //initialize import function
        $wares = new Wares();
        $selector = new Selector();
        $bookmark = new Bookmark();    
        $wo = new Workorders();        
        $ui = new Ui();
        $iterator = new XlsIterator();

        $keys = array();    
        $file = $iterator->IterateExcel(BASE_PATH."/uploads/$project_xls",1,1);            
        $sheets = $file->getSheetNames();        

        //get Custom sheet            
        $custom = array();    
        if(in_array("Custom",$sheets)) {//check if there is Custom sheet
            $custom = utilities::createCustomList(BASE_PATH."/uploads/$project_xls",$iterator,$sheets);                 
        }        
        //--------------------------------------------------------------------------

        //extract and parse validation tables 
        $validation = array();
        if(in_array("Validation Tables",$sheets)) {        
            $validation = utilities::createValidationList(BASE_PATH."/uploads/$project_xls",$iterator,$sheets);                 
        }    
    }catch(Exception $e) {
        utilities::reportError("Uncatchable error : ".$e->getMessage(), $f_id);
        echo json_encode(array("response"=>"ko"));return;
    }    
    $tot_a = count($white_list);    
    //set default wf and phase    
    utilities::setWfPhases();     
    for($a = 0; $a < $tot_a;++$a) {  
        $db = Zend_Db::factory($config->database);  
        Zend_Registry::set('db',$db);
        $select = new Zend_Db_Select($db);
        $inc = 3000;
        $sheet = $white_list[$a];   
        $check_list[$sheet] = $default_check_list;        
        $table = '';
        $f_type_id = 0;
        $sheet_configuration = array();
        //sheet request to import
        $sheet = $white_list[$a];
        $res_conf = array();//array with sheet configuration
        $select->reset();
        if(strpos($sheet,"t_wares") === 0) {
            $res_conf = $select->from("t_wares_types",array('f_id','f_short_name','f_sheet_name','f_module_name'))
                    ->where("f_sheet_name = ?",$sheet)->query()->fetch();    
            $table = "t_wares";
        }
        elseif(strpos($sheet,"t_workorders") === 0) {
            $res_conf = $select->from("t_workorders_types",array('f_id','f_short_name','f_sheet_name','f_module_name'))
                    ->where("f_sheet_name = ?",$sheet)->query()->fetch();
            $table = "t_workorders";
        }
        $select->reset();
        
        if(!empty($res_conf)) {    
            $f_type_id = $res_conf['f_id'];
            $sheet_configuration['table'] = 't_wares';
            $sheet_configuration['f_type'] = $res_conf['f_id'];
            if(!is_null($res_conf['f_module_name'])) {
                $sheet_configuration['mdl'] = $res_conf['f_module_name'];
            }
            if(!is_null($res_conf['f_short_name'])) {
                $sheet_configuration['short_name'] = $res_conf['f_short_name'];
            }
        }
        elseif(!$f_type_id && strpos($sheet,"t_selector") !== false){
            $f_type_id = (int)str_replace("t_selector_","",$sheet);    
            $table = 't_selectors';        
        }
        
        try {
            //**** PRIMO CICLO DA ANALIZZARE            
            if(!in_array($sheet, $sheets)){ /*utilities::reportError("Unknown sheet ".$sheet,false);*/return;}
            $pos = array_search($sheet, $sheets);

            $key_sheet = array();
            $file = $iterator->IterateExcel(BASE_PATH."/uploads/$project_xls",1,1);
            $rowIterator = $file->setActiveSheetIndex($pos)->getRowIterator();    
            $iterator->rowIteratorImport($rowIterator,$key_sheet,true);
            $keys[$sheet] = $key_sheet;
            $file->disconnectWorksheets();
            unset($file);
            utilities::createRollBack($sheet,$table,$db);
            if(!empty($sheet_configuration) || strpos($sheet, "t_selectors") !== false) {
                $custom_cols = utilities::get_tab_cols("t_custom_fields");
                $tab = strpos($sheet, "t_selectors") !== false?
                utilities::get_tab_cols("t_selectors"):utilities::get_tab_cols($sheet_configuration['table']);
                $tot_a = count($keys[$sheet]);            
                foreach($keys[$sheet] as $col){
                    //convert in lower case to avoid problem with mysql
                    $col_ok = strtolower($col);
                    if(!in_array($col_ok,$tab)) {
                        if(!in_array($col_ok,$custom_cols) && $col != 'f_parent_code' && !in_array($col_ok,$already_insert)) {                
                            //check special chars and not permitted char for mysql col name                
                            utilities::createCustomCol($col_ok, $custom[$col],$f_id);
                            $already_insert[] = $col_ok;
                        }
                    }
                }
            }
            unset($select);
            $db->closeConnection();
                        
            // include columns for the query
            if($table != '') {        
                utilities::setDefaultTableCols($table);
                utilities::setCreationTableCols();
                utilities::setCustomTableCols();
            }    
            if($f_type_id == 16) {
                utilities::setUserTableCols();
            }
            elseif($f_type_id == 9) {
                utilities::setWizardTableCols();
            }

            //in case o cross, increment the trunk from 3K to 10K
            if($table != 't_wares' && $table != 't_workorders' && $table != 't_selectors' && $table != 't_pair_cross') { // in case of cross, improve inc
                $inc = 10000;
            }
            
            //add check rules
//            foreach($keys[$sheet] as $sheet_key) {
//                if(isset($custom[$sheet_key]['f_check_rules']) && !empty($custom[$sheet_key]['f_check_rules']))
//                    $check_list[$sheet][$sheet_key] = $custom[$sheet_key]['f_check_rules'];
//            }
//            //add custom check rules
              foreach($keys[$sheet] as $line_key) {
                  if(isset($custom[$line_key]['f_check_rules']) && !empty($custom[$line_key]['f_check_rules'])){
                      if(isset($check_list[$sheet][$line_key])) unset($check_list[$sheet][$line_key]);
                        $check_list[$sheet][$line_key]['check'] = $custom[$line_key]['f_check_rules'];
                        $check_list[$sheet][$line_key]['check_value'] = $custom[$line_key]['f_check_value'];
                  }
              }
            //set default main table cols
            utilities::setDefaultTableCols($table);            
            $position = 1; 
            $objects = array();
            utilities::$array_uniques = array();
            do {                
                $db = Zend_Db::factory($config->database);  
                Zend_Registry::set('db',$db);
                echo $position.PHP_EOL;
                $not_end = true;                
                $file = $iterator->IterateExcel(BASE_PATH.'/uploads/'.$project_xls,$position,$inc,$sheet);
                die('OK');
                $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
                $objects = $iterator->rowIteratorImport($rowIterator,$keys[$sheet],false); 
                $file->disconnectWorksheets();
                unset($file); 
                $mdl = isset($sheet_configuration['mdl'])?$sheet_configuration['mdl']:'';
                utilities::manageTrunk($sheet, $check_list, $table, $f_type_id,$mdl,$custom,$position,$objects);
                if(count($objects)<$inc-1) $not_end = false;                              
                $position+=$inc;
                $db->closeConnection();
            }while($not_end);
            //enable db connection to finish ui
            $db = Zend_Db::factory($config->database);  
            Zend_Registry::set('db',$db);
            //create custom and bookmark
            if(!empty($sheet_configuration)) {
                utilities::createFathers($sheet_configuration['table'], $sheet_configuration['f_type'], $f_id);    
                if($sheet_configuration['table'] == "t_wares" && $sheet != 't_wares_wizard') {         
                    $wares->createParentField($sheet_configuration['f_type']);                           
                }
                if($sheet_configuration['table'] == "t_workorders") {                
                    $wo->addAssetField($sheet_configuration['f_type']);
                    if($sheet_configuration['f_type'] == 9) {
                        // change placeholder
                        $wo->completeOnCondition();                            
                    }
                }
                if(isset($sheet_configuration['short_name']) && isset($sheet_configuration['mdl'])) {
                    $bookmark->createBookmark($sheet_configuration['short_name'], $keys[$sheet], $custom);
                    $ui->createUi($sheet_configuration['short_name'], $keys[$sheet], $custom,$validation);                    
                }                
            }
            elseif($sheet == 't_cross_workorders_wares') {
                $wo->wares_opened_flag(); //icons about wares/workorders
                $wo->wo_asset();//fill information about asset cross with wo
                $wo->selectorWoFromAsset();// if wo is without selector, assign selector of asset/s associate
            }            
            if($table == "t_selectors") {                
                $selector->fatherSelector($f_type_id); //FUNZIONA         
            }            
            //update finish this sheet            
            $finish_sheet[] = $sheet;
            $db->update("t_queue",array('f_complete_sheet'=>implode(',',$finish_sheet)),"f_id = $f_id");
            $db->closeConnection();
        }catch(Exception $e) {
            utilities::reportError("Uncatchable error : ".$e->getMessage());
            echo json_encode(array("response"=>"ko"));return;
        }
    }
    //clear import file and queue to close the import
    unlink(BASE_PATH."/uploads/$project_xls");            
    $db = Zend_Db::factory($config->database);                  
    $db->delete("t_queue","f_id = $f_id");    
    echo "File $project_xls has been imported correctly in ".(time() - $time)." seconds".PHP_EOL;
    $db->closeConnection();
}