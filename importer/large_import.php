<?php
function import_csv() {
    $time = time();
    $wares_sheet_key = array(
        "t_wares_asset"=>array('table'=>'t_wares','f_type'=>1,'mdl'=>'mdl_asset_tg','short_name'=>'asset'),
        "t_wares_vendors"=>array('table'=>'t_wares','f_type'=>6,'mdl'=>'mdl_vendor_tg','short_name'=>'vendor'),
        "t_wares_tasks"=>array('table'=>'t_wares','f_type'=>10,'mdl'=>'mdl_task_tg','short_name'=>'task'),
        "t_wares_wizard"=>array('table'=>'t_wares','f_type'=>9),
        "t_wares_inventory"=>array('table'=>'t_wares','f_type'=>3,'mdl'=>'mdl_inv_tg','short_name'=>'inv'),
        "t_wares_labor"=>array('table'=>'t_wares','f_type'=>2,'mdl'=>'mdl_rsc_tg','short_name'=>'rsc'),
        "t_wares_tools"=>array('table'=>'t_wares','f_type'=>4,'mdl'=>'mdl_tool_tg','short_name'=>'tool'),
        "t_wares_contracts"=>array('table'=>'t_wares','f_type'=>7,'mdl'=>'mdl_contract_tg','short_name'=>'contract'),
        "t_wares_failurecodes"=>array('table'=>'t_wares','f_type'=>8,'mdl'=>'mdl_fc_tg','short_name'=>'fc'),
        "t_wares_documents"=>array('table'=>'t_wares','f_type'=>5,'mdl'=>'mdl_doc_tg','short_name'=>'doc'),
        "t_wares_action"=>array('table'=>'t_wares','f_type'=>11,'mdl'=>'mdl_action_tg','short_name'=>'action'),
        "t_wares_schedule"=>array('table'=>'t_wares','f_type'=>14,'mdl'=>'mdl_sch_tg','short_name'=>'sch'),
        "t_wares_users"=>array('table'=>'t_wares','f_type'=>16,'mdl'=>'mdl_user_tg','short_name'=>'user'),
        "t_workorders_workorder"=>array('table'=>'t_workorders','f_type'=>1,'mdl'=>'mdl_wo_tg','short_name'=>'wo'),
        "t_workorders_periodic_maint"=>array('table'=>'t_workorders','f_type'=>3,'mdl'=>'mdl_pm_tg','short_name'=>'pm'),
        "t_workorders_on_condition"=>array('table'=>'t_workorders','f_type'=>9,'mdl'=>'mdl_cond_tg','short_name'=>'cond'),
        "t_workorders_meter"=>array('table'=>'t_workorders','f_type'=>11,'mdl'=>'mdl_meter_tg','short_name'=>'meter'),
        "t_workorders_standard_workorder"=>array('table'=>'t_workorders','f_type'=>15,'mdl'=>'mdl_meter_tg','short_name'=>'std')        
    );
    
    ini_set("display_errors","On");
    defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));
    
    //Zend_session::writeClose();            
    date_default_timezone_set("Europe/Paris");
        
    $dir = opendir(BASE_PATH.'/uploads');
    $project_xls = "";
    //initialize database
    $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
    Zend_Registry::set('db_config',$config);
    $db = Zend_Db::factory($config->database);
    $select = new Zend_Db_Select($db);
    Zend_Registry::set('db',$db);
    
    $insert_file = "";
    while($entryName = readdir($dir)) {
        if($entryName == '.' || $entryName == '..') continue;
        if(strrpos($entryName, ".csv") !== false) {        
            $project_xls = $entryName;         
            //controllo su db che ci sia il file richiesto e la white list
            $select->reset();
            $res_xls = $select->from("t_queue")->where("f_file_name = '$project_xls'")->where("f_active = 3")->query()->fetchAll();        
            if(empty($res_xls)){$project_xls = ""; continue;}
            $xls = $res_xls[0];
            if(empty($xls['f_sheets'])){$project_xls = ""; continue; }                        
            $white_list = $xls['f_sheets'];    
            $f_id = $xls['f_id'];            
            //prendo in consegna la richiesta            
            $db->update("t_queue",array('f_active'=>0),"f_id = {$f_id}");                    
            break;
        }    
    }
    closedir($dir);
    if(empty($project_xls)) {        
        return;
    }
    Zend_Registry::set("f_id",$f_id);
    Zend_Registry::set("f_timestamp",time());
    
    $db->closeConnection();
    //initialize import function
    $wares = new Wares($f_id);
    $selector = new Selector($f_id);
    $bookmark = new Bookmark($f_id);    
    $wo = new Workorders($f_id);
    $mp = new mp_class($f_id);
    $ui = new Ui($f_id);    

    $keys = array();        
    $t_engagement = array();
    $t_engagement_meter = array();        
    //list of complete sheet
    $finish_sheet = array();            
    //set default wf and phase
    utilities::setWfPhases();        
    $db = Zend_Db::factory($config->database);  
    Zend_Registry::set('db',$db);
    $select = new Zend_Db_Select($db);
    $inc = 3000;
    $line = $white_list;             
    try {        
        $file = explode(PHP_EOL,file_get_contents(BASE_PATH."/uploads/$project_xls"));
        echo count($file);die;
        //$file = $iterator->IterateExcel(BASE_PATH."/uploads/$project_xls",1,1);        
        $keys[$line] = $key_sheet;
        $file->disconnectWorksheets();
        unset($file);

        //f_id of start in case of rollback
        $f_id_starts = array();

        $select->reset();
        if(array_key_exists($line,$wares_sheet_key)) {
            $f_type_id = isset($wares_sheet_key[$line]['f_type'])?$wares_sheet_key[$line]['f_type'] : 0;    
            $table = $wares_sheet_key[$line]['table'];        
        }
        elseif(!$f_type_id && strpos($line,"t_selector") !== false){
            $f_type_id = (int)str_replace("t_selector_","",$line);    
            $table = 't_selectors';        
        }    
        //this condition are only to catch the max f_id in case of rollback
        elseif(strpos($line,"t_cross_") !== false){ // simple cross
            switch($line) {
                case 't_cross_wares_selectors' : 
                    $res_max_id = $select->from("t_selector_ware",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_selector_ware"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_wares_wares' : 
                    $res_max_id = $select->from("t_wares_relations",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_wares_relations"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_workorders_wares' : 
                    $res_max_id = $select->from("t_ware_wo",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_ware_wo"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_workorders_selectors' : 
                    $res_max_id = $select->from("t_selector_wo",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_selector_wo"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
            }
        }
        //t_pair_cross, t_engagement and t_engagement_meter are saved in t_pair_cross table
        elseif($line == 't_pair_cross' || $line == 't_engagement' || $line == 't_engagement_meter') {
            $res_max_id = $select->from("t_pair_cross",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_pair_cross"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
            //catch max f_id of t_periodics in case of engagement
            if($line == 't_engagement' || $line == 't_engagement_meter') {
                $select->reset();
                $res_max_id = $select->from("t_periodics",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                $f_id_starts["t_periodics"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
            }
        }

        //if not empty table, compile ids relative table,table_parent,creation_date and custom fields
        if(!empty($table)) {        
            //do the same operation for father and custom fields
            $select->reset();        
            $res_max_id = $select->from("{$table}_parent",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["{$table}_parent"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 

            $select->reset();
            $res_max_id = $select->from($table,array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts[$table] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 


            $select->reset();        
            $res_max_id = $select->from("t_custom_fields",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_custom_fields"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 

            $select->reset();        
            $res_max_id = $select->from("t_creation_date",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_creation_date"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
        }

        if(array_key_exists($line, $wares_sheet_key) || strpos($line, "t_selectors") !== false) {
            $custom_cols = utilities::get_tab_cols("t_custom_fields");
            $tab = strpos($line, "t_selectors") !== false?
            utilities::get_tab_cols("t_selectors"):utilities::get_tab_cols($wares_sheet_key[$line]['table']);
            $tot_a = count($keys[$line]);            
            foreach($keys[$line] as $col){
                //convert in lower case to avoid problem with mysql
                $col_ok = strtolower($col);
                if(!in_array($col_ok,$tab)) {
                    if(!in_array($col_ok,$custom_cols) && $col != 'f_parent_code' && !in_array($col_ok,$already_insert)) {                
                        //check special chars and not permitted char for mysql col name                
                        utilities::createCustomCol($col_ok, $custom[$col],$f_id);
                        $already_insert[] = $col_ok;
                    }
                }
            }
        }

        //close existing connection (will be re-open when finish system)
        $compress_roolback = serialize($f_id_starts);
        $db->update("t_queue",array("f_rollback"=>$compress_roolback),"f_id = $f_id");
        unset($select);
        $db->closeConnection();

        //set f_type_id and table (if necessary)
        $f_type_id = 0;
        if(array_key_exists($line, $wares_sheet_key)){
            $f_type_id = $wares_sheet_key[$line]['f_type'];        
            $table = $wares_sheet_key[$line]['table'];        
        }
        elseif(strpos($line,"t_selector") !== false) {
            $f_type_id = (int)str_replace("t_selector_","",$line);
            $table = 't_selectors';
        }
        // include columns for the query
        if($table != '') {        
            utilities::setDefaultTableCols($table);
            utilities::setCreationTableCols();
            utilities::setCustomTableCols();
        }    
        if($f_type_id == 16) {
            utilities::setUserTableCols();
        }
        elseif($f_type_id == 9) {
            utilities::setWizardTableCols();
        }

        //in case o cross, increment the trunk from 3K to 10K
        if($table != 't_wares' && $table != 't_workorders' && $table != 't_selectors' && $table != 't_pair_cross') { // in case of cross, improve inc
            $inc = 10000;
        }
        //default check rules
        $check_list = array(
            $line=>array(
                'f_code'=>array('check'=>'unique','message'=>'f_code must be unique and not empty'),
                'f_wf_id'=>array('check'=>'wf_exist','message'=>"f_wf_id must exist.If you don't know what workflow assign,leave the column f_wf_id empty."),
                'f_phase_id'=>array('check'=>'phase_exist','message'=>"f_phase_id must exist.If you don't know what workflow assign,leave the column f_wf_id empty."),
                "f_start_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "f_end_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_wo_starting_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_wo_ending_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_offer_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_sch_end_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_contract_starting_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_contract_ending_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_imp_start_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_imp_end_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "f_title"=>array('check'=>'empty_field','message'=>'Mainsim don\'t allow empty f_title. '),
                "fc_pm_start_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "fc_pm_end_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
                "f_type_id_wares"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
                "f_type_id_selector"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
                "f_type_id_wares_master"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
                "f_type_id_wares_slave"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
                "f_type_id_workorder"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"')
            )            
        );
        //add check rules
        foreach($keys[$line] as $line_key) {
            if(isset($custom[$line_key]['f_check_rules']) && !empty($custom[$line_key]['f_check_rules']))
                $check_list[$line][$line_key] = $custom[$line_key]['f_check_rules'];
        }
        //set default main table cols
        utilities::setDefaultTableCols($table);            
        $position = 1; 
        $objects = array();
        utilities::$array_uniques = array();
        do {                
            $db = Zend_Db::factory($config->database);  
            Zend_Registry::set('db',$db);
            echo $position.PHP_EOL;
            $not_end = true;                
            $file = $iterator->IterateExcel(BASE_PATH.'/uploads/'.$project_xls,$position,$inc,$line);
            $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
            $objects = $iterator->rowIteratorImport($rowIterator,$keys[$line],false); 
            $file->disconnectWorksheets();
            unset($file);                        
            //create array with unique field
            foreach($check_list[$line] as $key_check => $value_check) {
                if($value_check['check'] == 'unique') {
                    utilities::setUniquesArray($key_check,$objects);
                }
            }                   
            //set f_order for t_creation_date
            if($table == 't_wares' || $table == 't_workorders' || $table == 't_selectors') {
                utilities::setCreationDateOrder($table,$f_type_id);
                if($table != 't_selectors') {
                    utilities::setFirstFcProgress($wares_sheet_key[$line]['mdl']);
                }
            }                                          
            //check rules                
            $tot_b = count($objects);                
            for($b = 0; $b < $tot_b;++$b) {    
                $row = $objects[$b];                    
                foreach($row as $key_check => $value) {
                    if(isset($check_list[$line][$key_check])) {
                        $col = $key_check;
                        $table = isset($wares_sheet_key[$line]['table'])?$wares_sheet_key[$line]['table']:'t_selectors';                        
                        if($col == 'f_code') {
                            if($wares_sheet_key[$line]['table'] == 't_wares') {
                                $col = 'fc_imp_code_wares_excel';
                            }
                            elseif($wares_sheet_key[$line]['table'] == 't_workorders') {
                                $col = 'fc_imp_code_wo_excel';
                            }
                            else {
                                $col = 'fc_imp_code_sel_excel';
                            }
                        }

                        if($table != '' && $f_type_id !=0 && isset($objects[$b][$key_check])){                                                        
                            if(!utilities::check($value, $check_list[$line][$key_check]['check'], $col,$table , $objects,$b,$f_type_id)) {                                
                                $message = isset($check_list[$line][$key_check]['message'])?$check_list[$line][$key_check]['message']:'error raised during the controll of the field '.$key_check;                                    
                                utilities::reportError("Error occurred at line ".($position+$b).' : '.$message);
                                echo json_encode(array('response'=>'ko'));
                                die;
                            }
                        }
                    }
                    if(isset($validation[$key_check])) {                            
                        if(!in_array($value,$validation[$key_check]['f_value_allow'])) {                
                            $message = 'The value inside the column '.$key_check." don't respect the validation table.";
                            utilities::reportError("Error occurred at line ".($position+$b).' : '.$message);
                            echo json_encode(array('response'=>'ko'));die;
                        }                            
                    }
                }

                if(strpos($line,"t_wares") !== false) {            
                    $wares->insertWares($objects[$b],$custom,$f_type_id);
                }                    
                elseif(strpos($line,"t_workorders") !== false) {            
                    $wo->insertWorkorder($objects[$b],$custom,$f_type_id);
                }    
                elseif($line == "t_pair_cross") {
                    $wo->insertPairCross($objects[$b]);
                }
                elseif($line == "t_engagement" ) {                                     
                    $t_engagement = $wo->createEngagement($objects[$b]); //FUNZIONA                        
                    $mp->periodiGenerator($t_engagement);//FUNZIONA                        
                    $wo->pm_frequency($t_engagement['f_code']);
                    $wo->addNextDueDate($t_engagement['f_code']);// set due date and subsequent due date
                }
                elseif($line == "t_engagement_meter") {                
                    $t_engagement_meter = $wo->createEngagement($objects[$b],11); //FUNZIONA                
                    $mp->periodiGenerator($t_engagement_meter);//FUNZIONA    
                }  
                elseif(strpos($line,"t_cross") !== false){
                    switch($line) {                            
                        case 't_cross_workorders_wares' ://cross between wares and workorders 
                            $wo->crossWaresWorkorders($objects[$b]);
                            break;
                        case 't_cross_wares_wares' : //cross between wares and wares
                            $wares->crossAllWares($objects[$b]);
                            break;
                        case 't_cross_wares_selectors' : //cross between wares and selectors                                    
                            $selector->crossSelectors($objects[$b],'t_wares');
                            break;
                        case 't_cross_workorders_selectors' : //cross between workorders and selectors                                    
                            $selector->crossSelectors($objects[$b],'t_workorders');
                            break;
                    }
                }                    
            }
            if(count($objects)<$inc-1) $not_end = false;                              
            $position+=$inc;
            if(strpos($line,"t_selector") !== false) {            
                $selector->insertSelectors($objects,$custom,$f_type_id);
            }
            $db->closeConnection();
        }while($not_end);
        //enable db connection to finish ui
        $db = Zend_Db::factory($config->database);  
        Zend_Registry::set('db',$db);
        //create custom and bookmark
        if(array_key_exists($line,$wares_sheet_key)) {
            utilities::createFathers($wares_sheet_key[$line]['table'], $wares_sheet_key[$line]['f_type'], $f_id);    
            if($wares_sheet_key[$line]['table'] == "t_wares") {         
                $wares->createParentField($wares_sheet_key[$line]['f_type']);
                if($wares_sheet_key[$line]['f_type'] == 1 || $wares_sheet_key[$line]['f_type'] == 9) { //if asset                                                
                    $wares->setWizardCustom();
                }        
            }
            if($wares_sheet_key[$line]['table'] == "t_workorders") {                
                $wo->addAssetField($wares_sheet_key[$line]['f_type']);
                if($wares_sheet_key[$line]['f_type'] == 3) {                    
                    $wo->asset_opened_flag(3,"fc_asset_opened_pm");
                }
                elseif($wares_sheet_key[$line]['f_type'] == 9) {
                    // change placeholder
                    $wo->completeOnCondition();        
                    $wo->asset_opened_flag(9,"fc_asset_opened_on_condition");
                }
            }
            if(isset($wares_sheet_key[$line]['short_name']) && isset($wares_sheet_key[$line]['mdl'])) {
                $bookmark->createBookmark($wares_sheet_key[$line]['short_name'], $value, $custom);
                $ui->createUi($wares_sheet_key[$line]['short_name'], $value, $custom,$validation);                    
            }                
        }

        if($table == "t_selectors") {                
            $selector->fatherSelector($f_type_id); //FUNZIONA         
        }            
        //update finish this sheet            
        $finish_sheet[] = $line;
        $db->update("t_queue",array('f_complete_sheet'=>implode(',',$finish_sheet)),"f_id = $f_id");
        $db->closeConnection();
    }catch(Exception $e) {
        utilities::reportError("Uncatchable error : ".$e->getMessage(), $f_id);
        echo json_encode(array("response"=>"ko"));return;
    }

    //clear import file and queue to close the import
    unlink(BASE_PATH."/uploads/$project_xls");            
    $db = Zend_Db::factory($config->database);                  
    $db->delete("t_queue","f_id = $f_id");    
    echo "File $project_xls has been imported correctly in ".(time() - $time)." seconds".PHP_EOL;
    $db->closeConnection();
}