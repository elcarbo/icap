<?php

class Workorders
{
    private $db;
    
    public function __construct() {}
    
    /**
     * TODO: modificare metodo
     * @param type $engagement
     * @param type $workorders_fcode 
     */
    public function createEngagement($engagement,$type = 3)
    {           
        $this->db = Zend_Registry::get('db');
        $time = Zend_Registry::get('f_timestamp');
        $engagements_complete = array();
        $select = new Zend_Db_Select($this->db);
        $data = $engagement;
        //extract every pm and assigned as key the f_code oh its        
        try {
            $res_code_wo = $select->from("t_workorders",array("f_code",'fc_imp_code_wo_excel'))
                    ->where("f_type_id = $type")->where("fc_imp_code_wo_excel = ?",$data['f_code'])->query()->fetch();
        }catch(Exception $e) {
            utilities::reportQueryError("t_workorders", $e->getMessage(), $select->__toString());
            die;
        }
        if(empty($res_code_wo)) return array();
        
        $f_code_wo = $res_code_wo["f_code"];
        
        //recupero f_code workorders        
        $data_exit = $engagement;
        $select->reset();        

        $data_custom = array();
        $data_pair = array ();

        $data_custom['fc_pm_start_date'] = strtotime($data['fc_pm_start_date']);
        $data_custom['fc_pm_end_date'] = strtotime($data['fc_pm_end_date']);
        $data_exit['fc_pm_start_date'] = $data_custom['fc_pm_start_date'];
        $data_exit['fc_pm_end_date'] = $data_custom['fc_pm_end_date'];

        $data_custom['fc_pm_forewarning_days'] = (int)$data['fc_pm_forewarning_days'];
        $data_custom['fc_pm_forewarning_hours'] = (int)$data['fc_pm_forewarning_hours'];
        $data_custom['fc_pm_forewarning_minutes'] = (int)$data['fc_pm_forewarning_minutes'];

        $data_custom['fc_pm_duration_days'] = (int)$data['fc_pm_duration_days'];
        $data_custom['fc_pm_duration_hours'] = (int)$data['fc_pm_duration_hours'];
        $data_custom['fc_pm_duration_minutes'] = (int)$data['fc_pm_duration_minutes'];

        $data_custom['fc_pm_attime_hours'] = (int)$data['fc_pm_attime_hours'];
        $data_custom['fc_pm_attime_minutes'] = (int)$data['fc_pm_attime_minutes'];            

        $data_custom['fc_pm_forewarning'] = (int)(($data['fc_pm_forewarning_days']*86400) + ($data['fc_pm_forewarning_hours'] * 3600) + ($data['fc_pm_forewarning_minutes'] * 60));
        $data_custom['fc_pm_at_time'] = (int)(($data['fc_pm_attime_hours'] * 3600) + ($data['fc_pm_attime_minutes'] * 60));
        $data_custom['fc_pm_duration'] = (int)(($data['fc_pm_duration_days']*86400) + ($data['fc_pm_duration_hours'] * 3600) + ($data['fc_pm_duration_minutes'] * 60));

        $data_exit['fc_pm_forewarning'] = (int)(($data['fc_pm_forewarning_days']*86400) + ($data['fc_pm_forewarning_hours'] * 3600) + ($data['fc_pm_forewarning_minutes'] * 60));
        $data_exit['fc_pm_at_time'] = (int)(($data['fc_pm_attime_hours'] * 3600) + ($data['fc_pm_attime_minutes'] * 60));
        $data_exit['fc_pm_duration'] = (int)(($data['fc_pm_duration_days']*86400) + ($data['fc_pm_duration_hours'] * 3600) + ($data['fc_pm_duration_minutes'] * 60));

        $f_fixed = substr ($data['fc_pm_fixed'],0,1);
        $data['fc_pm_fixed'] = (int)$f_fixed;
        $data_custom['fc_pm_fixed'] = (int)$f_fixed;

        $f_rules_type = explode (' : ',$data['fc_pm_rules_type_dayweek']);
        $data_custom['fc_pm_rules_type_dayweek'] = $f_rules_type[0];

        $f_rules_dayweek = str_replace('.',',',$data['fc_pm_rules_dayweek']);
        $data_custom['fc_pm_rules_dayweek'] = $f_rules_dayweek;

        $f_rules_type_ex_grp = explode (' : ',$data['fc_pm_rules_type_exception_group']);
        $data_custom['fc_pm_rules_type_exception_group'] = $f_rules_type_ex_grp[0];

        $f_rules_exception_group = explode (' : ',$data['fc_pm_rules_exception_group']);
        $data_custom['fc_pm_rules_exception_group'] = $f_rules_exception_group[0];

        $f_rules_dayweek_exception_group = str_replace('.',',',$data['fc_pm_rules_dayweek_exception_group']);
        $data_custom['fc_pm_rules_dayweek_exception_group'] = $f_rules_dayweek_exception_group;

        //NEW : manage multiple periodic generator for different asset inside it
        $f_rules_type = substr($data['fc_pm_start_date_type'],0,1);
        $data_custom['fc_pm_start_date_type'] = $f_rules_type;
        $data_exit['fc_pm_start_date_type'] = $f_rules_type;
        try {            
            $this->db->update("t_workorders",$data_custom,"f_code = $f_code_wo");
        }catch(Exception $e) {
            utilities::reportError("<div>Error occurred during an update. this is the error : ".$e->getMessage()."</div>");
            die;
        }

        //carico le pair cross

        $sequence = str_replace('.',',',$data['fc_pm_sequence']);
        $data_pair['fc_pm_sequence'] = $sequence;            
        $f_cyclic_type = explode (' : ',$data['fc_pm_cyclic_type']);
        $data_pair['fc_pm_cyclic_type'] = $f_cyclic_type[0];            
        $data_pair['fc_pm_cyclic_number'] = $data['fc_pm_cyclic_number'];            
        $f_periodic = explode (' : ',$data['fc_pm_periodic_type']);
        $data_pair['fc_pm_periodic_type'] = $f_periodic[0];
        $data_pair['f_code_main'] = $f_code_wo;        
        $data_pair['f_timestamp'] = $time;
        $data_pair['f_code_cross'] = -1;
        $data_pair['f_group_code'] = 0;                 

        try {
            $this->db->insert("t_pair_cross",$data_pair);            
        }catch(Exception $e) {
            $dump = Zend_Debug::dump($data_pair,null,false);
            utilities::reportInsertError("t_creation_date", $e->getMessage(), $dump);  
            die;
        }

        $data_exit['f_code'] = $f_code_wo;
        $engagements_complete = $data_exit;            
        
        return $engagements_complete;
    }
    
    public function insertWorkorder($wo,$custom_fields,$f_type_id = 0) 
    {   
        $this->db = Zend_Registry::get('db');
        $time = Zend_Registry::get('f_timestamp');
        $f_order = Zend_Registry::get('f_order');
        try {            
            $i = 0;
            $wo_cols = utilities::$default_table_cols;
            $creation_cols = utilities::$creation_table_cols;
            $custom_cols = utilities::$custom_table_cols;     
            $select = new Zend_Db_Select($this->db);
            $time_list = array(
                "f_start_date",
                "f_end_date",
                "fc_wo_starting_date",
                "fc_wo_ending_date",
                "fc_offer_date",
                "fc_sch_end_date",
                "fc_contract_starting_date",
                "fc_contract_ending_date"
            );        

            //get f_type_id info
            $f_type_info = array();
            try {
                $res = $select->from("t_workorders_types")
                    ->where("f_id = ?",$f_type_id)->query()->fetchAll();
                if(empty($res)) return;            
                $f_type_info = $res[0];            
            }catch(Exception $e) {                
                utilities::reportQueryError("t_creation_date", $e->getMessage(), $select->__toString());
                die;
            }

            //Recupero le colonne dalla tabella t_wares            
            if(empty($wo['f_type_id']) && !$f_type_id) return;       
            if(!empty($wo['f_type_id'])) {
                $exp_type = explode(' : ',$wo['f_type_id']);
                $f_type_id = $exp_type[0];
            }
            $wo_val = $wo;
            //elimino le colonne che non c'entrano con i wo
            $keys = array_keys($wo_val);
            foreach($keys as $line) {
                if(is_null($wo_val[$line])) {
                    $wo_val[$line] = '';
                }
                if(!in_array($line, $wo_cols)){
                    unset($wo_val[$line]);
                    if(!in_array($line,$custom_cols) && !in_array($line, $creation_cols) && $line != 'f_parent_code') {
                        unset($wo[$line]);
                    }
                }                        
            }            

            $f_type_id_periodic = 0;
            //if is a periodic generator, set f_type_workorder = 4
            if($f_type_id == 3 ) {
                $f_type_id_periodic = 4;
            }
            //if is a meter generator, set f_type_workorder = 7
            elseif($f_type_id == 11) {
                $f_type_id_periodic = 4;
            }            
            //creo il creation date
            $creation = array(
                'f_creation_date'=>!isset($wo['f_creation_date']) || empty($wo['f_creation_date'])?$time:strtotime($wo['f_creation_date']),
                'f_order'=>$f_order,
                'f_type'=>"WORKORDERS",
                'f_category'=>  strtoupper($f_type_info['f_type']),
                'f_creation_user'=>1,
                'f_title'=>!isset($wo['f_title'])  || $wo['f_title'] == ''?'':  utf8_decode($wo['f_title']),
                'f_description'=>!isset($wo['f_description'])  || $wo['f_description'] == ''?'':utf8_decode($wo['f_description']),
                'f_wf_id'=>!isset($wo['f_wf_id'])  || $wo['f_wf_id'] == ''?$f_type_info['f_wf_id']:$wo['f_wf_id'],
                'f_visibility'=>!isset($wo['f_visibility'])  || $wo['f_visibility'] == ''?-1:$wo['f_visibility'],
                'f_editability'=>!isset($wo['f_editability']) || $wo['f_editabiliti'] == ''?-1:$wo['f_editability'],
                'f_phase_id'=>!isset($wo['f_phase_id'])  || $wo['f_phase_id'] == ''?$f_type_info['f_wf_phase']:$wo['f_phase_id'],
                'f_timestamp'=>!isset($wo['f_timestamp']) || empty($wo['f_timestamp'])?$time:strtotime($wo['f_timestamp']),
                'fc_progress'=>utilities::$fc_progress
            );    
            utilities::$fc_progress++;
            if($creation['f_creation_date'] == 0) {
                $creation['f_creation_date'] = $time;
            }
            if($creation['f_timestamp'] == 0) {
               $creation['f_timestamp'] =  $time;
            }
            
            $f_order++;
            Zend_Registry::set('f_order',$f_order);
            try {
                $this->db->insert("t_creation_date",$creation);            
                $f_code = $this->db->lastInsertId();
            }catch(Exception $e) {                                
                $dump = Zend_Debug::dump($creation,null,false);
                utilities::reportInsertError("t_creation_date", $e->getMessage(), $dump);                
                die;
            }
            
            $wo_val['f_code'] = $f_code;            
            $wo_val['f_order'] = isset($wo['f_priority']) && $wo['f_priority'] != ''?$wo['f_order']:$f_order;
            $wo_val['f_type_id'] = $f_type_id;
            $wo_val['f_timestamp'] = $time;            
            $wo_val['f_priority'] = isset($wo['f_priority']) && $wo['f_priority'] != ''?$wo['f_priority']:0;     
            $wo_val['f_start_date'] = !isset($wo['f_start_date']) || $wo['f_start_date'] == ''?0:$wo['f_start_date'];
            $wo_val['f_end_date'] = !isset($wo['f_end_date']) || $wo['f_end_date'] == '' ?0:$wo['f_end_date'];;                                    
            $wo_val['f_counter'] = !isset($wo['f_counter'])  || $wo['f_counter'] == ''?1:$wo['f_counter'];            
            $wo_val['f_type_id_periodic'] = $f_type_id_periodic;
            $wo_val['fc_imp_code_wo_excel'] = $wo['f_code'];
            $wo_val['f_category'] = 5;
            unset($wo['f_type_workorder']);            
            try {
                foreach($wo_val as $key_val => &$line_val) {
                    if(in_array($key_val, $time_list)) {
                        if(is_int($line_val)){
                            $line_val = (int)$line_val;
                        }
                        else {
                            $line_val = (int)strtotime($line_val);
                        } 
                    }
                    $line_val = is_null($line_val) ? "" : $line_val;
                    $line_val = utf8_decode($line_val);
                }
                $this->db->insert("t_workorders",$wo_val);
            }
            catch(Exception $e) {                
                $message_dump = Zend_Debug::dump($wo,null,false);
                utilities::reportInsertError("t_workorders", $e->getMessage(), $message_dump);                
                die;
            }

            //carico il custom field
            $custom = array(                
                'f_timestamp'=>!isset($wo['f_timestamp']) || empty($wo['f_timestamp'])?$time:$wo['f_timestamp'],
                'f_code'=>$f_code
            );
            
            foreach($wo as $key => $line) {
                if(!array_key_exists($key, $wo_val)  && !in_array($key,$creation_cols)) { 
                    $not_custom = true;
                    $index_custom = -1;
                    $field = utilities::clean_col_name($key == 'f_parent_code' ? 'fc_imp_parent_code' : $key);
                    //verifico se trovo il custom field orginale fra quelli passati dall'excel.
                    foreach($custom_fields as $key_custom => $line_custom) {
                        if($key == $line_custom['f_title']) {                            
                            $not_custom = false;
                            $index_custom = $key_custom;
                            $field = $line_custom['f_title'];
                            break;
                        }
                    }
                    //verifico che sia coerente con il custom fields passato                    
                    if(!$not_custom) {
                        $type_custom = substr(str_replace(" : ","",$custom_fields[$key_custom]['f_type']),1);
                        switch($type_custom) {
                        case 'BOOLEAN' :$custom[$field] = (int)(bool)$line;                                
                                break;
                            case 'INT' : $custom[$field] = (int)$line;                                
                                break;
                            case 'DATE' : 
                                if(is_int($line)){
                                    $custom[$field] = (int)$line;
                                }
                                else {
                                    $custom[$field] = (int)strtotime($line);
                                }                                
                                break;  
                            case 'VARCHAR' :
                                $max_lenght = ( (int)$custom_fields[$field]['f_column_length']);
                                if(strlen($line) > $max_lenght) {
                                    utilities::reportError("Error : string $line for field $field is too long. Maximum characters allow: $max_lenght");
                                }
                                $custom[$field] = $line;
                                break;
                            default : $custom[$field] = $line;
                                break;
                        }
                    }
                    else {                        
                        if(in_array($field, $time_list)) {                            
                            if(is_int($line)){                                
                                $custom[$field] = (int)$line;
                            }
                            else {                                
                                $custom[$field] = (int)strtotime($line);
                            }    
                        }
                        else {
                            $custom[$field] = $line;
                        }                        
                    }                    
                }
            }        

            foreach($custom as &$line_val) {
                $line_val = is_null($line_val) ? "" : $line_val;
                $line_val = utf8_decode($line_val);
            }
            $custom['f_main_table'] = 't_workorders';
            $custom['f_main_table_type'] = $f_type_id;
            try {
                $this->db->insert("t_custom_fields",$custom);                        
            }catch(Exception $e) {                
                $message_dump = Zend_Debug::dump($custom,null,false);
                utilities::reportInsertError("t_custom_fields", $e->getMessage(), $message_dump);
                die;
            }                        
            $i++;                    
        }catch(Exception $e) {            
            utilities::reportError("unknown error : ".$e->getMessage());die;
        }                
    }
        
    public function completeOnCondition()
    {           
        $this->db = Zend_Registry::get('db');        
        $select = new Zend_Db_Select($this->db);
        $time = Zend_Registry::get('f_timestamp');
        try {         
            $res_oc = $select->from(array("t1"=>"t_workorders"),array(
                'f_code',
                'fc_cond_condition',
                'fc_cond_list_function',
                'fc_cond_code_meter',
                'fc_cond_value_reference',
                'fc_cond_run_operation',
                'fc_cond_operator',
                'fc_cond_average_number_value',
                'fc_cond_action_code'
            ))                
            ->where("t2.f_type_id = 9")->query()->fetchAll();        
        }catch(Exception $e) {
            utilities::reportQueryError("t_workorders", $e->getMessage(), $select->__toString());
            die;
        }        
        foreach($res_oc as $line) {            
            $f_code = $line['f_code'];
            if($line['fc_cond_list_function'] == 'Function') {
                $cond = $line['fc_cond_condition'];
                $matches = array();                                    
                if(preg_match_all("/\w+_\d+_\w{1,6}/", $cond,$matches)) {                                
                    foreach($matches[0] as &$match) {                    
                        $old_match = $match;
                        $match = str_replace("(","",$match);
                        $match = str_replace(")","",$match);
                        $exp_match = explode("_",$match);

                        //il primo definisce la tabella, il secondo il tipo e il terzo il code da cercare
                        $select->reset();
                        $cols_code = "fc_imp_code_%_excel";                        
                        $replace = "";
                        $table = '';
                        switch ($exp_match[0]) {
                            case 'wa' : $replace = "wares";
                                $table = 't_wares';
                                break;
                            case 'wo' : $replace = "wo";
                                $table = 't_workorders';
                                break;
                            case 'sel' : $replace = "sel";
                                $table = 't_selectors';
                                break;
                            default : return;
                        }
                        $cols_code = str_replace("%", $replace, $cols_code);
                        $cols_type = str_replace("%", $replace, $cols_type);
                        try {
                            $res_code = $select->from($table,array("f_code"))->where("$cols_code = ?",$exp_match[2])
                                ->where("f_type_id = ?",$exp_match[1])->query()->fetchAll();
                        }catch(Exception $e) {
                            utilities::reportQueryError("t_workorders", $e->getMessage(), $select->__toString());
                            die;
                        }                    
                        if(!empty($res_code)) {
                            //check if is a meter and create te wo_relation
                            if($replace == 'wo') {
                                $select->reset();
                                $res_check_meter = $select->from("t_workorders",'f_code')
                                        ->where("f_code = ?",$res_code[0]['f_code'])->where("f_type_id = 11")
                                        ->query()->fetchAll();
                                if(!empty($res_check_meter)) {
                                    $this->db->insert("t_wo_relations", array(
                                        'f_code_wo_master'=>$res_code[0]['f_code'],
                                        'f_type_id_wo_master'=>11,
                                        'f_code_wo_slave'=>$f_code,
                                        'f_type_id_wo_slave'=>9,
                                        'f_timestamp'=>$time
                                    ));
                                }
                            }                        
                            $cond = str_replace($old_match,"({$res_code[0]['f_code']})",$cond);                        
                        }
                    }

                    try {                        
                        $this->db->update("t_workorders",array("fc_cond_condition"=>$cond),"f_code = $f_code");
                    }catch(Exception $e) {
                        utilities::reportError("Error occurred during update. this is the error : ".$e->getMessage());
                        die;
                    }
                }
            }
            else {
                $function = 'if(';
                $f_codes_meter = array($line['fc_cond_code_meter']);
                $code_meter = $line['fc_cond_code_meter'];                
                if(empty($code_meter)) continue;                
                $select->reset();
                try{
                    $res_code_meter = $select->from("t_workorders",array("f_code"))
                        ->where("fc_imp_code_wo_excel = ?",$code_meter)->where("f_type_id = 11")->query()->fetchAll();
                }catch(Exception $e) {
                    utilities::reportQueryError("t_workorders", $e->getMessage(), $select->__toString());
                    echo json_encode(array('response'=>'ko'));die;
                }
                
                if(empty($res_code_meter)) continue;
                $code_meter = $res_code_meter[0]['f_code'];
                $val_ref = addslashes($line['fc_cond_value_reference']);
                $operation = utf8_decode($line['fc_cond_run_operation']);
                $cond_operator = '';

                switch($line['fc_cond_operator']) {
                    case 'Equal' : $cond_operator = '==';
                        break;
                    case 'Not equal' : $cond_operator = '!=';
                        break;
                    case 'Greater' : $cond_operator = '>';
                        break;
                    case 'Greater then or equal to' : $cond_operator = '>=';
                        break;
                    case 'Less' : $cond_operator = '<';
                        break;
                    case 'Less than or equal to' : $cond_operator = '<=';
                        break;
                }
                if($line['fc_cond_action_code'] != '') {
                    $select->reset();
                    try {
                        $res_code_act = $select->from("t_wares",array("f_code"))->where("f_type_id = 11")
                            ->where("fc_imp_code_wares_excel = ?",$line['fc_cond_action_code'])
                            ->query()->fetchAll();
                    }catch(Exception $e) {
                        utilities::reportQueryError("t_wares", $e->getMessage(), $select->__toString());
                        echo json_encode(array('response'=>'ko'));die;
                    }
                    if(!empty($res_code_act)) {
                        $operation = "FCODE(".$res_code_act[0]['f_code'].');';
                    }
                   
                }
                if($code_meter == '' || !ctype_digit($code_meter)) {                                    
                    continue;
                }                                
                elseif($val_ref == '') {                                    
                    continue;
                }
                else {
                    switch($line['fc_cond_list_function']) {
                        case 'Meter value' :                                         
                            $function.="METER_VALUE('$code_meter') $cond_operator '$val_ref')\{$operation\}";
                            break;
                        case 'Average' :
                            $avg_num = (int)($line['fc_cond_average_number_value']);                                        
                            if($avg_num == 0) {
                                continue;
                                $function = '';$f_codes_meter = array();
                            }
                            else {
                                $function.="AVG($code_meter,'$avg_num') $cond_operator '$val_ref')\{$operation\}";                                        
                            }
                            break;
                        case 'List' :
                            $function.="IN_LIST($code_meter,\"$val_ref\"))\{$operation\}";
                            break;
                        case 'Range' :
                            $function.="IN_RANGE($code_meter,$val_ref))\{$operation\}";
                            break;
                        case 'Every' :
                            $function.="EVERY($code_meter,$val_ref))\{$operation\}";
                            break;                                    
                        default : 
                            continue;
                            $function = '';$f_codes_meter = array();
                            break;
                    }
                    if($function != '') {
                        $this->db->update("t_workorders",array("fc_cond_condition"=>$function),"f_code = {$line['f_code']}");
                    }
                }
                
                if($function != '') {                    
                    foreach($f_codes_meter as $code_meter) {
                        $select->reset();
                        try {
                        $res_check = $select->from("t_workorders",array('f_code'))->where("f_type_id = 11")
                                ->where("f_code = ?",$code_meter)->query()->fetchAll();
                        }catch(Exception $e) {
                            utilities::reportQueryError("t_workorders", $e->getMessage(), $select->__toString());
                            echo json_encode(array('response'=>'ko'));die;
                        }
                        if(empty($res_check)) continue;
                        $this->db->insert("t_wo_relations",array(
                            'f_code_wo_master'=>$code_meter,
                            'f_type_id_wo_master'=>11,
                            'f_code_wo_slave'=>$f_code,
                            'f_type_id_wo_slave'=>9,                            
                            "f_timestamp"=>$time
                            )
                        );
                    }                    
                }
            }
        }        
    }
    
    /**
     * Create a pair cross 
     * between workorders and wares
     * @param type $data
     * @param type $custom_fields 
     */
    public function insertPairCross($line) 
    {        
        $this->db = Zend_Registry::get('db');
        $time = Zend_Registry::get('f_timestamp');
        
        $select = new Zend_Db_Select($this->db);        
        $time_list = array(
            "fc_asset_downtime_since", 	
            "fc_asset_downtime_till",
            "fc_inv_moving_date",
            "fc_rsc_since",
            "fc_rsc_till",
            "fc_tool_since",
            "fc_tool_till",
            "fc_vendor_date",
            "fc_task_downtime_since",
            "fc_task_downtime_till"
        );
                
        //lists of excel's codes
        $fcodes_main_excel = array();
        $fcodes_cross_excel = array();
        $group_codes = array();
        
        //lists of mainsim's codes
        $fcodes_main = array();
        $fcodes_cross = array();
        $fcodes_group = array();
        
        /**
         * @TODO da finire assolutamente
         */
        
        //get code main
        if(!empty($line['f_type_main']) && !empty($line['f_code_main'])){             
            $type = substr($line['f_type_main'], 0,  strpos($line['f_type_main'], " :"));
            $fcodes_main_excel[$type][] = "'{$line['f_code_main']}'";
        }
        //get code cross
        if(!empty($line['f_code_cross']) && !empty($line['f_type_cross'])) {            
            $type = substr($line['f_type_cross'], 0,  strpos($line['f_type_cross'], " :"));
            $fcodes_cross_excel[$type][] = "'{$line['f_code_cross']}'";
        }
        //get group_code
        if(!empty($line['f_group_code'])) {
            $group_codes[] = "'{$line['f_group_code']}'";
        }        
        
        foreach($fcodes_main_excel as $type => $codes) {
            $select->reset();
            $code_str = implode(',',$codes);
            $res_true_code = $select->from("t_workorders",array("f_code","fc_imp_code_wo_excel"))->where("f_type_id = ?",$type)
                    ->where("fc_imp_code_wo_excel IN ($code_str)")->query()->fetchAll();
            foreach($res_true_code as $true_code) {
                $fcodes_main[$type][$true_code['fc_imp_code_wo_excel']] = $true_code['f_code'];
            }                        
        }
        
        foreach($fcodes_cross_excel as $type => $codes) {
            $select->reset();
            $code_str = implode(',',$codes);
            $res_true_code = $select->from("t_wares",array("f_code","fc_imp_code_wares_excel"))->where("f_type_id = ?",$type)
                    ->where("fc_imp_code_wares_excel IN ($code_str)")->query()->fetchAll();
            foreach($res_true_code as $true_code) {
                $fcodes_cross[$type][$true_code['fc_imp_code_wares_excel']] = $true_code['f_code'];                
            }                        
        }
        
        $select->reset();
        if(!empty($group_codes)) {
            $code_str = implode(',',$group_codes);
            $res_true_group = $select->from("t_wares",array("f_code","fc_imp_code_wares_excel"))->where("f_type_id = 1")
                        ->where("fc_imp_code_wares_excel IN ($code_str)")->query()->fetchAll();
            foreach($res_true_group as $true_code) {
                $fcodes_group[$true_code['fc_imp_code_wares_excel']] = $true_code['f_code'];
            } 
        }

        $pair_cross = array();
        if(empty($line['f_type_main']) || empty($line['f_code_main']) || empty($line['f_code_cross']) || empty($line['f_type_cross'])) return;                            
        $type_main = substr($line['f_type_main'], 0,  strpos($line['f_type_main'], " :"));
        $type_cross = substr($line['f_type_cross'], 0,  strpos($line['f_type_cross'], " :"));
        $select->reset();            

        if(!isset($fcodes_main[$type_main][$line['f_code_main']]) || !isset($fcodes_cross[$type_cross][$line['f_code_cross']]))return;

        $pair_cross['f_code_main'] = $fcodes_main[$type_main][$line['f_code_main']];
        $pair_cross['f_code_cross'] = $fcodes_cross[$type_cross][$line['f_code_cross']];

        if(!empty($line['f_group_code']) && isset($fcodes_group[$line['f_group_code']])) {
            $pair_cross['f_group_code'] = $fcodes_group[$line['f_group_code']];                
            $select->reset();
            $res_wa_wo = $select->from("t_ware_wo",array('num'=>'count(*)'))
                    ->where("f_ware_id = ?",$pair_cross['f_group_code'])
                    ->where("f_wo_id = ?",$pair_cross['f_code_main'])
                    ->query()->fetchAll();
            if(!$res_wa_wo[0]['num']) {
                try {
                    $ware_wo = array(
                        "f_ware_id"=>$pair_cross['f_code_cross'],
                        "f_wo_id"=>$pair_cross['f_code_main'],
                        "f_level"=>0,
                        "f_timestamp"=>$time
                    );
                    $this->db->insert("t_ware_wo",$ware_wo);
                }catch(Exception $e) {                    
                    $message_dump = Zend_Debug::dump($ware_wo,null,false);
                    utilities::reportInsertError("t_ware_wo", $e->getMessage(), $message_dump);
                    die;
                }
            }
        }            

        $select->reset();
        $res_wa_wo = $select->from("t_ware_wo",array('num'=>'count(*)'))
                ->where("f_ware_id = ?",$pair_cross['f_code_cross'])
                ->where("f_wo_id = ?",$pair_cross['f_code_main'])
                ->query()->fetchAll();
        if(!$res_wa_wo[0]['num']) {
            try {
                $ware_wo = array(
                    "f_ware_id"=>$pair_cross['f_code_cross'],
                    "f_wo_id"=>$pair_cross['f_code_main'],
                    "f_level"=>0,
                    "f_timestamp"=>$time
                );
                $this->db->insert("t_ware_wo",$ware_wo);
            }catch(Exception $e) {                    
                $message_dump = Zend_Debug::dump($ware_wo,null,false);
                utilities::reportInsertError("t_ware_wo", $e->getMessage(), $message_dump);
                die;
            }
        }
        
        $pair_cross['f_timestamp'] = $time;

        $black_list = array('f_type_main','f_type_cross','f_code');

        foreach($line as $key => &$value) {
            if(!isset($pair_cross[$key]) && !in_array($key,$black_list)) {
                if(is_null($value)) $value = "";
                if(in_array($key,$time_list)) $value = strtotime ($value);
                if(strpos($key,'/') !== false && strtotime($value) != 0) $value = strtotime ($value);
                $pair_cross[$key] = utf8_decode($value);
            }
        }      
        try {
            $this->db->insert("t_pair_cross",$pair_cross);            
        }catch(Exception $e) {
            $message_dump = Zend_Debug::dump($pair_cross,null,false);
            utilities::reportInsertError("t_pair_cross", $e->getMessage(), $message_dump);
            die;
        }
    }
    
    public function addNextDueDate($f_code = 0)
    {
        if(!$f_code) return;        
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);                
        //search the first due date and subsequent due date        
        try {
            $res_due = $select->from("t_periodics",array("f_start_date"))->where("f_code = ?",$f_code)->where("f_executed = 0")
                ->where("f_start_date > ?",time())->order("f_start_date ASC")->limit(2)->query()->fetchAll();
        }catch(Exception $e) {                
            utilities::reportQueryError("t_periodics",$e->getMessage(),$select->__toString());
            die;  
        };
        $custom = array();            
        if(!empty($res_due)) {
            $custom['fc_pm_next_due_date'] = $res_due[0]['f_start_date'];
            if(isset($res_due[1])) {
                $custom['fc_pm_subsequent_due_date'] = $res_due[1]['f_start_date'];
            }
            try {
                $this->db->update("t_workorders",$custom,"f_code = {$f_code}");
            }catch(Exception $e) {
                utilities::reportError("<div>Error occurred during update in t_workorders. This is the error : ".$e->getMessage()."</div>");
                die;
            }
        }        
        $this->db->closeConnection();
    }
    
    public function addAssetField($f_type_id)
    {        
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);
        
        try {
            $res_pm = $select->from("t_workorders",array("f_code"))->where("f_type_id = ?",$f_type_id)->query()->fetchAll();
        }catch(Exception $e) {            
            utilities::reportQueryError("t_workorders",$e->getMessage(),$select->__toString());
            die;
        };
        $tot = count($res_pm);
        for($i = 0; $i < $tot; ++$i) {
            $code_pm = $res_pm[$i];
            $asset_hierarchies = array();
            $custom_fields = array();
            //search wares asset crossed with pm
            $select->reset();
            try {
                $res_asset = $select->from(array("t1"=>"t_wares"),array("f_code"))
                    ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array("f_title"))
                    ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id")                         
                    ->where("t2.f_wo_id = ?",$code_pm['f_code'])
                    ->where("t1.f_type_id = 1")
                    ->query()->fetchAll();
            }catch(Exception $e) {
                utilities::reportQueryError("t_wares", $e->getMessage(), $select->__toString());
                die;
            }
            $tot1 = count($res_asset);
            for($i1 = 0; $i1 < $tot1;++$i1) {                
                $f_code_wares_1 = $res_asset[$i1];
                //search asset hierarchy
                $select->reset();
                try {
                    $res_hy = $select->from("t_creation_date",array('fc_hierarchy'))
                                ->where("f_id = ?",$f_code_wares_1['f_code'])->query()->fetch();
                }catch(Exception $e) {
                    utilities::reportQueryError("t_creation_date", $e->getMessage(), $select->__toString());
                    die;
                }
                if(!empty($res_hy)) {
                    $asset_hierarchies[$f_code_wares_1['f_title']] = $res_hy['fc_hierarchy'];
                }                
            }
            $new_fc_asset_hy = array();
            foreach ($asset_hierarchies as $key_hy =>$value_hy) {                            
                $new_fc_asset_hy[] = !empty($value_hy)?"$key_hy ($value_hy)":"$key_hy";
            }
            $custom_fields['fc_wo_asset'] = implode(", ",$new_fc_asset_hy);
            
            try {
                $this->db->update("t_workorders",$custom_fields,"f_code = {$code_pm['f_code']}");
            }catch(Exception $e) {
                utilities::reportError("Error occurred during an update in t_custom_fields. this is the error : ".$e->getMessage());
                die;
            } 
        }
        $this->db->closeConnection();
    }
    
    public function pm_frequency($f_code)
    {        
        
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);
        $res_pm = $select->from(array("t1"=>"t_workorders"),array("f_code"))
                ->join(array("t2"=>"t_pair_cross"),"t1.f_code = t2.f_code_main",array("fc_pm_periodic_type","fc_pm_sequence","fc_pm_cyclic_number","fc_pm_cyclic_type"))
                ->where("t1.f_code = $f_code")->query()->fetch();                
        $line = $res_pm;
        $frequency = '';
        switch($line['fc_pm_periodic_type']){
            case 0: //weekly
                $seq = explode(',',$line['fc_pm_sequence']);
                $tot_day = count($seq);
                if($tot_day == 1) $frequency = 'WEEKLY';
                elseif($tot_day == 2) $frequency = 'BIWEEKLY';                            
                else $frequency = $tot_day.' TIMES A WEEK';
                break;
            case 1: //monthly
                switch($line['fc_pm_cyclic_type']) {                        
                    case 4 : //year
                        if($line['fc_pm_cyclic_number'] == 1) $frequency = 'ANNUAL';
                        elseif($line['fc_pm_cyclic_number'] == 2) $frequency = 'BIANNUAL';                            
                        else $frequency = $line['fc_pm_cyclic_number'].' YEARS';
                        break;
                }
                break;
            case 2: //periodic
                switch($line['fc_pm_cyclic_type']) {
                    case 1: //day
                        if($line['fc_pm_cyclic_number'] == 1) $frequency = 'DAILY';                            
                        else $frequency = "EVERY ".$line['fc_pm_cyclic_number'].' DAYS';
                        break;                        
                    case 2: //week
                        $frequency = $line['fc_pm_cyclic_number'].' WEEK';
                        break;
                    case 3: //month
                        if($line['fc_pm_cyclic_number'] == 1) $frequency = 'MONTHLY';
                        elseif($line['fc_pm_cyclic_number'] == 1) $frequency = 'BIMESTRAL';
                        elseif($line['fc_pm_cyclic_number'] == 1) $frequency = 'TRIMESTRAL';
                        elseif($line['fc_pm_cyclic_number'] == 3) $frequency = 'QUATERLY';
                        elseif($line['fc_pm_cyclic_number'] == 6) $frequency = 'HALF-YEARLY';
                        else $frequency = $line['fc_pm_cyclic_number'].' MONTHS';
                        break;
                    case 4: //year                            
                        if($line['fc_pm_cyclic_number'] == 1) $frequency = 'ANNUAL';
                        elseif($line['fc_pm_cyclic_number'] == 2) $frequency = 'BIANNUAL';                            
                        else $frequency = $line['fc_pm_cyclic_number'].' YEARS';
                        break;
                }
                break;

        }            
        $this->db->update("t_workorders",array("fc_pm_frequency"=>$frequency),"f_code = {$line['f_code']}");                    
        $this->db->closeConnection();
    }
    
    public function wares_opened_flag()
    {
        $this->db = Zend_Registry::get("db");
        $asset_fields = array(
            1=>array(1=>'fc_asset_opened_wo'),
            3=>array(1=>'fc_asset_opened_pm'),
            9=>array(1=>'fc_asset_opened_oc'),
            15=>array(1=>'fc_asset_opened_std')
        );   
        $wares_fields = array(
            1=>array(2=>"fc_wo_rsc",3=>"fc_wo_inventory",4=>"fc_wo_tool"),
            3=>array(2=>"fc_wo_rsc",3=>"fc_wo_inventory",4=>"fc_wo_tool"),
            9=>array(2=>"fc_wo_rsc",3=>"fc_wo_inventory",4=>"fc_wo_tool"),
            15=>array(2=>"fc_wo_rsc",3=>"fc_wo_inventory",4=>"fc_wo_tool")
        );   
        $select = new Zend_Db_Select($this->db);
        //fill wares fields with number of wo associates
        foreach($asset_fields as $wo_type => $fields) {
            foreach($fields as $wa_type => $field) {
                //check how many corrective wo are opened on this ware
                $select->reset();
                $res_pm = $select->from(array("t1"=>"t_wares"),array('f_code','num'=>'count(*)'))                
                    ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())                
                    ->join(array("t3"=>"t_creation_date"),"t2.f_wo_id = t3.f_id",array())
                    ->join(array("t6"=>"t_workorders"),"t3.f_id = t6.f_code",array())
                    ->join(array("t4"=>"t_wf_phases"),"t3.f_phase_id = t4.f_number and t3.f_wf_id = t4.f_wf_id",array())
                    ->join(array("t5"=>"t_wf_groups"),"t5.f_id = t4.f_group_id",array())
                    ->where("t6.f_type_id = {$wo_type}")
                    ->where("t1.f_type_id = $wa_type")                
                    ->where("t5.f_visibility != 0 ")->group("t1.f_code")->having("num > 0")
                    ->query()->fetchAll();
                    //echo count($res_pm);die;
                foreach($res_pm as $line) {
                    $select->reset();                        
                    $wo_asset = $line['num'];                    
                    $this->db->update("t_wares",array($field=>$wo_asset), "f_code = {$line['f_code']}");                                
                }
            }
        }
        //fill wo fields with number of wares associates
        foreach($wares_fields as $wo_type => $fields) {
            foreach($fields as $wa_type => $field) {
                //check how many corrective wo are opened on this ware
                $select->reset();
                $res_pm = $select->from(array("t1"=>"t_wares"),array())                
                    ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())                
                    ->join(array("t3"=>"t_creation_date"),"t2.f_wo_id = t3.f_id",array())
                    ->join(array("t6"=>"t_workorders"),"t3.f_id = t6.f_code",array('f_code','num'=>'count(*)'))
                    ->join(array("t4"=>"t_wf_phases"),"t3.f_phase_id = t4.f_number and t3.f_wf_id = t4.f_wf_id",array())
                    ->join(array("t5"=>"t_wf_groups"),"t5.f_id = t4.f_group_id",array())
                    ->where("t6.f_type_id = {$wo_type}")
                    ->where("t1.f_type_id = $wa_type")                
                    ->where("t5.f_visibility != 0 ")
                    ->group("t6.f_code")->having("num > 0")
                    ->query()->fetchAll();
                foreach($res_pm as $line) {
                    $select->reset();                        
                    $wo_asset = $line['num'];
                    $this->db->update("t_workorders",array($field=>$wo_asset), "f_code = {$line['f_code']}");            
                }
            }
        }
        $this->db->closeConnection();
    }
    
    /**
     * system to cross wares and workorders     
     */    
    public function crossWaresWorkorders($crosses = array())
    {           
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);    
        $time = Zend_Registry::get('f_timestamp');
        $wa = array();
        $wo = array();
        $tot_a = count($crosses);
        for($a = 0;$a < $tot_a;++$a) {
            if($crosses[$a]['f_code_wares'] == '' || $crosses[$a]['f_type_id_wares'] == ''
                || $crosses[$a]['f_code_workorder'] == '' || $crosses[$a]['f_type_id_workorder'] == '') continue;
            $type_wo = substr($crosses[$a]['f_type_id_workorder'], 0,  strpos($crosses[$a]['f_type_id_workorder'], " :"));
            $type_wa = substr($crosses[$a]['f_type_id_wares'], 0,  strpos($crosses[$a]['f_type_id_wares'], " :"));
            $wa[$type_wa][] = "'{$crosses[$a]['f_code_wares']}'";
            $wo[$type_wo][] = "'{$crosses[$a]['f_code_workorder']}'";                        
        }
        
        $type_wa_keys = array_keys($wa);
        $type_wo_keys = array_keys($wo);        
        //get true codes of wares
        $tot_b = count($type_wa_keys);
        for($b = 0;$b < $tot_b;++$b) {
            $key = $type_wa_keys[$b];            
            $wa_list = implode(',',$wa[$key]);
            if(empty($wa_list)) continue;                        
            $wa[$key] = array();
            $select->reset();
            $res_true_wa = $select->from("t_wares",array("f_code","fc_imp_code_wares_excel"))
                    ->where("f_type_id = ?",$key)->where("fc_imp_code_wares_excel IN ($wa_list)")
                    ->query()->fetchAll();
            $tot_c = count($res_true_wa);
            for($c = 0;$c < $tot_c;++$c) {
                $wa[$key][$res_true_wa[$c]['fc_imp_code_wares_excel']] = (int)$res_true_wa[$c]['f_code'];
            }
        }
        
        //get true codes of wos
        $tot_b = count($type_wo_keys);
        for($b = 0;$b < $tot_b;++$b) {
            $key = $type_wo_keys[$b];
            $wo_list = implode(',',$wo[$key]);
            if(empty($wo_list)) continue;            
            $wo[$key] = array();
            $select->reset();
            $res_true_wo = $select->from("t_workorders",array("f_code","fc_imp_code_wo_excel"))
                    ->where("f_type_id = ?",$key)->where("fc_imp_code_wo_excel IN ($wo_list)")
                    ->query()->fetchAll();
            $tot_c = count($res_true_wo);
            for($c = 0;$c < $tot_c;++$c) {
                $wo[$key][$res_true_wo[$c]['fc_imp_code_wo_excel']] = (int)$res_true_wo[$c]['f_code'];
            }
        }
        
        //insert cross
        $tot_b = count($crosses);
        for($b = 0;$b < $tot_b;++$b) {
            $line = $crosses[$b];
            if($line['f_code_wares'] == '' || $line['f_type_id_wares'] == '' || $line['f_code_workorder'] == '' || $line['f_type_id_workorder'] == '') continue;
            $type_wo = substr($line['f_type_id_workorder'], 0,  strpos($line['f_type_id_workorder'], " :"));
            $type_wa = substr($line['f_type_id_wares'], 0,  strpos($line['f_type_id_wares'], " :"));
            if(!isset($wa[$type_wa][$line['f_code_wares']]) || !isset($wo[$type_wo][$line['f_code_workorder']])) continue;
            $fcodewa = $wa[$type_wa][$line['f_code_wares']];
            $fcodewo = $wo[$type_wo][$line['f_code_workorder']];
            $select->reset();
            $check = $select->from("t_ware_wo",array("num"=>"count(*)"))->where("f_ware_id = ?",$fcodewa)
                    ->where("f_wo_id = ?",$fcodewo)->query()->fetch();
            if($check['num'] > 0) continue;
            //insert cross
            $this->db->insert("t_ware_wo",array(
                'f_ware_id'=>$fcodewa,
                'f_wo_id'=>$fcodewo,            
                'f_timestamp'=>  $time
            ));                        
        }
    }
    
    /**
     * fill information about asset cross with wo 
     */
    public function wo_asset()
    {
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);                
        
        $res_wo_ware = $select->from(array("t1"=>"t_ware_wo"),array())
                ->join(array("t2"=>"t_workorders"),"t1.f_wo_id = t2.f_code",array("f_code_wo"=>"DISTINCT(t2.f_code)"))
                ->join(array("t3"=>"t_wares"),"t1.f_ware_id = t3.f_code",array())
                ->where("t3.f_type_id = 1")
                ->query()->fetchAll();        
        $tot_a = count($res_wo_ware);
        for($a = 0;$a < $tot_a;++$a) {
            $select->reset();
            $wo_asset = array();
            $res_ware = $select->from(array("t1"=>"t_ware_wo"),array()) 
                ->join(array("t2"=>"t_creation_date"),"t1.f_ware_id = t2.f_id",array("f_title","fc_hierarchy"))
                ->join(array("t3"=>"t_wares"),"t1.f_ware_id = t3.f_code",array())
                ->where("t1.f_wo_id = ?",$res_wo_ware[$a]['f_code_wo'])->where("t3.f_type_id = 1")
                ->query()->fetchAll();
            $tot_b = count($res_ware);
            for($b = 0; $b < $tot_b; ++$b) {
                $wo_asset[$b] = $res_ware[$b]['f_title'];
                $wo_asset[$b] .= !empty($res_ware[$b]['fc_hierarchy'])?"({$res_ware[$b]['fc_hierarchy']})":"";
            }
            $wo_asset = implode(", ", $wo_asset);
            $this->db->update("t_workorders",array("fc_wo_asset"=>$wo_asset),"f_code = ".$res_wo_ware[$a]['f_code_wo']);
        }        
    }
    
    /**
     *Set empty workorders with wo of asset 
     */
    public function selectorWoFromAsset()
    {
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);                
        $res_wo = $select->from(array("t1"=>"t_ware_wo"),"f_wo_id")
                ->join(array("t2"=>"t_wares"),"f_code = f_ware_id",array('f_code'))                
                ->where("t2.f_type_id = 1")->where("not exists(select * from t_selector_wo where f_wo_id = t1.f_wo_id)")
                ->query()->fetchAll();
        $tot_a = count($res_wo);
        for($a = 0;$a < $tot_a;++$a) {            
            $wares_list = implode(',',utilities::getParentCode("t_wares_parent",$res_wo[$a]['f_code'],array($res_wo[$a]['f_code'])));
            $select->reset();
            $res_sel = $select->from("t_selector_ware",array("f_selector_id","f_timestamp"))->where("f_ware_id IN ($wares_list)")
                    ->query()->fetchAll();
            $tot_b = count($res_sel);
            for($b = 0;$b < $tot_b;++$b){
                $this->db->insert("t_selector_wo",array(
                    'f_wo_id'=>$res_wo[$a]['f_wo_id'],
                    'f_selector_id'=>$res_sel[$b]['f_selector_id'],
                    'f_timestamp'=>$res_sel[$b]['f_timestamp']
                ));                
            }            
        }
    }
}