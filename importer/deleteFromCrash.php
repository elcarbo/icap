<?php

/**
 *  Start a rollback in case of disaster from importer 
 */

require_once 'constants.php';
require_once 'utilities.php';

if(isset($_GET['id'])) {    
    $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
    $db = Zend_Db::factory($config->database);          
    $select = new Zend_Db_Select($db);
    $res = $select->from("t_queue",array("f_rollback","f_file_name"))->where("f_id = ?",$_GET['id'])->query()->fetchAll();
    if(!empty($res)) {        
        $rollback = unserialize($res[0]['f_rollback']);
        utilities::deleteDataInserted($_GET['id'],$rollback);         
        if(file_exists("uploads/".$res[0]['f_file_name'])) unlink("uploads/".$res[0]['f_file_name']);
        $db->delete("t_queue","f_id = {$_GET['id']}");
    }
}