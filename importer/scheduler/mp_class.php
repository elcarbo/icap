<?php

class mp_class
{
    private $db,$id_excel; 
    
    public function __construct($id_excel = 0) {        
        $this->id_excel = $id_excel;
    }
        
    /**
     * Gestione Eccezioni 
     */
    private function exception_groups($group,$date)
    {
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $this->db = Zend_Db::factory($config->database);
        
        $select = new Zend_Db_Select($this->db);
        $not_found = true;
        $res_exceptions =$select->from("t_groups_exceptions",array())
                ->join("t_days_exceptions","t_days_exceptions.f_code = t_groups_exceptions.f_id")
                ->where("t_groups_exceptions.f_id = ?",$group)->query()->fetchAll();
        $select->reset();
        //Se il giorno corrisponde a un eccezione, quella manutenzione non viene eseguita
        foreach($res_exceptions as $day_ex) {
            $not_day = true;$not_month = true;$not_year = true;            
            if(date('d',$date) == $day_ex['f_day_exception'] || empty($day_ex['f_day_exception'])) $not_day = false;
            if(date('m',$date) == $day_ex['f_month_exception'] || empty($day_ex['f_month_exception'])) $not_month = false;
            if(date('Y',$date) == $day_ex['f_year_exception'] || empty($day_ex['f_year_exception'])) $not_year = false;
            if(!$not_day && !$not_month && !$not_year) {
                $not_found = false;
                break;
            }            
        }
        $this->db->closeConnection();
        return $not_found;
    }
    
    public function weekly($periodic,$f_code_wo,$fake = false,$line_request = 0)
    {
        $counter_round = 0;
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $this->db = Zend_Db::factory($config->database);
        
        $select = new Zend_Db_Select($this->db);
        $start_date = $periodic['fc_pm_start_date'];
        $end_date = $periodic['fc_pm_end_date'];    
        $dow = date('N',$start_date);
        $start_week = mktime(date('H',$start_date),date('i',$start_date),date('s',$start_date),
                date('n',$start_date),date('j',$start_date) - ($dow - 1),date('Y',$start_date));
        
        $increment = $start_date;
        
        $sequence = str_replace(".", ",", $periodic['fc_pm_sequence']);        
        
        $sequence = explode(',',$sequence);
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic = explode(' : ',$periodic['fc_pm_cyclic_type']);
        $fc_pm_rules_dayweek = explode(',',$periodic['fc_pm_rules_dayweek']);   
        $check_fake_date = array();
        while($increment <= $end_date) {                          
            if($counter_round > 5000) {
                utilities::reportError("Something went wrong during the creation of you calendar : the request at line $line_request created 5000 periodics.", $this->id_excel);
                die;
            }
            foreach($sequence as $dow) {                                 
                while(date('N',$increment) < $dow) {
                    $increment = $this->increment("day", $increment, 1);
                }
                
                if($increment > $end_date) break;
                
                if(date('N',$increment) > $dow) {
                    continue;
                }                
                //verifico se il giorno in questione sia tra quelli in eccezione
                if(!empty($periodic['fc_pm_exception_group'])) {
                    $not_found = $this->exception_groups($periodic['fc_pm_exception_group'],$increment);
                    if(!$not_found) {
                        $increment = $this->increment("day", $increment, 1);                        
                        continue;
                    }
                }                
                //verifico che non ci siano eccezioni da gestire
                elseif(!empty($periodic['fc_pm_rules_exception_group'])) {
                    $not_found = $this->exception_groups($periodic['fc_pm_rules_exception_group'],$increment);
                    if(!$not_found) { //GESTISCO IL GIORNO SECONDO PROGRAMMA                        
                        $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$periodic['fc_pm_rules_exception_group']);                
                        if($mp_correct_day) {  
                            $select->reset();
                            $res_exist = $select->from("t_periodics")->where("f_code = ?",$f_code_wo)->where("f_start_date = ?",$increment + $periodic['fc_pm_at_time'])->query()->fetchAll();
                            if(!empty($res_exist)) {
                                $increment = $this->increment("day", $increment,1);
                                continue;
                            }
                            if(!$fake) {
                                if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;       
                                $this->createPeriodics($periodic, $f_code_wo, ($mp_correct_day + $periodic['fc_pm_at_time']), ($mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']));            
                                $counter_round++;
                            }
                            else {
                                if(in_array($increment,$check_fake_date)) {
                                    die("$increment still created");
                                }
                                $check_fake_date[] = $increment;
                                echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                            }
                        }
                        $increment = $this->increment("day", $increment,1);                        
                        unset($mp_correct_day);
                        continue;
                    }
                }
                //verifico se cade in un giorno della settimana non permesso e se cosÃ¬, seguo le regole per gestire.
                if(!in_array(date('N',$increment),$fc_pm_rules_dayweek) && strpos($periodic['fc_pm_rules_type_dayweek'],'always') === false) {                  
                    $group = false;
                    if(!empty($periodic['fc_pm_exception_group'])) $group = $periodic['fc_pm_exception_group'];
                    elseif(!empty($periodic['fc_pm_rules_exception_group'])) $group = $periodic['fc_pm_rules_exception_group'];

                    $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$group);                
                    if($mp_correct_day) {   
                        $select->reset();
                        $res_exist = $select->from("t_periodics")->where("f_code = ?",$f_code_wo)->where("f_start_date = ?",$increment + $periodic['fc_pm_at_time'])->query()->fetchAll();
                        if(!empty($res_exist)) {
                            echo $increment.' già presente 1';die;
                        }
                        if(!$fake) {
                            if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;
                            $this->createPeriodics($periodic, $f_code_wo, ($mp_correct_day + $periodic['fc_pm_at_time']), ($mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']));            
                            $counter_round++;
                        }
                        else {
                            if(in_array($increment,$check_fake_date)) {
                                die("$increment still created");
                            }
                            $check_fake_date[] = $increment;
                            echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                        }
                    }
                    $increment = $this->increment("day", $increment,$num_cyclic);                    
                    unset($mp_correct_day);
                    continue;
                }
                $select->reset();
                $res_exist = $select->from("t_periodics")->where("f_code = ?",$f_code_wo)->where("f_start_date = ?",$increment + $periodic['fc_pm_at_time'])->query()->fetchAll();
                if(!empty($res_exist)) {
                    echo $increment.' già presente 2';die;
                }
                if(!$fake) {
                    if(($increment + $periodic['fc_pm_at_time']) > $end_date) return;
                    $this->createPeriodics($periodic, $f_code_wo,($increment + $periodic['fc_pm_at_time']),($increment + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']));            
                    $counter_round++;
                }
                else {
                    if(in_array($increment,$check_fake_date)) {
                        die("$increment still created");
                    }
                    $check_fake_date[] = $increment;
                    echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                }
                //Incremento di un giorno
                $increment = $this->increment("day", $increment, 1);                   
            }            
            //finiti i giorni della settimana, verifico di quanto incrementare il contatore (settimane,mesi,anni)
            $start_week = $this->increment('week', $start_week,$num_cyclic);
            $increment = $start_week;
        }
        $this->db->closeConnection();
    }
    
    public function monthly($periodic,$f_code_wo,$fake = false,$line_request = 0)
    {      
        $counter_round = 0;
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $this->db = Zend_Db::factory($config->database);
        
        $start_date = $periodic['fc_pm_start_date'];
        $end_date = $periodic['fc_pm_end_date'];
        $increment = $start_date;
        
        $sequence = str_replace(".", ",", $periodic['fc_pm_sequence']);        
        $sequence = explode(',',$sequence);        
        if(empty($sequence)) {
            $sequence[0] = date('m',$start_date);
        }
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic = explode(' : ',$periodic['fc_pm_cyclic_type']);
        $fc_pm_rules_dayweek = explode(',',$periodic['fc_pm_rules_dayweek']);
        $check_fake_date = array();
        foreach($sequence  as $month) {            
            if($counter_round > 5000) {
                utilities::reportError("Something went wrong during the creation of you calendar : the request at line $line_request created 5000 periodics.", $this->id_excel);
                die;
            }
            if($month != "") {
                $increment = mktime(date('H',$start_date),date('i',$start_date),date('s',$start_date),$month,date('j',$start_date),date('Y',$start_date));
            }
            else {
                $increment = $start_date;
            }
            
            //se la prima data Ã¨ minore dello start date , la porto avanti.
            if($increment < $start_date) {            
                $increment = $this->increment ($type_cyclic[1], $increment, $num_cyclic);
            }
            
            while($increment <= $end_date) {
                //verifico se il giorno in questione sia tra quelli in eccezione
                if(!empty($periodic['fc_pm_exception_group'])) {
                    $not_found = $this->exception_groups($periodic['fc_pm_exception_group'],$increment);
                    if(!$not_found) {
                        $increment = $this->increment($type_cyclic[1], $increment, $num_cyclic);                        
                        continue;
                    }
                }                
                //verifico che non ci siano eccezioni da gestire
                elseif(!empty($periodic['fc_pm_rules_exception_group'])) {
                    $not_found = $this->exception_groups($periodic['fc_pm_rules_exception_group'],$increment);
                    if(!$not_found) { //GESTISCO IL GIORNO SECONDO PROGRAMMA                        
                        $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$periodic['fc_pm_rules_exception_group']);                
                        if($mp_correct_day) {
                            if(!$fake) {
                                if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;
                                $this->createPeriodics($periodic, $f_code_wo, $mp_correct_day + $periodic['fc_pm_at_time'], $mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                                $counter_round++;
                            }
                            else {
                                if(in_array($increment,$check_fake_date)) {
                                    die("$increment still created");
                                }
                                $check_fake_date[] = $increment;
                                echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                            }
                        }
                        $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);                        
                        unset($mp_correct_day);
                        continue;
                    }
                }                
                //verifico se cade in un giorno della settimana non permesso e se cosÃ¬, seguo le regole per gestire.
                if(!in_array(date('N',$increment),$fc_pm_rules_dayweek) && strpos($periodic['fc_pm_rules_type_dayweek'],'always') === false) {
                    $group = false;
                    if(!empty($periodic['fc_pm_exception_group'])) $group = $periodic['fc_pm_exception_group'];
                    elseif(!empty($periodic['fc_pm_rules_exception_group'])) $group = $periodic['fc_pm_rules_exception_group'];
                    
                    $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $fc_pm_rules_dayweek,$group);                
                    if($mp_correct_day) {
                        if(!$fake) {
                            if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;
                            $this->createPeriodics($periodic, $f_code_wo, $mp_correct_day + $periodic['fc_pm_at_time'], $mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);
                            $counter_round++;
                        }
                        else {
                            if(in_array($increment,$check_fake_date)) {
                                die("$increment still created");
                            }
                            $check_fake_date[] = $increment;
                            echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                        }                        
                    }
                    $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);                    
                    unset($mp_correct_day);
                    continue;
                }         
                if(!$fake) {
                    if(($increment + $periodic['fc_pm_at_time']) > $end_date) continue;
                    $this->createPeriodics($periodic, $f_code_wo, $increment + $periodic['fc_pm_at_time'], $increment + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                    $counter_round++;
                }
                else {
                    if(in_array($increment,$check_fake_date)) {
                        die("$increment still created");
                    }
                    $check_fake_date[] = $increment;
                    echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                }                        
                
                $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);                 
            }
        }  
        $this->db->closeConnection();
    }    
    
    public function dayofthemonth($periodic,$f_code_wo,$fake = false,$line_request = 0)
    {
        //check if number of round is over 5000
        $counter_round = 0;
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $this->db = Zend_Db::factory($config->database);
        
        $start_date = $periodic['fc_pm_start_date'];
        $end_date = $periodic['fc_pm_end_date'];  
        $fc_pm_sequence = str_replace('.', ',', $periodic['fc_pm_sequence']);
        $dom_array = explode(',',$fc_pm_sequence);
        
        $dow = explode(',',$periodic['fc_pm_rules_dayweek']);
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic = explode(' : ',$periodic['fc_pm_cyclic_type']);
        $fc_pm_forewarning = isset($periodic['fc_pm_forewarning']) && !is_null($periodic['fc_pm_forewarning'])?$periodic['fc_pm_forewarning']:0;
        
        //verifico se il giorno inizio manutenzione Ã¨ uguale al DoM
        foreach($dom_array as $dom) {            
            if($counter_round > 5000) {
                utilities::reportError("Something went wrong during the creation of you calendar : the request at line $line_request created 5000 periodics.", $this->id_excel);
                die;
            }
            $dom_filtered = $dom;
            if($dom_filtered == 'last') {
                $dom_filtered = date('j',mktime(date('H',$start_date),date('i',$start_date),date('s',$start_date),date('n',$start_date) + 1 ,0,date('Y',$start_date)));
            }
            if(date('j',$start_date) == $dom_filtered) {
                $increment = $start_date;
            }
            else {
                $increment = mktime(date('H',$start_date),date('i',$start_date),date('s',$start_date),date('n',$start_date),$dom_filtered,date('Y',$start_date));
            }        
            while($increment <= $end_date) {                 
                //verifico se il giorno in questione sia tra quelli in eccezione (escludere quindi)
                if(!empty($periodic['fc_pm_exception_group'])) {
                    $not_found = $this->exception_groups($periodic['fc_pm_exception_group'],$increment);
                    if(!$not_found) {                     
                        $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                        continue;
                    }
                }      
                //verifico che non ci siano eccezioni da gestire
                elseif(!empty($periodic['fc_pm_rules_exception_group'])) {
                    $not_found = $this->exception_groups($periodic['fc_pm_rules_exception_group'],$increment);
                    if(!$not_found) { //GESTISCO IL GIORNO SECONDO PROGRAMMA                        
                        $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$periodic['fc_pm_rules_exception_group']);                
                        if($mp_correct_day) {
                            if(!$fake) {
                                if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;
                                $this->createPeriodics($periodic, $f_code_wo, $mp_correct_day + $periodic['fc_pm_at_time'], $mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                                $counter_round++;
                            }
                            else {
                                if(in_array($increment,$check_fake_date)) {
                                    die("$increment still created");
                                }
                                $check_fake_date[] = $increment;
                                echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                            }                        
                        }
                        $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                        unset($mp_correct_day);
                        continue;
                    }
                }
                //verifico se cade in un giorno della settimana non permesso e se cosÃ¬, seguo le regole per gestire.
                if(!in_array(date('N',$increment),$dow) && strpos($periodic['fc_pm_rules_type_dayweek'],'always') === false) {                  
                    $group = false;
                    if(!empty($periodic['fc_pm_exception_group'])) $group = $periodic['fc_pm_exception_group'];
                    elseif(!empty($periodic['fc_pm_rules_exception_group'])) $group = $periodic['fc_pm_rules_exception_group'];

                    $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$group);                
                    if($mp_correct_day) {  
                        if(!$fake) {
                            if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;
                            $this->createPeriodics($periodic, $f_code_wo, $mp_correct_day + $periodic['fc_pm_at_time'], $mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                            $counter_round++;
                        }
                        else {
                            if(in_array($increment,$check_fake_date)) {
                                die("$increment still created");
                            }
                            $check_fake_date[] = $increment;
                            echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                        }                        
                    }
                    $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                    unset($mp_correct_day);
                    continue;
                }
                //verifico se cade in giorni particolari che vanno gestiti                
                if(!$fake) {
                    if(($increment + $periodic['fc_pm_at_time']) > $end_date) continue;
                    $this->createPeriodics($periodic, $f_code_wo, $increment + $periodic['fc_pm_at_time'], $increment + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                    $counter_round++;
                }
                else {
                    if(in_array($increment,$check_fake_date)) {
                        die("$increment still created");
                    }
                    $check_fake_date[] = $increment;
                    echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                }                        
                $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);                                    
            }
        }
        $this->db->closeConnection();
    }  
    
    public function ontimescheduled($periodic,$f_code_wo,$fake = false,$line_request)
    {
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $this->db = Zend_Db::factory($config->database);
        $counter_round = 0;
        $start_date = $periodic['fc_pm_start_date'];
        $end_date = $periodic['fc_pm_end_date'];
        $dow = explode(',',$periodic['fc_pm_rules_dayweek']);
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic = explode(' : ',$periodic['fc_pm_cyclic_type']);
        $fc_pm_forewarning = isset($periodic['fc_pm_forewarning']) && !is_null($periodic['fc_pm_forewarning'])?$periodic['fc_pm_forewarning']:0;
        $check_fake_date = array();
        //verifico se il giorno inizio manutenzione Ã¨ uguale al DoM
        if($start_date == strtotime($periodic['fc_pm_sequence']) ) {
            $increment = $start_date;
        }
        elseif($start_date > strtotime($periodic['fc_pm_sequence'])) {            
            $increment = $this->increment($type_cyclic[1], strtotime($periodic['fc_pm_sequence']), $num_cyclic);
        }
        else {
            $increment = strtotime($periodic['fc_pm_sequence']);
        }        
        while($increment <= $end_date) {            
            if($counter_round > 5000) {
                utilities::reportError("Something went wrong during the creation of you calendar : the request at line $line_request created 5000 periodics.", $this->id_excel);
                die;
            }
            //verifico se il giorno in questione sia tra quelli in eccezione (escludere quindi)
            if(!empty($periodic['fc_pm_exception_group'])) {
                $not_found = $this->exception_groups($periodic['fc_pm_exception_group'],$increment);
                if(!$not_found) {                     
                    $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                    if(!$increment) return;
                    continue;
                }
            }      
            //verifico che non ci siano eccezioni da gestire
            elseif(!empty($periodic['fc_pm_rules_exception_group'])) {
                $not_found = $this->exception_groups($periodic['fc_pm_rules_exception_group'],$increment);
                if(!$not_found) { //GESTISCO IL GIORNO SECONDO PROGRAMMA                    
                    if(!$increment) return;
                    $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$periodic['fc_pm_rules_exception_group']);                
                    if($mp_correct_day) {
                        if(!$fake) {
                            if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;
                            $this->createPeriodics($periodic, $f_code_wo, $mp_correct_day + $periodic['fc_pm_at_time'], $mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                            $counter_round++;
                        }
                        else {
                            if(in_array($increment,$check_fake_date)) {
                                die("$increment still created");
                            }
                            $check_fake_date[] = $increment;
                            echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                        }
                    }
                    $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                    unset($mp_correct_day);
                    if(!$increment) return;
                    continue;
                }
            }
            //verifico se cade in un giorno della settimana non permesso e se cosÃ¬, seguo le regole per gestire.
            if(!in_array(date('N',$increment),$dow) && strpos($periodic['fc_pm_rules_type_dayweek'],'always') === false) {                  
                $group = false;
                if(!empty($periodic['fc_pm_exception_group'])) $group = $periodic['fc_pm_exception_group'];
                elseif(!empty($periodic['fc_pm_rules_exception_group'])) $group = $periodic['fc_pm_rules_exception_group'];
                
                $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$group);                
                if($mp_correct_day) { 
                    if(!$fake) {
                        if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) continue;
                        $this->createPeriodics($periodic, $f_code_wo, $mp_correct_day + $periodic['fc_pm_at_time'], $mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                        $counter_round++;
                    }
                    else {
                        if(in_array($increment,$check_fake_date)) {
                            die("$increment still created");
                        }
                        $check_fake_date[] = $increment;
                        echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                    }
                }
                $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                if(!$increment) return;
                unset($mp_correct_day);
                continue;
            }
            //verifico se cade in giorni particolari che vanno gestiti
            if(!$fake) {
                if(($increment + $periodic['fc_pm_at_time']) > $end_date) continue;
                $this->createPeriodics($periodic, $f_code_wo, $increment + $periodic['fc_pm_at_time'], $increment + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                $counter_round++;
            }
            else {
                if(in_array($increment,$check_fake_date)) {
                    die("$increment still created");
                }
                $check_fake_date[] = $increment;
                echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
            }
            $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
            if(!$increment) return;
        }
        $this->db->closeConnection();
    }
    
    private function manageDayWeekException($time,$periodic,$dow,$group = false)
    {        
        $manage = explode(' : ',$periodic['fc_pm_rules_type_dayweek']);
        $value = 0;        
        switch($manage[0]) {
            case 0 : $value = $time;//LA ESEGUO COMUNQUE
                break;
            case 1 : //POSTICIPO AL PRIMO GIORNO DISPONIBILE
                $i = 1;
                while($i <= 8) { //SCORRO TUTTI I GIORNI (SE ARRIVO A 8 C'E' UN ERRORE)
                    $time = mktime(date('H',$time),date('i',$time),date('s',$time),date('n',$time),date('j',$time) + 1,date('Y',$time));
                    if(in_array(date('N',$time),$dow) && $group && $this->exception_groups($group, $time)) break;
                    $i++;
                }
                if($i == 8) die('ERRORE NELLA GESTIONE DEL DAY WEEK EXCEPTION');
                $value = $time;
                break;
            case 2 : //ANTICIPO AL PRIMO GIORNO DISPONIBILE
                $i = 1;
                while($i <= 8) { //SCORRO TUTTI I GIORNI (SE ARRIVO A 8 C'E' UN ERRORE)
                    $time = mktime(date('H',$time),date('i',$time),date('s',$time),date('n',$time),date('j',$time) - 1,date('Y',$time));                    
                    if(in_array(date('N',$time),$dow) && $group && $this->exception_groups($group, $time)) break;                        
                    $i++;
                }                
                if($i == 8) die('ERRORE NELLA GESTIONE DEL DAY WEEK EXCEPTION');
                $value = $time;                
                break;
            case 3 : //EXCLUDE
                break;
        }
        return $value;
    }
    
    public function periodic($periodic,$f_code_wo,$fake = false,$line_request = 0)
    {        
        $counter_round = 0;
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $this->db = Zend_Db::factory($config->database);
        
        $start_date = $periodic['fc_pm_start_date'];
        $end_date = $periodic['fc_pm_end_date'];
        $dow = explode(',',$periodic['fc_pm_rules_dayweek']);
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic = explode(' : ',$periodic['fc_pm_cyclic_type']);
        
        $check_fake_date = array();        
        $increment =$start_date;   
        
        while($increment <= $end_date) {            
            if($counter_round > 5000) {
                utilities::reportError("Something went wrong during the creation of you calendar : the request at line $line_request created 5000 periodics.", $this->id_excel);
                die;
            }
            if(!empty($periodic['fc_pm_exception_group'])) {
                $not_found = $this->exception_groups($periodic['fc_pm_exception_group'],$increment);
                if(!$not_found) {                     
                    $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                    if(!$increment) return;
                    continue;
                }
            }      
            //verifico che non ci siano eccezioni da gestire
            elseif(!empty($periodic['fc_pm_rules_exception_group'])) {
                $not_found = $this->exception_groups($periodic['fc_pm_rules_exception_group'],$increment);
                if(!$not_found) { //GESTISCO IL GIORNO SECONDO PROGRAMMA                    
                    if(!$increment) return;
                    $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$periodic['fc_pm_rules_exception_group']);                
                    if($mp_correct_day) {
                        if(!$fake) {
                            if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;
                            $this->createPeriodics($periodic, $f_code_wo, $mp_correct_day + $periodic['fc_pm_at_time'], $mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                            $counter_round++;
                        }
                        else {
                            if(in_array($increment,$check_fake_date)) {
                                die("$increment still created");
                            }
                            $check_fake_date[] = $increment;
                            echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                        }
                    }
                    $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                    unset($mp_correct_day);
                    if(!$increment) return;
                    continue;
                }
            }
            //verifico se cade in un giorno della settimana non permesso e se cosÃ¬, seguo le regole per gestire.
            if(!in_array(date('N',$increment),$dow) && strpos($periodic['fc_pm_rules_type_dayweek'],'always') === false) {                  
                $group = false;
                if(!empty($periodic['fc_pm_exception_group'])) $group = $periodic['fc_pm_exception_group'];
                elseif(!empty($periodic['fc_pm_rules_exception_group'])) $group = $periodic['fc_pm_rules_exception_group'];                
                $mp_correct_day = $this->manageDayWeekException($increment, $periodic, $dow,$group);                
                if($mp_correct_day) {                    
                    if(!$fake) {
                        if(($mp_correct_day + $periodic['fc_pm_at_time']) > $end_date) return;
                        $this->createPeriodics($periodic, $f_code_wo, $mp_correct_day + $periodic['fc_pm_at_time'], $mp_correct_day + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                        $counter_round++;
                    }else {
                        if(in_array($increment,$check_fake_date)) {
                            die("$increment still created");
                        }
                        $check_fake_date[] = $increment;
                        echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
                    }
                }
                $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
                if(!$increment) return;
                unset($mp_correct_day);
                continue;
            }
            //verifico se cade in giorni particolari che vanno gestiti
            if(!$fake) {
                if(($increment + $periodic['fc_pm_at_time']) > $end_date) { return;}
                $this->createPeriodics($periodic, $f_code_wo, $increment + $periodic['fc_pm_at_time'], $increment + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);            
                $counter_round++;
            }
            else {
                if(in_array($increment,$check_fake_date)) {
                    die("$increment still created");
                }
                $check_fake_date[] = $increment;
                echo 'data generazione : '.date('l d-m-Y H:i:s',$increment).'<br />';
            }
            
            $increment = $this->increment($type_cyclic[1], $increment,$num_cyclic);
        }   
        
        $this->db->closeConnection();
    }
    
    private function increment($type,$value,$num_cyclic)
    {
        switch($type) {
            case 'week' :  //week
                $value = mktime(date('H',$value),date('i',$value),date('s',$value),
                date('n',$value),date('j',$value) + (7 * $num_cyclic),date('Y',$value));
                break;
            case 'month' : //month                
                $value = mktime(date('H',$value),date('i',$value),date('s',$value),
                date('n',$value) + $num_cyclic,date('j',$value),date('Y',$value));
                break;
            case 'year' : //year
                $value = mktime(date('H',$value),date('i',$value),date('s',$value),
                date('n',$value),date('j',$value),date('Y',$value) + $num_cyclic);
                break;
            case 'day' : //day
                $value = mktime(date('H',$value),date('i',$value),date('s',$value),
                date('n',$value),date('j',$value) + $num_cyclic,date('Y',$value));
                break;
            case 'none' : //none
                $value = 0;
                break;
            default : 
                utilities::reportError('MP con tipo ciclicity errato', $this->id_excel);die;
                break;
        }
        return $value;
    }
    
    private function createPeriodics($periodic,$f_code,$start_date,$end_date)
    {        
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $this->db = Zend_Db::factory($config->database);
        
        $select = new Zend_Db_Select($this->db);
        //recupero dal wo i pezzi mancanti
        $res = $select->from("t_workorders")
                ->join("t_creation_date","f_code = t_creation_date.f_id")
                ->where("f_code = ?",$f_code)->query()->fetchAll();
        $select->reset();        
        if(!empty($res)) {
            $data = array(
                'f_code' => $f_code, 
                'f_code_ware'=>$periodic['f_code_ware'],
                'f_title'=>$res[0]['f_title'],
                'f_forewarning'=>$periodic['fc_pm_forewarning'],
                'f_start_date'=>$start_date,
                'f_end_date'=>$end_date,
                'f_periodic_type_id'=>$res[0]['f_type_id_periodic'],                
                'f_timestamp'=>time(),
                'f_mode'=>1,
                'f_available_type'=>1,
                'f_executed'=>0,
                'f_duration'=>$periodic['fc_pm_duration'],
                'f_visibility'=>-1,
                'f_editability'=>-1
            );

            $this->db->insert('t_periodics',$data);
        }
        
        $this->db->closeConnection();
    }
    
    public function periodiGenerator($periodic,$fake = false,$line_request = 0)
    {        
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $this->db = Zend_Db::factory($config->database);
        $i = 0;
        $sel = new Zend_Db_Select($this->db);
        //foreach($engagements as &$periodic) {
        $fc_pm_periodic_type = substr($periodic['fc_pm_periodic_type'],0,1);        
        
        if(!is_int($periodic['fc_pm_start_date']) || !is_int($periodic['fc_pm_end_date'])) return;
        
        $connect_wo_fcode = $periodic['f_code'];
        $line_eng = $line_request + $i;
        $sel->reset();
        $res_multiple = $sel->from("t_workorders","fc_pm_start_date_type")->where("f_code = ?",$periodic['f_code'])->query()->fetchAll();            
        if(isset($res_multiple[0]['fc_pm_start_date_type']) && $res_multiple[0]['fc_pm_start_date_type'] == 1) {
            $periodic['f_code_ware'] = 0;
            //verifico il tipo
            switch($fc_pm_periodic_type) {                
                case '0':$this->weekly($periodic,$connect_wo_fcode,$fake,$line_eng); //WEEKLY OK (TESTARE PER BENE)                                                
                    break;
                case '1':$this->monthly($periodic,$connect_wo_fcode,$fake,$line_eng);//monthly
                    break;
                case '2':$this->periodic($periodic,$connect_wo_fcode,$fake,$line_eng);//periodic
                    break;
                case '3':$this->dayofthemonth($periodic,$connect_wo_fcode,$fake,$line_eng); //MONTHLY OK (TESTARE PER BENE)
                    break;                
                case '4':$this->ontimescheduled($periodic,$connect_wo_fcode,$fake,$line_eng);// ON TIME SCHEDULED
                    break;        
            }                
        }

        //multiple start date from wares associated to pm
        elseif(isset($res_multiple[0]['fc_pm_start_date_type']) && $res_multiple[0]['fc_pm_start_date_type'] == 2) {                
            $sel->reset();
            $res_wares_sd = $sel->from("t_wares",array("f_start_date","f_code"))->join("t_ware_wo","f_code = f_ware_id",array())                
                ->where("f_wo_id = ?",$periodic['f_code'])->where("f_type_id = 1")
                ->where("f_start_date > 0")->where("f_wf_id = 9")->where("f_phase_id = 1")
                ->query()->fetchAll();

            foreach($res_wares_sd as $wares_sd) {
                $periodic['fc_pm_start_date'] = $wares_sd['f_start_date'];
                $periodic['f_code_ware'] = $wares_sd['f_code'];
                //verifico il tipo
                switch($fc_pm_periodic_type) {                
                    case '0':$this->weekly($periodic,$connect_wo_fcode,$fake,$line_eng); //WEEKLY OK (TESTARE PER BENE)
                        break;
                    case '1':$this->monthly($periodic,$connect_wo_fcode,$fake,$line_eng);//monthly
                        break;
                    case '2':$this->periodic($periodic,$connect_wo_fcode,$fake,$line_eng);//periodic
                        break;
                    case '3':$this->dayofthemonth($periodic,$connect_wo_fcode,$fake,$line_eng); //MONTHLY OK (TESTARE PER BENE)
                        break;                
                    case '4':$this->ontimescheduled($periodic,$connect_wo_fcode,$fake,$line_eng);// ON TIME SCHEDULED
                        break;        
                }
            }                
        }
        $i++;
        //}        
        $this->db->closeConnection();
    }    
}