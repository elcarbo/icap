<?php

class mp_read_class_importer
{
    /**
     *
     * @var Zend_Db
     */
    private $db;
    
    public function __construct() {                
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');        
        $this->db = Zend_Db::factory($config->database);
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    /**
     * read if there are starting periodics
     */
    public function readPeriodics($start_date = 0,$end_date = 0,$limit = 0)
    {      
        $end_date = $end_date>0?$end_date:time();        
        $select = new Zend_Db_Select($this->db);        
        $select->from(array("t1"=>"t_periodics"))
                ->join(array("t2"=>"t_workorders"),"t1.f_code = t2.f_code",array())
                ->join("t_creation_date","t_creation_date.f_id = t2.f_code",array())
                ->where("t2.f_type_id = 3")
                ->where("t_creation_date.f_phase_id = 1")                
                ->where("f_executed = 0")
                ->where("t1.f_start_date - t1.f_forewarning between $start_date and $end_date");                    
        if($limit != 0) {
            $select->limit($limit);
        }
        $res = $select->query()->fetchAll();        
        if(!$limit) {
            foreach($res as $line) {            
                $this->db->update("t_periodics",array('f_executed'=>1),"f_id = {$line['f_id']}");
            }
        }
        foreach($res as $line) {            
            $periodic = $line;
            //now we must do three check : 
            // 1: check if father is only a folder without an engagement
            // 2: check if father have an engagement and in this case the child's engagement will not use
            // 3: check if is a father with engagement, generate every child
            $select->reset();
            //first and second check 
            $res_father = $select->from(array("t1"=>"t_workorders_parent"),array())
                    ->join(array("t2"=>"t_periodics"),"t1.f_parent_code = t2.f_code",array("num"=>"count(*)"))
                    ->where("t1.f_active = 1")->where("t1.f_code = ?",$line['f_code'])
                    ->query()->fetchAll();
            
            if($res_father[0]['num'] > 0) continue;
            
            //third check
            $select->reset();
            $res_child = $select->from("t_workorders_parent",array("f_code"))
                    ->where("f_active = 1")->where("f_parent_code = ?",$line['f_code'])
                    ->query()->fetchAll();
            $f_codes = array($line['f_code']);
            foreach($res_child as $child) {
                $f_codes[] = $child['f_code'];
            }
            
            $select->reset();
            $res_wo_master = $select->from(array("t1"=>"t_workorders"),array('f_code','fc_pm_start_date_type'))                                        
                    ->where("t1.f_code = ?",$line['f_code'])->query()->fetchAll();                          
            if(empty($res_wo_master)) continue;
            
            foreach($f_codes as $f_code) {                
                $periodic['f_code'] = $f_code;
                $res_asset = array();
                $res_task = array();
                $res_pair = array();
                $other_cross = array();
                //PRENDO TUTTE LE PERIODICHE TROVATE E LE TRASFORMO IN RICHIESTE
                $select->reset();
                $res_wo = $select->from(array("t1"=>"t_workorders"))
                        ->join("t_creation_date","t1.f_code = t_creation_date.f_id")
                        ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code")
                        ->join(array("t3"=>"t_workorders_parent"),"t1.f_code = t3.f_code",array("f_parent_code"))
                        ->where("t3.f_active = 1")->where("t1.f_code = ?",$periodic['f_code'])->query()->fetchAll();                     
                if(empty($res_wo)) continue;
                
                //check if father is only a folder. If true check if exist the folder o create new
                $select->reset();
                $res_parent_check = $select->from(array("t1"=>"t_workorders_parent"),array("f_parent_code"))
                        ->join(array("t2"=>"t_workorders"),"t1.f_parent_code = t2.f_code",array("f_priority","f_user_id"))                            
                        ->join("t_creation_date","t2.f_code = t_creation_date.f_id",array("f_title","f_description"))
                        ->where("t1.f_active = 1")->where("f_parent_code != 0")->where("t1.f_code = ?",$f_code)
                        ->where("not exists(select * from t_pair_cross where f_code_cross = -1 and f_code_main = t1.f_parent_code)")
                        ->query()->fetchAll();
                //if start date type is simple                
                if($res_wo_master[0]['fc_pm_start_date_type'] == 1) {                                          
                    $select->reset();
                    $res_asset = $select->from(array("t1"=>"t_wares"),array('f_code'))
                        ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array("f_title"))
                        ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())
                        ->join(array("t3"=>"t_wf_phases"),"t_creation_date.f_phase_id = t3.f_number and t3.f_wf_id = t_creation_date.f_wf_id",array())
                        ->join(array("t4"=>"t_wf_groups"),"t4.f_id = t3.f_group_id",array())    
                        ->where("t4.f_visibility != 0")
                        ->where("t2.f_wo_id = ?",$res_wo_master[0]['f_code'])
                        ->where("t1.f_type_id = 1")->where("(t1.f_start_date < ".$periodic['f_start_date']." OR t1.f_start_date = 0)")
                        ->query()->fetchAll();
                    
                    $select->reset();
                    $res_task = $select->from(array("t1"=>"t_wares"),array('f_code','fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny','fc_task_tasklist'))
                        ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array("f_title"))
                        ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())                                                
                        ->where("t2.f_wo_id = ?",$res_wo_master[0]['f_code'])
                        ->where("t1.f_type_id = 10")->where("t_creation_date.f_phase_id = 1")->query()->fetchAll();      
                    
                    //insert other ware_wo cross
                    $select->reset();
                    $other_cross = $select->from("t_ware_wo")
                            ->join("t_wares","f_code = f_ware_id",array())
                            ->where("f_wo_id = ?",$res_wo_master[0]['f_code'])                            
                            ->where("f_type_id NOT IN (1,10)")
                            ->query()->fetchAll();                    
                    //Find all pair cross (excluding engagement) and add to system
                    $select->reset();
                    $res_pair = $select->from("t_pair_cross")
                            ->where("f_code_main = ?",$res_wo_master[0]['f_code'])                
                            ->where("f_code_cross != -1")->query()->fetchAll();                                        
                    
                    if(!empty($res_parent_check)) {
                        $select->reset();
                        $res_asset_fath = $select->from(array("t1"=>"t_wares"),array('f_code'))
                            ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array("f_title"))
                            ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())
                            ->join(array("t3"=>"t_wf_phases"),"t_creation_date.f_phase_id = t3.f_number and t3.f_wf_id = t_creation_date.f_wf_id",array())
                            ->join(array("t4"=>"t_wf_groups"),"t4.f_id = t3.f_group_id",array())    
                            ->where("t4.f_visibility != 0")
                            ->where("t2.f_wo_id = ?",$res_parent_check[0]['f_parent_code'])
                            ->where("t1.f_type_id = 1")->where("(t1.f_start_date < ".$periodic['f_start_date']." OR t1.f_start_date = 0)")
                            ->query()->fetchAll();                    
                        
                        foreach($res_asset_fath as $line_asset_fath) {
                            $not_found = true;
                            foreach($res_asset as $line_asset) {
                                if($line_asset['f_code'] == $line_asset_fath['f_code']){ $not_found = false; break;}                
                            }
                            if($not_found) {
                                $res_asset[] = $line_asset_fath;
                            }
                        }
                        $select->reset();
                        $res_task_fath = $select->from(array("t1"=>"t_wares"),array('f_code','fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny'))
                            ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array("f_title"))
                            ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())                                                      
                            ->where("t2.f_wo_id = ?",$res_parent_check[0]['f_parent_code'])
                            ->where("t1.f_type_id = 10")->where("t_creation_date.f_phase_id = 1")->query()->fetchAll();                    
                        
                        foreach($res_task_fath as $line_task_fath) {
                            $not_found = true;
                            foreach($res_task as $line_task) {
                                if($line_task['f_code'] == $line_task_fath['f_code']){ $not_found = false; break;}                
                            }
                            if($not_found) {
                                $res_task[] = $line_task_fath;
                            }
                        }
                        
                        $select->reset();
                        //insert other ware_wo cross
                        $other_cross_fath = $select->from("t_ware_wo")
                                ->join("t_wares","f_code = f_ware_id",array())
                                ->where("f_wo_id = ?",$res_parent_check[0]['f_parent_code'])                                
                                ->where("f_type_id NOT IN (1,10)")
                                ->query()->fetchAll();                    
                        
                        foreach($other_cross_fath as $line_cross_fath) {
                            $not_found = true;
                            foreach($other_cross as $line_cross) {
                                if($line_cross['f_wo_id'] == $line_cross_fath['f_wo_id'] &&  $line_cross['f_ware_id'] == $line_cross_fath['f_ware_id']){ 
                                    $not_found = false; 
                                    break;
                                }                
                            }
                            if($not_found) {
                                $other_cross[] = $line_cross_fath;
                            }
                        }                        
                        
                        //Find all pair cross (excluding engagement) and add to system
                        $select->reset();
                        $res_pair_fath = $select->from("t_pair_cross")
                                ->where("f_code_main = ?",$res_parent_check[0]['f_parent_code'])
                                ->where("f_code_cross != -1")->query()->fetchAll();
                        foreach($res_pair_fath as $line_pair_fath) {
                            $res_pair[] = $line_pair_fath;
                        }      
                    }
                    
                    $f_code_generated = $this->createWorkorders($periodic,$res_wo[0],$res_asset,$res_task,$other_cross,$res_pair);
                    
                }
                //if start date type is multiple
                elseif($res_wo_master[0]['fc_pm_start_date_type'] == 2) {                        
                    $select->reset();
                    $res_asset = $select->from(array("t1"=>"t_wares"),array('f_code'))       
                        ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array("f_title"))
                        ->join(array("t3"=>"t_wf_phases"),"t_creation_date.f_phase_id = t3.f_number and t3.f_wf_id = t_creation_date.f_wf_id",array())
                        ->join(array("t4"=>"t_wf_groups"),"t4.f_id = t3.f_group_id",array())    
                        ->where("t4.f_visibility != 0")->where("t1.f_code = ?",$periodic['f_code_ware'])
                        ->query()->fetchAll();   
                    $select->reset();
                    $res_task = $select->from(array("t1"=>"t_wares"),array('f_code','fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny'))
                        ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array("f_title"))
                        ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())                        
                        ->where("t2.f_wo_id = ?",$res_wo_master[0]['f_code'])
                        ->where("t1.f_type_id = 10")->where("t_creation_date.f_phase_id = 1")->query()->fetchAll();
                    //check if asset is decomminssioned or disabled
                    
                    //insert other ware_wo cross
                    $select->reset();
                    $other_cross = $select->from("t_ware_wo")
                            ->join("t_wares","f_code = f_ware_id",array())
                            ->where("f_wo_id = ?",$res_wo_master[0]['f_code'])                            
                            ->where("f_type_id NOT IN (1,10)")
                            ->query()->fetchAll();                    
                    //Find all pair cross (excluding engagement) and add to system
                    $select->reset();
                    $res_pair = $select->from("t_pair_cross")->where("f_code_main = ?",$res_wo_master[0]['f_code'])                
                            ->where("f_code_cross != -1")->query()->fetchAll();                                        
                    
                    if(!empty($res_parent_check)) {                        
                        $select->reset();
                        $res_task_fath = $select->from(array("t1"=>"t_wares"),array('f_code','fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny'))
                            ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array("f_title"))
                            ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())                            
                            ->where("t2.f_wo_id = ?",$res_parent_check[0]['f_parent_code'])
                            ->where("t1.f_type_id = 10")->where("t_creation_date.f_phase_id = 1")->query()->fetchAll();                    
                        
                        foreach($res_task_fath as $line_task_fath) {
                            $not_found = true;
                            foreach($res_task as $line_task) {
                                if($line_task['f_code'] == $line_task_fath['f_code']){ $not_found = false; break;}                
                            }
                            if($not_found) {
                                $res_task[] = $line_task_fath;
                            }
                        }
                        
                        $select->reset();
                        //insert other ware_wo cross
                        $other_cross_fath = $select->from("t_ware_wo")
                                ->join("t_wares","f_code = f_ware_id",array())
                                ->where("f_wo_id = ?",$res_parent_check[0]['f_parent_code'])                                
                                ->where("f_type_id NOT IN (1,10)")->query()->fetchAll();                    
                        
                        foreach($other_cross_fath as $line_cross_fath) {
                            $not_found = true;
                            foreach($other_cross as $line_cross) {
                                if($line_cross['f_wo_id'] == $line_cross_fath['f_wo_id'] &&  $line_cross['f_ware_id'] == $line_cross_fath['f_ware_id']){ 
                                    $not_found = false; 
                                    break;
                                }                
                            }
                            if($not_found) {
                                $other_cross[] = $line_cross_fath;
                            }
                        }                        
                        
                        //Find all pair cross (excluding engagement) and add to system
                        $select->reset();
                        $res_pair_fath = $select->from("t_pair_cross")->where("f_code_main = ?",$res_parent_check[0]['f_parent_code'])
                                ->where("f_code_cross != -1")->query()->fetchAll();
                        foreach($res_pair_fath as $line_pair_fath) {
                            $res_pair[] = $line_pair_fath;
                        }      
                    }
                    $f_code_generated = 0;
                    if(!empty($res_asset)){ $f_code_generated = $this->createWorkorders($periodic,$res_wo[0],$res_asset,$res_task,$other_cross,$res_pair);}
                }
                
                if(!empty($res_parent_check) && $f_code_generated) { // father exist and hasn't engagement.
                    //check if folder periodic already exist, otherwise we create one
                    $select->reset();
                    $res_check_father = $select->from(array("t1"=>"t_workorders"),array("f_code"))
                            ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array())
                            ->join(array("t2"=>"t_wf_phases"),"t_creation_date.f_wf_id = t2.f_wf_id and t_creation_date.f_phase_id = t2.f_number",array())
                            ->join(array("t3"=>"t_wf_groups"),"t2.f_group_id = t3.f_id",array())
                            ->where("f_type_id = 4")->where("f_code_periodic = ?",$res_parent_check[0]['f_parent_code'])
                            ->where("t3.f_visibility != 0")->query()->fetchAll();
                    if(empty($res_check_father)) {
                        //if parent folder not exist, create new father
                        $periodic_line = array("f_code"=>$res_parent_check[0]['f_parent_code'],"f_start_date"=>0);
                        $res_wo_fath = array(
                            "f_user_id"=>$res_parent_check[0]['f_user_id'],
                            "f_title"=>$res_parent_check[0]['f_title'],
                            "f_description"=>$res_parent_check[0]['f_description'],
                            "f_priority"=>$res_parent_check[0]['f_priority'],
                            'f_type_id_periodic'=>4,
                            'fc_pm_duration'=>0
                            );
                        $f_code_fath = $this->createWorkorders($periodic_line, $res_wo_fath, array(), array(), array(), array(), true);
                        $res_check_father[0]['f_code'] = $f_code_fath;
                    }
                    $this->db->update("t_workorders_parent",array("f_parent_code"=>$res_check_father[0]['f_code']),"f_active = 1 and f_code = $f_code_generated");
                }
            }
        }
        return count($res);  
    }
    
    private function createWorkorders($line,$res_wo,$res_asset = array(),$res_task = array(),$res_ware_wo = array(),$res_pair = array(),$father_folder = false)
    {        
        $select = new Zend_Db_Select($this->db);
        $data = array();
        //f_code of generator
        $f_code_genereator =  $line['f_code'];        
        
        //add to passed assets the asset of child(and not present)
        $res_asset_per = $select->from(array("t1"=>"t_wares"),array('f_code'))
            ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array('f_title')) 
            ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())
            ->join(array("t3"=>"t_wf_phases"),"t_creation_date.f_phase_id = t3.f_number and t3.f_wf_id = t_creation_date.f_wf_id",array())
            ->join(array("t4"=>"t_wf_groups"),"t4.f_id = t3.f_group_id",array())    
            ->where("t4.f_visibility != 0")
            ->where("t2.f_wo_id = ?",$line['f_code'])
            ->where("t1.f_type_id = 1")->where("(t1.f_start_date < {$line['f_start_date']} OR t1.f_start_date = 0)")
            ->query()->fetchAll();        
        
        foreach($res_asset_per as $line_asset_per) {
            $not_found = true;
            foreach($res_asset as $line_asset) {
                if($line_asset['f_code'] == $line_asset_per['f_code']){ $not_found = false; break;}                
            }
            if($not_found) {
                $res_asset[] = $line_asset_per;
            }
        }
        
        $select->reset();
        //add to passed tasks the task of child(and not present)
        $res_task_per = $select->from(array("t1"=>"t_wares"),array('f_code','fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny'))
            ->join("t_creation_date","t1.f_code = t_creation_date.f_id",array('f_title')) 
            ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())            
            ->where("t2.f_wo_id = ?",$line['f_code'])->where("t1.f_type_id = 10")
            ->where("t_creation_date.f_phase_id = 1")->query()->fetchAll();
        
        foreach($res_task_per as $line_task_per) {
            $not_found = true;
            foreach($res_task as $line_task) {
                if($line_task['f_code'] == $line_task_per['f_code']){ $not_found = false; break;}                
            }
            if($not_found) {
                $res_task[] = $line_task_per;
            }
        }
        
        //check if one of the task finded is a task list, if true, remove it and if necessary task included in the list                    
        $tot_a = count($res_task);
        $check_task_list = array();
        for($a = 0;$a < $tot_a;++$a) {                                       
            if(!empty($res_task[$a]['fc_task_tasklist'])) { // is a task
                $select->reset();
                $res_task_children = $select->from(array("t1"=>"t_wares_parent"),array())
                    ->join(array("t2"=>"t_wares"),"t1.f_code = t2.f_code",array('f_code','fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny','fc_task_tasklist'))
                    ->join(array("t3"=>"t_creation_date"),"t2.f_code = t3.f_id",array('f_title'))
                    ->where("t1.f_parent_code = ?",$res_task[$a]['f_code'])->where("t3.f_phase_id = 1")->query()->fetchAll();
                foreach($res_task_children as $line_task_children) {
                    $not_found = true;
                    foreach($res_task as $line_task) {
                        if($line_task['f_code'] == $line_task_children['f_code']) {$not_found = false;break;}
                    }
                    if($not_found) {
                        foreach($check_task_list as $line_check_task_list) {
                            if($line_task['f_code'] == $line_check_task_list['f_code']) {$not_found = false;break;}
                        }
                        if($not_found) {
                            $check_task_list[] = $line_task_children;
                        }
                    }
                }
            }                      
            else {
                $check_task_list[] = $res_task[$a];
            }
        }        
        $res_task = $check_task_list;
        //-----------------
        
        $select->reset();
        //insert other ware_wo cross
        $res_ware_wo_per = $select->from("t_ware_wo")
            ->join("t_wares","f_code = f_ware_id",array())
            ->where("f_wo_id = ?",$f_code_genereator)            
            ->where("f_type_id NOT IN (1,10)")
            ->query()->fetchAll();
        
        foreach($res_ware_wo_per as $line_ware_wo_per) {
			$not_found = true;
            foreach($res_ware_wo as $line_ware_wo) {
                if($line_ware_wo['f_ware_id'] == $line_ware_wo_per['f_ware_id']){ $not_found = false; break;}                
            }
            if($not_found) {
                $res_ware_wo[] = $line_ware_wo_per;
            }                     
        }
        
        //Find all pair cross (excluding engagement) and add to system
        $select->reset();
        $res_pair_per = $select->from("t_pair_cross")->where("f_code_main = ?",$f_code_genereator)                
                ->where("f_code_cross != -1")->query()->fetchAll();
        foreach($res_pair_per as $line_pair_per) {
            $res_pair[] = $line_pair_per;            
        }    
                
        //recupero il wf dal type_id
        $select->reset();
        $res_wf = $select->from("t_workorders_types",array('f_wf_id','f_wf_phase'))->where("f_id = ?",$res_wo['f_type_id_periodic'])->query()->fetch();
        $wf = !empty($res_wf) && $res_wf['f_wf_id']?$res_wf['f_wf_id']:1;
        $phase = !empty($res_wf) && $res_wf['f_wf_phase']?$res_wf['f_wf_phase']:1;
        $time = time();
        $data['f_type_id'] = $res_wo['f_type_id_periodic'];        
        $data['f_timestamp'] = $time;
        $data['f_code_periodic'] = $line['f_code'];        
        $data['f_user_id'] = $res_wo['f_user_id'];
        $data['f_start_date'] = $line['f_start_date']; //data inizio
        $data['f_end_date'] = $line['f_start_date'] + $res_wo['fc_pm_duration'];
        $data['f_priority'] = $res_wo['f_priority'];                      
        $data['f_counter'] = 0;
        
        //recupero il type_id da usare       
        $select->reset();
        $res_types = $select->from("t_workorders_types")->where("f_id = ?",$res_wo['f_type_id_periodic'])->query()->fetch();
        
        if(empty($res_types)) return;
        $select->reset();
        $res_creation = $select->from("t_creation_date")->where("f_category = ?",$res_types['f_type'])->order("f_order DESC")->limit(1)->query()->fetch();        
        $creation = array(
            'f_type'=>'WORKORDERS',
            'f_category'=>$res_types['f_type'],
            'f_order'=>!empty($res_creation)?$res_creation['f_order'] + 1:1,
            'f_creation_date'=>$time,
            'f_creation_user'=>1,
            'f_title'=>$res_wo['f_title'],
            'f_wf_id'=>$wf,
            'f_phase_id'=>$phase,
            'f_visibility'=>-1,
            'f_editability'=>-1,
            'f_description'=>$res_wo['f_description'],
            'f_timestamp'=>$time
        );                
        $f_code = $this->creationDate($creation);            
        $data['f_code'] = $f_code;
        $select->reset();
        $res_wo_order = $select->from("t_workorders")->where("f_type_id = ?",$res_wo['f_type_id_periodic'])->order("f_order DESC")->limit(1)->query()->fetch();
        $data['f_order'] = !empty($res_wo_order)?$res_wo_order['f_order'] + 1 : 1;
        //Insert wo    
        $this->db->insert("t_workorders",$data);     
        
        //Inserico i custom fields 
        $custom = array();        
        $select->reset();
        $custom = $select
            ->from("t_custom_fields")
            ->where("f_code = ?",$f_code_genereator)
            ->query()->fetch();                
        $custom["f_code"] = $f_code;
        $custom ['f_timestamp'] = $time;
        unset($custom['f_id']);    
                
        //if is not only a father folder, add asset hierarchy
        if(!$father_folder) {
            $select->reset();                
            $asset_hierarchies = array();
            foreach($res_asset as $f_code_wares_1) {            
                //recupero i vari asset hierarchy
                $select->reset();
                $res_hy = $select->from("t_custom_fields",array('fc_asset_hierarchy'))
                        ->where("f_code = ?",$f_code_wares_1['f_code'])->query()->fetchAll();
                if(!empty($res_hy)) {
                    $asset_hierarchies[$f_code_wares_1['f_title']] = $res_hy[0]['fc_asset_hierarchy'];
                }
            }
            $new_fc_asset_hy = array();
            foreach ($asset_hierarchies as $key_hy =>$value_hy) {
                $new_fc_asset_hy[] = !empty($value_hy)?"$key_hy($value_hy)":"$key_hy";
            }
            $custom['fc_asset_hierarchy'] = implode(", ",$new_fc_asset_hy);                
        }
        
        $this->db->insert("t_custom_fields", $custom);
        
        $select->reset();
        //script
        $res_script = $select->from("t_scripts")->where("f_type = 'generator'")->query()->fetchAll();
        foreach( $res_script as $script) {
            if(!empty($script['f_script'])) {
                eval($script['f_script']);
            }
        }        
        
        //insert cross between pm generator and periodic
        $cross_wo_rels = array(
            'f_code_wo_master'=>$f_code_genereator,
            'f_type_id_wo_master'=>3,
            'f_code_wo_slave'=>$f_code,
            'f_type_id_wo_slave'=>4,            
            'f_timestamp'=>$time
        );
        $this->db->insert('t_wo_relations',$cross_wo_rels);
        
        //Controllo se la periodica ha un padre e se quel padre va generato
        //Primo caso : controllo se ha una paternità
        $select->reset();
        $res_parent = $select->from("t_workorders_parent")->where("f_code = ?",$f_code_genereator)->where("f_active = 1")->query()->fetchAll();        
        $parent = array(
            "f_active"=>1,
            "f_timestamp"=>$time,
            "f_code"=>$f_code
        );
        $parent['f_parent_code'] = 0;
        if(isset($res_parent[0]['f_parent_code']) && $res_parent[0]['f_parent_code'] != 0) {                         
            $select->reset();
            $res_periodic = $select->from("t_periodics")->where("f_code = {$res_parent[0]['f_parent_code']}")->query()->fetchAll();            
            if(!empty($res_periodic)) {
                //find last insert code of this father generator and create hierarchy
                $select->reset();
                $res_parent_code = $select->from("t_workorders",array("f_code"))
                        ->where("f_code_periodic = ?",$res_parent[0]['f_parent_code'])
                        ->order("f_id DESC")->limit(1)->query()->fetchAll();                
                $parent['f_parent_code'] = isset($res_parent_code[0]['f_code'])?$res_parent_code[0]['f_code']:0;                
            }            
        }
        $this->db->insert("t_workorders_parent", $parent);
        
        //install selectors of the generator
        $select->reset();
        $res_selector = $select->from("t_selector_wo",array("f_selector_id","f_timestamp","f_nesting_level"))
                ->where("f_wo_id = ?",$f_code_genereator)->query()->fetchAll();
        foreach($res_selector as $line_selector) {
            $wo_sel = $line_selector;
            $wo_sel['f_wo_id'] = $f_code;
            $this->db->insert("t_selector_wo",$wo_sel);    
        }
        
        if($father_folder) { // if is a father folder, don't insert cross and return
            return $f_code;
        }
        
        //Inserisco gli asset se sono presenti.        
        foreach($res_asset as $line_asset) {
            $asset_edit = array(
                'f_ware_id'=>$line_asset['f_code'],
                'f_wo_id'=>$f_code,                
                'f_timestamp'=>$time                
            );            
            $this->db->insert("t_ware_wo",$asset_edit);                
        }

        foreach($res_ware_wo as $line_ware_wo) {
            $new_ware_wo = $line_ware_wo;
            unset($new_ware_wo['f_id']);
            $new_ware_wo['f_timestamp'] = $time;
            $new_ware_wo['f_wo_id'] = $f_code;
            $this->db->insert("t_ware_wo",$new_ware_wo);
        }
        
        foreach($res_task as $line_task) {            
            //check if is the turn of this task
            $allow_array = explode(",",$line_task['fc_task_exception_allow']);
            if(count($allow_array) == 1) {$allow_array = explode(".",$line_task['fc_task_exception_allow']);}
            $deny_array = explode(",",$line_task['fc_task_exception_deny']);
            if(count($deny_array) == 1) {$deny_array = explode(".",$line_task['fc_task_exception_deny']);}
            
            //if result not is 0, check if is allow
            $is_allow = false;
            $task_sequence = (int)$line_task['fc_task_sequence']?(int)$line_task['fc_task_sequence']:1;
            if(($res_wo['f_counter']%$task_sequence)) {
                //check if is allow
                foreach($allow_array as $allow) {
                    if($allow && !($res_wo['f_counter']%$allow)){
                        $is_allow = true;
                        break;
                    }
                }
            }            
            else { //if is an allow day, check if there are limitation for this task
                foreach ($deny_array as $deny) {
                    if($deny && !($res_wo['f_counter']%$deny)) {
                        $is_allow = false;
                        break;
                    }
                    else{
                        $is_allow = true;
                    }
                }
                if(empty($deny_array)) {
                    $is_allow = true;
                }
                if(!$is_allow) {
                    foreach($allow_array as $allow) {
                        if($allow && !($res_wo['f_counter']%$allow)){
                            $is_allow = true;
                            break;
                        }
                    }
                }
            }            
            //final check    
            if(!$is_allow) continue;
                        
            //hook task for every asset
            foreach($res_asset as $line_asset) {
                $per = array(
                    "f_code_main"=>$f_code,
                    'f_timestamp'=>$time,                    
                    'f_code_cross'=>$line_task['f_code'],
                    'f_group_code'=>$line_asset['f_code']
                );                 
                
                //recupero l'f_title                
                $per['f_title'] = $line_task['f_title'];
                
                unset($per['f_id']);                
                $this->db->insert("t_pair_cross",$per);   
            }
        }
        
        //insert specific task inserted before in asset
        foreach($res_asset as $line_asset) {
            $res_task_asset = array();
            $select->reset();
            $res_asset_task = $select->from(array("t1"=>"t_wares_relations"),array())
                ->join(array("t2"=>"t_wares"),"t1.f_code_ware_slave = t2.f_code",array('f_code','fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny','fc_task_tasklist'))                 
                ->join(array("t3"=>"t_creation_date"),"t2.f_code = t3.f_id",array('f_title'))   
                ->where("t1.f_code_ware_master = ?",$line_asset['f_code'])->where("t2.f_type_id = 10")->where("t3.f_phase_id = 1")
                ->query()->fetchAll();            
            foreach($res_asset_task as $line_asset_task) {                            
                if(empty($line_asset_task['fc_task_tasklist'])) {                    
                    $not_found = true;
                    foreach($res_task as $line_task) {
                        if($line_task['f_code'] == $line_asset_task['f_code'] ) {$not_found = false;break;}
                    }
                    if($not_found) {
                        $res_task_asset[] = $line_asset_task;
                    }                                                    
                }
                else { // if this is a task list, extract the list of task (only active)
                    $select->reset();
                    $res_asset_task_list = $select->from(array("t1"=>"t_wares_parent"),array())
                        ->join(array("t2"=>"t_wares"),"t1.f_code = t2.f_code",array('f_code','fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny','fc_task_tasklist'))
                        ->join(array("t3"=>"t_creation_date"),"t2.f_code = t3.f_id",array('f_title'))
                        ->where("t1.f_parent_code = ?",$line_asset_task['f_code'])
                        ->where("t3.f_phase_id = 1")->query()->fetchAll();                    
                    foreach($res_asset_task_list as $line_asset_task_list) {
                        $not_found = true;
                        foreach($res_task as $line_task) {
                            if($line_task['f_code'] == $line_asset_task_list['f_code'] ) {$not_found = false;break;}
                        }
                        if($not_found) {
                            $res_task_asset[] = $line_asset_task_list;
                        }
                    }
                }
            }
                        
            foreach($res_task_asset as $line_task) {            
                //check if is the turn of this task
                $allow_array = explode(",",$line_task['fc_task_exception_allow']);
                if(count($allow_array) == 1) {$allow_array = explode(".",$line_task['fc_task_exception_allow']);}
                $deny_array = explode(",",$line_task['fc_task_exception_deny']);
                if(count($deny_array) == 1) {$deny_array = explode(".",$line_task['fc_task_exception_deny']);}

                //if result not is 0, check if is allow
                $is_allow = false;
                if(($res_wo['f_counter']%$line_task['fc_task_sequence'])) {
                    //check if is allow
                    foreach($allow_array as $allow) {
                        if($allow && !($res_wo['f_counter']%$allow)){
                            $is_allow = true;
                            break;
                        }
                    }
                }            
                else { //if is an allow day, check if there are limitation for this task
                    foreach ($deny_array as $deny) {
                        if($deny && !($res_wo['f_counter']%$deny)) {
                            $is_allow = false;
                            break;
                        }
                        else{
                            $is_allow = true;
                        }
                    }
                    if(empty($deny_array)) {
                        $is_allow = true;
                    }
                    if(!$is_allow) {
                        foreach($allow_array as $allow) {
                            if($allow && !($res_wo['f_counter']%$allow)){
                                $is_allow = true;
                                break;
                            }
                        }
                    }
                }            
                //final check    
                if(!$is_allow) continue;

                //hook task for every asset
                $per = array(
                    "f_code_main"=>$f_code,
                    'f_timestamp'=>$time,                    
                    'f_code_cross'=>$line_task['f_code'],
                    'f_group_code'=>$line_asset['f_code']
                );              
                $per['f_title'] = $line_task['f_title'];
                unset($per['f_id']);                
                $this->db->insert("t_pair_cross",$per);                   
            }            
        }

        foreach($res_pair as $pair) {
            $per = $pair;
            $per["f_code_main"] = $f_code;
            $per["f_timestamp"] = $time;                                
            unset($per['f_id']);
            $this->db->insert("t_pair_cross",$per); 
        }
        
        //finished to create the periodic, increment my counter for task sub cyclic
        $this->incrementCounter($line['f_code']);
        //and increment next due date and subsequent due date
        $this->addNextDueDate($line['f_code']);
        return $f_code;
    }
    
    private function incrementCounter($f_code) 
    {
        $select = new Zend_Db_Select($this->db);
        $res_wo = $select->from("t_workorders","f_counter")->where("f_code = $f_code")->query()->fetch();
        $new_wo = $res_wo;        
        $new_wo['f_counter'] = (int)$new_wo['f_counter'];
        $new_wo['f_counter'] = $new_wo['f_counter']+1;        
        $this->db->update("t_workorders",$new_wo,"f_code = $f_code");                
    }
    
    private function creationDate($data)
    {
        $this->db->beginTransaction();
        $this->db->insert("t_creation_date", $data);
        $f_code = $this->db->lastInsertId();
        $this->db->commit();
        return $f_code;
    }    
    
    private function addNextDueDate($f_code)
    {
        $select = new Zend_Db_Select($this->db);        
        //search the first due date and subsequent due date        
        try {
            $res_due = $select->from("t_periodics",array("f_start_date"))->where("f_code = ?",$f_code)                
                ->where("f_executed = 0")->where("f_start_date > ?",time())
                ->order("f_start_date ASC")->limit(2)
                ->query()->fetchAll();
        }catch(Exception $e) {};
        $custom = array();            
        $custom['fc_pm_next_due_date'] = $res_due[0]['f_start_date'];
        $custom['fc_pm_subsequent_due_date'] = $res_due[1]['f_start_date'];
        $this->db->update("t_workorders",$custom,"f_code = {$f_code}");        
    }
    
    
}