<?php
ini_set("display_errors","On");

require_once 'constants.php';

$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
$db_config_array = $config->toArray();
$db = $db_config_array['database']['params']['dbname'];    

new Zend_Application_Module_Autoloader(array(
    'namespace' => 'Mainsim_',
    'basePath'  => APPLICATION_PATH .'/modules/default',
    'resourceTypes' => array ()));


date_default_timezone_set("Europe/Paris");
$imported = false;
if(isset($_POST['db'])) {
    $start_date = 0;
    $end_date = time();    
    $number = 0;
    if(!empty($_POST['start_date'])) {
        $start_date = date('Y',strtotime($_POST['start_date'])) != '1970' ?strtotime($_POST['start_date']):0;
    }    
    if(!empty($_POST['end_date'])) {
        $end_date = date('Y',strtotime($_POST['end_date'])) != '1970'?strtotime($_POST['end_date']):time();
    }    
    if($_POST['number_of_creation']) {
        $number = (int)$_POST['number_of_creation'];        
    }
    $reader = new Mainsim_Model_MeterReader();
    $number_created = $reader->readMeters($start_date,$end_date,$number,true);
    $imported = true;
}

?>
<html>
    <head>
        <title>importer - since big bang to future</title>        
        <script type="text/javascript" src="jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="default.css" />
    </head>
    <body>     
        <div><a href="index.php"><< Back to main menu</a></div>
        <?php if(!$imported) :?>
            <div  style="margin-left:auto;margin-right:auto;width:50%">
                <form action="generatore_meter.php" method="POST">
                    <div style="text-align:center">Generate meters.</div>
                    <div style="text-align:center; margin-bottom : 35px;">
                        Insert start and end date and to create meter reading. 
                        if every fields are empty, i create every meter.                
                    </div> 
                    <div style="text-align:center">
                        <div>
                            <div>
                                <p style="float:left;width:200px"><label for="start_date">Start Date</label></p>
                                <p style="float:left;"><input type="text" name="start_date" /><label for="start_date"> (d-m-Y ex : 01-02-2012)</label></p>
                                <br clear="all" />
                                <p style="float:left;width:200px"><label for="end_date">End Date</label></p>
                                <p style="float:left;"><input type="text" name="end_date" /><label for="end_date"> (d-m-Y ex : 01-02-2012)</label></p>
                                <br clear="all" />
                                <p style="float:left;width:200px"><label for="limit">Number of meter generated</label> </p>                            
                                <p style="float:left;"><input type="text" name="number_of_creation" /><label for="end_date"> (if empty, system will generate every meter finded)</label></p>
                                <br clear="all" />
                            </div>                        
                        </div>
                    </div>
                    <div id="choose_action" style="margin-top : 20px;" >
                        <div style="text-align:center">
                            <input type ="submit" value="Create" /> <br />
                            <input type ="hidden" value="<?php echo $db;?>" name="db"/>
                        </div>
                        <br clear ="all" />
                    </div>
                </form>
            </div>
        <?php else : ?>
            <div style="margin-top : 50px;text-align : center"><?php echo $number_created; ?> meters created! <a href="index.php">Click here</a> to come back home</div>                            
        <?php endif; ?>
        <div id="logo">&nbsp;</div>
        <div id="credits">
            for any problem e-mail to <a href="mailto:info@unp.it">UNPlugged</a>
        </div>
    </body>
</html>
    