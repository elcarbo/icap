<?php

if(isset($_POST['filename']) && file_exists("downloads/{$_POST['filename']}")){
    $filename = $_POST['filename'];
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',true);
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    readfile("downloads/$filename");
    unlink("downloads/$filename");
}
else {
    echo 'file non trovato';
}