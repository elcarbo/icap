<?php

/*
 * metodi per esportare parti o tutto
 * mainsim in excel
 */
if(isset($_POST['start_download'])) {    
    ini_set("display_errors","On");
    $root = "";

    require_once $root.'constants.php';
    //Zend_session::writeClose();        
    
    require_once(APPLICATION_PATH.'/../library/phpExcel/PHPExcel.php');

    include (FOLDER.'asset.php');
    include (FOLDER.'workorders.php');
    include (FOLDER.'ui.php');
    include (FOLDER.'selector.php');
    include (FOLDER.'bookmark.php');
    include (FOLDER.'export.php');
    include (FOLDER.'utilities.php');
    include (FOLDER.'scheduler/mp_class.php');
    date_default_timezone_set("Europe/Paris");
    $ws = array(
        array('sheet_name'=>'t_wares_asset','table'=>'t_wares','type'=>1,'moduleName'=>'mdl_asset_tg'),        
        array('sheet_name'=>'t_wares_task','table'=>'t_wares','type'=>10),
        array('sheet_name'=>'t_wares_labor','table'=>'t_wares','type'=>2),    
        array('sheet_name'=>'t_wares_inventory','table'=>'t_wares','type'=>3),    
        array('sheet_name'=>'t_wares_tool','table'=>'t_wares','type'=>4),    
        array('sheet_name'=>'t_wares_contracts','table'=>'t_wares','type'=>7),    
        array('sheet_name'=>'t_wares_failurecodes','table'=>'t_wares','type'=>8),    
        array('sheet_name'=>'t_wares_vendors','table'=>'t_wares','type'=>6),    
        array('sheet_name'=>'t_wares_documents','table'=>'t_wares','type'=>5),    
        array('sheet_name'=>'t_wares_wizard','table'=>'t_wares','type'=>9),    
        array('sheet_name'=>'t_wares_user','table'=>'t_wares','type'=>16),    
    );

    $wos = array(
        array('sheet_name'=>'t_workorders_workorder','table'=>'t_workorders','type'=>1,'moduleName'=>'mdl_wo_tg'),        
        array('sheet_name'=>'t_workorders_periodic','table'=>'t_workorders','type'=>4,'moduleName'=>'mdl_wo_tg'),
        array('sheet_name'=>'t_workorders_on_condition','table'=>'t_workorders','type'=>6,'moduleName'=>'mdl_cond_tg'),    
        array('sheet_name'=>'t_workorders_periodic_maint','table'=>'t_workorders','type'=>3,'moduleName'=>'mdl_pm_tg')
    );

    $objPHPExcel = new PHPExcel();
    // Set properties        
    $objPHPExcel->getProperties()->setCreator("Mainsim");        
    $objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
    $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
    $db = Zend_Db::factory($config->database);    
    //punto primo : estraggo tutti i wares con i loro custom fields 
    $export = new export();
    $num_sheet = 0;
    foreach($ws as $params) {    
        $pos = 0;
        $inc = 5000;
        $row = 1;
        if($num_sheet) {
            $objPHPExcel->createSheet(NULL, $num_sheet);
            $objPHPExcel->setActiveSheetIndex($num_sheet);
        }
        $objPHPExcel->getActiveSheet()->setTitle($params['sheet_name']);
        do {
            $not_end = true;
            $result = $export->exportWares($params,$pos,$inc);        

            $objPHPExcel->setActiveSheetIndexByName($params['sheet_name']);
            //Inserisco l'intestazione
            $first = $objPHPExcel->getActiveSheet()->getHighestColumn();
            $column = $first;

            foreach($result as $line) {
                $column = $first;
                if($row == 1) {                
                    foreach($line as $key => $value) {
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("$column{$row}","$key",PHPExcel_Cell_DataType::TYPE_STRING);                
                        $column++;
                    }
                }
                $row++;       
                $column = $first;
                foreach($line as $field) {                
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("$column{$row}","$field",PHPExcel_Cell_DataType::TYPE_STRING);                
                    $column++;
                }            
            }                    
            if(count($result) < $inc) $not_end = false;
            $pos+=$inc;
        }while($not_end);
        $num_sheet++;        
    }

    //punto secondo : estraggo tutti i selectors
    $select = new Zend_Db_Select($db);
    $res_types = $select->from("t_selectors_types")->where("f_id > 0")->query()->fetchAll();
    foreach($res_types as $type) {    
        $row = 1;
        if($num_sheet) {        
            $objPHPExcel->createSheet(NULL, $num_sheet);
            $objPHPExcel->setActiveSheetIndex($num_sheet);
        } 
        $result = $export->exportSelector($type['f_id']);        
        $objPHPExcel->getActiveSheet()->setTitle("t_selector_{$type['f_id']}");    
        //Inserisco l'intestazione
        $first = $objPHPExcel->getActiveSheet()->getHighestColumn();
        $column = $first;    
        foreach($result as $line) {
            $column = $first;
            if($row == 1) {                
                foreach($line as $key => $value) {
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("$column{$row}","$key",PHPExcel_Cell_DataType::TYPE_STRING);                
                    $column++;
                }
            }
            $row++;       
            $column = $first;
            foreach($line as $field) {                
                $objPHPExcel->getActiveSheet()->setCellValueExplicit("$column{$row}","$field",PHPExcel_Cell_DataType::TYPE_STRING);                
                $column++;
            }            
        }
        $num_sheet++;        
    }

    //punto terzo : estraggo tutti i wo e le pm.
    $select = new Zend_Db_Select($db);

    foreach($wos as $params) {    
        $row = 1;
        if($num_sheet) {        
            $objPHPExcel->createSheet(NULL, $num_sheet);
            $objPHPExcel->setActiveSheetIndex($num_sheet);
        } 
        $pos = 0;
        $inc = 5000;
        do {
            $not_end = true;
            $result = $export->exportWorkorders($params);        
            $objPHPExcel->getActiveSheet()->setTitle($params['sheet_name']);    
            //Inserisco l'intestazione
            $first = $objPHPExcel->getActiveSheet()->getHighestColumn();
            $column = $first;    
            foreach($result as $line) {
                $column = $first;
                if($row == 1) {                
                    foreach($line as $key => $value) {
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("$column{$row}","$key",PHPExcel_Cell_DataType::TYPE_STRING);                
                        $column++;
                    }
                }
                $row++;       
                $column = $first;
                foreach($line as $field) {                
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("$column{$row}","$field",PHPExcel_Cell_DataType::TYPE_STRING);                
                    $column++;
                }            
            }
            if(count($result) < $inc) {
                $not_end = false;
            }
            $pos+=$inc;
        }while($not_end);
        $num_sheet++;
        break;
    }

    //parte quarta : metto gli engagement
    $select->reset();
    $f_codes = $select->from("t_workorders",array("f_code"))->where("f_type_id = 3")->where("f_active = 1")->query()->fetchAll();
    if($num_sheet) {        
        $objPHPExcel->createSheet(NULL, $num_sheet);
        $objPHPExcel->setActiveSheetIndex($num_sheet);
    }     

    $result = $export->exportEngagements($f_codes);        
    $objPHPExcel->getActiveSheet()->setTitle("t_engagement");  
    $first = $objPHPExcel->getActiveSheet()->getHighestColumn();
    $row = 1;
    foreach($result as $line) {    
        $column = $first;
        if($row == 1) {                
            foreach($line as $key => $value) {
                $objPHPExcel->getActiveSheet()->setCellValueExplicit("$column{$row}","$key",PHPExcel_Cell_DataType::TYPE_STRING);                
                $column++;
            }
        }
        $row++;       
        $column = $first;
        foreach($line as $field) {                
            $objPHPExcel->getActiveSheet()->setCellValueExplicit("$column{$row}","$field",PHPExcel_Cell_DataType::TYPE_STRING);                
            $column++;
        }    
    }
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    //$objWriter->save('php://output');
    $filename = "mainsim_loading_template_".time().".xlsx";
    $objWriter->save("downloads/$filename");
    $objPHPExcel->disconnectWorksheets();
    echo json_encode(array('filename'=>$filename));die;
}
?>
<html>
    <head>
        <title>Download dati mainsim</title>
        <script type="text/javascript" src="jquery.js"></script>
        <script>
            $(document).ready(function(){
                $.post("exporter.php",{start_download : 1},function(response){
                    files = jQuery.parseJSON(response);                    
                    window.location.href = "downloads/"+files.filename;                    
                });                
            });    
        </script>
    </head>
    <body>
        <div>Stiamo scaricando i dati mainsim sul file excel. L'operazione potrebbe richiedere qualche minuto.
            Prego attendere.
        </div>        
    </body>
</html>