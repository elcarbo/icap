<?php 
require_once 'constants.php';
if(isset($_POST['id'])) {
    //faccio la chiamata
    $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');        
    $db = Zend_Db::factory($config->database);
    $select = new Zend_Db_Select($db);	
    $res = $select->from("t_queue",array("f_active",'f_message','f_file_name','f_id','f_complete_sheet'))
            ->where("f_id = '".$_POST['id']."'")->query()->fetchAll();
    $response = array();
    if(empty($res)) {
        $response['resp'] = 'ok';
    }
    else {
        $queue = $res[0];            
        if($queue['f_active'] == -1) {                
            if(file_exists(BASE_PATH."/uploads/{$queue['f_file_name']}")) {
                unlink(BASE_PATH."/uploads/{$queue['f_file_name']}");
            }
            $db->delete("t_queue","f_id = {$queue['f_id']}");
        }            
        $response['resp'] = $queue['f_active'];
        $response['message'] = $queue['f_message'];
        $response['complete_sheets'] = $queue['f_complete_sheet'];
    }
    echo json_encode($response);
    $db->closeConnection();
    die;
}
$sheets = explode(',',urldecode($_GET['sheets']));
?>

<html>
    <head>
        <title>Updater - follow progress of your request</title>
        <link rel="stylesheet" src="default.css" />
        <script type="text/javascript" src="jquery.js"></script>
        <script>
        var id = '<?php echo isset($_GET['id'])?$_GET['id'] : 0; ?>';                
        var start_imp = '<?php echo isset($_GET['start_imp'])?1: 0; ?>';
        var sheets_str = '<?php echo urldecode($_GET['sheets']);?>';                
        var sheets = sheets_str.split(',');

        function post(sheets,id,i,pos) {
            if(i >= sheets.length){ 
                $('#instant-message').remove();
                $('#text').html("PHP production is proud to tell you that everything has been imported. Thank you to choice us services. See you next time! <br /> <a href='load.php'>Clik here</a> to import something else. ");
                return; 
            }
            $('#'+sheets[i]+"-loader").css('display','');
            url = "instant_data_loader.php";
            inc = 1500;
            if(sheets[i] == 't_cross_wares_selectors' || sheets[i] == 't_cross_workorders_wares' 
                || sheets[i] == 't_cross_wares_wares' || sheets[i] == 't_cross_workorders_selectors') {
                inc = 5000;
            }
            if(sheets[i] == 't_engagement' || sheets[i] == 't_engagement_meter' ) {
                inc = 10;// engagements could be slow so i reduce the number of rec per time   
            }
            var data = {sheet : sheets[i], id : id,pos : pos,inc : inc};
            data.last_call =  i==(sheets.length - 1)?1:0;            
            $.post(url,data,function(response) {  
                try {
                    response_js = jQuery.parseJSON(response);                        
                }catch(e) {
                    $('#'+sheets[i]+"-loader").css('display','none');
                    $('#'+sheets[i]+"-ko").css('display','');
                    $('#text-error').html("<div>"+response+"</div>");
                    $.post("deleteFromCrash.php",data);
                    return 'ko';
                }
                if(response_js.response != 'continue') {
                    $('#'+sheets[i]+"-loader").css('display','none');
                    $('#'+sheets[i]+"-"+response_js.response).css('display','');
                    $('#'+sheets[i]+"-message").text('');
                    if(response_js.response == 'ko') {return 'ko';}
                    i++;
                    post(sheets,id,i,1);                    
                }
                else { // continue to create
                    if(response_js.isFinished == 1) {
                        post(sheets,id,i,0);// if pos == 0, create part relative parent/icons/bookmarks etc..
                        $('#'+sheets[i]+"-message").text('Finishing import');
                    }
                    else {
                        $('#'+sheets[i]+"-message").text('Loaded '+(pos+inc)+" rows");
                        pos = pos+inc;
                        post(sheets,id,i,pos);
                    }
                }
            });
        }

        $(document).ready(function(){
            if(start_imp != 0) {
                post(sheets,id,0,1);                    
            }            
            if(id) { 
                var sheet_loaded = 0;                    
                int = setInterval(function(){                        
                    $.post("updater.php",{id : id},function(response) {                            
                        response = $.parseJSON(response);
                        if(response.resp == 'ok') {
                                clearInterval(int);
                                $('#'+sheets[sheet_loaded]+"-loader").css('display','none');
                                $('#'+sheets[sheet_loaded]+"-ok").css('display','');
                                $('#img-loading').remove();
                                $('#instant-message').remove();
                                $('#text').html("PHP production is proud to tell you that everything has been imported. Thank you to choice us services. See you next time! <br /> <a href='load.php'>Clik here</a> to import something else. ");							
                        }
                        else if(response.resp == 0 && start_imp == 0) {
                            $('#text').html("We announce the assumption of your request. A team of highly trained monkeys has been dispatched to deal with this situation. <br />Thank you");
                            if(start_imp == 0) { //only if this is cron importer                            
                                $('#'+sheets[sheet_loaded]+"-loader").css('display','');
                            }
                            if(start_imp == 0) { //only if this is cron importer
                                var completes = response.complete_sheets.split(',');
                                if (response.complete_sheets != '' && ((completes.length) != sheet_loaded)) {
                                    //remove old waiting and declare this sheet complete
                                    $('#'+sheets[sheet_loaded]+"-loader").css('display','none');
                                    $('#'+sheets[sheet_loaded]+"-ok").css('display','');
                                    sheet_loaded++;
                                    $('#'+sheets[sheet_loaded]+"-loader").css('display','');
                                }                                
                            }                            
                        }
                        else if(response.resp == -1) {
                            clearInterval(int);
                            $('#img-loading').remove();
                            //remove old waiting and declare this sheet complete
                            $('#'+sheets[sheet_loaded]+"-loader").css('display','none');
                            $('#'+sheets[sheet_loaded]+"-ko").css('display','');
                            if(start_imp == 0) {                                
                                $('#instant-message').remove();
                            }
                            $('#text-error').html(response.message);
                        }
                    });
                },2000);
                
            }	
        });  
        </script>                
    </head>
    <body>
        <div>
            <a href="load.php">Back to load page</a>
        </div>
        <div>                
            <?php if(!isset($_GET['start_imp'])) :?>		
                <div id="text" style="margin-left:auto;margin-right:auto;width:80%;text-align:center;">Welcome in mainsim updater wainting room. We will inform you when the request will be accecpt and closed. Thank you </div>                                
            <?php else : ?> 
                <div id="text" style="margin-left:auto;margin-right:auto;width:80%;text-align:center;"></div>
                <p id="instant-message" style="margin-left:auto;margin-right:auto;width:80%;text-align:center;">
                    In this time you load data directly into db. Please <strong>don't leave</strong> till the closed of process.
                </p>                
            <?php endif;?>            
                <div id="loader_container" style="width:500px;margin-left:auto;margin-right:auto;">
                    <?php foreach($sheets as $sheet):?>
                        <div class="list">
                            <p style="float:left;width:50%"><?php echo $sheet;?></p>
                            <p style="float:left;width:50%">
                                <img id="<?php echo $sheet;?>-loader" src="images/ajax-loader.gif" style="display:none;width:25px;height:25px;"/>
                                <img id="<?php echo $sheet;?>-ok" src="images/tick.png" style="display:none;width:25px;height:25px;"/>
                                <img id="<?php echo $sheet;?>-ko" src="images/dispose.png" style="display:none;width:25px;height:25px;"/>
                                <div id="<?php echo $sheet;?>-message"></div>
                            </p><br clear="all" />
                        </div>                        
                    <?php endforeach;?>
                </div>
                <div id="text-error" style="margin-left:auto;margin-right:auto;width:80%;text-align:center;"></div>                
        </div>
    </body>
</html>