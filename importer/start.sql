-- Users
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (1, 'WARES', 'USER', 1, 1354701482, 1, 9, 1, 'Mainsim Administrator', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (2, 'WARES', 'USER', 2, 1354701482, 1, 9, 1, 'Mainsim Configurator', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (3, 'WARES', 'USER', 3, 1354701482, 1, 9, 1, 'Mainsim Support', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (4, 'WARES', 'USER', 4, 1354701482, 1, 9, 1, 'Mainsim Help Desk', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (5, 'WARES', 'USER', 5, 1354701482, 1, 9, 1, 'Mainsim Quality Assurance', '', -1, -1);

INSERT INTO t_wares (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES (NULL, 1, 16, 1354701482, 1, 1, 1); 
INSERT INTO t_wares (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES (NULL, 2, 16, 1354701482, 2, 1, 1);
INSERT INTO t_wares (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES (NULL, 3, 16, 1354701482, 3, 1, 1);
INSERT INTO t_wares (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES (NULL, 4, 16, 1354701482, 4, 1, 1);
INSERT INTO t_wares (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES (NULL, 5, 16, 1354701482, 5, 1, 1);

INSERT INTO t_wares_parent (f_id, f_code, f_parent_code, f_timestamp, f_active) VALUES
(NULL, 1, 0, 1354701482, 1),
(NULL, 2, 0, 1354701482, 1),
(NULL, 3, 0, 1354701482, 1),
(NULL, 4, 0, 1354701482, 1),
(NULL, 5, 0, 1354701482, 1); 

INSERT INTO t_custom_fields (f_id, f_code, f_timestamp) VALUES
(NULL, 1, 1354701482),
(NULL, 2, 1354701482),
(NULL, 3, 1354701482),
(NULL, 4, 1354701482),
(NULL, 5, 1354701482);

INSERT INTO t_users(f_id, f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES(NULL, 1, 'admin', '8a4dea98f8dac77d342f4cc785547e72b', 'Mainsim', 'Administrator', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');
INSERT INTO t_users(f_id, f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES(NULL, 2, 'config', 'd13bc37d8f70a315b04a20d6a9518b22a', 'Mainsim', 'Configurator', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');
INSERT INTO t_users(f_id, f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES(NULL, 3, 'support', '82482bfbc5e9a8018ee208b0d053ba0c4', 'Mainsim', 'Support', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');
INSERT INTO t_users(f_id, f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES(NULL, 4, 'helpdesk', 'b5a52c8852d5375d2f0d4f101452c3e30', 'Mainsim', 'Help Desk', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');
INSERT INTO t_users(f_id, f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES(NULL, 5, 'quality', 'b5a52c8852d5375d2f0d4f101452c3e30', 'Mainsim', 'Quality Assurance', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');

-- -----------------------------------------------------------------------------------------

-- Data manager selectors
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (6, 'SELECTORS', 'DATA MANAGER', 7, 1354701482, 1, 9, 1, 'Import', '', -1, 0);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (7, 'SELECTORS', 'DATA MANAGER', 8, 1354701482, 1, 9, 1, 'Export', '', -1, 0);

INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 6, -1, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 7, -1, 1354701482, 1, 1, 1, NULL);

INSERT INTO t_selectors_parent (f_id, f_code, f_parent_code, f_timestamp, f_active) VALUES
(NULL, 6, 0, 1354701482, 1),
(NULL, 7, 0, 1354701482, 1);

INSERT INTO t_custom_fields(f_id, f_code, f_timestamp, fc_level_level) VALUES
(NULL, 6, 1354701482, -1),
(NULL, 7, 1354701482, -1);

-- -----------------------------------------------------------------------------------------

-- User levels (roles)
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (8, 'SYSTEM', 'LEVEL', '8', '1354701482', '1', 9, 1, 'Administrator', '', -1, 0);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (9, 'SYSTEM', 'LEVEL', '9', '1354701482', '1', 9, 1, 'Super-User', '', -1, 0);

INSERT INTO t_systems (f_id, f_code, f_type_id, f_timestamp, f_user_id) VALUES (NULL, 8, 4, 1354701482, 1),(NULL, 9, 4, 1354701482, 1);

INSERT INTO t_systems_parent (f_id, f_code, f_parent_code, f_timestamp, f_active) VALUES (NULL, 8, 0, 1354701482, 1);
INSERT INTO t_systems_parent (f_id, f_code, f_parent_code, f_timestamp, f_active) VALUES (NULL, 9, 0, 1354701482, 1);

INSERT INTO t_custom_fields(f_id, f_code, f_timestamp, fc_level_level) VALUES
(NULL, 8, 1354701482, -1),
(NULL, 9, 1354701482, 2);

-- -----------------------------------------------------------------------------------------

-- Roots for standard selectors
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (10, 'SELECTORS', 'SELECTOR 1', '10', '1354701482', '1', 9, 1, 'Selector 01', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (11, 'SELECTORS', 'SELECTOR 2', '11', '1354701482', '1', 9, 1, 'Selector 02', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (12, 'SELECTORS', 'SELECTOR 3', '12', '1354701482', '1', 9, 1, 'Selector 03', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (13, 'SELECTORS', 'SELECTOR 4', '13', '1354701482', '1', 9, 1, 'Selector 04', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (14, 'SELECTORS', 'SELECTOR 5', '14', '1354701482', '1', 9, 1, 'Selector 05', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (15, 'SELECTORS', 'SELECTOR 6', '15', '1354701482', '1', 9, 1, 'Selector 06', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (16, 'SELECTORS', 'SELECTOR 7', '16', '1354701482', '1', 9, 1, 'Selector 07', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (17, 'SELECTORS', 'SELECTOR 8', '17', '1354701482', '1', 9, 1, 'Selector 08', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (18, 'SELECTORS', 'SELECTOR 9', '18', '1354701482', '1', 9, 1, 'Selector 09', '', -1, -1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability) VALUES (19, 'SELECTORS', 'SELECTOR 10', '19', '1354701482', '1', 9, 1, 'Selector 10', '', -1, -1);

INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 10, 1, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 11, 2, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 12, 3, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 13, 4, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 14, 5, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 15, 6, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 16, 7, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 17, 8, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 18, 9, 1354701482, 1, 1, 1, NULL);
INSERT INTO t_selectors (f_id, f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES (NULL, 19, 10, 1354701482, 1, 1, 1, NULL);

INSERT INTO t_selectors_parent (f_id, f_code, f_parent_code, f_timestamp, f_active) VALUES
(NULL, 10, 0, 1354701482, 1),
(NULL, 11, 0, 1354701482, 1),
(NULL, 12, 0, 1354701482, 1),
(NULL, 13, 0, 1354701482, 1),
(NULL, 14, 0, 1354701482, 1),
(NULL, 15, 0, 1354701482, 1),
(NULL, 16, 0, 1354701482, 1),
(NULL, 17, 0, 1354701482, 1),
(NULL, 18, 0, 1354701482, 1),
(NULL, 19, 0, 1354701482, 1);

INSERT INTO t_custom_fields(f_id, f_code, f_timestamp, fc_level_level) VALUES
(NULL, 10, 1354701482, -1),
(NULL, 11, 1354701482, -1),
(NULL, 12, 1354701482, -1),
(NULL, 13, 1354701482, -1),
(NULL, 14, 1354701482, -1),
(NULL, 15, 1354701482, -1),
(NULL, 16, 1354701482, -1),
(NULL, 17, 1354701482, -1),
(NULL, 18, 1354701482, -1),
(NULL, 19, 1354701482, -1);

-- -----------------------------------------------------------------------------------------

-- Cross user X level

INSERT INTO t_wares_systems (f_id, f_ware_id, f_system_id, f_timestamp) VALUES
(null, 1, 8, 1354701482),
(null, 2, 8, 1354701482),
(null, 3, 8, 1354701482),
(null, 4, 8, 1354701482),
(null, 5, 8, 1354701482);