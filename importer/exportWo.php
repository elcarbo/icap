<?php

require_once 'constants.php';

$limitSelect = 1000;
$workorderTypes = "(1,4,6,7,10,12,13)";
$errors = "0";

$db_old = Zend_Db::factory("PDO_MYSQL",array(
    'dbname'=>'mainsim3_tmp_3',
    'host'=>'192.168.0.12',
    'username'=>'root',
    'password'=>'1881'    
));          
$sel_old = new Zend_Db_Select($db_old);
//-------------------------------------------
$db_new = Zend_Db::factory("PDO_MYSQL",array(
    'dbname'=>'mainsim3_export_wo_test',
    'host'=>'192.168.0.12',
    'username'=>'root',
    'password'=>'1881'    
));

//get cols number of t_creation_date
$selectColsCounter = "show columns from t_creation_date";
// old db
$resultColsCounter = $db_old->query($selectColsCounter)->fetchAll();
$table['t_creation_date']['origin']['cols'] = count($resultColsCounter);
for($i = 0; $i < count($resultColsCounter); $i++){
    $table['t_creation_date']['origin']['fields']['__cd__' . $resultColsCounter[$i]['Field']] = $resultColsCounter[$i]['Field'];
}
// new db
$resultColsCounter = $db_new->query($selectColsCounter)->fetchAll();
$table['t_creation_date']['destination']['cols'] = count($resultColsCounter);
for($i = 0; $i < count($resultColsCounter); $i++){
    $table['t_creation_date']['destination']['fields'][] = $resultColsCounter[$i]['Field'];
}


//get cols number of t_workorders
$selectColsCounter = "show columns from t_workorders";
// old db
$resultColsCounter = $db_old->query($selectColsCounter)->fetchAll();
$table['t_workorders']['origin']['cols'] = count($resultColsCounter);
for($i = 0; $i < count($resultColsCounter); $i++){
    $table['t_workorders']['origin']['fields']['__wo__' . $resultColsCounter[$i]['Field']] = $resultColsCounter[$i]['Field'];
}
// new db
$resultColsCounter = $db_new->query($selectColsCounter)->fetchAll();
$table['t_workorders']['destination']['cols'] = count($resultColsCounter);
for($i = 0; $i < count($resultColsCounter); $i++){
    $table['t_workorders']['destination']['fields'][] = $resultColsCounter[$i]['Field'];
}

//get cols number of t_custom_fields
$selectColsCounter = "show columns from t_custom_fields";
// old db
$resultColsCounter = $db_old->query($selectColsCounter)->fetchAll();
$table['t_custom_fields']['origin']['cols'] = count($resultColsCounter);
for($i = 0; $i < count($resultColsCounter); $i++){
    $table['t_custom_fields']['origin']['fields']['__cu__' . $resultColsCounter[$i]['Field']] = $resultColsCounter[$i]['Field'];
}
// new db
$resultColsCounter = $db_new->query($selectColsCounter)->fetchAll();
$table['t_custom_fields']['destination']['cols'] = count($resultColsCounter);
for($i = 0; $i < count($resultColsCounter); $i++){
    $table['t_custom_fields']['destination']['fields'][] = $resultColsCounter[$i]['Field'];
}

// total workokorders
$selectCountWorkorders = $db_old->select()
                                ->from(array('wo' => 't_workorders'), array('num' => 'COUNT(*)'))
                                ->join(array('cu' => 't_custom_fields'), 'wo.f_timestamp = cu.f_timestamp', array())
                                ->where("wo.f_type_id in ". $workorderTypes);
                                //->where("wo.f_code = 6867");
$resultCountWorkorders = $db_old->query($selectCountWorkorders)->fetchAll();

// max f_order for pair cross category 'normal' (from creation_date table on new database)
$selectMaxPairCrossOrder = $db_new->select()
                                  ->from('t_creation_date', array(new Zend_Db_Expr('MAX(f_order) AS max')))
                                  ->where("f_type = 'PAIR CROSS'")
                                  ->where("f_category = 'NORMAL'");
$resultMaxPairCrossOrder = $db_new->query($selectMaxPairCrossOrder)->fetchAll();
$maxPairCrossOrder = $resultMaxPairCrossOrder[0]['max'];

// output
echo "\n\n" .
     "**********************************\n" .
     "WORKORDERS EXPORTER\n" .
     "**********************************\n\n";


$processedWorkorders = 0;
$offset = 0;
$codeArray = array();
while($processedWorkorders < $resultCountWorkorders[0]['num']){
    
    // get workorders data
    $selectWorkorders = $db_old->select()
                               ->from(array('wo' => 't_workorders'), $table['t_workorders']['origin']['fields'])
                               ->join(array('cd' => 't_creation_date'), 'cd.f_id = wo.f_code', $table['t_creation_date']['origin']['fields'])
                               ->join(array('cu' => 't_custom_fields'), 'wo.f_timestamp = cu.f_timestamp', $table['t_custom_fields']['origin']['fields'])
                               ->where("wo.f_type_id in " . $workorderTypes)
                               //->where("wo.f_code = 6867")
                               ->order(array('wo.f_code'))
                               ->limit($limitSelect, $offset);
    $resultWorkorders = $db_old->query($selectWorkorders)->fetchAll();
    
    // copy data into destination database
    for($i = 0; $i < count($resultWorkorders); $i++){
        
        // f_code not processed yet --> insert into creation_date table and set in currentOldCode variable
        if($currentOldCode != $resultWorkorders[$i]['__wo__f_code']){
            
            // update current old code
            $currentOldCode = $resultWorkorders[$i]['__wo__f_code'];
            
            // set variable to copy workorders-wares relations one time only (see line 159)
            $copyWorkordersWaresRelations = true;

            // ***************************************************
            // copy to t_creation_date table
            // ***************************************************
            // map destination fields with origin value. in case of missing field its value will be set to ''
            $data = null;
            for($j = 0; $j < count($table['t_creation_date']['destination']['fields']); $j++){
                $data[$table['t_creation_date']['destination']['fields'][$j]] = $resultWorkorders[$i]['__cd__' . $table['t_creation_date']['destination']['fields'][$j]] ? $resultWorkorders[$i]['__cd__' . $table['t_creation_date']['destination']['fields'][$j]] : '';
            }
            unset($data['f_id']);
            $db_new->insert('t_creation_date', $data);
            // get new f_code
            $newCode = $db_new->lastInsertId();
        }
        // ***************************************************
        // copy to t_workorders
        // ***************************************************
        // map destination fields with origin value. in case of missing field its value will be set to ''
        $data = null;
        for($j = 0; $j < count($table['t_workorders']['destination']['fields']); $j++){
            $data[$table['t_workorders']['destination']['fields'][$j]] = $resultWorkorders[$i]['__wo__' . $table['t_workorders']['destination']['fields'][$j]] ? $resultWorkorders[$i]['__wo__' . $table['t_workorders']['destination']['fields'][$j]] : '';
        }
        unset($data['f_id']);
        $data['f_code'] = $newCode;
        $db_new->insert('t_workorders', $data);
        // ***************************************************
        // copy to t_custom_fields
        // ***************************************************
        $data = null;
        for($j = 0; $j < count($table['t_custom_fields']['destination']['fields']); $j++){
            $data[$table['t_custom_fields']['destination']['fields'][$j]] = $resultWorkorders[$i]['__cu__' . $table['t_custom_fields']['destination']['fields'][$j]] ? $resultWorkorders[$i]['__cu__' . $table['t_custom_fields']['destination']['fields'][$j]] : '';
        }
        unset($data['f_id']);
        $data['f_code'] = $newCode;
        $db_new->insert('t_custom_fields', $data);
        // output
        echo "workorders: " . ($processedWorkorders + 1) . "/" . $resultCountWorkorders[0]['num'] . " (old code = " . $currentOldCode . ", id = " . $resultWorkorders[$i]['__wo__f_id'] . ")\n";
        
        // ***************************************************
        // copy workorders-wares relations
        // ***************************************************
        // one time copy
        if($copyWorkordersWaresRelations){
            // get wares related to workorder 'i'
            $selectWaresWorkorders = $db_old->select()
                                            ->from(array('wawo' => 't_ware_wo'))
                                            ->join(array('cu' => 't_custom_fields'), 'wawo.f_ware_id = cu.f_code', array('fc_imp_code_wares_excel','fc_imp_type_id_wares_excel'))
                                            ->where('wawo.f_wo_id = ?', $currentOldCode);
            $resultWaresWorkorders = $db_old->query($selectWaresWorkorders)->fetchAll();
            if(count($resultWaresWorkorders) > 0){
               // where clause to retrieve wares codes in ne database mapping 'fc_imp_code_wares_excel' and 'fc_imp_type_id_wares_excel' fields
               $aux = null;
               for($k = 0; $k < count($resultWaresWorkorders); $k++){
                   if(!$aux[$resultWaresWorkorders[$k]['fc_imp_code_wares_excel'] . '_' . $resultWaresWorkorders[$k]['fc_imp_type_id_wares_excel']]){
                        $aux[$resultWaresWorkorders[$k]['fc_imp_code_wares_excel'] . '_' . $resultWaresWorkorders[$k]['fc_imp_type_id_wares_excel']] = '(fc_imp_code_wares_excel = ' . $resultWaresWorkorders[$k]['fc_imp_code_wares_excel'] . ' AND fc_imp_type_id_wares_excel = ' . $resultWaresWorkorders[$k]['fc_imp_type_id_wares_excel'] . ')';
                   }
               }
               $whereExcel = implode(' OR ', $aux);
               // get excel code - ware code association in new database
               $selectNewExcelWares = $db_new->select()
                                             ->from('t_custom_fields', array('f_code', 'fc_imp_type_id_wares_excel', 'fc_imp_code_wares_excel'))
                                             ->where($whereExcel);
               $resultNewExcelWares = $db_new->query($selectNewExcelWares)->fetchAll();
               for($k = 0; $k < count($resultNewExcelWares); $k++){
                   $mapExcelCode[$resultNewExcelWares[$k]['fc_imp_code_wares_excel'] . '_' . $resultNewExcelWares[$k]['fc_imp_type_id_wares_excel']] = $resultNewExcelWares[$k]['f_code'];
               }

               // insert into ware_wo table 
               for($k = 0; $k < count($resultWaresWorkorders); $k++){
                   try{
                        $data = null;
                        $data['f_wo_id'] = $newCode;
                        $data['f_ware_id'] = $mapExcelCode[$resultWaresWorkorders[$k]['fc_imp_code_wares_excel'] . '_' . $resultWaresWorkorders[$k]['fc_imp_type_id_wares_excel']];
                        $data['f_level'] = $resultWaresWorkorders[$k]['f_level'];
                        $data['f_active'] = $resultWaresWorkorders[$k]['f_active'];
                        $data['f_timestamp'] = $resultWaresWorkorders[$k]['f_timestamp'];
                        $db_new->insert('t_ware_wo', $data);
                        // output
                        echo "\r\tworkoder-wares relations " . ($k + 1) . "/" . count($resultWaresWorkorders) . " (old ware code = " . $resultWaresWorkorders[$k]['f_ware_id'] . ", id = " . $resultWaresWorkorders[$k]['f_id'] . ")               ";
                   }
                   catch(Exception $e){
                        // output
                        $message = "[WORKORDER-WARE RELATIONS]\n" .
                                   "\t new : f_wo_id = " . $data['f_wo_id'] . "\tf_ware_id = " . $data['f_ware_id'] . "\tf_level  = " .  $data['f_level'] . "\tf_active = " . $data['f_active'] . "\tf_timestamp = " . $data['f_timestamp'] . "\n" .
                                   "\t old : f_wo_id = " . $resultWaresWorkorders[$k]['f_wo_id'] . "\tf_ware_id = " . $resultWaresWorkorders[$k]['f_ware_id'] . "\tf_level  = " .  $resultWaresWorkorders[$k]['f_level'] . "\tf_active = " . $resultWaresWorkorders[$k]['f_active'] . "\tf_timestamp = " . $resultWaresWorkorders[$k]['f_timestamp'] . "\n";
                        file_put_contents('exportWo.txt', $message, FILE_APPEND);
                        $errors++;
                   }
               }

               // ***************************************************
               // copy pair cross relations
               // ***************************************************
               //output
               echo "\n";
               for($k = 0; $k < count($resultWaresWorkorders); $k++){

                   // get pair relations of a ware-workorder copy
                   $selectPairCross = $db_old->select()
                                             ->from('t_pair_cross')
                                             ->where('f_code_main = ?', $currentOldCode)
                                             ->where('f_code_cross = ?', $resultWaresWorkorders[$k]['f_ware_id']);
                   $resultPairCross = $db_old->query($selectPairCross)->fetchAll();

                   if(count($resultPairCross) > 0 ){
                        for($r = 0; $r < count($resultPairCross); $r++){
                            //update max paircross order
                            $maxPairCrossOrder++;
                            $timestamp = time();

                            // insert into creation_date table (new db)
                            $data = null;
                            $data['f_type'] = 'PAIR CROSS';
                            $data['f_category'] = 'NORMAL';
                            $data['f_order'] = $maxPairCrossOrder;
                            $data['f_creation_date'] = $timestamp;
                            $data['f_creation_user'] = 1;
                            $db_new->insert('t_creation_date', $data);
                            // get new f_code
                            $newPairCrossCode = $db_new->lastInsertId();

                            // insert into t_paircross table (new db)
                            try{
                                $data = $resultPairCross[$r];
                                unset($data['f_id']);
                                $data['f_timestamp'] = $timestamp;
                                $data['f_code_main'] = $newCode;
                                $data['f_code_cross'] = $mapExcelCode[$resultWaresWorkorders[$r]['fc_imp_code_wares_excel'] . '_' . $resultWaresWorkorders[$r]['fc_imp_type_id_wares_excel']];
                                $data['f_code'] = $newPairCrossCode;
                                $db_new->insert('t_pair_cross', $data);
                                // output
                                echo "\r\t\tpair cross relation " . ($r + 1) . "/" . count($resultPairCross) . " (old pair code = " . $resultPairCross[$r]['f_code'] . ", id = " . $resultPairCross[$r]['f_id'] . ")               ";
                            }
                            catch(Exception $e){
                                // output
                                $message = "[WORKORDER-WARE PAIRCROSS]\n" .
                                           "\t new : f_timestamp = " . $data['f_timestamp'] . "\tf_code_main = " . $data['f_code_main'] . "\tf_code_cross  = " .  $data['f_code_cross'] . "\tf_code = " . $data['f_code'] . "\n" .
                                           "\t old : f_timestamp = " . $resultPairCross[$r]['f_timestamp'] . "\tf_code_main = " . $resultPairCross[$r]['f_code_main'] . "\tf_code_cross  = " .  $resultPairCross[$r]['f_code_cross'] . "\tf_code = " . $resultPairCross[$r]['f_code'] . "\n" .
                                file_put_contents('exportWo.txt', $message, FILE_APPEND);
                                $errors++;
                            }
                        }
                   }
                   else{
                        // output
                        echo "\r\tno pair cross relation found (old workorder code = " . $currentOldCode . ", ware code = " . $resultWaresWorkorders[$k]['f_ware_id'] . ")                         ";
                   }
               }

            }
            else{
                // output
                echo "\tno workorder-asset relation found";
            }
            // one time copy for i-th workerorder (see line 116-159)
            $copyWorkordersWaresRelations = false;
        }
        else{
            // output
            echo "\tworkorder relations already copied";
        }
        
        //output
        echo "\n";
        $processedWorkorders++;
    }
    
    // update offset
    $offset += $limitSelect; 
}

// echo 
echo "errori : " . $errors . "(see error log file)"; 