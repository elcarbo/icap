<?php

defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

//list of mandatory check made from system
$default_check_list = array(    
    'f_code'=>array('check'=>'uniqueCode','message'=>'f_code must be unique and not empty'),
    'f_wf_id'=>array('check'=>'wf_exist','message'=>"f_wf_id must exist.If you don't know what workflow assign,leave the column f_wf_id empty."),
    'f_phase_id'=>array('check'=>'phase_exist','message'=>"f_phase_id must exist.If you don't know what workflow assign,leave the column f_wf_id empty."),
    "f_start_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "f_end_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_wo_starting_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_wo_ending_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_offer_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_sch_end_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_contract_starting_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_contract_ending_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_imp_start_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_imp_end_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "f_title"=>array('check'=>'empty_field','message'=>'Mainsim don\'t allow empty f_title. '),
    "fc_pm_start_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "fc_pm_end_date"=>array('check'=>'valid_date','message'=>'Mainsim allow only date with english format (mm/dd/YYYY). '),
    "f_type_id_wares"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
    "f_type_id_selector"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
    "f_type_id_wares_master"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
    "f_type_id_wares_slave"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
    "f_type_id_workorder"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"')    
);
    

 
define('DEBUG',true);
