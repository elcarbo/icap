<?php

require_once 'constants.php';

if(!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->f_level != -1) {
    header("Location:deny.html");die;
}

$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
$conn = Zend_Db::factory($config->database);          

if(isset($_GET['id'])) { // if user pass id, delete it from t_queue **** TODO : remove file *****
    $id = $conn->quote($_GET['id']);
    $conn->delete("t_queue","f_id = $id");
}
//get all queue
$active_queue = array();
$res_active_queue = $conn->query("SELECT * FROM t_queue")->fetchAll();
foreach($res_active_queue as $line_queue) {
    $active_queue[] = $line_queue;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="imagetoolbar" CONTENT="no" />
        <title>Mainsim Importer</title>
        <script type="text/javascript" src="jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="default.css" />                
    </head>
    <body>
        <div><a href="index.php"><< Back to main menu</a></div>                
        <div>
            <?php if(!empty($active_queue)):?>
                <div> 
                    <table style='margin-left : auto;margin-right : auto;'>
                        <tr>
                            <th>f_id</th><th>f_file_name</th>
                            <th>f_sheets</th><th>f_active</th>
                            <th>f_message</th><th>f_timestamp</th>
                            <th>f_rollback</th><th>f_finish_sheets</th>                        
                            <th>Action</th>                        
                        </tr>
                        <?php foreach($active_queue as $queue) : ?>
                            <tr>
                                <td style='text-align: center'><?php echo $queue['f_id'];?></td>
                                <td style='text-align: center'><?php echo $queue['f_file_name'];?></td>
                                <td style='text-align: center'><?php echo $queue['f_sheets'];?></td>
                                <td style='text-align: center'><?php echo $queue['f_active'];?></td>
                                <td style='text-align: center'><?php echo $queue['f_message'];?></td>
                                <td style='text-align: center'><?php echo date('m/d/Y',$queue['f_timestamp']);?></td>
                                <td style='text-align: center'><?php echo $queue['f_rollback']?></td>
                                <td style='text-align: center'><?php echo $queue['f_finish_sheets']?></td>
                                <td style='text-align: center'>
                                    <a href="active_queue.php?id=<?php echo $queue['f_id']?>"><img src="images/dispose.png" alt="remove"/></a>
                                    <a href="updater.php?id=<?php echo $queue['f_id']?>&sheets=<?php echo urlencode($queue['f_sheets']);?>"><img src="images/search.png" alt="watch"/></a>                                
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </table>                
                </div>
            <?php else : ?>            
                <div style='text-align: center'> No active queue present now. You can import without problems. </div>
            <?php endif; ?>
        </div>        
        <div id="logo">&nbsp;</div>
        <div id="credits">
            for any problem e-mail to <a href="mailto:info@unp.it">UNPlugged</a>
        </div>
    </body>
</html>