<?php

class XlsIterator
{
    
    public function rowIteratorImport($rowIterator,&$keys,$get_keys = false)
    {
        $res = array();                
        foreach($rowIterator as $row){                
            $i = 0;
            $cellIterator = $row->getCellIterator();            
            $cellIterator->setIterateOnlyExistingCells(false);                         
            $rowIndex = $row->getRowIndex ();                
            
            $array = array();
            /**
             *@var PHPExcel_Cell $cell 
             */
            foreach ($cellIterator as  $cell) {              
                if($rowIndex == 1 && $get_keys) {                          
                    $key = trim($cell->getCalculatedValue());                    
                    if(strlen($key))
                        $keys[] = trim($cell->getCalculatedValue());
                }        
                else {                    
                    if(isset($keys[$i])){                    
                        if($cell->getDataType() != 'f' && $cell->getDataType() != 'n') {
                            $cell->setDataType(PHPExcel_Cell_DataType::TYPE_STRING); 
                        }
                        
                        if($cell->getDataType() != 'n') {
                            $array[$keys[$i]] = $cell->getCalculatedValue();                            
                        }
                        else {    
                            if(PHPExcel_Shared_Date::isDateTime($cell)) {                                 
                                 $array[$keys[$i]] = PHPExcel_Style_NumberFormat::toFormattedString($cell->getValue(), "M/D/YYYY");                                 
                            }
                            else {
                                $array[$keys[$i]] = $cell->getCalculatedValue();                                
                            }                            
                        }                        
                    }
                    $i++;
                    if($i > count($keys)) break;                    
                }
            }      
            //check if is the header
            if($rowIndex == 1) continue;   
            elseif($array['f_code'] == '' || is_null($array['f_code'])) continue;
            
            $res[] = $array;        
        }
        
        return $res;
    }
    
    public function IterateExcel($filename,$position,$inc,$sheet = false)
    {
        $ext = explode('.',$filename);
        $ext = array_pop($ext);
        if($ext == 'xls') {
            $excelReader = new PHPExcel_Reader_Excel5();    
        }
        else {
            $excelReader = new PHPExcel_Reader_Excel2007();
        }
        
        $chunkFilter = new ChunkReadFilter();
        $excelReader->setReadFilter($chunkFilter);            
        $chunkFilter->setRows($position, $inc);        
        if($sheet !== false) {
            $excelReader->setLoadSheetsOnly($sheet);
        }        
        
        $file = $excelReader->load($filename);
        
        return $file;
    }
}