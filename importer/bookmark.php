<?php

class bookmark
{
    private $db;    
    
    public function __construct() {}           

    public function createBookmark($table,$fields,$custom_fields)
    {
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);        
        //prendo tutti i campi ed escludo quelli che non sono custom
        $bookmark = array();
        foreach($fields as $value) {
            if(!array_key_exists($value, $custom_fields)) continue;            
            $bookmark[$value] = $custom_fields[$value];
        }
        $mdl = "mdl_{$table}_tg";        
        //extract default bookmarks                
        try {
            $res_bm = $select->from(array("t1"=>"t_creation_date"),"f_id")
                ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code",array())    
                ->where("f_creation_user = 0")->where("f_category = 'BOOKMARK'")
                ->where("fc_bkm_module = ?",$mdl)->query()->fetchAll();
        }catch(Exception $e){
            utilities::reportQueryError("t_systems",$e->getMessage(),$select->__toString());die;
        };

        $tot = count($res_bm);        
        //aggiungo i custom mancanti
        for($a = 0;$a <$tot;++$a){ 
            $default_bm = array();
            $select->reset();
            $res_bm_pair = $select->from("t_pair_cross","fc_bkm_bind")
                    ->where("f_code_main = ?",$res_bm[$a]['f_id'])
                    ->query()->fetchAll();
            $tot_bp = count($res_bm_pair);
            for($bp = 0;$bp < $tot_bp;++$bp) {
                $default_bm[] = $res_bm_pair[$bp]['fc_bkm_bind'];
            }
            foreach($bookmark as $key => $value) {                
                if(in_array($key,$default_bm)) continue;
                $type = 0; //TESTO
                $explode_type = explode(" : ",$value['f_type']);
                $explode_bkm = array();
                if(isset($value['f_bkm_visibility']) && !empty($value['f_bkm_visibility'])) {
                    $explode_bkm = explode(' : ',$value['f_bkm_visibility']);
                }
                switch($explode_type[0]) {
                    case 0 : //NUMERO                    
                    case 1 : $type = 1;
                        break;
                    case 5 : //DATE
                        $type = 3;
                        break;
                    case 6 : //BOOLEAN
                        $type = 2;
                        break;
                    case 4 : //ENUM
                        $type = 4;
                        break;
                }
                $new_pair = array(
                    'f_code_main'=>$res_bm[$a]['f_id'],
                    'f_code_cross'=>-1,
                    'f_title'=>'Field',
                    'f_timestamp'=>time(),
                    'fc_bkm_bind'=>$value['f_title'],
                    'fc_bkm_label'=>$value['f_label'],
                    'fc_bkm_type'=>$type,
                    'fc_bkm_locked'=>0,
                    'fc_bkm_visible'=>!empty($explode_bkm)?(int)$explode_bkm[0]:1,
                    'fc_bkm_width'=>110,
                    'fc_bkm_filter'=>$type == 4?json_encode($value['f_value']):"[]",
                    'fc_bkm_range'=>"[]",
                    'fc_bkm_prange'=>"[]",
                    'fc_bkm_script'=>is_null($value['f_conversion'])?'':$value['f_conversion'],
                    "fc_bkm_level" => -1,
                    "fc_bkm_group" => ''
                );
                $this->db->insert("t_pair_cross",$new_pair);
            }
        }
    }           
}