<?php

class Ui
{
    private $db;    
        
    public function __construct() {}
   
    private function createTxt($data,$ui_name)
    { 
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);
        $mandatory = (bool)strtolower(substr(str_replace(" : ", "", $data['f_mandatory']),0,1));
        $readonly = (bool)strtolower(substr(str_replace(" : ", "", $data['f_read_only']),0,1));
        $disabled = (bool)strtolower(substr(str_replace(" : ", "", $data['f_disabled']),0,1));
        $auto_complete = (bool)substr($data['f_disabled'],0,1);
        
        $textarea = "false";
        if(strpos($data['f_ui_type'],"TEXTAREA") !== false) {
            $textarea = "true";
        }
        
        $auto_complete_str = "false";
        if($auto_complete) {
            $auto_complete_str = "true";
        }
                
        $txtfld = array(
            'label'=>$data['f_label'],
            'info'=>$data['f_info'],
            'mandatory'=>$mandatory,
            'readonly'=>$readonly,
            'visible'=>true,
            'bind'=>$data['f_title'],
            'disabled'=>$disabled,
            'f_wo_types_visibility'=>isset($data['f_wo_types_visibility']) && !empty($data['f_wo_types_visibility'])?$data['f_wo_types_visibility']:'',
            'f_wares_types_visibility'=>isset($data['f_wares_types_visibility']) && !empty($data['f_wares_types_visibility'])?$data['f_wares_types_visibility']:'',
            'text'=>$textarea,
            'autocomplete'=>$auto_complete_str
        );
        
        if(strpos($data['f_type'],"DATE") !== false) {
            $txtfld['dtmpicker'] = 'datetime';
        }
        
        if($data['f_fieldset'] != "" && !is_null($data['f_fieldset'])) {
            $txtfld['groupName'] = $data['f_fieldset'];
        }
        
        // search every connection to selectors and add it on textfields
        $selectors = array();
        foreach($data as $key => $val) {
            if(strpos($key,"fc_imp_cross_selector_") !== false && !empty($val)) { // is a selector                
                $explode_sel = explode(',',$val);
                if(count($explode_sel) == 1) {
                    $explode_sel = explode('.',$val);
                }
                $type = str_replace("fc_imp_cross_selector_", "", $key);
                foreach($explode_sel as $code_sel) {
                    $select->reset();
                    try {
                        $res_sel = $select->from("t_selectors",array("f_code"))->where("fc_imp_code_sel_excel = ?",$code_sel)
                            ->where("f_type_id = ?",$type)->query()->fetchAll();
                    }catch(Exception $e) {                        
                        utilities::reportQueryError("t_selectors",$e->getMessage(),$select->__toString());die;
                    };
                    if(!empty($res_sel)) {
                        $selectors[] = $res_sel[0]['f_code'];
                    }
                }
                
            }
        }
        $txtfld['t_selectors'] = implode(',',$selectors);
        $properties = json_encode($txtfld);
        $insert = array(
            "f_instance_name"=>"txtfld_{$ui_name}_{$data['f_title']}",
            "f_properties"=>$properties,
            "f_type_id"=>6
        );
        
        $f_instance_name = $insert['f_instance_name'];
        //if num = 0
        
        try {
            $this->db->insert("t_ui_object_instances",$insert);
        }catch(Exception $e) {
            $dump = Zend_Debug::dump($insert,null,false);                    
            utilities::reportInsertError("t_ui_object_instances",$e->getMessage(),$dump);
            echo json_encode(array('response'=>'ko'));die;
        };
        $f_instance_name = $insert['f_instance_name'];
        //inserisco il padre
        $f_code = $this->db->lastInsertId();
        $parent = array(
            "f_code"=>$f_code,
            "f_parent_code"=>0,
            "f_timestamp"=>time()
        );
        try {
            $this->db->insert("t_ui_object_instances_parent",$parent);
        }catch(Exception $e) {                                        
            $dump = Zend_Debug::dump($parent,null,false);                    
            utilities::reportInsertError("t_ui_object_instances_parent",$e->getMessage(),$dump);
            echo json_encode(array('response'=>'ko'));die;
        };                                    
        return $f_instance_name;
    }
    
    private function createChk($data,$ui_name) 
    {
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);
        $mandatory = (bool)strtolower(substr(str_replace(" : ", "", $data['f_mandatory']),0,1));
        $readonly = (bool)strtolower(substr(str_replace(" : ", "", $data['f_read_only']),0,1));        

        $radio = "false";
        if(strpos($data['f_ui_type'],"RADIO") !== false) {
            $radio = "true";
        }
        
        $array_vals = explode(',',$data['f_value']);
        if(count($array_vals) == 1) {
            $array_vals = explode('.',$data['f_value']);
        }
        $buttons = array(array('value'=>true,'level'=>1,'selected'=>false,'label'=>''));
        
        if(!empty($array_vals)) {
            $buttons = array();
            foreach($array_vals as $line_vals) {
                $exp_vals = explode(' : ',$line_vals);
                $value = $exp_vals[0];
                $text = isset($exp_vals[1])?$exp_vals[1]:$value;
                $buttons[] = array('value'=>$value,'level'=>1,'selected'=>false,'label'=>$text);
            }
        }                
        
        $chkbx = array(
            'buttons'=>$buttons,
            'label'=>$data['f_label'],
            'info'=>$data['f_info'],
            'mandatory'=>$mandatory,
            'readonly'=>$readonly,
            'bind'=>$data['f_title'],
            'radio'=>$radio, //f_radio
            'f_wo_types_visibility'=>isset($data['f_wo_types_visibility']) && !empty($data['f_wo_types_visibility'])?$data['f_wo_types_visibility']:'',
            'f_wares_types_visibility'=>isset($data['f_wares_types_visibility']) && !empty($data['f_wares_types_visibility'])?$data['f_wares_types_visibility']:''
        );
        if($data['f_fieldset'] != "" && !is_null($data['f_fieldset'])) {
            $chkbx['groupName'] = $data['f_fieldset'];
        }
        
        // search every connection to selectors and add it on checkbox
        $selectors = array();
        foreach($data as $key => $val) {
            if(strpos($key,"fc_imp_cross_selector_") !== false && !empty($val)) { // is a selector                
                $explode_sel = explode(',',$val);
                if(count($explode_sel) == 1) {
                    $explode_sel = explode('.',$val);
                }
                $type = str_replace("fc_imp_cross_selector_", "", $key);
                foreach($explode_sel as $code_sel) {
                    $select->reset();
                    try {
                        $res_sel = $select->from("t_selectors",array("f_code"))->where("fc_imp_code_sel_excel = ?",$code_sel)
                            ->where("f_type_id = ?",$type)->query()->fetchAll();
                    }catch(Exception $e) {                        
                        utilities::reportQueryError("t_selectors",$e->getMessage(),$select->__toString());
                        echo json_encode(array('response'=>'ko'));die;
                    };
                    if(!empty($res_sel)) {
                        $selectors[] = $res_sel[0]['f_code'];
                    }
                }
                
            }
        }
        $chkbx['t_selectors'] = implode(',',$selectors);
        
        $properties = json_encode($chkbx);
        $insert = array(
            "f_instance_name"=>"chkbx_{$ui_name}_{$data['f_title']}",
            "f_properties"=>$properties,
            "f_type_id"=>8
        );
        
        $f_instance_name = $insert['f_instance_name'];
        
        
        try {
            $this->db->insert("t_ui_object_instances",$insert);
        }catch(Exception $e) {                                        
            $dump = Zend_Debug::dump($insert,null,false);                    
            utilities::reportInsertError("t_ui_object_instances",$e->getMessage(),$dump);
            echo json_encode(array('response'=>'ko'));die;
        }; 
        $f_instance_name = $insert['f_instance_name'];
        //inserisco il padre
        $f_code = $this->db->lastInsertId();
        $parent = array(
            "f_code"=>$f_code,
            "f_parent_code"=>0,            
            "f_timestamp"=>time()
        );
        try {
            $this->db->insert("t_ui_object_instances_parent",$parent);
        }catch(Exception $e) {                                        
            $dump = Zend_Debug::dump($parent,null,false);                    
            utilities::reportInsertError("t_ui_object_instances_parent",$e->getMessage(),$dump);
            echo json_encode(array('response'=>'ko'));die;
        };
        return $f_instance_name;
    }
    
    /**
     * creazione select options e select
     * @param type $data
     * @param type $table
     * @return type 
     */
    private function createSelect($data,$ui_name,$option_class) 
    {
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);
        $mandatory = (bool)strtolower(substr(str_replace(" : ", "", $data['f_mandatory']),0,1));
        $readonly = (bool)strtolower(substr(str_replace(" : ", "", $data['f_read_only']),0,1));        
        
        /**
         * creo la optionClass che contiene i vari option 
         */
        if(empty($option_class)) return '';
                
        //verifica se l'option class esiste già, se si si tiene il primo
        $f_option_class = $data['f_title'];
        $select->reset();
        try {
            $res_check = $select->from("t_ui_object_instances")->where("f_instance_name = ?",$f_option_class)->where("f_type_id = 12")->query()->fetchAll();
        }catch(Exception $e) {            
            utilities::reportQueryError("t_ui_object_instances",$e->getMessage(),$select->__toString());
            echo json_encode(array('response'=>'ko'));die;
        };
        if(empty($res_check)) {
            $f_properties = json_encode($option_class);
            $insert_option = array(
                'f_type_id'=>12,
                'f_properties'=>$f_properties,
                'f_instance_name'=>$f_option_class                
            );
            
            try {
                $this->db->insert("t_ui_object_instances",$insert_option);
            }catch(Exception $e){
                $dump = Zend_Debug::dump($insert_option,null,false);                    
                utilities::reportInsertError("t_ui_object_instances_parent",$e->getMessage(),$dump);
                echo json_encode(array('response'=>'ko'));die;
            };
            $f_code = $this->db->lastInsertId();
            $parent = array(
                "f_code" => $f_code,
                "f_parent_code" => 0,                
                "f_timestamp" => time()
            );
            try {
                $this->db->insert("t_ui_object_instances_parent",$parent);
            }catch(Exception $e){
                $dump = Zend_Debug::dump($parent,null,false);                    
                utilities::reportInsertError("t_ui_object_instances_parent",$e->getMessage(),$dump);
                echo json_encode(array('response'=>'ko'));die;
            };
            
        }        
        
        $slct = array(
            'category'=>$f_option_class,
            'label'=>$data['f_label'],
            'info'=>$data['f_info'],
            'mandatory'=>$mandatory,
            'readonly'=>$readonly,
            'bind'=>$data['f_title'],
            'radio'=>false,
            'f_wo_types_visibility'=>isset($data['f_wo_types_visibility']) && !empty($data['f_wo_types_visibility'])?$data['f_wo_types_visibility']:'',
            'f_wares_types_visibility'=>isset($data['f_wares_types_visibility']) && !empty($data['f_wares_types_visibility'])?$data['f_wares_types_visibility']:''
        );
        if($data['f_fieldset'] != "" && !is_null($data['f_fieldset'])) {
            $slct['groupName'] = $data['f_fieldset'];
        }
        // search every connection to selectors and add it on textfields
        $selectors = array();
        foreach($data as $key => $val) {
            if(strpos($key,"fc_imp_cross_selector_") !== false && !empty($val)) { // is a selector                
                $explode_sel = explode(',',$val);
                if(count($explode_sel) == 1) {
                    $explode_sel = explode('.',$val);
                }
                $type = str_replace("fc_imp_cross_selector_", "", $key);
                foreach($explode_sel as $code_sel) {
                    $select->reset();
                    try {
                        $res_sel = $select->from("t_selectors",array("f_code"))->where("fc_imp_code_sel_excel = ?",$code_sel)
                            ->where("f_type_id = ?",$type)->query()->fetchAll();
                    }catch(Exception $e) {                        
                        utilities::reportQueryError("t_selectors",$e->getMessage(),$select->__toString());die;
                    };
                    if(!empty($res_sel)) {
                        $selectors[] = $res_sel[0]['f_code'];
                    }
                }
                
            }
        }
        $slct['t_selectors'] = implode(',',$selectors);
        
        $properties = json_encode($slct);
        $insert = array(
            "f_instance_name"=>"slct_{$ui_name}_{$data['f_title']}",
            "f_properties"=>$properties,
            "f_type_id"=>7
        );
                
        $f_instance_name = $insert['f_instance_name'];
        
        
        try {
            $this->db->insert("t_ui_object_instances",$insert);
        }catch(Exception $e) {
            $dump = Zend_Debug::dump($insert,null,false);                    
            utilities::reportInsertError("t_ui_object_instances",$e->getMessage(),$dump);
            echo json_encode(array('response'=>'ko'));die;
        };
        $f_instance_name = $insert['f_instance_name'];
        //inserisco il padre
        $f_code = $this->db->lastInsertId();
        $parent = array(
            "f_code"=>$f_code,
            "f_parent_code"=>0,            
            "f_timestamp"=>time()
        );
        try {
            $this->db->insert("t_ui_object_instances_parent",$parent);
        }
        catch(Exception $e) {
            $dump = Zend_Debug::dump($parent,null,false);                    
            utilities::reportInsertError("t_ui_object_instances_parent",$e->getMessage(),$dump);
            echo json_encode(array('response'=>'ko'));die;
        }
        return $f_instance_name;
    }
    
    public function createUi($table,$fields,$custom,$validation)
    {
        $this->db = Zend_Registry::get("db");        
        $select = new Zend_Db_Select($this->db);
        //verifico se i campi all'interno corrispondono         
        $custom_fields = array();               
        foreach($fields as $line) {                        
            if(array_key_exists($line, $custom)){
                $custom_fields[] = $custom[$line];                
            }            
        }        
        
        //search every module linked to this table and create ui
        try {
            $res_module = $select->from("t_ui_object_instances",array("f_instance_name","f_properties"))
                ->where("f_type_id = 9")->where("(f_instance_name like '%{$table}_edit%' OR f_instance_name like '%{$table}_pp_edit%' )")
                ->where("f_properties like '%moModuleComponent%'")->query()->fetchAll();
        }catch(Exception $e) {            
            utilities::reportError("t_ui_object_instances",$e->getMessage(),$select->__toString());
            echo json_encode(array('response'=>'ko'));die;
        }
        foreach($res_module as $module ) {
            $mdl_fields = array(); 
            //now check if this module is an edit of pair cross
            $select->reset();
            $create_tg = substr($module['f_instance_name'],0,strpos($module['f_instance_name'],"edit_")).'tg';
            try {
                $res_pair = $select->from("t_ui_object_instances",array('f_code'))
                    ->where("f_type_id = 9")->where("f_instance_name = ?",$create_tg)
                    ->where("f_properties like '%moTreeGrid_light_check_pair%'")->query()->fetchAll();
            }catch(Exception $e) {                
                utilities::reportError("t_ui_object_instances",$e->getMessage(),$select->__toString());
                echo json_encode(array('response'=>'ko'));die;
            };
            //if this is a pair edit module, continue
            if(!empty($res_pair)) continue;
            
            //now create a part of the name used inside the ui
            $create_tg = substr($module['f_instance_name'],0,strpos($module['f_instance_name'],"edit_")+5).'tg';
            $ui_name = str_replace("mdl_", "", $create_tg);
            $ui_name = str_replace("_tg","",$ui_name);
            
            //find the fields create customs
            foreach($custom_fields as $line) {
                $type = substr(str_replace(" : ","",$line['f_ui_type']),1);            
                $select->reset();
                switch($type) {
                    case "TEXTAREA":
                    case "TEXTFIELD" : 
                        $ui_check = "txtfld_{$ui_name}_{$line['f_title']}";
                        $res_check = $select->from("t_ui_object_instances",array("f_code"))
                                ->where("f_instance_name = ?",$ui_check)->query()->fetchAll();
                        if(!empty($res_check)) continue;
                        $in = $this->createTxt($line,$ui_name);
                        if($in == '') continue;
                        $mdl_fields[] = $in;
                        break;
                    case "CHECKBOX" : 
                        $ui_check = "chkbx_{$ui_name}_{$line['f_title']}";
                        $res_check = $select->from("t_ui_object_instances",array("f_code"))
                                ->where("f_instance_name = ?",$ui_check)->query()->fetchAll();
                        if(!empty($res_check)) continue;
                        $in = $this->createChk($line,$ui_name);
                        if($in == '') continue;
                        $mdl_fields[] = $in;
                        break;
                    case "SELECT" : 
                        $ui_check = "slct_{$ui_name}_{$line['f_title']}";
                        $res_check = $select->from("t_ui_object_instances",array("f_code"))
                                ->where("f_instance_name = ?",$ui_check)->query()->fetchAll();
                        if(!empty($res_check)) continue;
                        $in = $this->createSelect($line,$ui_name,$validation[$line['f_category']]['f_ui_category']);
                        if($in == '') continue;
                        $mdl_fields[] = $in;
                        break;
                }
            }                                    
            $properties = json_decode($module['f_properties'],true);
            $fields = explode(',',$properties['fields']);
            foreach($mdl_fields as $line) {
                $fields[] = $line;
            }
            $properties['fields'] = implode(',',$fields);
            $json_prop = json_encode($properties);
            try {
                $this->db->update("t_ui_object_instances",array("f_properties"=>$json_prop),"f_instance_name = '{$module['f_instance_name']}' ");
            }catch(Exception $e) {
                $dump = Zend_Debug::dump($json_prop,null,false);                    
                utilities::reportError("t_ui_object_instances_parent",$e->getMessage(),$dump);
                echo json_encode(array('response'=>'ko'));die;
            };
        }        
    }
}