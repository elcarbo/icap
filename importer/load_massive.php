<?php
$loaded = false;

require_once 'constants.php';

if(!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->f_level != -1) {
    header("Location:deny.html");die;
}

$table_choose  = array(
    "t_selector_1","t_selector_2","t_selector_3","t_selector_4",
    "t_selector_5","t_selector_6","t_selector_7","t_wares_asset","t_wares_vendors",
    "t_wares_tasks","t_wares_wizard","t_wares_inventory","t_wares_cr",
    "t_workorders_periodic_maint","t_engagement","t_wares_labor","t_wares_tools",
    "t_wares_contracts","t_wares_failurecodes","t_wares_documents","t_workorders_standard_workorder",
    "t_workorders_meter","t_engagement_meter","t_wares_action","t_workorders_workorder","t_workorders_on_condition","t_pair_cross",
    "t_wares_users","t_cross_wares_selectors","t_cross_workorders_wares","t_cross_wares_wares","t_cross_workorders_selectors"
);

 $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
 $db_config_array = $config->toArray();
 
$conn = Zend_Db::factory($config->database);          
$error_message = '';
if(isset($_FILES["file"])) {    
    
    $db = $db_config_array['database']['params']['dbname'];        
    $res = $conn->query("SELECT COUNT(*) AS num FROM t_queue")->fetchAll();
    if($res[0]['num'] == 0){
        $ext = explode('.',$_FILES["file"]["name"]);
        $ext = array_pop($ext);    
        if ($ext == "csv"){
            if ($_FILES["file"]["error"] > 0){
                $error_message = "There was an error during the upload of your csv. Please try again. <br />";
            }
            else{            
                move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $_FILES["file"]["name"]);                
                $loaded = true;                
                $queue_insert = array(                    
                    'f_file_name'=>$_FILES["file"]["name"],
                    'f_sheets'=>$_POST['sheet'],
                    'f_active'=>3,
                    'f_timestamp'=>time()
                );

                $conn->insert("t_queue",$queue_insert);
                $f_code = $conn->lastInsertId();           
                $sheets = urlencode($_POST['sheet']);
            }        
        }
        else  {
            $error_message = "Invalid format file";
        }  
    }
    else {
        $error_message = "Importer already running.";
    }
}
$conn->closeConnection();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="imagetoolbar" CONTENT="no" />
        <title>Mainsim Importer</title>  
        <link rel="stylesheet" type="text/css" href="default.css" />
        <script type="text/javascript" src="jquery.js"></script>
        <script>
            function checkData(form) {
                var res = true;
                var file = $('#file').val();
                if(file == '') {
                    alert("Insert a file to load");
                    res = false;
                }                
                return res;
            }            
        </script>
    </head>
    <body>
        <div><a href="index.php"><< Back to index page</a></div>        
        <?php if(!empty($error_message)) : ?>
            <div style="text-align:center;font-size: 20;color:red"> <?php echo $error_message; ?></div>
        <?php endif;?>
        <?php if(!$loaded): ?>
        <div style="text-align:center">
            <h1>Massive importer</h1>
            <form enctype="multipart/form-data" action="load_massive.php" method="POST" onsubmit="return checkData(this);">        
                <p>
                    <label for="file">File:</label>
                    <input type ="file" name = "file" id="file"/>
                </p>                
                <p>
                    <label for="sheet">Choose the sheet</label>
                    <select name ="sheet">
                        <?php for($i = 0;$i < count($table_choose); $i++ ) :?>                                
                            <option value="<?php echo $table_choose[$i];?>"><?php echo $table_choose[$i];?></option>
                        <?php endfor;?>                            
                    </select>                    
                </p>
                <p>
                <input type="submit" value="Import" style="position: absolute; top: 350px; left: 900px;" />
                </p>
            </form>
        </div>
        <?php elseif(empty($error_message)) : ?>
            <div style="text-align:center">Excel inserted, a cron service will  provide to insert data to db.If you wanna follow the progess of import, go on <a href="updater.php?id=<?php echo $f_code?>&sheets=<?php echo $sheets;?>">updater</a><br /><a href="index.php">Home</a></div>
        <?php endif; ?>
        <div id="logo">&nbsp;</div>
        <div id="credits">
            for any problem e-mail to <a href="mailto:info@unp.it">UNPlugged</a>
        </div>
    </body>
</html>