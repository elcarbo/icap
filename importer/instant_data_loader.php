<?php
try {    
    //initialize constants
    ini_set("display_errors","On");
    defined('BASE_PATH') || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));
    
    //include constants and methods used during import
    require_once BASE_PATH.'/constants.php';
    //only su can use this tool
    if(!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->f_level != -1) {
        header("Location:deny.html");die;
    }
    //Zend_session::writeClose();

    require_once(APPLICATION_PATH.'/../library/phpExcel/PHPExcel.php');
    include (BASE_PATH.'/chunkReadFilter.php');
    include (BASE_PATH.'/Iterator.php');
    include (BASE_PATH.'/utilities.php');
    include (BASE_PATH.'/wares.php');
    include (BASE_PATH.'/workorders.php');
    include (BASE_PATH.'/ui.php');
    include (BASE_PATH.'/selector.php');
    include (BASE_PATH.'/bookmark.php');
    include (BASE_PATH.'/scheduler/mp_class.php');

    //initialize database
    $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');    
    Zend_Registry::set('db_config',$config);
    $db = Zend_Db::factory($config->database);
    $select = new Zend_Db_Select($db);
    Zend_Registry::set('db',$db);

    //type id of sheet (if exist)
    $f_type_id = 0;    
    //table to write
    $table = '';

    //position and increment for this sheet
    $pos_excel = $_POST['pos'];
    $inc_excel = $_POST['inc'];

    //possible response
    $resp = array('response'=>'ok','message'=>'');
    //directory with upload file in queue
    $dir = opendir(BASE_PATH.'/uploads');
    $project_xls = "";
    //check if there are any file in upload's directory
    while($entryName = readdir($dir)) {
        if($entryName == '.' || $entryName == '..') continue;
        if(strrpos($entryName, ".xls") !== false) {        
            $project_xls = $entryName;         
            //if find file, check if is present in queue
            $select->reset();
            $res_xls = $select->from("t_queue")->where("f_file_name = '$project_xls'")->where("f_active = 2")->query()->fetchAll();        
            if(empty($res_xls)){$project_xls = ""; continue;}
            $xls = $res_xls[0];
            if(empty($xls['f_sheets'])){$project_xls = ""; continue; }                                                  
            break;
        }    
    }
    closedir($dir);
    $sheet_configuration = array();
    //sheet request to import
    $sheet = $_POST['sheet'];
    $res_conf = array();//array with sheet configuration
    $select->reset();
    if(strpos($sheet,"t_wares") === 0) {
        $res_conf = $select->from("t_wares_types",array('f_id','f_short_name','f_sheet_name','f_module_name'))
                ->where("f_sheet_name = ?",$sheet)->query()->fetch();    
        $table = "t_wares";
    }
    elseif(strpos($sheet,"t_workorders") === 0) {
        $res_conf = $select->from("t_workorders_types",array('f_id','f_short_name','f_sheet_name','f_module_name'))
                ->where("f_sheet_name = ?",$sheet)->query()->fetch();
        $table = "t_workorders";
    }
    $select->reset();
    if(!empty($res_conf)) {    
        $f_type_id = $res_conf['f_id'];
        $sheet_configuration['table'] = $table;
        $sheet_configuration['f_type'] = $res_conf['f_id'];
        if(!is_null($res_conf['f_module_name'])) {
            $sheet_configuration['mdl'] = $res_conf['f_module_name'];
        }
        if(!is_null($res_conf['f_short_name'])) {
            $sheet_configuration['short_name'] = $res_conf['f_short_name'];
        }
    }
    elseif(!$f_type_id && strpos($sheet,"t_selector") !== false){
        $f_type_id = (int)str_replace("t_selector_","",$sheet);    
        $table = 't_selectors';        
    }
    
    //id of request in zend registry so is available in any part of system during import
    Zend_Registry::set("f_id",$_POST['id']);
    $f_id = $_POST['id'];
    Zend_Registry::set("f_timestamp",time());

    //check if last call to remove record from queue
    $last_call = $_POST['last_call'];

    //check if excel exist
    if(empty($project_xls)) {
        $resp["response"] = 'ko';
        utilities::reportError("Excel not found");
        echo json_encode($resp); die;        
    }

    //initialize method to import
    $wares = new Wares();
    $selector = new Selector();
    $wo = new Workorders();
    $bookmark = new Bookmark();
    $ui = new Ui();    
    $iterator = new XlsIterator();    

    $keys = array();
    $file = $iterator->IterateExcel(BASE_PATH."/uploads/$project_xls",1,1);
    $sheets = $file->getSheetNames();

    //check if passed sheet exist inside the excel
    if(!in_array($sheet, $sheets)){
        utilities::reportError("The sheet ".$sheet." is not in your excel.");
        echo json_encode(array("response"=>'ko'));
        die;        
    }
    
    //get header of sheet
    $pos = array_search($sheet, $sheets);    
    $key_sheet = array();
    $rowIterator = $file->setActiveSheetIndex($pos)->getRowIterator();    
    $iterator->rowIteratorImport($rowIterator,$key_sheet,true);
    $keys[$sheet] = $key_sheet;
    //--------------------------------------------------------------------------
    
    //get Custom sheet    
    $check_list[$sheet] = $default_check_list;    
    $custom = array();    
    if(in_array("Custom",$sheets)) {//check if there is Custom sheet
        $custom = utilities::createCustomList(BASE_PATH."/uploads/$project_xls",$iterator,$sheets);                 
    }        
    //--------------------------------------------------------------------------
    
    //extract and parse validation tables 
    $validation = array();
    if(in_array("Validation Tables",$sheets)) {        
        $validation = utilities::createValidationList(BASE_PATH."/uploads/$project_xls",$iterator,$sheets);                 
    }    
    //--------------------------------------------------------------------------
    
    //add custom check rules
    foreach($keys[$sheet] as $line_key) {
        if(isset($custom[$line_key]['f_check_rules']) && !empty($custom[$line_key]['f_check_rules'])){
            if(isset($check_list[$sheet][$line_key])) unset($check_list[$sheet][$line_key]);
            $check_list[$sheet][$line_key]['check'] = $custom[$line_key]['f_check_rules'];
            $check_list[$sheet][$line_key]['check_value'] = $custom[$line_key]['f_check_value'];
        }
    }        
    
    if($pos_excel) {        
        if($pos_excel == 1) { 
            // we set f_id of start only for the first read            
            utilities::createRollBack($sheet,$table,$db);                                      
            //create custom fields
            if(!empty($sheet_configuration) || strpos($sheet, "t_selectors") !== false) {
                //get custom and system cols already exist 
                $custom_cols = utilities::get_tab_cols("t_custom_fields");
                $tab = strpos($sheet, "t_selectors") !== false?
                        utilities::get_tab_cols("t_selectors"):utilities::get_tab_cols($sheet_configuration['table']);
                $already_insert = array();
                foreach($keys[$sheet] as $col){
                    //convert in lower case to avoid problem with mysql
                    $col_ok = strtolower($col);
                    if(!in_array($col_ok,$tab) && isset( $custom[$col])) {
                        if(!in_array($col_ok,$custom_cols) && $col != 'f_parent_code' && !in_array($col_ok,$already_insert)) {                
                            //check special chars and not permitted char for mysql col name                
                            utilities::createCustomCol($col_ok, $custom[$col]);
                            $already_insert[] = $col_ok;
                        }
                    }
                }
            }
        }        
        //set default wf and phase
        utilities::setWfPhases(); 
        //set default main table
        if($table != '') {        
            utilities::setDefaultTableCols($table);
            utilities::setCreationTableCols();
            utilities::setCustomTableCols();
        }    
        if($f_type_id == 16) {
            utilities::setUserTableCols();
        }
        elseif($f_type_id == 9) {
            utilities::setWizardTableCols();
        }         
        // cycling till finish to read            
        $not_end = true;        
        //get part of excel                      
        $file = $iterator->IterateExcel(BASE_PATH.'/uploads/'.$project_xls,$pos_excel,$inc_excel,$sheet);        
        $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
        $objects = $iterator->rowIteratorImport($rowIterator,$keys[$sheet],false);        
        $file->disconnectWorksheets();
        unset($file);        
        $db = Zend_Db::factory($config->database);
        Zend_Registry::set('db',$db);
        $mdl = isset($sheet_configuration['mdl'])?$sheet_configuration['mdl']:'';
        //work extracted data        
        utilities::manageTrunk($sheet, $check_list, $table, $f_type_id,$mdl,$custom,$pos_excel,$objects);
        $db->closeConnection();
        if(count($objects)<$inc_excel-1) $not_end = false;                                  
        $resp['response'] = 'continue';
        $resp['isFinished'] = $not_end?0:1;
        echo json_encode($resp);die;        
    }        
    //refresh connection
    Zend_Registry::set('db',Zend_Db::factory($config->database));
    //create custom and bookmark
    if(!empty($sheet_configuration)) {    
        utilities::createFathers($table, $f_type_id);    
        if($sheet_configuration['table'] == "t_wares" && $sheet != 't_wares_wizard') { //wares
            $wares->createParentField($f_type_id);            
            if($f_type_id == 16) { //if user 
                $wares->cryptPwd();
            }
        }
        if($sheet_configuration['table'] == "t_workorders") { //workorders        
            $wo->addAssetField($sheet_configuration['f_type']);            
            if($f_type_id == 9) {                
                $wo->completeOnCondition();//if uploaded on condition change placeholder with f_code
            }
            
        }
        if(isset($sheet_configuration['short_name']) && isset($sheet_configuration['mdl'])) {            
            $bookmark->createBookmark($sheet_configuration['short_name'], $keys[$sheet], $custom);
            $ui->createUi($sheet_configuration['short_name'], $keys[$sheet], $custom,$validation);            
        }        
    }elseif(strpos($sheet,"t_selector_") !== false) {           
        $type = str_replace('t_selector_', "", $sheet);
        $selector->fatherSelector($type); //FUNZIONA    
    }
    elseif($sheet == 't_cross_workorders_wares') {
        $wo->wares_opened_flag(); //icons about wares/workorders
        $wo->wo_asset();//fill information about asset cross with wo
        $wo->selectorWoFromAsset();// if wo is without selector, assign selector of asset/s associate
    }

    $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
    $db = Zend_Db::factory($config->database);
    
    if($last_call) {
        unlink(BASE_PATH."/uploads/$project_xls");        
        $db->delete("t_queue","f_id = $f_id") or die(mysql_error());
    }
    
}catch(Exception $e) {
    utilities::reportError("Uncatchable error : ".$e->getMessage());
    echo json_encode(array("response"=>"ko"));die;
}

$db->closeConnection();
echo json_encode($resp);