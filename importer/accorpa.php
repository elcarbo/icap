<?php

include 'constants.php';
include 'workorders.php';

$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini','production');
$db = Zend_Db::factory($config->database);
$select = new Zend_Db_Select($db);
$wo = new Workorders();

 $res = $db->query("SELECT * FROM `t_ui_object_instances` where f_instance_name like 'btn\_%\_%\_add' and f_type_id = 5")
         ->fetchAll();
 $tot = count($res);
 for($i = 0;$i < $tot;++$i) {
     $exp_inst = explode('_',$res[$i]['f_instance_name']);
     $prop = json_decode($res[$i]['f_properties'],true);
     if(is_null($prop)) {
         echo $res[$i]['f_instance_name'].'<br />';
     }
     $prop['action'] = "moLibrary.openOverlay('{$exp_inst[1]}','{$exp_inst[2]}')";
     $prop = json_encode($prop);
     $db->update("t_ui_object_instances",array('f_properties'=>$prop),"f_code = {$res[$i]['f_code']}");
 }
die('ok');
/*
$res = $select->from(array("t1"=>"t_workorders_parent"),array("f_parent_code"))
        ->join(array("t2"=>"t_pair_cross"),"t1.f_code = t2.f_code_main",array("f_code","f_code_main"))
        ->join(array("t3"=>"t_workorders"),"t1.f_code = t3.f_code",array())
        ->where("t1.f_active = 1")->where("t2.f_active = 1")->where("t3.f_type_id = 3")
        ->where("t2.f_code_cross = -1 ")->where("f_parent_code != 0")
        ->query()->fetchAll();
foreach($res as $line) {
    $db->update("t_pair_cross",array("f_code_main"=>$line['f_parent_code']),"f_code = {$line['f_code']} and f_active = 1");
    $db->update("t_periodics",array("f_code"=>$line['f_parent_code']),"f_code = {$line['f_code_main']} and f_active = 1");    
}
$wo->addNextDueDate();

$wo->pm_frequency();*/
/*
foreach($building as $build => $new_code) {
    $select->reset();
    $res = $select->from(array("t1"=>"t_workorders"),array("f_code_wo"=>"f_code"))
        ->join(array("t4"=>"t_workorders_parent"),"t1.f_code = t4.f_code",array())
        ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_wo_id",array())
        ->join(array("t3"=>"t_wares"),"t2.f_ware_id = t3.f_code",array("f_code_asset"=>"f_code"))
        ->where("t1.f_active = 1")->where("t2.f_active = 1")->where("t3.f_active = 1")        
        ->where("t1.f_title = '$build'")
        ->where("t1.f_code != $new_code")
        ->where("t1.f_type_id = 3")
        ->where("t4.f_parent_code = 0")
        ->where("t3.f_type_id = 1")->query()->fetchAll();
    
    foreach($res as $list) {                        
        //remove
        $db->delete("t_ware_wo","f_wo_id = {$list['f_code_wo']}");
        $db->delete("t_wo_relations","f_code_wo_master = {$list['f_code_wo']} OR f_code_wo_slave = {$list['f_code_wo']}");
        $db->delete("t_periodics","f_code = {$list['f_code_wo']}");
        $db->delete("t_selector_wo","f_wo_id = {$list['f_code_wo']}");
        $db->delete("t_workorders_parent","f_code = {$list['f_code_wo']}");
        $db->delete("t_workorders","f_code = {$list['f_code_wo']}");
        $db->delete("t_custom_fields","f_code = {$list['f_code_wo']}");
        $db->delete("t_pair_cross","f_code_main = {$list['f_code_wo']}");
        $db->delete("t_creation_date","f_id = {$list['f_code_wo']}");        
    }
}
*/