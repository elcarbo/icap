<?php
$loaded = false;

require_once 'constants.php';

if(!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->f_level != -1) {
    header("Location:deny.html");die;
}

$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
$db_config_array = $config->toArray();
 
$conn = Zend_Db::factory($config->database);          
$error_message = '';

$table_choose  = array();

$res_sel_sheet = $conn->query("SELECT f_sheet_name from t_selectors_types")->fetchAll();
$tot_a = count($res_sel_sheet);
for($a = 0;$a < $tot_a; ++$a) {
    if(!empty($res_sel_sheet[$a]['f_sheet_name']) && !is_null($res_sel_sheet[$a]['f_sheet_name']))
        $table_choose[] = $res_sel_sheet[$a]['f_sheet_name'];
}

$res_sel_sheet = $conn->query("SELECT f_sheet_name from t_wares_types")->fetchAll();
$tot_a = count($res_sel_sheet);
for($a = 0;$a < $tot_a; ++$a) {
    if(!empty($res_sel_sheet[$a]['f_sheet_name']) && !is_null($res_sel_sheet[$a]['f_sheet_name']))
        $table_choose[] = $res_sel_sheet[$a]['f_sheet_name'];
}

$res_sel_sheet = $conn->query("SELECT f_sheet_name from t_workorders_types")->fetchAll();
$tot_a = count($res_sel_sheet);
for($a = 0;$a < $tot_a; ++$a) {
    if(!empty($res_sel_sheet[$a]['f_sheet_name']) && !is_null($res_sel_sheet[$a]['f_sheet_name']))
        $table_choose[] = $res_sel_sheet[$a]['f_sheet_name'];
}

$table_choose = array_merge($table_choose,array("t_pair_cross","t_engagement","t_cross_wares_selectors","t_cross_workorders_wares","t_cross_wares_wares","t_cross_workorders_selectors","t_engagement_meter"));
 
if(isset($_FILES["file"])) {
    $res = $conn->query("SELECT COUNT(*) AS num FROM t_queue")->fetchAll();
    if($res[0]['num'] == 0){
        $ext = explode('.',$_FILES["file"]["name"]);
        $ext = array_pop($ext);    
        if ($ext == "xls" || $ext == "xlsx"){
            if ($_FILES["file"]["error"] > 0){
                $error_message = "There was an error during the upload of your excel. Please try again. <br />";
            }
            else{            
                move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $_FILES["file"]["name"]);                
                $loaded = true;                
                $queue_insert = array(                    
                    'f_file_name'=>$_FILES["file"]["name"],
                    'f_sheets'=>implode(',',$_POST['sheets']),
                    'f_active'=>$_POST['load_type'] == "instant"?2:1,
                    'f_timestamp'=>time()
                );

                $conn->insert("t_queue",$queue_insert);
                $f_code = $conn->lastInsertId();           
                $sheets = urlencode(implode(',',$_POST['sheets']));
                if($_POST['load_type'] == "instant") { 
                    //check if  excel's weight is over 5Mb
                    if($_FILES['file']['size'] > 5242880){
                        $error_message = 'Your excel file is too big to import using the instant import system.Please use the "Cron" system or reduce the dimension of the file under 5Mb.Thank you';
                        unlink("uploads/" . $_FILES["file"]["name"]);
                        $conn->delete("t_queue","f_id = $f_code");
                    }
                    else {
                        header("Location:updater.php?id=$f_code&sheets=".$sheets."&start_imp=1");
                        die();
                    }
                }
            }        
        }
        else  {
            $error_message = "Invalid file";
        }  
    }
    else {
        $error_message = "Importer already running.";
    }
}
$conn->closeConnection();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="imagetoolbar" CONTENT="no" />
        <title>Mainsim Importer</title>  
        <link rel="stylesheet" type="text/css" href="default.css" />
        <script type="text/javascript" src="jquery.js"></script>
        <script>
            function checkData(form) {
                var res = true;
                var file = $('#file').val();
                if(file == '') {
                    alert("Insert a file to load");
                    res = false;
                }
                var checked = false;
                $('.sheets').each(function(){                    
                    if($(this).attr('checked') != undefined) {
                        checked = true;
                    }
                });
                if(!checked ) {
                    alert("Select at list one card to load");
                    res = false;
                }
                return res;
            }
            
            $(document).ready(function(){
                $('.sheets').change(function(){
                    if($(this).attr('checked') == 'checked') {
                        var sheet = $(this).attr('value');                    
                        if((sheet == "t_engagement" || sheet == "t_engagement_meter") &&  $('#load_type_instant').attr('checked') == 'checked') {
                            alert("ATTENTION: if you want to create periodic maintenances or meters scheduling using instant type, yuo have to know that system will load these very slowly");
                        }
                    }
                });
            });
            
        </script>
    </head>
    <body>
        <div><a href="index.php"><< Back to index page</a></div>        
        <?php if(!empty($error_message)) : ?>
            <div style="text-align:center;font-size: 20;color:red"> <?php echo $error_message; ?></div>
        <?php endif;?>
        <?php if(!$loaded): ?>
        <div>
            <form enctype="multipart/form-data" action="load.php" method="POST" onsubmit="return checkData(this);">        
                <p>
                    <label for="file">File:</label>
                    <input type ="file" name = "file" id="file"/>
                </p>
                <p>
                    <label for="load_type">type of loading</label>
                    <label style="padding-left: 10px;"><input type="radio" name="load_type" value="cron" /> Cron (for big files or many rows)</label>
                    <label style="padding-left: 10px;"><input id="load_type_instant" type="radio" name="load_type" value="instant" checked="checked"/> Instant</label>
                </p>
                <p>
                    <table>
                            <tr>
                        <?php for($i = 0;$i < count($table_choose); $i++ ) :?>
                                <?php if($i && !($i%4)) :?>
                                    </tr><tr>
                                <?php endif;?>
                                <td><label><input class="sheets" type="checkbox" name="sheets[]" value="<?php echo $table_choose[$i];?>" /> <label for= "sheets[]" ><?php echo $table_choose[$i];?></label></label></td>
                        <?php endfor;?>
                            </tr>
                    </table>
                </p>
                <p>
                <input type="submit" value="Import"/>
                </p>
            </form>
        </div>
        <?php elseif(empty($error_message)) : ?>
            <div style="text-align:center">Excel inserted, a cron service will  provide to insert data to db.If you wanna follow the progess of import, go on <a href="updater.php?id=<?php echo $f_code?>&sheets=<?php echo $sheets;?>">updater</a><br /><a href="index.php">Home</a></div>
        <?php endif; ?>
        <div id="logo">&nbsp;</div>
        <div id="credits">
            for any problem e-mail to <a href="mailto:info@unp.it">UNPlugged</a>
        </div>
    </body>
</html>