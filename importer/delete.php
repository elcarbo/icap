<?php

require_once 'constants.php';
require_once 'utilities.php';

if(!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->f_level != -1) {
    header("Location:deny.html");die;
}
session_start();

function removeFileDir($path,$dir_code = '')
{
    $path = realpath($path);
    $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
    $files = iterator_to_array($objects);
    foreach($files as $name => $obj){
        if(is_file($name)) {            
            @unlink($name);            
        }
    }
    if(empty($dir_code)){
        $path = realpath($path);
        $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
        $files = iterator_to_array($objects);
        foreach($files as $name => $obj){
            if( is_dir($name)) {
                @rmdir($name);            
            }
        }
    }
    else{
        @rmdir($dir_code);
    }
}

$deleted = false;
$strage = 0;
$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');

if($config->multiDatabase == "On") {
    include APPLICATION_PATH.'/modules/default/models/Utilities.php';
    $host = $_SERVER['HTTP_HOST'];
    $php_self = '';
    if($_SERVER['HTTP_HOST'] == 'test.mainsim.com'){
        $expRU = explode('/',$_SERVER['REQUEST_URI']);    
        $php_self = '/'.$expRU[1];            
    }
    $host.=$php_self;      
    $resDb = Mainsim_Model_Utilities::getInstallationInfo($config->database,$host,'f_url');    
    if(empty($resDb)) {
        throw new Exception("Page not found");
    }
    $dbConfig = $resDb['database'];
    Zend_Registry::set('attachmentsFolder',$resDb['attachment_path']);
}
else {
    $dbConfig = $config->database;
    Zend_Registry::set('attachmentsFolder',$config->attachmentsFolder);
}

$conf = $dbConfig->toArray();    

$is_mysql = true;
if(stripos($conf['database']['adapter'], 'sqlsrv') !== false) {
    $is_mysql = false;
}

$conn = Zend_Db::factory($dbConfig);   
Zend_Registry::set('db',$conn);
//ASSOC SHORT CUT NAME TO CATEGORY NAME
$assoc_sc_category = array();
$res_wares_type = $conn->query("SELECT * FROM t_wares_types")->fetchAll();
$tot_a = count($res_wares_type);
for($a = 0;$a < $tot_a;++$a) {
    if(!empty($res_wares_type[$a]['f_short_name']) && !is_null($res_wares_type[$a]['f_short_name'])){
        $assoc_sc_category[$res_wares_type[$a]['f_type']] = $res_wares_type[$a]['f_short_name'];
    }
}

$res_wo_type = $conn->query("SELECT * FROM t_workorders_types")->fetchAll();
$tot_a = count($res_wo_type);
for($a = 0;$a < $tot_a;++$a) {
    if(!empty($res_wo_type[$a]['f_short_name']) && !is_null($res_wo_type[$a]['f_short_name'])){
        $assoc_sc_category[$res_wo_type[$a]['f_type']] = $res_wo_type[$a]['f_short_name'];
    }
}

$res_sel_type = $conn->query("SELECT * FROM t_selectors_types")->fetchAll();
$tot_a = count($res_sel_type);
for($a = 0;$a < $tot_a;++$a) {
    if(!empty($res_sel_type[$a]['f_short_name']) && !is_null($res_sel_type[$a]['f_short_name'])){
        $assoc_sc_category[$res_sel_type[$a]['f_type']] = $res_sel_type[$a]['f_short_name'];
    }
}

if(isset($_POST['types']) && $_POST['strage'] != "on") {
    $conn->beginTransaction();
    $category = $_POST['types'];        
    if(isset($assoc_sc_category[$category])) {
        $search_sc = $assoc_sc_category[$category];        
        //delete ui 
        if($_POST['delete_ui'] == "on") {                   
            utilities::clearUi($search_sc,$conn);
        }
        //delete bm
        if($_POST['delete_bm'] == "on") {             
            utilities::deleteBookmarks($conn,'mdl_'.$search_sc.'_tg');            
            utilities::clearBm($search_sc,$_POST['table']);    
        }        
    }
    
    if(!isset($_POST['strage'])) {
        try {            
            $tables = array('t_wares','t_workorders','t_selectors','t_systems');
            if(in_array($_POST['table'], $tables)) {
                $res_id = $conn->query("select f_id from {$_POST['table']}_types where f_type = '$category'")->fetch();
                if(!empty($res_id)) {
                    $conn->delete('t_progress_lists',"f_table = '{$_POST['table']}' and f_type_id = {$res_id['f_id']}");
                }
            }
            if($_POST['table'] == 't_wares') {                
                if($category == 'WIZARD') {
                    $conn->query("TRUNCATE TABLE t_wizard");
                }

                $user = '';
                if($category == 'USER') {
                    $user = ' AND f_id > 5';
                }
                $res_attach = $conn->query("SELECT distinct(f_code) as f_code from t_attachments where f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category')")->fetchAll();
                $tot_at = count($res_attach);
                for($at = 0;$at < $tot_at;++$at) {
                    $path_doc = realpath("../attachments/doc/{$res_attach[$at]['f_code']}");
                    if(!empty($path_doc)) {
                        removeFileDir($path_doc,$path_doc);
                    }
                    $path_img = realpath("../attachments/img/{$res_attach[$at]['f_code']}");
                    if(!empty($path_img)) {
                        removeFileDir($path_img,$path_img);
                    }                
                }                                        
                $conn->delete("t_wares_parent","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");                            
                $conn->delete("t_custom_fields_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");   
                $conn->delete("t_custom_fields","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");            
                $conn->delete("t_selector_ware","f_ware_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");
                $conn->delete("t_ware_wo","f_ware_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");
                $conn->delete("t_pair_cross","f_code_main IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category')");                
                $conn->delete("t_wares_relations","f_code_ware_master IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");
                $conn->delete("t_wares_relations","f_code_ware_slave IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");        
                $conn->delete("t_wares_systems","f_ware_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");        
                if($category == 'WIZARD'){
                    $conn->delete("t_wizard","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");   
                }
                if($category == 'USER'){
                    $conn->delete("t_users","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");   
                }                
                $conn->delete("t_wares_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");                                 
                $conn->delete("t_wares","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");                                     
                $conn->delete("t_attachments","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category')");
                $conn->delete("t_creation_date_history","f_type = 'WARES' AND f_category = '$category' $user");            
                $conn->delete("t_creation_date","f_type = 'WARES' AND f_category = '$category' $user");            
                $deleted = true;
            }
            elseif($_POST['table'] == 't_selectors'){
                $res_attach = $conn->query("SELECT distinct(f_code) as f_code from t_attachments where f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category')")->fetchAll();
                $tot_at = count($res_attach);
                for($at = 0;$at < $tot_at;++$at) {
                    $path_doc = realpath("../attachments/doc/{$res_attach[$at]['f_code']}");
                    if(!empty($path_doc)) {
                        removeFileDir($path_doc,$path_doc);
                    }
                    $path_img = realpath("../attachments/img/{$res_attach[$at]['f_code']}");
                    if(!empty($path_img)) {
                        removeFileDir($path_img,$path_img);
                    }                
                }                        
                $conn->delete("t_attachments","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category')");                        
                $conn->delete("t_selectors_parent","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category')");                            
                $conn->delete("t_custom_fields_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category' $user)"); 
                $conn->delete("t_custom_fields","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category')");
                $conn->delete("t_selector_ware","f_selector_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category')");
                $conn->delete("t_selector_wo","f_selector_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category')");                
                $conn->delete("t_selectors_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category')");
                $conn->delete("t_selectors","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SELECTORS' AND f_category = '$category')");
                $conn->delete("t_selector_system","f_selector_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)");        
                $conn->delete("t_creation_date_history","f_type = 'SELECTORS' AND f_category = '$category' $user");            
                $conn->delete("t_creation_date","f_type = 'SELECTORS' AND f_category = '$category'");    
                $deleted = true;
            }
            elseif($_POST['table'] == 't_workorders') {                 
                $res_attach = $conn->query("SELECT distinct(f_code) as f_code from t_attachments where f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')")->fetchAll();
                $tot_at = count($res_attach);
                $real_path = Zend_Registry::get('attachmentsFolder');
                for($at = 0;$at < $tot_at;++$at) {
                    $path_doc = realpath($real_path."/doc/{$res_attach[$at]['f_code']}");
                    if(!empty($path_doc)) {
                        removeFileDir($path_doc,$path_doc);
                    }
                    $path_img = realpath($real_path."/img/{$res_attach[$at]['f_code']}");
                    if(!empty($path_img)) {
                        removeFileDir($path_img,$path_img);
                    }                
                }                        
                $conn->delete("t_attachments","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");                        
                $conn->delete("t_workorders_parent","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");                                        
                $conn->delete("t_custom_fields_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category' $user)");            
                $conn->delete("t_custom_fields","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");
                $conn->delete("t_selector_wo","f_wo_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");
                $conn->delete("t_ware_wo","f_wo_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");            
                $conn->delete("t_wo_relations","f_code_wo_master IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')"); 
                $conn->delete("t_wo_relations","f_code_wo_slave IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')"); 
                $conn->query("delete from t_periodics where f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");                
                $conn->delete("t_pair_cross","f_code_main IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");                
                $conn->delete("t_workorders_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");
                $conn->delete("t_workorders","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS' AND f_category = '$category')");
                $conn->delete("t_creation_date_history","f_type = 'WORKORDERS' AND f_category = '$category' $user");            
                $conn->delete("t_creation_date","f_type = 'WORKORDERS' AND f_category = '$category'  $user");                
                $deleted = true;
            }
            elseif($_POST['table'] == 't_systems') {                   
                $res_attach = $conn->query("SELECT distinct(f_code) as f_code from t_attachments where f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')")->fetchAll();                
                $tot_at = count($res_attach);
                for($at = 0;$at < $tot_at;++$at) {
                    $path_doc = realpath("../attachments/doc/{$res_attach[$at]['f_code']}");
                    if(!empty($path_doc)) {
                        removeFileDir($path_doc,$path_doc);
                    }
                    $path_img = realpath("../attachments/img/{$res_attach[$at]['f_code']}");
                    if(!empty($path_img)) {
                        removeFileDir($path_img,$path_img);
                    }
                    $path_img = realpath("../attachments/data manager/exporter/{$res_attach[$at]['f_code']}");
                    if(!empty($path_img)) {
                        removeFileDir($path_img,$path_img);
                    }
                    $path_img = realpath("../attachments/data manager/importer/{$res_attach[$at]['f_code']}");                    
                    if(!empty($path_img)) {                        
                        removeFileDir($path_img,$path_img);
                    }                    
                }
                if($category == 'BOOKMARK') {
                    utilities::deleteBookmarks($conn);
                }
                else {                                        
                    $conn->delete("t_attachments","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");                        
                    $conn->delete("t_systems_parent","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");                                            
                    $conn->delete("t_custom_fields_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");            
                    $conn->delete("t_custom_fields","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");
                    $conn->delete("t_selector_system","f_system_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");
                    $conn->delete("t_wares_systems","f_system_id IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");            
                    $conn->delete("t_pair_cross","f_code_main IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");                    
                    $conn->delete("t_systems_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");
                    $conn->delete("t_systems","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'SYSTEMS' AND f_category = '$category')");
                    $conn->delete("t_creation_date_history","f_type = 'SYSTEMS' AND f_category = '$category'");            
                    $conn->delete("t_creation_date","f_type = 'SYSTEMS' AND f_category = '$category'");
                    
                }
                $deleted = true;
            }
            elseif($_POST['table'] == 'cross') {
                if($_POST['table'] != "t_pair_cross") {
                    $conn->query("TRUNCATE TABLE $category");
                }
                else {
                    $conn->delete("t_pair_cross","f_code_main IN (SELECT f_id FROM t_creation_date WHERE f_type != 'SYSTEMS')");
                }
                $deleted = true;
            }        
            $conn->commit();
        }catch(Exception $e) {
            $conn->rollBack();
            die($e->getMessage());
        }
    }
}
//sfascio er Db
elseif(isset($_POST['strage'])) {           
    //delete all ui and custom       
    foreach($assoc_sc_category as $line) {                                
        utilities::clearUi($line);         
        utilities::clearBm($line); // r3emove custom fields from default view        
        utilities::deleteBookmarks($conn); //delete custom view        
    }        
    utilities::clearCustom();        
    
    //prepare file with query to creation date, system,parent and custom fields
    $restoreFile = realpath(APPLICATION_PATH.'/../importer/').'/restore.sql';    
    $f_code_start = 25; //last f_code from start.sql    
    $select = new Zend_Db_Select($conn);
    $res_system_cd = $select->from(array("t1"=>"t_creation_date"))
        ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code",array())                                
        ->where("fc_bkm_user_id = 0")
        ->query()->fetchAll(); 
    
    $tot_a = count($res_system_cd);    
    for($a = 0;$a < $tot_a;++$a) {        
        $line = $res_system_cd[$a];
        $line['f_id'] = $f_code_start;
        $keys = array_keys($line);                
        $f_code = $res_system_cd[$a]['f_id'];        
        $insert = "INSERT INTO t_creation_date (".implode(',',$keys).") VALUES (?);";
        $insert = $conn->quoteInto($insert,$line);
        file_put_contents($restoreFile, $insert.PHP_EOL, FILE_APPEND);            
    
        //get System table value for f_code passed        
        $select->reset();
        $res_system = $select->from("t_systems")->where("f_code = ?",$f_code)
                ->query()->fetch();
        if(!empty($res_system)) {            
            $line = $res_system;
            unset($line['f_id']);            
            $line['f_code'] = $f_code_start;
            $keys = array_keys($line);            
            $insert = "INSERT INTO t_systems (".implode(',',$keys).") VALUES (?);";
            $insert = $conn->quoteInto($insert,$line);
            file_put_contents($restoreFile, $insert.PHP_EOL, FILE_APPEND);                
        }
        
        //get System table parent
        $select->reset();
        $res_system_pc = $select->from("t_systems_parent")
                ->where("f_code = ?",$f_code)
                ->where("f_active = 1")
                ->query()->fetch();        
        if(!empty($res_system_pc)) {            
            $line = $res_system_pc;
            $line['f_code'] = $f_code_start;
            unset($line['f_id']);
            $keys = array_keys($line);
            $insert = "INSERT INTO t_systems_parent (".implode(',',$keys).") VALUES (?);";
            $insert = $conn->quoteInto($insert,$line);
            file_put_contents($restoreFile, $insert.PHP_EOL, FILE_APPEND);
        }
        
        //get System table custom        
        $select->reset();
        $res_system_cf = $select->from("t_custom_fields")->where("f_code = ?",$f_code)
                ->query()->fetch();
        if(!empty($res_system_cf)) {
            $line = $res_system_cf;
            unset($line['f_id']);
            $keys = array_keys($line);                    
            $line['f_code'] = $f_code_start;
            $insert = "INSERT INTO t_custom_fields (".implode(',',$keys).") VALUES (?);";
            $insert = $conn->quoteInto($insert,$line);
            file_put_contents($restoreFile, $insert.PHP_EOL, FILE_APPEND);            
        }

        $select->reset();
        //get bookmark column        
        $res_pair_cross = $select->from("t_bookmarks")->where("f_code = ?",$f_code)->query()->fetchAll();
        $tot_pc = count($res_pair_cross);
        for($pc = 0;$pc < $tot_pc;++$pc) {
            $line = $res_pair_cross[$pc];
            $line['f_code'] = $f_code_start;            
            $keys = array_keys($line);        
            unset($keys[0]);
            unset($line['f_id']);
            $insert = "INSERT INTO t_bookmarks (".implode(',',$keys).") VALUES (?);";
            $insert = $conn->quoteInto($insert,$line);
            file_put_contents($restoreFile, $insert.PHP_EOL, FILE_APPEND);
        }
        $f_code_start++;
    }
    
    //WARES        
    $deleted = true;  
    $strage = 1;
    //wares    
    $conn->query("TRUNCATE TABLE t_wares_parent");
    $conn->query("TRUNCATE TABLE t_wares_relations");
    $conn->query("TRUNCATE TABLE t_ware_wo");    
    $conn->query("TRUNCATE TABLE t_selectors_support");
    $conn->query("TRUNCATE TABLE t_access_registry_history");
    $conn->query("TRUNCATE TABLE t_users");
    $conn->query("TRUNCATE TABLE t_wizard");    
    $conn->query("TRUNCATE TABLE t_bookmarks");
    $conn->query("TRUNCATE TABLE t_wares_systems");
    $conn->query("TRUNCATE TABLE t_progress_lists");
    
    //t_selectors    
    $conn->query("TRUNCATE TABLE t_selectors_parent");        
    $conn->query("TRUNCATE TABLE t_selector_wo");
    $conn->query("TRUNCATE TABLE t_selector_ware");   
    $conn->query("TRUNCATE TABLE t_selector_system");   
        
    //workorders
    $conn->query("TRUNCATE TABLE t_workorders_parent");    
    $conn->query("TRUNCATE TABLE t_systems_parent");    
    $conn->query("TRUNCATE TABLE t_wo_relations");    
    $conn->query("TRUNCATE TABLE t_pair_cross");
    $conn->query("TRUNCATE TABLE t_periodics");        
    $conn->query("TRUNCATE TABLE t_custom_fields_history");
    $conn->query("TRUNCATE TABLE t_custom_fields");
    $conn->query("TRUNCATE TABLE t_workorders_history");
    $conn->query("TRUNCATE TABLE t_wares_history");
    $conn->query("TRUNCATE TABLE t_selectors_history");
    $conn->query("TRUNCATE TABLE t_creation_date_history");
    $conn->query("TRUNCATE TABLE t_logs");
        
    //altro    
    $conn->query("TRUNCATE TABLE t_reverse_ajax");  
    $conn->query("TRUNCATE TABLE t_locked");  
    $conn->query("TRUNCATE TABLE t_attachments");  
    $conn->query("TRUNCATE TABLE t_queue"); 
    $conn->query("TRUNCATE TABLE t_meters_history");  
            
    $conn->delete("t_wares");    
    $conn->delete("t_workorders");    
    $conn->delete("t_selectors");
    $conn->delete("t_systems");
    $conn->delete("t_creation_date");    
            
    if($is_mysql) {
        $conn->query("ALTER TABLE t_wares AUTO_INCREMENT = 1");
        $conn->query("ALTER TABLE t_workorders AUTO_INCREMENT = 1");
        $conn->query("ALTER TABLE t_selectors AUTO_INCREMENT = 1");
        $conn->query("ALTER TABLE t_creation_date AUTO_INCREMENT = 1");    
        $conn->query("ALTER TABLE t_systems AUTO_INCREMENT = 1");    
    }
    else {
        try {$conn->query("DBCC CHECKIDENT (t_wares, RESEED, 1)");}catch(Exception $e){}
        try {$conn->query("DBCC CHECKIDENT (t_workorders, RESEED, 1)");}catch(Exception $e){}
        try {$conn->query("DBCC CHECKIDENT (t_selectors, RESEED, 1)");}catch(Exception $e){}
        try {$conn->query("DBCC CHECKIDENT (t_creation_date, RESEED, 1)");}catch(Exception $e){}
        try {$conn->query("DBCC CHECKIDENT (t_systems, RESEED, 1)");}catch(Exception $e){}
    }
        
    
    $sql = file_get_contents("start.sql");
    $array_query = explode(";",$sql);
    $tot = count($array_query);
    $conn->beginTransaction();
        
    for($i = 0; $i < $tot; ++$i) {
        if(empty($array_query[$i])) continue;
        if(!$is_mysql && strpos($array_query[$i],'INSERT INTO t_creation_date') !==false )  {            
            $conn->query("SET IDENTITY_INSERT t_creation_date ON");                        
        }
        try{
            $query = str_replace('UNIX_TIMESTAMP()',time(),$array_query[$i]);
            $conn->query($query);
        }catch(Exception $e) {            
            $conn->rollBack();
            echo($e->getMessage());die;
        }
        if(!$is_mysql && strpos($array_query[$i],'INSERT INTO t_creation_date') !==false)  {            
            $conn->query("SET IDENTITY_INSERT t_creation_date OFF");                        
        }
    }
    if(file_exists("restore.sql")){        
        $sql = file_get_contents("restore.sql");
        $array_query = explode(PHP_EOL,$sql);
        $tot = count($array_query);        
        for($i = 0; $i < $tot; ++$i) {
            if(empty($array_query[$i])) continue;
            if(!$is_mysql && strpos($array_query[$i],'INSERT INTO t_creation_date') !==false)  {            
                $conn->query("SET IDENTITY_INSERT t_creation_date ON");                        
            }
            try{
                $query = trim(str_replace('UNIX_TIMESTAMP()',time(),$array_query[$i]));
                if(!empty($query)) {
                    $conn->query($query);
                }
            }catch(Exception $e) {            
                $conn->rollBack();
                Zend_Debug::dump($query);
                echo($e->getMessage());die;
            }
            if(!$is_mysql && strpos($array_query[$i],'INSERT INTO t_creation_date') !==false)  {            
                $conn->query("SET IDENTITY_INSERT t_creation_date OFF");                        
            }
        }
        unlink("restore.sql");
    }
    $conn->query("TRUNCATE TABLE t_access_registry");
    $conn->commit();
    
    removeFileDir("../attachments/temp");
    removeFileDir("../attachments/doc");
    removeFileDir("../attachments/img");    
}
$conn->closeConnection();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="imagetoolbar" CONTENT="no" />
        <title>Mainsim Importer</title>
        <script type="text/javascript" src="jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="default.css" />        
        <script>
            function conferma(form) {
                var db = document.getElementById("db").value;
                if($('#strage').attr('checked') == 'checked') {
                    var res = confirm('Are you sure do you want delete ALL data inside '+db+'?');
                }
                else {   
                    var type = $('#types').val();
                    var table = $('#table').val();
                    if(type == 0 || table == 0) {
                        alert('Select table and types before delete');
                        return false;
                    }
                    var res = confirm("Are you sure you want to delete data in "+table+" of type "+type+" on "+db+"?");                 
                }
                
                return res;
            }
            var strage = <?php echo $strage;?>;
            if(strage == 1) {   
                var pathArray = window.location.pathname.split( '/' );
                var host = pathArray[1];
                parent.location.href = "/"+host+"/login/logout";
            }
            $(document).ready(function(){
                
                $("#delete_bm").change(function(){
                    if($(this).attr('checked') == 'checked') {
                        alert('Important: if you delete bookmarks, you cannot see your custom fields.')
                    }                
                });
                
                $("#delete_ui").change(function(){
                    if($(this).attr('checked') == 'checked') {
                        alert('Important: if you delete ui, You cannot edit custom fields.')
                    }                
                });
                
                $("#table").change(function(){
                    var db = $("#db").val();
                    var table = $("#table").val();
                    $.post("get_types.php",{"table":table},function(response) {
                        if(response != 0) {
                            $("#types option").each(function(e){
                                $(this).remove();
                            });
                            var types = jQuery.parseJSON(response);                           
                            for(var i = 0; i <types.length; i++ ) {
                                $("#types").append("<option value='"+types[i]+"'>"+types[i]+"</option>");
                            }                            
                        }
                    });
                });    
                
                $("#strage").change(function(){
                    if($(this).attr("checked") == "checked") {
                        $("#choose_action").css("display","none");
                    }
                    else {
                        $("#choose_action").css("display","block");
                    }
                });
            });            
        </script>        
    </head>
    <body>
        <div><a href="index.php"><< Back to main menu</a></div>        
        <?php if(!$deleted): ?>
        <div>
            <form action="delete.php" method="POST" onsubmit="return conferma(this);">            
                <div id="choose_action" style="margin-top : 20px;" >
                    <input type="hidden" value="<?php echo $conf['params']['dbname'];?>" id="db" />
                    <select name="table" id="table">
                        <option value = "0">Select type</option>
                        <option value = "t_wares">WARES</option>
                        <option value = "t_workorders">WORKORDERS</option>
                        <option value ="t_selectors">SELECTORS</option>
                        <option value ="t_systems">SYSTEM SETTINGS</option>                        
                        <option value ="cross">CROSS</option>
                    </select>
                    <select name="types" id="types" style="margin-left: 30px;">
                        <option value = "0">Select type</option>                
                    </select>
                    
                    <div style="left: 10px;margin-top: 20px;">
                        <label><input type ="checkbox" name="delete_ui" id="delete_ui" /> Back to default UI setting (deleting extra UI) </label>
                    </div>
                    <div style="left: 10px;">
                        <label><input type ="checkbox" name="delete_bm" id="delete_bm" /> Back to default bookmark setting (deleting extra bookmark's fields)  </label>
                    </div>
                    
                </div>            
                <div style="margin-top: 20px;">                    
                    <div style="left: 10px;">
                        <label><input type ="checkbox" name="strage" id="strage" /> Destroy all on <?php echo $conf['params']['dbname'];?></label>
                    </div>
                    <div style="left: 350px; position: absolute; top: 170px;">
                        <input type ="submit" value="Delete" /> <br />
                    </div>
                    <br clear ="all" />
                </div>
            </form>
        </div>
        <?php else : ?>        
            <div>Data deleted.<br />
                <?php if(!$strage) : ?>
                    <a href="delete.php">Click here</a> to delete other data.
                <?php else : ?>
                    System is restoring basic configuration. Please wait.
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div id="logo">&nbsp;</div>
        <div id="credits">
            for any problem e-mail to <a href="mailto:info@unp.it">UNPlugged</a>
        </div>
    </body>
</html>