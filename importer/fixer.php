<?php

include 'constants.php';
$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini','production');
$db = Zend_Db::factory($config->database);
$select = new Zend_Db_Select($db);

$res_asset = $select->from("t_custom_fields",array("cage","pn","f_code"))
        ->where("f_code IN (select f_code from t_wares where f_type_id = 1)")
        ->query()->fetchAll();
$tot_a = count($res_asset);
for($a = 0;$a < $tot_a;++$a) {    
    if(!empty($res_asset[$a]['pn']) && !empty($res_asset[$a]['cage'])) {
        $select->reset();
        //search cross with material
        $res_inv = $select->from(array("t1"=>"t_wares"),"f_code")
            ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array())
            ->where("pn = ?",$res_asset[$a]['pn'])
            ->where("cage = ?",$res_asset[$a]['cage'])
            ->where("t1.f_type_id = 3")->query()->fetch();
        if(!empty($res_inv)) {
            //cross master
            $db->insert("t_wares_relations",array(
                "f_code_ware_master"=>$res_asset[$a]['f_code'],
                "f_code_ware_slave"=>$res_inv['f_code'],
                "f_type_id_master"=>1,
                "f_type_id_slave"=>3                                
            ));
            //cross slave
            $db->insert("t_wares_relations",array(
                "f_code_ware_master"=>$res_inv['f_code'],
                "f_code_ware_slave"=>$res_asset[$a]['f_code'],
                "f_type_id_master"=>3,
                "f_type_id_slave"=>1                                
            ));                    
        }
    }            
    if(!empty($res_asset[$a]['cage'])) {
        $select->reset();                
        $res_cage = $select->from(array("t1"=>"t_wares"),"f_code")
            ->join(array("t2"=>"t_creation_date"),"t1.f_code = t2.f_id",array())                    
            ->where("f_title = ?",$res_asset[$a]['cage'])
            ->where("t1.f_type_id = 6")->query()->fetch();
        if(!empty($res_cage)) {
            //cross master
            $db->insert("t_wares_relations",array(
                "f_code_ware_master"=>$res_asset[$a]['f_code'],
                "f_code_ware_slave"=>$res_cage['f_code'],
                "f_type_id_master"=>1,
                "f_type_id_slave"=>6                                
            ));
            //cross slave
            $db->insert("t_wares_relations",array(
                "f_code_ware_master"=>$res_cage['f_code'],
                "f_code_ware_slave"=>$res_asset[$a]['f_code'],
                "f_type_id_master"=>6,
                "f_type_id_slave"=>1                                
            ));                    
        }
    }
}