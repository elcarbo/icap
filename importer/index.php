<?php 
    require_once 'constants.php';
    if(!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->f_level != -1) {
        header("Location:deny.html");die;
    }
?>
<html>
    <head>
        <title>Mainsim Importer</title>
        <link rel="stylesheet" type="text/css" href="default.css" />
    </head>
    <body>
        <div style="text-align:center;">
            <h1>Importer</h1>
            <h2>System for data entry in <b>main</b>sim&#8482;</h2>
        </div>
        <div id="menu">            
            <div class="item"><a href="delete.php">Remove elements</a></div>
            <div class="item"><a href="generatore_mp.php">Periodic generator</a></div>
            <div class="item"><a href="active_queue.php">Active Queue</a></div>            
        </div>
        <div id="logo">&nbsp;</div>
        <div id="credits">
            for any problem e-mail to <a href="mailto:info@unp.it">UNPlugged</a>
        </div>        
    </body>
        
</html>