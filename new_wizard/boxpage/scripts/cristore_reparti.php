<?php

$parent = 10;
$selectors = getSelectors($this->db,$parent);
//print_r($selectors); die;
$selectors = array_combine(array_column($selectors,'f_code'),array_column($selectors,'f_title'));
unset($selectors['10']);
//print_r($selectors); die;

$user = Zend_Auth::getInstance()->getIdentity(); 

if(!empty($user->selectors))
{
    $codes = explode(",",$user->selectors);
    $codes = array_combine(array_values($codes),array_keys($codes)); //swap key<->value
    $codes = array_intersect_key($selectors,$codes);
    if(count($codes) > 0) $selectors = $codes;
}

print(json_encode($selectors));

function getSelectors($db,$parent)
{
        $select = new Zend_Db_Select($db);
        $selectors = $select->from(array('slc' => 't_selectors'),array('f_code'))
        ->join(array('slcp' => 't_selectors_parent'),"slcp.f_code = slcp.f_code",array())
        ->join(array('cd' => 't_creation_date'),"cd.f_id = slc.f_code",array('f_title'))
        ->where("slc.f_type_id = 1")
        ->where("slcp.f_parent_code = $parent")
        ->query()->fetchAll();
        return $selectors;
}