<?php
//$name = $_POST['name'];
//var_dump('NOME   ');
//var_dump($name);
$selOp=new Zend_Db_Select($this->db);
$selOp->reset();
$resQty = $selOp->from('t_wares',array('f_code','fc_vendor_address','fc_vendor_zipcode','fc_vendor_city','fc_vendor_country'))
->join('t_creation_date', 't_creation_date.f_id = t_wares.f_code',array('f_title'))
->join('t_custom_fields', 't_custom_fields.f_code = t_wares.f_code',array('partita_iva','codice_fiscale'))  
->where('t_wares.f_type_id = 6')
->where("t_custom_fields.azienda_proprietaria = 1")
->query()->fetchAll();
$array = array();
$count = 0;


foreach($resQty as $value){
    //if($count > 10) break;
    array_push($array,$value['f_title']);
    array_push($array,$value['partita_iva']);
    array_push($array,$value['codice_fiscale']);
    array_push($array,$value['f_code']);
    array_push($array,$value['fc_vendor_address'].' '.$value['fc_vendor_zipcode'].' - '.$value['fc_vendor_city'].' - '.$value['fc_vendor_country']);
    //$count++;
}
return print(json_encode($array));