<?php

$this->db->query('SET CHARACTER SET utf8');

$selOp = new Zend_Db_Select($this->db);

    $str = $_POST['char'];

    $selOp->reset();


    $selOp->from('t_creation_date as t1', ['f_id', 'f_title'])
                    ->join('t_custom_fields as t2', 't2.f_code = t1.f_id', ['inv_gpt', 'matricola', 'produttore', 'modello', 'ospedale_presidio', 'unita_operativa', 'stanza', 'business_unit'])
                    ->join('t_wares as t3', 't3.f_code = t2.f_code AND t3.f_type_id = 25', [])
                    ->where("t2.inv_gpt like '%".$str."%'");
    #echo $selOp;
    $resQty = $selOp->query()->fetchAll();
#print_r($resQty);
    $array = array();
    $count = 0;
    
    foreach($resQty as $key => $value) {
        foreach($value as $k => $v) {
            $resQty[$key][$k] = mb_convert_encoding($v, 'HTML-ENTITIES', 'UTF-8');
        }
    }

    #print_r($resQty);

    foreach($resQty as $value) {
        if ($count > 10)
            break;
        
        array_push($array, $value['inv_gpt'] . "/" . str_replace('/', '', $value['f_title']) . "/" . str_replace('/', '', $value['matricola']) . "/" . $value['produttore'] . "/" . $value['modello'] . "/" . str_replace('"', '', $value['ospedale_presidio']) . "/" . $value['unita_operativa'] . "/" . $value['stanza'] . "/" . $value['business_unit'] . "/" . $value['f_id']);
        $count++;
    }


    $string = implode("_#_", $array);


    return print($string);

?>
