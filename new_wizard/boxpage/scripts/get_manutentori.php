<?php
$select = new Zend_Db_Select($this->db);
$select->from('t_creation_date', array('f_id','f_title', 'f_description'))
       ->where("f_category = 'LABOR'")
	   ->where("f_phase_id = 1");
$res = $select->query()->fetchAll();
$response = [];
for($i = 0; $i < count($res); $i++){
	$response[] = array(
		'text' => utf8_encode($res[$i]['f_title']),
		'id' => $res[$i]['f_id'],
	);
}
print(json_encode($response));
?>