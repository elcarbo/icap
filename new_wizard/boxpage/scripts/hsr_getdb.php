<?php

$param=json_decode($_POST["param"],true);
$fcodeware=$param["fcodeware"];
$percorso=$param["percorso"];
$settore=$param["settore"];

$zona=""; $localizzazione=""; $nident=""; $gruppo=""; $grp=""; $selected="";




$select = new Zend_Db_Select($this->db);
$select->from('t_creation_date', array('f_title', 'f_description'))
       ->join('t_custom_fields', 't_creation_date.f_id = t_custom_fields.f_code', array('n_identificativo_sanitario', 'classe_priorita', 'zona'))
       ->where('t_creation_date.f_id = ' . $fcodeware);
$res = $select->query()->fetchAll();
$res_data=$res[0];


   //                 0             1                   2                             3            4
   /*$que="SELECT t1.f_title, t1.f_description, t2.n_identificativo_sanitario, t2.classe_priorita, t2.zona ".
       "FROM t_creation_date t1, t_custom_fields t2 ".
       "WHERE t1.f_id=t2.f_code and t1.f_id=".$fcodeware;*/
  

  

$zona=$res_data["zona"];   // 4
$nident=$res_data["n_identificativo_sanitario"];  // 2
$g=$res_data["classe_priorita"];  // 3
   
//echo $zona." | ".$nident." | ".$g." + ".$settore." | ".$fcodeware; die;
   
   
$g=strtolower( trim($g) );
$ag=explode(" ",$g);
$grp=array_pop($ag);

if(!$grp) $grp="a";
   
$t=str_replace('"',"”", $res_data["f_title"]);    // 0
$t=str_replace("'","’",$t);
$localizzazione=$t;
   
$t=str_replace("<br>","",$res_data["f_title"]);  // 0    
$t=strtolower($t); 
$t=str_replace("'","",$t);
   
$selected=str_replace(" ","",$t);
       
$gruppo="gruppo".$grp;




//--------------------------------------------------------------------------------
// divisione

$divisione=""; $pe=""; $lun=0;
$my=Mainsim_Model_Utilities::isMysql();


$select->reset();

if($my){
    $select->from('t_osr_divisione', array('f_divisione', 'f_percorso'))
           ->where("f_gruppo ='" . $gruppo . "' AND INSTR('" . $percorso . "', f_percorso)");
}
else{
    $select->from('t_osr_divisione', array('f_divisione', 'f_percorso'))
           ->where("f_gruppo ='" . $gruppo . "' AND (CHARINDEX(f_percorso, '" . $percorso . "') <> 0)");
}

//$que="SELECT f_divisione, f_percorso FROM t_osr_divisione WHERE f_gruppo='$gruppo' AND INSTR('$percorso', f_percorso) ";

$res_data = $select->query()->fetchAll();
//echo($select);

for($i=0; $i<count($res_data); $i++){
    $dv  = $res_data[$i]['f_divisione'];
    $pe = strlen($res_data[$i]['f_percorso']);
    
    if($pe>$lun) { $divisione=$dv; $lun=$pe; } 
}


//--------------------------------------------------------------------------------
// priorita
$priorita=1; $lun=0; $pe=0; $pr=0;

$select->reset();

if(strpos($percorso,"#ascensori#")!==false) {  // ascensori
    
    if($my){
        $select->from('t_osr_ascensori', array('f_priorita', 'f_percorso'))
               ->where("f_ascensore ='" . $selected . "' AND INSTR('" . $percorso . "', f_percorso)");
    }
    else{
        $select->from('t_osr_ascensori', array('f_priorita', 'f_percorso'))
               ->where("f_ascensore ='" . $selected . "' AND (CHARINDEX(f_percorso, '" . $percorso . "') <> 0)");
    }
    
    $res_data = $select->query()->fetchAll();
    
    //$que="SELECT f_priorita, f_percorso FROM t_osr_ascensori WHERE f_ascensore='$selected' and INSTR('$percorso', f_percorso)";
  
    for($i = 0; $i < count($res_data); $i++){
        $pr  = $res_data[$i]['f_priorita'];
        $pe = strlen($res_data[$i]['f_percorso']);
        if($pe>$lun) { $priorita=$pr; $lun=$pe; } 
    }

} else {  // priorita generale

    if($my){
        $select->from('t_osr_priorita', array('f_priorita', 'f_percorso'))
               ->where("f_gruppo ='" . $gruppo . "' AND INSTR('" . $percorso . "', f_percorso)");
    }
    else{
        $select->from('t_osr_priorita', array('f_priorita', 'f_percorso'))
               ->where("f_gruppo ='" . $gruppo . "' AND (CHARINDEX(f_percorso, '" . $percorso . "') <> 0)");
    }
    $res_data = $select->query()->fetchAll();
    
    //$que="SELECT f_priorita, f_percorso FROM t_osr_priorita WHERE f_gruppo='$gruppo' and INSTR('$percorso', f_percorso)";
   
    for($i = 0; $i < count($res_data); $i++){
        $pr  = $res_data[$i]['f_priorita'];
        $pe = strlen($res_data[$i]['f_percorso']);
        if($pe>$lun) { $priorita=$pr; $lun=$pe; } 
    }

} 

//--------------------------------------------------------------------------------
// squadra operatore

$squadre=""; $lun=0; $pe=0;

$select->reset();

if($my){
    $select->from('t_osr_squadra', array('f_squadre', 'f_percorso'))
           ->where("f_settori ='" . $settore . "' and f_gruppo='" . $grp . "' AND INSTR('" . $percorso . "', f_percorso)");
}
else{
    $select->from('t_osr_squadra', array('f_squadre', 'f_percorso'))
           ->where("f_settori ='" . $settore . "' and f_gruppo='" . $grp . "' AND (CHARINDEX(f_percorso, '" . $percorso . "') <> 0)");
}

//$que="SELECT f_squadre, f_percorso FROM t_osr_squadra WHERE f_settori='$settore' and f_gruppo='$grp' and INSTR('$percorso', f_percorso) ";

$res_data = $select->query()->fetchAll();



$squadre="";
for($i=0; $i<count($res_data); $i++){
    $sq  = $res_data[$i]['f_squadre'];
    $pe = strlen($res_data[$i]['f_percorso']);
    if($pe>$lun) { $squadre=$sq; $lun=$pe; } 
}





//--------------------------------------------------------------------------------

$alv=array(
"meccanici"=>65536,
"meccanici(squadrag)"=>65536,      //  ???
"elettricisti"=>131072, 
"ascensori"=>262144, 
"distribuzionegas"=>524288, 
"postapneumatica"=>1048576, 
"edilecivile"=>2097152, 
//"segreteria"=>8192, 
//"contabilità"=>16384, 
//"magazzino"=>32768,
"antincendio"=>4194304
);
     
   
$slv=array(
"65536"=>"meccanici",
"131072"=>"elettricisti", 
"262144"=>"ascensori", 
"524288"=>"distribuzione gas", 
"1048576"=>"postapneumatica", 
"2097152"=>"edile civile", 
//"8192"=>"segreteria", 
//"16384"=>"contabilità", 
//"32768"=>"magazzino",
"4194304"=>"antincendio"
);   
 
   
   


 
 
// livello visibility WO $level | checkbox divisione $divisione
$sep=""; $dd="";

if(!$divisione) { $level=-1; $li=0;
} else {
	
    $level=0; $adv=explode("/",$divisione);  
    for($r=0;$r<count($adv);$r++){ 
       if(isset($alv[$adv[$r]]) ) { $li=$alv[$adv[$r]]; $level+=$li; $dd.=$sep.$slv[$li];  $sep=",";  }   // $dd      
    }
 }
 

//---------------------------------------------------------------------------
echo json_encode(array(
  "gruppo"=>$gruppo,
  "selected"=>$selected,
  "settore"=>$settore,    
  "Divisione"=>$dd,
  "Priorita"=>$priorita,
  "Squadre"=>$squadre,
  "Level"=>$level,
  "nident"=>$nident,
  "localizzazione"=>$localizzazione,
  "zona"=>$zona
));


?>
