<?PHP
try{

$selOp = new Zend_Db_Select($this->db);

if ($_POST['arr_articolo'] != '') {
	$array_articolo=explode("|#|",$_POST['arr_articolo']);
	$art=$array_articolo[0];
    $vendor_code = $array_articolo[1];
    //$arr_utente = explode('/', $user_mail);
    //print_r($user);
    $selOp->reset();

    //return $resUser['fc_usr_mail'].",".$resUser['fc_usr_phone'].",".$resUser['cdc'].",".$resUser['descSede'];

    $resArticoli = $selOp->from(array('tw_asset' => 't_creation_date'), array('f_description','f_title'))
					->join(array('t_cross' =>'t_wares_relations'), 't_cross.f_code_ware_master = tw_asset.f_id',array(''))
					->join(array('tc_asset' =>'t_custom_fields'), 'tc_asset.f_code = tw_asset.f_id',array('commessa'))
                    ->where("f_code_ware_slave =?",$vendor_code)->where('f_phase_id=1')->where("f_title like '%".$art."%'")->query()->fetchAll();
    //echo($selOp);

    $array = array();
    $count = 0;

    foreach ($resArticoli as $value) {
        if ($count > 10)
            break;
        
        array_push($array, $value['f_title'] . "#|#" . $value['f_description']. "#|#" ."COMMESSA: ". $value['commessa']);
        $count++;
    }
    $string = implode(",,,", $array);
    return print($string);
}

} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
?>
