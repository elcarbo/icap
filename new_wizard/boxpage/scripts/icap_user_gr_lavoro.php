<?php

$user = $_GET['user'];
$cond = "IF(f_ware_id = $user".", 1, 0) as flag";
$selOp = new Zend_Db_Select($this->db);
$selOp->reset();
$res = $selOp->from(array('slp' => 't_selectors_parent'),array('f_code'))
    ->join(array('cd' => 't_creation_date'), 'cd.f_id = slp.f_code',array('f_title as reparto'))
    ->join(array('mim' => 'matrice_invio_mail'), 
        "REPLACE(REPLACE(REPLACE(gruppo_mittente, ' ', ''), '\r', ''),'\n','') = REPLACE(REPLACE(REPLACE(cd.f_title, ' ', ''), '\r', ''),'\n','')"
        ,array('gruppo_destinatario'))
    ->joinLeft(array('sw' => 't_selector_ware'), "sw.f_selector_id = slp.f_code AND sw.f_ware_id = $user",array($cond))
    //->where("sl.f_type_id = 5") //gruppo di lavoro
    ->where("slp.f_parent_code = 14") // figli di Gruppo di lavoro
    //->group(array("fc_usr_mail"))
    ->query()->fetchAll();

//    $sql = $selOp->__toString();
//    var_dump($sql); die;

$default_groups = array_filter($res, function($value) {
    return $value['flag'] == 1;
});

 $default_groups = array_map(function($value) {
    $value['gruppo_destinatario'] = str_replace(array(' ', "\n", "\r"), '', $value['gruppo_destinatario']);
    return $value;
}, $default_groups);

//print_r($default_groups); die;
$default_groups = array_column($default_groups,'gruppo_destinatario');

$res = array_combine(array_column($res,'reparto'), array_values($res));

foreach($res as &$reparto){
    if(array_search(str_replace(array(' ', "\n", "\r"), '', $reparto['reparto']),$default_groups)!== false) $reparto['flag'] = 1;
    else $reparto['flag'] = 0;
    unset($reparto['reparto']);
    unset($reparto['gruppo_destinatario']);
}

//print_r($res); die;
echo json_encode($res);