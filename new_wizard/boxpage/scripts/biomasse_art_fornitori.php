<?php
/*$param=json_decode($_POST["param"],true);
$fcodeware=$param["fcodeware"];
$percorso=$param["percorso"];
$settore=$param["settore"];

$zona=""; $localizzazione=""; $nident=""; $gruppo=""; $grp=""; $selected="";*/
$char = $_POST['char'];
$type = $_POST['type'];
$ware = $_POST['extra'];

$userData = Zend_Auth::getInstance()->getIdentity();

$selOp=new Zend_Db_Select($this->db);
$selOp->reset();
$selOp->from(array('wr' => 't_wares_relations'), array())
    ->join(array('wr2' => 't_wares_relations'), 'wr.f_code_ware_slave = wr2.f_code_ware_master', array())
    ->join(array('wr5' => 't_wares_relations'), 'wr2.f_code_ware_slave = wr5.f_code_ware_master', array())
    ->join(array('wr3' => 't_wares_relations'), 'wr2.f_code_ware_master = wr3.f_code_ware_master', array())
    //->join(array('wr4' => 't_wares_relations'), 'wr3.f_code_ware_slave = wr4.f_code_ware_master', array())
    //->joinLeft(array('pc' => 't_pair_cross'), 'pc.f_code_main = wr4.f_code_ware_slave AND pc.f_code_cross = wr4.f_code_ware_master')
    ->join(array('cd' => 't_creation_date'), 'cd.f_id = wr3.f_code_ware_slave', array('f_id','f_title','f_description'))
    ->join(array('cf' => 't_custom_fields'), 'cf.f_code = wr3.f_code_ware_slave',array('tipo_legno'))
    ->where('wr.f_type_id_slave = 28')
    ->where('wr2.f_type_id_slave = 6')
    ->where('wr3.f_type_id_slave = 3')
    //->where('wr4.f_type_id_slave = 1')
    ->where('wr5.f_type_id_slave = 16 AND wr5.f_code_ware_slave = ' . $userData->f_id)
    ->where('wr.f_code_ware_master = ?', $ware)
    ->where("cd.f_title like '%". str_replace("'", "''", $char) . "%'" );
//echo $selOp; die;
$resQty = $selOp->query()->fetchAll();
//print_r($resQty);
// recupero eventuali paircross con bosco
for($i = 0; $i < count($resQty); $i++){
    $selOp->reset();
    $selOp->from('t_pair_cross', array('tonnellate_prelevabili'))
            ->where('t_pair_cross.f_code_main = ' . $_POST['extra'] . ' AND t_pair_cross.f_code_cross = ' . $resQty[$i]['f_id']);
    $res = $selOp->query()->fetch();
    $resQty[$i]['tonnellate_prelevabili'] = $res['tonnellate_prelevabili'];
}

$array = array(
    '_type' => $type,
    '_data' => []
);
$count = 0;
//print_r($resQty); die;
foreach($resQty as $value){
    if($count > 10) break;
    $array['_data'][] = [
        'f_id' => $value['f_id'],
        'f_title' => utf8_encode($value['f_title']),
        'f_description' => utf8_encode($value['f_description']),
        'tipo_legno' => utf8_encode($value['tipo_legno']),
        'tonnellate_prelevabili' => $value['tonnellate_prelevabili']
    ];
    $count++;
}	
//print_r($array); die;
	//die($type);
return print(json_encode($array));
?>
