<?php
$f_code = $_POST['f_code'];
$selOp = new Zend_Db_Select($this->db);

$data = $selOp->from(["t1" => "t_ware_wo"],[])
	->join(["t4" => "t_workorders"], "t4.f_code = t1.f_wo_id AND t4.f_type_id = 9", ['f_code']) // wo di tipo ON CONDITION GENERATOR
	->join(["t5" => "t_creation_date"], "t5.f_id = t4.f_code", []) // wo di tipo ON CONDITION GENERATOR
	->where("t1.f_ware_id = $f_code")
	->where("t5.f_phase_id = 1")        
	->query()->fetchAll();

echo json_encode($data);
