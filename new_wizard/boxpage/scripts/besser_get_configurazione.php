<?php

 /* $param=json_decode($_POST["param"],true);
   $fcodeware=$param["fcodeware"];
   $percorso=$param["percorso"];
   $settore=$param["settore"];

   $zona=""; $localizzazione=""; $nident=""; $gruppo=""; $grp=""; $selected=""; */
 $char = $_POST['char'];
 $code_stampo = $_POST['stampo'];
 $query_type = $_POST['query_type'];

 $selOp = new Zend_Db_Select($this->db);
 $selOp->from(['t_wares'], array('f_code'))
       ->join(['t_creation_date'], 't_creation_date.f_id = t_wares.f_code', array('f_title', 'f_description'))
        ->join(['t_wares_parent'], 't_creation_date.f_id = t_wares_parent.f_code', array(''));
		
 
 if ($query_type !== 'custom') {
    
     $resQty = $selOp->where('t_wares.f_type_id = 3')->where("t_creation_date.f_title like '%" . str_replace("'", "''", $char) . "%'")
             ->where("t_creation_date.f_phase_id =1")->where('f_parent_code = ' . $code_stampo)
             ->query()->fetchAll();
 } else {
    
     $resQty = $selOp->join(['t_wares_relations'], 't_wares_parent.f_parent_code = t_wares_relations.f_code_ware_master', array(''))
			->join(['tc2'=>"t_creation_date"], 'tc2.f_id=f_code_ware_master', array('articolo' => 'f_title'))
             ->where('t_wares.f_type_id = 3')
             ->where("t_creation_date.f_title like '%" . str_replace("'", "''", $char) . "%'")
             ->where("t_creation_date.f_phase_id =1")
             ->where('f_code_ware_slave = ' . $code_stampo)
             ->query()->fetchAll();

 }
 

 $array = array();
 $count = 0;
if($resQty){
 foreach ($resQty as $value) {
     $value['f_title'] = isset($value['articolo']) ? $value['f_title'] . " (" . $value['articolo'] . ")" : $value['f_title'];
     if ($count > 10)
         break;
     array_push($array, $value['f_title'] . "||" . $value['f_description'] . "||" . $value['f_code']);
     $count++;
}}else{
	array_push($array, 'NESSUNA CONFIGURAZIONE TROVATA' . "||" . 'Verificare che lo stampo scelto abbia almeno una configurazione in anagrafica articoli.' . "||" . "0000");
}
 //return $array;



 return print(json_encode($array));
?>
