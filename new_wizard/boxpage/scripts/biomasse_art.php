<?php
/*$param=json_decode($_POST["param"],true);
$fcodeware=$param["fcodeware"];
$percorso=$param["percorso"];
$settore=$param["settore"];

$zona=""; $localizzazione=""; $nident=""; $gruppo=""; $grp=""; $selected="";*/
$char = $_POST['char'];
$type = $_POST['type'];
$ware = $_POST['extra'];

$selOp=new Zend_Db_Select($this->db);
$selOp->reset();
$selOp->from('t_wares_relations',array())
    ->join('t_creation_date', 't_creation_date.f_id = t_wares_relations.f_code_ware_slave',array('f_id','f_title','f_description'))
    ->join('t_custom_fields', 't_custom_fields.f_code = t_wares_relations.f_code_ware_slave',array('tipo_legno'))
    ->join('t_pair_cross', 't_pair_cross.f_code_main = t_wares_relations.f_code_ware_master AND t_pair_cross.f_code_cross = t_wares_relations.f_code_ware_slave', array('tonnellate_prelevabili'))
    ->where('t_wares_relations.f_type_id_slave = 3')
    ->where('t_wares_relations.f_code_ware_master = ?', $ware)
    ->where("t_creation_date.f_title like '%". str_replace("'", "''", $char) . "%'" );

$resQty = $selOp->query()->fetchAll();
$array = array(
    '_type' => $type,
    '_data' => []
);
$count = 0;

foreach($resQty as $value){
    if($count > 10) break;
    $array['_data'][] = [
        'f_id' => $value['f_id'],
        'f_title' => utf8_encode($value['f_title']),
        'f_description' => utf8_encode($value['f_description']),
        'tipo_legno' => utf8_encode($value['tipo_legno']),
        'tonnellate_prelevabili' => $value['tonnellate_prelevabili']
    ];
    $count++;
}	
	//die($type);
return print(json_encode($array));

?>
