<?php

// restituisce i centri di costo type 24   SUPPLIES AND SERVICES

$par=json_decode($_POST["param"],true);
$codef=$par["usr_code"];

$cdc=array();

$select = new Zend_Db_Select($this->db);

$select->from('t_creation_date', array('f_id','f_title'))                                                           
       ->join('t_custom_fields', 't_creation_date.f_id = t_custom_fields.f_code', array('cdc', 'societa') )        // 'cdc', 'societa'        
       ->where("t_creation_date.f_category='SUPPLIES AND SERVICES' AND f_phase_id=1")
       ->order('t_custom_fields.cdc ASC')
       ->limit(2000);
$res_d = $select->query()->fetchAll();

                
for($i=0; $i<count($res_d); $i++){   

    $fid=$res_d[$i]['f_id'];
    $tit=utf8_decode($res_d[$i]['f_title']);
    $cdf=$res_d[$i]['cdc'];
    $soc=$res_d[$i]['societa'];
    
    if(!$cdf) $cdf="";
    if(!$soc) $soc="";

    
    $cdc[] = array( $fid, $tit, $cdf, $soc );                   // 'cdc', 'societa'  
}

$select->reset();

$select->from('t_wares_relations', array('f_code_ware_slave'))
       ->where("f_code_ware_master =  ". $codef)
	   ->where("f_type_id_slave = 24");
$res_d = $select->query()->fetch();

$usrcross=$res_d['f_code_ware_slave'];

 
echo json_encode( array("list"=>$cdc, "usr_cross"=>$usrcross) );
die();                                           


?>