<?php

$term= $_GET['term'];
//var_dump($term); die;
$selOp=new Zend_Db_Select($this->db);
$selOp->reset();
$resQty = $selOp->from(array('wa' => 't_wares'),array())
->join(array('cd' => 't_creation_date'), 'cd.f_id = wa.f_code',array('f_id','f_title','f_description'))
->join(array('cf' => 't_custom_fields'), 'cf.f_code = cd.f_id',array(''))
->where('wa.f_type_id = 27')
->where("cd.f_title like '%".$term."%' ")
->where("cd.f_phase_id <> 6" ) 
->query()->fetchAll();

$list = array();
foreach($resQty as $asset){
  $item = array();
  $item ["id"] = $asset['f_id'];
  $item ["value"] = $asset['f_title'];
  $item ["label"] = $asset['f_title']." - ".$asset['f_description'];
  array_push($list, $item);
}

//var_dump($list); die;

$availableTags = $list;
echo json_encode($availableTags);