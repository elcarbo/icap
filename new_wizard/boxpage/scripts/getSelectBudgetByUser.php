<?PHP

$selOp = new Zend_Db_Select($this->db);

$selOp->reset();

if(!empty($_POST['user'])) {
    $selOp->from(["t1" => "t_creation_date"],array('f_title','f_id'))
            ->join(["t2" => "t_wares_relations"], "t2.f_code_ware_master = ".$_POST['user']." AND t2.f_code_ware_slave = t1.f_id", [])
            ->join(["t3" => "t_wares"], "t2.f_code_ware_slave = t3.f_code AND t3.f_type_id = '28'", []);
            $res = $selOp->query()->fetchAll();

}
 
$data = array();

for($i = 0; $i < count($res); $i++) {
    $data[$i] = [];
    $data[$i]['f_code'] = $res[$i]['f_id'];
    $data[$i]['f_title'] = utf8_decode($res[$i]['f_title']);
}

#print_r(array_values($data));
print json_encode(array_values($data)); 

exit;