<?php

if ($_POST['par'] != '') {

    $selOp = new Zend_Db_Select($this->db);
        $selOp->reset();
        $resQty = $selOp->from('t_wares as t0', array())
                    ->join('t_custom_fields as t1', 't1.f_code = t0.f_code', array("f_code"))
                    ->join(array("t2"=>'t_creation_date'), 't2.f_id = t0.f_code', array('asset_title'=>'f_title'))
                    ->where('t0.f_type_id = 1')
                    ->where("t2.f_title like '%".$_POST['par']."%'")
                    ->where("t2.f_phase_id <> 3")->query()->fetchAll();
               //echo $selOp;die;
        $array = array();
        $count = 0;
        
        foreach($resQty as $value) {
            if ($count > 8)
                break;
            $value['asset_title'] = Mainsim_Model_Utilities::chg($value['asset_title']);
            if(!in_array($value['asset_title'], array_column($array,'asset_title'))) array_push($array,$value);
            $count++;
        }
        
        $data = array();
        foreach($array as $values) {
            array_push($data, implode("#*;",array_values($values)));
        }

        return print(json_encode($data));
}
?>
