<?php
/*$param=json_decode($_POST["param"],true);
$fcodeware=$param["fcodeware"];
$percorso=$param["percorso"];
$settore=$param["settore"];

$zona=""; $localizzazione=""; $nident=""; $gruppo=""; $grp=""; $selected="";*/
$asset = $_POST['asset'];

$selOp=new Zend_Db_Select($this->db);
$selOp->reset();
$resQty = $selOp->from('t_wares',array())
->join('t_creation_date', 't_creation_date.f_id = t_wares.f_code',array('f_title'))
->join('t_custom_fields', 't_custom_fields.f_code = t_creation_date.f_id',array('produttore','produttore_partita_iva','produttore_codice_fiscale','autorizzazione_taglio', 'tipo_legno',
    'codice_contratto','certificazione','tonnellate_prelevabili','proprietario_ragione_sociale','proprietario_codice_fiscale','proprietario_partita_iva'))
->where('t_wares.f_type_id = 1')->where("t_wares.f_code = ". $asset)
->where("t_creation_date.f_phase_id <> 3" ) 
->query()->fetch();

$selOp->reset();
$resven = $selOp->from('t_wares',array('fc_vendor_address','fc_vendor_zipcode','fc_vendor_city','fc_vendor_country'))
->join('t_creation_date', 't_creation_date.f_id = t_wares.f_code',array('f_title'))
->join('t_custom_fields', 't_custom_fields.f_code = t_creation_date.f_id',array('partita_iva','codice_fiscale','provincia'))
->where('t_wares.f_type_id = 6')->where("t_custom_fields.azienda_proprietaria = 1")
//->where("t_creation_date.f_phase_id <> 3" ) 
->query()->fetch();

$array = array(
 "produttore" => $resQty['produttore'],
 "proprietario_suolo" => $resQty['proprietario_ragione_sociale'],
 "proprietario_suolo_codice_fiscale" => $resQty['proprietario_codice_fiscale'],
 "proprietario_suolo_partita_iva" => $resQty['proprietario_partita_iva'],
 /*"produttore_partita_iva" => $resQty['produttore_partita_iva'],
 "produttore_codice_fiscale" => $resQty['produttore_codice_fiscale'],*/
 "proprietario_ragione_sociale" => $resven['f_title'],
 "proprietario_codice_fiscale" => $resven['codice_fiscale'],
 "proprietario_partita_iva" => $resven['partita_iva'],
 "autorizzazione_taglio" => htmlentities($resQty['autorizzazione_taglio']),
 "tonnellate_prelevabili" => $resQty['tonnellate_prelevabili'],
 "codice_contratto" => $resQty['codice_contratto'],
 "tipo_legno" => $resQty['tipo_legno'],
 "certificazione" => $resQty['certificazione'],
 "indirizzo" => $resven['fc_vendor_address']." ".$resven['fc_vendor_zipcode']." ".$resven['fc_vendor_city']." ".$resven['provincia']." ".$resven['fc_vendor_country']
 );

//var_dump($array); die;
return print(json_encode($array));

?>
