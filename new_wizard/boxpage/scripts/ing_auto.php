<?php

// restituisce gli asset prenotabili (fc_asset_bookable=1)

$param=json_decode($_POST["param"],true);


$select = new Zend_Db_Select($this->db);

$select->from('t_creation_date', array('f_id','f_title'))
       ->join('t_wares', 't_creation_date.f_id = t_wares.f_code' )
       ->where('t_wares.fc_asset_bookable = 1');
$res_data = $select->query()->fetchAll();

$auto=array( array("value"=>0, "text"=>"Selezionare") );
for($i=0; $i< count($res_data) ; $i++){
    $fc  = $res_data[$i]['f_id'];
    $tit = $res_data[$i]['f_title']; 
    
    $auto[]= array("value"=>$fc, "text"=>$tit );
      
}

echo json_encode($auto);




?>