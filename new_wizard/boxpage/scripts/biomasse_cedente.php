<?php

$userData = Zend_Auth::getInstance()->getIdentity();

$selOp=new Zend_Db_Select($this->db);
$selOp->from('t_wares_relations', array())
    ->join('t_wares', 't_wares_relations.f_code_ware_slave = t_wares.f_code', array('fc_vendor_address', 'fc_vendor_zipcode', 'fc_vendor_city', 'fc_vendor_country'))
    ->join('t_creation_date', 't_creation_date.f_id = t_wares.f_code', array('f_id', 'f_title'))
    ->join('t_custom_fields', 't_creation_date.f_id = t_custom_fields.f_code', array('partita_iva','codice_fiscale','provincia'))
    ->where('t_wares_relations.f_type_id_slave = 6')
    ->where('t_wares_relations.f_code_ware_master = ' . $userData->f_id);
$res = $selOp->query()->fetch();

$response = [];
foreach($res as $key => $value)
    $response[$key] = utf8_encode($value);	

return print(json_encode($response));

