<?php
$term= $_GET['term'];

$selOp=new Zend_Db_Select($this->db);
$selOp->reset();
$resQty = $selOp->from(array('wa' => 't_wares'),array())
->join(array('cd' => 't_creation_date'), 'cd.f_id = wa.f_code',array('f_id','f_title','fc_hierarchy'))
->join(array('cf' => 't_custom_fields'), 'cf.f_code = cd.f_id',array('categoria_asset'))
->where('wa.f_type_id = 1')->where("wa.fc_asset_class = 'Apparecchiatura'")
->where("cd.f_title like '%".$term."%' OR cf.categoria_asset like '%".$term."%' ")
->where("cd.f_phase_id = 1" ) 
->query()->fetchAll();


$list = array();
foreach($resQty as $asset){
    array_walk_recursive($asset, function(&$value, $key){
        if(!mb_detect_encoding($value, 'utf-8', true)){
                $value = utf8_encode($value);
        }
    });
  $item = array();
  $item["id"] = $asset['f_id'];
  $item["value"] = $asset['f_title'];
  $item["title"] = $asset['f_title'];
  $category = json_decode($asset['categoria_asset'],true);
  $item["category"] = $category['labels'][0];
  $item["hierarchy"] = $asset['fc_hierarchy'];
  $item["term"] = $term;
  array_push($list, $item);
}

echo json_encode($list);