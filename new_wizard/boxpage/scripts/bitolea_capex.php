<?php

$selOp = new Zend_Db_Select($this->db);
$selOp->from('t_creation_date', ['f_id', 'f_title'])
          ->join('t_wares', "t_creation_date.f_id = t_wares.f_code", [])
          ->where("t_wares.f_type_id in ('7')")
            ->where("t_creation_date.f_wf_id in ('9')")
            ->where("t_creation_date.f_phase_id in ('1')")
	->order('f_title asc');
$data['commesse'] = $selOp->query()->fetchAll();
foreach ($data['commesse'] as $key=>$value) {
	$data['commesse'][$key]['f_title'] = utf8_encode($data['commesse'][$key]['f_title']); 
}
return print(json_encode($data));
?>
