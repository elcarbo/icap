<?php

// restituisce gli asset prenotabili (fc_asset_bookable=1)
$type=json_decode($_POST["param"],true);
if($type == 0){
    $sel_id = 5462;
}
else{
    $sel_id = 5461;
}


$select = new Zend_Db_Select($this->db);

$select->from('t_creation_date', array('f_id','f_description', 'f_title'))
       ->join('t_custom_fields', 't_creation_date.f_id = t_custom_fields.f_code', array('descrizione','qta_min_ordinabile','qta_max_richiedibile','multiplo_ordine'))
       ->join('t_selector_ware', 't_selector_ware.f_ware_id = t_creation_date.f_id', array())
       ->where('t_selector_ware.f_selector_id = ' . $sel_id)
       ->where('t_creation_date.f_phase_id = 1');
$res_data = $select->query()->fetchAll();
$result = array();
for($i=0; $i< count($res_data) ; $i++){
    
    $result[] = array(
        $res_data[$i]['f_id'],
        utf8_encode($res_data[$i]['f_description']),
        utf8_encode($res_data[$i]['descrizione']),
        utf8_encode($res_data[$i]['f_title']),
        $res_data[$i]['qta_min_ordinabile'],
        $res_data[$i]['qta_max_richiedibile'],
        $res_data[$i]['multiplo_ordine'],
    );
}
echo json_encode($result);
?>