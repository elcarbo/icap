// JavaScript Document

/*

Debug method:
Open()
Close()
Hide()
viewTab(t)   t=string of db table || json
view(s)      s=string with HTML to display in window || var to display
viewHelp()


require:

jsTrim
isTouch                 // jsBrowser();
jsCreateElem
jsCoords
cancelEvent
*/


Debug={  FD:[], BOXj:null, Vj:null, jt:null, J:null, MASKj:null, odwn:"",
         ListCmd:[], ListPtr:0,
         closegif:"data:image/gif;base64,R0lGODlhIAAgAIAAAAAAAP///yH5BAEAAAEALAAAAAAgACAAAAIxjI+py+0Po5y02ouz3rwrAHyhBo5lV55e6h1ga7BrOm6qmr2JDvf+DwwKh8Si8bgpAAA7",
         resizegif:"data:image/gif;base64,R0lGODlhIAAgAJEDAJmZmY6OjqWlpf///yH5BAEAAAMALAAAAAAgACAAAAJNnI+py+0Po5y02ouz3rxz4SECGA5jUI4A6p1s54ZxKwTvV98kNhtj1jPVeDlR0RIMTpLHCtN2ed6WTSHUt3tID6fIFnt1fH22aemMKAAAOw==",
         gearpng:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAIoElEQVR42mJMSkpiQAa/fv1iYGRkZEAHP3/+BNOsrKwMzMzMDF++fGFgY2Nj+P//vzFQj/u/f//sgNK2QPkJLCws1TB9IDXI4OvXrw+Aat8A8U6g2u1A+SM/fvxgYGdnB5v9/ft3kJkMIiIiYD46+PDhAwofIIBYGMgDXEBLgoCeqvz7968WssSfP3/SeHl5W4DM7yA+yHEgB4EA0NHuv3//lgcyQRjk8Sqg2EOg/AQgfwkQvyHVIQABxESqBqCDCz5//vwQSC9GdzzUkSJAR7rAYhMUc6BYgMZWIbp6oIflgeb0f/v27SFQfT8ocEhxD0AAEe0BoOXaQIddA1rUD7RQBJ9aoENCockF5ECwJ4DJUglIu+MxnwtodgHQDlCMuBDrLoAAYsFiEAMTExM82pHE64EO1yTGUKBD/YExIQjE70F8ULoGeiSOGL2gGATatRuoXgfIvYoW+xjqAQKIBYsBYIUcHBxgNgwAM9RiWMgSEVt8QLXvkMWAIUt0sgAWEqCYxnA8KDa5uFBTGEAAYU1CIMvQfQv0wB5gMnjDQAcALJFWg0ogGAalCOTARAYAAYQzD4A8AcKgIhWKvwMNXkIPDwDtWgQKbRAGBSTIHehJGgYAAghvJgYZACoGQSEAwpycnLPoEPqHgcnkHtAukH1gD+AKfRAACCBs9YASEN9D9gTI8aBYAFZeOviiEwkcB+InWMQJ5iFgaF8H2QGsDBk+ffqENeMiA4AAYkGudYGajYGZ5wyQeRZoCKxyAZfnQEfHAUuXhUQGJKg8X40tfxNRCqUBY12Qn58/DK1gcAF6pgQotw6YL+ApASCAWJCLT2C5vRia1oyBjl0M9EQ/CAOLwRtAT8xkoBMANhdCgQ5NAzLXAR0dA8RpQI9pQgPTFui5ncCS6iGIDxBALKDiEgSglYgmepkMxK1kuAFUEQlS4glgAM4EBiZGzQyq8IBJqwWYT2JBfIAAYoGmZ66PHz9WUzEQ0yg1AJoSsDYrgLETA8wjbcDUcR0ggJiADmd48+ZNDDCzijAMIQB0byUoowMEEBOokQWMhSqGIQaASSwWmDcFAQKICegTY2gTlyoAGK1vgLgAWJ6DWqqgIo4RyFYAisUDM951atkDSmLAgHcBCCAmYK4OopahQAe2ASsfeWDRPBHIRXbsQ1DtCoxtLaBn0kFlBpU84QIQQExAX9hSqQZNBzqympDjgDExC1jyeQDVUuwJoNuNAQII5AEVSg0ChnoFqJmB3n3EWnMCMx43N/dhPj4+f2xdVxI7V8YAAcQEDDkpYM0WDAyZw+QYAtR7HVgmd4L6yaC2Cz5PgOocAQEBcJMY6IE9QHoLBQ2+60A70wECiAnKWQe02A4ooAUMIVA1/QNNPSg9m0BxG1qSmADqsMAwqPcFCmVsIQ/KeMDuKByD8gWx9kDdCbJvCdAsOyDWAiVHgABiwWIAKJPZADFyfxeUXs9C2cZooXoYS6ygjGTAmuQwPhI4isbHaQ/ULlFgifkGOekBBBALKFRAArC2N6kAWBFeJxDV4CY5DvCMFLuAHmADJVNYMx8EAAIIlInBloDSMAhTE4DyA8hCagFgAP+CNe9hTW6AAGJBT6cgTwDrBg4SQgVUCT7EFvKwGAVlXmz5AgikQANkxAKg2jewvjrIbJAdAAGE3COTB7U8gT58De3UEFvzusPGfZAxlszHwMPDg4KB6qxJLHkWg0b/kMUAAgiUB4JA7W2gw93x6DXG1RkBDXSJiIjMQmopgseDsKhjePXqFdgjSBWRL7H2QM2IATb7Y4CpBJTRQR2utQABxKyqqnoOaJAGBelSFJjkPgAD4iSoZQsqaWDjSrAxJlgsgNIvKCkB6wCQuA1QbSc5w5tAc6VAAQ90dzxAADEBfXOa0swFDPUJoFgkZuwHmi9sgSG5E9QCoXD04iVAADEBQ+QwpR4AhTSwlwTqQVURGtsEhdy7d+92AD3CRam9QA+cBQggJlAtTK3mLdBRrcBkcQZI50NHoGFABCgWB1RzCEivBXULqWEnsCA4CxBAjJGRkaAk8ABoMNX6BMilDwgTMQxDltnARqEQQACBGnOgyqafFr0maKeDJj0yYH2wGojfAwQQE7Rmmw3qSQ2xXmU9sCBgAAggWBH2DeibVtDYP47KitSQ3IM8ukfN0QpoE2UJrHsKEEAsSG2VWaDBJNgAEhCA+rZLQE1WoBoRYDV+CNcAKxYwC8fIXBoJSaQNWCyDWgUFyPkT1JMDpv0aWLsNIICYYI04IP4mJCRUCCxWHwJ9WABsEssDxQpBHQdBQcHDQNxIr7QBSt/Ajk81qK8BdIcCsOMTDHQXuJkNrARbgQH6ENZkAQggFrSo2QkaQQAlF1DegOV2EB8o3sDLy/sd2BHpIMINoEFcbTL71p1AB9ejlTjrgOLrgB4zBnrgLHJ/ACCACFbjoCQGyizQtgsfke4IpaB4PAVMyj9hw/oYFReaGEAAMeErZ0H9V1AxC253s7BwAruMabROPqBpWqRhGoKFB0AA4YwBUMcbNKyOZLALaLCXDh5wByZVJWBI30NuP+FqZwEEEBOeBhq4ZQnDQH4ovTIx+mQiKCZwjXYABBCGB2DzYrBuGwwDi1B/EsrpTcAkxwgsuZRFRUV1gA5gBM08kjDuGQdKOsgY5CZsngAIIBZsHkBOOkiZ2RsYC7OAHiFmrngOtKQAJwNguQ0ydxEwDxEswYCB9Q2otwFbVxNb/xoggJhIKB2OAEPABBiyhfimW0FNEmAa3gPqtICiHlQcQ5PAagJWfAOqmwAaWwWasZpYdwEEEKlrJUChA6pc5IFlciFyRkNKPrOAMfgdFIsgx8OSI9Dj94D6DmPzMFBPG9DxoEFh0FoKktpkAAFE7moVcGgBi1jQ0hobYB/YGMi3BqZVbWAbfSp6axTWBAGq7wZ6Shro6GtAfBao9wBQ+ABST41kABBgAMBUG/NDhgEmAAAAAElFTkSuQmCC",
         
Open:function(){
with(Debug){

var l,r,f,s="",j,mj;

  jsBrowser();
  odwn=isTouch?"ontouchstart":"onmousedown";

  MASKj=jsCreateElem("idDebugMask","div");
  MASKj.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF6;opacity:0.0;z-index:10010;display:none;");

  BOXj=jsCreateElem("idDebugBox","div");
  BOXj.setAttribute("style","position:absolute;left:10%;top:10%;width:70%;height:70%;background-color:#AAA;z-index:10000;"+
                            "box-shadow:3px 6px 12px #555;border-radius:6px;text-shadow:none;");
 
  str="<div id='idDebugBar' style='position:absolute;left:0px;top:0px;right:2px;height:28px;cursor:move;' >"+
    "<img src='"+gearpng+"' style='position:absolute;left:8px;top:4px;width:20px;cursor:pointer;' "+odwn+"='Debug.Hide()' />"+
    "<div style='position:absolute;left:34px;top:7px;font:bold 13px Arial;color:#000;'>Debug database</div>"+
    "<img src='"+closegif+"' style='position:absolute;right:0px;top:0px;cursor:pointer;' "+odwn+"='Debug.Close()' /></div>"+


    "<div style='position:absolute;left:2px;bottom:2px;height:32px;right:32px;'>"+
    "<input id='idDebugCmd' type='text' value='' style='position:absolute;left:0px;top:2px;width:100%;height:30px;background-color:#666;"+
    "border:0px solid #000;font:13px Arial;color:#FFF;padding:4px 2px 4px 8px;height:22px;' onkeyup='Debug.f_key(event)' /></div>"+
    
    "<img id='idDebugResize' src='"+resizegif+"' style='position:absolute;right:0;bottom:0;cursor:nw-resize;' />"+
    
    "<div id='idDebugView' style='position:absolute;left:2px;top:28px;right:2px;bottom:34px;background-color:#555;"+
    "overflow:auto;font:12px Arial;color:#FFF;padding:8px 4px 4px 8px;'></div>";
    
   BOXj.innerHTML=str;

    j=js$("idDebugBar");
    mj=js$("idDebugResize");
    J=js$("idDebugView");
    
    Vj=js$("idDebugView");
    jt=js$("idDebugCmd");

    // eventi move
    l=["touchstart","touchmove","touchend","mousedown"];
    r=3,f=4; if(isTouch) r=0,f=3; 
    for(;r<f;r++) { eval("j.on"+l[r]+"=function(e){Debug.f_evt(e,0);}");
                    eval("mj.on"+l[r]+"=function(e){Debug.f_evt(e,1);}"); }
    
    jt.focus();
}},


Close:function(m){
with(Debug){
 BOXj.style.display=m?"block":"none";
 var bj=js$("idDebugHideBox");
 if(bj) bj.style.display="none";
 
}},


Hide:function(){
with(Debug){
    Close();
    var bj=jsCreateElem("idDebugHideBox","div");
    bj.setAttribute("style","position:absolute;right:4px;bottom:4px;opacity:0.8;display:block;z-index:10000;");
    bj.innerHTML="<img src='"+gearpng+"' style='cursor:pointer;' onclick='Debug.Close(1); this.style.display=\"none\"' />";
}},


f_key:function(e){
with(Debug){

  if(!e) e=window.event;
  var cc,CC,c,C,jt,v,a,m,s="", er=0,na, wt=0, nodbl=0;
      t=(e.which)?e.which:e.keyCode;

  if(t==8 && e.shiftKey) { jt.value=""; return;}

  if(t==13) {
      cc=jsTrim(jt.value);
      if(cc){
          ListCmd.push(cc); 
          ListPtr=ListCmd.length; 
          if(ListPtr>32) { ListCmd.shift(); ListPtr--; }
          
          c=cc.toLowerCase();
 
            if(c=='quit' || c=='exit') { Close(); return; }
            if(c=='help' || c=='?') {  viewHelp(); return; }
            if(c=='hide') { Hide(); jt.value=""; return; }
            if(c=='? #' || c=='?#' || jsTrim(c)=="#") {  viewDblHelp(); return; }
            if(c=="? * -t") { viewAllTab(); return; }
                       
            if(c=="cls") J.innerHTML="";
            else { J.innerHTML+="<br><span style='color:#BBB;'>"+cc+"</span><br>";       
                   try{ v=Dbl.AUTOSAVE; } catch(e){ nodbl=1; Dbl={} }             
              with(Dbl){
                  // # query
                  if(cc.indexOf("#")>-1) {  if(nodbl) return;
                    wt=1;
                    C=cc.split("#");
                    CC=C.shift();
                    cc=CC+" query(\""+jsTrim(C.join("#"))+"\")";
                    s=cc.toLowerCase();
                  }
              
                  // ?   ? element -t -j
                 if(c.charAt(0)=="?") {  
                   s=cc.substring(1);
                   s=s.replace(/\s{2,}/g, ' '); 
                   s=jsTrim(s);
 
                   if(s=="-t"){ if(nodbl) return;
                                if(last_tab!="")  s=last_tab+" -t" }
 
                   a=s.split(" ");
                   na=a.length;

                   if(na>1){
                      if(a[na-1]=="-t") { wt=1;    // viewTable
                        if(!nodbl) { if(f_verify(a[0])) a[0]="'"+a[0]+"'"; }
                        a.pop(); }
                   } 

                    s=a.join(" ");
                    if(wt) s="viewTab("+s+");";
 
                     try{ v=eval(s); } catch(e){ jt.style.color="#F88"; er=1;
                                                 J.innerHTML+="<span style='color:#E88;'>"+e+"</span><br>"; }
                     
                     if(!er) { c=typeof(v);
                          if(c!="undefined") { 
                             if(c=="object") v=JSON.stringify(v);
                             J.innerHTML+=v+"<br>"; 
                          }
                     } 
 
                 } else  try{  eval(cc);
                         } catch(e){  jt.style.color="#E88"; er=1; 
                                      J.innerHTML+="<span style='color:#E88;'>"+e+"</span><br>"; }       
              }
          }
    
          if(!er) jt.value="";  
          f_scrollEnd();  
      }
  } else { jt.style.color="#FFF";
            
           if(t==38) { ListPtr++; if(ListPtr>=ListCmd.length) { ListPtr--; return; }
                       jt.value=ListCmd[ListPtr];  }
                       
           if(t==40) { ListPtr--; if(ListPtr<0) { ListPtr++; return; } 
                       jt.value=ListCmd[ListPtr];  }
  }

}},
 

f_scrollEnd:function(){
with(Debug){    
  var h,H;
  h=J.scrollHeight, H=J.offsetHeight;
  J.scrollTop=(h-H);
}},


// events
f_evt:function(e,g){
with(Debug){  
       
    var ev=e.type, nt=1, BOXj, t,x,y, i,ex,ey, P,F, v;
    if(isTouch) var tch=e.changedTouches, nt=tch.length;
  
  if(ev=="touchstart" || ev=="mousedown"){ 
          if(isTouch) t=tch[0], x=t.pageX, y=t.pageY, i=t.identifier;
          else { P=jsCoords(e), x=P.x, y=P.y, i=0;
                 MASKj.style.display="block";     
                 MASKj.onmousemove=function(e){f_evt(e,g); }
                 MASKj.onmouseup=function(e){ f_evt(e,g); }  
                 
               }                                       
          P=jsGetPos_ltwh(BOXj);                
          FD.push({ x:x, ix:x, iy:y, f:i, pl:P.l, pt:P.t, pw:P.w, ph:P.h });                             
   }
 
  if(ev=="touchmove" || ev=="mousemove"){
    if(FD.length) { F=FD[0];              
         for(r=0;r<nt;r++) { 
            i=0; if(isTouch) t=tch[r], i=t.identifier;   
            if(F.f!=i) continue;
              if(isTouch) ex=t.pageX, ey=t.pageY; else P=jsCoords(e), ex=P.x, ey=P.y; 
              x=ex-F.ix, y=ey-F.iy;
              if(g){   // resize
                x+=F.pw; if(x<160) x=160;
                y+=F.ph; if(y<120) y=120;
                BOXj.style.width=x+"px";
                BOXj.style.height=y+"px";       
              } else {  // move
                x+=F.pl;
                y+=F.pt; 
                BOXj.style.left=x+"px";
                BOXj.style.top=y+"px";   
              }        
    }}}
   
  if(ev=="touchend" || ev=="mouseup"){  
   if(FD.length){
      MASKj.onmousemove="";
      MASKj.onmouseup="";
      MASKj.style.display="none";
      FD=[];                    
   }} 
cancelEvent(e);   
}},


//----------------------------------------------------------------------------
// Views

viewAllTab:function(){
with(Debug){
  var r,a=Dbl.SHOWTABLES();
  for(r=0;r<a.length;r++) viewTab(a[r]);
  jt.value=""; 
}},

viewTab:function(t){     // t:   string=name of Dbl table   |  json [ {...},... ]   [ [...],... ]
  var r,s="",k,n,m,jj,cj, jd=[],z="";
  cj=Debug.Vj;
  
  if(typeof(t)=="string"){   // table
    with(Dbl){
      if(typeof(db[t])=="undefined") { cj.innerHTML+="table not found<br>"; return; }
      jd=db[t]["data"];
      n=db[t]["field"].length;
      info="<br>Database: <b>"+active_db+"</b> - Table: <b>"+t+"</b>";
      z="<tr>"; for(r=0;r<n;r++) z+="<th>"+db[t]["field"][r]+"</th>"; z+="<tr>";   // header
    }                                        
  } else { 
      if(typeof(t)!="object") s=t;
      else {
           jd=t; info="<br>Json view as table:";    // json resp
            
           if(!jd.length) { cj.innerHTML+="empty table."; return; }
           k=1;
           if(typeof(jd.length)=="undefined") k=2;
           else { if(typeof(jd[0].length)!="undefined") k=0;
           else if(typeof(jd[0])!='object') k=0; }
  
      jj=(k==1)?jd[0]:jd;
      if(k) { z="<tr>"; for(r in jj) z+="<th>"+r+"</th>"; z+="<tr>"; }
    }
  }  

  if(!s){
    s="<style>.cltable th { background-color:#AAA; font:bold 11px Arial; color:#444;  } "+
      ".cltable td { background-color:#888; font:11px Arial; color:#FFF;  } </style>"+
      "<div style='font:12px Arial;color:#DDD;'>"+info+"</div>"+
      "<table class='cltable' cellpadding='4px' cellspacing='2px'>"+z;     
     
    // body     
    for(r=0;r<jd.length;r++){   
      if(!jd[r]) continue;   // erased row
      s+="<tr>";         
      for(k in jd[r]) { col="";
           if(typeof(jd[r][k])!="string") col="style='color:#DEF'";
           s+="<td "+col+">"+jd[r][k]+"</td>";  
      } s+="</tr>";
    }  s+="</table>";
  }    
      
 cj.innerHTML+=s+"<br>";
 Debug.f_scrollEnd();
},


view:function(s){
with(Debug){
  c=typeof(s);
  if(c=="object") s=JSON.stringify(s);
  J.innerHTML+=s+"<br>";   
  f_scrollEnd(); 
}},



viewHelp:function(){
with(Debug){ 

  var s="<span style='font:15px Arial;line-height:20px;'><br><b>Help:</b><br>"+
        "<b>?</b> <i>(print var <b>-t</b> view table )</i><br>"+
        "<b>#</b> <i>(execute a query if local database exist)</i><br>"+
        "<b>cls</b> <i>(clear debug screen)</i><br>"+
        "<b>shift + canc</b> <i>(delete command row)</i><br>"+
        "<b>hide</b> <i>(hide debug window)</i><br>"+
        "<b>quit</b> <i>(close debug window)</i><br></span>"+
        "<br><b><i>Examples:</i></b><br>"+
        "jsOpacity(BOXj,85);<br>"+
        "? t_request -t<br>"+
"? -t (view last table)<br>"+    
"? * -t (view all table)<br>"+       
"# SELECT t_request GET f_code, f_title WHERE f_active>0 ORDER BY f_code DESC LIMIT 0,5<br>"+
"# UPDATE t_request SET f_description='--updated--', f_active=2 WHERE f_code&lt;5<br>"+
"# INSERT t_request SET f_code=10, f_title='Test', f_description='inserimento', f_real=0.02<br>"+
"# DELETE t_request WHERE f_code>3<br>"+
"? # SELECT t_request * WHERE progre>7<br>&nbsp;<br>";
        
  J.innerHTML+=s;     
  f_scrollEnd();
  jt.value="";
}},


viewDblHelp:function(){
with(Debug){ 

var s="<div style='font:bold 18px Arial;color:#FFF;'>Database Command List.</div>"+


"<br><span style='font:bold 14px Arial;color:#FFF;'>Properties:</span><br>"+
"- AUTOSAVE<br>"+
"- active_db<br>"+
"- last_tab<br>"+

"<br><span style='font:bold 14px Arial;color:#FFF;'>Method:</span><br>"+
"- list_db();<br>"+
"- reset_db(db_name);<br>"+
"- exist_db(db_name);<br>"+
"- create_db(db_name);<br>"+
"- save_db();<br>"+
"- save_tab(last_tab);<br>"+
"- connect_db(db_name);<br>"+
"- query(string, json);<br>"+

"- SHOWFIELDS(table)<br>"+
"- SHOWTABLES()<br>"+
"- SHOWVARS()<br>"+
"- SETVAR(key,value);<br>"+
"- GETVAR(key);<br>"+
"- DELVAR(key)<br>"+

"<br><span style='font:bold 14px Arial;color:#FFF;'>String for query():</span><br>"+
"- CREATE_TABLE table_name field type default, ...<br>"+
"- DROP_TABLE table<br>"+
"- TRUNCATE table<br>"+
"- INSERT table SET field=value, field=value, ...<br>"+
"- UPDATE table SET field=value, field=value, ... WHERE field op value<br>"+
"- SELECT table GET field1, field2, ... WHERE field op value ORDER BY field ASC/DESC LIMIT f,n<br>"+
"- DELETE table WHERE field op value<br>"+ 
"- INSERT table | json<br>"+  
"- UPDATE table WHERE field op value | json<br>";

  J.innerHTML+=s;     
  f_scrollEnd();
  jt.value="";
}}

} //