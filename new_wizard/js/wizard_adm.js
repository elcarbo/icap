// wiz administrator
/*

"name of file shown in form when tile is selected\n?fullscreen shows form using max width"
"example: ‘f_priority‘:‘2‘,‘f_description‘:‘id_path‘"
"link to ware:\nroot: links to top element.\nShow exit tile: show an ‘exit tile‘ from the second level onwards"
"list of system codes to include or exclude or not hyerarchy in ware connection"

"File"
"New"
"Edit"
"Select"
"Copy"
"Paste"
"Move"
"Paste Tree"
"Paste Alias"
"Delete"



"File\nShortcut: (F)"
"WO field sintax error!"
"New tile\nShortcut: (N)"
"Edit tile\nShortcut: (E)"
"Select box\nShortcut: (S)"
"Shortcut: (CTRL + C)"
"Shortcut: (CTRL + V)"
"Shortcut: (CTRL + X)"
"Shortcut: (CTRL + SHIFT + C)"
"Shortcut: (CTRL + W)"
"Shortcut: (CTRL + SHIFT + D)"

"Error in selector sistem codes"
"Delete tiles?"
"Wrong value!"
"Use Paste Alias and Delete to move alias tile"
"Error in selector sistem codes!"
"Enter the title!"
"Confirm Reset of Wizard?\nAll changes will be lost!"
"Confirm Reload of Wizard?"


"Title"
"Subtitle"
"Search field"
"Show subtitle in tile"
"Breadcrumb"
"Color"
"Embedded form"
"none"
"Image"
"Description"
"Icon"
"Choose"
"Remove"
"Shape"
"Square"
"Rectangle"
"Circle"
"Order"
"Visibility"
"WO assigned fields"
"Anchor name"
"Show exit tile"
"Selector sistem codes"
"Include"
"Exclude"


"Tile"
"New tile"

"Cancel"
"Save"

"Reload"
"Color Set"
"Test visibility"
"Delete wizard"

"Select color"
"Select icon"
*/



WizADM={ tl:56, mg:4, adm:1,
         fnt:"11px opensansR;",
         ja:null, jm:null, jp:null, js:null, jms:null, jico:null,
         SPA:"style='position:absolute;", 
         SEL:-1, aSel:[], SAVE:0,
         EDIT:-1, COPY:[], PoP:0, SHFT:0, CTRL:0,
         OM:"", OU:"", FD:[], FIND:0, Alist:[],
         SFD:{}, SFE:{},
         WO_pr:{}, WO_ow:{}, WO_field:{}, Anch:[],
         infs:{
          "boxsrc":"name of file shown in form when tile is selected\n?fullscreen shows form using max width",
          "fieldvalue":"example: ‘f_priority‘:‘2‘,‘f_description‘:‘id_path‘",
          "wareconnection":"link to ware:\nroot: links to top element.\nShow exit tile: show an ‘exit tile‘ from the second level onwards",
          "selectorlist":"list of system codes to include or exclude in ware connection"
         },

aTL:[
{ name:"File", Width:60, icon:"", fnc:"f_file()", alt:"File\nShortcut: (F)"},
{ name:"New", Width:60, icon:"", fnc:"f_newedit(0)", alt:"New tile\nShortcut: (N)"},
{ name:"Edit", Width:80, icon:"", fnc:"f_editmode()", alt:"Edit tile\nShortcut: (E)"},
{ name:"Select", Width:84, icon:"", fnc:"f_select(); ", alt:"Select box\nShortcut: (S)"},
{ name:"Copy", Width:64, icon:"", fnc:"f_copy()", alt:"Shortcut: (CTRL + C)" },

{ name:"Paste", Width:68, icon:"", fnc:"f_paste()", alt:"Shortcut: (CTRL + V)" },
{ name:"Move", Width:68, icon:"", fnc:"f_move()", alt:"Shortcut: (CTRL + X)" },

{ name:"Paste Tree", Width:130, icon:"", fnc:"f_pastetree()",  alt:"Shortcut: (CTRL + SHIFT + C)" },
{ name:"Paste Alias", Width:100, icon:"", fnc:"f_alias()",  alt:"Shortcut: (CTRL + W)" },

{ name:"Delete", Width:70, icon:"", fnc:"f_delete()", alt:"Shortcut: (CTRL + SHIFT + D)" }

],

clinput:"style='position:absolute;width:240px;padding:4px;font:13px opensansR;border:1px solid #4A6671;box-sizing:border-box; border-radius:2px;",
cltextarea:"style='position:absolute;width:486px;height:48px;border:1px solid #4A6671;box-sizing:border-box;font:13px opensansR;border-radius:2px;padding:3px;",
clbutton:"style='position:absolute;width:80px;bottom:0;border:0;font:14px opensansR;color:#FFF;border-radius:2px;padding:2px;cursor:pointer;background-color:#9FC24C;",


View:function(){
with(WizADM){

var str="",x=0,r;

  for(r=0;r<aTL.length;r++){   
    str+=drwBTN(x,r,aTL[r].Width);
    x+=aTL[r].Width+mg;
  }

if(!ja) ja=mf$("idAdm");
if(!jm) jm=mf$("idMaskAdm");
if(!jp) jp=mf$("idPopupAdm");
if(!js) js=mf$("idAdmSel");
if(!jms) jms=mf$("idMaskAdmSel");
if(!jico) jico=mf$("idIcoAdmSel");

ja.style.width=tl+"px";
ja.innerHTML=str;

// keyboard
document.onkeydown=WizADM.f_keydown; 
document.onkeyup=WizADM.f_keyup; 

 
adm=(Wizard.userLEV==-1)?1:0;              // if administrator 

}},




f_keydown:function(e){
with(WizADM){

  if(FIND) return true;

  if(!e) e=window.event;
  var k=(e.which)?e.which:e.keyCode;
  
  if(e.shiftKey) SHFT=1;
  if(e.ctrlKey)  CTRL=1;

  //mf$("debug").innerHTML=k+"<br>"+CTRL+"<br>"+SHFT
  
  if(!PoP){
    
    //file F
    if(k==70) f_file();

    // select mode S
    if(k==83) f_select();
    
    // edit mode E
    if(k==69) f_editmode();
    
    // new tile N
    if(k==78) f_newedit(0);  
    
    //copy C
    if(CTRL && k==67) f_copy();
    
    //move X
    if(CTRL && k==88) f_move();
    
    //alias W
    if(CTRL && k==87) f_alias();
    
    //paste V
    if(CTRL &&  k==86) { if(SHFT) f_pastetree(); else f_paste(); }
    
    //delete D
    if(CTRL && SHFT && k==68) f_delete(1);
    
    //Select All A
    if(CTRL && k==65) f_selectall();
    

  //esc
  if(k==27) {
    
    if(SEL>0 || EDIT>0) {     
        if(SEL>0) f_select();  // deactive selection mode
        if(EDIT>0) f_editmode();  // deactive edit mode
        Wizard.DrawTLs();
    }   
  }
  
  return false;
  } else {
  
  if(k==27) f_close_newedit();

  }
  
  
}},



f_keyup:function(e){
with(WizADM){

  if(!e) e=window.event;
  
  if(!e.shiftKey) SHFT=0;
  if(!e.ctrlKey)  CTRL=0;
}},



//----------------------------

f_file:function(){
with(WizADM){

Alist=[
{Txt:LANG("Reload"), Type:0, Icon:['FlaticonsStroke.58454'], Fnc:'WizADM.f_wizard_load()' }, 
{Txt:LANG("Save"), Type:0, Icon:['FlaticonsStroke.58722'], Fnc:'WizADM.f_wizard_save()' }, 
{Type:4 },
{Txt:LANG("Color Set"), Type:0, Fnc:'WizADM.f_colorset()' }
];

 
if(parent.moGlb.user.level==-1) {
  var r,admList=[
    {Txt:LANG("Test visibility"), Type:0, Fnc:'WizADM.f_testVisibility()' },
    {Type:4 },
    {Txt:LANG("Delete wizard"), Type:0, Fnc:'WizADM.ResetWizard()' },
    {Txt:LANG("Local DB"), Type:0, Fnc:'WizADM.PoP=1; Debug.Open();' },
    {Type:4 },
    {Txt:LANG("Export DB"), Type:0, Fnc:'WizADM.ExportDB();' },
    {Txt:LANG("Import DB"), Type:0, Fnc:'WizADM.clickImport();' }
    
  ];
  for(r=0;r<admList.length;r++) Alist.push(admList[r]);
}

moMenu.Start("WizADM.Alist",{l:16, t:44, w:60, h:0},2);
}},






f_colorset:function(){
with(WizADM){

  var str,w=560,h=280, col="";
  PoP=1;

  col=Wizard.Cols.join(", ");

  str="<div "+SPA+"left:50%;top:50%;width:"+w+"px;height:"+h+"px;margin-left:-"+(w/2)+"px;margin-top:-"+(h/2)+"px;"+
      "border-radius:2px;background-color:#FFF;border:1px solid #888;'>"+
      "<div "+SPA+"left:10px;top:10px;width:"+(w-20)+"px;height:"+(h-20)+"px;font:bold 14px opensansR;color:#000;'>"+LANG("Color Set:")+"</div>"+
      "<textarea id='idAdmTextareaColorSet' onkeyup='mf$(\"idAdmboxscolorset\").innerHTML=WizADM.f_viewColorSet()' "+
      SPA+"left:"+(w/3)+"px;top:40px;width:"+(w/3*2-10)+"px;height:"+(h-96)+"px;font:18px opensansR;border-radius:2px;border:1px solid #AAA;padding:4px;'>"+
      col+"</textarea>"+
      "<div id='idAdmboxscolorset' "+SPA+"left:10px;top:40px;width:"+(w/3-20)+"px;height:"+(h-90)+"px;overflow:auto;'></div>"+
      
      "<button "+clbutton+"left:10px;bottom:10px;' onclick='WizADM.f_close_colorset()'>"+LANG("Cancel")+"</button>"+
      "<button "+clbutton+"right:10px;bottom:10px;' onclick='WizADM.f_updateColorSet()'>"+LANG("Save")+"</button>"+

      "</div>";


  jp.innerHTML=str;
  
  mf$("idAdmboxscolorset").innerHTML=f_viewColorSet();
  
  jm.style.display="block";
  jp.style.display="block";
}},

f_close_colorset:function(){
with(WizADM){
  PoP=0;
  jp.style.display="none";
  jm.style.display="none";
}},



f_updateColorSet:function(){
with(WizADM){

  var lis=mf$("idAdmTextareaColorSet").value;
  lis=lis.replace(/ /g,""); 
 
  Wizard.Cols=lis.split(",");
  Dbl.SETVAR("wizard_color_set",JSON.stringify(Wizard.Cols));

  f_close_colorset();
  reStart();
}},


f_viewColorSet:function(){

  var r,lis,alis=[],s="";

  lis=mf$("idAdmTextareaColorSet").value;
  lis=lis.replace(/ /g,""); 

  alis=lis.split(",");
  
  for(r=0;r<alis.length;r++){
    s+="<div style='float:left;margin:6px;width:26px;height:28px;border:1px solid #AAA;background-color:"+alis[r]+";' title='"+alis[r]+"'></div> ";
  } 
  return s;  
},





f_testVisibility:function(){
with(WizADM){

  var v=prompt(LANG("Enter new user visibility to check:"),Wizard.userLEV);
  if(v==null || v=="" || v==0) return;
  if(!isNum(v)) { f_popAlert("Wrong value!"); return;  }
  Wizard.userLEV=parseInt(v);
  
  reStart();
}},


f_popAlert:function(msg){

  // desktop
  try { t=parent.moGlb.langTranslate(msg);  parent.moGlb.alert.show(t); return; } catch(e){}
  
  // mobile 
  try { t=parent.LANG(msg); parent.fAlert(t); return; } catch(e){}

  alert(msg);
},



reStart:function(){
with(Wizard){  
  f_resetVars();
  var ar=Dbl.query("SELECT t_wizard GET f_code WHERE f_parent_code='-0-'","Num");  
  PDR=ar[0][0];  
  pth=[PDR]; 
  Resize();       
}},




ResetWizard:function(){
  parent.moGlb.confirm.show(("<br>"+LANG("Confirm Reset of Wizard?<br>All changes will be lost!")), function(){ WizADM.actResetWizard(); }  );
},


actResetWizard:function(){
with(Wizard){ 

  Dbl.TRUNCATE("t_wizard");
  Dbl.TRUNCATE("t_wizard_temp");
  Dbl.query("INSERT "+table[0], [1, 1, '-0-', 'Wizard', '', '', '', -1, -1, '', '', 0, '', ''] );
 
 
  f_resetVars();
  PDR=1;  
  pth=[PDR]; 
  Paths=[];
  REStart=0;
 
  Resize();       
}},



f_wizard_load:function(){ 
  parent.moGlb.confirm.show(("<br>"+LANG("Confirm Reload of Wizard?")), function(){ WizADM.f_act_wizard_load(); }  );
},

  
f_act_wizard_load:function(){  
  Dbl.TRUNCATE("t_wizard"); 
  Wizard.Start();
},


f_wizard_save:function(){
with(WizADM){
  if(SAVE) return;
  var par={},a,rev,fid,scol;
  SAVE=1;
  a=Dbl.query("SELECT t_wizard GET f_id, f_order WHERE f_parent_code='-0-'");
  if(!a.length) { alert("Parent code not found!"); return; }
  rev=parseInt(a[0]["f_order"]);
  fid=a[0]["f_id"];
  Dbl.query("UPDATE t_wizard SET f_order="+(rev+1)+" WHERE f_id="+fid);
 
  a=Dbl.query("SELECT t_wizard * WHERE 1","NUM"); 
  par["data"]=a;
  scol=(JSON.parse(Dbl.GETVAR("wizard_color_set"))).join(",");    // string
  par["colors"]=scol;  

  Ajax.sendAjaxPost(Wizard.phpSave+Wizard.NS, "params="+JSON.stringify(par), "WizADM.fRetSave",1);    
}},


fRetSave:function(a){
with(WizADM){
  if(a!="ok") { alert("Save error:\n"+a); SAVE=0; return; }
  Wizard.DrawTLs();
  SAVE=0;
}},



//------------------------------

f_copy:function(){
with(WizADM){
  var r;
  COPY=[];
  for(r=0;r<aSel.length;r++) COPY.push(Wizard.TLxy[aSel[r]].f);
  f_deselall(); 
  Wizard.DrawTLs();
}},


f_move:function(){
with(WizADM){

if(!COPY.length) return;

  var r,fp,a,v,sf,p,mv=[];
  
    fp="-"+getPthParent()+"-";
    
   for(r=0;r<COPY.length;r++){   
    a=Dbl.query("SELECT t_wizard f_parent_code WHERE f_code="+COPY[r]); 
    sf=a[0].f_parent_code;
    p=sf.split("-");
    if(p.length==3) mv.push(COPY[r]);
    else {
        f_popAlert("Use Paste Alias and Delete to move alias tile");
        break;
      }
   } 
    
   // elements no alias  
   if(mv.length){ 
    Dbl.query("UPDATE t_wizard SET f_parent_code='"+fp+"' WHERE f_code IN ("+mv.join(",")+")");
   } 
    
  Wizard.DrawTLs(); 
}},


f_alias:function(){
with(WizADM){

if(!COPY.length) return;

  var r,fp,a,v,p;
      
      p=getPthParent()+"-";   // Wizard.PDR
      fp="-"+p;
      
      a=Dbl.query("SELECT t_wizard GET f_id,f_parent_code WHERE f_code IN ("+COPY.join(",")+")");
      for(r=0;r<a.length;r++){
        
        v=a[r].f_parent_code;
        if(v.indexOf(fp)==-1) v+=p; else continue;
        
        Dbl.query("UPDATE t_wizard SET f_parent_code='"+v+"' WHERE f_id="+a[r]["f_id"]);
    }
  Wizard.DrawTLs(); 
}},



f_paste:function(){
with(WizADM){

  if(!COPY.length) return;
  var r,fc,a,p;

      a=Dbl.query("SELECT t_wizard GET * WHERE f_code IN ("+COPY.join(",")+")");
      p=getPthParent();

      for(r=0;r<a.length;r++){
        a[r].f_code=f_getnewfcode();   // new f_code
        a[r].f_parent_code="-"+p+"-";
        Dbl.query("INSERT t_wizard",a[r]);
    }
  Wizard.DrawTLs(); 
}},



f_pastetree:function(){
with(WizADM){

  if(!COPY.length) return; 
  f_recurpaste(COPY,0);
   
  Wizard.DrawTLs();   
}},





f_recurpaste:function(ar,pp){
with(WizADM){

  var f,nf,a,ac,n,aa=[],p,ap,nap=[],v,CodR={};

  a=Dbl.query("SELECT t_wizard GET * WHERE f_code IN ("+ar.join(",")+")");
  
    // copy a
    for(r=0;r<a.length;r++){
    
        f=a[r].f_code;
        nf=f_getnewfcode();
  
        CodR[f]=nf;
        a[r].f_code=nf;             // new f_code
    
        if(!pp) { a[r].f_parent_code="-"+getPthParent()+"-";  // first time parent
        } else { 
          p=a[r].f_parent_code;
          ap=p.split("-");
          for(n=0;n<ap.length;n++){
            v=ap[n];
            if(!v) continue;
             if(typeof(pp[v])=="undefined") continue;
             nap.push(pp[v]);
          }
          if(!nap.length) continue;
          a[r].f_parent_code="-"+(nap.join("-"))+"-";
        }

        Dbl.query("INSERT t_wizard",a[r]);
     
      // find child  
      ac=Dbl.query("SELECT t_wizard GET f_code WHERE f_parent_code like '-"+f+"-'");  
 
      if(ac.length) { aa=[];
            for(n=0;n<ac.length;n++) aa.push(ac[n].f_code);
            f_recurpaste(aa,CodR);
      }        
    } 

}},

//---------------------------

f_delete:function(m){
with(WizADM){

  if(!aSel.length) return false;
  if(!m) {
    parent.moGlb.confirm.show(("<br>"+LANG("Delete tiles?")), function(){ window.WizADM.f_act_delete(); }  );
  } else f_act_delete();
  
  
}},  
  
f_act_delete:function(m){
with(WizADM){  
  
  var r,p,ad=[];  
  for(r=0;r<aSel.length;r++) ad.push(Wizard.TLxy[aSel[r]].f); 
  aSel=[];
  
  p=getPthParent(ad[0]);
  if(p<0) { alert("Parent error"); return; }
 
  f_deltree(ad,p);
  Wizard.DrawTLs();
}},

 

f_deltree:function(af,pr){   // pr = real father
with(WizADM){ 
  var r,f,sf,a,i,n,ps,ac=[],raf=[],p; 
  
  for(r=0;r<af.length;r++){
      a=Dbl.query("SELECT t_wizard f_parent_code WHERE f_code="+af[r]); 
      sf=a[0].f_parent_code;
      p=sf.split("-");
      if(p.length==3) raf.push(af[r]);
      else {
        p.pop();
        p.shift();
 
        i=p.indexOf(pr+"");
        p.splice(i,1);
        sf="-"+p.join("-")+"-";
        
        Dbl.query("UPDATE t_wizard SET f_parent_code='"+sf+"' WHERE f_code="+af[r]);
      }
  }
 
  if(raf.length){
    sf=raf.join(","); 
    Dbl.query("DELETE t_wizard WHERE f_code in ("+sf+")");
   
        for(r=0;r<raf.length;r++){
          f="-"+raf[r]+"-";
          a=Dbl.query("SELECT t_wizard f_id,f_code,f_parent_code WHERE f_parent_code like '"+f+"'");  
       
          for(n=0;n<a.length;n++){
            i=a[n]["f_id"];
            ps=a[n]["f_parent_code"];  
            
            if(ps==f) { ac.push(a[n]["f_code"]); continue; } 
            
            ps=ps.replace(new RegExp(f,'g'),"-"); 
            Dbl.query("UPDATE t_wizard SET f_parent_code='"+ps+"' WHERE f_id="+i);
          }
          if(ac.length) f_deltree(ac,raf[r]);
        }           
  }
  
}},


getPthParent:function(f){
with(WizADM){ 

  var r,n,pt,ps=moGlb.cloneArray(Wizard.pth);
  n=ps.length-1; if(!n) return ps[0];


  for(r=n;r>=0;r--){
    pt=ps[r]+"";
    if(pt.substring(0,2)=="w_") continue;
    return ps[r]; 
  }

  return -1;
}},


//---------------------------

f_editmode:function(e){
with(WizADM){

  EDIT*=-1;
  var d="none", zi="10";

if(EDIT>0){

  if(SEL>0) f_select();  // deactive selection mode

  d="block";
  zi="100";
  
  if(!isTouch) { 
      jico.src="img/mouse_edit.png";    
      moEvtMng.addEvent(document,"mousemove",WizADM.f_sele_ico); 
  }
}else{
 
  if(!isTouch) moEvtMng.subEvent(document,"mousemove",WizADM.f_sele_ico);
}

  //ja.style.zIndex=zi;
  jico.style.display=d;
}},

 

//---------------------------------------------------
// select

f_select:function(e){
with(WizADM){
  var r,l,d="none";
  
  SEL*=-1;

  if(SEL>0){ d="block"; 
  
    if(EDIT>0) f_editmode();  // deactive edit mode
    ja.style.zIndex="100";
    
    // events
      r=0,l=["touchstart","touchmove","touchend","mousedown"]; 
      for(;r<l.length;r++) {  eval("jms.on"+l[r]+"=function(e){ WizADM.EvtSel(e); return false; }"); }
 
             if(!isTouch) { moEvtMng.addEvent(jms,"mousemove",WizADM.f_sele_ico);
              jico.src="img/mouse_selection.png";
              
              jms.onmouseover=function(){ WHEELOBJ="Wizard";  }
             }
                     
  } else {
  
    ja.style.zIndex="10";
    
           if(!isTouch) moEvtMng.subEvent(jms,"mousemove",WizADM.f_sele_ico);
           js.style.display=d;
           f_deselall();      
  }

  jms.style.display=d;
  jico.style.display=d;
}},





EvtSel:function(e){
with(WizADM){   e=e||window.event;
 
  var ev=e.type, r,x,y,t,nx,ny,ex,ey, nt=1,p,lx,ty;
  if(isTouch) var tch=e.changedTouches, nt=tch.length;

  if(ev=="touchstart" || ev=="mousedown"){          
          if(isTouch) t=tch[0], ex=t.pageX, ey=t.pageY, i=t.identifier;
          else { P=Wizard.jsCoords(e), ex=P.x, ey=P.y, i=0;
                 OM=document.onmousemove; OU=document.mouseup;  
                 document.onmousemove=function(e){WizADM.EvtSel(e)}
                 document.onmouseup=function(e){WizADM.EvtSel(e)}     }
        
          
          js.style.left=ex+"px";
          js.style.top=ey+"px";
          js.style.width="2px"; 
          js.style.height="2px"; 
          js.style.display="block";
          
          FD.push({ x:ex, y:ey, ix:ex, iy:ey, f:i, mm:0 });          
  }
  
  if(ev=="touchmove" || ev=="mousemove"){
       if(FD.length) {
          for(r=0;r<nt;r++) {  
            i=0; if(isTouch) t=tch[r], i=t.identifier;   
            if(i!=FD[0].f) continue;
              if(isTouch) ex=t.pageX, ey=t.pageY; else P=Wizard.jsCoords(e), ex=P.x, ey=P.y;
              
      with(FD[0]){ mm++; 
          lx=ix, ty=iy, w=ex-ix, h=ey-iy;
          if(w<0) lx=ex, w*=-1;
          if(h<0) ty=ey, h*=-1;
          
          js.style.left=lx+"px";
          js.style.top=ty+"px";
          js.style.width=w+"px"; 
          js.style.height=h+"px";
          
          x=ex; y=ey;
      }    
      break;
  }}}

  if(ev=="touchend" || ev=="mouseup"){
    js.style.display="none"; 
    if(FD.length) {
            f_clickSel();  
            FD=[]; }                
    if(!isTouch) { if(!OM) OM=""; if(!OU) OU="";  document.onmousemove=OM; document.onmouseup=OU;  }                 
  }
  
moEvtMng.cancelEvent(e);
}},






f_clickSel:function(){
with(WizADM){

    var r,p, tx1,tx2,ty1,ty2, sx1,sx2,sy1,sy2,
      ns=0, sf, n=Wizard.TLxy.length, p=moGlb.getPosition(Wizard.jwb),
      dt=p.t+Wizard.tl, dx=p.l, dy=dt+Wizard.sct; 
 
   sx1=parseInt(js.style.left), sy1=parseInt(js.style.top), 
   sx2=sx1+parseInt(js.style.width), sy2=sy1+parseInt(js.style.height); 

  if(sy1<dt) { if(sy2>dt) sy1=dt; else n=0; } 
  

  for(r=0;r<n;r++) with(Wizard.TLxy[r]){                    
    tx1=x+dx, ty1=y+dy, tx2=tx1+wtl, ty2=ty1+Wizard.tl;   // TLxy:  x,y,wtl   i=index in aTile(children)   f=f_code
 
    if( sx2<tx1 || sx1>tx2  || sy2<ty1 || sy1>ty2 ) continue;   
    ns++;
    sf=f+"";
    if(sf.substring(0,2)=="w_") continue;
    f_AddSubSel(r);
  }
  
  if(!ns) f_deselall();    // deselect all 
}},



f_AddSubSel:function(r){
with(WizADM){ 

  var jtl,fc,o,io;
  
  fc=Wizard.TLxy[r].f;
  jtl=mf$("idTL"+fc);
  io=aSel.indexOf(r);

  if(io==-1){ aSel.push(r); o=30;
  } else { o=100; aSel.splice(io,1);  }

  moGlb.setOpacity(jtl,o);
}},



f_deselall:function(r){
with(WizADM){ 
  var r,fc,jtl;
  for(r=0;r<aSel.length;r++){   
    fc=Wizard.TLxy[aSel[r]].f;
    jtl=mf$("idTL"+fc);
    moGlb.setOpacity(jtl,100);
  }
  aSel=[];
}},



f_selectall:function(r){
with(WizADM){ 
   
  if(SEL<0) return;
   
  var r,fc,jtl,V=Wizard.TLxy; 
  aSel=[];
  
  for(r=0;r<V.length;r++){   
    aSel.push(r);
    fc=V[r].f;
    jtl=mf$("idTL"+fc);
    moGlb.setOpacity(jtl,30);
  }  

}},



f_sele_ico:function(e){
with(WizADM){
  var P=jsCoords(e), x=P.x, y=P.y; 
  jico.style.left=(x-20)+"px"; 
  jico.style.top=(y-15)+"px";
}},

//-----------------------------------------------------------------

drwBTN:function(x,r,ww){
with(WizADM){

var s,c0,c,inkcol,f="",onm;

  if(aTL[r].fnc) f="with(WizADM) {"+aTL[r].fnc+"}";


  if(typeof(aTL[r].bkcol)=="undefined") aTL[r].bkcol="#A1C24D";
  c=aTL[r].bkcol;
  inkcol="#FFF";   // Wizard.f_ink(c);

  if(typeof(aTL[r].alt)=="undefined") aTL[r].alt="";

  onm="onmouseover='this.style.backgroundColor=\"#D0D0D0\"; this.style.color=\"#808080\"; ' "+
      "onmouseout='this.style.backgroundColor=\""+c+"\"; this.style.color=\""+inkcol+"\"; ' ";

  s="<div id='idbtnwizadm"+r+"' "+SPA+"left:"+x+"px;top:0px;width:"+ww+"px;height:28px;border-radius:2px;background-color:"+c+";cursor:pointer;color:"+inkcol+";'"+
    " onclick='WizADM.btnTM("+r+",0); "+f+"' title='"+LANG(aTL[r].alt)+"' "+onm+">"+
    "<div style='margin:5px 8px 0 8px; font:14px opensansR;text-align:center;color:inherit;'>"+LANG(aTL[r].name)+"</div>"+  
    "</div>"+
    "<div id='idbtnwizadmopac"+r+"' "+SPA+"left:"+x+"px;top:0px;width:"+ww+"px;height:28px;border-radius:2px;background-color:#000;opacity:0.3;display:none;'></div>";

return s;

}},


btnTM:function(r,t){
with(WizADM){

mf$("idbtnwizadmopac"+r).style.display=(t)?"none":"block";

if(t) return;
t++;

setTimeout(function(){ WizADM.btnTM(r,t); },300);
}},


//-------------------------------------------------------------------------

f_newedit:function(fcode,ii){
with(WizADM){

PoP=1;

if(SEL>0) f_select();  // deactive selection mode
if(!fcode && EDIT>0) f_editmode();


var str,rr, w=520, h=630-(adm?0:80), y=34, defval,opt,P,sf,cc,ci,cf,opte,opta;

P=getPthParent();    //Wizard.PDR;

if(fcode) { 
  if(ii>-1) defval=Wizard.aTile[ii];
  else { defval=Wizard.aPdr; P=defval.f_parent_code; }
}

if(!fcode){
defval={
  f_code:fcode,
  f_parent_code:P,
  f_title:LANG("New tile"),
  f_subtitle:"",
  f_linktitle:"",
  f_color:"",
  f_visibility:-1,
  f_srcbox:"",
  f_imgtile:"",
  f_tilewidth:1,
  f_order:0,
  f_field:"",
  f_ware_start:""
};
}

tw=parseInt(defval.f_tilewidth);

if(tw==1) opt="<option value=1 selected>"+LANG("Square")+"</option><option value=2>"+LANG("Rectangle")+"</option><option value=5>"+LANG("Circle")+"</option>";
if(tw==2) opt="<option value=1>"+LANG("Square")+"</option><option value=2 selected>"+LANG("Rectangle")+"</option><option value=5>"+LANG("Circle")+"</option>";
if(tw==5) opt="<option value=1>"+LANG("Square")+"</option><option value=2>"+LANG("Rectangle")+"</option><option value=5 selected>"+LANG("Circle")+"</option>";
 

  str="<div "+SPA+"left:50%;top:0;width:"+w+"px;height:100%;margin-left:-"+(w/2)+"px;'>"+
      "<div "+SPA+"left:0;top:50%;width:"+(w-2)+"px;height:"+h+"px;margin-top:-"+(h/2)+"px;border-radius:2px;background-color:#FFF;'>"+
      
    "<div "+SPA+"left:8;top:8px;right:8px;height:28px;;background-color:"+Wizard.defCols[0]+";border-radius:2px;'>"+  
      "<div "+SPA+"left:8px;top:5px;font:bold 13px opensansR;color:#FFF;'>"+(fcode?LANG("Edit"):LANG("New") )+" "+LANG("Tile")+"</div>"+
    "</div>"+  
    
    "<div "+SPA+"right:16px;top:11px;width:20px;height:16px;font:16px FlaticonsStroke;color:#FFF;cursor:pointer;z-index:10;' onclick='WizADM.f_close_newedit();'></div>"+
      
      "<div "+SPA+"left:16px;top:16px;width:"+(w-32)+"px;height:"+(h-32)+"px;font:13px opensansR;color:#000;'>"+
 
      "<input id='idf_parent_code' type='hidden' value='"+P+"' />"+
      "<input id='idf_code' type='hidden' value='"+defval.f_code+"' />"+

  
      // Title
      "<span class='clabel' "+SPA+"left:0;top:"+y+"px;'>"+LANG("Title")+"</span>"+
      "<input id='idf_title' type='text' "+clinput+"top:"+(y+18)+"px;background-color:#A9BCC2;' value='"+defval.f_title+"' />";
      
      
  // +subtitle+
  SFD=f_subtitle_xtra(defval.f_subtitle);   //  SFD { .search    .subtitle    . tshow }
   
      
      
  // search field
  cc=(SFD.search)?58826:58783;
  str+="<div id='id_sfd0' "+SPA+"left:268px;top:"+(y+22)+"px;font:16px FlaticonsStroke;line-height:16px;color:"+Wizard.defCols[0]+";cursor:pointer;' onclick='WizADM.f_SFD(0);'>"+String.fromCharCode(cc)+"</div>"+
       "<div class='clabel' "+SPA+"left:290px;top:"+(y+22)+"px;'>"+LANG("Search field")+"</div>"; 
      
      
  y+=64;   
 
  // subtitle
  str+="<span class='clabel' "+SPA+"left:0;top:"+y+"px;'>"+LANG("Subtitle")+"</span>"+                                                        
       "<input id='idf_subtitle' type='text' "+clinput+"top:"+(y+18)+"px;' value='"+SFD.subtitle+"' />";
      
  // Show subtitle in tile
  cc=(SFD.tshow)?58826:58783; 
  str+="<div id='id_sfd1' "+SPA+"left:268px;top:"+(y+22)+"px;font:16px FlaticonsStroke;line-height:16px;color:"+Wizard.defCols[0]+";cursor:pointer;' onclick='WizADM.f_SFD(1);'>"+String.fromCharCode(cc)+"</div>"+
       "<div class='clabel' "+SPA+"left:290px;top:"+(y+22)+"px;'>"+LANG("Show subtitle in tile")+"</div>";    
                 
  y+=64;  
    
  // Breadcrumb
  str+="<span class='clabel' "+SPA+"left:0;top:"+y+"px;'>"+LANG("Breadcrumb")+"</span>"+
      "<input id='idf_linktitle' type='text' "+clinput+"top:"+(y+18)+"px;' value='"+defval.f_linktitle+"' />";
      
  // color
  str+="<span class='clabel' "+SPA+"left:268px;top:"+y+"px;'>"+LANG("Color")+"</span>"+
      "<div id='idf_color_cel' "+SPA+"left:458px;top:"+(y+18)+"px;width:28px;height:28px;border-radius:2px;background-color:"+(defval.f_color?defval.f_color:"#FFF")+
      ";box-sizing:border-box;border:1px solid "+Wizard.defCols[0]+";cursor:pointer;' onclick='ColorSHW.Show()'></div>"+
        "<input id='idf_color' type='text' "+clinput+"left:268px;top:"+(y+18)+"px;width:184px;' value='"+defval.f_color+"' onchange='WizADM.f_update_color(event)' />";    
      
            
  y+=64;   

  // Embedded form
  str+="<span class='clabel' "+SPA+"left:0;top:"+(y-4)+"px;'>"+f_inf(infs["boxsrc"])+LANG("Embedded form")+"</span>";
  
  if(adm){  
    str+="<input id='idf_srcbox' type='text' "+clinput+"top:"+(y+18)+"px;' value='"+defval.f_srcbox+"' />";
  }else{
  
    var fsel, efo={"none":"none","Image":"external_image.html","Description":"external_description.html"};
    opte="";
    for(rr in efo){
      fsel=(defval.f_srcbox==efo[rr])?"selected":""; 
      opte+="<option value='"+efo[rr]+"' "+fsel+">"+LANG(rr)+"</option>";
    }
       
    str+="<select id='idf_srcbox' "+clinput+"top:"+(y+18)+"px;width:248px;padding:0;height:28px;'>"+opte+"</select>";
  }
  
  
  // Icon
  cc=defval.f_imgtile?String.fromCharCode(defval.f_imgtile):"";
  str+="<span class='clabel' "+SPA+"left:268px;top:"+y+"px;'>"+LANG("Icon")+"</span>"+              // (58344-58843)
       "<input id='idf_imgtile' type='hidden' "+clinput+"left:268px;top:"+(y+18)+"px;width:190px;' value='"+defval.f_imgtile+"' />"+
       "<div id='idf_imgtile_icon' "+SPA+"left:422px;top:"+(y+12)+"px;height:32px;cursor:pointer;font:32px FlaticonsStroke;color:"+
       Wizard.defCols[0]+";line-height:32px;' onclick='FlaticonSHW.Show()'>"+cc+"</div>"+  
       "<div "+SPA+"left:268px;top:"+(y+28)+"px; cursor:pointer;text-decoration:underline;color:"+Wizard.defCols[0]+"' onclick='FlaticonSHW.Show()'>"+LANG("Choose")+"</div>"+
       "<div "+SPA+"left:342px;top:"+(y+28)+"px; cursor:pointer;text-decoration:underline;color:"+Wizard.defCols[0]+"' onclick='FlaticonSHW.Remove()'>"+LANG("Remove")+"</div>";
       
  y+=64;
  
  // shape
  str+="<span class='clabel' "+SPA+"left:0;top:"+y+"px;'>"+LANG("Shape")+"</span>"+
      "<select id='idf_tilewidth' "+clinput+"top:"+(y+18)+"px;width:248px;padding:0;height:28px;'>"+opt+"</select>";
  
    
  // order
  str+="<span class='clabel' "+SPA+"left:268px;top:"+y+"px;'>"+LANG("Order")+"</span>"+
       "<input id='idf_order' type='text' "+clinput+"left:268px;top:"+(y+18)+"px;width:"+(adm?100:220)+"px;' value='"+defval.f_order+"' />";
  
  
  // visibility
  if(adm) str+="<span class='clabel' "+SPA+"left:390px;top:"+y+"px;'>"+LANG("Visibility")+"</span>"; 
  str+="<input id='idf_visibility' type='"+(adm?"text":"hidden")+"' "+clinput+"left:390px;top:"+(y+18)+"px;width:96px;' value='"+defval.f_visibility+"' />";
      

  y+=64;  
  
  
  if(adm){
  
  // WO assigned fields
  str+="<span class='clabel' "+SPA+"left:0;top:"+(y-4)+"px;'>"+f_inf(infs["fieldvalue"])+LANG("WO assigned fields")+"</span>"+
       "<textarea id='idf_field' "+cltextarea+"top:"+(y+18)+"px;' >"+defval.f_field+"</textarea>";
      
      
  }else{
 
 
// WO_pr={"0":"none", "1":"Normal", "2":"Urgent", "3":"Immediate"  };
// WO_ow={"0":"none", "2807":"Tony Greco", "1":"mainsim administrator", "2":"Config" };
  
  
   var aff={};
   try{ if(defval.f_field) eval("aff={"+defval.f_field+"}"); } catch(e){ parent.moGlb.alert.show("<br>"+LANG("WO field sintax error!")); }
   
   WO_field={ "f_priority":0, "fc_owner_name":0 };
   for(rr in aff){
      if(rr=="f_priority" || rr=="fc_owner_name") WO_field[rr]=aff[rr];
   }
  
   // WO Priority
   opta="";
    for(rr in WO_pr){
      fsel=(WO_field.f_priority==rr)?"selected":""; 
      opta+="<option value='"+rr+"' "+fsel+">"+LANG(WO_pr[rr])+"</option>";
    }
       
    str+="<span class='clabel' "+SPA+"left:0;top:"+y+"px;'>"+LANG("WO Priority")+"</span>"+
         "<select id='idf_priority' "+clinput+"top:"+(y+18)+"px;width:248px;padding:0;height:28px;' onchange=''>"+opta+"</select>";
 
   // WO Owner
    opta="";
    for(rr in WO_ow){
      fsel=(WO_field.fc_owner_name==rr)?"selected":""; 
      opta+="<option value='"+rr+"' "+fsel+">"+LANG(WO_ow[rr])+"</option>";
    }
       
    str+="<span class='clabel' "+SPA+"left:268px;top:"+y+"px;'>"+LANG("WO Owner")+"</span>"+
         "<select id='idfc_owner_name' "+clinput+"left:268px;top:"+(y+18)+"px;width:220px;padding:0;height:28px;' onchange=''>"+opta+"</select>";
         
         
    str+="<textarea id='idf_field' style='display:none;'>"+defval.f_field+"</textarea>";     
  }    
   
      
  y+=84-(adm?0:20);    
      
  // Anchor name 
  SFE=f_anchor_xtra(defval.f_ware_start);   //  SFE { .exit .first  .anchor  .include  .selector  }   
 
  // alert(JSON.stringify(SFE) ) 
     
  opta="";
  for(rr=0;rr<Anch.length;rr++){
      fsel=(SFE.anchor==Anch[rr])?"selected":""; 
      opta+="<option value='"+Anch[rr]+"' "+fsel+">"+Anch[rr]+"</option>";
    }   
     
  str+="<span class='clabel' "+SPA+"left:0;top:"+(y-4)+"px;'>"+f_inf(infs["wareconnection"])+LANG("Anchor name")+"</span>"+
       "<select id='id_sfe_anchor' "+clinput+"top:"+(y+18)+"px;width:248px;padding:0;height:28px;'>"+opta+"</select>";
  
  
   //   "<input id='idf_ware_start' type='text' "+clinput+"top:"+(y+18)+"px;' value='"+defval.f_ware_start+"' />";
      
      
   
      
  // Show exit tile
  cc=(SFE.exit)?58826:58783;
  cf=(SFE.first)?58826:58783;
  
  str+="<div id='id_sfe0' "+SPA+"left:268px;top:"+(y+22)+"px;font:16px FlaticonsStroke;line-height:16px;color:"+Wizard.defCols[0]+
       ";cursor:pointer;' onclick='WizADM.f_SFE(0);'>"+String.fromCharCode(cc)+"</div>"+
      
       "<div class='clabel' "+SPA+"left:290px;top:"+(y+22)+"px;'>"+LANG("Show exit tile")+"</div>"+
       
       "<div id='id_sfe4' "+SPA+"left:440px;top:"+(y+22)+"px;font:16px FlaticonsStroke;line-height:16px;color:"+Wizard.defCols[0]+
       ";cursor:pointer;' onclick='WizADM.f_SFE(4);'>"+String.fromCharCode(cf)+"</div>"+
       
       "<div class='clabel' "+SPA+"left:460px;top:"+(y+22)+"px;'>"+LANG("1°")+"</div>"
       
           
      
  y+=64;
  
  // Selector    
  var dpn=+adm?"":"display:none;";
  str+="<span class='clabel' "+SPA+"left:0;top:"+(y-4)+"px;"+dpn+"'>"+f_inf(infs["selectorlist"])+LANG("Selector sistem codes")+"</span>"+
      "<input id='id_sfe_selector' type='text' "+clinput+"top:"+(y+18)+"px;"+dpn+"' value='"+SFE.selector+"' />";
  
  
    // ware include
  cf=58782, ci=58782, cn=58782;
  
  if(SFE.include<2) ci=58806;
  if(SFE.include==2) cf=58806;
  if(SFE.include==3) cn=58806;
 
  
  
  
  // include
  str+="<div id='id_sfe1' "+SPA+"left:268px;top:"+(y+22)+"px;font:16px FlaticonsStroke;line-height:16px;color:"+Wizard.defCols[0]+";cursor:pointer;"+dpn+"' onclick='WizADM.f_SFE(1)'>"+
        String.fromCharCode(ci)+"</div><div class='clabel' "+SPA+"left:288px;top:"+(y+22)+"px;"+dpn+"' title='"+LANG("Include")+"'>"+LANG("Incl.")+"</div>";

  // exclude
  str+="<div id='id_sfe2' "+SPA+"left:332px;top:"+(y+22)+"px;font:16px FlaticonsStroke;line-height:16px;color:"+Wizard.defCols[0]+";cursor:pointer;"+dpn+"' onclick='WizADM.f_SFE(2)'>"+
       String.fromCharCode(cf)+"</div><div class='clabel' "+SPA+"left:354px;top:"+(y+22)+"px;"+dpn+"' title='"+LANG("Exclude")+"'>"+LANG("Excl.")+"</div>";       
        
  // not hierarchy
  str+="<div id='id_sfe3' "+SPA+"left:400px;top:"+(y+22)+"px;font:16px FlaticonsStroke;line-height:16px;color:"+Wizard.defCols[0]+";cursor:pointer;"+dpn+"' onclick='WizADM.f_SFE(3)'>"+
  String.fromCharCode(cn)+"</div><div class='clabel' "+SPA+"left:420px;top:"+(y+22)+"px;"+dpn+"' title='"+LANG("No Hierarchy")+"'>"+LANG("No Hier.")+"</div>";       
       
      
      
  // buttons    
  str+="<button "+clbutton+"left:0;' onclick='WizADM.f_close_newedit()'>"+LANG("Cancel")+"</button>";
  sf=fcode+"";
  if(sf.substring(0,2)!="w_") str+="<button "+clbutton+"right:0;' onclick='WizADM.f_addeditTile()'>"+LANG("Save")+"</button>";     
      
  
  str+="</div></div></div>";



jp.innerHTML=str;
jm.style.display="block";
jp.style.display="block";

}},



f_subtitle_xtra:function(b){  //  SFD { .search    .subtitle    . tshow }   
  if(!b) b="";
  var n=b.length, rr={ search:0, subtitle:"", tshow:0 };
  if(!n) return rr;
  
  if(b.substring(0,1)=="+") { 
    rr.search=1; 
    if(n==1) return rr;
    b=b.substring(1); 
    n--;
  }
  if(b.charAt(n-1)=="+"){
    rr.tshow=1;
    if(n==1) return rr;   
    n--;
  } 
  rr.subtitle=b.substring(0,n);
  return rr;
},



// +root-1534,5325,7895,...  =>  +root@_ADD_1534,5325,...   _SUB_  _NHY_
f_anchor_xtra:function(b){  //  SFE { .exit .first  .anchor  .include  .selector  } 
  if(!b) b="";
  b=b.replace(new RegExp(" ",'g'),"");  // no space
  
  var i=0,c,n=b.length,bb, ab, 
      rr={ exit:1, first:0, anchor:"none", include:0, selector:"" };
  
  if(!n) return rr;
  
  rr.exit=0;
  if(b.charAt(0)=="+") {
    rr.exit=1;
    b=b.substring(1);
  }
  
  if(b.indexOf("@")>-1) { rr.first=1; ab=b.split("@"); b=ab.join("");  }
  
  c=""; i=0;
  if(b.indexOf("_ADD_")>-1) { c="_ADD_"; i=1; }  
  if(b.indexOf("_SUB_")>-1) { c="_SUB_"; i=2; } 
  if(b.indexOf("_NHY_")>-1) { c="_NHY_"; i=3; }    
 
  rr.include=i;
 
  if(c){  
    bb=b.split(c);
    rr.anchor=bb[0];
    rr.selector=bb[1];  
  } else rr.anchor=b;
  
  return rr;
},


f_SFD:function(m){
with(WizADM){ 
  
  var cc,bj=mf$("id_sfd"+m);
  
  // .search
  if(!m){
    SFD.search=(SFD.search)?0:1;
    cc=(SFD.search)?58826:58783;
    bj.innerHTML=String.fromCharCode(cc);
    
  }else{  // tshow
    SFD.tshow=(SFD.tshow)?0:1;
    cc=(SFD.tshow)?58826:58783;
    bj.innerHTML=String.fromCharCode(cc); 
  }
  
  
}},  
  
  
  
f_SFE:function(m){  // .exit    .include    .first
with(WizADM){ 
  
  
  var cc,ce,cn,cf;
  
  // .exit wares  | 4=first exit ware
  if(!m || m==4){
    
    if(!m){
      SFE.exit=(SFE.exit)?0:1;
    } else {
      SFE.first=(SFE.first)?0:1;    
    }
    
    cc=(SFE.exit)?58826:58783;   
    mf$("id_sfe0").innerHTML=String.fromCharCode(cc);
    
    cf=(SFE.first)?58826:58783;   
    mf$("id_sfe4").innerHTML=String.fromCharCode(cf);
    
    
  }else{  //  m=1|2|3    1=include | 2=exclude  | 3=no Hierarchy  
    
    SFE.include=m; 
 
    cc=(m==1)?58806:58782;
    ce=(m==2)?58806:58782;
    cn=(m==3)?58806:58782;  
    
    mf$("id_sfe1").innerHTML=String.fromCharCode(cc);
    mf$("id_sfe2").innerHTML=String.fromCharCode(ce); 
    mf$("id_sfe3").innerHTML=String.fromCharCode(cn);   
  }
 
}},   
  
  

f_update_color:function(e,b){
with(WizADM){ 
  var bj=mf$("idf_color");
  if(b) bj.value=b;
  var c=bj.value, isColor  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(c); 
  if(!isColor) bj.value="", c="#FFF"; 
  mf$("idf_color_cel").style.backgroundColor=c;
}},




f_inf:function(tx){
with(WizADM){
  var s="<span style='font:16px FlaticonsStroke;color:#AAA;cursor:default;' title='"+LANG(tx)+"'></span>&nbsp;";
  return s;
}},


f_addeditTile:function(){
with(WizADM){


var V,fc,a,v,P,r,ae,b,g;

P=mf$("idf_parent_code").value;
fc=parseInt(mf$("idf_code").value);


if(!fc) fc=f_getnewfcode();


V={
  f_code:fc,
  f_parent_code:"-"+P+"-",
  f_title:"New tile",
  f_subtitle:"",
  f_linktitle:"",
  f_color:"",
  f_visibility:-1,
  f_srcbox:"",
  f_imgtile:"",
  f_tilewidth:1,
  f_order:0,
  f_field:"",
  f_ware_start:""
}

 
for(k in V){
  if(k=="f_code" || k=="f_parent_code") continue;
 
  if(k=="f_ware_start"){
    SFE.anchor=mf$("id_sfe_anchor").value;
    SFE.selector=mf$("id_sfe_selector").value;
    v="";
    if(SFE.anchor!="none"){
    
      v=(SFE.exit?"+":"") + SFE.anchor + ((SFE.first && SFE.exit)?"@":"");
      b=SFE.selector;

      
      if(b){     
        // check SFE.selector
        b=b.replace(new RegExp(" ",'g'),"");   
        ae=b.split(",");
        for(r=0;r<ae.length;r++) if( !isNum(ae[r]) ) { f_popAlert("<br>"+LANG("Error in Selector sistem codes!")); return; }
      
        var ash="_ADD_";
        if(SFE.include==2) ash="_SUB_";
        if(SFE.include==3) ash="_NHY_";
 
        v+=ash+b;
   
      }
      
      // alert(v)
 
    }

  }else{
  
    v=mf$("id"+k).value;
    
    if(k=="f_title" && !v) { f_popAlert("Enter the title!"); return false; }
    if(k=="f_visibility") { if(!WizADM.isNum(v)) v=-1; }
    if(k=="f_srcbox") { if(v=="none") v=""; }
    if(k=="f_order") { if(!WizADM.isNum(v)) v=0; }
    if(k=="f_subtitle") {  // SFD { .search    .subtitle    . tshow }   
      if(SFD.search) v="+"+v;
      if(SFD.tshow)  v=v+"+";
    }
    
    if(k=="f_field") {  
      if(adm){
        // check sintax
        if(v){
          try{ eval("var aff={"+v+"}"); }catch(e){ parent.moGlb.alert.show("<br>"+LANG("WO field sintax error!\n"+v)); return false; }
        }
      }else{    
         // get select value of priority & owner    &&   image & description of srcbox
   
         WO_field={};      
         g=parseInt(mf$("idf_priority").value);   
         if(g) WO_field["f_priority"]=g;     
         g=parseInt(mf$("idfc_owner_name").value);
         if(g) WO_field["fc_owner_name"]=g;      
         g=mf$("idf_srcbox").value;
     
         if(g!="none") {    // image | description        "Image":"external_image.html","Description":"external_description.html"      
            if(g=="external_image.html"){      
                WO_field["picture_1"]="id_picture";         
            }
            if(g=="external_description.html"){     
                WO_field["f_description"]="id_descr";    
            } 
         } 
         // passed value
         var gg,sep="", aff={};
         if(v){
            try{ eval("aff={"+v+"}"); }catch(e){ parent.moGlb.alert.show("<br>"+LANG("WO field sintax error!"+v )); return false; }
          }     
          
         if(typeof(aff["f_description"])!="undefined") delete(aff["f_description"]); 
         if(typeof(aff["picture_1"])!="undefined") delete(aff["picture_1"]);   
           
            
        // merge in aff  
        for(g in WO_field) aff[g]=WO_field[g];
        // create string
        v="";
        for(g in aff) { gg=aff[g]; if(typeof(gg)!="number") gg="'"+gg+"'"; v+=sep+"'"+g+"':"+gg; sep=","; }        
      }
    }
  }
  
  if(k=="f_title" || k=="f_subtitle") v=v.multiReplace( {'"': '”',  "'": '’' } );
  
  V[k]=v;
}

if(EDIT>0){
  delete(V.f_parent_code);
  Dbl.query("UPDATE t_wizard WHERE f_code='"+fc+"'",V);  
  
} else {
  Dbl.query("INSERT t_wizard",V);
}

f_close_newedit();
Wizard.DrawTLs();
}},



f_close_newedit:function(){
with(WizADM){
  jm.style.display="none";
  jp.style.display="none";
  jp.innerHTML="";
  PoP=0;
}},


f_getnewfcode:function(){
  var a=Dbl.query("SELECT t_wizard f_code WHERE 1 ORDER BY f_code DESC limit 0,1");
  return parseInt(a[0].f_code)+1;
},



isNum:function(num) {
   return (!isNaN(num) && !isNaN(parseFloat(num))); 
},


//-----------------------------------

ExportDB:function(){
  var d=JSON.stringify(Dbl.db),
      myBlob=new Blob([d],{type: "application/octet-stream"}),
      dreader=new FileReader();
     
      dreader.onload = function(e) {
          var URL=e.target.result, dj=mf$("lnkDownload");
          dj.href=URL;
          dj.click();
      };     
  dreader.readAsDataURL(myBlob);
},


clickImport:function(){ 
  mf$("lnkUpload").click();
},


ImportDB:function(){
  var ofl, dj=mf$("lnkUpload"), 
      ureader=new FileReader();
 
  if(dj.files.length!=1) return;
  ofl=dj.files[0];  
 
  ureader.onload=function(e){
    WizADM.Sts=e.target.result;         
    parent.moGlb.confirm.show(("<br>"+LANG("Replace Wizard?")), function(){ WizADM.f_replaceWiz(); }  ); 
  }
 
 ureader.readAsText(ofl);
},


f_replaceWiz:function(){
    Dbl.db=JSON.parse(WizADM.Sts);
    Wizard.freset();  
}

  

} //  end WizADM






//----------------------------------------------------------------------------------------------- 
// show flaticons
FlaticonSHW={ SPA:"style='position:absolute;",

LIST:{'grid-1':'e3e8','grid-2':'e3e9','grid-3':'e3ea','window-1':'e3eb','collection-1':'e3ec','switch-window-1':'e3ed','add-window-1':'e3ee','browser-1':'e3ef','minimize-browser-1':'e3f0','new-browser-1':'e3f1','close-browser-1':'e3f2','browser-2':'e3f3','minimize-browser-2':'e3f4','new-browser-2':'e3f5','close-browser-2':'e3f6','multiple-browsers-1':'e3f7','browser-3':'e3f8','minimize-browser-3':'e3f9','new-browser-3':'e3fa','close-browser-3':'e3fb','folder-1':'e3fc','add-folder-1':'e3fd','remove-folder-1':'e3fe','upload-to-folder-1':'e3ff','download-to-folder-1':'e400','folder-2':'e401','open-folder-1':'e402','add-to-folder-1':'e403','document-1':'e404','document-2':'e405','document-3':'e406','document-4':'e407','multiple-documents-1':'e408','remove-documents-1':'e409','add-document-1':'e40a','close-document-1':'e40b','upload-document-1':'e40c','download-document-1':'e40d','import-document-1':'e40e','export-document-1':'e40f','clipboard-1':'e410','clipboard-1a':'e411','clipboard-1b':'e412','clipboard-2':'e413','remove-from-clipboard-1':'e414','add-to-clipboard-1':'e415','add-to-clipboard-2':'e416','clipboard-2a':'e417','clipboard-2b':'e418','comment-1':'e419','thinking-comment-1':'e41a','writing-comment-1':'e41b','posted-comment':'e41c','comment-2':'e41d','thinking-comment-2':'e41e','writing-comment-2':'e41f','posted-comment-2':'e420','chat-1':'e421','chat-2':'e422','chat-3':'e423','lock-1':'e424','unlock-1':'e425','lock-2':'e426','lock-3':'e427','unlock-3':'e428','key-1':'e429','key-2':'e42a','bell-1':'e42b','bell-2':'e42c','bell-3':'e42d','print-1':'e42e','calculator-1':'e42f','cloud-1':'e430','cloud-upload-1':'e431','cloud-download-1':'e432','cloud-add-1':'e433','cloud-2':'e434','home-1':'e435','home-2':'e436','home-3':'e437','open-book-1':'e438','open-book-2':'e439','open-book-3':'e43a','open-book-4':'e43b','book-1':'e43c','book-2':'e43d','book-3':'e43e','book-4':'e43f','book-5':'e440','book-6':'e441','newspaper-1':'e442','newspaper-2':'e443','newspaper-3':'e444','newspaper-4':'e445','journal-1':'e446','journal-2':'e447','journal-3':'e448','notebook-1':'e449','notebook-2':'e44a','notebook-3':'e44b','article-1':'e44c','article-2':'e44d','article-3':'e44e','notepad-1':'e44f','notepad-2':'e450','suitcase-1':'e451','suitcase-2':'e452','briefcase-1':'e453','toolbox':'e454','shirt-1':'e455','umbrella-1':'e456','coffee-1':'e457','glasses-1':'e458','zoom-1':'e459','zoom-2':'e45a','zoom-in-1':'e45b','eye-1':'e45c','eye-2':'e45d','eye-3':'e45e','eye-4':'e45f','iphone-1':'e460','ipad-mini-1-tablet-2':'e461','ipad-1':'e462','macbook-1':'e463','imac-1':'e464','smart-phone-1':'e465','tablet-1':'e466','laptop-1':'e467','computer-1':'e468','smart-phone-2':'e469','photo-1':'e46a','photo-gallery-1':'e46b','media-gallery-1':'e46c','video-camera-1':'e46d','tv-1':'e46e','film-strip-1':'e46f','camera-1':'e470','camera-2':'e471','camera-3':'e472','camera-4':'e473','calendar-1':'e474','calendar-2':'e475','calendar-3':'e476','calendar-4':'e477','calendar-5':'e478','taskpad-1':'e479','inbox-1':'e47a','inbox-checkmark-1':'e47b','inbox-upload-1':'e47c','inbox-add-1':'e47d','inbox-2':'e47e','inbox-3':'e47f','trash-1':'e480','trash-2':'e481','trash-3':'e482','trash-4':'e483','trash-5':'e484','bucket-1':'e485','paperclip-1':'e486','paperclip-2':'e487','compose-1':'e488','compose-2':'e489','compose-3':'e48a','pencil-1':'e48b','pencil-2':'e48c','brush-1':'e48d','compose-4':'e48e','feather-1':'e48f','compose-5':'e490','feather-2':'e491','mail-1':'e492','mail-2':'e493','mail-3':'e494','mail-4':'e495','send-1':'e496','share-1':'e497','share-2':'e498','reply-to-all-1':'e499','reply-1':'e49a','forward-1':'e49b','star-1':'e49c','star-2':'e49d','heart-1':'e49e','bookmark-1':'e49f','bookmark-2':'e4a0','bookmark-3':'e4a1','pin-1':'e4a2','share-3':'e4a3','share-4':'e4a4','share-5':'e4a5','link-1':'e4a6','link-2':'e4a7','link-3':'e4a8','link-4':'e4a9','question-mark-1':'e4aa','question-mark-2':'e4ab','exclamation-point-1':'e4ac','warning':'e4ad','info-1':'e4ae','info-2':'e4af','user-1':'e4b0','add-user-1':'e4b1','user-2':'e4b2','user-3':'e4b3','user-4':'e4b4','group-1':'e4b5','profile-1':'e4b6','id-1':'e4b7','id-2':'e4b8','contacts-1':'e4b9','contacts-2':'e4ba','moon-1':'e4bb','water-1':'e4bc','leaf-1':'e4bd','anchor-1':'e4be','trophy-1':'e4bf','gas-1':'e4c0','food-1':'e4c1','building-1':'e4c2','government-1':'e4c3','cart-1':'e4c4','cart-2':'e4c5','cart-3':'e4c6','tag-1':'e4c7','tag-2':'e4c8','money-1':'e4c9','money-2':'e4ca','shop-1':'e4cb','shop-2':'e4cc','basket-1':'e4cd','basket-2':'e4ce','gift-1':'e4cf','gift-2':'e4d0','bag-1':'e4d1','cube-1':'e4d2','ring-1':'e4d3','wallet-1':'e4d4','credit-card-1':'e4d5','flag-1':'e4d6','flag-2':'e4d7','battery-full-1':'e4d8','battery-ok-1':'e4d9','battery-low-1':'e4da','battery-empty-1':'e4db','battery-charging-0':'e4dc','battery-dead-1':'e4dd','battery-full-2':'e4de','battery-ok-2':'e4df','battery-low-2':'e4e0','battery-empty-2':'e4e1','battery-charging-1':'e4e2','battery-charging-2':'e4e3','wifi-1':'e4e4','wifi-rounded-1':'e4e5','wifi-2':'e4e6','wifi-rounded-2':'e4e7','wifi-3':'e4e8','wifi-rounded-3':'e4e9','graph-1':'e4ea','graph-2':'e4eb','bluetooth-1':'e4ec','history-1':'e4ed','time-1':'e4ee','time-2':'e4ef','alarm-clock-1':'e4f0','stopwatch-1':'e4f1','compass-1':'e4f2','gauge-1':'e4f3','gauge-2':'e4f4','map-1':'e4f5','location-pin-1':'e4f6','location-pin-2':'e4f7','location-pin-3':'e4f8','location-pin-4':'e4f9','location-pin-5':'e4fa','location-pin-6':'e4fb','location-arrow-1':'e4fc','globe-1':'e4fd','aim-1':'e4fe','target-1':'e4ff','call-1':'e500','call-2':'e501','call-3':'e502','call-4':'e503','lightbulb-1':'e504','lightbulb-2':'e505','lightbulb-3':'e506','see-all-1':'e507','command-1':'e508','option-1':'e509','control-1':'e50a','shift-1':'e50b','eject-1':'e50c','loading-1':'e50d','loading-2':'e50e','loading-3':'e50f','logout-1':'e510','asterisk-1':'e511','email-1':'e512','pound-1':'e513','settings-1':'e514','settings-2':'e515','wrench-1':'e516','wrench-2':'e517','work-1':'e518','scissors-1':'e519','type-1':'e51a','graph-3':'e51b','activity-monitor-1':'e51c','activity-1':'e51d','running-1':'e51e','error-1':'e51f','slider-1':'e520','slider-2':'e521','slider-3':'e522','toggle-1':'e523','server-1':'e524','network-1':'e525','copyright-1':'e526','crop-1':'e527','left-text-1':'e528','center-text-1':'e529','right-text-1':'e52a','paragraph-text-1':'e52b','align-top-1':'e52c','align-bottom-1':'e52d','align-horizontally-1':'e52e','align-left-1':'e52f','align-center-1':'e530','align-right-1':'e531','align-top-edges-1':'e532','distribute-vertical-centers-1':'e533','align-bottom-edges-1':'e534','align-left-edges-1':'e535','distribute-centers-horizontally-1':'e536','align-right-edges-1':'e537','layout-1':'e538','layout-2':'e539','layout-3':'e53a','layout-4':'e53b','menu-1':'e53c','menu-2':'e53d','menu-3':'e53e','menu-list-1':'e53f','menu-list-2':'e540','menu-list-3':'e541','menu-list-4':'e542','paragraph-justify-1':'e543','paragraph-left-justify-1':'e544','paragraph-left-1':'e545','paragraph-centered-1':'e546','paragraph-right-1':'e547','paragraph-right-justify-1':'e548','marquee-1':'e549','marquee-plus-1':'e54a','marquee-2':'e54b','marquee-plus-2':'e54c','sun-1':'e54d','sun-2':'e54e','sun-3':'e54f','turn-page-down-1':'e550','turn-page-up-1':'e551','slide-out-left-1':'e552','slide-in-right-1':'e553','slide-in-left-1':'e554','slide-out-right-1':'e555','fold-up-1':'e556','fold-down-1':'e557','fold-left-1':'e558','fold-right-1':'e559','move-up-1':'e55a','move-down-1':'e55b','login-1':'e55c','logout-2':'e55d','upload-1':'e55e','download-1':'e55f','upload-2':'e560','download-2':'e561','save-1':'e562','save-2':'e563','previous-1':'e564','rewind-1':'e565','stop-1':'e566','pause-1':'e567','play-1':'e568','fast-forward-1':'e569','next-1':'e56a','previous-2':'e56b','rewind-2':'e56c','stop-2':'e56d','pause-2':'e56e','play-2':'e56f','fast-forward-2':'e570','next-2':'e571','previous-3':'e572','rewind-3':'e573','pause-3':'e574','play-3':'e575','fast-forward-3':'e576','next-3':'e577','previous-4':'e578','pause-4':'e579','play-4':'e57a','next-4':'e57b','speaker-1':'e57c','speaker-2':'e57d','speaker-3':'e57e','speaker-4':'e57f','headphones-1':'e580','microphone-1':'e581','microphone-2':'e582','music-1':'e583','megaphone-1':'e584','infinity-1':'e585','shuffle-1':'e586','repeat-1':'e587','repeat-2':'e588','repeat-3':'e589','repeat-4':'e58a','refresh-1':'e58b','position-1':'e58c','minimize-1':'e58d','maximize-1':'e58e','maximize-2':'e58f','maximize-3':'e590','maximize-4':'e591','expand-vertically-1':'e592','expand-horizontally-1':'e593','arrow-up-1':'e594','arrow-down-1':'e595','arrow-left-1':'e596','arrow-right-1':'e597','arrow-up-2':'e598','arrow-down-2':'e599','arrow-left-2':'e59a','arrow-right-2':'e59b','left-angle-quote-1':'e59c','right-angle-quote-1':'e59d','left-angle-quote-2':'e59e','right-angle-quote-2':'e59f','plus-1':'e5a0','minus-1':'e5a1','checkmark-1':'e5a2','x-1':'e5a3','up-1':'e5a4','down-1':'e5a5','left-1':'e5a6','right-1':'e5a7','up-2':'e5a8','down-2':'e5a9','left-2':'e5aa','right-2':'e5ab','up-3':'e5ac','down-3':'e5ad','left-3':'e5ae','right-3':'e5af','up-4':'e5b0','down-4':'e5b1','left-4':'e5b2','right-4':'e5b3','plus-2':'e5b4','minus-2':'e5b5','checkmark-2':'e5b6','x-2':'e5b7','up-circle-1':'e5b8','down-circle-1':'e5b9','left-circle-1':'e5ba','right-circle-1':'e5bb','up-circle-2':'e5bc','down-circle-2':'e5bd','left-circle-2':'e5be','right-circle-2':'e5bf','up-circle-3':'e5c0','down-circle-3':'e5c1','left-circle-3':'e5c2','right-circle-3':'e5c3','up-circle-4':'e5c4','down-circle-4':'e5c5','left-circle-4':'e5c6','right-circle-4':'e5c7','plus-3':'e5c8','minus-3':'e5c9','checkmark-3':'e5ca','x-3':'e5cb','up-square-1':'e5cc','down-square-1':'e5cd','left-square-1':'e5ce','right-square-1':'e5cf','up-square-2':'e5d0','down-square-2':'e5d1','left-square-2':'e5d2','right-square-2':'e5d3','up-square-3':'e5d4','down-square-3':'e5d5','left-square-3':'e5d6','right-square-3':'e5d7','up-square-4':'e5d8','down-square-4':'e5d9','left-square-4':'e5da','right-square-4':'e5db'},


Show:function(){  
with(FlaticonSHW){
  
  var s,v,k,W,size,Str="", WH, x=10, y=0;
  
  size=32;
  WH=size+16;
  W=470;

  Str="<div "+SPA+"left:8;top:8px;right:8px;height:28px;;background-color:"+Wizard.defCols[0]+";border-radius:2px;'>"+  
      "<div "+SPA+"left:8px;top:5px;font:bold 13px opensansR;color:#FFF;'>"+LANG("Select icon")+"</div>"+
    "</div>"+ 
    "<div "+SPA+"right:16px;top:11px;cursor:pointer;font:16px FlaticonsStroke;color:#FFF;' onclick='FlaticonSHW.Close();'></div>"+
    "<div "+SPA+"left:0px;top:50px;right:2px;bottom:10px;overflow:auto;'>";

  for(k in LIST){
    v=h2d(LIST[k]);
    Str+="<div "+SPA+"left:"+x+"px;top:"+y+"px;font:"+size+"px FlaticonsStroke;cursor:default;' title='"+k+" ("+v+
         ")' onclick='FlaticonSHW.Save("+v+")'>"+String.fromCharCode(v)+"</div>";
    x+=WH; if(x>(W-60)) { y+=WH; x=10; }
  }
  
  Str+="</div>"
  
  mf$("idAdmFont").innerHTML=Str;
  mf$("idAdmFont").style.display="block";
  mf$("idMaskAdmFont").style.display="block";
}},



Save:function(v){
with(FlaticonSHW){
  mf$("idf_imgtile").value=v;
  mf$("idf_imgtile_icon").innerHTML=String.fromCharCode(v);
  Close();
}},


Remove:function(){
with(FlaticonSHW){
  mf$("idf_imgtile").value="";
  mf$("idf_imgtile_icon").innerHTML="";
}},


Close:function(){
  mf$("idAdmFont").style.display="none";
  mf$("idMaskAdmFont").style.display="none"; 

},

d2h:function(d) {return d.toString(16);},

h2d:function(h) {return parseInt(h,16);}

} // end



//----------------------------------------------------------------------------------------------- 
// show palette colors

ColorSHW={ SPA:"style='position:absolute;",

  col_set:"#000000,#252525,#343434,#4e4e4e,#686868,#757575,#8e8e8e,#a4a4a4,#b8b8b8,#c5c5c5,#d0d0d0,#d7d7d7,#e1e1e1,#eaeaea,#f4f4f4,#ffffff,#412000,#542801,#763701,#9b4f01,#c26807,#e47b06,#ff901a,#ffab1d,#fec51e,#ffd03c,#ffd84c,#ffe550,#fef455,#fff96f,#ffff8f,#ffffa9,#461904,#721f11,#9f241d,#b33921,#c7511f,#e36920,#fc8121,#fd8c24,#fe982c,#ffae38,#ffb947,#ffbf51,#ffc66d,#ffd687,#ffe497,#ffe7ab,#5d1e0c,#7a240d,#982c0f,#b02f0f,#bf3624,#d34e2b,#e7623f,#f36e4b,#fd7855,#ff8a69,#ff987d,#ffa38a,#ffb29e,#ffc1b2,#ffd1c4,#ffdad1,#4a1801,#721f00,#a81300,#c72109,#df2411,#eb3b24,#fa5237,#fc6047,#ff705f,#ff7f7e,#ff8f8e,#ff9d9e,#ffaaad,#ffb8bc,#ffc8ce,#ffcade,#490037,#66004a,#80035f,#960f75,#aa2288,#ba3d99,#ca4da9,#d75ab6,#e467c3,#ef72ce,#fb7eda,#ff8de2,#ff9ee5,#ffa4e7,#ffafea,#feb8ec,#48036d,#5b0487,#640d90,#7a23a6,#923bbe,#9c45c8,#a64fd2,#b15add,#bc65e8,#c46df0,#cd76f9,#d583ff,#da90ff,#de9cfe,#e3a9fe,#e6b7ff,#051e82,#0726a5,#082fca,#253dd3,#444cdd,#4f59ec,#5a68ff,#6575ff,#7183ff,#8090ff,#90a0ff,#97a9ff,#9fb2ff,#afbeff,#c0ccff,#ccd3ff,#0b0778,#201c8d,#3531a2,#4642b3,#5753c4,#615dce,#6d69da,#7b77e8,#8985f6,#918dfe,#9d98ff,#a7a4ff,#b2affe,#bbb8ff,#c3c1ff,#d3d1ff,#1d295b,#1c3877,#1d4992,#1d5cac,#1d71c7,#3286cf,#479bd9,#4da9ec,#55b5ff,#69caff,#74cbff,#82d3ff,#8cdafe,#9fd4fe,#b5e2ff,#c0ebfe,#004b59,#005d6e,#006f83,#00849b,#009ac0,#00abc9,#00bcdf,#00d0f6,#10dcff,#3ee1ff,#65e7ff,#76ebff,#8cedfe,#99effe,#b1f3ff,#c7f6fe,#004800,#005400,#026b04,#0d760f,#178019,#279228,#37a437,#4eb94f,#51cd51,#71da73,#7be47d,#84ed86,#99f19a,#b4f7b2,#c3fac3,#cdfdcd,#164000,#1c5300,#236600,#287800,#2e8c00,#3a980c,#47a519,#51af23,#5cba2e,#71cf43,#85e357,#8deb5f,#97f569,#a0fe72,#b1fe8a,#bcfe9a,#2c3500,#394400,#445101,#485600,#607100,#6c7f01,#798d0a,#8b9f1c,#9eb22f,#abbf3c,#b8cc49,#c2d653,#cee153,#dbef6c,#e8fc79,#f2ffab,#463a08,#4c3f0a,#544609,#6b5809,#907608,#aa8c0a,#c0a220,#cfb12f,#ddbf3d,#e5c745,#ecce4c,#f5d862,#fae276,#fced98,#fdf4a9,#fdf3be,#401a03,#581f04,#702508,#8d3a12,#ab511f,#b46427,#bf772f,#d1853a,#e09343,#eda04e,#f9ad59,#fcb75c,#ffc05f,#ffca6a,#ffcf7d,#ffda96",
         
 

Show:function(){
with(ColorSHW){
  
  var v,k,Str="", x=0, y=0, ww=30, hh=26, acol,onm;
  acol=col_set.split(",");
  
 

  Str="<div "+SPA+"left:8;top:8px;right:8px;height:28px;;background-color:"+Wizard.defCols[0]+";border-radius:2px;'>"+  
      "<div "+SPA+"left:8px;top:5px;font:bold 13px opensansR;color:#FFF;'>"+LANG("Select color")+"</div>"+
    "</div>"+ 
    "<div "+SPA+"right:16px;top:11px;cursor:pointer;font:16px FlaticonsStroke;color:#FFF;' onclick='ColorSHW.Close();'></div>"+
    "<div "+SPA+"left:0px;top:50px;right:2px;bottom:10px;overflow:auto;'>";

  for(k=0;k<acol.length;k++){
    v=acol[k];
    onm="onmouseover='this.style.borderColor=\"#444\";this.style.zIndex=10;' onmouseout='this.style.borderColor=\"#DDD\";this.style.zIndex=1;'";
    Str+="<div "+SPA+"left:"+(x+8)+"px;top:"+y+"px;width:"+ww+"px;height:"+hh+"px;background-color:"+v+";border:1px solid #DDD;"+
    "z-index:1;cursor:default;' "+onm+" onclick='ColorSHW.Save(\""+v+"\")'></div>";
    x+=ww; if(x>=(ww*16) ) { x=0; y+=hh; }
  }
  
  Str+="</div>"
  
  mf$("idAdmColp").innerHTML=Str;
  mf$("idAdmColp").style.display="block";
  mf$("idMaskAdmColp").style.display="block";
}},



Save:function(v){
with(ColorSHW){
  WizADM.f_update_color(0,v);
  Close();
}},

Close:function(){
  mf$("idAdmColp").style.display="none";
  mf$("idMaskAdmColp").style.display="none"; 

}


} // end


 