/* ?adm=1
 
 WareExit   =>   0=no exit in ware_connection       1=insert exit in every level no first  3=even first
 
 picture_x  =>   id_picture    contain base64 picture from camera o gallery   
 
 wizard_color_set
 wizard_upload_max_file => default 5MB
 
 MandatoryWare => if t_wares is mandatory before send WO
 */

staticUrl = "../";

Wizard = {ADM: 0, NCOLS: 3, INBUILD: 0,
    DBNAME: "db_mainsim_locale",
    table: ["t_wizard", "t_wizard_temp"],
    tabid: 0, userLEV: -1, NS: "",
    WareStart: "",

    vers: 0, tl: 120, mg: 6, tlmg: 0, sw: 16,
    J: null, jbx: null, jmb: null, jex: null, jtew: null,
    jwb: null, jwtb: null, jab: null, jfr: null, jbt: null, jBT: null, jful: null,

    WW: 0, HH: 0, WB: 0, HB: 0, TY: 16,
    sct: 0, sch: 0, scH: 0, PDR: "",
    pth: [], Paths: [], MeD: 0,
    Fnt: 13, FNT: 30,
    aTile: [], aPdr: {}, pTL: {}, tempPDR: "", BACK: 0, oPDR: "", RFD: {},

    SPA: "style='position:absolute;", OM: "", OU: "",
    vWare: ["", 0, 0, ""],
    WareExit: 0,

    wizboxpage: "boxpage/",
    wizImg: "img/",

    phpRead: staticUrl + "wizard/read-wizard",
    phpSave: staticUrl + "wizard/save-wizard",
    phpWares: staticUrl + "wizard/wares-wizard",
    phpSend: staticUrl + "workorder/wizard?ns=1&resp=1",
    phpReadSel: staticUrl + "wizard/read-select",
    upload_max_file: 5242880,
    MandatoryWare: 0,

    noanim: 0, inLOADING: 0, FD: [],

    TLxy: [], Stp: 4,
    TL_ACTIONS_xy: [],
    VAL: {}, refVal: {}, VxTile: {},
    REStart: 0,

    Cols: [],
    defCols: ["#496670", "#6E8D97", "#4ECDC4", "#AED138", "#FF6B6B", "#C44D58"], //   wizard_color_set    A9BCC2
    rotCol: 0, pdCOL: "", chCOL: "",

    Start: function () {
        with (Wizard) {

            f_resetVars();

            // user level from desktop or mobile   
            try {
                userLEV = parent.moGlb.user.level;
                // if administrator
                var lo = location.href, alo;
                alo = lo.split("adm=1");
                if (alo.length > 1)
                    ADM = 1;
            } catch (e) {
                userLEV = parent.mobileWO.user_level;    // from mobile
                NS = "?ns=1";
                parent.WizWinObj(window);
            }

            J = mf$("idStage");
            var r, t0, t1, t2, conn, xt;

            // read data from database or database in localstorage (and check if out of date)
            conn = Dbl.connect_db(DBNAME);
            if (!conn) {
                // create db
                Dbl.create_db(DBNAME);
                conn = Dbl.connect_db(DBNAME);
                if (!conn) {
                    WizADM.f_popAlert("No DB connected!");
                    return;
                }
            }

            INBUILD = 1;
            Dbl.AUTOSAVE = 1;

            t0 = "(f_id inc," +
                    "f_code int,";

            t1 = "f_parent_code string," +
                    "f_title string title," +
                    "f_subtitle string," +
                    "f_imgtile string," +
                    "f_linktitle string," +
                    "f_visibility int -1," +
                    "f_order int 0," +
                    "f_field string," +
                    "f_color string," +
                    "f_tilewidth int," +
                    "f_srcbox string," +
                    "f_ware_start string,"+
                    "fc_documents string," +
                    "fc_asset_wo string," +
                    "fc_asset_opened_wo string," +
                    "fc_asset_opened_pm string," +
                    "fc_asset_opened_on_condition string)";

            t2 = "(f_id inc," +
                    "f_code string,";

            Dbl.DROP_TABLE("t_wizard");
            Dbl.DROP_TABLE("t_wizard_temp");
            xt = Dbl.query("create_table t_wizard " + t0 + t1);
            Dbl.query("create_table t_wizard_temp " + t2 + t1);

            //Debug.Open();


            // select administrator WO priority and WO owner and Anchor name    
            if (ADM) {
                Ajax.sendAjaxPost(phpReadSel + NS, "version=" + vers, "Wizard.fRetRsel");
            }


            // if  offline && exists tables && MOBILE (NS)
            if (NS && !xt) {
                var status = parent.OnOffline.STATUS;  // 0 offline   1 online
                if (!status) {

                    WareStart = Dbl.GETVAR("WareStart");
                    if (WareStart.substring(0, 1) == "+")
                        WareExit = 1;
                    if (WareStart.indexOf("@") != -1)
                        WareExit *= 2;

                    INBUILD = 0;
                    f_run();
                    return;
                }
            }

            // version of t_wizard in f_order of root element
            vers = f_version();
            Ajax.sendAjaxPost(phpRead + NS, "version=" + vers, "Wizard.fRetLCh");

        }
    },

    fRetRsel: function (a) {
        with (WizADM) {

            var k, kk, ar = {};
            try {
                ar = JSON.parse(a);
            } catch (e) {
                alert("Select error!" + a);
                return;
            }

            WO_pr = {};
            WO_ow = {};
            Anch = ['none', 'root'];

            if (ar.wo_priority) {
                for (k in ar.wo_priority) {
                    kk = k;
                    if (k == "none")
                        kk = "0";
                    WO_pr[kk] = ar.wo_priority[k];
                }
            }
            if (ar.wo_owner)
                WO_ow = moGlb.cloneArray(ar.wo_owner);
            if (ar.anchor_name)
                Anch = moGlb.cloneArray(ar.anchor_name);

        }
    },

    f_version: function () {
        with (Wizard) {
            var s, a;
            a = Dbl.query("SELECT t_wizard GET f_order WHERE f_parent_code='-0-'");
            if (!a.length) {
                Dbl.query("INSERT " + table[0], [1, 1, '-0-', 'Wizard', '', '', '', -1, -1, '', '', 0, '', '']);
                s = -1;
            } else
                s = a[0]["f_order"];
            return s;
        }
    },

    fRetLCh: function (a) {
        with (Wizard) {

            var ar = [], DAT = [], v, r, rr, V, lis, b, bb;
            try {
                ar = JSON.parse(a);
            } catch (e) {
                alert("Data error!" + a);
                return;
            }
            DAT = ar.data;

            this.asset_actions = ar.asset_actions;

            if (typeof (ar.wizard_upload_max_file) != "undefined")
                upload_max_file = parseInt(ar.wizard_upload_max_file);
            else
                upload_max_file = 5242880;

            if (typeof (ar.MandatoryWare) != "undefined")
                MandatoryWare = parseInt(ar.MandatoryWare);
            else
                MandatoryWare = 0;


            f_waitLoading(0);
            RFD = {};

            if (DAT.length) {     // DAT if different checksum

                vers = ar.version; // update old checksum

                // insert in t_wizard new data
                Dbl.TRUNCATE(table[0]);
                Dbl.query("INSERT " + table[0], DAT);

                // update parent code   1,2,3  =>  -1-2-3-
                V = Dbl.db[table[0]].data;

                for (r = 0; r < V.length; r++) {
                    RFD[ V[r][1] + "" ] = (V[r][12] ? 1 : 0);               // if f_srcbox is set => when back dont delete
                    v = V[r][2] + "";
                    v = v.multiReplace({" ": "", ",": "-"});
                    if (v.substring(0, 1) != "-")
                        v = "-" + v;
                    if (v.substring(v.length - 1) != "-")
                        v += "-";
                    V[r][2] = v;
                    if (V[r][13] == "null" || !V[r][13])
                        V[r][13] = "";
                }

                // verify ware connection to load (only one connection offline)
                // exit WareStart + => WareExit=1 | 3
                V = Dbl.db["t_wizard"].data;
                b = "";
                for (r = 0; r < V.length; r++) {
                    b = V[r][13];      // ware connection
                    if (b)
                        break;
                }

                if (b) {
                    p = V[r][1];                        // f_code (pdr)
                    for (rr = r + 1; rr < V.length; rr++) {
                        bb = V[rr][13];
                        if (bb && bb != b) {
                            b = "";
                            break;
                        }
                    }
                }

                if (b) {
                    WareStart = b;
                    Dbl.SETVAR("WareStart", WareStart);
                    WareExit = 0;
                    if (b.substring(0, 1) == "+") {
                        WareExit = 1;
                        b = b.substring(1);
                    }
                    if (b.indexOf("@") != -1) {
                        WareExit *= 2;
                        b = (b.split("@")).join("");
                    }
                    f_waitLoading(1);
                    Ajax.sendAjaxPost(phpWares + NS, "f_ware_start=" + b + "&pdr=" + p, "Wizard.fRetWares");
                } else
                    INBUILD = 0;


            } else {
                INBUILD = 0;
                V = Dbl.db[table[0]].data;
                try {
                    for (r = 0; r < V.length; r++) {
                        RFD[ V[r][1] + "" ] = (V[r][12] ? 1 : 0);   // if f_srcbox is set => when back dont delete
                    }
                } catch (e) {
                }


            }

            // colors
            if (ar["colors"]) {

                if (typeof (ar["colors"]) == "string") {

                    lis = ar["colors"].replace(/ /g, "");
                    Cols = lis.split(",");

                } else
                    Cols = ar["colors"];

                Dbl.SETVAR("wizard_color_set", JSON.stringify(Cols));
            }

            f_run();
        }
    },

    f_run: function () {
        with (Wizard) {

            if (INBUILD)
                return;

            Cols = JSON.parse(Dbl.GETVAR("wizard_color_set"));
            if (!Cols) {
                Cols = defCols;
                Dbl.SETVAR("wizard_color_set", JSON.stringify(Cols));
            }

            ar = Dbl.query("SELECT " + table[0] + " GET f_code WHERE f_parent_code='-0-'", "Num");
            PDR = ar[0][0];

            pth = [PDR];
            f_set_VxTile(0, 0);
            Resize();
        }
    },

    fRetWares: function (a) {
        with (Wizard) {

            var dat = [], v, r, V;
            try {
                dat = JSON.parse(a);
            } catch (e) {
                alert("Wares error!\n" + a);
                return;
            }

            Dbl.TRUNCATE(table[1]);

            if (dat.length) {

                Dbl.query("INSERT " + table[1], dat);

                // update parent code   1,2,3  =>  -1-2-3-
                V = Dbl.db[table[1]].data;
                for (r = 0; r < V.length; r++) {
                    v = V[r][2] + "";
                    v = v.multiReplace({" ": "", ",": "-"});
                    V[r][2] = "-" + v + "-";
                }
            }

            if (INBUILD) {
                INBUILD = 0;
                f_waitLoading(0);
                f_run();
                return;
            }

            jab.style.display = "none";
            inLOADING = 0;
            f_waitLoading(0);
            DrawTLs();

            if (!MeD)
                noanim = 0;
        }
    },

    Resize: function (e) {
        with (Wizard) {

            if (REStart) {
                REStart = 0;
                freset(1);
                return;
            }

            //if(MeD) return;
            WW = moGlb.getClientWidth(), HH = moGlb.getClientHeight();
            if (ADM)
                HH -= 48;

            tl = 120, mg = 6, sw = mg * 2 + 4, TY = 16;
            tlmg = tl + mg;

            NCOLS = 3;

            if (!isTouch) {
                var n = parseInt((WW - sw - tl) / tlmg);
                if (n > 5)
                    n = 5;
                if (n < 3)
                    n = 3;
                NCOLS = n;
            }

            WB = tlmg * NCOLS + sw;

            if (WW < WB) {
                WB = WW;
                tlmg = parseInt((WB - sw) / NCOLS);
                tl = tlmg - mg;
                sw = WB - (tlmg * NCOLS);
            }

            if (isTouch) {
                HB = HH, TY = 0;
            } else {
                HB = HH - (TY + 50); // TY 16px top   80px footer
            }

            sch = HB - tl - mg;
            Draw0();
            if (ADM)
                WizADM.View();

        }
    },

    Draw0: function () {
        with (Wizard) {

            var str = "", stt = "", r, onm;

            str += "<div id='idwbox' " + (isTouch ? "" : "class='clbox'") + " " + SPA + "left:50%;top:" + (TY + ADM * 48) + "px;width:" + WB + "px;height:" + HB + "px;margin-left:-" + (WB / 2) + "px;' >" +
                    // scroll
                    "<div " + SPA + "left:" + parseInt(WB - sw + mg) + "px;top:" + tl + "px;width:4px;height:" + (HB - tl) + "px;overflow:hidden;'>" +
                    "<div id='idScrl'></div>" +
                    "</div>" +
                    // text box
                    "<div id='idwtext' " + SPA + "left:" + mg + "px;top:" + mg + "px;width:" + (WB - mg * 2) + "px;height:" + (tl - mg * 2) + "px;font:" + FNT + "px opensansL;color:#000;'>Wizard</div>" +
                    // box contain tile
                    "<div " + SPA + "left:0;top:" + tl + "px;width:" + (WB - sw) + "px;height:" + (HB - tl - mg) + "px;overflow:hidden;'>" +
                    // tile box
                    "<div id='idbox' " + SPA + "left:0;top:0px;width:100%;height:100%;'></div>" +       
                    // asset action box
                    "<div id='id_asset_action' "+SPA+"left:20px;top:0px;width:96%;height:100%;display:none;background-color:white;border-radius: 10px;'></div>"+
                    "</div>" +
                    // anim box
                    "<div id='idabox' class='cltile' " + SPA + "left:0;top:0;width:100%;height:100%;display:none;'></div>" +
                    // mask box
                    "<div id='idmbox' " + SPA + "left:0;top:0;width:100%;height:100%;background-color:#58F;opacity:0.0;filter:alpha(opacity=0);-moz-opacity:0.0;'></div>" +
                    // exit box
                    "<div id='idebox' " + SPA + "left:0;top:0;width:100%;height:100%;display:none;'></div>" +
                    // form 
                    "<div id='idfrmbtn' " + SPA + "left:0;top:0;width:100%;height:100%;display:none;'>" +
                    "<iframe id='ifrmbox' src='' " + SPA + "left:" + mg + "px;top:" + mg + "px;width:" + (WB - mg * 2) + "px;height:" + (HB - 40) + "px;background-color:#FFF;" + // background-color:#FFF;  ???
                    "-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;' frameborder=0></iframe>" +
                    "<div id='idbtnf' " + SPA + "left:0px;bottom:0px;width:" + WB + "px;height:" + (40 - mg * 2) + "px;overflow:hidden;'></div>" +
                    "</div>" +
                    "</div>" +
                    // path
                    "<div id='idPath' " + SPA + "left:5%;top:" + (HB + 32 + ADM * 48) + "px;width:90%;font:13px opensansL;color:#444;text-align:center;'></div>";


            J.innerHTML = str;

            jwb = mf$("idwbox");
            jsc = mf$("idScrl");
            jwtb = mf$("idwtext");
            jbx = mf$("idbox");
            jab = mf$("idabox");
            jmb = mf$("idmbox");
            jex = mf$("idebox");
            jbt = mf$("idfrmbtn");
            jfr = mf$("ifrmbox");
            jBT = mf$("idbtnf");
            jful = mf$("idFullscreen");
            jpt = mf$("idPath");
            jtew = mf$("idTestADMEndWizard");

            if (isTouch)
                jpt.style.display = "none";

// events
            var r = 0, l = ["touchstart", "touchmove", "touchend", "mousedown","click"];
            for (; r < l.length; r++) {
                eval("jmb.on" + l[r] + "=function(e){ e=e||window.event; Wizard.Evt(e); return false; }");
            }

            if (!isTouch) {
                jmb.onmouseover = function () {
                    WHEELOBJ = "Wizard";
                }
                jmb.onmouseout = function () {
                    WHEELOBJ = "";
                }
            }

            rotCol = 0;
            getCol();
            DrawTLs();
        }
    },

    getCol: function (i) {
        with (Wizard) {
            var n = Cols.length;
            if (i)
                rotCol += i;
            if (rotCol >= n)
                rotCol %= n;
            if (rotCol < 0)
                rotCol += n;

            pdCOL = Cols[rotCol];
            rotCol++;
            if (rotCol >= n)
                rotCol %= n;
            if (rotCol < 0)
                rotCol += n;
            chCOL = Cols[rotCol];
        }
    },

    DrawTLs: function (ftx) {
        with (Wizard) {

            if (!ftx)
                ftx = "";

            // mf$("debug").innerHTML=pth.join("<br>");

            var r, a, b, str = "", c, cc, ptr = "", n, nr, nc, x, y, pco, tb, bt, mbt, bft = "", tipi;

            sct = 0;
            tb = tabid;

            if (vWare[0]) {
                if (vWare[1] == PDR)
                    tb = 0;
                if (vWare[2] == PDR) {
                    tb = 0;
                    vWare[0] = "";
                    tabid = 0;
                }
            }



            if (tempPDR)
                tb = 1;
            PDR += "";
            if (PDR.substring(0, 2) == "w_")
                tb = 1;
            if (PDR == (tempPDR + ""))
                tb = 0;

            a = Dbl.query("SELECT " + table[tb] + " GET * WHERE f_code='" + PDR + "'");   // get parent props
            aPdr = a[0];



            TLxy = [];
            TL_ACTIONS_xy = [];

            try {
                if (aPdr.f_color)
                    pdCOL = aPdr.f_color;
            } catch (e) {
                return;
            }
            jwb.style.backgroundColor = pdCOL;


            if (!MeD) {  // tiles  

                //console.log(aPdr); console.log(PDR+" | "+tempPDR+" | "+ftx)       
                var pws = aPdr.f_ware_start;

                aTile = f_getChildren(ftx);    // aTile children of PDR  
                n = aTile.length;

                //console.log(pws+" | "+aTile[0]["f_ware_start"]+" | "+PDR+" | "+n)

                if (PDR == 1 && pws != "" && n == 1 && aTile[0]["f_ware_start"] == pws) {
                    TLxy.push({x: 0, y: 0, i: 0, wtl: tl, f: aTile[0]["f_code"]});
                    clickTiles(0, 0, aTile[0]["f_code"]);
                    return;
                }



                nr = Math.ceil(n / NCOLS);

                c = 0;
                y = 0;
                tipi = 0;

                for (r = 0; r < n; r++) {
                    nc = ((aTile[r].f_tilewidth & 3) == 2) ? 2 : 1;
                    if ((c + nc) > NCOLS)
                        y += tlmg, c = 0;
                    x = mg + tlmg * c;
                    str += viewTL(aTile[r], x, y, r);
                    c += nc;

                    // check tile_picture
                    if ((aTile[r].f_field).indexOf("tile_picture") != -1) {
                        try {
                            eval("b={" + aTile[r].f_field + "}");
                        } catch (e) {
                        }
                        var pctr = b["tile_picture"];
                        tipi++;
                    }
                }

                scH = y + tl + mg;

                // str+=images 
                if (aPdr.f_subtitle) {
                    if (aPdr.f_subtitle.indexOf("<<#") !== -1) {
                        pctr = aPdr.f_subtitle.substring(aPdr.f_subtitle.indexOf("<<#")+3,aPdr.f_subtitle.indexOf("#>>"));
                        str += "<div id='id_tilepicture' style='position:absolute;left:" + mg + "px;top:" + scH + "px;right:" + mg + "px;height:660px;overflow:hidden;'>"+
                        "<img src='boxpage/img/" + pctr + "' style='width:100%;display: block;margin-left:auto;margin-right: auto;'/></div>";
                        scH += 640 + mg;
                        aPdr.f_subtitle = aPdr.f_subtitle.substring(0,aPdr.f_subtitle.indexOf("<<#"))+aPdr.f_subtitle.substring(aPdr.f_subtitle.indexOf("#>>")+3,aPdr.f_subtitle.length);
                    }
                    
                }

                jsc.style.display = (scH > sch) ? "block" : "none";

                pco = f_ink(pdCOL);
                ptr = LANG(aPdr.f_title);
                mbt = 0;

                if (aPdr.f_subtitle) {
                    bt = aPdr.f_subtitle;
                    var nbt = bt.length;
                    if (nbt > 1) {
                        if (bt.charAt(nbt - 1) == "+")
                            bt = bt.substring(0, nbt - 1);
                    }
                    bft = "";
                    if (bt.substring(0, 1) == "+") {
                        bt = bt.substring(1);
                        if (bt.length > 1)
                            bft = "<br>";
                        bft += "<div id='id_filter_pos'>&nbsp;</div>";
                        mbt = 1;
                    }
                    ptr += "<span style='font:15px opensansL;color:" + pco + ";'><br>" + (LANG(bt) + bft) + "</span>";
                }

                jwtb.style.color = pco;
                jwtb.innerHTML = ptr;
                jbx.style.top = sct + "px";   // scroll

                if (mbt)
                    f_filter_act(pdCOL, pco);
                else
                    f_filterClear();

            } else {   // form

                f_filterClear();

                // fullscreen
                if (aPdr.f_srcbox.indexOf("fullscreen") != -1) {
                    jful.style.display = "block";
                    setParent(jfr, jful);

                    var Wf = jful.offsetWidth, Hf = jful.offsetHeight;
                    jfr.style.width = (Wf - mg * 2) + "px";
                    jfr.style.height = (Hf - mg * 2) + "px";

                } else {
                    setParent(jfr, jbt);
                    jful.style.display = "none";
                    jfr.style.width = (WB - mg * 2) + "px";
                    jfr.style.height = (HB - 40) + "px";
                }

                noanim = 1;

                jBT.innerHTML = f_butn();
                jbt.style.display = "block";
                jfr.src = wizboxpage + aPdr.f_srcbox;
            }

            Fscrbar();
            jbx.innerHTML = str;



            if (!isTouch)
                jpt.innerHTML = drawPth();   // path
            else
                drawPth();
        }
    },

    setParent: function (el, newParent) {
        newParent.appendChild(el);
    },

    f_filterClear: function () {
        mf$("idDivFilter").style.display = "none";
        mf$("idInputFilter").value = '';
    },

    f_filter_act: function (pcol, icol) {
        with (Wizard) {

            var pj, p = moGlb.getPosition(mf$("id_filter_pos"));

            pj = mf$("idDivFilter");

            pj.style.left = p.l + "px";
            pj.style.top = (p.t + 8) + "px";
            pj.style.color = icol;
            pj.style.borderColor = icol;
            pj.style.backgroundColor = pcol;
            pj.style.display = "block";

            mf$("idInputFilter").placeholder = LANG("Find");
        }
    },

    FindKey: function (e) {
        with (Wizard) {
            var tx = mf$("idInputFilter").value;
            DrawTLs(tx);
        }
    },

    viewTL: function (a, x, y, i) {
        with (Wizard) {

            var s, im = "", wtl, inkcol, grad, c0, c1, f, bt, nbt, tbt = "", rdn = 0, kr = 3, isAlias = 0,
                    tw = a.f_tilewidth;

            wtl = tl;
            if (tw & 2)
                wtl = tl * 2 + mg;
            if (tw & 4)
                rdn = 1;

            if (a.f_color) {
                c1 = a.f_color;
                if (c1 == pdCOL)
                    c1 = chCOL;
            } else
                c1 = chCOL;

            c0 = colorTone(c1, 0.9, 0);
            inkcol = f_ink(c1);

            if (a.f_imgtile) {
                f = parseInt(a.f_imgtile);
                if (f > 58343 && f < 58844) {
                    im = "<div " + SPA + "left:0;width:100%;bottom:3px;font:42px FlaticonsStroke;color:" + inkcol +
                            ";opacity:0.5;text-align:" + (rdn ? "center" : "right") + ";'><span style='margin-right:10px;margin-left:10px;'>" + String.fromCharCode(f) + "</span></div>";
                }
            }


            if (ADM) {
                isAlias = ((a.f_parent_code).split("-")).length - 3;  // console.log(a.f_title+" | "+isAlias);

                if (isAlias) {

                    im += "<div " + SPA + "left:0;width:100%;bottom:" + (rdn ? (wtl / 2 - 12) : 8) + "px;font:16px FlaticonsStroke;color:" + inkcol +
                            ";opacity:0.5;text-align:left;'><span style='margin-left:6px;'>" + String.fromCharCode(58521) + "</span></div>";

                }
            }

            grad = "background-color:" + c1 + ";";
            grad += "background:-moz-linear-gradient(left, " + c1 + ", " + c0 + ");" +
                    "background:-webkit-linear-gradient(left, " + c1 + ", " + c0 + ");" +
                    "background:-o-linear-gradient(left, " + c1 + ", " + c0 + ");" +
                    "background:-ms-linear-gradient(left, " + c1 + ", " + c0 + ");" +
                    "background:linear-gradient(left, " + c1 + ", " + c0 + ");";

            bt = a.f_subtitle;
            nbt = bt.length;
            if (nbt) {
                if (bt.substring(0, 1) == "+") {
                    bt = bt.substring(1);
                    nbt--;
                }
                if (bt.charAt(nbt - 1) == "+")
                    tbt = "<br><span style='font:12px;'>" + LANG(bt.substring(0, nbt - 1)) + "</span>";
            }

            // tile
            s="<div id='idTL"+a.f_code+"' class='cltile' "+SPA+"left:"+(x+rdn*kr)+"px;top:"+(y+rdn*kr)+"px;width:"+(wtl-rdn*kr*2)+"px;height:"+(tl-rdn*kr*2)+"px;"+
                grad+";color:"+inkcol+";font:"+Fnt+"px opensansL;"+(rdn?"border-radius:"+(tl/2)+"px;":"")+"'>"+
                im+"<div "+SPA+"left:6px;top:6px;width:"+(wtl-12-rdn*kr*2)+"px;height:"+(tl-12-rdn*kr*2)+"px;"+(rdn?"text-align:center;":"")+"'>"+( LANG(a.f_title) + LANG(tbt) );

            // actions
            if(this.asset_actions.length > 0 && a.fc_documents != undefined){
                var spaceBetweenButtons = 7;
                var buttonWidth = buttonHeight = 24;
                var icon;
                var offset = 0;
                for(var q = 0; q < this.asset_actions.length; q++){
                    icon = '';
                    counter = '';
                    switch(this.asset_actions[q]){
                        
                        case 'docs': 
                            if(a.fc_documents != ''){
                                icon = String.fromCharCode(58376);
                                counter = a.fc_documents;
                            }
                            break;
                        
                        case 'info': 
                            icon = String.fromCharCode(58543);
                            break;
                        
                        case 'workorders':
                            if(a.fc_asset_wo != '' && (a.fc_asset_opened_wo > 0 || a.fc_asset_opened_pm > 0 || a.fc_asset_opened_on_condition > 0)){
                                icon = String.fromCharCode(58648);
                                counter = Number(a.fc_asset_opened_wo) + Number(a.fc_asset_opened_on_condition) + Number(a.fc_asset_opened_pm);
                            }
                            break;
                    }
                    if(icon != ''){
                        s += '<span class="tileAction" onclick="Wizard.f_asset_action(\'' + a.f_code + '\',\'' + this.asset_actions[q] + '\');" style="bottom:0px;background:' + colorTone(c1, 0.7, 0) + ';left:' + (offset * spaceBetweenButtons + buttonWidth * offset) + 'px">' + icon + ' <font style="font:10px FlaticonsStroke;">' + counter + '</font></span>';
                        offset++;
                    }
                    // add action to actions array
                    //TL_ACTIONS_xy.push({x: x + (i * spaceBetweenButtons + buttonWidth * i), y: y + 120 - spaceBetweenButtons - buttonHeight, i: i, w: buttonWidth, h:buttonWidth, f: a.f_code, a: actions[i]});
                }
            }

            s += '</div></div>';
            TLxy.push({x: x, y: y, i: i, wtl: wtl, f: a.f_code});  // x,y,w   i=index in aTile(children)   f=f_code
            
            /*
            c0 = "#ff0000";
            grad="background-color:"+c1+";";
            grad+="background:-moz-linear-gradient(left, "+c1+", "+c0+");"+
                "background:-webkit-linear-gradient(left, "+c1+", "+c0+");"+
                "background:-o-linear-gradient(left, "+c1+", "+c0+");"+
                "background:-ms-linear-gradient(left, "+c1+", "+c0+");"+
                "background:linear-gradient(left, "+c1+", "+c0+");";
            */

            
          
            return s;
        }
    },

    SetValueForm: function (locknext) {
        with (Wizard) {

            var r, ifrmboxj, a, k, tg, nm, aj, v, i, rk;

            ifrmboxj = jfr.contentWindow;
            frm = document;
            if (ifrmboxj)
                frm = ifrmboxj.document;

            a = getElClassName(frm, 'memfield');

            for (r = 0; r < a.length; r++) {
                aj = a[r];
                tg = aj.tagName.toLowerCase();
                k = aj.id;
                nm = aj.name;
                rk = refVal[k];

                //  alert(rk+"\nid: "+k)     

                if (typeof (VAL[rk]) == "undefined")
                    continue;
                v = VAL[rk];

                if (tg == "textarea" || tg == "input")
                    aj.value = v;
                if (tg == "checkbox")
                    aj.checked = v;
                if (tg == "select") {
                    for (i = 0; i < aj.options.length; i++)
                        if (aj.options[i].value == v) {
                            aj.options.selectedIndex = i;
                            break;
                        }
                }
            }

            if (!locknext)
                setTimeout(function () {
                    Wizard.noanim = 0;
                }, 500);
        }
    },

    f_butn: function () {
        with (Wizard) {

            var c, w, h, bc = "#666", ic = "#FFF",
                    bf, a, af, wbf, waf, s = "";

            c = f_ink(pdCOL);
            if (c == "#FFF")
                ic = "#000", bc = "#BBB";

            w = (WB - mg * 4) / 2, h = 34 - mg * 2;

            s = "<div class='cltile' " + SPA + "left:" + mg + "px;bottom:" + mg + "px;width:" + w + "px;height:" + h + "px;font:" + Fnt + "px opensansR;color:#FFF;cursor:pointer;" +
                    "background-color:#9FC24C;text-align:center;line-height:" + h + "px;' onclick='Wizard.nextForm(0)'>" + LANG("Back") + "</div>";

            s += "<div class='cltile' " + SPA + "left:" + (mg * 3 + w) + "px;bottom:" + mg + "px;width:" + w + "px;height:" + h + "px;font:" + Fnt + "px opensansR;color:#FFF;cursor:pointer;" +
                    "background-color:#9FC24C;text-align:center;line-height:" + h + "px;' onclick='Wizard.nextForm(1)'>" + LANG("Next") + "</div>";

            return s;
        }
    },

    nextForm: function (m) {
        with (Wizard) {
            if (noanim)
                return false;
            var frm, r, n, aj, tg, cl, ii, nm, vl, k, b, c = {}, ifrmboxj, zz = 0, bk;

            // next
            if (m) {  // VAL  
                ifrmboxj = jfr.contentWindow;
                frm = document;
                if (ifrmboxj)
                    frm = ifrmboxj.document;

                b = getValField(pTL, 1);
                bk = 0;
                if (b) {
                    a = getElClassName(frm, 'memfield');
                    n = a.length;

                    for (r = 0; r < n; r++) {
                        aj = a[r];
                        tg = aj.tagName.toLowerCase();
                        cl = aj.className;
                        ii = aj.id;
                        nm = aj.name;

                        if (tg == "textarea")
                            vl = aj.value;
                        if (tg == "select")
                            vl = aj.value == '0' || isNaN(aj.value) ? aj.value : parseInt(aj.value);
                        if (tg == "input") {
                            if (aj.getAttribute("type") == "checkbox") {
                                var NM = aj.name, vl = "";
                                if (NM) {
                                    var vj = frm.forms[0], rr, nn = parseInt(vj.elements[NM].length), arisp = [], ij;
                                    for (rr = 0; rr < nn; rr++) {
                                        ij = vj.elements[NM][rr];
                                        if (ij.checked)
                                            arisp.push(ij.value);
                                    }
                                    vl = arisp.join(",");
                                    tg = "multicheckbox";
                                } else {
                                    vl = (aj.checked) ? 1 : 0;
                                    tg = "checkbox";
                                }
                            } else
                                vl = aj.value;
                        }
                        if (cl.indexOf("mandatory") > -1 && !vl) {

                            WizADM.f_popAlert("Compile mandatory fields");
                            return;
                        }
                        if (ii)
                            c[ii] = vl;

                    } // fine for

                    // VAL
                    if (n) {
                        for (k in b) {
                            if (typeof (c[b[k]]) != "undefined")
                                VAL[k] = c[b[k]];
                            refVal[b[k]] = k;
                        }
                    }
                } // if f_field   
                zz = 1;
            } else {

                pth.pop();
                PDR = pth.pop();
                pth.push(PDR);
                noanim = 0;
                getCol(-2);
                if ((PDR + "").substring(0, 2) == "w_")
                    tempPDR = vWare[1];    // WizADM.getPthParent();  
                bk = 1;
            }

            f_set_VxTile(zz, bk);

            setParent(jfr, jbt);
            jful.style.display = "none";
            jbt.style.display = "none";
            jfr.src = "";
            MeD = 0;
            DrawTLs();
        }
    },

    f_set_VxTile: function (m, b) {    // m:   0=normal    1=next form 
        with (Wizard) {

            if (typeof (VxTile[PDR]) != "undefined" && !m) {

                if (pTL.f_srcbox && !b) {   //alert(PDR+" | "+pTL.f_srcbox+" | back: "+b)

                    // update solo i VAL del form
                    var r, k, ak = getFieldKey();
                    for (r = 0; r < ak.length; r++) {
                        k = ak[r];
                        VAL[k] = VxTile[PDR][k];
                    }

                } else
                    VAL = moGlb.cloneArray(VxTile[PDR]);

            } else
                VxTile[PDR] = moGlb.cloneArray(VAL);

        }
    },

    getFieldKey: function () {
        with (Wizard) {
            var b, k, ak = [], o = pTL;
            try {
                eval("b={" + o.f_field + "}");
                for (k in b) {
                    if (typeof (b[k]) == "string") {
                        s = b[k].substring(0, 3);
                        if (s == "id_" || s == "+id")
                            ak.push(k);
                    }
                }
            } catch (e) {
            }
            return ak;
        }
    },

    f_delete_back: function (t) {
        with (Wizard) {
            var i = RFD[t];
            if (!i) {
                if (typeof (VxTile[t]) != "undefined") {
                    //alert("back delete: "+t+"\n\nsrcbox: "+i) ;
                    delete(VxTile[t]);
                }
            }
        }
    },

//------------------------------------------------------------------------------
// events


    Evt: function (e) {
        with (Wizard) {


            if (noanim) {
                moEvtMng.cancelEvent(e);
                return false;
            }

            var ev = e.type, r, x, y, t, nx, ny, ex, ey, nt = 1, p, istch = (ev.substring(0, 5) == "touch") ? 1 : 0;
            if (istch)
                var tch = e.changedTouches, nt = tch.length;


            if (ev == "touchstart" || ev == "mousedown") {
                if (FD.length)
                    return false;
                if (istch) {
                    t = tch[0], ex = t.pageX, ey = t.pageY, i = t.identifier;
                    jmb.onmousedown = "";
                } else {
                    P = jsCoords(e), ex = P.x, ey = P.y, i = 0;
                    OM = document.onmousemove;
                    OU = document.mouseup;
                    document.onmousemove = function (e) {
                        Wizard.Evt(e)
                    }
                    document.onmouseup = function (e) {
                        Wizard.Evt(e)
                    }
                    jmb.ontouchstart = "";
                    jmb.ontouchmove = "";
                    jmb.ontouchend = "";
                }

                FD.push({x: ex, y: ey, ix: ex, iy: ey, f: i, sc: sct, mm: 0});
            }

            if (ev == "touchmove" || ev == "mousemove") {
                if (FD.length) {
                    for (r = 0; r < nt; r++) {
                        i = 0;
                        if (istch)
                            t = tch[r], i = t.identifier;
                        if (i != FD[0].f)
                            continue;
                        if (istch)
                            ex = t.pageX, ey = t.pageY;
                        else
                            P = jsCoords(e), ex = P.x, ey = P.y;

                        with (FD[0]) {
                            mm++;
                            if (sch < scH) {
                                sct = sc + (ey - iy);
                                if (sct < (sch - scH))
                                    sct = sch - scH;
                                if (sct > 0)
                                    sct = 0;
                                jbx.style.top = sct + "px";
                                Fscrbar();
                            }
                        }
                        break;
                    }
                }
            }

            if (ev == "touchend" || ev == "mouseup") {
                if (FD.length) {
                    with (FD[0]) {
                        if (mm < 5) {
                            clickTiles(ix, iy);
                        }
                    }                               // click su tiles o parent
                    FD = [];
                }
                if (!istch) {
                    if (!OM)
                        OM = "";
                    if (!OU)
                        OU = "";
                    document.onmousemove = OM;
                    document.onmouseup = OU;
                }
            }

            moEvtMng.cancelEvent(e);
        }
    },

    Wheel: function (d) {
        with (Wizard) {
            if (sch < scH) {
                sct += d * 16;
                if (sct < (sch - scH))
                    sct = sch - scH;
                if (sct > 0)
                    sct = 0;
                jbx.style.top = sct + "px";
                Fscrbar();
            }
        }
    },

    Fscrbar: function () {
        with (Wizard) {

            var tt = 0, hh = sch, k;
            if (sch < scH) {
                k = sch / scH;
                hh = k * sch;
                tt = k * -sct;
            }
            jsc.style.top = tt + "px";
            jsc.style.height = hh + "px";

        }
    },

    clickTiles: 
    function (ix, iy, xyp) {
        with (Wizard) {

            oPDR = PDR;
            var r, k, b, sr, l, t, w, h, c, dl, dt, dw, dh, ww, rr, ii = -1, wn, wst, delt,
                    n = TLxy.length, p = moGlb.getPosition(jwb),
                    nx = ix - p.l, ny = iy - p.t - tl - sct,
                    F = -1, bk = 0;

            MeD = 0;


            if (!xyp) {

                for (r = 0; r < n; r++)
                    with (TLxy[r]) {                                                  // x,y,wtl   i=index in aTile(children)   f=f_code
                        if (nx > x && nx < (x + wtl) && ny > y && ny < (y + tl)) {
                            F = f;
                            rr = r;
                            ii = i;
                            break;
                        }
                    }

            } else {

                F = xyp;
                rr = 0;
                ii = TLxy[0].i;

            }


            if (WizADM.EDIT > 0) {
                if (F < 0)
                    F = PDR;
                WizADM.f_newedit(F, ii);
                f_filterClear();
                return;
            }

            tempPDR = "";

            if (F == -1) {  // back

                if (ADM) {
                    jtew.innerHTML = "";
                    jtew.style.display = "none";
                }

                BACK = 1;
                if (pth.length > 1) {
                    PDR += "";
                    if (PDR.substring(0, 2) == "w_") {
                        vWare[0] = vWare[3];
                        tabid = 1;
                    }

                    delt = pth.pop();
                    f_delete_back(delt);  // remove VxTile value after

                    PDR = pth.pop();
                    pth.push(PDR);
                    bk = 1;

                } else
                    return;

            } else {  // tile
                
                // check if an action button has been clicked
                /*
                for (k = 0; k < TL_ACTIONS_xy.length; k++){              
                    if (nx > TL_ACTIONS_xy[k].x && nx < (TL_ACTIONS_xy[k].x + TL_ACTIONS_xy[k].w) && ny > TL_ACTIONS_xy[k].y && ny < (TL_ACTIONS_xy[k].y + TL_ACTIONS_xy[k].h)) {
                        f_asset_action(TL_ACTIONS_xy[k].f, TL_ACTIONS_xy[k].a);
                        return;
                    }
                }*/

                
                BACK = 0;

                if (F != "w_0") {  // if exit

                    PDR = F;
                    pth.push(F);
                    pTL = aTile[ii];   // selected tile properties

                    if (vWare[0]) {
                        wn = Dbl.query("SELECT " + table[1] + " count(*) WHERE f_parent_code like ('-" + pTL.f_code + "-') and f_visibility & " + userLEV);
                        if (!wn) {
                            vWare[0] = "";
                            tabid = 0;
                            tempPDR = vWare[1];
                        }

                    }
                } else {

                    // exit
                    vWare[0] = "";
                    tabid = 0;
                    tempPDR = vWare[1];
                }
            }

            f_filterClear();
            noanim = 1;

            if (!bk) {  // next

                with (TLxy[rr])
                    l = x, t = y + tl + sct, ww = wtl;

                c = aTile[ii].f_color;
                if (!c)
                    c = chCOL;
                MeD = (aTile[ii].f_srcbox) ? 1 : 0;
                getCol();

                // VAL  -> f_field (no id_) 
                b = getValField(aTile[ii], 0);
                if (b) {
                    for (k in b) {
                        if (typeof (b[k]) == "string")
                            sr = b[k].substring(0, 1);
                        else
                            sr = "";
                        if (sr == "+") {
                            sb = b[k].substring(1);
                            VAL[k] += sb;
                        }    //  +  add value
                        else
                            VAL[k] = b[k];
                    }
                }

                f_jab(l, t, ww, tl, c);

                dl = l / Stp, dt = t / Stp, dw = (WB - ww) / Stp, dh = (HB - tl) / Stp;
                f_anim(l, t, ww, tl, dl, dt, dw, dh, 0, bk);

            } else { // back

                getCol(-2);
                DrawTLs();

                f_jab(0, 0, WB, HB, c);
                dl = -((WB - tl) / 2) / Stp, dt = -((HB - tl) / 2) / Stp, dw = (tl - WB) / Stp, dh = (tl - HB) / Stp;

                f_anim(0, 0, WB, HB, dl, dt, dw, dh, 0, bk);
            }

            f_set_VxTile(0, bk);


            if (ii > -1)
                if (aTile[ii].f_ware_start) {
                    vWare[0] = aTile[ii].f_ware_start;
                    vWare[3] = vWare[0];
                    vWare[2] = pth[pth.length - 2];
                    vWare[1] = PDR;

                    WareStart = Dbl.GETVAR("WareStart");
                    if (!WareStart)
                        WareStart = "";
                    WareExit = (WareStart.substring(0, 1) == "+") ? 1 : 0;
                    if (WareStart.indexOf("@") != -1)
                        WareExit *= 2;

                    if (WareStart != vWare[0]) {
                        WareStart = vWare[0];
                        wst = WareStart;
                        WareExit = 0;
                        if (WareStart.substring(0, 1) == "+") {
                            WareExit = 1;
                            wst = WareStart.substring(1);
                        }
                        if (wst.indexOf("@") != -1) {
                            WareExit *= 2;
                            wst = (wst.split("@")).join("");
                        }
                        Dbl.SETVAR("WareStart", WareStart);

                        inLOADING = 1;
                        Ajax.sendAjaxPost(phpWares + NS, "f_ware_start=" + wst + "&pdr=" + PDR, "Wizard.fRetWares");

                    } else {

                        // update PDR in t_wizard_temp
                        var vpd = "'-" + PDR + "-'";

                        Dbl.query("UPDATE " + table[1] + " SET f_parent_code=" + vpd + " WHERE f_parent_code notlike '-w_'");

                        jab.style.display = "none";
                        DrawTLs();
                        if (!MeD)
                            noanim = 0;
                    }

                }


        }
    },

    getValField: function (o, m) {   // f_field: "f_title:'Oggetto pericolante', f_subtitle:'id_descr', f_name:'id_name'"
        with (Wizard) {                // m: 0=valfield   1=field

            var b, k, a = {}, s, p;

            p = o.f_code + "";
            if (p.substring(0, 2) == "w_" && p.substring(0, 3) != "w_0") {
                VAL["t_wares"] = p.substring(2); // t_wares   auto assignment
                VAL["t_initial"] = p.substring(2); // salvo il valore iniziale degli eventuali asset da associare al wo
            }

            if (!o.f_field)
                return "";

            try {
                eval("b={" + o.f_field + "}");
                for (k in b) {
                    if (typeof (b[k]) == "string")
                        s = b[k].substring(0, 3);
                    else
                        s = "";
                    v = 0;
                    if (s != "id_" && s != "+id")
                        v = 1;
                    if ((!m && v) || (m && !v))
                        a[k] = b[k];
                }
            } catch (e) {
                alert("WO assigned fields error!");
                return {};
            }
            return a;
        }
    },

    f_jab: function (l, t, w, h, c) {
        with (Wizard) {
            jab.style.left = l + "px";
            jab.style.top = t + "px";
            jab.style.width = w + "px";
            jab.style.height = h + "px";
            jab.style.backgroundColor = c;
            jab.style.display = "block";
        }
    },

    f_anim: function (l, t, w, h, dl, dt, dw, dh, s, bk) {
        with (Wizard) {

            s++;
            l -= dl, t -= dt, w += dw, h += dh;

            if (bk)
                moGlb.setOpacity(jab, 100 - s * 20);

            jab.style.left = l + "px";
            jab.style.top = t + "px";
            jab.style.width = w + "px";
            jab.style.height = h + "px";

            if (s >= Stp) {
                moGlb.setOpacity(jab, 100);
                if (inLOADING) {
                    f_waitLoading(1);
                    return;
                }
                jab.style.display = "none";
                DrawTLs();
                if (!MeD)
                    noanim = 0;

                // End wizard         
                if (!aTile.length) {
                    if (!ADM) {

                        try { // mobile
                            parent.mobileWO.f_call_actend(window, 1);
                            parent.mobileWO.f_confirm_create_wizard();
                            return;
                        } catch (e) {
                        }


                        try { // moglb     
                            parent.moGlb.confirm.show(("<br>" + LANG("Confirming the creation of the work order?")), function () {
                                window.Wizard.ActEnd();
                            });
                            return;
                        } catch (e) {
                        }

                    } else
                        ActEnd();
                }
                return;
            }
            setTimeout(function () {
                Wizard.f_anim(l, t, w, h, dl, dt, dw, dh, s, bk);
            }, 50);
        }
    },

    f_waitLoading: function (m) {

        try {

            if (m)
                parent.moGlb.loader.show();
            else
                parent.moGlb.loader.hide();

        } catch (e) {

            mf$("idloader").style.display = m ? "block" : "none";

        }
    },

    drawPth: function () {
        with (Wizard) {

            var s = "", a, n = pth.length, r, sep = "", pr, p, i, tg0 = "", tg1 = "", bt;

            Paths = [];
            for (r = 0; r < n; r++) {

                i = pth[r] + "";
                p = (i.substring(0, 2) == "w_") ? 1 : 0;
                a = Dbl.query("SELECT " + table[p] + " GET * WHERE f_code='" + i + "'");

                if (!a[0])
                    return "";
                pr = a[0].f_linktitle;
                if (!pr)
                    pr = getTitlePath(a[0].f_title, aPdr.f_subtitle.substring(aPdr.f_subtitle.indexOf("<<#")+3,aPdr.f_subtitle.indexOf("#>>")));           // linktitle || f_title + [ t_subtitle if + ]

                pr = pr.multiReplace({"<br>": " ", "</br>": " "});

                Paths.push({txt: pr});

                tg0 = "", tg1 = "";

                s += sep + tg0 + LANG(pr) + tg1;
                sep = " | ";
            }

            return s;
        }
    },

    f_getChildren: function (ftx) {
        with (Wizard) {
            var r, v, l, p = PDR, wn, at, az = [];

            if (vWare[0])
                tabid = 1;

            if (tempPDR)
                p = tempPDR;

            p += "";

            if (BACK && oPDR.substring(0, 2) == "w_")
                delete(VAL["t_wares"]);

            if (p.substring(0, 2) == "w_" && BACK && oPDR.substring(0, 2) != "w_") {
                wn = Dbl.query("SELECT " + table[1] + " count(*) WHERE f_parent_code like ('-" + pTL.f_code + "-') and f_visibility & " + userLEV);
                if (!wn) {
                    p = vWare[1];
                }
            }

            at = Dbl.query("SELECT " + table[tabid] + " GET * WHERE f_parent_code like '-" + p + "-' and f_visibility & " + userLEV + " ORDER BY f_order");


            // if insert tile exit in ware connection
            if (typeof (p) == "string") {
                if (tabid && (WareExit > 1 || (WareExit && p.substring(0, 2) == "w_"))) {
                    var dexit = {
                        f_code: "w_0",
                        f_parent_code: p,
                        f_title: LANG("General request"),
                        f_subtitle: "",
                        f_imgtile: "",
                        f_linktitle: "",
                        f_visibility: -1,
                        f_order: 0,
                        f_field: "",
                        f_color: "#C0C0C0",
                        f_tilewidth: 1
                    };
                    at.unshift(dexit);
                }
            }

            if (!ftx)
                return at;
            ftx = ftx.toLowerCase();
            for (r = 0; r < at.length; r++) {
                v = (at[r].f_title).toLowerCase();
                l = (at[r].f_subtitle).toLowerCase();
                if (v.indexOf(ftx) > -1) {
                    az.push(at[r]);
                    continue;
                }
                if (l.indexOf(ftx) > -1)
                    az.push(at[r]);
            }

            return az;
        }
    },

    freset: function (m) {
        with (Wizard) {

//if(!m) { if(!confirm("confirm reset")) return; }

            jex.style.display = "none";
            MeD = 0;
            noanim = 0;
            f_resetVars();
            WizADM.reStart();
        }
    },

    ActEnd: function () {
        with (Wizard) {

            var r, n, k, par = "", nowo = 0, t, tnow, a = [], err;

            f_default_par();

            for (k in VAL) {
                if (k == "exit") {
                    if (VAL[k] == "reservation")
                        nowo = 1;

                    /* ... other exits */
                }
            }


            if (ADM) {
                par = "<span style='font:bold 18px opensansL;color:#000;'>" + LANG("End of Wizard") + ":</span><br><br>" +
                        "<b>Parameters:</b><br>";

                for (k in VAL) {
                    par += k + "=" + VAL[k] + "<br>";
                }

                jtew.innerHTML = par;
                jtew.style.display = "block";
                if (!nowo)
                    return;
            }


            // no workorder
            if (nowo) {

                // 1 = reservation 
                if (nowo == 1) {
                    var KEY = "id_reservation";

                    // verify expire time of reservations in localstorage 
                    var l = localStorage.getItem(KEY);

                    //   alert("localstorage: "+l);

                    a = [], err = 1;
                    if (l) {
                        par = JSON.parse(l);
                        tnow = f_get_now();
                        for (r = 0; r < par.length; r++) {
                            t = parseInt(par[r].tms);
                            if (tnow > t) {
                                err = 1;
                                break;
                            }
                            a.push(parseInt(par[r].fcode));
                            err = 0;
                        }
                    }

                    localStorage.setItem(KEY, "[]");

                    if (!a.length) {                                                // se scadute delete
                        WizADM.f_popAlert("<br>Prenotazioni scadute!");
                        Ajax.sendAjaxPost("../calendar/reservation-delete", "param=" + JSON.stringify(a), "Wizard.RetEnd", 1);
                        return;
                    }

                    Ajax.sendAjaxPost("../calendar/reservation-end", "param=" + JSON.stringify(a), "Wizard.RetEnd", 1);       // reservation-end => OK
                }


                //...


                return;
            }



            if (Paths.length) {
                noanim = 1;

                var k, twa = 0, par = "", stri = "<input type='hidden' name='Ext' value=1 />", dfj = mf$("divfrm");

                if (MandatoryWare) {
                    for (k in VAL) {
                        if (k == "t_wares")
                            twa = 1;
                    }
                    if (!twa) {
                        WizADM.f_popAlert("Asset non inserito!");
                        noanim = 0;
                        return;
                    }
                }

                par = JSON.stringify(VAL);

                // verify mobile online
                try {
                    if (!parent.OnOffline.STATUS) { // put in localstorage Request to Create 
                        var acr, nacr, pwo;
                        acr = parent.Dbl.GETVAR("cache_request");
                        nacr = acr.length;
                        pwo = parent.MaxOffWoWiz;

                        if (nacr >= pwo) {
                            setTimeout("WizADM.f_popAlert('You have exceeded the maximum number of requests created offline!')", 1200);
                            return;
                        }

                        acr.push(par);
                        parent.Dbl.SETVAR("cache_request", acr);

                        RetEnd("OK");
                        return;
                    }
                } catch (e) {
                }


                Ajax.sendAjaxPost(phpSend, "params=" + par, "Wizard.RetEnd", 1);

                Wizard.EndSended = 0;
                Wizard.TimeEnd(0);
            }

        }
    },

    f_default_par: function () {
        with (Wizard) {
            var r, p = [];
            for (r = 0; r < Paths.length; r++)
                p.push(Paths[r].txt);

            if (typeof (VAL["f_title"]) == "undefined")
                VAL["f_title"] = LANG("Request from wizard");
            if (typeof (VAL["f_description"]) == "undefined")
                VAL["f_description"] = p.join(" -> ");

            return;
        }
    },

    TimeEnd: function (t) {
        with (Wizard) {

            if (EndSended)
                return;
            t++;
            if (t > 250) {
                if (!ADM) {
                    WizADM.f_popAlert("Trasmission data timeout");
                    Start();
                    return;
                }
            }

            setTimeout(function () {
                Wizard.TimeEnd(t);
            }, 100);
        }
    },

    RetEnd: function (v) {
        with (Wizard) {

            var s, ajv;
            if (v != "OK") {      // {"message":"Error: ... "}

                try {
                    ajv = JSON.parse(v);
                    s = ajv["message"];
                } catch (e) {
                    try {
                        mes = JSON.parse(v.substring(0, v.length - 2));
                        s = mes["message"];
                    } catch (f) {
                        s = LANG("Error sending request!") + " " + v;
                    }
                    
                }

                WizADM.f_popAlert(s);
            }

            EndSended = 1;
            f_resetVars();
            jex.innerHTML = "<div class='cltile' " + SPA + "left:" + mg + "px;top:" + (HB - tl - mg) + "px;width:" + tl * 3 + "px;height:" + tl + "px;" +
                    "background-color:#AAA;color:000;font:" + Fnt + "px opensansL;'>" +
                    "<div " + SPA + "left:6px;top:6px;width:" + (tl * 3 - 12) + "px;height:" + (tl - 12) + "px;cursor:pointer;' onclick='Wizard.freset(1)'>" + "<H3><b>" + LANG("Your request has been sent") + ".<br><b>" + LANG("Click here to create a new one") + ".</b></H3>" + "</div>" +
                    "</div>";
            REStart = 1;
            jex.style.display = "block";
        }
    },

    f_resetVars: function () {
        with (Wizard) {
            Paths = [];
            aTile = [], aPdr = {}, pTL = {}, tempPDR = "", BACK = 0, oPDR = "", WareExit = 0, TLxy = [],
                    vWare = ["", 0, 0, ""], VAL = {}, refVal = {}, VxTile = {}, pth = [];
            tabid = 0;
        }
    },

// functions

    getTitlePath: function (t, st) { // title, subtitle 
        with (Wizard) {
            t = LANG(t);
            if (st) {
                if (st.substring(0, 1) == "+")
                    st = st.substring(1);
                var n = st.length;
                if (n > 1) {
                    if (st.charAt(n - 1) == "+")
                        t += " - " + LANG(st.substring(0, n - 1));
                    else
                        t += " - " + LANG(st);
                }
            }

            return t;
        }
    },

    getWareTitle: function (c) {
        with (Wizard) {

            var a, s = "", v = VAL["t_wares"];
            if (!v)
                return "";

            a = Dbl.query("SELECT t_wizard_temp GET f_title,f_subtitle WHERE f_code='w_" + v + "'");
            if (a.length)
                s = getTitlePath(a[0].f_title, a[0].f_subtitle);

            return s;
        }
    },

    f_ink: function (c) {
        with (Wizard) {
            var s = 0, v, bn = "#FFF";

            v = colorTodec(c);
            s = (v[0] + v[1] + v[2]) / 3 * 0.9;

            if (s > 128)
                bn = "#000";
            return bn;
        }
    },

    jsCoords: function (e) {
        e = e || window.event;
        if (e.pageX || e.pageY)
            return {x: e.pageX, y: e.pageY};
        return {x: e.clientX + document.body.scrollLeft - document.body.clientLeft,
            y: e.clientY + document.body.scrollTop - document.body.clientTop};
    },

// da #000 o #000000  to  rgb o rgba
    colorTodec: function (c) {
        var n = c.length;
        d = (n > 4) ? 2 : 1, dd = 0, v = "", a = [];
        for (var r = 1; r < n; r++) {
            v += c.charAt(r);
            dd++;
            if (dd == d) {
                a.push(parseInt((d > 1) ? v : v + v, 16));
                v = "";
                dd = 0;
            }
        }

        return a;
    },

// da #000 o #000000  to  rfb o rgba
    colorToRGBa: function (c, a) {
        var A = "a";
        if (typeof (a) == u)
            A = "";
        var n = c.length;
        d = (n > 4) ? 2 : 1, dd = 0, v = "", s = "", rgb = "rgb" + A + "(";
        for (var r = 1; r < n; r++) {
            v += c.charAt(r);
            dd++;
            if (dd == d) {
                rgb += s + (parseInt((d > 1) ? v : v + v, 16));
                v = "";
                dd = 0;
                s = ",";
            }
        }
        if (A)
            A = "," + a;
        return rgb + A + ")";
    },

// c = colore in formato #000   o   #000000
// t = tonalità    0 <= t < 1
// m = formato  0=#000000  1=rgb(00,00,00)
    colorTone: function (c, t, m, a) {
        if (t < 0 || t >= 1)
            return c;
        if (!m)
            m = 0;
        else
            m = 1;
        var r, n = c.length, d = (n > 4) ? 2 : 1, dd = 0, s = [], v = "", v0, v1;
        for (r = 1; r < n; r++) {
            v += c.charAt(r);
            dd++;
            if (dd == d) {
                v0 = parseInt((d > 1) ? v : (v + v), 16);
                v1 = parseInt(v0 + ((255 - v0) * (1 - t)));
                if (!m)
                    v1 = v1.toString(16);
                if (v1.length < 2)
                    v1 = "0" + v1;
                s.push(v1);
                v = "";
                dd = 0;
            }
        }
        if (m) {
            v0 = "rgb" + (a ? "a" : "") + "(" + s.join(",") + (a ? ("," + a) : "") + ")";
        } else
            v0 = "#" + s.join("");
        return v0;
    },

    getElClassName: function (j, cln) {
        if (document.getElementsByClassName)
            return j.getElementsByClassName(cln);
        var a = [], re = new RegExp('\\b' + cln + '\\b'),
                el = j.getElementsByTagName("*");
        for (var r = 0; r < el.length; r++)
            if (re.test(el[r].className))
                a.push(el[r]);
        return a;
    },

    f_get_now: function () {
        var dtnow = new Date();
        var Tnow = parseInt((dtnow.getTime()) / 1000);
        return Tnow;
    },

    setCrossWareWo: function (arrayCodes) {
        // aggiungo a VAL[t_wares] gli fcode di asset da crossare col wo
        with(Wizard) {
            var t = new Array();
            if (!isNaN(VAL['t_initial']) && parseInt(Number(VAL['t_initial'])) == VAL['t_initial'] && !isNaN(parseInt(VAL['t_initial'], 10))) { // se è intero
                t.push(VAL['t_initial']);
            }
            arrayCodes.forEach(function(e) {
                if (!isNaN(e) && parseInt(Number(e)) == e && !isNaN(parseInt(e, 10))) { // se è intero
                    t.push(e);
                }
            });
            VAL["t_wares"] = t.join();
            //console.log(VAL["t_wares"]);
        }
        
    },
    
    //**************************************
    // asset action
    //**************************************

    f_asset_action:function(asset_code, action){
        
        document.getElementById('id_asset_action').innerHTML= "<div class='asset_action_loader_container'><div class='asset_action_loader'></div></div>";
        document.getElementById('idbox').style.display = 'none';
        document.getElementById('idmbox').style.display = 'none';
        document.getElementById('id_asset_action').style.display = 'block';

        // add wo icons
        if(action == 'workorders'){
            typeIcons = parent.moLibrary.category_cod;
        }
        else{
            typeIcons = '';
        }

        // recupero template action
        Ajax.sendAjaxPost(staticUrl+"wizard/asset-action-template", "asset_code=" + asset_code + "&action=" + action + '&type_icons=' + typeIcons + '&div_height=' + document.getElementById('id_asset_action').clientHeight, "Wizard.f_asset_action_template"); 
    },

    f_asset_action_template:function(resp){
        document.getElementById('id_asset_action').innerHTML = resp;
    },

    f_asset_action_close_template:function(resp){
       document.getElementById('idbox').style.display = 'block';
       document.getElementById('idmbox').style.display = 'block';
       document.getElementById('id_asset_action').style.display = 'none';
    },

    f_asset_action_accordion:function(el, type){
        el.classList.toggle("active");
        var panel = el.nextElementSibling;

        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            if(type == 'doc'){
                var sessionID = el.getAttribute('sessionID');
                var typeDoc = el.getAttribute('type');
                if(typeDoc == 'xlsx'){
                    //document.getElementById(sessionID).src = 'https://docs.google.com/gview?embedded=true&url=' + 'http://' + document.location.host + "/document/get-file?session_id=" + sessionID;
                    var req = new XMLHttpRequest();
                        req.open("GET", 'http://' + document.location.host + "/document/get-file?session_id=" + sessionID, true);
                        req.responseType = "arraybuffer";

                        req.onload = function(e) {
                            var data = new Uint8Array(req.response);
                            var workbook = XLSX.read(data, {type:"array"});
                            var json = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
                            var grid = canvasDatagrid();
                            grid.data = json;
                            grid.style.fontSize = '8px';
                            var iframe = document.getElementById(sessionID);
                            iframe.contentDocument.body.appendChild(grid);

                        }
                        req.send();
                }
                else if(typeDoc == 'docx' || typeDoc == 'doc'){
                    var req = new XMLHttpRequest();
                    req.open("GET", 'http://' + document.location.host + "/document/get-file?session_id=" + sessionID, true);
                    req.responseType = "arraybuffer";
                    req.onload = function(e) {
                        mammoth.convertToHtml({arrayBuffer: req.response})
                        .then(function(result){
                            var html = result.value; // The generated HTML
                            var iframe = document.getElementById(sessionID);
                            iframe.contentDocument.body.innerHTML = html;
                        })
                        .done();
                    }
                    req.send();
                }
                else{
                    document.getElementById(sessionID).src = 'http://' + document.location.host + "/document/get-file?session_id=" + sessionID;
                }
            }
            panel.style.display = "block";

        }
    }


} // end      





LANG = function (txt) {
    var t = "";
    try {
        t = parent.moGlb.langTranslate(txt);
        return t;
    } catch (e) {
    }
    try {
        t = parent.LANG(txt);
        return t;
    } catch (e) {
    }
    return txt;
}


// mf$("debug").innerHTML=nx+", "+ny+" | "+x+", "+y;
