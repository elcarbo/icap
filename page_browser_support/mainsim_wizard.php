<?php
date_default_timezone_set('Europe/Paris');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

include APPLICATION_PATH.'/../library/Zend/Auth.php';
if(!Zend_Auth::getInstance()->hasIdentity()) {
    header("Location:../");
    die;
}


?>
<!DOCTYPE>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <title>mainsim wizard</title>
<style>

body {
        overflow:hidden; margin:0;      
}

.clhead {
  position:absolute;left:0;top:0;width:100%;height:100%;
  background:url("../public/msim_images/default/_theme/header_background.gif") 0 0 repeat-x;

}

a {  font:11px Arial; text-decoration:none; cursor:pointer; color:#000; }

a:hover { text-decoration:underline; color:#000; }


</style>  
<script>  
  
function init(){

    var CNTj=document.getElementById("idCNT"), HH=CNTj.offsetHeight, 
        j=document.getElementById("idfrm");
    
    j.style.height=(HH-50)+"px";
    j.src="../wizard/wizard.php?calljax=wizard_test.php";
}


  
</script>
  </head>
  <body onload="init()">

<div class="clhead" id='idCNT'>
<img src="../public/msim_images/default/_theme/logo_mainsim.gif" />
<img src='../public/msim_images/default/thumbnails/man.gif' style='position:absolute;left:100%;top:5px;margin-left:-46px;' />
<a href='../login/logout?ns=1' style='position:absolute;left:100%;top:28px;margin-left:-88px;' >Logout</a>

<iframe id='idfrm' src='' style='position:absolute;left:0;top:50px;width:100%;height:100px;' frameborder="0"></iframe>
</div>
  </body>
</html>