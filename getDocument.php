<?php
defined('BASE_PATH_LIB')
    || define('BASE_PATH_LIB', realpath(dirname(__FILE__) . '/'));
ini_set("display_errors","On");

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/./application'));
error_reporting(E_ALL);

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

/*
 * File che richiamato da crontab, 
 * avvia i file previsti
 */

date_default_timezone_set('Europe/Paris');
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(BASE_PATH_LIB.'/library'),
    get_include_path(),
)));
include BASE_PATH_LIB.'/library/Zend/Config/Ini.php';
include BASE_PATH_LIB.'/library/Zend/Db.php';

$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
if($config->multiDatabase == "On") {
    include APPLICATION_PATH.'/modules/default/models/Utilities.php';
    $host = $_SERVER['HTTP_HOST'];
    $php_self = '';
    if($_SERVER['HTTP_HOST'] == 'test.mainsim.com'){
        $expRU = explode('/',$_SERVER['REQUEST_URI']);    
        $php_self = '/'.$expRU[1];            
    }
    $host.=$php_self;      
    $resDb = Mainsim_Model_Utilities::getInstallationInfo($config->database,$host,'f_url');    
    if(empty($resDb)) {
        throw new Exception("Page not found");
    }
    $dbConfig = $resDb['database'];
}
else {
    $dbConfig = $config->database;
}
$db = Zend_Db::factory($dbConfig);

$query = "SELECT * FROM t_attachments WHERE ";

if(isset($_GET['session_id'])) {
    $query.="f_session_id = '".$_GET['session_id']."'";
}
else {
    $query.="f_code = '".$_GET['f_code']."'";
}
$size = isset($_GET["size"]) ? $_GET["size"] : "doc";
$query.=" AND f_type = '".$size."'";

$res = $db->query($query)->fetch();
if(empty($res)) {
    die('No file Found');
}

$content = $res['f_path'];
$filename = $res['f_file_name'].'.'.$res['f_file_ext'];
header('Content-Type: '.$res['f_mime']);
header('Content-disposition: filename="'.$filename.'"');

echo file_get_contents($content);
die;
?>
