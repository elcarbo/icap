<?php

require('mc_table.php');

class PDF_Time_Sheet extends PDF_MC_Table {

    public $tls;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4') {
        parent::__construct($orientation, $unit, $format);
        $this->tls = new Mainsim_Model_Translate();
    }

    function set_params($company_info, $size) {

        $this->logo_path = $company_info["logo"];

        $pdf_sizes = array(
            "A0" =>
            array("SHORT" => 841, "LONG" => 1189),
            "A1" =>
            array("SHORT" => 594, "LONG" => 841),
            "A2" =>
            array("SHORT" => 420, "LONG" => 594),
            "A3" =>
            array("SHORT" => 297, "LONG" => 420),
            "A4" =>
            array("SHORT" => 210, "LONG" => 297),
            "A5" =>
            array("SHORT" => 148, "LONG" => 210),
        );

        $this->shortSide = $pdf_sizes[$size]['SHORT'];
        $this->longSide = $pdf_sizes[$size]['LONG'];
    }

    function Header() {
        $this->SetTextColor($this->R[0], $this->G[0], $this->B[0]);
        //if (!($this->first_page && $this->PageNo() == 1) && $this->blank_page == false) {
        $header_y_Start = 17;
        $nh = $this->header_height;
        $ch = $nh / 2;
        $headw = $this->col_w;

        if ($this->yearly) {
            $this->SetBatch(false, 1);
            $start = 1;
            $end = $this->weeks_in_current_year + 1;
            $this->Bookmark($this->tls->_("Weeks from") . " " . $start . $this->tls->_("to") . " " . ($end - 1), 1, 0);
        } else {


            $start = 1;
            $end = $this->day_in_current_month + 1;
            $this->SetBatch(1);
        }



        $this->SetFont('Arial', 'B', 8.2);
        $title1 = ($this->yearly) ? $this->tls->_("Yearly time sheet") . "  " . $this->current_year : $this->tls->_("Timesheet of") . " " . $this->tls->_(ucfirst(strtolower($this->current_month_text))) . " " . $this->current_year;
        $title2 = "               " . $this->tls->_("from") . " " . $start . " " . $this->tls->_("to") . " " . ($end - 1);
        $this->SetFillColor(245, 245, 245);
        $this->rect(4, $header_y_Start + 5 + $ch, 91, $nh, "F");
        $this->rect(4, $header_y_Start + $ch, 91, $nh, "F");
        $this->setxy(4, 6.5);
        $this->SetFont('Arial', 'B', 14);
        $this->cell(78, 5, $title1, 0, 0, "L");
        $this->SetFont('Arial', '');
        $this->cell(75, 5, $title2, 0, 0, "L");
        $this->SetFont('Arial', 'B', 8);
        $this->SetTextColor(0, 0, 0);
        $this->setXY(4, $header_y_Start + $ch);
        $this->Cell(9, $nh, "Id", 1, 0, "C");
        $this->Cell(82, $nh, $this->tls->_("Title"), 1, 0, "C");
        $this->SetFont('Arial', 'B', 7);
        $this->setXY(4, $header_y_Start + 5 + $ch);
        $this->Cell(58, $nh, $this->tls->_("Asset"), 1, 0, "C");
        $this->Cell(20, $nh, $this->tls->_("Frequency"), 1, 0);
        $this->Cell(13, $nh, $this->tls->_("Priority", false), 1, 0);

        $this->setx($this->time_sheet_start_x);
        $this->rect($this->x, $this->y, $headw * ($end - $start), $nh, "F");


        for ($n = $start; $n < $end; $n++) {
            $this->SetFont('Arial', 'B', 7);
            $week_range = $this->week_range($n);
            $this->setfillcolor($this->R[5], $this->G[5], $this->B[5]);
            if (in_array(date("N", mktime(null, null, null, $this->current_month, $n, $this->current_year)), array(6, 7)) && (!$this->yearly)) {
                $this->rect($this->time_sheet_start_x + ($n - $start) * $headw, $this->y, $headw, $nh, "F");
            }
            $label = ($this->yearly) ? $this->tls->_(" Week") : $this->tls->_(date("D", mktime(null, null, null, $this->current_month, $n, $this->current_year)));
            $this->cell($headw, $nh, $label . " " . $n, 1, 0, "C");

            if ($this->yearly) {
                $this->setxy($this->x - $headw, $this->y - $nh);
                $this->SetFillColor(245, 245, 245);
                $this->SetFont('Arial', 'B', 6);
                $this->rect($this->x, $this->y, $headw, $nh, "F");
                $this->cell($headw, $nh, $week_range[0] . "-" . $week_range[1], 1, 0, "C");
                $this->setxy($this->x, $this->y + $nh);
            }
        }
        $y_end = $this->y;

        $legenda = array($this->tls->_("Planned"), $this->tls->_("Not Performed"), $this->tls->_("Performed on planned date"), $this->tls->_("Performed before the planned date"), $this->tls->_("Performed with delay"));
        //Legenda
        $this->SetTextColor($this->R[0], $this->G[0], $this->B[0]);
        $this->setxy($this->longSide - 195, 6.5);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(26, 5, $this->tls->_("Color Map"), 0, 0, "L");
        $this->SetFont('Arial', '', 6);
        $this->SetFillColor($this->R[0], $this->G[0], $this->B[0]);
        $this->rect($this->x, $this->y, 4.6, 4.6, "F");
        $this->setx($this->x + 6);
        $this->Cell(15, 5, $this->tls->_($legenda[0]), 0, 0, "L");
        $this->SetFillColor($this->R[2], $this->G[2], $this->B[2]);
        $this->rect($this->x, $this->y, 4.6, 4.6, "F");
        $this->setx($this->x + 6);
        $this->Cell(22, 5, $this->tls->_($legenda[1]), 0, 0, "L");
        $this->SetFillColor($this->R[1], $this->G[1], $this->B[1]);
        $this->rect($this->x, $this->y, 4.6, 4.6, "F");
        $this->setx($this->x + 6);
        $this->Cell(34, 5, $this->tls->_($legenda[2]), 0, 0, "L");
        $this->SetFillColor($this->R[4], $this->G[4], $this->B[4]);
        $this->rect($this->x, $this->y, 4.6, 4.6, "F");
        $this->setx($this->x + 6);
        $this->Cell(36, 5, $this->tls->_($legenda[3]), 0, 0, "L");
        $this->SetFillColor($this->R[3], $this->G[3], $this->B[3]);
        $this->rect($this->x, $this->y, 4.6, 4.6, "F");
        $this->setx($this->x + 6);
        $this->Cell(34, 5, $this->tls->_($legenda[4]), 0, 0, "L");
        /* } else if ($this->blank_page == false) {
          $this->Bookmark('Title');
          $this->setxy(4, 170);
          $this->SetFont('Arial', 'B', 28);
          $title = ($this->yearly) ? $this->tls->_("Yearly time sheet") : $this->tls->_("Monthly time sheet");
          $this->Cell(0, 8, $title, 0, 0, "C");
          $this->image($this->logo_path, $this->longSide / 2 - (80.6 / 2), $this->shortSide / 2 - (11), 80.6, 22);
          $delta_line = 10;
          } else {

          } */

        $this->setfillcolor($this->R[0], $this->G[0], $this->B[0]);
        $this->rect(4, 15 - $delta_line, $this->longSide - 8, 1.6, "F");
        //$this->y=$y_end+$nh+1;
        $this->y = 35;
    }

    function PDF_setting($first_page = true, $index = true, $days_limit = 31, $weeks_limit = 53) {

        $this->first_page = $first_page;
        $this->index = $index;
        $this->blank_page = false;

        $this->header_height = 5;
        $this->body_height = 4.7;

        $this->col_w = ($this->yearly == true) ? ($this->longSide - 107) / $weeks_limit : ($this->longSide - 107) / $days_limit;
        $this->time_sheet_start_x = (($this->yearly == true) ? 97.5 : 98.4);
        $this->days_limit = $days_limit;
        $this->weeks_limit = $weeks_limit;
        $this->_set_default_colors();
        $this->PDF_set = true;
        return true;
    }

    function Set_Blank_Page($blank = true) {
        $this->blank_page = $blank;
    }

    function Time_Sheet_Setting($month = 1, $year = 2015, $yearly = false) {

//Set the array of column height
        $n_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $this->current_month_text = date("F", mktime(null, null, null, $month));
        $this->day_in_current_month = $n_day;
        $this->current_month = $month;
        $this->current_year = $year;
        $this->yearly = $yearly;
        $this->bkm = true;
        $dt = new DateTime('December 28th, ' . $year);
        $this->weeks_in_current_year = $dt->format('W'); # 53/52
        $check_pdf_set = isset($this->PDF_set) ? "" : $this->PDF_setting();
        return $n_day;
    }

    function Footer() {

        //if (!($this->first_page && $this->PageNo() == 1)) {
        $this->SetFont('Arial', '', 7);
        $auth = Zend_Auth::getInstance()->getIdentity();
        $this->SetXY(4, -6.5);
        $this->Cell(170, 5, 'mainsim - what maintenance can be - www.mainsim.com', 0, 0, 'L', "", "www.mainsim.com");
        $this->Cell(0, 5, $this->tls->_('Page') . ' ' . $this->PageNo() . " - " . $this->tls->_("Printed by") . "  " . $auth->fc_usr_firstname . " " . $auth->fc_usr_lastname . " - " . date("F j, Y", time()), 0, 0, 'R');
        //}
        $this->setfillcolor($this->R[0], $this->G[0], $this->B[0]);
        $this->rect(4, $this->shortSide - 8, $this->longSide - 8, 1.6, "F");
    }

    function _set_default_colors() {
        $index = 0;
//Blue 0
        $this->R[$index] = 74;
        $this->G[$index] = 102;
        $this->B[$index] = 113;

        $this->R[$index + 100] = 109;
        $this->G[$index + 100] = 146;
        $this->B[$index + 100] = 162;
        $index++;

//Green 1
        $this->R[$index] = 161;
        $this->G[$index] = 194;
        $this->B[$index] = 77;
        $index++;

//Red 2
        $this->R[$index] = 198;
        $this->G[$index] = 18;
        $this->B[$index] = 1;

        $this->R[$index + 100] = 200;
        $this->G[$index + 100] = 72;
        $this->B[$index + 100] = 60;
        $index++;
//Orange 3
        $this->R[$index] = 250;
        $this->G[$index] = 160;
        $this->B[$index] = 4;
        $index++;

//Ardesia 4
        $this->R[$index] = 0;
        $this->G[$index] = 102;
        $this->B[$index] = 102;
        $index++;

//Sunday grey 5 sunday/saturday
        $this->R[$index] = 215;
        $this->G[$index] = 215;
        $this->B[$index] = 215;
        $index++;

        $this->R["default"] = 220;
        $this->G["default"] = 220;
        $this->B["default"] = 220;

        $this->R["header_green"] = 102;
        $this->G["header_green"] = 204;
        $this->B["header_green"] = 0;
    }

    function setBatch($d = 1, $t = false) {

        //funzione ormai inutile -- 7 ottobre 2016

        $this->decade = $d;
        $this->trimestre = isset($t) ? $t : false;
    }

    function week_range($week) {
        $week = (strlen($week) == 1) ? "0" . $week : $week;
        $start = strtotime($this->current_year . "W" . $week . "1");
        $end = strtotime($this->current_year . "W" . $week . "7");
        //var_dump(array($this->TmpToDM($start), $this->TmpToDM($end), $start, $end,$start,$end + 86399)); die();
        //Data di inizio e data di fine della n-ssima settimana, sia in testo che in timestamp
        return array($this->TmpToDM($start), $this->TmpToDM($end), $start, $end + 86399);
    }

    function Row_ts($start = 0, $data, $style = 'D', $height = 0, $link = "") {
        $def_style = $style;
//Calculate the height of the row
        $nb = 0;
        for ($i = $start; $i < $data[2] + $start - 1; $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i - $start], $data[0][$i]));
        if ($height == 0)
            $h = 5 * $nb;
        else
            $h = $height;
//Issue a page break first if needed
        $this->CheckPageBreak($h);
//Draw the cells of the row

        for ($i = $start; $i < count($data[0]) + $start; $i++) {
            $w = $this->widths[$i - $start];
            $a = isset($this->aligns[$i - $start]) ? $this->aligns[$i - $start] : 'L';
//Save the current position

            $x = $this->GetX();
            $y = $this->GetY();
//Draw the border
            $style = $def_style;
            $this->setfillcolor($this->R["default"], $this->G["default"], $this->B["default"]);

            if (in_array(date("N", mktime(null, null, null, $this->current_month, $i, $this->current_year)), array(6, 7)) && ($this->trimestre == FALSE)) {

                $this->setfillcolor($this->R[5], $this->G[5], $this->B[5]);
                $style = "FD";
            }

            if (in_array($data[1][$i], array(0, 1, 2, 3, 4))) {
                if (isset($this->R[$data[1][$i] + 100]))
                    $this->setfillcolor($this->R[$data[1][$i] + 100], $this->G[$data[1][$i] + 100], $this->B[$data[1][$i] + 100]);
                else
                    $this->setfillcolor($this->R[$data[1][$i]], $this->G[$data[1][$i]], $this->B[$data[1][$i]]);
                $style = "FD";
            }
            $this->Rect($x, $y, $w, $h, $style);

            if ($i == 0 AND $link) {
                $this->Link($x, $y, $w, $h, $link);
            }
//Print the text
            $h_line = ($this->GetStringWidth($data[0][$i]) > ($w - 1.2)) ? 4 : $h;
            
            $this->MultiCell($w, $h_line, $data[0][$i], 0, $a);


//Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
//Go to the next line
        $this->Ln($h);
    }

    function SetHeight($alt) {
//Set the array of column height
        $this->height = $alt;
    }

    function TmpToDM($timestamp) {
        return date('Y', $timestamp) != 1970 ? date('d/m', $timestamp) : '';
    }

    function TmpToTmo24($timestamp) {
        return mktime(0, 0, 0, date("n", $timestamp), date("j", $timestamp), date("Y", $timestamp));
    }

    function Translate_sequence($sequence, $pm_frequency = "") {

        $dictionary = array(
            "MENSILE" => "1M",
            "BIMESTRALE" => "2M",
            "TRIMESTRALE" => "3M",
            "QUADRIMESTRALE" => "4M",
            "SEMESTRALE" => "6M",
            "OTTO MESI" => "8M",
            "ANNUALE" => "12M",
            "SETTIMANALE" => "7G",
            "BISETTIMANALE" => "15G",
            "6 ANNI" => "6AN",
            "5 ANNI" => "5AN",
            "4 ANNI" => "4AN",
            "2 ANNI" => "2AN",
            "MONTHLY" => "1M",
            "4 MONTHS" => "4M",
            "12 MONTHS" => "12M",
            "12 MONTH" => "12M",
            "24 MONTHS" => "24M",
            "24 MONTH" => "24M",
            "WEEKLY" => "7G",
            "BIWEEKLY" => "15G",
            "ANNUAL" => "12M",
            "2 ANNI" => "24M",
            "BIMESTRAL" => "2M",
            "TRIMESTRAL" => "3M",
            "QUATERLY" => "3M",
            "HALF" => "6M",
            "HALF-YEARLY" => "6M",
            "EVERY 2 DAYS" => "2DAYS",
            "EVERY 3 DAYS" => "3DAYS",
            "EVERY 4 DAYS" => "4DAYS",
            "EVERY 5 DAYS" => "5DAYS",
            "EVERY 6 DAYS" => "6DAYS",
        );
        
        $output = !empty($sequence) ? "[" . $this->tls->_($dictionary[trim(strtoupper($sequence))]) . "]" : "[" . $this->tls->_($dictionary[trim(strtoupper($pm_frequency))]) . "]";
        $output = $output == "[]" ? "" : $output;
        return $output;
    }

    function getTaskSequence($select, $f_code) {



        $select->reset();
        $res_task = $select->from(array("t1" => "t_creation_date"), array())
                        ->join(array("t2" => "t_ware_wo"), "t2.f_ware_id=t1.f_id", array())
                        ->join(array("t3" => "t_wares"), "t3.f_code=t1.f_id", array('fc_task_sequence'))
                        ->where("t1.f_category='TASK'")->where("f_phase_id=1")
                        ->where("t2.f_wo_id=" . $f_code)
                        ->query()->fetchAll();

        //Zend_Debug::dump($res_task);die;

        if ($res_task) {

            foreach ($res_task as $task) {
                $array_task[] = $task['fc_task_sequence'];
            }
            $array_task = array_unique($array_task);
            rsort($array_task);

            return $array_task;
        }
    }

    function getFrequencyDescription($select, $array_task, $row,$max_tmp, $pm_multi = false) {

        if (!$pm_multi) {
            //echo "<br> COUNTER  ";            var_dump($row['f_counter']);
            if ($row['next_date'] <= $row['f_start_date']) {

                $select->reset();
                $select->from(array("t1" => "t_periodics"), array("num_periodiche" => "count(f_id)"))
                        ->where("f_code =(?)", $row['f_code'])
                        ->where('f_asset_code is null')
                        ->where("f_start_date <= (?)", $row['f_start_date'])
                        ->where("f_start_date > (?)", $row['next_date'])
                        ->where("t1.f_timestamp =?", $max_tmp);

                /* if ($asset_code) {
                  $select->where('f_asset_code =' . $asset_code);
                  } */
                $res_periodics = $select->query()->fetch();
                //echo "<br> periodiche - executed= 0  "; var_dump($res_periodics["num_periodiche"]);
                if ($res_periodics) {

                    $wo_sequence = $row['f_counter'] + ($res_periodics["num_periodiche"]);
                }
            } else {
                $select->reset();
                $select->from(array("t1" => "t_periodics"), array("num_periodiche" => "count(f_id)"))
                        ->where("f_code =(?)", $row['f_code'])
                        ->where('f_asset_code is null')
                        ->where("f_start_date >= (?)", $row['f_start_date'])
                        ->where("f_start_date <= (?)", $row['next_date'])
                        ->where("t1.f_timestamp =?", $max_tmp);
                /* if ($asset_code) {
                  $select->where('f_asset_code =' . $asset_code);
                  } */
                $res_periodics = $select->query()->fetch();
                //echo "<br> periodiche - executed= 1  ";                var_dump($res_periodics["num_periodiche"]);
                if ($res_periodics) {

                    $wo_sequence = ($row['f_counter'] - $res_periodics["num_periodiche"]);
                }
            }

            //echo "<br> Sequence  ";            var_dump($wo_sequence);
            $actual_max_task_sequence = 1;

            for ($i = 0; $i < count($array_task); $i++) {


                if (($wo_sequence % $array_task[$i] ) == 0) {
                    $actual_max_task_sequence = $array_task[$i];
                    break;
                }
            }
            // echo "<br> Array task  ";            var_dump($array_task);
            //echo "<br> Max Seq  ";            var_dump($actual_max_task_sequence);
            $name_fr = '';
            
            
            $map_week_empty = array_combine(range(0,36), array_fill(0, 37, ''));
            // map_week è un array chiave => valore che mappa il numero di settimane con la frequenza
            
            $map_week = array(1=>'Settimanale',2=>'Bisettimanale',4=>'Mensile',8=>'Bimestrale',12=>'Trimestrale',16=>'Quadrimestrale'
                ,24=>'Semestrale',32=>'Otto mesi',48=>'Annuale',96=>'24 MONTHS',288 => '6 anni');
            
            $map_week = array_replace($map_week_empty, $map_week);
            
            //moltiplicatore settimane
            $moltiplicatore = 1;
            if(strpos($row['fc_pm_frequency'], 'MONTHLY') !== FALSE) $moltiplicatore = 4;
            else if (strpos($row['fc_pm_frequency'], 'WEEKLY') !== FALSE) $moltiplicatore = 1;
            else if (strpos($row['fc_pm_frequency'], 'BIWEEKLY') !== FALSE) $moltiplicatore = 2;
            else if (strpos($row['fc_pm_frequency'], 'ANNUAL') !== FALSE || strpos($row['fc_pm_frequency'], '12 MONTH') !== FALSE) $moltiplicatore = 48;
            else if (strpos($row['fc_pm_frequency'], 'BIMESTRAL') !== FALSE) $moltiplicatore = 8;
            else if (strpos($row['fc_pm_frequency'], 'TRIMESTRAL') !== FALSE || strpos($row['fc_pm_frequency'], 'QUATERLY') !== FALSE ) $moltiplicatore = 12;
            else if (strpos($row['fc_pm_frequency'], '4 MONTH') !== FALSE) $moltiplicatore = 16;
            else if (strpos($row['fc_pm_frequency'], 'HALF') !== FALSE) $moltiplicatore = 24;


            $tot_week = $moltiplicatore*$actual_max_task_sequence;            
            $name_fr = $map_week[$tot_week];
            
//            if (strpos($row['fc_pm_frequency'], 'MONTHLY') !== FALSE) {
//                if ($actual_max_task_sequence == 1)
//                    $name_fr = 'Mensile';
//                else if ($actual_max_task_sequence == 2)
//                    $name_fr = 'Bimestrale';
//                else if ($actual_max_task_sequence == 3)
//                    $name_fr = 'Trimestrale';
//                else if ($actual_max_task_sequence == 4)
//                    $name_fr = 'Quadrimestrale';
//                else if ($actual_max_task_sequence == 6)
//                    $name_fr = 'Semestrale';
//                else if ($actual_max_task_sequence == 8)
//                    $name_fr = 'Otto mesi';
//                else if ($actual_max_task_sequence == 12)
//                    $name_fr = 'Annuale';
//            }
//            else if (strpos($row['fc_pm_frequency'], 'WEEKLY') !== FALSE) {
//                if ($actual_max_task_sequence == 1)
//                    $name_fr = 'Settimanale';
//                else if ($actual_max_task_sequence == 2)
//                    $name_fr = 'Bisettimanale';
//            }
//            else if (strpos($row['fc_pm_frequency'], 'BIWEEKLY') !== FALSE) {
//                if ($actual_max_task_sequence == 1)
//                    $name_fr = 'Bisettimanale';
//            }
//            else if (strpos($row['fc_pm_frequency'], 'ANNUAL') !== FALSE || strpos($row['fc_pm_frequency'], '12 MONTH') !== FALSE) {
//                if ($actual_max_task_sequence == 1)
//                    $name_fr = 'Annuale';
//                else if ($actual_max_task_sequence == 2)
//                    $name_fr = '24 MONTHS';
//            }
//            else if (strpos($row['fc_pm_frequency'], 'BIMESTRAL') !== FALSE) {
//                if ($actual_max_task_sequence == 1)
//                    $name_fr = 'Bimestrale';
//            }
//            else if (strpos($row['fc_pm_frequency'], 'TRIMESTRAL') !== FALSE || strpos($row['fc_pm_frequency'], 'QUATERLY') !== FALSE ) {
//                if($actual_max_task_sequence == 1)
//                    $name_fr = 'Trimestrale';
//                else if($actual_max_task_sequence == 2)
//                    $name_fr = 'Semestrale';
//                else if($actual_max_task_sequence == 3)
//                    $name_fr = 'Nove mesi';
//                else if($actual_max_task_sequence == 4)
//                    $name_fr = 'Annuale';
//            }
//            else if (strpos($row['fc_pm_frequency'], '4 MONTH') !== FALSE) {
//                if ($actual_max_task_sequence == 1)
//                    $name_fr = 'Quadrimestrale';
//            }
//            else if (strpos($row['fc_pm_frequency'], 'HALF') !== FALSE) {
//                if ($actual_max_task_sequence == 1)
//                    $name_fr = 'Semestrale';
//                else if ($actual_max_task_sequence == 2)
//                    $name_fr = 'Annuale';
//                else if ($actual_max_task_sequence == 12)
//                    $name_fr = '6 anni';
//            }

            return $name_fr;
        } else
            return "";
    }

    function GetPeriodicDates_monthly($select, $f_code, $month = 1, $n_day = 31, $n_weeks = 52, $year = 2015, $yearly = false, $pm_multi = false, $asset_code = "") {



        if ($yearly) {
            $n_columns = $n_weeks;
        } else {
            $n_columns = $n_day;
        }


        for ($index = 1; $index <= $n_columns; $index++) {
            $date_range = $this->week_range($index);
            $square[$index] = "";
            $check[$index] = 100;
            
            $select->reset();
            $res_max_tmp=$select->from(array("t_periodics"), array("MAX(f_timestamp) AS MAX_TMP"))
                    ->where("t_periodics.f_code =?", $f_code)->query()->fetch();
             $max_tmp=isset($res_max_tmp["MAX_TMP"])?$res_max_tmp["MAX_TMP"]:0;
            $select->reset();
            $select->from(array("t_periodics"), array("f_code", "f_start_date", "f_end_date", "f_executed", "f_forewarning", "f_generated_code"))
                    ->join(array("t2" => "t_workorders"), "t2.f_code=t_periodics.f_code", array("fc_pm_frequency", "f_counter", "next_date" => "fc_pm_next_due_date"))
                    ->where("t_periodics.f_code =?", $f_code)
                    ->where("t_periodics.f_timestamp =?", $max_tmp);
            if ($pm_multi) {
                $select->where('f_asset_code =' . $asset_code);
            } else {

                $select->where('f_asset_code is NULL');
            }

            if ($this->yearly)
                $select->where('t_periodics.f_start_date >=' . $date_range[2])->where('t_periodics.f_start_date <=' . $date_range[3]);
            else {
                $day_start = mktime(0, 0, 0, $month, $index, $year);
                $day_end = mktime(23, 59, 59, $month, $index, $year);
                $select->where("t_periodics.f_start_date >=" . $day_start)->where("t_periodics.f_start_date <=" . $day_end);
            }
            $select->order("t_periodics.f_start_date ASC");
            $res = $select->query()->fetchall();
            $array_task = $this->getTaskSequence($select, $f_code);
            //echo "RES<br>";var_dump($res);
            //questo $res contiene tutte le righe di t_periodics per quel generatore, comprese nell'intervallo di tempo del cronoprogramma 
            foreach ($res as $vl) {


                if (count($array_task) > 0) {

                    $name_fr = $this->getFrequencyDescription($select, $array_task, $vl,$max_tmp, $pm_multi);

                    //var_dump($name_fr);
                    $square[$index] = $this->Translate_sequence($name_fr);

                    //var_dump($square[$index]);
                } else {
                    $sequence = $this->Translate_sequence("", $vl['fc_pm_frequency']);
                    $square[$index] = $sequence;
                }

                //se è stato generato un wo oppure se si tratta di WO prima della data di inserimento della PM
                if ($vl['f_executed'] == 1) {

                    //var_dump($array_task);die;
                    //se è stato generato un WO (
                    if ($vl['f_generated_code']) {
                        
                         
                        
                        $select->reset();
                        $res_executed = $select->from(array("t_workorders"), array("f_code", "fc_wo_starting_date", "fc_wo_ending_date", "f_start_date", "f_end_date", 'f_code_periodic', "fc_pm_frequency"))
                                        ->join(array("t1" => "t_creation_date"), "t1.f_id=t_workorders.f_code", array("f_creation_date", "fc_progress"))
                                        //->join(array("t2" => "t_custom_fields"), "t2.f_code=t_workorders.f_code", array("sequence" => "name_frequency"))
                                        ->where("t1.f_id =?", $vl['f_generated_code'])
                                        ->order('fc_wo_ending_date asc')
                                        ->query()->fetch();
                        
                        $square[$index].=" #WO".$res_executed['fc_progress'];
                        //Zend_Debug::dump($res_executed);
                        //se sono compilare sia la data pianificata di fine che la data effettiva di fine faccio un controllo sulle date
                        if (isset($vl["f_end_date"]) && isset($res_executed['fc_wo_ending_date'])) {

                            $difference_in_hour = floor((($res_executed['fc_wo_ending_date'] - $vl["f_end_date"]) / 3600) * 2) / 2;
                            $difference_in_day = floor(($this->TmpToTmo24($res_executed['fc_wo_ending_date']) - $this->TmpToTmo24($vl["f_end_date"])) / 86400);
                            $days = (abs($difference_in_day) >= 2) ? "days" : "day";
                            $sign = ($difference_in_hour >= 0) ? "+" : "";

                            //se la data effettiva di fine è minore della data pianificata di fine e
                            if ($this->TmpToTmo24($vl['f_start_date']) <= $this->TmpToTmo24($res_executed['fc_wo_ending_date']) && $this->TmpToTmo24($res_executed['fc_wo_ending_date']) <= $this->TmpToTmo24($vl["f_end_date"])) {
                                $check[$index] = 1; //green
                            } else if ($this->TmpToTmo24($res_executed['fc_wo_ending_date']) <= $this->TmpToTmo24($vl["f_end_date"])) {
                                $square[$index] .=" " . $this->TmpToDM($res_executed['fc_wo_ending_date']) . "\n" . $sign . $difference_in_day . " " . $days;
                                $check[$index] = 4; //ardesia
                                //eseguito in anticipo
                            } else {

                                $square[$index] .=" " . $this->TmpToDM($res_executed['fc_wo_ending_date']) . "\n" . $sign . $difference_in_day . " " . $days;
                                $check[$index] = 3; //orange
                            }
                        } else if (isset($res_executed['fc_wo_ending_date'])) {

                            if ($this->TmpToTmo24($vl['f_start_date']) == $this->TmpToTmo24($res_executed['fc_wo_ending_date'])) {
                                $check[$index] = 1; //green
                            } else {
                                $difference_in_day = floor(($this->TmpToTmo24($res_executed['fc_wo_ending_date']) - $this->TmpToTmo24($vl['f_start_date'])) / 86400);
                                $days = (abs($difference_in_day) >= 2) ? "days" : "day";
                                $sign = ($difference_in_hour >= 0) ? "" : "-";
                                $square[$index] .=" " . $this->TmpToDM($res_executed['fc_wo_ending_date']) . "\n" . $sign . $difference_in_day . " " . $days;
                                $check[$index] = 1; //green
                            }
                        } else if ($vl['f_start_date'] < time()) {
                            $check[$index] = 2; //red
                        } else {
                            $check[$index] = 0; //blue
                        }
                    } else {
                        $check[$index] = 1;
                    } //green}
                } else {


                    if ($vl['f_start_date'] < time()) {
                        $check[$index] = 1; //Green
                    } else {

                        $check[$index] = 0; //blue
                    }
                }
            }
        }

        //var_dump($square);
        //var_dump($check);
        return array($square, $check, $n_columns);
    }

    function monthly_skimmed_wo($select, $codes, $year, $month, $days_in_current_month) {

        $select->reset();
        $select->from(array("t_periodics"), array("f_code", "f_start_date"))
                ->where("f_code in (?)", $codes);

        if ($month == 0 || $this->yearly) {
            $day_start = mktime(0, 0, 0, 1, 1, $year);
            $day_end = mktime(23, 59, 59, 12, 31, $year);
        } else {
            $day_start = mktime(0, 0, 0, $month, 1, $year);
            $day_end = mktime(23, 59, 59, $month, $days_in_current_month, $year);
        }
        $select->where("f_start_date >=" . $day_start . " AND f_start_date <=" . $day_end);

        $select->order("f_start_date ASC");
        $res = $select->query()->fetchall();

        foreach ($res as $line) {
            $new_codes[$line["f_code"]] = $line["f_code"];
        }

        return $new_codes;
    }

    function translate_slc($vl, $translations, $values = array(0, 1)) {

        for ($i = 0; $i < count($translations); $i++) {

            $value = isset($values[$i]) ? $values[$i] : $i;
            $converted_value = ($vl === $value) ? $translations[$i] : "";
        }
        return $converted_value;
    }

}

?>