<?php

require('library/fpdf/FPDI-FPDF/fpdi.php');

class mainsim_PDF extends FPDI { /* public $height */

    var $doc_FontStyle;   //Stile del font, salvato temporaneamente durante le operazioni di write
    var $doc_FontSizePt;   //Size del font, salvato temporaneamente durante le operazioni di write
    var $temp_x;            //posizione prima delle operazioni di write
    var $temp_t;            //posizione prima delle operazioni di write

    function TmpToUE($timestamp) {
        return date('Y', $timestamp) != 1970 ? date('d.m.Y', $timestamp) : '';
    }

    function prepareWrite($x_start, $y_start, &$text, &$fontSizePt = -1, &$fontStyle = nul) {

        /* CONFIGURIAMO LA POSIZIONE DI PARTENZA x,y */
        $this->temp_x = $this->x;
        $this->temp_y = $this->y;
        $this->setXY($x_start, $y_start);

        /* CONFIGURIAMO CARATTERISTICHE DEL FONT */

        //$fontFamily=$fontFamily<1?$this->FontFamily:$fontFamily;
        $this->doc_FontStyle = $this->FontStyle;
        $fontStyle = $fontStyle == null ? $this->doc_FontStyle : $fontStyle;
        $this->doc_FontSizePt = $this->FontSizePt;
        $fontSizePt = $fontSizePt < 1 ? $this->doc_FontSizePt : $fontSizePt;
        $this->setFont($this->FontFamily, $fontStyle, $fontSizePt);

        $text = ((str_replace("  ", " ", str_replace(["\r", "\n"], ['', ''], $text))));
        $text = (str_replace('|||', "\n", $text));
    }

    private function endWrite() {

        $this->setFont($this->FontFamily, $this->doc_FontStyle, $this->doc_FontSizePt);
        $this->setXY($this->temp_x, $this->temp_y);
    }

    function msmim_Write_FitScale($x_start, $y_start, $w, $h, $text, $border = false, $link = "", $align = "", $fill = false, $fontSizePt = -1, $fontStyle = null) {
        $this->msim_Write_FitScale($x_start, $y_start, $w, $h, $text, $border, $link, $align, $fill, $fontSizePt, $fontStyle);
    }

    function msim_Write_FitScale($x_start, $y_start, $w, $h, $text, $border = false, $link = "", $align = "", $fill = false, $fontSizePt = -1, $fontStyle = null) {

        $this->prepareWrite($x_start, $y_start, $text, $fontSizePt, $fontStyle);

        //Get string width
        $str_width = $this->GetStringWidth($text);

        //Calculate ratio to fit cell
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $ratio = ($w - $this->cMargin * 2) / max($str_width, 1);

        $fit = ($ratio < 1);
        if ($fit) {

            //Calculate horizontal scaling
            $horiz_scale = $ratio * 100.0;
            //Set horizontal scaling
            $this->_out(sprintf('BT %.2F Tz ET', $horiz_scale));

            //Override user alignment (since text will fill up cell)
            $align = '';
        }

        //Pass on to Cell method
        $this->Cell($w, $h, $text, $border, $ln, $align, $fill, $link);

        //Reset character spacing/horizontal scaling
        if ($fit)
            $this->_out('BT ' . ( '100 Tz' ) . ' ET');

        $this->endWrite();
    }

    ///msimWrite scrive il testo dato come se fosse una normale funzione di CELL
    function msim_Write_MultiLine($x_start, $y_start, $w, $h, $text, $border = false, $fontStyle = -1, $fontSizePt = -1) {

        $this->prepareWrite($x_start, $y_start, $text, $fontSizePt, $fontStyle);


        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;

        $nl = $this->NbLines($w, $text);
        $maxNl = $h / ($fontSizePt / $this->k + 0.5);
//var_dump($nl);
//var_dump($maxNl);echo'<br>';echo'<br>';
        while ($nl > $maxNl) {
            $fontSizePt = $fontSizePt * 0.95;
            $this->setFont($this->FontFamily, $fontStyle, $fontSizePt);
            $maxNl = ($h - 1) / ($fontSizePt / $this->k);
            $nl = $this->NbLines($w, $text);
//var_dump($nl);
//var_dump($maxNl);echo'<br>';
        }
//         die;

        $this->MultiCell($w, $h / $nl, $text, $border);

        $this->endWrite();
    }

    function drawSquare($side, $check = false, $x = null, $y = null) {
        $x = null ? $this->x : $x;
        $y = null ? $this->y : $y;
        $this->rect($x, $y, $side, $side);
        if ($check) {
            $this->ticSquare($side, $x, $y);
        }
    }

    function ticSquare($side, $x = null, $y = null) {
        $x = null ? $this->x : $x;
        $y = null ? $this->y : $y;

        $delta_side = $side * 0.05;
        $scaled_side = $side + $delta_side;

        $this->line($x - ($delta_side), $y - ($delta_side), $scaled_side + $x, $scaled_side + $y);
        $this->line($scaled_side + $x, $y - $delta_side, $x - $delta_side, $scaled_side + $y);
    }

    function table($x, $y, $table) {


        $this->setXY($x, $y);
        // loop rows
        for ($i = 0; $i < count($table); $i++) {

            // loop columns
            for ($j = 0; $j < count($table[$i]); $j++) {

                // cell data
                $cellData = $table[$i][$j];
                if (isset($cellData['size'])) {
                    $this->SetFontSize($cellData['size']);
                }

                // create sub table
                if (isset($cellData['table'])) {
                    $x1 = $this->GetX();
                    $y1 = $this->GetY();
                    $this->subTable($x1, $y1, $cellData['table']);
                    // reposition cursor at the end of the cell
                    if ($j < count($table[$i]) - 1) {
                        $widthSubTable = 0;
                        for ($z = 0; $z < count($cellData['table'][0]); $z++) { //[0]['w']);die();
                            $widthSubTable += $cellData['table'][0][$z]['w'];
                        }
                        $this->setXY($x1 + $widthSubTable, $y1);
                    }
                } else if (isset($cellData['checkbox'])) {
                    $this->CheckBox($cellData['w'], $cellData['h'], $cellData['text']);
                } else {
                    if (isset($cellData['bg'])) {
                        $prevBg = $this->FillColor;
                        $this->SetFillColor($cellData['bg'][0], $cellData['bg'][1], $cellData['bg'][2]);
                        $this->Cell($cellData['w'], $cellData['h'], $cellData['text'], $cellData['b'], '', $cellData['p'], true);
                        $this->FillColor = $prevBg;
                    } else {
                        $this->Cell($cellData['w'], $cellData['h'], $cellData['text'], $cellData['b'], '', $cellData['p']);
                    }
                }

                if (isset($cellData['size'])) {
                    $this->SetFontSize($this->fontSize);
                }
            }
            $this->Ln();
            $this->setX($x);
        }
    }

    function CheckBox($cellData) {

        // draw cell
        for ($i = 0; $i <= strlen($cellData['b']); $i++) {
            $char = substr($cellData['b'], $i, 1);
            switch ($char) {
                case 'L':
                    $this->Line($this->GetX(), $this->GetY(), $this->GetX(), $this->GetY() + $cellData['h']);
                    break;
                case 'R':
                    $this->Line($this->GetX() + $cellData['w'], $this->GetY(), $this->GetX() + $cellData['w'], $this->GetY() + $cellData['h']);
                    break;
                case 'T':
                    $this->Line($this->GetX(), $this->GetY(), $this->GetX() + $cellData['w'], $this->GetY());
                    break;
                case 'B':
                    $this->Line($this->GetX(), $this->GetY() + $cellData['h'], $this->GetX() + $cellData['w'], $this->GetY() + $cellData['h']);
                    break;
            }
        }
        //$this->Rect($this->GetX(), $this->GetY(), $cellData['w'], $cellData['h']);


        $side = 5;
        // get top left checkbox 
        $cbX = ($this->GetX() + ($cellData['w'] / 3 / 2)) - ($side / 2);
        $cbY = ($this->GetY() + ($cellData['h'] / 2)) - ($side / 2);
        $this->Rect($cbX, $cbY, $side, $side);
        if ($cellData['checkbox'] === true) {
            $this->Line($cbX, $cbY, $cbX + $side, $cbY + $side);
            $this->Line($cbX + $side, $cbY, $cbX, $cbY + $side);
        }

        // draw text
        $this->SetX($this->GetX() + $cellData['w'] / 3);
        $this->Cell($cellData['w'] / 3 * 2, $cellData['h'], $cellData['text'], '', '', $cellData['p']);
    }

    function subTable($x, $y, $table) {
        $this->setXY($x, $y);
        //$this->setDrawColor(255,0,0);
        //$this->setTextColor(255,0,0);
        // loop rows
        for ($i = 0; $i < count($table); $i++) {

            // loop columns
            for ($j = 0; $j < count($table[$i]); $j++) {

                // cell data
                $cellData = $table[$i][$j];

                if (isset($cellData['size'])) {
                    $this->SetFontSize($cellData['size']);
                }

                if (isset($cellData['checkbox'])) {
                    $this->CheckBox($cellData, $parentBorder);
                } else {
                    if (isset($cellData['bg'])) {
                        $prevBg = $this->FillColor;
                        $this->SetFillColor($cellData['bg'][0], $cellData['bg'][1], $cellData['bg'][2]);
                        $this->Cell($cellData['w'], $cellData['h'], $cellData['text'], $cellData['b'], '', $cellData['p'], true);
                        $this->FillColor = $prevBg;
                    } else {
                        $this->Cell($cellData['w'], $cellData['h'], $cellData['text'], $cellData['b'], '', $cellData['p']);
                    }
                }

                if (isset($cellData['size'])) {
                    $this->SetFontSize($this->fontSize);
                }
            }

            if ($i < count($table) - 1) {
                $this->Ln();
            }
            $this->setX($x);
        }
    }

    function msim_LoadTemplate($attach_params, $db, $pStart = 1, $nPage = 'ALL', $orientation = "P") {

        $doc = $this->msim_GetTemplate($attach_params, $db);


        $pageCount = $this->setSourceFile($doc['path']);

        $page_start = $pStart > 0 ? min($pStart, $pageCount) : 1;
        $pages = ($nPage == 'ALL' || !($nPage > 0)) ? ($pageCount - $page_start + 1) : min($nPage, ($pageCount - $page_start + 1));

        for ($i = 0; $i < $pages; $i++) {
            $tplIdx = $this->importPage($i + $page_start);
            $this->addPage($orientation);
            $this->useTemplate($tplIdx);
        }

        //$this->page = $current_page;
        return $i - $page_start + 1;
    }

    function msim_GetTemplate($attach_params, $db) {
        $params = explode("|", $attach_params);

        /* "77910|fc_doc_attach|t_wares" --> $params[0]=f_code, $params[1]=bind , $params[2]=table */
        if (count($params) != 3)
            return "CONTROLLARE I PARAMETRI DI CARICAMENTO DEL TEMPLATE";

        $keys = ['f_code', 'bind_attach', 'table'];
        $params = array_combine($keys, $params);

        $select = new Zend_Db_Select($db);
        $select->from($params['table'], array(trim($params['bind_attach'])));

        if (!in_array($params['table'], ['t_creation_date, t_pair_cross']))
            $res_template = $select->where("f_code = (?)", $params['f_code'])->query()->fetch();
        else
            $res_template = $select->where("f_id = (?)", $params['f_code'])->query()->fetch();

        $select->reset();
        if ($res_template) {
            $msim_doc = new Mainsim_Model_Document();
            $session_arr = (explode("|", $res_template[$params['bind_attach']]));
            $session = $session_arr[0];

            $doc = $msim_doc->getFile("f_session_id", $session, "doc");
            $content = $doc['f_path'];
            $filename = $doc['f_file_name'] . '.' . $doc['f_file_ext'];

            if (!file_exists($content)) {
                echo "<br><br><h2> NON E' STATO POSSIBILE RITROVARE IL DOCUMENTO ALLEGATO. <br>CONTROLLARE CHE IL DOCUMENTO RICERCATO SIA STATO CARICATO IN ANAGRAFICA<br>CODICE ERRORE: NF_PATH";
                die;
            }
            return(["path" => $doc['f_path'], "file_name" => $filename]);
        } else {
            echo "<br><br><h2> NON E' STATO POSSIBILE RITROVARE IL DOCUMENTO ALLEGATO. <br><br>CONTROLLARE I PARAMETRI DI ATTACH<br><br>CODICE ERRORE: NF_PARAM_ATTACH";
            die;
        }

        $tplIdx = $pdf->importPage(1);
        $pdf->addPage('L');
        $pdf->useTemplate($tplIdx, 10, 10, 280);
        $pdf->ln(150);
    }

    function msim_getData($f_code, $data_type = 'MAIN') {

        $db = Zend_Db::factory(Zend_Registry::get('db'));
        $trsl = new Mainsim_Model_Translate(null, $this->db);
        try {
            $select = new Zend_Db_Select($db);
            $select->reset();
            $res_type = $select->from('t_creation_date', array("f_type", "f_category"))
                            ->where("f_id = ?", $f_code)->query()->fetch();

            if (!$res_type)
                throw new Exception($trsl->_("F_CODE NON EXISTS") . (". CODICE ERRORE: Q01"));


            $type_table = "t_" . $res_type['f_type'];

            if (strpos($data_type, 'MAIN') !== FALSE) {
                $select->reset();
                $select->from(["tcd" => 't_creation_date'])
                        ->join(["twp" => "t_wf_phases"], 'tcd.f_wf_id=twp.f_wf_id AND twp.f_number=tcd.f_phase_id')
                        ->join(["tcf" => 't_custom_fields'], 'tcd.f_id=tcf.f_code')
                        ->join(["type_table" => $type_table], 'tcd.f_id=type_table.f_code');

                if ($res_type['f_category'] == 'USER')
                    $select->join(["tus" => 't_users'], 'tcd.f_id=tus.f_code');

                $res_data['MAIN'] = $select->where("tcd.f_id = ?", $f_code)->query()->fetch();
            }
            if (strpos($data_type, 'CROSS_MASTER') !== FALSE) {

                $cross_tables = ["WORKORDERS" =>
                        ["t_ware_wo" =>
                            ["self" => 'f_wo_id',
                            "cross" => 'f_ware_id',
                            "cross_table" => "t_wares"],
                        "t_wo_relations" =>
                            ["self" => 'f_code_wo_master',
                            "cross" => 'f_code_wo_slave',
                            "cross_table" => "t_workorders"],
                        "t_selector_wo" =>
                            ["self" => 'f_wo_id',
                            "cross" => 'f_selector_id',
                            "cross_table" => "t_selectors"]],
                    "WARES" =>
                        ["t_ware_wo" =>
                            ["self" => 'f_ware_id',
                            "cross" => 'f_wo_id',
                            "cross_table" => "t_workorders"],
                        "t_wares_relations" =>
                            ["self" => 'f_code_ware_slave',
                            "cross" => 'f_code_ware_slave',
                            "cross_table" => "t_wares"],
                        "t_selector_ware" =>
                            ["self" => 'f_ware_id',
                            "cross" => 'f_selector_id',
                            "cross_table" => "t_selectors"],],
                    "SELECTORS" =>
                        ["t_selector_ware" =>
                            ["self" => 'f_selector_id',
                            "cross" => 'f_ware_id',
                            "cross_table" => "t_wares"],
                        "t_selector_wo" =>
                            ["self" => 'f_selector_id',
                            "cross" => 'f_wo_id',
                            "cross_table" => "t_workorders"]]];

                $select->reset();
                $select->from(['crossed_creation' => 't_creation_date'])->join(['tcf' => 't_custom_fields'], 'tcf.f_code=crossed_creation.f_id');

                foreach ($cross_tables[$res_type['f_type']] as $table => $field) {

                    $res_cross[$table] = $select->join($table, $table . "." . $field['cross'] . '=crossed_creation.f_id')
                                    ->join([$field['cross_table']], $field['cross_table'] . '.f_code=' . $table . "." . $field['cross'])
                                    ->where($table . "." . $field['self'] . "=" . $f_code)->query()->fetchAll();
                }

                foreach ($res_cross as $table => $cross) {

                    foreach ($cross as $line_cross) {
                        $crossed[$line_cross['f_type']][$line_cross['f_category']] = $line_cross;
                    }
                }
                $res_data["CROSS_MASTER"] = $crossed;
            }
            if (strpos($data_type, 'PAIR_MASTER') !== FALSE) {
                $select->reset();
                $res_data['PAIR_MASTER'] = $select->from(["tpc" => "t_pair_cross"])
                                ->joinleft(["tcd" => "t_creation_date"], "tpc.f_code_cross=tcd.f_id", ["crossed_title" => "f_title", 'f_category', 'f_phase_id', 'f_creation_date'])
                                ->where("tpc.f_code_main = ?", $f_code)->query()->fetchAll();
            }

            return $res_data;
        } catch (Exception $e) {
            $error = $trsl->_("Error during query") . ": " . $e->getMessage();
            return $error;
        }
    }

}
