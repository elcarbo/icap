-- Users
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES (1, 'WARES', 'USER', 1, 1435241555, 1, 9, 1, 'Mainsim Administrator', '', -1, -1,1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES (2, 'WARES', 'USER', 2, 1435241555, 1, 9, 1, 'Mainsim Configurator', '', -1, -1,2);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES (3, 'WARES', 'USER', 3, 1435241555, 1, 9, 1, 'Mainsim Support', '', -1, -1,3);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES (4, 'WARES', 'USER', 4, 1435241555, 1, 9, 1, 'Mainsim Help Desk', '', -1, -1,4);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES (5, 'WARES', 'USER', 5, 1435241555, 1, 9, 1, 'Mainsim Quality Assurance', '', -1, -1,5);

INSERT INTO t_wares ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES ( 1, 16, 1435241555, 1, 1, 1); 
INSERT INTO t_wares ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES ( 2, 16, 1435241555, 2, 1, 1);
INSERT INTO t_wares ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES ( 3, 16, 1435241555, 3, 1, 1);
INSERT INTO t_wares ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES ( 4, 16, 1435241555, 4, 1, 1);
INSERT INTO t_wares ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES ( 5, 16, 1435241555, 5, 1, 1);

INSERT INTO t_wares_parent ( f_code, f_parent_code, f_timestamp, f_active) VALUES
( 1, 0, 1435241555, 1),
( 2, 0, 1435241555, 1),
( 3, 0, 1435241555, 1),
( 4, 0, 1435241555, 1),
( 5, 0, 1435241555, 1); 

INSERT INTO t_custom_fields ( f_code, f_timestamp) VALUES
( 1, 1435241555),
( 2, 1435241555),
( 3, 1435241555),
( 4, 1435241555),
( 5, 1435241555);

INSERT INTO t_users( f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES( 1, 'admin', '8a4dea98f8dac77d342f4cc785547e72b', 'Mainsim', 'Administrator', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');
INSERT INTO t_users( f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES( 2, 'config', 'd13bc37d8f70a315b04a20d6a9518b22a', 'Mainsim', 'Configurator', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');
INSERT INTO t_users( f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES( 3, 'support', '82482bfbc5e9a8018ee208b0d053ba0c4', 'Mainsim', 'Support', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');
INSERT INTO t_users( f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES( 4, 'helpdesk', 'b5a52c8852d5375d2f0d4f101452c3e30', 'Mainsim', 'Help Desk', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');
INSERT INTO t_users( f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES( 5, 'quality', 'b5a52c8852d5375d2f0d4f101452c3e30', 'Mainsim', 'Quality Assurance', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, -1, 'Administrator');

-- -----------------------------------------------------------------------------------------

-- Data manager selectors
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES (6, 'SELECTORS', 'DATA MANAGER', 7, 1435241555, 1, 9, 1, 'Import', '', -1, 0,1);
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES (7, 'SELECTORS', 'DATA MANAGER', 8, 1435241555, 1, 9, 1, 'Export', '', -1, 0,2);

INSERT INTO t_selectors ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES ( 6, -1, 1435241555, 1, 1, 1, NULL);
INSERT INTO t_selectors ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES ( 7, -1, 1435241555, 1, 1, 1, NULL);

INSERT INTO t_selectors_parent ( f_code, f_parent_code, f_timestamp, f_active) VALUES
( 6, 0, 1435241555, 1),
( 7, 0, 1435241555, 1);

INSERT INTO t_custom_fields( f_code, f_timestamp) VALUES
( 6, 1435241555),
( 7, 1435241555);

-- -----------------------------------------------------------------------------------------

-- User levels (roles)
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) 
VALUES (8, 'SYSTEM', 'LEVEL', '1', '1435241555', '1', 9, 1, 'Administrator', '', 1, 0,1),
  (9, 'SYSTEM', 'LEVEL', '2', '1435241555', '1', 9, 1, 'Super-User', '', 1, 0,2),
  (25, 'SYSTEM', 'LEVEL', '3', '1435241555', '1', 9, 1, 'Supervisor', '', -1, 0,3),
  (26, 'SYSTEM', 'LEVEL', '4', '1435241555', '1', 9, 1, 'Service Requestor', '', -1, 0,4),
  (27, 'SYSTEM', 'LEVEL', '5', '1435241555', '1', 9, 1, 'Internal Mantainer', '', -1, 0,5),
  (28, 'SYSTEM', 'LEVEL', '6', '1435241555', '1', 9, 1, 'External Mantainer', '', -1, 0,6),
  (29, 'SYSTEM', 'LEVEL', '7', '1435241555', '1', 9, 1, 'Supervisor PRO', '', -1, 0,7),
  (38, 'SYSTEM', 'LEVEL', '7', '1435241555', '1', 9, 1, 'Supervisor FREE', '', -1, 0,8);

INSERT INTO t_systems ( f_code, f_type_id, f_timestamp, f_user_id, fc_lvl_level) VALUES 
    (8, 4, 1435241555, 1, -1), 
    (9, 4, 1435241555, 1, 2),
    (25, 4, 1435241555, 1, 4),
    (26, 4, 1435241555, 1, 8),
    (27, 4, 1435241555, 1, 16),
    (28, 4, 1435241555, 1, 32),
    (29, 4, 1435241555, 1, 64),
    (38, 4, 1435241555, 1, 128);

INSERT INTO t_systems_parent ( f_code, f_parent_code, f_timestamp, f_active) 
    VALUES ( 8, 0, 1435241555, 1),
    ( 9, 0, 1435241555, 1),
    ( 25, 0, 1435241555, 1),
    ( 26, 0, 1435241555, 1),
    ( 27, 0, 1435241555, 1),
    ( 28, 0, 1435241555, 1),
    ( 29, 0, 1435241555, 1),
    ( 38, 0, 1435241555, 1);

INSERT INTO t_custom_fields( f_code, f_timestamp) VALUES
    ( 8, 1435241555),( 9, 1435241555),
    ( 25, 1435241555),( 26, 1435241555),
    ( 27, 1435241555),( 28, 1435241555),
    ( 29, 1435241555),( 38, 1435241555);

-- -----------------------------------------------------------------------------------------

-- Roots for standard selectors
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES 
 (10, 'SELECTORS', 'SELECTOR 1', '10', '1435241555', '1', 9, 1, 'Selector 01', '', -1, -1,1),
 (11, 'SELECTORS', 'SELECTOR 2', '11', '1435241555', '1', 9, 1, 'Selector 02', '', -1, -1,1),
 (12, 'SELECTORS', 'SELECTOR 3', '12', '1435241555', '1', 9, 1, 'Selector 03', '', -1, -1,1),
 (13, 'SELECTORS', 'SELECTOR 4', '13', '1435241555', '1', 9, 1, 'Selector 04', '', -1, -1,1),
 (14, 'SELECTORS', 'SELECTOR 5', '14', '1435241555', '1', 9, 1, 'Selector 05', '', -1, -1,1),
 (15, 'SELECTORS', 'SELECTOR 6', '15', '1435241555', '1', 9, 1, 'Selector 06', '', -1, -1,1),
 (16, 'SELECTORS', 'SELECTOR 7', '16', '1435241555', '1', 9, 1, 'Selector 07', '', -1, -1,1),
 (17, 'SELECTORS', 'SELECTOR 8', '17', '1435241555', '1', 9, 1, 'Selector 08', '', -1, -1,1),
 (18, 'SELECTORS', 'SELECTOR 9', '18', '1435241555', '1', 9, 1, 'Selector 09', '', -1, -1,1),
 (19, 'SELECTORS', 'SELECTOR 10', '19', '1435241555', '1', 9, 1, 'Selector 10', '', -1, -1,1);

INSERT INTO t_selectors ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority, fc_imp_code_sel_excel) VALUES 
 ( 10, 1, 1435241555, 1, 1, 1, NULL),
 ( 11, 2, 1435241555, 1, 1, 1, NULL),
 ( 12, 3, 1435241555, 1, 1, 1, NULL),
 ( 13, 4, 1435241555, 1, 1, 1, NULL),
 ( 14, 5, 1435241555, 1, 1, 1, NULL),
 ( 15, 6, 1435241555, 1, 1, 1, NULL),
 ( 16, 7, 1435241555, 1, 1, 1, NULL),
 ( 17, 8, 1435241555, 1, 1, 1, NULL),
 ( 18, 9, 1435241555, 1, 1, 1, NULL),
 ( 19, 10, 1435241555, 1, 1, 1, NULL);

INSERT INTO t_selectors_parent ( f_code, f_parent_code, f_timestamp, f_active) VALUES
( 10, 0, 1435241555, 1),
( 11, 0, 1435241555, 1),
( 12, 0, 1435241555, 1),
( 13, 0, 1435241555, 1),
( 14, 0, 1435241555, 1),
( 15, 0, 1435241555, 1),
( 16, 0, 1435241555, 1),
( 17, 0, 1435241555, 1),
( 18, 0, 1435241555, 1),
( 19, 0, 1435241555, 1);

INSERT INTO t_custom_fields( f_code, f_timestamp) VALUES
( 10, 1435241555),
( 11, 1435241555),
( 12, 1435241555),
( 13, 1435241555),
( 14, 1435241555),
( 15, 1435241555),
( 16, 1435241555),
( 17, 1435241555),
( 18, 1435241555),
( 19, 1435241555);

-- -----------------------------------------------------------------------------------------
-- Action priority
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES 
(20, 'WARES', 'ACTION', 1, 1435241555, 1, 9, 1, 'Lowest', '', -1, -1,1),
(21, 'WARES', 'ACTION', 2, 1435241555, 1, 9, 1, 'Low', '', -1, -1,2),
(22, 'WARES', 'ACTION', 3, 1435241555, 1, 9, 1, 'Normal', '', -1, -1,3),
(23, 'WARES', 'ACTION', 4, 1435241555, 1, 9, 1, 'High', '', -1, -1,4),
(24, 'WARES', 'ACTION', 5, 1435241555, 1, 9, 1, 'Highest', '', -1, -1,5);

INSERT INTO t_wares ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority,fc_action_priority_value,fc_action_priority_taking_charge,fc_action_priority_selected,fc_action_priority_color,fc_action_type) VALUES 
( 20, 11, 1435241555, 1, 1, 1,1,10,1,'#5b7326','priority'),
( 21, 11, 1435241555, 2, 1, 1,2,8,null,'#ffff00','priority'),
( 22, 11, 1435241555, 3, 1, 1,3,6,null,'#ffa300','priority'),
( 23, 11, 1435241555, 4, 1, 1,4,4,null,'#c61201','priority'),
( 24, 11, 1435241555, 5, 1, 1,5,1,null,'#7a0708','priority');

INSERT INTO t_wares_parent ( f_code, f_parent_code, f_timestamp, f_active) VALUES
( 20, 0, 1435241555, 1),
( 21, 0, 1435241555, 1),
( 22, 0, 1435241555, 1),
( 23, 0, 1435241555, 1),
( 24, 0, 1435241555, 1); 

INSERT INTO t_custom_fields ( f_code, f_timestamp) VALUES
( 20, 1435241555),
( 21, 1435241555),
( 22, 1435241555),
( 23, 1435241555),
( 24, 1435241555);


-- SERVICE REQUESTOR FREE / SUPERVISOR FREE -----------------------------------------------------------------------------------------
INSERT INTO t_creation_date (f_id, f_type, f_category, f_order, f_creation_date, f_creation_user, f_wf_id, f_phase_id, f_title, f_description, f_visibility, f_editability,fc_progress) VALUES
 (30, 'WARES', 'USER', 6, 1435241555, 1, 9, 1, 'Mainsim Requestor 1', '', -1, -1,6),
 (31, 'WARES', 'USER', 7, 1435241555, 1, 9, 2, 'Mainsim Requestor 2', '', -1, -1,7),
 (32, 'WARES', 'USER', 8, 1435241555, 1, 9, 2, 'Mainsim Requestor 3', '', -1, -1,8),
 (33, 'WARES', 'USER', 9, 1435241555, 1, 9, 2, 'Mainsim Requestor 4', '', -1, -1,9),
 (34, 'WARES', 'USER', 10, 1435241555, 1, 9, 2, 'Mainsim Requestor 5', '', -1, -1,10),
 (35, 'WARES', 'USER', 11, 1435241555, 1, 9, 2, 'Mainsim Requestor 6', '', -1, -1,11),
 (36, 'WARES', 'USER', 12, 1435241555, 1, 9, 2, 'Mainsim Requestor 7', '', -1, -1,12),
 (37, 'WARES', 'USER', 13, 1435241555, 1, 9, 1, 'Mainsim Supervisor', '', -1, -1,13);

INSERT INTO t_wares ( f_code, f_type_id, f_timestamp, f_order, f_user_id, f_priority) VALUES 
 ( 30, 16, 1435241555, 1, 1, 1),
 ( 31, 16, 1435241555, 2, 1, 1),
 ( 32, 16, 1435241555, 3, 1, 1),
 ( 33, 16, 1435241555, 4, 1, 1),
 ( 34, 16, 1435241555, 3, 1, 1),
 ( 35, 16, 1435241555, 4, 1, 1),
 ( 36, 16, 1435241555, 5, 1, 1),
 ( 37, 16, 1435241555, 1, 1, 1);

INSERT INTO t_wares_parent ( f_code, f_parent_code, f_timestamp, f_active) VALUES
 ( 30, 0, 1435241555, 1),
 ( 31, 0, 1435241555, 1),
 ( 32, 0, 1435241555, 1),
 ( 33, 0, 1435241555, 1),
 ( 34, 0, 1435241555, 1),
 ( 35, 0, 1435241555, 1),
 ( 36, 0, 1435241555, 1),
 ( 37, 0, 1435241555, 1);  

INSERT INTO t_custom_fields ( f_code, f_timestamp) VALUES
 ( 30, 1435241555),
 ( 31, 1435241555),
 ( 32, 1435241555),
 ( 33, 1435241555),
 ( 34, 1435241555),
 ( 35, 1435241555),
 ( 36, 1435241555),
 ( 37, 1435241555); 

INSERT INTO t_users( f_code, fc_usr_usn, fc_usr_pwd, fc_usr_firstname, fc_usr_lastname, fc_usr_mail, fc_usr_gender, fc_usr_language, fc_usr_status, fc_usr_usn_registration, fc_usr_pwd_registration, fc_usr_usn_expiration, fc_usr_pwd_expiration, fc_usr_group_id, fc_usr_level, fc_usr_level_text) VALUES
( 30, 'user.requestor1', '313ed0558152d04dc20036dbd850dc9bd', 'Mainsim', 'Requestor 1', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, 8, 'Service Requestor'),
( 31, 'user.requestor2', '313ed0558152d04dc20036dbd850dc9bd', 'Mainsim', 'Requestor 2', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, 8, 'Service Requestor'),
( 32, 'user.requestor3', '313ed0558152d04dc20036dbd850dc9bd', 'Mainsim', 'Requestor 3', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, 8, 'Service Requestor'),
( 33, 'user.requestor4', '313ed0558152d04dc20036dbd850dc9bd', 'Mainsim', 'Requestor 4', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, 8, 'Service Requestor'),
( 34, 'user.requestor5', '313ed0558152d04dc20036dbd850dc9bd', 'Mainsim', 'Requestor 5', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, 8, 'Service Requestor'),
( 35, 'user.requestor6', '313ed0558152d04dc20036dbd850dc9bd', 'Mainsim', 'Requestor 6', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, 8, 'Service Requestor'),
( 36, 'user.requestor7', '313ed0558152d04dc20036dbd850dc9bd', 'Mainsim', 'Requestor 7', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, 8, 'Service Requestor'),
( 37, 'user.supervisor', '313ed0558152d04dc20036dbd850dc9bd', 'Mainsim', 'Supervisor', 'test@unp.it', 0, 1, 1, 0, 0, 0, 0, 0, 128, 'Supervisor FREE');


-- -----------------------------------------------------------------------------------------

-- Cross user X level

INSERT INTO t_wares_systems ( f_ware_id, f_system_id, f_timestamp) VALUES
( 1, 8, 1435241555),
( 2, 8, 1435241555),
( 3, 8, 1435241555),
( 4, 8, 1435241555),
( 5, 8, 1435241555),
( 30, 26, 1435241555),
( 31, 26, 1435241555),
( 32, 26, 1435241555),
( 33, 26, 1435241555),
( 34, 26, 1435241555),
( 35, 26, 1435241555),
( 36, 26, 1435241555),
( 37, 38, 1435241555);

-- insert into t_progress list for new elements
INSERT INTO t_progress_lists ( f_table, f_type_id, fc_progress) VALUES
( 't_selectors', -1, 1),( 't_selectors', -1, 2),( 't_selectors', 1, 1),
( 't_selectors', 2, 1),( 't_selectors', 3, 1),( 't_selectors', 4, 1),
( 't_selectors', 5, 1),( 't_selectors', 6, 1),( 't_selectors', 7, 1),
( 't_selectors', 8, 1),( 't_selectors', 9, 1),( 't_selectors', 10, 1),
( 't_wares', 16, 1),( 't_wares', 16, 2),( 't_wares', 16, 3),( 't_wares', 16, 4),
( 't_wares', 16, 5),( 't_wares', 16, 6),( 't_wares', 16, 7),( 't_wares', 16, 8),
( 't_wares', 16, 9),( 't_wares', 16, 10),( 't_wares', 16, 11),( 't_wares', 16, 12),
( 't_wares', 16, 13),( 't_systems', 4, 1),( 't_systems', 4, 2),( 't_systems', 4, 3),
( 't_systems', 4, 4),( 't_systems', 4, 5),( 't_systems', 4, 6),( 't_systems', 4, 7),
( 't_systems', 4, 8),( 't_wares', 11, 1),( 't_wares', 11, 2),( 't_wares', 11, 3),
( 't_wares', 11, 4),( 't_wares', 11, 5);