<?php

class Mainsim_Plugin_Auth extends Zend_Controller_Plugin_Abstract 
{
		
	/**
	 * @see Zend_Controller_Plugin_Abstract::preDispatch()
	 *
	 * @param Zend_Controller_Request_Abstract $request
	 */
	public function preDispatch(Zend_Controller_Request_Abstract $request) 
        {
		$controller = $request->getControllerName();
		$action     = $request->getActionName();
		$acl        = new Mainsim_Acl();
		$role       = 'guest'; // ruolo iniziale
                $module     = $request->getModuleName();                
		//Zend_Debug::dump(Zend_Auth::getInstance()->getIdentity());
		if(Zend_Auth::getInstance()->hasIdentity()){
                    $user_info = Zend_Auth::getInstance()->getIdentity();
                    $role = 'member';
                    if($module == 'admin') {                        
                        if($user_info->f_level == -1){
                            $role = 'admin';
                        }
                        else {                            
                            $request->setControllerName('error'); //cambio il controller e lo indirizzo alla pagina di errore
                            $request->setActionName('acl');                           
                        }
                    }
                    elseif($user_info->f_level == -1){
                        $role = 'admin';
                    }                       
		}
		
		if(!$acl->isAllowed($role,$controller,$action)) {
                    $request->setControllerName('error'); //cambio il controller e lo indirizzo alla pagina di errore
                    $request->setActionName('acl');
		}
		
	}
	
	

	
}