<?php

class Mainsim_Plugin_Acl extends Zend_Controller_Plugin_Abstract 
{
  function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
  {
    $acl = new Mainsim_Acl();
    Zend_Registry::set('acl',$acl);
  }
}
