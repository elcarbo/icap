<?php

class Mainsim_Acl extends Zend_Acl 
{
  function __construct()
  {
    $this->addResource(new Zend_Acl_Resource('autocomplete'));
    $this->addResource(new Zend_Acl_Resource('open-map'));
    $this->addResource(new Zend_Acl_Resource('index')); // creo aggiungo le risorsa all'Acl
    $this->addResource(new Zend_Acl_Resource('error'));
    $this->addResource(new Zend_Acl_Resource('mainpage'));
    $this->addResource(new Zend_Acl_Resource('login'));
    $this->addResource(new Zend_Acl_Resource('start-center'));
    $this->addResource(new Zend_Acl_Resource('plan'));
    $this->addResource(new Zend_Acl_Resource('wizard'));
    $this->addResource(new Zend_Acl_Resource('script'));
    $this->addResource(new Zend_Acl_Resource('treegrid'));
    $this->addResource(new Zend_Acl_Resource('wares'));
    $this->addResource(new Zend_Acl_Resource('workflows'));
    $this->addResource(new Zend_Acl_Resource('bookmarks'));
    $this->addResource(new Zend_Acl_Resource('workorder'));
    $this->addResource(new Zend_Acl_Resource('pdf-creator'));
    $this->addResource(new Zend_Acl_Resource('pdf'));
    $this->addResource(new Zend_Acl_Resource('selectors'));    
    $this->addResource(new Zend_Acl_Resource('cobie-importer'));    
    $this->addResource(new Zend_Acl_Resource('editor-layout')); 
    $this->addResource(new Zend_Acl_Resource('ui'));
    $this->addResource(new Zend_Acl_Resource('map'));
    $this->addResource(new Zend_Acl_Resource('select')); 
    $this->addResource(new Zend_Acl_Resource('summary'));
    $this->addResource(new Zend_Acl_Resource('export'));
    $this->addResource(new Zend_Acl_Resource('inspection'));
    $this->addResource(new Zend_Acl_Resource('uploads'));
    $this->addResource(new Zend_Acl_Resource('utilities'));
    $this->addResource(new Zend_Acl_Resource('database-admin'));     
    $this->addResource(new Zend_Acl_Resource('module-importer'));     
    $this->addResource(new Zend_Acl_Resource('pdf-writer'));
    $this->addResource(new Zend_Acl_Resource('scripts'));
    $this->addResource(new Zend_Acl_Resource('menu'));
    $this->addResource(new Zend_Acl_Resource('mail'));
    $this->addResource(new Zend_Acl_Resource('system'));  
    $this->addResource(new Zend_Acl_Resource('datamanager'));  
    $this->addResource(new Zend_Acl_Resource('sql'));
    $this->addResource(new Zend_Acl_Resource('update'));
    $this->addResource(new Zend_Acl_Resource('kpi'));
    $this->addResource(new Zend_Acl_Resource('system-editor'));
    $this->addResource(new Zend_Acl_Resource('test'));
    $this->addResource(new Zend_Acl_Resource('mainsim-object'));
    $this->addResource(new Zend_Acl_Resource('calendar'));
    $this->addResource(new Zend_Acl_Resource('document'));
    $this->addResource(new Zend_Acl_Resource('help'));
    $this->addResource(new Zend_Acl_Resource('reservation'));
    $this->addResource(new Zend_Acl_Resource('registration'));
    $this->addResource(new Zend_Acl_Resource('db-updater'));
    $this->addResource(new Zend_Acl_Resource('installer'));
    $this->addResource(new Zend_Acl_Resource('pricelist'));

    // mobile
    $this->addResource(new Zend_Acl_Resource('mobile-login')); 
    $this->addResource(new Zend_Acl_Resource('mobile-sync'));
    $this->addResource(new Zend_Acl_Resource('mobile-workorders'));
    $this->addResource(new Zend_Acl_Resource('mobile-wares'));
    
    $this->addRole(new Zend_Acl_Role('guest'));
    $this->addRole(new Zend_Acl_Role('member'),'guest'); // aggiungo guest per ereditare i suoi permessi    
    $this->addRole(new Zend_Acl_Role('admin'),'member');
    
    //associazine dei permessi ai ruoli
    $this->allow('guest','login');
    $this->allow('guest','error');
    $this->allow('guest','index');
    $this->allow('guest','mainpage');
    $this->allow('guest','workorder');
    $this->allow('guest','help');
    $this->allow('guest','registration');
    $this->allow('guest','installer');
    $this->allow('guest','mail');
	$this->allow('guest','script');
    // mobile
    $this->allow('guest','mobile-login');
    $this->allow('guest','mobile-sync');
    $this->allow('guest','mobile-workorders');
    $this->allow('guest','mobile-wares');
    
    $this->allow('member','mainpage'); // permessi per membri
    $this->allow('member','start-center');
    $this->allow('member','autocomplete');    
    $this->allow('member','plan');
    $this->allow('member','inspection');
    $this->allow('member','wizard');
    $this->allow('member','script');
    $this->allow('member','treegrid');
    $this->allow('member','wares');
    $this->allow('member','selectors');
    $this->allow('member','workflows');
    $this->allow('member','bookmarks');
    $this->allow('member','pdf-creator');
    $this->allow('member','pdf');
    $this->allow('member','cobie-importer');
    $this->allow('member','workorder');    
    $this->allow('member','summary');
    $this->allow('member','open-map');
    $this->allow('member','export');
    $this->allow('member','map');
    $this->allow('member','uploads');
    $this->allow('member','utilities');
    $this->allow('member','select');
    $this->allow('member','module-importer');
    $this->allow('member','menu');
    $this->allow('member','system');    
    $this->allow('member','kpi');
    $this->allow('member','mainsim-object');
    $this->allow('member','calendar');
    $this->allow('member','document');
    $this->allow('member','reservation');
    $this->allow('member','pricelist');
    
    $this->allow('admin','editor-layout');
    $this->allow('admin','ui');
    $this->allow('admin','database-admin');    
    $this->allow('admin','pdf-writer');
    $this->allow('admin','update');
    $this->allow('admin','datamanager');
    $this->allow('admin','sql');    
    $this->allow('admin','system-editor');    
    $this->allow('admin','scripts');    
    $this->allow('admin','db-updater');    
  }
}