<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASE_PATH_LIB')
    || define('BASE_PATH_LIB', realpath(dirname(__FILE__) . '/../'));
ini_set("display_errors","On");

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
error_reporting(E_ALL);

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

/*
 * File che richiamato da crontab, 
 * avvia i file previsti
 */

date_default_timezone_set('Europe/Paris');
set_include_path(implode(PATH_SEPARATOR, 
        array(
            realpath(BASE_PATH_LIB.'/library'),
            get_include_path()
        )
    )
);

/** Zend_Application */

require_once 'Zend/Application.php';
require_once('library/phpExcel/PHPExcel.php');

// Create application, bootstrap, and run
new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

new Zend_Application_Module_Autoloader(array(
    'namespace' => 'Mainsim_',
    'basePath'  => APPLICATION_PATH .'/modules/default',
    'resourceTypes' => array ()));

new Zend_Application_Module_Autoloader(array(
    'namespace' => 'MainsimAdmin_',
    'basePath'  => APPLICATION_PATH .'/modules/admin',
    'resourceTypes' => array ()));

//database configuration

//include BASE_PATH_LIB.'/importer/import.php';
require_once APPLICATION_PATH.'/configs/constants.php';
require_once APPLICATION_PATH.'/configs/script_special_functions.php';
require_once APPLICATION_PATH.'/configs/errorHandler.php';

set_error_handler('mainsimErrorHandler');