var moSignature = {  aP:[], J:null, j:null, cj:null, L:0, T:0, ll:1.2, 
         lmax:1.8, lmin:0.8, ldec:0.1, W:780, H:480,
         matrx:[], col:"#808080", win: "",

Start: function(txt, md, cont) {    // md:   0=nuovo  1=edita  2=visualizza
with(moSignature){   if(!md) md=0;

  if(!txt) txt=""; J=mf$(cont);

  var svcnc="onclick='moSignature.Save();'>Save";
  if(md==2) svcnc="onclick='moSignature.openClose();'>Exit";

    var wbd = mf$("moSignature_w");
    if(!wbd) {
        wbd = moGlb.createElement("moSignature_w", "div");
        with(wbd.style) display='none', padding='5px';
    }
  
//  wbd.innerHTML = "<div class='wrt_ang' style='width:"+W+"px;height:"+H+"px;background-color:#f0f0f0;'>"+
//  "<img src='edit.png' style='position:absolute;left:10px;top:9px;' />"+  
//  "<div style='position:absolute;left:32px;top:12px;font:bold 12px Arial;'>"+txt+"</div>"+
//  "<img src='cancel.png' style='position:absolute;right:6px;top:4px;cursor:pointer;' onclick='moSignature.close();' />"+
//  "<div style='position:absolute;left:10px;top:38px;width:"+(W-20)+"px;height:"+(H-76)+"px;background-color:#fff;'></div>"+
//  "<canvas id='idcnvsig' style='position:absolute;left:10px;top:38px;' width='"+(W-20)+"' height='"+(H-76)+"'></canvas>"+
//  ((md==2)?"":"<button class='wrt_btn' style='left:10px;bottom:8px;' onclick='with(moSignature) cj.clearRect(0,0,W,H);'>Clear</button>")+
//  "<button class='wrt_btn' style='right:10px;bottom:8px;' "+svcnc+"</button>"+
//  "</div>";

    wbd.innerHTML = "<canvas id='idcnvsig' width='"+(W)+"' height='"+(H-20)+"' style='border:1px solid #a8a8a8;'></canvas><p/>" +
        ((md==2)?"":"<button class='wrt_btn' style='float: left;' onclick='with(moSignature) cj.clearRect(0,0,W,H);'>Clear</button>")+
        "<button class='wrt_btn' style='float:right;' "+svcnc+"</button>";

    win = moComp.createComponent("moSignature_w", "moWindow", { wc: W, hc: H, onload: function () {
        mf$("moSignature_w").style.display = "block";
    }});
    mf$(win.getContainerId()).appendChild(wbd);    
    win.show({ title: "Put a signature" });


  j=mf$("idcnvsig");  cj=j.getContext("2d");
  var P=moGlb.getPosition(j), r, ml=matrx.length;
  L=P.l; T=P.t;

  // write
  if(md<2) {  var l=["touchstart","touchmove","touchend","mousedown","mousemove","mouseup"]; 
    for(r=0;r<l.length;r++) {  eval("j.on"+l[r]+"=function(e){ moSignature.Evt(e); }"); }  }
         
         //drawSig();
  // view matrx         
  if(md && ml) for(r=0;r<ml;r++) {
     var w=W-20, h=H-76, x=0, y=0, a=0;
     cj.fillStyle=col;   
     for(var r=0;r<matrx.length;r++) {  n=matrx[r];       
        if(!n) { cj.fillRect(x,y,1,1); n=1; }
        x+=n; if(x>w) { a=x%w;  y+=((x-a)/w);  x=a; }     
   }}                 
}},
 
//drawSig: function (v) {
//    with(moSignature) {
//        // view matrx         
//      if(md && ml) for(r=0;r<ml;r++) {
//         var w=W-20, h=H-76, x=0, y=0, a=0;
//         cj.fillStyle=col;   
//         for(var r=0;r<matrx.length;r++) {  n=matrx[r];
//            if(!n) { cj.fillRect(x,y,1,1); n=1; }
//            x+=n; if(x>w) { a=x%w;  y+=((x-a)/w);  x=a; }     
//       }} 
//    }
//},

openClose:function(s) {  
with(moSignature) {   
  if(!s) s=""; J.innerHTML=s;     
}},

close: function () {moSignature.win.hide();},


Save:function() {  
with(moSignature) {    
    var w=W-20, h=H-76, idt=cj.getImageData(0,0,w,h), r, pxs=idt.data, nb=0, v; 
    matrx=[];
    for(var r=0;r<pxs.length;r+=4) {
    v=pxs[r+3]; if(!v) { nb++;  continue; }
    if(nb) matrx.push(nb);
    matrx.push(0);  nb=0; } 
    if(nb) matrx.push(nb); 
          
    if(matrx.length<2) { moGlb.alert.show(moGlb.langTranslate("Insert a signature")); return false;  }
    close(); 
    
    // mf$("debug").innerHTML="Dimensione: "+parseInt(matrx.length/1024)+"Kb - ("+matrx.length+")";             // size    
}},



Draw:function(){
with(moSignature){  
  var n=aP.length; if(n>3) { aP.shift(); n--; }
  var B=aP[n-2], C=aP[n-1], m0, m1, dm, kk=0;
if(n>=3){ kk=1; var A=aP[n-3];
if( (B.y-A.y) && (C.y-B.y) ){
  m0=-1*(B.x-A.x)/(B.y-A.y);
  m1=-1*(C.x-B.x)/(C.y-B.y);
  dm=Math.abs(m0-m1);  
} else dm=0.01;
if(dm<0.05) kk=0;   // se allineati
else {
var p0= { x:A.x+((B.x-A.x)/2), y:A.y+((B.y-A.y)/2) }, 
    p1= { x:B.x+((C.x-B.x)/2), y:B.y+((C.y-B.y)/2) },
    q0=p0.y-m0*p0.x,
    q1=p1.y-m1*p1.x;    
var cx=(q1-q0)/(m0-m1), cy=m1*cx+q1, rr=Distz(cx,cy,B.x,B.y);        
var a0=Math.atan2(B.y-cy,B.x-cx), a1=Math.atan2(C.y-cy,C.x-cx), u=0, a=a0-a1;
if(a<0) a+=6.283185307179586; var u=(a>3.141592653589793)?0:1;           
}}
with(cj) { strokeStyle=col; lineWidth=ll; lineCap="round"; 
  if(kk){ arc(cx,cy,rr,a0,a1,u); stroke();  
  } else { beginPath();
  moveTo(B.x,B.y); lineTo(C.x,C.y);  
  closePath(); stroke(); }   
} return 1;
}},


Distz:function (x1,y1,x2,y2){
var x=Math.abs(x2-x1), y=Math.abs(y2-y1);
return Math.sqrt(x*x+y*y);
},

Evt:function(e) {  
with(moSignature){               
  var r; if(isTouch) var tch=e.changedTouches;
  var P=moGlb.getPosition(mf$("idcnvsig"));
  L = P.l, T = P.t;
switch (e.type) {  
  case "touchstart":
       if(!aP.length) { var t=tch[0], x=t.pageX, y=t.pageY, i=t.identifier;
          aP.push( { f:i, x:x-L, y:y-T  });  }
  break; 
  case "touchmove":
       if(aP.length) {
        var idf=aP[0].f, t,i,nx,ny;
        for(r=0;r<tch.length;r++) { t=tch[r], i=t.identifier;
          if(i!=idf) continue;
          nx=t.pageX-L, ny=t.pageY-T; 
          aP.push({ f:i, x:nx, y:ny });
          Draw(); break; }}
  break;
  case "touchend": aP=[]; 
  break;     
  case "mousedown":
        var P=moEvtMng.getMouseXY(e, mf$("idcnvsig")); aP=[{ x:P.x, y:P.y }];  
  break;
  case "mousemove":
      if(aP.length){ var P=moEvtMng.getMouseXY(e, mf$("idcnvsig")), nx=P.x, ny=P.y; 
            aP.push({ x:nx, y:ny }); Draw(); }
  break;
  case "mouseup": aP=[]; break;  
}  
e.preventDefault();
return false;
}}
 
} //  fine moSignature