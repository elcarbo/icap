/* ajax

methods:
Ajax.sendAjaxPost( fileserver, post_value, fnc_name );    
Ajax.sendAjaxGet( fileserver, fnc_name );

properties:
Disable   
fnc_offline
fnc_check

*/


Ajax = { Disable:0,              // 1: stop Ajax call 
         fnc_offline:"",        // function run if can not reach server
         fnc_check:"",         // function run when arrive response from server 
         Na:[],               // processi attivi   1 impegnato    0 libero
         jx:[],              // array di oggetti ajax
         Cache:{},
         Counts:0,
         AC:[],
         offTM:0,
         Offline:0,
         
sendAjaxPost:function(fileserver, post_value, fnc, encode, no_cache) {
with(Ajax){
  
  var exct=["/mainsim3/login/update-time", "treegrid/reverse-ajax", "priority","open-wo","emergency","periodic","work-request"];
  if( exct.indexOf(fileserver)!=-1) no_cache=1;
  

  if(Disable) return false;
  if(!fnc) fnc = "Ajax.result";
  if(!no_cache) no_cache=0;

  Counts++;
  
  var i=searchId(), FNC, dt=Counts;
  jx[i]=new XMLHttpRequest();

  FNC="Ajax.Frisp"+i;

  eval(FNC+"=function(){ if(Ajax.Disable) return; "+
    "if(Ajax.jx["+i+"].readyState==4) { "+                                         
       "if(!Ajax.jx["+i+"].status || Ajax.jx["+i+"].status>500){ "+ (fnc_offline?(fnc_offline+"(\""+fileserver+"\",\""+fnc+"\");"):"") + " Ajax.f_offTime();  return; }; "+ 
       "if(Ajax.jx["+i+"].status==200){ "+                                    
          "var risp=Ajax.jx["+i+"].responseText; " + (fnc_check?(fnc_check+"(); "):"") +fnc+"(risp); Ajax.f_Out_Cache("+dt+","+i+","+no_cache+"); Ajax.Na["+i+"]=0; "+
       " } else {  Ajax.f_Out_Cache("+dt+","+i+","+no_cache+"); Ajax.Na["+i+"]=0; };"+
    " }; "+
  "}"); 
         
    jx[i].open("post",fileserver,true);
    jx[i].onreadystatechange=eval(FNC);
    jx[i].setRequestHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
         
    if(encode) {
      var val = post_value.split("="), v = val.shift();
          post_value = v+"="+encodeURIComponent(val.join("=")); 
    }
   
   if(!no_cache) f_In_Cache(dt,fileserver,i,post_value);      
   jx[i].send(post_value);
   
}
return true;
},




f_In_Cache:function(dt,fs,i,vl){
with(Ajax){

   // push new ajax call 
   Cache["id_"+dt]={ "fileserver":fs, "jxi":i, "pvalue":vl, "idcount":dt };
    
  // console.log("IN "+"id_"+dt+" | "+fs);
}},





f_Out_Cache:function(dt,i,nc){
with(Ajax){

  if(nc) return;

  if(typeof(Cache["id_"+dt]) != "undefined")  delete(Cache["id_"+dt]);
  
  if(AC.length) f_ResCycle();
 
 // console.log("OUT: "+"id_"+dt+" length: "+Object.keys(Cache).length);

}},





f_Resume:function(fn){
with(Ajax){

  Offline=0;
  var k,n=Object.keys(Cache).length;
  
  // check numbers and time in standby
  if(!n) { moInit.evtRestart(0); return; }
  if(n>30 || f_diffTime()>120 ) { moInit.evtRestart(1); return; }                 //  30 = num max calls in cache  120sec = max time   |=>  reload mainsim
 
  AC=[]; 
  for(k in Cache) AC.push(k);
  AC.sort();

  f_ResCycle();
}},


f_ResCycle:function(){
with(Ajax){
   
  if(Offline) return; 
 
  try { var FNC, k=AC.shift(), i=Cache[k].jxi; }catch(e){ moInit.evtRestart(1);  return; }
  
   jx[i]=new XMLHttpRequest();
   FNC="Ajax.Frisp"+i;
   jx[i].open("post",Cache[k].fileserver,true);
   jx[i].onreadystatechange=eval(FNC);
   jx[i].setRequestHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");              
   jx[i].send(Cache[k].pvalue);
 
 
  if(!AC.length) {
    moInit.evtRestart(0);
    return;
  }
  
  
}},


f_offTime:function(){
with(Ajax){
  Offline=1;
  AC=[];
  offTM=f_getTime();
}},



f_diffTime:function(){
with(Ajax){
  if(!offTM) return 0;
  var t=f_getTime()-offTM;
  offTM=0;
  return t;
}},

f_getTime:function(){
  return parseInt((new Date()).getTime()/1000);
},








sendAjaxGet:function(fileserver,nomefunz){
     with(Ajax){
         if(Disable) return false;
         if(!fnc) fnc = "Ajax.none";
         var i=searchId();
         jx[i]=new XMLHttpRequest();
         var FNC="Ajax.Frisp"+i;
          
         eval(FNC+"=function(){ if(Ajax.jx["+i+"].readyState==4) if(Ajax.jx["+i+"].status==200){"+
              "  var risp=Ajax.jx["+i+"].responseText;  "+fnc+"(risp); Ajax.Na["+i+"]=0;  }}");          
            
         jx[i].open("get",fileserver,true);
         jx[i].onreadystatechange=eval( FNC );
         jx[i].send(null); 
         }
     return true;    
}, 


searchId:function(){
     with(Ajax){
         var n=Na.length, i=-1;
         for(var r=0;r<n;r++) if(!Na[r]) {i=r;break;}
         if(i<0) {Na.push(1);return n;}
         Na[i]=1;    
         return i;
         }         
},


result:function (o) {
     if(o) {
        o = JSON.parse(o);
        if(moGlb.isset(o.message)) moGlb.alert.show(moGlb.langTranslate(o.message));
		    else if(moGlb.isset(o.error)) moGlb.alert.show(moGlb.langTranslate(o.error));
        else if(typeof o == "string" && o.indexOf('SQLSTATE') >= 0) moGlb.alert.show(moGlb.langTranslate("SQL error: check your bookmark fields"));
        moGlb.loader.hide();
    }
}   
 
} // fine Ajax