moPreload = {
    load: function (imgs, fn, i) {
        if(!i) i = 0;
        //moGlb.loader.hide(); moGlb.loader.show(moGlb.langTranslate("Loading progress")+" "+Math.ceil((i*100)/imgs.length)+"%");
        var loadingText = moGlb.langTranslate("Loading progress")+" "+Math.ceil((i*100)/imgs.length)+"%";
        if(moGlb.loader.isVisible()) moGlb.loader.setText(loadingText);
        else moGlb.loader.show(loadingText);
        if(i == imgs.length) {fn();return;}
        if(!moGlb.isset(i)) i = 0;
        var imgName = moPreload.createImgName(imgs[i]);
        if(moGlb.isset(moGlb.img[imgName])) moPreload.load(imgs, fn, i+1);
        else {
            //moPreload.singleLoad((moGlb.baseUrl).replace("/admin", "")+(imgs[i].indexOf("session_id") >= 0 ? "/getDocument.php?size=thumbnail&" : "/public/msim_images/")+imgs[i], imgName,
            moPreload.singleLoad((moGlb.baseUrl).replace("/admin", "")+(imgs[i].indexOf("session_id") >= 0 ? "/document/get-file?size=thumbnail&" : "/public/msim_images/")+imgs[i], imgName,
                function (e) {moPreload.load(imgs, fn, i+1);},
                function (e) {moPreload.load(imgs, fn, i+1);}
            );
        }
    },
    createImgName: function (value) {
        if(value.indexOf("session_id") >= 0) {  // Db image
            return "thumbnails/"+((value.split("&")[1]).split("=")[1]);
        } return value.substr(value.indexOf("/")+1); // Disk image
    },
    get: function (path, imgNames) {
        with(moGlb) {
            var imgs = [];
            for(var i in imgNames) imgs.push(isset(img[path+"/"+imgNames[i]]) ? img[path+"/"+imgNames[i]] : "");
            return imgs;
        }
    },
    singleLoad: function (src, imgName, fnload, fnerror) {
        moGlb.img[imgName] = new Image();
        moGlb.img[imgName].src = src;
        if(fnload) moGlb.img[imgName].onload = fnload;
        if(fnerror) moGlb.img[imgName].onerror = fnerror;
    }
};