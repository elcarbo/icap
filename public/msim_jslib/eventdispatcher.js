var EventDispatcher = {
    modules: {}, delay: 300,
    listEvt:[],
    timeWait:0, 

    register: function (module, exclude) {    
        with(EventDispatcher) { 
            if(typeof exclude != "undefined" && typeof exclude != "object") exclude = [ exclude ];
            modules[module] = (exclude ? exclude : "all");
        }
    },

    send: function (module, key, values) {
        with(EventDispatcher) {
          listEvt.push( { module: module, key: key, values: values, ts: new Date().getTime() });         
          if(!timeWait) { timeWait=true; f_deleay(); }
    }},

   f_deleay:function(){ 
      with(EventDispatcher){
        var n=listEvt.length;
        if(!n) { timeWait=false; return; }
        setTimeout("EventDispatcher.dispatch("+n+")", delay/2);

        if(!timeWait) return;
        setTimeout("EventDispatcher.f_deleay()", delay);
   }}, 

    dispatch: function (n) {
        with(EventDispatcher) {
           
          var r,ev,events={};
          for(r=0;r<n;r++) ev=listEvt.shift();
          events[ev.module]=ev; 
           
            for(var j in events){
                for(var i in modules){
                    with(events[j])
                       if(module != i && !moArray.hasElement(modules[i], module)){
                            (moComp.getComponent(i)).listener(module, key, values);
  
                    } } }  

        }
    },

    unregister: function (module) {with(EventDispatcher) {delete(modules[module]);}}
};