var moDtmPkr = {
    cnvId: "calendar_cnv",                     // Canvas id
    datePicker: true,
    timePicker: true,
    showWeek: false,
    hasWin: true,
    title: "",
    
    // Background colors
    bgColor: moColors.bgNone,                // Calendar background color, default white
    selectedBgColor: moColors.bgSelected,        // Background color for the selected day, default green
    overBgColor: moColors.bgOver,            // Background color when overing a day, default light green
    dBgColor: moColors.bg,                // Background color for a day, default light gray
    dMonthBgColor: moColors.bg,           // Background color for days in the selected month, default the same as bgColor
    dWeekBgColor: "#7EBCF2",            // Background color for days in the selected week, default light green
    todayBgColor: moColors.bgHighlightColor,           // Background color for today, default the same as bgColor
    
    // Text colors
    color: moColors.text,                   // Calendar text color, default black
    selectedColor: moColors.text,           // Text color for the selected day, default the same as monthColor
    overColor: moColors.text,               // Text color when overing a day, default the same as monthColor
    dColor: moColors.textDisabled,                   // Default text color, default gray
    dMonthColor: moColors.text,              // Text color for days in the selected month, default black
    dWeekColor: moColors.text,               // Text color for days in the selected week, default the same as monthColor
    todayColor: moColors.text,              // Text color for today, default the same as monthColor
    
    // Text styles
    style: moGlb.fontNormal,                // Calendar text style, default 11px Arial
    monthStyle: moGlb.fontBold,       // Style for the month label at the top
    selectedStyle: moGlb.fontNormal,        // Text style for the selected day, default the same as style
    overStyle: moGlb.fontNormal,            // Text style when overing a day, default the same as style
    dStyle: moGlb.fontNormal,                // Default text style, default 11px Arial
    dMonthStyle: moGlb.fontNormal,           // Text style for days in the selected month, default the same as style
    dWeekStyle: moGlb.fontNormal,            // Text style for days in the selected week, default the same as style
    todayStyle: moGlb.fontBold,      // Text style for today, default the same as style but bolder
    
    /**/
    // Strokes
    stroke: moColors.border,          // Calendar stroke, default null
    selectedStroke: moColors.border,         // Stroke for the selected day, default null
    overStroke: moColors.border,             // Stroke when overing a day, default null
    dStroke: moColors.border,                // Default text stroke, default null
    dMonthStroke: moColors.border,           // Stroke for days in the selected month, default null
    dWeekStroke: moColors.border,            // Stroke for days in the selected week, default null
    todayStroke: moColors.border,            // Stroke for today, default null
    /**/
    
//    // Background colors
//    bgColor: "#ffffff",                // Calendar background color, default white
//    selectedBgColor: "#96CC87",        // Background color for the selected day, default green
//    overBgColor: "#E8FAE3",            // Background color when overing a day, default light green
//    dBgColor: "#F5F7F5",                // Background color for a day, default light gray
//    dMonthBgColor: "#F5F7F5",           // Background color for days in the selected month, default the same as bgColor
//    dWeekBgColor: "#7EBCF2",            // Background color for days in the selected week, default light green
//    todayBgColor: "#F6C182",           // Background color for today, default the same as bgColor
//    
//    // Text colors
//    color: "#000000",                   // Calendar text color, default black
//    selectedColor: "#000000",           // Text color for the selected day, default the same as monthColor
//    overColor: "#000000",               // Text color when overing a day, default the same as monthColor
//    dColor: "#a8a8a8",                   // Default text color, default gray
//    dMonthColor: "#000000",              // Text color for days in the selected month, default black
//    dWeekColor: "#000000",               // Text color for days in the selected week, default the same as monthColor
//    todayColor: "#000000",              // Text color for today, default the same as monthColor
//    /**/
//    
//    // Strokes
//    stroke: "#a8a8a8",          // Calendar stroke, default null
//    selectedStroke: "",         // Stroke for the selected day, default null
//    overStroke: "",             // Stroke when overing a day, default null
//    dStroke: "",                // Default text stroke, default null
//    dMonthStroke: "",           // Stroke for days in the selected month, default null
//    dWeekStroke: "",            // Stroke for days in the selected week, default null
//    todayStroke: "",            // Stroke for today, default null
//    /**/
    
    // Size and margin
    padding: 10, dayW: 22, dayH: 20, space: 2,
    dayRows: 6, dayCols: 7, timeW: 20, timeH: 15,
    
    // CSS
    className: "", top: 0, left: 0,
    
    // Images
    imgW: 16, imgH: 16, imgPath: "dtmpicker/components/", images: [],
    imageNames: [ "left.png", "right.png", "expand.png", "reduce.png", "today.png", "cancel.png", "ok.png", "clock.png" ],
    
    // Utility
    touch: moGlb.touch,
    
    // Date formats and labels (inherited from globals)
    weekDays: moGlb.weekDays, months: moGlb.months,
    weekLabel: moGlb.weekLabel, weekStartsOn: moGlb.weekStartsOn,                    // Day of the week from which the week starts
    monthFormat: "{F} {Y}", //selectedDateFormat: "{D2}, {d}/{m}/{Y}",
    //dateTimeFormat: moGlb.localization.f_datetime, dateFormat: moGlb.localization.f_date, timeFormat: moGlb.localization.f_time,
    ifield: "",
     
    prv: {
        ctx: "", innerCnvId: "", innerCtx: "",        
        month1st: "",           // First day for the shown month
        
        mouseOver: false,       // If the mouse is over the canvas or not
        sliding: false,
        
        selectedDate: null,     // Selected date
                
        overDate: null,         // Day on which the mouse is over
        overMonth: null,         // Month on which the mouse is over in the month picker view
        overYear: null,         // Year on which the mouse is over in the month picker view
        
        dayXY: [],              // Coords for every day on screen
        monthXY: [],            // Coords for every month on screen in the month picker view
        yearXY: [],             // Coords for every year on screen in the month picker view        
        showMonthPicker: false,
        
        monthW: 0, monthH: 0,             // Month label size in month picker
        
        strhour: null, strminute: null, meridian: null, dateFieldFormat: null, hFormat: null, mFormat: null, ampmFormat: null,        
        hFieldId: "", mFieldId: "",
        win: null    
    },
    
    init: function (props) {
        if(moGlb.isset(props.ifield)) props.field = '';
        else if(moGlb.isset(props.field)) props.ifield = '';
        moGlb.mergeProperties(moDtmPkr, props);
        
        with(moDtmPkr) {
            setDefaultValues(props);
            
            // window.onkeyup = function (e) {moDtmPkr.onKeyUp(e);}
            
            var size = getSize();prv.w = size.w, prv.h = size.h;
            resize();
                        
            prv.monthW = Math.round((prv.w - (padding * 2) - (space * 4)) / 4);
            prv.monthH = Math.round((prv.h - (padding * 5) - (dayH * 3) - (space * 5)) / 6);
            
            setPickerParts();

            //if(hasWin && !prv.win) prv.win = moComp.createComponent("dtm", "moWindow", {wc: prv.w, hc: prv.h-20}, true);
            var cnv = getCanvas(cnvId);
            if(hasWin && !prv.win) prv.win = moComp.createComponent("dtm", "moPopup", {
                contentid: cnvId, title: moGlb.langTranslate(title), zindex: moGlb.zdtmpicker,                
                width: size.w-4, height: size.h-5
            }, true);
            prv.win.setTitle(title);
            prv.ctx = cnv.getContext('2d');
            
            prv.dateFieldFormat = moGlb.localization.f_datetime;
            if(!timePicker) prv.dateFieldFormat = moGlb.localization.f_date;
            
            prv.selectedDate = new Date();
            prv.selectedDate.setMinutes(0, 0, 0);
            
            var ts = parseInt(ifield?mf$(ifield+"_hidden").value:field.getProperty("value"))*1000;
            if(ts && !isNaN(ts)) {
                with(prv) {
                    selectedDate = new Date(parseInt(ts));
                    selectedMonth = selectedDate.getMonth();
                    selectedYear = selectedDate.getFullYear();
                }                
                setMonth1st(prv.selectedMonth, prv.selectedYear);                
            } else setMonth1st();
            
            if(timePicker) initTime();
        }
    },
    
    setDefaultValues: function (props) {
        with(moDtmPkr) {
            if(!props.datePicker) datePicker = true;
            if(!props.timePicker) timePicker = true;
            if(!props.showWeek) showWeek = false;
            if(!props.weekDays) weekDays = moGlb.weekDays;
            if(!props.months) months = moGlb.months;
            if(!props.weekLabel) weekLabel = moGlb.weekLabel;
            if(!props.weekStartsOn) weekStartsOn = moGlb.weekStartsOn;
            if(!props.monthFormat) monthFormat = "{F} {Y}";
            if(!props.selectedDateFormat) selectedDateFormat = "{D2}, "+moGlb.localization.f_date;
            if(!props.dateTimeFormat) dateTimeFormat = moGlb.localization.f_datetime;
            if(!props.dateFormat) dateFormat = moGlb.localization.f_date;
            if(!props.timeFormat) timeFormat = moGlb.localization.f_time;
            // if(!props.hfield) hfield = mf$(props.field.id + "_hidden");            
        }
    },
    
    initTime: function () {
        with(moDtmPkr) {
            if(prv.dateFieldFormat.indexOf("g") >= 0) prv.hFormat = "{g}";
            else if(prv.dateFieldFormat.indexOf("h") >= 0) prv.hFormat = "{h}";
            else if(prv.dateFieldFormat.indexOf("G") >= 0) prv.hFormat = "{G}";
            else if(prv.dateFieldFormat.indexOf("H") >= 0) prv.hFormat = "{H}";
            
            if(prv.dateFieldFormat.indexOf("i") >= 0) prv.mFormat = "{i}";
            else if(prv.dateFieldFormat.indexOf("I") >= 0) prv.mFormat = "{I}";
            
            if(prv.dateFieldFormat.indexOf("a") >= 0) prv.ampmFormat = "{a}";
            else if(prv.dateFieldFormat.indexOf("A") >= 0) prv.ampmFormat = "{A}";
            else prv.ampmFormat = null;

            if(prv.ampmFormat) prv.meridian = prv.selectedDate.format(prv.ampmFormat);
            else prv.meridian = null;
            
            prv.strhour = prv.selectedDate.format(prv.hFormat);
            prv.strminute = prv.selectedDate.format(prv.mFormat);
        }
    },
    
    setPickerParts: function () {
        with(moDtmPkr) {
            // Header
            var xi = padding, yi = padding, xf = xi + imgW, yf = yi + dayH;            
            prv.prevMonth = {xi: xi, yi: yi, xf: xf, yf: yf};   // Left arrow
            
            var xi = prv.w - padding - imgW, xf = xi + imgW;            
            prv.nextMonth = {xi: xi, yi: yi, xf: xf, yf: yf};   // Right arrow
            
            var xi = prv.prevMonth.xf, xf = prv.nextMonth.xi;
            prv.header = {xi: xi, yi: yi, xf: xf, yf: yf};   // Month name
            
            // Middle
            var xi = prv.prevMonth.xi, yi = prv.prevMonth.yf + padding, xf = prv.nextMonth.xf, yf = yi + (dayH * 7) + (space * 6);
            prv.dayPicker = {xi: xi, yi: yi, xf: xf, yf: yf};
            
            var xf = xi + (prv.monthW * 2) + space;
            prv.monthPicker = {xi: xi, yi: yi, xf: xf, yf: yf};
            
            var xi = xf + (space * 2), xf = prv.nextMonth.xf;
            prv.yearPicker = {xi: xi, yi: yi, xf: xf, yf: yf};
            prv.prevYear = {xi: xi, yi: yi, xf: xi + prv.monthW, yf: yi + prv.monthH};
            prv.nextYear = {xi: xf - prv.monthW, yi: yf - prv.monthH, xf: xf, yf: yf};
            
            // Info
            var xi = prv.prevMonth.xi, xf = xf - imgW, yi = yf + padding, yf = yi + dayH;
            prv.info = {xi: xi, yi: yi, xf: xf, yf: yf};
            
            var xi = xf, xf = xi + imgW;
            prv.today = {xi: xi, yi: yi, xf: xf, yf: yf};
            
            // Actions
            var xi = prv.prevMonth.xi, xf = xi + 24, yi = yf + padding, yf = yi + dayH;
            prv.time = {xi: xi, yi: yi, xf: xf, yf: yf};
            
            var xi = imgW + (space * 3) + (timeW * 2) + (padding * 4), xf = xi + imgW;
            prv.ampm = {xi: xi, yi: yi, xf: xf, yf: yf};
                        
            var xi = prv.dayPicker.xf - (imgW * 2) - padding, xf = xi + imgW;
            prv.cancel = {xi: xi, yi: yi, xf: xf, yf: yf};
            
            var xi = xf + padding, xf = xi + imgW;
            prv.ok = {xi: xi, yi: yi, xf: xf, yf: yf};
        }
    },
    
    isIn: function (x, y, range) {
        return (x > range.xi && x < range.xf) && (y > range.yi && y < range.yf);
    },

    show: function (props) {
        with(moDtmPkr) {
            init(props);
            try{
                mf$(prv.hFieldId).value = prv.strhour;
                mf$(prv.mFieldId).value = prv.strminute;
            } catch(ex) {}            
            //images = moPreload.get("components/dtmpicker", imageNames);
            draw();
            prv.win.show();
        }
    },
    
    showDtmPicker: function (props) {
        props.datePicker = true;
        props.timePicker = true;
        moDtmPkr.show(props);
    },
    
    showDatePicker: function (props) {
        props.datePicker = true;
        props.timePicker = false;
        moDtmPkr.show(props);
    },

    hide: function () {
        with(moDtmPkr) {
            prv.selectedDate = null;
            prv.selectedMonth = null;
            prv.selectedYear = null;                       
            
            if(prv.win) prv.win.hide();
        }        
    },

    // Drawing
    drawBorder: function (x, y, w, h) {
        with(moDtmPkr) {
            prv.ctx.strokeStyle = stroke;
            prv.ctx.fillRect(x + 0.5, y + 0.5, w, h);
        }
    },
    
    drawMonthHeader: function (x, y) {
        with(moDtmPkr) {
            var w = (dayW * 7 + space * 6) + (2 * padding), h = 0,
                txt = prv.month1st.format(monthFormat), txtW = prv.ctx.measureText(txt).width;
//            prv.ctx.drawImage(images[0], x, y);   // Left arrow
            moCnvUtils.cnvIconFont(prv.ctx, 58790, x, y, 16, '#496670');
//            prv.ctx.drawImage(images[1], w - imgW - padding, y); // Right arrow
            moCnvUtils.cnvIconFont(prv.ctx, 58791, w - imgW - padding, y, 16, '#496670');
            
            moCnvUtils.write(prv.ctx, txt, w/2, y + 12, color, monthStyle);
            
            //prv.ctx.drawImage(prv.showMonthPicker ? images[3] : images[2], w/2 + txtW/2 + 7, y); // Expand arrow/Reduce arrow
            moCnvUtils.cnvIconFont(prv.ctx, prv.showMonthPicker ? 58788 : 58789, w/2 + txtW/2 + 7, y, 16, '#496670');
        }
    },

    drawDays: function (x0, y0) {
        with(moDtmPkr) {
            if(!prv.showMonthPicker && mf$(prv.innerCnvId)) slidePicker(false);
            
            var day = new Date(prv.month1st.getFullYear(), prv.month1st.getMonth(), 1);
            day.setDate(1 - ((prv.month1st.getDay() - weekStartsOn) + 7) % 7);
            
            txt = "";
            for(var i=0; i<=dayRows; i++) {
                for(var j=0; j<dayCols; j++) {
                    var x = x0 + (dayW * j) + (space * j);
                    var y = y0 + (dayH * i) + (space * i);
                    var txt = (i > 0 ? day.getDate() : getDayLabel(j, 2));
                    var txtW = prv.ctx.measureText(txt).width;

                    var styleSet = getDayStyleSet(i > 0 ? day : null);
                    
                    prv.ctx.fillStyle = styleSet.bgClr;
                    prv.ctx.fillRect(x, y, dayW, dayH);
                    moCnvUtils.write(prv.ctx, txt, x + dayW/2, y + 14, styleSet.clr, styleSet.stl);
                    
                    if(i > 0) {
                        if(prv.dayXY.length < (dayRows + 1) * dayCols) prv.dayXY[prv.dayXY.length] = {
                            date: day.getDate(),
                            month: day.getMonth(),
                            year: day.getFullYear(),
                            xi: x, yi: y, xf: x + dayW, yf: y + dayH
                        };
                        day.setDate(day.getDate() + 1);            
                    }
                }
            }
        }
    },
    
    drawMonthYearPicker: function (x, y) {
        with(moDtmPkr) {
            prv.innerCnvId = cnvId + "_inner", w = prv.w - 2;
            var cnv = mf$(prv.innerCnvId);
            if(!cnv) cnv = moCnvUtils.createCanvas(prv.innerCnvId, w, 6, "top:"+prv.monthPicker.yi+"px;left:1px;z-index: "+(moGlb.zdtmpicker+1)+"; background-color: #ffffff;", "", mf$(prv.win.getContainerId()));
            prv.innerCtx = cnv.getContext('2d');
            cnv.onclick = function (e) {moDtmPkr.clickDispatcher(e, 1);};
            if(!touch) {
                cnv.onmousemove = function (e) {moDtmPkr.overDispatcher(e, 1);};
                cnv.onmouseover = function (e) {moDtmPkr.setMouseOver(true);};
                cnv.onmouseout = function (e) {moDtmPkr.setMouseOver();};
            }
            
            slidePicker(true);
        }
    },
    
    drawMonthPicker: function (x0, y0) {
       with(moDtmPkr) {
            for(var i=0; i<dayRows; i++) {
                for(var j=0; j<2; j++) {
                    var x = x0 + (prv.monthW * j) + (space * j);
                    var y = y0 + (prv.monthH * i) + (space * i);
                    var mm = i + (j * 6);
                    var txt = getMonthLabel(mm, 3);
                    var txtW = prv.ctx.measureText(txt).width;

                    var styleSet = getMonthStyleSet(mm);
                    
                    //moCnvUtils.drawRect(prv.innerCtx, x, y, prv.monthW, prv.monthH, styleSet.bgClr);
                    prv.innerCtx.fillStyle = styleSet.bgClr;
                    prv.innerCtx.fillRect(x, y, prv.monthW, prv.monthH),
                    moCnvUtils.write(prv.innerCtx, txt, x + prv.monthW/2, y + 16, styleSet.clr, styleSet.stl);
                    
                    if(prv.monthXY.length < dayRows * 4) prv.monthXY[prv.monthXY.length] = {value: mm, xi: x, yi: y + prv.monthPicker.yi, xf: x + prv.monthW, yf: y + prv.monthPicker.yi + prv.monthH};
                }
            }
        }
    },
    
    drawYearPicker: function (x0, y0) {
        with(moDtmPkr) {
            var yyyy = prv.month1st.getFullYear() - 5;
            
            for(var i=0; i<dayRows; i++) {
                for(var j=0; j<2; j++) {
                    var x = x0 + (prv.monthW * j) + (space * j);
                    var y = y0 + (prv.monthH * i) + (space * i);
                    var txt = yyyy + i + (j * 6);
            
                    var styleSet = getYearStyleSet((i == 0 && j == 0) || (i == dayRows - 1 && j == 1) ? null : txt);
                    prv.innerCtx.fillStyle = styleSet.bgClr;
                    prv.innerCtx.fillRect(x, y, prv.monthW, prv.monthH);
                    //moCnvUtils.drawRect(prv.innerCtx, x, y, prv.monthW, prv.monthH, styleSet.bgClr);
                        
                    if(i == 0 && j == 0) {  // Left arrow
                        //prv.innerCtx.drawImage(images[0], x, y + (prv.monthH/2 - imgH/2));
                        moCnvUtils.cnvIconFont(prv.innerCtx, 58790, x, y + (prv.monthH/2 - imgH/2), 16, '#496670');
                    } else if(i == dayRows - 1 && j == 1) { // Right arrow
                        //prv.innerCtx.drawImage(images[1], x + (prv.monthW - imgW), y + (prv.monthH/2 - imgH/2));
                        moCnvUtils.cnvIconFont(prv.innerCtx, 58791, x, y + (prv.monthH/2 - imgH/2), 16, '#496670');
                    } else {
                        var txtW = prv.innerCtx.measureText(txt).width;                        
                        moCnvUtils.write(prv.innerCtx, txt, x + prv.monthW/2, y + 16, styleSet.clr, styleSet.stl);
                        if(prv.yearXY.length < dayRows * 4) prv.yearXY[prv.yearXY.length] = {value: txt, xi: x, yi: y + prv.monthPicker.yi, xf: x + prv.monthW, yf: y + prv.monthPicker.yi + prv.monthH};
                    }
                }
            }
        }
    },
    
    drawInfo: function (x, y, w) {
        with(moDtmPkr) {
            if(prv.selectedDate) {
                var t_date = prv.selectedDate.format(selectedDateFormat),
                    week = prv.selectedDate.getWeek();
                moCnvUtils.write(prv.ctx, t_date + " [" + weekLabel.substr(0, 1) + " " + week + "]", x + 2, y + 13, color, style, "left");
                //prv.ctx.drawImage(images[4], w - imgW, y); // Select today
                moCnvUtils.cnvIconFont(prv.ctx, 58486, w-imgW, y, 16, '#496670');
            }
        }
    },
    
    drawActions: function (x, y, w) {
        with(moDtmPkr) {
//            prv.ctx.drawImage(images[5], w - (imgW * 2) - padding, y); // Cancel
//            prv.ctx.drawImage(images[6], w - imgW, y); // Ok
            moCnvUtils.cnvIconFont(prv.ctx, 58807, w - (imgW * 2) - padding*2, y, 16, '#496670');
            moCnvUtils.cnvIconFont(prv.ctx, 58806, w - imgW - padding, y, 16, '#496670');
            
            if(timePicker) drawTimePicker(prv.time.xi, prv.time.yi, prv.time.xf);
        }
    },
    
    drawTimePicker: function (x, y, w) {
        with(moDtmPkr) {
//            prv.ctx.drawImage(images[7], x, y); // Clock icon
            moCnvUtils.cnvIconFont(prv.ctx, 58606, x, y, 16, '#496670');
            
            x += imgW + (space * 2);
            var hf = createTimeField("hour", x, y-2, prv.strhour);
            prv.hFieldId = hf.getAttribute("id");
            
            x += timeW + padding;
            moCnvUtils.write(prv.ctx, ":", x, y+13, color, style, "left");
            
            x += padding;
            var mf = createTimeField("minute", x, y-2, prv.strminute);
            prv.mFieldId = mf.getAttribute("id");
            
            if(prv.meridian != null) moCnvUtils.write(prv.ctx, prv.meridian, prv.ampm.xi, y+13, color, style, "left");
        }
    },
    
    draw: function () {
        with(moDtmPkr) {
            prv.ctx.clearRect(0, 0, prv.w, prv.h);            
            var x, y;
            
            x = 0, y = 0;
            // drawBorder(0, 0, prv.w - 1, prv.h - 1);

            x += padding, y += padding;
            drawMonthHeader(x, y);

            y += dayH + padding;
            
            dayXY = [];
            drawDays(x, y);
            if(prv.showMonthPicker) drawMonthYearPicker(x, y);

            y += (dayH * 7) + (space * 6) + padding;
            drawInfo(x, y, prv.w - padding);

            y += dayH + padding;
            drawActions(x, y, prv.w - padding);
        }
    },

    redraw: function () {
        with(moDtmPkr) {
            prv.dayXY = [];
            prv.monthXY = [];
            prv.yearXY = [];
            
            prv.ctx.clearRect(0, 0, prv.w, prv.h);
            draw();
        }        
    },
    
    // Event manager
//    onKeyUp: function (e) {
//        with(moDtmPkr) {if(e.keyCode == 27) hide();}
//    },
    
    onHourTyped: function () {
        with(moDtmPkr) {prv.strhour = mf$(prv.hFieldId).value;}
    },
    
    onMinuteTyped: function (e) {
        with(moDtmPkr) {
            prv.strminute = mf$(prv.mFieldId).value;
            if(e.keyCode == 9) { // Tab
                setTimeout("mf$('"+prv.hFieldId+"').focus()", 250);
            }
        }
    },
    
    onHourChange: function () {
        with(moDtmPkr) {
            if(prv.strhour != "") {
                if(prv.meridian != null && moCheck.check(prv.strhour, "range_1_12") == 1) {
                    prv.selectedDate.set12Hours(prv.strhour, prv.meridian.toUpperCase() == "PM");
                } else if(prv.meridian == null && moCheck.check(prv.strhour, "range_0_24") == 1) {
                    prv.selectedDate.setHours(prv.strhour);
                }
                prv.strhour = prv.selectedDate.format(prv.hFormat);
            }
            
            mf$(prv.hFieldId).value = prv.strhour;
        }
    },
    
    onMinuteChange: function () {
        with(moDtmPkr) {                
           if(prv.strminute != "" && moCheck.check(prv.strminute, "range_0_59") == 1)
                prv.selectedDate.setMinutes(prv.strminute);
            prv.strminute = prv.selectedDate.format(prv.mFormat);
            mf$(prv.mFieldId).value = prv.strminute;
        }
    },
        
    setMouseOver: function (over) {
        with(moDtmPkr.prv) {
            mouseOver = (sliding ? false : (over ? true : false));
        }
    },
    
    clickDispatcher: function (e) {
        with(moDtmPkr) {eventDispatcher(e, true);}
    },
    
    overDispatcher: function (e) {
        with(moDtmPkr) {if(prv.mouseOver) eventDispatcher(e);}
    },
    
    eventDispatcher: function (e, click) {
        with(moDtmPkr) {
            var coords = moEvtMng.getMouseXY(e, mf$(prv.win.getContainerId()));
            setCursorPointer(false);
            
            with(coords) {
                // Reset data
                if(!prv.showMonthPicker && !isIn(x, y, prv.dayPicker)) prv.overDate = null;
                if(prv.showMonthPicker && !isIn(x, y, prv.monthPicker)) prv.overMonth = null;
                if(prv.showMonthPicker && !isIn(x, y, prv.yearPicker)) prv.overYear = null;

                if(click) {
                    if(isIn(x, y, prv.header)) return changePicker();
                    else if(isIn(x, y, prv.prevMonth)) return changeMonth();
                    else if(isIn(x, y, prv.nextMonth)) return changeMonth(true);
                    else if(prv.showMonthPicker && isIn(x, y, prv.prevYear)) return changeYear();
                    else if(prv.showMonthPicker && isIn(x, y, prv.nextYear)) return changeYear(true);
                    else if(isIn(x, y, prv.today)) return selectToday(); 
                    else if(isIn(x, y, prv.time)) return selectNow();
                    else if(isIn(x, y, prv.ampm)) return selectMeridian();
                    else if(isIn(x, y, prv.cancel)) return cancelSelection();
                    else if(isIn(x, y, prv.ok)) return confirmSelection();
                } else { // Pointer cursor
                    if(isIn(x, y, prv.header)) return setCursorPointer(true);
                    else if(isIn(x, y, prv.prevMonth)) return setCursorPointer(true);
                    else if(isIn(x, y, prv.nextMonth)) return setCursorPointer(true);
                    else if(prv.showMonthPicker && isIn(x, y, prv.prevYear)) setCursorPointer(true);
                    else if(prv.showMonthPicker && isIn(x, y, prv.nextYear)) setCursorPointer(true);
                    else if(isIn(x, y, prv.today)) return setCursorPointer(true);
                    else if(isIn(x, y, prv.time)) return setCursorPointer(true);
                    else if(isIn(x, y, prv.ampm)) return setCursorPointer(true);
                    else if(isIn(x, y, prv.cancel)) return setCursorPointer(true);
                    else if(isIn(x, y, prv.ok)) return setCursorPointer(true);
                }

                if(!prv.showMonthPicker && isIn(x, y, prv.dayPicker)) return dispatchDay(x, y, click);
                else if(prv.showMonthPicker && isIn(x, y, prv.monthPicker)) return dispatchMonth(x, y, click);
                else if(prv.showMonthPicker && isIn(x, y, prv.yearPicker)) return dispatchYear(x, y, click);
            }
            
            redraw();
        }
    },
    
    changePicker: function () {
        with(moDtmPkr) {
            with(prv) {showMonthPicker = !showMonthPicker;}
            if(!prv.showMonthPicker) setMonth1st(prv.selectedMonth, prv.selectedYear);
            redraw();
        }
    },
    
    changeMonth: function (next) {
        with(moDtmPkr) {
            var d = new Date(prv.month1st.getFullYear(), prv.month1st.getMonth(), 1);
            d.setMonth(d.getMonth() + (next ? 1 : -1));
            setMonth1st(d.getMonth(), d.getFullYear());
            redraw();
        }
    },
    
    changeYear: function (next) {
        with(moDtmPkr) {
            var yr = prv.month1st.getFullYear();
            if(next) {
                yr = yr + 10;
                if(yr > 2033) yr = 2033;
            } else {
                yr = yr - 10;
                if(yr < 1906) yr = 1906;
            }
            setMonth1st(prv.month1st.getMonth(), yr);
            redraw();
        }
    },
    
    selectToday: function () {
        with(moDtmPkr) {
            var hours = prv.selectedDate.getHours();
            var minutes = prv.selectedDate.getMinutes();
            prv.selectedDate = new Date();
            prv.selectedDate.setHours(hours);
            prv.selectedDate.setMinutes(minutes);
            setMonth1st(prv.selectedDate.getMonth(), prv.selectedDate.getFullYear());
            prv.showMonthPicker = false;
            redraw();
        }
    },
    
    selectNow: function () {
        with(moDtmPkr) {
            with(prv) {
                var now = new Date();
                prv.strhour = now.format(prv.hFormat);
                prv.strminute = now.format(prv.mFormat);
                if(prv.ampmFormat) prv.meridian = now.format(prv.ampmFormat);
                prv.selectedDate.setHours(now.getHours());
                prv.selectedDate.setMinutes(now.getMinutes());
            }
            redraw();
        }
    },
    
    selectMeridian: function () {
        with(moDtmPkr) {
            switch(prv.meridian) {
                case "am":prv.meridian = "pm";break;
                case "AM":prv.meridian = "PM";break;
                case "pm":prv.meridian = "am";break;
                case "PM":prv.meridian = "AM";break;
                default:prv.meridian = null;
            }
            
            onHourChange();
            redraw();
        }
    },
    
    cancelSelection: function () {
        with(moDtmPkr) {
            with(prv) {selectedDate = "";selectedHours = "";selectedMinutes = "";}
            setField();hide();
        }
    },
    
    confirmSelection: function () {
        with(moDtmPkr) {
            if(!prv.selectedDate) prv.selectedDate = new Date();
            setField();hide();
        }
    },
    
    setField: function () {
        with(moDtmPkr) {
          try{  mf$(ifield?ifield:field.fieldid).focus(); }catch(e){}
            if(ifield) {
                mf$(ifield).value = prv.selectedDate ? prv.selectedDate.format("{d}/{m}/{Y}, {H}:{i}") : "";
                mf$(ifield+"_hidden").value = prv.selectedDate ? parseInt(prv.selectedDate.format("{U}")) : "";
            } else {
                field.setProperties({
                    value: prv.selectedDate ? parseInt(prv.selectedDate.format("{U}")) : "",
                    description: prv.selectedDate ? prv.selectedDate.format(prv.dateFieldFormat) : ""
                });
            }
        }
    },
    
    dispatchDay: function (x, y, click) {
        with(moDtmPkr) {
            for(var i=0; i<prv.dayXY.length; i++)
                if(isIn(x, y, prv.dayXY[i])) {
                    if(click) {
                        var sDate = new Date(prv.dayXY[i].year, prv.dayXY[i].month, prv.dayXY[i].date, prv.selectedDate.getHours(), prv.selectedDate.getMinutes());
                        if(!prv.selectedDate || !prv.selectedDate.hasSameDate(sDate)) {
                            prv.selectedDate = sDate;
                            prv.selectedMonth = prv.selectedDate.getMonth();
                            prv.selectedYear = prv.selectedDate.getFullYear();                            
                            setMonth1st(prv.selectedDate.getMonth(), prv.selectedDate.getFullYear());
                            redraw();
                        }
                    } else {
                        var oDate = new Date(prv.dayXY[i].year, prv.dayXY[i].month, prv.dayXY[i].date);
                        if(!prv.overDate || !prv.overDate.hasSameDate(oDate)) {
                            prv.overDate = new Date(prv.dayXY[i].year, prv.dayXY[i].month, prv.dayXY[i].date);
                            redraw();
                        }
                    }
                    break;
                }
        }        
    },
    
    dispatchMonth: function (x, y, click) {
        with(moDtmPkr) {
            for(var i=0; i<prv.monthXY.length; i++)
                if(isIn(x, y, prv.monthXY[i])) {
                    if(click) {
                        if(prv.selectedMonth != prv.monthXY[i].value) {
                            prv.selectedMonth = prv.monthXY[i].value;
                            // setMonth1st();
                            redraw();
                        }
                    } else {
                        if(prv.overMonth != prv.monthXY[i].value) {
                            prv.overMonth = prv.monthXY[i].value;
                            redraw();
                        }
                    }
                    break;
                }
        }
    },
    
    dispatchYear: function (x, y, click) {
        with(moDtmPkr) {
            for(var i=0; i<prv.yearXY.length; i++)
                if(isIn(x, y, prv.yearXY[i])) {
                    if(click) {
                        if(prv.selectedYear != prv.yearXY[i].value) {
                            prv.selectedYear = prv.yearXY[i].value;
                            // setMonth1st();
                            redraw();
                        }
                    } else {
                        if(prv.overYear != prv.yearXY[i].value) {
                            prv.overYear = prv.yearXY[i].value;
                            redraw();
                        }
                    }
                    break;
                }
        }
    },
    
    // Animation and features
    slidePicker: function (down) {
        with(moDtmPkr) {
            var cnv = mf$(prv.innerCnvId), h = parseInt(cnv.height), hf = prv.monthPicker.yf - prv.monthPicker.yi;
            h += 15 * (down ? 1 : -1);
            if(down) {
                if(h > hf) h = hf;
            } else if(h < 0) {
                h = 0;
                prv.innerCtx.clearRect(0, 0, prv.w, prv.monthPicker.yf - prv.monthPicker.yi);
                //document.body.removeChild(mf$(prv.innerCnvId));
            }            
            
            cnv.height = h;
            var ty = h - hf;
            prv.innerCtx.translate(0, ty);
            drawMonthPicker(prv.monthPicker.xi - 1, 0);
            drawYearPicker(prv.yearPicker.xi - 1, 0);
            if((down && h < hf) || (!down && h > 0)) {
                prv.sliding = true;
                //setTimeout(slidePicker, 40, down);
                setTimeout("moDtmPkr.slidePicker("+down+")", 40);
            } else {
                prv.sliding = false;
            }
        }
    },
    
    setCursorPointer: function (over) {
        document.body.style.cursor = over ? "pointer" : "default";
    },
    
    getDayLabel: function (wday, length) {
        with(moDtmPkr) {
            return weekDays[(wday + weekStartsOn) % dayCols].substr(0, (length ? length : null));
            }
    },
    
    getMonthLabel: function (monthIndex, length) {
        with(moDtmPkr) {
            return months[monthIndex].substr(0, (length ? length : null));
            }
    },
    
    getDayStyleSet: function (day) {
        with(moDtmPkr) {
            if(!day) return {bgClr: bgColor, clr: color, stl: style};
            if(prv.selectedDate && day.hasSameDate(prv.selectedDate)) return {bgClr: selectedBgColor, clr: selectedColor, stl: selectedStyle};
            if(day.hasSameDate(new Date())) return {bgClr: todayBgColor, clr: todayColor, stl: todayStyle};
            if(prv.overDate && day.hasSameDate(prv.overDate)) return {bgClr: overBgColor, clr: overColor, stl: overStyle};
            if(showWeek && prv.selectedDate && prv.selectedDate.getFullYear() == day.getFullYear() && day.hasSameWeek(prv.selectedDate)) return {bgClr: dWeekBgColor, clr: dWeekColor, stl: dWeekStyle};
            if(day.hasSameMonth(prv.month1st)) return {bgClr: dMonthBgColor, clr: dMonthColor, stl: dMonthStyle};
            return {bgClr: dBgColor, clr: dColor, stl: dStyle};
        }
    },
    
    getMonthStyleSet: function (mm) {
        with(moDtmPkr) {
            if(prv.selectedMonth != null && prv.selectedMonth == mm) return {bgClr: selectedBgColor, clr: selectedColor, stl: selectedStyle};
            if(prv.overMonth != null && prv.overMonth == mm) return {bgClr: overBgColor, clr: overColor, stl: overStyle};
            return {bgClr: dMonthBgColor, clr: dMonthColor, stl: dMonthStyle};
        }
    },
    
    getYearStyleSet: function (yyyy) {
        with(moDtmPkr) {
            if(!yyyy) return {bgClr: bgColor, clr: color, stl: style};
            if(prv.selectedYear != null && prv.selectedYear == yyyy) return {bgClr: selectedBgColor, clr: selectedColor, stl: selectedStyle};
            if(prv.overYear != null && prv.overYear == yyyy) return {bgClr: overBgColor, clr: overColor, stl: overStyle};
            return {bgClr: dMonthBgColor, clr: dMonthColor, stl: dMonthStyle};
        }
    },

    getStartingDay: function (date) {
        with(moDtmPkr) {
            monthFirst.setDate(prv.monthFirst.getDate() - prv.monthFirst.getDay() + weekStartsOn);
            return prv.monthFirst.getDate();
        }
    },

    setMonth1st: function (mn, yr) {
        with(moDtmPkr) {
            var now = new Date();
            if(mn == null) mn = now.getMonth();
            if(yr == null) yr = now.getFullYear();
            prv.month1st = new Date(yr, mn, 1);
        }
    },
    
    getCanvas: function (cnvId) {
        with(moDtmPkr) {
            var cnv = mf$(cnvId);
            //if(!cnv) cnv = moCnvUtils.createCanvas(cnvId, prv.w, prv.h, "top:0px;left:0px;z-index: "+moGlb.zpopup + "; background-color: #ffffff;", className, mf$(prv.win.getContainerId()));
            if(!cnv) cnv = moCnvUtils.createCanvas(cnvId, prv.w, prv.h, "top:0px;left:0px;z-index: "+moGlb.zdtmpicker + "; background-color: #ffffff;", className);
            else with(cnv.style) top = top+"px";left = left+"px";
            
            cnv.onclick = function (e) {moDtmPkr.clickDispatcher(e);};
            if(!touch) {
                cnv.onmousemove = function (e) {moDtmPkr.overDispatcher(e);};
                cnv.onmouseover = function (e) {moDtmPkr.setMouseOver(true);};
                cnv.onmouseout = function (e) {moDtmPkr.setMouseOver();};
            }
            
            return cnv;
        }
    },
    
    createTimeField: function (type, x, y, value) {
        with(moDtmPkr) {
            var fld = mf$(type + "_input_field");
            if(!fld) {
                var fld = moGlb.createElement(type + "_input_field", "input", mf$(prv.win.getContainerId()));
                fld.setAttribute("type", "text");
                fld.setAttribute("size", 2);
                fld.setAttribute("maxlength", 2);
                if(value) fld.value = value;
                //fld.setAttribute("class", "mbody component field");
                fld.setAttribute("style", "position: absolute; top:"+(y+1)+"px;left:"+x+"px;px; z-index: " + parseInt(moGlb.zdtmpicker + 1) + "; width: " + timeW + "px; height: " + timeH + "px; text-align: center;border: 1px solid #60828D; font: 15px opensansR;");

                if(type == "hour") {
                    fld.onkeyup = function (e) {moDtmPkr.onHourTyped();};
                    fld.onmouseup = function (e) {moDtmPkr.onHourTyped();};
                    fld.onchange = function (e) {moDtmPkr.onHourChange();};
                } else {
                    fld.onkeyup = function (e) {moDtmPkr.onMinuteTyped(e);};
                    fld.onmouseup = function (e) {moDtmPkr.onMinuteTyped(e);};
                    fld.onchange= function (e) {moDtmPkr.onMinuteChange();};
                }
            }
            return fld;
        }
    },
    
    getSize: function () {
        with(moDtmPkr) {
            return {
                w: (dayW * 7 + space * 6) + (padding * 2) + 1,
                h: (dayH * 7 + space * 6) + (dayH * 3) + (padding * 5) + 1
            };
        }
    },
    
    getPosition: function () {
        with(moDtmPkr) {
            return {
                t: moGlb.getClientHeight()/2 - getSize().h/2,
                l: moGlb.getClientWidth()/2 - getSize().w/2
            };
        }
    },
    
    resize: function () {
        with(moDtmPkr) {
            top = moGlb.getClientHeight()/2 - prv.h/2;
            left = moGlb.getClientWidth()/2 - prv.w/2;
        }
    }
};