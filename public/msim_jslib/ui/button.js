function moButton(name) {    
    this.w = 0, this.h = 28, this.top = 0, this.left = 0, this.visible = true, this.type = "moButton",
    this.cnv = null, this.cnvm = null, this.containerId = "",
    this.iconon = "", this.iconoff = "", this.label = "", this.action = "", this.menu = "", this.bmenu = "", this.clabel = "",
    this.bWidth = 1, this. bColor = moColors.btnBorder, this.bRadius = 2, this.componentBox = "",
    this.pname = '', this.groupName = "msim-default", this.componentBox = '', this.onload = "", // Compliancy with ui
    
    this.bg = "#A1C24D",           //moColors.bg, 
    this.bgOver = "#E0E0E0",       //moColors.bgOver, 
    this.bgSelected = "#6F8D97",   // moColors.bgSelected, 
    this.bgDisabled = "#F0F0F0",   // moColors.bgDisabled, 
    
    this.font = moGlb.fontNormal, 
    this.color = "#ffffff",         // moColors.text, 
    this.colorOver = "#888888",     // moColors.textOver, 
    this.colorPressed = "#ffffff",  //moColors.textOver, 
    this.colorDisabled = "#A0A0A0", //moColors.textDisabled,
    
    this.hPadding = 5, this.iconH = 16, this.arrowW = 12, this.on = true, this.disabled = false, this.zindex = 1;
    this.name = name, this.click = false, this.over = false, this.icons = [], this.level = -1;
    
    this.create = function (props) {
        if(!this.visible) return; moGlb.mergeProperties(this, props);        
        with(this) {
            if(!onload && moGlb.isset(moLibrary[name+"_onload"])) onload = name+"_onload";
            label = moGlb.langTranslate(label);
            var tw = moCnvUtils.measureDivText(label, font), tmpw = tw + (hPadding*2) + (iconon || iconoff ? iconH+(label != "" ? hPadding : 0) : 0) + (menu != "" ? hPadding+arrowW : 0);
            if(!w) w=tmpw;
            else if(tw > w-hPadding*2) {
                clabel = "";
                for(var i=0; i<label.length; i++) {
                    if(moCnvUtils.measureDivText(clabel + label[i] + "...", font) < w-hPadding*2) clabel += label[i];
                    else {clabel += "...";break;}
                }
            }
            icons=[ iconon, iconoff ];
        }
    };
    
this.display = function (l, t, cid) {
  with(this) {
            top = t, left = l, containerId = cid;
            try {with(mf$(name+"_cnv").style) {top=t+"px"; left=l+"px";}
            mf$(cid).appendChild(mf$(name+"_cnv"));} catch(ex) {}            
            draw(1);  moLibrary.exec(onload, this);
}};
    
this.createCanvas = function () {
  with(this) {
            var l,t;
            
            if(cnv) return;
            cnv=moGlb.createElement(name + "_cnv","div",mf$(containerId));
            
            l=left, t=top;
            
            with(cnv.style){
              left=l+"px";
              top=t+"px";
              width=w+"px";
              height=h+"px";
              zIndex=zindex;
              borderRadius="2px";
              overflow="hidden";
            }
 
            if(clabel) cnv.setAttribute("title", label);

}};
    
   
    
this.draw=function(ii){
with(this) {
    if(typeof(ii)=="undefined") { ii=0; }
    
    if(!ii && !mf$("idbuttonicon_"+name)) ii=1;
    
    if(ii) { createCanvas(); cnv.innerHTML=""; } 
    
    var stl=getStyle(over, click, this);
    
    cnv.style.backgroundColor=stl.bg;
         
      if(this.icons.length>0) {
        var arr_flt, flt=(!on && !moGlb.isempty(icons[1])) ? icons[1] : icons[0];
            arr_flt = flt.split(".");

        if(ii){              
              if(arr_flt[0]=="FlaticonsStroke") {
                var x=hPadding, y=3;
                cnv.innerHTML+="<div id='idbuttonicon_"+name+"' style='position:absolute;left:"+x+
                               "px;top:"+y+"px;font:16px FlaticonsStroke;color:"+stl.color+";'>"+String.fromCharCode(arr_flt[1])+"</div>";               
              } 
        }else{
            var jfl=mf$("idbuttonicon_"+name);
            jfl.style.color=stl.color;
            if(arr_flt[0]=="FlaticonsStroke") jfl.innerHTML=String.fromCharCode(arr_flt[1]);        
        }
      }
      
  if(label) {
   if(ii){                                    
    
      var s=moCnvUtils.writeDiv(clabel?clabel:label, (!icons.length && !menu ? w/2 : (icons.length>0 ? iconH+(hPadding*2) : hPadding)), 
            hPadding, stl.color, font, (!icons.length && !menu?"center":"left"), "idbuttontextlbl_"+name);
      cnv.innerHTML+=s; 
    
   } else {
    
      mf$("idbuttontextlbl_"+name).style.color=stl.color;
   }
  } 
   if(menu!="") {
      if(ii){ 
        var x=w-hPadding-arrowW, y=7;
        cnv.innerHTML+="<div id='idbuttonarrow_"+name+"' style='position:absolute;left:"+x+"px;top:"+y+"px;font:12px FlaticonsStroke;color:"+stl.color+";'>"+String.fromCharCode(58789)+"</div>";
      }else{
        mf$("idbuttonarrow_"+name).style.color=stl.color;
      }                 
   }


       
  if(ii){      
     cnv.innerHTML+="<div id='idbuttonmask_"+name+"' style='position:absolute;left:0;top:0;width:100%;height:100%;background-color:#00F;opacity:0.0;z-index:2;cursor:pointer;'></div>";       
     this.cnvm=mf$("idbuttonmask_"+name);        
            
           // events  
     if(!isTouch){  
         cnvm.onmousedown = function() { if(disabled) return false; click=true; draw(0); /*return false;*/ }
         cnvm.onmouseup = function() { f_btn_action(); }
         
         cnvm.onmouseout = function() {if(disabled) return; over=false; draw(0);}
         cnvm.onmouseover = function() {if(disabled) return; over=true; draw(0);}
  
     } else {  // touch
              
         cnvm.onclick = function() { f_btn_action(); }
     }         
  }        
            
}};
    
    
this.f_btn_action=function () {
with(this) {
  if(disabled) return false;
  over=true;
  click=true;
  draw(0);
                    
  over=false;
  click=false;
  setTimeout(name+".draw(0);",350);
                    
  if(action) {
    console.log(name+'_before_fn');
    window.settings = {
        beforeFnName: name+'_before_fn'
    }
    var fn = window[settings.beforeFnName]; 
    if(typeof fn == 'function') {
      fn()
    }
    if(typeof action == "function") action();
    else moLibrary.exec(action, this);
    setTimeout(function() {
      console.log(name+'_after_fn');
      window.settings = {
          afterFnName: name+'_after_fn'
      }
      var fn = window[settings.afterFnName]; 
      if(typeof fn == 'function') {
        fn()
      }
    }, 1500)
  } else if(menu != "") {
    var bpos = moGlb.getPosition(cnv);
    moMenu.Start(menu, {l: bpos.l, t: bpos.t, w: bpos.w, h: bpos.h}, 2);
         }
               
  if(typeof icons != "undefined" && icons.length > 1) { 
    on = !on;
    draw(0);
  }     
}}      
    
    
this.getStyle=function(over, click, props) {
with(this) {
  with(props) {
    if(disabled) return {bg: bgDisabled, border: bColor, color: colorDisabled};
    if(click) return {bg: bgSelected, border: bColor, color: colorPressed};
    if(over) return {bg: bgOver, border: bColor, color: colorOver};
    return {bg: bg, border: bColor, color: color};                
  }
}}     
    
    
    
    
    this.hide = function (hidden) { with(this) { if(cnv) cnv.style.display = (hidden ? "none" : "block"); } };
    this.setVisibility = function () {
        with(this) {
            if(!moGlb.isempty(componentBox)) {
                moGlb[(isVisible()?"remove":"add")+"Class"](mf$(componentBox), "hide");
                eval(pname).setFieldsetVisibility(groupName);
            }
        }
    };
    this.getPosition = function () {return moGlb.getPosition(this.cnv);};
    this.setProperties = function (props) {for(var i in props) this[i] = props[i]; with(this) if(containerId) draw(0);};
    this.getProperty = function (p) {try {return this[p];} catch(ex) {moDebug.log("Button "+this.name+" hasn't got any property called "+p);}};
    this.setData = function (data) {};
    this.getData = function () {};
    this.resize = function () { };
    this.isVisible = function () {with(this) return visible && (moGlb.user.level & level);};
    
    // Functions for compliancy with input fields when a button is in a component module
    this.consistencyCheck = function (batchMode) { return true; };
    this.reset = function () {};
    this.isBatchable = function () { return false; };
};