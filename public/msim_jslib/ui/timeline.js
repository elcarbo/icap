// timeline

// EditMode   0 disable
// DataMode   0 PM    1 WO
// Module     treegrid module name


function moTimeline(nm,z) {


// props
this.name=nm;

var props={ modTG:"", TGJ:null, Vscr:null, Oscr:null,
            Pila:[], PSCD:[],
            jj:null, J:null, W:0, H:0, zL:0, zT:0, addStp:0, OH:0, oh:0, KK:1, SH:0, Huw:1000, Wsc:0, PXS:0, PAGE:0, NPAGE:0, SC0:0, W0:0, W1:0, Yi:0,
             mgL:0, mgT:0, mgR:0, mgB:0, HeadH:24, VW:18, infW:10,                                                    
            CNV:null, CNJ:null, BDN:null, BDJ:null, MBDJ:null, OBDJ:null, IFGRJ:null, FRTXJ:null, maskj:null, TLTPJ:null, WTP:0,                                             
            optr:0, displ:1, Tstat:0, 
            EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove', OM:"", OU:"", FD:{},
            OVR:-2, Clc:0, Xps:0, Yps:0, oXps:0, oYps:0, VR0:[],VR1:[], ZOOM:0, ZM:0, TR:1, nsteps:3,
            GRD:1, LDBJ:null, nodrw:0, MAXLoad:150, AUpdt:-1, seCons:1, seMed:-1, Redu:1, PAN:0,
            EditMode:1, DataMode:0,
            XPTR:0, Yy:0,
            DST:[0,1,0,1332633600,1351386000]                                              // ora legale   "25/03/2012 02:00","28/10/2012 02:00"  
}

for(var i in props) this[i]=(typeof z[i]=='undefined')?props[i]:z[i];

this.JTM=[];
this.toLD=[];
this.toLDY=[];
this.FCLoaded=[];
this.TIme=[];
this.PLB=0;
this.retBR=0;
this.nodisp=0;
this.NLoad=0;
this.aLOad=[];
this.Med=[0,0];
this.idEDIT=[0,0];         //  [0] f_code     [1] idn in JTM[fc][1][idn]
this.Xx=[0,0,0,0,0]; 
this.XXP=[];
this.UndoStack=[];

this.IM=moGlb.baseUrl+"/public/msim_images/default/";
this.Yt=0;                          // posizione top Y nel canvas body alto SH  

this.colrs=["#FFFFFF","#A1C24D","#4A6671","#6F8D97","#E2EECA","#E0E0E0","#A0A0A0","#F4F4F4","rgba(161,194,77,0.3)"]; 

this.Fmenu=[ {Txt:moGlb.langTranslate("Availability"), Type:2, Status:1, Icon:['','checked'], Fnc:this.name+".chgDisp()" },                                                // ## 
             {Txt:moGlb.langTranslate("Grid"), Type:2, Icon:['checked',''], Fnc:this.name+".GRD*=-1; "+this.name+".draw();" },
             {Txt:moGlb.langTranslate("Final"), Type:2,  Icon:['checked',''], Fnc:this.name+".seCons*=-1; "+this.name+".draw();" },         
             {Txt:moGlb.langTranslate("Average"), Type:2,   Icon:['','checked'], Fnc:this.name+".seMed*=-1;"+this.name+".draw();" },   
             {Type:4 },  
             {Txt:moGlb.langTranslate("Scale up"),  Type:0, Fnc:this.name+".changeScaleAnim(1,0)"},
             {Txt:moGlb.langTranslate("Scale down"),  Type:0, Fnc:this.name+".changeScaleAnim(-1,0)"},
             {Txt:moGlb.langTranslate("Year"),  Type:1,  Sub:[] },
             {Type:4 },
             {Txt:moGlb.langTranslate("Auto refresh"), Type:2,  Status:0, Icon:['','checked'], Fnc:this.name+".AUpdt*=-1;" },
             {Txt:moGlb.langTranslate("Reload Timeline"), Type:0,  Fnc:this.name+".reLoad();" },
             {Txt:moGlb.langTranslate("Fast view"), Type:2, Icon:['','checked'],Fnc:this.name+".Redu*=-1; "+this.name+".draw();" },
             {Type:4 },
             {Txt:moGlb.langTranslate("Delete"), Type:0, Disable:1, Fnc:this.name+".f_delete();" },
             {Txt:moGlb.langTranslate("Undo"), Type:0, Disable:1, Fnc:this.name+".f_undo();" }
             
           ];




// scala
this.SCALE_INI=0;    // 1327964400 => 1/1/2012 00:00
this.SCALE_END=0;
this.TIMERANGE=0;
this.ANNO=0;
this.TODAY=0;
                                                     // 0:15min   1:ore    2:giorn   3:[week]   4:[mmesi]  5:[qtr]  6:[SCALE_INI, SCALE_END]               
this.arrstep=[900,3600,86400,[],[],[],[] ];                // secondi      604800 ,  2678400,10713600, 31622400


 //          0:15min     1:ore    2:giorn    3:week    4:[mmesi]    5:[qtr]     6:[SCALE_INI, SCALE_END]
this.arrscale=[900,     3600,      86400,    604800,    2678400,    7905600,    31622400 ];

this.scale=3; 


this.giorni=[moGlb.langTranslate("Sunday"),moGlb.langTranslate("Monday"),moGlb.langTranslate("Tuesday"),
    moGlb.langTranslate("Wednesday"), moGlb.langTranslate("Thursday"), moGlb.langTranslate("Friday"),
    moGlb.langTranslate("Saturday"), moGlb.langTranslate("Sunday")]; 
this.abbgiorni=[moGlb.langTranslate("Sun"),moGlb.langTranslate("Mon"), moGlb.langTranslate("Tue"),
    moGlb.langTranslate("Wed"), moGlb.langTranslate("Thu"), moGlb.langTranslate("Fri"),
    moGlb.langTranslate("Sat"), moGlb.langTranslate("Sun")]; 

this.mesi=["",moGlb.langTranslate("January"), moGlb.langTranslate("February"), moGlb.langTranslate("March"),
    moGlb.langTranslate("April"), moGlb.langTranslate("May"), moGlb.langTranslate("June"),
    moGlb.langTranslate("July"), moGlb.langTranslate("August"), moGlb.langTranslate("September"),
    moGlb.langTranslate("October"), moGlb.langTranslate("November"), moGlb.langTranslate("December") ]; 

this.abbmesi=["",moGlb.langTranslate("Jan"), moGlb.langTranslate("Feb"), moGlb.langTranslate("Mar"),
    moGlb.langTranslate("Apr"), moGlb.langTranslate("May"), moGlb.langTranslate("Jun"), 
    moGlb.langTranslate("Jul"), moGlb.langTranslate("Aug"),
    moGlb.langTranslate("Sep"), moGlb.langTranslate("Oct"), moGlb.langTranslate("Nov"),
    moGlb.langTranslate("Dec")]; 

this.weekstart=1;
this.WeekPHASE=0;
this.NWEEK0=0;

 
this.TypeColor=['#dbe9bf','#fb9999','#fb99ff'];      // ,'#c5dc97'
this.aBlns=[];




// metodi
this.resize=resize;
this.postSize=postSize;
this.Display=Display; 
this.resize_animation=resize_animation;
this.predraw=predraw;
this.draw0=draw0;
this.reLoad=reLoad;
this.retoLD=retoLD;
this.delOverLoad=delOverLoad;

this.findPageScale0=findPageScale0;
this.findPageScale1=findPageScale1;
this.drawScale=drawScale;
this.arrDrawSc=arrDrawSc;
this.writeNameScale=writeNameScale;
this.progressLoadBar=progressLoadBar;
this.changeScaleAnim=changeScaleAnim;
this.Wheel=Wheel;



this.draw=draw;
this.fieldlist=fieldlist;
this.Obarpos=Obarpos; 
this.fcXchg=fcXchg;
this.chgTime=chgTime;
this.changeYear=changeYear;
this.ClearTL=ClearTL; 
this.headClick=headClick;
this.ZoomPlus=ZoomPlus;


this.timeBar=timeBar;
this.dispoBar=dispoBar;
this.scrollTday=scrollTday;
this.Normz=Normz; 
this.moveToTime=moveToTime;
this.optrToTime=optrToTime;
this.retTMbar=retTMbar;
this.f_wait_message=f_wait_message;
 
this.bodydown=bodydown;
this.bodyup=bodyup;
this.bodypan=bodypan;
this.fromXxtoXxp=fromXxtoXxp;
this.bodyTip=bodyTip;
this.cicleTip=cicleTip;
this.wiewTip=wiewTip;
this.overTip=overTip;
this.delTip=delTip;
this.f_EndEdit=f_EndEdit;

this.chgDisp=chgDisp;
this.gmahmDate=gmahmDate;
this.inRange=inRange;
this.f_undo=f_undo;
this.f_delete=f_delete;
this.jaxUpdt=jaxUpdt;
this.retTMbarChange=retTMbarChange;

this.f_DST=f_DST;



with(this){

  if(isTouch) EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';

 

  // via x test
/*
 JTM[41605]=[
      // disponibilità 
      [ {t0:1327964400, t1:864000*2, type:0 },                      // type=0   colore '#c5dc97'
        {t0:1332253883, t1:864000*3, type:2 },                     // type=1   colore '#fb9999'
        {t0:1334928683, t1:864000*7, type:1 }
      
      ],           
 
      [    // impegnativa
       {t0:(1327964400+180000), t1:100000, dt:60000, nb:2, n:0, title:"preventiva" },         // n=0 preventiva
       {t0:(1327964400+200000), t1:150000, dt:0, nb:2, n:1, title:"consuntivazione" },        // n=1 consuntivo
       {t0:(1333464415+380000), t1:140000, dt:20000, nb:2, n:0, title:"preventiva" },
       {t0:(1333464415+480000), t1:160000, dt:0, nb:2, n:1, title:"consuntivazione" },       
       {t0:(1334464415), t1:140000, dt:20000, nb:1, n:0, title:"preventiva" }
      ]
  ];

  FCLoaded[41605]=1;
  TIme[41605]=0;
 */


}




function resize(l0,t0,w0,h0,i){   //  moGlb.layoutPrefixId = "layout_"+i attivo
  with(this){ 

  if(!modTG) { moAlert.show(moGlb.langTranslate("no Name TreeGrid inizialized"));  return; }                 
                       
   eval("TGJ="+modTG+";");
                       
    // creo contenitore modulo jj prima esecuzione
   var uu=0;
  if(!jj) { J=mf$(i);
           jj=moGlb.createElement("id"+name,"div",J);
           with(jj.style) overflow="hidden";   
     
    draw0();     
              
   // creo scrollbar   
   eval("Oscr"+name+"=new OVscroll('Oscr"+name+"',{ displ:0, OV:1, L:0, T:0, H:w0, HH:100, Fnc:'"+name+".Obarpos', pid:'"+jj.id+"' });");
   } else uu=1;
   
         
    with(jj.style) left=l0+"px",top=t0+"px",width=w0+"px",height=h0+"px";
    W=w0, H=h0;
    
    jj.style.display = 'block';
    var p=moGlb.getPosition(jj);   
    zL=p.l, zT=p.t;
    
    
     postSize();
     if(uu) draw();
}}    
    
    
    
function postSize(){
  with(this){
  
   
    // inizializzo scale
    var T=yearTimestamp(ANNO), stt, aa;
    
    SCALE_INI=T[0];
    SCALE_END=T[1];
    TIMERANGE=T[2];
    ANNO=T[3];
    TODAY=T[4];
    
    arrscale[3]=TIMERANGE/((TIMERANGE/86400)/7);
    arrscale[4]=TIMERANGE/12;
    arrscale[5]=TIMERANGE/4;
    arrscale[6]=TIMERANGE;
    
    
   for(var r=0;r<11;r++){ stt=0; aa=ANNO-5+r;
    if( ANNO==aa ) stt=1;
    Fmenu[7].Sub[r]={Txt:moGlb.langTranslate("Year")+" "+aa, Type:3, Status:stt, Group:2, Icon:['','radio'], Fnc:name+".changeYear("+aa+")" };
   }
    
    // partenza settimana dell'anno rispetto a weekstart 
    var dt,j;
    for(var i=1;i<8;i++) {  dt=new Date(ANNO,0,i);
                            j=dt.getDay(); 
                            if(j!=weekstart) continue;                // se primo giorno settimana    
     WeekPHASE=i; 
     if(i>1) NWEEK0=getWeek( new Date(ANNO,0,i-1) );
    }

    
    var dt=new Date(), r,tm;
    dt.setHours(0,0,0);
    
    
    // Week    scale=3   
    var p0=0,n=1;
    arrstep[3][0]=SCALE_INI; 
    if(WeekPHASE>1) { p0=(WeekPHASE-1)*86400; arrstep[3][1]=p0+SCALE_INI; n=2; }
    
    while(n<55) { 
    
    p0=arrstep[3][n-1]+604800;
    if(p0>=SCALE_END) break;
    arrstep[3][n]=p0;                     
    n++;
    }  arrstep[3].push(SCALE_END);

 
    
    // Month    scale=4
    for(r=0;r<12;r++) {  dt.setFullYear(ANNO,r,1);
                         tm=parseInt((dt.getTime())/1000);
                         arrstep[4][r]=tm;                     // timestamp inizio mese secondi
    }  arrstep[4].push(SCALE_END);
    
    
    
    // trimestri   scale=5
    for(r=0;r<4;r++) {  dt.setFullYear(ANNO,r*3,1);
                        tm=parseInt((dt.getTime())/1000);
                        arrstep[5][r]=tm;                     // timestamp inizio mese secondi
    } arrstep[5].push(SCALE_END);
    
    // YEAR  scale=6
    arrstep[6]=[SCALE_INI, SCALE_END];   
 
    predraw();  
    addStp=0;       
}}



function Display(i){
with(this){  if(i==displ) return;
if(i) { var nb="block"; displ=1; }
else { var nb="none"; displ=0; }
jj.style.display=nb;
}}




function resize_animation(l0,t0,w0,h0,i){
with(this){ 
addStp=600;

resize(l0,t0,w0,h0,i);
}}



function changeYear(a){
with(this){
  ANNO=a;
  postSize();
  drawScale();
}}


function fcXchg(){
with(this){ 

  var r, nf=TGJ.Abody.length, uf, F=[], Y=[];
  
  for(r=0;r<nf;r++) { F.push(TGJ.Abody[r].f), Y.push(TGJ.Abody[r].y);   }             

  uf=F.join();
  Pila.push([uf, Y.join()]);

  if(!Tstat)  { Tstat=1;  chgTime(uf,0);  } 
   
}}


function chgTime(uf,t){
with(this){            

var nuf="";
if(Pila.length) {  PSCD=Pila.pop(); nuf=PSCD[0]; Pila=[]; }               // PSCD  fcodes ,  e posizy ,

if(nuf && uf!=nuf) { f_wait_message(1);
                     ClearTL(); 
                     toLD=[];
                     t=0;  uf=nuf;  }
else { if(t==1) {  
  FRTXJ.innerHTML=""; f_wait_message(0);
  nodrw=0;
  draw(); }   }

if(t>3 && PLB) t=3;

// update timebar se inatività 
  if(AUpdt>0 && t>19 && !(t%10)){  
      
   if(PSCD.length){   
    var f=PSCD[0].split(","), r, m=0;
    for(r=1;r<f.length;r++) if(TIme[f[r]]<TIme[f[m]]) m=r;
    retoLD(f[m]);
  } }
 
// clean dei non visualizzati se fcode caricati NLoad > MAXLoad
if(NLoad>MAXLoad) { delOverLoad(); }
  
t++;                     
setTimeout(name+".chgTime('"+uf+"',"+t+")",2000);
}}


function delOverLoad(){
  with(this){ 
   if(PSCD.length){
     var n=parseInt(MAXLoad/10), af=PSCD[0].split(","), r,v, k=0;

     for(r=0;;) {  
     if(r==aLOad.length || k==n) break;
     v=aLOad[r];
     if(af.indexOf(v)>-1) { r++; continue; }
     
     delete FCLoaded[v];
     delete toLDY[v];
     delete JTM[v];
     delete TIme[v];
     aLOad.splice(r,1);
     NLoad--; 
     k++;    
     }
}}}




function draw0(){
  with(this){ 
    var hbody=H-HeadH-VW-mgT-mgB;   
       
    var honm="onclick='"+name+".headClick(event,this)'",
        evm="onmousemove='"+modTG+".bodymove(event); "+name+".bodyTip(event); ' onmouseout='"+modTG+".bodyout(event); "+name+".OVR=-2; if("+name+".WTP) "+name+".delTip();'",  
        onm=EVTDOWN+"='"+name+".bodydown(event)' "+EVTUP+"='"+name+".bodyup(event)' "+evm+" onmouseover='WHEELOBJ=\""+name+"\"; '";
        
        if(isTouch) onm="", honm="";
   
    var s="<canvas id='headlu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+mgT+"px;cursor:pointer;' width='"+H+"' height='"+HeadH+"' "+honm+"></canvas>"+ 
          "<canvas id='bodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px' width='40' height='"+hbody+"'></canvas>"+ 
          "<div id='overbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px;width:40px;height:"+hbody+";overflow:hidden;'></div>"+
          "<canvas id='toolTip"+name+"' style='position:absolute;left:0;top:0;display:none;' width='10' height='10'></canvas>"+                 
          "<div id='loadbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px;width:0;height:0;overflow:hidden;'></div>"+         
          "<div id='maskbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px;width:40px;height:"+hbody+";overflow:hidden;background-color:#fff;' "+onm+"></div>"+
          
          "<div id='msgtmln"+name+"' class='loader' style='position:absolute;left:50%;top:50%;width:200px;height:80px;"+
          "margin-left:-100px;margin-top:-60px;background:#fff;opacity:0.75;display:none;'>"+
          "<img src='"+IM+"ico_loading.gif' style='position:absolute;left:50%;top:27px;margin-left:-8px;' />"+
          "<div style='position:absolute;left:0;width:100%;text-align:center;top:52px;font:11px Arial;color:#505050;'>"+(moGlb.langTranslate("Waiting..."))+"</div></div>"+
 
          "<div style='position:absolute;right:"+mgR+"px;top:"+mgT+"px;cursor:pointer;' onclick='"+name+".fieldlist(event)'>"+
          "<div style='margin:2px 0 0 2px;'>"+moCnvUtils.IconFont(58445,16,"#4A6671")+"</div>"+
          "</div>"+

          "<div id='infogrid"+name+"' style='position:absolute;left:"+mgL+"px;bottom:"+mgB+"px;height:"+VW+"px;width:10px;overflow:hidden;'>"+
          "<div id='infotxt"+name+"' style='font:13px opensansR;margin:2px 4px 0 8px;'></div></div>";
                   
    jj.innerHTML=s; 
    CNJ=mf$("headlu"+name), CNV=CNJ.getContext("2d");
    BDJ=mf$("bodylu"+name), BDN=BDJ.getContext("2d");
    MBDJ=mf$("maskbodylu"+name);
    OBDJ=mf$("overbodylu"+name);
    IFGRJ=mf$("infogrid"+name); 
    FRTXJ=mf$("infotxt"+name);
    TLTPJ=mf$("toolTip"+name);

    LDBJ=mf$("loadbodylu"+name);

    moGlb.setOpacity(MBDJ,0);

    if(isTouch){
    var r=0, l=["touchstart","touchmove","touchend"], fn=["bodydown","bodypan","bodyup"]; 
      for(;r<l.length;r++) {  eval("MBDJ.on"+l[r]+"=function(e){ "+name+"."+fn[r]+"(e); }"); }  
    
    eval("CNJ.on"+l[2]+"=function(e){ "+name+".headClick(e,CNJ); }"); 
    }
    
}}






function predraw(){
with(this){  

 infW=parseInt((W-mgL-mgR)/4);

 var sl=W-VW-mgR, st=mgT+HeadH,         // resize srollb
    ol=mgL+infW, ot=H-VW-mgB; 
    OH=W-mgL-mgR; 
    SH=H-HeadH-VW-mgB-mgT;
    
  oh=OH-infW;
  KK=oh/OH;
 
 // scala
 var P=Normz(); PAGE=P[0]; PXS=P[1]; NPAGE=P[2]; Huw=P[3];
 
  eval("Oscr=Oscr"+name); 
  if(OH<64) Oscr.Display(0); else { if(!Oscr.displ) Oscr.Display(1); Oscr.Resize( { L:ol, T:ot, H:oh, HH:Huw   } ); }
  
  if(W<=moGlb.moduleMinWH) { Display(0);  return; } // dimensione minima
  Display(1);

  scrollTday();
 
  // header   
  Wsc=OH-VW; 
  drawScale();
  
  
    // body
  BDJ.width=OH;
  MBDJ.style.width=OH+"px";
  OBDJ.style.width=OH+"px";
  LDBJ.style.width=OH+"px";
  
  BDJ.height=SH;
  MBDJ.style.height=SH+"px";
  OBDJ.style.height=SH+"px";
  LDBJ.style.height=SH+"px";
  
  // info
  IFGRJ.style.width=infW+"px";    
}}




////////////////////////////////////////////////////////////////////////////////////
// inpu scala   output  PAGE, PXS
function Normz(s){
  with(this){   if(!s) s=scale;

 var tp0=typeof(arrstep[s]), n=1, pg,px, hu;
 n=TIMERANGE/arrscale[s];   
 
 pg=TIMERANGE/n;  px=pg/OH;  hu=n*oh;   // moAlert.show(pg+" | "+n+" - "+TIMERANGE/arrscale[s])
 
 return [pg,px,n,hu];
}}

           
// restituisce valore per optr dal timestamp
function moveToTime(t){
  with(this){  
  t-=SCALE_INI;
  return Huw/TIMERANGE*t;  
}}


 
// da optr  ritorna timestamp
function optrToTime(o){
  with(this){      if(!o) o=optr;
  
  return TIMERANGE/Huw*o; 
}}





function reLoad(){
  with(this){
  
  FCLoaded=[];
  toLD=[];
  toLDY=[];
  JTM=[];
  TIme=[];
  aLOad=[];
  NLoad=0; 
  draw(); 
  
}}

 

////////////////////////////////////////////////////////////////////////////////////

// crea array di divisioni visibili nella PAGE OH
function findPageScale0(){  //  s=scala
  with(this){    
                    
   var s=scale, itm=optrToTime(), etm=itm+PAGE, r,vv,v, vr=[];               
          
     if(s<3) {
                 
      vv=itm%PAGE, v=etm-vv;   r=parseInt(itm/PAGE);
          vr.push({ x:(v-itm)/PXS, s:r, tx:writeNameScale(s,r), optr0:v-PAGE });
            vr.push({ x:OH, s:r+1, tx:writeNameScale(s,r+1) });
 
     } else {
  
      for(r=1;r<arrstep[s].length;r++) { v=arrstep[s][r] - SCALE_INI;  
        
             if(v>=itm && v<=etm)  vr.push({ x:(v-itm)/PXS, s:r, tx:writeNameScale(s,r), optr0:v-PAGE }); 
             if(v>etm) break;
                           }
      vr.push({ x:OH, s:r, tx:writeNameScale(s,r) });                                  
     }   
     return vr;
}}



// scala inferiore
function findPageScale1(){  //  s=scala
  with(this){   

  var s=scale-1, itm=optrToTime(), etm=itm+PAGE, k,r,vv,v,Pn, nn, vr=[];               
 
      nn=arrscale[scale]/arrscale[s];
  
      v=0;
      Pn=PAGE/nn;
 
 if(s<3) {
                 
      vv=itm%Pn, v=itm-vv+Pn;   r=parseInt(itm/Pn);
      while(v<etm){
          vr.push({ x:(v-itm)/PXS, s:r, tx:writeNameScale(s,r), optr0:v-Pn });
          v+=Pn; r++;  
            }
           vr.push({ x:OH, s:r, tx:writeNameScale(s,r) });
 
     } else {
  
      for(r=1;r<arrstep[s].length;r++) { v=arrstep[s][r] - SCALE_INI;  
        
             if(v>itm && v<etm)  vr.push({ x:(v-itm)/PXS, s:r, tx:writeNameScale(s,r), optr0:v-Pn }); 
             if(v>etm) break;
                           }
      vr.push({ x:OH, s:r, tx:writeNameScale(s,r) });                      
     }   
     return vr;
}}





function drawScale(){
  with(this){                   
      var r,t,dx;          
          
      VR0=findPageScale0();
      VR1=findPageScale1();            
                    
     CNJ.width=Wsc;
     with(CNV){
     
    // sfondo head
    //  fillStyle=colrs[7]; fillRect(0,0,Wsc,10); 
    //  fillStyle=colrs[4]; fillRect(0,10,Wsc,14);      
      lineWidth=1; beginPath(); strokeStyle=colrs[6]; 
      moveTo(0,HeadH-0.5); lineTo(Wsc-0.5,HeadH-0.5); lineTo(Wsc-0.5,1);  
      stroke(); closePath();
 
      textBaseline="alphabetic"; textAlign="center";

      // disegna scala0 e 1
     arrDrawSc(CNV,VR0,0); 
     arrDrawSc(CNV,VR1,1);   
 
     }
     if(PSCD.length) draw();   
}}


     
     
function arrDrawSc(CNV,vr,m){
  with(this) with(CNV){ 
     var r,t,dx, p0=0,p1, y=(m)?21:11, f=(m)?"10px Arial":"bold 11px Arial";
     font=f;
     
     for(r=0;r<vr.length;r++) {  
        fillStyle="#444";
        p1=vr[r].x; dx=p1-p0; t=vr[r].tx;
        
        if(dx>16){
          if(moCnvUtils.measureText(t,f)>dx) t=moGlb.reduceText(t,f,dx);
          fillText(t,(p0+p1)/2, y); }

      fillStyle="#888";
      fillRect(p1,m?13:1,1,y+1);
      
      p0=p1;
      }     
}}     
     
     
       


function draw(){
  with(this){    XPTR=0; Xx[0]=SCALE_INI;   
 if(PSCD.length){
  Yt=TGJ.Yt, Yi=TGJ.Yi;
  var r,z, fc,inl=0, uf=PSCD[0].split(","), uy=PSCD[1].split(","), f="11px Arial", y=parseInt(uy[0]), c, cy=y/Yi, sn;

  aBlns=[];

  // evidenzia se oggi 
  var itm=optrToTime(), etm=itm+PAGE, tdy=f_DST(TODAY)-SCALE_INI;
  
  with(BDN){  clearRect(0,0,OH,SH);
    font=f; textBaseline="alphabetic"; textAlign="left";  
  
  for(r=0;r<uf.length;r++){ c=cy&1; y=parseInt(uy[r])
                      
            // falsariga
              fillStyle=TGJ.ROWBKCOL[c];
              fillRect(0,y+1,OH,Yi-2); 
              fillStyle=TGJ.ROWBDCOL;
              fillRect(0,y+Yi-1,OH,1);                      
    cy++;
    }

    // today
     if(tdy>itm-86400 && tdy<etm) {
      var l=(tdy-itm)/PXS, w=86400/PXS;
       fillStyle="rgba(224,224,224,0.4)";
       fillRect(l,0,w,SH);
    }

  if(nodrw) return;

  for(r=0;r<uf.length;r++){   y=parseInt(uy[r])

    fc=parseInt(uf[r]); 
    toLDY[fc]=y;      
                
    if(EditMode && idEDIT[0]==fc) { fillStyle="#f8e4e4"; fillRect(0,y+1,OH,Yi-2);   }            
                
                
    if(!FCLoaded[fc]) { inl++; 
      if(toLD.indexOf(fc)==-1) toLD.push(fc);     
    } else  { 
 
        // disegna disponibilità   
      if(!nodisp) for(n=0;n<JTM[fc][0].length;n++) 
          if(dispoBar(JTM[fc][0][n], fc)>1) break;
   

       // Media  seMed   proiezione consuntivi/preventivi 
       Med=[0,0]; 
        if(seMed>0){   var jt, sum0=0, sum1=0, n1=0, s0;
          for(n=0;n<JTM[fc][1].length;n++){
                 jt=JTM[fc][1][n];
                 if(jt.nb==2) { if(!jt.n) s0=parseInt(jt.t0); 
                                else sum0+=(parseInt(jt.t0)-s0), sum1+=parseInt(jt.t1), n1++; } 
          }
         if(n1) Med=[sum0/n1, sum1/n1];        
        }
   
    
        // disegna timebar di fc nel range optr
        for(n=0;n<JTM[fc][1].length;n++) {   if( timeBar(JTM[fc][1][n],fc,0,n) > 1 ) break;  }
         
       // disegna medie
      if(seMed>0 && Med[1]) for(n=0;n<JTM[fc][1].length;n++) {
        
        sn=JTM[fc][1][n].n;
        if(!sn) { if( timeBar(JTM[fc][1][n], fc, 1, n) > 1 ) break; }
 
        }                                                                               
    }}
             
    if(inl && !PLB) progressLoadBar();
  
  if(GRD>0){ fillStyle="rgba(96,96,96,0.3)";
  for(r=0;r<VR1.length;r++) fillRect(VR1[r].x,0,1,SH); }    
  }
}}}



function ClearTL(){
  with(this){
        
    FRTXJ.innerHTML=moGlb.langTranslate("Waiting...");
    TGJ.sedrw=2; TGJ.draw();
}}



function retoLD(fc){
  with(this){ 

if(toLD.indexOf(fc)==-1) toLD.push(fc);
if(!PLB) progressLoadBar();

}}


function progressLoadBar(){
  with(this){   
  PLB=1;
  FRTXJ.innerHTML=moGlb.langTranslate("Loading...");  


  if(!toLD.length) { PLB=0; retBR=0; FRTXJ.innerHTML=""; LDBJ.innerHTML=""; return;  } 
  
  if(!retBR){
  var f=toLD.shift();
  
  LDBJ.innerHTML="<img src='"+IM+"ico_loading.gif' style='position:absolute;left:10px;top:"+(toLDY[f]+11)+"px;' />";

  
  Ajax.sendAjaxPost( moGlb.baseUrl+"/treegrid/jax-timebar", "fcode="+f+"&DataMode="+DataMode+"&Module="+modTG+"&year="+ANNO, name+".retTMbar" );
  retBR=1;
  }
   
setTimeout(name+".progressLoadBar()",500);  
}}






function retTMbar(a){
  with(this){   

  var ar=JSON.parse(a), fc=ar.fcode, dt=new Date();
  JTM[fc]=ar.data;
 
  TIme[fc]=parseInt(dt.getTime());
  FCLoaded[fc]=fc;
  NLoad++;
  aLOad.push(fc);
  
  if(!ZOOM) draw();
   
  retBR=0;
}}



function f_wait_message(m){
  with(this){   

    var msj=mf$("msgtmln"+name);
    msj.style.display=m?"block":"none";
  
}}

function headClick(e,hj){   e=e||window.event;
  with(this){ 

   var p=moGlb.getPosition(hj);
   var P=isTouch?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);
   
   var y=P.y-p.t, y2=p.h/2, x=P.x-p.l, sg=(y>y2)?-1:1;                                                           // sg.  0 scala su   1 giu
  
   var vr=(sg<0)?VR1:VR0,p1, px=-1;
  
   for(r=0;r<vr.length;r++) { p1=vr[r].x; 
      if(x<p1) { px=r; break; } 
        }

   changeScaleAnim(sg,px);


moEvtMng.cancelEvent(e);
return false;
}}



function Wheel(d){
  with(this){ 
   d*=-1;
   var vr=(d<0)?VR1:VR0, p1, px=-1, r, ss=scale+d;
   for(r=0;r<vr.length;r++) { p1=vr[r].x; 
      if(Xps<p1) { px=r; break; }   }
   
   changeScaleAnim(d,px); 
}}





function changeScaleAnim(sg,px){
  with(this){ 

   var ss=scale; ss+=sg;
    if(ss<1 || ss>6 || ZOOM) return;

   var vr=(sg<0)?VR1:VR0, x0,x1,dx,o0=0,k,si;    //   cliccato vr[px].x

    
   k=arrscale[scale]/arrscale[ss];
   
  if(sg<0){  // ingrandimento  
 
 
   dx=vr[1].x-vr[0].x;
   try{    x1=vr[px].x, x0=x1-dx, o0=vr[px].optr0;
           if(x1==OH) x0=vr[px-1].x, o0=vr[px-1].optr0; 
   } catch(e){  // alert(px+" | "+vr.length); 
   }
    
   
   if(!isTouch) ZoomPlus(x0,nsteps, o0, k,sg);    // Zoom x0, dx    --->    0, OH   12 steps
   else { scale+=sg;                              
          var P=Normz(); PAGE=P[0]; PXS=P[1]; NPAGE=P[2]; Huw=P[3];
          optr=moveToTime(o0+SCALE_INI);
          if(optr<0) optr=0;   
          Oscr.Resize( { HH:Huw, ptr:optr } );  
           drawScale();
          }
     
   } else {   // riduzione
 
     try{  var t=vr[px].s;   } catch(e){ t=1; // alert(px+" | "+vr.length); 
     }
     x0=0, o0=0;

     if(scale<3) si=arrscale[scale]*t+SCALE_INI; else si=arrstep[scale][t-1];                     //moAlert.show(gmahmDate(si))


     if(ss<3) {  o0=(parseInt(si/arrscale[ss]))*arrscale[ss];
     
     
     } else { for(var r=0;r<arrstep[ss].length;r++) {  x1=arrstep[ss][r];
              if(si>=x0 && si<x1) { o0=arrstep[ss][r-1]; break; } 
              x0=x1; }
     }
 
    scale=ss;                              
    var P=Normz(); PAGE=P[0]; PXS=P[1]; NPAGE=P[2]; Huw=P[3];
    optr=moveToTime(o0);
    if(optr<0) optr=0;   
    Oscr.Resize( { HH:Huw, ptr:optr } );  
    drawScale();
   }
 
}}




 

function ZoomPlus(xi,steps,optr0,k,sg){    
  with(this){

ZOOM=1;
 
steps--;  
if(steps<0) {  scale+=sg;                              
               var P=Normz(); PAGE=P[0]; PXS=P[1]; NPAGE=P[2]; Huw=P[3];
               optr=moveToTime(optr0+SCALE_INI);
               if(optr<0) optr=0;   
               Oscr.Resize( { HH:Huw, ptr:optr } );  
               ZOOM=0;
               drawScale();
               return; }


ZM=1+(k-1)/nsteps*(nsteps-steps), TR=xi-xi*ZM-(xi/nsteps)*(nsteps-steps);  

// scale canvas
with(BDN){ 
clearRect(0,0,OH,SH);
save();
translate(TR,0);
draw();
restore();
}

setTimeout(name+".ZoomPlus("+xi+","+steps+","+optr0+","+k+","+sg+")",60);
}}









function writeNameScale(s,sv){
  with(this){
 
  var v="",r;

 if(s==6) {        // anno
  v=moGlb.langTranslate("Year")+" "+ANNO;
 }


 if(s==5) {             // trimestre
   if(sv>=5) sv=4;
  
    var aa=" "+ANNO; 
    if(s!=scale) { aa="";  }
    v=sv+moGlb.langTranslate(" Quarter")+aa;
  }


  if(s==4) {             // mesi 
   
    if(sv>=12) sv=12;
  
    var aa=" "+ANNO;
    sv=parseInt(sv);
    if(s!=scale)  aa="";
    v=mesi[sv]+aa;  
  }
  
  
  if(s==3) {          // settimana 
    if(WeekPHASE<5) {  if(sv==1) vv=NWEEK0; else vv=sv-1; }
    else vv=sv;

    if(sv>arrstep[3].length-2) vv=getWeek(new Date(ANNO,11,31));

    v=moGlb.langTranslate("Week")+" "+vv;
    if(s==scale) v+="  ("+ANNO+")";
      
  }  

  if(s==2) {          // giorni
  
    var b=((!(ANNO%4)&&ANNO%100)||!(ANNO%400))?1:0,
        M=[0,31,59+b,90+b,120+b,151+b,181+b,212+b,243+b,273+b,304+b,334+b];
             
    for(r=0;r<M.length;r++) if(sv<M[r]) break;
     
    v=sv-M[r-1]+1; if(v<10) v="0"+v;
    var j=(new Date(ANNO,r-1,v)).getDay();         // giorno settimana
        
    if(s==scale)  v=giorni[j]+" "+v+" "+mesi[r]+" "+ANNO;
    else v=abbgiorni[j]+" "+v+" "+abbmesi[r]; 
  }
    
  if(s==1) { v=sv%24;  if(v<10) v="0"+v; 
              if(s==scale) { 
                      var dt=new Date((SCALE_INI+(sv-1)*3600)*1000), 
                          giorno = dt.getDate(),
                          mese = parseInt(dt.getMonth()) +1,
                          anno = dt.getFullYear();
                        
                        if(giorno<10) giorno="0"+giorno;
                        if(mese<10) mese="0"+mese;
                        
                        v=moGlb.langTranslate("Hours")+" "+v+" ("+giorno+"/"+mese+"/"+anno+")";
                           }      
           }
  
if(!s) { v=sv%4; v*=15; if(v<10) v="0"+v;  }
  
  return v;
}}







//-------------------------------------------------------------
// eventi



function bodydown(e){
  with(this){
                   
   if(PAN) { bodyup(e); return false; }                 
                   
   if(WTP) delTip();
   var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e),
       x=P.x-(zL+mgL), y=P.y-(zT+mgT+HeadH), md=0, de=10;
   
    var ff=0, idn=0, jt0=0, jt1=0, jt2=0;  
 
   if(EditMode && idEDIT[0]>0){    
   
   if( y>Yy && y<(Yy+Yi) && x>=XXP[1]-6 && x<=XXP[3]) {  fromXxtoXxp();
   
   if( (XXP[3]-XXP[2])<(de*3) ) de=(XXP[3]-XXP[1])/3;
   
      if( x<=(XXP[1]+de) ) md=1;                  // 1 = preavviso
      if( x>=XXP[2] && x<=(XXP[2]+de) ) md=2;     // 2 = t0 inizio
      if( x>=(XXP[3]-de) ) md=4;                  // 4 = t1 fine  
      if( x>(XXP[2]+de) && x<(XXP[3]-de) ) md=7;  // 7 = spostameto    1+2+4
      
     ff=aBlns[OVR].fc, idn=aBlns[OVR].idn, 
     jt0=JTM[ff][1][idn].dt, jt1=JTM[ff][1][idn].t0, jt2=JTM[ff][1][idn].t1;
   }}
 
      
FD={ix:P.x, iy:P.y, it:optr, mode:md, i:idn, f:ff, jt0:jt0, jt1:jt1, jt2:jt2 };

   if(!isTouch){
   OM=MBDJ.onmousemove; OU=MBDJ.onmouseup;  
   
   eval("MBDJ.onmousemove=function(e){ "+name+".bodypan(e); }; "+
        "MBDJ.onmouseup=function(e){ "+name+".bodyup(e); };"); 
   }

  PAN=1;

  moEvtMng.cancelEvent(e);
  return false;
}}





function bodypan(e){
  with(this){ 
 
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e),
      dx=P.x-FD.ix, x=P.x-(zL+mgL), y=P.y-(zT+mgT+HeadH),
      md=FD.mode;
 
  if(md) {  // drag timebar selezionata
 
    var dxs=dx*PXS, xt0=JTM[FD.f][1][FD.i].t0, xt1=JTM[FD.f][1][FD.i].t1, xt2=JTM[FD.f][1][FD.i].dt,
        itm=optrToTime()+SCALE_INI, etm=itm+PAGE;
    
    if(md==1) { xt2=FD.jt0 - dxs; if(xt2<0) xt2=0; }    
    
    if(md==2) { xt0=FD.jt1+dxs; xt1=FD.jt2-dxs;  if(xt1<1800) xt0=JTM[FD.f][1][FD.i].t0, xt1=JTM[FD.f][1][FD.i].t1;   }   
    
    if(md==4) { xt1=FD.jt2+dxs; if(xt1<1800) xt1=1800; }
 
    if(md==7) { xt0=FD.jt1+dxs;  }

    if(xt0<XXP[0] || xt0<itm || (xt1+xt0)>XXP[4] || (xt1+xt0)>etm ) {  xt0=JTM[FD.f][1][FD.i].t0, xt1=JTM[FD.f][1][FD.i].t1, xt2=JTM[FD.f][1][FD.i].dt; }
     
    JTM[FD.f][1][FD.i].t0=xt0;
    JTM[FD.f][1][FD.i].t1=xt1;
    JTM[FD.f][1][FD.i].dt=xt2;
 
    // mf$("debug").innerHTML=xt0+" | dx: "+dx+"<br>optr: "+itm;

    draw();
 
  } else {  // panning

  optr=FD.it-dx;  
  if(optr<0) optr=0;
  if(optr>(Huw-oh) ) optr=Huw-oh;
  
  Oscr.Resize( { ptr:optr } ); 
  drawScale();
  }
 
  XXP=fromXxtoXxp();
 
  moEvtMng.cancelEvent(e);
  return false;
}}
  



function bodyup(e){
  with(this){
           
  PAN=0;         
           
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);
  
  
  if(EditMode && inRange(FD.ix,P.x,FD.iy,P.y) ) {  
  
      if(isTouch) { var  Xps=P.x-(zL+mgL), Yps=P.y-(zT+mgT+HeadH); }
      overTip();
      
        var k=0;
        if(idEDIT[0]>0) f_EndEdit();            
        else { if(OVR>-1) with(aBlns[OVR]) { if(nb==1 && nn==0) {
                    
             var ed=parseInt(TGJ.JStore[fc].f_editability);
            
        if(ed){ idEDIT=[fc,idn]; k=1; 
            
            var jt0=JTM[fc][1][idn].dt, jt1=JTM[fc][1][idn].t0, jt2=JTM[fc][1][idn].t1;
            
            UndoStack.push( { fc:fc, idn:idn, t0:jt1, t1:jt2, dt:jt0 } );
            Fmenu[14].Disable=0; // undo
            Fmenu[13].Disable=0; // delete
            
        }}}}
   
  draw();  
  if(k) XXP=fromXxtoXxp();
                               
  } 
  
  FD={};
  MBDJ.onmousemove=OM; MBDJ.onmouseup=OU; 
  
  if(isTouch) moEvtMng.cancelEvent(e);
  else return false;
}}
 



function fromXxtoXxp(){
  with(this){

    var oo=optrToTime(), r, Xxp=[];
    
    Xxp[0]=Xx[0];  
    Xxp[4]=Xx[4]; 
    
    for(r=1;r<4;r++){ Xxp[r]=((Xx[r]-oo)/PXS); }  
 
return Xxp;
}}





function f_EndEdit(){
  with(this){ 
 
 // ajax update t_periodics
 jaxUpdt();
 
 idEDIT=[0,0];
 Fmenu[13].Disable=1;
 
}}






function f_undo(){
  with(this){

 var p=UndoStack.pop();
 
 JTM[p.fc][1][p.idn].t0=p.t0,
 JTM[p.fc][1][p.idn].t1=p.t1,
 JTM[p.fc][1][p.idn].dt=p.dt;

 jaxUpdt(p.fc,p.idn);

 if(!UndoStack.length) Fmenu[14].Disable=1;
 
 draw();
}}




function f_delete(){
  with(this){

 var fc=idEDIT[0], idn=idEDIT[1];

 JTM[fc][1][idn].t0=0,
 JTM[fc][1][idn].t1=0,
 JTM[fc][1][idn].dt=0;

 jaxUpdt();


 f_EndEdit();
 draw();
}}



//-----
function jaxUpdt(f,i){
  with(this){   

  if(!f) { var fc=idEDIT[0], idn=idEDIT[1]; 
  } else { var fc=f, idn=i;  }
  
  var fid=JTM[fc][1][idn].fid,
    dt=JTM[fc][1][idn].dt,
    t0=JTM[fc][1][idn].t0,
    t1=JTM[fc][1][idn].t1;
          

 Ajax.sendAjaxPost( "treegrid/jax-timebar-change", "fid="+fid+"&dt="+dt+"&t0="+t0+"&t1="+t1+"&DataMode="+DataMode+"&Module="+modTG, name+".retTMbarChange" );
}}


function retTMbarChange(a){
  with(this){   
  if(a!="ok") moAlert.show(moGlb.langTranslate("error")+"\n"+a);
}}







// tooltip

function bodyTip(e){
  with(this){
 
 if(typeof(FD.ix)!="undefined") return;
 
 
  var P=moEvtMng.getMouseXY(e),  
      x=P.x-(zL+mgL), y=P.y-(zT+mgT+HeadH); 
  
  Xps=x, Yps=y; overTip();

  if(!Clc) cicleTip(0);
  if(WTP) delTip();
  
}}


function overTip(){
  with(this){                      
 
  for(var r=0;r<aBlns.length;r++) with(aBlns[r]){
      if(Xps>l && Xps<(l+w) && Yps>=t && Yps<=t+h) { OVR=r; return;  }
  }
  OVR=-1;
}}


function cicleTip(t){
  with(this){

  Clc=1;
  if(OVR==-2 || typeof(FD.ix)!="undefined") { Clc=0; return; }
  t++;
  
  if(Xps!=oXps || Yps!=oYps) t=0;  
  if(t>15 && OVR>-1) { wiewTip(); Clc=0; return; }

  oXps=Xps, oYps=Yps; 

setTimeout(name+".cicleTip("+t+")",50);
}}




function wiewTip(){             // { l:(oo-dt-4), t:y, w:t10, h:h, dal:p.t0, al:(p.t0+p.t1), prev:(p.t0-p.dt), des:"", fc:fc, tx:p.title  }
  with(this){

    //  testo
    var rg=[], nrg=2, vx=0, mx=0, f="11px Arial", ctj, tw,th,x,y;
    
    if(typeof(aBlns[OVR])=="undefined") return;
    
    with(aBlns[OVR]){
    
         rg[0]=tx+" - "+moGlb.langTranslate("Code")+": "+fc;
         if(prev) { rg.push(moGlb.langTranslate("Forewarning")+": "+gmahmDate(prev)); nrg++; }
         rg.push( moGlb.langTranslate("From")+": "+gmahmDate(dal)+" "+moGlb.langTranslate("to")+": "+gmahmDate(al) );
    
    for(r=0;r<nrg;r++){     
        vx=moCnvUtils.measureText(rg[r],f);     
        if(vx>mx) mx=vx; }
    }    

    
    tw=mx+11, th=nrg*13+11;
    x=Xps-parseInt(tw/3); if(x<2) x=2; if((x+tw)>OH) x=OH-(tw+6); 
    y=Yps-(th+10); if(y<2) y=Yps+5; 
    
    y+=(mgT+HeadH), x+=mgL;

    ctj=TLTPJ.getContext("2d");
    TLTPJ.width=(tw+5), TLTPJ.height=(th+5);
    TLTPJ.style.left=x+"px"; TLTPJ.style.top=y+"px"; TLTPJ.style.display="block";
    
   with(ctj){
   
     fillStyle="#efefef";
     strokeStyle="#7c7b72";
   
     moCnvUtils.roundRect(ctj, 0.5, 0.5, tw, th, 3);
     fill();
     stroke();
   
     textBaseline="alphabetic"; textAlign="left"; font=f;
     fillStyle="#444";
     
     for(r=0;r<nrg;r++) fillText(rg[r],6,17+r*13); 
   } 

     WTP=1;  
}}



function delTip(){
  with(this){

  TLTPJ.style.display="none";
  WTP=0;
}}


// menu  -----------------------------------------------------------------------
function fieldlist(){
  with(this){

  var l=zL+W-mgR-VW, t=zT+mgT-2, w=VW, h=HeadH; 
  moMenu.Start(name+".Fmenu",{l:l, t:t, w:w, h:h},2);

}}




function Obarpos(i){
  with(this){
  optr=i; 
  W0=optrToTime(), W1=W0+PAGE;
  drawScale();
}} 



function scrollTday(){ 
  with(this){    

  var tnow = moveToTime(nowTimestamp()), dd=0;  
  if(PAGE>86400) { dd=(PAGE-86400)/2;  
                   dd=oh/PAGE*dd;                          
                   tnow -=dd; }
    
  
  optr=tnow; 
  if(optr<0) optr=0;
  if(optr>(Huw-oh) ) optr=Huw-oh;
 

  Oscr.Resize( { ptr:optr } ); 
}}

/*

t0  data inizio
t1  data fine
dt  preavviso     -1 non configurabile

y   posizione testa riga  Yi altezza
nb  numero di bar della riga
n   n esima bar di nb                                                                      

scala  1px= n sec   (OH numero pixel width)    

                       secondi      pixel
15 min                     900        100         1px = 9 sec
ora                       3600        400         1px = 9 sec
giorno                   86400      width         1px = 86400/OH sec
settimana               604800      width         1px = 604800/OH sec

PXS  num secondi per 1 pixel            
*/




function timeBar(p, fc, av, idn){    //  p { t0,t1,dt, y, nb n }  nb=1/2    n=0 preventiva    n=1 consuntiva   |   av=1 media     |  idn riferimento  JTM[fc][1][idn]  
  with(this){    
      
  if(!p.t0 && !p.t1 && !p.dt) return 0;
     
  if(!av){              
    var nb=p.nb;                       
    if(p.n==1 && seCons<0) return 0; 
    if(seCons<0 && nb>1) nb=1;           
  }   
           
    p.t0=parseInt(p.t0), p.t1=parseInt(p.t1), p.dt=parseInt(p.dt);           
    var t0=f_DST(p.t0)-SCALE_INI, t1=t0+p.t1, dt=p.dt;
     
  if(av) t0+=Med[0], t1=t0+Med[1], dt=0, nb=1;  
     
    if(t1<W0) return 0;             // 0 prima di PAGE    2 dopo
    if((t0-dt)>W1) return 2;
    
    var y=toLDY[fc]-0.5, h=12, oo=t0-optrToTime(), sa=5, t10,l1=1,l2=2;  
    
    
    // range movimento timebar
    if(EditMode && p.n==0 && XPTR<3 && idEDIT[0]==fc){     

        if(idEDIT[1]==idn) XPTR++;
    
        if(!XPTR) Xx[0]=p.t0+p.t1;
        if(XPTR==1) Xx[1]=t0-dt, Xx[2]=t0, Xx[3]=t1, Yy=y, Xx[4]=SCALE_END;
        if(XPTR==2) Xx[4]=p.t0;
    
        if(XPTR>0) XPTR++;        
    }   
                  
    t0/=PXS, t1/=PXS, dt/=PXS, oo/=PXS;
    
    
        if(nb==1) y+=(Yi-h)/2;
        if(nb==2) h=6, y+=(Yi-16)/2+p.n*10, sa=2;
 
        if(!p.n) c0="#e3f189", c1="#6ba801"; 
        if(p.n==1) c0="#ffc579", c1="#ff9000";  //c0="#eee", c1="#aaa";  
                      
    t10=t1-t0; var o4, o1=oo-dt, grd;
 
    if(ZOOM){ oo*=ZM; o1*=ZM; dt*=ZM;
              l1*=ZM; l2*=ZM; t10*=ZM; }
    o4=oo-dt-4;
 
    // disegno
    with(BDN){
          strokeStyle=av?"rgba(0,0,0,0.6)":"#7b7a79";
          
          if(t10<5) {  
          
              if(t10<3) { 
                if(!av){
                 t10=0;
                 fillStyle="#999";
                 fillRect(oo, y, l1, h);
                 }
                 } else{
                 fillStyle=(av)?"rgba(255,255,255,0.4)":c0;
                 fillRect(oo, y, l2, h); 
                 strokeRect(oo, y, l2, h);  
                 t10=2; }          
          } else {

          save();
          
          moCnvUtils.roundRect(BDN, oo, y, t10, h, 1);
          stroke();
          
          if(dt) {                                   // se preavviso esiste
          moCnvUtils.roundRect(BDN, o4, y, 4, h, 1);                     
          stroke(); }
          
          if(!av && Redu>0) { shadowOffsetX=2; shadowOffsetY=2; shadowBlur=2; shadowColor='rgba(0,0,0, 0.4)'; }
          
          
          if(av) grd="rgba(255,255,255,0.4)";
          else { 
          
                if(EditMode && idEDIT[0]==fc && idEDIT[1]==idn) c0="#ecc", c1="#d44"; 
  
                if(Redu>0){
                 grd=createLinearGradient(0,y,0,y+h);
                 grd.addColorStop(0,c0);
                 grd.addColorStop(1,c1); }        
                else grd=c1;
               }
         
          fillStyle=grd;

          moCnvUtils.roundRect(BDN, oo, y, t10, h, 1);                     
          fill(); 
          
          if(dt) {                                   // se preavviso esiste
          moCnvUtils.roundRect(BDN, o4, y, 4, h, 1);                     
          fill();  
          fillStyle="#7b7a79";
          fillRect(o1,y+sa+0.5,dt,1);
          }
          
          restore(); 
        }        
    }
              
   if(av) return 1;           
              
   if(t10 && !ZOOM) { aBlns.push({ l:(oo-dt-4), t:y, w:t10+dt+4, h:h, dal:p.t0, al:(p.t0+p.t1), prev: (dt?(p.t0-p.dt):0), des:"", fc:fc, tx:p.title, nb:p.nb, nn:p.n, idn:idn  }); }   
   return 1;
}}






function dispoBar(p,fc){    //  p { t0,t1,dt,  type}         
  with(this){

    // normalizzo a PXS
    var t0=p.t0-SCALE_INI, t1=t0+p.t1;
   
    if(t1<W0) return 0;             // 0 prima di PAGE    2 dopo
    if((t0)>W1) return 2;

    var y=toLDY[fc]-0.5, h=6, oo=t0-optrToTime(), sa=5;  
    
    t0/=PXS, t1/=PXS, oo/=PXS;
    y+=(Yi-h)/2;
               
  var t10=t1-t0;
    if(ZOOM) oo*=ZM, t10*=ZM;
    
    // disegno         
    BDN.fillStyle=TypeColor[p.type];
    BDN.fillRect(oo,y,t10,h);      

   return 1;
}}


function chgDisp(){
with(this){

  nodisp=nodisp?0:1;
  if(!ZOOM) draw();
}}



//--------------------------------------------------------------------------------------
// extra method

this.yearTimestamp=yearTimestamp;
this.nowTimestamp=nowTimestamp;
this.getWeek=getWeek;
this.ifWeekNew=ifWeekNew;


/*
ore= dt.getHours();  
giorno= dt.getDate();
mese =  dt.getMonth();
anno =  dt.getFullYear();

*/
// GMT
function yearTimestamp(anno){                                       // ANNO     mese: 0-11   giorno 1-31

var dt=new Date();
if(!anno) anno=dt.getFullYear();

dt.setHours(0,0,0);
var today=parseInt((dt.getTime())/1000);

dt.setFullYear(anno,0,1); 
var tm0=parseInt((dt.getTime())/1000);
 
dt.setFullYear(anno+1);
var tm1=parseInt((dt.getTime())/1000), dtm=tm1-tm0;
 
return [tm0,tm1, dtm, anno, today];       // SCALE_INI=T[0]; SCALE_END=T[1]; TIMERANGE=T[2]; ANNO=T[3]; TODAY=T[4];
}

 

function nowTimestamp(){

var dt=new Date(), tms=parseInt((dt.getTime())/1000); 
return tms;
}






function getWeek(dt){       
  var M=[0,31,59,90,120,151,181,212,243,273,304,334], 
      g=dt.getDate(),m=dt.getMonth(),y=dt.getFullYear(),         // gg mm aa
      b=((!(y%4)&&y%100)||!(y%400))?1:0,                         // b bisestile
      n=g+M[m]+((b&&m>1)?1:0),                                  // n esimo giorno...
      j=(new Date(y,0,1)).getDay(); if(!j) j=7;                // j  1-7 lun-dom   1 gennaio 
  var k=(j>4)?(j-2):(j+5), w=parseInt((n+k)/7);                // w nÂ° settimana 
  if(!w) { return getWeek( new Date(y-1,11,31) ); }      
  var d=(new Date(y,11,31)).getDay();                    // verifico ultima settimana anno
  if(d<4 && n>(365+b-d)) return 1;  
  return w;
}


function ifWeekNew(y){
  var d=(new Date(y,11,31)).getDay();
  if(d<4) return 1; 
  return 0;
}



function gmahmDate(t){

var dt = new Date(t*1000), 
ore=dt.getHours(),
min=dt.getMinutes(),
// sec=dt.getSeconds(),   
giorno = dt.getDate(),
mese = parseInt(dt.getMonth()) +1,
anno = dt.getFullYear();


if(giorno<10) giorno="0"+giorno;
if(mese<10) mese="0"+mese;
if(ore<10) ore="0"+ore;
if(min<10) min="0"+min;


return giorno+"/"+mese+"/"+anno+" "+ore+":"+min;
}



function inRange(x1,x2,y1,y2){
  var d=4; if( Math.abs(x1-x2)<d && Math.abs(y1-y2)<d ) return 1;
  return 0;
}




function f_DST(t,md){    // DST:[0,-1,0,1332633600,1351386000]   ora legale
with(this){              // md=0 aggiunge ora legale   md=1  ripristina senza ora legale 
  var hh=(!md)?3600:-3600;  
    if(DST[0] && t<=DST[3]) t+=DST[0]*hh;
    if(DST[1] && t>DST[3] && t<DST[4]) t+=DST[1]*hh;
    if(DST[2] && t>=DST[4]) t+=DST[2]*hh;
  return t;
}}


} // end timeline