/* 
  Selector

properties:
noMenuMode   0 = show menu    1 = hide
openRoot     1 = open Root at startup
f_type_id   selector type
orderby   fieldname
order     ASC/DESC


ModSel: 0 default 
         1 multiple
         2 direct
         3 direct multiple


methods:
-Resize()
-Reset()
-SetMode(i)
-GetSelected(f_code)


            0       1          2             3             4
Ypos = [ f_code, f_title, f_unique, openclose:-1|0|1, is last sibling 0|1 ]      // openclose  0=leaf   >0 closed  <0 opened
Rfp: unique => position in Ypos
*/


var moSelector=function(nm, z){  // nm=istance obj name | z=params

this.create = function(z) {
if(!nm) return;

// properties
props={ f_type_id:1, openRoot:1, orderby:"",order:"", ModSel:0, noMenuMode:0, FontH:11, FontF:"opensansR", mod: '',
        J:null, JO:null, JE:null, JL:null, JC:null, cj:null, TGJ:null, Vscr:null, Wc:0, Hc:0, marg:6,
        EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove', dwn:0, PE:{}, Ix:0,Iy:0, IMV:0, noevt:0,
        Yi:0, Yt:0, vptr:0, Ivptr:0, VW:12, UNQ:"", Rfp:{}, 
        tBox:null, oBox:null, nBox:null, Cj:null, cCj:null,        
         
}
for(var i in props) this[i]=(typeof z[i]=='undefined')?props[i]:z[i];
}

this.name=nm;
this.Ypos=[];
this.Sel=[];
this.SelU=[];
this.aBD=[];
this.CallReadChildren="readchildren.php";
this.IM = moGlb.baseUrl+"/public/msim_images/default/";

this.colrs=["#FFFFFF",
            "#A5B3B8","#4A6671",   // green
            "#E0E0E0",             // vertical
            "#A0A0A0","#000000"];  // black
 

this.ViewMenu=function(j){
with(this){
  var P=moGlb.getPosition(j), l=P.l+2, t=P.t; 
  moMenu.Start(name+".Smenu",{l:l, t:t, w:18, h:20},2);
}}


this.Draw=function(){
with(this){
  var r,n,nn,y,ry,c,py,
          
      nar=Ypos.length;
  if(!nar) return;    // no rows
  Yt=vptr%Yi, n=Hc+Yt;
  y=vptr-Yt;
  ry=y/Yi;
    
  cj.clearRect(0,0,Wc,Hc);
  py=-Yt;
  aBD=[];
 
    for(r=ry;r<nar;r++){
      Drow(r,py,0); 
      py+=Yi;
      if(py>Hc) break;
    }
}}



this.Drow=function(i,y,m){
with(this){
  var r,oc,xx,yy,uq,nq,f,yf,cc,aq,le,jj,pb,upb;
    uq=Ypos[i][2]; aq=uq.split("|"); nq=aq.length-1;  // number of nesting
    xx=nq*18; yy=y+(Yi-16)/2;
    oc=Ypos[i][3];  
  jj=m?cCj:cj;    
    cc=4;  //grey
    if(Sel.length){
      for(r=0;r<Sel.length;r++){
       f=Sel[r];
       
       if(ModSel<2){
        if(aq.indexOf(f+"")!=-1) { cc=5; break; }    // black selected
       } else {
        if(f==Ypos[i][0]) { cc=5; break; }
       }
      }
    } else cc=5;
    
    // folders
      f=oc?((oc>0)?58364:58369):58355;
      moCnvUtils.cnvIconFont(jj,f,xx+18,yy,16,colrs[cc-3]);
       
  // if leaf
  le=(i<Ypos.length-1 && uq.length==Ypos[i+1][2].length)?1:2;  
  
with(jj){  
    // vertical line
    fillStyle=colrs[3];
    
    // parent is last element?
    upb=aq[0]; 
    for(r=0;r<nq-1;r++) {
       upb+="|"+aq[r+1]; 
       pb=Rfp[upb];
       if(!Ypos[pb][4]) fillRect((r+1)*18+7,y,1,Yi);
    }
    
    fillRect(nq*18+7,y,1,Yi/le);
    fillRect(nq*18+9,y+Yi/2,8,1);
    
    if(oc) {
      fillStyle=colrs[0];
      fillRect(xx,yy+2,14,14);
      
      // plus/minus
        f=(oc>0)?58824:58825;   // 58804:58805;
        moCnvUtils.cnvIconFont(jj,f,xx,yy+1,14,colrs[4]);
      
    }    
    // title
    fillStyle=colrs[cc];
    font=FontH+"px "+FontF; 
    textBaseline="alphabetic"; 
    textAlign="left"; 
    yf=y+FontH+(Yi-FontH)/2;
    fillText(Ypos[i][1], xx+40, yf); 
  } 
  aBD.push({y:y+Yi, x:xx, idy:i });
}}


 
this.sel_down=function(e){  
with(this){ moEvtMng.cancelEvent(e); 
  
  moMenu.Close();
  
  if(noevt) return false;
  if(dwn) return false; 
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e); 
  Ix=P.x,Iy=P.y,Ivptr=vptr;
  PE=moGlb.getPosition(JE);
  IMV=0;
  if(!isTouch){
    JO.style.display="none";
    TGJ.style.display="block";
    eval("TGJ."+EVTMOVE+"=function(e){"+name+".sel_move(e); }");
    eval("TGJ."+EVTUP+"=function(e){"+name+".sel_up(e); }");
  }
  dwn=1;
}}

this.sel_move=function(e){ 
with(this){ moEvtMng.cancelEvent(e); if(noevt) return false;
 
  var i,y,v,h, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e,JE);
 
  if(!dwn){
    i=OvrClick(P.y);
    if(i<0) JO.style.display="none";
    else { JO.style.top=(marg+aBD[i].y-Yi)+"px"; JO.style.display="block"; }
  } else {  
    IMV++;    
    if(IMV&1) return false;  
    if(isTouch) P.y-=PE.t;
      h=Ypos.length*Yi;
      if(h<=Hc) v=0; else  {
        v=Ivptr+Iy-P.y-PE.t;  
        if(v+Hc>h) v=h-Hc;
      }
      if(v<0) v=0;
      vptr=v; 
      Vscr.Resize( { ptr:vptr } ); 
      Draw();   
  } 
}}


this.sel_up=function(e){
with(this){ moEvtMng.cancelEvent(e); if(noevt) return false;  
  var r,i,f,x,y,ox; 
  // click
  if(IMV<4){
    r=OvrClick(Iy-PE.t);
    if(r>-1){
      i=aBD[r].idy; f=Ypos[i][0];
      x=aBD[r].x; ox=Ix-PE.l;
      if(Ypos[i][3] && ox>x && ox<x+26) OpenClose(i);            
      else addSEL(f,i);
     }
  }
  dwn=0;
  TGJ.style.display="none";
}}
 

this.OvrClick=function(y){
with(this){
  var r,y1=0,y2,v=-1;
  for(r=0;r<aBD.length;r++){
    y2=aBD[r].y;
    if(y>=y1 && y<y2) { v=r; break; }
    y1=y2;
  }
  return v;
}}


this.addSEL=function(fc,i){
with(this){
  if(fc==Ypos[0][0] && ModSel<2) { Reset(1); return; } // select root with children => Sel[]
  var r,n,uq,uc,f,p;
  if(ModSel&1) { 
    n=Sel.indexOf(fc);
    if(n!=-1)  { 
      Sel.splice(n,1); 
      SelU.splice(n,1);
    }else {
      if(ModSel==1){   // remove selected children of fc
        uq=Ypos[i][2];
        for(n=i+1;n<Ypos.length;n++){
          uc=Ypos[n][2];
          if(uc.length<=uq.length) break; 
          f=Ypos[n][0];
          p=Sel.indexOf(f);
          if(p!=-1) { Sel.splice(p,1); SelU.splice(p,1); }
        }  
        // do not add if is child of Selected
        n=uq.length;
        for(r=0;r<SelU.length;r++){
          uc=SelU[r];
          p=uc.length;
          if(n<=p) continue;
          f=uq.substring(0,p);
          if(f==uc) return;   // is child!! no outAction
        } 
      } 
     Sel.push(fc);  SelU.push(Ypos[i][2]); 
    }
  }else{ 
    if(Sel.indexOf(fc)!=-1) { 
      Sel=[];
      SelU=[];
    } else {
      Sel=[fc];
      SelU.push(Ypos[i][2]);
    }
  }
  Draw();
  outAction();
}}

this.OpenClose=function(i){
with(this){ 
  var r,y=0,hh,n=0, f=Ypos[i][0], oc=Ypos[i][3];
  Ypos[i][3]*=-1; 
  if(oc<0) { // close   
    for(r=i+1;r<Ypos.length;r++){
      uq=Ypos[r][2];
      if(uq.indexOf(f+"")==-1) break;
      n++; 
    }
    for(r=0;r<aBD.length;r++){
      if(aBD[r].idy==i) { y=aBD[r].y; break; }
    }
    tBox=cj.getImageData(0,0,Wc,y);
    hh=getOtherRow(i+1,n);
    Ypos.splice(i+1,n);
    UpdateUnique();
    AnimOC(y,0,3,n*Yi,hh,-1);    
  }else{ // open
    Load(Ypos[i][0],Ypos[i][2]);    
  }
}}


this.Vbarpos=function(i){
with(this){
  vptr=i;
  Draw();  
  if(!isTouch) JO.style.display="none";
}}


this.Resize=function(){
with(this){
  Wc=J.offsetWidth - marg*2, Hc=J.offsetHeight - marg*2;
  JC.height=Hc;
  JC.width=Wc-VW; 

  JE.style.width=Wc+"px"; JE.style.height=Hc+"px";
 
  UpdateVscr(1);
  vptr=0;
  Draw();
}}


this.UpdateVscr=function(m){
with(this){
  var h=Ypos.length*Yi;
  if(m)  vptr=0; else { if(h>Hc && vptr+Hc>h) vptr=h-Hc; }
  if(Hc>=h) Vscr.Display(0); else { if(!Vscr.displ) Vscr.Display(1); Vscr.Resize( { L:Wc-marg, T:marg, H:Hc, HH:h, ptr:vptr } ); }
}}

this.SetMode=function(i){
with(this){
  if(i==ModSel) return;
  ModSel=i; eval(mod).setMode(i);
  Reset(1);
}}

this.GetSelected=function(){
with(this){
  return { Selected:Sel, Mode:ModSel };
}}


 
 
this.Load=function(p,unq){  // p = parent f_code   unq = univoce code
with(this){
   noevt=1; UNQ=unq;   

    var r,i,nq,aq, y=0, l=marg, t=marg+1+(Yi-16)/2;
    if(unq){ 
      aq=(unq+"").split("|"); nq=aq.length-2;
      l+=nq*18;
      if(l<0) l=-100;
        i=Rfp[unq];
        for(r=0;r<aBD.length;r++){
          if(aBD[r].idy==i) { y=aBD[r].y-Yi; break; }
        }
       t+=y; 
    }
    JL.style.left=l+"px"; JL.style.top=t+"px"; JL.style.display='block';
   
   //Ajax.sendAjaxPost(CallReadChildren,"type="+f_type_id+"&id="+p+"&orderby="+orderby+"&order="+order, name+".retLoad");
   Ajax.sendAjaxPost(moGlb.baseUrl+"/selectors/get", "param="+JSON.stringify({ stype: f_type_id, parent: p, orderby: orderby, order: order}), name+".retLoad");
}}


this.retLoad=function(a){
with(this){
  try { var ar=JSON.parse(a); }catch(e){ moGlb.alert.show(moGlb.langTranslate(a)); return; }
  if(moGlb.isset(a.message)) { moGlb.alert.show(moGlb.langTranslate(a.message)); return; }
  if(ar.data.length == 0) { mf$(name+"Loa").style.display = 'none'; return; }
  var r,fc,rw,pd,nr,uq,ps=0,p,y=0,hh,sib;  
  pd=ar.parent;
  rw=ar.data;
  if(UNQ) { ps=Rfp[UNQ]+1; UNQ+="|"; }
  nr=rw.length;
  eval(mod).setInheritedLabel(rw[0][1]);
  for(r=0;r<nr;r++){      
    fc=rw[r][0];  // 0=f_code 1=f_title  2=nchild  (0=leaf)  
    uq=UNQ+fc;
    sib=(r==nr-1)?1:0;
    Ypos.splice(ps+r,0,[ fc, rw[r][1], uq, rw[r][2], sib ]);
  }   
  UpdateUnique(); 

  if(!UNQ && openRoot){ 
    Ypos[0][3]=-1;
    Draw();
    p=rw[0][0];
    Load(p,p);
    return;
  }
  
  // animate
  p=ps-1; 
  if(p>=0) {
    for(r=0;r<aBD.length;r++){ if(aBD[r].idy==p) { y=aBD[r].y+1; break; }}
    tBox=cj.getImageData(0,0,Wc,y-1 );
    oBox=cj.getImageData(0,y,Wc,(Hc-y) );
    hh=getNewRow(p+1,nr);
    AnimOC(y,0,3,nr*Yi,hh,1);  
  } else {
    UpdateVscr();
    Draw();
    noevt=0;
    JL.style.display="none";
  }  
}}


this.UpdateUnique=function(){
with(this){
  var r,i,uq; 
  Rfp={};
  for(r=0;r<Ypos.length;r++){
    uq=Ypos[r][2];
    Rfp[uq]=r;
  }
}}


// open
this.getNewRow=function(p,nr){
with(this){ 
 if(!Cj) {
  Cj=moGlb.createElement("idcnvhidd"+name,"canvas");
  Cj.style.display="none";
  
  cCj=Cj.getContext("2d");
 } 
  var r,h,i=0,k=Hc,hh;  
  Cj.width=Wc, Cj.height=Hc;
  cCj.clearRect(0,0,Wc,Hc);   
  h=nr*Yi;
  nc=parseInt(Hc/Yi)-1; 
  if(nr>nc) i=nr-nc;
  for(r=nr-1;r>=i;r--){
    k-=Yi;
    Drow(p+r,k,1);
  }
  hh=Hc-k; 
  try { nBox=cCj.getImageData(0,k,Wc,hh); }catch(e){ nBox=null; }
  return hh;
}}


// close
this.getOtherRow=function(p,nr){
with(this){ 
  var r,h,i,n,k=0,nn; 
  Cj.width=Wc, Cj.height=Hc;
  cCj.clearRect(0,0,Wc,Hc);   
  h=nr*Yi;
  nc=parseInt(Hc/Yi)-1; 
  i=(nr>nc)?nc:nr;
  for(r=0;r<i;r++){  
    Drow(p+r,k,1);
    k+=Yi;
  }   
  try {nBox=cCj.getImageData(0,0,Wc,k); }catch(e){ nBox=null; }
  
  // oBox
  cCj.clearRect(0,0,Wc,Hc);
  n=Ypos.length;
  nn=p+nr+nc; if(nn>n) nn=n;
  h=0;
  for(r=p+nr;r<nn;r++){  
    Drow(r,h,1);
    h+=Yi;
  } 
  oBox=h?oBox=cCj.getImageData(0,0,Wc,h):null;
  return k;
}}



this.AnimOC=function(y,t,k,tf,df,d){
with(this){  
  cj.clearRect(0,y,Wc,(Hc-y));
  t+=k; k+=5; if(t>=tf || t>(Hc-Yi) ) t=tf; 
  var yy=y+d*t;
  if(nBox)cj.putImageData(nBox,0,yy-((d>0)?df:0));
  cj.putImageData(tBox,0,0);
  if(oBox) cj.putImageData(oBox,0,yy+((d<0)?df:0));
  if(t==tf){ UpdateVscr(); Draw(); noevt=0; JL.style.display="none"; return; }
  
setTimeout(name+".AnimOC("+y+","+t+","+k+","+tf+","+df+","+d+")",40);
}}
 
 
this.Display=function(i){
with(this){
  J.style.display=i?"block":"none"; 
}}


this.Reset=function(i){
with(this){
  Sel=[];
  SelU=[];
  Draw();
  if(i) outAction();
}}


this.outAction=function(){
with(this){
    
   // console.log( "Selez: "+Sel.join(",") )

    eval(mod).sender();
 // mf$("idebug").innerHTML="N. "+Sel.length+" | modo: "+ModSel+"<br>"+Sel.join(","); 
}}
 
 
 
// Start -------------------------------------------- 
this.Start=function(container){ 
with(this) {

Ypos=[];
Sel=[];   // array of fcode
SelU=[];  // array of unique selected
aBD=[];

  Yi=isTouch?28:22;  // row height
  FontH=11;
  
  
// menu mode
this.Smenu=[
  {Txt:moGlb.langTranslate("All"),  Type:0, Fnc:name+".Reset(1)" }, 
  {Type:4},
  {Txt:moGlb.langTranslate("Check single tree"),  Type:3, Status:1, Group:1, Icon:['','radio'], Fnc:name+".SetMode(0)" },
  {Txt:moGlb.langTranslate("Check multiple trees"), Type:3, Status:0, Group:1, Icon:['','radio'], Fnc:name+".SetMode(1)" }, 
  {Txt:moGlb.langTranslate("Check single node"), Type:3, Status:0, Group:1, Icon:['','radio'], Fnc:name+".SetMode(2)" }, 
  {Txt:moGlb.langTranslate("Check multiple nodes"), Type:3, Status:0, Group:1, Icon:['','radio'], Fnc:name+".SetMode(3)" } 
];

  if(container) J=mf$(container); else return;
  Wc=J.offsetWidth - marg*2, Hc=J.offsetHeight - marg*2;
 
  var s="<canvas id='"+name+"Cnv' style='position:absolute; width='"+(Wc-VW)+"' height='"+Hc+"'></canvas>"+
        
        "<div id='"+name+"Ovr' style='position:absolute;left:0px;top:0;width:100%;height:"+Yi+"px;background-color:#A0A0A0;opacity:0.1;display:none;'></div>"+   // over
        "<img id='"+name+"Loa' style='position:absolute;display:none;' src='"+IM+"ico_loading.gif' />"+
        "<div id='"+name+"Evt' style='position:absolute;left:0;top:"+marg+"px;width:100%;height:"+Hc+"px;background-color:#FFF;opacity:0.01;'></div>";           // events
      
      if(!noMenuMode){  
        s+="<div style='position:absolute;left:4px;bottom:4px;font:16px FlaticonsStroke;color:#A0A0A0;cursor:pointer;'"+
        "onclick='"+name+".ViewMenu(this)'>"+
        String.fromCharCode(58445)+"</div>";
        
      } 
        
  J.innerHTML=s;
  JC=mf$(name+"Cnv");
  JO=mf$(name+"Ovr");
  JE=mf$(name+"Evt");
  JL=mf$(name+"Loa");
  cj=JC.getContext("2d");
  
  if(isTouch) { EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';  }

  // scrollbar  
  eval("Vscr"+name+"=new OVscroll('Vscr"+name+"',{ displ:0, L:0, T:0, H:Hc, HH:1, Fnc:'"+name+".Vbarpos', pid:'"+J.id+"' }); ");    // VW=Vscr"+name+".W;
  eval("Vscr=Vscr"+name);

  TGJ=moGlb.createElement("maskTreegridDragScroll","div",document.body);
  TGJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(moGlb.zloader+500));

 
  // events
  eval("JE."+EVTDOWN+"=function(e){"+name+".sel_down(e); }");
  
  if(isTouch){ 
    eval("JE."+EVTMOVE+"=function(e){"+name+".sel_move(e); }");
    eval("JE."+EVTUP+"=function(e){"+name+".sel_up(e); }");  
  } else {    
    JE.onmouseover=function(e){ WHEELOBJ="Vscr"+name;  }
    JE.onmouseout=function(e){ WHEELOBJ=""; JO.style.display="none"; }
    eval("JE."+EVTMOVE+"=function(e){"+name+".sel_move(e); }");   
  }


  JC.width=Wc-VW;

  Load(0,""); 
 
}
 
}

} // end selector