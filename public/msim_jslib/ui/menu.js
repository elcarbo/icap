// menu
/*
 
Include  global.js

Call to: mfBrowser();   (isTouch)

Start(n,e,m)         n=JSON menu, e=event  m=modo 0=click tag   e=event       // Button o tag
                                                  1=click punto x,y           // clic DX
                                                  2=e contiene { l,t,w,h }    // tastiera


props:
Zi        zindex prima finestra
ZoomX     
ZoomY
ZoomTX     
ZoomTY
 
use:
<button onclick='jsMenu.Start("menu0",event,0)'>test menu</button>
oncontextmenu=function(e){ jsMenu.Start("menu0",e,1); return false; }  
   
   
Colors= {

  bg:#f2f2f2
  bgOver
  separator
  text
  textOver
  textDisabled
  
}   
   
                                                  
Fin[n] = { l,t,w,h, j, cj, ei, ef, ov, rm, Ovr[ [hini, hfin, ov], ... ]  }
        
j, cj    obj canvas        
ei       init elements di rm
ef       end elements
et       elem total in window (if divided)
ov       over
rm       json level                                               




 menu0=[ 
{Txt:"Barra degli strumenti", TxtDx:"f11", Type:1, Sub:[
              {Txt:"Barra dei menu", Type:2, Status:0, Icon:['','checked'] },
              {Txt:"Barra di navigazione", Type:2, Status:0, Icon:['checked','checked'] },
              {Txt:"Barra dei segnalibri", Type:2, Status:0, Icon:['','checked'] },
              {Type:4 }, 
              {Txt:"Personalizza", Type:0 }
                                                 ]},
{Txt:"Barra di stato", Type:2, Status:1, Icon:['','checked']},
{Txt:"Barra laterale", Type:2, Status:1, Icon:['','checked']}, 
{Type:4 }, 
{Txt:"Stop", Type:0, TxtDx:"ctrl+Alt+Enter", Disable:1}, 
{Txt:"Ricarica", Type:0, Fnc:'moGlb.alert.show(\"Ricaricato!\")' }, 
{Type:4 },
{Txt:"Codifica caratteri", Type:1, Sub:[
              {Txt:"Riconoscimento automatico della lingua", Type:1, Sub:[
                                {Txt:"(Disattivato)", Type:3, Status:1, Group:2, Icon:['','radio'] },
                                {Txt:"(Cinese)", Type:3, Group:2, Icon:['','radio'] },
                                {Txt:"(Giapponese)", Type:3, Group:2, Icon:['','radio'] },
                                {Txt:"(Russo)", Type:3, Group:2, Icon:['','radio'] },
                                {Txt:"(Asia Orientale)", Type:3, Group:2, Icon:['','radio'] },
                                {Txt:"Altri", Type:1, Sub:[
                                                  {Txt:"(Italia)", Type:3, Status:1, Group:2, Icon:['','radio'] },
                                                  {Txt:"(Spagna)", Type:3, Group:2, Icon:['','radio'] },
                                                  {Txt:"(Francia)", Type:3, Group:2, Icon:['','radio'] },
                                                  {Txt:"(Austria)", Type:3, Group:2, Icon:['','radio'] },
                                                  {Txt:"(Germania)", Type:3, Group:2, Icon:['','radio'] },
                                                  {Txt:"Altri", Type:1, Sub:[
                                                                       {Txt:"(America)", Type:3, Status:1, Group:2, Icon:['','radio'] },
                                                                       {Txt:"(Asia)", Type:3, Group:2, Icon:['','radio'] },
                                                                       {Txt:"(Europa)", Type:3, Group:2, Icon:['','radio'] },
                                                                       {Txt:"(Australia)", Type:3, Group:2, Icon:['','radio'] },
                                                                       {Txt:"(Africa)", Type:3, Group:2, Icon:['','radio'] }   
                                                                          ]},
                                                          ]},
                                                                          ]},
              {Txt:"Nessun Riconoscimento"},
              {Type:4 },
              {Txt:"Occidentale (ISO-8859-1)", Status:1, Type:3, Group:1, Icon:['','radio'] },
              {Txt:"Unicode (UTF-8)",                   Type:3, Group:1, Icon:['','radio'] },
              {Txt:"Europa centrale (Windows-1250)",    Type:3, Group:1, Icon:['','radio'] },
              {Txt:"Occidentale (Windows-1252)",        Type:3, Group:1, Icon:['','radio'] }
                                    ]},
{Type:4 }, 
{Txt:"Search", Type:0, Icon:['checked']}, 
{Txt:"Lock", Txt1:"Unlock", Type:2, Status:1, Icon:['checked','checked']}, 
{Txt:"Group by this field", Type:2, Status:0, Icon:['checked','checked']}, 
{Txt:"Hidden", Type:0 }
];
 
*/

var moMenu={  WW:0, HH:0, M:null, Hmax:0, NM:"menutempcanvasfin", IM: "",
          icoCnv:[], Fin:[], immg:[], 
          Zi:2000, Zoom:1, ZoomT:1.2, marg:4, CC:0, TT:0, 
          EVENT_RESIZE:'', EVENT_DOWN:'', ZZ:1, endFNC:"",
          fnt:"14px opensansL",
          H4:10, HN4:25,

Colors:{
  bg:"rgba(74,102,113,0.9)",        // #4A6671
  bgOver:"rgba(111,141,151,0.8)",   // #6F8D97
  separator:"#CCC",
  text:"#FFF",
  textOver:"#EEE",
  textDisabled:"#AAA"
}, 


Start:function(n,e,m){  
  with(moMenu){
     
    IM = moGlb.imgPath+"menu/";
    var B={};
    if(!m) { var j=e.target || e.srcElement; B=moGlb.getPosition(j); }
    if(m==1) { var p=moEvtMng.getMouseXY(e); B={l:p.x, t:p.y, w:0, h:0}; }
    if(m==2) B={l:e.l, t:e.t, w:e.w, h:e.h};

    if(!icoCnv.length) setIcoCnv();
    
    Fin=[];
    WW=moGlb.getClientWidth(), HH=moGlb.getClientHeight(); Hmax=HH-marg*2-7;   //  space between rows  7=border+gradient
     
    if(isTouch) ZZ=ZoomT; else ZZ=Zoom;  
    
    
    M=[], nn=eval(n);   
    for(var r=0;r<nn.length;r++) {      
    if( typeof(nn[r].Level)=="undefined" || nn[r].Level&moGlb.user.level) M.push(nn[r]);  }  


    newFin(B,"M"); 
   
    if(isTouch) EVENT_DOWN='touchstart', EVENT_RESIZE='orientationchange';
    else EVENT_DOWN='mousedown', EVENT_RESIZE='resize';   
    moEvtMng.addEvent(document, EVENT_DOWN, moMenu.Close); 
    moEvtMng.addEvent(window, EVENT_RESIZE, moMenu.Close);
  return false; 
  }
},


newFin:function(B,s,i){
  with(moMenu){   if(!i) i=0;   // if more windows 
    var w=0, h=2, r=i, rr=r, g=0, txt="", a=eval(s), n=a.length, u, d, q=[], qq;
    
    // window size w, h
    for(;r<n;r++) { 
        var c={Type:0, Status:0, Icon:["",""], Group:-1, Disable:0, Fnc:"", TxtDx:"", Txt1:"", Sub:[]};
            for(u in c) a[r][u]=(typeof a[r][u]=='undefined')?c[u]:a[r][u];

            txt=(a[r].Status && a[r].Txt1)?a[r].Txt1:moGlb.langTranslate(a[r].Txt); 
            d=0; if(a[r].TxtDx) txt+=a[r].TxtDx, d=10;
            txt=moGlb.langTranslate(txt);
            g=moCnvUtils.measureText(txt,fnt)+d; if(g>w) w=g;

            if(a[r].Type==4) h+=H4;
            else { q.push([rr,h,w]);  h+=HN4; rr=r+1; }  
                                               
            if((h+H4+1)*ZZ>Hmax) { qq=q.pop(); rr=qq[0]; h=qq[1]; w=qq[2];  break; } 
            }
    h+=H4-5; w+=52;
    
    // window position
    var  l=marg, t=marg, nf=Fin.length;

    if(!nf){ 
      if((B.l+w*ZZ)<WW) l=B.l; else { if((w*ZZ)<(B.l+B.w)) l=B.l+B.w-w*ZZ; }                    // first window
      if((B.t+B.h+1+h*ZZ)<HH) t=(B.t+B.h+1); else { if((B.t-h*ZZ)>marg) t=B.t-h*ZZ-marg; }
    } else {                                                                                    // next windows
      l=B.l+B.w-8; t=B.t; 
      if((l+w*ZZ)>WW) { l=B.l-10-w*ZZ; if(l<marg) l=marg;}
      if((B.t+h*ZZ)>(HH-3*ZZ-marg)) { t=HH-marg-(h+3)*ZZ; if(t<marg) t=marg; }
    }

    // create Fin   
    var j=moGlb.createElement(NM+nf,"canvas"), cj=j.getContext('2d');    
    Fin.push({ l:l, t:t, w:w, h:h, j:j, cj:cj, ei:i, ef:rr, et:n, ov:-1, rm:s, Ovr:[] });
    
    // dim Fin  event click move out
    with(j.style) { left=l+"px"; top=t+"px"; zIndex=Zi+nf; }
    j.width=(w+7)*ZZ; j.height=(h+7)*ZZ;
      
    if(!isTouch) { j.onmousedown=function(e){ Down(e,nf); }
                   j.onmouseout=function(e) { Out(e,nf) }
                   j.onmousemove=function(e){ Move(e,nf); }
                 if(!nf && !isTouch) moEvtMng.addEvent(document, "keydown", moMenu.KeyD);
                 }
            else { j.ontouchstart=function(e) { Down(e,nf); }  }
    
    // preload img icon and draw
    var  icc=0, CC=0, H=2; 
    for(u=i;u<rr;u++) {
    
    if(a[u].Type==4) { H+=H4; Fin[nf].Ovr.push([-1,-1,u]); }       // over posiz
    else { H+=HN4; Fin[nf].Ovr.push([H-HN4,H,u]); }
    
    var k=a[u].Icon[a[u].Status];
    if(typeof(icoCnv[k])!="undefined") continue; 
    
    if(k && !immg[k]) { 
      var ak=k.split(".");
      if(ak[0]!="FlaticonsStroke") { immg[k] = moGlb.img["menu/"+k]; icc++; CC++; }
     }

    }    
    if(icc) imgWait(icc,0,nf); else Draw(nf); 
  }
},


imgWait:function(i,k,nf){ k++;
    with(moMenu){ if(i==CC || k>200) { Draw(nf); return; } 
    } setTimeout("moMenu.imgWait("+i+","+k+","+nf+")",20);
},
   
   
Draw:function(f){  
    with(moMenu){
        
    with(Fin[f]){
        with(cj){ 
        clearRect(0,0,(w+7)*ZZ,(h+7)*ZZ);
        var a=eval(rm), r=0, hh=3, tp=0, tx='', g, q=-1;
    
    save(); scale(ZZ,ZZ);

  clearRect(0,0,w,h);  

        fillStyle=Colors.bg;   
        moCnvUtils.roundRect(cj,0,0,w,h,2); fill();    

        // text
        font=fnt; textBaseline="alphabetic"; textAlign="left"; 

    if(ef<et) q=ef-1; 
    for(r=ei;r<ef;r++) { tp=(r!=q)?a[r].Type:5; 

        // separator
        if(tp==4) { 
        fillStyle=Colors.separator; fillRect(8,hh+5,w-16,1);
     
        hh+=H4;   
        continue; }
 
        fillStyle=Colors.text;
        if(ov==r) {  fillStyle=Colors.bgOver; 
            fillRect(0, Ovr[r-ei][0]-2, w, Ovr[r-ei][1]-Ovr[r-ei][0]+6);
            fillStyle=Colors.textOver; }
 
        // text
        if(tp!=5) { tx=(a[r].Status && a[r].Txt1)?a[r].Txt1:moGlb.langTranslate(a[r].Txt);  
                    tx=moGlb.langTranslate(tx);
                   }
          else { tx="..."; font="bold "+fnt; } 
        if(tx) { if(a[r].Disable) fillStyle=Colors.textDisabled;
                 fillText(tx,30,hh+H4+8);
                 tx=moGlb.langTranslate(a[r].TxtDx); 
                 if(tx){ g=moCnvUtils.measureText(tx,fnt);
                 fillText(tx,w-g-21,hh+H4+8);} }
               
         // arrow dx
        if(tp==1 || tp==5) { beginPath(); fillStyle=Colors.separator;
        moveTo(w-13,hh+9);lineTo(w-9,hh+12.5);lineTo(w-13,hh+16);
        closePath();fill();
        }       
                                        
        //icon
        if(tp!=5){ var k=a[r].Icon[a[r].Status],colfk=""; 
        if(typeof(icoCnv[k])!="undefined") { var x=4, y=hh+2; eval(icoCnv[k]); }
        else if(k) { 
                var kc,ak=k.split(".");
                if(ak[0]=="FlaticonsStroke") {
                  var aak=ak[1].split("#"); 
                  kc=parseInt(aak[0]);
                  if(aak.length==2) colfk="#"+aak[1];
                  
                  save();
                  font="16px FlaticonsStroke";
                  textBaseline="top"; textAlign="left";
                  if(colfk) fillStyle=colfk;
                  else { if(a[r].Disable) fillStyle="#999"; else fillStyle="#DDD"; }
                  
                  fillText(String.fromCharCode(kc),7,hh+5);
                  restore();   
                } else { /* drawImage(immg[k], 4, hh+4); */ } 
             
             }
        } 
               
        hh+=HN4;
        }
    restore();
    }}}
},


Move:function(e,i){    
  with(moMenu){ 
    with(Fin[i]){
      var P=moEvtMng.getMouseXY(e), y=P.y-t, o=-1, q=-2; 
      if(ef<et) q=ef-1;
      for(var r=0;r<Ovr.length;r++){
        if( y>=Ovr[r][0]*ZZ && y<=Ovr[r][1]*ZZ ) { o=Ovr[r][2];  break; }
        }  
      if(ov!=o) { ov=o; Draw(i);
         if(ov==q) {  TT=0;  Twait(i); }
         else { var a=eval(rm);  TT=-10;                
                if(ov>-1) { if(a[ov].Type==1 && !a[ov].Disable && !isTouch) {  TT=0; Twait(i); }              
               }}        
         if(Fin.length>(i+1)) Close(0,i+1);
      }  
    }
  return false;
  }
},


Twait:function(i){
  with(moMenu){
      if(TT<0 || isTouch || !Fin.length) return; 
      TT++; if(TT>18) { with(Fin[i]){ 
                        if(ov>-1){ var m=0, s=rm, ii=ef-1, o=ov-ei;
                          if(ei!=0 || ef<et) m=1;          
                          if(!m || ov!=ii) { s+="["+ov+"].Sub"; ii=0; }
                         var g=Ovr[o][0]*ZZ, B={l:l+9, t:t+g, w:w*ZZ, h:0 };
                          var checkVisibility = eval(s);
                          var lunghezza = checkVisibility.length;
                          while (lunghezza--) {
                            if(! (typeof(checkVisibility[lunghezza].Level)=="undefined" || checkVisibility[lunghezza].Level&moGlb.user.level)) 
                              checkVisibility.splice(lunghezza,1);  
                          } 
                         newFin(B,s,ii); }   
                         }
                      return; } 
      }
  setTimeout("moMenu.Twait("+i+")",20);
},


Down:function(e,i){
  with(moMenu){  
    with(Fin[i]){
      var P=(isTouch)?moEvtMng.getTouchXY(e,0):moEvtMng.getMouseXY(e), x=P.x, y=P.y-t, o=-1, q=-2, a=eval(rm); 
      if(ef<et) q=ef-1;
  moEvtMng.cancelEvent(e);
      for(var r=0;r<Ovr.length;r++){
        if( y>=Ovr[r][0]*ZZ && y<=Ovr[r][1]*ZZ ) { o=Ovr[r][2];  break; }
        }  
      ov=o; Draw(i);
      if(typeof a[ov] == "undefined") return false;
      if(!a[ov].Disable){      
      if(ov>-1){  var ty=a[ov].Type;
        if(ov==q || ty==1) {
          if(isTouch){    
                var m=0, s=rm, ii=ef-1, o=ov-ei;
                 if(ei!=0 || ef<et) m=1;          
                 if(!m || ov!=ii) { s+="["+ov+"].Sub"; ii=0; }
                 var g=Ovr[o][0]*ZZ, B={l:l+9, t:t+g, w:w*ZZ, h:0 };

                 if(Fin.length>(i+1)) Close(0,i+1);
                 newFin(B,s,ii);
                     } else  return false;        
            } else Sele(ty,i);       
  }}}}
  return false;
},


Sele:function(ty,i){
  with(moMenu) with(Fin[i]) {  var  a=eval(rm);
    if(ty==2) { a[ov].Status=a[ov].Status?0:1; }
    if(ty==3) { var gr=a[ov].Group; 
                for(var r=0;r<a.length;r++) { if(a[r].Group==gr) a[r].Status=0; }
                a[ov].Status=1; } 

    if(a[ov].Fnc) { 
       if(a[ov].NoClose) eval(a[ov].Fnc); 
       else {
         Close(0,0); 
         eval(a[ov].Fnc); 
         return false;
       } 
    }
    
    if(a[ov].NoClose) { Draw(i); return false; }                                              // NoClose
    Close(0,0);
  }
},


Out:function(e,i){
  with(moMenu){ 
  if(Fin.length==(i+1)) { Fin[i].ov=-1; Draw(i); }  
  }
},


KeyD:function(e){ e=e||window.event;
  with(moMenu){ 
    var k=(e.which)?e.which:e.keyCode, fx=0, fy=0, i=Fin.length-1, q=-2; 
    if(k==37) fx=-1; if(k==39) fx=1;
    if(k==38) fy=-1; if(k==40) fy=1;
    if(k==27) { if(i) fx=-1; else { Close(0); return false; } } 
    if(fx || fy || k==13) {  
    with(Fin[i]){ var a=eval(rm);
      if(fy){ if(ov<0) ov=(fy<0)?(ef-1):ei;
              else { for(var c=0;c<(ef-ei);c++) { ov=ovfy(ov,fy,ei,ef);
                     if(a[ov].Type!=4) break; }
              } Draw(i); }             
      if(fx<0 && i) { Close(0,i); return false; }
      if(ov>-1){ var ty=a[ov].Type; if(ef<et) q=ef-1;          
          if((fx>0 || k==13) && (ty==1 || q==ov)) { 
              var s=rm, o=ov-ei;  
              if(ty==1) { s+="["+ov+"].Sub"; q=0; }
              var g=Ovr[o][0]*ZZ, B={l:l+9, t:t+g, w:w*ZZ, h:0 };
              newFin(B,s,q); return; }                     
         if(k==13) { if(ty!=4) Sele(ty,i); }
    }}}}
},


ovfy:function(ov,fy,ei,ef){
 ov+=fy; if(ov<ei) ov=ef-1;
 if(ov>=ef) ov=ei;
 return ov;
},


setIcoCnv:function(){
   with(moMenu){     
    icoCnv["radio"]="fillStyle=Colors.separator;fillRect(x+8,y+7,4,6);fillRect(x+7,y+8,6,4);"; 
    icoCnv["checked"]="fillStyle=Colors.separator;for(var u=4;u<11;u++){uu=u+2;if(u>6)uu=14-u;fillRect(x+u+2,y+uu+2,1,3);}";        
  }
},


Close:function(e,i){
  with(moMenu){  if(!i) i=0;
  var J=document.body, nf=Fin.length;
  for(var r=i;r<nf;r++) J.removeChild(Fin[r].j); 
  for(var r=i;r<nf;r++) Fin.pop();
  
  if(!i) { moEvtMng.subEvent(document, EVENT_DOWN, moMenu.Close); 
           if(!isTouch) moEvtMng.subEvent(document, "keydown", moMenu.KeyD);
           moEvtMng.subEvent(window, EVENT_RESIZE, moMenu.Resize); 
           if(endFNC) eval(endFNC);                             
           endFNC="";
           }
  }
}
} // end moMenu                            

 