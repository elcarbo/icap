var moSelect = function (name) {
    this.w = 183, this.h = 27, this.className = "", this.options = [], this.category = "", this.reload = false,
    this.type = "moSelect", this.server = "", this.querystring = "", this.dIndex = 0;
    this.create = function (props) {
        with(this) {
            baseCreate(props);
            var field = moGlb.createElement(fieldid, "select"), o = this;
            field.setAttribute("class", className);
            field.setAttribute("style", "width:"+w+"px;height:"+h+"px;");
            moEvtMng.addEvent(field, "change", function () {
                var s = eval(name).getOption();
                eval(name).setProperties({value: s[0], description: s[1]});
                moLibrary.exec(onchange, o);
            });
            integrityCheck();setStyle();field.style.display = "none";
            if(options.length > 0) setOptions(); appendInfo();
        }
    };
    this.display = function (l0, t0, cid) {
        with(this) {try {
            if(arguments.length > 0) {
                var field = mf$(fieldid);
                with(field.style) {top = "0px";left = l0+"px";display = "block";}
                if(mf$(cid)) mf$(cid).appendChild(field);
                field.style.display = "block";
            }
            if(reload || moGlb.isempty(options) || options.length == 0) getOptions();
            else if(onload) moLibrary.exec(onload, this);
            } catch(ex) { }
        }
    };
    this.getOptions = function (srv, qs) {
        with(this) {
            if(moGlb.isset(srv)) server = srv; if(moGlb.isset(qs)) querystring = qs; else qs = querystring;
            try {
                var dt = eval(eval(pname).pname).data, chg = {};
                for(var i in dt) chg['{'+i+'}'] = dt[i];
                qs = querystring.multiReplace(chg);
            } catch(ex) {}            
//            if(category) options = moGlb.options[category]; 
//            else if(server) {setLoader(true); Ajax.sendAjaxPost(moGlb.baseUrl+"/"+server, querystring, name+".setOptions");return;}
            if(server) {
                setLoader(true);
                if(server.indexOf('/') > 0) Ajax.sendAjaxPost(moGlb.baseUrl+"/"+server, qs, name+".setOptions");
                else Ajax.sendAjaxPost(moGlb.baseUrl+"/select/get-options", 'script='+server+'&'+qs, name+".setOptions");
                return;
            }
            fill();
        }
    };
    this.setOptions = function (items, fillOnly) {
        with(this) {
            if(moGlb.isset(items) && items !== '') {
                reset(true); setLoader(); options = JSON.parse(items);
                if(options && moGlb.isset(options.message)) { moGlb.alert.show(moGlb.langTranslate(options.message)); return; }                
            }
            fill();
            if(moGlb.isset(items)) {
                try { selectItem(eval(eval(pname).pname).data[bind]); } catch(ex) {} // Go to edit module
                if(!fillOnly && onload) moLibrary.exec(onload, this);
            }
        }
    };
    this.fill = function () {
        with(this) {
            dIndex = 0;
            for(var i in options) {
                if(options[i].selected) dIndex = parseInt(i);
                addOption(options[i]);
            }
            if(!moGlb.isempty(options)) selectItem(options[dIndex].value);  
        }
    };
    this.addOption = function (item, position) {
        with(this) {
            var field = mf$(fieldid);if(!field) return;
            position = position ? position : field.options.length;
            var opt = new Option(moGlb.langTranslate(item.label), item.value);
            field.options.add(opt, position);
            if(item.selected) selectItem(item.value);            
        }
    };
    this.removeOption = function (key) {
        with(this) {
            var field = mf$(fieldid);            
            if(!field) return;
            for(var i=0; i<field.options.length; i++)
                if(field.options[i].value == key) {
                    field.remove(i);break;
                }
        }
    };
    this.selectItem = function (key) {
        with(this) {
            var field = mf$(fieldid), sIndex = dIndex;
            if(!field || options.length == 0) return;
            for(var i=0; i<field.options.length; i++) if(field.options[i].value == key) { sIndex = i; break; }
            field.selectedIndex = sIndex;
            value = options[sIndex].value; description = options[sIndex].label;
        }
    };
    this.getOption = function (index) {
        with(this) {
            var field = mf$(fieldid);
            if(!moGlb.isset(index)) index = field.selectedIndex; // get selected one
            return [ field.options[index].value, field.options[index].label ];
        }
    };
    this.getItem = function (index) {
        with(this) {
            if(!moGlb.isset(index)) index = mf$(fieldid).selectedIndex; // get selected one
            return options[index];
        }
    };
    this.recharge = function (qs) { with(this) { reset(true); getOptions(server, qs); } };
    this.reset = function (empty) {
        with(this) {
            if(empty) discard();
            //else if(options.length > 0) {selectItem(options[dIndex].value);return;}
            else if(options.length > 0) selectItem(options[dIndex].value);
            batch = false;
        }
    };
    this.discard = function () {
        with(this) {
            var field = mf$(fieldid), count = field.options.length;
            while(field.options.length > 0) field.options.remove(0);
            options = {}; dIndex = 0; value = ''; description = '';
        }
    };
    this.integrityCheck = function () {
        with(this) {
            if(disabled || readonly) mandatory = false;
            else if(mandatory) {disabled = false;visible = true;}
        }
    };    
    this.setStyle = function () {
        with(this) {
            var classes = [ className ];
            if(readonly) classes.push("readonly");
            if(disabled) classes.push("disabled");
            if(mandatory) classes.push("mandatory");
            if(visible) classes.push("notvisible");
            mf$(fieldid).setAttribute("class", classes.join(" "));
        }
    };
    this.setLoader = function (on) {
        with(this) {
            var field = mf$(fieldid);
            if(on) moGlb.addClass(field, "loading");
            else moGlb.removeClass(field, "loading");
        }
    };
    this.setField = function () {
        with(this) {
            if(options.length || this.bind == "f_priority") {
                try {
                    selectItem(value);
                    if(readonly || disabled) mf$(fieldid).setAttribute("disabled", true); else mf$(fieldid).removeAttribute("disabled");
                } catch(ex) {moDebug.log(ex);}
            }
        }
    };    
};
moSelect.prototype = new moUI();