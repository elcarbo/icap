/* Scroll OV

OV  0=verticale   1=orizzontale
ov   0=out 1=over

resize     H altezza scrollbar     HH altezza da rappresentare    
cursTo(i)  cambia posizione cursore

WH  0|1 wheel
*/


function OVscroll(nm,z){
var p={ OV:0, L:0, T:0, H:0, W:isTouch?32:18, curw:8, HH:0, hc:40, hmin:40,  Fnc:"", upFnc:"", ptr:0, displ:1, 
        ov:0, WH:0, pid: moGlb.bd, zi: moGlb.zscroll, Pmaskj:null, maskj:null,
        yc:0, iy:0, dy:0, imv:0 };
        
for(var i in p) this[i]=(typeof z[i]=='undefined')?p[i]:z[i]; 

this.name=nm;
this.oldUp="";
this.oldMv="";

var j=moGlb.createElement("idvscroll"+nm,"canvas", mf$(this.pid));     // J,j = canvas   cnv=context
j.style.zIndex=this.zi; 
this.J=j; this.cnv=j.getContext("2d");

j.style.top="-1000px";

// metodi
this.Display=Display;
this.Draw=Draw;
this.PosPtr=PosPtr;
this.Resize=Resize;
this.Wheel=Wheel;
this.cursTo=cursTo; 
this.Cursore=Cursore;
this.f_evtdown=f_evtdown;
this.f_evtmove=f_evtmove;
this.f_evtup=f_evtup;
 
with(this){
var o=this;

  if(!isTouch) {     
      j.onmouseover=function(){ if(H<HH) { ov=1; o.Cursore(); }  }
      j.onmouseout=function() { if(H<HH) { ov=0; o.Cursore(); }  }
      eval("j.onmousedown=function(e){ "+name+".f_evtdown(e); return false; }");     
  } else {
      eval("j.ontouchstart=function(e){ "+name+".f_evtdown(e); return false; }");
      eval("j.ontouchend=function(e){ "+name+".f_evtup(e); return false; }");
      eval("j.ontouchmove=function(e){ "+name+".f_evtmove(e); return false; }");
  }
 
Resize(); 
}

function f_evtdown(e){
with(this){ 

   if(!H || H>HH) return false;
   
   var p=mf$(pid), P=isTouch?moEvtMng.getTouchXY(e,0,p):moEvtMng.getMouseXY(e,p), ay=OV?P.x:P.y;  
   
   iy=ay-yc, imv=0;
   
   if(!isTouch) {
    oldUp=document.onmouseup, 
    oldMv=document.onmousemove;
    
    eval("document.onmousemove=function(e){ "+name+".f_evtmove(e); return false; }");
    eval("document.onmouseup=function(e){ "+name+".f_evtup(e); return false; }");   
   
    maskj=moGlb.createElement("maskdrag","div");                      
    with(maskj.style) zIndex=moGlb.zdrag-1, left=0,top=0,width='100%',height='100%',backgroundColor="#ff0", opacity="0";  }
 
  moEvtMng.cancelEvent(e);
  return false; 
}}

function f_evtmove(e){
with(this){ 
  imv++; 
  var p=mf$(pid), P=isTouch?moEvtMng.getTouchXY(e,0,p):moEvtMng.getMouseXY(e,p);  
  dy=(OV?P.x:P.y)-iy;
  Draw(); 
  moEvtMng.cancelEvent(e); 
  return false;
}}




function f_evtup(e){
with(this){ 
  
  if(!isTouch){
    document.onmouseup=oldUp; 
    document.onmousemove=oldMv;   
    Pmaskj=maskj.parentNode; 
    Pmaskj.removeChild(maskj);
  }
  
  if(imv<3) {                   // click
    var k,py=iy-(OV?L:T);
    if(py<0 || py>hc){
      k=(py<0)?-1:1;
      ptr+=H*k; PosPtr(ptr); 
  }}      
  
  if(upFnc) { eval(upFnc+"()");   }
  
  moEvtMng.cancelEvent(e);
  return false;
}}



function Wheel(d){   
with(this){ 
WH=1; ptr-=84*d; PosPtr(ptr);
}}

function Display(i){
with(this){  if(i==displ) return;
if(!i) {  var nobl="none";  displ=0;
} else {  var nobl="block"; displ=1; }
J.style.display=nobl;
Draw();
}}

function Resize(z){ if(!z)z={}; 
with(this){
var oH=H, oHH=HH, optr=ptr;
var p={ L:L, T:T, H:H, W:W, HH:HH, ptr:ptr };
for(var i in p) this[i]=(typeof z[i]=='undefined')?p[i]:z[i]; 

hc=parseInt(H*H/HH);   
if(hc<hmin) hc=hmin;
if(H>=HH) hc=0; 
if(H>HH || oH>oHH) ptr=0;
cursTo(ptr);                   
}}

function PosPtr(i){
with(this){   
var h=HH-H;
if(i<0) i=0; if(i>h) i=h;
dy=(H-hc)*i/h;
ptr=i;
Draw();
}}


function cursTo(i){
with(this){  
PosPtr(i);  
if(displ) eval(Fnc+"("+i+")");
}}


function Draw(){
with(this){      

if(HH<1 || !displ) return;

if(H<HH) {  
 yc=dy;  
 if(yc<0) yc=0;
 if(yc>H-hc) yc=H-hc; 
 ptr=parseInt(yc*(HH-H)/(H-hc));
}  

with(J.style){ left=L+"px", top=T+"px"; }  //  backgroundColor="#FF8"
var ovW=W,ovH=H;  if(OV) ovW=H,ovH=W; 
J.width=ovW, J.height=ovH;
 

// disegna
if(H>=HH) return;
Cursore(); 
if(displ)  eval(Fnc+"("+ptr+")"); WH=0; 
}}
 

function Cursore(){
with(this) {
var lw=parseInt((W-curw)/2);  
with(cnv){    
  fillStyle=ov?"rgba(128,128,128,0.8)":"rgba(200,200,200,0.8)"; 
  if(OV) {
    clearRect(0,0,H,W);
    moCnvUtils.roundRect(cnv,yc,lw,hc,curw,2); 
    fill();
  }else {
    clearRect(0,0,W,H);
    moCnvUtils.roundRect(cnv,lw,yc,curw,hc,2);
    fill();
  }
}
}}

} //