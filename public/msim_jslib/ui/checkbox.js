var moCheckbox = function (name) {
    this.type = "moCheckbox", this.buttons = [], this.cols = 0, this.radio = false, this.padding = 4,
    this.server = "", this.querystring = "";
    var btnXY = [], colw = 0, iconWh = 16, rowH = 24, rows = 0, icons = [],
    imgUrls = [ 58783, 58826, 58782, 58806 ],
    colors = { 4: "#496670", 2: "#d6d7d9", 1: "#d6d7d9", 0: "#6e8d96" },
    baseButtons = [], yc = 11; // Correction factor for y in canvas texts
    this.create = function (props) { with(this) { baseCreate(props); if(!padding) padding = 4; if(server && moGlb.isempty(buttons)) getButtons(); else setButtons(); } };
    this.getButtons = function (s,qs) { with(this) { 
            if (s) server=s;
            if (qs) querystring=qs;
            if(server.indexOf('/') > 0)Ajax.sendAjaxPost(moGlb.baseUrl+"/"+server, querystring, name+".setButtons"); 
            else Ajax.sendAjaxPost(moGlb.baseUrl+"/select/get-options", 'script='+server+'&'+querystring, name+".setButtons");
        } };
    this.setButtons = function (btn) {
        with(this) {
            if(btn) buttons = JSON.parse(btn);
            setSize();baseButtons = moArray.clone(buttons);
            if(buttons[0]!==undefined)
            if(buttons[0].hasOwnProperty('forceDisplay'))
            var disp=buttons[0].forceDisplay?'block':'none';
            else var disp='none';
            var cnv = moCnvUtils.createCanvas(fieldid, w, h, "display: "+disp+";", "", "", true), o = this;
            cnv.onclick = function (e) {
                if(readonly || disabled) return;
                var coords = moEvtMng.getMouseXY(e, mf$(fieldid), 0, -eval(eval(name).pname).mbody.scrollTop),
                x = Math.floor(coords.x/colw), y = Math.floor(coords.y/rowH), index = (cols*y)+x;
                if(radio && !buttons[index].selected) {
                    for(var i in buttons) buttons[i].selected = false;
                    buttons[index].selected = true;
                    value = buttons[index].value;
                } else if(!radio) {
                    buttons[index].selected = !buttons[index].selected;
                    var v = [];value = "";for(var i in buttons) if(buttons[i].selected) v.push(buttons[i].value); value = v.join(",");
                }
                draw(); moLibrary.exec(onchange, o);
            };            
            draw(); appendInfo();
        }
    };
    this.draw = function () {
        with(this) {
            var ctx = mf$(fieldid).getContext('2d'), values = [];
            with(ctx) {
                clearRect(0, 0, w, h);
                for(var i=0; i<buttons.length; i++) {
                    var x = colw*(i%cols)+padding, y = (Math.floor(i/cols)*rowH)+padding, color = colors[0], clr_index = parseInt((mandatory?"1":"0")+(readonly?"1":"0")+(disabled?"1":"0"), 2);
                    if(moGlb.isset(colors[clr_index])) color = colors[clr_index];
                    moCnvUtils.cnvIconFont(ctx, imgUrls[parseInt((radio?"1":"0")+(buttons[i].selected?"1":"0"), 2)], x, y, 16, color);
                    moCnvUtils.write(mf$(fieldid).getContext('2d'), moGlb.langTranslate(buttons[i].label), x+padding+iconWh+0.5, y+yc+0.5,
                    (readonly || disabled ? moColors.textDisabled : moColors.text), disabled ? "italic "+moGlb.fontNormal : "", "left");
                    if(buttons[i].selected) values.push(buttons[i].value);
                }
                value = values.join(',');
            }
        }
    };
    this.setSize = function () {
        with(this) {
            if(!cols) cols = buttons.length;
            var maxlength = 0, len = 0;
            for(var i in buttons) {
                len = moCnvUtils.measureText(moGlb.langTranslate(buttons[i].label), moGlb.fontNormal);
                if(len > maxlength) maxlength = len;
            }
            colw = padding*2+iconWh+maxlength, rows = Math.ceil(buttons.length/cols);
            w = colw*cols+padding*2, h = (rowH+padding)*Math.ceil(buttons.length/cols);
        }
    };
    this.display = function (l0, t0, cid) {
        with(this) {
            if(arguments.length > 0) {
                if(mf$(cid)) mf$(cid).appendChild(mf$(fieldid));
                mf$(fieldid).style.display = "block";
                draw();
            }
            moLibrary.exec(onload, this);
        }
    };
    
    this.getItem = function (index) {
        with(this) {
            var v = [];btns = "";
            for(var i in buttons) 
                if(buttons[i].selected) 
                    v.push((buttons[i])); 
            btns = JSON.stringify(v);
            return btns;
        }
    };
    
    this.setField = function () {
        with(this) {
            var v = (value.toString()).split(","), empty = true;
            for(var i in buttons) { buttons[i].selected = moArray.contains(buttons[i].value, v); empty &= !buttons[i].selected; }
            if(empty) { buttons = moArray.clone(baseButtons); var sel = []; for(var i in buttons) if(buttons[i].selected) sel.push(buttons[i].value); value=sel.join(','); } draw();
        }
    };
    this.reset = function () {with(this) {value = "", batch = false, buttons = moArray.clone(baseButtons);draw();}};
};
moCheckbox.prototype = new moUI();