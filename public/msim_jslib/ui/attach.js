var moAttach = {
    filename: "", uploadForm: "uploadForm", contentId: "attachContainer", attachWin: null,
    show: function (params) {
        with(moAttach) {
            var div = (mf$(contentId) ? mf$(contentId) : moAttach.createUIObject());
            div.style.display = "block";moAttach.setForm(params);
            if(!attachWin) attachWin = moComp.createComponent("attachWin", "moPopup", {
                width: 280, height: 130,
                contentid: contentId, title: moGlb.langTranslate("File upload"), zindex: moGlb.zattach
            });            
            attachWin.show();
        }
    },
    remove: function (params) {
        moGlb.confirm.show(moGlb.langTranslate("Are you sure you want to proceed?"), function () {
            Ajax.sendAjaxPost(moGlb.baseUrl+"/uploads/remove", "params="+JSON.stringify(params), params.callback);
        });
//        var resp = moGlb.confirm.show(moGlb.langTranslate("Are you sure you want to proceed?"));
//        if(resp) Ajax.sendAjaxPost(moGlb.baseUrl+"/uploads/remove", "params="+JSON.stringify(params), params.callback);
    },
    hide: function () {try {attachWin.hide();} catch (ex) {}},
    createUIObject: function () {
        var div = moGlb.createElement(moAttach.contentId, "div");
        div.setAttribute("class", "attachWindow");

        var form = moGlb.createElement(moAttach.uploadForm, "form", div, true);
        with(form) {
            setAttribute("name", "frma");setAttribute("method", "post");setAttribute("action", moGlb.baseUrl+"/uploads/upload");
            setAttribute("enctype", "multipart/form-data");setAttribute("target", "ifrm");
            onsubmit = function () { return false; };
            //innerHTML = moGlb.langTranslate("Choose file to attach or insert a link");
        }

        with(moGlb.createElement("", "div", form)) { setAttribute("class", "doc"); innerHTML = moGlb.langTranslate("Select a file"); }
        
        var file = moGlb.createElement("", "input", form);        
        with(file) {setAttribute("name", "f_data");setAttribute("type", "file");onchange = function (e) {moAttach.setCleanFileName(this);}};        
        
        var callback = moGlb.createElement("attachCallback", "input", form);
        with(callback) {setAttribute("name", "callback");setAttribute("type", "hidden");}
        
        var fakename = moGlb.createElement("", "input", form);
        with(fakename) {setAttribute("name", "fakename");setAttribute("class", "default");setAttribute("type", "text");setAttribute("disabled", true);}
        
        var linkname = moGlb.createElement("", "input", form);
        with(linkname) {setAttribute("name", "f_linkname");setAttribute("class", "default");setAttribute("type", "text");}
                
        with(moGlb.createElement("", "div", form)) { setAttribute("class", "url"); innerHTML = moGlb.langTranslate("or paste a web url"); }
        
        var filename = moGlb.createElement("", "input", form);
        with(filename) {setAttribute("name", "f_filename");setAttribute("type", "hidden");}
        
        var fieldname = moGlb.createElement("", "input", form);
        with(fieldname) {setAttribute("name", "f_fieldname");setAttribute("type", "hidden");}
        
        var uploadtype = moGlb.createElement("", "input", form);
        with(uploadtype) {setAttribute("name", "uploadType");setAttribute("type", "hidden");}
        
        var fcode = moGlb.createElement("", "input", form);
        with(fcode) {setAttribute("name", "f_code");setAttribute("type", "hidden");}
        
        var button = moGlb.createElement("", "button", form);
        with(button) {onclick = function (e) {moAttach.attach();};innerHTML = moGlb.langTranslate("Attach");}

        return div;
    },
    setCleanFileName: function (field) {
        moAttach.filename = (field.value.split("\\")).pop();
        with(mf$(moAttach.uploadForm).elements) {fakename.value = moAttach.filename;f_filename.value = moAttach.filename;}
    },
    attach: function () {
        with(mf$(moAttach.uploadForm)) {
            var file = elements.f_data.value, link = elements.f_linkname.value;
            if(file && link) {
                moGlb.alert.show(moGlb.langTranslate("You can only upload a file or set a link"));
                mf$(moAttach.uploadForm).reset();
            } else if(file || link) {
                submit();moAttach.hide();moGlb.loader.show();
            }
        }
    },
    callback: function (msg, callback, name, tmpName) {
        mf$(moAttach.uploadForm).reset();moGlb.loader.hide();
        if(msg) moGlb.alert.show(moGlb.langTranslate(msg));
        else {var fn = callback.split(".");eval(fn[0])[fn[1]](name, tmpName);}
    },
    setForm: function (params) {
        with(this) {
            with(mf$(moAttach.uploadForm)) {
                var d_link = (moGlb.isset(params.uploadType) && params.uploadType == 'image' ? 'none' : 'block');
                getElementsByClassName('doc')[0].style.display = d_link;
                elements.f_linkname.style.display = d_link;
                getElementsByClassName('url')[0].style.display = d_link;
                if(!moGlb.isset(params.uploadType)) params.uploadType = 'doc';                
                for(var key in params) if(moGlb.isset(elements[key])) elements[key].value = params[key];
            }
        }
    }
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// drag drop multiple in file & link
// from treegrids


moDropAttach={
    list:[], totalSize:0, totalProgress:0, TOT:0, 
    LST:[], Fnc:"", 
    MAXSIZE: 50 * 1024 * 1024,                            // 50 Mb
    j:null, cj:null, rj:null, JA:null, mj:null,
    cl_fs:"font:13px opensansR;margin:10px;padding:6px;border-radius:8px;",
    cl_s:"background-color:#A9BCC2;",
    cl_f:"background-color:#FAA;position:relative;left:0px;width:288px;text-align:center;",
    phpupload:"uploads/dragdrop-doc-upload",    //"docs/upload_test.php",     "http://"+moGlb.baseUrl+

// drag drop
handleDrop:function(e,f){  

with(moDropAttach){      
 
  Fnc=f?f:""; 
  
  var Inr="<div style='font:16px opensansR; font-weight: bold; padding: 10px 15px 5px;'>UPLOAD File: <span id='count_moDropAttach'>0</span></div>"+
      "<canvas id='idcnv_moDropAttach' style='margin:2px 0 0 10px;' width='300' height='24'></canvas>"+
      "<div style='position:absolute;left:0px;top:62px;width:320px;height:248px;overflow:auto;'>"+
      "<div id='idresult_moDropAttach'></div></div>";

  LST=[]; list=[];           


  mj=moGlb.createElement("idmask_moDropAttach","div");
  with(mj.style){ 
        position="absolute";
        left="0"; top="0"; width="100%"; height="100%";
        backgroundColor="#000";
        opacity="0.25";
        display="none";
        overflow="hidden";
        zIndex=moGlb.zmask;
  }



  JA=moGlb.createElement("idpop_moDropAttach","div");
  with(JA.style){ 
        position="absolute";
        left="50%";
        top="50%";
        backgroundColor="#EEE";
        border="1px solid #888";
        borderRadius="9px";
        opacity="0.75";
        display="none";
        overflow="hidden";
        zIndex=moGlb.zloader;
  }
 
  JA.innerHTML=Inr;
  j=mf$("idcnv_moDropAttach"), cj=j.getContext("2d"), rj=mf$("idresult_moDropAttach"); 
  boxApp(1,320,320);
  processFiles(e.dataTransfer.files);
}},


// process bunch of files
processFiles:function(filelist) {
with(moDropAttach){
        if(!filelist || !filelist.length || list.length) return;

        totalSize=0;
        totalProgress=0;
        rj.textContent='';

        TOT=filelist.length;
 
        for (var i = 0; i < filelist.length; i++) {
            list.push(filelist[i]);
            totalSize += filelist[i].size;
        }
        uploadNext();
}},


// draw progress
drawProgress:function(progress) {
with(moDropAttach) with(cj){
        clearRect(0, 0, j.width, j.height); 

        beginPath();
        strokeStyle = '#AAA';
        fillStyle = '#A1C24D';
        fillRect(0, 0, progress * 300, 24);
        closePath();

        font = 'bold 14px opensansR';
        fillStyle = '#FFF';
        fillText(Math.floor(progress*100) + '%', 4, 18);
}},

 

// on complete - start next file
handleComplete:function(file,status){
with(moDropAttach){
  
   totalProgress += file.size;
   drawProgress(totalProgress / totalSize);
   uploadNext();
}},



// update progress
handleProgress:function(e) {
with(moDropAttach){
        var progress = totalProgress + e.loaded;
        drawProgress(progress/totalSize);
}},


// upload file
uploadFile:function(file){
with(moDropAttach){

        // prepare XMLHttpRequest
        var s="",jn,jna, xhr = new XMLHttpRequest();
   
        xhr.open('POST', phpupload);
        xhr.onload = function() {
            
            try{ jna=JSON.parse(this.responseText);
                 jn=moGlb.cloneArray(jna[0]);
                 var sz=jn.size?jn.size:"100%";
                 s="<div style='"+cl_fs+cl_s+"'>"+jn.f_title+" - Size: "+sz+"</div>";
                 console.log(jn);
                 LST.push( jn );    // { "name":jn.name, "size":jn.size, "f_code":jn.f_code }
                    
            }catch(e){   }
            
            rj.innerHTML += s;
            handleComplete(file,1);
        };
        xhr.onerror = function() {
            rj.textContent = this.responseText;
            handleComplete(file, 0);
        };
        xhr.upload.onprogress = function(e) {
            handleProgress(e);
        }
        xhr.upload.onloadstart = function(e) {
        }

        // prepare FormData
        var fd = new FormData();  
        fd.append('attachments', file); 
        xhr.send(fd);
}},

// upload next file
uploadNext:function() {

with(moDropAttach){

        if(list.length) { mf$("count_moDropAttach").innerHTML=TOT+" / "+(TOT-(list.length-1));

            var nextFile = list.shift();
            if (nextFile.size >= MAXSIZE) { // 2 Gb
                rj.innerHTML += "<div style='"+cl_fs+cl_f+"'>Too big file (max filesize exceeded)</div>";
                handleComplete(nextFile,0);
            } else {
                uploadFile(nextFile);
            }
        } else {
            // end uploading
            setTimeout("moDropAttach.fClose()",1500); 
        }
}},


fClose:function(){
with(moDropAttach){
  JA.style.opacity="1.0";
  boxApp(0);
  
  // update/show result
  if(Fnc) eval(Fnc+"(moDropAttach.LST);");
}},

// mask + popup show/hide
boxApp:function(m,w,h){   // m: 0 hide    w,h  <0 margin
  var aj=mf$("idpop_moDropAttach"),l,t, q="px";  
  if(!m) nb="none";
  else { nb="block";  
         if(!w || !h) w=-64,h=-80;
         var W=moGlb.getClientWidth(), H=moGlb.getClientHeight();
         if(w<0) w=W+w;
         if(h<0) h=H+h;
         l=-parseInt((w)/2), t=-parseInt((h)/2);
         with(aj.style) marginLeft=l+q, marginTop=t+q, width=w+q,height=h+q; 
  }
   moDropAttach.mj.style.display=nb; aj.style.display=nb;   
   if(m) return {w:w,h:h};
}

 
} //

