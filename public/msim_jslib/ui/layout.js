// lay v3
/*


objname=new LAYOUT("objname", arrLay, index)

Proprietà:
minwh         larghezza minima
Drgw          larghezza box per il drag
name   = nome oggetto
arrLay = array layout
index  = indice array Lay
 
metodi: 
display (s,b)
  s=moGlb.bd;   stringa <div> contenitore Padre del layout
  b             se display 0/1






--------------------------------------------------------------------------------
DATA layout:

idp:-1, ov:0, leaf:0, tp:1, wh:200, level:1, module:1

idp     id padre
ov      0=orizzontale  1=verticale
leaf    0=ha figli  >0 foglia indice del modulo module    // deprecated
level   visibilità modulo
lock    1=bloccato no resize

tp  valore   0=% o 1=fixed
wh  width  o height    (dipende da ov)
--------------------------------------------------------------------------------
 
 
output:

Lysize[i]={ l,t,w,h, ol,ot,ow,oh }
Last:[]
Cnt[i] = [ 0=left, 1=width, 2=top, 3=height ]
Res[i] = {  da:di,ad:i, pdr:p, ov:cur, l:a[0], w:a[1], t:a[2], h:a[3], min: n*minwh , max:  }   // resize tra due (da ad) 
 

j contenitore layout  (display)
J padre di j


* solo i leaf=1  hanno module e level (di visibilità)


*/
function moLayout(nm) {

this.name=nm, this.Lay={}, this.index=0, this.container="", this.active = 0, this.lyt = {}, this.open = false;
this.create = function (p) {
//    this.Lay=p.lay;
//    this.index=p.ind;
    this.lyt = p;
    //this.container = p.container;
    
    this.minwh=moGlb.moduleMinWH;
    
this.Drgw=6, this.cont = null;
this.imgPath = moGlb.baseUrl.replace("/admin", "")+"/public/msim_images/default/_layout/";
var props={J:null, j:null, LY:[], Psz:[], nly:0, Anim:0, noanim:1,tmpnoanim:1,
            Lysize:[], Last:[], Cnt:[], Res:[], LT:[], Drgw2:0}
for(var i in props) this[i]=props[i];


this.aexp=[];

with(this) {
    // creo LY e marco i del (da cancellare)
    //for(var r=0;r<Lay[index].layout.length;r++) {
    

   if(moGlb.getClientWidth()<1025) { 
     var anm=name.split("_");    // alert(name+"\n"+JSON.stringify(lyt.layout))
     if(anm.length==2){ var nvrt=0;
        for(var r=0;r<lyt.layout.length;r++) if(lyt.layout[r].ov) nvrt++; 
        if(nvrt==2 && lyt.layout[1].ov && !lyt.layout[1].module) lyt.layout[1].wh=36;
     }
   } 
    for(var r=0;r<lyt.layout.length;r++) {
        LY[r] = moGlb.cloneArray(lyt.layout[r]);
        if(typeof(LY[r].minv)=="undefined") LY[r]["minv"]=minwh; 
        if(typeof(LY[r].acc)=="undefined") LY[r]["acc"]=0; 
    }
    
    
   // if(name=="lyt_asset") alert(JSON.stringify(LY));
    
}

// metodi
this.display=display;
this.resize=resize;
this.recurBuilt=recurBuilt;
this.minmax=minmax;
this.ridraw=ridraw;
this.drawres=drawres;
this.size=size;
this.delshift=delshift;
this.delrow=delrow;
this.dele=dele;
this.expand=expand;
this.getBoxId=getBoxId;

this.getProperty=getProperty;
this.setProperty=setProperty;
this.canExpand=canExpand;

function display(b, s) {
  with(this) {
      if(b && !j) {
          if(!s) s = Container; else Container = s; 
          J=mf$(s), Psz=moGlb.getPosition(J);
          if(s==moGlb.bd) moResize.add(name);                                                // se layout principale subisce il resize 

          // crea contenitore j e Ly (ridotto) rispetto alla visibilità 
          j = moGlb.createElement(name,"div",J);
          with(j.style) left="0px", top="0px", width="100%", height="100%";

         delshift();

          nly=LY.length;
                   
        if(nly==1){
          var m=LY[0].module;
          LY=[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":38,"acc":0},
              {"idp": 0,"ov":1,"tp":0,"wh":100,"level":-1,"module":m,"lock":0,"minv":38,"acc":0}];
              nly=2;
        }  
          
          
     //  if(name=="lyt_asset" || name=="lyt_wizard") alert(JSON.stringify(LY));   
          
          
          if(!nly) return;
          for(var r=0;r<nly;r++) {
              Lysize[r]={l:0,t:0,w:0,h:0, ol:0,ot:0,ow:0,oh:0};
          }
          
          for(var i=0;i< LY.length;i++) {
              try { (eval(LY[i].module)).display(b); }
              catch (ex) { if(!moGlb.isempty(LY[i].module)) moDebug.log("Warning: Module "+LY[i].module+" doesn't exist"); }
          }          
      }
      if(j) j.style.display = (b) ? "block" : "none";
      for(i=0;i<LY.length;i++) if(LY[i].module && eval(LY[i].module)) eval(LY[i].module).setActive(b);
      if(b) resize();
      active = b;
  }
}

//this.setVisible = function (v) {
//    with(this) {
//        visible = v;
//        for(var i in lyt.layout) if(lyt.layout[i].module) eval(lyt.layout[i].module).setVisible(v);        
//    }
//};

/**
 * get the list of modules in layout
 */
this.getModules = function () {
    with(this) {
        var modules = [];
        for(var i in lyt.layout) if(lyt.layout[i].module) modules.push(lyt.layout[i].module);
        return modules;
    }
};

function getBoxId(nm){
  with(this){    
    for(var i=0; i<LY.length; i++) if(LY[i].module==nm) return i;
  return -1; 
}}


function getProperty(nm, p) {with(this) {return LY[getBoxId(nm)][p];}}
function setProperty(nm, p, v) {with(this) {LY[getBoxId(nm)][p] = v;}}


function resize() {
  with(this){
 
    Drgw2=Drgw+Drgw;
    Psz=moGlb.getPosition(J);
    Cnt[0]=[0, Psz.w, 0, Psz.h];
    
   if(!Psz.w && !Psz.h) return false; 
      
    Res=[];
    recurBuilt(0);
    drawres();
  }
}





function recurBuilt(p){
  with(this){
         // trovo figli e calcolo totale % (s) e fixed (S)  
         var fg=[], fgs=[], fgS=[], cur=LY[p].ov, s=0, S=0, i=0, L=Cnt[p][cur*2], ll=L, T=Cnt[p][cur*2+1], v=0, a=[], MIN;        // S=fixed pixel    s=%     T=totale orizz o vert
         for(var r=1;r<nly;r++) if(LY[r].idp==p) {fg.push(r);i=LY[r].wh;if(LY[r].tp) {fgS.push(r);S+=i;} else {fgs.push(r);s+=i;}}
            
         // se sommatoria fixed diversa da totale pixel
         var nfg=fg.length;
         if(!s && T!=S) {for(r=(nfg-1);r>=0;r--) {i=fg[r];v=LY[i].wh+T-S;MIN=LY[i].minv;if(v>MIN) {LY[i].wh=v;break;}}  
         } else {
         // normalizzazione percentuali su totpixel
         var Ts=T-S, ns=fgs.length, nS=fgS.length, u,uu,su=0, ru;   
                 
         if(Ts!=s) {for(r=0;r<ns;r++) {ru=fgs[r];u=LY[ru].wh;MIN=LY[ru].minv;
                                        if(u==MIN) uu=u;
                                        else {if(r==(ns-1)) uu=Ts-su;else {uu=parseInt(u*Ts/s);}
                                               if(uu<MIN) uu=MIN;}
                                        LY[ru].wh=uu;su+=uu;}                                                              
         if(Ts!=su){
         if(nS) for(r=(nS-1);r>=0;r--) {i=fgS[r];v=LY[i].wh+Ts-su;MIN=LY[i].minv;if(v>MIN) {LY[i].wh=v;break;}} 
         else for(r=(ns-1);r>=0;r--) {i=fgs[r];v=LY[i].wh;MIN=LY[i].minv; 
                    if(v>MIN) {LY[i].wh=v+Ts-su;break;} 
                    if(!r) {LY[fgs[ns-1]].wh=v+Ts-su;break;} 
         }}}}
                
         // for fratelli - calcolo dimensioni box Cnt
         v=0;   
         for(r=0;r<nfg;r++) {i=fg[r];for(var n=0;n<4;n++) a[n]=Cnt[p][n];    
         v=LY[i].wh;a[cur*2]=L, a[cur*2+1]=v;L+=v;
         Cnt[i]=[ a[0], a[1], a[2], a[3] ];

         with(Lysize[i]) {
             if(active) ol=l, ot=t, ow=w, oh=h;
             l=a[0], t=a[2], w=a[1], h=a[3]; 
         }

         // resize
         if(r) {var di=fg[r-1], oo=(cur)?0:1;a[cur*2]-=Drgw, a[cur*2+1]=Drgw2, a[oo*2]+=Drgw, a[oo*2+1]-=Drgw2;  
         var s1=minmax(di,oo), s2=minmax(i,oo);  
  
          Res.push( {da:di,ad:i, pdr:p, ov:cur, l:a[0], w:a[1], t:a[2], h:a[3], minda:s1, maxad:s2} );      // n1*minwh   n2*minwh
         }
         if(!LY[i].module) recurBuilt(i); else ridraw(i);
         } 
}}



function minmax(i,oo){   //   var minda=LY[da].minv*min, maxad=LY[ad].minv*max;
 with(this){
    var r,k, s=0, sum=[], SUM=0, f=0; 
    for(r=0;r<nly;r++) {k=LY[r].idp; 
        if(k==i) {f++;
            if(LY[r].ov==oo) {                              
              if(!LY[r].module) {s=minmax(r,oo);SUM+=s;}
              else SUM+=LY[r].minv;                             
            } else {
              if(!LY[r].module) {s=minmax(r,oo);sum.push(s);}
              else sum.push(LY[i].minv);
            }          
    }}    
    for(r=0;r<sum.length;r++) {if(sum[r]>SUM) SUM=sum[r];}   
    if(!f) SUM=LY[i].minv;
 
    return SUM;  
}}

function ridraw(i){
 with(this){
     try {eval(LY[i].module);} catch(ex) {if(!moGlb.isempty(LY[i].module)) moDebug.log("Warning: Module "+LY[i].module+" doesn't exist");return;}
      var ii=LY[i].module, fres;
      if(!ii) return;
      with(Lysize[i]){fres="modResize"; 
      if(l!=ol || t!=ot || w!=ow || h!=oh) eval(ii+"."+fres+"("+l+","+t+","+w+","+h+",'"+name+"')");
}}}


function drawres(){
  with(this){var rj,dr,d,nn;aexp=[];
    for(r=0;r<Res.length;r++) {
       with(Res[r]) { 

        var LT=[Psz.l,Psz.t], lm=[0,0,0,0];l1=Cnt[da][ov*2]+LT[ov]+minda-Drgw,           
            lm[ov*2]=l1, l2=Cnt[ad][ov*2+1]+Cnt[ad][ov*2]+LT[ov]-maxad+Drgw,         
            lm[ov*2+1]=l2, b=(ov)?t:l, b+=LT[ov]; 
            
            
          if(LY[da].lock || LY[ad].lock) continue;
            if(b>=lm[ov*2] && b<=lm[ov*2+1]){
                if(!mf$("idres"+name+r)) moGlb.createElement("idres"+name+r,"div",j);
                rj=mf$("idres"+name+r); 
                with(rj.style) zIndex=moGlb.zdrag, left=l+"px", top=t+"px", width=w+"px", height=h+"px", overflow="hidden",
                               cursor=(ov)?"n-resize":"e-resize",
                               backgroundColor="";    // moGlb.setOpacity(rj,55);  
                                           
                                           
                d=(ov)?t:l;d+=LT[ov]+Drgw;dr=((d-l1)>(l2-d))?1:0;  
                if(!(d-l1-Drgw) || !(l2-d-Drgw)) dr=(dr)?0:1;        
                nn=(dr)?(l2-Drgw2):l1;              
                                   
            var EVENT_DOWN='onmousedown', EVENT_UP='onmouseup', EVV="";
                if(moGlb.isTouch) EVENT_DOWN='ontouchstart', EVENT_UP='ontouchend', 
                  EVV="jj.ontouchmove=moGlb.drag.Move(event); jj.ontouchend=moGlb.drag.Stop(event);";
                     
               aexp[r]=[l1,l2-Drgw2,nn]; 
               
                rj.innerHTML="<img src='"+imgPath+"fr"+ov+"_"+dr+".png' "+EVENT_UP+"='"+name+".size("+r+","+nn+")' "+EVENT_DOWN+"='moEvtMng.cancelEvent(event)' "+
                             "style='position:absolute;left:50%;top:50%;margin-left:-24px;margin-top:-24px;cursor:pointer;' />";                   
               
                eval("rj."+EVENT_DOWN+"=function(e) { var jj=e.target||e.srcElement; jj.innerHTML=''; jj.style.backgroundColor='#CCC'; "+
                " moGlb.drag.Start(e,{ Vinc:"+(ov+1)+", Lim:1, Lm0:"+lm[0]+", Lm1:"+lm[1]+", Lm2:"+lm[2]+", Lm3:"+lm[3]+", Fnc:'"+name+".size("+r+",0)' }); }");
  }}}}
} 
  
function expand(nm1, nm2, d) {   // d  0:open   1:close
with(this) {

  open=(d==0);
  var g,r,a,b,i=-1,ima,imb;    

  if(nm1.indexOf("edit")==-1) {
    g=getBoxId(nm1);
    ima=LY[g].idp;
    imb=getBoxId(nm2);
    if(LY[imb].idp==ima) ima=g;
    
  } else {
    ima=getBoxId(nm1);
    g=getBoxId(nm2);
    imb=LY[g].idp;
    if(LY[ima].idp==imb) imb=g;
  }

  for(r=0; r< Res.length; r++) {
    a=Res[r].da, b=Res[r].ad;
    if( (a==ima && b==imb) || (b==ima && a==imb) ) { i=r; break; }
  }
  
  //alert(nm1+": "+ima+" | "+nm2+": "+imb); alert(i);

  if(i>-1) eval(name+".size("+i+","+aexp[i][d]+")");     
}}


function canExpand(nm) {
    with(this) {      
        var b = LY[getBoxId(nm)];
        return b.acc > 0;
    }
}

function size(i,nn){   // nn=0 drag
  with(this){ 
    with(Res[i]) {var L,T,whda,whad, d, z=1;  
    
      var v1=LY[da].wh, v2=LY[ad].wh, v12=v1+v2; 
            
      if(!nn) with(mf$("idres"+name+i).style) L=parseInt(left), T=parseInt(top);
      else {L=nn-Psz.l, T=nn-Psz.t;  
          
         d=(ov)?(T-t):(L-l);              
         var vv1=v1+d, vv2=v12-vv1; 
                 
      if(!Last[da] || !Last[ad] || LY[da].acc || LY[ad].acc ){  }
      else{
        if(v1==minda || v2==maxad ) {if(v1==v2) return;
        z=0;
        if(v1==minda) {whda=Last[da], whad=v12-whda;if(whad<maxad) z=1;}
        if(v2==maxad) {whad=Last[ad], whda=v12-whad;if(whda<minda) z=1;}            
 
        if(!z) LY[da].wh=whda, LY[ad].wh=whad;
      }}}      
      if(z){d=(ov)?(T-t):(L-l);  
        LY[da].wh+=d; 
        LY[ad].wh=v12-LY[da].wh;} 
      Last[da]=v1;
      Last[ad]=v2;

      resize(); 
          
  }}
  return false;
}   
  
  

function delshift(){
  with(this){
  var i=0, r=1;
  for(;r<LY.length;r++) { 
    if(!(LY[r].level & moGlb.user.level) ) {   // LY[r].leaf && 
      i=r;
      break; 
    }  
  }  
  if(i) {delrow(i); delshift();}
  }
}
 
 
function delrow(i){  
with(this){
       var r,p=LY[i].idp, fg=[], n=0, son=0;
       for(r=1;r<LY.length;r++)  if(LY[r].idp==p) {fg.push(r); if(r!=i) son=r; n++;} 
       if(n>2) dele(i);
       else {
        if(LY[son].module) {
          ii=son;
          LY[p].module=LY[ii].module;
          LY[p].level=LY[ii].level;
          dele(fg[1]);
          dele(fg[0]);
        } else {
          var pp=LY[p].idp; 
          for(var r=i;r<LY.length;r++) if(LY[r].idp==son) LY[r].idp=(pp<0)?0:pp;
          dele(fg[1]);
          dele(fg[0]);
          if(pp<0) LY[0].ov=(LY[0].ov)?0:1; 
          else dele(p);
        }
      }                       
}}


function dele(i){
  with(this){
    LY.splice(i,1);
    for(var r=i;r<LY.length;r++) if(LY[r].idp>i) LY[r].idp--;
  }
}
};
} // fine LAYOUT