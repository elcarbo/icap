var moUI = function (name) {
    this.name = name, this.w = 0, this.h = 0, this.t = 0, this.l = 0,
    this.mandatory = false, this.readonly = false, this.visible = true, this.disabled = false, //this.level = -1, this.level_edit = -1,
    this.m_on = false, this.m_level = 0, this.m_selector = '', this.m_wf = '', this.m_type = '', this.m_value = '', // mandatory
    this.v_on = true, this.v_level = -1, this.v_selector = '', this.v_wf = '', this.v_type = '', this.v_value = '', // visible
    this.r_on = false, this.r_level = 0, this.r_selector = '', this.r_wf = '', this.r_type = '', this.r_value = '', // readonly
    this.d_on = false, this.d_level = 0, this.d_selector = '', this.d_wf = '', this.d_type = '', this.d_value = '', // disabled
    this.error = false, this.onchange = "", this.onload = "", this.onclear = "",
    this.value = "", this.label = "", this.description = "", this.info = "", /*this.innervalue = "",*/
    this.fieldid = "", this.className = "field", this.borderWidth = 1, this.bind = "", this.componentBox = "", this.format = "", this.pname = "",
    this.dtmpicker = "", this.batch = false, this.groupName = "msim-default", this.nomobile = false, this.unique = false,
    //this.v_wf = 0, this.v_phase = 0, this.v_type_id = 0, this.v_selectors = "";//, this.t_selectors = "", this.t_wf = {}, this.types = "";
    
    this.baseCreate = function (props) {
        with(this) {
            moGlb.mergeProperties(this, props);fieldid = name+"_comp";
            if(!groupName) groupName = "msim-default";
            if(!onload && moGlb.isset(moLibrary[name+"_onload"])) onload = name+"_onload";
            if(!onchange && moGlb.isset(moLibrary[name+"_onchange"])) onchange = name+"_onchange";
            if(!onclear && moGlb.isset(moLibrary[name+"_onclear"])) onclear = name+"_onclear";
            if(!info) info = label;
        }
    };
    this.create = function (props) { with(this) { baseCreate(props); appendInfo(); } };
    this.appendInfo = function () {};    
    this.setProperties = function (props) { //console.log('props: ' + props.value);
        if(props.value && !moGlb.isset(props.description)) props.description = props.value;
        if(!props.info && props.label) props.info = props.label;
        moGlb.mergeProperties(this, props);
        with(this) {
            if(dtmpicker == "datetime" || dtmpicker == "date" || dtmpicker == "time") {
                if(value) {
                    value = parseInt(value);
                    if(!isNaN(value)) {
                        if(dtmpicker == "datetime") description = new Date(value*1000).format(moGlb.localization.f_datetime);
                        else if(dtmpicker == "date") description = new Date(value*1000).format(moGlb.localization.f_date);
                        else if(dtmpicker == "time") description = new Date(value*1000).format(moGlb.localization.f_time);
                    } else description = "";
                    if(description == "0") description = "";
                } else description = "";
            } else if(!moGlb.isempty(value) && moGlb.isset(this.attach) && attach) { description = value.split("|")[1]; }
            try {
                var exp = checkExp.split("_");
                if(value && exp[0] == 'decimal') { value = parseFloat(value).toFixed(parseInt(exp[1])); if(isNaN(value)) value = parseFloat(0).toFixed(parseInt(exp[1])); description = value; }
            } catch(ex) { /* sets value only if the fiels is a textfield */ }
            mandatory = isMandatory(); readonly = isReadonly(); visible = isVisible(); disabled = isDisabled();
            var oldpck = (typeof oldPck !== 'undefined' && oldPck == true)? true:false;
            if (typeof isbatch !== "undefined")
                if (isbatch && (isReadonly()||isDisabled()||oldpck)) {
                    visible = false;
                    mandatory = false;
                }   
            
            integrityCheck();setField();checkField();setVisibility();//setVisibility(moGlb.isset(props.visible));
            try { if(eval(eval(pname).pname).isBatch) computeBatch(); } catch(ex) {}
        }
    };
    this.getProperty = function (prop) {try {return this[prop];} catch(ex) {return null;}};
    this.integrityCheck = function () {};
    this.setStyle = function () {};
    this.checkField = function () { this.setStyle(); };
    this.setError = function (err) { with(this) { error = err; setStyle(); } };
    this.getSize = function () {with(this) {return {w: w, h: h}}};
    this.getPosition = function () {return moGlb.getPosition(mf$(this.fieldid));};
    this.getValue = function (cast) {
        with(this) {
            var v = value;
            if(cast == 'int') v = parseInt(value);
            else if(cast == 'double') v = parseFloat(value);
            else return value;
            return (isNaN(v) ? 0 : v);
        }
    };
    this.get = function (cast) {return this.getValue(cast);};
    this.set = function (v) { this.setProperties({ value: v, description: v}); };
    this.setVisibility = function (refreshScroll) {
        with(this) {
            if(!moGlb.isempty(componentBox)) {
                moGlb[(visible?"remove":"add")+"Class"](mf$(componentBox), "hide");
                eval(pname).setFieldsetVisibility(groupName);
                //if(refreshScroll) eval(pname).setScrollbar();
                eval(pname).setScrollbar();
            }
        }
    };
    this.setData = function (data) {
        with(this) {
            setProperties(data); moLibrary.exec(onload, this);
        }
    };
    this.computeBatch = function () {
        with(this) {if(batch && isBatchable()) return; mf$(name+"_batch").setAttribute("class", "batch "+(isBatchable() ? "off" : "hide")); }
    };
    this.setBatch = function () {with(this) { batch = !batch;mf$(name+"_batch").setAttribute("class", "batch "+(batch ? "on" : "off"));}};
    this.consistencyCheck = function (batchMode) {with(this) {return (!error && ((batchMode && (!visible || !batch || !mandatory || !moGlb.isempty(value))) || (!visible || !mandatory || value)));}};
    this.mandatoryCheck = function () { with(this) { return !mandatory || value != ''; } };
    this.errorCheck = function () { return (!this.isVisible() || !this.error); };
    this.isBatchable = function () {
        with(this) {
            return (
                    /*!m_level && !m_selector && !m_wf && !m_type && !m_value &&
                v_on &&  v_level == -1 && !v_selector && !v_wf && !v_type && !v_value &&
               // !r_on && !r_level && !r_selector && !r_wf && !r_type && !r_value &&
                !d_on && !d_level && !d_selector && !d_wf && !d_type && !d_value
                */
                 v_on                 
            );
        }
    };
    this.isProp = function (prop) {
        var tp = prop.substr(0, 1), on = this[tp+'_on'], lv = this[tp+'_level'], sl = this[tp+'_selector'], wf = this[tp+'_wf'],
        ty = this[tp+'_type'], vl = this[tp+'_value'];        
        with(this) {
            this.evaluate = function (val, cond) {
                if(cond.substr(0, 2) == '!=') {
                    //console.log("'"+val+"' != "+cond.substr(2)+" => "+eval("'"+val+"' != "+cond.substr(2)));
                    return eval("'"+val+"' != "+cond.substr(2)) == 1;
                }
                if(cond.substr(0, 2) == '>=') {
                    //console.log(val+' >= '+cond.substr(2)+' => '+eval(val+' >= '+cond.substr(2)));                    
                    return eval(val+' >= '+parseFloat(cond.substr(2))) == 1;
                }                
                if(cond.substr(0, 2) == '<=') {
                    //console.log(val+' <= '+cond.substr(2)+' => '+eval(val+' <= '+cond.substr(2)));
                    return eval(val+' <= '+parseFloat(cond.substr(2))) == 1;
                }
                if(cond.substr(0, 1) == '=') {
                    if(typeof val == 'string') val = "'"+val+"'";
                    //console.log(name+": "+val+' == '+cond.substr(1)+' => '+eval(val+' == '+cond.substr(1)));
                    return eval(val+' == '+cond.substr(1)) == 1;
                }
                if(cond.substr(0, 1) == '>') {
                    //console.log(val+' > '+cond.substr(1)+' => '+eval(val+' > '+cond.substr(1)));
                    return eval(val+' > '+parseFloat(cond.substr(1))) == 1;
                }
                if(cond.substr(0, 1) == '<') {
                    //console.log(val+' < '+cond.substr(1)+' => '+eval(val+' < '+cond.substr(1)));
                    return eval(val+' < '+parseFloat(cond.substr(1))) == 1;
                }
                if(cond.substr(0, 1) == '%' && cond.substr(cond.length-1, 1) == '%') {
                    //console.log(val.indexOf(cond.substr(1, cond.length-2)) > -1+' => '+eval(val.indexOf(cond.substr(1, cond.length-2)) > -1));
                    return val.indexOf(cond.substr(1, cond.length-2)) > -1;
                }
                if(cond.substr(cond.length-1, 1) == '%') {
                    //console.log(val.indexOf(cond.substr(0, cond.length-2)) == 0+' => '+eval(val.indexOf(cond.substr(0, cond.length-2)) == 0));                    
                    return val.indexOf(cond.substr(0, cond.length-2)) == 0;
                }
                if(cond.substr(0, 1) == '%') {
                    //console.log(val.indexOf(cond.substr(1)) == val.length-cond.length-1+' => '+eval(val.indexOf(cond.substr(1)) == val.length-cond.length-1));
                    return val.indexOf(cond.substr(1)) == val.length-cond.length-1;
                }                
            };
            
            var dt, rValue = (prop == 'visible');
            try { dt = eval(eval(pname).pname).data; } catch(ex) { return on; };
            if(!moGlb.isempty(vl)) { // check property for values from other fields
                if(typeof vl == 'string') vl = JSON.parse(vl);
                for(var eOr in vl) {
                    var aRes = true; if(prop == 'visible') rValue = false;
                    for(var eAnd in vl[eOr]) {
                        //if(name == 'txtfld_wo_edit_description') console.log("aRes: "+aRes+" && "+evaluate(dt[eAnd], vl[eOr][eAnd]));
                        aRes = aRes && evaluate(dt[eAnd], vl[eOr][eAnd]);
                    }
                    //if(name == 'txtfld_wo_edit_description') console.log("rValue: "+rValue+" || "+aRes);
                    rValue = aRes || rValue;
                }
            }            
//            if(prop == 'visible' && name == 'txtfld_asset_edit_description') {
//                console.log(name+" ----------- "+prop+" => "+( // object inner visibility
//                on && // object inner visibility
//                (moGlb.user.level & lv) && // user level
//                (moGlb.isempty(ty) || moArray.contains(dt.f_type_id, ty.split(","))) && // object type
//                (moGlb.isempty(wf) || moArray.contains(dt.f_wf_id+'_'+dt.f_phase_id, wf.split(','))) && // workflow
//                (moGlb.isempty(sl) || moGlb.isempty(dt.t_selectors) || !moGlb.isempty(moArray.intersect(sl.split(','), dt.t_selectors.split(",")))) &&
//                rValue));
//                console.log("value: "+rValue);
//            }
//            if(name == 'txtfld_asset_edit_description') {
//                console.log('-------------------');
////                console.log(on);
//                console.log(moGlb.user.level);
//                console.log(lv);
//                console.log((moGlb.user.level & lv));
//                console.log(v_level);
////                console.log((moGlb.isempty(ty) || moArray.contains(dt.f_type_id, ty.split(","))));
////                console.log((moGlb.isempty(wf) || moArray.contains(dt.f_wf_id+'_'+dt.f_phase_id, wf.split(','))));                
////                console.log(moGlb.isempty(sl));
////                console.log(!moGlb.isset(dt.t_selectors));
////                console.log(moGlb.isempty(dt.t_selectors));
////                console.log(!moGlb.isempty(moArray.intersect(sl.split(','), dt.t_selectors.split(","))));
//                console.log((moGlb.isempty(sl) || !moGlb.isset(dt.t_selectors) || moGlb.isempty(dt.t_selectors) || !moGlb.isempty(moArray.intersect(sl.split(','), dt.t_selectors.split(",")))));
//                console.log(rValue);
//            }
            var res;
            if(prop == 'visible') {
                res = on && // object inner visibility
                (moGlb.user.level & lv) && // user level
                (moGlb.isempty(ty) || moArray.contains(dt.f_type_id, ty.split(","))) && // object type
                (moGlb.isempty(wf) || moArray.contains(dt.f_wf_id+'_'+dt.f_phase_id, wf.split(','))) && // workflow
                (moGlb.isempty(sl) || !moGlb.isset(dt.t_selectors) || moGlb.isempty(dt.t_selectors) || !moGlb.isempty(moArray.intersect(sl.split(','), dt.t_selectors.split(",")))) &&
                rValue; // values
            } else {
                res = on || // object inner visibility
                (moGlb.user.level > -1 && (moGlb.user.level & lv)) || // user level
                (!moGlb.isempty(ty) && moArray.contains(dt.f_type_id, ty.split(","))) || // object type
                (!moGlb.isempty(wf) && moArray.contains(dt.f_wf_id+'_'+dt.f_phase_id, wf.split(','))) || // workflow
                (!moGlb.isempty(sl) && moGlb.isset(dt.t_selectors) && !moGlb.isempty(dt.t_selectors) && !moGlb.isempty(moArray.intersect(sl.split(','), dt.t_selectors.split(",")))) ||
                rValue; // values
            }
            return res;
        }
    };
    this.isMandatory = function () { return this.isProp('mandatory'); };
    this.isVisible = function () { return this.isProp('visible'); 
//        with(this) {
//           return visible && // object inner visibility
//            (moGlb.user.level & level) && // user level
//            (moGlb.isempty(types) || moArray.contains(v_type_id, types.split(","))) && // object type
//            (moGlb.isempty(t_wf) || !moGlb.isset(t_wf.visible) || !moGlb.isset(t_wf.visible[v_wf]) || moArray.contains(v_phase, t_wf.visible[v_.wf])) &&
//            (moGlb.isempty(t_selectors) || (!moGlb.isempty(v_selectors) && !moGlb.isempty(moArray.intersect(t_selectors.split(","), v_selectors.split(","))))); // selectors
//        }
    };
    this.isReadonly = function () { return this.isProp('readonly'); };
    this.isDisabled = function () { return this.isProp('disabled'); };
    this.getEditModule = function () { with(this) { return eval(eval(pname).pname); } };
    this.setFocus = function () { try { mf$(this.fieldid).focus(); } catch(ex) {} };
};