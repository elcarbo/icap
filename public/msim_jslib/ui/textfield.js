var moTextField = function (name) {
    this.w = 175, this.h = 21, this.text = false, this.dtmpicker = "", this.type = "moTextField", this.maxlength = 0,
    this.autocomplete = false, this.className = "", this.attach = false, this.pwd = false, this.dvalue = "", this.checkExp = "",
    this.ac = {acId: "", itemH: 20, shadow: 3, padding: 3, ipadding: 2, margin: 0, w: 0, h: 0, over: -1, results: [],
        select: "", from: "", where: "", order: "", groupby: "", having: "",
        draw: function () { 
            with(eval(name).ac) {
                var cnv = mf$(acId), ctx = cnv.getContext('2d');
                ctx.clearRect(0, 0, (w+shadow)*moGlb.zoom, (h+shadow)*moGlb.zoom);
                
                // Shadow
                with(ctx) {
                    save();scale(moGlb.zoom, moGlb.zoom);save();
                    shadowOffsetX = shadow*moGlb.zoom/3*2, shadowOffsetY = shadow*moGlb.zoom/3*2;
                    shadowBlur = shadow*moGlb.zoom, shadowColor=moColors.shadow;
                    fillRect(shadow*moGlb.zoom, shadow*moGlb.zoom, w-shadow*moGlb.zoom, h-shadow*moGlb.zoom);
                    restore();
                }
                
                h = padding*2+itemH*results.length;cnv.height = h;
                for(var i=0; i<results.length; i++) {
                    // var splitted = results[i].split(label);
                    if(i == over) {with(ctx) {fillStyle = moColors.bgOver;fillRect(padding, itemH*i+padding, w-padding, itemH);}}
                    moCnvUtils.write(ctx, results[i], padding+ipadding, itemH*i+13+padding, (i == over ? moColors.textOver : moColors.text), moGlb.fontNormal, "left");                    
                }
            }
        },
        display: function (l, t) {
            with(eval(name).ac) {
                var acd = mf$(acId);
                //with(acd.style) {top = t+"px";left = l+"px";display = (visible ? "block" : "none");}
                with(acd.style) {top = t+"px";left = l+"px";display = "block";}
            }
        },
        hide: function () {with(eval(name).ac) {mf$(acId).style.display = "none";}},
        selectItem: function () {with(eval(name)) {setProperties({value: ac.results[ac.over], description: ac.results[ac.over]});ac.over = -1;ac.hide();moLibrary.exec(onchange, eval(name));}},
        getResults: function (v) {
            with(eval(name)) {
                //if(v) Ajax.sendAjaxPost(moGlb.baseUrl+"/autocomplete/get-list", "field="+bind+"&table="+autocomplete+"&value="+v, name+".ac.setResults");
                var ids_wo = [];
                if(typeof mdl_wo_tg != 'undefined') {
                    ids_wo = mdl_wo_tg.getSelectedIds();
                }
                var ids_ware = [];
                if(typeof mdl_ware_tg != 'undefined') {
                    ids_ware = mdl_ware_tg.getSelectedIds();
                }
                if(v) Ajax.sendAjaxPost(moGlb.baseUrl+"/autocomplete/get-list", "script=autocomplete."+autocomplete+"&value="+v+"&f_code_wo="+ids_wo.join(',')+"&f_code_ware="+ids_ware.join(','), name+".ac.setResults");
            }
        },
        setResults: function (o) {with(eval(name).ac) {if(o) {results = JSON.parse(o);draw();} else hide();}}
    };
    this.create = function (props) {
        with(this) {
            var o_ac, n_ac, ob=this;
            if(props.ac) {o_ac = ac, n_ac = props.ac;moGlb.mergeProperties(o_ac, n_ac);}
            baseCreate(props);if(props.ac) this.ac = o_ac;
            var field = moGlb.createElement(fieldid, text ? "textarea": "input"), o = this;
            if(maxlength) field.setAttribute("maxlength", maxlength);
            if(!isReadonly() && !isDisabled() && (dtmpicker == "date" || dtmpicker == "time" || dtmpicker == "datetime")) 
                field.onclick = function (e) {
                    //AGGIUNTA DA LARA PER READONLY DA JS 17/02/2016
                    if(!ob.r_on) moDtmPkr.showDtmPicker({field: moComp.getComponent(name), title: label});return false;
                };
            if(attach) field.onclick = function (e) {
                var coords = moEvtMng.getMouseXY(e, field);
                    if(!ob.r_on && value && coords.x < 20) { // Clear fields
                        o.reset(true);
                        o.value = "";
                        moGlb.removeClass(field, 'link');
                    } else if(!ob.r_on && !value || coords.x > parseInt(field.scrollWidth)-20) {// Open attach window
                         moAttach.show({f_code: eval(eval(pname).pname).data.f_code, f_fieldname: name, callback: name+".attachCallback"});
                    } else if(value && value != 'remove'){
                        moGlb.openDoc(value);
                    } // Open document
            };
            integrityCheck();setStyle();setField();
            field.setAttribute("type", pwd ? 'password' : 'text');
            field.setAttribute("class", className);
            field.setAttribute("style", "width:"+w+"px;height:"+((text&&h<60)?60:h)+"px;display:none;");
            //if(readonly) field.setAttribute("readonly", readonly);
            if(!moGlb.isempty(autocomplete)) { // Set autocomplete
                with(ac) {
                    acId = name+"_autocomplete", w = parseInt(this.w)+parseInt(this.borderWidth*2), h = itemH;
                    var cnv = moCnvUtils.createCanvas(acId, (w+shadow)*moGlb.zoom, (h+shadow)*moGlb.zoom, "display:none;z-index:"+moGlb.zmenu+";", "autocomplete", mf$(moGlb.bd));
                    cnv.onmousemove = function (e) {
                        with(eval(name).ac) {
                            var coords = moEvtMng.getMouseXY(e, cnv), tover = (coords.y >= padding && coords.y < h*moGlb.zoom-padding) ? Math.floor((coords.y/moGlb.zoom - padding)/itemH) : -1;
                            if(over != tover) {over = tover;draw();}
                        }
                    };
                    cnv.onmouseout = function (e) {with(eval(name).ac) {over = -1;draw();
                        uscita = setTimeout(name+".ac.hide();", 1000);}};
                    cnv.onmouseenter = function (e) {with(eval(name).ac) {over = 1;draw();clearTimeout(uscita);}};
                    cnv.onmousedown = function (e) {with(eval(name).ac) {if(over >= 0) selectItem();}};
                }
            }
            
            // Events
            var o = this;            
            //for(var e in check) field[e] = function (e) {checkField(e);};
            field[dtmpicker ? "onblur" : "onchange"] = function (e) {
                if(!dtmpicker && !attach) { var input = mf$(fieldid).value; description = input, value = input; }
               
                if(checkExp.indexOf('decimal') == 0) {
                   
                    value=value.replace(moGlb.localization.f_decimal_separator,'.');
                    set(value);
                    if(value.indexOf('.') < 0){
                    var n = parseInt(checkExp.split('_')[1]);
                    v = value+'.';
                    for(var i=0; i<n; i++) v += '0';
                    set(v);
                    }
                }                
                checkField();moLibrary.exec(onchange, o);                
            };
            if(autocomplete) field.onblur = function (e) {ac.hide();};            
            field.onmouseup = function (e) {if(attach || dtmpicker == 'date' || dtmpicker == 'time' || dtmpicker == 'datetime') moEvtMng.cancelEvent(e); };
            field.onmousedown = function (e) {if(attach || dtmpicker == 'date' || dtmpicker == 'time' || dtmpicker == 'datetime') moEvtMng.cancelEvent(e); else {e.stopPropagation();checkField();}};
            field.onkeyup = function (e) {
                with(eval(name)) {
                    if(isReadonly() || isDisabled() || attach || dtmpicker == 'date' || dtmpicker == 'time' || dtmpicker == 'datetime') { moEvtMng.cancelEvent(e); return false; }
                    var input = mf$(fieldid).value;
                    if(e.keyCode == 38 || e.keyCode == 40) { // arrows
                        with(ac) {
                            if(e.keyCode == 38) over = (over == 0) ?  results.length-1: over-1; // up
                            else if(e.keyCode == 40) over = (over == results.length-1) ? 0 : over+1; // down
                            draw();
                        }                                
                    }
                    else if(e.keyCode == 13) {with(ac) {if(over >= 0) selectItem();}} // Enter
                    else if(e.keyCode == 9 || e.keyCode == 27) ac.hide(); // Tab and Esc
                    else {
                        checkField();
                        if(autocomplete) {
                            if(mf$(ac.acId).style.display == "none") {
                                var fpos = moGlb.getPosition(mf$(fieldid));
                                ac.display(fpos.l, fpos.t+fpos.h-moGlb.getPosition(mf$(moGlb.bd)).t-eval(eval(name).pname).mbody.scrollTop);
                            } else {
                                if(e.keyCode == 38 || e.keyCode == 40) {
                                    with(ac) {
                                        if(e.keyCode == 38) over = (over == 0) ?  results.length-1: over-1; // up
                                        else if(e.keyCode == 40) over = (over == results.length-1) ? 0 : over+1; // down
                                        draw();
                                    }                                
                                }
                                else if(e.keyCode == 13) {with(ac) {if(over >= 0) selectItem();}} // Enter
                                else if(e.keyCode == 9 || e.keyCode == 27) ac.hide(); // Tab and Esc
                            }
                            if(description != input) ac.getResults(input);
                        }
                    }
                    description = input, value = input;
                }
            };
            field.onkeydown = function (e) {if(isReadonly() || isDisabled() || attach || dtmpicker == 'date' || dtmpicker == 'time' || dtmpicker == 'datetime') moEvtMng.cancelEvent(e);};
            appendInfo();
        }
    };
    this.display = function (l0, t0, cid) {
        with(this) {
            if(arguments.length > 0) {
                var field = mf$(fieldid);
                with(field.style) {top = padding+"px";left = l0+"px";display = "block";}
                if(mf$(cid)) mf$(cid).appendChild(field);                
                field.style.display = "block";
            }
            if(value == '' && dvalue) set(dvalue);
            if(onload) moLibrary.exec(onload, this);
            checkField();
        }
    };
    this.reset = function () {with(this) {value = "", description = value;batch = false;setField();}};
    this.integrityCheck = function () {
        with(this) {
            if(text) {password = false;autocomplete = false;}
            if(isDisabled()) {error = false;mandatory = false;}
            else if(mandatory) { /* readonly = false; */disabled = false;visible = true;}
            else if(isReadonly() || error) {mandatory = false;disabled = false;/*visible = true;*/}
        }
    };
    this.setStyle = function () {//console.log('setStyle');
        with(this) {
            var classes = [];
            if(className) classes.push(className);
            if(attach && value) classes.push("link");
            if(error) classes.push("error");
            else {
                if(dtmpicker == "date" || dtmpicker == "time" || dtmpicker == "datetime") classes.push("datetime");
                if(attach) classes.push("attach");                
                if(isDisabled()) classes.push("disabled");
                if(isMandatory()) classes.push("mandatory");
                if(!attach && isReadonly()) classes.push("readonly");
                if(!isVisible()) classes.push("notvisible");
                //if(!attach && mf$(fieldid).value == description) classes.push("label");
            }
            mf$(fieldid).setAttribute("class", classes.join(" "));
        }
    };
    this.setField = function () {
        with(this) {
            try {
                var t_width = moCnvUtils.measureText((attach && description == undefined) ? value : description);
                //VECCHIO CODICE mf$(fieldid).value = (description ? description : value);
                //console.log(this);
                if(this.attach == 1 || this.dtmpicker == 'datetime' || this.dtmpicker == 'date' || this.dtmpicker == 'time')  mf$(fieldid).value = description;
                else mf$(fieldid).value = value;
                if(isDisabled()) mf$(fieldid).setAttribute('disabled', true); else mf$(fieldid).removeAttribute('disabled');
                if(isReadonly()) mf$(fieldid).setAttribute('readonly', true); else mf$(fieldid).removeAttribute('readonly');
                
                if(attach && description == undefined){this.description = value; document.getElementById(this.fieldid).value = value;}
                mf$(fieldid).style.width = (attach && value && t_width+20 > w ? moCnvUtils.measureText(description == undefined ? value : description)+20 : w)+"px";
                //if(attach && onchange) moLibrary.exec(onchange, this); 
            } catch(ex) {moDebug.log(ex);}
        }
    };
    this.checkField = function () {
        with(this) {
            error = !moCheck.check(mf$(fieldid).value, checkExp) && !isReadonly() && !isDisabled();
            /*integrityCheck();*/ setStyle();
        }
    };    
    //this.attachCallback = function (sid, filename) {with(this) { setProperties({description: filename, value: (sid ? sid+"|"+filename : filename)});moLibrary.exec(onchange, eval(name));}};
    this.attachCallback = function (sid, filename) {this.setProperties({description: filename, value: (sid ? sid+"|"+filename : filename)}); if(this.document == undefined){ this.description = filename; document.getElementById(this.fieldid).value = filename;}};
    this.getOldValue = function () {with(this) {return (moGlb.isset(eval(eval(pname).pname).data[bind]) ? eval(eval(pname).pname).data[bind] : '');}};
};
moTextField.prototype = new moUI();