moPicklist = function (name) {
    this.w = 175, this.h = 27, this.type = "moPicklist", this.className = "picklist", this.picktype = "", this.table = "",
    this.isparent = false, this.ftype = "", this.filters = "", this.column = "{f_title}", this.onclear = '', this.nome_utente_change_flag = false, this.oldPck = true;
    var fieldid = "", /*btnid = "", */result = {};
    this.create = function (props) {
        with(this) {
            baseCreate(props); //btnid = name+"_btn";
            var field = moGlb.createElement(fieldid, "input"), o = this;
            field.setAttribute("readonly", true);
            //field.setAttribute("class", className+" readonly");
            field.setAttribute("style", "width:"+w+"px;height:"+h+"px;display:none;");
            field.onclick = function (e) {
                var coords = moEvtMng.getMouseXY(e, field);
                if(!onclear && moGlb.isset(moLibrary[name+"_onclear"])) onclear = name+"_onclear";
                if(!isReadonly() && !isDisabled()) {
                    if(coords.x < 20 && !moGlb.isempty(value)) { // clear
                        o.reset(true);
                        try { if(onclear) moLibrary.exec(onclear, o); } catch(ex) {moDebug.log(ex);}
                    } else if(coords.x > parseInt(mf$(fieldid).scrollWidth) - 20) {
                        o.openList();
                    }
                }
            };
//            var btn = moComp.createComponent(btnid, "moButton", {
//                iconon: "FlaticonsStroke.58457",
//                action: name+".openList()"
//            }, true);
            appendInfo();setStyle();
        }
    };
    this.display = function (l0, t0, cid) {
        with(this) {
            if(arguments.length > 0) {
                var cpos = moGlb.getPosition(mf$(componentBox)), field = mf$(fieldid);
                //with(field.style) {top = padding+"px"; left = l0+"px"; width = (cpos.w-50)+"px"; display = "block";}
                with(field.style) {top = padding+"px"; left = l0+"px"; width = w+"px"; display = "block";}
                if(mf$(cid)) mf$(cid).appendChild(field);
                field.style.display = "block";
                //eval(btnid).display(l0+w+16, t0+16, cid);                
                //eval(btnid).display(cpos.w > 0 ? cpos.w-30:parseInt(w)+19, t0+13, cid);
            }
            try {if(onload) moLibrary.exec(onload, this);} catch(ex) {moDebug.log(ex);}
        }
    };    
    this.setField = function () {
        with(this) {
            try {
                //VECCHIO CODICE mf$(fieldid).value = (description ? description : value);
                mf$(fieldid).value = value;
                //eval(btnid).setProperties({disabled: readonly || disabled});                
            } catch(ex) {moDebug.log(ex);}
        }
    };
    this.setStyle = function () {
        with(this) {
            var classes = [ 'picklist' ]; if(className) classes.push(className);
            if(!isReadonly() && !isDisabled() && !moGlb.isempty(value)) classes.push("cancel");
            if(mandatory) classes.push("mandatory");
            mf$(fieldid).setAttribute("class", classes.join(" "));
        }
    };
    this.reset = function (clear) {
        with(this) 
        {
            var nm = name.split("_");
            var old_value  = value;
            set('');if(!clear) batch = false;
            eval("mdl_"+nm[1]+"_edit").addCrossData('', table+(isparent ? '_parent' : ''), ftype);
            if (table == "t_selectors") {
                eval("mdl_"+nm[1]+"_edit").removeCrossDataFromName(old_value, table+(isparent ? '_parent' : ''), ftype, "slc");
            } else {
                eval("mdl_"+nm[1]+"_edit").removeCrossDataFromName(old_value, table+(isparent ? '_parent' : ''), ftype, picktype);

            setField();
        }
    }
    };
    this.openList = function () {
        with(this) {
            var overlay = '', o = this;
            var mod = name.split('_'); mod = mod[1];
            try { overlay = eval("ov_"+picktype); } catch(ex) {                
                overlay = moComp.createComponent("ov_"+picktype, 'moOverlay', { m_mod: mod, c_mod: picktype, picklist: name });
            }
            overlay.picklist = name;
            if(moLibrary.exists(filters)) moLibrary[filters](o);
            overlay.show();                        
        }
    };
    this.setResult = function (res) {
        with(this) {//alert('aa');
            var nm = name.split("_"), clabel = column, o = this, f_code = res[0].f_code;
            eval("mdl_"+nm[1]+"_edit").data[bind+'_code'] = f_code; 
            eval("mdl_"+nm[1]+"_edit").addCrossData(f_code, table+(isparent ? '_parent' : ''), ftype);
            if(!nome_utente_change_flag) {
                eval("mdl_"+nm[1]+"_edit").addCustomData('utente_ex', f_code);
                nome_utente_change_flag = true;
            }
            var old_value  = value;
            if (old_value != "") {
                if (table == "t_selectors") {
                    eval("mdl_"+nm[1]+"_edit").removeCrossDataFromName(old_value, table+(isparent ? '_parent' : ''), ftype, "slc");
                } else {
                    eval("mdl_"+nm[1]+"_edit").removeCrossDataFromName(old_value, table+(isparent ? '_parent' : ''), ftype, picktype);
                }
            }
            result = res[0];
            for(var i in result) clabel = clabel.replace('{'+i+'}', moGlb.langTranslate(result[i]));
            set(clabel);
            if(onchange) moLibrary.exec(onchange, o);
        }
    };
    this.getResult = function () { return result; };
};
moPicklist.prototype = new moUI();