var moImage = function (name) {
    this.type = "moImage", this.onclick = "", this.noimage = moGlb.imgDefaultPath+"components/photo-1.png", this.sessionId = 0;
    this.create = function (props) {
        with(this) {
            baseCreate(props);
            var div = moGlb.createElement(fieldid, "div", "", true);
            div.setAttribute("class", "image");
            div.style.backgroundImage = "url("+noimage+")";
            var o = this;div.onclick = function (e) {openPopup(o)};
            appendInfo();
        }
    };
    this.display = function (l0, t0, cid) {
        with(this) {
            if(arguments.length > 0) {
                if(mf$(cid)) mf$(cid).appendChild(mf$(fieldid));
                mf$(fieldid).style.display = "block";
            }
            /*try {if(onload) eval(onload);} catch(ex) {moDebug.log(ex);}*/moLibrary.exec(onload, this);
        }
    };
    //this.setField = function () {with(this) mf$(fieldid).style.backgroundImage = "url("+(value ? moGlb.baseUrl+"/getDocument.php?session_id="+(value.split("|")[0])+"&size=medium&uploadType=image" : noimage)+")";};
    this.setField = function () {with(this) mf$(fieldid).style.backgroundImage = "url("+(value ? moGlb.baseUrl+"/document/get-file?session_id="+(value.split("|")[0])+"&size=medium&uploadType=image" : noimage)+")";};
    this.reset = function () {with(this) {value = "", batch = false; setField();}};
    this.openPopup = function (o) {
        with(o) {
            var win = null, wtype = (value ? "img" : "attach");
            if(value) getPopup().show({title: moGlb.langTranslate("View")});            
            //else moAttach.show({f_code: (sessionId ? sessionId : (eval(eval(pname).pname).data.f_code ? eval(eval(pname).pname).data.f_code : moGlb.user.code)), f_fieldname: name, callback: name+".attachCallback", uploadType: "image"});
            else moAttach.show({f_fieldname: name, callback: name+".attachCallback", uploadType: "image"});
        }
    };
    this.attachCallback = function (sid, filename) {
        this.setProperties({value: (sid && filename ? sid+"|"+filename : ""), description: filename});
        //if(sid) moPreload.singleLoad(moGlb.baseUrl+"/getDocument.php?session_id="+sid+"&size=thumbnail&uploadType=image", "thumbnails/"+filename);
        //if(sid) moPreload.singleLoad(moGlb.baseUrl+"/document/get-file?session_id="+sid+"&size=thumbnail&uploadType=image", "thumbnails/"+filename);
    };
    this.getPopup = function () {
        with(this) {
            var imageWin = null, fieldName = name, fcode = (sessionId ? sessionId : (eval(eval(pname).pname) ? eval(eval(pname).pname).data.f_code : moGlb.user.code));
            try {imageWin = eval("imgWin");imageWin.name;} catch (ex) {
                var div = moGlb.createElement("imgContainer", "div");
                div.setAttribute("class", "imgWindow");

                var btnEdit = moGlb.createElement("", "button", div);
                with(btnEdit) {setAttribute("class", "imgPopupBtn right"); onclick = function (e) {imageWin.hide();moAttach.show({f_code: fcode, f_fieldname: fieldName, uploadType: "image", callback: fieldName+".attachCallback"});};innerHTML = moGlb.langTranslate("Change");}
                
                var btnDelete = moGlb.createElement("", "button", div);
                with(btnDelete) {setAttribute("class", "imgPopupBtn left"); onclick = function (e) {imageWin.hide();moAttach.remove({f_code: fcode, f_fieldname: fieldName, uploadType: "image", callback: fieldName+".attachCallback"});};innerHTML = moGlb.langTranslate("Remove");}
                
                imageWin = moComp.createComponent("imgWin", "moPopup", {width: 600, height: 480, contentid: div.id }, true);
            }
//            var icon = value.split(".");
            //mf$("imgContainer").style.backgroundImage = "url("+moGlb.baseUrl+"/getDocument.php?session_id="+(value.split("|")[0])+"&size=high&uploadType=image)";
            mf$("imgContainer").style.backgroundImage = "url("+moGlb.baseUrl+"/document/get-file?session_id="+(value.split("|")[0])+"&size=high&uploadType=image)";
            return imageWin;
        }
    };
};
moImage.prototype = new moUI();