/*
 *  Types of search for every field type
 *  "string": [ [ 0, "contain" ], [ 1, "not contain" ], [ 2, "start with" ], [ 3, "end with" ], [ 4, "are equal to" ], [ 5, "are not equal to" ], [ 6, "empty" ], [ 7, "not empty" ] ],
 *  "number": [ [ 0, "are equal to" ], [ 1, "are not equal to" ], [ 2, "are greater than" ], [ 3, "are less than" ], [ 4, "empty" ], [ 5, "not empty" ] ],
 *  "date": [ [ 0, "are before" ], [ 1, "are after" ], [ 2, "are equal to" ], [ 3, "are not equal to" ], [ 4, "between" ], [ 5, "empty" ], [ 6, "not empty" ] ]
 */
var moSearchWindow = {
    tg: "", field: "", add: "",
    show: function (tg, field, wtype, add) {
        moSearchWindow.tg = tg, moSearchWindow.field = field, moSearchWindow.add = add;
        with(moSearchWindow) {            
            var stype = typeToString(wtype), win = "";
            try {win = eval("searchWindow_"+stype+"_win");win.name;
            } catch(ex) {
                var div = mf$("searchWindow_"+stype);if(!div) div = create(wtype);
                win = moComp.createComponent("searchWindow_"+stype+"_win", "moPopup", {
                    //width: 250, height: 150,
                    width: 218, height: (stype == 'date' ? 200 : 150),
                    zindex: 1100, contentid: "searchWindow_"+stype, title: moGlb.langTranslate("Perform your search")}, true);
            }
            win.show();
            mf$("searchValue_"+stype).focus();
        }
    },
    hide: function (stype) {
        eval("searchWindow_"+stype+"_win").hide();mf$("searchForm_"+stype).reset();
        //mf$("searchWindow_"+stype).style.display = "none";
    },
    find: function (wtype) {
        with(moSearchWindow) {
            var stype = typeToString(wtype), win = "", sensitivity = false;
            var condition = mf$("searchCondition_"+stype).options[mf$("searchCondition_"+stype).selectedIndex].value;
            var val = mf$("searchValue_"+stype+(stype=="date"?"_hidden":"")).value, value = '';
            if(wtype == 3) {
                var d = new Date(val*1000), yyyy = d.getFullYear(), mm = d.getMonth(), dd = d.getDate();
                val = new Date(yyyy, mm, dd, 0, 0, 0, 0).format('{U}');
            }
            value = Base64.encode(val);            
            if(wtype == 0) {sensitivity = mf$("searchSensitivity").checked === true;}            
            else if(wtype == 3 && condition == 4) {var valueDateLimit = Base64.encode(mf$("searchValueDateLimit_hidden").value);   }
            //if(!value) moGlb.alert.show("Set a search parameter");
            //else {
                eval(tg).TrovaSearch({condition: condition, main: value, added: valueDateLimit, type: wtype, sensitivity: sensitivity, fieldIndex: moSearchWindow.field, add: add});
                hide(stype);
            //}
        }
    },
    typeToString: function (ntype) {
        switch(ntype) {
            case 1: return "number";
            case 3: return "date";
            default: return "string";
        }
    },    
    create: function (ntype) {
        with(moSearchWindow) {
            var stype = typeToString(ntype), selectOptions = {
                "string": [ [ 0, "contain" ], [ 1, "not contain" ], [ 2, "start with" ], [ 3, "end with" ], [ 4, "are equal to" ], [ 5, "are not equal to" ], [ 6, "empty" ], [ 7, "not empty" ] ],
                "number": [ [ 0, "are equal to" ], [ 1, "are not equal to" ], [ 2, "are greater than" ], [ 3, "are less than" ], [ 4, "empty" ], [ 5, "not empty" ] ],
                "date": [ [ 0, "are before" ], [ 1, "are after" ], [ 2, "are equal to" ], [ 3, "are not equal to" ], [ 4, "between" ], [ 5, "empty" ], [ 6, "not empty" ] ]
            }, label = stype == "string"?"text":(stype=="date"?"date":"value");
            for(var i in selectOptions) for(var j in selectOptions[i][j]) {selectOptions[i][j] = moGlb.langTranslate(selectOptions[i][j]); }
            if(mf$("searchWindow_"+stype)) return;
            
            var div = moGlb.createElement("searchWindow_"+stype, "div", "", true), form = moGlb.createElement("searchForm_"+stype, "form", div);
            div.setAttribute("class", "searchWindow "+stype);form.onsubmit = function (e) {return false;}
            form.innerHTML = "<div>"+moGlb.langTranslate("Find")+" "+moGlb.langTranslate(label)+moGlb.langTranslate("s that")+"</div>";
            form.innerHTML += "<select id='searchCondition_"+stype+"'"+(stype == "date" ? "onChange='mf$(\"searchValueDateLimitContainer\").style.display = (this.selectedIndex == 4 ? \"none\" : \"block\")'":"")+"></select>";
            var selectField = mf$("searchCondition_"+stype);
            for(var i=0; i<selectOptions[stype].length; i++) { // Add options
                var opt = new Option(moGlb.langTranslate(selectOptions[stype][i][1]), selectOptions[stype][i][0], i==0);
                selectField.options.add(opt, i);
            }
            with(selectField) {setAttribute("class", "default");style.width = "200px";}
            
            form.innerHTML += "<div>"+moGlb.langTranslate("this ")+moGlb.langTranslate(label)+"</div>";
            
            var value = moGlb.createElement("searchValue_"+stype, "input", form, true);
            with(value) {
                setAttribute("type", "text");setAttribute("class", "default");
                if(stype == "date") {
                    setAttribute("class", "default date");
                    setAttribute("onclick", "moDtmPkr.showDtmPicker({ifield: 'searchValue_"+stype+"', title: moGlb.langTranslate('Select a date'), datePicker: true}); return false;");
                }
            }            
            
            if(stype == "date") moGlb.createElement("searchValue_"+stype+"_hidden", "input", form, true).setAttribute("type", "hidden");

            if(stype == "string") {
                moGlb.createElement("searchSensitivity", "input", form, true).setAttribute("type", "checkbox");
                form.innerHTML += "<span>"+moGlb.langTranslate("case sensitive")+"</span>";
            } else if(stype == "date") {
                moGlb.createElement("searchValueDateLimitContainer", "span", form).setAttribute("class", "dateMask");
                form.innerHTML += "<div>"+moGlb.langTranslate("and this date")+"</div>";
                
                var limit = moGlb.createElement("searchValueDateLimit", "input", form, true);
                with(limit) {setAttribute("type", "text");setAttribute("readonly", "true");setAttribute("class", "default date");onclick = function (e) {
                    moDtmPkr.showDtmPicker({ifield: "searchValueDateLimit", title: moGlb.langTranslate("Select a date"), datePicker: true});return false;
                };}
                
                moGlb.createElement("searchValueDateLimit_hidden", "input", form, true).setAttribute("type", "hidden");
            }

            var button = moGlb.createElement("", "button", form);
            with(button) {if(stype == "date") setAttribute("class", "date");onclick = function (e) {moSearchWindow.find(ntype);};innerHTML = moGlb.langTranslate("Search");}

            return div;
        }
    }
};