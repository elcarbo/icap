function moTreeGrid_light(nm,z) {


// props
this.name=nm;

this.create=function (z) {
var props={ Vscr:null, Oscr:null, bookmark_name:'', bookmark_id:0, moduleName: "",
            Ttable:"t_ware", crud:"", ftype:1, J:null, jj:null, W:0, H:0, zL:0, zT:0,
            nodrw:0, sescr:0, sedrw:3, SEDRW:3, Stp:50, addStp:0,
            NOEVT:0, noAnim:0, 
            mgL:0, mgT:0, mgR:0, mgB:0, VW:18, padf:4, iext:0, RX1:0, szW:32,                                                       // margini
            HeadH:24, cnvLS:56, mecn:14, Hlw:0, Huw:0, NL:0, NU:0, NT:0, OH:0, SH:0, 
            msgH:0, adds:0, upds:0,                                                                                                 // cnvLS w canvas locked e selected | head lock w | head unlock w
            CNV:null, CNJ:null, BDN:null, oBDN:null, BDJ:null, MBDJ:null, OBDJ:null, HBDJ:null, maskj:null, ccMm:0,
            IMD:null, OMD:null, FMD:null, TGJ:null,            
            optr:0, vptr:0, OVR:-1, SEL:-1, bOVR:-1,
            Hy:0, asHy:0, displ:1, lkEdit:0, unlkEdit:0, olEdit:0,                                              
            EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove', oldmouseup:"", NMM:0, Cur:0, 
            dwnBD:0, noWH:0, seBlk:0, blkStp:600, aFavorite:{},
            Hnr:0, PXi:0, PYi:0, Root:"", checkedIds:[],
            Max:400, MaxOver:550, Nrig:1, SORT:[], FILTER:[], SEARCH:[], AND:[], SELECTOR:[],
            lastrefresh:0  }
            
for(var i in props) this[i]=(typeof z[i]=='undefined')?props[i]:z[i];
}
this.Yi=32;            // altezza riga
this.Yt=0;             // posizione top Y nel canvas body alto SH  
this.AR=[];
this.mCK=[];
this.JStore=[];
this.multifcode=[];
this.Header=[]; 
this.Abody=[];
this.ROWBKCOL=["#FFFFFF","#FAFAFA","#FEFF9E"];
this.ROWBDCOL="#EDEDED";
this.ROWCOLTXT=moColors.text;


this.arrCHK=[];         // locked by checkbox
this.aChecked=[];       // Atomic checked!!
this.arrLK=[];          // locked da altri user
this.arrNd=[];          // array degli fcode apri/chiudi  (se asHy)       // { l,t, Ypx }

this.Ypos=[];
this.rifY=[];
this.aRange=[];
this.aPRange=[];
this.aFilter=[];

this.IM=moGlb.imgDefaultPath+"/";
this.IMpth="treegrid/";
this.img=[]; 
this.imgsrc=["check0.png","check1.png","dropdown15x13.gif","dropdown_menu_column.png", "open0.png", "open1.png", "folder0.png","folder1.png","leaf.png",              // 0-8
             "lock1.png","lock2.png","lock3.png"                                                                                                                      // 9
            ];
this.Curs=["default","pointer","col-resize"];

this.menulist=[];
this.Fmenu=[ {Txt:moGlb.langTranslate("Sort ascending"),  Type:3, Status:0, Group:1, Icon:['order_az_off.png','order_az.png'], Fnc:"" },
             {Txt:moGlb.langTranslate("Sort descending"), Type:3, Status:0, Group:1, Icon:['order_za_off.png','order_za.png'], Fnc:"" }, 
             {Type:4 }, 
             {Txt:moGlb.langTranslate("Add filter"), Disable:0, Type:1, Sub:[
                                            {Txt:moGlb.langTranslate("Add Sort"), Type:1, Sub:[
                                                             {Txt:moGlb.langTranslate("Sort ascending"), Type:3, Status:0, Group:2, Icon:['order_az_off.png','order_az.png'], Fnc:"" },
                                                             {Txt:moGlb.langTranslate("Sort descending"), Type:3, Status:0, Group:2, Icon:['order_za_off.png','order_za.png'], Fnc:"" } 
                                                                                   ]},
                                            {Txt:moGlb.langTranslate("Add search"), Type:0, Icon:['search.png'], Fnc:"" },
                                            {Txt:moGlb.langTranslate("Add filter by"), Disable:1, Type:1, Sub:[]} 
                                                      ]}, 
             {Txt:moGlb.langTranslate("Clear all filter"), Disable:0, Type:0, Fnc:this.name+".actclearfilter()"},
             {Type:4 }, 
             {Txt:moGlb.langTranslate("Search"), Type:0, Icon:['search.png'], Fnc:"" },
             {Txt:moGlb.langTranslate("Filter by"), Disable:1, Type:1, Sub:[]},
             {Txt:moGlb.langTranslate("Lock / unlock"), Type:2, Status:1, Icon:['lock.png','unlock.png'], Fnc:"" },               // 08
     //      {Txt:"Group by this field", Type:2, Status:0, Icon:['check0.png','check1.png'], Fnc:this.name+".ActGroup()" }, 

             {Txt:moGlb.langTranslate("Hierarchy"), Type:2, Status:0, Icon:['','checked'], Fnc:this.name+".acthierarchy()" },                          // 09
             {Txt:moGlb.langTranslate("Hidden"), Type:0, Fnc:"" }       // this.name+".acthidden(colonna)                                              // 10
           ];

this.FilerData=[
{Txt:moGlb.langTranslate("Today")},
{Txt:moGlb.langTranslate("Yesterday") },
{Txt:moGlb.langTranslate("Tomorrow") },
{Txt:moGlb.langTranslate("Current week") },
{Txt:moGlb.langTranslate("Last week") },
{Txt:moGlb.langTranslate("Next week") },
{Txt:moGlb.langTranslate("Current month") },
{Txt:moGlb.langTranslate("Last month") },
{Txt:moGlb.langTranslate("Next month") },
{Txt:moGlb.langTranslate("Current year") },
{Txt:moGlb.langTranslate("Last year") }
];
 
this.Filterby=[];

this.menuChecked=[
{Txt:moGlb.langTranslate("Add viewed"), Type:0, Fnc:this.name+".actmenuchek(0)"},
{Txt:moGlb.langTranslate("Sub viewed"), Type:0, Fnc:this.name+".actmenuchek(1)"},
{Txt:moGlb.langTranslate("Uncheck all"), Type:0, Fnc:this.name+".deCheck()"}
];


this.serchSelect=[
      [moGlb.langTranslate("Contains"),moGlb.langTranslate("It does not contain"),moGlb.langTranslate("Equal"),moGlb.langTranslate("Different")],                               // 0 = testo
      [moGlb.langTranslate("Equal"),moGlb.langTranslate("Different"),moGlb.langTranslate("Greater than"),moGlb.langTranslate("Less than"),moGlb.langTranslate("Expression")],                // 1 = numerico
      [moGlb.langTranslate("True"),moGlb.langTranslate("False")],                                                           // 2=booleano
      [moGlb.langTranslate("Equal"),moGlb.langTranslate("Different"),moGlb.langTranslate("Greater than"),moGlb.langTranslate("Less than"),moGlb.langTranslate("Period")]                   // 3=data
];

// metodi
this.getHeader=getHeader;
this.setHeader=setHeader;
this.Display=Display;                                          // IN
this.resize=resize;
this.lockEdit=lockEdit;
this.closeEdit=closeEdit;
this.checkDetect=checkDetect;

this.predraw=predraw;
this.draw0=draw0;
this.draw1=draw1;
this.draw=draw;
this.draw2=draw2;
this.ClearB=ClearB;
this.fMaskLoad=fMaskLoad;
this.animdraw=animdraw;
this.tmanimdrw=tmanimdrw;

this.Cell=Cell;
this.hCell=hCell;

this.RowCells=RowCells;
this.RCells=RCells;
this.clipRow=clipRow;

this.loadroot=loadroot;
this.retroot=retroot;
this.jaxULock=jaxULock;
 
 

this.SeChkDechk=SeChkDechk;
this.deCheck=deCheck;
 

this.Vbarpos=Vbarpos;
this.Obarpos=Obarpos;  
this.scrollO=scrollO;
this.SeSort=SeSort;
this.SeFilterby=SeFilterby;
this.SeSearch=SeSearch;

this.headown=headown;  
this.headmove=headmove;
this.headout=headout;

this.oversel=oversel;
this.overmove=overmove;
this.overMove=overMove;

this.bodymove=bodymove;
this.bodyout=bodyout;
this.bodydown=bodydown;
this.bodyup=bodyup;
this.drawbodyover=drawbodyover;

this.maskmove=maskmove;
this.maskup=maskup;
this.headRitAnimat=headRitAnimat;
this.endAnim=endAnim;

this.fieldlist=fieldlist;
this.viewmenucheck=viewmenucheck;
this.createmenu=createmenu;
this.acthidden=acthidden;
this.acthierarchy=acthierarchy;
this.actlock=actlock;
this.actsort=actsort;
this.actaddsort=actaddsort;
this.actfilterby=actfilterby;

this.actsearch=actsearch;
this.closefSearch=closefSearch;
this.TrovaSearch=TrovaSearch;
this.changeidSel0=changeidSel0;

this.actclearfilter=actclearfilter;
this.actmenuchek=actmenuchek;
this.viewchecked=viewchecked;
this.viewall=viewall;
this.actselector=actselector;


this.ActRefresh=ActRefresh;
this.timeRefresh=timeRefresh;
this.fnoAct=fnoAct;
this.ResetYLS=ResetYLS;

this.updHeader=updHeader;
this.fmenuList=fmenuList;

this.refresh = refresh;
this.getSelectedRows = getSelectedRows;



this.colrs=["#F0F0F0","#A1C24D","#4A6671","#6F8D97","#E2EECA","#E0E0E0","#A0A0A0","#DDDDDD","rgba(161,194,77,0.3)"];

// IN --------------------------------------------------------------------------


function Display(i){
with(this){  if(i==displ) return;
if(i) { var nb="block"; displ=1; }
else { var nb="none"; displ=0; }
j.style.display=nb;
}}


function resize(l0,t0,w0,h0,i){   //  moGlb.layoutPrefixId = "layout_"+i attivo
  with(this){ 
                      
  if(typeof jj == "undefined" || !jj) { J=mf$(i);
           jj=moGlb.createElement("id"+name,"div",J);
           jj.style.overflow="hidden";   
  
    loadroot(); 
    draw0();     
                
      // scrollbars   
      eval("Vscr"+name+"=new OVscroll('Vscr"+name+"',{ displ:0, L:-100, T:0, H:h0, HH:1, Fnc:'"+name+".Vbarpos', pid:'"+jj.id+"' }); VW=Vscr"+name+".W;"); 
      eval("Oscr"+name+"=new OVscroll('Oscr"+name+"',{ displ:0, OV:1, L:0, T:0, H:w0, HH:1, Fnc:'"+name+".Obarpos', pid:'"+jj.id+"' });");   
   } 
         
    jj.style.left=l0+"px";
    jj.style.top=t0+"px";
    jj.style.width=w0+"px";
    jj.style.height=h0+"px";
    
    W=w0, H=h0;
    
    var p=moGlb.getPosition(jj);   
    zL=p.l, zT=p.t;
    
    if(Hlw) predraw();  
    addStp=0;   
}}
 


this.unlockElements = function () {};

function getSelectedRows() { return this.arrCHK; }


function lockEdit(v) {
with(this){
  if(v) unlkEdit=1; else unlkEdit=0;
}}




function closeEdit(){
with(this){ 
jaxULock([lkEdit],0,0);
 for(var r=0;r<arrCHK.length;r++) if(arrCHK[r]==lkEdit) { arrCHK.splice(r,1); break; } 
unlkEdit=0;
lkEdit=0;
olEdit=0;
}}




function checkDetect(){ return this.aChecked; }
 
 
 
 
//--------------------------------------------------------------------------------------------------------------------------------------- 
// LOAD record


function loadroot(){ 
  with(this){    
    var ja=JSON.stringify({ Level:0, Tab:[Ttable, ftype], module_name: crud });
    Ajax.sendAjaxPost( moGlb.baseUrl+"/treegrid/jax-read-root", "param="+ja , this.name+".retroot" );
}} //




// tipo di root e personal setting field
function retroot(a){
  with(this){  

  var ar=JSON.parse(a), ln=ar.n0;
  if(moGlb.isset(ar.message)) moGlb.alert.show(moGlb.langTranslate(ar.message));
  if(!Hy) asHy=0;
 
  Fmenu[9].Status=asHy;   // menu [9]= Hierarchy
  if(!Hy) Fmenu[9].Disable=1;

  moMenu.IM=IM;  
  setHeader(ar.bookmark, ar.bookmark_id, ar.bookmark_name); 

  LoaData(-1, 0, 0, 0, "root");    // carico primi record          
}} //




function setHeader(ab, bi, bn){   // bookmark,  bookmark_name,  bookmark_id
with(this){

   if(!bn) bn="";
   if(!bi) bi="";
   bookmark_name=bn, bookmark_id=bi; 

   NT=0; Header=[]; AR=[];  Huw=0;

  // carico i fields e costruisco i menu per head 
  // calcolo Header
  var prm=["fieldb","label","type","lock","hidd","wdth","filter","range","prange","script","x1","x2"], NL=0, NU=0, sw=0, r, fb, c, fz, ki, fld;  
  aRange=[], aPRange=[], aFilter=[];                                                                                        
                                                                                                             
  for(r=0;r<ab.length;r++) { Header[r]={}; 

    fz=ab[r][0]; AR[fz]=[]; // salvo in AR   filter  range  prange  (del campo fieldb)
    for(ki=6;ki<9;ki++) AR[fz][prm[ki]]=ab[r][ki];     // es. AR[fieldb][filter]=array bookmark
  
    for(var i=0;i<prm.length;i++) Header[r][prm[i]]=(i<6)?ab[r][i]:"";                           // trasformo in associativo      se<6 

    // Range
    var ar7=ab[r][7].length;                                                                                                                        
    if(ar7) { fz=ab[r][0]; aRange[fz]=[]; 
              for(var n=0;n<ar7;n+=2) aRange[fz].push( {exp:ab[r][7][n], val:ab[r][7][n+1]} );
              }
  
    // aPRange
    var ar8=ab[r][8].length;                                                                                                                        
    if(ar8) { aPRange.push( { field:ab[r][0], condz:[], typ:ab[r][2] } );  c=aPRange.length-1;
              for(var n=0;n<ar8;n+=2) aPRange[c].condz.push( {exp:ab[r][8][n], val:ab[r][8][n+1]} );
              }
    
    // script
    ar9=ab[r][9]; 
    Header[r][prm[9]]=(ar9)?ab[r][9]:"";
       
    // select
    if(Header[r].type==4) { fb=Header[r].fieldb; Filterby[fb]=[];                                    // array('0'=>'Nessuna','1'=>'Normale','2'=>'Urgente','3'=>'Immediata')   
                aFilter[fb]=[];         
                for(k in ab[r][6]) {
                    aFilter[fb][k]=ab[r][6][k];
                   Filterby[fb].push({Txt:ab[r][6][k], Type:3, Value:k, Status:0, Group:3, Icon:['','checked'], Fnc:"" });                      
                }
 
                        }
    
    with(Header[r]) { NT++; if(hidd) { x1=sw; sw+=wdth; x2=sw;                                   // larghezza header lock e unlock
                               if(lock) NL++, Hlw=sw; else Huw+=wdth, NU++; } }               
  }   
   
  fmenuList();
  predraw();
}}



function getHeader(){
with(this){  
  var abk=[], r, a, k;
  for(r=0;r<Header.length;r++){ 
     a=Header[r];
     abk.push( [ a.fieldb,
                 a.label,
                 a.type,
                 a.lock,
                 a.hidd,
                 a.wdth,
                 AR[a.fieldb]["filter"],
                 AR[a.fieldb]["range"],
                 AR[a.fieldb]["prange"],
                 a.script
                ]);
  }

return [abk, bookmark_id, bookmark_name];
}}
 


// DRAW -------------------------------------------------------------------------------------------------------------------------- 


function predraw(){
with(this){  


 // resize srollb
 var sl=W-VW-mgR, st=mgT+HeadH+msgH, 
     ol=mgL+cnvLS+Hlw, ot=H-VW-mgB; 
    
     OH=W-cnvLS-Hlw-mgL-mgR; 
     SH=H-HeadH-VW-mgB-mgT-msgH;
  
  eval("Vscr=Vscr"+name);
  eval("Oscr=Oscr"+name); 
    
  if(SH<64 || W<80) Vscr.Display(0); else { if(!Vscr.displ) Vscr.Display(1); Vscr.Resize( { L:sl, T:st, H:SH, HH:Hnr*Yi } ); }
  if(OH<64) Oscr.Display(0); else { if(!Oscr.displ) Oscr.Display(1); Oscr.Resize( { L:ol, T:ot, H:OH, HH:Huw } ); }

 // resize infogrid 
 mf$("infogrid"+name).style.width=ol+"px"; 
 draw1();
}}                                                                                                                   


function draw0(){
with(this){ 

  if(isTouch) EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';  

  var hbody=H-HeadH-VW-mgT-mgB-msgH,onm="",z,s="",zz,c2,tc,lc;
    
  z="z-index:", zz=0; lc=20+mgL, tc=(4+mgT)-3; 

  if(isTouch) {  
     onm=EVTDOWN+"='"+name+".bodydown(event)' "+EVTMOVE+"='"+name+".bodymove(event)' "+EVTUP+"='"+name+".bodyup(event)' ";
  
  } else {     
    onm=" onmouseover='WHEELOBJ=\"Vscr"+name+"\"' onmouseout='"+name+".bodyout(event)' "+EVTMOVE+"='"+name+".overMove(event)' "+EVTDOWN+"='"+name+".bodydown(event)' ";
   
    TGJ=moGlb.createElement("maskTreegridDragScroll","div",document.body);
    TGJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(moGlb.zloader+500));      
  }    
   
   // left head
  s="<canvas id='lcksel"+name+"' style='position:absolute;left:"+mgL+"px;top:"+mgT+"px;"+z+zz+";' width='"+cnvLS+"' height='"+HeadH+"'></canvas>"+
  
    "<div id='checklcksel"+name+"' style='position:absolute;left:"+lc+"px;width:34px;top:"+tc+"px;"+
    z+(zz+1)+";cursor:pointer;color:"+colrs[5]+";font:16px FlaticonsStroke;text-align:center;'"+
    " onclick='"+name+".viewmenucheck()'>"+String.fromCharCode(58826)+"</div>"+
 
    // head      
    "<canvas id='headlu"+name+"' style='position:absolute;left:"+(mgL+cnvLS)+"px;top:"+mgT+"px' width='"+Hlw+"' height='"+HeadH+"' ></canvas>"+       
    "<div id='idmsgrev"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px;width:10px;height:"+msgH+";overflow:hidden;"+
    "background-color:#f0f0f0;cursor:pointer;'></div>"+
           
    "<canvas id='bodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px' width='10' height='"+hbody+"'></canvas>"+ 
    "<div id='overbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px;width:10px;height:"+hbody+";overflow:hidden;'></div>"+
              
    "<div id='maskbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px;width:10px;height:"+hbody+";overflow:hidden;background-color:#ff0;opacity:0.0;' "+onm+"></div>"+
 
     // header
    "<div style='position:absolute;left:"+mgR+"px;top:"+mgT+"px;width:"+VW+"px;height:"+HeadH+"px;cursor:pointer;' onclick='"+name+".fieldlist(event)'>"+
    "<div style='margin:2px 0 0 2px;'>"+moCnvUtils.IconFont(58445,16,colrs[2])+"</div>"+
    "</div>"+

    // footer info
    "<div id='infogrid"+name+"' style='position:absolute;left:"+mgL+"px;bottom:"+mgB+"px;height:"+VW+"px;width:"+cnvLS+"px;overflow:hidden;'>"+
    "<div id='infotxt"+name+"' style='font:13px opensansR;margin:2px 4px 0 8px;color:#000;'>No record found.</div></div";
           
  jj.innerHTML=s;  
  CNJ=mf$("headlu"+name), CNV=CNJ.getContext("2d");
  BDJ=mf$("bodylu"+name), BDN=BDJ.getContext("2d");
  MBDJ=mf$("maskbodylu"+name);
  OBDJ=mf$("overbodylu"+name);
  HBDJ=mf$("idmsgrev"+name);
 
  eval("CNJ."+EVTDOWN+"=function(e){"+name+".headown(e); }");
  if(!isTouch){
    eval("CNJ."+EVTMOVE+"=function(e){"+name+".headmove(e); }");
    eval("CNJ.onmouseout=function(e){"+name+".headout(e); }"); 
    
  } else {
    eval("CNJ."+EVTMOVE+"=function(e){"+name+".maskmove(e); }");
    eval("CNJ."+EVTUP+"=function(e){"+name+".maskup(e); }");   
  } 
      
}}



// caching per il ridisegno dei canvas head e body
function draw(tt){    
  with(this){  SEDRW!=sedrw;      
       if(!tt) { if(!nodrw) { tt=1; nodrw=1; }
                 else return; } 
   if(tt>1) { if(SEDRW&1) draw1(); if(SEDRW&2) draw2(); nodrw=0; sedrw=3; return; }
   tt++;  
setTimeout(name+".draw("+tt+")",(Stp+addStp));
}}





function draw1(){
  with(this){           

  if(W<=moGlb.moduleMinWH) { Display(0);  return; } // dimensione minima
  Display(1);
 
  // cella locked-selected
  var cnv=mf$("lcksel"+name).getContext("2d"), c2=(cnvLS-mecn)/2;
  Cell(cnv,0,c2,0);
  Cell(cnv,c2,cnvLS,0); 

  var nk=arrCHK.length; 
  ck=(nk<2 && (lkEdit || !nk))?0:1; 
  mf$("checklcksel"+name).style.color=colrs[ck?2:5]; 

 
  // celle lock & unlock 
  CNJ.width=Hlw+OH;      
  for(var r=0;r<NT;r++) { if(Header[r].hidd) hCell(r);   }
}}







function draw2(no){
  with(this){    if(!no) no=0;   
 
  // bodywheel 
  if(!no){ var uw=cnvLS+Hlw+OH;
  with(BDJ) width=uw, height=SH;
  MBDJ.style.width=uw+"px"; MBDJ.style.height=SH+"px";
  OBDJ.style.width=uw+"px"; OBDJ.style.height=SH+"px";
  HBDJ.style.width=W+"px"; 
  }
  
  var nar=Ypos.length;
  
  
  Yt=vptr%Yi, Yt*=-1;                                           // Yt delta posizione iniziale
  var n0=(SH-Yt); nr=parseInt(n0/Yi), nr+=(n0%Yi)?1:0;          // nr = numero righe visualizzabili nel canvas top = Yt

  Nrig=nr; 
  
  //Ypos 
  var nn=0, p0,p1, Yx=1, y=vptr+Yt;   if(nar>100) { if(Ypos[nar>>1].posY<y) nn=nar>>1;  }     // ottimizzo lista per ricerca Ypos[ Yx ].posY
  if(!nar || (nar==1 && Ypos[0].posY<0)) { ClearB(1); return; }
 
 
  
                                         
  // cerco inizio visualizzazione  Yx
  for(var r=nn;r<(nar-1);r++) { 
    p0=Ypos[r].posY, p1=Ypos[r+1].posY;
    
//    moGlb.alert.show(p0+" | "+p1+" - "+(nar-1))
    
    Yx=r;
    if(vptr>=p0 && vptr<p1) break; 
    Yx=nar-1;
  }
 
  

   
  arrNd=[], Abody=[]; 
  var Y=Yt, x=Yx, or=0, cy=y/Yi, c, nbk=0, pc,ps,cr,bx,p1=0, fx=x;                              
  ClearB(0);
 

 /*
nr  numero righe
y   testa visualizzazione (vptr+(-Yt))   per Ypos
Y   Yt pos del canvas   

x   elemento partenza in Ypos ed incrementato se visualizzato
fx  ultimo elemento visualizzato
p0
nar Ypos.length
 */
 
// disegno nr righe a partire da Yt
  for(r=0;r<nr;r++){
 
     if(typeof(Ypos[x])=="undefined") break;
 
      p0=Ypos[x].posY;         // pc padre ultimo elemento (i blank sono fratelli dell'ultimo Ypos)
      c=cy&1;
 
      // verifico se esiste o blank 
      if(p0>=y && p0<(y+Yi))  
          RowCells(Ypos[x],Y,c);                                  // esiste
          x++;                                                   // Ypos successivo
 
  y+=Yi; Y+=Yi; cy++;
  } // fine for
  
  ClearB(1);                                
}}



function RowCells(Ypx,y,c){                    // Ypx = Ypos { }
  with(this)  {
  
  var bdw=cnvLS+Hlw+OH, k, bkg,brd,g;    // falsariga 
  // sfondo riga 
  with(BDN) {
  
  k=1; 
  bkg=ROWBKCOL[c];   
  fillStyle=bkg;
  fillRect(0,y+k,bdw,Yi-2*k);
  fillStyle=ROWBDCOL;
  fillRect(0,y+Yi-1,bdw,1);
  
  // evidenzia
  for(r=0;r<NT;r++) with(Header[r]) if(hidd && r==SEL) { save(); g=clipRow(x1,x2,y,lock);
  if(!g) break;
  if(r==SEL) { fillStyle=fillStyle=colrs[8];                                                          //  #@#     moColors.bgOver;
   fillRect(g[0],y,g[1]-g[0],Yi); }                            
  restore(); break;}
                                                                                                                 
  // se blank
  if(typeof(Ypx)=="number" || !Ypx) return;
  
  // Rcelle lock & unlock 
  var fc=Ypx.fcode, rtr=Ypx.rtree, r;
  
  if(aFavorite[fc]) { fillStyle=aFavorite[fc];                                                      //---    se evidenziato aFavorite[fcode]=Colore    da crud
                      clearRect(0,y+k,bdw,Yi-2*k);
                      fillRect(0,y+k,bdw,Yi-2*k); }                                                                                    
  } 

   
  // chech & lock da LStore
  var afc=JStore[fc];  if(!afc) { moGlb.alert.show(moGlb.langTranslate("no data record found!")+" "+fc); return; }
  
  var ld=4, dd=29, vv=y+parseInt((Yi-16)/2), slt=0,flti, lck=afc.selected;
  
  if(lck>0) { if(lck>1) slt=1;  // lucchetto
    flti=58404;
    moCnvUtils.cnvIconFont(BDN,flti,ld,vv,16,colrs[1]);   
  }        
              
  flti=slt?58826:58783;
  moCnvUtils.cnvIconFont(BDN,flti,dd,vv,16,colrs[2]);                           // checkbox
                                      
 
  Abody.push({y:y, f:fc, rtree:rtr}); 
  
  // colore testo riga in aPRange
  var f,v,nv,i,cz,vz,tz,virg, colr=ROWCOLTXT, stz;
  for(var r=0;r<aPRange.length;r++) {  f=aPRange[r].field; nv=aPRange[r].condz.length, tz=aPRange[r].typ;
                                v=afc[f]; virg=(!tz)?"'":"";  stz = { "{val}": virg+v+virg };   
                                for(i=0;i<nv;i++) {
                                cz=aPRange[r].condz[i].exp, vz=aPRange[r].condz[i].val;
                                
                                cz=cz.multiReplace(stz); 
                                eval("if("+cz+") colr='"+vz+"';");
                                if(colr!=ROWCOLTXT) break; }
                             } 
    
  for(r=0;r<NT;r++) if(Header[r].hidd) RCells(afc, r, y, Ypx, colr);                      // campi riga
  
}}





 

function RCells(rig, r, y, Ypx, colr){       //  progre LStore,  r per Header[r]  fieldb, x1,x2, ...
  with(this)  {

  var X1,X2,x21,c1,c2, lr=OH+Hlw, t="", f="13px opensansR", d=0, nh, v;
  

  with(Header[r]) with(BDN){
      
  X1=x1, X2=x2;
  if(!lock) {  X1-=optr, X2-=optr; if(X2<=Hlw || X1>=lr) return; }

 save();
  x21=X2-X1-padf*2, X1+=cnvLS+padf;
  t=rig[fieldb], v=t;
  
  // clip  
   clipRow(x1,x2,y,lock);
  
  if(type!=5){                                                                                           
    font=f; textBaseline="alphabetic"; textAlign="left";     
    
    var d=0, yy; 
    if(fieldb=="f_title" && asHy){                                                      // se tree  (f_title)
                d=Ypx.levl*16; nh=Ypx.nch; 
                yy=y+(Yi-16)/2;   
       if(nh) {   
       
                var flti=58364, flcol=colrs[2], flic=6+Ypx.open;
                if(flic==7) flti=58369; 
                moCnvUtils.cnvIconFont(BDN,flti,X1+d+19,yy-1,16,flcol);
                                                                                      // img 4open  6folder     
               // drawImage(moGlb.img[ IMpth+imgsrc[6+Ypx.open] ],X1+d+16-3,yy);
               
                if(nh!=-1) {
                
                var flti=58825, flic=4+Ypx.open;
                if(flic==4) flti=58824;
                moCnvUtils.cnvIconFont(BDN,flti,X1+d,yy+1,14,colrs[6]);
                
                //drawImage(moGlb.img[ IMpth+imgsrc[4+Ypx.open] ],X1+d-3,yy); 
                arrNd.push({ l:X1+d-5, t:yy, oby:Ypx }); 
                }
                
                
       } else {                 
                moCnvUtils.cnvIconFont(BDN,58355,X1+d+19,yy,16,colrs[2]);     // leaf
             //   drawImage(moGlb.img[ IMpth+imgsrc[8] ],X1+d+16-3,yy);   
       }                                   // foglia
       d+=32; x21-=d; if(x21<16) x21=16;
    }   
 
    if(typeof t == "object") t = JSON.stringify(t);
    
    if(script && moLibrary.exists(script)) {   //            <------------------------------------------   se esiste delegate script
 
       var TIPtext="";
       
       var xfield=X1+d, yfield=y, vfield=v, f_cod=Ypx.fcode;
       
       //if(!v || v == "0") {} else { eval(moGlb.delegates[script]); }
       fillStyle = colr;
        eval(moLibrary.getSource(script));
       if(TIPtext) {Arrtip.push( { l:X1+d, t:y, w:x21, h:Yi, tx:TIPtext} );   }
        
    }
    else {if(type==4) { try {  t=moGlb.langTranslate(aFilter[fieldb][v]); }catch(e){}  } if(moCnvUtils.measureText(t,f)>x21) t=moGlb.reduceText(t,f,x21);
    //else if(type==2) eval(moLibrary.getSource('bool'));

  
    
  var cz,vz,tz,virg, ccl=colr, virg=(!type)?"'":"", stz = { "{val}": virg+v+virg };         
  if(aRange[fieldb]){
    for(var g=0;g<aRange[fieldb].length;g++) { 
      cz=aRange[fieldb][g].exp, vz=aRange[fieldb][g].val; 
      
      cz=cz.multiReplace(stz); 
      
      eval("if("+cz+") ccl='"+vz+"';");  
      if(ccl!=colr) break; }
  }
 
    var ed=parseInt(rig["f_editability"]); 
    fillStyle=ccl;                                    
    
    fillText(t,X1+d+7,y+8+(Yi-7)/2); 
    }
  
  } else {                              // drawImage per type 5
  
             }  
 
 restore(); 
  }
 

}}




function clipRow(X1,X2,y,lock){ 
  with(this) with(BDN) {
  var lr=OH+Hlw;
  if(!lock) {  X1-=optr, X2-=optr; 
               if(X2<=Hlw || X1>=lr) return false;
               if(X1<Hlw) X1=Hlw;
               if(X2>lr) X2=lr;  }
 
   X1+=cnvLS, X2+=cnvLS;
   beginPath(); rect(X1,y,X2-X1,Yi); clip();
   
   return [X1,X2];
}}                                                                   




// 0 cancella   1 disegna separazione lock/unlock
function ClearB(m){
  with(this) with(BDN) {
    if(!m) clearRect(0,0,cnvLS+Hlw+OH,SH);
    else{ fillStyle=colrs[6];
          fillRect(cnvLS+Hlw-1,0,1,SH); }
}}



function fMaskLoad(x,y){
  with(this){
  var mj=moGlb.createElement("maskmodule"+name,"div",jj);
  
  if(!NOEVT){
    mj.style.left=0; 
    mj.style.top=0; 
    mj.style.width="100%"; 
    mj.style.height="100%"; 
    mj.style.zIndex=10; 
    mj.style.display="block"; 
    
    mj.onmousedown=function(e){ moEvtMng.cancelEvent(e); return false; }
    mj.onmouseover=function(e){ moEvtMng.cancelEvent(e); return false; }
    
    mj.innerHTML="<img src='"+IM+"ico_loading.gif' style='position:absolute;left:"+(x-17)+"px;top:"+y+"px;width:16px;height:16px;' />";     
  } else {
    if(NOEVT>32) { mj.innerHTML=""; mj.style.display="none"; NOEVT=0; return; } 
  }
    NOEVT++;
setTimeout(name+".fMaskLoad()",200);
}}





function Cell(cnv,x1,x2,s){
  with(this) with(cnv) {
      
      fillStyle=colrs[s]; fillRect(x1+1,0,x2-x1,HeadH);
       
      lineWidth=1; beginPath(); strokeStyle=colrs[6];  
      moveTo(x1+1,HeadH-0.5); lineTo(x2-0.5,HeadH-0.5); lineTo(x2-0.5,1);  
      stroke(); closePath();
}}   


function hCell(i){
  with(this) with(CNV) with(Header[i]){
 
  var X1=x1, X2=x2, s=0, lr=OH+Hlw;
  if(i>=NL) X1=x1-optr, X2=x2-optr;
  if(X1>lr) return;
  if(X2>=lr) X2=lr;
 
  if(i==OVR) s=7;
  if(i==SEL) s=4;
  
  Cell(CNV,X1,X2,s);
  
    // icon left (sort, filter, search) 
  var asrt=SeSort(fieldb), Srt=asrt[0], flb=SeFilterby(fieldb), Srh=SeSearch(fieldb);           // sort   e/o  (filterby | search)
  
  
  // testo
  var f="bold 13px opensansR", d=(Srt)?16:8;
  d+=(flb<0 && Srh<0)?0:15; 
  
  var x21=X2-X1-d-10, t=moGlb.langTranslate(label);      
  font=f; 
  textBaseline="alphabetic"; 
  textAlign="left"; 
  fillStyle=colrs[2]; 
        
  if(moCnvUtils.measureText(t,f)>x21) t=moGlb.reduceText(t,f,x21);
  fillText(t, X1+d, 15.5);     
 
  // dropdown
  if(!Cur && (s==7 || (s==4 && i==OVR))) moCnvUtils.cnvIconFont(CNV,58445,X2-18,3,16,colrs[6]);      
      
  // arrow sort    1 asc   -1 desc
  if(Srt) { var l=X1+1;  
     fillStyle="#888";                                          //  #@# moColors.tgArrow; 
     beginPath();
     var t0=11,t1=15; if(Srt>0) t0=15,t1=11; 
     moveTo(l+2.5,t0); lineTo(l+9.5,t0);
     lineTo(l+6,t1); closePath();  fill(); }
     
  if(flb>-1 || Srh>-1) { var l=X1+d-16;   
   moCnvUtils.cnvIconFont(CNV,58456,l,5,12,colrs[2]);  
  }
      
         
  if(i==(NL-1)) { beginPath(); rect(Hlw,0,OH,HeadH); clip(); }          
}} 


//   fldb = header[x].fieldb       xchg=se Srt==0/-1 pone Srt=1 | se Srt=1 pone Srt=-1      --->  aggiorna/crea in SORT[]      Ms xchg con valore 1 /-1
function SeSort(fldb, xchg, Ms){         // return [Srt, pos]  Srt:0 no, 1 asc, -1 desc  | pos se multisort >0
  with(this){    if(!xchg) { xchg=0; Ms=0; }  if(xchg && !Ms) Ms=0;
    var Srt=0;
    for(var r=0;r<SORT.length;r++) if(SORT[r].f==fldb) { Srt=(SORT[r].m=='ASC')?1:-1; break; }  
    
     
    
    if(xchg) {  if(Ms) { S=(Ms<0)?'DESC':'ASC'; 
                         if(!Srt) SORT.push({f:fldb, m:S }); 
                         else SORT[r].m=S;
                         Srt=Ms;
                } else {
    
              if(Srt==1) { Srt=-1; SORT[r].m='DESC'; }
              else { if(!Srt) { SORT.push({f:fldb, m:'ASC'}); }    // add sort        
                     else SORT[r].m='ASC';
                     Srt=1; }
                }
            }                  
  return [Srt,r];
}}




function SeFilterby(fldb){     // restituisce posizione in FILTER se presente else -1 non trovato
  with(this){
  
  for(var r=0;r<FILTER.length;r++) if(FILTER[r].f==fldb) return r;
  return -1;

}}


function SeSearch(fldb){
  with(this){

  for(var r=0;r<SEARCH.length;r++) if(SEARCH[r].f==fldb) return r;
  return -1;

}}







// EVENTI --------------------------------------------------

function Vbarpos(i){
  with(this){
  vptr=i; bOVR=-1;  
  sedrw=2; draw();  
  drawbodyover(-1);
}}


function Obarpos(i){
  with(this){
  optr=i; 
  sedrw=2; draw(1);
}}


function bodydown(e){  moEvtMng.cancelEvent(e);
with(this){
 
  mCK=[]; noWH=0;
  if(dwnBD) return false;
  
  moMenu.Close();
  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);
  
  eval("Vscr=Vscr"+name);
  eval("Oscr=Oscr"+name); 
  mCK[0]={ iy:P.y, ix:P.x, By:Vscr.ptr, Bx:Oscr.ptr};
        
  dwnBD=1;
 
  if(!isTouch){
   TGJ.style.display="block";
   eval("TGJ.onmousemove=function(e){"+name+".bodymove(e); }");
   eval("TGJ.onmouseup=function(e){"+name+".bodyup(e); }");  
  }    
  return false;
}}


function bodymove(e){ moEvtMng.cancelEvent(e);
with(this){
 
  var i,dx,dy, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), ex=P.x, ey=P.y;

  if(!mCK.length) return false;

  // drag scrollbars
  if(dwnBD==1) {
    if(ex<(zL+Hlw)) noWH=0;
    else {
      dx=Math.abs(ex-mCK[0].ix);
      dy=Math.abs(ey-mCK[0].iy);  
      if(dx>dy) noWH=1; else noWH=0;  // 0=vertical   1=orizzontal
    }
  }
  
  dwnBD++;
  if(dwnBD>2 && dwnBD&1){

    if(noWH){
      eval("Oscr=Oscr"+name);
      i=mCK[0].Bx+(mCK[0].ix-ex);
      Oscr.PosPtr(i);
    }else{
      eval("Vscr=Vscr"+name);
      i=mCK[0].By+(mCK[0].iy-ey);
      Vscr.PosPtr(i);
    }
  }
  return false;
}}



function overMove(e){ moEvtMng.cancelEvent(e);
with(this){

  var Bt=zT+mgT+HeadH+msgH, Bl=zL+mgL, 
      P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e),
      p, bovr=-1;
 
    for(var r=0;r<Abody.length;r++) {
      p=Bt+Abody[r].y;
      if(P.y>=p && P.y<(p+Yi)) { bovr=Abody[r].f; break; }
    }    
    
    // over rows
    if(bovr!=bOVR) { bOVR=bovr; drawbodyover(p-Bt); } 
  return false;           
}}



function bodyup(e){  moEvtMng.cancelEvent(e);
with(this){   
 
  if(!isTouch) TGJ.style.display="none";
 
  // if click
  if(!dwnBD || dwnBD>3) { dwnBD=0; return false; } 
 
  var c2=(cnvLS-mecn)/2, Bli=zL+mgL+c2, Blf=Bli+c2, Bt=zT+mgT+HeadH+msgH,
      P={ x:mCK[0].ix, y:mCK[0].iy }, 
      Px=P.x-zL-mgL, Py=P.y-Bt,
      nd=arrNd.length, fc=0, md=0, p;
 
  mCK=[]; dwnBD=0;
 
    if(nd){
      for(var r=0;r<nd;r++) with(arrNd[r]) {
       if(Px>=l && Px<=(l+32) && Py>=t && Py<(t+Yi)) {     //  moGlb.alert.show(Px+" | "+P.y+" | "+oby.fcode);
          fMaskLoad(l+4,t+mgT+HeadH-1+msgH);
          if(oby.open) CloseP(oby); else OpenP(oby);                  // apre/chiude
          return false;  }    
    }}
    
       
    // edit o check
    for(var r=0;r<Abody.length;r++) {
    p=Bt+Abody[r].y;
    if(P.y>=p && P.y<(p+Yi)){ fc=Abody[r].f; md=1;
      if(P.x>Bli && P.x<Blf) md=2;                         
      break; }
    }
 
    if(md) var afc=JStore[fc], lk=afc.selected;
    
    // editare md=1
    if(md==1) {  
      if((!lk || lk==2)) {
        if(!lkEdit) { 
          lkEdit=fc; olEdit=0; jaxULock([fc],3,1);
          if(crud) {
            var v = JStore[fc]; v.f_visibility = 1, v.f_editability = 1, v.isedit = 1;
            eval(crud).edit(v);
          }
        } // blocco per editing
        else { olEdit=0;
               jaxULock([lkEdit],0,0); 
               for(var r=0;r<arrCHK.length;r++) if(arrCHK[r]==lkEdit) { arrCHK.splice(r,1); break; }   // tolgo edit
               if(fc!=lkEdit) { lkEdit=fc; jaxULock([fc],3,1);
                if(crud) {
                  var v = JStore[fc]; v.f_visibility = 1, v.f_editability = 1, v.isedit = 1;
                  eval(crud).edit(v);           
                }
              }
        }                           
      }}
             
    // checking md=2     se lk=1 o 3  nessuna azione
    if(md==2) {                   
        if(!lk) jaxULock([fc],2,1);                 //  [fcode], tipo,  modo: 0=unlock  1=lock   2=update
        if(lk==2) jaxULock([fc],0,0);
   }        
  return false;          
}}







/*
tipo lock:
0   non selezionato
1   bloccata da altro user (sola lettura)
2   checkbox
3   in Editing

*/ 


function jaxULock(fc,ty,md){         // fc array fcode    modo: 0=unlock  1=lock   2=update
  with(this){
 
   var pr,afc,i, cfc=moGlb.cloneArray(fc);
 
 
  if(md==2) return; // update
 
 
  // attivo/disattivo lucchetto in LStore (per elementi con più padri!)

  for(i=0;i<cfc.length;i++){ 
    if(!JStore[cfc[i]]) continue;
            JStore[cfc[i]].selected=ty;  
  }

  
  
  // array dei checked
  for(i=0;i<cfc.length;i++){
      if(ty>=2 && md==1) { arrCHK.push(cfc[i]); }
      if(!ty && !md) { for(var r=0;r<arrCHK.length;r++) if(arrCHK[r]==cfc[i]) { arrCHK.splice(r,1); }}
  
  } //
  
  
  checkedIds=moGlb.cloneArray(arrCHK);
  
  draw();
}}









function SeChkDechk(){      
  with(this){
  var n=arrCHK.length, s=mf$("checklcksel"+name).src;
  if(s.indexOf("check0")>-1) actmenuchek(0);
  else deCheck();
}}


function deCheck(){        // deseleziona tutti i check possibili
  with(this){

  var pr,afc, fc, n=arrCHK.length, ank=[];
 
  for(var r=0;r<n;r++) { fc=arrCHK[r]; if(fc==lkEdit) continue;
  
  ank.push(fc);
  
   if(!JStore[fc]) {} else JStore[fc].selected=0; 

  }
 
  
  jaxULock(ank,0,0);
}}


     
 


 



function bodyout(e){
with(this){  
  WHEELOBJ="";  //dwnBD=0;
  if(bOVR!=-1) { bOVR=-1;  }

  OBDJ.innerHTML="";                  
}}


 
function drawbodyover(p){
  with(this){ 
  
  if(bOVR<0 || p==-1) OBDJ.innerHTML=""; 
  else {
  var o=10, op="opacity:."+o+";filter:alpha(opacity="+o+");-moz-opacity:."+o+";", c="#404040";
  OBDJ.innerHTML="<div style='position:absolute;left:0;top:"+(p)+"px;width:100%;height:"+(Yi-1)+"px;background-color:"+c+";"+op+"'></div>";
  }

}}




function headown(e){
  with(this){   
  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s=oversel(P.x), ovr=s[0]; 
  Cur=(s[1]==2)?1:0;       
  if(OVR!=ovr) OVR=ovr;
  PXi=P.x, PYi=P.y;     
 
 if(!NMM){ NMM=1; ccMm=0;
 
  // creo movimento su maskera
  maskj=moGlb.createElement("maskDrag"+name,"div");
  maskj.style.zIndex=moGlb.zdrag+1;
  maskj.style.left=0;
  maskj.style.top=0;
  maskj.style.width='100%';
  maskj.style.height='100%';     // , backgroundColor="#ff9"

  eval("maskj."+EVTMOVE+"=function(e){ maskmove(e); }; oldmouseup=document."+EVTUP+"; document."+EVTUP+"=function(e){ maskup(e); };  ");
  } else NMM=0;
  
  
  if(isTouch) moEvtMng.cancelEvent(e);
  return false;  
}}






function maskmove(e){
  with(this){

  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s=overmove(P.x), stri;        // s=[over, mid, X1, X2, pri, ins, scroll]           scroll 0 -10 -20
  
  if(!ccMm) {
 
  // sposta else  Drag
  if(!Cur) stri="<img id='idtmpins' src='"+IM+"insert_field.png' style='position:absolute;display:none;' />"+
                    "<div id='idtmpdrgh' style='position:absolute;width:158px;height:30px;background-color:"+colrs[2]+";opacity:0.9;border-radius:2px;'>"+
                    "<div id='idtmpsimb' style='position:absolute;left:6px;top:5px;' >"+moCnvUtils.IconFont(58827,16,"#D88")+"</div>"+
                    "<div style='position:absolute;left:30px;top:6px;width:120px;height:16px;font:13px opensansR;color:#FFF;overflow:hidden;'>"+
                    moGlb.langTranslate(Header[OVR].label)+"</div></div>";
  else {   
         iext=s[0]; if(!s[1]) iext=s[4];
         RX1=Header[iext].x1 + mgL+cnvLS+zL;
         if(iext>=NL) RX1-=optr;  
         stri= "<canvas id='idtmpdrgh' style='position:absolute;left:"+RX1+"px;top:"+(zT+mgT+HeadH)+"px;' width='40' height='"+(SH+1)+"'></canvas>";
       }
   maskj.innerHTML=stri;     
  }
  
  
  ccMm++;
  
  if(!Cur) {   // sposta
  mf$("idtmpdrgh").style.left=(P.x+16)+"px";
  mf$("idtmpdrgh").style.top=(P.y+16)+"px";
  
  var li=0,ti=0,di="none", srins=moCnvUtils.IconFont(58827,16,"#D88");
  if(s[5]){ di="block", li=(s[1])?s[3]:s[2], ti=zT+mgT-4;
            srins=moCnvUtils.IconFont(58826,16,"#CED"); }
  
  // if scroll left:-10 right;-20
  if(s[6]<-5) {
  if(s[6]==-10) { srins=moCnvUtils.IconFont(58774,16,"#FFFFFF"); if(!sescr) { sescr=1; scrollO(-1); } }
  if(s[6]==-20) { srins=moCnvUtils.IconFont(58775,16,"#FFFFFF"); if(!sescr) { sescr=1; scrollO(1); } }  
  } else sescr=0;
  
  mf$("idtmpins").style.display=di;
  mf$("idtmpins").style.left=(li- ((Browser==1)?5:7) )+"px";
  mf$("idtmpins").style.top=ti+"px";
  
  mf$("idtmpsimb").innerHTML=srins;
  
  
  } else {   // resize
  
   szW=P.x-RX1; if(szW<32) szW=32;
 
   var cj=mf$("idtmpdrgh"), cnn=cj.getContext("2d");
   
   cj.width=szW;
   with(cnn){
   clearRect(0,0,szW,SH);
   fillStyle=fillStyle="rgba(224,224,224,0.5)";                      // #@# moColors.maskMoveFill; 
   fillRect(0,0,szW,SH);
   lineWidth=1;
   strokeStyle="#808080";                                            // #@# moColors.maskMoveStroke;  
   beginPath();
   moveTo(0.5,0);
   lineTo(0.5,SH+0.5);
   lineTo(szW-0.5,SH+0.5)
   lineTo(szW-0.5,0);
   closePath();
   stroke();
   }
  
  }
  
 
  moEvtMng.cancelEvent(e);
  return false;
}}






function maskup(e){
  with(this){

  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s;

  if(P.x==PXi && P.y==PYi) {                  // click
         
      if(!Cur){
      s=oversel(P.x), sel=s[0]; 
      if(s[1]) createmenu(sel); 
      else { if(!fnoAct()) { 
             if(SEL!=sel) { actclearfilter(1); SEL=sel; } 
             NMM=0; 
             SeSort(Header[sel].fieldb,1); ActRefresh();                      //   sort click header
             sedrw=1; draw(); 
             }   
          }
      }
  } else {                                    // drag   
     
     if(!Cur){   // ordine field    
              s=overmove(P.x);
              if(s[5]) {
              var i0=OVR, i1=s[0]+s[1]; lk=Header[s[0]].lock, dse=0;
              if(i1<=SEL && OVR>SEL) dse=1; 
              if(i1>SEL && OVR<=SEL) dse=-1;
              var v=Header.splice(i0,1);
              if(i1>OVR) i1--;

              Header.splice(i1,0,v[0]); Header[i1].lock=lk;
              
              if(OVR==SEL) SEL=i1; else SEL+=dse;  // mantengo selezioanto      
              fmenuList();
              updHeader(); 
                
              } else {            // aimazione ritorno 
                      var iij=mf$("idtmpdrgh");
                      if(!iij) { endAnim();  }
                      else {
                       var x=parseInt(iij.style.left), y=parseInt(iij.style.top);  
                       with(Header[OVR]){ var xa=x1+mgL+cnvLS+zL, ya=zT+mgT;   }
                       var nxa=Math.abs((xa-x)/50), nya=Math.abs((ya-y)/50), nn=parseInt((nxa+nya)/2); if(nn>8) nn=8;
                       if(nn<2) nn=2; var dx=parseInt((xa-x)/nn), dy=parseInt((ya-y)/nn);
                       
                       eval("maskj."+EVTMOVE+"='';");
                       headRitAnimat(x,y,dx,dy,0,nn);
                      }
             if(isTouch) moEvtMng.cancelEvent(e);      
             else return false;
                }  
     
     } else {   // resize field
     
     with(Header[iext]){   wdth=szW;   }  
     updHeader();
     
     }
  
      OVR=-1; sedrw=3; draw(); 
      NMM=0;
      }
      
  var pj=maskj.parentNode; pj.removeChild(maskj);
  eval("document."+EVTUP+"=oldmouseup");       
  sescr=0;    
  if(isTouch) moEvtMng.cancelEvent(e);      
  else return false;
}}



function headRitAnimat(x,y,dx,dy,t,nt){
  with(this){

  t++;
  var nx=x+dx*t, ny=y+dy*t;
  mf$("idtmpdrgh").style.left=nx+"px";
  mf$("idtmpdrgh").style.top=ny+"px";

  if(t>nt) {  endAnim(); return false;  }

setTimeout(name+".headRitAnimat("+x+","+y+","+dx+","+dy+","+t+","+nt+")",50);
}}


function endAnim(){
  with(this){
  
  var pj=maskj.parentNode; pj.removeChild(maskj);
  eval("document."+EVTUP+"=oldmouseup");       
  sescr=0; OVR=-1; sedrw=3; draw();
  NMM=0;        
  return false; 
}}



function scrollO(d,t){  if(!t) t=0;  
  with(this){   if(!sescr) return;
 
  t++;
  if(t>3){
  optr+=d*16; 
  if(optr<0) optr=0;
  if(optr>(Huw-OH) ) optr=Huw-OH;

  eval("Oscr=Oscr"+name); 

  Oscr.Resize( { ptr:optr } ); 
  if(!optr || optr==(Huw-OH) ) return;
  }

setTimeout(name+".scrollO("+d+","+t+")",60);
}}



function headmove(e){    
  with(this){  if(NMM) return;
   
   var P=moEvtMng.getMouseXY(e), s=oversel(P.x), ovr=s[0], cur=Curs[s[1]], oC=Cur;
   Cur=(s[1]==2)?1:0;
 
   if(OVR!=ovr || oC!=Cur) { OVR=ovr; sedrw=3; draw(); }
   CNJ.style.cursor=cur;
   
   if(isTouch) moEvtMng.cancelEvent(e);      
   else return false; 
}}
                                       

function oversel(x){    // restituisce  [over, cur]   cur: 0:normal  1:pointer  2:col-resize
  with(this){   
   var dx=mgL+zL+cnvLS, lr=OH+Hlw, ovr=-1, X1=0,X2=0, cur=0, pri=-1, mid=0, x21=0;    
   for(var i=0;i<NT;i++)  with(Header[i]){  if(!hidd) continue;
   if(i>=NL) { X1=x1-optr, X2=x2-optr;  if(X2<=Hlw) continue;
               if(X1<Hlw) X1=Hlw;  if(X1>=lr) break;
               if(X2>lr) X2=lr;  }
   else X1=x1, X2=x2;        
   X1+=dx, X2+=dx,  x21=(X2-X1)/2;
   if(x>=X1 && x<X2) { ovr=i; mid=(x<(X1+x21))?0:1;  break; }  
   pri=i;  
   }
   if(x>(X2-24)) cur=1;
   if(x>(X2-6) || x<(X1+6)) cur=2;
   
   if( (pri<0 && !mid)  ) cur=0;  
   
   return [ovr,cur,pri,mid];   
}}



function overmove(x){    // restituisce  [over, mid, X1, X2, pri, ins, scroll]   mid: 0:left  1:right    pri=field precedente  nxi=field successivo  ins=se inserimento possibile
  with(this){
   var dx=mgL+zL+cnvLS, lr=OH+Hlw, ovr=-1, X1=0,X2=0, mid=0, x21=0, rg=12, pri=-1, nxi=-1, ins=1, PRI=-1, NXI=-1, sc=0; 
   
   if(x>(dx+Hlw) && x<(dx+rg+Hlw)) sc=-10;
   if(x>(dx+lr-rg) && x<(dx+lr)) sc=-20;
      
   for(var i=0;i<NT;i++) with(Header[i]) {  if(!hidd) continue;
   if(i>=NL) { X1=x1-optr, X2=x2-optr; if(X2<=Hlw) continue;
               if(X1<Hlw) X1=Hlw;  if(X1>=lr) break; if(X2>lr) X2=lr;  }
   else X1=x1, X2=x2;        
   X1+=dx, X2+=dx, x21=(X2-X1)/2;
   if(x>=X1 && x<X2) { ovr=i;  mid=(x<(X1+x21))?0:1;  break;  }
   pri=i; }  
    
   // cerco PRI e NXI di OVR;
   var b=0;
   for(i=0;i<NT;i++) with(Header[i]) { if(!hidd) continue; 
   if(b) { NXI=i; break; }
   if(i==OVR) { b=1; continue; }
   PRI=i; }

   if(ovr<0 || ovr==OVR || ((ovr==NXI && ovr!=NL) && !mid) || ((ovr==PRI && ovr!=(NL-1) ) && mid) ) ins=0;
     
   return [ovr,mid,X1,X2,pri,ins,sc];   
}}




function headout(e){
  with(this){   
  if(NMM) return;
  OVR=-1; 
  sedrw=1; draw();
}}


                          







// menu  -----------------------------------------------------------------------
function fieldlist(){
  with(this){

  var l=zL+mgR+4, t=zT+mgT-2, w=VW, h=HeadH; 
  moMenu.Start(name+".menulist",{l:l, t:t, w:w, h:h},2);

}}



function viewmenucheck(){
  with(this){
  var l=zL-mgL+cnvLS-mecn, t=zT+mgT-2, w=VW, h=HeadH; 
  moMenu.Start(name+".menuChecked",{l:l, t:t, w:w, h:h},2);
}}





function createmenu(i){
  with(this) with(Header[i]){
 
  // aggiornamenti Fmenu
  
  Fmenu[3].Disable=(i==SEL)?1:0;   // disabilito add filter se selezionato
  Fmenu[3].Sub[2].Disable=1;
  
  sef=1;
  if(type==3 || type==4) { 

                 Fmenu[7].Sub=[];
                 Fmenu[3].Sub[2].Sub=[];

               var fly=SeFilterby(fieldb), fsl=-1, stu, rr;  
               if(fly>-1) fsl=FILTER[fly].v; 

                 var tx;  
                 for(var r=0;r<((type==4)?(Filterby[fieldb].length):(FilerData.length));r++) {
                 
                 if(type==4) tx=Filterby[fieldb][r].Txt, rr=Filterby[fieldb][r].Value;
                 else tx=FilerData[r].Txt, rr=r;
                 
                 stu=(rr==fsl)?1:0;
                 
                 Fmenu[7].Sub[r]={ Txt:tx, Status:(i==SEL)?stu:0, Type:3, Group:((i+20)*10), Icon:['','checked'], Fnc:name+".actfilterby("+i+","+rr+",0,"+type+")" };  
                 Fmenu[3].Sub[2].Sub[r]={ Txt:tx, Status:stu, Type:3, Group:((i+20)*10), Icon:['','checked'], Fnc:name+".actfilterby("+i+","+rr+",1,"+type+")" };  
                 }

                 sef=0; 
                 Fmenu[3].Sub[2].Disable=0;
               }             

  Fmenu[7].Disable=sef;    // disab filter se non campo type 3 o 4
  
  
  
  // se SORT attivo
  var asrt=SeSort(fieldb), Srt=asrt[0], Srp=asrt[1];
  Fmenu[0].Status=0; Fmenu[1].Status=0;
  Fmenu[3].Sub[0].Sub[0].Status=0; Fmenu[3].Sub[0].Sub[1].Status=0;
  
  Fmenu[0].Fnc=name+".actsort("+i+",1)";
  Fmenu[1].Fnc=name+".actsort("+i+",-1)";
  Fmenu[3].Sub[0].Sub[0].Fnc=name+".actaddsort("+i+",1)";
  Fmenu[3].Sub[0].Sub[1].Fnc=name+".actaddsort("+i+",-1)";
   
  if(Srt) { if(!Srp){       // [0]/[1]
  if(Srt>0) Fmenu[0].Status=1; 
  if(Srt<0) Fmenu[1].Status=1; 

  } else {         // [3].Sub[0].Sub[0]/[1]
  if(Srt>0) Fmenu[3].Sub[0].Sub[0].Status=1; 
  if(Srt<0) Fmenu[3].Sub[0].Sub[1].Status=1; 

  }}
  
  
  // Fnc hidden  10
  Fmenu[10].Fnc=name+".acthidden("+i+")";
 
 
  // Fnc lock   8
  Fmenu[8].Fnc=name+".actlock("+i+")";
  Fmenu[8].Status=Header[i].lock;
 
 
  // Search  6  
  if(type>3) {  Fmenu[6].Disable=1;  Fmenu[3].Disable=1;   }
  else { 
    Fmenu[6].Disable=0;  Fmenu[3].Disable=0;
    Fmenu[6].Fnc=name+".actsearch("+i+","+type+",0)";    
    Fmenu[3].Sub[1].Fnc=name+".actsearch("+i+","+type+",1)";     
  }  
    
 
    
      
      
  NMM=1;
  var X2=x2, lr=OH+Hlw;
  if(i>=NL) X2=x2-optr;
  if(X2>=lr) X2=lr;
  var l=zL+mgL+cnvLS+X2-20, t=zT+mgT, w=20, h=HeadH;
  
  
  moMenu.endFNC=name+".NMM=0; "+name+".headout();";                 // NMM  chiusura menu
  
  moMenu.Start(name+".Fmenu",{l:l, t:t, w:w, h:h},2);

}}


function fnoAct(){   
  with(this){ 
//  if(lkEdit) {  moGlb.alert.show("Close/save the request in editing.");
//                return 1; }
  return 0;
}}


// Hidden
function acthidden(i){      // v= -1 da Ovr     m=stato 
  with(this){    if(fnoAct()) return;
    with(Header[i]){ hidd=(hidd)?0:1;
                     menulist[i].Status=hidd;
                     if(lock) { actlock(i); return; }
                    }
  updHeader(); 
  sedrw=2; draw();
}}
  


// Lock/unlock
function actlock(i){      // v= -1 da Ovr     m=stato 
  with(this){     if(fnoAct()) return;
    with(Header[i]){ lock=(lock)?0:1;  if(!lock) NL--;  }
 
    var a=Header.splice(i,1);
    Header.splice(NL,0,a[0]);
 
    fmenuList();
    updHeader(); 
  sedrw=2; draw();
}}





function acthierarchy(){      // come grid/tree
  with(this){    if(fnoAct()) return;
  
  asHy=asHy?0:1;       
   
  ActRefresh();
}}
  


function actsort(i,v,z){      // i=Header   v= 1 / -1
  with(this) with(Header[i]){     if(fnoAct()) {  return; }

    if(!z) z=0;

    if(i!=SEL) { actclearfilter(); SEL=i; z=1; }
    SeSort(fieldb,1,v);
 
    if(!z) ActRefresh();
}}


function actaddsort(i,v,z){
  with(this) with(Header[i]){   if(fnoAct()) return;
  
  if(!z) z=0;
 
  if(SEL<0) { actsort(i,v,0); z=1; }
  else SeSort(fieldb,1,v);
 
  if(!z) ActRefresh();
}}


function actfilterby(i,v,sb,ty){  // nome fieldb   v=value    sb  0=SEL  1=add filter      ty=type 3=data 4 select
  with(this) with(Header[i]){   if(fnoAct()) return;


  if(!sb) { actclearfilter(1); SEL=i; }
  
  var sf=SeFilterby(fieldb);
  if(sf>-1) FILTER[sf].v=v;
  else FILTER.push( { f:fieldb, v:v, ty:ty } )
 
  ActRefresh();
}}



 


function actclearfilter(z){
  with(this){    if(fnoAct()) return;

  SORT=[ {f: eval(crud).orderby, m: eval(crud).order} ]; FILTER=[]; SEARCH=[];  SEL=-1;

  if(!z) ActRefresh();
}}





function actmenuchek(v){
  with(this){  if(!v) v=0;

  var r, ab=Abody.length, af=[], fc, afc, lk;

  if(!v){ // add viewed
    for(r=0;r<ab;r++) { fc=Abody[r].f; 
    
        lk=JStore[fc].selected;
    
        if(!lk) af.push(fc); }
    if(af.length) jaxULock(af,2,1);
 }
 else
 {  // deseleziona viewed
    for(r=0;r<ab;r++) { fc=Abody[r].f; 
    
        lk=JStore[fc].selected;
    
        if(lk==2) af.push(fc); }
    if(af.length) jaxULock(af,0,0);
  
 }
}}
 
 
 
function actselector(jse){
  with(this){ 

if(!jse) SELECTOR=[];
else SELECTOR=moGlb.cloneArray(jse);

ActRefresh();
}} 
 
 
                                                                               
 
 
 
function viewchecked(){
  with(this){ 

  asHy=0;
 
  if(arrCHK.length) {  AND=[ { f:"f_code", z:" in ("+arrCHK.join(",")+") " } ];
                       
  } else { AND=[]; if(Hy&1) asHy=1; else asHy=0; }
  
 timeRefresh();
}}
 
 
 
 
 
 
function viewall(){
  with(this){ 

  if(Hy&1) asHy=1;
 
  
  timeRefresh();
}}
 
 
 
 
 
 
 
 
// type 0=testo | 1=numerico | 2=booleano | 3=data | 4=select (filter) | 5=immagine 
 
/*

this.serchSelect=[
      ["Contiene","Non contiene","Uguale","Diverso"],                               // 0 = testo
      ["Uguale","Diverso","Maggiore di","Minore di","Espressione"],                // 1 = numerico
      ["Vero","Falso"],                                                           // 2=booleano
      ["Uguale","Diverso","Maggiore di","Minore di","Periodo"]                   // 3=data
];


*/ 
 
 
 // Search
function actsearch(i,ty,m){    // i=Header  ty=type campo  m=1 addsearch
  with(this){   if(fnoAct()) return;

if(ty>3) { moGlb.alert.show(moGlb.langTranslate("This search is not available.")); return false; }

// creo maskj e finestra
  maskj=moGlb.createElement("maskDrag"+name,"div");
  maskj.style.zIndex=moGlb.zdrag+1;
  maskj.style.left=0;
  maskj.style.top=0; 
  maskj.style.width='100%';
  maskj.style.height='100%';
  maskj.style.backgroundColor="#fff";
   
  moGlb.setOpacity(maskj,50);
  
  eval("maskj."+EVTDOWN+"=function(e){ "+name+".closefSearch(e);  }; ");  // chiusura


  var sj=moGlb.createElement("idBoxSearch","div"), 
      bw=280, bh=120,
      bl=zL+parseInt((W-bw)/2), bt=zT+parseInt((H-bh)/2);
  
   sj.style.zIndex=moGlb.zdrag+2;
   sj.style.left=bl+"px";
   sj.style.top=bt+"px";
   sj.style.width=bw+"px";
   sj.style.height=bh+"px";
   sj.style.backgroundColor="#fff";
   sj.style.border="1px solid #aaa";
  
//-----  
  
  var optSl="";
  for(var r=0;r<serchSelect[ty].length;r++) optSl+="<option value="+r+">"+serchSelect[ty][r]+"</option>";
  
  var Sfnt="font:13px opensansR;";
  
  
  sj.innerHTML="<form action='' method=''><span style='font:16px FlaticonsStroke;color:#000000;position:absolute;left:"+(bw-26)+"px;top:10px;cursor:pointer;' onclick='"+name+".closefSearch(event);'>"+String.fromCharCode(58827)+"</span>"+
               "<div style='position:absolute;left:10px;top:10px;font:13px opensansR;'><b>"+Header[i].label+ //" | tipo: "+ty+
               "</b><div style='position:absolute;left:0;top:26px;'>"+moGlb.langTranslate("Search:")+
               "<br><input id='idval0"+name+"' type='text' style='"+Sfnt+"width:220px;' /></div>"+
               "<div style='position:absolute;left:150px;top:26px;'>&nbsp;<br><input id='idval1"+name+"' type='text' style='"+Sfnt+"width:200px;display:none' /></div>"+
            //  "<div style='position:absolute;left:0;top:66px;width:200px;"+Sfnt+"'><input id='idchk0"+name+"' type='checkbox' checked style='padding-left:0;margin-left:0;' /> Maiuscole/Minuscole</div>"+
            //   "<div style='position:absolute;left:0;top:96px;width:200px;'><select id='idsel0"+name+"' onchange='"+name+".changeidSel0(this,"+ty+")' style='width:230px;"+Sfnt+"'>"+optSl+"</select></div>"+
            //   "<div style='position:absolute;left:0;top:120px;width:240px;"+Sfnt+"'><input  id='idchk1"+name+"' type='checkbox' style='padding-left:0;margin-left:0;' /> Cerca in tutte le fasi</div>"+
               "<button style='position:absolute;left:0;top:80px;"+Sfnt+"' onclick='"+name+".TrovaSearch(event,"+i+","+ty+","+m+");'>"+moGlb.langTranslate("Find")+"</button>"+
               "</form></div>"; 

  mf$("idval0"+name).focus();

//--

}}
 
function changeidSel0(sj,ty){
  with(this){
  
if(ty!=3) return; 
var v=sj.value,d=(v==4)?"block":"none"; 
mf$("idval1"+name).style.display=d;
}} 
 
 
function closefSearch(e){
  with(this){

var pj=maskj.parentNode; pj.removeChild(maskj); 
var sj=mf$('idBoxSearch'); pj=sj.parentNode; pj.removeChild(sj); 

moEvtMng.cancelEvent(e); 
}}


 
 
 
/*
SEARCH

f:    fieldb
v0:   valore da cercare
v1:   valore in range date
Mm:   0: ignora maiuscole e minuscole
type: tipo di campo 0-3
All:  0=solo nei gruppi di fase attivi  / 1=cerca in tutte le fasi dei workflow
 
*/ 
function TrovaSearch(e,i,ty,m){
  with(this){

  var inp0=mf$("idval0"+name).value,      // valore da cercare
   inp1=mf$("idval1"+name).value,
 // chk0=mf$("idchk0"+name).checked,   // se maiuscole e minuscole
 // sel0=mf$("idsel0"+name).value,     // scelta contiene, ....
 //  chk1=mf$("idchk1"+name).checked,
   fld=Header[i].fieldb,
   chk0=0, sel0=0, chk1=0;
 
//chk0=(chk0)?0:1;
 
if(!m) { actclearfilter(1); SEL=i; SEARCH=[];  }   
var ss=SeSearch(Header[i].fieldb); if(ss>-1) SEARCH.splice(ss,1);
 
SEARCH.push( { f:fld, v0:inp0, v1:inp1, Mm:chk0, Sel:sel0, type:ty, All:chk1 } )

 
closefSearch(e);
ActRefresh();
}} 
 
 
 
 
 
 
 
 
 
 


function ActRefresh(itm){ 
  with(this){
 
  //if(lkEdit) { moGlb.alert.show("Close/save the request in editing.");  return;  }


  // lastrefresh
  if(!itm){
    var t=lastrefresh;
    lastrefresh=Math.floor(new Date().getTime()/1000);
    if((lastrefresh-t)<2) return;                           // do not refresh if delta time < 2 seconds
  }


  if(inLo==1) inLo=-1;
  aLoad=[];  // scarico load
  if(!inLo) { timeRefresh(); return; }     // attendo completamento processi
 
setTimeout(name+".ActRefresh(1)",50);
}}


function timeRefresh(){
  with(this){    
   ResetYLS(); 
   LoaData(-1, 0, 0, 0, "root");  
}}




 
 
function ResetYLS(){    // resetta LStore e Ypos
  with(this){
    
    var f,r;   
  
    JStore=[];
   
    Ypos=[];
    rifY=[];
    multifcode=[];
    Hnr=0;
    Abody=[];
    
    arrCHK=[];    // non cancello i checkkati
    arrLK=[];
    Root="";
    
    adds=0; upds=0; msgH=-1;    
}}
 
 
 
 
 
 
 
  
//---------------------------------------------------------------------------------

function updHeader(){      // aggiorno per hidden e lock 
  with(this){

  NT=0; NU=0; NL=0; Hlw=0; Huw=0; var sw=0; 
  for(var r=0;r<Header.length;r++) with(Header[r]) {  NT++;   
    if(hidd) { x1=sw; sw+=wdth; x2=sw;                                   // larghezza header lock e unlock
    if(lock) NL++, Hlw=sw; else Huw+=wdth, NU++; } }
 
 
  eval("Oscr=Oscr"+name); 
  Oscr.Resize( { HH:Huw } );
  
  
  predraw();
}} 

function fmenuList(){ // creo menu list field 
  with(this){

  menulist=[];
  for(i=0;i<Header.length;i++) with(Header[i]){  
  menulist.push( {Txt:label, Type:2, Status:hidd, Icon:['', 'FlaticonsStroke.58786'], Fnc:name+".acthidden("+i+")"} );
  } 

}}
 //----------------------------------------------------------------------------



 
 
 


 

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// ENGY
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



 
 
// props
this.Ypos=[];          // array Ypos[]={ posY:, fcode:, levl, nch }   -->  Ypos.length N. elementi caricati  levl livello in tree
this.rifY=[];          // indice posizione f_code -> Ypos
this.inLo=0; 
this.aLoad=[];


this.stpclear=1000;
this.ndmax=5;
this.istatus=0;
this.infostri="";
this.oinfostri="-";
this.timeinf=0;

// metodi
this.OpenP=OpenP;
this.CloseP=CloseP;
this.RemoveEl=RemoveEl;

this.finfo=finfo;
this.ftimefinfo=ftimefinfo;

this.loadata=loadata;
this.LoaData=LoaData;

this.retdata=retdata;     



/*
1   loading     -1 end loading
2   clearing    -2 end clearing
*/


function finfo(v){
  with(this){ 
 
    if(v<0) { if(istatus&(v*-1)) istatus&=(istatus+v); } else istatus|=v;
    var stri="", icl="#a0a0a0", bi="";
 
if(istatus&1) stri+=moGlb.langTranslate("Loading...");

if(istatus&4) icl="a02020";

if(istatus&2) { if(stri) bi="&nbsp;/&nbsp;"; stri+="<span style='color:"+icl+";'>"+bi+moGlb.langTranslate("Cleaning up...")+"</span>";  }

if(istatus) stri="<img src='"+IM+"ico_loading.gif' height='8px' />&nbsp;"+stri;

infostri=stri; 
if(timeinf<=0) { timeinf=2; ftimefinfo(); }

}}



function ftimefinfo(){  
  with(this){  timeinf--;    
                                      
   if(infostri!="") { if(oinfostri!=infostri) { mf$("infotxt"+name).innerHTML=infostri; oinfostri=infostri; } 
                      timeinf=2; }                                                      
     if(timeinf<=0) { mf$("infotxt"+name).innerHTML=""; oinfostri="-"; return;   }
     
setTimeout(name+".ftimefinfo()",500);
}}










// cancella fcode: splice Ypos / rifY[rtree]  / Lstore.Del 
function RemoveEl(fc, rtr){  
  with(this){       if(!rifY[rtr]) return; 

    var f=rifY[rtr], cd, r, prg, ar; 
    Ypos.splice(f,1);  delete rifY[rtr];   

    for(r=f;r<Ypos.length;r++)  { cd=Ypos[r].rtree; rifY[cd]=r; }  // aggiorno i riferimenti 
    
    multifcode[fc]--; if(multifcode[fc]<1) delete JStore[fc]; 
          
    
}}



function OpenP(yp){
  with(this){ 
  var nb=yp.nch;
        aLoad.push([yp.fcode,0,nb,yp.levl, yp.rtree]); loadata();
}}



function CloseP(yp){
  with(this){   
 
  nodrw=1;  
  
  yp.open=0; 
  var fc=yp.fcode, rtr=yp.rtree, pp=yp.levl, ni=yp.nch, i=rifY[rtr], k=0, ny=Ypos.length, ii=i, nhi, fi, pr, lsr, afi=[], rti;
  i++, ii++;
  
  // tolgo figli e nipoti
  while(i<ny && Ypos[i].levl>pp) { 
  
  fi=Ypos[i].fcode; rti=Ypos[i].rtree; 
  
  if(JStore[fi].selected) afi.push(fi);  multifcode[fi]--; if(multifcode[fi]<1) delete JStore[fi];
   
   
  delete rifY[rti];
  
  if(Ypos[i].open) ni+=Ypos[i].nch; i++;  k++; }   
  
  jaxULock(afi,0,0);  
  Ypos.splice(ii,k);
   
  // aggiorno posizioni successive ed i riferimenti
  ny=Ypos.length, nhi=ni*Yi;
  for(var r=ii;r<ny;r++) { Ypos[r].posY-=nhi; rifY[Ypos[r].rtree]=r; }

  Hnr-=ni;
 
 
 
  
  eval("Vscr=Vscr"+name);
  Vscr.Resize( { HH:Hnr*Yi } ); 
  animdraw(rtr,ni,ni,1); 
  
}}








function loadata(){
with(this){ 

if(!inLo && aLoad.length) { inLo=1; var a=aLoad.shift();  LoaData(a[0],a[1],a[2],a[3],a[4],a[5]); }       
if(!aLoad.length) return;       

setTimeout(name+".loadata()",100);
}} //





function LoaData(pp, li, ln, lvl, rtr){
with(this){    


// SORT[]

// FILTER[]

// SEARCH[]

// AND[]

// SELECTOR

// FILTER
 

// mf$("debug3").innerHTML+=pp+" | limit: "+li+", "+ln+" | lev: "+lvl+" | rtree: "+rtr+" | Hnr: "+Hnr+"<br>";      
// rtr tree del padre pp 

var z=rifY[rtr], vtime=0;
if(z) vtime=Ypos[z].timestamp;

                     
var ja=JSON.stringify({ Hierarchy:asHy, Prnt:pp, Tab:[Ttable, ftype], module_name: crud, Sort:SORT, Filter:FILTER, Search:SEARCH, Ands:AND, Selector:SELECTOR, Limit:[li,ln], rtree:rtr, timestamp:vtime });
Ajax.sendAjaxPost( moGlb.baseUrl+"/treegrid/jax-read-child", "param="+ja , name+".retdata" );

finfo(1);
}}








/*
ar: json  ritorno

{ ntot: n, data:[ [0]head,  [1...n]value ];

ntot:   -1  se treegrid   n se grid

data:
array[0]        header[   ]              
array[1....n]   value[  ]              // f_parent_code=[padre0, padre1, ...]


*/

function retdata(a){
  with(this){
  
  finfo(-1);
  if(inLo<0) { inLo=0; seBlk=0; return; }
   
  nodrw=1; 

  

  var ar=JSON.parse(a), nar=(ar.data.length)-1, prg,fcd, vtime=ar.other.timestamp, mdf=ar.other.modify;
    if(moGlb.isset(ar.message)) moGlb.alert.show(moGlb.langTranslate(ar.message));

    

 // reload
 if(!Root) {   
 
     var nn=parseInt(ar.ntot);       
     Root={ f_code:0, rtree:"root", nch:nn, levl:-1, open:1, timestamp:-1  }             // root implicito 
     
     JStore[0]={ f_code:0, rtree:"root", nch:nn, levl:-1, open:1 };       
                        
     Ypos.push({ posY:-Yi, fcode:0, rtree:"root", fparent:-1, fpos:0, nch:nn, levl:-1, open:1, timestamp:-1 }); 
     rifY["root"]=0;             
 
      ln=nn;
      Hnr=0;
 
      if(nn) LoaData(0, 0, ln, 0, "root");                                // carico primi record
      else  {
         mf$("infotxt"+name).innerHTML=moGlb.langTranslate("0 Record found.");   
         ClearB(0);
      }
 
      eval("Vscr"+name+".Resize( { ptr:0 } );");
      return;
   }
 
 
   
 
  if(nar<0) { if(NOEVT) NOEVT=100; draw();
              // moGlb.alert.show("no record child\n"+nar);
              inLo=0; return; }
 
  

  var ahe=[], rhe=[], ng=ar.data[0].length, aa, g;
  for(var r=0;r<ng;r++) { ahe[r]=ar.data[0][r]; rhe[ar.data[0][r]]=r; }          // 0->"fpdr", 1->"nch", 2->"selected", 3->"fpos, 4->"rtree" (padre)
                                                                                 // f_editability
                                                                                 
  var prnt=ar.data[1][0],         // padre chiamante
      rtr=ar.data[1][4],          // rif padre
      p=rifY[rtr],                // posizione padre in Ypos
      lev=Ypos[p].levl+1,         // level figli
      ps=ar.data[1][3],           // posizione limit inizio tra fratelli  
      ny=Ypos.length,
      y=Ypos[p].posY,             // posiz px padre
      ych=y+ps*Yi,                // posY primo
      ypo=Ypos[p].open,           // se padre open o close
      i=p+1;
    
 

  // inserisco in localstorage  
  for(r=1;r<=nar;r++){
     fcd=parseInt(ar.data[r][rhe.f_code]); 
     
     JStore[fcd]={}; 
     aa={}; for(g=0;g<ng;g++) {  JStore[fcd][ahe[g]]=ar.data[r][g]; }  
     
                                                    
     rifY[rtr+"|"+fcd]=i;          
     ych+=Yi;
 
   
     Ypos.splice(i,0,{posY:ych, fcode:fcd, fpdr:prnt, rtree:rtr+"|"+fcd, fpos:ar.data[r][3], nch:ar.data[r][1], levl:lev, open:0, timestamp:vtime });               //------------------------------------------------ 
     if(!multifcode[fcd]) multifcode[fcd]=1; else multifcode[fcd]++;
     i++;
     }
     
     ny=Ypos.length;                        
     for(r=i-1;r<ny;r++) { rifY[Ypos[r].rtree]=r;  }             // aggiorno i riferimenti
     
 
     if(!ypo){                                                  // aumento dal fratello del padre di nch*Yi se non era !open
     while(i<ny && Ypos[i].fpdr==prnt) i++;                     // trovo primo fratello
     var dnh=Ypos[p].nch*Yi;
     for(r=i;r<ny;r++) Ypos[r].posY+=dnh;
     }
 
     Ypos[p].open=1; 
 
     Hnr+=Ypos[p].nch;
     eval("Vscr=Vscr"+name); 
      
      

             
     Vscr.Resize( { HH:Hnr*Yi } ); 
     
 
   animdraw(rtr,nar,Ypos[p].nch,0); 
 
  
  inLo=0;
   
}}  






 
  
 

//------------------------------------------------------------------------------
// animazione open


function animdraw(rtr,nar,nah,oc){
with(this){  
 
if(noAnim) return;
 
CDJ=moGlb.createElement("idcanvanim"+name,"canvas",j); 
  
var ww=cnvLS+Hlw+OH;
with(CDJ) width=ww, height=SH;
CDJ.style.left=mgL+"px";
CDJ.style.top=(mgT+HeadH)+"px";
CDJ.style.display="none";   // , backgroundColor="#ff0";

if(rtr=="root") { if(NOEVT) NOEVT=100; nodrw=0; sedrw=2; draw(); return; }
 
var y=0;
for(var r=0;r<Abody.length;r++) if(Abody[r].rtree==rtr) { y=Abody[r].y+Yi; break; } 


tf=nar*Yi;
if(nar<nah) {  tf=nah*Yi; if(tf>SH) { tf=SH-(SH%Yi)+Yi;  }   }


if(SH<=y) { if(NOEVT) NOEVT=100; nodrw=0; sedrw=2; draw(); return; }


if(!oc){

OMD=BDN.getImageData(0,0,ww,y);
FMD=BDN.getImageData(0,SH-tf,ww,tf);
oBDN=BDN; BDN=CDJ.getContext("2d");

draw2(1);
IMD=BDN.getImageData(0,y,ww,SH-y);


} else {

OMD=BDN.getImageData(0,0,ww,y);           // A

if(tf>SH) { tf=SH-(SH%Yi)+Yi; }
FMD=BDN.getImageData(0,y,ww,tf);          // B1
oBDN=BDN; BDN=CDJ.getContext("2d");

draw2(1);
IMD=BDN.getImageData(0,y,ww,SH-y);         // B2+C

if(tf>SH) { tf=SH-(SH%Yi)+Yi;  }
 
}

tmanimdrw(y, ww, 0, 3, tf , oc);
}}


function refresh() { with(this) { AND = []; actclearfilter(); } }

function tmanimdrw(yi, ww, t, k, tf, m){
with(this){

t+=k; k+=5; if(t>=tf) t=tf; 

oBDN.clearRect(0,0,ww,SH);

if(!m){

oBDN.putImageData(IMD,0,yi+t-tf);
oBDN.putImageData(FMD,0,SH-tf+t);

} else {

oBDN.putImageData(FMD,0,yi-t);     // B1
oBDN.putImageData(IMD,0,yi-t+tf);        // B2 + C

}



oBDN.putImageData(OMD,0,0);

if(t>=tf) { BDN=oBDN; if(NOEVT) NOEVT=100; nodrw=0; sedrw=2; draw(); return; }

setTimeout(name+".tmanimdrw("+yi+","+ww+","+t+","+k+","+tf+","+m+")",40);
}}


  



} // fine treeGrid

