/*------------------------------------------------------------------------------------

solo al primo resize carica loadroot



updateChecked(arrJStore);
deleteSelected();
saveList();


//------------------------------------------------------------------------------------
*/


function moTreeGrid_light_check(nm,z) {


// props
this.name=nm;

this.create = function (z) {
var props={ history:0,
            Vscr:null, Oscr:null, bookmark_name:'', bookmark_id:0, moduleName: "",
            checkmode:1, 
            f_code:0, Ttable:"t_ware", crud:"", ftype:1, J:null, jj:null, W:0, H:0, zL:0, zT:0,  ig:[],
            nodrw:0, sescr:0, sedrw:3, SEDRW:3, Stp:40, addStp:0, timelock:5000, tUlk:1,
            NOEVT:0, noAnim:0, 
            mgL:0, mgT:0, mgR:0, mgB:0, VW:18, padf:4, iext:0, RX1:0, szW:32,                                                       // margini
            HeadH:24, cnvLS:56, mecn:14, Hlw:0, Huw:0, NL:0, NU:0, NT:0, OH:0, SH:0,                                    // cnvLS w canvas locked e selected | head lock w | head unlock w
            msgH:0, adds:0, upds:0, skipwf: false,
            CNV:null, CNJ:null, BDN:null, oBDN:null, BDJ:null, MBDJ:null, OBDJ:null, HBDJ:null, maskj:null, ccMm:0,
            IMD:null, OMD:null, FMD:null, TGJ:null,
            optr:0, vptr:0, OVR:-1, SEL:-1, bOVR:-1,
            Hy:0, asHy:0, displ:1, lkEdit:0, unlkEdit:0, olEdit:0,                                              
            EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove', oldmouseup:"", NMM:0, Cur:0, 
            dwnBD:0, noWH:0,
            Hnr:0, hiddfcode:[], PXi:0, PYi:0,   
            First:1, rowH: 0, hCross: 0
          }             
            
for(var i in props) this[i]=(typeof z[i]=='undefined')?props[i]:z[i];
this.Yi=32;            // row height
}

this.Yi=32;
this.Yt=0;
this.AR=[];
this.mCK=[];
this.JStore={};
this.Header=[]; 
this.Abody=[];
this.ROWBKCOL=["#FFFFFF","#FAFAFA","#FEFF9E"];
this.ROWBDCOL="#EDEDED";
this.ROWCOLTXT=moColors.text;
this.arrACTION={};


this.arrCHK=[];         // locked by checkbox 
 

this.Ypos=[];
this.rifY=[];
this.aRange=[];
this.aPRange=[];
this.aFilter=[];

this.IM=moGlb.baseUrl+"/public/msim_images/default/";
this.Curs=["default","default","col-resize"];

this.menulist=[];


// metodi
this.getHeader=getHeader;
this.setHeader=setHeader;
this.Display=Display;                                          // IN
this.resize=resize;

this.updateChecked=updateChecked;
this.deleteSelected=deleteSelected;
this.saveList=saveList;

this.predraw=predraw;
this.draw0=draw0;
this.draw1=draw1;
this.draw=draw;
this.draw2=draw2;
this.ClearB=ClearB;
this.fMaskLoad=fMaskLoad;

this.Cell=Cell;
this.hCell=hCell;

this.RowCells=RowCells;
this.RCells=RCells;
this.clipRow=clipRow;

this.loadroot=loadroot;
this.retroot=retroot;

this.HnrRifPos=HnrRifPos;
 
this.SeChkDechk=SeChkDechk;

this.Vbarpos=Vbarpos;
this.Obarpos=Obarpos;  
this.scrollO=scrollO;

this.headown=headown; 
this.headmove=headmove;
this.headout=headout;

this.oversel=oversel;
this.overmove=overmove;
this.overMove=overMove;

this.bodymove=bodymove;
this.bodyout=bodyout;
this.bodydown=bodydown;
this.bodyup=bodyup;
this.drawbodyover=drawbodyover;

this.maskmove=maskmove;
this.maskup=maskup;
this.headRitAnimat=headRitAnimat;
this.endAnim=endAnim;

this.fieldlist=fieldlist;

this.acthidden=acthidden;
this.actlock=actlock;

this.updHeader=updHeader;
this.fmenuList=fmenuList;

this.chgOrderUP=chgOrderUP;
this.chgOrderDOWN=chgOrderDOWN;

this.dropAttach=dropAttach;


this.colrs=["#F0F0F0","#A1C24D","#4A6671","#6F8D97","#E2EECA","#E0E0E0","#A0A0A0","#DDDDDD","rgba(161,194,77,0.3)"];
 


//------------------------------------------------------------------------------
// chg Order UP/DOWN

function chgOrderDOWN(){
with(this){
 
if(arrCHK.length==1){
  var v=arrCHK[0], p=rifY[v], a;
  if(p>=(Ypos.length-1)) return; 
  a=Ypos.splice(p+1,1);
  Ypos.splice(p,0,a[0]);
}
 
HnrRifPos(1);
}}


function chgOrderUP(){  
with(this){

if(arrCHK.length==1){
  var v=arrCHK[0], p=rifY[v], a;
  if(p<1) return; 
  a=Ypos.splice(p,1);
  Ypos.splice(p-1,0,a[0]); 
}
 
HnrRifPos(1);
}}




this.rowNumber=function(){ 
with(this){ 
  var i, n=0; for(i in JStore) n++;  return n; 
}};

// IN --------------------------------------------------------------------------


function updateChecked(arr,md){     // arr contiene JStore dei selezionati!    md bypass parent control
with(this){
if(!md) md=0;
var r,fc,g,rt, art;    

for(r=0;r<arr.length;r++) {
    fc=arr[r].f_code;

   if(!md) {
    rt=arr[r]["rtree"];
    if(rt) art = rt.split("|");   
    if(fc==f_code ||  art.indexOf(f_code)>-1) { moGlb.alert.show(moGlb.langTranslate("You can't select yourself! One of the selected elements has been rejected")); continue; }
   } 
    
        // merge
   if(typeof(rifY[fc])=="undefined") {
    JStore[fc]={};
    for(g in arr[r]) {JStore[fc][g]=arr[r][g];}   
    Ypos.push({fcode:fc} );      
   }
    
}   
   
HnrRifPos();
}}

this.refresh = function () {this.HnrRifPos();};


function Display(i){
with(this){if(i==displ) return;
if(i) {var nb="block";displ=1;}
else {var nb="none";displ=0;}
j.style.display=nb;
}}



function resize(l0,t0,w0,h0,i){ 
  with(this){ 

  if(typeof jj == "undefined" || !jj) {J=mf$(i);
           jj=moGlb.createElement("id"+name,"div",J);
           jj.style.overflow="hidden";   
   
  if(First) loadroot();
  draw0();     
              
      // scrollbars   
      eval("Vscr"+name+"=new OVscroll('Vscr"+name+"',{ displ:0, L:-100, T:0, H:h0, HH:1, Fnc:'"+name+".Vbarpos', pid:'"+jj.id+"' }); VW=Vscr"+name+".W;"); 
      eval("Oscr"+name+"=new OVscroll('Oscr"+name+"',{ displ:0, OV:1, L:0, T:0, H:w0, HH:1, Fnc:'"+name+".Obarpos', pid:'"+jj.id+"' });");      
   } 
 
    jj.style.left=l0+"px";
    jj.style.top=t0+"px";
    jj.style.width=w0+"px";
    jj.style.height=h0+"px";
    
    W=w0, H=h0;
    
    var p=moGlb.getPosition(jj);   
    zL=p.l, zT=p.t;
    
    if(Hlw) predraw();  
    addStp=0; 
    
    if(!First) {sedrw=2;draw();}   
}}


  
//--------------------------------------------------------------------------------------------------------------------------------------- 
// LOAD records

function loadroot(){     
  with(this){    
          
    var ja=JSON.stringify({Level:0, Tab:[Ttable, ftype], f_code:f_code, module_name: crud, skipwf: skipwf, hiddenCross: hCross});
    Ajax.sendAjaxPost( moGlb.baseUrl+"/treegrid/jax-read-root-chk"+(history?"-history":""), "param="+ja , this.name+".retroot" );
    
    finfo(1);
}} //


// tipo di root e personal setting
//  field
function retroot(a){                                // leggo bookmark e tutte le righe 
  with(this){  
 
  // moGlb.alert.show(name+"\n\n"+a)
 
  finfo(-1);

  var ar=JSON.parse(a), NT=0, nar=(ar.data.length)-1,fcd,r,n; 
  if(moGlb.isset(ar.message)) moGlb.alert.show(moGlb.langTranslate(ar.message));

  moMenu.IM=IM; 
  setHeader(ar.bookmark, ar.bookmark_id, ar.bookmark_name); 
   
  JStore={}; Ypos=[]; hiddfcode=[];  
  
  if(nar>0) {   //  JStore e Ypos  rifY[fc]
    var ahe=[], rhe=[], ng=ar.data[0].length, g;
    
    for(r=0;r<ng;r++) {ahe[r]=ar.data[0][r];rhe[ar.data[0][r]]=r;}           // ahe[rhe] = indice      rhe[indice] = r      posiz indice
    history=parseInt(history);  
      
      for(r=1;r<=nar;r++){
          fcd=parseInt(ar.data[r][rhe.f_code])+((history)?("_"+r):""); 
          JStore[fcd]={}; 
          for(g=0;g<ng;g++) {JStore[fcd][ahe[g]]=ar.data[r][g];}  
          
          if(typeof(JStore[fcd].hiddenCross)=="undefined") JStore[fcd]["hiddenCross"]=0;         // hiddenCross   
                 
          if(!JStore[fcd].hiddenCross) Ypos.push({fcode:fcd});
          else hiddfcode.push(fcd);          
      }  
  }

HnrRifPos();
eval(crud).oncompletion();
}} //





function setHeader(ab, bi, bn){   // bookmark,  bookmark_name,  bookmark_id
with(this){
    
   if(!bn) bn="";
   if(!bi) bi="";
   bookmark_name=bn, bookmark_id=bi; 

   NT=0;Header=[];AR=[];Huw=0;

  // carico i fields e costruisco i menu per head 
  // calcolo Header
  var prm=["fieldb","label","type","lock","hidd","wdth","filter","range","prange","script","visibility","x1","x2"],NT=0, NL=0, NU=0, sw=0, r, fb, c, fz, ki, fld;  
  aRange=[], aPRange=[], arhdn=[], arlck=[], aFilter=[];                                                                                       
                                                                                                             
  if(ab) {for(r=0;r<ab.length;r++) {Header[r]={}; 

    fz=ab[r][0];AR[fz]=[]; // salvo in AR   filter  range  prange  (del campo fieldb)
    for(ki=6;ki<9;ki++) AR[fz][prm[ki]]=ab[r][ki];     // es. AR[fieldb][filter]=array bookmark
  
    for(var i=0;i<prm.length;i++) Header[r][prm[i]]=(i<6)?ab[r][i]:"";                           // trasformo in associativo      se<6 

    // Range
    var ar7=ab[r][7].length;                                                                                                                        
    if(ar7) {fz=ab[r][0];aRange[fz]=[]; 
              for(var n=0;n<ar7;n+=2) aRange[fz].push( {exp:ab[r][7][n], val:ab[r][7][n+1]} );
              }
  
    // aPRange
    var ar8=ab[r][8].length;                                                                                                                        
    if(ar8) {aPRange.push( {field:ab[r][0], condz:[], typ:ab[r][2]} );c=aPRange.length-1;
              for(var n=0;n<ar8;n+=2) aPRange[c].condz.push( {exp:ab[r][8][n], val:ab[r][8][n+1]} );
              }
    
    // script
    ar9=ab[r][9]; 
    Header[r][prm[9]]=(ar9)?ab[r][9]:"";
       
    // visibility   
    ar10=ab[r][10]; 
    if(typeof(ar10)=="undefined") ar10=-1;
    Header[r][prm[10]]=ar10;   
       
    // select
    if(Header[r].type==4) {fb=Header[r].fieldb;if(typeof Filterby == "undefined") Filterby = [];Filterby[fb]=[]; aFilter[fb]=[]; // array('0'=>'Nessuna','1'=>'Normale','2'=>'Urgente','3'=>'Immediata')   
                          for(k in ab[r][6]) {
                              aFilter[fb][k]=ab[r][6][k];
                              Filterby[fb].push({Txt:ab[r][6][k], Type:3, Value:k, Status:0, Group:3, Icon:['','checked'], Fnc:""});      
                          }                          
                        }
    
    with(Header[r]) {if(!(moGlb.user.level&visibility) || !hidd) {arhdn.push(r);arlck.push(lock);hidd=1;}

                        NT++;x1=sw;sw+=wdth;x2=sw;                                   
                        if(lock) NL++,Hlw=sw; else Huw+=wdth,NU++; 
                    }              
  }}
   
  var a,ii,k=0,lk,iii;
  fmenuList();
  
 
  for(r=0;r<arhdn.length;r++) {ii=arhdn[r];lk=parseInt(arlck[r]);
     
    //alert(ii+" | "+k+" | "+lk+" | "+(ii-k))
  
    iii=ii;if(lk) iii=ii-k;
  
    with(Header[iii]){hidd=0;
                        menulist[ii].Status=0; 
                     
           if(lk) {lock=0;NL--;
                    a=Header.splice(iii,1);
                    Header.splice(NL,0,a[0]);
                    k++;                  
  }}}

  updHeader(1);
  predraw(); 
}}



function getHeader(){
with(this){  
  var abk=[], r, a, k;
  for(r=0;r<Header.length;r++){ 
     a=Header[r];
     abk.push( [ a.fieldb,
                 a.label,
                 a.type,
                 a.lock,
                 a.hidd,
                 a.wdth,
                 AR[a.fieldb]["filter"],
                 AR[a.fieldb]["range"],
                 AR[a.fieldb]["prange"],
                 a.script
                ]);
  }
return [abk, bookmark_id, bookmark_name];
}}
 



// DRAW -------------------------------------------------------------------------------------------------------------------------- 

function predraw(){
with(this) {
  try{
    eval("Vscr"+name);
  } catch(ex) {return;}


 // resize srollb
  var sl=W-VW-mgR, st=mgT+HeadH+msgH, 
      ol=mgL+cnvLS+Hlw, ot=H-VW-mgB; 
      OH=W-cnvLS-Hlw-mgL-mgR;
      SH=H-HeadH-VW-mgB-mgT-msgH;
      
  eval("Vscr=Vscr"+name);
  eval("Oscr=Oscr"+name);
    
  if(SH<64 || W<80) Vscr.Display(0);else {if(!Vscr.displ) Vscr.Display(1);Vscr.Resize( {L:sl, T:st, H:SH, HH:Hnr*Yi} );}
  if(OH<64) Oscr.Display(0);else {if(!Oscr.displ) Oscr.Display(1);Oscr.Resize( {L:ol, T:ot, H:OH, HH:Huw} );}

 // resize infogrid 
 mf$("infogrid"+name).style.width=ol+"px";
 
 draw1();
}}                                                                                                                   


function draw0(){
with(this){ 

  if(isTouch) EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove'; 

    var hbody=H-HeadH-VW-mgT-mgB-msgH,onm="",z,s="",zz,c2,tc,lc;
    
  z="z-index:", zz=0; lc=20+mgL, tc=(4+mgT)-3;


  if(isTouch) {  
     onm=EVTDOWN+"='"+name+".bodydown(event)' "+EVTMOVE+"='"+name+".bodymove(event)' "+EVTUP+"='"+name+".bodyup(event)' ";
  
  } else {     
    onm=" onmouseover='WHEELOBJ=\"Vscr"+name+"\"' onmouseout='"+name+".bodyout(event)' "+EVTMOVE+"='"+name+".overMove(event)' "+EVTDOWN+"='"+name+".bodydown(event)' ";
   
    TGJ=moGlb.createElement("maskTreegridDragScroll","div",document.body);
    TGJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(moGlb.zloader+500));      
  }       
                 
   
   // left head
  s="<canvas id='lcksel"+name+"' style='position:absolute;left:"+mgL+"px;top:"+mgT+"px;"+z+zz+";' width='"+cnvLS+"' height='"+HeadH+"'></canvas>"+
  
    "<div id='checklcksel"+name+"' style='position:absolute;left:"+lc+"px;width:34px;top:"+tc+"px;"+
    z+(zz+1)+";cursor:pointer;color:"+colrs[5]+";font:16px FlaticonsStroke;text-align:center;'"+
    " onclick='"+name+".SeChkDechk()'>"+String.fromCharCode(58826)+"</div>"+ 
 
    // head      
    "<canvas id='headlu"+name+"' style='position:absolute;left:"+(mgL+cnvLS)+"px;top:"+mgT+"px' width='"+Hlw+"' height='"+HeadH+"' ></canvas>"+       
    "<div id='idmsgrev"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px;width:10px;height:"+msgH+";overflow:hidden;"+
    "background-color:#f0f0f0;cursor:pointer;'></div>"+
          
    "<canvas id='bodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px' width='10' height='"+hbody+"'></canvas>"+ 
    "<div id='overbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px;width:10px;height:"+hbody+";overflow:hidden;'></div>"+
              
    "<div id='maskbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px;width:10px;height:"+hbody+";overflow:hidden;background-color:#ff0;opacity:0.0;' "+onm+"></div>"+
 
     // header
    "<div style='position:absolute;left:"+mgR+"px;top:"+mgT+"px;width:"+VW+"px;height:"+HeadH+"px;cursor:pointer;' onclick='"+name+".fieldlist(event)'>"+
    "<div style='margin:2px 0 0 2px;'>"+moCnvUtils.IconFont(58445,16,colrs[2])+"</div>"+
    "</div>"+

    // footer info
    "<div id='infogrid"+name+"' style='position:absolute;left:"+mgL+"px;bottom:"+mgB+"px;height:"+VW+"px;width:"+cnvLS+"px;overflow:hidden;'>"+
    "<div id='infotxt"+name+"' style='font:13px opensansR;margin:2px 4px 0 8px;color:#000;'>No record found.</div></div";
           
  jj.innerHTML=s; 
  CNJ=mf$("headlu"+name), CNV=CNJ.getContext("2d");
  BDJ=mf$("bodylu"+name), BDN=BDJ.getContext("2d");
  MBDJ=mf$("maskbodylu"+name);
  OBDJ=mf$("overbodylu"+name);
  HBDJ=mf$("idmsgrev"+name);
 
  eval("CNJ."+EVTDOWN+"=function(e){"+name+".headown(e); }");
  if(!isTouch){
    eval("CNJ."+EVTMOVE+"=function(e){"+name+".headmove(e); }");
    eval("CNJ.onmouseout=function(e){"+name+".headout(e); }"); 
    
  } else {
    eval("CNJ."+EVTMOVE+"=function(e){"+name+".maskmove(e); }");
    eval("CNJ."+EVTUP+"=function(e){"+name+".maskup(e); }");   
  } 
 
 
  // drag drop 
  MBDJ.addEventListener('drop', function(e){ 
  
      moEvtMng.cancelEvent(e);
        
      try{
        var anmbt=name.split("_");
        var vbt=mf$("btn_"+anmbt[1]+"_doc_add_cnv").style.display;
      }catch(e){ return false; }
      
      if(name.indexOf("_doc_tg_tg")!=-1) {                                             // mdl_wo_doc_tg_tg
        moDropAttach.handleDrop(e,name+".dropAttach");
      } 
 
      
    }, false);
    
  MBDJ.addEventListener('dragover',function(e){moEvtMng.cancelEvent(e);},false); 
}}


function dropAttach(lst){
with(this){
  //alert(JSON.stringify(lst) );
  if(lst.length) updateChecked(lst,0);
}}





// caching per il ridisegno dei canvas head e body
function draw(tt){      
with(this){
  SEDRW!=sedrw;      
  if(!tt) {if(!nodrw) {tt=1;nodrw=1;}
  else return;} 
  if(tt>1) {if(SEDRW&1) draw1();if(SEDRW&2) draw2(); nodrw=0;sedrw=3; return;}
  tt++;
   
setTimeout(name+".draw("+tt+")",(Stp+addStp));
}}





function draw1(){
with(this){           

  if(W<=moGlb.moduleMinWH) {Display(0);return;} // dimensione minima
  Display(1);
 
  // cell locked-selected
  var cnv=mf$("lcksel"+name).getContext("2d"), c2=(cnvLS-mecn)/2;
  Cell(cnv,0,c2,0);
  Cell(cnv,c2,cnvLS,0); 

  var nk=arrCHK.length; 
  ck=(nk<1)?0:1; 
  mf$("checklcksel"+name).style.color=colrs[ck?2:5];              
 
  // celle lock & unlock 
  CNJ.width=Hlw+OH;      
  for(var r=0;r<NT;r++) {if(Header[r].hidd) hCell(r);}
}}




function draw2(no){   
  with(this){    if(!no) no=0;   
 
  // bodywheel 
  if(!no){ var uw=cnvLS+Hlw+OH;
    with(BDJ) width=uw, height=SH;
    MBDJ.style.width=uw+"px"; MBDJ.style.height=SH+"px";
    OBDJ.style.width=uw+"px"; OBDJ.style.height=SH+"px";
    HBDJ.style.width=W+"px"; 
  }

  var nar=Ypos.length;
   
  Yt=vptr%Yi, Yt*=-1;                                           // Yt delta posizione iniziale
  var n0=(SH-Yt); nr=parseInt(n0/Yi), nr+=(n0%Yi)?1:0;          // nr = numero righe visualizzabili nel canvas top = Yt
  
  //Ypos 
  var r,p0,p1, Yx=0, y=vptr+Yt; 
  for(r=0;r<(nar-1);r++) { 
    p0=Ypos[r].posY, p1=Ypos[r+1].posY;  
    Yx=r;
    if(vptr>=p0 && vptr<p1) break; 
    Yx=nar-1;
  }

  Abody=[];  
  var Y=Yt, x=Yx, or=0, cy=y/Yi, c, nbk=0, pc,ps,cr,bx,p1=0, fx=x;                              
  ClearB(0);
  
// draw nr rows from Yt
  for(r=0;r<nr;r++){
 
    if(typeof(Ypos[x])=="undefined") break;
    p0=Ypos[x].posY;  
    c=cy&1;  
    RowCells(Ypos[x],Y,c);                                 
    x++;               

  y+=Yi;Y+=Yi;cy++;
  } // end for
  
  ClearB(1);    
  
  // script  treegrid light if exist   mdl_wo_asset_tg_tg_script  -> txt_fld_wo_edit_assets
  var scrpt=name+"_script";
  if(moLibrary.exists(scrpt)) eval(moLibrary.getSource(scrpt));  
}}

 


function RowCells(Ypx,y,c){                    // Ypx = Ypos { }
  with(this)  {
 
  var bdw=cnvLS+Hlw+OH, k, bkg,brd,g;    // falsariga 
  // sfondo riga 
  with(BDN) {
  
  k=1; 
  bkg=ROWBKCOL[c];   
  fillStyle=bkg;
  fillRect(0,y+k,bdw,Yi-2*k);
  fillStyle=ROWBDCOL;
  fillRect(0,y+Yi-1,bdw,1);
  
  // evidenzia
  for(r=0;r<NT;r++) with(Header[r]) if(hidd && r==SEL) {save();g=clipRow(x1,x2,y,lock);
  if(!g) break;
  if(r==SEL) {fillStyle=fillStyle=colrs[8];                                                         //  #@#     moColors.bgOver;
   fillRect(g[0],y,g[1]-g[0],Yi);}                            
  restore();break;}
                                                                                                                
  // Rcelle lock & unlock 
  var fc=Ypx.fcode, r;                                                                                  
  } 

  // chech & lock da LStore
  var afc=JStore[fc]; 
     
  if(!afc) {moGlb.alert.show(moGlb.langTranslate("no data record found!")+" "+fc);return;}
   
  var dd=29, vv=y+parseInt((Yi-16)/2), slt=0,flti;
   
  if(arrCHK.indexOf(fc)>-1) slt=1;
  
  flti=slt?58826:58783;
  moCnvUtils.cnvIconFont(BDN,flti,dd,vv,16,colrs[2]);

  Abody.push({y:y, f:fc}); 
  
  // colore testo riga in aPRange
  var f,v,nv,i,cz,vz,tz,virg, colr=ROWCOLTXT, stz;  
  for(r=0;r<NT;r++) if(Header[r].hidd) RCells(afc, r, y, Ypx, colr);                      // campi riga 
}}




 

function RCells(rig, r, y, Ypx, colr){       //  progre LStore,  r per Header[r]  fieldb, x1,x2, ...
  with(this)  {

  var X1,X2,x21,c1,c2, lr=OH+Hlw, t="", f="13px opensansR", d=0, nh, v;
  

  with(Header[r]) with(BDN){
      
  X1=x1, X2=x2;
  if(!lock) {X1-=optr, X2-=optr;if(X2<=Hlw || X1>=lr) return;}

 save();
  x21=X2-X1-padf*2, X1+=cnvLS+padf;
  t=rig[fieldb], v=t;
 
  if(typeof(v)=="undefined") return; 
  
  // clip  
   clipRow(x1,x2,y,lock);
                                                                                          
    font=f;textBaseline="alphabetic";textAlign="left";     
    
    var d=0, yy;  
 
 
      var cz,vz,tz,virg, ccl=colr, virg=(!type)?"'":"", stz = {"{val}": virg+v+virg};         
        if(aRange[fieldb]){
        for(var g=0;g<aRange[fieldb].length;g++) { 
        cz=aRange[fieldb][g].exp, vz=aRange[fieldb][g].val; 
        cz=cz.multiReplace(stz); 
        eval("if("+cz+") ccl='"+vz+"';");  
        if(ccl!=colr) break;}
        }
 
        var ed=parseInt(rig["f_editability"]);
        if(!ed) ccl="#A0A0A0";    // Colors.colorTone(ccl,0.6);
 
        colr=ccl;
 
 
    if(script && moLibrary.exists(script)) {   //            <------------------------------------------   se esiste delegate script
 
       var TIPtext="", ACTIONclick="";
       
       var xfield=X1+d, yfield=y, vfield=v, f_cod=Ypx.fcode;  // colr colore
       
       //if(!v || v == "0") {} else { eval(moGlb.delegates[script]); }
       fillStyle = colr; 
 
        eval(moLibrary.getSource(script));
       
       if(ACTIONclick=='viewDocument' || ACTIONclick=='viewSvg') {arrACTION[f_cod]=v;}
        
        
    } else {
        if(type==4) { try { t=v; if(moGlb.isset(aFilter[fieldb][v])) t=moGlb.langTranslate(aFilter[fieldb][v]); }catch(e){}  }
        else if(type==2) eval(moLibrary.getSource('bool'));
       if(moCnvUtils.measureText(t,f)>x21) t=moGlb.reduceText(t,f,x21);
 
        fillStyle=colr;                                    
        fillText(t,X1+d,y+8+(Yi-7)/2);     
    }
 
 restore(); 
  }
 
}}





function clipRow(X1,X2,y,lock){ 
  with(this) with(BDN) {
  var lr=OH+Hlw;
  if(!lock) {X1-=optr, X2-=optr; 
               if(X2<=Hlw || X1>=lr) return false;
               if(X1<Hlw) X1=Hlw;
               if(X2>lr) X2=lr;}
 
   X1+=cnvLS, X2+=cnvLS;
   beginPath();rect(X1,y,X2-X1,Yi);clip();closePath();
   
   return [X1,X2];
}}                                                                   




// 0 cancella   1 disegna separazione lock/unlock
function ClearB(m){
  with(this) with(BDN) {
    if(!m) clearRect(0,0,cnvLS+Hlw+OH,SH);
    else{fillStyle=colrs[6];
          fillRect(cnvLS+Hlw-1,0,1,SH);}
}}



function fMaskLoad(x,y){
  with(this){
  var mj=moGlb.createElement("maskmodule"+name,"div",jj);
  
  if(!NOEVT){
    mj.style.left=0;
    mj.style.top=0;
    mj.style.width="100%";
    mj.style.height="100%";
    mj.style.zIndex=10;
    mj.style.display="block";
    
    mj.onmousedown=function(e){moEvtMng.cancelEvent(e);return false;}
    mj.onmouseover=function(e){moEvtMng.cancelEvent(e);return false;}
    
    mj.innerHTML="<img src='"+IM+"ico_loading.gif' style='position:absolute;left:"+(x-17)+"px;top:"+y+"px;width:16px;height:16px;' />";     
  } else {
    if(NOEVT>32) {mj.innerHTML="";mj.style.display="none";NOEVT=0;return;} 
  }
    NOEVT++;
setTimeout(name+".fMaskLoad()",200);
}}









function Cell(cnv,x1,x2,s){
  with(this) with(cnv) {
      
      fillStyle=colrs[s]; fillRect(x1+1,0,x2-x1,HeadH);
       
      lineWidth=1; beginPath(); strokeStyle=colrs[6];  
      moveTo(x1+1,HeadH-0.5); lineTo(x2-0.5,HeadH-0.5); lineTo(x2-0.5,1);  
      stroke(); closePath();
}}   


function hCell(i){
  with(this) with(CNV) with(Header[i]){
 
  var X1=x1, X2=x2, s=0, lr=OH+Hlw;
  if(i>=NL) X1=x1-optr, X2=x2-optr;
  if(X1>lr) return;
  if(X2>=lr) X2=lr;
 
  if(i==OVR) s=7;
  if(i==SEL) s=4;
  
  Cell(CNV,X1,X2,s);
  

  // testo
  var f="bold 13px opensansR", d=4;
 
  
  var x21=X2-X1-(d+4), t=moGlb.langTranslate(label);     
  font=f; 
  textBaseline="alphabetic"; 
  textAlign="left"; 
  fillStyle=colrs[2];  
        
  if(moCnvUtils.measureText(t,f)>x21) t=moGlb.reduceText(t,f,x21);                        
  fillText(t,X1+d,8.5+(HeadH-8)/2);
             
  if(i==(NL-1)) {beginPath();rect(Hlw,0,OH,HeadH);clip();}          
}} 





// EVENTI --------------------------------------------------

function Vbarpos(i){
  with(this){
  vptr=i;bOVR=-1;  
  sedrw=2;draw();  
  drawbodyover(-1);
}}


function Obarpos(i){
  with(this){
  optr=i; 
  sedrw=2;draw(1);
}}



function bodydown(e){  moEvtMng.cancelEvent(e);
with(this){
 
  mCK=[]; noWH=0;
  if(dwnBD) return false;
  
  moMenu.Close();
  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);
  
  eval("Vscr=Vscr"+name);
  eval("Oscr=Oscr"+name); 
  mCK[0]={ iy:P.y, ix:P.x, By:Vscr.ptr, Bx:Oscr.ptr};
        
  dwnBD=1;
 
  if(!isTouch){
   TGJ.style.display="block";
   eval("TGJ.onmousemove=function(e){"+name+".bodymove(e); }");
   eval("TGJ.onmouseup=function(e){"+name+".bodyup(e); }");  
  } 
  return false;
}}

 
function bodymove(e){ moEvtMng.cancelEvent(e);
with(this){
 
  var i,dx,dy, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), ex=P.x, ey=P.y;

  if(!mCK.length) return false;

  // drag scrollbars
  if(dwnBD==1) {
    if(ex<(zL+Hlw)) noWH=0;
    else {
      dx=Math.abs(ex-mCK[0].ix);
      dy=Math.abs(ey-mCK[0].iy);  
      if(dx>dy) noWH=1; else noWH=0;  // 0=vertical   1=orizzontal
    }
  }
  
  dwnBD++;
  if(dwnBD>2 && dwnBD&1){

    if(noWH){
      eval("Oscr=Oscr"+name);
      i=mCK[0].Bx+(mCK[0].ix-ex);
      Oscr.PosPtr(i);
    }else{
      eval("Vscr=Vscr"+name);
      i=mCK[0].By+(mCK[0].iy-ey);
      Vscr.PosPtr(i);
    }
  }
  return false;
}}

 

function overMove(e){ moEvtMng.cancelEvent(e);
with(this){

  var Bt=zT+mgT+HeadH+msgH, Bl=zL+mgL, 
      P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e),
      p, bovr=-1;
 
    for(var r=0;r<Abody.length;r++) {
      p=Bt+Abody[r].y;
      if(P.y>=p && P.y<(p+Yi)) { bovr=Abody[r].f; break; }
    }    
    
    // over rows
    if(bovr!=bOVR) { bOVR=bovr; drawbodyover(p-Bt); } 
  return false;           
}}





function bodyup(e){  moEvtMng.cancelEvent(e);
with(this){  

  if(!isTouch) TGJ.style.display="none";
 
  // if click
  if(!dwnBD || dwnBD>3) { dwnBD=0; return false; } 
 
  var c2=(cnvLS-mecn)/2, Bli=zL+mgL+c2, Blf=Bli+c2, Bt=zT+mgT+HeadH+msgH,
      P={ x:mCK[0].ix, y:mCK[0].iy }, 
      Px=P.x-zL-mgL, Py=P.y-Bt,
      fc=0, md=0, p;
 
  mCK=[]; dwnBD=0;
 
    
    
    // edit o check
    for(var r=0;r<Abody.length;r++) {
    p=Bt+Abody[r].y;
    if(P.y>=p && P.y<(p+Yi)){fc=Abody[r].f;md=1;  
    if(P.x>Bli && P.x<Blf) md=2;
    break;}
    }
    
    if(md==1 && moGlb.isset(arrACTION[fc])) moGlb.openDoc(arrACTION[fc]);

    // check
    if(md==2) {  if(arrCHK.indexOf(fc)>-1) {for(var r=0;r<arrCHK.length;r++) if(arrCHK[r]==fc) {arrCHK.splice(r,1);}
                } else arrCHK.push(fc);
            sedrw=3;draw(); 
    }
  
  return false;     
}}


 


function bodyout(e){
  with(this){WHEELOBJ="";  
             if(bOVR!=-1) {bOVR=-1;}
  OBDJ.innerHTML="";                  
}}



 
function drawbodyover(p){
  with(this){ 
  
  if(bOVR<0 || p==-1) OBDJ.innerHTML=""; 
  else {
  var o=10, op="opacity:."+o+";filter:alpha(opacity="+o+");-moz-opacity:."+o+";", c="#404040";  
  OBDJ.innerHTML="<div style='position:absolute;left:0;top:"+(p)+"px;width:100%;height:"+(Yi-1)+"px;background-color:"+c+";"+op+"'></div>";
  }

}}




function headown(e){
  with(this){   
  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s=oversel(P.x), ovr=s[0]; 
  Cur=(s[1]==2)?1:0;  
       
  if(OVR!=ovr) OVR=ovr;
  PXi=P.x, PYi=P.y;     
 
 if(!NMM){NMM=1;ccMm=0;
 
  // creo movimento su maskera
  maskj=moGlb.createElement("maskDrag"+name,"div");
  
  maskj.style.zIndex=moGlb.zdrag+1;
  maskj.style.left=0;
  maskj.style.top=0;
  maskj.style.width='100%'; 
  maskj.style.height='100%';     // , backgroundColor="#ff9"

  eval("maskj."+EVTMOVE+"=function(e){ maskmove(e); }; oldmouseup=document."+EVTUP+"; document."+EVTUP+"=function(e){ maskup(e); };  ");
  } else NMM=0;
  
  
  if(isTouch) moEvtMng.cancelEvent(e);
  else return false;   
}}






function maskmove(e){
  with(this){

  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s=overmove(P.x), stri;        // s=[over, mid, X1, X2, pri, ins, scroll]           scroll 0 -10 -20
  
  if(!ccMm) {
 
  // sposta else  Drag
  if(!Cur) stri="<img id='idtmpins' src='"+IM+"insert_field.png' style='position:absolute;display:none;' />"+
                    "<div id='idtmpdrgh' style='position:absolute;width:158px;height:30px;background-color:"+colrs[2]+";opacity:0.9;border-radius:2px;'>"+
                    "<div id='idtmpsimb' style='position:absolute;left:6px;top:5px;' >"+moCnvUtils.IconFont(58827,16,"#D88")+"</div>"+
                    "<div style='position:absolute;left:30px;top:6px;width:120px;height:16px;font:13px opensansR;color:#FFF;overflow:hidden;'>"+
                    moGlb.langTranslate(Header[OVR].label)+"</div></div>";
  else {   
         iext=s[0];if(!s[1]) iext=s[4];
         RX1=Header[iext].x1 + mgL+cnvLS+zL;
         if(iext>=NL) RX1-=optr;  
         stri= "<canvas id='idtmpdrgh' style='position:absolute;left:"+RX1+"px;top:"+(zT+mgT+HeadH)+"px;' width='40' height='"+(SH+1)+"'></canvas>";
       }
   maskj.innerHTML=stri;     
  }
  
  
  ccMm++;
  
  if(!Cur) {   // sposta
  
   mf$("idtmpdrgh").style.left=(P.x+16)+"px";
   mf$("idtmpdrgh").style.top=(P.y+16)+"px";
  
  var li=0,ti=0,di="none", srins=moCnvUtils.IconFont(58827,16,"#D88");
  if(s[5]){ di="block", li=(s[1])?s[3]:s[2], ti=zT+mgT-4;
            srins=moCnvUtils.IconFont(58826,16,"#CED"); }
  
  // if scroll left:-10 right;-20
  if(s[6]<-5) {
  if(s[6]==-10) { srins=moCnvUtils.IconFont(58774,16,"#FFFFFF"); if(!sescr) { sescr=1; scrollO(-1); } }
  if(s[6]==-20) { srins=moCnvUtils.IconFont(58775,16,"#FFFFFF"); if(!sescr) { sescr=1; scrollO(1); } }  
  } else sescr=0;
  
  mf$("idtmpins").style.display=di;
  mf$("idtmpins").style.left=(li-5)+"px";
  mf$("idtmpins").style.top=ti+"px";
  
  mf$("idtmpsimb").innerHTML=srins;
  
  
  } else {   // resize
  
   szW=P.x-RX1;if(szW<32) szW=32;
 
   var cj=mf$("idtmpdrgh"), cnn=cj.getContext("2d");
   
   cj.width=szW;
   with(cnn){
   clearRect(0,0,szW,SH);
   fillStyle=fillStyle="rgba(224,224,224,0.5)";                      // #@# moColors.maskMoveFill; 
   fillRect(0,0,szW,SH);
   lineWidth=1;
   strokeStyle="#808080";                                            // #@# moColors.maskMoveStroke;  
   beginPath();
   moveTo(0.5,0);
   lineTo(0.5,SH+0.5);
   lineTo(szW-0.5,SH+0.5)
   lineTo(szW-0.5,0);
   closePath();
   stroke();
   }
  
  }
  
 
  moEvtMng.cancelEvent(e);
  return false;
}}





function SeChkDechk(){      
  with(this){

  if(arrCHK.length) arrCHK=[];
  else {
    for(var r=0;r<Ypos.length;r++) arrCHK.push(Ypos[r].fcode); 
  }
  sedrw=3;draw();
}}





function maskup(e){
  with(this){

  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s;

  if(P.x==PXi && P.y==PYi) {                  // click
         

  } else {                                    // drag   
     
     if(!Cur){   // ordine field    
              s=overmove(P.x);
              if(s[5]) {
              var i0=OVR, i1=s[0]+s[1];lk=Header[s[0]].lock, dse=0;
              if(i1<=SEL && OVR>SEL) dse=1; 
              if(i1>SEL && OVR<=SEL) dse=-1;
              var v=Header.splice(i0,1);
              if(i1>OVR) i1--;

              Header.splice(i1,0,v[0]);Header[i1].lock=lk;
              
              if(OVR==SEL) SEL=i1; else SEL+=dse;  // mantengo selezioanto      
              fmenuList();
              updHeader(); 
                
              } else {            // aimazione ritorno 
                      var iij=mf$("idtmpdrgh");
                      if(!iij) {endAnim();}
                      else {
                       
                       var x=parseInt(iij.style.left), y=parseInt(iij.style.top);
                       
                       with(Header[OVR]){var xa=x1+mgL+cnvLS+zL, ya=zT+mgT;}
                       var nxa=Math.abs((xa-x)/50), nya=Math.abs((ya-y)/50), nn=parseInt((nxa+nya)/2);if(nn>8) nn=8;
                       if(nn<2) nn=2;var dx=parseInt((xa-x)/nn), dy=parseInt((ya-y)/nn);
                       
                       eval("maskj."+EVTMOVE+"='';");
                       headRitAnimat(x,y,dx,dy,0,nn);
                      }
                  if(isTouch) moEvtMng.cancelEvent(e);
                  else return false;  
                }  
     
     } else {   // resize field
     
     with(Header[iext]){wdth=szW;}  
     updHeader();
     
     }
  
      OVR=-1;sedrw=3;draw(); 
      NMM=0;
      }
      
  var pj=maskj.parentNode;pj.removeChild(maskj);
  eval("document."+EVTUP+"=oldmouseup");       
  sescr=0;  
    
  if(isTouch) moEvtMng.cancelEvent(e);
  else return false;  
}}



function headRitAnimat(x,y,dx,dy,t,nt){
  with(this){

  t++;
  var nx=x+dx*t, ny=y+dy*t;
  mf$("idtmpdrgh").style.left=nx+"px";
  mf$("idtmpdrgh").style.top=ny+"px";

  if(t>nt) {endAnim();return false;}

setTimeout(name+".headRitAnimat("+x+","+y+","+dx+","+dy+","+t+","+nt+")",50);
}}


function endAnim(){
  with(this){
  
  var pj=maskj.parentNode;pj.removeChild(maskj);
  eval("document."+EVTUP+"=oldmouseup");       
  sescr=0;OVR=-1;sedrw=3;draw();
  NMM=0;        
  return false; 
}}



function scrollO(d,t){if(!t) t=0;  
  with(this){if(!sescr) return;
 
  t++;
  if(t>3){
  optr+=d*16; 
  if(optr<0) optr=0;
  if(optr>(Huw-OH) ) optr=Huw-OH;

  eval("Oscr=Oscr"+name); 

  Oscr.Resize( {ptr:optr} ); 
  if(!optr || optr==(Huw-OH) ) return;
  }


setTimeout(name+".scrollO("+d+","+t+")",60);
}}



function headmove(e){    
  with(this){if(NMM) return;
   
   var P=moEvtMng.getMouseXY(e), s=oversel(P.x), ovr=s[0], cur=Curs[s[1]], oC=Cur;
   Cur=(s[1]==2)?1:0;
 
   if(OVR!=ovr || oC!=Cur) {OVR=ovr;sedrw=3;draw();}
   CNJ.style.cursor=cur;
   
   return false;  
}}
                                       

function oversel(x){    // restituisce  [over, cur]   cur: 0:normal  1:pointer  2:col-resize
  with(this){   
   var dx=mgL+zL+cnvLS, lr=OH+Hlw, ovr=-1, X1=0,X2=0, cur=0, pri=-1, mid=0, x21=0;    
   for(var i=0;i<NT;i++)  with(Header[i]){if(!hidd) continue;
   if(i>=NL) {X1=x1-optr, X2=x2-optr;if(X2<=Hlw) continue;
               if(X1<Hlw) X1=Hlw;if(X1>=lr) break;
               if(X2>lr) X2=lr;}
   else X1=x1, X2=x2;        
   X1+=dx, X2+=dx,  x21=(X2-X1)/2;
   if(x>=X1 && x<X2) {ovr=i;mid=(x<(X1+x21))?0:1;break;}  
   pri=i;  
   }
   if(x>(X2-24)) cur=1;
   if(x>(X2-6) || x<(X1+6)) cur=2;
   
   if( (pri<0 && !mid)  ) cur=0;  
   
   return [ovr,cur,pri,mid];   
}}



function overmove(x){    // restituisce  [over, mid, X1, X2, pri, ins, scroll]   mid: 0:left  1:right    pri=field precedente  nxi=field successivo  ins=se inserimento possibile
  with(this){
   var dx=mgL+zL+cnvLS, lr=OH+Hlw, ovr=-1, X1=0,X2=0, mid=0, x21=0, rg=12, pri=-1, nxi=-1, ins=1, PRI=-1, NXI=-1, sc=0; 
   
   if(x>(dx+Hlw) && x<(dx+rg+Hlw)) sc=-10;
   if(x>(dx+lr-rg) && x<(dx+lr)) sc=-20;
      
   for(var i=0;i<NT;i++) with(Header[i]) {if(!hidd) continue;
   if(i>=NL) {X1=x1-optr, X2=x2-optr;if(X2<=Hlw) continue;
               if(X1<Hlw) X1=Hlw;if(X1>=lr) break;if(X2>lr) X2=lr;}
   else X1=x1, X2=x2;        
   X1+=dx, X2+=dx, x21=(X2-X1)/2;
   if(x>=X1 && x<X2) {ovr=i;mid=(x<(X1+x21))?0:1;break;}
   pri=i;}  
    
   // cerco PRI e NXI di OVR;
   var b=0;
   for(i=0;i<NT;i++) with(Header[i]) {if(!hidd) continue; 
   if(b) {NXI=i;break;}
   if(i==OVR) {b=1;continue;}
   PRI=i;}

   if(ovr<0 || ovr==OVR || ((ovr==NXI && ovr!=NL) && !mid) || ((ovr==PRI && ovr!=(NL-1) ) && mid) ) ins=0;
     
   return [ovr,mid,X1,X2,pri,ins,sc];   
}}




function headout(e){
  with(this){   
  if(NMM) return;
  OVR=-1; 
  sedrw=1;draw();
}}


                          







// menu  -----------------------------------------------------------------------
function fieldlist(){
  with(this){

  var l=zL+mgR+4, t=zT+mgT-2, w=VW, h=HeadH; 
  moMenu.Start(name+".menulist",{l:l, t:t, w:w, h:h},2);

}}


 



// Hidden
function acthidden(i){      // v= -1 da Ovr     m=stato 
  with(this){    
    with(Header[i]){hidd=(hidd)?0:1;
                     menulist[i].Status=hidd;
                     if(lock) {actlock(i);return;}
                    }
  updHeader(); 
  sedrw=2;draw();
}}
  


// Lock/unlock
function actlock(i){      // v= -1 da Ovr     m=stato 
  with(this){    
    with(Header[i]){lock=(lock)?0:1;if(!lock) NL--;}
 
    var a=Header.splice(i,1);
    Header.splice(NL,0,a[0]);
 
    fmenuList();
    updHeader(); 
  sedrw=2;draw();
}}



 
 
 
 
 
  
//---------------------------------------------------------------------------------

function updHeader(m){      // aggiorno per hidden e lock 
  with(this){

  NT=0;NU=0;NL=0;Hlw=0;Huw=0;var sw=0; 
  for(var r=0;r<Header.length;r++) with(Header[r]) {NT++;   
    if(hidd) {x1=sw;sw+=wdth;x2=sw;                                   // larghezza header lock e unlock
    if(lock) NL++, Hlw=sw; else Huw+=wdth, NU++;}}
 
 if(m) return;
 
  eval("Oscr=Oscr"+name); 
  Oscr.Resize( {HH:Huw} );
   
  predraw();
}} 

function fmenuList(){ // creo menu list field 
  with(this){

  menulist=[];
  for(i=0;i<Header.length;i++) with(Header[i]){  
  menulist.push( {Txt:label, Type:2, Status:hidd, Icon:[ '', 'FlaticonsStroke.58786' ], Fnc:name+".acthidden("+i+")"} );
  } 

}}



 
 

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// ENGY
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


 
 
// props
this.Ypos=[];          
this.rifY=[];        
this.aLoad=[];


this.stpclear=1000;
this.ndmax=5;
this.istatus=0;
this.infostri="";
this.oinfostri="-";
this.timeinf=0;

// metodi

this.finfo=finfo;
this.ftimefinfo=ftimefinfo;
 

/*
1   loading     -1 end loading
2   clearing    -2 end clearing
*/


function finfo(v){
  with(this){ 
 
    if(v<0) {if(istatus&(v*-1)) istatus&=(istatus+v);} else istatus|=v;
    var stri="", icl="#a0a0a0", bi="";
 
if(istatus&1) stri+=moGlb.langTranslate("Loading...");

if(istatus&4) icl="a02020";

if(istatus&2) {if(stri) bi="&nbsp;/&nbsp;";stri+="<span style='color:"+icl+";'>"+bi+moGlb.langTranslate("Cleaning up...")+"</span>";}

if(istatus) stri="<img src='"+IM+"ico_loading.gif' height='8px' />&nbsp;"+stri;

infostri=stri; 
if(timeinf<=0) {timeinf=2;ftimefinfo();}

}}



function ftimefinfo(){  
  with(this){timeinf--;    
                                      
   if(infostri!="") {if(oinfostri!=infostri) {if(mf$("infotxt"+name)) mf$("infotxt"+name).innerHTML=infostri;oinfostri=infostri;} 
                      timeinf=2;}                                                      
     if(timeinf<=0) {if(mf$("infotxt"+name)) mf$("infotxt"+name).innerHTML="";oinfostri="-";return;}
     
setTimeout(name+".ftimefinfo()",500);
}}


 



/*
ar: json  ritorno
{ ntot: n, data:[ [0]head,  [1...n]value ];
ntot:   -1  se treegrid   n se grid
data:
array[0]        header[   ]              
array[1....n]   value[  ]         

*/


 

function HnrRifPos(m){
  with(this){

  rifY=[];
  if(!m) arrCHK=[];

  var fc, r, n=Ypos.length, ych=0; 
 
  for(r=0;r<n;r++) {
    fc=Ypos[r].fcode;
    rifY[fc]=r;
    Ypos[r].posY=ych;            
    ych+=Yi;                   
  }
     Hnr=n;
     if(!First){        
       try{
       eval("Vscr=Vscr"+name); 
       var hyr=n?(Hnr*Yi):Vscr.H; 
              Vscr.Resize( {HH:hyr} );   
              nodrw=0;sedrw=2;draw();  
       } catch(ev) {}            
     } else First=0;             
}}



function deleteSelected(){
with(this){  
  if(!arrCHK.length) return;
    moGlb.confirm.show(moGlb.langTranslate("Do you confirm deletion?"), function () {
        var r,n,fc;
        for(r=0;r<arrCHK.length;r++) { fc=arrCHK[r]; delete JStore[fc]; Ypos[rifY[fc]]=-1; }
        for(r=0;r<arrCHK.length;r++){
           for(n=0;n<Ypos.length;n++) {if(Ypos[n]==-1) {Ypos.splice(n,1);break;}}
        }
        HnrRifPos();
    });
}}


function saveList(){
with(this){

  var k,n, a=[];

  for(n=0;n<Ypos.length;n++) a.push(Ypos[n].fcode);
  for(n=0;n<hiddfcode.length;n++) a.push(hiddfcode[n]);  // hiddenCross

  return {f_code:a, f_code_main:f_code, f_type:ftype, Ttable:Ttable};
}}


this.count = function () { return this.Ypos.length; }

} // fine tgl1



