// module treegrid

 
/*

//////////////////////////////////////////////////////////////////////////////////////

checkedIds=[ ... ];

actselector(jse)    
jse=[fcode0,fcode1,...]

//////////////////////////////////////////////////////////////////////////////////////



ricordarsi:
- lkEdit  f_code in editing     0=nessun edit
 

methods:

IN1598
   closeEdit()      forza unlock Edit
   deCheck()        deseleziona i chekkati
   checkDetect()    return array of selected item escluso lkEdit

   Reversajax( [ fcode0, fcode1, ...n ] )

OUT



fixs:
-scrollbar mask



// Hy  0|1 se Tree   0|2 Se root implicito fcode(0)

 asHy 1=tree    0=grid

 n0 n1
[0, n]  lista piatta - root implicito: fcode 0
[>1,n]  tree         - root implicito: fcode 0
[1,1]   tree         - root


Header:
0 fieldb  nome database
1 label   nome campo visualizzato
2 type:   0=testo | 1=numerico | 2=booleano | 3=data | 4=select (filter) | 5=image
3 lock    0=unlock | 1=lock
4 hidd:   0=nascosto | 1=Visibile 
5 wdth   larghezza campo Header in px
6 filter: [...] se type=4 select 


x1
x2








Props:

Abody { y:posiz canvas, f:f_code riga }
 
LStore[progre]: { idf, idp:[ idp0, idp1, ... ], nch:1000, child:[ gidf0, gidf1, ... ], selected  }    open in Ypos 
JStore

selected: 
0   non selezionato
1   bloccata da altro user (sola lettura)
2   checkbox
3   in Editing
  
resize(l,t,w,h,i)   "layout"+i  div contenitore layout
 


menuprio=[
{Txt:"Normale", Type:3, Status:0, Group:3, Icon:['','checked'],   Fnc:name+".FilterMenu(0)" },
{Txt:"Urgente", Type:3, Status:0, Group:3, Icon:['','checked'],   Fnc:name+".FilterMenu(1)" },
{Txt:"Immediata", Type:3, Status:0, Group:3, Icon:['','checked'], Fnc:name+".FilterMenu(2)" }
];




hierarchy: 0 | 1
parent: f_parent_code

 


tab: [nometab, f_type]

Ands:[ f_priority=2, descrizione like '%guasto%' , ...  ]

Sort:[sort1, sort2]
    sort1:  default: f_order  clear filter     { f:descrizione,  m:ASC }
    sort2:  add sort / default: none            status DESC

Group: group by

Limit: [ini, num]


OUTPUT

arr[  
      { nch, open, selected, field0:value0, ..., f_parent_code:[pdr0, ...],  }
]

aPRange=[]
aPRange[r].condz = [ { exp:"==2", val:"#F7941D" }, { exp:"==3", val:"#F02020"} ] 
aPRange[r].field = nomefield
aPRange[r].typ = tipo numerico/stringa

aRange[field]= [{ exp:"==2", val:"#F7941D" }, { exp:"==3", val:"#F02020"}]  -> condz

*/

/*
getTrimestri={   ini_date:"01/05/2009",
calc:function(val){  
with(getTrimestri){
  if(!val) return 0
  d0=madate(ini_date); 
  d1=madate(val);  
  da=(d1[1]-d0[1])*12;      
  da+=(da>12)?(d1[0]-d0[0]):(d1[0]-d0[0]);
  return parseInt(da/3)+1;
}},

madate:function(v){ var a=v.split("/"); 
if(typeof(a[1])=="undefined" || typeof(a[2])=="undefined") return [0,0];
if(a[1].substr(0,1)=="0") a[1]=a[1].substr(1,1);
return [parseInt(a[1]), parseInt(a[2])];
} 
} //

*/

function moTreeGrid(nm, z) {
// props
this.name=nm;



this.create = function (z) {
  
var props={ modTM:"", Vscr:null, Oscr:null, bookmark_name:'', bookmark_id:0, moduleName: "", d_ded:0,
            checkmode:0, Ttable:"t_ware", crud:"", ispopup: false, ftype:1, fname: '', J:null, jj:null, W:0, H:0, zL:0, zT:0,
            nodrw:0, sescr:0, sedrw:3, SEDRW:3, Stp:50, addStp:0, defStp:150,
            timelock:moInit.timeTest,  tUlk:1, f_code: 0,
            NOEVT:0, noAnim:0, Pareverse:0,
            mgL:0, mgT:0, mgR:0, mgB:0, VW:18, padf:4, iext:0, RX1:0, szW:32,                                                       // margini
            HeadH:24, cnvLS:56, mecn:14, Hlw:0, Huw:0, NL:0, NU:0, NT:0, OH:0, SH:0, 
            msgH:0, adds:0, upds:0,                                                                                                 // cnvLS w canvas locked e selected | head lock w | head unlock w
            CNV:null, CNJ:null, BDN:null, oBDN:null, BDJ:null, MBDJ:null, OBDJ:null, HBDJ:null, maskj:null, mask2j:null, ccMm:0,
            IMD:null, OMD:null, FMD:null, TGJ:null,             
            optr:0, vptr:0, OVR:-1, SEL:-1, bOVR:-1,
            Hy:1, asHy:1, displ:1, lkEdit:0, olEdit:0,
            EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove', oldmouseup:"", NMM:0, Cur:0, 
            dwnBD:0, noWH:0, seBlk:0, blkStp:600, skipwf: false,
            Hnr:0, PXi:0, PYi:0, Root:"", LJ:0, checkedIds:[],
            Max:500, MaxOver:650, nBlock:40, Nrig:1, 
            SORT:[], FILTER:[], SEARCH:[], AND:[], SELECTOR:{}, LIKE:{},
            fieldTree: "f_title", rowH:32,
            aFavorite: {}, FavoriteId: 0, data_id: 0, data_name: "",
            TLTPJ:null, WTP:0, Arrtip:[], Clc:0, Xps:0, Yps:0, oXps:0, oYps:0, tipOVR:-1,
            FirstSort:"", FirstSortAD:"", ref_fault_field:"", self: 0, ignoreSelector: false, recursiveOpen:0,
            rowcounter:0,
            bkMenu:0, lastrefresh:0, editing:0, pcross_inv:0
        }

for(var i in props) this[i]=(typeof z[i]=='undefined')?props[i]:z[i];
this.Yi=32;            // row height
}

this.Yt=0;             // posizione top Y nel canvas body alto SH  
this.AR=[];
this.mCK=[];
this.JStore=[];
this.multifcode=[];
this.Header=[]; 
this.Abody=[];
this.ROWBKCOL=["#FFFFFF","#FAFAFA","#FEFF9E"];
this.ROWBDCOL="#EDEDED";
this.ROWCOLTXT=moColors.text;


this.arrCHK=[];         // locked by checkbox
this.countCheck=0;
this.aChecked=[];       // Atomic checked!!
this.arrLK=[];          // locked da altri user
this.arrMB={};          // if locked by mobile   [fcode]=0|1   1: mobile
this.arrNd=[];          // array degli fcode apri/chiudi  (se asHy)       // { l,t, Ypx }
this.blank=[];
this.Ypos=[];
this.rifY=[];
this.aRange=[];
this.aPRange=[];
this.revList=[];
this.aFilter=[];
this.arrACTION={};   // action click on cell

this.IM=moGlb.baseUrl+"/public/msim_images/default/"; 
this.Curs=["default","pointer","col-resize"];



this.tmpList=[];
this.menulist=[];
this.Fmenu=[ {Txt:moGlb.langTranslate("Sort ascending"),  Type:3, Status:0, Group:1, Icon:['','FlaticonsStroke.58715'], Fnc:"" },
             {Txt:moGlb.langTranslate("Sort descending"), Type:3, Status:0, Group:1, Icon:['','FlaticonsStroke.58714'], Fnc:"" }, 
             {Type:4 }, 
             {Txt:moGlb.langTranslate("Add filter"), Disable:0, Type:1, Sub:[
                                            {Txt:moGlb.langTranslate("Add sort"), Type:1, Sub:[
                                                             {Txt:moGlb.langTranslate("Sort ascending"), Type:3, Status:0, Group:2, Icon:['','FlaticonsStroke.58715'], Fnc:"" },
                                                             {Txt:moGlb.langTranslate("Sort descending"), Type:3, Status:0, Group:2, Icon:['','FlaticonsStroke.58714'], Fnc:"" } 
                                                                                   ]},
                                            {Txt:moGlb.langTranslate("Add search"), Type:0, Icon:['FlaticonsStroke.58457'], Fnc:"" },
                                            {Txt:moGlb.langTranslate("Add filter by"), Disable:1, Type:1, Sub:[]} 
                                                      ]}, 
             {Txt:moGlb.langTranslate("Clear all filter"), Disable:0, Type:0, Fnc:this.name+".actclearfilter(2)"},
             {Type:4 }, 
             {Txt:moGlb.langTranslate("Search"), Type:0, Icon:['FlaticonsStroke.58457'], Fnc:"" },
             {Txt:moGlb.langTranslate("Filter by"), Disable:1, Type:1, Sub:[]},
             {Txt:moGlb.langTranslate("Lock / unlock"), Type:2, Status:1, Icon:['FlaticonsStroke.58405','FlaticonsStroke.58404'], Fnc:"" },               
             {Txt:moGlb.langTranslate("Hierarchy"), Type:2, Status:0, Icon:['','checked'], Fnc:this.name+".acthierarchy()" },
             {Txt:moGlb.langTranslate("Hidden"), Type:0, Fnc:"" }                                           
           ];

this.FilerData=[
{Txt:moGlb.langTranslate("Today")},
{Txt:moGlb.langTranslate("Yesterday") },
{Txt:moGlb.langTranslate("Tomorrow") },
{Txt:moGlb.langTranslate("Current week") },
{Txt:moGlb.langTranslate("Last week") },
{Txt:moGlb.langTranslate("Next week") },
{Txt:moGlb.langTranslate("Current month") },
{Txt:moGlb.langTranslate("Last month") },
{Txt:moGlb.langTranslate("Next month") },
{Txt:moGlb.langTranslate("Current year") },
{Txt:moGlb.langTranslate("Last year") },
{Txt:moGlb.langTranslate("Last 3 days") },
{Txt:moGlb.langTranslate("Last 30 days") },
{Txt:moGlb.langTranslate("Last 365 days") }
];
 
this.Filterby=[];

this.menuChecked=[
{Txt:moGlb.langTranslate("Add viewed"), Type:0, Fnc:this.name+".actmenuchek(0)"},
{Txt:moGlb.langTranslate("Sub viewed"), Type:0, Fnc:this.name+".actmenuchek(1)"},
{Txt:moGlb.langTranslate("Uncheck all"), Type:0, Fnc:this.name+".deCheck()"}
];


this.serchSelect=[
      [moGlb.langTranslate("Contains"), moGlb.langTranslate("It does not contain"), moGlb.langTranslate("Equal"), moGlb.langTranslate("Different")],                               // 0 = testo
      [moGlb.langTranslate("Equal"), moGlb.langTranslate("Different"), moGlb.langTranslate("Greater than"), moGlb.langTranslate("Less than"), moGlb.langTranslate("Expression")],                // 1 = numerico
      [moGlb.langTranslate("True"), moGlb.langTranslate("Falso")],                                                           // 2=booleano
      [moGlb.langTranslate("Equal"), moGlb.langTranslate("Different"), moGlb.langTranslate("Greater than"), moGlb.langTranslate("Less than"), moGlb.langTranslate("Period")]                   // 3=data
];

this.searchWindows = [];  

// metodi
this.getSelectedRows=getSelectedRows;

this.fHeadDisp=fHeadDisp;
this.chgHeaderUpdt=chgHeaderUpdt;
this.getHeader=getHeader;
this.setHeader=setHeader;
this.Display=Display;  
this.resize=resize;
this.closeEdit=closeEdit;
this.checkDetect=checkDetect;

this.predraw=predraw;
this.draw0=draw0;
this.draw1=draw1;
this.draw=draw;
this.draw2=draw2;
this.ClearB=ClearB;
this.fMaskLoad=fMaskLoad;
this.animdraw=animdraw;
this.tmanimdrw=tmanimdrw;
this.noFound=noFound;

this.Cell=Cell;
this.hCell=hCell;

this.RowCells=RowCells;
this.RCells=RCells;
this.clipRow=clipRow;

this.loadroot=loadroot;
this.retroot=retroot;
this.jaxULock=jaxULock;
this.retULock=retULock;
 

this.deCheck=deCheck;
this.timeUlock=timeUlock;
this.retotherLock=retotherLock;

this.Vbarpos=Vbarpos;
this.Obarpos=Obarpos; 
this.endScroll=endScroll; 
this.scrollO=scrollO;
this.SeSort=SeSort;
this.SeFilterby=SeFilterby;
this.SeSearch=SeSearch;

this.headown=headown;  
this.headmove=headmove;
this.headout=headout;

this.oversel=oversel;
this.overmoveh=overmoveh;
this.overMove=overMove;

this.bodymove=bodymove;                                                                                                                                                               
this.bodyout=bodyout;
this.bodydown=bodydown;
this.bodyup=bodyup;
this.drawbodyover=drawbodyover;

this.maskmove=maskmove;
this.maskup=maskup;
this.headRitAnimat=headRitAnimat;
this.endAnim=endAnim;

this.fieldlist=fieldlist;
this.viewmenucheck=viewmenucheck;
this.createmenu=createmenu;
this.acthidden=acthidden;
this.view_all_field=view_all_field;
this.acthierarchy=acthierarchy;
this.actlock=actlock;
this.actsort=actsort;
this.actaddsort=actaddsort;
this.actfilterby=actfilterby;

this.actsearch=actsearch;
this.actLike=actLike;
this.globSearch=globSearch;
this.closefSearch=closefSearch;
this.TrovaSearch=TrovaSearch;
this.changeidSel0=changeidSel0;

this.actclearfilter=actclearfilter;
this.actmenuchek=actmenuchek;
this.viewchecked=viewchecked;
this.viewall=viewall;
this.actselector=actselector;
this.addAnd = addAnd;

this.ActRefresh=ActRefresh;
this.timeRefresh=timeRefresh;
this.fnoAct=fnoAct;
this.ResetYLS=ResetYLS;

this.updHeader=updHeader;
this.fmenuList=fmenuList;

this.Reversajax=Reversajax;
this.Reverseflash=Reverseflash;
this.insReverse=insReverse;
this.canvColorRevers=canvColorRevers;

this.ifExist=ifExist;
this.ifarrEQual=ifarrEQual;
this.f_alert=f_alert;

this.timeBlank=timeBlank;
this.updateChecked = updateChecked;
//this.setFavourite = setFavourite;
//this.getFavourite = getFavourite;
this.setSummaryCondition = setSummaryCondition;

this.cicleTip=cicleTip;
this.wiewTip=wiewTip;
this.overTip=overTip;
this.delTip=delTip;

this.unlockElements=unlockElements;
this.refresh = refresh;


this.searchField=[];
this.colrs=["#F0F0F0","#A1C24D","#4A6671","#6F8D97","#E2EECA","#E0E0E0","#A0A0A0","#DDDDDD","rgba(161,194,77,0.3)"];

 this.fromSearch = false;

// IN --------------------------------------------------------------------------


function Display(i){
with(this){ if(i==displ) return;
if(i) { var nb="block"; displ=1; }
else { var nb="none"; displ=0; }
j.style.display=nb;
}}


        
function resize(l0,t0,w0,h0,i){   //  moGlb.layoutPrefixId = "layout_"+i attivo
  with(this){ 
    if(modTM) { try { eval(crud.replace('tg', 'tl')); } catch(ex) { modTM = ''; } }
        
        
              
    // create container jj first execution
  if(typeof(jj)=="undefined" || !jj) { 
      J=mf$(i);
 
      jj=moGlb.createElement("id"+name,"div",J);
      with(jj.style) overflow="hidden";   
 
      draw0(); 
      loadroot(); 
      timeBlank();
         
 
      // create scrollbars   
      eval("Vscr"+name+"=new OVscroll('Vscr"+name+"',{ displ:0, L:-100, T:4, H:h0-8, HH:1, Fnc:'"+name+".Vbarpos', upFnc:'"+name+".endScroll', pid:'"+jj.id+"' }); VW=Vscr"+name+".W;"); 
      eval("Oscr"+name+"=new OVscroll('Oscr"+name+"',{ displ:0, OV:1, L:0, T:0, H:w0, HH:1, Fnc:'"+name+".Obarpos', upFnc:'"+name+".endScroll', pid:'"+jj.id+"' });");      
    
      if(!checkmode) timeUlock();  
   } 
          
    with(jj.style) { left=l0+"px",top=t0+"px",width=w0+"px",height=h0+"px"; }
    W=w0, H=h0;
    
    var p=moGlb.getPosition(jj);   
    zL=p.l, zT=p.t;
    
    if(Hlw) predraw();  
    addStp=0;   
}}



 

 




function closeEdit(){
with(this){
    
 unlockElements();
 for(var r=0;r<arrCHK.length;r++) if(arrCHK[r]==lkEdit) { arrCHK.splice(r,1); break; } 
 

sedrw=2; draw();
}}

function unlockElements() { with(this) { jaxULock(arrCHK,0,0); lkEdit=0; olEdit=0; } }


function checkDetect(){ return this.aChecked; }
 
 
function getSelectedRows(){
    with(this){   
  
    var jr=[], ak;
    for(var r=0;r<arrCHK.length;r++) { ak=arrCHK[r];
        jr.push(JStore[ak]);    
    }
   return jr;
}}
 
//--------------------------------------------------------------------------------------------------------------------------------------- 
// LOAD record

function loadroot(){
with(this){

  var ja=JSON.stringify({ Level:0, Tab:[Ttable, ftype, fname], f_code: f_code, module_name: crud, self: self, ignoreSelector: ignoreSelector, skipwf: skipwf, pcross_inv:pcross_inv });
  Ajax.sendAjaxPost( moGlb.baseUrl+"/treegrid/jax-read-root", "param="+ja , this.name+".retroot" );
}}


// tipo di root e personal setting field
function retroot(a){
  with(this){  

  var ar=JSON.parse(a), ln=ar.n0;
  if(moGlb.isset(ar.message)) moGlb.alert.show(moGlb.langTranslate(ar.message));
  aFavorite=ar.aFavorite, FavoriteId=ar.FavoriteId;    
  if(!Hy) asHy=0;
 
  if(typeof ar.checkCross == "undefined") {}
  else { if(ar.checkCross.length) checkedIds = moGlb.cloneArray(ar.checkCross); }
 
  if(checkmode){
    menuChecked.push( {Txt:moGlb.langTranslate("View checked"), Type:0, Fnc:this.name+".viewchecked()"} );
    menuChecked.push( {Txt:moGlb.langTranslate("View all"), Type:0, Fnc:this.name+".viewall()"} );
  
    if(checkedIds.length) { 
      arrCHK=moGlb.cloneArray(checkedIds);
      AND=[ { f:"f_code", z:" in ("+arrCHK.join(",")+") " } ];
      asHy=1;
    }
  }
  Fmenu[9].Status=asHy;   // menu [9]= Hierarchy
  if(!Hy) Fmenu[9].Disable=1;
  moMenu.IM=IM;
  setHeader(ar.bookmark, ar.bookmark_id, ar.bookmark_name); 
  rowcounter=0;
  LoaData(-1, 0, 0, 0, "root", 0);    // load first record root         
}} //



function setHeader(ab, bi, bn){   // bookmark,  bookmark_name,  bookmark_id
with(this){  
 
//--------------------------------------------------
// setting in: treegrid_search_fields

  searchField=moGlb.getSearchFields(name);
 
  // search in   mdl_wo_crud_header    ico 58457
  var snm, anm, snmj=null, srj=null, hlp=0;
  
    anm=name.split("_tg_tg");
  if(anm.length==2){
  
    snm=anm[0]+"_crud_header";
    try{ snmj=eval(snm); }catch(e){}
    if(snmj){
      srj=moGlb.createElement("id_"+snm+"_search","div",snmj);  
      if(srj){
      
        // if exist mdl_wo_crud_help
        try{ eval(anm[0]+"_crud_help"); hlp=32; }catch(e){ hlp=6; }
        
        with(srj.style){
          position="absolute";
          top="3px";
          right=hlp+"px";
          font="16px FlaticonsStroke";
          color="#FFF";
          cursor="pointer";
        }
              
        srj.innerHTML=String.fromCharCode(58457);
        srj.onclick=function(e){   if(!moGlb.lockEdit) globSearch();  }
      }   
    } 
  }
//-------------------------------------------
 
  if(!bn) bn="";
  if(!bi) bi="";
  bookmark_name=bn, bookmark_id=bi; 

  NT=0, NL=0, NU=0; Header=[]; AR=[];  Hlw=0, Huw=0; SEL = -1;


  //alert(name+"\n"+JSON.stringify(ab))

  // load fields and build menu for head 
  // Header
  //          0         1     2       3       4     5       6       7         8       9         10         11      12   13
  var prm=["fieldb","label","type","lock","hidd","wdth","filter","range","prange","script","visibility","grouped","x1","x2"],
  sw=0, r,rr,i, fb, c, fz, ki, fld, btxt;  
  aRange=[], aPRange=[], aFilter=[];                                                                                        
              
  if(!ab) moDebug.log("Missing or incorrect bookmark for module "+crud);
  
  rr=0; 
  for(r=0;r<ab.length;r++) { 
  
    // visibility   
    var ar10=ab[r][10]; 
    if(typeof(ar10)=="undefined") ar10=-1;
    
    
    if(!(moGlb.user.level & ar10) ) continue;
  
    Header[rr]={}; 
 
    fz=ab[r][0]; AR[fz]=[]; // salvo in AR   filter  range  prange  (del campo fieldb)
    for(ki=6;ki<9;ki++) AR[fz][prm[ki]]=ab[r][ki];     // es. AR[fieldb][filter]=array bookmark
  
    for(i=0;i<prm.length;i++) Header[rr][prm[i]]=(i<6)?ab[r][i]:"";                           // trasformo in associativo      se<6 

    // Range
    var ar7=ab[r][7].length;
    if(ar7) { fz=ab[r][0]; aRange[fz]=[]; 
      for(var n=0;n<ar7;n+=2) aRange[fz].push( {exp:ab[r][7][n], val:ab[r][7][n+1]} );
    }
  
    // aPRange 
    var ar8=ab[r][8].length;                                                                                                                        
    if(ar8) { aPRange.push( { field:ab[r][0], condz:[], typ:ab[r][2] } );  c=aPRange.length-1;                
      for(var n=0;n<ar8;n+=2) aPRange[c].condz.push( {exp:ab[r][8][n], val:ab[r][8][n+1]} );
    }   
    
    // script
    var ar9=ab[r][9]; 
    Header[rr][prm[9]]=(ar9)?ab[r][9]:"";
 

    // select
    if(Header[rr].type==4) { 
      fb=Header[rr].fieldb; Filterby[fb]=[];                                    // array('0'=>'Nessuna','1'=>'Normale','2'=>'Urgente','3'=>'Immediata')   
      aFilter[fb]=[];
      for(k in ab[r][6]){
        aFilter[fb][k]=ab[r][6][k];
        Filterby[fb].push({Txt:ab[r][6][k], Type:3, Value:k, Status:0, Group:3, Icon:['','checked'], Fnc:"" });  
      }
     }
      
    // select
    if(Header[rr].type==2) { 
      fb=Header[rr].fieldb; Filterby[fb]=[];                                   
      aFilter[fb]=[];
        btxt=moGlb.langTranslate("True");
        aFilter[fb][1]=btxt;
        Filterby[fb].push({Txt:btxt, Type:3, Value:1, Status:0, Group:3, Icon:['','checked'], Fnc:"" });
        
        btxt=moGlb.langTranslate("False");
        aFilter[fb][0]=btxt;
        Filterby[fb].push({Txt:btxt, Type:3, Value:0, Status:0, Group:3, Icon:['','checked'], Fnc:"" });      
     }  
      
      
    // visibility  
    Header[rr]["visibility"]=ar10;
    
    // grouped   
    var ar11=ab[r][11]; 
    if(typeof(ar11)=="undefined") ar11="";
    Header[rr]["grouped"]=ar11;
    
   rr++;
  
  }   // fine for
  
  fHeadDisp(); 
   
      // se sort iniziale imposto SEL
      if(SORT.length){ FirstSort=SORT[0].f, FirstSortAD=SORT[0].m; 
      for(r=0;r<Header.length;r++) { if(Header[r].fieldb==FirstSort){ SEL=r; break; } } 
      }
 
  fmenuList();
  updHeader();  
}}



function fHeadDisp(){          // header disposition  ( lock - hidden_lock - unlock )  
with(this){  

  var r,i,n=Header.length, aH=[], al=[];
  
  for(r=0;r<n;r++){
    if(Header[r].lock){
      if(Header[r].hidd) aH.push( moGlb.cloneArray(Header[r]) );   // lock
      else al.push(r);                                            // hidden_lock
    }
  }
  
  for(r=0;r<al.length;r++) {                            // hidden_lock
    aH.push( moGlb.cloneArray(Header[ al[r] ]) );   
  }
  
  for(r=0;r<n;r++){                                     // unlock
    if(!Header[r].lock) aH.push( moGlb.cloneArray(Header[r]) );                                 
  }
  
  Header=moGlb.cloneArray(aH);
}}





function chgHeaderUpdt(he){  // return Header from popup bookmark
with(this){
    Header=he;
    fHeadDisp();
    fmenuList();
    updHeader(); 
    sedrw=3; draw();    
}}









function getHeader(){
with(this){  

var abk=[], r, a, k;

for(r=0;r<Header.length;r++){ 
   a=Header[r];
   abk.push( [ a.fieldb,
               a.label,
               a.type,
               a.lock,
               a.hidd,
               a.wdth,
               AR[a.fieldb]["filter"],
               AR[a.fieldb]["range"],
               AR[a.fieldb]["prange"],
               a.script,
               a.visibility,
               a.grouped
              ]);
}

return [abk, bookmark_id, bookmark_name];
}}


// DRAW -------------------------------------------------------------------------------------------------------------------------- 

function predraw(){
with(this){  
 // resize srollb
 var sl=W-VW-mgR, st=mgT+HeadH+msgH, 
     ol=mgL+cnvLS+Hlw, ot=H-VW-mgB; 
    
     OH=W-cnvLS-Hlw-mgL-mgR;           
     SH=H-HeadH-VW-mgB-mgT-msgH;
  
  eval("Vscr=Vscr"+name);
  eval("Oscr=Oscr"+name); 
    
  if(SH<64 || W<80) Vscr.Display(0); else { if(!Vscr.displ) Vscr.Display(1); Vscr.Resize( { L:sl, T:st, H:SH, HH:Hnr*Yi } ); }
  if(OH<64) Oscr.Display(0); else { if(!Oscr.displ) Oscr.Display(1); Oscr.Resize( { L:ol, T:ot, H:OH, HH:Huw } ); }

 // resize infogrid 
 mf$("infogrid"+name).style.width=ol+"px";
 draw1();
}}                                                                                                                   

  
  
function draw0(){
with(this){    

  if(isTouch) EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';

  var hbody=H-HeadH-VW-mgT-mgB-msgH,onm="",z,s="",zz,tc,lc;
    
  z="z-index:", zz=0; lc=20+mgL, tc=(4+mgT)-3;  
  
  if(isTouch) {  
     onm=EVTDOWN+"='"+name+".bodydown(event)' "+EVTMOVE+"='"+name+".bodymove(event)' "+EVTUP+"='"+name+".bodyup(event)' ";
  
  } else {     
    onm=" onmouseover='WHEELOBJ=\"Vscr"+name+"\"' onmouseout='"+name+".bodyout(event)' "+EVTMOVE+"='"+name+".overMove(event)' "+EVTDOWN+"='"+name+".bodydown(event)' ";
   
    TGJ=moGlb.createElement("maskTreegridDragScroll","div",document.body);
    TGJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(moGlb.zloader+500));      
  }      
               

    // left head
  s="<canvas id='lcksel"+name+"' style='position:absolute;left:"+mgL+"px;top:"+mgT+"px;"+z+zz+";' width='"+cnvLS+"' height='"+HeadH+"'></canvas>"+
 
    "<div id='checklcksel"+name+"' style='position:absolute;left:"+lc+"px;width:34px;top:"+tc+"px;"+
    z+(zz+1)+";cursor:pointer;color:"+colrs[5]+";font:16px FlaticonsStroke;text-align:center;'"+
    " onclick='"+name+".viewmenucheck()'>"+String.fromCharCode(58826)+"</div>"+ 
 
    // head      
    "<canvas id='headlu"+name+"' style='position:absolute;left:"+(mgL+cnvLS)+"px;top:"+mgT+"px' width='"+Hlw+"' height='"+HeadH+"' ></canvas>"+       
    "<div id='idmsgrev"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px;width:10px;height:"+msgH+";overflow:hidden;"+
    "background-color:#f0f0f0;cursor:pointer;'></div>"+
           
    "<canvas id='bodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px' width='10' height='"+hbody+"'></canvas>"+ 
    "<div id='overbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px;width:10px;height:"+hbody+";overflow:hidden;'></div>"+
         
    "<canvas id='toolTip"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px;display:none;' width='10' height='10'></canvas>"+      

    "<div id='maskbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px;width:10px;height:"+hbody+";overflow:hidden;background-color:#ff0;opacity:0.0;' "+onm+"></div>"+
 
    // header
    "<div style='position:absolute;left:"+mgR+"px;top:"+mgT+"px;width:"+VW+"px;height:"+HeadH+"px;cursor:pointer;' onclick='"+name+".fieldlist(event)'>"+
    "<div style='margin:2px 0 0 2px;'>"+moCnvUtils.IconFont(58445,16,colrs[2])+"</div>"+
    "</div>"+


    // footer info
    "<div id='infogrid"+name+"' style='position:absolute;left:"+mgL+"px;bottom:"+mgB+"px;height:"+VW+"px;width:"+cnvLS+"px;overflow:hidden;'>"+
    "<div id='infotxt"+name+"' style='font:13px opensansR;margin:2px 4px 0 8px;color:#000;'>No record found.</div></div";
           
  jj.innerHTML=s;  
  CNJ=mf$("headlu"+name), CNV=CNJ.getContext("2d");
  BDJ=mf$("bodylu"+name), BDN=BDJ.getContext("2d");
  MBDJ=mf$("maskbodylu"+name);
  OBDJ=mf$("overbodylu"+name);
  HBDJ=mf$("idmsgrev"+name);
  TLTPJ=mf$("toolTip"+name);
        
  eval("CNJ."+EVTDOWN+"=function(e){"+name+".headown(e); }");
  if(!isTouch){
    eval("CNJ."+EVTMOVE+"=function(e){"+name+".headmove(e); }");
    eval("CNJ.onmouseout=function(e){"+name+".headout(e); }"); 
    
  } else {
    eval("CNJ."+EVTMOVE+"=function(e){"+name+".maskmove(e); }");
    eval("CNJ."+EVTUP+"=function(e){"+name+".maskup(e); }");   
  } 
  
  
  
  
  // drag drop upload attach
  MBDJ.addEventListener('drop', function(e){ 
    moEvtMng.cancelEvent(e);  
    
      var lv=true, dd=parseInt(d_ded);
      if(moGlb.user.level>0 && (moGlb.user.level & dd) ) lv=false;     
      
      if( (name=="mdl_doc_tg_tg" || name=="mdl_doc_pp_tg_tg") && lv) {  
      
        //alert("Drop file!"); 
        moDropAttach.handleDrop(e,"");   // name+".refresh"
      }  
      
    }, false);
    
  MBDJ.addEventListener('dragover',function(e){moEvtMng.cancelEvent(e);},false);
  
  
  
}}



// caching per il ridisegno dei canvas head e body
function draw(tt){    
  with(this){SEDRW|=sedrw;      
    if(!tt) {if(!nodrw) {tt=1;nodrw=1; } else return; } 
    if(tt>1) { if(SEDRW&1) draw1(); if(SEDRW&2) draw2(); nodrw=0; sedrw=3; return; }
   tt++;  
setTimeout(name+".draw("+tt+")",(Stp+addStp));
}}





function draw1(){
with(this){           

  if(W<=moGlb.moduleMinWH) { Display(0);  return; } // dimensione minima
  Display(1);
 
  // cell locked-selected
  var cnv=mf$("lcksel"+name).getContext("2d"), c2=(cnvLS-mecn)/2;
  Cell(cnv,0,c2,0);
  Cell(cnv,c2,cnvLS,0); 

  var nk=arrCHK.length, hvis; 
  ck=(nk<2 && (lkEdit || !nk))?0:1; 
  mf$("checklcksel"+name).style.color=colrs[ck?2:5];
 
  // celle lock & unlock 
  CNJ.width=Hlw+OH;      
  for(var r=0;r<NT;r++) { if(Header[r].hidd) hCell(r); }
}}



function noFound(){
with(this){ 
  with(BDN){  
    if(Ypos.length<2){
      font="13px opensansR"; textBaseline="alphabetic"; textAlign="left";
      fillStyle="#888"; fillText(moGlb.langTranslate("No match found."),8,20); 
    }
  }
}}




function draw2(no){
  with(this){    if(!no) no=0;   
 addStp=0;
if(msgH>0) canvColorRevers(colrs[1]); 
 try{
  // bodywheel 
  if(!no){ var uw=cnvLS+Hlw+OH;
    with(BDJ) width=uw, height=SH;
    with(MBDJ.style) width=uw+"px", height=SH+"px";
    with(OBDJ.style) width=uw+"px", height=SH+"px";
    with(HBDJ.style) width=W+"px"; 
  }
 }catch(e){  return false; };
 
    
 
  var nar=Ypos.length;
  Arrtip=[];
  arrACTION={};
  
  Yt=vptr%Yi, Yt*=-1;                                           // Yt delta posizione iniziale
  var n0=(SH-Yt); nr=parseInt(n0/Yi), nr+=(n0%Yi)?1:0;          // nr = numero righe visualizzabili nel canvas top = Yt
  Nrig=nr; 
  
  //Ypos 
  var nn=0, p0,p1, Yx=1, y=vptr+Yt;   if(nar>100) { if(Ypos[nar>>1].posY<y) nn=nar>>1;  }     // ottimizzo lista per ricerca Ypos[ Yx ].posY
  if(!nar || (nar==1 && Ypos[0].posY<0)) { ClearB(1);  
                                           setTimeout(name+".noFound()",1000);
                                           return; }
 
 
  f_countCheck();
                                         
  // cerco inizio visualizzazione  Yx
  for(var r=nn;r<(nar-1);r++) { 
    p0=Ypos[r].posY, p1=Ypos[r+1].posY;
    
//    moGlb.alert.show(p0+" | "+p1+" - "+(nar-1))
    
    Yx=r;
    if(vptr>=p0 && vptr<p1) break; 
    Yx=nar-1;
  }
 
  

   
  arrNd=[], blank=[], Abody=[]; 
  var Y=Yt, x=Yx, or=0, cy=y/Yi, c, nbk=0, pc,ps,cr,bx,p1=0, fx=x;                              
  ClearB(0);
 

 /*
nr  numero righe
y   testa visualizzazione (vptr+(-Yt))   per Ypos
Y   Yt pos del canvas   

x   elemento partenza in Ypos ed incrementato se visualizzato
fx  ultimo elemento visualizzato
p0
nar Ypos.length
 */
 
 
 
// disegno nr righe a partire da Yt
  for(r=0;r<nr;r++){
 
      p0=Ypos[x].posY;         
      c=cy&1;
      
      
//  mf$("debug").innerHTML="Yx13: " +Ypos[13].fcode+" | "+r; moGlb.alert.show("riga: "+x)    
      
      // verifico se esiste o blank 
      if(p0>=y && p0<(y+Yi)) {  RowCells(Ypos[x],Y,c);                                  // esiste
                                x++;                                                   // Ypos successivo
      } else { RowCells(0,Y,c);                                                       // non esiste, blank
        
               bx=(r)?(x-1):x;   
               
               pc=Ypos[bx].fpdr, ps=Ypos[bx].fpos+1;                                // pc padre ultimo elemento (i blank sono fratelli dell'ultimo Ypos)
               if(!r) { p1=(y-Ypos[bx].posY)/Yi; ps+=p1-1;  }
        
               cr=0; nbk=blank.length;
               if(nbk && r>(or+1)) cr=1;    // discontinuità blank 
               else { if(!nbk) cr=1; 
                      else blank[nbk-1].limn++; }     // incrementa limit
 
 
               if(cr) { var artr=Ypos[bx].rtree.split("|"); artr.pop();  
                        blank.push({ bpdr:pc, limi:ps, limn:1, lev:Ypos[bx].levl, rtree:artr.join("|") }); }
               or=r;
               if(p0<(y+Yi)) x++;    // se prendere successivo
              }   

 
 if(x>=nar) {                           // tutti gli altri blank o non ci sono altri elementi da visualizzare Hnr no scrollbar
     cr=(or==r)?1:0;                    // se aggiungo al blank precedente                                    
     nbk=blank.length-cr;
     
     for(var i=(r+1);i<=nr;i++) { if(cy>=(Hnr-1)) break;
       RowCells(0,Y,c); Y+=Yi; cy++; c=cy&1; 
       if(!cr) { bx=x-1;   var artr=Ypos[bx].rtree.split("|"); artr.pop(); 
                 blank.push({ bpdr:Ypos[x-1].fpdr, limi:Ypos[bx].fpos+1, limn:1, lev:Ypos[bx].levl, rtree:artr.join("|") }); 
                 cr=1; continue; }
           
                 
        if(!blank[nbk]) break;         
        blank[nbk].limn++;       // incrementa limit
       }
       break;} 

  y+=Yi; Y+=Yi; cy++;
  } // fine for
  
  ClearB(1);                                
 

   // mf$("debug").innerHTML="Yx "+Yx+" | nrighe: "+nr+" | Yt: "+Yt+" | vptr: "+(vptr)+" | Hnr: "+Hnr;    
}}








 


function RowCells(Ypx,y,c){                    // Ypx = Ypos { } | 0
  with(this)  {
  
  // Timeline     
  var BDNtm=null; if(modTM) eval("var BDNtm="+modTM+".BDN, Wtm="+modTM+".OH, tmYt="+modTM+".Yt;");   //   mf$("debug").innerHTML=Wtm
  
  var bdw=cnvLS+Hlw+OH, k, bkg,brd,g;
  
  // background row 
  with(BDN) {
  
  k=1; bkg=ROWBKCOL[c];   
  if(lkEdit==Ypx.fcode) bkg="#C8D9DE";   // marked in edit
  
  fillStyle=bkg;
  fillRect(0,y+k,bdw,Yi-2*k);
  fillStyle=ROWBDCOL;
  fillRect(0,y+Yi-1,bdw,1);
  
  // timeline
  if(BDNtm && Yt!=tmYt) {  
      BDNtm.fillStyle=bkg;
      BDNtm.fillRect(0,y+1,Wtm,Yi-2); 
      BDNtm.fillStyle=ROWBDCOL;
      BDNtm.fillRect(0,y+Yi-1,Wtm,1); 
    }
  
  // mark
  for(r=0;r<NT;r++) with(Header[r]) if(hidd && r==SEL) { save(); g=clipRow(x1,x2,y,lock);
  if(!g) break;
  if(r==SEL) { fillStyle=colrs[8];                                                          //  #@#     moColors.bgOver;
   fillRect(g[0],y,g[1]-g[0],Yi); }                            
   restore(); break;}
                                                                                                                
  // if blank
  if(typeof(Ypx)=="number" || !Ypx) return;
 
  // Rcelle lock & unlock 
  var fc=Ypx.fcode, rtr=Ypx.rtree, r;
  
  if(aFavorite[fc]) {                                                       //---    if marked aFavorite[fcode]=Color    from crud
    fillStyle=moGlb.colorToRGBa(aFavorite[fc],0.3);
    fillRect(0,y+k,bdw,Yi-2*k); }
  } 

  // chech & lock da LStore
  var afc=JStore[fc];  if(!afc) { moGlb.alert.show("no data record found!"+" "+fc); return; }
  
  var ld=4, dd=29, vv=y+parseInt((Yi-16)/2), slt=0, lck=afc.selected;
  
  if(lck>0) { 
    if(lck>1) slt=1; 
    if(arrMB[fc] && arrMB[fc][0]) lck=4;
    
    var flti=58404, flcol=colrs[1];
    if(lck==2) flcol=colrs[2];
    if(lck==3) flti=58507, flcol=colrs[2];
    if(lck==4) flti=58473;
     
    if(!checkmode) moCnvUtils.cnvIconFont(BDN,flti,ld,vv,16,flcol);  
  }
  
   // checkbox
  flti=slt?58826:58783;
  moCnvUtils.cnvIconFont(BDN,flti,dd,vv,16,colrs[2]);
                                 
  Abody.push({y:y, f:fc, rtree:rtr}); 
  
  // text color row in aPRange
  var f,v,nv,i,cz,vz,tz,virg, colr=ROWCOLTXT, stz;
  for(var r=0;r<aPRange.length;r++) {  f=aPRange[r].field; nv=aPRange[r].condz.length, tz=aPRange[r].typ;
                                v=afc[f]; virg=(!tz)?"'":"";  stz = { "{val}": virg+v+virg, "\n":" " };   
                                for(i=0;i<nv;i++) {
                                cz=aPRange[r].condz[i].exp, vz=aPRange[r].condz[i].val;
                                
                                cz=cz.multiReplace(stz); 
                                eval("if("+cz+") colr='"+vz+"';");
                                if(colr!=ROWCOLTXT) break; }
                             } 
  
  for(r=0;r<NT;r++) if(Header[r].hidd) RCells(afc, r, y, Ypx, colr);                      // campi riga
 
  // timeline
  if(modTM) eval(modTM+".fcXchg();")
}}





 

function RCells(rig, r, y, Ypx, colr){       //  progre LStore,  r per Header[r]  fieldb, x1,x2, ...
with(this){

  var X1,X2,x21,c1,c2, lr=OH+Hlw, t="", f="13px opensansR", d=0, nh, v;
  
  with(Header[r]) with(BDN){
      
     var d=0, yy; 
     yy=y+(Yi-16)/2;
   
  X1=x1, X2=x2;
  if(!lock) {  X1-=optr, X2-=optr; if(X2<=Hlw || X1>=lr) return; }

 save();
  x21=X2-X1-padf*2, X1+=cnvLS+padf;
  t=rig[fieldb], v=t;
  
  // clip  
  clipRow(x1,x2,y,lock);
                                                                                      
  font=f; textBaseline="alphabetic"; textAlign="left";     

  if(fieldb==fieldTree && asHy){                                                      // if tree  (f_title)
                d=Ypx.levl*16; nh=Ypx.nch; 
        
    // folder con fault  
    var vfault=0;               
    if(ref_fault_field) {
        try{ vfault=parseInt(rig[ref_fault_field]); }catch(e){}   
    }             
      var vlfolder=vfault?7:0;       
                
       if(nh) {  
                var flti=58364, flcol=colrs[2], flic=6+Ypx.open;
                if(flic==7) flti=58369; 
                if(vfault) flcol="#B00";  // fault folder icon
                moCnvUtils.cnvIconFont(BDN,flti,X1+d+19,yy-1,16,flcol);  
                
                if(nh!=-1) {
                
                var flti=58825, flic=4+Ypx.open;
                if(flic==4) flti=58824;
                moCnvUtils.cnvIconFont(BDN,flti,X1+d,yy+1,14,colrs[6]);

                arrNd.push({ l:X1+d-5, t:yy, oby:Ypx }); }         
       } else {  
                flti=58355;     
                flcol=(vfault)?"#B00":colrs[2];  // if fault red icon   
                moCnvUtils.cnvIconFont(BDN,flti,X1+d+19,yy,16,flcol); 
       }   
       
       d+=32; x21-=d; if(x21<16) x21=16;
  }
        var cz,vz,tz,virg, ccl=colr, virg=(!type)?"'":"", stz = { "{val}": virg+v+virg };         
        if(aRange[fieldb]){
          for(var g=0;g<aRange[fieldb].length;g++) { 
          cz=aRange[fieldb][g].exp, vz=aRange[fieldb][g].val; 
          cz=cz.multiReplace(stz); 
          eval("if("+cz+") ccl='"+vz+"';");  
          if(ccl!=colr) break; }
        }
 
        var ed=parseInt(rig["f_editability"]);
        if(!ed) ccl="#A0A0A0";
 
        colr=ccl;
 
 
    if(script && moLibrary.exists(script)) {   // if delegate script
                            
       var TIPtext="", ACTIONclick="", 
           xfield=X1+d, yfield=y, vfield=v, f_cod=Ypx.fcode, f_lock=lock;  // colr color
       
       //if(!v || v == "0") {} else { eval(moGlb.delegates[script]); }
       fillStyle = colr; 
 
       try { eval(moLibrary.getSource(script)); } catch(e) { moDebug.log("cell script error! "+script); moDebug.log(e);}
       
       if(TIPtext) {Arrtip.push( { l:X1+d, t:y, w:x21, h:Yi, tx:TIPtext} );   }
       //if(ACTIONclick) {arrACTION[f_cod]=[ACTIONclick,xfield,x21];}
        
    } else {
        if(type==4 || type==2) { try { t=v; if(moGlb.isset(aFilter[fieldb][v])) t=moGlb.langTranslate(aFilter[fieldb][v]); }catch(e){}  }
        
       if(moCnvUtils.measureText(t,f)>x21) t=moGlb.reduceText(t,f,x21);
 
        fillStyle=colr;
        if(t=="null" || t===null) t="";
        fillText(t,X1+d+7,y+8+(Yi-7)/2); 
    }

 restore(); 
  }
 
}}




function clipRow(X1,X2,y,lock){ 
  with(this) with(BDN) {
  var lr=OH+Hlw;
  if(!lock) {  X1-=optr, X2-=optr; 
               if(X2<=Hlw || X1>=lr) return false;
               if(X1<Hlw) X1=Hlw;
               if(X2>lr) X2=lr;  }
 
   X1+=cnvLS, X2+=cnvLS;
   
   beginPath(); // rect(X1,y,X2-X1,Yi); 
   moveTo(X1,y); lineTo(X2, y); lineTo(X2, y+Yi);  
   lineTo(X1, y+Yi); closePath();
   clip();
   
   return [X1,X2];
}}                                                                   



// 0 cancella   1 disegna separazione lock/unlock
function ClearB(m){
  with(this) with(BDN) {
    if(!m) clearRect(0,0,cnvLS+Hlw+OH,SH);
    else{ fillStyle=colrs[6];
          fillRect(cnvLS+Hlw-1,0,1,SH); }
}}



function fMaskLoad(x,y){
  with(this){
  var mj=moGlb.createElement("maskmodule"+name,"div",jj);
  
  if(!NOEVT){
    mj.style.left=0; mj.style.top=0; mj.style.width="100%"; mj.style.height="100%"; mj.style.zIndex=10; mj.style.display="block";
    
    mj.onmousedown=function(e){ moEvtMng.cancelEvent(e); return false; }
    mj.onmouseover=function(e){ moEvtMng.cancelEvent(e); return false; }
    
    mj.innerHTML="<img src='"+IM+"ico_loading.gif' style='position:absolute;left:"+(x-12)+"px;top:"+(y+4)+"px;' />";     
  } else {
    if(NOEVT>32) { mj.innerHTML=""; mj.style.display="none"; NOEVT=0; return; } 
  }
    NOEVT++;
setTimeout(name+".fMaskLoad()",200);
}}








function Cell(cnv,x1,x2,s){
  with(this) with(cnv) {
      
      fillStyle=colrs[s]; fillRect(x1+1,0,x2-x1,HeadH);
       
      lineWidth=1; beginPath(); strokeStyle=colrs[6];  
      moveTo(x1+1,HeadH-0.5); lineTo(x2-0.5,HeadH-0.5); lineTo(x2-0.5,1);  
      stroke(); closePath();
}}   


function hCell(i){
  with(this) with(CNV) with(Header[i]){
 
  var X1=x1, X2=x2, s=0, lr=OH+Hlw;
  if(i>=NL) X1=x1-optr, X2=x2-optr;
  if(X1>lr) return;
  if(X2>=lr) X2=lr;
 
  if(i==OVR) s=7;
  if(i==SEL) s=4;
  
  Cell(CNV,X1,X2,s);
  
    // icon left (sort, filter, search) 
  var asrt=SeSort(fieldb), Srt=asrt[0], flb=SeFilterby(fieldb), Srh=SeSearch(fieldb);           // sort   e/o  (filterby | search)
  
  
  // text
  var f="bold 13px opensansR", d=(Srt)?16:8;
  d+=(flb<0 && Srh<0)?0:15; 
  
  var x21=X2-X1-d-10, t=moGlb.langTranslate(label);     
  font=f; 
  textBaseline="alphabetic"; 
  textAlign="left"; 
  fillStyle=colrs[2];  
        
  if(moCnvUtils.measureText(t,f)>x21) t=moGlb.reduceText(t,f,x21);                        
  fillText(t, X1+d, 15.5);
       
  // dropdown
  if(!Cur && (s==7 || (s==4 && i==OVR))) moCnvUtils.cnvIconFont(CNV,58445,X2-18,3,16,colrs[6]);  
      
  // arrow sort    1 asc   -1 desc
  if(Srt) { var l=X1+1;  
     fillStyle="#666";                                          //  #@# moColors.tgArrow; 
     beginPath();
     var t0=11,t1=15; if(Srt>0) t0=15,t1=11; 
     moveTo(l+2.5,t0); lineTo(l+9.5,t0);
     lineTo(l+6,t1); closePath();  fill(); }
     
  if(flb>-1 || Srh>-1) { var l=X1+d-16;   
   moCnvUtils.cnvIconFont(CNV,58456,l,5,12,colrs[2]);  
  }
         
      
  if(i==(NL-1)) { beginPath();  // rect(Hlw,0,OH,HeadH);
                  moveTo(Hlw,0); lineTo(Hlw+OH, 0); lineTo(Hlw+OH, HeadH);  
                  lineTo(Hlw, HeadH); closePath();
                  clip(); }     
      
}} 


//   fldb = header[x].fieldb         
//   xchg=se Srt==0/-1 pone Srt=1 | se Srt=1 pone Srt=-1      --->  aggiorna/crea in SORT[]      
//   Ms xchg con valore 1 /-1

function SeSort(fldb, xchg, Ms){         // return [Srt, pos]  Srt:0 no, 1 asc, -1 desc  | pos se multisort >0
  with(this){    
 
  
    if(!xchg) { xchg=0; Ms=0; }  if(xchg && !Ms) Ms=0;
    
    
    
    var Srt=0;
    for(var r=0;r<SORT.length;r++) if(SORT[r].f==fldb) { Srt=(SORT[r].m=='ASC')?1:-1; break; }  

    if(xchg) {  if(Ms) { S=(Ms<0)?'DESC':'ASC'; 
                         if(!Srt) SORT.push({f:fldb, m:S }); 
                         else SORT[r].m=S;
                         Srt=Ms;
                } else {
    
              if(Srt==1) { Srt=-1; SORT[r].m='DESC'; }
              else { if(!Srt) { SORT.push({f:fldb, m:'ASC'}); }    // add sort        
                     else SORT[r].m='ASC';
                     Srt=1; }
                }
            }                  
  return [Srt,r];
}}




function SeFilterby(fldb){     // restituisce posizione in FILTER se presente else -1 non trovato
  with(this){
  
  for(var r=0;r<FILTER.length;r++) if(FILTER[r].f==fldb) return r;
  return -1;

}}


function SeSearch(fldb){
  with(this){

  for(var r=0;r<SEARCH.length;r++) if(SEARCH[r].f==fldb) return r;
  return -1;

}}







// EVENTI --------------------------------------------------
 

function bodydown(e){  moEvtMng.cancelEvent(e);
with(this){
 
  mCK=[]; noWH=0;
  if(dwnBD) return false;
  
  moMenu.Close();
 
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);
 
  eval("Vscr=Vscr"+name);
  eval("Oscr=Oscr"+name); 
  mCK[0]={ iy:P.y, ix:P.x, By:Vscr.ptr, Bx:Oscr.ptr};
        
  dwnBD=1;
 
  if(!isTouch){
   TGJ.style.display="block";
   eval("TGJ.onmousemove=function(e){"+name+".bodymove(e); }");
   eval("TGJ.onmouseup=function(e){"+name+".bodyup(e); }");    
  }   
  return false; 
}}


function bodymove(e){ moEvtMng.cancelEvent(e);
with(this){
 
  var i,dx,dy, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), ex=P.x, ey=P.y;

  if(!mCK.length) return false;

  // drag scrollbars
  if(dwnBD==1) {
    if(ex<(zL+Hlw)) noWH=0;
    else {
      dx=Math.abs(ex-mCK[0].ix);
      dy=Math.abs(ey-mCK[0].iy);  
      if(dx>dy) noWH=1; else noWH=0;  // 0=vertical   1=orizzontal
    }
  }
  
  dwnBD++;
  if(dwnBD>2 && dwnBD&1){
 
    if(noWH){
      eval("Oscr=Oscr"+name);
      i=mCK[0].Bx+(mCK[0].ix-ex);
      addStp=defStp/(isTouch?3:2);
      Oscr.PosPtr(i);
    }else{
      eval("Vscr=Vscr"+name);
      i=mCK[0].By+(mCK[0].iy-ey);
      addStp=defStp/(isTouch?3:2);
      Vscr.PosPtr(i);
    }
  }

  return false;
}}



function overMove(e){ 
with(this){

  var Bt=zT+mgT+HeadH+msgH, Bl=zL+mgL, 
      P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e),
      p, bovr=-1;
 
    // tooltip
    Xps=P.x-Bl, Yps=P.y-Bt; overTip();
    if(!Clc) cicleTip(0);
    if(WTP) delTip();
    
    for(var r=0;r<Abody.length;r++) {
      p=Bt+Abody[r].y;
      if(P.y>=p && P.y<(p+Yi)) { bovr=Abody[r].f; break; }
    }    
 
    // over rows
    if(bovr!=bOVR) { bOVR=bovr; drawbodyover(p-Bt); } 
    
  moEvtMng.cancelEvent(e);          
}}



 


function bodyup(e){  moEvtMng.cancelEvent(e);
with(this){   
  
  if(!isTouch) TGJ.style.display="none";
 
 
  // if click
  if(!dwnBD || dwnBD>3) { dwnBD=0; return false; } 
 
  
  var c2=36, Bli=zL+mgL+22, Blf=Bli+c2, Bt=zT+mgT+HeadH+msgH,
      P={ x:mCK[0].ix, y:mCK[0].iy }, 
      Px=P.x-zL-mgL, Py=P.y-Bt,
      nd=arrNd.length, fc=0, md=0, p;
 
  mCK=[]; dwnBD=0;
  
    // if Tree open/close    
    if(nd){
      for(var r=0;r<nd;r++) with(arrNd[r]) {
        if(Px>=l && Px<=(l+32) && Py>=t && Py<(t+Yi)) {     //  moGlb.alert.show(Px+" | "+P.y+" | "+oby.fcode);
          fc=parseInt(oby.fcode); 
          
          if(fc==editing) { moGlb.alert.show(moGlb.langTranslate("<br>You are editing this item.")); return false; }
          
          if( revList.indexOf(fc)>-1 ) { 
              moGlb.alert.show(moGlb.langTranslate("Node was modified.\nUpdate page before open/close\nthis node."));
              return false; 
          } 
          if(oby.open) {   // se chiudere
            if(testCloseP(oby)) CloseP(oby);
            else return false;                  
          } else OpenP(oby);                  // apre/chiude
          
          fMaskLoad(l+4,t+mgT+HeadH-1+msgH);
          return false;  }    
      }
    }
 
    // edit 1 or check 2 or locked
    for(var r=0;r<Abody.length;r++) {
      p=Bt+Abody[r].y;
      if(P.y>=p && P.y<(p+Yi)){ 
        fc=Abody[r].f; md=1;
 
        if(fc==editing) { moGlb.alert.show(moGlb.langTranslate("<br>You are editing this item.")); return false; }
        
        if(P.x>Bli && P.x<Blf) md=2;   
        if(P.x<Bli) {   // locked user
          if(arrMB[fc]){
            Yps+=Yi-6;
            wiewTip(-34,p,0,Yi,moGlb.langTranslate("Locked by")+": "+arrMB[fc][1]);
          }
          return false;
        }                      
        
        break; 
      }
    }
 
    //  f_type_id not supported are not cliccable!
    if(fc){ 
      var atyp=ftype.toString().split(","), tp=JStore[fc].f_type_id + "";
      if(ftype >= 0 && atyp.indexOf(tp)==-1) { 
        moGlb.alert.show(moGlb.langTranslate("This Item is not supported in this module!"));  
        return false; 
      }
    }
 
    if(md) var afc=JStore[fc], lk=afc.selected;
    
    if(ispopup && md==1) md=2;
    
    // Edit md=1 or arrACTION (ACTIONclick)
    if(md==1 && !checkmode) {  

      // ACTIONclick
    if(typeof(arrACTION[fc]) != "undefined") {
      var lx=arrACTION[fc][1], wx=lx+arrACTION[fc][2];
       if(Px>lx && Px<wx) {
        var fnc=arrACTION[fc][0];
        if(fnc) { eval(fnc); return false; }
       }
    }
 
    var ed=JStore[fc].f_editability;

      // if readonly edit  
      if(lk==1 || !ed) {
        if(!ispopup && crud) eval(crud).edit(fc);
        return false;
      }                   

      //if(!lk || lk==2 || lk == 3) {
          
      if(lk==3) return false;
      
       // blocco per editing     
      if(!lkEdit) { 
        moGlb.loader.show(moGlb.langTranslate("Loading..."));  
        lkEdit=fc; 
        olEdit=0; 
        jaxULock([fc],3,1);
      } else { 
            olEdit=0;
            jaxULock([lkEdit],0,0); 
            for(var r=0;r<arrCHK.length;r++) if(arrCHK[r]==lkEdit) { arrCHK.splice(r,1); break; }   // tolgo edit
              if(fc!=lkEdit)  { lkEdit=fc; jaxULock([fc],3,1); }
      } 
                       
         // if(lk == 3) { if(crud) eval(eval(crud).pname).reset(); }             
        // }           
    }
             
    // ceckking md=2     se lk=1 o 3  nessuna azione
    if(md==2) {                  
      if(!lk) jaxULock([fc],2,1);                 //  [fcode], tipo,  modo: 0=unlock  1=lock   2=update
      if(lk==2) jaxULock([fc],0,0);
   }        
    return false;   
}}



 

function bodyout(e){
with(this){   
  WHEELOBJ="";  
 
  if(bOVR!=-1) { bOVR=-1;  }
  OBDJ.innerHTML="";
  
  // timeline
  if(modTM) eval(modTM+".OBDJ.innerHTML='';"); 
  
  tipOVR=-2;              
  if(WTP) delTip();   
}}




function testCloseP(yp){
with(this){   
  var fc=yp.fcode, rtr=yp.rtree, pp=yp.levl, ni=yp.nch, i=rifY[rtr], ny=Ypos.length, fi;
  i++;
  while(i<ny && Ypos[i].levl>pp) {
    fi=Ypos[i].fcode;
    if(fi==lkEdit) { moGlb.alert.show(moGlb.langTranslate("Close Request")); return 0; }
    i++;
  }
  return 1; 
}}


function Vbarpos(i){
with(this){
  vptr=i; bOVR=-1;  
   addStp=defStp;
  sedrw=2; draw();  
  drawbodyover(-1);
}}


function Obarpos(i){
with(this){
  optr=i; 
   addStp=defStp;
  sedrw=2; draw();
}}


function endScroll(){
with(this){
  addStp=0;
  sedrw=3; draw(1);
}}


/*
tipo lock:
0   non selezionato
1   bloccata da altro user (sola lettura)
2   checkbox
3   in Editing

*/ 


function jaxULock(fc,ty,md){         // fc array fcode    modo: 0=unlock  1=lock    // 2=update
  with(this){
 
   var pr,afc,i, cfc=moGlb.cloneArray(fc);
 
if(!checkmode && this.moduleName.indexOf('_pp') == -1) {
   lockFcodeGlobal = cfc;
  var par=JSON.stringify({ table:1, ftype:ftype, fname: fname, fcode:cfc, typ:ty, mode:md, user:moGlb.userId, self: self, ignoreSelector: ignoreSelector, skipwf: skipwf, pcross_inv:pcross_inv });  
  Ajax.sendAjaxPost( moGlb.baseUrl+"/treegrid/jax-lock", "param="+par, this.name+".retULock" );
}
 
  if(md==2) return; // update
 
  // attivo/disattivo lucchetto in JStore (per elementi con più padri!)

  for(i=0;i<cfc.length;i++){ 
     if(!JStore[cfc[i]]) continue;
     JStore[cfc[i]].selected=ty;  
  }
 
  // array dei checked
  for(i=0;i<cfc.length;i++){
      if(ty>=2 && md==1) { arrCHK.push(cfc[i]); }
      if(!ty && !md) { for(var r=0;r<arrCHK.length;r++) if(arrCHK[r]==cfc[i]) { arrCHK.splice(r,1); }}
  
  } //
  
  checkedIds=moGlb.cloneArray(arrCHK);
  setTimeout(name+".draw()",100);
}}





function retULock(a){     // se non -  codice non lockato
  with(this){
 
  var ri=JSON.parse(a),f, ark=[]; 
  if(!ri) ri=[];  
  if(moGlb.isset(ri.message)) moGlb.alert.show(moGlb.langTranslate(ri.message));      
  if(ri.length) {    
    for(var r=0;r<arrCHK.length;r++) { f=arrCHK[r];     
      if(ri.indexOf(f)==-1) ark.push(f);      // non valido!
    }
    arrCHK=moGlb.cloneArray(ark);
  } else {  
    aChecked=[];
    for(var r=0; r<arrCHK.length;r++){ 
      var v=arrCHK[r]; 
      if(v==lkEdit) continue;
      aChecked.push(v); 
    }
  }
  
  if(arrCHK.indexOf(lkEdit)==-1) { olEdit=0; lkEdit=0; sedrw=2; draw(); } 
  else { 
    if(!olEdit){ 
      olEdit=1;
      if(!ispopup && crud) eval(crud).edit(lkEdit);
    }
  } 
 
 sedrw=3; draw(3); 
}}



 


function deCheck(){  // deselected all check 
  with(this){
  
  var pr,afc, fc, n=arrCHK.length, ank=[]; 
  for(var r=0;r<n;r++) { fc=arrCHK[r]; if(fc==lkEdit) continue; 
  ank.push(fc);
   if(!JStore[fc]) {}
   else JStore[fc].selected=0; 
  }  
  if(checkmode) AND=[];
  jaxULock(ank,0,0);
}}



 

function timeUlock(){   // mantiene i lock e attiva quelli degli altri user  ogni 10 secondi?   (timelock)
  with(this){

    
  if(tUlk && J.style.display=="block") {    //  && J.style.display=="block"
                       
    // update dei selezionati
   if(arrCHK.length || lkEdit) jaxULock(arrCHK,2,2);
  
    // leggo i locked degli altri users
 /*   var par=JSON.stringify({ 
      table:1, 
      ftype:ftype, 
      fname: fname, 
      user:moGlb.userId, 
      self: self,
      ignoreSelector: ignoreSelector });
    
    Ajax.sendAjaxPost( moGlb.baseUrl+"/treegrid/jax-lock-other-user", "param="+par, this.name+".retotherLock" );
  */ 
    tUlk=0; 
  }
  
  
setTimeout(name+".timeUlock()", timelock);
}}



       
               
function retotherLock(rr){   // return locked by other user
  with(this){
      
  tUlk=1; 
  if(!rr) rr=[];

  var r1=arrLK.length, r2, pr, ri=[];
  for(var r=0;r<r1;r++) {
  
    if(!JStore[arrLK[r]]) continue;
     else JStore[arrLK[r]].selected=0;
  }

  
  arrMB={};
  for(r=0;r<rr.length;r++) {
    arrMB[ rr[r][0] ]=[ rr[r][1], rr[r][2] ];
    ri.push(rr[r][0]);
  }
 
  arrLK=ri;
 // if(moGlb.isset(arrLK.message)) moGlb.alert.show(moGlb.langTranslate(arrLK.message));
  r2=arrLK.length;   
  for(var r=0;r<r2;r++) {
       if(!JStore[arrLK[r]]) continue; 
       JStore[arrLK[r]].selected=1;      
  }

  if(!r1 && !r2) return;  
  sedrw=2; draw();
}}





 



//-------- tooltip ------------------------


function overTip(){
with(this){                        
  for(var r=0;r<Arrtip.length;r++) with(Arrtip[r]){
      if(Xps>l && Xps<(l+w) && Yps>=t && Yps<=t+h) { tipOVR=r; return;  }
  }
  tipOVR=-1;
}}
 
 

function cicleTip(z){
  with(this){

  Clc=1;
  if(tipOVR==-2) { Clc=0; return; }
  z++;
  
  if(Xps!=oXps || Yps!=oYps) z=0;  
  if(z>15 && tipOVR>-1) { 
    with(Arrtip[tipOVR]){ wiewTip(l,t,w,h,tx); }
    Clc=0; 
    return; 
  }
  oXps=Xps, oYps=Yps; 

setTimeout(name+".cicleTip("+z+")",50);
}}




function wiewTip(l,t,w,h,tx){             // { l:(oo-dt-4), t:y, w:t10, h:h, dal:p.t0, al:(p.t0+p.t1), prev:(p.t0-p.dt), des:"", fc:fc, tx:p.title  }
with(this){
  
  var rg=[], nrg=0, vx=0, mx=0, f="13px opensansR", ctj, tw,th,x,y, t="", ww=cnvLS+Hlw+OH, wmx=parseInt(ww*2/3), 
      ax,nax,rx,sx,cx,sep;

  rg=tx.split("\n");
  nrg=rg.length;
 
  for(r=0;r<nrg;r++){     
    vx=moCnvUtils.measureText(rg[r],f);     
    if(vx>wmx) {
      ax=rg[r].split(" ");
      nax=ax.length;
      sx="", cx=0;
      for(rx=0;rx<nax;rx++){
        cx+=moCnvUtils.measureText(ax[rx]+" ",f);
        if(cx>wmx) { sep="\n"; cx=0; } else sep=" ";
        sx+=ax[rx]+sep;
      } 
      rg[r]=sx;
    } 
  }
  
  tx=rg.join("\n");
  rg=tx.split("\n");
  nrg=rg.length;
 
  for(r=0;r<nrg;r++){     
    vx=moCnvUtils.measureText(rg[r]+"",f);     
    if(vx>mx) mx=vx; 
  }

  tw=mx+11, th=nrg*16+11;
  x=Xps-parseInt(tw/3); if(x<(cnvLS)) x=cnvLS; if((x+tw)>ww ) x=ww-(tw+6); 
  if(l<0) x=cnvLS+l;
  y=Yps-(th+10); if(y<2) y=Yps+5; 
    
  y+=(mgT+HeadH+msgH); 
    
  ctj=TLTPJ.getContext("2d");
  TLTPJ.style.left=x+"px"; TLTPJ.style.top=y+"px";  
  TLTPJ.width=(tw+5), TLTPJ.height=(th+5);
  TLTPJ.style.display="block";
  
  // console.log("x "+x+" |y "+y+" l "+(TLTPJ.style.left))
 
   with(ctj){    
     fillStyle=colrs[2];
     moCnvUtils.roundRect(ctj, 0, 0, tw, th, 1);
     fill();

     textBaseline="alphabetic"; textAlign="left"; font=f;
     fillStyle="#FFF";
     
     for(r=0;r<nrg;r++) fillText(rg[r],6,18+r*16); 
   } 
 
  setTimeout(name+".WTP=1;",1000);  
}}



function delTip(){
  with(this){

  TLTPJ.style.display="none";
  WTP=0;
}}

 
//----------------------------------- 








function drawbodyover(p){
  with(this){ 
  
   // se timeline  
  if(modTM) eval("var otmj="+modTM+".OBDJ;") 
  
  if(bOVR<0 || p==-1) { OBDJ.innerHTML=""; if(modTM) otmj.innerHTML="";  }
  else {
  var o=10, op="opacity:."+o+";filter:alpha(opacity="+o+");-moz-opacity:."+o+";", c="#404040"; 
   
  var ddvv="<div style='position:absolute;left:0;top:"+(p)+"px;width:100%;height:"+(Yi-1)+"px;background-color:"+c+";"+op+"'></div>";
  OBDJ.innerHTML=ddvv;
  if(modTM) otmj.innerHTML=ddvv;
  }

}}




function headown(e){
  with(this){   
 
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s=oversel(P.x), ovr=s[0]; 
  Cur=(s[1]==2)?1:0;       
  if(OVR!=ovr) OVR=ovr;
  PXi=P.x, PYi=P.y;     
 
 if(!NMM){ NMM=1; ccMm=0;
 
  // creo movimento su maskera
  maskj=moGlb.createElement("maskDrag"+name,"div");
  
    with(maskj.style){
    zIndex=moGlb.zdrag+1; 
    left=0; 
    top=0; 
    width='100%'; 
    height='100%';    
    backgroundColor="#ff2";
    opacity=0.0;
  }
  
  // creo evideziazione su div
  mask2j=moGlb.createElement("mask2Drag"+name,"div");
  
  with(mask2j.style){
    zIndex=moGlb.zdrag; 
    left=0; 
    top=0; 
    width='100%'; 
    height='100%';    
  }

  eval("maskj."+EVTMOVE+"=function(e){ maskmove(e); }; oldmouseup=document."+EVTUP+"; document."+EVTUP+"=function(e){ maskup(e); };  ");
  } else NMM=0;
  
  
  moEvtMng.cancelEvent(e);
  return false; 
}}






function maskmove(e){
  with(this){

  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s=overmoveh(P.x), stri;        // s=[over, mid, X1, X2, pri, ins, scroll]           scroll 0 -10 -20
  
  if(!ccMm) {
  
  // sposta else  Drag
  if(!Cur) stri="<img id='idtmpins' src='"+IM+"insert_field.png' style='position:absolute;display:none;' />"+
                    "<div id='idtmpdrgh' style='position:absolute;width:158px;height:30px;background-color:"+colrs[2]+";opacity:0.9;border-radius:2px;'>"+
                    "<div id='idtmpsimb' style='position:absolute;left:6px;top:5px;' >"+moCnvUtils.IconFont(58827,16,"#B00")+"</div>"+
                    "<div style='position:absolute;left:30px;top:6px;width:120px;height:16px;font:13px opensansR;color:#FFF;overflow:hidden;'>"+
                    moGlb.langTranslate(Header[OVR].label)+"</div></div>";
  else {   
         iext=s[0]; if(!s[1]) iext=s[4];
         RX1=Header[iext].x1 + mgL+cnvLS+zL;
         if(iext>=NL) RX1-=optr;  
         stri= "<canvas id='idtmpdrgh' style='position:absolute;left:"+RX1+"px;top:"+(zT+mgT+HeadH)+"px;' width='40' height='"+(SH+1)+"'></canvas>";
       }
   mask2j.innerHTML=stri;     
  }
  
  
  ccMm++;
  
  if(!Cur) {   // move
  var tmprj=mf$("idtmpdrgh");
  tmprj.style.left=(P.x+16)+"px"; tmprj.style.top=(P.y+16)+"px"; 
  
  var li=0,ti=0,di="none", srins=moCnvUtils.IconFont(58827,16,"#B00");
  if(s[5]){ di="block", li=(s[1])?s[3]:s[2], ti=zT+mgT-4;
            srins=moCnvUtils.IconFont(58826,16,"#0B0"); }
  
  // if scroll left:-10 right;-20
  if(s[6]<-5) {
  if(s[6]==-10) { srins=moCnvUtils.IconFont(58774,16,"#FFFFFF"); if(!sescr) { sescr=1; scrollO(-1); } }
  if(s[6]==-20) { srins=moCnvUtils.IconFont(58775,16,"#FFFFFF"); if(!sescr) { sescr=1; scrollO(1); } }  
  } else sescr=0;

  var tmprj=mf$("idtmpins");
  tmprj.style.display=di; tmprj.style.left=(li-(6) )+"px"; tmprj.style.top=ti+"px";   // (Browser>2)?8:6
  mf$("idtmpsimb").innerHTML=srins;
  
  
  } else {   // resize
  
   szW=P.x-RX1; if(szW<32) szW=32;
 
   var cj=mf$("idtmpdrgh"), cnn=cj.getContext("2d");
   
   cj.width=szW;
   with(cnn){
   clearRect(0,0,szW,SH);
   fillStyle=fillStyle="rgba(224,224,224,0.5)";                      // #@# moColors.maskMoveFill; 
   fillRect(0,0,szW,SH);
   lineWidth=1;
   strokeStyle="#808080";                                            // #@# moColors.maskMoveStroke;  
   beginPath();
   moveTo(0.5,0);
   lineTo(0.5,SH+0.5);
   lineTo(szW-0.5,SH+0.5)
   lineTo(szW-0.5,0);
   closePath();
   stroke();
   }
  
  }
  
 
  moEvtMng.cancelEvent(e);
  return false;
}}






function maskup(e){
  with(this){

  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s;

  if(P.x==PXi && P.y==PYi) {                  // click
         
      if(!Cur){
      s=oversel(P.x), sel=s[0]; 
      if(s[1]) createmenu(sel); 
      else { if(!fnoAct()) { 
             if(SEL!=sel) {  
               /* actclearfilter(1);   */
             SORT=[]; 
               
             SEL=sel; } 
             NMM=0; 
             SeSort(Header[sel].fieldb,1); ActRefresh();                      //   sort click header
             sedrw=1; draw(); 
             }   
          }
      }
  } else {                                    // drag   
     
     if(!Cur){   // order field    
              s=overmoveh(P.x);
              if(s[5]) {
              var i0=OVR, i1=s[0]+s[1]; lk=Header[s[0]].lock, dse=0;
              if(i1<=SEL && OVR>SEL) dse=1; 
              if(i1>SEL && OVR<=SEL) dse=-1;
              var v=Header.splice(i0,1);
              if(i1>OVR) i1--;

              Header.splice(i1,0,v[0]); Header[i1].lock=lk;
              
              if(OVR==SEL) SEL=i1; else SEL+=dse;  // mantengo selezioanto      
              fmenuList();
              updHeader(); 
                
              } else {            // animation return 
                      var iij=mf$("idtmpdrgh");
                      if(!iij) { endAnim();  }
                      else {
                       var x=parseInt(iij.style.left), y=parseInt(iij.style.top);  
                       with(Header[OVR]){ var xa=x1+mgL+cnvLS+zL, ya=zT+mgT;   }
                       var nxa=Math.abs((xa-x)/50), nya=Math.abs((ya-y)/50), nn=parseInt((nxa+nya)/2); if(nn>8) nn=8;
                       if(nn<2) nn=2; var dx=parseInt((xa-x)/nn), dy=parseInt((ya-y)/nn);
                       
                       eval("maskj."+EVTMOVE+"='';");
                       headRitAnimat(x,y,dx,dy,0,nn);
                      }
               if(isTouch) moEvtMng.cancelEvent(e);      
               else return false;
                }  
     
     } else {   // resize field
     
     with(Header[iext]){   wdth=szW;   }  
     updHeader();
     
     }
  
      OVR=-1; sedrw=3; draw(); 
      NMM=0;
      }
      
  var pj=maskj.parentNode, ppj=mask2j.parentNode; 
      pj.removeChild(maskj);
      ppj.removeChild(mask2j);
  
  eval("document."+EVTUP+"=oldmouseup");       
  sescr=0;    
  if(isTouch) moEvtMng.cancelEvent(e);      
  else return false;
}}



function headRitAnimat(x,y,dx,dy,t,nt){
  with(this){
  
  t++;
    var nx=x+dx*t, ny=y+dy*t, tmprj=mf$("idtmpdrgh");
    tmprj.style.left=nx+"px";
    tmprj.style.top=ny+"px"; 

  if(t>nt) { endAnim(); return false;  }

setTimeout(name+".headRitAnimat("+x+","+y+","+dx+","+dy+","+t+","+nt+")",50);
}}


function endAnim(){
  with(this){
  
  var pj=maskj.parentNode, ppj=mask2j.parentNode;  
  pj.removeChild(maskj);
  ppj.removeChild(mask2j);
  
  eval("document."+EVTUP+"=oldmouseup");       
  sescr=0; OVR=-1; sedrw=3; draw();
  NMM=0;        
  return false; 
}}



function scrollO(d,t){  if(!t) t=0;  
  with(this){   if(!sescr) return;
 
  t++;
  if(t>3){
  optr+=d*16; 
  if(optr<0) optr=0;
  if(optr>(Huw-OH) ) optr=Huw-OH;

  eval("Oscr=Oscr"+name); 

  Oscr.Resize( { ptr:optr } ); 
  if(!optr || optr==(Huw-OH) ) return;
  }

setTimeout(name+".scrollO("+d+","+t+")",60);
}}



function headmove(e){    
  with(this){  if(NMM) return;
   
   var P=moEvtMng.getMouseXY(e), s=oversel(P.x), ovr=s[0], cur=Curs[s[1]], oC=Cur;
   Cur=(s[1]==2)?1:0;
 
   if(OVR!=ovr || oC!=Cur) { OVR=ovr; sedrw=3; draw(); }
   CNJ.style.cursor=cur;
   
   if(isTouch) moEvtMng.cancelEvent(e);      
   else return false;  
}}
                                       

function oversel(x){    // return  [over, cur]   cur: 0:normal  1:pointer  2:col-resize
  with(this){   
  
   if(ispopup) x-=4;
  
   var dx=mgL+zL+cnvLS, lr=OH+Hlw, ovr=-1, X1=0,X2=0, cur=0, pri=-1, mid=0, x21=0;    
   for(var i=0;i<NT;i++)  with(Header[i]){  if(!hidd) continue;
   if(i>=NL) { X1=x1-optr, X2=x2-optr;  if(X2<=Hlw) continue;
               if(X1<Hlw) X1=Hlw;  if(X1>=lr) break;
               if(X2>lr) X2=lr;  }
   else X1=x1, X2=x2;        
   X1+=dx, X2+=dx,  x21=(X2-X1)/2;
   if(x>=X1 && x<X2) { ovr=i; mid=(x<(X1+x21))?0:1;  break; }  
   pri=i;  
   }
   if(x>(X2-24)) cur=1;
   if(x>(X2-6) || x<(X1+6)) cur=2;
   
   if( (pri<0 && !mid)  ) cur=0;  
   
   return [ovr,cur,pri,mid];   
}}



function overmoveh(x){    // return  [over, mid, X1, X2, pri, ins, scroll]   mid: 0:left  1:right    pri=field orev  nxi=field next  ins=if insert is possible
  with(this){
   var dx=mgL+zL+cnvLS, lr=OH+Hlw, ovr=-1, X1=0,X2=0, mid=0, x21=0, rg=12, pri=-1, nxi=-1, ins=1, PRI=-1, NXI=-1, sc=0; 
   
   if(x>(dx+Hlw) && x<(dx+rg+Hlw)) sc=-10;
   if(x>(dx+lr-rg) && x<(dx+lr)) sc=-20;
      
   for(var i=0;i<NT;i++) with(Header[i]) {  if(!hidd) continue;
   if(i>=NL) { X1=x1-optr, X2=x2-optr; if(X2<=Hlw) continue;
               if(X1<Hlw) X1=Hlw;  if(X1>=lr) break; if(X2>lr) X2=lr;  }
   else X1=x1, X2=x2;        
   X1+=dx, X2+=dx, x21=(X2-X1)/2;
   if(x>=X1 && x<X2) { ovr=i;  mid=(x<(X1+x21))?0:1;  break;  }
   pri=i; }  
    
   // search PRI e NXI di OVR;
   var b=0;
   for(i=0;i<NT;i++) with(Header[i]) { if(!hidd) continue; 
   if(b) { NXI=i; break; }
   if(i==OVR) { b=1; continue; }
   PRI=i; }

   if(ovr<0 || ovr==OVR || ((ovr==NXI && ovr!=NL) && !mid) || ((ovr==PRI && ovr!=(NL-1) ) && mid) ) ins=0;
     
   return [ovr,mid,X1,X2,pri,ins,sc];   
}}




function headout(e){
  with(this){   
  if(NMM) return;
  OVR=-1; 
  sedrw=1; draw();
}}


                          


// global search ---------------------------------------------------------------
function globSearch(){
  with(this){
  
      // popup search
      var jbkm=moGlb.createElement("idglbsrc","div");

        jbkm.style.left="0";
        jbkm.style.top="0";
        jbkm.style.width="240px";
        jbkm.style.height="100px";
        jbkm.style.backgroundColor="#FFF";

       win = moComp.createComponent("idglbsrc_win", "moPopup", {
        width:240, height:100,
        zindex: 1090, contentid:'idglbsrc', title: moGlb.langTranslate("Search text")}, true);
        win.show();
      GlobalSearch.Start(name,"idglbsrc",Header,win);
}}



// menu  -----------------------------------------------------------------------
function fieldlist(){
  with(this){

  if(!bkMenu){
  
      // popup bookmark multiselect & grouped
      var hp,jbkm=moGlb.createElement("idbkmselect","div");
      hp=moGlb.getClientHeight()-220;

        jbkm.style.left="0";
        jbkm.style.top="0";
        jbkm.style.width="320px";
        jbkm.style.height=hp+"px";
        jbkm.style.backgroundColor="#FFF";

       win = moComp.createComponent("idbkmselect_win", "moPopup", {
        width: 320, height: hp,
        zindex: 1100, contentid: 'idbkmselect', title: moGlb.langTranslate("Setting grid columns")}, true);
        win.show();
      BkmSelect.Start(name,"idbkmselect",Header,win);

  } else {
 
    var l=zL+W-mgR-VW, t=zT+mgT-2, w=VW, h=HeadH, rr=0,r;   
    tmpList=[];
    for(r=0;r<menulist.length;r++) { 
      if(menulist[r]==-1) continue;
      tmpList[rr]=menulist[r];
      rr++;
    }  
    moMenu.Start(name+".tmpList",{l:l, t:t, w:w, h:h},2);    
  }
}}



function viewmenucheck(){
  with(this){
  var l=zL-mgL+cnvLS-mecn, t=zT+mgT-2, w=VW, h=HeadH; 
  moMenu.Start(name+".menuChecked",{l:l, t:t, w:w, h:h},2);
}}





function createmenu(i){
  with(this) with(Header[i]){
 
  // updates Fmenu
  Fmenu[3].Disable=(i==SEL)?1:0;   // disable add filter if selected
  Fmenu[3].Sub[2].Disable=1;
  
  sef=1;
  if(type==3 || type==4 || type==2) { 
                 Fmenu[7].Sub=[];
                 Fmenu[3].Sub[2].Sub=[];

               var fly=SeFilterby(fieldb), fsl=-1, stu, rr;  
               if(fly>-1) fsl=FILTER[fly].v; 

                 var tx;  
                 for(var r=0;r<((type==4 || type==2)?(Filterby[fieldb].length):(FilerData.length));r++) {
                 
                 if(type==4 || type==2) tx=Filterby[fieldb][r].Txt, rr=Filterby[fieldb][r].Value;
                 else tx=FilerData[r].Txt, rr=r;
                 
                 stu=(rr==fsl)?1:0;
                 
                 Fmenu[7].Sub[r]={ Txt:tx, Status:(i==SEL)?stu:0, Type:3, Group:((i+20)*10), Icon:['','checked'], Fnc:name+".actfilterby("+i+",'"+rr+"',0,"+type+")" };  
                 Fmenu[3].Sub[2].Sub[r]={ Txt:tx, Status:stu, Type:3, Group:((i+20)*10), Icon:['','checked'], Fnc:name+".actfilterby("+i+",'"+rr+"',1,"+type+")" };  
                 }

                 sef=0; 
                 Fmenu[3].Sub[2].Disable=0;
  }             

  if(type==5) Fmenu[3].Disable=1;            

  Fmenu[7].Disable=sef;    // disab filter se non campo type 2, 3 o 4
  
  
  
  // se SORT attivo
  var asrt=SeSort(fieldb), Srt=asrt[0], Srp=asrt[1]; 
  Fmenu[0].Status=0; Fmenu[1].Status=0;
  Fmenu[3].Sub[0].Sub[0].Status=0; Fmenu[3].Sub[0].Sub[1].Status=0;
  
  Fmenu[0].Fnc=name+".actsort("+i+",1)";
  Fmenu[1].Fnc=name+".actsort("+i+",-1)";
  Fmenu[3].Sub[0].Sub[0].Fnc=name+".actaddsort("+i+",1)";
  Fmenu[3].Sub[0].Sub[1].Fnc=name+".actaddsort("+i+",-1)";
   
  if(Srt) { if(!Srp){       // [0]/[1]
  if(Srt>0) Fmenu[0].Status=1; 
  if(Srt<0) Fmenu[1].Status=1; 

  } else {         // [3].Sub[0].Sub[0]/[1]
  if(Srt>0) Fmenu[3].Sub[0].Sub[0].Status=1; 
  if(Srt<0) Fmenu[3].Sub[0].Sub[1].Status=1; 

  }}
  
  
  // Fnc hidden  10
  Fmenu[10].Fnc=name+".acthidden("+i+")";
 
  // hyerarchy
  Fmenu[9].Status=asHy; 
    
  // Fnc lock   8
  Fmenu[8].Fnc=name+".actlock("+i+")";
  Fmenu[8].Status=Header[i].lock;
 
 
  // Search  6  
  if(type==4 || type==2) { Fmenu[6].Disable=1;
             //Fmenu[3].Disable=1;
  }
  else { 
      
  Fmenu[6].Disable=0;
  Fmenu[6].Fnc=name+".actsearch("+i+","+type+",0)";    
  Fmenu[3].Sub[1].Fnc=name+".actsearch("+i+","+type+",1)";     
  }  
     
  NMM=1;
  var X2=x2, lr=OH+Hlw;
  if(i>=NL) X2=x2-optr;
  if(X2>=lr) X2=lr;
  var l=zL+mgL+cnvLS+X2-20, t=zT+mgT, w=20, h=HeadH;
  
  moMenu.endFNC=name+".NMM=0; "+name+".headout();";                 // NMM   menu closed
  
  moMenu.Start(name+".Fmenu",{l:l, t:t, w:w, h:h},2);
}}


function fnoAct(){   
  with(this){ 
  
  if(!ispopup && crud) eval(eval(crud).pname).reset();  // cancel edit
  
  return 0;
}}

 

// Hidden
function acthidden(i){      // v= -1 da Ovr     m=stato 
  with(this){
    if(fnoAct()) return;
    
    
    with(Header[i]){ hidd=(hidd)?0:1;
                     menulist[i+1].Status=hidd; 
                     if(lock) { actlock(i); return; }
                    }
  updHeader(); 
  sedrw=2; draw();
}}
  


// Lock/unlock
function actlock(i){      // v= -1 da Ovr     m=stato 
  with(this){     if(fnoAct()) return;
    with(Header[i]){ lock=(lock)?0:1;  if(!lock) NL--;  }
 
    var a=Header.splice(i,1);
    Header.splice(NL,0,a[0]);
 
    fmenuList();
    updHeader(); 
    sedrw=2; draw(); 
}}



// view all field
function view_all_field(){      // v= -1 from Ovr     m=status 
with(this){
  var i;
  for(i=0;i<Header.length;i++) with(Header[i]){
   // if(hidd) continue;
    hidd=1;
    menulist[i+1].Status=1;                  
  }
  updHeader(); 
  sedrw=2; draw();
}}



function acthierarchy(){      // as grid/tree
with(this){    if(fnoAct()) return;
  
  asHy=asHy?0:1;       
   
  ActRefresh();
}}
  


function actsort(i,v,z){      // i=Header   v= 1 / -1
with(this) with(Header[i]){     if(fnoAct()) {  return; }

    if(!z) z=0;
    if(i!=SEL) { 
       SORT=[]; /* actclearfilter(); */
       ActRefresh();
       SEL=i; z=1; 
    }
    SeSort(fieldb,1,v);
 
    if(!z) ActRefresh();
}}


function actaddsort(i,v,z){
with(this) with(Header[i]){   if(fnoAct()) return;
  
  if(!z) z=0;
  if(SEL<0) { actsort(i,v,0); z=1; }
  else SeSort(fieldb,1,v);
 
  if(!z) ActRefresh();
}}


function actfilterby(i,v,sb,ty){  // nome fieldb   v=value    sb  0=SEL  1=add filter      ty=type 3=data 4 select
with(this) with(Header[i]){   if(fnoAct()) return;

  if(!sb) { /* actclearfilter(1); */ SEL=i; }
  
  var sf=SeFilterby(fieldb);
  if(sf>-1) FILTER[sf].v=v;
  else FILTER.push( { f:fieldb, v:v, ty:ty } )
 
  ActRefresh();
}}



 


function actclearfilter(z){ 
with(this){    if(fnoAct()) return;
 
  SORT=[]; SEL=-1;
  if(z==2) {     
      if(FirstSort)  { 
         for(var r=0;r<Header.length;r++) { if(Header[r].fieldb==FirstSort){ SEL=r;  break; } }
         SORT=[{ f:FirstSort, m:FirstSortAD }];    
      }
   z=0;   
 }
 
  FILTER=[]; SEARCH=[]; AND=[];   
  
  if(!z) {
    GlobalSearch.f_active(0,name);
    LIKE={};
  }

 
  if(z<3) SELECTOR={};
 fromSearch = true
  if(!z) ActRefresh();
}}





function actmenuchek(v){
  with(this){  if(!v) v=0;

  var r, ab=Abody.length, af=[], fc, afc, lk;

  if(!v){ // add viewed
    for(r=0;r<ab;r++) { fc=Abody[r].f; 
 
        lk=JStore[fc].selected;
    
        if(!lk) af.push(fc); }
    if(af.length) jaxULock(af,2,1);
 }
 else
 {  // deseleziona viewed
    for(r=0;r<ab;r++) { fc=Abody[r].f; 
    
        lk=JStore[fc].selected;
    
        if(lk==2) af.push(fc); }
    if(af.length) jaxULock(af,0,0);
  
 }
}}
 
this.manageSelector = function (module, sel, dhierarchy) {
    with(this) {
        if(sel.selectors.length == 0) delete(SELECTOR[module]);
        else SELECTOR[module] = { s: sel.selectors, h: sel.hierarchy }; //asHy = sel.hierarchy;
        //if(moGlb.isempty(SELECTOR)) asHy = dhierarchy;
        ActRefresh(1);
    }
};
 
function actselector(jse){
  with(this){ 
 
if(!jse) SELECTOR={};
else SELECTOR=moGlb.cloneArray(jse);

ActRefresh(1);
}} 
 

function addAnd(v) {
    with(this) {
        AND = [ v ];
        ActRefresh();
    }
}

 
 
function viewchecked(){
  with(this){ 

  asHy=0;
 
  if(arrCHK.length) {  AND=[ { f:"f_code", z:" in ("+arrCHK.join(",")+") " } ];
                       
  } else { AND=[]; if(Hy&1) asHy=1; else asHy=0; }
  
 timeRefresh();
}}
 
 
 
 
 
 
function viewall(){
  with(this){ 

  if(Hy&1) asHy=1;
  if(checkmode) AND=[];
  
  timeRefresh();
}}
 
 
 
 
function f_alert(msg){
    
    //if(!msg) msg=moGlb.langTranslate("Close or save the element in editing");
    moGlb.alert.show(moGlb.langTranslate(msg));
     
}
 
 
 
// type 0=testo | 1=numerico | 2=booleano | 3=data | 4=select (filter) | 5=immagine 
 
/*

this.serchSelect=[
      ["Contiene","Non contiene","Uguale","Diverso"],                               // 0 = testo
      ["Uguale","Diverso","Maggiore di","Minore di","Espressione"],                // 1 = numerico
      ["Vero","Falso"],                                                           // 2=booleano
      ["Uguale","Diverso","Maggiore di","Minore di","Periodo"]                   // 3=data
];


*/ 
 
 
 // Search
function actsearch(i,ty,m){    // i=Header  ty=type campo  m=1 addsearch
  with(this){   if(fnoAct()) return;

if(ty>3) { moGlb.alert.show(moGlb.langTranslate("This search is not available")); return false; }

moSearchWindow.show(name, i, ty, m);

}}
 
function changeidSel0(sj,ty){
  with(this){
  
if(ty!=3) return; 
var v=sj.value,d=(v==4)?"block":"none"; 
mf$("idval1"+name).style.display=d;
}} 
 
 
function closefSearch(e){
  with(this){

var pj=maskj.parentNode; pj.removeChild(maskj); 
var sj=mf$('idBoxSearch'); pj=sj.parentNode; pj.removeChild(sj); 

moEvtMng.cancelEvent(e); 
return false;
}}


 
 
 
/*
SEARCH

f:    fieldb
v0:   valore da cercare
v1:   valore in range date
Mm:   0: ignora maiuscole e minuscole
type: tipo di campo 0-3
All:  0=solo nei gruppi di fase attivi  / 1=cerca in tutte le fasi dei workflow
 
*/ 
function TrovaSearch(data) {
  with(this){
    fromSearch = true;
//  var inp0=mf$("idval0"+name).value,      // valore da cercare
//   inp1=mf$("idval1"+name).value,
//   chk0=mf$("idchk0"+name).checked,   // se maiuscole e minuscole
//   sel0=mf$("idsel0"+name).value,     // scelta contiene, ....
//   chk1=mf$("idchk1"+name).checked,
//   fld=Header[i].fieldb;
    //asHy = 0;
    with(data) {
        var inp0 = main,      // valore da cercare
        inp1 = added,
        chk0 = sensitivity,   // se maiuscole e minuscole
        sel0 = condition,     // scelta contiene, ....
        chk1 = "",
        fld = Header[fieldIndex].fieldb,
        ty = type,
        i = fieldIndex,
        m = add;
    }
 
 // controllare compilazione....  inp0  inp1

chk0=(chk0)?1:0;

if(!m) { actclearfilter(3); SEL=i; SEARCH=[];  }   
var ss=SeSearch(Header[i].fieldb); if(ss>-1) SEARCH.splice(ss,1);
 
// SEARCH.push( { f:fld, v0:inp0, v1:inp1, Mm:chk0, Sel:sel0, type:ty, All:chk1 } )
SEARCH.push( { f:fld, v0:inp0, v1:inp1, Mm:chk0, Sel:sel0, type:ty } )

 
//closefSearch(e);
ActRefresh();
}} 

 
 
 
function actLike(tx){
with(this){

   LIKE={};     
   if(tx) { LIKE["Text"]=Base64.encode(tx); LIKE["Fields"]=moGlb.cloneArray(searchField) };

ActRefresh();
}} 


function ActRefresh(itm){ 
with(this){

  // lastrefresh
  if(!itm){
    var t=lastrefresh;
    lastrefresh=Math.floor(new Date().getTime()/1000);
    if((lastrefresh-t)<1) return;                           // do not refresh if delta time < 2 seconds
  }
 
  if(inLo==1) inLo=-1;
  aLoad=[];  // scarico load
  if(!inLo) { timeRefresh(); return; }     // attendo completamento processi
 
   setTimeout(name+".ActRefresh(1)", 50);
}}


function timeRefresh(){ 
  with(this){    
   ResetYLS(); 
   rowcounter=0; 
   LoaData(-1, 0, 0, 0, "root", 0);  
}}




 
 
function ResetYLS(){    // resetta LStore e Ypos
  with(this){
    
    var f,r;   
    JStore=[];   
    Ypos=[];
    rifY=[];
    multifcode=[];
    Hnr=0;
    Abody=[];
    blank=[];
    seBlk=0;
    
  if(!checkmode) arrCHK=[];    // non cancello i checkkati
    arrLK=[];
    Root="";
    
    revList=[];
    
    adds=0; upds=0; msgH=-1; insReverse(); 
}}
 
 
 
 
 
 
 
  
//---------------------------------------------------------------------------------

function updHeader(){      // update per hidden e lock 
  with(this){

  NT=0; NU=0; NL=0; Hlw=0; Huw=0; var sw=0; 
  for(var r=0;r<Header.length;r++) with(Header[r]) {  
    NT++;   
    hidd=parseInt(hidd);
    if(hidd) { 
      x1=sw; sw+=wdth; x2=sw;                                   // hidd==1 => visible   width header lock e unlock
      if(lock) NL++, Hlw=sw; else Huw+=wdth, NU++; 
    } 
  }
 
  eval("Oscr=Oscr"+name); 
  Oscr.Resize( { HH:Huw } );
 
  predraw();
}}  

function fmenuList(){ // create  menu list x fields 
  with(this){
  var hvis;  
  menulist=[ { Txt:moGlb.langTranslate("Select All"), Type:0, Fnc:name+".view_all_field()" } ];
  for(i=0;i<Header.length;i++) with(Header[i]){ 
   
    hvis=visibility&moGlb.user.level;   
    if(!hvis) continue; 
    
    menulist.push( {Txt:label, Type:2, Status:hidd, Icon:[ '', 'FlaticonsStroke.58786' ], Fnc:name+".acthidden("+i+")"} );
    
  } 

}}
 //----------------------------------------------------------------------------



function timeBlank(){      // seBlk=1   in attesa risposta ajax prima di processare altri blank
  with(this){

  if(!seBlk && blank.length){  seBlk=1;
    var p=blank[0].bpdr, nb=(blank[0].limn<nBlock)?blank[0].limn:nBlock;    
                                                                  
     aLoad.push([p, blank[0].limi ,nb, blank[0].lev, blank[0].rtree,1, 0]); loadata();
  }        

setTimeout(name+".timeBlank()",blkStp);
}}


function updateChecked(arr){
with(this){
arrCHK=moGlb.cloneArray(arr);
}}

/**
 * Inspection
 */
this.getView = function () {with(this) { var header = getHeader(); return {id: header[1], name: header[2], data: header[0]}; } }
this.getData = function () {with(this) return {id: data_id, name: data_name, data: { Sort: SORT, Filter: FILTER, Search: SEARCH, Ands: AND, Selector: SELECTOR, Like:LIKE} }; }
this.setView = function (view) { this.setHeader(view.data, view.id, view.name); }
this.setData = function (data) {
    with(this) {
        data_id = data.id, data_name = data.name;
        if(moGlb.isempty(data.data)) { SORT=[]; FILTER=[]; SEARCH=[]; AND=[]; SELECTOR={}; LIKE={}; }
        else {
            with(data) {
                if(moGlb.isset(data.Sort)) SORT = data.Sort;
                if(moGlb.isset(data.Filter)) FILTER = data.Filter;
                if(moGlb.isset(data.Search)) SEARCH = data.Search;
                if(moGlb.isset(data.Ands)) AND = data.Ands;
                if(moGlb.isset(data.Selector)) SELECTOR = data.Selector;
                if(moGlb.isset(data.Like)) LIKE = data.Like;
            }
        }
        ActRefresh();
    }
}
this.getBookmark = function () {with(this) return {id: FavoriteId, properties: aFavorite}; }
this.setBookmark = function (items, clr, fromtg) {
    with(this) {
        if(items.length > 0) {
            var f_codes = [];        
            if(moGlb.isempty(aFavorite)) aFavorite = {};
            for(var i in items) {
                f_codes.push(items[i].f_code);
                if(clr) aFavorite[items[i].f_code] = clr;
                else if(aFavorite[items[i].f_code]) delete(aFavorite[items[i].f_code]);
    //            f_codes.push(items[i].f_code);
    //            if(aFavorite[items[i].f_code] && aFavorite[items[i].f_code] == clr) delete(aFavorite[items[i].f_code]);
    //            else aFavorite[items[i].f_code] = clr;
            }
            if(!fromtg) jaxULock(f_codes, 0, 0);
        } else if(!clr) {
            aFavorite = [];
        } else moGlb.alert.show(moGlb.langTranslate("Select items to highlight"));
    }
}

function refresh() {  
with(this){    
      actclearfilter();
      // this.loadroot();
}}


function setSummaryCondition(value, recs) {
    with(this) {
    
       if(typeof(value.hierarchy)!="undefined") asHy = value.hierarchy;   
        
        if(recs == 0) {
          AND = [];
        }

        AND.push({ f: value.field, z: value.sql });
    }
}

//-----------------------------------------------------------------------------

/*

jrv=[{ "code":782, 
           "opid":[778],
           "pid":[778],
           "action":"upd",
           "node":{ f_title:"modificato782", f_priority:3  }
        },
        {  "code":788, 
           "opid":[778],
           "pid":[887],
           "action":"upd",
           "node":{ f_title:"spostato in 887", f_priority:2   }
        },
        { "code":300000, 
           "opid":[-1],
           "pid":[778],
           "action":"add",
           "node":{ f_title:"creato 300000 778", f_priority:3   }
        },     
        { "code":890, 
           "opid":[887],
           "pid":[-1],
           "action":"del"
        }   
           
        ]; 


*/

   
function insReverse(){
  with(this){  
  
if(msgH>0) { //canvColorRevers(colrs[1]);  
             mf$("idupd"+name).innerHTML=upds;
             mf$("idadd"+name).innerHTML=adds;  
             return; }
         
if(!msgH) msgH=32; else msgH=0;

var hb=H-HeadH-VW-mgT-mgB-msgH, tp=mgT+HeadH+msgH;

SH-=msgH;  

BDJ.style.top=tp+"px"; //BDJ.height=hb;
OBDJ.style.top=tp+"px"; //OBDJ.style.height=hb+"px";
MBDJ.style.top=tp+"px"; //BDJ.style.height=hb+"px";

with(HBDJ.style) height=msgH+"px";
predraw();
 
if(msgH){

  var stl="font:13px opensansR;color:#FFF;";
  HBDJ.innerHTML="<canvas id='idboxcanvasrevrs"+name+"' style='position:absolute;left:3px;top:3px;' width='"+(W-6)+"' height='26'></canvas>"+
  "<div style='position:absolute;left:50%;top:6px;width:180px;height:18px;margin-left:-90px;white-space:nowrap;'>"+moCnvUtils.IconFont(58541,16,"#FFF")+                       
  "<span style='"+stl+"position:absolute;left:22px;top:1px;color:#FFF;'>"+moGlb.langTranslate("Modified:")+"&nbsp;<span id='idupd"+name+"' style='"+stl+"'>"+upds+"</span>"+
  "&nbsp;&nbsp;-&nbsp;&nbsp;"+moGlb.langTranslate("Insertions:")+"&nbsp;<span id='idadd"+name+"' style='"+stl+"'>"+adds+"</span></span></div>"+
  "<div id='idboxoverrevrs"+name+"' style='position:absolute;left:0px;top:0px;width:100%;height:32px;background-color:#ffffff;opacity:0;'>&nbsp;</div>";
  
  canvColorRevers(colrs[1]);
  
  var oj=mf$("idboxoverrevrs"+name), o = this;
  
  oj.onmouseover=function(e){ o.canvColorRevers(colrs[7]); }
  oj.onmouseout=function(e){ o.canvColorRevers(colrs[1]); }
  oj.onclick=function(e){  o.AND = []; o.ActRefresh();  return false; }

} else {


HBDJ.onmouseover="";
HBDJ.onmouseout="";
HBDJ.onclick="";
HBDJ.innerHTML="";


}




}}


function canvColorRevers(bkg){
  with(this){ 

  var ibrj=mf$("idboxcanvasrevrs"+name); if(!ibrj) return; 
  
  var cibrj=ibrj.getContext("2d"), R=3, b=2, ww=W-6, hh=26; 
  ibrj.width=ww;

with(cibrj){
clearRect(0,0,ww,26);
fillStyle=bkg;
beginPath();
moveTo(0.5,R);
bezierCurveTo(0.5,b, R-b,0.5, R,0.5);
lineTo(ww-R,0.5);
bezierCurveTo(ww-R+b,0.5, ww-0.5,b, ww-0.5,R);
lineTo(ww-0.5,hh-R);
bezierCurveTo(ww-0.5,hh-R+b, ww-R+b,hh-0.5, ww-R,hh-0.5);
lineTo(R,hh-0.5);
bezierCurveTo(R-b,hh-0.5, 0.5,hh-R+b, 0.5,hh-R);
closePath();
fill();
}

}}




//  fcode       record/campi modificati
function Reversajax(jrv){          
  with(this) {
   moGlb.loader.hide();
 
  
   if(!BDJ) return;       
   
  // alert(JSON.stringify(jrv))
               
    var r,i,n,fc, ni=jrv.length, p0,p1,jn, if01,ifmax=0;
    if(!ni) return;   // nessun codice!

    for(r=0;r<ni;r++){ fc=jrv[r].code; 

      for(i=0;i<Abody.length;i++) { 
        if(fc==Abody[i].f) Reverseflash(i);                 // flash
        if(modTM) eval(modTM+".retoLD("+fc+");");      // update timeline
      }  
    
    // alert("r: "+r+" | i: "+i+" | "+Abody.length+" | "+name+"rnd "+(1000*Math.random())+"\n\n"+JSON.stringify(jrv[r]) );
    
    // array di padri sortati
    p0=(jrv[r].opid).sort();
    p1=(jrv[r].pid).sort();
    jn=jrv[r].node;
 
   // se non esistono i padri
   if( !ifExist(p0) && !ifExist(p1) ) continue; 
  
   if01=ifarrEQual(p0,p1);     
   
/*
0 upd  uguali
1 add  [-1]  [p1,p2,...pn]
2 upd move parent  [p1,p2,..]  [p1... pn]
3 del  [p1,p2,...pn]  [-1]
4 add from parent
5 del from parent
*/



  // 0 update
  if(!if01) { 
    if(!JStore[fc]) continue; 
    upds++; 
    for(k in jn) { JStore[fc][k]=jn[k]; } 
  }
  

  // 1 add
  if(if01==1) {  var ad=0; 
      for(var u=0;u<p1.length;u++)  {
          for(var rz=0;rz<Ypos.length;rz++) { 
              if(Ypos[rz].fcode!=p1[u]) continue;
              
              // se padre chiuso nessuna azione
              if(!Ypos[rz].open) continue;

              // padre aperto!
              ad=1; break; }
              if(ad) break;}
      if(ad) adds++; 
  }
  
  // 4 add from parent
  if(if01==4) { upds++; adds++; }

  // 3 del
  if(if01==3) { upds++; }
  
  // 5 del from
  if(if01==5) { upds++; adds++;  }
  
  // 2 move
  if(if01==2) { upds++; adds++;  }
  
  if(if01>ifmax) ifmax=if01;
  
  if(!JStore[fc]) continue;
  
} 

  //alert(ifmax+" | "+upds+" | "+adds)
     
  sedrw=2; draw();    
 
  // barra modifiche
  if(typeof(if01)=="undefined") return;  
  
  if(ifmax>0) insReverse(); 
  else {  
      if(msgH>0) mf$("idupd"+name).innerHTML=upds; 
  }
      
  sedrw=2; draw();
}}








function Reverseflash(i){ 
  with(this){
    var y=Abody[i].y;
        with(BDN) {
          save();
          fillStyle=fillStyle="rgba(161,194,77,0.7)";                    //   #@#      moColors.reverseFlashFill;    A1C24D
          fillRect(0,y,W,Yi);
          restore();
        }
}}



 
 
function ifExist(p) {
  with(this){
  for(var r=0;r<p.length;r++) { if(!JStore[p[r]]) return false; }  
  return true;
}}


/*
0 upd  uguali
1 add  [-1]  [p1,p2,...pn]
2 upd move parent  [p1,p2,..]  [p1... pn]
3 del  [p1,p2,...pn]  [-1]
4 add from parent
5 del from parent
*/

function ifarrEQual(p0,p1){
    with(this){

if(p0[0]==-1) return 1;
if(p1[0]==-1) return 3;
var n0=p0.length, n1=p1.length, nn=(n0>n1)?n1:n0;


var apd=[]; for(var r=0;r<n0;r++) apd[p0[r]]=1;
            for(var r=0;r<n1;r++) apd[p1[r]]=1;
            
            for(r in apd) { if(revList.indexOf(r)==-1) revList.push(parseInt(r));   }
     

for(r=0;r<nn;r++) if(p0[r]!=p1[r]) return 2;
if(n1>n0) return 4;
if(n0>n1) return 5;
return 0;
}}



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// ENGY
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



 
 
// props
this.Ypos=[];          // array Ypos[]={ posY:, fcode:, levl, nch }   -->  Ypos.length N. elementi caricati  levl livello in tree
this.rifY=[];          // indice posizione f_code -> Ypos
this.inLo=0; 
this.aLoad=[];
this.seclear=0;

this.stpclear=1000;
this.ndmax=5;
this.istatus=0;
this.infostri="";
this.oinfostri="-";
this.timeinf=0;

// metodi
this.OpenP=OpenP;
this.CloseP=CloseP;
this.testCloseP=testCloseP;
this.RemoveEl=RemoveEl;
this.ClearEl=ClearEl;
this.finfo=finfo;
this.ftimefinfo=ftimefinfo;

this.loadata=loadata;
this.LoaData=LoaData;

this.retdata=retdata;     
this.retson=retson;       // carica i blank (fratelli)
this.reParent=reParent;




this.f_countCheck=function(){
with(this){
  var n=arrCHK.length; 
  if(countCheck==n) return; 
  countCheck=n;
  finfo();
}} 



/*
1   loading     -1 end loading
2   clearing    -2 end clearing
*/


function finfo(v){
with(this){ 
 
  if(v<0) { if(istatus&(v*-1)) istatus&=(istatus+v); } else istatus|=v;
  var stri="", icl="#a0a0a0", bi="";
 
  if(istatus&1) stri+=moGlb.langTranslate("Loading...");
    
  if(istatus&4) icl="#a02020";
    
  if(istatus&2) { if(stri) bi="&nbsp;/&nbsp;"; stri+="<span style='color:"+icl+";'>"+bi+moGlb.langTranslate("Cleaning up...")+"</span>";  }

  infostri=(rowcounter)+" "+moGlb.langTranslate("elements")+" "+(countCheck?("- "+countCheck+" "+moGlb.langTranslate("checked")):"")+stri;
if(timeinf<=0) { timeinf=2; ftimefinfo(); }

}}



function ftimefinfo(){  
  with(this){  timeinf--;    
                                      
   if(infostri!="") { if(oinfostri!=infostri) {
           if(mf$("infotxt"+name)) mf$("infotxt"+name).innerHTML=infostri;
           oinfostri=infostri; } 
                      timeinf=2; }                                                      
     if(timeinf<=0) { mf$("infotxt"+name).innerHTML=""; oinfostri="-"; return;   }
     
setTimeout(name+".ftimefinfo()",500);
}}








function ClearEl(){   // v totale elementi in localstorage                    
  with(this){                                  

    if(recursiveOpen) return;
  var ny=Ypos.length;
 
if(ny>Max) {  seclear=1;

  var nd=ny-Max, Nr=1, c=0, fc=-1, rtr="", afc, r, rr, i, x;  
  if(ny<MaxOver) { finfo(-4); nd=ndmax; }                               // nd = numero elementi da cancellare
  else { finfo(4); nd=ny-(MaxOver-nBlock*2); }
  
  Nr=Abody.length; if(Nr) fc=Abody[0].f, rtr=Abody[0].rtree;     // Nr=numero righe visualizzate    Yx=posizione in Ypos
 
if(fc>-1) {  x=rifY[rtr];

                                               //     mf$("debug2").innerHTML="Ypos n: "+Ypos.length+" | Yx: "+x+" | fx: "+fc;

  var pd=0, opd, npd, asl=[];

 for(r=ny-3;r>10;r--) {
    if(r>(x-3) && r<(x+Nr+3) ) continue;
  
    fc=Ypos[r].fcode; 
    rtr=Ypos[r].rtree;
    opd=Ypos[r-1].fpdr;
    npd=Ypos[r+1].fpdr;
    pd=Ypos[r].fpdr;     
    if(pd!=npd || opd!=pd) continue;
    
    afc=JStore[fc];
    if(!afc) continue; 
    
    // se open o selezionato o first child o precedente non fratello o se visualizzato o ultimo se figlio unico   e non ultimo fratello
    if(Ypos[r].open || afc.selected>1) continue;   
    asl.push({ f:fc, r:rtr }); 
    c++; if(c>=nd) break;
  }
    for(r=0;r<asl.length;r++) RemoveEl(asl[r].f, asl[r].r);
} 
} else { seclear=0; finfo(-2); return; }
  
setTimeout(name+".ClearEl()", stpclear); 
}}


// cancella fcode: splice Ypos / rifY[rtree]  / Lstore.Del 
function RemoveEl(fc, rtr){  
  with(this){       if(!rifY[rtr]) return; 

    var f=rifY[rtr], cd, r, prg, ar; 
    Ypos.splice(f,1);  delete rifY[rtr];   

    for(r=f;r<Ypos.length;r++)  { cd=Ypos[r].rtree; rifY[cd]=r; }  // aggiorno i riferimenti 
    
    multifcode[fc]--; if(multifcode[fc]<1) delete JStore[fc];     
}}



function OpenP(yp){
  with(this){ 
  
  recursiveOpen=(moKeyMng.hasKey(16))?1:0;  
  var fc=yp.fcode, nb, nt, nc=yp.nch;   
  
  if(recursiveOpen) { 
    nt=Ypos.length+nc;
    if(nt<Max) nb=nc; else { recursiveOpen=0; aLoad=[]; }
  } 
  
  if(!recursiveOpen) nb=(nc<nBlock)?nc:nBlock;
  
  aLoad.push([fc,0,nb,yp.levl, yp.rtree ,0, 1]); loadata();
}}



function CloseP(yp){
  with(this){   
 
  nodrw=1;  
  
  yp.open=0; 
  var fc=yp.fcode, rtr=yp.rtree, pp=yp.levl, ni=yp.nch, i=rifY[rtr], k=0, ny=Ypos.length, nhi, fi, pr, lsr, afi=[], rti;
  i++;
  
  var ii=i;
 
  // tolgo figli e nipoti
  while(i<ny && Ypos[i].levl>pp) { 
  
  fi=Ypos[i].fcode; rti=Ypos[i].rtree; 
  
  if(JStore[fi].selected) afi.push(fi);  multifcode[fi]--; if(multifcode[fi]<1) delete JStore[fi];
    
  delete rifY[rti];
  
  if(Ypos[i].open) ni+=Ypos[i].nch; i++;  k++; }   
  
  if(!checkmode) jaxULock(afi,0,0);  
  Ypos.splice(ii,k);
   
  // aggiorno posizioni successive ed i riferimenti
  ny=Ypos.length, nhi=ni*Yi;
  for(var r=ii;r<ny;r++) { Ypos[r].posY-=nhi; rifY[Ypos[r].rtree]=r; }

  Hnr-=ni;
 
 rowcounter=0;   
  for(r=0;r<Ypos.length;r++) { 
    if(Ypos[r].open) rowcounter+=Ypos[r].nch;  
  }
 finfo(8);
 
 if(Pareverse) return ni;
  
  eval("Vscr=Vscr"+name);
  Vscr.Resize( { HH:Hnr*Yi } ); 
  animdraw(rtr,ni,ni,1); 
  
}}








function loadata(){
with(this){ 

if(!inLo && aLoad.length) { inLo=1; var a=aLoad.shift();  LoaData(a[0],a[1],a[2],a[3],a[4],a[5], a[6]); }       
if(!aLoad.length) return;       

setTimeout(name+".loadata()",100);
}} //





function LoaData(pp, li, ln, lvl, rtr, son, nod){

with(this){      
  
  if(!son) son=0;
  if(!nod) nod=0;    // open folder

// SORT[]

// FILTER[]

// SEARCH[]

// AND[]

// SELECTOR

// FILTER
 
//if(!pp && ln) eval(crud).updateListeners();   // update summary
if(fromSearch) eval(crud).updateListeners();
fromSearch = false;
// alert(pp+" | limit: "+li+", "+ln+" | lev: "+lvl+" | rtree: "+rtr+" | Hnr: "+Hnr);      
// rtr tree del padre pp 

var z=rifY[rtr], vtime=0;
if(z) vtime=Ypos[z].timestamp;

moGlb.loader.show(moGlb.langTranslate("Loading..."));
 
 //console.log( {SORT:SORT, SELECTOR:SELECTOR, FILTER:FILTER, AND:AND, SEARCH:SEARCH, LIKE:LIKE } );
if(name=="mdl_prl_pp_tg_tg") {  
  if(typeof(this.forceLIKE)!="undefined") LIKE={ "Text":Base64.encode(forceLIKE), "Fields":["f_code"] }; 
}

var AN=[], FI=[], SR=[], SE={}, LI={};

if(!nod) { 

  AN=moGlb.cloneArray(AND), 
  FI=moGlb.cloneArray(FILTER), 
  SR=moGlb.cloneArray(SEARCH), 
  SE=moGlb.cloneArray(SELECTOR), 
  LI=moGlb.cloneArray(LIKE);
  
}else{ 
   /*
    for(var r=0;r<AND.length;r++){
      if( (AND[r].z).indexOf("Closing") != -1 ) { AN=moGlb.cloneArray(AND); break; }
      if( (AND[r].z).indexOf("Deleting") != -1 ) { AN=moGlb.cloneArray(AND); break; }     
    }*/
	var sk,k;

    for(k=0;k<AND.length;k++){
      sk=AND[k]["z"];
      if(sk.indexOf("Closing")>-1 || sk.indexOf("Deleting")>-1){
         AN=[ { "f":AND[k]["f"] ,"z":AND[k]["z"] } ]; 
         break;
      }
    }
}

//**************************************
// PICKLIST FILTERS
//**************************************
if(this.PICKLIST_SEARCH != undefined && this.PICKLIST_SEARCH != null){
  SR = SR.concat(PICKLIST_SEARCH);
}

if(this.PICKLIST_SELECTOR != undefined && this.PICKLIST_SELECTOR != null){
  SE.picklist_selector = PICKLIST_SELECTOR.picklist_selector;
}


var ja=JSON.stringify({ Hierarchy:asHy, Prnt:pp, Tab:[Ttable, (this.PICKLIST_FTYPE != undefined && this.PICKLIST_FTYPE) ? this.PICKLIST_FTYPE : ftype, fname], module_name: crud, Sort:SORT, 
   // Filter:FILTER, Search:SEARCH, Ands:AND, Selector:SELECTOR, Like:LIKE, 
    Filter:FI, Search:SR, Ands:AN, Selector:SE, Like:LI,
    Limit:[li,ln], rtree:rtr, timestamp:vtime, self: self, ignoreSelector: ignoreSelector, skipwf: skipwf, pcross_inv:pcross_inv, fromPicklist : (this.FROM_PICKLIST != undefined && this.FROM_PICKLIST) ? true : false });


 // alert(ja)

Ajax.sendAjaxPost( moGlb.baseUrl+"/treegrid/jax-read-child", "param="+ja , name+((son)?".retson":".retdata") );

finfo(1);
}}








/*
ar: json  ritorno

{ ntot: n, data:[ [0]head,  [1...n]value ];

ntot:   -1  se treegrid   n se grid

data:
array[0]        header[   ]              
array[1....n]   value[  ]              // f_parent_code=[padre0, padre1, ...]


*/

function retdata(a){
  with(this) {

  finfo(-1);moGlb.loader.hide();
  if(inLo<0) { inLo=0; seBlk=0; return; }
   
  nodrw=1; 

  try{var ar=JSON.parse(a), nar=(ar.data.length)-1, prg,fcd, vtime=ar.other.timestamp, mdf=ar.other.modify; }catch(e){ alert(e+"\n"+a); ar=[]; nar=0; vtime=0; mdf=0;  }
  if(moGlb.isset(ar.message)) moGlb.alert.show(moGlb.langTranslate(ar.message));

 // reload
 if(!Root) {    
 
     var nn=parseInt(ar.ntot);       
     Root={ f_code:0, rtree:"root", nch:nn, levl:-1, open:1, timestamp:-1  }             // root implicito 
     
     JStore[0]={ f_code:0, rtree:"root", nch:nn, levl:-1, open:1 };       
                        
     Ypos.push({ posY:-Yi, fcode:0, rtree:"root", fparent:-1, fpos:0, nch:nn, levl:-1, open:1, timestamp:-1 }); 
     rifY["root"]=0;             
 
      ln=(nn<nBlock)?nn:nBlock;
      Hnr=0;
 
      if(nn) { rowcounter=0; LoaData(0, 0, ln, 0, "root", 0); }                                // carico primi record
      else  { mf$("infotxt"+name).innerHTML=moGlb.langTranslate("0 Record found.");   }
 
      eval("Vscr"+name+".Resize( { ptr:0 } );");
      return;
   }
 
 
 
 if(mdf && !Pareverse) if(reParent(0, ar.other.rtree, ar.other.nch)) { inLo=0; return; }      // modifica reverse apertura padre
 
 
 
  if(nar<0) { if(NOEVT) NOEVT=100; draw(); inLo=0; blank=[];
            //  moGlb.alert.show(moGlb.langTranslate("Please wait!\nUpdate data."));
              setTimeout(function(){ actclearfilter(2);},100);
              return; }
 
  

  var ahe=[], rhe=[], ng=ar.data[0].length, aa, g, Nld, nt;
  for(var r=0;r<ng;r++) { ahe[r]=ar.data[0][r]; rhe[ar.data[0][r]]=r; }          // 0->"fpdr", 1->"nch", 2->"selected", 3->"fpos, 4->"rtree" (padre)
 
  var prnt=ar.data[1][0],         // padre chiamante
      rtr=ar.data[1][4],          // rif padre
      p=rifY[rtr],                // posizione padre in Ypos
      lev=Ypos[p].levl+1,         // level figli
      ps=ar.data[1][3],           // posizione limit inizio tra fratelli  
      ny=Ypos.length,
      y=Ypos[p].posY,             // posiz px padre
      ych=y+ps*Yi,                // posY primo
      ypo=Ypos[p].open,           // se padre open o close
      i=p+1;
    
  rowcounter+=Ypos[p].nch; 
  Nld=Ypos.length+nar;

  // inserisco in JStore  
  for(r=1;r<=nar;r++){ 
     fcd=parseInt(ar.data[r][rhe.f_code]); 
     

     if(checkmode) for(s=0;s<arrCHK.length;s++) { if( arrCHK[s]==fcd ) { ar.data[r][2]=2;  break; } }
     
     JStore[fcd]={}; 
     aa={}; for(g=0;g<ng;g++) { JStore[fcd][ahe[g]]=ar.data[r][g]; }  
                                                   
     rifY[rtr+"|"+fcd]=i;          
     ych+=Yi;
 
     nch=ar.data[r][1];
     Ypos.splice(i,0,{posY:ych, fcode:fcd, fpdr:prnt, rtree:rtr+"|"+fcd, fpos:ar.data[r][3], nch:nch, levl:lev, open:0, timestamp:vtime });               //------------------------------------------------ 

             // if recursive open
             if(nch && recursiveOpen){
                nt=Nld+nch;
                if(nt<Max) { 
                  Nld=nt; 
                  aLoad.push( [fcd, 0, nch, lev, rtr+"|"+fcd, 0,1] );
                } 
             }
 
     if(!multifcode[fcd]) multifcode[fcd]=1; else multifcode[fcd]++;
     i++;
     }
     
     ny=Ypos.length;                        
     for(r=i-1;r<ny;r++) { rifY[Ypos[r].rtree]=r;  }             // aggiorno i riferimenti
     
 
     if(!ypo){                                                  // aumento dal fratello del padre di nch*Yi se non era !open
     while(i<ny && Ypos[i].fpdr==prnt) i++;                     // trovo primo fratello
     var dnh=Ypos[p].nch*Yi;
     for(r=i;r<ny;r++) Ypos[r].posY+=dnh;
     }
 
     Ypos[p].open=1; 
 
     Hnr+=Ypos[p].nch;
     eval("Vscr=Vscr"+name); 
      
      

     // se riapertura da reverse ajax
     if(Pareverse){  seBlk=0;  
                     nodrw=0; noAnim=0;
                     setTimeout("moGlb.loader.hide()", 1000);  }
                       
     if(Pareverse==1) {
      var ht=Hnr*Yi, yy=Ypos[p].posY;                  
      if((ht-yy)<0) yy=0;
      
      Vscr.Resize( { HH:ht, ptr:yy } );
         
      } else {        
      Vscr.Resize( { HH:Hnr*Yi } ); 
      }
  
  
  if(Pareverse==2) Pareverse=0; 
  
  if(!Pareverse) animdraw(rtr,nar,Ypos[p].nch,0); 
  else Pareverse=0; 
  
  inLo=0;
  if(!seclear && ny>Max) { finfo(2); ClearEl(); }
  
  finfo(8);
  
  if(!aLoad.length) { 
    recursiveOpen=0; 
  } else if(recursiveOpen) loadata();
  
}}  






function retson(a){
  with(this){            
 
  finfo(-1); moGlb.loader.hide();
  if(inLo<0) { inLo=0; seBlk=0; return; }   
                               
  var ar=JSON.parse(a), nar=(ar.data.length)-1, prg,fcd, mdf=ar.other.modify, vtime=ar.other.timestamp;
  if(moGlb.isset(ar.message)) moGlb.alert.show(moGlb.langTranslate(ar.message));
 
   if(mdf && reParent(1, ar.other.rtree, ar.other.nch)) return;                   // modifica reverse padre nei blank
 
  
  if(nar<0) { if(NOEVT) NOEVT=100; inLo=0; blank=[];
           //   moGlb.alert.show(moGlb.langTranslate("Please wait!\nUpdate data."));
              setTimeout(function(){ actclearfilter(2);},100);
              return; }
 
  var ahe=[], rhe=[], ng=ar.data[0].length, aa, g, nk=0;
  for(var r=0;r<ng;r++) { ahe[r]=ar.data[0][r]; rhe[ar.data[0][r]]=r;  }          // 0->"fpdr", 1->"nch", 2->"selected", 3->"fpos", 4->"rtree" (padre)

//alert("retson")
  
  var prnt=ar.data[1][0],         // fcode padre chiamante
      rtr=ar.data[1][4],
      p=rifY[rtr],               // posizione padre in Ypos
      lev=Ypos[p].levl+1,         // level figli
      ps=ar.data[1][3],           // posizione limit inizio tra fratelli  
      ny=Ypos.length,
      y=Ypos[p].posY,             // posiz px padre
      ypo=Ypos[p].open,           // se padre open o close
      i=p+1;
  
      if(!ypo) { inLo=0; return; }
 
        var ii=i;     
                         
        while(i<ny){
        if(Ypos[i].levl>lev) { nk++; i++; continue;  }
        if(Ypos[i].fpdr==prnt && Ypos[i].fpos<ps) { i++; continue; }
        break;  }
    
 
        var ych=Ypos[i-1].posY + (ps-(Ypos[i-1].fpos+1))*Yi;        // i punto inserimento fratelli
 
  for(var r=1;r<=nar;r++){ 
     fcd=parseInt(ar.data[r][rhe.f_code]);
     
     sele=0;
     if(checkmode) for(s=0;s<arrCHK.length;s++) if( arrCHK[s]==fcd ) { ar.data[r][2]=2;  break; }
     
     JStore[fcd]={}; 
     aa={}; for(g=0;g<ng;g++) { JStore[fcd][ahe[g]]=ar.data[r][g]; }
                                                        
     rifY[rtr+"|"+fcd]=i;          
     ych+=Yi;
 
     Ypos.splice(i,0,{posY:ych, fcode:fcd, fpdr:prnt, rtree:rtr+"|"+fcd, fpos:ar.data[r][3], nch:ar.data[r][1], levl:lev, open:0, timestamp:vtime });               //------------------------------------------------ 
     if(!multifcode[fcd]) multifcode[fcd]=1; else multifcode[fcd]++;
     i++;
     }
 
     ny=Ypos.length;                      
     for(r=i-1;r<ny;r++) rifY[Ypos[r].rtree]=r;              // aggiorno i riferimenti
 
  
  if(NOEVT) NOEVT=100; blank=[]; sedrw=2; seBlk=0; draw();
  inLo=0;
  if(!seclear && ny>Max) { finfo(2); ClearEl(); }
}}  



// m 0=retdata  1=retsoon
function reParent(m, rtr, nnn){
with(this){  

var i;
 
if(m) {  // son
          moGlb.loader.show(moGlb.langTranslate("Reload Child"));
                Pareverse=1;   
                i=rifY[rtr];
                if(!i) { // moGlb.alert.show(moGlb.langTranslate("Index not found1!")); 
                         inLo=0;
                         finfo(-1);
                         moGlb.loader.hide();
                         return 0;  }
                noAnim=1;
                nodrw=1;
                inLo=-1;
                CloseP(Ypos[i]);
             
                Ypos[i].nch=parseInt(nnn);        // numero nuovo figli
                    
                inLo=0;
                OpenP(Ypos[i]);       
                return 1;        
}
else { // father
                moGlb.loader.show(moGlb.langTranslate("Update")+"<br>"+moGlb.langTranslate("Element"));
                
                Pareverse=2; 
                i=rifY[rtr];
                
                if(!i) {  
                            // moGlb.alert.show(i+" | "+rtr+" | "+nnn)
                    
                            //moGlb.alert.show(moGlb.langTranslate("Index not found2!")); 
                         inLo=0;
                         finfo(-1);
                         moGlb.loader.hide();
                         return 0;  }
                
                nodrw=1;

                Ypos[i].nch=parseInt(nnn);        // numero nuovo figli
                    
                inLo=0;
                OpenP(Ypos[i]);       

                return 1;
} 
 
              
return 0;
}}




 

//------------------------------------------------------------------------------
// animazione open


function animdraw(rtr,nar,nah,oc){
with(this){  
 
if(noAnim) return;
if(recursiveOpen>1) { if(NOEVT) NOEVT=100; nodrw=0; sedrw=2; draw(); return;  }

if(recursiveOpen) recursiveOpen++;
 
CDJ=moGlb.createElement("idcanvanim"+name,"canvas"); 
  
var ww=cnvLS+Hlw+OH;

CDJ.width=ww; CDJ.height=SH;

CDJ.style.left=mgL+"px";
CDJ.style.top=(mgT+HeadH)+"px";
CDJ.style.display="none";  

 // , backgroundColor="#ff0";

if(rtr=="root") { if(NOEVT) NOEVT=100; nodrw=0; sedrw=2; draw(); return; }
 
var y=0;
for(var r=0;r<Abody.length;r++) if(Abody[r].rtree==rtr) { y=Abody[r].y+Yi; break; } 


tf=nar*Yi;
if(nar<nah) {  tf=nah*Yi; if(tf>SH) { tf=SH-(SH%Yi)+Yi;  }   }


if(SH<=y) { if(NOEVT) NOEVT=100; nodrw=0; sedrw=2; draw(); return; }


if(!oc){

OMD=BDN.getImageData(0,0,ww,y);
FMD=BDN.getImageData(0,SH-tf,ww,tf);
oBDN=BDN; BDN=CDJ.getContext("2d");

draw2(1);
IMD=BDN.getImageData(0,y,ww,SH-y);


} else {

OMD=BDN.getImageData(0,0,ww,y);           // A

if(tf>SH) { tf=SH-(SH%Yi)+Yi; }
FMD=BDN.getImageData(0,y,ww,tf);          // B1
oBDN=BDN; BDN=CDJ.getContext("2d");

draw2(1);
IMD=BDN.getImageData(0,y,ww,SH-y);         // B2+C

if(tf>SH) { tf=SH-(SH%Yi)+Yi;  }
 
}

tmanimdrw(y, ww, 0, 3, tf , oc);
}}


 

function tmanimdrw(yi, ww, t, k, tf, m){
with(this){

t+=k; k+=5; if(t>=tf) t=tf; 

oBDN.clearRect(0,0,ww,SH);

if(!m){

oBDN.putImageData(IMD,0,yi+t-tf);
oBDN.putImageData(FMD,0,SH-tf+t);

} else {

oBDN.putImageData(FMD,0,yi-t);     // B1
oBDN.putImageData(IMD,0,yi-t+tf);        // B2 + C

}



oBDN.putImageData(OMD,0,0);

if(t>=tf) { BDN=oBDN; if(NOEVT) NOEVT=100; nodrw=0; sedrw=2; draw(); return; }

setTimeout(name+".tmanimdrw("+yi+","+ww+","+t+","+k+","+tf+","+m+")",40);
}}

} // fine treeGrid









/* bookmark multicheck & grouping

Start(objname,container,head){

            0                 1     2      3            
Ypos = [ index in Header, label, type, checked:0|1 ]       

type:  0=normal
       1=separator
       2=group element visible opened
       3=group element visible closed
       4=hidden (if group is closed)


Header => jh
     0         1     2       3       4     5       6       7         8       9         10         11      12   13
  "fieldb","label","type","lock","hidd","wdth","filter","range","prange","script","visibility","grouped","x1","x2


*/
 
BkmSelect={ J:null, Tj:null, Cj:null, cj:null, jh:null, W:0,H:0, jl:null, Win:null, sF:[],
            Vscr:null, JE:null, TGJ:null,  Wc:0, Hc:0,
            EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove',
            Mode:0, vptr:0, Yt:0, Yi:32, aBD:[],
            Ix:0,Iy:0, IMV:0, dwn:0, VW:18,
            Ypos:[], PE:{}, aGrp:{}, NAR:0, HNAR:0,
            fnt:"13px opensansR",          

Start:function(objname,container,head,wn){
with(BkmSelect){
  
  Win=wn;  // obj popup
  var r,s;
  Tj=(typeof(objname)=="string")?eval(objname):objname;                          // object treegrid
  J=(typeof(container)=="string")?mf$(container):container;  
 
  jh=(typeof(head)=="string")?JSON.parse(head):moGlb.cloneArray(head);          // jh copy of JSON Header treegrid

  sF=moGlb.cloneArray(Tj.searchField);                                          // sF => copy of searchField

  moEvtMng.addEvent(document,"keyup",BkmSelect.f_keyup);  

  jl=[
    {Txt:moGlb.langTranslate("by sort"), Type:3, Status:1, Group:2, Icon:['','radio'], Fnc:"BkmSelect.chgMode(0)" },
    {Txt:moGlb.langTranslate("by group"), Type:3, Group:2, Icon:['','radio'], Fnc:"BkmSelect.chgMode(1)" }
  ];

  if(isTouch) EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';
 
  W=J.offsetWidth, H=J.offsetHeight;
  Hc=H-48; Wc=W;
  
  s="<canvas id='idbkmcnv' style='position:absolute;left:0;top:0;' width='"+W+"' height='"+Hc+"'></canvas>"+
    
    "<div id='BkmSelectOvr' style='position:absolute;left:0px;top:0;width:100%;height:"+Yi+"px;background-color:#A0A0A0;opacity:0.2;display:none;'></div>"+   // over
    "<div id='BkmSelectEvt' style='position:absolute;left:0;top:0;width:100%;height:"+Hc+"px;background-color:#FFF;opacity:0.01;'></div>"+                    // events drag click
    
    "<button style='position:absolute;left:40px;bottom:2px;' onclick='BkmSelect.f_close()'>"+moGlb.langTranslate("Cancel")+"</button>"+
    "<button style='position:absolute;right:10px;bottom:2px;' onclick='BkmSelect.f_save()'>"+moGlb.langTranslate("Save")+"</button>"+
   
    "<div style='position:absolute;left:10px;bottom:4px;cursor:pointer;' onclick='BkmSelect.viewMenu(event)'>"+moCnvUtils.IconFont(58445,16,"#4A6671")+"</div>";
   
   
  J.innerHTML=s;
  J.style.display="block";

  Cj=mf$("idbkmcnv"); cj=Cj.getContext("2d");
  JO=mf$("BkmSelectOvr");
  JE=mf$("BkmSelectEvt");
 
  VscrBkm=null;
  VscrBkm=new OVscroll('VscrBkm',{ displ:0, L:0, T:0, H:Hc, HH:1, Fnc:"BkmSelect.Vbarpos", pid:J.id });    
  Vscr=VscrBkm;

  TGJ=moGlb.createElement("maskBkmDragScroll_1","div",document.body);
  TGJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(moGlb.zloader+500));

 
  // events
  eval("JE."+EVTDOWN+"=function(e){ BkmSelect.sel_down(e); }");
  
  if(isTouch){ 
    eval("JE."+EVTMOVE+"=function(e){BkmSelect.sel_move(e); }");
    eval("JE."+EVTUP+"=function(e){BkmSelect.sel_up(e); }");  
  } else {    
    JE.onmouseover=function(e){ WHEELOBJ="VscrBkm"; }
    JE.onmouseout=function(e){ WHEELOBJ=""; JO.style.display="none"; }
    eval("JE."+EVTMOVE+"=function(e){BkmSelect.sel_move(e); }");   
  }
 
  chgMode(0);  
}},


viewMenu:function(e){
with(BkmSelect){
  moMenu.Start(jl,e,0);
}},

 

chgMode:function(i){  // Change view Mode
with(BkmSelect){
  
  var r,k,m,d,v,g,l=1, n=jh.length;
  Mode=i;
  
  Ypos=[]; 
  aGrp={};
  
  if(Mode){ // by group
  var def = false;
    // search group
    for(r=0;r<n;r++){
      g=jh[r].grouped;
      if(!g) g="default";
        if(typeof(aGrp[g])=="undefined") aGrp[g]=[];
        aGrp[g].push(r);
    }
    
    // create Ypos (first default field)
    if (aGrp["default"]) {
        def = true;
    Ypos.push([ -1, "Default (ungrouped)", 3, 0]);
    v=0;
    for(r=0;r<aGrp["default"].length;r++) {
      k=aGrp["default"][r];
      d=jh[k].hidd;
      v+=d;
      Ypos.push([ k, jh[k].label, 4, d, 0]);  // hidden
    }
    if(v==aGrp["default"].length) Ypos[0][3]=1;
    }

    
    // other groups
    for(g in aGrp){
      if(g=="default") continue;
      if (def) {
          
      Ypos.push([ -1, "separator", 1, 0 ]);
  }
  def = true;
      v=0; m=Ypos.length;
      Ypos.push([ -1, g, 3, 0]);
      for(r=0;r<aGrp[g].length;r++) {
        k=aGrp[g][r];
        d=jh[k].hidd;
        v+=d;
        Ypos.push([ k, jh[k].label, 4, d, m ]);   // hidden
      }
      if(v==aGrp[g].length) Ypos[m][3]=1;
    }
   
  
  } else { // by sort
  
      for(r=0;r<n;r++){
        ll=(jh[r].lock)?1:0;
        if(l && !ll) { l=0; Ypos.push([ -1, "separator", 1, 0 ]); }
        Ypos.push([ r, jh[r].label, 0, jh[r].hidd ]);
      }     
  }

  vptr=0; Yt=0;
  Draw();
  Vscr.Resize( { L:Wc-VW, T:0, H:Hc, HH:0, ptr:vptr } ); 
  UpdateVscr();
}},



Draw:function(){
with(BkmSelect){
  var r,b=0,yn=0,y=0,nar,n,ry,py;
  nar=Ypos.length; if(!nar) return;    // no rows  
  Yt=vptr%Yi, n=Hc+Yt;
  y=vptr-Yt;
  ry=y/Yi;  
  cj.clearRect(0,0,Wc,Hc);   //alert(Wc+" | "+Hc)
  py=-Yt;
  aBD=[];
//  aggiunto 31/08/2018 da elCarbo (bugfix scrolling barra verticale)
    var diff = vptr;
    for(r=0;r<nar && diff > 0;r++){
      if(Ypos[r][2]&4) continue;   // hidden elements in group
      //  aggiunto 31/08/2018 per testing
      if(Ypos[r][2] == 1) diff -= Yi/4;
      else diff -= Yi;      
    }
  ry=r;py=0;
  //
  
  for(r=ry;r<nar;r++){
      if(Ypos[r][2]&4) continue;   // hidden elements in group
      bkg=b&1;   
      yi=f_row(r, py, bkg);
      if(yi==Yi) b++;   
      py+=yi;
      if(py>Hc) break;      
  } 
}},



f_row:function(k,y,bkg){    // type:  0=normal   1=separator   (2=group element visible opened   3=group element visible closed)   4=hidden (if group is closed)
with(BkmSelect){

  var ty=0,rw,x=0, mg=10, ck,yt,b=0,t,tt;
  rw=Ypos[k];
  ck=rw[3];
 
  with(cj){
    
      // separator
      if(rw[2]==1){    
        fillStyle="#AAA";     
        yt=y+parseInt(Yi/8); 
        fillRect(10,yt,Wc-28,1);
        return Yi/4;
      }

      // falsariga
      fillStyle=bkg?"#F4F4F4":"#FFF";
      fillRect(0,y,W,Yi);
  
    // checkbox
    x+=mg;
    t=rw[0];
    
    if(jh[t]) ty=jh[t].type;
    tt=(t<0 || ty==3 || ty==5)?"":(jh[t].fieldb);  // se search field (no folder & type data)
    
    f_check_cnv(x,y,ck,rw[1], tt );
    x+=16+mg;

    // arrow grouped element
    if(Mode){
      if(rw[2]&2){
        f_Arrow(x,y,rw[2]);
        b=1;
      } 
      x+=16+mg;
    }

    // label        
    font=b?("bold "+fnt):fnt; textBaseline="alphabetic"; textAlign="left";
    fillStyle=b?"#606060":"#000";     
    yt=y+parseInt((Yi-8)/2)+9;    
    fillText(moGlb.langTranslate(rw[1]),x,yt);
   
  }

  aBD.push({y:y+Yi, idy:k });
  return Yi;
}},


f_Arrow:function(x,y,i){  
with(BkmSelect){
  var c=16,t,ch;
  t=y+c+parseInt((Yi-c)/2);
  
  with(cj){
    font="16px FlaticonsStroke"; textBaseline="alphabetic"; textAlign="left";
    fillStyle="#A0A0A0";  
    ch=(i==2)?58825:58824;        
    fillText( String.fromCharCode(ch), x, t);
  }

}},



f_check_cnv:function(x,y,i,tx,tt){  // i: 1=checked
with(BkmSelect){

  var h=14,t,flti;
  t=y+parseInt((Yi-h)/2)-1;

  flti=i?58826:58783;
  moCnvUtils.cnvIconFont(cj,flti,x,t,16,"#4A6671");


  // search field
  if(tt){
    var colsrc=(sF.indexOf(tt)>-1)?"#4A6671":"#DDD";
    moCnvUtils.cnvIconFont(cj,58457,W-40,t,16,colsrc);
  }

}},




UpdateVscr:function(){
with(BkmSelect){
  var n,r;
  
  NAR=0;
  HNAR=0;
  
  n=Ypos.length;
  
  if(Mode) {
    for(r=0;r<n;r++) {
      if(Ypos[r][2]!=4) {
        NAR++;
        HNAR+=(Ypos[r][2]==1)?(Yi/4):Yi;
      }  
    }
  }else {
    NAR=n;
    HNAR=NAR*Yi;  
  }
  
  if(HNAR>Hc && vptr+Hc>HNAR) vptr=HNAR-Hc;
  if(Hc>=HNAR) { Vscr.Display(0); vptr=0; Draw(); }
  else { 
    if(!Vscr.displ) { Vscr.Display(1); }
    Vscr.Resize( { L:Wc-VW, H:Hc, HH:HNAR, ptr:vptr } ); 
  }
}},



//---------------------------------------------------------------------------------
// events

sel_down:function(e){
with(BkmSelect){ moEvtMng.cancelEvent(e); 
  moMenu.Close();
  if(dwn) return false;  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e); 
  Ix=P.x,Iy=P.y,Ivptr=vptr;
  PE=moGlb.getPosition(JE);
  IMV=0;
  if(!isTouch){
    JO.style.display="none";
    TGJ.style.display="block";
    eval("TGJ."+EVTMOVE+"=function(e){BkmSelect.sel_move(e); }");
    eval("TGJ."+EVTUP+"=function(e){BkmSelect.sel_up(e); }");
  }
  dwn=1;
}},

sel_move:function(e){
with(BkmSelect){ moEvtMng.cancelEvent(e);  
  var i,y,v,h, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e,JE);
  if(!dwn){
    i=OvrClick(P.y);
    if(i<0) JO.style.display="none";
    else with(JO.style) top=(aBD[i].y-Yi)+"px", display="block";
  } else {  
    IMV++;    
    if(IMV&1 || !Vscr.displ) return false;  
 
    if(isTouch) P.y-=PE.t;   
      if(HNAR<=Hc) v=0; else  {
        v=Ivptr+Iy-P.y-PE.t;  
        if(v+Hc>HNAR) v=HNAR-Hc;
      }
      if(v<0) v=0;
      vptr=v; 
      Vscr.Resize( { ptr:vptr } ); 
      Draw();   
  } 
}},


sel_up:function(e){
with(BkmSelect){ moEvtMng.cancelEvent(e);  
  var r,i,f,x,y,ck,k;   
  // click
  if(IMV<4){
    r=OvrClick(Iy-PE.t);
    if(r>-1){
      i=aBD[r].idy; f=Ypos[i][0];
      x=Ix-PE.l;
 
          if(Mode){  // grouped
            k=Ypos[i][2];
     
            if( (k==2 || k==3) && (x>36 && x<64) ) OpenClose(i); 
            else addCHK(i,x);
          
          } else {   // sorted
            if(!Ypos[i][2]) addCHK(i,x);
          }    
     }
  }
  dwn=0;
  TGJ.style.display="none";

}},



addCHK:function(i,x){
with(BkmSelect){

  var r,ck,ty,n,m,c;
  
  if(x>274){
    m=Ypos[i][0];
    
    if(m<0){  // group
    
    
    }else{    //field
    
      c=jh[m].fieldb;
      n=sF.indexOf(c);
      if(n<0) sF.push(c);
      else sF.splice(n,1);
 
      Draw();
    }
    
  }else{
  
      ck=Ypos[i][3];
      ck=ck?0:1;         
      Ypos[i][3]=ck;
      
      if(Ypos[i][2]&2){ // group row
      
        for(r=i+1;r<Ypos.length;r++){
          ty=Ypos[r][2];
          if(ty==1) continue;
          if(ty==2 || ty==3) break;   
          Ypos[r][3]=ck;
          jh[ Ypos[r][0] ].hidd=ck;
        }  
      
      } else { // field
      
        jh[ Ypos[i][0] ].hidd=ck;   
        if(Mode){   
          m=Ypos[i][4]; // rif group
          if(!ck) Ypos[m][3]=0;
          else {
            n=0; c=0;
            for(r=m+1;r<Ypos.length;r++){
              ty=Ypos[r][2];
              if(ty==1) continue;
              if(ty==2 || ty==3) break;
              c++;
              n+=Ypos[r][3];
            } 
            if(c==n) Ypos[m][3]=1; // all children are checked
          } 
        }  
      }          
      Draw();
  }
}},



OpenClose:function(i){
with(BkmSelect){
  var r,st,ty;
  st=Ypos[i][2];   // 3=close  2=open   
    Ypos[i][2]=(st==3)?2:3;  
    for(r=i+1;r<Ypos.length;r++){
      ty=Ypos[r][2];
      if(ty==1) continue;
      if(ty==2 || ty==3) break;
      ty=(st==3)?0:4;
      Ypos[r][2]=ty;
    }          
  Draw();
  UpdateVscr();
}},


OvrClick:function(y){
with(BkmSelect){
  var r,y1=0,y2,v=-1;
  for(r=0;r<aBD.length;r++){
    y2=aBD[r].y;
    if(y>=y1 && y<y2) { v=r; break; }
    y1=y2;
  }
  return v;
}},



Vbarpos:function(i){
with(BkmSelect){
  vptr=i;  
  Draw();  
  if(!isTouch) JO.style.display="none";
}},


f_save:function(){
with(BkmSelect){
  try{ 
    Tj.chgHeaderUpdt(jh);    // update treegrid Tj
    Tj.searchField=moGlb.cloneArray(sF);
  }catch(e){ alert(moGlb.langTranslate(e)+moGlb.langTranslate("\n\ntreegrid method not fount")); }      
  f_close();
}},


f_close:function(){
  moEvtMng.subEvent(document,"keyup",BkmSelect.f_keyup);  
  BkmSelect.Win.hide();
},

f_keyup:function(e){
  if(!e) e=window.event;
  var key = (e.which) ? e.which : e.keyCode;
  if(key==27) BkmSelect.f_close();
},

} // end



//------------------------------------------------------------------------------


GlobalSearch={ Tj:null, J:null, Win:null, jtx:null, Name:"",
    fnt:"font:13px OpensansR;",
    old_text:{}, 
    isactive:{},
                   


Start:function(objname,container,head,wn){
with(GlobalSearch){
              
  Name=f_mdl(objname);

  Win=wn;
  Tj=(typeof(objname)=="string")?eval(objname):objname;                         // object treegrid  
  J=(typeof(container)=="string")?mf$(container):container;   

  draw();
}},

                
f_mdl:function(m){
  var a=m.split("_");
  return (a[0]+"_"+a[1]);
},



draw:function(){
with(GlobalSearch){

  var s,b;
    
  b=fnt+"color:#FFF;background-color:#A1C24D;border:0;border-radius:2px;cursor:pointer;' "+
    "onmouseover='this.style.backgroundColor=\"#E0E0E0\"' "+
    "onmouseout='this.style.backgroundColor=\"#A1C24D\"' "+
    "onmousedown='this.style.backgroundColor=\"#6F8D97\"' onmousemove='return false;'";  
    
  s="<input id='id_inp_GlobalSearch' type='text' value='' style='position:absolute;left:0px;top:20px;width:100%;height:27px;"+
     "border-radius: 2px;border:1px solid #4A6671;box-sizing:border-box;"+fnt+"padding-top:3px;' onkeyup='GlobalSearch.f_keyup(event)' />"+
  
     "<div style='position:absolute;left:0;width:100%;bottom:3px;text-align:center;'><span style='font:16px FlaticonsStroke;color:#4A6671;cursor:pointer;' "+
     "onclick='"+Tj.name+".fieldlist(event)'>"+String.fromCharCode(58445)+"</span></div>"+
     
     "<button style='position:absolute;right:0;bottom:0;width:60px;height:28px;"+b+" onclick='GlobalSearch.f_search()'>"+moGlb.langTranslate("Search")+"</button>"+
     "<button style='position:absolute;left:0;bottom:0;width:60px;height:28px;"+b+" onclick='GlobalSearch.f_close()'>"+moGlb.langTranslate("Cancel")+"</button>";
     
  J.innerHTML=s;
  jtx=mf$("id_inp_GlobalSearch");
  if(!old_text[Name]) old_text[Name]="";
  jtx.value=old_text[Name];
  jtx.setSelectionRange(0, jtx.value.length);
  jtx.focus();
  
}},


f_active:function(status,name){
with(GlobalSearch){

  var nm=(!name)?Name:(f_mdl(name));
  isactive[nm]=status;

  if(!status) old_text[nm]="";
 
  if(nm){
   f_enable_disable(0);
  }
}},


f_enable_disable:function(lock,name){
with(GlobalSearch){
  var snm, nm, srj;
  
  nm=(!name)?Name:(f_mdl(name)); 
  snm="id_"+nm+"_crud_header_search";    // id_mdl_wo_crud_header_search
   
  try{ 
     srj=mf$(snm); 
     srj.style.color=(lock)?"#AAA":(isactive[nm]?"#A1C24D":"#FFF"); 
  }catch(e){}       
}},

f_keyup:function(e){
  if(!e) e=window.event;
  var key = (e.which) ? e.which : e.keyCode;
  if(key==13) GlobalSearch.f_search();
  if(key==27) GlobalSearch.f_close();
},


f_search:function(){
with(GlobalSearch){
  f_close();
  var t=jtx.value;
 
  if(!t && !old_text[Name]) {
    f_active(0);
    return;
  }
  
  f_active( t?1:0 );
  old_text[Name]=t;
  Tj.actLike(t);

}},

f_close:function(){
  GlobalSearch.Win.hide();
}

} //

