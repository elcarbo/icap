/*------------------------------------------------------------------------------------
    
// button methods
 
f_insert_folder()
  parent folder item
  child folder item
  item from list
  new item
  
deleteSelected();             
f_copy
f_paste

f_up            // move up
f_down

f_tofinal      // trasform in final




updateChecked(arrJStore);
deleteSelected();
saveList();

  
f_creationCode()      genera un codice univoco negativo temporaneo da tradurre al save della rit

//------------------------------------------------------------------------------------
*/


function moTreeGrid_light_pricelist(nm,z) {


// props
this.name=nm;

this.create = function (z) {
var props={ pairCross:0,
            Vscr:null, Oscr:null, bookmark_name:'', bookmark_id:0, moduleName: "",
            checkmode:1, 
            f_code:0, Ttable:"t_pricelist", crud:"", ftype:25, J:null, jj:null, W:0, H:0, zL:0, zT:0,  ig:[],
            nodrw:0, sescr:0, sedrw:3, SEDRW:3, Stp:40, timelock:5000, tUlk:1,
            NOEVT:0, noAnim:0, 
            mgL:0, mgT:0, mgR:0, mgB:0, VW:18, padf:4, iext:0, RX1:0, szW:32,                                                       // margini
            HeadH:24, cnvLS:56, mecn:14, Hlw:0, Huw:0, NL:0, NU:0, NT:0, OH:0, SH:0,                                    // cnvLS w canvas locked e selected | head lock w | head unlock w
            msgH:0, adds:0, upds:0,
            CNV:null, CNJ:null, BDN:null, oBDN:null, BDJ:null, MBDJ:null, OBDJ:null, HBDJ:null, maskj:null, ccMm:0,
            IMD:null, OMD:null, FMD:null, TGJ:null,
            optr:0, vptr:0, OVR:-1, SEL:-1, bOVR:-1, skipwf: false,
            Hy:0, displ:1, lkEdit:0, unlkEdit:0,                                              
            EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove', oldmouseup:"", NMM:0, Cur:0, 
            dwnBD:0, noWH:0,
            Hnr:0, PXi:0, PYi:0,  
            First:1,
            fcreationCode:-1,
            typeid_main:1,
            rowH:0,
            DTAB:"",
            QoF:1,
            aBKM:[],
          }
            
for(var i in props) this[i]=(typeof z[i]=='undefined')?props[i]:z[i];
this.Yi=32;            // row height
}


this.Yi=32;           
this.Yt=0; 
this.AR=[];
this.mCK=[];
this.JStore={};


this.arrNd=[];

this.Header=[]; 
this.Abody=[];
this.ROWBKCOL=["#FFFFFF","#FAFAFA","#FEFF9E"];
this.ROWBDCOL="#EDEDED";
this.ROWCOLTXT=moColors.text;
 
this.arrCHK=[];         // locked by checkbox 
 
this.Ypos=[];
this.rifY=[];
this.aRange=[];
this.aPRange=[];
this.aFilter=[];

this.IM=moGlb.baseUrl+"/public/msim_images/default/";

this.Curs=["default","default","col-resize"];

this.menulist=[];


// metodi
this.getHeader=getHeader;
this.setHeader=setHeader;
this.Display=Display;                                          // IN
this.resize=resize;

this.updateChecked=updateChecked;
this.deleteSelected=deleteSelected;
this.f_remove=f_remove;
this.f_cut=f_cut;
this.f_copy=f_copy;
this.f_paste=f_paste;

this.f_addChild=f_addChild;
this.f_up=f_up;
this.f_down=f_down;
this.f_tofinal=f_tofinal;
this.f_insert_folder=f_insert_folder;

this.f_add_elements=f_add_elements;
this.f_insert_item=f_insert_item;

this.f_delTree=f_delTree;
this.f_del=f_del;
this.f_swap=f_swap;
this.NextYposSibling=NextYposSibling;
this.hasChild=hasChild;
this.saveList=saveList;
this.f_creationCode=f_creationCode;

this.predraw=predraw;
this.draw0=draw0;
this.draw1=draw1;
this.draw=draw;
this.draw2=draw2;
this.ClearB=ClearB;
this.fMaskLoad=fMaskLoad;

this.Cell=Cell;
this.hCell=hCell;

this.RowCells=RowCells;
this.RCells=RCells;
this.clipRow=clipRow;

this.loadroot=loadroot;
this.retroot=retroot;

 
this.HnrRifPos=HnrRifPos;
 
this.SeChkDechk=SeChkDechk;
this.deCheck=deCheck;

this.Vbarpos=Vbarpos;
this.Obarpos=Obarpos;  
this.scrollO=scrollO;

this.headown=headown; 
this.headmove=headmove;
this.headout=headout;

this.oversel=oversel;
this.overmove=overmove;
this.overMove=overMove;

this.bodymove=bodymove;
this.bodyout=bodyout;
this.bodydown=bodydown;
this.bodyup=bodyup;
this.drawbodyover=drawbodyover;

this.maskmove=maskmove;
this.maskup=maskup;
this.headRitAnimat=headRitAnimat;
this.endAnim=endAnim;

this.fieldlist=fieldlist;

this.acthidden=acthidden;
this.actlock=actlock;

this.updHeader=updHeader;
this.fmenuList=fmenuList;

this.OpenP=OpenP;
this.CloseP=CloseP; 

this.getSelectedRows=getSelectedRows;


this.colrs=["#F0F0F0","#A1C24D","#4A6671","#6F8D97","#E2EECA","#E0E0E0","#A0A0A0","#DDDDDD","rgba(161,194,77,0.3)"];

 
// IN --------------------------------------------------------------------------



function f_add_elements(){
with(this){

  moGlb.QoF=QoF;   
  mdl_wo_prl_tg.addElements();

}}


function updateChecked(arr){     // arr contiene JStore dei selezionati!
with(this){
  
  if(!arr.length) return false;
  var f,r,fc,g,gg,rd,J,fs,i, rtr, o, jt, pd, res, rdo,
      nn=arrCHK.length;    


  if(!nn || nn>1) {  // root  
     nn=0;   
     rd=DBL.query("SELECT MAX(fc_order) FROM "+DTAB+" WHERE fc_code_parent=0");    // max sibling order
     rd++; 
     rtr="0";
     pd=0;
     
  } else {
  
    fs=arrCHK[0];
    i=rifY[fs];
    o=Ypos[i];
    jt=JStore[fs];
        
    if(o.fc_type==1){  // child  if folder
    
        rtr=o.rtree+"|"+fs;
        rd=DBL.query("SELECT MIN(fc_order) FROM "+DTAB+" WHERE fc_code_parent="+fs);
        rd-=arr.length;  
        pd=fs;        
                                      
 
    } else {  // sibling

        rd=jt.fc_order; 
        rtr=o.rtree;
        pd=jt.fc_code_parent;
        rdo=rd+arr.length;
        
        res=DBL.query("SELECT fid,f_id FROM "+DTAB+" WHERE fc_code_parent="+pd+" AND fc_order>"+rd+" ORDER BY fc_order");
                
        for(r=0;r<res.length;r++){  
          JStore[ res[r].f_id ].fc_order=rdo+r;
          DBL.query("UPDATE "+DTAB+" SET fc_order="+(rdo+r)+" WHERE fid="+res[r].fid);
        }
                                       
    }
  }
  

  for(r=0;r<arr.length;r++) {
  
      f=f_creationCode();              // f_id
      fc=parseInt(arr[r].f_code);      // fc_code_prl
 
      JStore[f]={ f_id:f, fc_type_prl:QoF };
      J=JStore[f];
      
        for(g in arr[r]) {          
          gg=g;
          if(g=="f_title") gg="fc_wo_prl_title";
          if(g=="f_description") gg="fc_wo_prl_description";
          if(g=="f_code") gg="fc_code_prl";    
          if(g.substring(0,6)=="fc_prl") { gg="fc_wo"+g.substring(2); }
          
          if(aBKM.indexOf(gg)==-1) continue;     // filter by bookmark
          JStore[f][gg]=arr[r][g];
        }
      
      J.fc_type=0;
      J.rtree=rtr;
      J.fc_code_parent=pd;
      J.fc_order=rd;
        
      for(g=0;g<aBKM.length;g++) { gg=aBKM[g]; if( typeof(J[gg])=="undefined" ) JStore[f][gg]=""; }
    
      if(!nn){
          
          Ypos.push({posY:0, fcode:f, f_parent:pd, fc_type:0, open:0, rtree:rtr} );
                    
        } else {  
          if(o.open || !o.fc_type){        // if open or no folder
              i++;   
              Ypos.splice(i,0,{ "posY":0, "fcode":f, "f_parent":pd, "fc_type":0, "open":0, "rtree":rtr }  );       
          }
        }
        
    
                 
      rd++;        
      DBL.query("INSERT INTO "+DTAB,J);
  }  

  HnrRifPos();
}}


 

this.saveJob=function(arr) {
with(this){



  HnrRifPos();
}}




this.rowNumber=function(){ with(this){ var i,n=0; for(i in JStore) n++; return n; }};

this.refresh=function(){ this.HnrRifPos(); };

function Display(i){
with(this){
  if(i==displ) return;
  if(i) {var nb="block";displ=1;}
  else {var nb="none";displ=0;}
  j.style.display=nb;
}}



function resize(l0,t0,w0,h0,i){ 
with(this){ 

  if(typeof jj=="undefined" || !jj) {
    J=mf$(i);
    jj=moGlb.createElement("id"+name,"div",J);
    jj.style.overflow="hidden";   
  
    if(First) loadroot(); 
    draw0();     
                
    // scrollbars   
    eval("Vscr"+name+"=new OVscroll('Vscr"+name+"',{ displ:0, L:-100, T:0, H:h0, HH:1, Fnc:'"+name+".Vbarpos', pid:'"+jj.id+"' }); VW=Vscr"+name+".W;"); 
    eval("Oscr"+name+"=new OVscroll('Oscr"+name+"',{ displ:0, OV:1, L:0, T:0, H:w0, HH:1, Fnc:'"+name+".Obarpos', pid:'"+jj.id+"' });");    
  } 
        
    jj.style.left=l0+"px";
    jj.style.top=t0+"px";
    jj.style.width=w0+"px";
    jj.style.height=h0+"px";  
    W=w0, H=h0;
  
    var p=moGlb.getPosition(jj);   
    zL=p.l, zT=p.t;    
    if(Hlw) predraw();     
    if(!First) { sedrw=2; draw(); }   
}}



 
//--------------------------------------------------------------------------------------------------------------------------------------- 
// LOAD records


function loadroot(){           // type del ware cross
with(this){    
    
    QoF=(name.indexOf("2")>-1)?2:1;   // 1=QUOTE   2=FINAL
    
    var ja=JSON.stringify({Level:0, Tab:[Ttable, ftype], f_code:f_code, qof:QoF});  
                 
//////////////
/*
var a={
"bookmark":[
  ["fc_wo_prl_item_code","Item code",0,1,1,90,[],[],[],"",-1,""],
  ["fc_wo_prl_title","Title",0,1,1,280,[],[],[],"",-1,""],
  ["f_id","System Code",0,0,0,200,[],[],[],"",-1,""],
  ["fc_wo_prl_description","Description",0,0,1,220,[],[],[],"",-1,""],
  ["fc_wo_prl_unit_price","Unit_price",0,0,1,120,[],[],[],"",-1,""],
  ["fc_wo_prl_um","U.M.",0,0,1,60,[],[],[],"",-1,""],
  ["fc_wo_prl_quantity","Quantity",0,0,1,80,[],[],[],"",-1,""],
  ["fc_wo_prl_cost","Cost",0,0,1,150,[],[],[],"",-1,""],
    
  ["fc_wo_prl_custom","Custom",0,0,1,120,[],[],[],"",-1,""]
], 
"data": [
  [ "f_id",
    "fc_wo_prl_item_code",
    "fc_wo_prl_title",
    "fc_wo_prl_description",
    "fc_wo_prl_unit_price",
    "fc_wo_prl_um",
    "fc_wo_prl_quantity",
    "fc_wo_prl_cost",
    "fc_wo_prl_timestamp",       // timestamp of modifiy    
 //   "fc_code_wo",            // f_code workorder
    "fc_code_prl",             // price list ware code | 0 if custom row
    "fc_type_prl",             // 1=quote   2=final
    "fc_type",                 // 0=item   1=folder   2=total
    "fc_code_parent",          // parent
    "fc_order",                // sibling order 
    "rtree",                   // parent tree    
    "fc_wo_prl_custom"         
  ],
  [
    10,
    "cd010",
    "listino1",
    "descrizione1",
    "150",
    "pezzi",
    "4",
    "600", 
    0,
    1,
    1,
    0,
    0,
    3,
    "0",
    "custom1"
  ], 
  [
    11,
    "cd011",
    "listino2",
    "descrizione2",
    "120",
    "pezzi",
    "2",
    "480",  
    0,
    1,
    1,
    0,
    0,
    1,
    "0",
    "custom2"
  ], 
  [
    12,
    "",
    "folder1",
    "cartella",
    "",
    "",
    "",
    "",  
    0,
    0,            // price list | 0 custom item 
    1,            // quote
    1,            // 0=item   1=folder   2=total
    0,            // parent
    2,            // order
    "0",          // rtree
    ""            // custom
  ],
    [
    13,
    "cd013",
    "listino3",
    "descrizione3",
    "130",
    "pezzi",
    "2",
    "480",  
    0,
    1,
    1,
    0,
    12,          // parent
    1,
    "0|12",      // rtree
    "custom3"
  ], 
  [
    14,
    "cd014",
    "listino4",
    "descrizione4",
    "120",
    "pezzi",
    "2",
    "480",  
    0,
    1,
    1,
    0,
    12,          // parent
    2,
    "0|12",      // rtree
    "custom4"
  ], 
  [
    15,
    "",
    "folder 15",
    "f5",
    "120",
    "pezzi",
    "2",
    "480",  
    0,
    1,
    1,
    1,
    12,          // parent
    3,
    "0|12",      // rtree
    "custom5"
  ],
  [
    16,
    "cd016",
    "listino6",
    "descrizione4",
    "120",
    "pezzi",
    "2",
    "480",  
    0,
    1,
    1,
    0,
    15,          // parent
    1,
    "0|12|15",      // rtree
    "custom6"
  ]
 
] 
  
};
      
 
*/
//////////////   
   
   
   
   Ajax.sendAjaxPost( moGlb.baseUrl+"/pricelist/jax-read", "param="+ja , this.name+".retroot" );
 
}} //



// tipo di root e personal setting field
function retroot(a){                         
with(this){    

  var ar;
  try{ ar=JSON.parse(a); } catch(ex) { moDebug.log(moGlb.langTranslate("Missing bookmark for module")+" "+name); return; }

 
  var NT=0, D=ar.data, nar=D.length,  fcd, fcp, r,n,i, s,resp,ry; 
 
 
  moMenu.IM=IM; 
  setHeader(ar.bookmark);
   
  JStore={};    
  Ypos=[];
  arrCHK=[];
 
 
  s="CREATE DATABASE if Not EXISTS mainsim3";                                   // create database
  resp=DBL.query(s); if(!resp) { alert("db light error!"); return false;  }
  DBL.connect("mainsim3");
 
  DTAB="t_"+name;
 
  s="CREATE TABLE IF NOT EXISTS `"+DTAB+"` ("+                                  // create table  with alla data need to extract Ypos children 
    "`fid` int(10) unsigned NOT NULL AUTO_INCREMENT,"+ 
    "`f_id` int,"+
    "`fc_code_parent` int,"+
    "`fc_code_prl` int,"+
    "`fc_type_prl` int,"+
    "`fc_type` int,"+
    "`fc_order` int,"+
    "`rtree` string,"+
    "PRIMARY KEY (`ai`),"+
    "KEY (`f_id`),"+
    "KEY (`fc_code_parent`)"+
    ");";

  resp=DBL.query(s);
  if(resp===false) { alert(DBL.ER+" | "+DBL.error() ); return; }
 
  // truncate
  DBL.query("TRUNCATE "+DTAB);
  
  aBKM=[];
  var ahe=[], rhe=[], ng=D[0].length, g;
  for(r=0;r<ng;r++) { ahe[r]=D[0][r];  aBKM.push(D[0][r]);  rhe[D[0][r]]=r; }           // ahe[rhe] = indice      rhe[indice] = r      posiz indice
 
  // JStore[f_id]
  for(r=1;r<nar;r++){
  
    // no total fc_type=2 
    if(D[r][ rhe["fc_type"] ] ==2 ) continue; 
  
    fcd=parseInt( D[r][rhe["f_id"]]);
    
    JStore[fcd]={}; 
    for(g=0;g<ng;g++) {
      vd=D[r][g];
      if(vd==null) vd="";
      if(ahe[g]=="rtree" && !vd) vd="0";
      JStore[fcd][ahe[g]]=vd;   
    }
    
    DBL.query("INSERT INTO `"+DTAB+"`",JStore[fcd]);
  }
  
  // ConsoleL.View();  
 
  // get Ypos with parent root 0
  s="SELECT * from "+DTAB+" WHERE fc_code_parent=0 order by fc_order";
  resp=DBL.query(s);
  
  for(r=0;r<resp.length;r++){
    ry=resp[r];
    Ypos.push( {posY:0, fcode:ry.f_id, f_parent:ry.fc_code_parent, fc_type:ry.fc_type, open:0, rtree:ry.rtree } )
  }
  
 
 
  HnrRifPos();
  eval(crud).oncompletion();
}} //



          
 
 


// DRAW -------------------------------------------------------------------------------------------------------------------------- 


function predraw(){
with(this) {
    try{
      eval("Vscr"+name);
    } catch(ex) {return;}

 // resize srollb
  var sl=W-VW-mgR, st=mgT+HeadH+msgH, 
      ol=mgL+cnvLS+Hlw, ot=H-VW-mgB; 
      
      OH=W-cnvLS-Hlw-mgL-mgR;
      SH=H-HeadH-VW-mgB-mgT-msgH;
      
  eval("Vscr=Vscr"+name);
  eval("Oscr=Oscr"+name);
    
  if(SH<64 || W<80) Vscr.Display(0);else {if(!Vscr.displ) Vscr.Display(1);Vscr.Resize( {L:sl, T:st, H:SH, HH:Hnr*Yi} );}
  if(OH<64) Oscr.Display(0);else {if(!Oscr.displ) Oscr.Display(1);Oscr.Resize( {L:ol, T:ot, H:OH, HH:Huw} );}

 // resize infogrid 
 mf$("infogrid"+name).style.width=ol+"px";
 
 draw1();
}}                                                                                                                   


function draw0(){
with(this){ 

  if(isTouch) EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';  
    
    var hbody=H-HeadH-VW-mgT-mgB-msgH,onm="",z,s="",zz,c2,tc,lc;
    
  z="z-index:", zz=0; lc=20+mgL, tc=(4+mgT)-3;

  if(isTouch) {  
     onm=EVTDOWN+"='"+name+".bodydown(event)' "+EVTMOVE+"='"+name+".bodymove(event)' "+EVTUP+"='"+name+".bodyup(event)' ";
  
  } else {     
    onm=" onmouseover='WHEELOBJ=\"Vscr"+name+"\"' onmouseout='"+name+".bodyout(event)' "+EVTMOVE+"='"+name+".overMove(event)' "+EVTDOWN+"='"+name+".bodydown(event)' ";
   
    TGJ=moGlb.createElement("maskTreegridDragScroll","div",document.body);
    TGJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(moGlb.zloader+500));      
  }  
  
   // left head
  s="<canvas id='lcksel"+name+"' style='position:absolute;left:"+mgL+"px;top:"+mgT+"px;"+z+zz+";' width='"+cnvLS+"' height='"+HeadH+"'></canvas>"+
  
    "<div id='checklcksel"+name+"' style='position:absolute;left:"+lc+"px;width:34px;top:"+tc+"px;"+
    z+(zz+1)+";cursor:pointer;color:"+colrs[5]+";font:16px FlaticonsStroke;text-align:center;'"+
    " onclick='"+name+".SeChkDechk()'>"+String.fromCharCode(58826)+"</div>"+ 
 
    // head      
    "<canvas id='headlu"+name+"' style='position:absolute;left:"+(mgL+cnvLS)+"px;top:"+mgT+"px' width='"+Hlw+"' height='"+HeadH+"' ></canvas>"+       
    "<div id='idmsgrev"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH)+"px;width:10px;height:"+msgH+";overflow:hidden;"+
    "background-color:#f0f0f0;cursor:pointer;'></div>"+
          
    "<canvas id='bodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px' width='10' height='"+hbody+"'></canvas>"+ 
    "<div id='overbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px;width:10px;height:"+hbody+";overflow:hidden;'></div>"+
              
    "<div id='maskbodylu"+name+"' style='position:absolute;left:"+mgL+"px;top:"+(mgT+HeadH+msgH)+"px;width:10px;height:"+hbody+";overflow:hidden;background-color:#ff0;opacity:0.0;' "+onm+"></div>"+
 
     // header
    "<div style='position:absolute;left:"+mgR+"px;top:"+mgT+"px;width:"+VW+"px;height:"+HeadH+"px;cursor:pointer;' onclick='"+name+".fieldlist(event)'>"+
    "<div style='margin:2px 0 0 2px;'>"+moCnvUtils.IconFont(58445,16,colrs[2])+"</div>"+
    "</div>"+

    // footer info
    "<div id='infogrid"+name+"' style='position:absolute;left:"+mgL+"px;bottom:"+mgB+"px;height:"+VW+"px;width:"+cnvLS+"px;overflow:hidden;'>"+
    "<div id='infotxt"+name+"' style='font:13px opensansR;margin:2px 4px 0 8px;color:#000;'></div></div";
           
  jj.innerHTML=s; 
  CNJ=mf$("headlu"+name), CNV=CNJ.getContext("2d");
  BDJ=mf$("bodylu"+name), BDN=BDJ.getContext("2d");
  MBDJ=mf$("maskbodylu"+name);
  OBDJ=mf$("overbodylu"+name);
  HBDJ=mf$("idmsgrev"+name);
 
  eval("CNJ."+EVTDOWN+"=function(e){"+name+".headown(e); }");
  if(!isTouch){
    eval("CNJ."+EVTMOVE+"=function(e){"+name+".headmove(e); }");
    eval("CNJ.onmouseout=function(e){"+name+".headout(e); }"); 
    
  } else {
    eval("CNJ."+EVTMOVE+"=function(e){"+name+".maskmove(e); }");
    eval("CNJ."+EVTUP+"=function(e){"+name+".maskup(e); }");   
  } 
 
     // drag drop 
  MBDJ.addEventListener('drop', function(e){  moEvtMng.cancelEvent(e); }, false);
  MBDJ.addEventListener('dragover',function(e){moEvtMng.cancelEvent(e);},false);
}}



// caching per il ridisegno dei canvas head e body
function draw(tt){      
with(this){
  SEDRW!=sedrw;      
  if(!tt){
    if(!nodrw){tt=1; nodrw=1;}
    else return;
  }
  if(tt>1){
    if(SEDRW&1) draw1();
    if(SEDRW&2) draw2();
    nodrw=0; sedrw=3; 
    return;
  }
  tt++;
setTimeout(name+".draw("+tt+")",Stp);
}}





function draw1(){
with(this){           
  if(W<=moGlb.moduleMinWH){Display(0);return;} // dimensione minima
  Display(1);
 
  // cella locked-selected
  var cnv=mf$("lcksel"+name).getContext("2d"), c2=(cnvLS-mecn)/2;
  Cell(cnv,0,c2,0);
  Cell(cnv,c2,cnvLS,0); 

  var nk=arrCHK.length; 
  ck=(nk<1)?0:1; 
  
  mf$("checklcksel"+name).style.color=colrs[ck?2:5];                
 
  // celle lock & unlock 
  CNJ.width=Hlw+OH;      
  for(var r=0;r<NT;r++) {if(Header[r].hidd) hCell(r);}
}}

 
 

function draw2(no){   
with(this){if(!no) no=0;   
 
  // bodywheel 
  if(!no){var uw=cnvLS+Hlw+OH;
  with(BDJ) width=uw, height=SH;
    MBDJ.style.width=uw+"px"; MBDJ.style.height=SH+"px";
    OBDJ.style.width=uw+"px"; OBDJ.style.height=SH+"px";
    HBDJ.style.width=W+"px"; 
  }
  
  var nar=Ypos.length;
   
  Yt=vptr%Yi, Yt*=-1;                                           // Yt delta posizione iniziale
  var n0=(SH-Yt);nr=parseInt(n0/Yi), nr+=(n0%Yi)?1:0;          // nr = numero righe visualizzabili nel canvas top = Yt

  //Ypos 
  var p0,p1, Yx=0, y=vptr+Yt; 
                                    
  // cerco inizio visualizzazione  Yx
  for(var r=0;r<(nar-1);r++) { 
    p0=Ypos[r].posY, p1=Ypos[r+1].posY;  
    Yx=r;
    if(vptr>=p0 && vptr<p1) break; 
    Yx=nar-1;
  }
  
  Abody=[]; arrNd=[]; 
  var Y=Yt, x=Yx, or=0, cy=y/Yi, c, nbk=0, pc,ps,cr,bx,p1=0, fx=x;                              
  ClearB(0);
  
// disegno nr righe a partire da Yt
  for(r=0;r<nr;r++){ 
     if(typeof(Ypos[x])=="undefined") break;
     
     p0=Ypos[x].posY;    
     c=cy&1;
  
     RowCells(Ypos[x],Y,c);                                 
     x++;                                                   // Ypos successivo

     y+=Yi;Y+=Yi;cy++;
  } 
  
  ClearB(1);   
    
  // script generale treegrid light se esiste   
  var scrpt=name+"_script";
  if(moLibrary.exists(scrpt)) eval(moLibrary.getSource(scrpt));  
}}

 


function RowCells(Ypx,y,c){                    // Ypx = Ypos { }
with(this)  {
 
  var bdw=cnvLS+Hlw+OH, k, bkg,brd,g;
   
  // sfondo riga 
  with(BDN){ 
    k=1; 
    bkg=ROWBKCOL[c];   
    fillStyle=bkg;
    fillRect(0,y+k,bdw,Yi-2*k);
    fillStyle=ROWBDCOL;
    fillRect(0,y+Yi-1,bdw,1);
    
    // evidenzia
    for(r=0;r<NT;r++) with(Header[r]) if(hidd && r==SEL) {
      save(); 
        g=clipRow(x1,x2,y,lock);
        if(!g) break;
        if(r==SEL) {fillStyle=colrs[8];                                         
        fillRect(g[0],y,g[1]-g[0],Yi);}                            
      restore();
      break;
    }                                                                               
  } 

  // Rcelle lock & unlock 
  var fc=Ypx.fcode, r, afc=JStore[fc]; 

  var dd=29, vv=y+parseInt((Yi-16)/2), slt=0, flti;
 
  if(arrCHK.indexOf(fc)>-1) slt=1;
  flti=slt?58826:58783;
  moCnvUtils.cnvIconFont(BDN,flti,dd,vv,16,colrs[2]);    // checked

  Abody.push({y:y, f:fc}); 
  
  // colore testo riga in aPRange
  var f,v,nv,i,cz,vz,tz,virg, colr=ROWCOLTXT, stz;
  
  for(var r=0;r<aPRange.length;r++){
    f=aPRange[r].field;nv=aPRange[r].condz.length, tz=aPRange[r].typ;
    v=afc[f];virg=(!tz)?"'":"";stz = {"{val}": virg+v+virg};   
    
    for(i=0;i<nv;i++){
      cz=aPRange[r].condz[i].exp, vz=aPRange[r].condz[i].val;
      cz=cz.multiReplace(stz); 
      eval("if("+cz+") colr='"+vz+"';");
      if(colr!=ROWCOLTXT) break;
    }
  } 
   
  for(r=0;r<NT;r++) if(Header[r].hidd) RCells(afc, r, y, Ypx, colr);             // cell 
}}




function RCells(rig, r, y, Ypx, colr){       //  progre LStore,  r per Header[r]  fieldb, x1,x2, ...
with(this)  {
  var X1,X2,x21,c1,c2, lr=OH+Hlw, t="", f="13px opensansR", d=0, nh, v, pair_script="", sp, nd=0,d=0, yy, flti,flic;
  
  with(Header[r]) with(BDN){     
    X1=x1, X2=x2;
    if(!lock) {X1-=optr, X2-=optr; if(X2<=Hlw || X1>=lr) return;}
  
    save();
    x21=X2-X1-padf*2, X1+=cnvLS+padf;
    t=rig[fieldb], v=t;
      
    if(!Ypx.fc_type){                                                                   // if item script     fieldb +  _pricelist    (in moLibrary default.js )
      // se esiste delegates _pricelist  =>   v!="undefined"   
      sp=moLibrary.getSource(fieldb+"_pricelist");
      if(sp){ v=0; pair_script=fieldb+"_pricelist"; }       
    } 
    
    if(typeof(v)=="undefined") return; 
    
    // clip  
    clipRow(x1,x2,y,lock);
  
    if(type<5){                                                                                           
      font=f;textBaseline="alphabetic";textAlign="left";     

      if(fieldb.indexOf("_title")>-1){                                                      // if tree  (f_title)
        nh=Ypx.fc_type;                                                            
        yy=y+(Yi-16)/2;   
        
        nd=((""+Ypx.rtree).split("|")).length-1;
        if(nd<0) nd=0;
        d=nd*16;
   
        if(nh==1){                                                                // img 4open  6folder  
          flti=58364, flcol=colrs[2], flic=+Ypx.open;
          if(flic) flti=58369;                                                    // folder icon
          moCnvUtils.cnvIconFont(BDN,flti, X1+d+19, yy-1,16,flcol); 

        
       //  if(hasChild(Ypx.fcode)){}
            flti=58825, flic=+Ypx.open;                                            // plus/minus icon  
            if(!flic) flti=58824;
            moCnvUtils.cnvIconFont(BDN,flti, X1+d, yy+1,14,colrs[6]);
            arrNd.push({l:X1+d-5, t:yy, oby:Ypx});
            
        } else {    
                   
          moCnvUtils.cnvIconFont(BDN,58355, X1+d+19, yy,16,colrs[2]);            // item
  
        }                                   
          d+=32; x21-=d; if(x21<16) x21=16;
      }   
   
   
      if(pair_script || (script && moLibrary.exists(script))) {                  // se esiste delegate script
         var xfield=X1+d, yfield=y, vfield=v, f_cod=Ypx.fcode; 
  
         var TIPtext="";
         fillStyle = colr;
         
         if(pair_script) {
             eval(moLibrary.getSource(pair_script));
         
         } //else{ if(!v || v == "0") {} else {eval(moGlb.delegates[script]);} }			
            eval(moLibrary.getSource(script));
         } else {
          if(type==4) { try { t=v; if(moGlb.isset(aFilter[fieldb][v])) t=moGlb.langTranslate(aFilter[fieldb][v]); }catch(e){}  }
          //else if(type==2) eval(moLibrary.getSource('bool'));
         if(moCnvUtils.measureText(t,f)>x21) t=moGlb.reduceText(t,f,x21);
   
          var cz,vz,tz,virg, ccl=colr, virg=(!type)?"'":"", stz = {"{val}": virg+v+virg};         
          if(aRange[fieldb]){
          for(var g=0;g<aRange[fieldb].length;g++) { 
          cz=aRange[fieldb][g].exp, vz=aRange[fieldb][g].val; 
          cz=cz.multiReplace(stz); 
          eval("if("+cz+") ccl='"+vz+"';");  
          if(ccl!=colr) break;}
          }
  
          fillStyle=ccl;                                        
          fillText(t,X1+d+7,y+8+(Yi-7)/2); 
      }
    } else {                                                   //   per type 5
            
    }  
   
   restore(); 
}
 

}}





function clipRow(X1,X2,y,lock){ 
with(this) with(BDN) {
  var lr=OH+Hlw;
  if(!lock){
    X1-=optr, X2-=optr; 
      if(X2<=Hlw || X1>=lr) return false;
      if(X1<Hlw) X1=Hlw;
      if(X2>lr) X2=lr;}
 
   X1+=cnvLS, X2+=cnvLS;
   beginPath(); rect(X1,y,X2-X1,Yi); clip(); closePath();
   
   return [X1,X2];
}}                                                                   




// 0 cancella   1 disegna separazione lock/unlock
function ClearB(m){
with(this) with(BDN) {
  if(!m) clearRect(0,0,cnvLS+Hlw+OH,SH);
  else{
    fillStyle=colrs[6];
    fillRect(cnvLS+Hlw-1,0,1,SH);
  }
}}



function Cell(cnv,x1,x2,s){
with(this) with(cnv) {      
  fillStyle=colrs[s]; fillRect(x1+1,0,x2-x1,HeadH);     
  lineWidth=1; 
  beginPath(); strokeStyle=colrs[6];  
  moveTo(x1+1,HeadH-0.5); lineTo(x2-0.5,HeadH-0.5); lineTo(x2-0.5,1);  
  stroke(); closePath();
}}   



function hCell(i){
with(this) with(CNV) with(Header[i]){
 
  var X1=x1, X2=x2, s=0, lr=OH+Hlw;
  if(i>=NL) X1=x1-optr, X2=x2-optr;
  if(X1>lr) return;
  if(X2>=lr) X2=lr;
 
  if(i==OVR) s=7;
  if(i==SEL) s=4;
  
  Cell(CNV,X1,X2,s);
  
  // testo
  var f="bold 13px opensansR", d=4, 
      x21=X2-X1-(d+4), t=moGlb.langTranslate(label);     
  font=f; 
  textBaseline="alphabetic"; 
  textAlign="left"; 
  fillStyle=colrs[2]; 
        
  if(moCnvUtils.measureText(t,f)>x21) t=moGlb.reduceText(t,f,x21);                        
  fillText(t,X1+d,8.5+(HeadH-8)/2);
   
  if(i==(NL-1)){ beginPath(); rect(Hlw,0,OH,HeadH); clip(); }          
}} 


function NextYposSibling(i){
with(this){
  var r,g, d=(Ypos[i].rtree).length;
  for(r=i+1;r<Ypos.length;r++){
    g=(Ypos[r].rtree).length;
    if(g<=d) return r;
  }
return i+1;
}}

function hasChild(p){
with(this){
  var k;
  for(k in JStore) if(JStore[k].fc_code_parent==p) return 1;
  return 0;
}}


// EVENTI --------------------------------------------------

function Vbarpos(i){
  with(this){
  vptr=i;bOVR=-1;  
  sedrw=2;draw();  
  drawbodyover(-1);
}}


function Obarpos(i){
  with(this){
  optr=i; 
  sedrw=2;draw(1);
}}



function bodydown(e){  moEvtMng.cancelEvent(e);
with(this){
 
  mCK=[]; noWH=0; 
  if(dwnBD) return false;
  
  moMenu.Close();
  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);
  
  eval("Vscr=Vscr"+name);
  eval("Oscr=Oscr"+name); 
  mCK[0]={ iy:P.y, ix:P.x, By:Vscr.ptr, Bx:Oscr.ptr};
        
  dwnBD=1;
 
  if(!isTouch){
   TGJ.style.display="block";
   eval("TGJ.onmousemove=function(e){"+name+".bodymove(e); }");
   eval("TGJ.onmouseup=function(e){"+name+".bodyup(e); }");  
  }   
  return false;
}}


function bodymove(e){ moEvtMng.cancelEvent(e);
with(this){
 
  var i,dx,dy, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), ex=P.x, ey=P.y;

  if(!mCK.length) return false;

  // drag scrollbars
  if(dwnBD==1) {
    if(ex<(zL+Hlw)) noWH=0;
    else {
      dx=Math.abs(ex-mCK[0].ix);
      dy=Math.abs(ey-mCK[0].iy);  
      if(dx>dy) noWH=1; else noWH=0;  // 0=vertical   1=orizzontal
    }
  }
  
  dwnBD++;
  if(dwnBD>2 && dwnBD&1){

    if(noWH){
      eval("Oscr=Oscr"+name);
      i=mCK[0].Bx+(mCK[0].ix-ex);
      Oscr.PosPtr(i);
    }else{
      eval("Vscr=Vscr"+name);
      i=mCK[0].By+(mCK[0].iy-ey);
      Vscr.PosPtr(i);
    }
  }
  return false;
}}


 

function overMove(e){ moEvtMng.cancelEvent(e);
with(this){

  var Bt=zT+mgT+HeadH+msgH, Bl=zL+mgL, 
      P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e),
      p, bovr=-1;
 
    for(var r=0;r<Abody.length;r++) {
      p=Bt+Abody[r].y;
      if(P.y>=p && P.y<(p+Yi)) { bovr=Abody[r].f; break; }
    }    
    
    // over rows
    if(bovr!=bOVR) { bOVR=bovr; drawbodyover(p-Bt); } 
  return false;           
}}





function bodyup(e){ moEvtMng.cancelEvent(e);
with(this){ 
 
  if(!isTouch) TGJ.style.display="none";
 
  // if click
  if(!dwnBD || dwnBD>3) { dwnBD=0; return false; } 
 
  var c2=(cnvLS-mecn)/2, Bli=zL+mgL+c2, Blf=Bli+c2, Bt=zT+mgT+HeadH+msgH,
      P={ x:mCK[0].ix, y:mCK[0].iy }, 
      Px=P.x-zL-mgL, Py=P.y-Bt,
      fc=0, nd=arrNd.length, md=0, p,g;
 
  mCK=[]; dwnBD=0;
 
     // se Tree open/close    
    if(nd && pairCross!=2){
      for(var r=0;r<nd;r++) with(arrNd[r]) {
       if(Px>=l && Px<=(l+32) && Py>=t && Py<(t+Yi)) {     //  moGlb.alert.show(Px+" | "+P.y+" | "+oby.fcode);
            if(oby.open) CloseP(oby); else OpenP(oby);                  // apre/chiude   
          return false;}    
    }}

    // edit o check
    for(var r=0;r<Abody.length;r++) {   
      p=Bt+Abody[r].y;
      if(P.y>=p && P.y<(p+Yi)){
        fc=Abody[r].f; 
        md=1;  
        if(P.x>Bli && P.x<Blf) md=2;
        break;
      }
    }
 
    // check  
    if(md==2) {  
       var cfi=arrCHK.indexOf(fc);
       if(cfi>-1) {    // decheck 
       
          arrCHK.splice(cfi,1);
           
       } else {  // check
                  
          arrCHK.push(fc);
              
       }                       
     sedrw=3;draw(); 
    } //
 
       
     //  EDIT se child
    if(md==1) {    
       var afl,fl,pr=rifY[fc];   
              
        fl=eval("mdl_wo_prl"+QoF+"_edit_c1").fields;                           
                
        if(typeof(fl)=="string") {
         fl=fl.multiReplace({ '\\|':',' } );
         afl=fl.split(","); 
        } else afl=fl;     
              
       if(Ypos[pr].fc_type==1) { // edit folder
       
         f_folder_Field(afl,0);

       }else{   // edit item
         var cp=1, cdp=parseInt(JStore[fc].fc_code_prl);
         if(cdp) cp=-1; 
       //  alert(cp)
         f_folder_Field(afl,cp);                       
       }
                         
       if(crud) eval(crud).edit(JStore[fc]);         
                      
    }   

  return false;     
}}


                                   
this.f_folder_Field=function(arf,md){     // md  0=folder       1 new price list      -1 price list 
with(this){
   
// new   
// r_on = 0
var fron=[
"txtfld_wo_prl1_edit_item_code",
"txtfld_wo_prl1_edit_title",
"txtfld_wo_prl1_edit_prl_listino",
"txtfld_wo_prl1_prl_misura",
"txtfld_wo_prl1_edit_unit_price",
"txtfld_wo_prl1_prl_percentuale_sconto",
"txtfld_wo_prl1_prl_k_moltiplicativo",

"txtfld_wo_prl2_edit_item_code",
"txtfld_wo_prl2_edit_title",
"txtfld_wo_prl2_edit_prl_listino",
"txtfld_wo_prl2_prl_misura",
"txtfld_wo_prl2_edit_unit_price",
"txtfld_wo_prl2_prl_percentuale_sconto",
"txtfld_wo_prl2_prl_k_moltiplicativo"
];

// mandatory
var fmon=[
"txtfld_wo_prl1_edit_title",
"txtfld_wo_prl1_edit_unit_price",

"txtfld_wo_prl2_edit_title",
"txtfld_wo_prl2_edit_unit_price"
];
   
             
  var r,R,o;
 
  
  for(r=0;r<arf.length;r++){
    R=arf[r];
  
  if(!md) {  // folder
    o=eval(R);
    if(R.indexOf("title")!=-1 || R.indexOf("description")!=-1) {
      o.r_on=0;
      continue;
    }
     
    o.m_on=0;
    o.v_level=0; 
    continue;
  }   
  
  o=eval(R);
  o.v_level=-1;
  o.m_on=(fmon.indexOf(R)!=-1)?1:0;
  
  if(md>0) {   // new
     
     if(fron.indexOf(R)!=-1)   o.r_on=0;        
   } else {   // edit
     if(fron.indexOf(R)!=-1)   o.r_on=1;
   }

  }
   
}}
 









function bodyout(e){
with(this){
  WHEELOBJ="";  
  if(bOVR!=-1) {bOVR=-1;}
  OBDJ.innerHTML="";                  
}}

 
 
function drawbodyover(p){
with(this){  
  if(bOVR==-1 || p==-1) OBDJ.innerHTML=""; 
  else {
  var o=10, op="opacity:."+o+";filter:alpha(opacity="+o+");-moz-opacity:."+o+";", c="#404040"; 
  OBDJ.innerHTML="<div style='position:absolute;left:0;top:"+(p)+"px;width:100%;height:"+(Yi-1)+"px;background-color:"+c+";"+op+"'></div>";
  }
}}




function headown(e){
with(this){    
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s=oversel(P.x), ovr=s[0]; 
  Cur=(s[1]==2)?1:0;  
       
  if(OVR!=ovr) OVR=ovr;
  PXi=P.x, PYi=P.y;     
 
 if(!NMM){NMM=1;ccMm=0;
 
  // creo movimento su maskera
  maskj=moGlb.createElement("maskDrag"+name,"div");
  maskj.style.zIndex=moGlb.zdrag+1;
  maskj.style.left=0;
  maskj.style.top=0;
  maskj.style.width='100%';
  maskj.style.height='100%';  

  eval("maskj."+EVTMOVE+"=function(e){ maskmove(e); }; oldmouseup=document."+EVTUP+"; document."+EVTUP+"=function(e){ maskup(e); };  ");
  } else NMM=0;
    
  if(isTouch) moEvtMng.cancelEvent(e);
  else return false;   
}}



function maskmove(e){
with(this){
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s=overmove(P.x), stri;        // s=[over, mid, X1, X2, pri, ins, scroll]           scroll 0 -10 -20 
  if(!ccMm) {
 
  // sposta else  Drag
  if(!Cur) stri="<img id='idtmpins' src='"+IM+"insert_field.png' style='position:absolute;display:none;' />"+
                    "<div id='idtmpdrgh' style='position:absolute;width:158px;height:30px;background-color:"+colrs[2]+";opacity:0.9;border-radius:2px;'>"+
                    "<div id='idtmpsimb' style='position:absolute;left:6px;top:5px;' >"+moCnvUtils.IconFont(58827,16,"#D88")+"</div>"+
                    "<div style='position:absolute;left:30px;top:6px;width:120px;height:16px;font:13px opensansR;color:#FFF;overflow:hidden;'>"+
                    moGlb.langTranslate(Header[OVR].label)+"</div></div>";
  else {   
         iext=s[0];if(!s[1]) iext=s[4];
         RX1=Header[iext].x1 + mgL+cnvLS+zL;
         if(iext>=NL) RX1-=optr;  
         stri= "<canvas id='idtmpdrgh' style='position:absolute;left:"+RX1+"px;top:"+(zT+mgT+HeadH)+"px;' width='40' height='"+(SH+1)+"'></canvas>";
       }
   maskj.innerHTML=stri;     
  }
    
  ccMm++;
  
  if(!Cur) {   // sposta
  mf$("idtmpdrgh").style.left=(P.x+16)+"px";
  mf$("idtmpdrgh").style.top=(P.y+16)+"px";
  
  var li=0,ti=0,di="none", srins=moCnvUtils.IconFont(58827,16,"#D88");
  if(s[5]){ di="block", li=(s[1])?s[3]:s[2], ti=zT+mgT-4;
            srins=moCnvUtils.IconFont(58826,16,"#CED"); }
  
  // if scroll left:-10 right;-20
  if(s[6]<-5) {
  if(s[6]==-10) { srins=moCnvUtils.IconFont(58774,16,"#FFFFFF"); if(!sescr) { sescr=1; scrollO(-1); } }
  if(s[6]==-20) { srins=moCnvUtils.IconFont(58775,16,"#FFFFFF"); if(!sescr) { sescr=1; scrollO(1); } }  
  } else sescr=0;

  mf$("idtmpins").style.display=di;
  mf$("idtmpins").style.left=(li-5)+"px", top=ti+"px";
  
  mf$("idtmpsimb").innerHTML=srins;
  
  
  } else {   // resize
  
   szW=P.x-RX1;if(szW<32) szW=32;
 
   var cj=mf$("idtmpdrgh"), cnn=cj.getContext("2d");
   
   cj.width=szW;
   with(cnn){
     clearRect(0,0,szW,SH);
     fillStyle="rgba(224,224,224,0.5)";                      // #@# moColors.maskMoveFill; 
     fillRect(0,0,szW,SH);
     lineWidth=1;
     strokeStyle="#808080";                                            // #@# moColors.maskMoveStroke;  
     beginPath();
     moveTo(0.5,0);
     lineTo(0.5,SH+0.5);
     lineTo(szW-0.5,SH+0.5)
     lineTo(szW-0.5,0);
     closePath();
     stroke();
   } 
  }   
  moEvtMng.cancelEvent(e);
  return false;
}}







function SeChkDechk(){      
with(this){
  
  var fc,r,s,f;
  if(arrCHK.length) deCheck(); else {
    for(r=0;r<Ypos.length;r++) { 
        fc=Ypos[r].fcode;
        arrCHK.push(fc); 
    }}
  
  sedrw=3; draw();
}}

function deCheck(){        // deseleziona tutti i check possibili
  this.arrCHK=[];
}





function maskup(e){
  with(this){

  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e), s;

  if(P.x==PXi && P.y==PYi) {                  // click
         

  } else {                                    // drag   
     
     if(!Cur){   // ordine field    
              s=overmove(P.x);
              if(s[5]) {
              var i0=OVR, i1=s[0]+s[1];lk=Header[s[0]].lock, dse=0;
              if(i1<=SEL && OVR>SEL) dse=1; 
              if(i1>SEL && OVR<=SEL) dse=-1;
              var v=Header.splice(i0,1);
              if(i1>OVR) i1--;

              Header.splice(i1,0,v[0]);Header[i1].lock=lk;
              
              if(OVR==SEL) SEL=i1; else SEL+=dse;  // mantengo selezioanto      
              fmenuList();
              updHeader(); 
                
              } else {            // aimazione ritorno 
                      var iij=mf$("idtmpdrgh");
                      if(!iij) {endAnim();}
                      else {
                       var x=parseInt(iij.style.left), y=parseInt(iij.style.top);
                       with(Header[OVR]){var xa=x1+mgL+cnvLS+zL, ya=zT+mgT;}
                       var nxa=Math.abs((xa-x)/50), nya=Math.abs((ya-y)/50), nn=parseInt((nxa+nya)/2);if(nn>8) nn=8;
                       if(nn<2) nn=2;var dx=parseInt((xa-x)/nn), dy=parseInt((ya-y)/nn);
                       
                       eval("maskj."+EVTMOVE+"='';");
                       headRitAnimat(x,y,dx,dy,0,nn);
                      }
                  if(isTouch) moEvtMng.cancelEvent(e);
                  else return false;  
                }  
     
     } else {   // resize field
     
     with(Header[iext]){wdth=szW;}  
     updHeader();
     
     }
  
      OVR=-1;sedrw=3;draw(); 
      NMM=0;
      }
      
  var pj=maskj.parentNode;pj.removeChild(maskj);
  eval("document."+EVTUP+"=oldmouseup");       
  sescr=0;  
    
  if(isTouch) moEvtMng.cancelEvent(e);
  else return false;  
}}



function headRitAnimat(x,y,dx,dy,t,nt){
  with(this){

  t++;
  var nx=x+dx*t, ny=y+dy*t;
  mf$("idtmpdrgh").style.left=nx+"px";
  mf$("idtmpdrgh").style.top=ny+"px";

  if(t>nt) {endAnim();return false;}

setTimeout(name+".headRitAnimat("+x+","+y+","+dx+","+dy+","+t+","+nt+")",50);
}}


function endAnim(){
  with(this){
  
  var pj=maskj.parentNode;pj.removeChild(maskj);
  eval("document."+EVTUP+"=oldmouseup");       
  sescr=0;OVR=-1;sedrw=3;draw();
  NMM=0;        
  return false; 
}}



function scrollO(d,t){if(!t) t=0;  
  with(this){if(!sescr) return;
 
  t++;
  if(t>3){
  optr+=d*16; 
  if(optr<0) optr=0;
  if(optr>(Huw-OH) ) optr=Huw-OH;

  eval("Oscr=Oscr"+name); 

  Oscr.Resize( {ptr:optr} ); 
  if(!optr || optr==(Huw-OH) ) return;
  }


setTimeout(name+".scrollO("+d+","+t+")",60);
}}



function headmove(e){    
  with(this){if(NMM) return;
   
   var P=moEvtMng.getMouseXY(e), s=oversel(P.x), ovr=s[0], cur=Curs[s[1]], oC=Cur;
   Cur=(s[1]==2)?1:0;
 
   if(OVR!=ovr || oC!=Cur) {OVR=ovr;sedrw=3;draw();}
   CNJ.style.cursor=cur;
   
   return false;  
}}
                                       

function oversel(x){    // restituisce  [over, cur]   cur: 0:normal  1:pointer  2:col-resize
  with(this){   
   var dx=mgL+zL+cnvLS, lr=OH+Hlw, ovr=-1, X1=0,X2=0, cur=0, pri=-1, mid=0, x21=0;    
   for(var i=0;i<NT;i++)  with(Header[i]){if(!hidd) continue;
   if(i>=NL) {X1=x1-optr, X2=x2-optr;if(X2<=Hlw) continue;
               if(X1<Hlw) X1=Hlw;if(X1>=lr) break;
               if(X2>lr) X2=lr;}
   else X1=x1, X2=x2;        
   X1+=dx, X2+=dx,  x21=(X2-X1)/2;
   if(x>=X1 && x<X2) {ovr=i;mid=(x<(X1+x21))?0:1;break;}  
   pri=i;  
   }
   if(x>(X2-24)) cur=1;
   if(x>(X2-6) || x<(X1+6)) cur=2;
   
   if( (pri<0 && !mid)  ) cur=0;  
   
   return [ovr,cur,pri,mid];   
}}



function overmove(x){    // restituisce  [over, mid, X1, X2, pri, ins, scroll]   mid: 0:left  1:right    pri=field precedente  nxi=field successivo  ins=se inserimento possibile
  with(this){
   var dx=mgL+zL+cnvLS, lr=OH+Hlw, ovr=-1, X1=0,X2=0, mid=0, x21=0, rg=12, pri=-1, nxi=-1, ins=1, PRI=-1, NXI=-1, sc=0; 
   
   if(x>(dx+Hlw) && x<(dx+rg+Hlw)) sc=-10;
   if(x>(dx+lr-rg) && x<(dx+lr)) sc=-20;
      
   for(var i=0;i<NT;i++) with(Header[i]) {if(!hidd) continue;
   if(i>=NL) {X1=x1-optr, X2=x2-optr;if(X2<=Hlw) continue;
               if(X1<Hlw) X1=Hlw;if(X1>=lr) break;if(X2>lr) X2=lr;}
   else X1=x1, X2=x2;        
   X1+=dx, X2+=dx, x21=(X2-X1)/2;
   if(x>=X1 && x<X2) {ovr=i;mid=(x<(X1+x21))?0:1;break;}
   pri=i;}  
    
   // cerco PRI e NXI di OVR;
   var b=0;
   for(i=0;i<NT;i++) with(Header[i]) {if(!hidd) continue; 
   if(b) {NXI=i;break;}
   if(i==OVR) {b=1;continue;}
   PRI=i;}

   if(ovr<0 || ovr==OVR || ((ovr==NXI && ovr!=NL) && !mid) || ((ovr==PRI && ovr!=(NL-1) ) && mid) ) ins=0;
     
   return [ovr,mid,X1,X2,pri,ins,sc];   
}}




function headout(e){
  with(this){   
  if(NMM) return;
  OVR=-1; 
  sedrw=1;draw();
}}


                          




// menu  -----------------------------------------------------------------------
function fieldlist(){
with(this){
  var l=zL+mgR+4, t=zT+mgT-2, w=VW, h=HeadH; 
  moMenu.Start(name+".menulist",{l:l, t:t, w:w, h:h},2);
}}



// Hidden
function acthidden(i){      // v= -1 da Ovr     m=stato 
with(this){    
  with(Header[i]){
    hidd=(hidd)?0:1;
    menulist[i].Status=hidd;
    if(lock) {actlock(i);return;}
  }
  updHeader(); 
  sedrw=2; draw();
}}
  


// Lock/unlock
function actlock(i){      // v= -1 da Ovr     m=stato 
with(this){    
  with(Header[i]){lock=(lock)?0:1;if(!lock) NL--;}
 
  var a=Header.splice(i,1);
  Header.splice(NL,0,a[0]);
 
  fmenuList();
  updHeader(); 
  sedrw=2;draw();
}}

 
  
//---------------------------------------------------------------------------------

function updHeader(m){      // aggiorno per hidden e lock 
with(this){

  NT=0;NU=0;NL=0;Hlw=0;Huw=0;var sw=0; 
  for(var r=0;r<Header.length;r++) with(Header[r]) {NT++;   
    if(hidd) {x1=sw;sw+=wdth;x2=sw;                                   // larghezza header lock e unlock
    if(lock) NL++, Hlw=sw; else Huw+=wdth, NU++;}} 
 if(m) return;
  eval("Oscr=Oscr"+name); 
  Oscr.Resize( {HH:Huw} );  
  predraw();
}} 

function fmenuList(){ // creo menu list field 
with(this){
  menulist=[];
  for(i=0;i<Header.length;i++) with(Header[i]){  
    menulist.push( {Txt:label, Type:2, Status:hidd, Icon:['', 'FlaticonsStroke.58786'], Fnc:name+".acthidden("+i+")"} );
  } 
}}


 
//------------------------------------------------------------------------------
// ENGY
//------------------------------------------------------------------------------
 
// props
this.Ypos=[];          
this.rifY=[];        
this.aLoad=[];

this.stpclear=1000;
this.ndmax=5;
this.istatus=0;
this.infostri="";
this.oinfostri="-";
this.timeinf=0;

// metodi
this.finfo=finfo;
this.ftimefinfo=ftimefinfo;
 

/*
1   loading     -1 end loading
2   clearing    -2 end clearing
*/


function finfo(){
with(this){ 
 
  var No=0,ttl=0,k,v;
  
  No=DBL.query("SELECT count(*) FROM "+DTAB+" WHERE fc_type=0"); 
  
  /*
  for(k in JStore) {
    v=JStore[k]["fc_wo_prl_cost"];
    if(!v) v=0.0;  
    ttl+=parseFloat(v);            
  }  
  */
  
  if( mf$("infotxt"+name) ) mf$("infotxt"+name).innerHTML="No. items: "+No;       // <b>Total cost: "+ttl+"</b> - 
 
}}



function ftimefinfo(){  
with(this){
                                    
 
}}


 
 
function OpenP(yp){
with(this){
  yp.open=1;
  var fc=parseInt(yp.fcode), ni=yp.fc_type, i=rifY[fc], r,resp,ry;
 
  if(ni!=1) return;
 
  // read children
  resp=DBL.query("SELECT * FROM "+DTAB+" WHERE fc_code_parent="+fc+" order by fc_order");   
      
  for(r=0;r<resp.length;r++) {  
    ry=resp[r];
    i++;
    Ypos.splice( i,0, {posY:0, fcode:ry.f_id, f_parent:ry.fc_code_parent, fc_type:ry.fc_type, open:0, rtree:ry.rtree } ); 
  }
    
  HnrRifPos();
}} 
 
 

function CloseP(yp){
with(this){
  yp.open=0; 
  var fc=yp.fcode, ni=yp.fc_type, i=rifY[fc], k=0, ny=Ypos.length, lf;
  i++; 
  
  if(ni!=1) return;
  
  // delete children 
  for(var r=i;r<ny;r++) {
    lf=Ypos[r].rtree.split("|");
    if(Ypos[r].f_parent==fc || lf.indexOf(""+fc)!=-1) k++; else break;  
  }
  Ypos.splice(i,k); 
  
  HnrRifPos(); 
}}


 

/*
ar: json  ritorno
{ ntot: n, data:[ [0]head,  [1...n]value ];
ntot:   -1  se treegrid   n se grid
data:
array[0]        header[   ]              
array[1....n]   value[  ]         

*/
 
function HnrRifPos(v,m){
with(this){

  var s,r,fc,ych=0;

  rifY=[];
  if(!v) arrCHK=[];

  n=Ypos.length;
 
  for(r=0;r<n;r++){
    fc=parseInt(Ypos[r].fcode);
    rifY[fc]=r; 
    Ypos[r].posY=ych;            
    ych+=Yi;
  }
  
  Hnr=n;
  
  
  if(m) return;
  
  if(!First){
    try{ 
      eval("Vscr=Vscr"+name); 
      var hyr=(n)?(Hnr*Yi):Vscr.H;
      Vscr.Resize( {HH:hyr} ); 
          
      nodrw=0; sedrw=2;
      setTimeout(name+".draw()", 100); 
     
    }catch(e){}
  } else First=0;
  
  
  finfo();
}}

//------------------------------------------------------------------------------
// buttons:  copy paste remove  up down

function f_copy(m){      // if m ==1 no ridraw
with(this){

  if(!arrCHK.length) {  return; }

  var r,f,C,k,rt,i, ai=["0"];
  moGlb.COPY={};  
  for(r=0;r<arrCHK.length;r++){
    f=arrCHK[r];
    if( typeof( moGlb.COPY[f] ) != "undefined" ) continue;   
    moGlb.COPY[f]=moGlb.cloneArray(JStore[f]);  
    f_addChild(f);   
  }
 
  
  // trasform in equivalent structure!!
  C=moGlb.COPY;
  for(k in C) ai.push(""+k);
  for(k in C) {
    rt=(C[k].rtree).split("|"); 
    i=0;
    for(r=(rt.length-1);r>=0;r--){
      if( ai.indexOf(rt[r]) == -1 ) { rt.splice(r,1); i=1; }    // parent does not  exist 
    } 
    if(i) C[k].rtree=rt.join("|");
  }
  
     
  if(!m) HnrRifPos(); 
}}



function f_addChild(p){
with(this){ 
  var r,f, res=DBL.query("SELECT f_id,fc_type FROM "+DTAB+" WHERE fc_code_parent="+p);  
  for(r=0;r<res.length;r++){
    f=res[r].f_id;
    if(typeof(moGlb.COPY[f])=="undefined") moGlb.COPY[f]=moGlb.cloneArray(JStore[f]); 
    if(res[r].fc_type==1) f_addChild(f); 
  }
}}


function f_cut(){
with(this){
  f_copy(1);
  deleteSelected(1); 
}}



function deleteSelected(m) {  
with(this){
  var r,f; 
  if(!arrCHK.length) return;

  if(!m) moGlb.confirm.show("<br>"+moGlb.langTranslate("Do you confirm deletion?"), function () {
          f_remove();     
         });
  else f_remove();
}}


function f_remove(){
with(this){
   var r,f;
   for(r=0;r<arrCHK.length;r++){
     f=arrCHK[r];
     if(f) f_delTree(f);
   }      
   HnrRifPos();
}}




function f_paste(){  
with(this){
 
  var k,st=0;
  for(k in moGlb.COPY) { st++; break; }
  
  if(!st){
    moGlb.alert.show( "<br>"+moGlb.langTranslate("No elements to paste!") );
    return false;
  }
  
  var r,fs,C,rd,o, ger=[], ki, art, nr,
      i=0, ai={"0":"0"}, nmin,mr,mp=0, 
      n=arrCHK.length;
  
  if(n>1) {
    moGlb.alert.show( "<br>"+moGlb.langTranslate("Select one item!") );
    return false;
  }
 
  C=moGlb.COPY;
  
  
  // create new fid  and search root elements in COPY
  for(k in C){    
    if( typeof(ai[k])=="undefined" ) ai[k]=f_creationCode();   // new code relation    
    mr=C[k].rtree.length;
    if(!i) nmin=mr; else { if(mr<nmin) { nmin=mr; mp=C[k]["fc_code_parent"]; } }
    i++;    
  }
  
  
  // get elem root
  for(k in C){
    if(C[k].rtree.length == nmin) ger.push( ai[k] ); 
  }

  // update all f_id in JStore
  for(k in C){
    ki=ai[k]; 
    JStore[ki]=moGlb.cloneArray(C[k]);
    JStore[ki]["f_id"]=ki;                                                            // f_if
    JStore[ki]["fc_type_prl"]=QoF;                                                    // fc_type_prl    
    JStore[ki]["fc_code_parent"]=ai[ C[k]["fc_code_parent"] ] | 0;                    // fc_code_parent 
    art=(JStore[ki].rtree).split("|");                                                // rtree
    for(nr=0;nr<art.length;nr++) art[nr]=ai[ art[nr] ];
    JStore[ki].rtree=art.join("|"); 
  }


  // parent and order in children
   if(n){  // checked   
    
      fs=arrCHK[0];
      o=Ypos[ rifY[fs] ];
                 
      var py,rdo,res,rtr, jt=JStore[fs];
          
      i=rifY[fs]; 
 
      if(o.fc_type==1){      //  1= folder      //  child
      
        rtr=o.rtree+"|"+fs;
      
        rd=DBL.query("SELECT MIN(fc_order) FROM "+DTAB+" WHERE fc_code_parent="+fs);
        rd-=ger.length;                               

        for(r=0;r<ger.length;r++) {
          k=ger[r];
          JStore[k].fc_order=rd;
          JStore[k].fc_code_parent=fs;
          i++; 
          if(o.open){
            Ypos.splice(i,0,{ "posY":0, "fcode":k, "f_parent":fs, "fc_type":JStore[k].fc_type, "open":0, "rtree":rtr }  );
          }
          
          rd++;
        }
         
        // update all rtree 
         for(k in C){
           ki=ai[k];
                     
           art=(JStore[ki].rtree).split("|");
           art.shift();  
           JStore[ki].rtree=rtr + (art.length ? ("|"+(art.join("|"))):""); 
               
           DBL.query("INSERT INTO "+DTAB, JStore[ki]);
         }



      }else{            // open=0 sibling
      
        rd=jt.fc_order; rdo=rd;                               
        
        for(r=0;r<ger.length;r++) {
          k=ger[r];
          rd++;
          JStore[k].fc_order=rd;
          JStore[k].fc_code_parent=o.f_parent;
          i++; 
          Ypos.splice(i,0,{ "posY":0, "fcode":k, "f_parent":o.f_parent, "fc_type":JStore[k].fc_type, "open":0, "rtree":o.rtree }  );
        }
         
        rd++;
        
        res=DBL.query("SELECT fid,f_id FROM "+DTAB+" WHERE fc_code_parent="+o.f_parent+" AND fc_order>"+rdo+" ORDER BY fc_order");
        
        for(r=0;r<res.length;r++){  
          JStore[ res[r].f_id ].fc_order=rd+r;
          DBL.query("UPDATE "+DTAB+" SET fc_order="+(rd+r)+" WHERE fid="+res[r].fid);
        }
        
         // update all rtree 
         for(k in C){
           ki=ai[k];
           if(o.rtree!="0"){           
              art=(JStore[ki].rtree).split("|");
              art.shift();  
              JStore[ki].rtree=o.rtree + (art.length ? ("|"+(art.join("|"))):""); 
           }   
           DBL.query("INSERT INTO "+DTAB, JStore[ki]);
         }
      
      
      
      } //
      
   
   }else{   // root after
            
      rd=DBL.query("SELECT MAX(fc_order) FROM "+DTAB+" WHERE fc_code_parent=0");    // max sibling order
      rd++;
      
      for(k in C){
        ki=ai[k];
        DBL.query("INSERT INTO "+DTAB, JStore[ki]);
      }
      
      for(r=0;r<ger.length;r++) {
        k=ger[r];
        JStore[k].fc_order=rd+r;
        Ypos.push( { "posY":0, "fcode":k, "f_parent":0, "fc_type":JStore[k].fc_type, "open":0, "rtree":"0" }  );
      }
     
   }   
  
 
  
   HnrRifPos(); 
}}




 
 
 
function f_up(){  
with(this){
 
   if(arrCHK.length!=1) { moGlb.alert.show( "<br>"+moGlb.langTranslate("Select one item!") ); return; }

   var r,p,ii=-1,i, f=arrCHK[0];
   i=rifY[f];
   p=Ypos[i].f_parent;
   
   for(r=i-1;r>=0;r--){
     if(Ypos[r].f_parent==p) { ii=r; break;   } 
   }
          
                    
   if(ii>-1) f_swap( f, Ypos[ii].fcode );
  
   HnrRifPos(1); 
}} 





function f_down(){   
with(this){
 
   if(arrCHK.length!=1) { moGlb.alert.show( "<br>"+moGlb.langTranslate("Select one item!") ); return; }

   var r,p,ii=-1,i, f=arrCHK[0];
    
   i=rifY[f]; 
   p=Ypos[i].f_parent;
   
   for(r=i+1;r<Ypos.length;r++){
     if(Ypos[r].f_parent==p) { ii=r; break; } 
   }
 
   if(ii>-1) f_swap( f, Ypos[ii].fcode );
 
   HnrRifPos(1); 
}} 



function f_swap(f1,f2){    // swap sibling in Ypos and fc_order in JStore and DTAB
with(this){

   var f,i1,i2,a1,a2,o1,o2;
   
   i1=rifY[f1];
   i2=rifY[f2];
   
   if(Ypos[i1].open || Ypos[i2].open)  { moGlb.alert.show( "<br>"+moGlb.langTranslate("Close folder before moving!") ); return false; } 
    
   a1=moGlb.cloneArray(Ypos[i1]);
   a2=moGlb.cloneArray(Ypos[i2]);
   
   Ypos[i1]=moGlb.cloneArray(a2);
   Ypos[i1].posY=a1.posY;
   Ypos[i2]=moGlb.cloneArray(a1);
   Ypos[i2].posY=a2.posY;
   
   o1=JStore[f1].fc_order;
   o2=JStore[f2].fc_order;
   
   JStore[f1].fc_order=o2;
   JStore[f2].fc_order=o1;
 
   DBL.query("UPDATE "+DTAB+" SET fc_order="+o2+" WHERE f_id="+f1);
   DBL.query("UPDATE "+DTAB+" SET fc_order="+o1+" WHERE f_id="+f2);   
}}





function f_tofinal(){  
with(this){
    if(QoF!=1) return;
    moGlb.confirm.show("<br>"+moGlb.langTranslate("Do you confirm moving quote to final cost? Final costs will be deleted."), function () {
      
      
      var r,s0,s1;
      
      s0 = DBL.query("SELECT f_id FROM "+DTAB+" WHERE fc_code_parent=0","NUM");
      
      arrCHK=[];
      for(r=0;r<s0.length;r++) arrCHK.push(s0[r][0]);
      
      var tempCOPY=moGlb.cloneArray(moGlb.COPY); 
      f_copy();
      
      DBL.query("TRUNCATE t_mdl_wo_prl2_tg_tg");
      mdl_wo_prl2_tg_tg.arrCHK=[];
      mdl_wo_prl2_tg_tg.Ypos=[];
      mdl_wo_prl2_tg_tg.f_paste();
            
      moGlb.COPY=moGlb.cloneArray(tempCOPY);          
  });
}} 

 
 



function f_delTree(f){
with(this){  
  if(!f || typeof(JStore[f])=="undefined" ) return false; 
  if(JStore[f].fc_type==1){
    var R,r;
    R=DBL.query("SELECT * FROM "+DTAB+" WHERE fc_code_parent="+f);
    for(r=0;r<R.length;r++) f_delTree(R[r].f_id);   
    f_del(f); 
  } else f_del(f);  
  

}}

function f_del(f){
with(this){
  
  var i=rifY[f]; 
  if(typeof(i)!="undefined") {
    Ypos.splice(i,1);
    HnrRifPos(1,1);
  }  
   
  DBL.query("DELETE from "+DTAB+" WHERE f_id="+f);
  delete(JStore[f]);
}}



//--------------------------------
 
function f_insert_folder(){   
with(this){

  var r,n,rtr="0", rd, f, jt, fc,fp,R,o,oi,i, py, m=0, g,gg,         
  
      jrw={ f_id:0, fc_code_parent:0, fc_code_prl:0, fc_type_prl:QoF, fc_type:1, fc_order:0, rtree:"0" },      // folder
      
      yps={posY:0, fcode:0, f_parent:0, fc_type:1, open:0, rtree:"0" };
  
  n=arrCHK.length;
  if(n>1) { moGlb.alert.show( "<br>"+moGlb.langTranslate("Select one item!") ); return; }  // check less than two item
  
  fc=f_creationCode();
  jrw.f_id=fc;
  yps.fcode=fc;
  
  jrw["fc_wo_prl_title"]=moGlb.langTranslate("new folder");
  
  if(!n){  // root
     if(Ypos.length){ 
       rd=DBL.query("SELECT MAX(fc_order) FROM "+DTAB+" WHERE fc_code_parent=0");          // max order
       jrw.fc_order=rd+1;        
       py=Ypos[(Ypos.length-1) ].posY + Yi;    // vertical position
       yps.posY=py;   
     }
    
    // other in bookmark
    for(g=0;g<aBKM.length;g++) { gg=aBKM[g]; if( typeof(jrw[gg])=="undefined" ) jrw[gg]=""; }
 
     DBL.query("INSERT INTO "+DTAB,jrw);
     JStore[fc]=jrw; 
     Ypos.push( yps ); 
  
  } else {  // inside

     f=arrCHK[0];
     jt=JStore[f]; 
     i=rifY[f]; 
     
     m=Ypos[i].fc_type;               // m:  0=sibling   1figlia         // .open
     py=Ypos[i].posY + Yi;
     yps.posY=py;

     if(m!=1) {   // item checked or as sibling =>  insert folder after
        o=jrw.fc_order;
        jrw.fc_order=o;
        
        fp=jt.fc_code_parent;
        jrw.fc_code_parent=fp;
        jrw.rtree=jt.rtree;
        
        // after
        R=DBL.query("SELECT * FROM "+DTAB+" WHERE fc_code_parent="+fp+" AND fc_order>"+o);
        
        JStore[fc]=jrw;
        DBL.query("INSERT INTO "+DTAB,jrw); 
        yps.f_parent=fp;
        yps.rtree=jt.rtree;

        for(r=0;r<R.length;r++){
          oi=R[r].fc_order;
          oi++;
          JStore[R[r].f_id].fc_order=oi;
          DBL.query("UPDATE "+DTAB+" SET fc_order="+oi+" WHERE fid="+R[r].fid);
        }

        Ypos.splice( NextYposSibling(i), 0, yps);
       
      
       }else{  // m first child  => folder selected

            jrw.fc_code_parent=f;
            jrw.rtree=jt.rtree+"|"+f;      
            R=DBL.query("SELECT MIN(fc_order) FROM "+DTAB+" WHERE fc_code_parent="+f);
            jrw.fc_order=R-1;
            
            JStore[fc]=jrw;
            DBL.query("INSERT INTO "+DTAB,jrw);
         
         if(Ypos[i].open){   // if open 
            yps.f_parent=f;
            yps.rtree=jrw.rtree;     
            Ypos.splice( i+1, 0, yps);
         } 
           
       } 
  }
  
  HnrRifPos();
}}






function f_insert_item(){   
with(this){

  var m,i,r,f,fc,rd,py,jt,o,fp,R, 
      n=arrCHK.length,
      jrw={ f_id:0, fc_code_parent:0, fc_code_prl:0, fc_type_prl:QoF, fc_type:0, fc_order:0, rtree:"0" },      // item 
      yps={posY:0, fcode:0, f_parent:0, fc_type:0, open:0, rtree:"0" };
  
  fc=f_creationCode();
  jrw.f_id=fc;
  jrw["fc_wo_prl_title"]=moGlb.langTranslate("new item");
  jrw["fc_wo_prl_unit_price"]="";
  jrw["fc_wo_prl_um"]="";
  jrw["fc_wo_prl_quantity"]="";
  jrw["fc_wo_prl_cost"]="";
  
  yps.fcode=fc;
  
  
  if(n>1 || !n){  // last position in root
      
    Ypos.push(yps);    
    rd=DBL.query("SELECT MAX(fc_order) FROM "+DTAB+" WHERE fc_code_parent=0");          // max order
    jrw.fc_order=rd+1;
    JStore[fc]=jrw; 
    DBL.query("INSERT INTO "+DTAB,jrw);    
  
  }else{          // child or sibling
  
    f=arrCHK[0]; 
    i=rifY[f];    
    m=Ypos[i].fc_type;               // m:  0=sibling   1figlia          // .open
    jt=JStore[f];
 
    
    if(m!=1) {     // sibling
    
        o=jt.fc_order;
        jrw.fc_order=o+1;
        
        fp=jt.fc_code_parent;
        jrw.fc_code_parent=fp;
        jrw.rtree=jt.rtree;
        
        // after
        R=DBL.query("SELECT * FROM "+DTAB+" WHERE fc_code_parent="+fp+" AND fc_order>"+o);
        
        JStore[fc]=jrw;
        DBL.query("INSERT INTO "+DTAB,jrw); 
        yps.f_parent=fp;
        yps.rtree=jt.rtree;

        for(r=0;r<R.length;r++){
          o=R[r].fc_order;
          o++;
          JStore[R[r].f_id].fc_order=o;
          DBL.query("UPDATE "+DTAB+" SET fc_order="+o+" WHERE fid="+R[r].fid);
        }

        Ypos.splice( NextYposSibling(i), 0, yps);
    
    
    
    }else{      // child
    
        jrw.fc_code_parent=f;
        jrw.rtree=jt.rtree+"|"+f;      
        R=DBL.query("SELECT MIN(fc_order) FROM "+DTAB+" WHERE fc_code_parent="+f);
        jrw.fc_order=R-1;
            
        JStore[fc]=jrw;
        DBL.query("INSERT INTO "+DTAB,jrw);
            
      // if open update Ypos
      if(Ypos[i].open){ 
        yps.f_parent=f;
        yps.rtree=jrw.rtree;          
        Ypos.splice( i+1, 0, yps);
      }
    
    
    }
  
  
  
  }

   HnrRifPos();

}}






function f_creationCode(){
with(this){     
  fcreationCode--;                  
  return fcreationCode;
}}





//------------------------------------------------------------------------------
// save 

function saveList(){
with(this){
 
 var n, a=[],b=[], o1,o2;
 
 o1=mdl_wo_prl1_tg_tg;       // mdl_wo_prl (x) _tg_tg
 o2=mdl_wo_prl2_tg_tg;
 
 return {f_code:a, f_pair1:o1.JStore, f_pair2:o2.JStore, f_code_main:f_code, f_type:ftype, Ttable:Ttable };
}}




//------------------------------------------------------------------------------


function setHeader(ab){   // bookmark
with(this){ 
   NT=0;Header=[]; AR=[]; Huw=0;
  // carico i fields e costruisco i menu per head 
  // calcolo Header
  var prm=["fieldb","label","type","lock","hidd","wdth","filter","range","prange","script","visibility","x1","x2"],NT=0, NL=0, NU=0, sw=0, r, fb, c, fz, ki, fld;  
  aRange=[], aPRange=[], arhdn=[], arlck=[], aFilter=[];                                                                                       
                                                                                                             
  if(ab){
    for(r=0;r<ab.length;r++){
      Header[r]={}; 

      fz=ab[r][0];AR[fz]=[]; // salvo in AR   filter  range  prange  (del campo fieldb)
      for(ki=6;ki<9;ki++) AR[fz][prm[ki]]=ab[r][ki];     // es. AR[fieldb][filter]=array bookmark
    
      for(var i=0;i<prm.length;i++) Header[r][prm[i]]=(i<6)?ab[r][i]:"";                           // trasformo in associativo      se<6 
  
      // Range
      var ar7=ab[r][7].length;                                                                                                                        
      if(ar7) {fz=ab[r][0];aRange[fz]=[]; 
               for(var n=0;n<ar7;n+=2) aRange[fz].push( {exp:ab[r][7][n], val:ab[r][7][n+1]} );
      }
      // aPRange
      var ar8=ab[r][8].length;                                                                                                                        
      if(ar8) {aPRange.push( {field:ab[r][0], condz:[], typ:ab[r][2]} );c=aPRange.length-1;
               for(var n=0;n<ar8;n+=2) aPRange[c].condz.push( {exp:ab[r][8][n], val:ab[r][8][n+1]} );
      }      
      // script
      ar9=ab[r][9]; 
      Header[r][prm[9]]=(ar9)?ab[r][9]:"";        
      // visibility   
      ar10=ab[r][10]; 
      if(typeof(ar10)=="undefined") ar10=-1;
      Header[r][prm[10]]=ar10;            
      // select
      if(Header[r].type==4) {
        fb=Header[r].fieldb;if(typeof Filterby == "undefined") Filterby = [];Filterby[fb]=[]; aFilter[fb]=[]; // array('0'=>'Nessuna','1'=>'Normale','2'=>'Urgente','3'=>'Immediata')   
          for(k in ab[r][6]) {
            aFilter[fb][k]=ab[r][6][k];
            Filterby[fb].push({Txt:ab[r][6][k], Type:3, Value:k, Status:0, Group:3, Icon:['','checked'], Fnc:""});                                
      }}
      
      with(Header[r]) {  
        if(!(moGlb.user.level&visibility) || !hidd) { arhdn.push(r); arlck.push(lock); hidd=1; }
          NT++; x1=sw; sw+=wdth; x2=sw;                                   
          if(lock) NL++,Hlw=sw; else Huw+=wdth,NU++; 
      }                
  }}
   
  var a,ii,k=0,lk,iii;
  fmenuList();
  
  for(r=0;r<arhdn.length;r++) { ii=arhdn[r]; lk=parseInt(arlck[r]);  
    iii=ii; if(lk) iii=ii-k;  
    with(Header[iii]){ hidd=0;
                        menulist[ii].Status=0;                      
           if(lk) { lock=0;   NL--;
                    a=Header.splice(iii,1);
                    Header.splice(NL,0,a[0]);
                    k++;                  
  }}}

  updHeader(1);
  predraw(); 
}}



function getHeader(){
with(this){  
  var abk=[], r, a, k;
  for(r=0;r<Header.length;r++){ 
     a=Header[r];
     abk.push( [ a.fieldb,
                 a.label,
                 a.type,
                 a.lock,
                 a.hidd,
                 a.wdth,
                 AR[a.fieldb]["filter"],
                 AR[a.fieldb]["range"],
                 AR[a.fieldb]["prange"],
                 a.script
                ]);
  }
return [abk, bookmark_id, bookmark_name];
}}




function fMaskLoad(x,y){
with(this){
  var mj=moGlb.createElement("maskmodule"+name,"div",jj);
  
  if(!NOEVT){
    mj.style.left=0;
    mj.style.top=0;
    mj.style.width="100%";
    mj.style.height="100%";
    mj.style.zIndex=10;
    mj.style.display="block";
    
    mj.onmousedown=function(e){moEvtMng.cancelEvent(e);return false;}
    mj.onmouseover=function(e){moEvtMng.cancelEvent(e);return false;}
    
    mj.innerHTML="<img src='"+IM+"ico_loading.gif' style='position:absolute;left:"+(x-17)+"px;top:"+y+"px;width:16px;height:16px;' />";     
  } else {
    if(NOEVT>32) {mj.innerHTML="";mj.style.display="none";NOEVT=0;return;} 
  }
    NOEVT++;
setTimeout(name+".fMaskLoad()",200);
}}


function getSelectedRows(){
with(this){    
  var r,jr=[],ak;
  for(r=0;r<arrCHK.length;r++){ 
    ak=arrCHK[r];
    jr.push(JStore[ak]);
  }
  return jr;
}}

} //
