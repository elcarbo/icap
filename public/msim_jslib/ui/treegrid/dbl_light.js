
DBL = {  active_db:"", last_tab:"", last_query:"", last_id:0,
         db:{}, 
         AUTOSAVE:1, 
         JStore:{}, 
         ver:"2.10",
    
ER:0,
ERROR:[ "",
"no query data!",                                // 1
"command name error!",                           // 2
"incomplete query!",                             // 3
"from table error!",                             // 4
"query sintax error!",                           // 5
"this browser does not support localstorage!",   // 6
"database not found",                            // 7
"database storage error!",                       // 8
"table not found!",                              // 9
"multiple auto_increment fields!",               // 10
"default type error!",                           // 11
"table already exist!",                          // 12
"table name error!",                             // 13
"json value required!",                          // 14
"field name not found",                          // 15
"wrong number of value!",                        // 16
"var not found!",                                // 17
"update SET/json value together!",               // 18
"table locked!",                                 // 19
"field type error!",                             // 20
"localstorage quota exceeded!"                   // 21
],


query:function(s,j){
with(DBL){
  ER=0; last_id=0;
  if(!s) { ER=1; return false; }
  s=fTrim(s);
  s=fReplace(s,{"\\n":""} );  
  var R,t,c;  
  R=f_CMD(s); c=R.Cmd, s=R.Str;  
  if(!R) { ER=2; return false; }
  R=DBL[c](s,j);  
  last_query=c+" "+s; 
  return R;  
}},
 
 
// DATABASE ---------------------------------------------------------------------
 
CREATE_DATABASE:function(s){    //  CREATE DATABASE [IF NOT EXISTS] db_name 
with(DBL){
   ER=0;
 
   s=fTrRe(s);  
   var i,a,dbn,ine=0; 
   i=f_NEXTOF("IF NOT EXISTS ",s);
   if(!i) { 
      a=s.split(" ");
      if(a.length>1) { ER=5; return false; }
      dbn=a.pop();   
   } else dbn=i.value, ine=1;
 
   i=exist_database(dbn);
   if(i){ if(ine) return dbn; else { ER=7; return false; } }
   
   i=list_dbl();
   i.push(dbn);  
    Set("DBL.list_dbl",i); 
    Set("DBL."+dbn,[]); 
    db={};
    active_db=dbn;  
    return dbn;
}},


SHOW_DATABASES:function(){     //  SHOW DATABASES
with(DBL){   

   ER=0;
   return list_dbl();
}},


DROP_DATABASE:function(s){     // DROP DATABASE `db_name`
with(DBL){

   ER=0; 
   s=fTrRe(s);   
   if(!s) { ER=3; return false; }    
   if(!del_dbl(s) ) { ER=7; return false; } 
   if(s==active_db) active_db=""; 
   return s;      
}},


// DATABASE methods --------------------------------------------------------------------------------------- 
 
exist_database:function(db_name){
  ER=0;
  var e=false, l=DBL.list_dbl();
  if(!db_name) return e;
  if(l.indexOf(db_name)!=-1) e=true;  
  return e;
},


list_dbl:function(){

  var l=DBL.Get("DBL.list_dbl");
  if(!l) l=[];
  return l;
},


del_dbl:function(db_name){
with(DBL){
  ER=0;
  if(!exist_database(db_name)) return false;  
  var r,m,l, atv=Get("DBL."+db_name);
  if(atv==null) return false;
  for(r=0;r<atv.length;r++){ m=atv[r];
    Del("DBL."+db_name+"."+m);
  }
  Del("DBL."+db_name); 
  l=list_dbl();
  for(r=0;r<l.length;r++) { if(l[r]==db_name) { l.splice(r,1); break; } }
  Set("DBL.list_dbl",l); 
  return true;
}},

 
save_database:function(){
with(DBL){
  ER=0;
  if(active_db) {
    var k, atv=[];
    for(k in db) { 
        Set("DBL."+active_db+"."+k,db[k]);
        atv.push(k);
    }
    Set("DBL."+active_db,atv);
    return true; 
  } 
  return false;
}},


save_table:function(t){
with(DBL){   
   ER=0;
   if(!active_db) return false;
   if(!t) { if(!last_tab) return false; t=last_tab;  }
   if(db[t]["temp"]) return true;    
   upd_list_tables();
   Set("DBL."+active_db+"."+t,db[t]);  
   return true;
}},



upd_list_tables:function(){
with(DBL){
  if(!active_db) return;
  var a=[],t;
  for(t in db){
    if(db[t].temp) continue;   
    a.push(t);
  }
  Set("DBL."+active_db,a);
}},



connect:function(db_name){
with(DBL){
  ER=0;
  last_tab=""; 
  if(!db_name) {             // disconnect
    active_db="", db={}; 
    return true;            
  }     
  if(!exist_database(db_name)) { ER=7; return false; }
  var r,m,t, atv=Get("DBL."+db_name);
  if(atv==null) { ER=8; return false; }  
  db={};
  for(r=0;r<atv.length;r++){
    m=atv[r];
    t=Get("DBL."+db_name+"."+m);
    if(!t) t={};
    db[m]=t;
  }
active_db=db_name;
return db_name;
}},


close:function(db_name){      // disconnect
  DBL.connect();
},


f_verify:function(t){
with(DBL){
   return (active_db && typeof(db[t])!="undefined")?1:0;
}},


counter:function(t){
with(DBL){
   return f_verify(t)?db[t].count:0;
}},


//-------------------------------
// index  fAN  fNA  

f_index_fields:function(t){
with(DBL){
    var r,v, D=db[t];     
    for(r=0;r<db[t]["field"].length;r++) { 
        v=D["field"][r];
        D.fan[v]=r;
        D.fna[r]=v;   
    }   
}},

AtoN:function(t,A){ return DBL.db[t].fan[A]; },        // position field    field Name to pos number
NtoA:function(t,N){ return DBL.db[t].fna[N]; },


// ---------------------------------------------------------------------------------------------------------
//  commands        
   
SHOW_FIELDS:function(t){     // SHOW FIELDS [table_name]  
with(DBL){
  ER=0; 
  t=fTrRe(t);
  if(!t) t=last_tab;
  if(!f_verify(t)) { ER=9; return false; }
return fCloneA(db[t].field);  
}},


SHOW_TABLES:function(){    // SHOW TABLES      
with(DBL){ 
  ER=0;
  if(!active_db) return [];
    var k, atv=[];
    for(k in db) { if(k!="vars") atv.push(k); } 
  return atv;
}},   
    
   
CREATE_TABLE:function(s,j){    // CREATE TABLE [IF NOT EXISTS] [TEMP] tab_name ( field_name type [unsigned] [DEFAULT] [value] [not] [null] auto_increment [primary key], .... )
with(DBL){                
   s=fTrRe(s);
   var t,a,v,n,r,str,i,p,ii,vv,b,f,y,l,fu,
       ine=0, aui=0, temp=0;
         
   if(f_POS("IF NOT EXISTS ",s)!=-1) {
    ine=1; s=s.substring(14); } 
   a=s.split(" ");
   t=a.shift();     // table name
   if(t.toUpperCase()=="TEMP") { temp=1; t=a.shift(); }   // temporary
             
   if(typeof(db[t])!="undefined") {  
      if(!ine) { ER=12; return false; } 
      return t; }    // return table_name if exist
   
   // create
   s=a.join(" ");
   n=s.lastIndexOf(")");   // no engine charset etc...
   s=s.substring(1,n);  
   a=f_REMOVE_STRINGS(s);
   s=a[0];
   str=a[1];
db[t]={"field":[],"blank":[],"key":{},"fan":{},"fna":{},"type":[],"data":[],"count":0,"erased":[],"locked":0,"temp":temp };
   a=s.split(",");
   for(r=0;r<a.length;r++){ 
        b=(a[r]).split(" ");
        f=b.shift();
        fu=f.toUpperCase();   
        if(fu=="PRIMARY" || fu=="KEY" || fu=="INDEX"){    
          f=fReplace( b.join(""), {"\\(":"", "\\)":"", "KEY":""} );   // field name for KEY
          db[t]["key"][f]={};
          continue;
        }      
        y=b.shift().toLowerCase();
        if(y.indexOf("int")!=-1) {
              y="int"; v=0;
        } else if(y.indexOf("float")!=-1 || y.indexOf("double")!=-1 || y.indexOf("real")!=-1 || y.indexOf("decimal")!=-1 || y.indexOf("numeric")!=-1){
              y="real"; v=0.0;
        } else if(y.indexOf("inc")!=-1) {
              y="inc"; v=1; 
              if(aui) { ER=10; return false; }
              aui++;
        } else { y="string";  v=""; }  
        
        ii=b.length;
        for(i=0;i<ii;i++) {
            p=b[i].toLowerCase();
            if(p=="auto_increment" && y=="int") {
                 if(aui) { ER=10; return false; }
                 aui++; y="inc"; v=1;
            }
            if(p=="default" && i<(ii-1)){
                 i++; vv=b[i];
                 if(vv=="#") { vv="'"+(str.shift())+"'"; }
                 if(vv.toUpperCase()!="NULL"){ vv=eval(vv);
                  if(typeof(v)!=typeof(vv)) { ER=11;  return false; }
                  v=vv;
                 } 
            }  
        }
      db[t]["field"].push(f);
      db[t]["type"].push(y);  
      db[t]["blank"].push(v);  
   }   
f_index_fields(t);
last_tab=t;
if(AUTOSAVE) save_table(t); 
return true;
}},
    

DROP_TABLE:function(t){     // DROP TABLE  table_name
with(DBL){
  t=fTrRe(t); 

  if(t=="TEMP") {for(k in db) if(k!="vars" && db[k].temp) DROP_TABLE(k); } 
  if(!f_verify(t)) { ER=9; return false; }
  if(db[t].locked) { ER=19; return false; }
  delete db[t];   
  Del("DBL."+active_db+"."+t); 
  upd_list_tables();
return true;
}},

   
TRUNCATE:function(s){   // TRUNCATE [TABLE] table_name
with(DBL){ 
   s=fTrRe(s);
   var t,a=s.split(" "), n=a.length,Dk,k;
   t=a[0];
   if(n>1) { 
    if(a[0].toUpperCase()=="TABLE") t=a[1]; else n=3;
    if(n>2) { ER=13; return false; }
   }  
   if(!f_verify(t)) { ER=9; return false; }
   if(db[t].locked) { ER=19; return false; }  
   with(db[t]) data=[], count=0, erased=[]; 
   f_index_fields(t);  
   Dk=db[t]["key"];
   for(k in Dk){ Dk[k]={}; }  
last_tab=t;
if(AUTOSAVE) save_table(t);
return true;  
}},
   
   
                         // INSERT INTO table | json
INSERT:function(s,j){    // INSERT INTO table_name (field1, field2, ...) VALUES (value1, value2, ...) [ , (value1, value2, ...), .... ]
with(DBL){
  var r,m,a,i,t,ss,as,typ=3; 
  i=s.indexOf("(");
  if(i==-1) { ss=s; 
    if(!j) { ER=14; return false;  }
  } else { ss=s.substring(0,i);  s=s.substring(i); }
  ss=fTrRe(ss); 
  as=ss.split(" ");
  if(as[0].toUpperCase()=="INTO" && as.length==2) t=as[1];
  else { ER=13; return false; }
  
  if(db[t].locked) { ER=19; return false; }
      
  if(i!=-1) j=f_strToJson(t,s,j);    // string to json 
  if(!j) return false;                                                                                        // !j is not enought
  
  // insert row
  if(typeof(j.length)=="undefined") typ=4;
  else { if(typeof(j[0].length)!="undefined") typ=2;
         else  if(typeof(j[0])!='object') typ=1;  }

  m=(typ>2)?1:0;
  if(typ&2) { for(r=0;r<j.length;r++) f_insert_row(t,j[r],m);  }
  else f_insert_row(t,j,m);
  
last_tab=t;    
if(AUTOSAVE) save_table(t);
return true; 
}},


DELETE:function(s){    // DELETE FROM table_name [WHERE 1]
with(DBL){
  s=fTrim(s);
  var a,R,i,w,t,k,Dd,Dk,v,y,p;   
  i=f_NEXTOF("FROM",s); 
  if(!i) { ER=4; return false; }
  t=i.value;
  if(!f_verify(t)) { ER=9; return false; }
  if(db[t].locked) { ER=19; return false; } 
  i=f_POS("WHERE ",s);
  if(i==-1) w="WHERE 1"; else w=s.substring(i+6);
  a=f_where(t,w,0);
  Dd=db[t]["data"], Dk=db[t]["key"];
  for(r=0;r<a.length;r++){
    i=a[r]; 
    // delete key
    for(k in Dk){     
        p=AtoN(t,k);
        v=Dd[i][p];    
    y=f_binSrc(Dk[k][v],i);
    if(y[1]) {  continue;  }
    Dk[k][v].splice(y[0],1);
    if(!Dk[k][v].length) delete Dk[k][v];  
   }
    // delete row  
    db[t].erased.push(i);
    Dd[i]=null;  
  } 
last_tab=t;
if(AUTOSAVE) save_table(t);
return true;
}},


                         // UPDATE table WHERE 1 | json 
UPDATE:function(s,j){    // UPDATE table SET field1=value1, field2=value2 WHERE 1
with(DBL){
  var t,r,D,Dk,v,ov,w,a,p,k,i,z,y, m=0,
      st="", wh="", tj=(typeof(j)=="undefined");
  s=fTrRe(s);
  p=f_POS("WHERE ",s);
  if(p==-1) s+=" WHERE 1";  
  a=s.split(" ");
  t=a.shift();
  if(!f_verify(t)) { ER=9; return false; } 
  if(db[t].locked) { ER=19; return false; }

  s=a.join(" "); 
  w=(s.substring(0,6)).toUpperCase();
  v=w.substring(0,4);
  
  if(w=="WHERE ") m=1; // json
  else { m=2;
    if(v=="SET " ) s=s.substring(4); 
    if(!tj) { ER=18; return false; }
  }
  
  if(m==1 && tj) { ER=14; return false; }
  if(m==2){  // SET   
    p=f_POS("WHERE ",s);
    st=s; 
    if(p>0) { st=s.substring(0,p);
              wh=s.substring(p+6); }
    j=f_setToJson(t,st);
    if(!j) return (j===false)?false:0;
  }
  
  if(m==1) wh=s.substring(6);
  a=f_where(t,wh,0);
  D=db[t]["data"], Dk=db[t]["key"];
   
  for(r=0;r<a.length;r++){ p=a[r];            // p=pos row
    for(k in j){                              // k=field name
      i=AtoN(t,k);                            // i=pos col     
      y=db[t]["type"][i];
      if(y=="inc") continue;   // no update if auto increment       
      ov=D[p][i], v=j[k]; if(ov==v) continue; 
      D[p][i]=f_TypeValue(y,v);      
    // update key
    if(!Dk[k]) continue;
    if(!Dk[k][v]) Dk[k][v]=[p]; else Dk[k][v].push(p); 
    // delete key
    y=f_binSrc(Dk[k][ov],p);
    if(y[1]) {  continue;  }
    Dk[k][ov].splice(y[0],1);
    if(!Dk[k][ov].length) delete Dk[k][ov];  
    } 
  }     
last_tab=t;
if(AUTOSAVE) save_table(t);
return a.length;
}},


SELECT:function(s,m){   // SELECT field1, fnc(field2), ... FROM table_name WHERE field op value, ... ORDER BY field ASC, field2 DESC,... LIMIT f,n
with(DBL){                 // m = NUMERIC NUM N    default ASSOC A
  var t,r,i,rw, k, md, p0,p1,p2,p3, sf, Dd, ss,
      n=0, f="", wh="", fields=[], a=[], resp=[], rs=[];
  s=fTrRe(s);
  p0=f_POS("FROM ",s);
  p1=f_POS("WHERE ",s); 
  if(p1==-1) {   
    p2=f_POS("ORDER BY ",s);
    p3=f_POS("LIMIT ",s);
    
    k=0;
    if(p2!=-1) k=p2; 
    else { 
      if(p3!=-1) k=p3; else s+=" WHERE 1"; 
    }
    if(k) {
      sf=s.substring(0,k);
      ss=s.substring(k);
      s=sf+"WHERE 1 "+ss;
    }
  }  
  p1=f_POS("WHERE ",s);
  if(p0==-1) { ER=4; return false; }
  t=s.substring(p0+5,p1-1);
  if(!f_verify(t)) { ER=9; return false; }
  
  Dd=db[t]["data"];

  if(!m) m="";
  m=m.toUpperCase().substring(0,1);
  md=(m=="N")?0:1;

  f=fReplace(s.substring(0,p0-1),{" ":""});   //  f = field list
  if(f=="*") fields=SHOW_FIELDS(t);           //  * = all fields
  else fields=f.split(",");
 
  wh=s.substring(p1+6);
  a=f_where(t,wh,1);     // a[...] result
 
  last_tab=t;
  sf=fields[0].toUpperCase();
 
  if(sf=="COUNT(*)") return a.length;

  // DISTINCT
  if(sf.substring(0,8)=="DISTINCT")  {  
    ss=fields[0].slice(9,sf.length-1); f=fTrim(ss);  k=AtoN(t,f); 
    if(typeof(k)=="undefined") { ER=15; return false; }     
      rs=[]; 
      for(r=0;r<a.length;r++){
        rw=Dd[a[r]][k];
        if(rs.indexOf(rw)==-1) rs.push(rw);     
      }
      if(!md) return rs;
      for(r=0;r<rs.length;r++) { resp[r]={}; resp[r][f]=rs[r]; }
      return resp;     
  }   
  sf=sf.substring(0,3); 
  if(sf=="MAX" || sf=="MIN" || sf=="AVG" || sf=="SUM"){
    ss=fields[0].slice(4,fields[0].length-1); f=fTrim(ss); 
    k=AtoN(t,f);
    if(typeof(k)=="undefined") { ER=15; return false; }
    if(db[t]["type"][k]=="string") { ER=20; return false; }
    rs=0;  p0=0,p1=0;
    for(r=0;r<a.length;r++){
        rw=Dd[a[r]][k];
        if(!r) p0=rw, p1=rw;
        else {
          if(rw<p0) p0=rw;
          if(rw>p1) p1=rw;
        }
        rs+=rw;   
    }   
    if(sf=="MIN") rs=p0;
    if(sf=="MAX") rs=p1;
    if(sf=="AVG") if(r) rs=rs/r;
   return rs;
  }         

  n=fields.length;
  for(r=0;r<a.length;r++){
    rw=Dd[a[r]];
    rs=[];
    for(i=0;i<n;i++){       // put value in rs array
      k=AtoN(t,fields[i]);
        if(typeof(k)=="undefined") { ER=15; return false; }
        rs.push(rw[k]);
    }      
    rw=md?{}:[];
    for(i=0;i<n;i++){       // NUMERIC / ASSOCIATIVE   
      if(md) rw[ fields[i] ]=rs[i]; else rw[i]=rs[i];    
    }
    resp.push(rw);   
  }

return resp;
}},


LOCK:function(t){
with(DBL){
  if(!t && last_tab) t=last_tab;
  if(!f_verify(t)) { ER=9; return false; }
  db[t].locked=1;
last_tab=t;
if(AUTOSAVE) save_table(t);
return true;
}},


UNLOCK:function(t){
with(DBL){
  if(!t && last_tab) t=last_tab;
  if(!f_verify(t)) { ER=9; return false; }
  db[t].locked=0;
last_tab=t;
if(AUTOSAVE) save_table(t);
return true;
}},


//-------

SETVAR:function(k,v){      //  SETVAR(key,value);  query("SETVAR key1=value1, key2=value2, ... );
with(DBL){
  if(!active_db) { ER=7; return false; }
  if(typeof(DBL.db["vars"])=="undefined") DBL.db["vars"]={}; 
  if(typeof(v)=="undefined") {
    var r,a,n,ar,av,s;
    a=f_REMOVE_STRINGS(k); 
    k=fTrRe(a[0]), ar=a[1];
    av=k.split(",");
    for(r=0;r<av.length;r++) {
      s=fReplace(av[r],{" ":""});
      a=s.split("=");
      k=a[0];
      v=a[1]; if(v=="#") v=ar.shift();
      else v=eval(v);   
      DBL.db["vars"][k]=v;  
    }
  } else DBL.db["vars"][k]=v;  
  DBL.save_table("vars"); 
}},


GETVAR:function(k){ 
  if(!DBL.active_db) { ER=7; return false; }
  if(typeof(DBL.db["vars"])=="undefined") { ER=17; return false; }
  if(typeof(DBL.db["vars"][k])=="undefined") { ER=17; return false; } 
  return DBL.db["vars"][k];
},


DELVAR:function(k){
  if(!DBL.active_db) { ER=7; return false; }
  if(typeof(DBL.db["vars"])=="undefined") { ER=17; return false; }
  if(typeof(DBL.db["vars"][k])=="undefined") { ER=17; return false; }
  delete DBL.db["vars"][k];
  DBL.save_table("vars");  
  return true;
},

//-------

// parsing WHERE -> return array of index true    // md=1 SELECT(order by, limit)   0=UPDATE-DELETE
f_where:function(t,s,md){   
  s=DBL.fTrim(s);
  var a,v,r,n,f,g,vg,s0,s1,ss,D,k, 
      fk=0, m=0,i=0,
      str=[],as=[],S1=[], rex0,rex1,
      or=[],aor=[],lm=[],lmi,lmn,resp=[], 
      where="", orderby="", limit="";

  a=DBL.f_REMOVE_STRINGS(s);
  s=a[0];
  str=a[1];
        
if(md){   // SELECT   
  ss=s.toLowerCase();
  as=ss.split(" limit ");
  if(as.length>1) limit=as.pop();
  ss=as.join(" limit ");  
  as=ss.split(" order by ");
  if(as.length>1) orderby=as.pop();  
  i=as[0].length, m=orderby.length, r=limit.length;  
  where=s.substring(0,i);
  orderby=s.substring(i+10,i+10+m);
  limit=s.substring(s.length-r);       
} else where=s;

// calculate f o v
ss=where;
with(DBL){ 

  // parse where
  D=db[t];
  
  // substitute field
  n=false;
  for(r=0;r<D.field.length;r++) {
      f=D.field[r], g="RW["+r+"]";
      s=ss;
      ss=ss.replace(new RegExp(f,'g'),g);
      if(s!=ss) { if(n===false) n=r; else n=true; }   // -1 no optimization 
  }

  ss=ss.toUpperCase();
  ss=fReplace(ss,{ "AND":"&&", "OR":"||", " ":"", "]=":"]=="  });
   
  // LIKE
  ss=fReplace(ss,{"NOTLIKE#":".indexOf(#)==-1", "LIKE#":".indexOf(#)>-1" });
  
  // IN NOTIN
  rex0=new RegExp('RW\[[0-9]+\](NOT)*IN\\([0-9#,]+\\)','g');               
  as=[];
           
  for(;;){ 
    a=rex0.exec(ss); if(!a) break;
    n=true;
    as.push( { i:a.index, f:a.index+a[0].length, z:f_InNotin(a[0]) } );
  } 
  for(r=(as.length-1);r>=0;r--) {
    s0=ss.substring(0,as[r].i);
    s1=ss.substring(as[r].f);
    ss=s0+as[r].z+s1;
  }
  // BETWEEN 
  rex1=new RegExp('RW\[[0-9]+\]BETWEEN[0-9#]+&&[0-9#]+','g');
  as=[];  
  for(;;){ 
    a=rex1.exec(ss); if(!a) break;
    as.push( { i:a.index, f:a.index+a[0].length, z:f_Between(a[0]) } );
  }  
  for(r=(as.length-1);r>=0;r--) {
    s0=ss.substring(0,as[r].i);
    s1=ss.substring(as[r].f);
    ss=s0+as[r].z+s1;
  }    
  // field value
  a=ss.split("#"); ss="";
  for(r=0;r<(a.length-1);r++)  ss+=a[r]+"'"+str[r]+"'";
  ss+=a[r];
 
// key optimization in WHERE field=value
if(D.key && typeof(n)=="number") {   
  a=ss.split("]==");
  if(a.length==2) { 
    f=D.field[n]; 
    v=eval( a[1] );
    for(k in D.key) { if(k==f) { 
     if(v!="" && typeof(D.key[f])!="undefined") { resp=D["key"][f][v]; if(!resp) resp=[]; fk=1; break; }
}}}} 
  // order by
  if(orderby) {
    aor=orderby.split(","); 
    for(g=0;g<aor.length;g++){
      vg=fTrim(aor[g]);
      or[g]=vg.split(" ");
      if(or[g].length>1) or[g][1]=or[g][1].toLowerCase(); else or[g][1]=""; 
    }  
  }       
  if(fk) S1=f_ifsearch(t,resp,or,1); else S1=f_ifsearch(t,ss,or,0);    
  // limit 
  if(limit && S1) { lm=limit.split(","); lmi=parseInt(lm[0]); n=S1.length;
          if(lm.length>1) lmn=parseInt(lm[1]); else lmn=n;
          if((lmi+lmn)>n) lmn=n-lmi;        
  S1=S1.slice(lmi, (lmi+lmn) );  
  }
return S1;
}},


f_ifsearch:function(t,s,or,m){   // s=if row | array of result    or = fields to order by     m: 1=fast search by key
with(DBL){
  if(m && !or.length) return s;
  var r,rr,p,v,k,i,f,n,d,ad=[],af=[], 
      RW, Dd=db[t]["data"], resp=[], vresp=[], nor=or.length;    
  for(n=0;n<nor;n++){
    f=AtoN(t,or[n][0]);
    if(typeof(f)=="undefined") { ER=15; return false; }
    af.push(f);
    d=(or[n][1]=="desc")?1:0;       //  1=DESC
    ad.push(d);
  }            
try{
  if(m){                            // key sort
    for(n=0;n<s.length;n++){
      r=s[n]; RW=Dd[r]; 
      if(RW==null) continue;    
      i=f_msort(0,nor,af,ad,RW,vresp);       
      vresp.splice(i,0,RW);
      resp.splice(i,0,r);
    }  
  } else { 
    s="if("+s+") k=1";
    for(r=0;r<Dd.length;r++){ RW=Dd[r]; 
      if(RW==null) continue;
      k=0; eval(s);
      if(k) { 
        if(!nor) resp.push(r);
        else { i=f_msort(0,nor,af,ad,RW,vresp);   // i=position to insert
               vresp.splice(i,0,RW);
               resp.splice(i,0,r);                              
      }}}  
  }
} catch(e){ ER=5; return false; }

return resp;
}},


f_msort:function(n,nor,af,ad,RW,avv){
   var p,i,i0,i1,j,avr, f=af[n], d=ad[n], v=RW[f];                 // f=field pos in RW   d=direction sort       
   p=DBL.f_binSearch(avv,f,v,d); 
   i=p[0];           
     if(!p[1]) {
        while(avv[i][f]==v) { i--; if(i<0) break; }
        i++; i0=i;
        while(avv[i][f]==v) { i++; if(i>=avv.length) break; }
        i1=i;
        n++;
        if(n<nor) {
          avr=avv.slice(i0,i1);
          j=DBL.f_msort(n,nor,af,ad,RW,avr);         
          i=i0+j;
    }}
return i;   
},


f_InNotin:function(s){    // RW[x]NOTIN(1,2,#)
   var I="IN",op="==",a,k,r,z="(",sep="";
   if(s.indexOf("NOTIN")!=-1) I="NOTIN", op="!=";
   s=DBL.fReplace(s,{"\\(":"","\\)":""});
   a=s.split(I);   
   k=a[0];
   a=a[1].split(",");
   for(r=0;r<a.length;r++) { z+=sep+k+op+a[r]; sep="||"; }
   z+=")";
   
return z;   // (RW[x]!=1 || RW[x]!=2 || RW[x]!=#)
},

f_Between:function(s){    // RW[x]BETWEENn&&m 
   var a,k;
   a=s.split("BETWEEN");
   k=a[0];
   a=a[1].split("&&");
   return "("+k+">="+a[0]+"&&"+k+"<="+a[1]+")";         // (RW[x]>=n && RW[x]<=m)
}, 


f_binSearch:function(A,k,x,m) {  
  var i, p=0, q=A.length-1; 
  if(q<0) return [0,1];                      // p[0]=pos in index_sort | p[1] 0=exist  1=not found 
if(m){  
  while(p<=q) { 
      i=parseInt((q+p)/2);
      if(A[i][k]==x) return [i,0];
      if(A[i][k]<x) q=i-1; else p=i+1; 
    }       
  if(A[i][k]>x) i++;        
} else {
    while(p<=q) { 
      i=parseInt((q+p)/2);
      if(A[i][k]==x) return [i,0];
      if(A[i][k]>x) q=i-1; else p=i+1; 
    }       
  if(A[i][k]<x) i++; 
}
return [i,1];
},


f_binSrc:function(A,x) {  
  var i, p=0, q=A.length-1; 
  if(q<0) return [0,1];
  while(p<=q) { 
    i=parseInt((q+p)/2);
    if(A[i]==x) return [i,0];
    if(A[i]>x) q=i-1; else p=i+1; 
  }      
if(A[i]<x) i++;      
return [i,1];
},

//-------

f_POS:function(p,s){
   var ss=s.toUpperCase();
   return ss.indexOf(p);
},


f_NEXTOF:function(p,s){
  var r,i,l,c,v="";
  i=DBL.f_POS(p,s); if(i==-1) return false;  
  l=i+p.length;   
  for(r=l;r<s.length;r++){
    c=s.charAt(r);
    if(c==" "){ if(!v) continue; break; }
    v+=c; l++;
  }
  return { value:v, from:i, to:l };
},


f_CMD:function(s){    //  return R:   false | { R.Cmd command   R.Str  string without command
var cmd,r,c,i,w="",ss=s+" ";
cmd=["CREATE","DROP","SHOW",
     "CREATE_DATABASE","CREATE_TABLE",
     "DROP_DATABASE","DROP_TABLE",
     "SHOW_DATABASES","SHOW_FIELDS","SHOW_TABLES",
     "TRUNCATE","SELECT","INSERT","UPDATE","DELETE",
     "LOCK","UNLOCK",
     "SETVAR","GETVAR","DELVAR"];
    
for(r=0;r<ss.length;r++) { c=ss.charAt(r);  
  if(c==" ") {  if(ss.charAt(r+1)==" ") continue;  
    i=cmd.indexOf(w);
    if(i==-1) return false;
    if(i>2) return { Cmd:w, Str:s.substring(r) };
    c="_";
  } 
 w+=c.toUpperCase();
}
return false;
},
 

f_REMOVE_STRINGS:function(s){  // substitute string 'string' with # => as[string0, string1, ....]
  var v,r,g=[],o=[],w="",m=0,i=0,ss="";
  for(r=0;r<s.length;r++) { 
    v=s.charAt(r);  
    if(v=="'"){ 
      if(m) { o.push( [w,i,r+1] ); w=""; m=0; } 
      else m=1,i=r;      
    continue; 
    }    
    if(m) w+=v;
  }
  if(o.length){ i=0; 
    for(r=0;r<o.length;r++) {
      ss+=s.substring(i,o[r][1])+"#";
      i=o[r][2];  
    g.push(o[r][0]); 
        
    }
    ss+=s.substring(i);
    s=ss; 
  }
  s=s.replace(/\s{2,}/g, ' ');                    // remove multiple spaces 
  s=DBL.fReplace(s, {", ":",", " ,":","} );       // remove , with near space
   
  return [s,g];
},



f_strToJson:function(t,s,j){    // string:   (field1, field2, ...) VALUES (value1, value2, ...) [ ,  (field1, field2, ...), (value1, value2, ...), .... ]
with(DBL){
  if(!j) j=[];
  var r,a,as,ss,i,f,k,nf,av,vs,vj,v,af,fld;
  
  a=f_REMOVE_STRINGS(s);
  ss=a[0], as=a[1];

  f=ss.indexOf(")");
  if(f==-1) { ER=5; return false; }
  fld=ss.substring(1,f);
  fld=fReplace(fld,{"`":"", "‘":"", "’":"", "\\n":""});
  af=fld.split(",");
  
  nf=af.length;
  for(r=0;r<nf;r++) {   // field name verify
    k=AtoN(t,af[r]);
    if(typeof(k)=="undefined") { ER=15; return false; }
  }
  for(;;) {
   ss=ss.substring(f+1);
   i=ss.indexOf("(");
   f=ss.indexOf(")");
   if(i==-1 || f==-1) break;
   vs=ss.substring(i+1,f);
   vs=fReplace(vs,{" ":""});
   av=vs.split(",");  
    
   if(av.length!=nf) { ER=16; return false; }
   vj={};
     for(r=0;r<nf;r++) {
      v=av[r]; if(v=="#") v="'"+as.shift()+"'";   
      vj[af[r]]=eval(v);   
     }
   j.push(vj);
  } 

return j;
}},


f_setToJson:function(t,s){   // field = value, ... | return json { field:value, ... }
with(DBL){           
  var p,f,v,i,c,n,k, j={};         
  s=fTrim(s);         
  while(s.indexOf("=")!=-1){
    p=s.indexOf("=");  if(!p) return j;
    f=s.substring(0,p);
    f=fTrim(f);         
    k=AtoN(t,f);
    if(typeof(k)=="undefined") { ER=15; return false; }        
    s=s.substring(p+1);
    v="",n=0; 
    for(i=0;i<s.length;i++){ c=s.charAt(i);
      if(c=="'" && !n) { n=1; continue; }
      if(c==" " && !v) continue;
      if(c=="'" && n) { n++; continue };
      if(c==",") break;                                  
      v+=c; 
   }                    
   v=fTrim(v);
   if(db[t]["type"][k]!="string") j[f]=eval(v); else j[f]=v;
   s=s.substring(i+1);                    
 }
  return j;
}},


f_insert_row:function(t,a,m){   // a=array row, m   0=array  1=associative 
with(DBL){
  var k,n,y,v,i,p, ii=0, D,Dd;
      D=db[t], Dd=D["data"];
  if(D.erased.length) i=D.erased.shift();
  else i=D["data"].length;
  n=D.count+1;      
  Dd[i]=fCloneA(D["blank"]);              
    for(k in a){           // k field name  
     p=m?AtoN(t,k):k;     // field pos 
     if(typeof(p)=="undefined") continue;
     y=D["type"][p];     // field type
     v=a[k];            // value
     if(y=="inc") { v=parseInt(v); v=(v<n)?n:v; D.count=v; ii=1; }    // auto increment correction
     Dd[i][p]=f_TypeValue(y,v);
    } 
    p=D["type"].indexOf("inc");                   // auto increment if not specify 
    if(p!=-1 && !ii) { D.count=n; Dd[i][p]=n; }   
    for(k in D["key"]){      // key: { field0:{ value1:[pos1, pos2, ....], value2:[], ...  }, ... },
        p=AtoN(t,k);
        y=Dd[i][p];  
        if(!D["key"][k][y]) D["key"][k][y]=[i]; else D["key"][k][y].push(i);
    }  
  last_id=D.count;   
}},

 

f_TypeValue:function(y,v,t){      // y=type  v=value   t=undefined
with(DBL){

  if(t) { var p=AtoN(t,y);        // y=field name   t=table
          y=db[t]["type"][p]; }

 if(y=="string") return v+"";
 if(y=="int" || y=="inc") return parseInt(v);
 if(y=="real") return parseFloat(v);
 return v;
}},

//------------------------------------------------------------------------------
// localstorage

Set:function(k,val){
  var s;
  if(typeof(val)=="string") s=val; else s=JSON.stringify(val);
  DBL.JStore[k]=s;   
  return true;    
},

Get:function(k){
with(DBL){
  var l=JStore[k]; 
  if(typeof(l)=="undefined") return null;
  return JSON.parse(l); 
}},

Del:function(k){
with(DBL){  
  var l=JStore[k]; 
  if(typeof(l)=="undefined") return null;
  delete DBL.JStore[k];
  return; 
}},


 


//------------------------------------------------------------------------------
// functions

fReplace:function(str,s){  
  for(var k in s) { str=str.replace(new RegExp(k,'g'), s[k] );   }
return str;  
},

fTrim:function(s){
  return s.replace(/^\s+|\s+$/g,""); 
},


fTrRe:function(s){
with(DBL){
  s=fTrim(s);
  s=s.replace(/\s{2,}/g, ' ');                              // remove multiple spaces
  return fReplace(s,{"`":"", "‘":"", "’":"", "\\n":""});
}},

fCloneA:function(A){  var D,C=[];
for(D in A) C[D]=A[D];
return C;
},

fCloneO:function(A){ 
if(!A) return A; 
var D,C=(typeof(A.length)=="undefined")?{}:[]; 
for(D in A) { C[D]=(typeof(A[D])=='object')?(DBL.fCloneO(A[D],D)):A[D];   }
return C;
},

error:function(){
with(DBL){
   if(ER) return ERROR[ER];
   return false;   
}}


} // end DBL light





//////////////////////////////////////////////////////////////////////////////////

ConsoleL = { Bj:null, Jt:null, jt:null, jb:null, mj:null, Mj:null, FD:[], SP:"style='position:absolute;", 
            ListCmd:["?# SHOW TABLES","quit","hide","cls","? *","?"], ListPtr:6, ver:2, 
            resizegif:"data:image/gif;base64,R0lGODlhIAAgAJEDAJmZmY6OjqWlpf///yH5BAEAAAMALAAAAAAgACAAAAJNnI+py+0Po5y02ouz3rxz4SECGA5jUI4A6p1s54ZxKwTvV98kNhtj1jPVeDlR0RIMTpLHCtN2ed6WTSHUt3tID6fIFnt1fH22aemMKAAAOw==",
            closegif:"data:image/gif;base64,R0lGODlhIAAgAIAAAAAAAP///yH5BAEAAAEALAAAAAAgACAAAAIxjI+py+0Po5y02ouz3rwrAHyhBo5lV55e6h1ga7BrOm6qmr2JDvf+DwwKh8Si8bgpAAA7",
            maxgif:"data:image/gif;base64,R0lGODlhHAAcAJEAAAAAAP///////wAAACH5BAEAAAIALAAAAAAcABwAAAIxlI+py+0Po5y02ouzZqD7b30iEJJJVypotSLt9BpxNM9QbVIjyO72BgwKh8Si8fgoAAA7",
            mingif:"data:image/gif;base64,R0lGODlhHAAcAJEAAAAAAP///////wAAACH5BAEAAAIALAAAAAAcABwAAAIglI+py+0Po5y02ouz3rz7D4YaQJamZaaAyLbuC8dyVgAAOw==",
         
View:function(){
with(ConsoleL){  

  var r,s,l;

  Mj=jCreateElem("idConsoleLMask","div");
  Mj.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF6;opacity:0.0;z-index:10010;display:none;");

  Bj=jCreateElem("idConsoleLBox","div");
  Bj.setAttribute("style","position:absolute;left:10%;top:10%;width:70%;height:70%;background-color:#AAA;z-index:10000;box-shadow:3px 6px 12px #555;border-radius:3px;");
    
  s="<div id='idConsoleLBar' "+SP+"left:0px;top:0px;right:2px;height:28px;cursor:move;' >"+
    "<div "+SP+"left:8px;top:7px;font:bold 13px Arial;color:#000;'>ConsoleL</div>"+
    "<img src='"+mingif+"' "+SP+"right:84px;top:0px;cursor:pointer;' onmousedown='ConsoleL.f_Hide()' ontouchstart='ConsoleL.f_Hide()' />"+
    "<img src='"+maxgif+"' "+SP+"right:44px;top:0px;cursor:pointer;' onmousedown='ConsoleL.f_Big(event)' ontouchstart='ConsoleL.f_Big(event)' />"+
    "<img src='"+closegif+"' "+SP+"right:4px;top:0px;cursor:pointer;' onmousedown='ConsoleL.f_Close()' ontouchstart='ConsoleL.f_Close()' /></div>"+
    
    "<div "+SP+"left:2px;bottom:2px;height:32px;right:32px;'>"+
    "<input id='idConsoleLCmd' type='text' value='' "+SP+"left:0px;top:2px;width:100%;height:30px;background-color:#444;"+
    "border:0px solid #000;font:13px Arial;color:#FFF;padding:4px 2px 4px 8px;' onkeyup='ConsoleL.f_Key(event)' /></div>"+ 
    "<img id='idConsoleLResize' src='"+resizegif+"' style='position:absolute;right:0;bottom:0;cursor:nw-resize;' />"+ 
      
    "<div id='idConsoleLView' "+SP+"left:2px;top:28px;right:2px;bottom:34px;background-color:#444;"+  
    "overflow:auto;font:12px Arial;color:#FFF;padding:8px 4px 4px 8px;'></div>";
    
  Bj.innerHTML=s;
  jb=j$("idConsoleLBar");
  mj=j$("idConsoleLResize");
  Jt=j$("idConsoleLView");
  jt=j$("idConsoleLCmd");
   
  // eventi move
  l=["touchstart","touchmove","touchend","mousedown"];
  for(r=0;r<4;r++) { 
    eval("jb.on"+l[r]+"=function(e){ConsoleL.f_evt(e,0);}");
    eval("mj.on"+l[r]+"=function(e){ConsoleL.f_evt(e,1);}");  
  }

  try{ver=DBL.ver;}catch(e){}
  Jt.innerHTML="<div style='font:bold 16px Arial;color:#CCC;'>Local Database <span style='font:bold 13px Arial;'>ver: "+ver+"</span><br></div>"+
    "<b>?</b> <i>Help | Print</i><br>"+
    "<b>quit</b> <i>close this console</i><br>";

  jt.focus();
}},



// events
f_evt:function(e,g){
with(ConsoleL){  
       
  var ev=e.type,nt=1,tch=null,r,t,x,y,i,ex,ey,P,F,v;
  if(ev.substring(0,5)=="touch") tch=e.changedTouches, nt=tch.length;
  
  if(ev=="touchstart" || ev=="mousedown"){ 
          if(tch) t=tch[0], x=t.pageX, y=t.pageY, i=t.identifier;
          else { P=jCoords(e), x=P.x, y=P.y, i=0;
                 Mj.style.display="block";     
                 Mj.onmousemove=function(e){f_evt(e,g); }
                 Mj.onmouseup=function(e){ f_evt(e,g); }                   
               }                                       
          P=jGetPos(Bj);              
          FD.push({ x:x, ix:x, iy:y, f:i, pl:P.l, pt:P.t, pw:P.w, ph:P.h });                             
   }
 
  if(ev=="touchmove" || ev=="mousemove"){
    if(FD.length) { F=FD[0];              
         for(r=0;r<nt;r++) { 
            i=0; if(tch) t=tch[r], i=t.identifier;   
            if(F.f!=i) continue;
              if(tch) ex=t.pageX, ey=t.pageY; else P=jCoords(e), ex=P.x, ey=P.y; 
              x=ex-F.ix, y=ey-F.iy;
              
              if(!g) {  // move
                x+=F.pl;
                y+=F.pt; 
                Bj.style.left=x+"px";
                Bj.style.top=y+"px";   
              } 
              
              if(g==1){   // resize
                x+=F.pw; if(x<160) x=180;
                y+=F.ph; if(y<120) y=160;
                Bj.style.width=x+"px";
                Bj.style.height=y+"px";                   
              }   
    }}}
   
  if(ev=="touchend" || ev=="mouseup"){  
   if(FD.length){
      Mj.onmousemove="";
      Mj.onmouseup="";
      Mj.style.display="none";
      FD=[];                    
   }} 
   
e.preventDefault();   
}},
 


f_Big:function(e){
with(ConsoleL){  
  Mj.style.display="block";  
  var w=Mj.offsetWidth-16, h=Mj.offsetHeight-16;
  Mj.style.display="none"; 
  with(Bj.style) left="8px",top="8px",width=w+"px",height=h+"px";    
e.preventDefault(); 
}},



f_Scrl:function(){
with(ConsoleL){    
  var h,H;  
  h=Jt.scrollHeight, H=Jt.offsetHeight;
  Jt.scrollTop=(h-H);
}},


f_Close:function(m){
with(ConsoleL){
 Bj.style.display=m?"block":"none";
 var tj=j$("idConsoleLHideBox");
 if(tj) tj.style.display="none";
 jt.focus();
}},


f_Hide:function(){
with(ConsoleL){
  f_Close(0);
  var tj=jCreateElem("idConsoleLHideBox","div");
    tj.setAttribute("style","position:absolute;right:10px;bottom:10px;width:32px;height:32px;opacity:0.5;background-color:#AAA;display:block;cursor:pointer;z-index:14000;");
    tj.innerHTML="<div "+SP+"left:2px;top:2px;right:2px;height:18px;background-color:#444;overflow:hidden;'></div>"+
                 "<div "+SP+"left:2px;bottom:2px;right:2px;height:8px;background-color:#444;overflow:hidden;'></div>";
    tj.onclick=function() { ConsoleL.f_Close(1);  }        
}},


f_Key:function(e){
with(ConsoleL) with(DBL){ 
  if(!e) e=window.event;   
  var V,v,sv,aV,aV0,s,a,y,k,cv,cl,r,i,en="0123456789ABCDEF",
      ltb=[],qu=0,pr=0,er=0,R="",
      t=(e.which)?e.which:e.keyCode;
  if(t==8 && e.shiftKey) { jt.value=""; return;}  
  if(t==13){
    ltb=SHOW_TABLES(); 
    v=fTrim(jt.value);
    if(v){
        ListCmd.push(v); 
        ListPtr=ListCmd.length; 
        if(ListPtr>32) { ListCmd.shift(); ListPtr--; }  
        V=v.toUpperCase();    
        if(V=='QUIT' || V=='EXIT') { f_Close(); return; }
        if(V=='HIDE') { f_Hide(); jt.value=""; return; }
        if(V=='HELP' || V=='?') { f_Help(0); return; }
        if(V=='? #' || V=='?#' || V=="#") { f_Help(1); return; }
        if(V=="CLS") { Jt.innerHTML=""; jt.value=""; return; }
        if(V=="NOW") v="?"+v+"()";
        if(V=="?NOW" || V=="? NOW") v+="()";     
        Jt.innerHTML+="<br><span style='color:#BBB;'>"+v+"</span><br>";
          // # query qu
          sv=v.substring(0,6);
          cv=sv.indexOf("#");
          if(cv>-1){
          
            // is color?
            if(sv.length>3){
              cl=(v.substring(cv+1,cv+4)).toUpperCase();
              i=1;
              for(r=0;r<3;r++){ if(en.indexOf(cl.charAt(r))<0) { i=0; break; }  }
              if(i) { Color(v.substring(cv)); return;  }
            }
            ER=0; if(V.indexOf("SELECT")!=-1) qu=1;
            aV=v.split("#");
            aV0=aV.shift();
            v=aV0+" query(\""+fTrim(aV.join("#"))+"\")";
          }
          // ? print pr
          if(v.charAt(0)=="?") { 
          pr=1;  
          v=fTrim(v.substring(1));
          if(ltb.indexOf(v)!=-1) { viewTab(v); return; }
          if(v=="*") { viewAllTab(); return; }
          }          
                   
      try{ R=eval(v); } catch(e){ jt.style.color="#F88"; er=1; Jt.innerHTML+="<span style='color:#E88;'>"+e+"</span><br>"; return; } 
          
      // view             
      if(!er) { y=typeof(R);        
        if(pr){
          if(qu && y=="object") { viewTab(R); return;  }          
          if(y=="object") {            
            R=preJsn(R);    // R=JSON.stringify(R);
          }
          Jt.innerHTML+=R+"<br>"; 
        }
        // if query error
        if(qu && ER) { Jt.innerHTML+="<span style='color:#F88;'>("+ER+") "+error(ER)+"</span><br>"; }
        jt.value="";  
        f_Scrl(); 
      }             
    } else {
      f_Cmd("<br>");
    }  
  } else { 
    jt.style.color="#FFF";        
    if(t==38) { ListPtr++; if(ListPtr>=ListCmd.length) { ListPtr=0;  }
                jt.value=ListCmd[ListPtr]; }                    
    if(t==40) { ListPtr--; if(ListPtr<0) { ListPtr=ListCmd.length-1; } 
                jt.value=ListCmd[ListPtr]; }
  }
}},


f_Cmd:function(s){
with(ConsoleL){
  Jt.innerHTML+=s;     
  f_Scrl();
  jt.value="";
}},

            
Color:function(c){  
  c=c.replace(/\s{1,}/g,'');
  var r,s="", a=c.split(","); 
  for(r=0;r<a.length;r++)   
  s+="<div style='float:left;width:64px;height:64px;margin:8px;padding:6px;background-color:"+
  a[r]+";font:12px Arial;color:"+ConsoleL.Ink(a[r])+";'>Ink(Color)</div>";  
  ConsoleL.f_Cmd(s+"<div style='clear:both;height:0;'></div><br>");
},


Ink:function(c){
  var r,i="#FFFFFF", n=c.length, d=(n>4)?2:1, dd=0, v="", rgb=0;
  for(r=1;r<n;r++) { 
    v+=c.charAt(r); dd++;  
    if(dd==d) { v=(d>1)?v:(v+v);  
      rgb+=parseInt(v,16);  
      v=""; dd=0; }    
  }
  rgb/=3;  
  if(rgb>144) i="#000000";
return i;
}, 


preJsn:function(jn,n){
with(ConsoleL){
  if(!jn) return "null";
  if(!n) n=0;  
  var i,s,
      b=["[","]","{","}"],
      br="<br>", 
  i=(jn.length)?0:2; 
  s=b[i] + br + jsonView(jn,n+1) + br + nIndent(n) + b[i+1];
  return s;
}},


nIndent:function(n){
  var r,g=3,s="";    
  for(r=0;r<(n*g);r++) s+="&nbsp;";
  return s;
},

jsonView:function(jn,n){
with(ConsoleL){
  if(!n) n=0;
  var k,i,y,s="",tbs="", ts="",
      sep="",
      br="<br>",   
  tbs=nIndent(n); 
  ts=nIndent(n+1);
  i=(jn.length)?0:2;
  n++;          
  for(k in jn){      
      if(sep) s+=sep + br;      
      v=jn[k];
      y=typeof(v);
      lbl=i?('"'+k+'" : '):"";      
      if(y!="object") {
        g=(y=="string")?'"':'';
        s+= ts + lbl + g+v+g;  
      }else {
        s += ts + lbl + preJsn(v,n);
      }
    sep=",";
  }
  return s;
}},


viewAllTab:function(){
with(ConsoleL){
  var r,a=DBL.SHOW_TABLES();
  for(r=0;r<a.length;r++) viewTab(a[r]);
}},


viewTab:function(t){     // t:   string=name of DBL table   |  json [ {...},... ]   [ [...],... ]
with(ConsoleL){ 
  var r,s="",k,n,m,jj,cj, jd=[],z="",v;    
  info="";
  if(typeof(t)=="string"){   // table
    with(DBL){
      if(typeof(db[t])=="undefined") { Jt.innerHTML+="table not found<br>"; return; }
      jd=db[t]["data"];
      n=db[t]["field"].length;
      info="<br>Database: <b>"+active_db+"</b> - Table: <b>"+t+"</b>";
      z="<tr>"; for(r=0;r<n;r++) z+="<th style='background-color:"+(db[t].temp?"#CAA":"#AAA")+";'>"+db[t]["field"][r]+"</th>"; z+="<tr>";   // header
    }                                        
  } else { 
      if(typeof(t)!="object") s=t;
      else {
           jd=t; info="<br>Json view as table:";    // json resp 
           if(!jd.length) { Jt.innerHTML+="empty table."; return; }
           k=1;
           if(typeof(jd.length)=="undefined") k=2;
           else { if(typeof(jd[0].length)!="undefined") k=0;
           else if(typeof(jd[0])!='object') k=0; } 
      jj=(k==1)?jd[0]:jd;
      if(k) { z="<tr>"; for(r in jj) z+="<th>"+r+"</th>"; z+="<tr>"; }
    }
  }  
  if(!s){
    s="<style>.cltable th { background-color:#AAA; font:bold 11px Arial; color:#444;  } "+
      ".cltable td { background-color:#888; font:11px Arial; color:#FFF;  } </style>"+
      "<div style='font:12px Arial;color:#DDD;'>"+info+"</div>"+
      "<table class='cltable' cellpadding='4px' cellspacing='2px'>"+z;         
    // body     
    for(r=0;r<jd.length;r++){   
      if(!jd[r]) continue;   // erased row
      s+="<tr>";         
      for(k in jd[r]) { col="";
           v=jd[r][k];
           if(typeof(v)!="string") { col="style='color:#DEF'";
               if(parseInt(v)==v && v>1000000000) { v+="<br><span style='font:9px Arial;color:#ECE6CB'>("+fdate(v)+")</span>";  }        // as data 
           }
           s+="<td "+col+">"+v+"</td>";  
      } s+="</tr>";
    }  s+="</table>";
  }          
  f_Cmd(s);
}},


opacity:function(v){
with(ConsoleL){  
  v=parseInt(v);
  if(v<0 || v>100) return;
  if(Bj) Bj.style.opacity=v/100;
}},


f_Help:function(m){      // m:   0 help for ?   1 help for DBL
with(ConsoleL){ 
  var s="";
if(m) {  // DBL

s="<br><div style='font:bold 16px Arial;color:#FFF;'>Database Command List.</div>"+
"<br><span style='font:bold 14px Arial;color:#FFF;text-decoration:underline;'>Properties:</span><br>"+
"- active_db<br>"+
"- last_tab<br>"+
"- last_query<br>"+
"- last_id<br>"+
"- AUTOSAVE<br>"+
"- ER<br>"+
"<br><span style='font:bold 14px Arial;color:#FFF;text-decoration:underline;'>Method:</span><br>"+
"- query(string,[json])<br>"+
"- connect([db_name])<br>"+
"- save_database()<br>"+
"- save_table([tab_name])<br>"+
"- exist_database(db_name)<br>"+
"- f_verify(tab_name)<br>"+
"- close()<br>"+
"- error()<br>"+
"<br><span style='font:bold 14px Arial;color:#FFF;text-decoration:underline;cursor:pointer;' "+
    "onclick='var itftj=ConsoleL.j$(\"idinfoDBLfieldquery\"); itftj.style.display=(itftj.style.display==\"none\")?\"block\":\"none\";'>Query():</span><br>"+
"<span id='idinfoDBLfieldquery' style='display:none;'>"+
"- CREATE DATABASE [IF NOT EXISTS] db_name<br>"+ 
"- SHOW DATABASES<br>"+
"- DROP DATABASE db_name<br>"+ 
"- SHOW TABLES<br>"+  
"- SHOW FIELDS tab_name<br>"+ 
"<br>- CREATE TABLE tab_name ( field_name type [unsigned] [DEFAULT] [value] [not] [null] [auto_increment] [key], .... )<br>"+
"- TRUNCATE [TABLE] tab_name<br>"+
"- DROP TABLE tab_name<br>"+
"- LOCK tab_name<br>"+
"- UNLOCK tab_name<br>"+    
"<br>- INSERT INTO table_name (field1, field2, ...) VALUES (value1, value2, ...) [ , (value1, value2, ...), .... ]<br>"+
"- DELETE FROM table_name [WHERE 1]<br>"+
"- UPDATE table SET field1=value1, field2=value2 [WHERE...]<br>"+
"- SELECT fnc(field1), ... FROM table_name WHERE field op value, ... ORDER BY field ASC,... LIMIT f,n<br>"+ 
"- query(\"INSERT INTO table_name\", json);<br>"+  
"- query(\"UPDATE table_name [WHERE...]\", json);<br>"+  
"<br>- SETVAR var_name1=value1, var_name2=value2, ...<br>"+
"- GETVAR var_name1 [, var_name2, ... ]<br>"+
"- DELVAR var_name1 [, var_name2, ...]<br>"+
"</span>"+
"<br><span style='font:bold 14px Arial;color:#FFF;text-decoration:underline;cursor:pointer;' "+
    "onclick='var itftj=ConsoleL.j$(\"idinfoDBLfieldtype\"); itftj.style.display=(itftj.style.display==\"none\")?\"block\":\"none\";'>Field types:</span><br>"+
"<span id='idinfoDBLfieldtype' style='display:none;'>"+
"- inc<br>"+
"- int<br>"+
"- string<br>"+
"- real<br>"+
"- varchar (string)<br>"+
"- text (string)<br>"+
"- tinytext<br>"+
"- float (real)<br>"+
"- double (real)<br>"+
"- int auto increment (inc)"+
"</span>"+
"<br><span style='font:bold 14px Arial;color:#FFF;text-decoration:underline;cursor:pointer;' "+
    "onclick='var itftj=ConsoleL.j$(\"idinfoDBLfieldfnc\"); itftj.style.display=(itftj.style.display==\"none\")?\"block\":\"none\";'>Field function in SELECT:</span><br>"+
"<span id='idinfoDBLfieldfnc' style='display:none;'>"+
"- * all fields"+
"- distinct(field_name)<br>"+
"- max(field_name)<br>"+
"- min(field_name)<br>"+
"- avg(field_name)<br>"+
"- sum(field_name)<br>"+
"- count(*)<br>"+
"</span>"+
"<br><span style='font:bold 14px Arial;color:#FFF;text-decoration:underline;cursor:pointer;' "+
    "onclick='var itftj=ConsoleL.j$(\"idinfoDBLfieldoper\"); itftj.style.display=(itftj.style.display==\"none\")?\"block\":\"none\";'>Where operator in SELECT:</span><br>"+
"<span id='idinfoDBLfieldoper' style='display:none;'>"+
"- ( =, !=, >, <, >=, <=, <>, &, | )  standard operator<br>"+
"- LIKE 'value' ('%' and '_' are not valid inside string)<br>"+
"- BETWEEN field1 AND field2<br>"+
"- IN (...)<br>"+
"- NOT (NOT LIKE, NOT IN)"+
"</span>"+
"<br>";

} else {  // ?

s="<br><span style='font:16px Arial;line-height:28px;'><b>Help:</b><br></span>"+
  "<span style='font:12px Arial;line-height:18px;'><b>?</b> <i>(print vars, array, json and table of query result)</i><br>"+
  "<b>#</b> <i>(execute a query)</i><br>"+
  "<span style='text-decoration:underline;cursor:pointer;' onclick='with(ConsoleL) { Jt.innerHTML=\"\"; jt.value=\"\"; jt.focus(); }'><b>cls</b></span> <i>(clear ConsoleL window)</i><br>"+
  "<b>shift + canc</b> <i>(clear command row)</i><br>"+
  "<span style='text-decoration:underline;cursor:pointer;' onclick='ConsoleL.f_Hide()'><b>hide</b></span> <i>(hide ConsoleL)</i><br>"+
  "<b>quit or exit</b> <i>(close ConsoleL)</i><br>"+
  "<b>help</b> <i>(alias of '?')</i><br>"+
  "<b>opacity(x)</b> <i>(opacity of ConsoleL 0<x<100)</i><br>"+
  "<b>#color1, #color2, ...</b> <i>(show boxs with the colors and Ink(color) for text )</i><br>"+
  "<b>now</b> <i>(show date and timestamp)</i><br>"+
  "<b>? *</b> <i>view all tables of active db</i><br></span>";
}
  f_Cmd(s);
}},


fdate:function(t){
with(ConsoleL){
  var d=new Date(t*1000),
  h=fzero(d.getHours(),2),
  m=fzero(d.getMinutes(),2),
  D=fzero(d.getDate(),2),
  M=fzero(parseInt(d.getMonth())+1,2),
  Y=d.getFullYear();
return D+"/"+M+"/"+Y+" - "+h+":"+m;
}},

fnow:function(){
  return parseInt((new Date()).getTime()/1000);
},

now:function(){
with(ConsoleL){
  var t=fnow();
  return "date: "+fdate(t)+"<br>timestamp: "+t+"<br>";
}},

fzero:function(v,n){
  var l=(v+"").length,r;
  for(r=l;r<n;r++) v="0"+v;
  return v;
},


jCreateElem:function(nm,tpe,pdr){ 
with(ConsoleL){
if(!j$(nm)){ 
if(!pdr) pdr=document.body;
var j=document.createElement(tpe);
j.id=nm; pdr.appendChild(j);
j.style.position="absolute";
return j;
} else return j$(nm);
}}, 


jCoords:function(e){ e=e||window.event;  if(e.pageX || e.pageY) return {x:e.pageX, y:e.pageY}; 
return { x:e.clientX+document.body.scrollLeft-document.body.clientLeft, 
         y:e.clientY+document.body.scrollTop-document.body.clientTop }; 
},

jGetPos:function(j){ 
var L=0,T=0,W=j.offsetWidth,H=j.offsetHeight;
while(j.offsetParent){ 
L+=j.offsetLeft; T+=j.offsetTop;
j=j.offsetParent; } 
L+=j.offsetLeft; T+=j.offsetTop;
return {l:L,t:T,w:W,h:H};
},

j$:function(l){ return document.getElementById(l)}


} // end ConsoleL
