moPicklist = function (name) {
    this.w = 175, this.h = 27, this.type = "moPicklist", this.className = "picklist", this.picktype = "", this.table = "",
            this.isparent = false, this.ftype = "", this.filters = "", this.column = "{f_title}", this.nome_utente_change_flag = false;
    this.tagsObj = null;
    var fieldid = "", /*btnid = "", */result = {};

    //********************************************
    // treegrid backend filters
    //********************************************
    this.BACKEND_SEARCH_CONF = [];
    this.BACKEND_SEARCH = [];
    this.addBackendSearch = function (type, sel, field, value) {
        this.BACKEND_SEARCH.push({"f": field, "v0": Base64.encode(value), "Sel": sel, "type": type, "Mm": 0});
    }
    this.removeBackendSearch = function (field) {
        for(var i = 0; i < this.BACKEND_SEARCH; i++){
            if(this.BACKEND_SEARCH[i].f == field)
            break;
        }
        this.BACKEND_SEARCH.splice(i, 1);
    }

    this.BACKEND_SELECTOR_CONF = []; //Array per tenere i selettori messi in conf
    this.BACKEND_SELECTOR = {
        "picklist_selector": {
            "s": [],
            "h": 0
        }
    }
    this.addBackendSelector = function (selectorCode) {
        if (this.BACKEND_SELECTOR.picklist_selector.s.indexOf(selectorCode) == -1) {
            this.BACKEND_SELECTOR.picklist_selector.s.push(selectorCode);
        }
    }
    this.removeBackendSelector = function (selectorCode) {
        // remove single selector
        if (selectorCode != undefined) {
            var index = array.indexOf(selectorCode);
            if (index > -1) {
                this.BACKEND_SELECTOR.picklist_selector.s.splice(index, 1);
            }
        }
        // remove all selectors
        else {
            this.BACKEND_SELECTOR.picklist_selector.s = [];
        }
        // reimposto i selettori inseriti da configurazione
        for (var code in this.BACKEND_SELECTOR_CONF) {
            if (this.BACKEND_SELECTOR.picklist_selector.s.indexOf(this.BACKEND_SELECTOR_CONF[code]) == -1) {
                this.addBackendSelector(this.BACKEND_SELECTOR_CONF[code]);
            }
        }
    }


    this.create = function (props) {
        with (this) {

            baseCreate(props);

            var field = moGlb.createElement(fieldid, "div", false, "relative"), o = this;
            field.setAttribute("readonly", true);

          
    };
    this.display = function (l0, t0, cid) {
        with (this) {
            if (arguments.length > 0) {
                var cpos = moGlb.getPosition(mf$(componentBox)), field = mf$(fieldid);
                //with(field.style) {top = padding+"px"; left = l0+"px"; width = (cpos.w-50)+"px"; display = "block";}
                //with(field.style) {top = padding+"px"; left = l0+"px"; width = w+"px"; display = "block";}
                if (mf$(cid))
                    mf$(cid).appendChild(field);
                field.style.display = "block";
                field.className = "input textarea clearfix";
                field.style.minHeight = "25px";
                field.style.width = w + "px";
                var o = this;

                this.tagsObj = new Taggle(field.id, {
                    onTagRemove: function (event, tag, code) {
                        if (event != undefined) {
                            if (event != 1) {
                                event.stopPropagation();
                                window[this.picklist].remove(tag, code);
                            }
    
                            //console.log(code);
                        }
                    },
                    picklist: this.name,
                    preserveCase: true
                });
                
                // autocomplete init
            var container = this.tagsObj.getContainer();
            $(this.tagsObj.getInput()).autocomplete({
                // ajax backend call
                source: function(request, response){
                
                    // add autocomplete search to picklist filters
                    this.options.pickListObj.addBackendSearch(0, 0, 'f_title', request.term);
    
                    var params = {
                        "Hierarchy": 0,
                        "Prnt": -1,
                        "Tab": [this.options.pickListObj.table, this.options.pickListObj.ftype, ""],
                        "module_name": this.options.pickListObj.pname.replace('_c1', ''),
                        "Sort": [{
                            "f": "f_title",
                            "m": "ASC"
                        }],
                        "Filter": [],
                        "Search": this.options.pickListObj.BACKEND_SEARCH,
                        "Ands": [],
                        "Selector": this.options.pickListObj.BACKEND_SELECTOR,
                        "Like": {},
                        "Limit": [0, 0],
                        "rtree": "root",
                        "timestamp": 0,
                        "self": "",
                        "ignoreSelector": "",
                        "skipwf": "",
                        "pcross_inv": 0,
                        "fromPicklist": true,
                        "picklistName": this.options.pickListObj.name,
                        "picklistType": this.options.pickListObj.picktype
                    };
                
                    $.ajax({
                        method: 'POST',
                        data: 'params=' + JSON.stringify(params),
                        url: moGlb.baseUrl + "/autocomplete/pick-list"
                    })
                    .success(function(resp){
                        var responseObj = JSON.parse(resp);
                        eval(responseObj.picklistName).removeBackendSearch('f_title');
                        response(responseObj.data);
                    });
                },
                appendTo: container,
                position: { at: "left bottom", of: container },
                select: function(event, data) {
                    event.preventDefault();
                    //Add the tag if user clicks
                    if (event.which === 1) {
                        eval(data.item.picklistName).setResult([{f_code : data.item.value, f_title : data.item.label}]);
                        var picklistObj = eval(data.item.picklistName);
                        if (!(picklistObj.isbatch && !picklistObj.batch)) {
                            var aux = eval(data.item.picklistName).pname.split("_");
                            try {
                                if (eval(data.item.picklistName).table == "t_selectors") {
                                    eval("mdl_"+ aux[1] +"_slc_tg").setSelected([data.item], true);
                                } else {
                                    eval("mdl_"+ aux[1] +"_" + eval(data.item.picklistName).picktype + "_tg").setSelected([data.item], true);
                                }
                            } catch (e) {}
                        }
                    }
                    this.blur();
                },
                pickListObj : this,
                open: function() { 
                    $('#' + container.id + ' .ui-menu').width(container.clientWidth + 50);
                    $('#' + container.id + ' .ui-menu').css('margin-top', 2);
                }
            });
    
                if (props.filter_newpicklist != "" && props.filter_newpicklist != undefined) {
                    var filtri = JSON.parse(props.filter_newpicklist);
                    for (var code in filtri.selectors) {
                        BACKEND_SELECTOR_CONF.push(parseInt(filtri.selectors[code]));
                        addBackendSelector(parseInt(filtri.selectors[code]));
                    }
                    for (var i in filtri.search) {
                        addBackendSearch(filtri.search[i].type, filtri.search[i].sel, filtri.search[i].field, filtri.search[i].value);
                    }
                }

                // init text field style
                this.tagsObj.input.style.border = '1px solid #9a9a9a';
                this.tagsObj.input.style.padding = '0px';
                this.tagsObj.input.style.margin = '1px 0px 0px 0px';
                this.tagsObj.input.style.height = '20px';
                this.tagsObj.input.style.boxShadow = 'none';

                this.tagsObj.add("+", -1, -1, "add", function () {
                    o.openList();
                });
                this.tagsObj.add("-", -1, -1, "remove", function () {
                    o.removeAll();
                });
                
            }

                
            }
    
            try {
                if (onload)
                    moLibrary.exec(onload, this);
                
            } catch (ex) {
                moDebug.log(ex);
            }
            this.setReadonlyStyle(); 
        }
       
    };
    this.setReadonlyStyle = function () {
        with (this) {
            try {
                var ron = readonly ? "none" : "";
                var nodes = document.getElementById(fieldid).children[0].children
                for(var i=0; i< nodes.length; i++) {
                    if (nodes[i].classList.length == 0) {
                        nodes[i].style.display = ron;
                    }
                    if (nodes[i].classList.contains("taggle_operation")) {
                        nodes[i].style.display = ron;
                    }
                    // Delete x
                    let butt = nodes[i].children[1];
                    if (typeof (butt) !== "undefined" && butt.type == "button") {
                        nodes[i].children[1].style.display = ron;
                    }
                }
            } catch (e) {
                //console.log(e);
            }
        }
    };

    this.remove = function (tag, code) {
        var nm = name.split("_");
        values = JSON.parse(this.value);
        values.labels.splice(values.labels.indexOf(tag), 1);
        values.codes.splice(values.codes.indexOf(code), 1);
        eval("mdl_" + nm[1] + "_edit").removeCrossData(code, this.table + (this.isparent ? '_parent' : ''), this.ftype, this.picktype);
        //this.setProperties({value: JSON.stringify({codes: codes, labels: labels}), description: JSON.stringify({codes: codes, labels: labels})});
        if (values.codes.length == 0) {
            this.value = this.description = "";
        } else {
            this.value = this.description = JSON.stringify(values);
        }
        o = this;
        if (this.value == "") {
            try {
                if (o.onclear)
                    moLibrary.exec(o.onclear, o);
            } catch (ex) {
                moDebug.log(ex);
            }
        } else {
            try {
                if (o.onchange)
                    moLibrary.exec(o.onchange, o);
            } catch (ex) {
                moDebug.log(ex);
            }
        }
    }

    this.removeAll = function () {
        var nm = name.split("_");
        if (this.value != "") {
            values = JSON.parse(this.value);
            for (var i = 0; i < values.codes.length; i++) {
                eval("mdl_" + nm[1] + "_edit").removeCrossData(values.codes[i], this.table + (this.isparent ? '_parent' : ''), this.ftype, this.picktype);
            }
            this.tagsObj.removeAll();
            this.value = this.description = '';
            o = this;
            try {
                if (o.onclear)
                    moLibrary.exec(o.onclear, o);
            } catch (ex) {
                moDebug.log(ex);
            }
        }
        var modu = eval("mdl_" + nm[1] + "_edit");
        if (!modu.data[this.table + (this.isparent ? '_parent' : '')+"_"+this.ftype]) {
            modu.data[this.table + (this.isparent ? '_parent' : '')+"_"+this.ftype] = {
                    f_code : [],
                    f_pair : [],
                    f_type : this.ftype,
                    Ttable : this.table + (this.isparent ? '_parent' : '')
                };
        }
    }


    this.setField = function () {
        /*
         with(this) {
         try {
         //VECCHIO CODICE mf$(fieldid).value = (description ? description : value);
         mf$(fieldid).value = value;
         //eval(btnid).setProperties({disabled: readonly || disabled});                
         } catch(ex) {moDebug.log(ex);}
         }*/
        // load selection from db data
        var nm = name.split("_"), o = this;

        if (this.tagsObj != null) {
            this.tagsObj.removeAll();
            if (this.value != '' && this.value != null) {
                var values = JSON.parse(this.value);
                for (var i = 0; i < values.codes.length; i++) {
                    this.tagsObj.add(values.labels[i], i, values.codes[i]);
                    eval("mdl_" + nm[1] + "_edit").data[this.bind + '_code'] = values.codes[i];
                    eval("mdl_" + nm[1] + "_edit").addCrossData(values.codes[i], this.table + (this.isparent ? '_parent' : ''), this.ftype, this.picktype);
                }
                this.tagsObj.input.style.marginTop = '1px';
            }
            // no elements selected --> adjust picklist input field margin top 
            else{
                this.tagsObj.input.style.marginTop = '0px';
            }
        }

        
    };
    this.setStyle = function () {
        with(this) {
            this.setReadonlyStyle(); 
        }
        /*
         with(this) {
         var classes = [ 'picklist' ]; if(className) classes.push(className);
         if(!isReadonly() && !isDisabled() && !moGlb.isempty(value)) classes.push("cancel");
         if(mandatory) classes.push("mandatory");
         mf$(fieldid).setAttribute("class", classes.join(" "));
         }*/
    };
    this.reset = function (clear) {
        with(this) {
            this.setReadonlyStyle(); 
        }
        /*
         with(this) 
         {
         var nm = name.split("_");
         set('');if(!clear) batch = false;
         eval("mdl_"+nm[1]+"_edit").addCrossData('', table+(isparent ? '_parent' : ''), ftype, picktype);
         //setField();
         if(this.tagsObj != null){
         this.tagsObj.removeAll();
         }
         }*/
    };
    this.openList = function () {
        var c_mod;
        with (this) {
            var overlay = '', o = this;
            var mod = name.split('_');
            mod = mod[1];
            // get type id and use slc for all selector picklists
            if (picktype.indexOf('slc') != -1) {
                var split = picktype.split("_");
                c_mod = split[0];
            } else {
                c_mod = picktype;
            }
            try {
                overlay = eval("ov_" + c_mode);
            } catch (ex) {
                overlay = moComp.createComponent("ov_" + c_mod, 'moOverlay', {m_mod: mod, c_mod: c_mod, picklist: name});
            }

            overlay.picklist = name;
            if (moLibrary.exists(filters))
                moLibrary[filters](o);

            // filter type for selector treegrid popup
            if(picktype.indexOf('slc') != -1){
                mdl_slc_pp_tg_tg.PICKLIST_FTYPE = split[1];
            }

            // check if there are picklist filters and add them to treegrid
            if (this.BACKEND_SEARCH.length > 0) {
                eval('mdl_' + c_mod + '_pp_tg_tg').PICKLIST_SEARCH = BACKEND_SEARCH;
            } else {
                eval('mdl_' + c_mod + '_pp_tg_tg').PICKLIST_SEARCH = null;
            }

            // check if there are picklist filters and add them to treegrid
            if (this.BACKEND_SELECTOR.picklist_selector.s.length > 0) {
                eval('mdl_' + c_mod + '_pp_tg_tg').PICKLIST_SELECTOR = BACKEND_SELECTOR;
            } else {
                eval('mdl_' + c_mod + '_pp_tg_tg').PICKLIST_SELECTOR = null;
            }

            eval('mdl_' + c_mod + '_pp_tg_tg').FROM_PICKLIST = true;

            overlay.show();                        
        }
    };
    this.setResult = function (res) {
        with (this) {
            if (!(isbatch && !batch)) {
            var nm = name.split("_"), clabel = column, o = this;
            var values = {
                codes: [],
                labels: []
            };
            var oldValues = "";
            try {
            // single mode, remove previous selection
            if (this.selection_type == undefined || this.selection_type == 'single') {
                if (this.value != "" && this.value != null) {
                    oldValues = JSON.parse(this.value);
                }
                this.tagsObj.removeAll()
            }
            //  multiple mode, load previous selections
            else {
                if (this.value != "" && this.value != null) {
                    values = JSON.parse(this.value);
                }
            }
            // NICO: 08/10/2018 Remove old values
            if (oldValues !== "") {
                for (var v in oldValues.codes) {
                    eval("mdl_" + nm[1] + "_edit").removeCrossData(oldValues.codes[v],table + (isparent ? '_parent' : ''), ftype, picktype);
                }
            }
        }catch (e) {
            console.log("Invalid value");
        }
            // loop through selected items
            for (var i = 0; i < res.length; i++) {
                eval("mdl_" + nm[1] + "_edit").data[bind + '_code'] = res[i].f_code;
                eval("mdl_" + nm[1] + "_edit").addCrossData(res[i].f_code, table + (isparent ? '_parent' : ''), ftype, picktype);
                if (!nome_utente_change_flag) {
                    eval("mdl_" + nm[1] + "_edit").addCustomData('utente_ex', res[i].f_code);
                    nome_utente_change_flag = true;
                }
                result = res[i];
                // check if selected element is already in list
                if (values.codes.indexOf(result.f_code) == -1) {
                    for (var j in result) {
                        clabel = column;
                        clabel = clabel.replace('{' + j + '}', moGlb.langTranslate(result[j]));
                        if (clabel != column) {
                            values.labels.push(clabel);
                            values.codes.push(res[i].f_code);
                            this.tagsObj.add(clabel, values.codes.length + 1, res[i].f_code);
                        }
                    }
                }

            }
            //this.setProperties({value: JSON.stringify(values), description: JSON.stringify(values)});
            this.value = this.description = JSON.stringify(values);
            if (onchange)
                moLibrary.exec(onchange, o);
            }
            this.setReadonlyStyle(); 
        }
    };
    this.getResult = function () {
        return result;
    };
};
moPicklist.prototype = new moUI();