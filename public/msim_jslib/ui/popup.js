/*
 * Popup
 */
var moPopup = function (name) {
    this.name = '', this.uiname = '', this.title = '', this.contentid = '',
    this.animate = false, this.shadow = false, this.cssClass = 'win',
    this.mask = true, this.transparentMask = false, this.maskId = '',
    this.width = 0, this.height = 0, this.left = undefined, this.top = undefined, this.active = false,
    this.zindex = 0, this.bdWidth = 2, this.hdH = 28, this.onload = '', this.onunload = '', this.onresize = '';
    this.baseCreate = function (props) {moGlb.mergeProperties(this, (moGlb.isset(props)?props:{}));with(this) {uiname = contentid+"_win";maskId = contentid+"_mask";draw();}};
    this.create = function (props) { this.baseCreate(props); };
    this.set = function(param, value) {this[param] = value;this.refresh();};
    this.get = function(param) {if(moGlb.isset(this[param])) return this[param];return moGlb.alert.show(moGlb.langTranslate("Can't find property ")+param);};
    this.draw = function (redraw) {with(this) {try {drawMask();drawWindow();drawHeader();drawBody();if(!redraw) display(false);} catch (ex) {moDebug.log("Problems in creating popup: "+ex);}}};
    this.drawMask = function () {
        with(this) {
            try {
                var msk = moGlb.createElement(maskId, 'div');
                msk.setAttribute("class", 'mask'+(transparentMask?' transparent':''));
                msk.style.zIndex = (zindex?zindex:moGlb.zpopup)-5;
            } catch(ex) {moDebug.log(moGlb.langTranslate("Problems in creating popup mask for ")+name+": "+ex);}
        }
    };
    this.drawWindow = function () {
        with(this) {
            try {
                var d = moGlb.createElement(uiname, "div");
                d.style.zIndex = (zindex?zindex:moGlb.zpopup);d.setAttribute("class", cssClass+(shadow ? " shadow": ""));
                setPosition();
            } catch(ex) {moDebug.log(moGlb.langTranslate("Problems in creating popup window for ")+name+": "+ex);}
        }
    };
    this.drawHeader = function () {
        with(this) {
            try {
                var hd = moGlb.createElement(uiname+"_hd", "div", mf$(uiname));hd.setAttribute("class", "hd");
                hd.innerHTML = "<div class='win_tlt'>"+moGlb.langTranslate(title)+"</div><div class='btn' onclick='"+name+".hide();'>"+moCnvUtils.IconFont(58827, 16, "#ffffff")+"</div>";
            } catch(ex) {moDebug.log(moGlb.langTranslate("Problems in creating popup header for ")+name+": "+ex);}
        }
    };
    this.drawBody = function () {
        with(this) {
            try {
                with(moGlb.createElement(uiname+"_bd", "div", mf$(uiname))) {
                    if(mf$(contentid)) appendChild(mf$(contentid));setAttribute("class", "bd");
                }
            } catch(ex) {moDebug.log(moGlb.langTranslate("Problems in creating popup body for ")+name+": "+ex);}
        }
    };
    this.setTitle = function (tlt) {with(this) { getHeader().getElementsByClassName('win_tlt')[0].innerHTML = tlt; }};
    this.getHeader = function () {return mf$(this.uiname+"_hd")};
    this.getBody = function () {return mf$(this.uiname+"_bd")};
    this.refresh = function () {this.redraw(true);};
    this.show = function () {with(this) { setPosition(); display(true); if(onload) onload(); try { eval(contentid).display(true, getContainerId());} catch(ex) {} }};
    this.hide = function () { with(this) { display(false); if(onunload) onunload(); } };
    this.display = function (on) {with(this) { setPosition(); active = on; mf$(uiname).style.display = (on?'block':'none');displayMask(on);}};    
//    this.setSize = function () { this.width = moGlb.getClientWidth()-80; this.height = moGlb.getClientHeight()-135; };
//    this.resize = function () {
//        with(this) {
//            var o = this; setSize(); draw(true);
//            if(active) eval(contentid).resize();
//            if(onresize) moLibrary.exec(onresize, o);
//        }
//    };
    this.isVisible = function (on) {with(this) {return (mf$(uiname).style.display == 'block');}};
    this.getContainerId = function () {return this.getBody().id;};
    this.setPosition = function () {
        with(this) {
            var content_size = {};
            if(mf$(contentid)) content_size = moGlb.getPosition(mf$(contentid));
            if(width) content_size.w = width;if(height) content_size.h = height;
            content_size.t = parseInt(moGlb.isset(top)?top:(moGlb.getClientHeight()-content_size.h)/2);
            content_size.l = parseInt(moGlb.isset(left)?left:(moGlb.getClientWidth()-content_size.w)/2);
            with(mf$(uiname).style) {
                width = (content_size.w+(bdWidth*2))+'px', height = (content_size.h+(bdWidth*2)+hdH)+'px',
                top = content_size.t+'px', left = content_size.l+'px';
            }
        }
    };
    this.displayMask = function (on) {with(this) {try {mf$(maskId).style.display = (mask&&on?'block':'none');} catch(ex) {moDebug.log(ex);}}};
    this.enableClose = function (enable) { with(this) { mf$(uiname+"_hd").getElementsByClassName('btn')[0].style.display = (enable ? 'block' : 'none'); } };
    this.name = name;    
};

/*
 * Loader
 */
var moLoader = function () {
    this.contentid = 'popupLoader', this.hdH = 0, this.bdWidth = 0, this.cssClass = 'loader', this.zindex = 1700,
    this.text = 'Operation in progress. Please wait', this.closeAfter = 0, this.height = 180, this.left = 0,
    this.show = function (txt) {
        with(this) {
            setText(txt); display(true);
            if(closeAfter) setTimeout(name+'.hide()', closeAfter * 1000);
        }
    };
    this.setText = function (txt) { with(this) { mf$(contentid).getElementsByClassName('text')[0].innerHTML = moGlb.langTranslate(moGlb.isset(txt)?txt:text); } };
    this.resize = function () { with(this) { width = moGlb.getClientWidth(); setPosition(); } };
    with(this) {
        width = moGlb.getClientWidth();
        with(moGlb.createElement(contentid, 'div')) {
            innerHTML = "<div class='text'></div>";
        }
    }
};
moLoader.prototype = new moPopup();

/*
 * Offline
 */
var moOffline = function (name) {
    this.contentid = 'offline', this.bdWidth = 0, this.cssClass = 'offline', this.zindex = 1700,    
    this.closeAfter = 0, this.height = 180, this.left = 0, this.zindex = 10000, this.title = 'Alert';
    this.resize = function () { with(this) { width = moGlb.getClientWidth(); setPosition(); } };
    with(this) {
        width = moGlb.getClientWidth();
        with(moGlb.createElement(contentid, 'div')) {
            innerHTML = "<div class='title'><span class='alert'>"+String.fromCharCode(58540)+"</span>"+
                "<span>"+moGlb.langTranslate("You are currently offline")+"</span></div>"+
                "<div class='text'>"+moGlb.langTranslate("<b>main</b>sim can't estabilish a connection to the server.")+"<br/>"+
                moGlb.langTranslate("Attempting to restore network connection. <b>main</b>sim will resume")+"<br/>"+
                moGlb.langTranslate("once the connection has been re-estabilished. Please wait.")+
            "</div>";
        }
    }
    this.name = name;
};
moOffline.prototype = new moPopup();

/*
 * Alert
 */
var moAlert = function (name) {
    this.contentid = 'popupAlert', this.zindex = 1700, this.text = "Generic warning!",
    this.width = 315, this.height = 125, this.title = moGlb.langTranslate("Warning!");
    this.show = function (txt) { with(this) { setText(txt); display(true); mf$(contentid+"_btnok").focus(); } };
    this.setText = function (txt) {
        with(this) {
            var el = mf$(contentid).getElementsByClassName('text')[0];
            el.innerHTML = moGlb.langTranslate(moGlb.isset(txt)?txt:moGlb.langTranslate(text));
            moGlb.verticalAlign(mf$(contentid));
        }
    };
    this.resize = function () { this.setPosition(); };
    with(this) {
        with(moGlb.createElement(contentid, 'div')) {
            setAttribute("class", "alert");
            innerHTML = "<div class='text'></div>"+
                "<div class='submit'>"+
                    "<button id='"+contentid+"_btnok' onclick='moGlb.alert.hide();'>"+moGlb.langTranslate("Ok")+"</button>"+
                "</div>";
        }        
    }
    this.name = name;
};
moAlert.prototype = new moPopup();

/*
 * Confirm
 */
var moConfirm = function (name) {
    this.contentid = 'popupConfirm', this.zindex = 1700, this.text = moGlb.langTranslate("Warning!"),
    this.width = 315, this.height = 125, this.title = "Confirm requested", this.onconfirm = '', this.oncancel = '', caller = '';
    this.show = function (txt, ok, canc, tlt) {
        with(this) {
            setText(txt); setTitle(moGlb.isset(tlt)?tlt:moGlb.langTranslate(title)); display(true);
            onconfirm = (moGlb.isset(ok)?ok:''); //caller = (moGlb.isset(o)?o:'');
            oncancel = (moGlb.isset(canc)?canc:'');
            mf$(contentid+"_btncancel").focus();
            document.getElementById(contentid+"_btnok").value = moGlb.langTranslate("Ok");
            document.getElementById(contentid+"_btncancel").value = moGlb.langTranslate("Cancel");
        }
    };
    this.setText = function (txt) { with(this) { mf$(contentid).getElementsByClassName('text')[0].innerHTML = moGlb.langTranslate(moGlb.isset(txt)?txt:moGlb.langTranslate(text)); } };
    this.confirm = function () { with(this) { if(onconfirm) onconfirm(); hide(); } };
    this.cancel = function () { with(this) { if(oncancel) oncancel(); hide(); } };
    this.resize = function () { this.setPosition(); };
    with(this) {
        with(moGlb.createElement(contentid, 'div')) {
            setAttribute("class", "confirm");
            innerHTML = "<div class='text'></div>"+
                    "<div class='submit'>"+
                        "<input id='"+contentid+"_btnok' class='ok' type='button' value='' onclick='moGlb.confirm.confirm();'/>"+
                        "<input id='"+contentid+"_btncancel' class='cancel' type='button' value='' onclick='moGlb.confirm.cancel();'/>"+
                    "</div>";
        }
    }
    this.name = name;
};
moConfirm.prototype = new moPopup();


var moOverlay = function (name) {
    this.title = /*moGlb.langTranslate(*/'Select items to add'/*)*/, this.shadow = false, this.top = 60, this.cssClass = 'win overlay',
    this.m_mod = '', this.c_mod = '', this.filter = '', this.onselect = '', this.picklist = '', this.first = true;
    var layouts = {
        summary: '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":0,"wh":1288,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":1,"ov":0,"tp":0,"wh":389,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":1,"ov":0,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_pp_cancel","acc":0,"lock":1,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":243,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":1045,"level":-1,"module":"mdl_::module::_pp_crud","acc":0,"lock":0,"minv":36},{"idp":4,"ov":0,"tp":1,"wh":114,"level":-1,"module":"mdl_::module::_pp_summary_inbox","acc":0,"lock":0,"minv":36},{"idp":4,"ov":0,"tp":0,"wh":151,"level":-1,"module":"mdl_::module::_pp_summary","acc":0,"lock":0,"minv":36},{"idp":4,"ov":0,"module":"::selector1::","tp":0,"wh":168,"level":-1,"acc":0,"lock":0,"minv":36}]}', //I-SM-SL-C
        slc2: '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":0,"wh":1288,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":1,"ov":0,"tp":0,"wh":389,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":1,"ov":0,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_pp_cancel","acc":0,"lock":1,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":240,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":1048,"level":-1,"module":"mdl_::module::_pp_crud","acc":0,"lock":0,"minv":36},{"idp":4,"ov":0,"tp":0,"wh":187,"level":-1,"module":"::selector1::","acc":0,"lock":0,"minv":36},{"idp":4,"ov":0,"tp":0,"wh":202,"level":-1,"module":"::selector2::","acc":0,"lock":0,"minv":36}]}', //SL-SL-C
        slc1: '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":0,"wh":1288,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":1,"ov":0,"tp":0,"wh":389,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":1,"ov":0,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_pp_cancel","acc":0,"lock":1,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":236,"level":-1,"module":"::selector1::","acc":0,"lock":0,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":1052,"level":-1,"module":"mdl_::module::_pp_crud","acc":0,"lock":0,"minv":36}]}', //SL-C
        none: '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":0,"wh":1288,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":1,"ov":0,"tp":0,"wh":389,"level":-1,"module":"mdl_::module::_pp_crud","acc":0,"lock":0,"minv":36},{"idp":1,"ov":0,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_pp_cancel","acc":0,"lock":1,"minv":36}]}', //C
        slc3: '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":0,"wh":1288,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":1,"ov":0,"tp":0,"wh":389,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":1,"ov":0,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_pp_cancel","acc":0,"lock":1,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":273,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":1015,"level":-1,"module":"mdl_::module::_pp_crud","acc":0,"lock":0,"minv":36},{"idp":4,"ov":0,"tp":0,"wh":112,"level":-1,"module":"::selector1::","acc":0,"lock":0,"minv":36},{"idp":4,"ov":0,"tp":0,"wh":136,"level":-1,"module":"::selector2::","acc":0,"lock":0,"minv":36},{"idp":4,"ov":0,"module":"::selector3::","tp":0,"wh":141,"level":-1,"acc":0,"lock":0,"minv":36}]}', //SL-SL-SL-C
        //crud: '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":0,"wh":1288,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":1,"ov":0,"tp":1,"wh":34,"level":-1,"module":"mdl_::module::_pp_cb","acc":0,"lock":1,"minv":25},{"idp":1,"ov":0,"tp":0,"wh":389,"level":-1,"module":"mdl_::module::_pp_tg","acc":0,"lock":0,"minv":36}]}',
        crud: '{"layout":[{"idp":-1,"ov":1,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":0,"tp":1,"wh":36,"level":-1,"module":"mdl_::module::_cb","acc":0,"lock":1,"minv":36},{"idp":0,"ov":0,"tp":1,"wh":393,"level":-1,"module":"mdl_::module::_tg","acc":0,"lock":0,"minv":36}]}',
        //timeline: '{"layout":[{"idp":-1,"ov":1,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":0,"tp":1,"wh":34,"level":-1,"module":"mdl_::module::_pp_cb","acc":0,"lock":1,"minv":25},{"idp":0,"ov":0,"module":"","tp":0,"wh":391,"level":-1,"acc":0,"lock":0,"minv":36},{"idp":2,"ov":1,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_pp_tl","acc":0,"lock":0,"minv":36},{"idp":2,"ov":1,"module":"mdl_::module::_pp_tg","tp":0,"wh":1252,"level":-1,"acc":0,"lock":0,"minv":36}]}'        
        timeline: '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"module":"","tp":0,"wh":1219,"level":-1,"acc":0,"lock":0,"minv":36},{"idp":0,"ov":1,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_tl","acc":1,"lock":0,"minv":36},{"idp":1,"ov":0,"tp":1,"wh":36,"level":-1,"module":"mdl_::module::_cb","acc":0,"lock":1,"minv":36},{"idp":1,"ov":0,"tp":0,"wh":393,"level":-1,"module":"mdl_::module::_tg","acc":0,"lock":0,"minv":36}]}'
    };
    this.create = function (props) {
        with(this) {
            var mod = (props.c_mod.indexOf('parent') == 0?props.m_mod:props.c_mod.split("$")[0]);
            contentid = 'lyt_'+mod+'_pp', width = moGlb.getClientWidth()-80, height = moGlb.getClientHeight()-135;
            baseCreate(props);//createUi(mod);
            try { eval(contentid); }
            catch(ex) {
                moGlb.loader.show();
                try{createUi(mod);}catch(ex) {}
                moGlb.loader.hide();
            }
        }
    };        
    this.getLayoutStruct = function (lyt) {
        var l = eval(lyt).lyt.layout, mods = [], slc = 0, smr = 0;//, listenTo = [];
        for(var i in l) {
            if(moGlb.isset(l[i].module) && l[i].module) {
                mods.push(l[i].module);
                //if(l[i].module.indexOf('crud') < 0) listenTo.push(l[i].module);
                if(l[i].module.indexOf('selector') > 0) slc++;
                else if(l[i].module.indexOf('inbox') > 0) smr++;
            }
        }
        return { type: (smr?'summary':(slc?'slc'+slc:'none')), mods: mods/*, listenTo: listenTo.join(',')*/ };
    };
    this.hideEdit = function () {};
    this.select = function () {
        with(this) {
            var mod = (c_mod.indexOf('parent') == 0?m_mod:c_mod.split("$")[0]), tg = 'mdl_'+m_mod+'_'+c_mod+'_tg', pp_tg = 'mdl_'+mod+'_pp_tg', o = this;
            var codes = eval(pp_tg).getSelected();
            if(picklist){
                var picklistObj = eval(picklist);
                if((picklistObj.selection_type == undefined || picklistObj.selection_type == 'single') && codes.length > 1){
                    moGlb.alert.show(moGlb.langTranslate("Select only one item"));
                }
                else if(codes.length == 0){
                    moGlb.alert.show(moGlb.langTranslate("Select one or more items"));
                }
                else{
                    picklistObj.setResult(codes);
                    // Controllo se sono nel multiedit e la spunta è selezionata
                    if (!(picklistObj.isbatch && !picklistObj.batch)) {
                        try { eval(tg).setSelected(codes, true); } catch(ex) {
                            if (picklistObj.table == "t_selectors") {
                                try {
                                    eval("mdl_"+m_mod+"_slc_tg").setSelected(codes, true);
                                } catch (e) {
                                }
                            }
                        }
                    }
                    picklist = '';
                    hide();
                }
            }
            else{
                if(codes.length > 0) {
                    try { eval(tg).setSelected(codes, true); } catch(ex) {}
                    hide();
                } else moGlb.alert.show(moGlb.langTranslate("Select one or more items"));
            }
            if(onselect) moLibrary.exec(onselect, o);
        }
    };
    this.show = function (editingCode,n_m_mod,n_c_mod) {
        with(this) {

            var filters = null;

            if(n_m_mod == undefined) n_m_mod = m_mod;
            m_mod = n_m_mod;
            if(n_c_mod == undefined) n_c_mod = c_mod;
            c_mod = n_c_mod;

            
//          try {
//                var mod = (c_mod=='parent'?m_mod:c_mod), tg = 'mdl_'+m_mod+'_'+c_mod+'_tg', pp_tg = 'mdl_'+mod+'_pp_tg';
//                eval(eval(pp_tg).gridName).setData({data: {
//                    Filter: [{f: "f_type_id", v: eval(tg).ftype, ty: 4}]
//                }});
                eval('mdl_'+(c_mod == 'parent' ? m_mod : c_mod)+'_pp_tg').setEditingCode(editingCode);
                display(true);
                eval(contentid).display(true, getContainerId());
                // Controllo se la picklist è già stata aperta una volta.
                // La refresh deve essere eseguita solo dalla seconda volta in poi
                if (eval('mdl_'+(c_mod == 'parent' ? m_mod : c_mod)+'_pp_tg').firsttime)
                    eval('mdl_'+(c_mod == 'parent' ? m_mod : c_mod)+'_pp_tg').refresh();
                eval('mdl_'+(c_mod == 'parent' ? m_mod : c_mod)+'_pp_tg').firsttime = true;
                onload();
//            } catch(ex) { console.log(ex); }
        }
    };
    //this.onload = function () { with(this) { with(eval(contentid)) { for(var i in LY) if(LY[i].module && eval(LY[i].module).type == "moModuleCrud") eval(eval(LY[i].module).treeGrid).refresh(); } } };
    this.onload = function () {};
    this.close = function () { with(this) { 
        if(onunload) moLibrary.exec(onunload, o); 
        if(picklist != ''){
            var tg = eval('mdl_' + (c_mod == 'parent' ? m_mod : c_mod) + '_pp_tg_tg');
            tg.FROM_PICKLIST = false;
            tg.PICKLIST_SEARCH = null;
            tg.PICKLIST_SELECTOR = null;
            tg.PICKLIST_FTYPE = null;
            picklist = '';
        }
         hide(); 
    } };
    this.hide = function () {
        with(this) {
          /*  var pp_tg = 'mdl_'+(c_mod.indexOf('parent') == 0?m_mod:c_mod.split("$")[0])+'_pp_tg';
            with(eval(pp_tg)) { try { closeEdit(); reset(); } catch(ex) {} }
            */
            display(false); eval(contentid).display(false);
        }
    };
    this.setSize = function () { this.width = moGlb.getClientWidth()-80; this.height = moGlb.getClientHeight()-135; };
    this.resize = function () {
        with(this) {
            var o = this; setSize(); draw(true);
            eval(contentid).resize();
            if(onresize) moLibrary.exec(onresize, o);
        }
    };
    this.createUi = function (mod) {
        with(this) {
            var struct = getLayoutStruct('lyt_'+mod), rep = {}, slc_n = 1, tl = (eval('lyt_'+mod+'_crud').lyt.layout[3].module.indexOf('_tl') > 0);
            rep[mod+'_'] = mod+'_pp_'; rep['::module::'] = mod;
            
            // Menu
            eval('mn_'+mod+'_pp_inspect = {menu:[]}');
            
            // Buttons
            moComp.createComponent('btn_'+mod+'_pp_all', 'moButton', {label: "All", iconon: "FlaticonsStroke.58344", action: 'mdl_'+mod+'_pp_tg.reset();', visible: 1, level: -1});
            moComp.createComponent('btn_'+mod+'_pp_inspect', 'moButton', {label: "Views", iconon: "FlaticonsStroke.58461", menu: 'mn_'+mod+'_pp_inspect', visible: 1, level: -1});            
            
            // Modules            
            for(var i in struct.mods) { // Creating external modules
                var mdl = eval(struct.mods[i]), mdl_name = struct.mods[i].replace(mod, mod+'_pp');
                moComp.createComponent(mdl_name, mdl.type, JSON.parse(mdl.json.multiReplace(rep)));
                if(mdl.type == 'moModuleSelector') { rep['::selector'+slc_n+'::'] = mdl_name; slc_n++; }
            }
            // Layouts
            moComp.createComponent('lyt_'+mod+'_pp', 'moLayout', JSON.parse(layouts[struct.type].multiReplace(rep))); // Base layout
            moComp.createComponent('lyt_'+mod+'_pp_crud', 'moLayout', JSON.parse(layouts[tl ? 'timeline' : 'crud'].multiReplace({'::module::': mod+'_pp'}))); // Crud layout
            //
            // Command bar            
            with(moComp.createComponent('mdl_'+mod+'_pp_cb', 'moModuleCommandBar', JSON.parse(eval('mdl_'+mod+'_cb').json.multiReplace(rep)))) {
                elements = 'btn_'+mod+'_pp_all,btn_'+mod+'_pp_inspect';
            }            
            // Time line            
            if(tl) with(moComp.createComponent('mdl_'+mod+'_pp_tl', 'moModuleTimeLine', JSON.parse(eval('mdl_'+mod+'_tl').json.multiReplace(rep)))) {
                tgName = 'mdl_'+mod+'_pp_tg';
            }
            // Tree grid            
            with(moComp.createComponent('mdl_'+mod+'_pp_tg', 'moModuleTreeGrid', JSON.parse(eval('mdl_'+mod+'_tg').json))) {
                if(listenTo.length > 0)for(var i in listenTo) listenTo[i] = listenTo[i].multiReplace(rep);   
            }
            
            // Creating bottom command bar
            moComp.createComponent('mdl_'+mod+'_pp_cancel', 'moModuleCommandBar', { type: "moModuleCommandBar", noheader: 1, noframe: 1, noshadow: 1, elements: "btn_"+mod+"_pp_cancel|btn_"+mod+"_pp_select", minsz: 25, noscroll: 1, margin: {t:4, l: 4, r: 4, b: 0}, padding:0 });
            moComp.createComponent('btn_'+mod+'_pp_cancel', 'moButton', {label: "Cancel", iconon: "FlaticonsStroke.58827", action: 'ov_'+mod+'.close();', visible: 1, level: -1});
            moComp.createComponent('btn_'+mod+'_pp_select', 'moButton', {label: "Select", iconon: "FlaticonsStroke.58826", action: 'ov_'+mod+'.select();', visible: 1, level: -1});
        }
    };
    this.name = name;
};
moOverlay.prototype = new moPopup();