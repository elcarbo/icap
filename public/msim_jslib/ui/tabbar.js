// tabbar new

function moTabbar(name){
    this.Name=name, this.Elm=[], this.bkElm=[], this.tabL = 0, this.tabB = 0, this.tabW = 40, this.tabH = 28, this.tabMarg = 1, this.tabPadd = 10, this.Lbtn = 28,
    this.Container = "", this.lContainer = "", this.Ov = -1, this.Sl = 0, this.Fnc = "", this.pname = "";
    this.Par=[], this.vIs = {}; this.NvIs=0;  this.oNvIs=0; this.RFRSH=0;
    this.Font = "14px opensansR";
 
  this.Colors={
    fgTabTxtSel: "#202020", fgTabTxtNosel: "#A0A0A0",  fgTabTxtOver: "#FFF", fgTabTxtDisable:'#F0F0F0',fgTabTxtElem:'#000',
    bgTabSel: "#FFF", bgTabNosel: "#E8E8E8", bgTabOver: "#A1C24D"
  };
    
this.yPar=[];
this.Btn=0;
this.VisMenu=0;
this.mOv=-1;
this.Mpos=[];

// metodi
this.draw=draw;
this.drawtab=drawtab;
this.resize=resize;
this.hfont=hfont;
this.showmenu=showmenu;
this.hidemenu=hidemenu;
this.outime=outime;
this.mdraw=mdraw;
this.display=display; 
this.hide=hide;
this.getLayoutList=getLayoutList;
this.callLayout=callLayout;
this.f_cmp=f_cmp;
this.setVisibility=setVisibility; 
this.reset=reset;
               
//eventi
this.move=move;
this.out=out;
this.up=up;
this.Lmove=Lmove;
this.Lout=Lout;
this.Lup=Lup;     

this.GetCountElem=GetCountElem;
this.f_refresh=f_refresh;

    
this.create = function (Prop) {
  for(var i in Prop) this[i] = Prop[i];
}

function reset(){
with(this){
  RFRSH=0;
  NvIs=0; oNvIs=0; Sl=0;  
  callLayout();
  display("","",1);
  draw();
}}



function setVisibility(vis){
with(this){
  vIs=vis; 
  NvIs++; 
  for(var i in bkElm) {
      eval(bkElm[i].layout).display(false, lContainer);
  }  
  display("","",1);
  RFRSH=30;
  f_refresh();
}}


function hide(hide) {
    mf$("tab"+name).style.display = (hide ? "none" : "block");
}


function display(cId, lcId, m) {    // vIs = { category:"", selector:"", wf:"" }
with(this){

  if(m){
    if(!Container) return; 
  } else { 
    Container = cId, lContainer = lcId;
    if(isTouch) Lbtn=32;
  }  
    
    // create container for menu and hidden
    var j=null;
    if(!mf$("listabhidden"+Name)) {
      j=moGlb.createElement("listabhidden"+Name,"canvas");
      j.style.display="none";
      j.style.zIndex=moGlb.zmenu
    } else {
      j=mf$("listabhidden"+Name);
    }
    this.mtx=j.getContext('2d'); 
    this.Lj=j;

    if(!isTouch){
      j.onmousemove=function(e){eval(name).Lmove(e);}
      j.onmouseout=function(e){eval(name).Lout(e);}   
      j.onmouseup=function(e){eval(name).Lup(e);}   
    } else {
      j.onclick=function(e){eval(name).Lup(e);}
    }

    // create canvas
    var pdr=null; if(Container) pdr=mf$(Container);
    j=moGlb.createElement("tab"+name,"canvas",pdr);         
    
 
  
  /*--- check visibility of tabs vIs ------------------------------------
  
  //  criteria
  Elm[i].v_type="";         // all category
  Elm[i].v_selector="";     // all selector cross
  Elm[i].v_wf="";           // all workflows and phases
  
  vIs={
   category:""           "1,3,4"         type list
   selector:"",          "15,32,64,..."  f_code list
   wf:""                 "3_2,4_1,.."    wf_phase,...
  }
  */  

  if(!bkElm.length) bkElm=moArray.clone(Elm); 
  else Elm=moArray.clone(bkElm); 
  
  if(!NvIs){  // reset
      Elm=[];
      Elm.push(bkElm[0]);
      
  } else {
  
      var i, adel=[], Ccat, Csel, Cwfp;
      
      if(!vIs) vIs={};
      Ccat=vIs.category;
      Csel=vIs.selector;
      Cwfp=vIs.wf;
      
      if(!Ccat) Ccat=[]; else Ccat=(Ccat.multiReplace({" ":""})).split(",");
      if(!Csel) Csel=[]; else Csel=(Csel.multiReplace({" ":""})).split(",");
      if(!Cwfp) Cwfp=[]; else Cwfp=(Cwfp.multiReplace({" ":""})).split(",");
    
      for(i=0;i<Elm.length;i++) {    
      
        // userLevel
        if(!(moGlb.user.level & Elm[i].level)) { adel.push(i); continue; }  
        
        // v_type  1,3,4
        if(Elm[i].v_type) if(!f_cmp(Elm[i].v_type,Ccat)) { adel.push(i); continue; }  
        
        // v_selector = string of list es. 15,32,64,... 
        if(Elm[i].v_selector) if(!f_cmp(Elm[i].v_selector,Csel)) { adel.push(i); continue; }    
         
        // v_wf  3_2,4_1,... 
        if(Elm[i].v_wf) if(!f_cmp(Elm[i].v_wf,Cwfp)) { adel.push(i); continue; }   
        
      } //
      
      if(adel.length){
        for(i=(Elm.length-1);i>=0;i--){
        if(adel.indexOf(i)>-1) Elm.splice(i,1); 
      }}  
   }
   
  //----------------------------------------------------------------

  this.No=Elm.length;
  this.Obj=j;
  this.ctx=j.getContext('2d');
        
    var hF=moGlb.getFontHeight(hfont(Font));
    this.tF=parseInt((tabH-hF)/2)+hF;  
    
    if(!isTouch){
      j.onmousemove=function(e){eval(name).move(e);}
      j.onmouseout=function(e){eval(name).out(e);}
      j.onmouseup=function(e){eval(name).up(e);}
    }else{
      j.onclick=function(e){eval(name).up(e);}
    }  

  if(pname && moGlb.isset(eval(eval(this.pname).pname)) && !eval(eval(this.pname).pname).pinned) Sl=0;
  callLayout();
    
  oNvIs=NvIs;
  } //
}


function f_cmp(s1,a2){

  if(!a2.length) return false;
  var r,g={}, s1=(s1.multiReplace({" ":""})).split(",");
  
  for(r=0;r<s1.length;r++) {
    if(a2.indexOf(s1[r])>-1) return true; 
  }  
  return false;
}


function callLayout(){
with(this){

  for(var i in Elm) {
//    if(!Elm[i].disable && (Elm[i].level & moGlb.user.level)) {
      eval(Elm[i].layout).display(Sl==i, lContainer);
//    }
  }  
}} 


function hfont(s){
  var n="",v;
  for(var r=0;r<s.length;r++){v=s.charCodeAt(r); 
  if(v>47 && v<58) n+=(v-48);
  }
  return parseInt(n);
} //


function getLayoutList(all) {
with(this) {
  var list = [];
  if(all) for(var i in bkElm) list.push(bkElm[i].layout);
  else for(var i in Elm) list.push(Elm[i].layout);
  return list;
}}


function resize(){
with(this){
  var ww = moGlb.getPosition(mf$(Container)).w;
  tabW=ww;
  if(tabW<40) tabW=40;  
  
  with(Obj.style) left=tabL+"px", bottom=tabB+"px";
  Obj.width=tabW; Obj.height=tabH; 
  
  var p=moGlb.getPosition(Obj);
  this.xo=p.l;
  this.yo=p.t; 
  draw();
}}


function draw(){  
with(this){                                        
  ctx.clearRect(0,0,tabW,tabH);
  Par=[], Vs=1;  // 0 no showed
   
  for(var r=0;r<No;r++){
    var tx=moGlb.langTranslate(Elm[r].label), w=moCnvUtils.measureText(tx,Font)+tabPadd*2;   // text width
    if(r==Sl) wSl=w;
    Par[r]={W:w, Tx:tx, Xi:0, Xf:0, Vis:0};
  } //  
  Par.push( {W:Lbtn, Tx:"", Xi:0, Xf:Lbtn, Vis:0} );
  
  var x=13, Slh=0, xw=0;
  
  // tabs to show
  for(r=0;r<No;r++){
      with(Par[r]) Xi=x, w=W, xw=x+w, Xf=xw, Vis=1;
  if(xw>tabW) {Par[No].Vis=1; 
    for(var n=r;n>=0;n--) with(Par[n]){ 
    Vis=0;  
    if(n!=Sl) {
      if( Slh && (Xi+Lbtn)<tabW )  {x=Xi;break;}  
      if(!Slh && (Xi+wSl+tabMarg+Lbtn)<tabW ) {Par[Sl].Vis=1;Par[Sl].Xi=Xi, x=Xi+wSl, Par[Sl].Xf=x;x+=tabMarg;break;}
    } else Slh=0;
    
    if(n==1 && (wSl+Lbtn+2+tabMarg)>tabW) {x=13; Par[Sl].Vis=0; Par[0].Vis=0; break;}        
    } // for n
  Par[No].Xi=x, Par[No].Xf=x+Lbtn;
  break;
  }
  if(r==Sl) Slh=1;
  x=xw+tabMarg;
  } // 
  
  
  ctx.fillStyle=Colors.bgTabNosel;  
  moCnvUtils.roundRect(ctx,0,0,12,tabH,2);
  ctx.fill();
  
  // draw Par
  for(var r=0;r<=No;r++)with(Par[r]){
  if(Vis) drawtab(r,Xi,W,Tx);
}
 
}}


function f_refresh(){
with(this){
  draw();
  // console.log(RFRSH+" | refresh")
  RFRSH--;
  if(RFRSH>0) setTimeout(name+".f_refresh()",400 );
}}


function drawtab(r,x,w,tx){
with(this){
  b=1, h=tabH;                                                              
// canvas TAB
with(this.Colors){
  var bgCol=bgTabNosel, txCol=GetCountElem(r), dsb=1;
  
  if(!Elm[r]) dsb=0;else {if(!Elm[r].disable) dsb=0;}
  if(dsb) {txCol = fgTabTxtDisable;}
  
  if(r==Ov) bgCol=bgTabOver, txCol=fgTabTxtOver;
  if(r==Sl) bgCol=bgTabSel,  txCol=fgTabTxtSel;
}

with(ctx){    
  fillStyle=bgCol;  
  moCnvUtils.roundRect(ctx,x,0,w,h,2);
  fill(); 
  fillStyle=txCol;
  if(r<No){
    font=Font;
    textBaseline="alphabetic";
    fillText( tx, x+tabPadd, tF );
  } else {
    trh=parseInt((h-3)/2)+1;
    for(var i=0;i<3;i++) fillRect(x+(parseInt((Lbtn-5)/2))+i,trh+i,5-i-i,1); 
  } 
}
}}

function move(e){
with(this){   
  var p=moEvtMng.getMouseXY(e, mf$("tab"+name)), x=p.x-tabL, cur="pointer", nv=-1;
  for(var r=0;r<Par.length;r++) with(Par[r]) {if(!Vis) continue;
  if(x>=Xi && x<Xf) {nv=r;break;}
  }  
  var dsb=1;if(!Elm[nv]) dsb=0;else {if(!Elm[nv].disable) dsb=0;}
  if(dsb) nv=-1;
  if(nv==Sl || nv<0) cur="default";
  if(nv!=Ov) {Ov=nv;draw();if(Ov==No) showmenu();}
  if(nv<0 || Ov!=No) hidemenu();
  Obj.style.cursor=cur;

  if(RFRSH<18) RFRSH=0;

  return false;
}}


function out(){with(this){
if(VisMenu && Ov==No) {VisMenu=0;outime();}
else {Ov=-1;draw();}
}}


function outime(i){
with(this){
  if(i) {if(!VisMenu || Ov!=No) {VisMenu=0;hidemenu();Ov=-1;draw();}
         return;}
setTimeout(name+".outime(1)",150);
}}


function up(e){
with(this){  
  var p=moEvtMng.getMouseXY(e, mf$("tab"+name)), x=p.x-tabL;nv=-1;
  for(var r=0;r<Par.length;r++) with(Par[r]) {
    if(!Vis) continue;
    if(x>=Xi && x<Xf) {nv=r;break;}
  }  
  var dsb=1;if(!Elm[nv]) dsb=0;else {if(!Elm[nv].disable) dsb=0;}
  if(dsb) nv=-1;
  if(nv!=-1 && nv!=Sl) {
    if(nv!=No) {
      Sl=nv;
      callLayout();
      // custom function after tab button
      console.log('tab_' + Elm[Sl].label + '_fn');
      window.settings = {
        tabFnName: 'tab_' + Elm[Sl].label + '_fn'
      }
      var fn = window[settings.tabFnName]; 
      if(typeof fn == 'function') {
          fn()
      }
      hidemenu();}
      else {if(VisMenu && !isTouch) hidemenu(); else showmenu(e);}
      draw();
  }                   
}}


function showmenu(e){
with(this){VisMenu=1;mOv=-1;     
  var cpos = moGlb.getPosition(mf$("tab"+name)), ml=Par[No].Xi+cpos.l, mt=cpos.t+tabH, mw=0, mh=0, nh=0, t2=2;
  yPar=[];
  for(var r=0;r<No;r++) with(Par[r]){if(Vis) continue;
  var sz=moCnvUtils.measureText(Tx,Font)+tabPadd*4+2;
  if(sz>mw) mw=sz;
  var yi=t2+tabH*nh;
  yPar[nh]={Yi:yi, Yf:yi+tabH};
  nh++;
  }
  mh=nh*tabH+5;
  if((moGlb.getClientWidth()-mw-2)<ml) ml-=mw-22;
  if(ml<1) ml=1;
  with(Lj) {width=mw+10;height=mh+10;}
  with(Lj.style){left=ml+"px";top=mt+"px";
  display="block";}
  Mpos=[ml,mt,mw,mh];
  mdraw();
  if(isTouch) {moEvtMng.cancelEvent(e); 
             moEvtMng.addEvent(document,'touchend', moGlb.hideMenuTab);}
}}


function mdraw(){with(this){
  var ml=Mpos[0], mt=Mpos[1], mw=Mpos[2], mh=Mpos[3], t2=2;
  
  with(mtx){
  clearRect(0,0,mw,mh); 
  fillStyle="rgba(240,240,240,0.9)"; 
  moCnvUtils.roundRect(mtx,0,1,mw,mh,2);
  fill();

  var i=0, r;
    for(r=0;r<No;r++) with(Par[r]){if(Vis) continue;  
    if(mOv==r) { fillStyle=this.Colors.bgTabOver;   
                 fillRect(1, yPar[i].Yi, mw-2, tabH+3);}    
    font=Font;
    textBaseline="alphabetic";
    
    with(this.Colors) {
      var txCol=(mOv==r)?fgTabTxtOver:GetCountElem(r);
      var dsb=1;
      if(!Elm[r]) dsb=0; else {if(!Elm[r].disable) dsb=0;}
      if(dsb) txCol=fgTabTxtDisable;
    }   
    fillStyle=txCol;
    fillText(Tx,tabPadd, t2+tF+2+i*tabH );
    i++;
    }
  } 
}}


function hidemenu(){with(this){VisMenu=0;
  Lj.style.display="none";
}}


function Lmove(e){with(this){VisMenu=1; 
  var cpos = moGlb.getPosition(mf$("listabhidden"+Name)), p=moEvtMng.getMouseXY(e), y=p.y-(yo+tabH-1), cur="pointer", nv=-1, nh=0;      
  for(var r=0;r<Par.length;r++) {if(Par[r].Vis) continue;
  if(y>=yPar[nh].Yi && y<yPar[nh].Yf) {nv=r;break;}
  nh++;
  }
  if(nv!=mOv) {mOv=nv;mdraw();}
  if(nv<0) cur="default";
  Lj.style.cursor=cur;

  return false;
}}


function Lout(e){with(this){  
Ov=-1;outime();
}}


function Lup(e){with(this){ 
var p=moEvtMng.getMouseXY(e, mf$("listabhidden"+Name)), y=p.y, nv=-1, nh=0;
for(var r=0;r<Par.length;r++) {if(Par[r].Vis) continue;
if(y>=yPar[nh].Yi && y<yPar[nh].Yf) {nv=r;break;}
nh++;
}
var dsb=1;if(!Elm[nv]) dsb=0;else {if(!Elm[nv].disable) dsb=0;}
if(dsb) nv=-1;
if(nv!=-1) {
  Sl=nv;  callLayout();
  Par[Sl].Vis=1;draw();
}
hidemenu();
}}


function GetCountElem(i){
with(this){
   var ly, c=Colors.fgTabTxtNosel, n=0; 
   try{
    ly=Elm[i].layout;
    ly="mdl" + ly.substring(3) + "_tg_tg";
    eval("n="+ly+".Ypos.length;"); 
    if(n) c=Colors.fgTabTxtElem; 
   }catch(e){}
  return c;
}}

} //



//--------------------------------------------------------------------------------------------------------------------------
// tabbar new
moTabbarmenu={ J:null, jl:null, maskj:null,
               Elm:[], Grp:[], Sel:0, Selv:0, SeLY:-1,
               DH:37, OVR:0, TMC:0, Frst:0,
      bkg:"#6F8D97",
      bkg_dark:"#4A6671",
      green:"#A1C24D",

Start:function(el,Cnt){
with(moTabbarmenu){

  var r,a,y,gr,ngr,fnt,ch,col,onm, Str="";
  
  // alert(JSON.stringify(el)); return
  
  J=mf$(Cnt);
  jl=moGlb.createElement("listaMainMenuhidden","div");
  with(jl.style) display="none", position="absolute", lineHeight=DH+"px";
  
  maskj=moGlb.createElement("masklistaMainMenuhidden","div");
  with(maskj.style) display="none", position="absolute", width="100%", height="100%";
  
  jl.style.zIndex=moGlb.zmenu;
  
  if(!isTouch){
    jl.onmouseover=function(){ OVR++;  } 
    jl.onmouseout=function(){  f_close_menu();  }  
  }  
    
  // visibility & Grouping
  gr=0; ngr=0;
  for(r=0;r<el.length;r++){
    if(moGlb.user.level & el[r].level) {
      a=el[r];
      if(!moGlb.isset(a.label) || !a.label) { Grp.push(a); gr=1;  }     // is group if no label 
      else { Elm.push(a); if(gr && !Frst) Frst=ngr; else ngr++; } 
    }
  }
 
  
  // create left icons menu
  Str="<div id='idtbrmenuSel' style='position:absolute;left:3px;top:"+(DH*2)+"px;width:34px;height:"+DH+"px;background-color:#DDD;"+
      "border-top-left-radius:2px;border-bottom-left-radius:2px;z-index:"+(moGlb.zmenu-2)+";'></div>";

  for(r=0;r<Grp.length;r++){
    y=r*DH+DH*2;
    a=Grp[r].icon.split(".");
    
    fnt=a[0], ch=a[1];
    col=(r==Sel)?bkg_dark:"#FFF";
    
    if(isTouch){
      onm="onclick='moTabbarmenu.f_open_menu("+r+")'";  
      maskj.style.display="block";
    }else{
      onm="onmouseover='moTabbarmenu.f_open_menu("+r+")' onmouseout='moTabbarmenu.f_close_menu()'";
    }
    Str+="<div id='idtbrmenu"+r+"' style='position:absolute;left:0;top:"+y+"px;width:37px;height:"+DH+"px;"+
         "font:16px "+fnt+";line-height:"+DH+"px;text-align:center;color:"+col+";cursor:default;z-index:"+(moGlb.zmenu-1)+";' "+onm+">"+String.fromCharCode(ch)+"</div>";
  }

  J.innerHTML=Str;
  
}},


reset:function(){


},


f_open_menu:function(i){
with(moTabbarmenu){

  OVR++; 
  var g,r,n,bk,tt,hh,hC,col,crn,op,
      s="",onm, atx=[];
  
  g=Grp[i].group;
  atx.push({ label:g, value:-1 });
  
  for(r=0;r<Elm.length;r++){
    if(Elm[r].group==g) atx.push({ label:Elm[r].label, value:r }); 
  }
  
  n=atx.length;
  for(r=0;r<n;r++){
    bk=r?bkg_dark:bkg;
    v=atx[r].value;  
    
    col="#FFF";
    onm=""; 
    if(v==Selv) col=green;
    else {  
      if(r) { onm="onclick='moTabbarmenu.f_clicksel("+v+","+i+")' ";
        if(!isTouch) onm+="onmouseover='moTabbarmenu.f_overout(this,1)' onmouseout='moTabbarmenu.f_overout(this,0)'";
      }
    }
    
    crn=""; op="";
    if(!r) crn="border-top-right-radius:2px;";
    if(r==(n-1)) crn+="border-bottom-right-radius:2px;";    
    if(r) op="opacity:0.92;";
    
    s+="<div style='height:"+DH+"px;font:18px opensansL;line-height:"+DH+"px;color:"+col+";padding:0 24px 0 8px;"+
       "cursor:"+(r?"pointer":"default")+";background-color:"+bk+";"+op+crn+"' "+onm+">"+
       moGlb.langTranslate(atx[r].label)+"</div>";
  }
  
  tt=(i+2)*DH;
  hh=n*DH;
  hC=J.offsetHeight;
  if( (tt+hh)>hC ) tt=hC-hh;
  
  jl.innerHTML=s;
  with(jl.style) {
    left="37px";
    top=tt+"px";
    height=hh+"px";
    display="block";
    overflow="hidden";
  }
  
 // if(isTouch) maskj.onclick=f_click_close;
  
}},


f_close_menu:function(i,t){
with(moTabbarmenu){
  OVR--;
  if(TMC) TMC=10; else { TMC=10; f_time_close(); }
}},


f_click_close:function(){
with(moTabbarmenu){
  jl.style.display="none"; 
  maskj.style.display="none";

}},


f_time_close:function(){
with(moTabbarmenu){ 
 TMC--; 
 if(TMC<=0) { TMC=0;
  if(OVR<1) { jl.style.display="none"; OVR=0; }
  return; 
 }
setTimeout("moTabbarmenu.f_time_close()",50); 
}},

f_overout:function(j,m){
with(moTabbarmenu){
  j.style.backgroundColor=m?green:bkg_dark;
}},


f_clicksel:function(v,i){    // v<0  first view
with(moTabbarmenu){

  if(moGlb.lockEdit) { 
      with(eval(moGlb.lockEditName)){
        eval(moGlb.lockEditName).cancel();
      }
    //moGlb.alert.show("<br>"+moGlb.langTranslate("Close the form before<br>proceeding with other feature."));
    //return false;
  }

  jl.style.display="none";
  mf$("idtbrmenuSel").style.top=(i*DH+DH*2)+"px";
  
  mf$("idtbrmenu"+Sel).style.color="#FFF";
  Sel=i;
  mf$("idtbrmenu"+Sel).style.color=bkg_dark;
  
  if(v<0) v=Frst;  
  
  Selv=v;
 
  // exec Layout
  var ly=Elm[v].layout;
  if(ly) eval(ly);

  if(SeLY>-1) eval(Elm[SeLY].layout).display(0, moGlb.bd);
  eval(Elm[v].layout).display(1, moGlb.bd);
  SeLY=v;
}}


} //