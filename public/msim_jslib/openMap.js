var unpMaps = {
    config : {
        baseUrl : 'http://localhost/mainsim3/',
        imagesUrl : 'http://localhost/mainsim3/library/openmap/leaflet/images/',
        //uploadUrl : 'http://localhost/mainsim3/getDocument.php'
        uploadUrl : 'http://localhost/mainsim3/document/get-file'
    },
    padString : function(string, where, length, pad){
        var padString = string;
        while(padString.length < length){
            if(where == 'L'){
                padString = pad + padString;
            }
            else if(where == 'R'){
                padString = padString + pad;
            }
            else{
                break;
            }
        }
        return padString;
    },
    subString : function(string, maxLength){
        if(string){
            return string.length > maxLength ? string.substr(0, maxLength) + '...' : string;
        }
        else return '';
    },
    lowerCaseString : function(string){
        if(string){
            return string.toLowerCase();
        }
        else{
            return null;
        }
    },
    map : null,
    layers : [],
    activeLayer : null,
    backControl : null,
    init : function(){
        this.map = L.map('map').setView([45.4666211, 9.1906166], 13);
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
            maxZoom: 18
        }).addTo(this.map);
        
        this.map.on('zoomstart', function(e){
            unpMaps.deleteAdditionalInfoCircles();
        });
        this.map.on('dragstart', function(e){
            unpMaps.deleteAdditionalInfoCircles();
        });
        // search control
        var searchControl = L.Control.extend({
            options: {
                position: 'topright'
            },
            onAdd: function (map) {
                // create the control container with a particular class name
                var container = L.DomUtil.create('div', 'searchPanel leaflet-control-search');
                container.innerHTML = '<div id="searchInput">' +
                                      '<input type="text" id="locationInput">&nbsp<a id="locationLink" class="leaflet-control-search-icon search-link" href="javascript:unpMaps.searchLocation();"></a>' +
                                      '</div>' + 
                                      '<div id="searchResult" style="display : none;">' +
                                      '</div>';
                return container;
            }
        });
        this.map.addControl(new searchControl());
        L.DomEvent.disableClickPropagation(document.getElementById('locationInput'));
        L.DomEvent.disableClickPropagation(document.getElementById('locationLink'));
    },
    // remove additional info circles around the cluster
    deleteAdditionalInfoCircles : function(){
        try{
            document.getElementById('map').removeChild(document.getElementById('c1'));
            document.getElementById('map').removeChild(document.getElementById('c2'));
            document.getElementById('map').removeChild(document.getElementById('c3'));
            document.getElementById('map').removeChild(document.getElementById('c4'));
        }
        catch(e){}
    },
    createAddtionalInfoCircles : function(a){
        var aux = a.layer.getAllChildMarkers();
               var p0 = 0, p1 = 0, p2 = 0, p3 = 0;
               for(var i = 0; i  < aux.length; i++){
                   switch(aux[i].priority){
                        case '0':
                           p0++;
                           break;
                        case '1':
                           p1++;
                           break;
                        case '2':
                           p2++;
                           break;
                        case '3':
                           p3++;
                           break;
                   }
               }
               var center = unpMaps.map.latLngToContainerPoint([a.layer._latlng.lat, a.layer._latlng.lng]);
               
               // a, b cathetus (tangent point of the center circle at PI/6)
               var a = Math.round(20 * Math.cos(Math.PI/3));
               var b = Math.round(20 * Math.sin(Math.PI/3));
               
               // first circle center
               var centerX1 = center.x + a * 2; 
               var centerY1 = center.y - b * 2; 
               // first div top left corner
               var top1 = centerY1 - 20;
               var left1 = centerX1 - 20;
               // first div
               var el1 = document.createElement('div');
               el1.id = 'c1';
               el1.style.borderRadius = '20px;';
               el1.style.width = '40px';
               el1.style.height = '40px';
               el1.style.position = 'absolute';
               el1.style.top = top1 + 'px';
               el1.style.left = left1 + 'px';
               el1.className = 'marker-cluster marker-cluster-large-grey';
               el1.innerHTML = '<div><span>' + p0 + '</span></div>';
               document.getElementById('map').appendChild(el1); 
               
               // second div top left corner
               var left2 = center.x + 20;
               var top2 = center.y - 20;
               // second div
               var el2 = document.createElement('div');
               el2.id = 'c2';
               el2.style.borderRadius = '20px;';
               el2.style.width = '40px';
               el2.style.height = '40px';
               el2.style.position = 'absolute';
               el2.style.top = top2 + 'px';
               el2.style.left = left2 + 'px';
               el2.className = 'marker-cluster marker-cluster-large-green';
               el2.innerHTML = '<div><span>' + p1 + '</span></div>';
               document.getElementById('map').appendChild(el2); 
               
               // third circle center
               var centerX3 = center.x + a * 2; 
               var centerY3 = center.y + b * 2; 
               // third div top left corner
               var top3 = centerY3 - 20;
               var left3 = centerX3 - 20;
               var el3 = document.createElement('div');
               el3.id = 'c3';
               el3.style.borderRadius = '20px;';
               el3.style.width = '40px';
               el3.style.height = '40px';
               el3.style.opacity = '1.0';
               el3.style.position = 'absolute';
               el3.style.top = top3 + 'px';
               el3.style.left = left3 + 'px';
               el3.className = 'marker-cluster marker-cluster-large-orange';
               el3.innerHTML = '<div><span>' + p2 + '</span></div>';
               document.getElementById('map').appendChild(el3);
               
               // fourth circle center
               var centerX4 = center.x - a * 2; 
               var centerY4 = center.y + b * 2; 
               // fourth div top left corner
               var top4 = centerY4 - 20;
               var left4 = centerX4 - 20;
               var el4 = document.createElement('div');
               el4.id = 'c4';
               el4.style.borderRadius = '20px;';
               el4.style.width = '40px';
               el4.style.height = '40px';
               el4.style.position = 'absolute';
               el4.style.top = top4 + 'px';
               el4.style.left = left4 + 'px';
               el4.className = 'marker-cluster marker-cluster-large-red';
               el4.innerHTML = '<div><span>' + p3 + '</span></div>';
               document.getElementById('map').appendChild(el4);
    },
    // show assets
    showAssets : function(){
       callback = function(response){            
            if(response != 'null'){
                var objResponse = JSON.parse(response);
                var newLayer = {
                    markers : new Array(),
                    bounds : null
                }
                // cluster coords array, it's used to properly resize the map
                var coords = [];

                for(var i = 0; i < objResponse.length; i++) {
                    coords.push({
                        lat : objResponse[i].lat,
                        lng : objResponse[i].lng
                    });
                    var newMarker = unpMaps.createMarker(objResponse[i].lat, objResponse[i].lng, unpMaps.createAssetIcon(), unpMaps.createHtmlPopup('a', objResponse[i]));
                    newLayer.markers.push(newMarker);
                }
                if(coords.length > 1){
                    newLayer.bounds = unpMaps.fitMap(coords);
                }
                unpMaps.layers['a'] = newLayer;
                unpMaps.createLayer('a', 'cluster');
            }
            else {
                moGlb.alert.show(moGlb.langTranslate('there are no assets'));
            }
       }
       
       Ajax.sendAjaxPost(this.config.baseUrl + 'open-map/get-assets', null, 'callback', false); 
    },
    // shows the assets related to the work order 
    showWorkOrderAssets : function(id, code){
       callback = function(response){
            var objResponse = JSON.parse(response);
            var newLayer = {
                markers : new Array(),
                bounds : null
            }
            // cluster coords array, it's used to properly resize the map
            var coords = [];
            for(var i = 0; i < objResponse.length; i++){
                coords.push({
                    lat : objResponse[i].lat,
                    lng : objResponse[i].lng
                });
                var newMarker = unpMaps.createMarker(objResponse[i].lat, objResponse[i].lng, unpMaps.createAssetIcon(), unpMaps.createHtmlPopup('a', objResponse[i]));
                newLayer.markers.push(newMarker);
            }
            newLayer.bounds = unpMaps.fitMap(coords);
            unpMaps.layers['wa_' + id] = newLayer;
            unpMaps.createLayer('wa_' + id, 'markers');
            
            // control to return to the workorder view
            backControl = L.Control.extend({
                options: {
                    position: 'bottomleft'
                },
                onAdd: function (map) {
                    // create the control container with a particular class name
                    var container = L.DomUtil.create('div', 'leaflet-control-back');
                    container.innerHTML = '<div id="back">' + 
                                                '<a id="backLink" href="javascript:unpMaps.backToWorkOrders()">back</a>' +
                                                '<span style="margin-left : 10px;">(work order ' + code + ')</span>' +
                                          '</div>'; 
                    return container;
                }
            });
            unpMaps.backControl = new backControl();
            unpMaps.map.addControl(unpMaps.backControl);
            L.DomEvent.disableClickPropagation(document.getElementById('backLink'));
       }
       
       if(this.layers['wa_' + id] == undefined){
            Ajax.sendAjaxPost(this.config.baseUrl + 'open-map/get-workorder-assets/id/' + id, null, 'callback', false);
       }
       else{
            unpMaps.createLayer('wa_' + id, 'markers');
       }
    },
    // shows the workorders or the first asset with coordinates related to the workorder
    showWorkOrders : function(){
        
        callback = function(response){
            
            if(response != 'null'){
                var objResponse = JSON.parse(response);
                // cluster coords array, it's used to properly resize the map
                var coords = [];
                var newLayer = {
                    markers : new Array(),
                    bounds : null
                }
                var icon;

                for(var i = 0; i < objResponse.length; i++){
                    coords.push({
                        lat : objResponse[i].lat,
                        lng : objResponse[i].lng
                    });
                var newMarker = unpMaps.createMarker(objResponse[i].lat, objResponse[i].lng, unpMaps.createWorkorderIcon(objResponse[i].priority, objResponse[i].categoryId), unpMaps.createHtmlPopup('w', objResponse[i]));
                    newMarker.priority = objResponse[i].priority;
                    newLayer.markers.push(newMarker);
                }
                newLayer.bounds = unpMaps.fitMap(coords);
                unpMaps.layers['w'] = newLayer;
                unpMaps.createLayer('w', 'cluster');
            }
            else{
                moGlb.alert.show(moGlb.langTranslate('there are no workorders'));
            }
        }
        if(this.layers['w'] == undefined || true){
            Ajax.sendAjaxPost(this.config.baseUrl + 'open-map/get-workorders', null, 'callback', false);
        }
        else{
           this.createLayer('w', 'cluster');
        }
    },
    // create new marker
    createMarker : function(lat, lng, icon, html, index){
        var marker;
        marker = L.marker([lat, lng],{icon : icon});
        marker.bindPopup(html);
        return marker;
    },
    createWorkorderIcon : function(priority, workorderCategory){
         return L.icon({
                iconUrl: this.config.imagesUrl + 'markers/' + workorderCategory + '_' + priority + '.png',
                iconSize: [29, 45]/*,
                iconAnchor: [22, 94],
                popupAnchor: [-3, -76],
                shadowUrl: 'my-icon-shadow.png',
                shadowSize: [68, 95],
                shadowAnchor: [22, 94]*/
            });
    },
    createAssetIcon : function(){
         return L.icon({
                iconUrl: this.config.imagesUrl + 'markers/marker-icon_blue.png',
                iconSize: [29, 45]/*,
                iconAnchor: [22, 94],
                popupAnchor: [-3, -76],
                shadowUrl: 'my-icon-shadow.png',
                shadowSize: [68, 95],
                shadowAnchor: [22, 94]*/
            });
    },
    createLayer : function(code, type){

        // destroy activeLayer
        if(this.activeLayer != null){
            this.map.removeLayer(this.activeLayer);
        }
        
        // create new active layer
        if(type == 'cluster'){
            this.activeLayer = new L.MarkerClusterGroup();
            this.activeLayer.on('clustermouseover', function (a) {
                // additional info circles only for workorders
                if(code == 'w'){
                    unpMaps.createAddtionalInfoCircles(a);
                }
            });
            
            this.activeLayer.on('clustermouseout', function (a) {
                // additional info circles only for workorders
                if(code == 'w'){
                    unpMaps.deleteAdditionalInfoCircles();
                }
            });
        }
        else if(type == 'markers'){
            this.activeLayer = new L.layerGroup();
        }
        
        for(var i = 0; i  < this.layers[code]['markers'].length; i++){
            this.activeLayer.addLayer(this.layers[code]['markers'][i]);
        }
        
        if(this.layers[code].bounds != undefined){
            try{
                this.map.fitBounds([
                    [this.layers[code].bounds.minLat, this.layers[code].bounds.minLng],
                    [this.layers[code].bounds.maxLat, this.layers[code].bounds.maxLng]
                ]);
            }
            catch(e){}
        }
        
        this.map.addLayer(this.activeLayer);
    },
    // resize the map to contain all points of the 'coords' array
    fitMap : function(coords){
        var minLat, minLng, maxLat, maxLng;
        
        for(var i = 0; i < coords.length; i++){
            // min latitude
            if(i == 0){
                minLat = coords[i].lat;
            }
            else{
                minLat = Math.min(coords[i].lat, coords[i].lat);
            }
            // max latitude
            if(i == 0){
                maxLat = coords[i].lat;
            }
            else{
                maxLat = Math.max(maxLat, coords[i].lat);
            }
            //min longitude
            if(i == 0){
                minLng = coords[i].lng;
            }
            else{
                minLng = Math.min(minLng, coords[i].lng);
            }
            // max longitude
            if(i == 0){
                maxLng = coords[i].lng;
            }
            else{
                maxLng = Math.max(maxLng, coords[i].lng);
            }
        }
        return {
            minLat : minLat,
            minLng : minLng,
            maxLat : maxLat,
            maxLng : maxLng
        }
    },
    // return the locations related to the user input
    searchLocation : function(){
        callback = function(response){
            var objResponse = JSON.parse(response);
            // format results
            var html = '';
            for(var i = 0; i < objResponse.length; i++){
                html += '<div class="search-result">' +
                        '<a  href="javascript:unpMaps.centerLocation(\'' + objResponse[i].lat + '\',\'' + objResponse[i].lon + '\');">' + objResponse[i].display_name + '</a>' +
                        '</div>';
            }
            document.getElementById('searchResult').innerHTML = html;
            document.getElementById('searchResult').style.display = 'block';
        }
        Ajax.sendAjaxPost(this.config.baseUrl + 'open-map/search-location/location/' + document.getElementById('locationInput').value, null, 'callback', false);
    },
    centerLocation : function(lat, lng){
        this.map.setView([lat, lng], 18);
        this.resetSearch();
    },
    // reset and hide search results
    resetSearch : function(){
        document.getElementById('searchResult').innerHTML = '';
        document.getElementById('locationInput').value = '';
        document.getElementById('searchResult').style.display = 'none';
    },
    createHtmlPopup : function(type, objData){
        // asset template
        if(type == 'a'){
            
            // icon 
            if(objData.icon != ''){
                aux = objData.icon.split('|');
                iconUrl = this.config.imagesUrl + '?session_id=' + aux[0] + '&size=thumbnail&uploadType=image'; 
            } 
            else{
                iconUrl = this.config.imagesUrl + '/_no_image_160.jpg';
            }

            html = '<div style="position : relative; left : 0; top : 0; width : 240px;">';

            html += '<div style="width : 240px; border-bottom : 1px solid black; height : 16px;">' +
                        '<div style="float : left;"><b>' + objData.title + '</b></div>' +
                     '</div>';
            html += '<div>' +
                        '<div style="width : 140px; float : left;">' +
                            '<div style="margin : 10 0 0 0px;">' + 
                                '<b>Description</b><br>' + 
                                '<div style="height : 65px; background-color : #F7F5F5;">' + objData.description + '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div style="width : 100px; float : left;">' +
                            '<div style="margin : 10px;"><img style="border : 1px solid black;" width="80" height="80" src="' + iconUrl + '"></div>' +
                        '</div>' + 
                    '</div>' +
                    '<div>' + 
                        '<div><b>Hierarchy</b>&nbsp;' + objData.hierarchy + '</div>' +
                        '<div><b>Selector</b>&nbsp;' + objData.selector + '</div>' +
                        '<div><b>Class</b>&nbsp;' + objData.assetClass + '</div>' +
                    '</div>';
        }
        // workorder template
        else if(type == 'w'){
            // icon 
            if(objData.icon != ''){
                aux = objData.icon.split('|');
                iconUrl = this.config.baseUrl + '?session_id=' + aux[0] + '&size=thumbnail&uploadType=image'; 
            } 
            else {
                iconUrl = this.config.imagesUrl + '/_no_image_160.jpg';
            }

            html = '<div style="position : relative; left : 0; top : 0; width : 240px; height : 130px;">';

            html += '<div style="width : 240px; border-bottom : 1px solid black; height : 16px;">' +
                        '<div style="float : left;"><b>Code</b>&nbsp;' + this.padString(objData.code, 'L', 5, '0') + '</div>' +
                        '<div style="float : right;"><b>Phase</b>&nbsp;' + objData.phase + '</div>' +
                     '</div>';
            html += '<div>' +
                        '<div style="width : 140px; float : left;">' +
                        '<div style="margin-top : 10px;"><b>Title</b>&nbsp;<span title="' +  this.lowerCaseString(objData.title) + '">' + this.lowerCaseString(this.subString(objData.title, 15)) + '</span></div>' +
                            '<div>' + 
                                '<b>Description</b><br>' + 
                                '<div style="height : 50px; background-color : #F7F5F5;">' + this.lowerCaseString(objData.description) + '</div>' +
                            '</div>' + 
                        '</div>' +
                        '<div style="width : 100px; float : left;">' +
                            '<div style="margin : 10px;"><img style="border : 1px solid black;" width="80" height="80" src="' + iconUrl + '"></div>' +
                        '</div>' + 
                    '</div>';
            if(objData.assets){
                html += '<div style="margin-left : 156px;"><a href="javascript:unpMaps.showWorkOrderAssets(' + objData.id + ',' + this.padString(objData.code, 'L', 5, '0') + ')">show assets</a></div>';
            }
            html += '</div>';
        }
        return html;
    },
    // redisplay orders after display of assets associated to a specific work order 
    backToWorkOrders : function(){
        this.map.removeControl(unpMaps.backControl);
        this.showWorkOrders();
    }
}