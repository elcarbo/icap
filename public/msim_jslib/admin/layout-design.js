// layout administrator v3
/*

layout formato:

{ idp:-1, ov:0, module:1, tp:1, wh:200, level:1, [lock:1] }

module  nome modulo
idp     id padre
ov      0=orizzontale  1=verticale
level   visibilit� modulo
lock    1=bloccato no resize
acc     1=accordion  0=default
minv    se non definito minwh

tp  valore   0=% o 1=fixed
wh  width  o height    (dipende da ov)
 
*/

 
 
var LAYOUT_cut = { 

 minwh:moGlb.moduleMinWH, Drgw:5, 

 j:null, LY:[], Psz:[], nly:0, 
 Lysize:[], Last:[], Cnt:[], Res:[], LT:[], Drgw2:10, 
 X:0,Y:0, SHFT:0, CTRL:0, voj:null, J:null, IM:"", ww: 0, hh: 0,
 maskj:null, maskjw: null, IM: "../public/msim_images/default/admin/layout/",

Edit:function(jso, s){
    with(LAYOUT_cut) {
        moGlb.loader.show();
        if(!moGlb.isempty(jso)) LY=(JSON.parse(jso).layout);else LY=[ {idp:-1, ov:0 , tp:0, wh:100, level:1, module:"mdl0"}];
            
        // creo LY e marco i del (da cancellare)
        for(var r=0;r<LY.length;r++) { 
            if(typeof(LY[r].lock)=="undefined") LY[r]["lock"]=0; 
            if(typeof(LY[r].minv)=="undefined") LY[r]["minv"]=minwh;
            if(typeof(LY[r].acc)=="undefined") LY[r]["acc"]=0;

            LY[r]["lock"]=parseInt(LY[r]["lock"]); 
            LY[r]["acc"]=parseInt(LY[r]["acc"]);
            LY[r]["minv"]=parseInt(LY[r]["minv"]);
        }

        if(!mf$('laybox')) { // First time
            j=mf$(s)/*, Psz = moGlb.getPosition(j)*/;
            j.innerHTML = "";
            J=moGlb.createElement("laybox","div",j);  
            with(J.style) width="100%", height="100%", zIndex=30, cursor="default";

            if(!maskj) {
                maskj=moGlb.createElement("maskly_layout","div",j);
                with(maskj.style) display='none', padding="5px";
            }

            voj=moGlb.createElement("imgcut","div",j);  
            with(voj.style) width="100%", height="100%", zIndex=20, cursor="default";    

            j.onmousemove=function(e) {var P=moEvtMng.getMouseXY(e);X=P.x, Y=P.y;drwCut();}
            document.onkeydown=function(e){if(!e) e=window.event;if(e.ctrlKey) CTRL=1;if(e.shiftKey) SHFT=1;drwCut();}
            document.onkeyup=function(e){if(!e) e=window.event;if(!e.ctrlKey)  CTRL=0;if(!e.shiftKey) SHFT=0;drwCut();}
        }
        
        setTimeout("LAYOUT_cut.resize()", 1000);
        //resize();
    }
},


Save:function(){
  with(LAYOUT_cut){
      if(LY.length == 1) {
          LY.push(JSON.parse(JSON.stringify(LY[0])));
          LY[1].module = LY[0].module;LY[0].module = "";
          LY[1].idp = 0;LY[1].ov = (parseInt(LY[0].ov)+1)%2;
      }
  return JSON.stringify({layout: LY});  
}},


drwCut:function() { 
  with(LAYOUT_cut) {
  var ov='verticale.gif', kx=Psz.l+1, ky=Psz.t+20;
  if(CTRL) {ov='orizzontale.gif';kx=Psz.l+10, ky=Psz.t+1;}   
  voj.innerHTML="<img src='"+IM+ov+"' style='position:absolute;left:"+(X-kx)+"px;top:"+(Y-ky)+"px;' />"+
                    "<div style='position:absolute;left:"+(X-kx+35)+"px;top:"+(Y-ky+35)+"px;font:10px Arial;color:#000;'>CTRL: rotate<br>SHIFT: "+((SHFT)?"fixed":"%")+"</div>";
}},


resize:function(){
with(LAYOUT_cut) {    
  //if(!IM) IM = "../../public/msim_images/default/admin/layout/";
  nly=LY.length;if(!nly) { /*moGlb.alert.show("no layout found!");*/return;}
    for(var r=0;r<nly;r++)  Lysize[r]={l:0,t:0,w:0,h:0, ol:0,ot:0,ow:0,oh:0}
    
 
  J.innerHTML="";Cnt=[];Res=[]; 
    Drgw2=Drgw+Drgw;
    Psz = moGlb.getPosition(j);
    Cnt[0]=[0, Psz.w, 0, Psz.h]; LT=[Psz.l,Psz.t],

    recurBuilt(0);
    drawres(); 
    
    // alert(JSON.stringify(LY) )
    
    if(nly==1){Lysize[0]={l:0,t:0, w:Psz.w, h:Psz.h, ol:0,ot:0,ow:Psz.w,oh:Psz.h};ridraw(0);}
}},


recurBuilt:function(p){
  with(LAYOUT_cut){
         var fg=[], fgs=[], fgS=[], cur=LY[p].ov, s=0, S=0, i=0, L=Cnt[p][cur*2], ll=L, T=Cnt[p][cur*2+1], v=0, a=[], MIN;        // S=fixed pixel    s=%     T=totale orizz o vert
         for(var r=1;r<nly;r++) if(LY[r].idp==p) {fg.push(r);i=LY[r].wh;if(LY[r].tp) {fgS.push(r);S+=i;} else {fgs.push(r);s+=i;}}
            
         // se sommatoria fixed diversa da totale pixel
         var nfg=fg.length;
         if(!s && T!=S) {for(r=(nfg-1);r>=0;r--) {i=fg[r];v=LY[i].wh+T-S;MIN=LY[i].minv;if(v>MIN) {LY[i].wh=v;break;}}  
         } else {
         // normalizzazione percentuali su totpixel
         var Ts=T-S, ns=fgs.length, nS=fgS.length, u,uu,su=0, ru;   
                 
         if(Ts!=s) {for(r=0;r<ns;r++) {ru=fgs[r];u=LY[ru].wh;MIN=LY[ru].minv;
                                        if(u==MIN) uu=u;
                                        else {if(r==(ns-1)) uu=Ts-su;else {uu=parseInt(u*Ts/s);}
                                               if(uu<MIN) uu=MIN;}
                                        LY[ru].wh=uu;su+=uu;}                                                              
         if(Ts!=su){
         if(nS) for(r=(nS-1);r>=0;r--) {i=fgS[r];v=LY[i].wh+Ts-su;MIN=LY[i].minv;if(v>MIN) {LY[i].wh=v;break;}} 
         else for(r=(ns-1);r>=0;r--) {i=fgs[r];v=LY[i].wh;MIN=LY[i].minv; 
                    if(v>MIN) {LY[i].wh=v+Ts-su;break;} 
                    if(!r) {LY[fgs[ns-1]].wh=v+Ts-su;break;} 
         }}}}
                
         // for fratelli - calcolo dimensioni box Cnt
         v=0;   
         for(r=0;r<nfg;r++) {i=fg[r];for(var n=0;n<4;n++) a[n]=Cnt[p][n];    
         v=LY[i].wh;a[cur*2]=L, a[cur*2+1]=v;L+=v;
         Cnt[i]=[ a[0], a[1], a[2], a[3] ];

         with(Lysize[i]) ol=l, ot=t, ow=w, oh=h, l=a[0], t=a[2], w=a[1], h=a[3]; 

         // resize
         if(r) {var di=fg[r-1], oo=(cur)?0:1;a[cur*2]-=Drgw, a[cur*2+1]=Drgw2, a[oo*2]+=Drgw, a[oo*2+1]-=Drgw2;  
         var s1=minmax(di,oo), s2=minmax(i,oo);  
  
         Res.push( {da:di,ad:i, pdr:p, ov:cur, l:a[0], w:a[1], t:a[2], h:a[3], minda:s1, maxad:s2} );
         }
         if(!LY[i].module) recurBuilt(i); else ridraw(i);
         }          
}},


minmax:function(i,oo){ 
 with(LAYOUT_cut){
    var r,k, s=0, sum=[], SUM=0, f=0; 
    for(r=0;r<nly;r++) {k=LY[r].idp; 
        if(k==i) {f++;
            if(LY[r].ov==oo) {                              
              if(!LY[r].module) {s=minmax(r,oo);SUM+=s;}
              else SUM+=LY[r].minv;                             
            } else {
              if(!LY[r].module) {s=minmax(r,oo);sum.push(s);}
              else sum.push(LY[i].minv);
            }          
    }}    
    for(r=0;r<sum.length;r++) {if(sum[r]>SUM) SUM=sum[r];}   
    if(!f) SUM=LY[i].minv;
 
  return SUM;  
}},


ridraw:function(i){
 with(LAYOUT_cut){
     var ii=LY[i].module;
      if(!ii) return; 
      with(Lysize[i]){   
       
      var p=LY[i].idp;if(p<0) p=0; 
      var orve=[LY[p].tp, LY[i].tp], md=[1,0];if(LY[i].ov) orve=[LY[i].tp, LY[p].tp], md=[0,1]; 
      
      J.innerHTML+="<div style='position:absolute;left:"+(l+3)+"px;top:"+(t+3)+"px;width:"+(w-8)+"px;height:"+(h-8)+"px;"+     
      "overflow:hidden;border:1px solid #aaa;;background-color:#eee;opacity:.70; filter:alpha(opacity=70);-moz-opacity:.70;' onclick='LAYOUT_cut.CutBox("+i+")'>"+
      "<div style='position:absolute;left:3px;top:3px;width:"+(w-12)+"px;height:"+(h-12)+"px;overflow:hidden;font:11px Arial;cursor:default;' onmouseover='this.style.cursor=\"default\";'>"+
      "<strong>Box: "+i+
      "</strong><br>"+LY[i].module+

      "<img src='"+IM+"delete_16.png' style='position:absolute;left:100%;top:4px;margin-left:-22px;cursor:pointer;' onclick='LAYOUT_cut.fDelete(event,"+i+"); moEvtMng.cancelEvent(event);' />"+
      "<img src='"+IM+"info_16.png' style='position:absolute;left:100%;top:4px;margin-left:-42px;cursor:pointer;' onclick='LAYOUT_cut.fParam(event,"+i+"); moEvtMng.cancelEvent(event);' />"+ 
      
      "<img src='"+IM+((orve[0])?'fixed':'percentage')+"0.gif' style='position:absolute;left:50%;bottom:2px;margin-left:-16px;cursor:pointer;' onclick='LAYOUT_cut.FixedPerc(event,"+i+","+md[0]+"); moEvtMng.cancelEvent(event);' />"+
      "<img src='"+IM+((orve[1])?'fixed':'percentage')+"1.gif' style='position:absolute;left:0px;top:50%;margin-top:-16px;cursor:pointer;' onclick='LAYOUT_cut.FixedPerc(event,"+i+","+md[1]+"); moEvtMng.cancelEvent(event);' />"+
      
      "</div></div>";
      moGlb.loader.hide();
}}},


FixedPerc:function(e,i,m){
 with(LAYOUT_cut){
       if(!i) return;
       var v=(m)?LY[i].idp:i;    //  m=1 padre   0=figlio
       with(LY[v]){  
       var s=0, S=0, Ts=0, q,r, oo=(ov)?0:1;
       for(r=1;r<nly;r++) if(LY[r].idp==idp) {q=LY[r].wh;if(!LY[r].tp) {s+=q;Ts+=Cnt[r][oo*2+1];}} 
 
       if(tp) {tp=0;if(!s) wh=100; else wh=wh*s/Ts;}                                          // da fix a % 
       else {tp=1;wh=Cnt[v][oo*2+1];}                                                           // da % a fix 
       } //  
  resize();
}},


CutBox:function(i){
 with(LAYOUT_cut){
 
   if(LY[i].idp==-1 && CTRL) LY[0].ov=1;  // inverti radice primo taglio orizzontale
 
   with(LY[i]) var v=wh, pp=tp, o=ov, pa=idp, le=level;    
   var p=SHFT, oo=(o)?0:1;    
       
      if(o==CTRL) {LY[i].module="";                                                    // figli   
         
        var tu=Cnt[i][o*2+1], wh1=((o)?Y:X)-Cnt[i][o*2]-LT[o], wh2=tu-wh1; 
        
//        LY.push({idp:i, ov:oo, tp:p, wh:wh1, level:le, module:'module0', acc:0, lock:0, minv:minwh});
//        LY.push({idp:i, ov:oo, tp:p, wh:wh2, level:le, module:'module0', acc:0, lock:0, minv:minwh});
        LY.push({idp:i, ov:oo, tp:p, wh:wh1, level:le, module:'new_module', acc:0, lock:0, minv:minwh});
        LY.push({idp:i, ov:oo, tp:p, wh:wh2, level:le, module:'new_module', acc:0, lock:0, minv:minwh});
        
      } else {fg=0;                                                                   //  fratello
        for(var r=1;r<nly;r++) if(LY[r].idp==pa && r>i) {fg=r;break;}
        
        tu=Cnt[i][oo*2+1], wh1=((oo)?Y:X)-Cnt[i][oo*2]-LT[oo], wh2=tu-wh1; 
         
        // % cutted in %
        if(!p && !pp) {var q=1, wh1= wh1*v/tu, wh2=v-wh1;wh1*=q, wh2*=q;}
        
        // fixed cutted in %
        if(pp && !p) {var TS=0,CTS=0;for(var r=1;r<nly;r++) with(LY[r]) if(idp==pa){if(!tp){TS+=wh;CTS+=Cnt[r][oo*2+1];}}wh2=wh2*TS/CTS;}
        
        // % cutted in fixed
        if(!pp && p) wh1=wh1/tu*v;

        LY[i].wh=wh1;
//        LY.push({idp:pa, ov:o, module:'module0', tp:p, wh:wh2, level:le, acc:0, lock:0, minv:minwh});
        LY.push({idp:pa, ov:o, module:'new_module', tp:p, wh:wh2, level:le, acc:0, lock:0, minv:minwh});
          if(fg){var a=LY.pop();LY.splice(fg,0,a); 
          for(var r=fg+1;r<LY.length;r++) if(LY[r].idp>=fg) LY[r].idp++;}
      }     
      
    resize();
}},


fDelete:function(e, i) {
 with(LAYOUT_cut){
    if(!i) return;
    if(LY.length == 2) { moGlb.alert.show(moGlb.langTranslate("You cannot delete the last box")); return; }
    moGlb.confirm.show(moGlb.langTranslate("Confirm the deletion of the box")+" "+i+"?", function () {    
        delrow(i);
        resize();
    });
// if(!confirm("confirm the deletion of the box: "+i)) return false;
// delrow(i);  
// resize();
}},

 
fParam:function(e,i){
 with(LAYOUT_cut){
  var f="font:11px Arial;color:#000;", p="width:160px;border:1px solid #aaa;background-color:#fff;"+f, c="cursor:pointer;", d="style='cursor:pointer;'";
  
  
      maskj.innerHTML="<b>Module name</b><br>"+
    "<input id='lyinp0' type='text' value='"+LY[i].module+"' class='input_admin' /><p/>"+
    
    "<b>Level visibility</b><br>"+
    "<input id='lyinp1' type='text' value='"+LY[i].level+"' class='input_admin' /><p/>"+
    
    "<img id='idimgpar0' src='"+IM+"check"+LY[i].lock+".gif' onclick='LAYOUT_cut.chgImgchk(this)' "+d+" /> Locked<p/>"+
    
    "<img id='idimgpar1' src='"+IM+"check"+LY[i].acc+".gif' onclick='LAYOUT_cut.chgImgchk(this)' "+d+" /> Accordion<p/>"+
    
    "<b>Minimal width</b><br>"+
    "<input id='lyinp2' type='text' value='"+LY[i].minv+"' class='input_admin' /><p/>"+
    
    "<button style='position:absolute;left:5px;bottom:5px;"+f+c+"' onclick='LAYOUT_cut.closeParam(event);'>Cancel</button>"+
    "<button style='position:absolute;right:5px;bottom:5px;"+f+c+"' onclick='LAYOUT_cut.applyParam(event,"+i+");'>Apply</button>";
     
//  maskjw = moComp.createComponent("maskj", "moWindow", {wc: 175, hc: 200});
//  mf$(maskjw.getContainerId()).appendChild(maskj);
//  maskj.style.display = "block";
//  maskjw.show({title: "Properties"});
    var win_w = 175, win_h = 235, win_t = 10;
    maskj.setAttribute("style", "width:"+win_w+"px;height:"+win_h+"px;padding:5px;");
    maskjw = moComp.createComponent("maskjw_layout", "moPopup", {contentid: maskj.id, title: 'Properties', width: win_w, height: win_h});  
    maskjw.show();
}},


applyParam:function(e,i){
 with(LAYOUT_cut){  
 var m=mf$("lyinp0").value;
 if(!m) {moGlb.alert.show(moGlb.langTranslate("Module name is required!"));return;}

 LY[i].module=m;
 
 var v=mf$("lyinp1").value;if(v=="") {moGlb.alert.show(moGlb.langTranslate("Level is required!"));return;}
 v=parseInt(v);  // if(v<=0)  {moGlb.alert.show("Level must be greater than 0!");return;} 
 LY[i].level=v; 
 
 LY[i].lock=chgImgchk(mf$("idimgpar0"));
 LY[i].acc=chgImgchk(mf$("idimgpar1"));
 
 var v=mf$("lyinp2").value;if(v=="") v=minwh;
 v=parseInt(v);if(v<0) v=minwh; 
 LY[i].minv=v;

 closeParam(e);
 resize();
}},



closeParam:function(e){  
with(LAYOUT_cut){
  moEvtMng.cancelEvent(e);
  maskj.innerHTML=""; 
  maskjw.hide();        
}},


chgImgchk:function(ij){
 with(LAYOUT_cut){
     
 
 
var b=ij.src, a=b.split("."), aa=a.pop(), aa=a.pop();
var c=aa.length, b=parseInt(aa.substring(c-1,c));

ij.src=IM+"check"+(b?0:1)+".gif";
if(b) b=1;
return b;
}},



drawres:function(){
  with(LAYOUT_cut){var rj,dr,d,nn;
    for(r=0;r<Res.length;r++) {

       with(Res[r]) { 

          var lm=[0,0,0,0];l1=Cnt[da][ov*2]+LT[ov]+minda-Drgw,           
            lm[ov*2]=l1, l2=Cnt[ad][ov*2+1]+Cnt[ad][ov*2]+LT[ov]-maxad+Drgw,         
            lm[ov*2+1]=l2, b=(ov)?t:l, b+=LT[ov];
        
          if(LY[da].lock || LY[ad].lock) continue;
            if(b>=lm[ov*2] && b<=lm[ov*2+1]){
                if(!mf$("idres"+r)) moGlb.createElement("idres"+r,"div",J);
                rj=mf$("idres"+r); 
                with(rj.style) zIndex=moGlb.zdrag, left=l+"px", top=t+"px", width=w+"px", height=h+"px", overflow="hidden",
                               cursor=(ov)?"n-resize":"e-resize", 
                               backgroundColor="#f0f0f0";moGlb.setOpacity(rj,55);  
                                           
                d=(ov)?t:l;d+=LT[ov]+Drgw;dr=((d-l1)>(l2-d))?1:0;  
                if(!(d-l1-Drgw) || !(l2-d-Drgw)) dr=(dr)?0:1;        
                nn=(dr)?(l2-Drgw2):l1;                            
                                   
                var EVENT_DOWN='onmousedown', EVENT_UP='onmouseup', EVV="";
                if(moGlb.isTouch) EVENT_DOWN='ontouchstart', EVENT_UP='ontouchend', 
                  EVV="jj.ontouchmove=moGlb.drag.Move(event); jj.ontouchend=moGlb.drag.Stop(event);";
                
                rj.innerHTML="<img src='"+IM+"fr"+ov+"_"+dr+".png' "+EVENT_UP+"='LAYOUT_cut.size("+r+","+nn+")' "+EVENT_DOWN+
                "='moEvtMng.cancelEvent(event)'  "+
                             "style='position:absolute;left:50%;top:50%;margin-left:-18px;margin-top:-18px;width:36px;cursor:pointer;' />";                   
                
                eval("rj."+EVENT_DOWN+"=function(e) { var jj=e.target||e.srcElement; jj.innerHTML=''; jj.style.backgroundColor='#bbb'; "+
                " moGlb.drag.Start(e,{ Vinc:"+(ov+1)+", Lim:1, Lm0:"+lm[0]+", Lm1:"+lm[1]+", Lm2:"+lm[2]+", Lm3:"+lm[3]+", Fnc:'LAYOUT_cut.size("+r+")' }); }");               
  }}}}
},



size:function(i,nn){  
  with(LAYOUT_cut){    
       with(Res[i]) {var L,T,whda,whad, d, z=1;   
            var v1=LY[da].wh, v2=LY[ad].wh, v12=v1+v2; 
                  
            if(!nn) with(mf$("idres"+i).style) L=parseInt(left), T=parseInt(top);
            else {L=nn-Psz.l, T=nn-Psz.t;  
                
               d=(ov)?(T-t):(L-l);              
               var vv1=v1+d, vv2=v12-vv1; 
                       
            if(!Last[da] || !Last[ad] || LY[da].acc || LY[ad].acc ){  }
            else{
              if(v1==minda || v2==maxad ) {if(v1==v2) return;
              z=0;
              if(v1==minda) {whda=Last[da], whad=v12-whda;if(whad<maxad) z=1;}
              if(v2==maxad) {whad=Last[ad], whda=v12-whad;if(whda<minda) z=1;}            
       
              if(!z) LY[da].wh=whda, LY[ad].wh=whad;
            }}}      
            if(z){d=(ov)?(T-t):(L-l);  
              LY[da].wh+=d; 
              LY[ad].wh=v12-LY[da].wh;} 
            Last[da]=v1;
            Last[ad]=v2;
      
        resize();
  }}
  return false;
}, 
  

 
delrow:function(i){  
  with(LAYOUT_cut){    
       var p=LY[i].idp, fg=[], n=0, son=0;
       for(var r=1;r<LY.length;r++)  if(LY[r].idp==p) {fg.push(r);if(r!=i) son=r;n++;} 
       if(n>2) dele(i);
       else {if(LY[son].module) {ii=son;LY[p].module=LY[ii].module;LY[p].level=LY[ii].level;
                                   dele(fg[1]);dele(fg[0]);} 
              else {var pp=LY[p].idp;  
                      for(var r=1;r<LY.length;r++) if(LY[r].idp==son) LY[r].idp=(pp<0)?0:pp;
                      dele(fg[1]);dele(fg[0]);if(pp<0) LY[0].ov=(LY[0].ov)?0:1; else dele(p);}
            }                       
}},

dele:function(i){
  with(LAYOUT_cut){
    LY.splice(i,1);
    for(var r=i;r<LY.length;r++) if(LY[r].idp>i) LY[r].idp--;
}}

} //