/* menu adm

IN   -->   moMenu_adm.Start(container, json)
 json menu
 container div

OUT  -->   menujson=moMenu_adm.gridtojsn();
  return json menu
*/


moMenu_adm={ J:null, maskj:null, popj:null, popjs: null, grid:[], jsn:[], 
          btnMode:-1, FC:0, V:[],
          aType:["plain","children","image","radio","separator"],
          SPA:"style='position:absolute;left:",          

Start:function(container, jsnIN) {
with(moMenu_adm){
    moGlb.loader.show();
  var r,s,ji;
  J=mf$(container);
  ji=moGlb.cloneArray(jsnIN);
  jsntogrid(ji);
  Draw(); tabDraw();
  Btn(0);
//  maskj=mf$("idmaskmenuadm");
//  if(!popj) popj = moGlb.createElement('idpopmenuadm', 'div');
}},


jsntogrid:function(ji){
with(moMenu_adm){
  FC=0;
  grid=[{ Txt:"Root", TxtDx:"", Type:1, Status:0, Disable:0, Level:-1, Icon0:"", Icon1:"", Group:0, Fnc:"", Fcode:0, Parent:"" }];
  jsnChild(ji,0);
}},


jsnChild:function(ji,prnt){ // ji=sub[]
with(moMenu_adm){
  var r,I,pp,u="undefined";
    for(r=0;r<ji.length;r++)with(ji[r]){   
      I=["",""];
      if(typeof(ji[r].Icon)!="undefined") I[0]=Icon[0], I[1]=Icon[1];     
      if(typeof(ji[r].TxtDx)==u) ji[r].TxtDx="";
      if(typeof(ji[r].Group)==u) ji[r].Group=0;
      if(typeof(ji[r].Level)==u) ji[r].Level=-1;
      if(typeof(ji[r].Status)==u) ji[r].Status=0;
      if(typeof(ji[r].Fnc)==u) ji[r].Fnc="";
      if(typeof(ji[r].Type)==u) ji[r].Type=0;
      if(typeof(ji[r].Txt)==u) ji[r].Txt="";
      if(typeof(ji[r].Disable)==u) ji[r].Disable=0;  
      FC++;
      grid.push( { Txt:Txt, TxtDx:TxtDx, Type:Type, Status:Status, Disable:Disable, Level:Level, Icon0:I[0], Icon1:I[1], Group:Group, Fnc:Fnc, Fcode:FC, Parent:prnt+"" } )    
      pp=prnt+"|"+FC; 
      if(Type==1) jsnChild(Sub,pp);
  }
}},


gridtojsn:function(){
with(moMenu_adm){
  var r;
  jsn={menu: []};
  recursivGtoJ(0,"jsn.menu");
  
  return JSON.stringify(jsn);
}},


recursivGtoJ:function(p,sj){
with(moMenu_adm){
  var p=p+"";
  var r,k=0,pa, j=eval(sj);
 
  for(r=1;r<grid.length;r++){ 
    pa=grid[r].Parent+"";
    if(pa==p) { 
      j.push( f_eq(grid[r]) );
      if(grid[r].Type==1) {
        pp=p+"|"+grid[r].Fcode;
        recursivGtoJ(pp, sj+"["+k+"].Sub");
      }
      k++;
    }  
  }  
}},


f_eq:function(rwj){
with(moMenu_adm){
  var tp=rwj.Type;
  if(tp==4) return { Type:4 };
  vj={ Txt:rwj.Txt, Type:parseInt(rwj.Type), Disable:parseInt(rwj.Disable), Fnc:rwj.Fnc, Level:parseInt(rwj.Level)  }   
  if(rwj.TxtDx) vj["TxtDx"]=rwj.TxtDx;
  
  if(tp==1) vj["Sub"]=[];
  if(tp==2) { vj["Status"]=parseInt(rwj.Status); vj["Icon"]=[rwj.Icon0, rwj.Icon1]; } 
  return vj;
}},


Draw:function() {
with(moMenu_adm){

  var s="<button id='idbtnmoMenu_adm0' "+SPA+"10px;top:10px;width:60px;' onmouseover='moMenu_adm.Btn_overout(0,0)' onmouseout='moMenu_adm.Btn_overout(1,0)' onclick='moMenu_adm.Btn(0)' title='Edit'><span style='font:16px FlaticonsStroke;'>"+String.fromCharCode(58505)+"</span></button>"+
        "<button id='idbtnmoMenu_adm1' "+SPA+"75px;top:10px;width:100px;' onmouseover='moMenu_adm.Btn_overout(0,1)' onmouseout='moMenu_adm.Btn_overout(1,1)' onclick='moMenu_adm.Btn(1)' title='Add sibling'><span style='font:16px FlaticonsStroke;'>"+String.fromCharCode(58349)+"</span></button>"+
        "<button id='idbtnmoMenu_adm2' "+SPA+"180px;top:10px;width:100px;' onmouseover='moMenu_adm.Btn_overout(0,2)' onmouseout='moMenu_adm.Btn_overout(1,2)' onclick='moMenu_adm.Btn(2)' title='Add child'><span style='font:16px FlaticonsStroke;'>"+String.fromCharCode(58661)+"</span></button>"+
        "<div id='idconttablemenuadm' "+SPA+"8px;right:8px;top:50px;bottom:8px;overflow:auto;'></div>";//+
        //"<div id='idmaskmenuadm' "+SPA+"left:0;top:0;width:100%;height:100%;background-color:#444;opacity:0.2;display:none;'></div>"+
        //"<div id='idpopmenuadm' "+SPA+"0px;top:0px;width:355px;height:320px;position:relative;background-color:#fff;display:none;'></div>";
  J.innerHTML=s;
  moGlb.loader.hide();
}},


tabDraw:function(){
with(moMenu_adm){

  var r,b,s,c,cc,an,rr,onm,tp, sp="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",ss="";

  s="<table class='admin' cellpadding='2px' cellspacing='2px' "+SPA+"0;top:0px;width:100%;font:12px Arial;'><tr>"+
    "<th style='width:120px;'>Label</th>"+
    "<th style='width:60px;'>Shortcut</th>"+
    "<th style='width:60px;'>Type</th>"+
    "<th style='width:30px;'>Group</th>"+
    "<th style='width:40px;'>Selected</th>"+
    "<th style='width:40px;'>Disable</th>"+
    "<th style='width:100px;'>Icon on</th>"+
    "<th style='width:100px;'>Icon off</th>"+
    "<th style='width:30px;'>Level</th>"+
    "<th style='width:120px;'>Action</th>"+
    "</tr>";

  for(r=0;r<grid.length;r++){
    c=(r&1)?"#f0f0f0":"#fff"; cc="color:#808080;cursor:default;"; tp=""; b="normal";
    ss=""; 
    if(r){ cc="cursor:pointer;";
      an=(grid[r].Parent+"").split("|");
      for(rr=0;rr<an.length;rr++) ss+=sp;
      tp=grid[r].Type;
      if(tp==1) b="bold";
      if(tp==4) ss+="-----------------";
    }
    
    onm="onmouseover='this.style.backgroundColor=\"#CCC\";' onmouseout='this.style.backgroundColor=\""+c+"\";' onclick='moMenu_adm.Action("+r+")'";
    
    s+="<tr style='background-color:"+c+"' "+onm+">"+
       "<td style='border:0;"+cc+"font-weight:"+b+";'>"+ss+grid[r].Txt+"</td>"+
       "<td style='border:0;text-align:center;'>"+(moGlb.isset(grid[r].TxtDx)?grid[r].TxtDx:'')+"</td>"+
       "<td style='border:0;text-align:center;'>"+(r?aType[tp]:"")+"</td>"+
       "<td style='border:0;text-align:center;'>"+(moGlb.isset(grid[r].Group)?grid[r].Group:'')+"</td>"+
       "<td style='border:0;text-align:center;font:16px FlaticonsStroke;color:#a1c24d;'>"+(moGlb.isset(grid[r].Status) && parseInt(grid[r].Status)?String.fromCharCode(58786):'')+"</td>"+
       "<td style='border:0;text-align:center;font:16px FlaticonsStroke;color:#a1c24d;'>"+(moGlb.isset(grid[r].Disable) && parseInt(grid[r].Disable)?String.fromCharCode(58786):'')+"</td>"+
       "<td style='border:0;text-align:center;'>"+(moGlb.isset(grid[r].Icon0)?grid[r].Icon0:'')+"</td>"+
       "<td style='border:0;text-align:center;'>"+(moGlb.isset(grid[r].Icon1)?grid[r].Icon1:'')+"</td>"+
       "<td style='border:0;text-align:center;'>"+(moGlb.isset(grid[r].Level)?grid[r].Level:'')+"</td>"+
       "<td style='border:0;text-align:center;'>"+(moGlb.isset(grid[r].Fnc)?grid[r].Fnc:'')+"</td>"+       
       "</tr>";
  }
  s+="</table>";
  
  mf$("idconttablemenuadm").innerHTML=s;
}},


Action:function(i){
with(moMenu_adm){
  var tp=grid[i].Type;
  if(btnMode<2 && !i) { moGlb.alert.show(moGlb.langTranslate("Can't 'Edit' or 'Add sibling' to Root!")); return; } 
  if(btnMode==2 && tp!=1) { moGlb.alert.show(moGlb.langTranslate("Select correct Type to add children!")); return;  }
  f_form(i);
}},



f_form:function(i){  
with(moMenu_adm) {
  var s="",j, sw='class="input_admin"';//sw="style='width:100%;border:1px solid #aaaaaa;border-shadow: none;'";
  V=["","",0,"",0,0,"","",-1,""];  // Add:  child & sibling

  if(!btnMode) with(grid[i]){  // Edit
    V[0]=Txt;
    V[1]=TxtDx;
    V[2]=moGlb.isset(Type)?Type:'';        // select 5o
    V[3]=moGlb.isset(Group)?Group:'';
    V[4]=moGlb.isset(Status)?Status:'';      // select 2o
    V[5]=moGlb.isset(Disable)?Disable:'';     // select 2o
    V[6]=moGlb.isset(Icon0)?Icon0:'';
    V[7]=moGlb.isset(Icon1)?Icon1:'';
    V[8]=Level;
    V[9]=Fnc;
  }
    V[10]=grid[i].Parent;

  s=//"<span "+SPA+"8px;top:8px;font:bold 12px Arial;'>Menu Node</span><br><br>"+
    "<table cellpadding='5' cellspacing='1' style='width:97%;font:11px Arial;'>"+
    "<tr>"+
    "<td><b>Label</b><br><input id='idTxt' type='text' value='"+V[0]+"' "+sw+"/></td>"+
    "<td><b>Shortcut</b><br><input id='idTxtDx' type='text' value='"+V[1]+"' "+sw+" /></td>"+
    "</tr>"+    
    "<tr>"+
    "<td><b>Type</b><br><select id='idType' "+sw+" onchange='moMenu_adm.f_chgseltype("+i+")'><option value='0'>plain</option><option value=1>children</option><option value=2>image</option><option value=3>radio</option><option value=4>separator</option></select></td>"+
    "<td><b>Group</b><br><input id='idGroup' type='text' value='"+V[3]+"' "+sw+" /></td>"+
    "</tr>"+    
    "<tr>"+
    "<td><b>Selected</b><br><select id='idStatus' "+sw+"><option value=0>no</option><option value=1>yes</option></select></td>"+
    "<td><b>Disable</b><br><select id='idDisable' "+sw+"><option value=0>no</option><option value=1>yes</option></select></td>"+
    "</tr>"+    
    "<tr>"+
    "<td><b>Icon on</b><br><input id='idIcon0' type='text' value='"+V[6]+"' "+sw+"/></td>"+
    "<td><b>Icon off</b><br><input id='idIcon1' type='text' value='"+V[7]+"' "+sw+" /></td>"+
    "</tr>"+    
    "<tr>"+
    "<td valign='top'><b>Level</b><br><input id='idLevel' type='text' value='"+V[8]+"' "+sw+"/></td>"+
    "<td><b>Action</b><br><textarea id='idFnc' class='input_admin'/>"+V[9]+"</textarea></td>"+
    "</tr>"+   
    "</table>"+
    "<button "+SPA+"5px;width:70px;' onclick='moMenu_adm.f_form_close()'>Cancel</button>"+
    "<button style='position:absolute;right:5px;width:70px;' onclick='moMenu_adm.f_form_save("+i+")'>Save</button>";

  if(!btnMode) s+="<button "+SPA+"80px;width:70px;' onclick='moMenu_adm.f_form_delete("+i+")'>Delete</button>";
 
//  //maskj.style.display="block";
//  popj.innerHTML=s;
//  //popj.style.display="block";
//  try { popjw.id; } catch(ex) { popjw = moComp.createComponent("maskjw_menu", "moPopup", {contentid: popj.id, title: 'Properties', width: 355, height: 320}); }
//  popjw.show(); popj.style.display = 'block';

    if(!mf$('idpopmenuadm')) { // first time
        moGlb.createElement('idpopmenuadm', 'div');
        moComp.createComponent("maskjw_menu", "moPopup", {contentid: 'idpopmenuadm', title: 'Properties', width: 355, height: 320});
    }
    mf$('idpopmenuadm').innerHTML = s;
    maskjw_menu.show();
    
  // value selects  
  if(!btnMode) {
    j=mf$("idType"); j.options[ V[2] ].selected=true;
    j=mf$("idStatus"); j.options[ V[4] ].selected=true; 
    j=mf$("idDisable"); j.options[ V[5] ].selected=true; 
  }  
}},




f_chgseltype:function(i){
with(moMenu_adm){

  var sj=mf$("idType"), v,f,n=i+1, ng=grid.length,a;
  v=parseInt(sj.value);
  if(v!=1 && n<ng){
     f=grid[i].Fcode+"";
     a=(grid[n].Parent).split("|"); 
     if(a.indexOf(f)!=-1) { 
      moGlb.alert.show(moGlb.langTranslate("Can't change Type if there are children!"));  
      sj.options[1].selected=true; 
    }
  }

}},


f_form_save:function(i){
with(moMenu_adm){

  var r,v,f,o,rw={}, ac=["Txt","TxtDx","Type","Group","Status","Disable","Icon0","Icon1","Level","Fnc","Parent"];
 
  // Edit
  if(!btnMode) {   
    for(r=0;r<10;r++) { 
      v=mf$("id"+ac[r]).value;
      grid[i][ ac[r] ] = v;
    }
  } else { // new    
    for(r=0;r<10;r++) { 
      v=mf$("id"+ac[r]).value;
      rw[ ac[r] ] = v;
    }
    FC++;
    rw["Fcode"]=FC;  
    rw["Parent"]=grid[i].Parent;
 
    if(btnMode==2) {
      f=grid[i].Fcode+"";
      if(f=="0") rw["Parent"]=f;
      else rw["Parent"]+="|"+f   
    }
    grid.splice(i+1,0,rw);
  }
  f_form_close();
  tabDraw();
}},


f_form_close:function(){
with(moMenu_adm){
//  maskj.style.display="none";
//  popj.style.display="none"; 
//    popj.innerHTML="";
//    popjw.hide();  
    maskjw_menu.hide();
}},


f_form_delete:function(i){
with(moMenu_adm){
  var r,f,s="",k=1,p,a;
  if(grid[i].Type==1) s=" and children";
  
  moGlb.confirm.show(moGlb.langTranslate("Delete selected row")+" "+s+"?", function () {    
        if(grid[i].Type==1) { 
        f=""+grid[i].Fcode; 
        for(r=i+1;r<grid.length;r++){
          p=grid[r].Parent+"";

          a=p.split("|");
          if( a.indexOf(f)==-1 ) break;
          k++;
        }    
      }
      grid.splice(i,k);    // delete children + parent 
      f_form_close();
      tabDraw();
    });
  
  
//  if(!confirm("Delete selected row"+s+"?")) return;
//  
//  if(grid[i].Type==1) { 
//    f=""+grid[i].Fcode; 
//    for(r=i+1;r<grid.length;r++){
//      p=grid[r].Parent+"";
// 
//      a=p.split("|");
//      if( a.indexOf(f)==-1 ) break;
//      k++;
//    }    
//  }
//  grid.splice(i,k);    // delete children + parent 
//  f_form_close();
//  tabDraw();
}},

Btn_overout:function(m,i){
with(moMenu_adm){

  if(btnMode==i) return false;
  var j=mf$("idbtnmoMenu_adm"+i);
  j.style.backgroundColor=m?"#A1C24D":"#f0f0f0";
  j.style.color=m?"#f0f0f0":"#888";

}},

Btn:function(i){
with(moMenu_adm){
  var r,c,b,j,c1;
  if(i==btnMode) return;
  btnMode=i; 
  for(r=0;r<3;r++){
    if(r==i) c="#f0f0f0",b="bold",c1="#D0D0D0";
    else c="#A1C24D",b="normal",c1="#f0f0f0"; 
    j=mf$("idbtnmoMenu_adm"+r);
    j.style.backgroundColor=c;
    j.style.color=c1;
    j.style.fontWeight=b;
  }
}}

} //
