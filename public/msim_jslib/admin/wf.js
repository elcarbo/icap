// workflow
var moWf = {W:0,H:0, J:null,
     workflows:[], groups:[], phases:[], exit0:"",
     NWf:0, EWf:[], noSave:0, WfLABEL:"", 
     SPA:"style='position:absolute;",
     selgroup:"", PRM:0, RW:0, INUSE:0,
     imgPath: "../../public/msim_images/default/admin/_workflow",

Start:function(a){
with(moWf){
 
  workflows=a[0];
  groups=a[1];


  W=moGlb.getClientWidth(), H=moGlb.getClientHeight();
  J=mf$("idcont");

  var r,str;

  // draw
  str="<select id='idselwf' style='position:absolute;left:0;top:0;width:190px;' onChange='moWf.editWf();'>";   // this.options[this.selectedIndex].value
      //"<option value='0'>"+moGlb.langTranslate("Choose Workflow")+"</option>";  
  for(r=0;r<workflows.length;r++) str+="<option value='"+workflows[r].f_id+"'>"+workflows[r].f_id+") "+workflows[r].f_name+"</option>"; 
  "</select>";
  mf$("idboxsel").innerHTML=str;
  
  selgroup="";
  for(r=0;r<groups.length;r++) selgroup+="<option value='"+groups[r].f_id+"'>"+groups[r].f_group+"</option>";
  
  moWf.editWf();  // init     mf$("idselwf").options[0].value
}},

editWf:function(){   // i=num wf
with(moWf){

  var sej=mf$("idselwf"), i=parseInt(sej.value);

  if(i==NWf || !i) return;
  WfLABEL=sej.options[sej.selectedIndex].text;

  if(noSave) {
        if(!confirm(moGlb.langTranslate("Exit without saving?"))) return; 
        noSave=0;
  }
    
  NWf=i;
  if(!i) {J.innerHTML="";return;}
  
  //Ajax.sendAjaxPost("readwf.php", "wfn="+NWf, "moWf.EditWf");
  Ajax.sendAjaxPost(moGlb.baseUrl+"get-wf", "wfn="+NWf, "moWf.EditWf", true);
}}, 
 
 
EditWf:function(a){  
with(moWf){
 
  try { var ar=JSON.parse(a); }catch(e){ alert(moGlb.langTranslate(a)); return; } 
 
  phases=ar[0];
  exit0=ar[1];
  INUSE=ar[2]; if(!INUSE) INUSE=0;

  var clna={}, r=0,g, n=1, ca, fn;
  
  EWf=[ {"f_name":"Creation"} ];  
  
  // create var temp
  for(r=0;r<phases.length;r++) with(phases[r]){ 
  
    fn=parseInt(f_number); if(!fn) continue;
  
        // le fasi mancanti devono essere create
        for(g=n;g<fn;g++){  
          EWf.push({          
                "f_wf_id":NWf,
                "f_number":g,
                "f_order":g,
                "f_percentage":10,
                "f_group_id":1,
                "f_name":"Phase name",
                "f_summary_name": "",
                "f_visibility":-1,
                "f_editability":-1,
                "f_exits":[]   
         });           
        }
  
   n=fn;
   clna=moArray.clone(phases[r]);
   EWf.push( clna );
   n++;
  } //
  
  Draw();
}},

Draw:function(){
with(moWf){
  var r, ne=EWf.length, str, t, f="font:12px Arial;text-align:right;",
      w=200, TP=30, geo, grp, vis, vs;
  
  // save button
  str="<div style='position:absolute;left:10px;top:10px;font:bold 12px Arial;'>Workflow: "+WfLABEL+"</div>"+
      /*"<button "+SPA+"right:200px;top:20px;width:80px;' onclick='moWf.SaveWf(0)'>"+moGlb.langTranslate("Cancel")+"</button>"+*/
      "<button "+SPA+"right:200px;top:20px;width:80px;' onclick='moWf.printWf()'>"+moGlb.langTranslate("Print WF")+"</button>"+
      //"<button "+SPA+"right:20px;top:20px;width:80px;' onclick='moWf.SaveWf(1)'>"+moGlb.langTranslate("Save WF")+"</button>"+      
      "<button "+SPA+"right:110px;top:20px;width:80px;' onclick='moWf.SaveWf(0)'>"+moGlb.langTranslate("Cancel")+"</button>"+
        "<button "+SPA+"right:20px;top:20px;width:80px;' onclick='moWf.SaveWf(1)'>"+moGlb.langTranslate("Save WF")+"</button>"+
        "<div id='idsavemsg' "+SPA+"right:20px;top:50px;font:12px Arial;color:#484;'></div>";
 
  
  str+="<div "+SPA+"left:10px;top:"+TP+"px;width:"+w+"px;height:30px;"+f+"line-height:30px;'>"+EWf[0].f_name+"</div>"+
  "<div "+SPA+"left:"+(w+40)+"px;top:"+(TP+5)+"px;width:130px;font:12px Arial;text-align:center;"+
  "height:20px;line-height:20px;border:1px solid #6A6A6A;background-color:#E6E7E8;cursor:pointer;' onclick='moWf.viewPhase0()'>"+moGlb.langTranslate("Script")+"</div>";

  for(r=1;r<ne;r++) {
  
    t=TP+40+(r-1)*90;
    str+="<div "+SPA+"left:10px;top:"+t+"px;width:"+w+"px;height:72px;"+f+"line-height:70px;'>"+EWf[r].f_name+" <b>("+r+")</b></div>";
    
    geo=(EWf[r].f_exits.length>1)?"rumble":"rectangle"; 
    grp=getGroup(EWf[r].f_group_id);
    
    vs=parseInt(grp.f_visibility);

    if(!vs) geo="ellipse";
    
    vis=imgPath+"/"+geo+"0.gif";
    str+="<img id='idgeo"+r+"' src='"+vis+"' "+SPA+"left:"+(w+40)+"px;top:"+t+"px;' onclick='moWf.viewExit("+r+");' />";


    // se inserimento
    if(r>=INUSE) str+="<img src='"+imgPath+"/add.png' "+SPA+"left:"+(w+96)+"px;top:"+(t+74)+"px;' onclick='moWf.PhAddSub("+r+",1)' />";
    else str+="<div "+SPA+"left:"+(w+99)+"px;top:"+(t+74)+"px;width:11px;height:11px;background-color:#DDD;border:1px solid #AAA;'></div>";
    
    if(r>1 && r>INUSE)  str+="<img src='"+imgPath+"/sub.png' "+SPA+"left:"+(w+16)+"px;top:"+(t+28)+"px;' onclick='moWf.PhAddSub("+r+",-1)' />";
    else str+="<div "+SPA+"left:"+(w+16)+"px;top:"+(t+28)+"px;width:11px;height:11px;background-color:#DDD;border:1px solid #AAA;'></div>";
    
    // edit exits
    str+="<img id='idnoteph"+r+"' src='"+imgPath+"/note0.png' "+SPA+"left:"+(w+176)+"px;top:"+(t+28)+"px;' onclick='moWf.viewPhaseParam("+r+")' />";
 
  }
   
    var s0="text-align:right;padding-right:8px;",rr;  
    
    
     // div per link   
    str+="<div id='idarrow' "+SPA+"left:"+(w+198)+"px;top:"+(TP+40)+";width:34px;height:"+t+"px;'></div>"+
    
    // box param
         "<div id='idparam' class='clbox' "+SPA+"left:"+(w+240)+"px;top:10px;width:512px;height:530px;display:none;'>"+
         "<span "+SPA+"left:10px;top:10px;font:bold 14px Arial;'><img src='"+imgPath+"/note1.png'>&nbsp;Fase&nbsp;<span id='idpnum'>0</span></span>"+
         
         "<span "+SPA+"left:160px;top:10px;font:12px Arial;'>Percentage: <input id='idpperc' type='text' style='width:54px;"+s0+"'> %</span>"+
         "<span "+SPA+"right:10px;top:10px;font:12px Arial;'>Group: <select id='idpgrup' style='width:106px;'>"+selgroup+"</select></span>"+
         
         "<span "+SPA+"left:10px;top:46px;font:12px Arial;'>Order: <input id='idporder' type='text' style='width:88px;"+s0+"'></span>"+
         "<span "+SPA+"left:160px;top:46px;font:12px Arial;'>Visibility: <input id='idpvisi' type='text' style='width:88px;"+s0+"'></span>"+
         "<span "+SPA+"right:10px;top:46px;font:12px Arial;'>Editability: <input id='idpedit' type='text' style='width:88px;"+s0+"' ></span>"+
         
       "<div "+SPA+"left:10px;right:10px;top:80px;height:1px;overflow:hidden;background-color:#AAA;'></div>"+
         
         "<span "+SPA+"left:10px;top:95px;font:12px Arial;'>"+moGlb.langTranslate("Phase Name:")+"</span>"+
         "<input id='idpphnm' type='text' "+SPA+"left:150px;top:92px;width:350px;' />"+
         "<span "+SPA+"left:10px;top:125px;font:12px Arial;'>"+moGlb.langTranslate("Summary Category:")+"</span>"+
         "<input id='idsumnm' type='text' "+SPA+"left:150px;top:122px;width:350px;' />"+
         
         "<div "+SPA+"left:10px;right:10px;top:155px;height:331px;overflow:auto;background-color:#FFF;border:1px solid #AAA;border-radius:5px;'>"+
       
          "<span "+SPA+"left:8px;top:5px;font:bold 11px Arial;'>"+moGlb.langTranslate("N.")+"</span>"+                                                   //##
          "<span "+SPA+"left:32px;top:5px;font:bold 11px Arial;'>"+moGlb.langTranslate("Description")+"</span>"+
          "<span "+SPA+"left:316px;top:5px;font:bold 11px Arial;'>"+moGlb.langTranslate("Visib.")+"</span>"+
          "<span "+SPA+"left:370px;top:5px;font:bold 11px Arial;'>"+moGlb.langTranslate("Exit")+"</span>"+
          "<span "+SPA+"left:406px;top:5px;font:bold 11px Arial;'>"+moGlb.langTranslate("Script")+"</span>";
       
       for(r=0;r<20;r++){rr=r+1;
       
        str+="<span "+SPA+"left:1px;top:"+(28+r*30)+"px;width:20px;text-align:right;font:12px Arial;'>"+((rr<10)?("0"+rr):rr)+"</span>"+                    
             "<input id='idptbdes"+r+"' type='text' "+SPA+"left:32px;top:"+(24+r*30)+"px;width:278px;' />"+                                                             
             "<input id='idptbvis"+r+"' type='text' "+SPA+"left:316px;top:"+(24+r*30)+"px;width:48px;text-align:right;' value='-1' />"+                           //##
             "<input id='idptbext"+r+"' type='text' "+SPA+"left:370px;top:"+(24+r*30)+"px;width:28px;text-align:right;' />"+
             "<button id='idptbbut"+r+"' "+SPA+"left:406px;top:"+(24+r*30)+"px;width:56px;padding:2px;' onclick='moWf.EditScript("+r+")'>Create</button>"+
             "<textarea id='idpscripthidd"+r+"' style='display:none;'></textarea>";
       }

       str+="</div>"+ 
        
         "<button "+SPA+"left:10px;bottom:10px;width:80px;' onclick='moWf.viewParamClose()'>"+moGlb.langTranslate("Cancel")+"</button>"+
         "<button "+SPA+"right:10px;bottom:10px;width:80px;' onclick='moWf.viewParamSave()'>"+moGlb.langTranslate("Save")+"</button>"+
         "</div>";


      // box edit script
      str+="<div id='idpmask' "+SPA+"left:0;top:0;width:100%;height:100%;background-color:#FFF;opacity:0.7;display:none;'></div>"+
      "<div id='idpscrp' class='clbox' "+SPA+"left:10%;top:10%;width:80%;height:80%;display:none;'>"+
        "<div "+SPA+"left:10px;top:10px;font:bold 14px Arial;'>Edit Script</div>"+
        "<div "+SPA+"left:10px;top:32px;right:10px;bottom:40px;'>"+
        "<textarea  id='idscript' "+SPA+"left:0;top:0;width:100%;height:100%;font:14px Arial;padding:8px;'></textarea></div>"+
      
        "<button "+SPA+"left:10px;bottom:10px;width:80px;' onclick='moWf.f_scriptClose(0)'>"+moGlb.langTranslate("Cancel")+"</button>"+
        "<button "+SPA+"right:10px;bottom:10px;width:80px;' onclick='moWf.f_scriptClose(1)'>"+moGlb.langTranslate("Save")+"</button>"+
      "</div>";



  J.innerHTML=str;

}},




PhAddSub:function(i,m){     //   i=phase   m: 1 | -1  mode
with(moWf){
var r,g,x,a,fn;

if(m<0)  {if(!confirm(moGlb.langTranslate("Confirm Deleting?"))) return;}
    // move exits and phase number!
    for(r=1;r<EWf.length;r++){    
      
      fn=parseInt(EWf[r].f_number);
      if(r>i) EWf[r].f_number=fn+m;
      
   //   alert("r: "+r+" | i: "+i+" | m: "+m+"\n\n"+EWf[r].f_number)
              
      for(g=0;g<EWf[r].f_exits.length;g++) { 
            x=parseInt(EWf[r].f_exits[g].f_exit);
            if(x>i) {x+=m;EWf[r].f_exits[g].f_exit=x;}   
    }}



if(m>0) {a={"f_wf_id":NWf,   // add
             "f_number":i+1,
             "f_order":i+1, //added by Quintino
             "f_percentage":10,
             "f_group_id":1,
             "f_name":"Phase name",
             "f_visibility":-1,
             "f_editability":-1,
             "f_exits":[]   
            };

    EWf.splice(i+1,0,a);           

} else EWf.splice(i,1);     // sub


noSave=1;
Draw(); // ridraw

}},


viewExit:function(i){
with(moWf){

  var r,n, ne=EWf.length, ig,sr,jg, ja, str, oi=i-1, ii=0, E,phin, mx=i, min=i;
 
  for(r=1;r<ne;r++) { 
        jg=mf$("idgeo"+r);
        ig=jg.src;
        n=ig.length;
        sr=ig.substr(-5);
        jg.src=ig.substr(0,n-5)+((r==i)?"1.gif":"0.gif");
  }


  
  ja=mf$("idarrow"); 
  ja.innerHTML="";
  
  if(!i) return;
  
  var deltay=31;                                      //##
  
  E=EWf[i].f_exits;
  if(!E.length) return;
 
  str="<img src='"+imgPath+"/arw_out.gif' "+SPA+"left:0;top:"+(oi*90+deltay)+"px;' />"+                   //##
                   "<div class='clarwo' style='top:"+(oi*90+deltay+4)+"px;'></div>";          //##
  
  
  for(r=0;r<E.length;r++){
    phin=E[r].f_exit;if(typeof(phin)=="undefined") continue;
    phin=parseInt(phin);
    if(phin>mx) mx=phin;
    ii=phin-1;
    if(phin<min) min=phin;
    
    str+="<img src='"+imgPath+"/arw_in.gif' "+SPA+"left:0;top:"+(ii*90+deltay)+"px;' />"+           //##
                   "<div class='clarwo' style='top:"+(ii*90+deltay+4)+"px;'></div>";        //##
  }
  
 
  
   mx--;min--;  //   from  min   to  max  
          str+="<div class='clarwv' style='top:"+(min*90+deltay+4)+"px;height:"+( (mx-min)*90 )+"px;'></div>";      //##
  

  ja.innerHTML=str;
 
}},

/*
      EWf.push({          
                "f_wf_id":NWf,
                "f_number":g,
                "f_percentage":10,
                "f_group_id":1,
                "f_name":"Phase name",
                "f_visibility":-1,
                "f_editability":-1,
                "f_exits":[]  
*/



viewPhase0:function(){
with(moWf){

  viewParamClose();
  PRM=0;
  EditScript();  

}},



 


viewPhaseParam:function(i){
with(moWf){

  PRM=i;
  viewExit(i);

  var dt=parseInt(J.scrollTop), paj=mf$("idparam");
 
  paj.style.top=(dt+10)+"px";

  noteColor(i);
  var r,b,g,sj;

  mf$("idpnum").innerHTML=EWf[i].f_number;
  mf$("idpperc").value=EWf[i].f_percentage;
  
  sj=mf$("idpgrup");g=EWf[i].f_group_id;
  for(r=0;r<sj.options.length;r++) {b=parseInt(sj.options[r].value); 
  if(b==g) {sj.options[r].selected=true;break;}}

  mf$("idpvisi").value=EWf[i].f_visibility;
  mf$("idpedit").value=EWf[i].f_editability;
  mf$("idporder").value=EWf[i].f_order;
    
  mf$("idpphnm").value=EWf[i].f_name;
  mf$("idsumnm").value=EWf[i].f_summary_name;
  
  // exits
  for(r=0;r<20;r++) {
  
    mf$("idptbdes"+r).value="";
    mf$("idptbext"+r).value="";
    mf$("idptbvis"+r).value="-1";                                                       //##
    mf$("idpscripthidd"+r).value="";
    mf$("idptbbut"+r).childNodes[0].nodeValue="Create";  
  }
  
  for(r=0;r<EWf[i].f_exits.length;r++) with(EWf[i].f_exits[r]){
  
    mf$("idptbdes"+r).value=f_description;
    mf$("idptbext"+r).value=f_exit;
    
    if(typeof(EWf[i].f_exits[r].f_visibility)=="undefined") EWf[i].f_exits[r].f_visibility=-1;    //##
    mf$("idptbvis"+r).value=f_visibility;                                                   //##
    
    mf$("idpscripthidd"+r).value=f_script;
    
    b="Edit";if(!f_script) b="Create";  
    mf$("idptbbut"+r).childNodes[0].nodeValue=b;
  }


  paj.style.display="block";

}},



noteColor:function(i){
with(moWf){
  var r,ne=EWf.length;
  for(r=1;r<ne;r++) mf$("idnoteph"+r).src=""+imgPath+"/note"+((r==i)?1:0)+".png";
}},



viewParamClose:function(){
with(moWf){
  noteColor(-1);
  mf$("idparam").style.display="none";
}},





viewParamSave:function(){
with(moWf){

  var r,g,i=PRM, v0,v1,v2,v3, ae=[];
  
  v0=mf$("idpperc").value;
  if(!isNum(v0)) {alert(moGlb.langTranslate("field error! (Percentage)"));return;}
  EWf[i].f_percentage=v0;
  
  sj=mf$("idpgrup");EWf[i].f_group_id=sj.value;
 

  v0=mf$("idpvisi").value;
  if(!isNum(v0)) {alert(moGlb.langTranslate("field error! (Visibility)"));return;}
  EWf[i].f_visibility=v0;
  
  v0=mf$("idporder").value;
  if(!isNum(v0)) {alert(moGlb.langTranslate("field error! (Order)"));return;}
  EWf[i].f_order=v0;
   
  v0=mf$("idpedit").value;
  if(!isNum(v0)) {alert(moGlb.langTranslate("field error! (Editability)"));return;}
  EWf[i].f_editability=v0;  
    
  v0=mf$("idpphnm").value;
  if(!v0) {alert(moGlb.langTranslate("field error! (Phase name)"));return;}
  EWf[i].f_name=v0;
  
  v0=mf$("idsumnm").value;  
  EWf[i].f_summary_name=v0;
  
  


  // exits

  for(r=0;r<20;r++) {
    v0=mf$("idptbdes"+r).value;
    v1=mf$("idptbext"+r).value;
    v2=mf$("idpscripthidd"+r).value;
    v3=parseInt(mf$("idptbvis"+r).value);                                                                         //##
    
    if(!v0 || !v1 || !v3) {EWf[i].f_exits=ae;break;}

    if(!isNum(v1)) { alert(moGlb.langTranslate("field error! (Exit")+" "+(r+1)+")"+v1); return;}
    if(v1>=EWf.length) { alert(moGlb.langTranslate("Exit value too big!")); return; }
  

  ae.push({f_description:v0, f_exit:v1, f_script:v2, f_visibility:v3});                                     //##
    
  }
  
  noSave=1;
  viewExit(0);
  viewParamClose();
  Draw();
}},









EditScript:function(rw){    // PRM    rw    editor script exit   |  m=1    exit0;
with(moWf){

  var v; 
  if(!PRM) v=exit0;
  else {
    RW=rw;
    v=mf$("idpscripthidd"+rw).value;     
  }
  

  mf$("idscript").value=Base64.decode(v);
  mf$("idpmask").style.display="block";   
  mf$("idpscrp").style.display="block";

}},



f_scriptClose:function(m){  // 0 cancel    1 save
with(moWf){

  var b, c=Base64.encode(mf$("idscript").value);

  if(m) {if(PRM) mf$("idpscripthidd"+RW).value=c;
          else {exit0=c;noSave=1;}
  }
    
  if(PRM) {b="Edit";if(!c) b="Create";  
            mf$("idptbbut"+RW).childNodes[0].nodeValue=b;}
  
  
  mf$("idpmask").style.display="none";   
  mf$("idpscrp").style.display="none";
}},




SaveWf:function(i){  // i:  0 Cancel    1 Save
with(moWf){

  if(i) { // save
      
      //mf$("idsavemsg").innerHTML=moGlb.langTranslate("Workflow saved");
      
      var ewf=moArray.clone(EWf);
      ewf.shift();
      var par={"ext0":exit0, "phases":ewf, "wfn":NWf, "mode":"update"};
      //Ajax.sendAjaxPost("savewf.php", "par="+JSON.stringify(par), "moWf.Saved");
      Ajax.sendAjaxPost(moGlb.baseUrl+"save-wf", "par="+JSON.stringify(par), "moWf.Saved", true);
  } else {
  
    NWf=0;PRM=0;exit0="";EWf=[];noSave=0;J.innerHTML="";
    
  } 
  
   
}},

printWf: function (o) {
    with(moWf) {
        if(o) {
            o = JSON.parse(o);
            if(moGlb.isset(o.error)) moGlb.alert.show(moGlb.langTranslate(o.error));
        } else {
            var ewf = moArray.clone(EWf);
            ewf.shift();	
            var par = {wfn: NWf};console.dir(NWf);
            Ajax.sendAjaxPost(moGlb.baseUrl+"print-wf", "par="+JSON.stringify(par), "moWf.printWf", true);
			var url = moGlb.baseUrl+"print-wf/wf_id/";
			window.open(url+NWf, '_blank');
        }
    }
},


Saved:function(a){
with(moWf) {
  //if(a!="ok"){alert(a);}
  //setTimeout("mf$('idsavemsg').innerHTML='';",2000);
  
  noSave=0;
  
    var res = JSON.parse(a);
    if(moGlb.isset(res.message)) mf$("idsavemsg").innerHTML=moGlb.langTranslate(res.message); // Message
    else if(moGlb.isset(res.error)) alert(moGlb.langTranslate(res.error)); // Error
}},




createWf:function(){
with(moWf){

   var nm = prompt(moGlb.langTranslate("Insert Workflow Name:"), "");
   if(!nm) { alert(moGlb.langTranslate("Insert Name!")); return; }
    Ajax.sendAjaxPost(moGlb.baseUrl+"create-wf", "f_name="+nm, "moWf.Refresh", true);
   
//   NWf=-999;
//   
//   var ewf=[
//   
//              {"f_wf_id":NWf,
//                "f_number":1,
//                "f_percentage":10,
//                "f_group_id":1,
//                "f_name":"Open",
//                "f_visibility":-1,
//                "f_editability":-1,
//                "f_exits":[
//                            {"f_description":"Close", "f_exit":2, "f_script":"", "f_visibility":-1}, 
//                            {"f_description":"Delete", "f_exit":3, "f_script":"", "f_visibility":-1}  
//                          ] 
//              },
//              
//              {"f_wf_id":NWf,
//                "f_number":2,
//                "f_percentage":100,
//                "f_group_id":6,
//                "f_name":"Close",
//                "f_visibility":-1,
//                "f_editability":-1,
//                "f_exits":[] 
//              },
//              
//              {"f_wf_id":NWf,
//                "f_number":3,
//                "f_percentage":0,
//                "f_group_id":7,
//                "f_name":"Delete",
//                "f_visibility":-1,
//                "f_editability":-1,
//                "f_exits":[] 
//              }         
//   ];
//
//
//   var par={"ext0":"", "phases":ewf, "wfn":NWf, "mode":"create", "wfname":nm};
//      //Ajax.sendAjaxPost("savewf.php", "par="+JSON.stringify(par), "moWf.Refresh");
//      Ajax.sendAjaxPost(moGlb.baseUrl+"save-wf", "par="+JSON.stringify(par), "moWf.Refresh");
}},


deleteWf:function(){
with(moWf){

  var sej=mf$("idselwf"), i=parseInt(sej.value), nm=sej.options[sej.selectedIndex].text;

  if(!i) return;
  
  if(noSave) {alert(moGlb.langTranslate("Save the Workflow before deleting!"));return;}  
  if(!confirm(moGlb.langTranslate("Confirm Deleting of Workflow:")+" "+nm+"?")) return; 
        

  NWf=i;
  var par={"ext0":"", "phases":[], "wfn":NWf, "mode":"delete"};
  //Ajax.sendAjaxPost("savewf.php", "par="+JSON.stringify(par), "moWf.Refresh");
  Ajax.sendAjaxPost(moGlb.baseUrl+"delete-wf", "wf="+i, "moWf.Refresh", true);
}},




Refresh: function (a) {
    with(moWf) {
        NWf=0;PRM=0;exit0="";EWf=[];noSave=0;J.innerHTML=""; 
        var res = JSON.parse(a); if(!res) return;
        if(moGlb.isset(res.message)) alert(moGlb.langTranslate(res.message)); // Message
        if(moGlb.isset(res.action) && res.action == "del") { // after deleting            
            for(var i in mf$("idselwf").options) {
                if(mf$("idselwf").options[i].value == parseInt(res.wf)) {
                    mf$("idselwf").remove([i]);
                    mf$("idselwf").selectedIndex = 0;
                    moWf.editWf(mf$("idselwf").options[0].value);
                    break;
                }
            }
        } else if(moGlb.isset(res.action) && res.action == "add") { // after deleting            
            mf$("idselwf").options.add(new Option(res.label, res.value));
            mf$("idselwf").selectedIndex = mf$("idselwf").options.length - 1;
            moWf.editWf(res.value);
        }
    }
},



getGroup:function(i){
with(moWf){

  var v,r;
  for(r=0;r<groups.length;r++) {
    v=parseInt(groups[r].f_id);
    if(v==i) return groups[r];  
  }

  return false;
}}



} // end moWf