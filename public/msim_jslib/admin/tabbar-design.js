// JavaScript Document

/*
TB={"tabs":[ {"label":"Start Center","layout":"lyt1","level":1},
            {"label":"Gestione RIT","layout":"lyt2","level":1},
            {"label":"Flotta","layout":"lyt3","level":1},
            {"label":"Cage","layout":"lyt7","level":1},
            {"label":"Manutenzioni Programmate","level":1,"disable":true},
            {"label":"Gestione Dati del Campo","level":1,"disable":true},
            {"label":"Utenti","layout":"lyt6","level":1}
          ]
};

*/
// TODO MODIFY IMG PATH
var moTabbar_adm = {J:null, Psz:[], IM:"",
                 f:'font:11px Arial;color:#000;',ff:"style='font:13px Arial;cursor:pointer;'", fb:"",
                 atb:[], Tbj:[], nT:0, aSel:[],
                 maskj:null, maskjw:null,ismain: false,                
                
Edit:function(S, s, main){
  with(moTabbar_adm) {
    moGlb.loader.show();
    ismain = main;
    J=mf$(s);Psz=moGlb.getPosition(J);

    if(moGlb.isempty(S)) jso={"tabs":[]};else {jso=JSON.parse(S);}
    
    Tbj=moGlb.cloneArray(jso.tabs);
    nT=Tbj.length;

    resize();
}},


resize:function(){
  with(moTabbar_adm){ 
    if(!IM) IM = "../public/msim_images/default/admin/tabs/";
  Psz=moGlb.getPosition(J); /*Psz.h = moGlb.getClientHeight();  */
   J.innerHTML = "<table cellpadding='2px' cellspacing='2px' style='width:350px'><tr>"+
    "<td><button "+ff+" title='Up' onclick='moTabbar_adm.UpDwn(-1)' style:'width:80px;' onmouseover='moTabbar_adm.mouse_over(this,true)' onmouseout='moTabbar_adm.mouse_over(this,false)'><span style='font:16px FlaticonsStroke;'>"+String.fromCharCode(58788)+"</span></button></td>"+
    "<td><button "+ff+" title='Down' onclick='moTabbar_adm.UpDwn(1)' style:'width:80px;' onmouseover='moTabbar_adm.mouse_over(this,true)' onmouseout='moTabbar_adm.mouse_over(this,false)'><span style='font:16px FlaticonsStroke;'>"+String.fromCharCode(58789)+"</span></button></td>"+
    "<td><button "+ff+" title='New' onclick='moTabbar_adm.ClickV(-1)' style:'width:80px;' onmouseover='moTabbar_adm.mouse_over(this,true)' onmouseout='moTabbar_adm.mouse_over(this,false)'><span style='font:16px FlaticonsStroke;'>"+String.fromCharCode(58378)+"</span></button></td>"+
    "<td><button "+ff+" title='Delete' onclick='moTabbar_adm.Deleterow()' style:'width:80px;' onmouseover='moTabbar_adm.mouse_over(this,true)' onmouseout='moTabbar_adm.mouse_over(this,false)'><span style='font:16px FlaticonsStroke;'>"+String.fromCharCode(58827)+"</span></button></td></tr></table>"+
    "<div id='idtb_tabbar' style='position:absolute;left:0;top:40px;width:"+(Psz.w-2)+"px;height:"+(Psz.h-40)+"px;overflow:auto;'></div>";
    
  if(!maskj) {
        maskj=moGlb.createElement("maskly_tabbar","div",J);
        with(maskj.style) display='none', padding="5px";
    }
  Draw();
}},


Draw:function(){
  with(moTabbar_adm){

  fb="font:11px Arial;background:url("+IM+"headtab.png) right top;text-align:left;height:24px;";

  var ly, ft, onm, bk, s="<table class='admin'><tr>"+
        "<th style='width:24px;'><img src='"+IM+"check0.gif' style='cursor:pointer;' onclick='moTabbar_adm.deSel()' title='deselect' /></th>"+
        "<th style='width:40px;'>Order</th>"+
        "<th style='width:350px;'>Group</th>"+
        "<th style='width:250px;'>Icon</th>"+
        "<th style='width:350px;'>Label</th>"+
        "<th style='width:250px;'>Layout</th>"+
        "<th style='width:50px;'>Level</th>"+
        "<th style='width:150px;'>Type</th>"+
        "<th style='width:150px;'>Selector</th>"+
        "<th style='width:150px;'>Workflow</th>"+
        "<th style='width:50px;'>Disable</th>"+
    "</tr>";
 
  for(var r=0;r<nT;r++) {
  bk=(r&1)?"#f8f8f8":"#fff";
    
  if(!Tbj[r].disable) Tbj[r].disable=false;
  ly=Tbj[r].layout;
  if(!ly) ly="";
  
  onm="onmouseover='this.style.backgroundColor=\"#e2e2e2\"' onmouseout='this.style.backgroundColor=\""+bk+"\"'";
  
  s+="<tr style='background-color:"+bk+";' "+onm+" onclick='moTabbar_adm.ClickV("+r+");'>"+
  "<td onclick='moTabbar_adm.ClickC("+r+"); moEvtMng.cancelEvent(event); return false;' id='idimgseltab"+r+"' style='cursor:pointer;font: 14px FlaticonsStroke;' class='off'>"+String.fromCharCode(58783)+"</td>"+
  "<td>"+r+"</td>"+
  "<td>"+(moGlb.isset(Tbj[r].group)?Tbj[r].group:'')+"</td>"+
  "<td>"+(moGlb.isset(Tbj[r].icon)?Tbj[r].icon:'')+"</td>"+
  "<td>"+(moGlb.isset(Tbj[r].label)?Tbj[r].label:'')+"</td>"+
  "<td>"+ly+"</td>"+
  "<td>"+(moGlb.isset(Tbj[r].level)?Tbj[r].level:"")+"</td>"+
  "<td>"+(moGlb.isset(Tbj[r].v_type)?Tbj[r].v_type:"")+"</td>"+
  "<td>"+(moGlb.isset(Tbj[r].v_selector)?Tbj[r].v_selector:"")+"</td>"+
  "<td>"+(moGlb.isset(Tbj[r].v_wf)?Tbj[r].v_wf:"")+"</td>"+
  "<td>"+Tbj[r].disable+"</td>"+
  "</tr>";
  }

 s+="</table>";

   mf$("idtb_tabbar").innerHTML=s;
   moGlb.loader.hide();
}},






ClickV:function(i){
  with(moTabbar_adm){

 var /*p="width:160px;border:1px solid #aaa;background-color:#fff;"+f,*/ c="cursor:pointer;", d="style='cursor:pointer;'";
  
  if(i>=0) { 
  
  var grp="";if(moGlb.isset(Tbj[i].group) && !moGlb.isempty(Tbj[i].group)) grp=Tbj[i].group;
  var icon="";if(moGlb.isset(Tbj[i].icon) && !moGlb.isempty(Tbj[i].icon)) icon=Tbj[i].icon;
  var lb="";if(Tbj[i].label) lb=Tbj[i].label;
  var ly="";if(Tbj[i].layout) ly=Tbj[i].layout;
  var le=Tbj[i].level;
  var te="";if(moGlb.isset(Tbj[i].v_type) && !moGlb.isempty(Tbj[i].v_type)) te=Tbj[i].v_type;
  var se="";if(moGlb.isset(Tbj[i].v_selector) && !moGlb.isempty(Tbj[i].v_selector)) se=Tbj[i].v_selector;
  var we="";if(moGlb.isset(Tbj[i].v_wf) && !moGlb.isempty(Tbj[i].v_wf)) we=Tbj[i].v_wf;
  var dis=(Tbj[i].disable)?1:0;
  } else {
  var lb="", ly="", le=-1, dis=0, grp = '', icon = '', te='', se='', we='';
  }         
  
    if(!maskj) {
        maskj = moGlb.createElement("maskly_tabbar","div",J);
        with(maskj.style) display='none', padding="5px";
    }

//  if(maskj.innerHTML == '') {
//    maskj.innerHTML="<table cellspacing='3' cellpadding='3'><tr><th>Group</th>"+

//  if(maskj.innerHTML=='') { maskj.innerHTML="m";
  
    var Sm="<table cellspacing='3' cellpadding='3'><tr><th>Group</th>"+
    "<td><input id='tbinpg' type='text' value='"+grp+"' class='input_admin' /></td>"+
    "<th>Icon</th>"+
    "<td><input id='tbinpicon' type='text' value='"+icon+"' class='input_admin' /></td></tr>"+
    "<tr><th>Label</th>"+
    "<td><input id='tbinp0' type='text' value='"+lb+"' class='input_admin' /></td>"+    
    "<th>Layout</th>"+
    "<td><input id='tbinp1' type='text' value='"+ly+"' class='input_admin' /></td></tr>"+    
    "<tr><th>Level visibility</th>"+
    "<td><input id='tbinp2' type='text' value='"+le+"' class='input_admin' /></td>"+
    "<th>Type visibility</th>"+
    "<td><input id='tbinp3' type='text' value='"+te+"' class='input_admin' /></td></tr>"+
    "<tr><th>Selector visibility</th>"+
    "<td><input id='tbinp4' type='text' value='"+se+"' class='input_admin' /></td>"+
    "<th>Workflow visibility</th>"+
    "<td><input id='tbinp5' type='text' value='"+we+"' class='input_admin' /></td></tr>"+
    "<tr><td colspan='2'><img id='idimgdis0' src='"+IM+"check"+dis+".gif' onclick='moTabbar_adm.chgImgchk(this)' "+d+" /> Disable</td></tr>"+
    "<tr><td><button style='font:bold 12px Arial;color:#fff;width:45%;position:absolute;left:5px;bottom:5px;"+c+"' onclick='moTabbar_adm.closeClickV(event);'>Cancel</button></td>"+
    "<td><button style='font:bold 12px Arial;color:#fff;width:45%;position:absolute;right:5px;bottom:5px;"+c+"' onclick='moTabbar_adm.applyClickV(event,"+i+");'>Apply</button></td></tr>";

    var w=470, h=210;
    maskjw = moComp.createComponent("maskjw_tabbar", "moPopup", {contentid: maskj.id, title: 'Properties', width: w, height: h},true);
//  }
    
     maskjw.show();
     mf$("maskly_tabbar_win_bd").innerHTML=Sm;   
}},



chgImgchk:function(ij){
  with(moTabbar_adm){

  var a=ij.src, ii=1;
  if(a.indexOf("check1")>-1) ii=0;
  
   ij.src=IM+"check"+ii+".gif";
   return ii?0:1; 
}},


applyClickV:function(e,i){
  with(moTabbar_adm){
      var grp=mf$("tbinpg").value, icon = mf$("tbinpicon").value;
      if(!grp) grp = '';
      if(!icon) icon = '';
      var te=mf$("tbinp3").value, se=mf$("tbinp4").value, we=mf$("tbinp5").value;
      if(!te) te = ''; if(!se) se = ''; if(!we) we = '';

 var m0=mf$("tbinp0").value;
 if(!ismain && !m0) {moGlb.alert.show(moGlb.langTranslate("Label is required!"));return;}
 
 var m1=mf$("tbinp1").value;
 if(!ismain && !m1) {moGlb.alert.show(moGlb.langTranslate("Layout is required!"));return;}

 var v=mf$("tbinp2").value;if(v=="") {moGlb.alert.show(moGlb.langTranslate("Level is required!"));return;}
 v=parseInt(v);if(v<-1)  {moGlb.alert.show(moGlb.langTranslate("Level must be >= -1"));return;} 

 if(i<0) {Tbj.push({});i=nT;nT++;}

Tbj[i].group=grp;
Tbj[i].icon=icon;
 Tbj[i].label=m0;Tbj[i].layout=m1;
 Tbj[i].level=v;Tbj[i].v_type=te;Tbj[i].v_selector=se;Tbj[i].v_wf=we;
 Tbj[i].disable=chgImgchk(mf$("idimgdis0"))?true:false;

 closeClickV(e);
 Draw();
}},


closeClickV:function(e){
with(moTabbar_adm){
   moEvtMng.cancelEvent(e);
   mf$("maskly_tabbar_win_bd").innerHTML = '';
   maskjw.hide();  
}},


Deleterow:function(){
  with(moTabbar_adm){  
      
  if(!aSel.length) {
    moGlb.alert.show(moGlb.langTranslate("Select one element!"));
  }else {
    moGlb.confirm.show(moGlb.langTranslate("Confirm the deletion?"), function () {      
      var r=nT-1;
      for(;r>=0;r--){
        if(aSel.indexOf(r)>-1) { Tbj.splice(r,1); nT--; }
      }
        deSel();
        Draw();      
    });
  }

}},



ClickC:function(i){
  with(moTabbar_adm) {
    i=+i;  
    with(mf$("idimgseltab"+i)) {
        var r,select=(className=='off');      
        if(select) {
            className='on';
            aSel.push(i);
        } else {
            for(r=0;r<aSel.length;r++) { 
              if(aSel[r]==i) { aSel.splice(r,1); className='off'; break; } 
            }
        }
         innerHTML = String.fromCharCode( (className=='off')?58783:58826 );
    }        
}},






UpDwn:function(d){
  with(moTabbar_adm){

  if(aSel.length!=1) {
    moGlb.alert.show(moGlb.langTranslate("Select one element!"));
    return;
  }
  
  if(d<0 && !aSel[0]) return;
  if(d>0 && aSel[0]==(nT-1)) return;
  
  var sj=Tbj.splice(aSel[0],1);
  
  if(d<0){ 
    Tbj.splice(aSel[0]-1,0,sj[0]); aSel[0]--;  
  } else { 
    Tbj.splice(aSel[0]+1,0,sj[0]);aSel[0]++;
  }
  
  Draw();
  d=aSel[0];
  deSel();
  ClickC(d);
}},



deSel:function(){
  with(moTabbar_adm){
    for(var r=0;r<nT;r++) {
        with(mf$("idimgseltab"+r)) {
            className='off';
            innerHTML = String.fromCharCode(58783);
        }
    }
    aSel=[];
}},

mouse_over:function(val,over){
    val.style.backgroundColor = over?"#f0f0f0":"#A1C24D";
},

Save:function(){
  with(moTabbar_adm){
  var a={"tabs":Tbj}, a=JSON.stringify(a);
                                                                                   // alert.show(a)
  return a;
}}

} //  end