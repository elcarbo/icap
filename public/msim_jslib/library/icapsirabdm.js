
var btn_wo_comment_set_before_fn = function() {
	setTimeout(function() {
		if(moGlb.user.level == 2) {
			var comments = mdl_wo_comment_tg_tg.Ypos
			for(var i in comments) {
				if(comments[i].fcode < 0) {
					document.querySelector('#slct_wo_phases_comp').removeChild(document.querySelector('#slct_wo_phases_comp').lastChild)
				}
			}
		}
	}, 1000);
}

var btn_wo_comment_remove_after_fn = function() {
	if(moGlb.user.level == 2) {
		var comments = mdl_wo_comment_tg_tg.Ypos
		var c = 0
		for(var i in comments) {
			if(moGlb.user.level == 2 && comments[i].fcode < 0) {
				++c
			}
		}
		if(c == 0) {
			var op = new Option('Approvato', 3);
			document.querySelector('#slct_wo_phases_comp').options[document.querySelector('#slct_wo_phases_comp').options.length] = op
		}
	}
}

moLibrary.response_days = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), open = eval('txtfld_' + mod + '_edit_opening_date').get('int'),
                start = eval('txtfld_' + mod + '_edit_starting_date').get('int'),
                end = eval('txtfld_' + mod + '_edit_ending_date').get('int')/*, rd = Math.floor((start-open)/(60*60))*/;
        var rd = Math.round((start - open) / (60 * 60));
        if (start < open && start != 0) {
           // moGlb.alert.show(moGlb.langTranslate('Starting date cannot come before opening date'));
           // eval('txtfld_' + mod + '_edit_response_days').set('');
            //eval('txtfld_' + mod + '_edit_starting_date').set('');
        } else if (start > open)
            eval('txtfld_' + mod + '_edit_response_days').set(rd);
        if (start > end && end != 0) {
            moGlb.alert.show(moGlb.langTranslate('Starting date cannot come after ending date'));
            eval('txtfld_' + mod + '_edit_starting_date').set('');
            eval('txtfld_' + mod + '_edit_response_days').set('');
        }
        eval('txtfld_' + mod + '_edit_starting_date').setProperties({m_on: end != 0});
        if (start == '')
            eval('txtfld_' + mod + '_edit_response_days').set('');
    }
};

moLibrary.check_start_end_date_planned = function (o) {
    var mod = moLibrary.getModule(o.name);

    var dp = eval('txtfld_' + mod + '_edit_planned_data_pianificata').get('int') || 0;
    var od = eval('txtfld_' + mod + '_edit_opening_date').get('int') || 0;
    var ed = eval('txtfld_' + mod + '_edit_planned_ending_date').get('int') || 0;

    with (o) {
        if ( dp && (dp < od) ) {
            var old = getOldValue();
            moGlb.alert.show(moGlb.langTranslate('La Data Pianificata non può precedere la Data Richiesta/Generazione'));
            setProperties({value: old, description: (old ? old : '')});
        }
        else   if ( ed && ( dp > ed ) ) {
            var old = getOldValue();
            moGlb.alert.show(moGlb.langTranslate('La Data Scadenza non può precedere la Data Pianificata'));
            setProperties({value: old, description: (old ? old : '')});
        }
    }
};


 moLibrary.txtfld_wo_edit_ending_date_onchange = function (o) {
    with (o) {
        moLibrary.check_start_end_date(o);
        var mod = moLibrary.getModule(o.name), open = eval('txtfld_' + mod + '_edit_opening_date').get('int'),
                start = o.get('int');


        if (start < open && start != 0) {
        } else if (start > open) {
            var rd = moLibrary.get_number_of_days(open, start);
            eval('txtfld_' + mod + '_edit_resolution_days').set(rd);
        } else if (start == 0) {
            eval('txtfld_' + mod + '_edit_resolution_days').set('');
        }
    }
};

//moLibrary.pck_wo_edit_cdc_onchange = function (o) {    
//    var cdc = pck_wo_edit_cdc.get(),
//        commessa = pck_wo_edit_commessa.get();
//        if ((cdc != null && cdc != '' && cdc !=0) && (commessa != null && commessa != '' && commessa != 0)) {
//            moGlb.alert.show('Hai già selezionato la Commessa'); 
//            pck_wo_edit_cdc.removeAll();
//        }
//};

//moLibrary.pck_wo_edit_commessa_onchange = function (o) {
//    var cdc = pck_wo_edit_cdc.get(),
//        commessa = pck_wo_edit_commessa.get();
//        if ((cdc != null && cdc != '' && cdc !=0) && (commessa != null && commessa != '' && commessa != 0)) {
//            moGlb.alert.show('Hai già selezionato il Cdc');
//            pck_wo_edit_commessa.removeAll();
//        }
//};

moLibrary.chkbx_wo_edit_permesso_onchange = function (o) {
    var permesso = chkbx_wo_edit_permesso.get();

        if (permesso=='si') {
        txtfld_wo_edit_data_emissione_permesso.setProperties({m_on: true});
        }else{
        txtfld_wo_edit_data_emissione_permesso.setProperties({m_on: false});
        }
};

moLibrary.chkbx_wo_edit_permesso_onload = function (o) {
    moLibrary.chkbx_wo_edit_permesso_onchange(o);
};

check_criticita = function (o){
    var seveso = chkbx_asset_edit_seveso.get(),
        obblighi = chkbx_asset_edit_obblighi_di_legge.get(),
        aia = chkbx_asset_edit_aia.get(),
        business = chkbx_asset_edit_business.get();

    if(seveso == 'no' && obblighi == 'no' && aia == 'no' && business == 'no'){
        chkbx_asset_edit_classe_criticita.set('B');
    }else{
        chkbx_asset_edit_classe_criticita.set('A');
    }
    
}

moLibrary.chkbx_asset_edit_seveso_onchange = function (o){
    check_criticita();
}
moLibrary.chkbx_asset_edit_obblighi_di_legge_onchange = function (o){
    check_criticita();
}
moLibrary.chkbx_asset_edit_aia_onchange = function (o){
    check_criticita();
}
moLibrary.chkbx_asset_edit_business_onchange = function (o){
    check_criticita();
}

moLibrary.new_picklist_treegrid_format = function () {
    try {
        var labels = JSON.parse(t)['labels'];
        t = labels.join(", ");
    } catch (e) {

    }
    if (moCnvUtils.measureText(t, f) > x21)
        t = moGlb.reduceText(t, f, x21);
    fillStyle = colr;
    if (t == "null" || t === null)
        t = "";
    fillText(t, X1 + d + 7, y + 8 + (Yi - 7) / 2);
    TIPtext = v;
    
}

moLibrary.pck_wo_edit_ditta_esterna_onload = function (o) { 
  var wo_data = mdl_wo_edit.data;
    if(wo_data.timestamp == undefined) 
    {
        mdl_wo_edit.addCustomData('timestamp',Date.now());
        
        if(o.value){
          var obj= JSON.parse(o.value);
          var fcodes = obj.codes;
        }

        slc_wo_edit_pl.reset(true);
        document.getElementById("slc_wo_edit_pl_comp").innerHTML = "";
        slc_wo_edit_pl.querystring = "vendors=''";
        if(fcodes && fcodes.length >0) slc_wo_edit_pl.getOptions('getManodopera', 'vendors='+fcodes.join());
    }
}

moLibrary.pck_wo_edit_ditta_esterna_onchange = function (o) { 
     var fcodes = null;
    slc_wo_edit_pl.reset(true);
    document.getElementById("slc_wo_edit_pl_comp").innerHTML = "";
    if(o.value){
          var obj= JSON.parse(o.value);
          var fcodes = obj.codes;
    }
    
    if(fcodes && fcodes.length >0) slc_wo_edit_pl.getOptions('getManodopera', 'vendors='+fcodes.join());
}

moLibrary.pck_wo_edit_ditta_esterna_onclear = function (o) { 
    slc_wo_edit_pl.reset(true);
    document.getElementById("slc_wo_edit_pl_comp").innerHTML = "";
}

moLibrary.category = function () {
    if (v) {
        var cod = ['', 58595, 58573, 58608, 58608, 58460, 58611, 58424, 58386, 58611, 58595, 58424, 58645, 58583, '', 58385, '', '', '', 58460, 58379];
        var script_x = X1 + d, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2);
        var col = colrs[2];
        if (cod[v] == 58583)
            col = '#C61201';
        try {
            moCnvUtils.cnvIconFont(BDN, cod[v], script_x + 20, y + script_ts - 1, 16, col);/*drawImage(moGlb.img['delegates/wo'+v+'.png'], script_x+20, y+script_ts);*/
        } catch (ex) {/*moDebug.log("Image not found");*/
        }
        var lbl = ['', "Ordine di Lavoro", moGlb.langTranslate('Purchase Order'), moGlb.langTranslate('Planned maintenance'), "Manutenzione Programmata", moGlb.langTranslate("Inspection"), moGlb.langTranslate("On Condition"), moGlb.langTranslate("Meter Reading"), "Man. Programmata - DE", moGlb.langTranslate('Condition'), moGlb.langTranslate("Corrective from Negative Task"), moGlb.langTranslate('Meter'), moGlb.langTranslate('Work Request'), 'Ordine di Lavoro - DE', '', '', '', '', '', moGlb.langTranslate("Planned inspection"), moGlb.langTranslate("Contract Expiration")];
        TIPtext = lbl[v];
    }
};

moLibrary.slct_wo_phases_onchange = function (o) {
    if (mdl_wo_edit.data.f_phase_id == 1)
        pck_wo_edit_owner_name.setProperties({m_on: (o.get('int') != 7 && o.get('int') != 0)});
    txtfld_wo_edit_ending_date.setProperties({m_on: (o.get('int') == 6)});
    txtfld_wo_edit_add_value_meter.setProperties({m_on: (mdl_wo_edit.data.fc_wo_linked_pm_oc == 1 && o.get('int') == 6)});
    //obbligatorietà all'onchange in fase 'trasmesso a ditta esterna'
    if([2,34].includes(parseInt(mdl_wo_edit.data.f_wf_id)) && o.value == 3){
        pck_wo_edit_ditta_esterna.setProperties({'m_on' : true});
    }
    else pck_wo_edit_ditta_esterna.setProperties({'m_on' : false});
    
    //obbligatorietà all'onchange in fase 'Lavoro terminato'
    if(([2,34,37].includes(parseInt(mdl_wo_edit.data.f_wf_id)) && o.value == 6) || ([36].includes(parseInt(mdl_wo_edit.data.f_wf_id)) && o.value == 3)){
        if(txtfld_wo_edit_ending_date.get() == '')txtfld_wo_edit_ending_date.set(Math.floor(Date.now()/1000));
        mdl_wo_edit.addCustomData('data_fine_lavori',true);
    }
    else if(mdl_wo_edit.data.data_fine_lavori){
        txtfld_wo_edit_ending_date.set('');
    }
}

moLibrary.wo_rsc_fee_type = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), fee = 0, th = eval('txtfld_' + mod + '_edit_total_hours').get('double');
        if (value == 'standard')
            fee = eval('txtfld_' + mod + '_edit_standard').get('double');
        else if (value == 'overtime')
            fee = eval('txtfld_' + mod + '_edit_overtime').get('double');
        else if (value == 'overnight')
            fee = eval('txtfld_' + mod + '_edit_overnight').get('double');
        else if (value == 'holiday')
            fee = eval('txtfld_' + mod + '_edit_holiday').get('double');
        else if (value == 'ritiro_materiale')
            fee = eval('txtfld_' + mod + '_edit_ritiro_materiale').get('double');
        else if (value == 'chiamata_feriale_prima_ora')
            fee = eval('txtfld_' + mod + '_edit_chiamata_feriale_prima_ora').get('double');
        else if (value == 'chiamata_feriale_seconda_ora')
            fee = eval('txtfld_' + mod + '_edit_chiamata_feriale_seconda_ora').get('double');
        else if (value == 'chiamata_nott_prima_ora')
            fee = eval('txtfld_' + mod + '_edit_chiamata_nott_prima_ora').get('double');
        else if (value == 'chiamata_nott_seconda_ora')
            fee = eval('txtfld_' + mod + '_edit_chiamata_nott_seconda_ora').get('double');
        else if (value == 'lavoro_sabato')
            fee = eval('txtfld_' + mod + '_edit_lavoro_sabato').get('double');
        
        eval('txtfld_' + mod + '_edit_fee').set(fee);
        eval('txtfld_' + mod + '_edit_cost').set((th * fee).toFixed(2));
    }
};

moLibrary.slct_asset_edit_class_onchange = function (o) {
    var classe = slct_asset_edit_class.get();
    pck_asset_edit_cdc.setProperties({m_on: false,v_on: false});
    pck_asset_edit_slc_1.setProperties({m_on: false,v_on: false});
    pck_asset_edit_categoria_asset.setProperties({m_on: false,v_on: false});

    if(classe == 'apparecchiatura'){
        pck_asset_edit_categoria_asset.setProperties({m_on: true,v_on: true});
    }    
    else if(classe == 'impianto') {
        pck_asset_edit_categoria_asset.setProperties({m_on: false,v_on: false});
    }
    else if(classe == 'cdc' || classe == 'area') {
        pck_asset_edit_slc_1.setProperties({m_on: true,v_on: true});
        if(classe == 'cdc') pck_asset_edit_cdc.setProperties({m_on: true,v_on: true});
    }
};

moLibrary.slct_asset_edit_class_onload = function (o) {
     moLibrary.slct_asset_edit_class_onchange(o);
}

moLibrary.txtfld_pm_edit_fattore_ripartizione_onload = function(o) {
	// Questa operazione viene fatta solo per visualizzare all'utente i dati
	// Ad ogni salva uno script backend calcola un JSON corretto sul DB
        try {
            var data_wo = mdl_pm_edit.data; 
            if(data_wo.fattore_ripartizione){
                var values = JSON.parse(data_wo.fattore_ripartizione);
                var valori = "";
                values.forEach(function (e){
                            valori += e.t+" -> "+e.v+ "\n";
                });
                o.set(valori);
            }
        } catch (e) {
            // L'onload viene chiamata piu volte 
            // è necessario le volte successive alla prima catturare 
            // silenziosamente l'eccezione della parse
        }
}



moLibrary.txtfld_wo_edit_fattore_ripartizione_onload = function(o) {
	// Questa operazione viene fatta solo per visualizzare all'utente i dati
	// Ad ogni salva uno script backend calcola un JSON corretto sul DB
        try {
            var data_wo = mdl_wo_edit.data; 
            if(data_wo.fattore_ripartizione){
                var values = JSON.parse(data_wo.fattore_ripartizione);
                var valori = "";
                values.forEach(function (e){
                            valori += e.t+" -> "+e.v+ "\n";
                });
                
                o.set(valori);
            }
        } catch (e) {
            // L'onload viene chiamata piu volte 
            // è necessario le volte successive alla prima catturare 
            // silenziosamente l'eccezione della parse
        }
}


moLibrary.ddtPopup = function () {
    var popup = moComp.createComponent("ddtAttachWin", "moPopup", {contentid: 'ddtAttach_popup', title: 'Import DDT', width: 250, height: 100,zindex:945}); 
    popup.show();
    var leftArray = document.getElementById(popup.uiname).style.left.split("px");
    var topArray = document.getElementById(popup.uiname).style.top.split("px");
    
    //riduco il margine sinistro della finestra del 5%
    if(leftArray.length > 0){
        document.getElementById(popup.uiname).style.left = parseInt(leftArray[0])*0.95 + "px";
    }

    //riduco il margine alto della finestra del 5%
    if(topArray.length > 0){
        document.getElementById(popup.uiname).style.top = parseInt(topArray[0])*0.95 + "px";
    }

    var zindex = parseInt(document.getElementById(popup.uiname).style.zIndex) +30;
    var on_check = 
    "var rect = txtfld_doc_ddt_attach.getBoundingClientRect(); var onclear = false; if(rect.right - event.pageX <= 20) onclear = true;";

    var on_clear = "this.value = '';document.getElementById('doc_ddt_attach_sid').value = ''; document.getElementById('doc_ddt_attach_filename').value = '';";   

    var on_attach = "moAttach.show({f_code: \'\', f_fieldname: \'txtfld_doc_ddt_attach\', callback: \'moLibrary.attachCustomCallback\'"+
            ",zindex:"+zindex+"});";

    var on_click = on_check+"if(onclear){"+on_clear+"} else {"+on_attach +"}";

    var on_import = "var fc_doc_attach = document.getElementById('doc_ddt_attach_sid').value+'|'+document.getElementById('txtfld_doc_ddt_attach').value;if(fc_doc_attach !== null && fc_doc_attach !== '|') {moComp.getComponent('ddtAttachWin').hide();moLibrary.computeExcel(fc_doc_attach);}else moGlb.alert.show('File non allegato');";    
    var body = 
        '<div style="font-size:12px;">' +
        '<br>' +
        '<div><label for="attach">File</label>'+
        '<input type="text" id="txtfld_doc_ddt_attach" name="allegato" readonly '+
        'style= "margin-left:10px; width:212px; background: url(\'public/msim_images/default/components/x-2.png\') no-repeat; background-position: center right;"'+
        ' onclick = "'+on_click+'"' + ' placeholder="clicca per aggiungere un file">'+             
        '<br>' +
        '<br>' +
        '<button onclick = "'+on_import+'" style="float:right; top:15px;">Importa</button>'+
        '<input id="doc_ddt_attach_sid" name="sid" type="hidden" style="position: absolute;" >'+
        '<input id="doc_ddt_attach_filename" name="filename" type="hidden" style="position: absolute;" >'+
        '</div>';

    document.getElementById(popup.getContainerId()).innerHTML = body;  
 
}

moLibrary.attachCustomCallback = function (sid, filename) {
    var types = ['xlsx'];
    document.getElementById('txtfld_doc_ddt_attach').value = '';
    document.getElementById('doc_ddt_attach_sid').value = '';
    document.getElementById('doc_ddt_attach_filename').value = '';
    
    var ext = filename.split('.').pop();
    var msg = "Formato '"+ ext +"' non compatibile. Allegare files di tipo '" + types.join("','")+"'";
    if(!types.includes(ext)) {moGlb.alert.show(moGlb.langTranslate(msg));   return;}
    document.getElementById('txtfld_doc_ddt_attach').value = filename;
    document.getElementById('doc_ddt_attach_sid').value = sid;
    document.getElementById('doc_ddt_attach_filename').value = sid+"."+ext;
};


var requestId = 0;
var computeTimer = null;
var fc_doc_attach = null;

moLibrary.computeExcel = function(fc_doc_attach_) {
    var d = new Date();
    requestId = d.getTime();
    fc_doc_attach = fc_doc_attach_;
    //moGlb.loader.show();
    document.getElementById('idbuttontextlbl_btn_wo_import_ddt').innerHTML = 'Update: 0%'; 
    document.getElementById('idbuttontextlbl_btn_wo_import_ddt').style.fontSize  = "12px";
    Ajax.sendAjaxPost(moGlb.baseUrl+"/script/execute/", 'script=computeExcel&curl=1&rid='+requestId+'&fc_doc_attach='+fc_doc_attach+'&userId='+moGlb.user.code, 'moLibrary.startComputeExcel');
    btn_wo_import_ddt.setProperties({"disabled": true});
}

moLibrary.startComputeExcel = function(response) {
    var obj = JSON.parse(response);
    if(obj.code == 3){
        var error = moComp.createComponent("ddt_import_menu", "moPopup", {contentid: 'import_ddt_popup', title: 'Import DDT', width: 300, height: 100}); 
        error.show();
        var txt = "<div>" + obj.message + "</div>";
        document.getElementById('import_ddt_popup_win_bd').innerHTML = txt;
        document.getElementById('idbuttonmask_btn_wo_import_ddt').style.pointerEvents = '';
        requestId = obj.id;
        btn_wo_import_ddt.setProperties({"disabled": false});
        document.getElementById('idbuttontextlbl_btn_wo_import_ddt').innerHTML = 'Import DDT';
        document.getElementById('idbuttontextlbl_btn_wo_import_ddt').style.fontSize  = "13px";

        return;
    }

    computeTimer = setInterval(function(){Ajax.sendAjaxPost(moGlb.baseUrl+"/script/execute/", 'script=computeExcel&curl=0&rid='+requestId+'&fc_doc_attach='+fc_doc_attach+'&userId='+moGlb.user.code, 'moLibrary.checkComputeExcelDone')}, 2000);
}


moLibrary.checkComputeExcelDone = function(response) {
	
        var obj = JSON.parse(response);
        if(obj.code == 1){
            document.getElementById('btn_wo_import_ddt_cnv').style.backgroundColor = 'rgb(243, 133, 39)';
            $('#btn_wo_import_ddt_cnv').css({'background-image': 'linear-gradient(to right,  #ff0000 ' + obj.message + '%, #dddddd ' + obj.message + '%)'});
            document.getElementById('idbuttontextlbl_btn_wo_import_ddt').innerHTML = 'Update: ' + obj.message + '%';
            return;
        }
        // errore
        else if(obj.code == -1){
            var error = moComp.createComponent("ddt_import_menu", "moPopup", {contentid: 'import_ddt_popup', title: 'Import DDT', width: 300, height: 100}); 
            error.show();
            var txt = "<div>" + obj.message + "</div>";
            document.getElementById('import_ddt_popup_win_bd').innerHTML = txt;
            document.getElementById('idbuttonmask_btn_wo_import_ddt').style.pointerEvents = '';
            document.getElementById('idbuttontextlbl_btn_wo_import_ddt').style.fontSize  = "13px";
            requestId = obj.message;
        }
        // import finito
        else if(obj.code == 2){
            
            // popup fine 
            var error = moComp.createComponent("ddt_import_menu", "moPopup", {contentid: 'import_ddt_popup', title: 'Import DDT', width: 230, height: 100}); 
            error.show();
            var stats = JSON.parse(obj.stats);
            var action = "window.open('../../pdf-creator/print-pdf/f_code/0/mockup/1/script_name/logs_import_ddt?f_transact_id="+ obj.f_transact_id+"'"
             +",'_blank').focus();";
            var txt = '<div style="font-size:12px;">' +
                        '<br>' +
                        '<div> Elementi analizzati <b>' + stats.total + '</b></div>' +
                        '<div style="color:green"> Elementi aggiornati <b>' + stats.update + '</b>' +
                        '<input type="button" style = "position: relative; float: right;" onclick='+action+' value="Dettagli"></div>'+
                        '<div style="color:orange"> Elementi non trovati <b>' + stats.not_found + '</b></div>' +
                        '<div style="color:red"> Elementi bloccati <b>' + stats.locked + '</b></div>' +
                      '</div>';
            document.getElementById('import_ddt_popup_win_bd').innerHTML = txt;
            document.getElementById('idbuttonmask_btn_wo_import_ddt').style.pointerEvents = '';
            requestId = obj.message;
        }
        clearTimeout(computeTimer);
        document.getElementById('idbuttonmask_btn_wo_import_ddt').style.pointerEvents = '';
        $('#btn_wo_import_ddt_cnv').css({'background-image': 'linear-gradient(to right,  #ff0000 100%, #dddddd 100%)'});

        document.getElementById('idbuttontextlbl_btn_wo_import_ddt').innerHTML = 'Update: 100%';
        // reset button
        document.getElementById('idbuttonmask_btn_wo_import_ddt').style.backgroundColor = "#a1c24d";
        document.getElementById('btn_wo_import_ddt_cnv').style.backgroundImage='';
        document.getElementById('idbuttontextlbl_btn_wo_import_ddt').innerHTML = 'Import DDT';
        document.getElementById('idbuttontextlbl_btn_wo_import_ddt').style.fontSize  = "13px";
        btn_wo_import_ddt.setProperties({"disabled": false});
}

moLibrary.set_total_cost = function () {
    var downtime = txtfld_wo_edit_costs_for_downtime.getValue('double'), materials = txtfld_wo_edit_costs_for_material.getValue('double'),
            labors = txtfld_wo_edit_costs_for_labor.getValue('double'), tools = txtfld_wo_edit_costs_for_tool.getValue('double'),
            services = txtfld_wo_edit_costs_for_service.getValue('double'), ddt = txtfld_wo_edit_costs_for_ddt.getValue('double');
    var scr_tot = (parseFloat(downtime) + parseFloat(materials) + parseFloat(labors) + parseFloat(tools) + parseFloat(services)+ parseFloat(ddt)).toFixed(2);
    txtfld_wo_edit_total_costs.set(scr_tot);
};

function setTotalLaborsHours(mod){
    var totalHours = 0;
    // recupero voci
    var voci = eval("mdl_"+mod+"_rsc_tg_tg").childJStore;
    for(var k in voci){
        totalHours += Number(voci[k].fc_rsc_total_hours); 
    }
    
    eval("txtfld_"+mod+"_edit_ore_mdo_tot").set((totalHours).toFixed(2));
}

btn_wo_rsc_remove_after_fn = btn_wo_rsc_set_after_fn = function(){
    setTotalLaborsHours("wo");
}


moLibrary.txtfld_wo_edit_progress_onload = function (o) {
    var disable = ([1,4].includes(parseInt(mdl_wo_edit.data.f_type_id)));
    btn_wo_rsc_new.setProperties({disabled: disable});
    btn_wo_rsc_add.setProperties({disabled: disable});
    btn_wo_rsc_remove.setProperties({disabled: disable});   
    btn_wo_rsc_set.setProperties({disabled: disable});   
    
    btn_wo_asset_add.setProperties({disabled: !disable});
    btn_wo_asset_remove.setProperties({disabled: !disable});
    btn_wo_asset_new.setProperties({disabled: !disable});
}

moLibrary.fc_rsc_fee_onload = function (o) {
    mod = moLibrary.getModule(o.name);
    var type = eval('slct_' + mod + '_edit_fee_type').get(), fee = 0;
    if (type == 'standard')
        fee = eval('txtfld_' + mod + '_edit_standard').get('double');
    else if (type == 'overtime')
        fee = eval('txtfld_' + mod + '_edit_overtime').get('double');
    else if (type == 'overnight')
        fee = eval('txtfld_' + mod + '_edit_overnight').get('double');
    else if (type == 'holiday')
        fee = eval('txtfld_' + mod + '_edit_holiday').get('double');
    else if (type == 'ritiro_materiale')
        fee = eval('txtfld_' + mod + '_edit_ritiro_materiale').get('double');
    else if (type == 'chiamata_feriale_prima_ora')
        fee = eval('txtfld_' + mod + '_edit_chiamata_feriale_prima_ora').get('double');
    else if (type == 'chiamata_feriale_seconda_ora')
        fee = eval('txtfld_' + mod + '_edit_chiamata_feriale_seconda_ora').get('double');
    else if (type == 'chiamata_nott_prima_ora')
        fee = eval('txtfld_' + mod + '_edit_chiamata_nott_prima_ora').get('double');
    else if (type == 'chiamata_nott_seconda_ora')
        fee = eval('txtfld_' + mod + '_edit_chiamata_nott_seconda_ora').get('double');
    else if (type == 'lavoro_sabato')
        fee = eval('txtfld_' + mod + '_edit_lavoro_sabato').get('double');
    
    eval('txtfld_' + mod + '_edit_fee').set(fee);
};

moLibrary.txtfld_wo_rsc_edit_lavoro_sabato_onload = function (o) {
    moLibrary.wo_rsc_fee_type(slct_wo_rsc_edit_fee_type);
};
