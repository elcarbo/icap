var moLibrary = {
    o: null,
    exec: function (script, o) {
        try {
            if (moGlb.isempty(script))
                return;
            if (moGlb.isset(moGlb.delegates[script]))
                with (o) {
                    eval(moGlb.delegates[script]);
                    return;
                }
            else if (moGlb.isset(moLibrary[script]))
                return moLibrary[script](o);
            with (o) {
                try {
                    eval(script);
                } catch (ex) {
                }
            }
        } catch (ex) {
            moDebug.log("JS script " + script + " for " + o.name + " failed: " + ex);
        }
    },
    getSource: function (script) {
        if (moLibrary.exists(script)) {
            if (moGlb.isset(moGlb.delegates[script]))
                return moGlb.delegates[script];
            return moLibrary[script].toString().replace('function ()', '');
        }
    },
    getModule: function (name) {
        var namea = name.split("_"), mod = [];
        for (var i = 1; i < namea.length; i++) {
            if (namea[i] == 'edit' || namea[i] == 'phases')
                return mod.join('_');
            mod.push(namea[i]);
        }
        return false;
    },
    // for alternative modules with $
    getModuleSubtype: function (name) {
        var smod = moLibrary.getModule(name).split("#");
        if (smod.length == 2)
            return smod[1];
        return '';
    },
    exists: function (script) {
        return moGlb.isset(moGlb.delegates[script]) || moGlb.isset(moLibrary[script]);
    },
    /**
     * Return the element ui type (textfield, select, checkbox, image)
     */
    typeToString: function (name) {
        return name.split("_")[0];
    }
};

moLibrary.slct_pm_edit_type_onchange = moLibrary.slct_pm_edit_type_onload = function (o) {
    if (o.get() == 'classic') {
        txtfld_pm_edit_starting_date.setProperties({r_on: false, m_on: true});
    } else {
        txtfld_pm_edit_starting_date.setProperties({r_on: true, m_on: false});
        txtfld_pm_edit_starting_date.set('');
    }
}

//moLibrary.txtfld_wo_edit_starting_date_onload = function(o) { o.setProperties({m_on: false}); }
moLibrary.opening_date = function (o) {
    with (o) {
        if (get('int') == 0)
            set(parseInt(new Date().getTime() / 1000));
    }
};
moLibrary.check_start_end_date = function (o, start, end, msg) {
    var setting = false;
    try {
        JSON.parse(moGlb.CHECK_DATES);
        setting = true;
    } catch (e) {

    }



    with (o) {
        if (!start)
            start = 'starting_date';
        if (!end)
            end = 'ending_date';
        if (!msg)
            msg = 'Warning: end cannot come before start!';
        var mod = moLibrary.getModule(o.name), sd = eval('txtfld_' + mod + '_edit_' + start).get('int'), ed = eval('txtfld_' + mod + '_edit_' + end).get('int');
        if (mod == 'wo') {
            eval('txtfld_' + mod + '_edit_' + start).setProperties({m_on: (ed && !sd)});
        }
        if (!sd || !end)
            return false;
        if (ed && sd > ed) {
            var old = getOldValue();
            moGlb.alert.show(moGlb.langTranslate(msg));
            setProperties({value: old, description: (old ? old : ' ')});
            //if(old == 0) eval('txtfld_'+mod+'_edit_'+start).setProperties({m_on: false});
            return false;
        }
        return {start: sd, end: ed};
    }
};

moLibrary.txtfld_pm_edit_starting_date_onchange = function (o) {
    moLibrary.check_start_end_date(o);
    var mod = moLibrary.getModule(o.name);
    eval('txtfld_' + mod + '_edit_starting_date').setProperties({m_on: eval('txtfld_' + mod + '_edit_ending_date').get('int') > 0 && eval('txtfld_' + mod + '_edit_starting_date').get('int') == 0});

}

moLibrary.txtfld_pm_edit_ending_date_onchange = moLibrary.txtfld_pm_edit_starting_date_onchange;

moLibrary.txtfld_wo_edit_starting_date_onchange = moLibrary.check_start_end_date;
//moLibrary.txtfld_wo_edit_ending_date_onchange = moLibrary.check_start_end_date;

moLibrary.two_dates_are_equal = function (inizio, fine) {
    if (inizio.getFullYear() == fine.getFullYear())
        if (inizio.getMonth() == fine.getMonth())
            if (inizio.getDate() == fine.getDate())
                return true;

    return false;

}

moLibrary.control_end_date = function (fine) {
    var old_fine = fine;
    //fine = moLibrary.holiday_control(new Date(fine*1000));

    var new_fine = fine.getTime() / 1000;
    if (old_fine != new_fine)
        moGlb.alert.show(moGlb.langTranslate('Ending date is a day off , so will be considered the first working day available'));

    return fine;
}

moLibrary.get_number_of_days = function (inizio, fine) {
    var days = 0;
    // fine = moLibrary.control_end_date(fine);
    // fine = fine.getTime()/1000;


    while (/*!moLibrary.two_dates_are_equal(new Date(inizio*1000), new Date(fine*1000)) ||*/ ((inizio + (3600 * 12)) <= fine)) {
        inizio = inizio + (3600 * 24);
        //inizio = moLibrary.holiday_control(new Date(inizio*1000));
        //inizio = inizio.getTime()/1000;

        days++;
    }
    return days;
}

moLibrary.txtfld_wo_edit_ending_date_onchange = function (o) {
    with (o) {
        moLibrary.check_start_end_date(o);
        var mod = moLibrary.getModule(o.name), open = eval('txtfld_' + mod + '_edit_opening_date').get('int'),
                start = o.get('int');


        if (start < open && start != 0) {
            moGlb.alert.show(moGlb.langTranslate('Ending date cannot come before opening date'));
            eval('txtfld_' + mod + '_edit_ending_date').set('');
            eval('txtfld_' + mod + '_edit_starting_date').setProperties({m_on: false});
        } else if (start > open) {
            var rd = moLibrary.get_number_of_days(open, start);
            eval('txtfld_' + mod + '_edit_resolution_days').set(rd);
        } else if (start == 0) {
            eval('txtfld_' + mod + '_edit_resolution_days').set('');
        }
    }
};
/*
 * WO
 */
moLibrary.response_days = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), open = eval('txtfld_' + mod + '_edit_opening_date').get('int'),
                start = eval('txtfld_' + mod + '_edit_starting_date').get('int'),
                end = eval('txtfld_' + mod + '_edit_ending_date').get('int')/*, rd = Math.floor((start-open)/(60*60))*/;
        var rd = Math.round((start - open) / (60 * 60));
        if (start < open && start != 0) {
            moGlb.alert.show(moGlb.langTranslate('Starting date cannot come before opening date'));
            eval('txtfld_' + mod + '_edit_response_days').set('');
            eval('txtfld_' + mod + '_edit_starting_date').set('');
        } else if (start > open)
            eval('txtfld_' + mod + '_edit_response_days').set(rd);
        if (start > end && end != 0) {
            moGlb.alert.show(moGlb.langTranslate('Starting date cannot come after ending date'));
            eval('txtfld_' + mod + '_edit_starting_date').set('');
            eval('txtfld_' + mod + '_edit_response_days').set('');
        }
        eval('txtfld_' + mod + '_edit_starting_date').setProperties({m_on: end != 0});
        if (start == '')
            eval('txtfld_' + mod + '_edit_response_days').set('');
    }
};
moLibrary.check_start_end_date_planned = function (o) {
    var mod = moLibrary.getModule(o.name);
    if ((o.name == 'txtfld_' + mod + '_edit_planned_starting_date') && (o.get('int') < eval('txtfld_' + mod + '_edit_opening_date').get('int'))) {
        moGlb.alert.show(moGlb.langTranslate('Planned starting date cannot come before opening date'));
        eval('txtfld_' + mod + '_edit_planned_starting_date').set('');
    }
    moLibrary.check_start_end_date(o, "planned_starting_date", "planned_ending_date");
};

moLibrary.slct_wo_edit_priority_onload = function (o) {
    var mod = moLibrary.getModule(o.name);
    if (mod != 'std' && eval('txtfld_' + mod + '_edit_planned_starting_date').get('int') == 0)
        moLibrary.priority_set_dates(o);

}
moLibrary.priority_set_dates = function (o) {
    with (o) {//convertire le ore in giorni

        var mod = moLibrary.getModule(o.name);
        if (mod != 'std' && eval('mdl_' + mod + '_edit').data.f_type_id != 4) {
            var opening_date = eval('txtfld_' + mod + '_edit_opening_date').get('int');
            var date = new Date(opening_date * 1000);
            if (moGlb.priorities[eval('slct_' + mod + '_edit_priority').get('int')]) {
                var hours = moGlb.priorities[eval('slct_' + mod + '_edit_priority').get('int')].hours;
                var di = moLibrary.scan_date(date, hours);
                eval('txtfld_' + mod + '_edit_planned_starting_date').set(di.getTime() / 1000);
                /*if(mdl_wo_edit.data.f_type_id == 1 || mdl_wo_edit.data.f_type_id == 13){
                 var mod = moLibrary.getModule(o.name), priority = eval('slct_'+mod+'_edit_priority').get('int'),
                 date = eval('txtfld_'+mod+'_edit_opening_date').get('int'), gaps = [ '', moGlb.PRIORITY_NORMAL, moGlb.PRIORITY_HIGH, moGlb.PRIORITY_IMMEDIATE ];
                 if(parseInt(eval('mdl_'+mod+'_edit').data.f_type_id) == 1) { // correctives only
                 eval('txtfld_'+mod+'_edit_planned_starting_date').set(priority==0?'':date+(parseInt(gaps[priority])*60*60));
                 }
                 }*/
            }
        }
    }
};
/*
 * WO/STD - Asset
 */
moLibrary.wo_asset_since_till = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), dates = moLibrary.check_start_end_date(o, 'downtime_since', 'downtime_till'), hrs = 0,
                uc = eval('txtfld_' + mod + '_edit_downtime_hourly_cost').get('double'), cost = '0.00';
        if (dates && (dates.end > dates.start))
            hrs = (dates.end - dates.start) / (60 * 60), cost = (hrs * uc).toFixed(2);
        var e = dates.end, s = dates.start;
        if (dates.end == undefined)
            e = '';
        if (dates.start == undefined)
            s = '';

        if (e != eval('mdl_' + mod + '_edit').data.fc_rsc_till || s != eval('mdl_' + mod + '_edit').data.fc_rsc_since) {
            if (hrs == 0)
                hrs = "0.00";

            eval('txtfld_' + mod + '_edit_downtime_hours').set(hrs);
            eval('txtfld_' + mod + '_edit_downtime_cost').set(cost);

            eval('txtfld_' + mod + '_edit_downtime_since').setProperties({m_on: eval('txtfld_' + mod + '_edit_downtime_till').get('int') > 0 && eval('txtfld_' + mod + '_edit_downtime_since').get('int') == 0});
        }
    }
};
/*OLD - BEFORE 13/01/2016
 * moLibrary.wo_asset_since_till = function (o) {
 with(o) {
 var mod = moLibrary.getModule(o.name), dates = moLibrary.check_start_end_date(o, 'downtime_since', 'downtime_till'), hrs = 0,
 uc = eval('txtfld_'+mod+'_edit_downtime_hourly_cost').get('double'), cost = '0.00';
 if(dates && (dates.end > dates.start)) hrs = Math.round((dates.end-dates.start)/(60*60)), cost = (hrs*uc).toFixed(2);
 eval('txtfld_'+mod+'_edit_downtime_hours').set(hrs);eval('txtfld_'+mod+'_edit_downtime_cost').set(cost);
 eval('txtfld_' + mod + '_edit_downtime_since').setProperties({m_on: true});
 }
 };*/
moLibrary.wo_asset_hours = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), hrs = get('double'), uc = eval('txtfld_' + mod + '_edit_downtime_hourly_cost').get('double'),
                cost = (hrs * uc).toFixed(2);
        eval('txtfld_' + mod + '_edit_downtime_cost').set(cost);
    }
};
/**/
/*moLibrary.std_code_visibility = function (o) {with(o) {setProperties({v_on: !moGlb.isempty(value)});}};
 
 moLibrary.txtfld_wo_edit_std_onload = function (o){
 moLibrary.std_code_visibility(o);
 }*/
/*
 * WO/PM - Rsc
 */
/*
 * OLD - BEFORE 13/01/2016
 * moLibrary.wo_rsc_since_till = function (o) {
 with(o) {
 var mod = moLibrary.getModule(o.name), dates = moLibrary.check_start_end_date(o, 'since', 'till'), hrs = 0, mp = eval('txtfld_'+mod+'_edit_manpower').get('int'),
 uc = eval('txtfld_'+mod+'_edit_fee').get('double'), cost = '0.00';
 
 var e = dates.end, s = dates.start;
 if(dates.end == undefined) e = '';
 if(dates.start == undefined) s = '';
 
 if(e != eval('mdl_' + mod + '_edit').data.fc_rsc_till || s != eval('mdl_' + mod + '_edit').data.fc_rsc_since){
 if(dates && (dates.end > dates.start)) hrs = Math.round((dates.end-dates.start)/(60*60)), cost = (hrs*mp*uc).toFixed(2);
 eval('txtfld_'+mod+'_edit_hours').set(hrs);eval('txtfld_'+mod+'_edit_total_hours').set(hrs*mp);eval('txtfld_'+mod+'_edit_cost').set(cost);
 
 eval('txtfld_'+mod+'_edit_since').setProperties({m_on:  eval('txtfld_'+mod+'_edit_till').get('int') > 0 &&  eval('txtfld_'+mod+'_edit_since').get('int') == 0});
 }
 }
 
 };*/

moLibrary.txtfld_wo_rsc_edit_till_onload = function (o) {
    var mod = moLibrary.getModule(o.name);
    eval('txtfld_' + mod + '_edit_since').setProperties({m_on: eval('txtfld_' + mod + '_edit_till').get('int') > 0 && eval('txtfld_' + mod + '_edit_since').get('int') == 0});
}
/*
 * OLD - BEFORE 13/01/2016
 moLibrary.wo_rsc_hours_manpower = function (o) {
 var mod = moLibrary.getModule(o.name), hrs = eval('txtfld_'+mod+'_edit_hours').get('double'), mp = eval('txtfld_'+mod+'_edit_manpower').get('int'),
 uc = eval('txtfld_'+mod+'_edit_fee').get('double'), th = (hrs*mp).toFixed(1);
 eval('txtfld_'+mod+'_edit_total_hours').set(th);eval('txtfld_'+mod+'_edit_cost').set((th*uc).toFixed(2));
 };*/
moLibrary.wo_rsc_fee_type = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), fee = 0, th = eval('txtfld_' + mod + '_edit_total_hours').get('double');
        if (value == 'standard')
            fee = eval('txtfld_' + mod + '_edit_standard').get('double');
        else if (value == 'overtime')
            fee = eval('txtfld_' + mod + '_edit_overtime').get('double');
        else if (value == 'overnight')
            fee = eval('txtfld_' + mod + '_edit_overnight').get('double');
        else if (value == 'holiday')
            fee = eval('txtfld_' + mod + '_edit_holiday').get('double');
        eval('txtfld_' + mod + '_edit_fee').set(fee);
        eval('txtfld_' + mod + '_edit_cost').set((th * fee).toFixed(2));
    }
};
moLibrary.fc_rsc_default_manpower = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        if (eval('txtfld_' + mod + '_edit_manpower').get('int') == 0)
            eval('txtfld_' + mod + '_edit_manpower').set(1);
    }
};
moLibrary.fc_rsc_cost_onload = function (o) {
    with (o) {
        set(moGlb.isempty(value) ? '0.00' : value);
    }
};
moLibrary.fc_rsc_fee_onload = function (o) {
    with (o) {
        if (moGlb.isempty(value))
            set(eval('txtfld_' + moLibrary.getModule(o.name) + '_edit_standard').get('double'));
    }
};

moLibrary.pck_wo_edit_owner_name_onchange = function (o) {
//    Ajax.sendAjaxPost(moGlb.baseUrl + "/script/execute", "script=getOwnerData&owner="+o.get(), "moLibrary.setOwnerData");
    var mod = moLibrary.getModule(o.name);
    var item = o.getResult();
    eval('txtfld_' + mod + '_edit_owner_email').set(item.fc_usr_mail);
    eval('txtfld_' + mod + '_edit_owner_phone').set(item.fc_usr_phone);
    eval('mdl_' + mod + '_edit').data.fc_owner_code = item.f_code;
    mdl_wo_edit.addCrossData(item.f_code, 't_ware_wo', 16);
};

moLibrary.pck_wo_edit_owner_name_onclear = function (o) {

    var mod = moLibrary.getModule(o.name);
    var item = o.getResult();
    eval('txtfld_' + mod + '_edit_owner_email').set('');
    eval('txtfld_' + mod + '_edit_owner_phone').set('');
    eval('mdl_' + mod + '_edit').data.fc_owner_code = '';
};
moLibrary.pck_wo_edit_owner_name_onload = function (o) {
//    Ajax.sendAjaxPost(moGlb.baseUrl + "/script/execute", "script=getOwnerData&owner="+o.get(), "moLibrary.setOwnerData");
}

moLibrary.pck_std_edit_owner_name_onchange = moLibrary.pck_wo_edit_owner_name_onchange;

moLibrary.txtfld_wo_edit_planned_starting_date_onload = function (o) {
    var mod = moLibrary.getModule(o.name);
    if (o.get('int') == 0 && moGlb.isset(slct_wo_edit_priority.get()))
        moLibrary.priority_set_dates(o);
    eval('txtfld_' + mod + '_edit_starting_date').setProperties({m_on: eval('txtfld_' + mod + '_edit_starting_date').get('int') != 0});
}

moLibrary.txtfld_asset_edit_start_date_onchange = function (o) {
    moLibrary.sum_months(o.get('int'), txtfld_asset_edit_expected_life.get('int'));
}
moLibrary.txtfld_asset_edit_expected_life_onchange = function (o) {
    moLibrary.sum_months(txtfld_asset_edit_start_date.get('int'), o.get('int'));
}

moLibrary.sum_months = function (data, months) {
    var data_string = new Date(data * 1000);
    var month = data_string.getMonth();
    data_string.setMonth(month + months);

    txtfld_asset_edit_end_life.set(data_string / 1000);
    mdl_asset_edit.data.fc_asset_end_life = data_string / 1000;
}

/*Email in Actions*/
moLibrary.list_of_mail = function (o) {
    var list = o.get();
    var array_email = list.split(';');
    var new_list = new Array();

    for (var email = 0; email < array_email.length; email++)
        if (moLibrary.is_email(array_email[email].split(' ').join('')))
            new_list.push(array_email[email].split(' ').join(''));

    o.set(new_list.join(';'));
};

moLibrary.is_email = function (email) {
    if (!moCheck.checkMail(email)) {
        moGlb.alert.show(moGlb.langTranslate("Error: email") + " '" + email + "' " + moGlb.langTranslate("is wrong"));
        return false;
    }

    return true;
};

moLibrary.txtfld_action_edit_to_onchange = function (o) {
    moLibrary.list_of_mail(o);
};
moLibrary.txtfld_action_edit_cc_onchange = function (o) {
    moLibrary.list_of_mail(o);
};
moLibrary.txtfld_action_edit_ccn_onchange = function (o) {
    moLibrary.list_of_mail(o);
};
/**/
/*
 * WO/Inv IGNORA
 */
moLibrary.fc_inv_moved_quantity = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), qty = get("int"), uc = eval("txtfld_" + mod + "_edit_unit_cost").get('int');
        eval("txtfld_" + mod + "_edit_cost").set(qty * uc);
    }
};
/*
 * WO/PM - Tool IGNORA
 */
moLibrary.wo_tool_since_till = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), dates = moLibrary.check_start_end_date(o, 'since', 'till'), hrs = 0,
                uc = eval('txtfld_' + mod + '_edit_unit_cost').get('double'), cost = '0.00';
        if (dates)
            hrs = Math.round((dates.end - dates.start) / (60 * 60)), cost = (hrs * uc).toFixed(2);
        eval('txtfld_' + mod + '_edit_hours').set(hrs);
        eval('txtfld_' + mod + '_edit_cost').set(cost);
    }
};

/*IGNORA*/
moLibrary.wo_tool_hours = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), hrs = get('double'), uc = eval('txtfld_' + mod + '_edit_unit_cost').get('double'),
                cost = (hrs * uc).toFixed(2);
        eval('txtfld_' + mod + '_edit_cost').set(cost);
    }
};

/*
 * WO/PM - Vendor IGNORA
 */
moLibrary.wo_vendor_cost = function (o) {
    with (o) {
        set(get('double').toFixed(2));
    }
};
/*
 * WO/PO IGNORA
 */
moLibrary.wo_po_material_units_onload = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        if (moGlb.isset(eval(eval('mdl_' + mod + '_tg').gridName).JStore[eval('mdl_' + mod + '_edit').data.f_code_cross]))
            set(eval(eval('mdl_' + mod + '_tg').gridName).JStore[eval('mdl_' + mod + '_edit').data.f_code_cross]['fc_material_units']);
    }
};
//IGNORA
moLibrary.wo_po_material_package_capacity_onload = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        if (moGlb.isset(eval(eval('mdl_' + mod + '_tg').gridName).JStore[eval('mdl_' + mod + '_edit').data.f_code_cross]))
            set(eval(eval('mdl_' + mod + '_tg').gridName).JStore[eval('mdl_' + mod + '_edit').data.f_code_cross]['fc_material_package_capacity']);
    }
};
//IGNORA
moLibrary.wo_po_material_package_type_onload = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        if (moGlb.isset(eval(eval('mdl_' + mod + '_tg').gridName).JStore[eval('mdl_' + mod + '_edit').data.f_code_cross]))
            set(eval(eval('mdl_' + mod + '_tg').gridName).JStore[eval('mdl_' + mod + '_edit').data.f_code_cross]['fc_material_package_type']);
    }
};
//IGNORA
moLibrary.wo_po_material_carrier_unit_change = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        eval('txtfld_' + mod + '_edit_package_capacity_carrier').setProperties({v_on: (eval(name).get() == 1), m_on: (eval(name).get() == 1)});
        eval('txtfld_' + mod + '_edit_package_type_carrier').setProperties({v_on: (eval(name).get() == 1), m_on: (eval(name).get() == 1)});
        eval('txtfld_' + mod + '_edit_package_capacity').setProperties({v_on: (eval(name).get() == 0)});
        eval('txtfld_' + mod + '_edit_package_type').setProperties({v_on: (eval(name).get() == 0)});
    }
    //if there is a particular package, provide to change default package with
    var spare_arrived = eval('txtfld_' + mod + '_edit_spare_arrived').get();
    var multiplier = eval('txtfld_' + mod + '_edit_package_capacity').get();
    if (eval('chkbx_' + mod + '_edit_carrier_unit_change').get() == 1) {
        multiplier = eval('txtfld_' + mod + '_edit_package_capacity_carrier').get();
    }
    eval('txtfld_' + mod + '_edit_real_spares_arrived').set(spare_arrived * multiplier);
    if (eval('chkbx_po_material_edit_cost_type').get() == 0) {
        eval('txtfld_' + mod + '_edit_total_price').set((spare_arrived * multiplier) * eval('txtfld_' + mod + '_edit_price').get());
    }
};
//IGNORA
moLibrary.wo_po_material_cost_type = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        var val_check = eval(name).get();
        eval('txtfld_' + mod + '_edit_total_price').setProperties({r_on: (val_check == 0), m_on: (val_check == 1)});
        eval('txtfld_' + mod + '_edit_price').setProperties({r_on: (val_check == 1), m_on: (val_check == 0)});
        var real_spare_arrived = eval('txtfld_' + mod + '_edit_real_spares_arrived').get("double");
        var total_price = eval('txtfld_' + mod + '_edit_total_price').get("double");
        var price = eval('txtfld_' + mod + '_edit_price').get("double");
        //eval('txtfld_'+mod+'_edit_package_capacity').setProperties({v_on: (eval(name).get() == 0)});
        //eval('txtfld_'+mod+'_edit_package_type').setProperties({v_on: (eval(name).get() == 0)});
        if (val_check == 0) {
            if (price <= 0 || real_spare_arrived <= 0) {
                eval('txtfld_' + mod + '_edit_total_price').set(0);
            } else {
                eval('txtfld_' + mod + '_edit_total_price').set(price * eval('txtfld_' + mod + '_edit_real_spares_arrived').get("double"));
            }
        } else if (val_check == 1) {
            if (isNaN(eval('txtfld_' + mod + '_edit_total_price').get("double")) || isNaN(eval('txtfld_' + mod + '_edit_real_spares_arrived').get("double"))) {
                eval('txtfld_' + mod + '_edit_price').set(0);
            } else {
                eval('txtfld_' + mod + '_edit_price').set(eval('txtfld_' + mod + '_edit_total_price').get("double") / eval('txtfld_' + mod + '_edit_real_spares_arrived').get("double"));
            }
        }
    }

    //if there is a particular package, provide to change default package with
    /*var spare_arrived = eval('txtfld_'+mod+'_edit_spare_arrived').get();
     var multiplier = eval('txtfld_'+mod+'_edit_package_capacity').get();
     if(eval('chkbx_'+mod+'_edit_carrier_unit_change').get() == 1) {
     multiplier = eval('txtfld_'+mod+'_edit_package_capacity_carrier').get();
     }
     eval('txtfld_'+mod+'_edit_real_spares_arrived').set(spare_arrived*multiplier);*/
};
//IGNORA
moLibrary.wo_po_material_spare_arrived_change = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        //if there is a particular package, provide to change default package with
        var spare_arrived = eval('txtfld_' + mod + '_edit_spare_arrived').get();
        var multiplier = eval('txtfld_' + mod + '_edit_package_capacity').get();
        if (eval('chkbx_' + mod + '_edit_carrier_unit_change').get() == 1) {
            multiplier = eval('txtfld_' + mod + '_edit_package_capacity_carrier').get();
        }
        eval('txtfld_' + mod + '_edit_real_spares_arrived').set(spare_arrived * multiplier);
        var val_check = eval("chkbx_po_material_edit_cost_type").get();
        var real_spare_arrived = spare_arrived * multiplier;
        var total_price = eval('txtfld_' + mod + '_edit_real_spares_arrived').get("double");
        var price = eval('txtfld_' + mod + '_edit_price').get("double");

        if (val_check == 0) {
            if (price <= 0 || real_spare_arrived <= 0) {
                eval('txtfld_' + mod + '_edit_total_price').set(0);
            } else {
                eval('txtfld_' + mod + '_edit_total_price').set(price * real_spare_arrived);
            }
        } else if (val_check == 1) {
            if (total_price <= 0 || real_spare_arrived <= 0) {
                eval('txtfld_' + mod + '_edit_price').set(0);
            } else {
                eval('txtfld_' + mod + '_edit_price').set(total_price / real_spare_arrived);
            }
        }
    }
};
//IGNORA
moLibrary.wo_po_material_price_change = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        //if there is a particular package, provide to change default package with
        var real_spare_arrived = eval('txtfld_' + mod + '_edit_real_spares_arrived').get("double");
        var price = eval(o.name).get("double");
        var val_check = eval("chkbx_po_material_edit_cost_type").get("int");
        var rightField = val_check == 1 ? 'txtfld_' + mod + '_edit_price' : 'txtfld_' + mod + '_edit_total_price';
        if (price <= 0 || real_spare_arrived) {
            eval(rightField).set(0);
        }
        if (val_check == 1) {
            eval(rightField).set(price / real_spare_arrived);
        }
        if (val_check == 0) {
            eval(rightField).set(price * real_spare_arrived);
        }
    }
};
/**
 * WO - Mov IGNORA
 */
moLibrary.wo_mov_hide_if_empty = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        eval('txtfld_' + mod + '_edit_po_moved_quantity').setProperties({v_on: (eval(name).get() != 0)});
    }
};
/*
 * Comment
 */
moLibrary.mdl_wo_comment_edit_save = function (data) {
    var d = new Date();
    data.fc_comment_editor_id = moGlb.user.code;
    data.fc_comment_editor_name = moGlb.user.fullname;
    data.fc_comment_date = (new Date()).format("U");
    data.fc_comment_phase_id = mdl_wo_edit.data.f_phase_id;
    data.fc_comment_phase_name = mdl_wo_edit.data.f_name;
    data.fc_comment_date = d.format("{U}");
    return true;
};
moLibrary.mdl_wco_comment_edit_save = function (data) {
    var d = new Date();
    data.fc_comment_editor_id = moGlb.user.code;
    data.fc_comment_editor_name = moGlb.user.fullname;
    data.fc_comment_date = (new Date()).format("U");
    data.fc_comment_phase_id = mdl_wo_edit.data.f_phase_id;
    data.fc_comment_phase_name = mdl_wo_edit.data.f_name;
    data.fc_comment_date = d.format("{U}");
    return true;
};
moLibrary.mdl_pm_comment_edit_save = function (data) {
    var d = new Date();
    data.fc_comment_editor_id = moGlb.user.code;
    data.fc_comment_editor_name = moGlb.user.fullname;
    data.fc_comment_date = (new Date()).format("U");
    data.fc_comment_phase_id = mdl_wo_edit.data.f_phase_id;
    data.fc_comment_phase_name = mdl_wo_edit.data.f_name;
    data.fc_comment_date = d.format("{U}");
    return true;
};
moLibrary.mdl_asset_comment_edit_save = function (data) {
    var d = new Date();
    data.fc_comment_editor_id = moGlb.user.code;
    data.fc_comment_editor_name = moGlb.user.fullname;
    data.fc_comment_date = (new Date()).format("U");
    data.fc_comment_phase_id = mdl_wo_edit.data.f_phase_id;
    data.fc_comment_phase_name = mdl_wo_edit.data.f_name;
    data.fc_comment_date = d.format("{U}");
    return true;
};
/**/
moLibrary.cond_operation_available = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        txtfld_cond_edit_average_number_value.setProperties({v_on: (eval(name).get() == 'AVG')});
        slct_cond_edit_list_operation.setProperties({v_on: (eval(name).get() == 'AVG' || eval(name).get() == 'METER_VALUE')});
        txtfld_cond_edit_condition.setProperties({v_on: (eval(name).get() == 'Function')});
    }
};
moLibrary.slct_cond_edit_list_function_onload = moLibrary.cond_operation_available;
moLibrary.slct_cond_edit_list_function_onchange = moLibrary.cond_operation_available;
moLibrary.pck_cond_edit_code_meter_onchange = function (o) {
    var res = o.getResult();
    //mdl_cond_edit.setEditData("fc_cond_code_meter", res.f_code);
    mdl_cond_edit.data.fc_cond_code_meter = res.f_code;
}
/*
 moLibrary.closing_date = function (o) {
 with(o) {
 var mod = moLibrary.getModule(o.name), mdn = ( (eval('mdl_'+mod+'_edit').data.f_wf_id == 2 && value == 6) || (eval('mdl_'+mod+'_edit').data.f_wf_id == 16 && value == 3) );
 eval('txtfld_'+mod+'_edit_ending_date').setProperties({m_on: mdn});
 }
 };
 */
/**
 * Treegrid bookmark IGNORA
 */
moLibrary.wf_phase_bar = function () {
    var sval = parseInt(v), script_w = 100, script_hs = 11, script_ts = (parseInt(Yi - script_hs) / 2),
            script_col = ['#F00', '#FF0', '#080'], script_ns = script_col.length;
    if (sval) {
        var script_wn = script_w / 100 * sval, script_n = 1 / (script_ns - 1), script_r, script_ln,
                script_ln = createLinearGradient(xfield, y, xfield + script_w - 2, y + Yi);
        for (script_r = 0; script_r < script_ns; script_r++)
            script_ln.addColorStop(script_r * script_n, script_col[script_r]);
    } else {
        script_ln = '#F00';
        script_wn = script_w;
    }
    save();
    fillStyle = script_col[script_ln];
    fillRect(xfield + 0.5, y + script_ts + 0.5, script_wn, script_hs);
    restore();
    TIPtext = moGlb.langTranslate('Workflow progress') + ': ' + v + '%';
};
moLibrary.wf_phase_number = function () {
    var sval = parseInt(v), script_hs = 11, script_ts = (parseInt(Yi - script_hs) / 2),
            script_col = ['#FF0000', '#FF2800', '#FF6300', '#FFA300', '#FFDA00', '#FFFF00', '#BCF017', '#7DD02C', '#42AD3E', '#17964B'];
    save();
    if (!sval) {
        sval = 10;
        script_col = ['#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000'];
    } else {
        sval = parseInt(sval / 100 * 10);
    }
    for (script_r = 0; script_r < sval; script_r++) {
        script_ln = parseInt(script_r);
        fillStyle = script_col[script_ln];
        fillRect(xfield + script_r * 8, y + script_ts + 0.5, 6, script_hs);
    }
    restore();
    TIPtext = moGlb.langTranslate('Workflow progress') + ': ' + v + '%';
};
moLibrary.wf_phase = function () {
    var sval = parseInt(JStore[f_cod].f_percentage), script_hs = 11, script_ts = (parseInt(Yi - script_hs) / 2),
            script_col = ['#FF0000', '#FF2800', '#FF6300', '#FFA300', '#FFDA00', '#FFFF00', '#BCF017', '#7DD02C', '#42AD3E', '#17964B'];
    save();
    if (!sval) {
        sval = 10;
        script_col = ['#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000', '#FF0000'];
    } else {
        sval = parseInt(sval / 100 * 10);
    }
    for (script_r = 0; script_r < sval; script_r++) {
        script_ln = parseInt(script_r);
        fillStyle = script_col[script_ln];
        fillRect(xfield + script_r * 8, yfield + 20, 7, 4);
    }
    restore();
    TIPtext = moGlb.langTranslate('Workflow phase') + ': ' + v;

    var phname = v;
    if (!phname)
        phname = "";
    else {
        phname = moGlb.langTranslate(phname);
        fillText(phname, xfield - 1, y + script_ts + 5);
    }
};
moLibrary.category_cod = ['', 58595, 58573, 58608, 58608, 58460, 58611, 58424, 58425, 58611, 58595, 58424, 58645, 58583, '', 58385, '', '', '', 58460, 58379];
moLibrary.category = function () {

    if (v) {
        var script_x = X1 + d, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2);
        var col = colrs[2];
        if (moLibrary.category_cod[v] == 58583)
            col = '#C61201';
        try {
            moCnvUtils.cnvIconFont(BDN, moLibrary.category_cod[v], script_x + 20, y + script_ts - 1, 16, col);/*drawImage(moGlb.img['delegates/wo'+v+'.png'], script_x+20, y+script_ts);*/
        } catch (ex) {/*moDebug.log("Image not found");*/
        }
        var lbl = ['', moGlb.langTranslate("Corrective"), moGlb.langTranslate('Purchase Order'), moGlb.langTranslate('Planned maintenance'), moGlb.langTranslate("Planned"), moGlb.langTranslate("Inspection"), moGlb.langTranslate("On Condition"), moGlb.langTranslate("Meter Reading"), moGlb.langTranslate('Reservation'), moGlb.langTranslate('Condition'), moGlb.langTranslate("Corrective from Negative Task"), moGlb.langTranslate('Meter'), moGlb.langTranslate('Work Request'), moGlb.langTranslate('Emergency'), '', '', '', '', '', moGlb.langTranslate("Planned inspection"), moGlb.langTranslate("Contract Expiration")];
        TIPtext = lbl[v];
    }
};

moLibrary.frequency_translate = function () {
    if (v) {
        var fr = v.split(" ");
        var ris = [];
        for (var i in fr) {
            ris.push(moGlb.langTranslate(fr[i]));
        }
        fillText(ris.join(" "), xfield, yfield + 21);
    }
};

moLibrary.get_text_row = function () {
    if (moCnvUtils.measureText(t, f) > x21)
        t = moGlb.reduceText(t, f, x21);
    fillStyle = colr;
    if (t == "null" || t === null)
        t = "";
    fillText(t, X1 + d + 7, y + 8 + (Yi - 7) / 2);
    TIPtext = v;
}

moLibrary.action_type = function () {
    if (v) {
        var icon = 0;
        if (v == 'email') {
            icon = 58514;
            TIPtext = 'Email';
        }
        if (v == 'priority') {
            icon = 58540;
            TIPtext = 'Priority';
        }

        if (icon == 0)
            moDebug.log("Type action not found");
        //var cod = [ '', 58595, '', 58608, 58608, 58460, 58611, 58424, '', '', 58595,'','',58583];
        var script_x = X1 + d, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2);
        // var col = colrs[2];
        //if(cod[v] == 58583) col = '#C61201';
        try {
            moCnvUtils.cnvIconFont(BDN, icon, script_x + 20, y + script_ts - 1, 16, colrs[2]);/*drawImage(moGlb.img['delegates/wo'+v+'.png'], script_x+20, y+script_ts);*/
        } catch (ex) {/*moDebug.log("Image not found");*/
        }
        //var lbl = [ '', "Corrective", '', '', "Periodic", "Inspection", "On Condition", "Meter Reading", '', '', "Corrective from Negative Task",'','','Emergency' ];
        // TIPtext=lbl[v];
    }
};
moLibrary.online = function () {
    if (v) {
        var script_x = X1 + d, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2);
        var lbl = ['offline', 'pc', 'mobile'];
        if (v > 0) {
            try {
                /*drawImage(moGlb.img['delegates/'+lbl[v]+'.png'], script_x+20, y+script_ts)*/moCnvUtils.cnvIconFont(BDN, 58621, script_x + 20, y + script_ts - 1, 16, colrs[2]);
            } catch (ex) {/*moDebug.log("Image for connection status "+lbl[v]+".png not found");*/
            }
        }
        TIPtext = (v > 0 ? "online from " : "") + lbl[v];
    }
};
moLibrary.connection_time = function () {
    if (v) {
        v = parseInt(v);
        var hh = Math.floor(v / (60 * 60)), ii = v - (hh * 60 * 60), ss = 0;
        if (ii > 60) {
            var iii = Math.floor(ii / 60);
            ss = ii - (iii * 60);
            ii = iii;
        } else {
            ss = ii;
            ii = 0;
        }

        if (ii < 10)
            ii = '0' + ii;
        if (ss < 10)
            ss = '0' + ss;
        fillText(hh + ":" + ii + ":" + ss, xfield, yfield + 21);
    }
};
//IGNORA
moLibrary.empty_spare = function () {
    if (v == 1) {
        var script_x = X1 + d, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2);
        try {
            moCnvUtils.cnvIconFont(BDN, 58540, script_x + 20, y + script_ts, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        //var lbl={"1":"Corrective","4":"Periodic","5":"Inspection","6":"On Condition","7":"Meter Reading","10":"Corrective from Negative Task"};
        //TIPtext=lbl[v];
    }
};

moLibrary.priority = function () {
    //  ACTIONclick='alert(12345);';
    if (moGlb.priorities[v]) {
        var img = moGlb.priorities[v].color;

        /*if(v == "3") img = "#B00";
         else if(v == "2") img = "#E80";
         else if(v == "1") img = "#080";*/
        if (!img)
            fillText("", X1 + d, y + (parseInt(Yi - 11) / 2) + 11);
        else {
            moCnvUtils.cnvIconFont(BDN, 58540, X1 + d + 15, y + (parseInt(Yi - 14) / 2) - 1, 16, img);
            //  drawImage(moGlb.img['delegates/alert_'+img+'.png'], X1+d+15, y+(parseInt(Yi-14)/2));
        }
    }
};
//IGNORA
moLibrary.stock_type = function () {
    var text_stock = "Not Defined";
    if (v == "1")
        text_stock = "Stock";
    else if (v == "2")
        text_stock = "Non-Stock";
    fillText(text_stock, X1 + d, y + (parseInt(Yi - 11) / 2) + 11);
};

moLibrary.datetime = function () {
    if (v != 0)
        fillText(moLibrary.dateFormat(v, moGlb.localization.f_datetime), X1 + d, y + parseInt((Yi - 11) / 2) + 11);
};
moLibrary.date = function () {
    if (v > 0)
        fillText(moGlb.langTranslate(moLibrary.dateFormat(v, "{D}")) + " " + moLibrary.dateFormat(v, moGlb.localization.f_date), X1 + d, y + parseInt((Yi - 11) / 2) + 11);
};
moLibrary.time = function () {
    if (v > 0)
        fillText(moLibrary.dateFormat(v, moGlb.localization.f_time), X1 + d, y + parseInt((Yi - 11) / 2) + 11);
};
//IGNORA
moLibrary.dateFormat = function (ts, frmt) {
    return new Date(ts * 1000).format(frmt);
};
moLibrary.bool = function () {
    if (moGlb.isset(v) && !moGlb.isempty(v) && v != 'false' && v != '0') {
        //try {drawImage(moGlb.img['delegates/tick.png'], X1+d, y+(parseInt(Yi-14)/2));} catch(ex) {moDebug.log("Image tick.png not found");}
        moCnvUtils.cnvIconFont(BDN, 58786, X1 + d + 15, y + (parseInt(Yi - 14) / 2) - 1, 16, '#a1c24d');
    }
};

//come normal bool ma pone una x rossa quando il valore è 0,empty,null,false
moLibrary.bool_2 = function () {
    if (moGlb.isset(v) && !moGlb.isempty(v) && v != 'false' && v !== '0') {
        //try {drawImage(moGlb.img['delegates/tick.png'], X1+d, y+(parseInt(Yi-14)/2));} catch(ex) {moDebug.log("Image tick.png not found");}
        moCnvUtils.cnvIconFont(BDN, 58786, X1 + d + 15, y + (parseInt(Yi - 14) / 2) - 1, 16, '#a1c24d');
    } else {
        moCnvUtils.cnvIconFont(BDN, 58787, X1 + d + 15, y + (parseInt(Yi - 14) / 2) - 1, 16, '#EC4949');
    }
};

//doc_attach_onload = function (o) {with(o) {if(value) setProperties({description: value.split("|")[1]});}};
moLibrary.document_attach = function () {
    if (v) {
        var vv = v.split("|");
        fillText(vv.length > 1 ? vv[1] : vv[0], xfield, yfield + 21);
        ACTIONclick = 'viewDocument';
    }
};
moLibrary.gender = function () {
    try {
        moCnvUtils.cnvIconFont(BDN, 58544, X1 + d, y + (parseInt(Yi - 24) / 2), 16, (parseInt(v) == 1 ? '#e95dac' : '#2886b5'));/*drawImage(moGlb.img["thumbnails/"+(parseInt(v) == 1?'wo':'')+"man.png"], X1+d, y+(parseInt(Yi-24)/2), 24, 24);*/
    } catch (ex) {
        moDebug.log("Image not found");
    }
};

/*disegna cerchietto di status in treegrid*/
//IGNORA
moLibrary.draw_flag = function (ctx, x, y, dim, lang) {
    with (ctx) {
        //v = 1 Eng
        if (lang == 1) {
            //rettangolo blu
            fillStyle = 'rgb(88, 107, 200)';
            fillRect(x, y, dim + 10, dim);
            strokeRect(x, y, dim + 10, dim);

            //linee diagonali bianche
            lineWidth = 3;
            strokeStyle = '#FFF';
            beginPath();
            moveTo(x, y);
            lineTo(x + (dim + 10), y + dim);
            closePath();
            stroke();

            strokeStyle = '#FFF';
            beginPath();
            moveTo(x + (dim + 10), y);
            lineTo(x, y + dim);
            closePath();
            stroke();

            //linee diagonali rosse
            lineWidth = 1;
            strokeStyle = '#EC4949';
            beginPath();
            moveTo(x, y);
            lineTo(x + (dim + 10), y + dim);
            closePath();
            stroke();

            strokeStyle = '#EC4949';
            beginPath();
            moveTo(x + (dim + 10), y);
            lineTo(x, y + dim);
            closePath();
            stroke();


            //rettangolo verticale bianco
            fillStyle = '#FFF';
            fillRect((x + (((dim + 10) / 5) * 2)), y, ((dim + 10) / 5) + 1, dim);

            //rettangolo orizzonatale bianco
            fillStyle = '#FFF';
            fillRect(x, y + (dim / 3), dim + 10, (dim / 3) + 1);

            //rettangolo verticale rosso
            fillStyle = '#EC4949';
            fillRect((x + (((dim + 10) / 5.5) * 2) + 2), y, (dim + 10) / 6, dim);

            //rettangolo orizzonatale rosso
            fillStyle = '#EC4949';
            fillRect(x, y + (dim / 3.5) + 2, dim + 10, dim / 4);

            //contorno nero
            strokeStyle = '#000';
            strokeRect(x, y, dim + 10, dim);


        }
        //v = 2 Ita
        if (lang == 2) {
            fillStyle = '#EC4949';
            fillRect(x, y, (dim + 10) / 3, dim);
            fillStyle = '#5BA567';
            fillRect(x + (((dim + 10) / 3) * 2), y, (dim + 10) / 3, dim);
            strokeRect(x, y, dim + 10, dim);
        }
        //v = 3 Fr
        if (lang == 3) {
            fillStyle = '#EC4949';
            fillRect(x, y, (dim + 10) / 3, dim);
            fillStyle = 'rgb(88, 107, 200)';
            fillRect(x + (((dim + 10) / 3) * 2), y, (dim + 10) / 3, dim);
            strokeRect(x, y, dim + 10, dim);
        }
        //v = 4 Sp
        if (lang == 4) {
            fillStyle = '#EC4949';
            fillRect(x, y, dim + 10, dim / 3);
            fillStyle = '#F9F27E';
            fillRect(x, y + (dim / 3), (dim + 10), dim / 3);
            fillStyle = '#EC4949';
            fillRect(x, y + ((dim / 3) * 2), dim + 10, dim / 3);
            strokeRect(x, y, dim + 10, dim);
        }
    }
}
//IGNORA
moLibrary.language = function () {
    if (!moGlb.isempty(v)) {
        try {/*console.log(v);
         switch(v){
         case '1': mdl_usr_edit.setEditData('fc_usr_language','english');
         break;
         case '2': mdl_usr_edit.setEditData('fc_usr_language','italian');
         break;
         case '3': mdl_usr_edit.setEditData('fc_usr_language','french');
         break;
         case '4': mdl_usr_edit.setEditData('fc_usr_language','spanish');
         break;
         }*/
            /*moLibrary.draw_flag(BDN,X1+d, y+(parseInt(Yi-24)/2),18,v);*//*drawImage(moGlb.img["delegates/lang"+parseInt(v)+".png"], X1+d, y+(parseInt(Yi-24)/2), 24, 24);*/
        } catch (ex) {
            moDebug.log("Image lang" + parseInt(v) + ".png" + " not found " + ex);
        }
    }
};
//IGNORA
moLibrary.costs = function () {
    if (parseInt(v)) {
        var script_w = 100, script_hs = 11, script_ts = (parseInt(Yi - script_hs) / 2),
                script_col = ['#2080B0', '#8080f0'], script_ns = script_col.length, script_wn = script_w / 1000 * v, script_n = 1 / (script_ns - 1), script_r,
                script_ln = createLinearGradient(xfield, y, xfield + script_w - 2, y + Yi);
        for (script_r = 0; script_r < script_ns; script_r++)
            script_ln.addColorStop(script_r * script_n, script_col[script_r]);
        save();
        fillStyle = script_ln;
        fillRect(xfield + 0.5, y + script_ts + 0.5, script_wn, script_hs);
        restore();
    }
};
//IGNORA
moLibrary.totcosts = function () {
    if (parseInt(v)) {
        var script_w = 100, script_ds = X1 + d, script_hs = 11, script_ts = (parseInt(Yi - script_hs) / 2), script_col = ['#f44', '#f88'],
                script_ns = script_col.length, script_wn = script_w / 1000 * v, script_n = 1 / (script_ns - 1), script_r,
                script_ln = createLinearGradient(xfield, y, xfield + script_w - 2, y + Yi);
        for (script_r = 0; script_r < script_ns; script_r++)
            script_ln.addColorStop(script_r * script_n, script_col[script_r]);
        save();
        fillStyle = script_ln;
        fillRect(xfield + 0.5, y + script_ts + 0.5, script_wn, script_hs);
        restore();
    }
};
//IGNORA
moLibrary.istog2 = function () {
    var script_w = 100, script_ds = X1 + d, script_hs = 11, script_ts = (parseInt(Yi - script_hs) / 2), script_col = ['#F00', '#080'], v2 = rig['fc_wo_total_costs'],
            script_wn = 1, script_v1 = script_wn * v, script_v2 = script_wn * v2, script_tot = script_v1 + script_v2, script_r, script_ln;
    if (script_tot > 1) {
        var script_ln = createLinearGradient(script_ds, y, script_ds + script_tot - 2, y);
        script_ln.addColorStop(0, script_col[0]);
        script_ln.addColorStop(script_v1 / script_tot, script_col[0]);
        script_ln.addColorStop(script_v1 / script_tot + 0.001, script_col[1]);
        script_ln.addColorStop(1, script_col[1]);
        save();
        fillStyle = script_ln;
        fillRect(script_ds + 0.5, y + script_ts + 0.5, script_tot, script_hs);
        restore();
    }
};
//IGNORA
moLibrary.budget = function () {
    var script_w = 100, script_hs = 11, script_ts = (parseInt(Yi - script_hs) / 2), script_col = ['#F00', '#080'], v2 = rig['fc_wo_total_costs'],
            script_wn = 0.1, script_tot = script_wn * v, script_v2 = script_wn * v2, script_v1 = script_tot - script_v2, script_r, script_ln;
    if (script_tot > 1) {
        var script_ln = createLinearGradient(xfield, yfield, xfield + script_tot - 2, yfield);
        script_ln.addColorStop(0, script_col[0]);
        script_ln.addColorStop(script_v1 / script_tot, script_col[0]);
        script_ln.addColorStop(script_v1 / script_tot + 0.001, script_col[1]);
        script_ln.addColorStop(1, script_col[1]);
        save();
        fillStyle = script_ln;
        fillRect(xfield + 0.5, yfield + script_ts + 0.5, script_tot, script_hs);
        restore();
    }
};/*
 moLibrary.users = function () {
 if(!moGlb.isset(v) || moGlb.isempty(v)) {
 moCnvUtils.cnvIconFont(BDN, 58544, X1+d+20, y+(parseInt(Yi-24)/2), 16, (parseInt(JStore[f_cod]["fc_usr_gender"]) == 1?'#e01a1a':'#162ce0'));
 } else {
 var img = new Image(), av = v.split("|");
 img.src = moGlb.baseUrl+"/getDocument.php?session_id="+av[0]+"&size=thumbnail&uploadType=image";
 img.onload = function () {
 var script_ds=X1+d+20, script_hs=24, script_ts=(parseInt(Yi-script_hs)/2);
 save();
 BDN.drawImage(img, script_ds, y+script_ts, script_hs,script_hs);
 restore();
 };
 }
 };*/
moLibrary.user = function () {
    var field = fieldb.replace('avatar', 'name'), script_x = X1 + d + 10, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2);
    if (v) {
        var av = v.split("|"),
                url = moGlb.baseUrl + "/document/get-file?session_id=" + av[0] + "&size=thumbnail&uploadType=image",
                script_ds = X1 + d + 3, script_hs = 24, script_ts = (parseInt(Yi - script_hs) / 2),
                script_ll = Hlw + optr,
                script_lock = f_lock;

        moCnvUtils.draw_canvas_image(url, BDN, script_ds, y + script_ts, script_hs, script_hs, script_ll, script_lock);   // url, canvas_obj, left,top. width,height

        /*
         img = new Image(), av = v.split("|");
         img.src = moGlb.baseUrl+"/document/get-file?session_id="+av[0]+"&size=thumbnail&uploadType=image";
         img.onload = function () {
         var script_ds=X1+d+3, script_hs=24, script_ts=(parseInt(Yi-script_hs)/2);
         save(); drawImage(img, script_ds, y+script_ts, script_hs,script_hs); restore();
         };
         */



    } else
        moCnvUtils.cnvIconFont(BDN, 58544, script_x - 10, y + script_ts - 3, 16, (parseInt(JStore[f_cod].fc_usr_gender) == 1 ? '#E95DAC' : '#2886B5'));

    // Tooltip
    if (JStore[f_cod][field]) {
        TIPtext = JStore[f_cod][field];
        fillText(JStore[f_cod][field], xfield + 30, yfield + 21);
    }
};
//IGNORA
moLibrary.euro = function () {
    if (v)
        v = parseFloat(v).toFixed(2);
    else
        v = parseFloat(0).toFixed(2);
    v += " \\u20AC";
    fillText(v, xfield, yfield + 21);
};
moLibrary.cost = function () {
    if (v)
        v = parseFloat(v).toFixed(2);
    else
        v = parseFloat(0).toFixed(2);
    fillText(v + " " + moGlb.CURRENCY, xfield, yfield + 21);
};
moLibrary.currency = function () {
    var thousand = moGlb.localization.f_thousand_separator, decimal = moGlb.localization.f_decimal_separator;
    if (v) {
        v = parseFloat(v).toFixed(2);
        if (!isNaN(v)) {
            var splitted_n = v.split('.'), n = '';
            for (var i = 0; i < splitted_n[0].length; i++) {
                if (i > 0 && (i % 3) == 0)
                    n = thousand + n;
                n = splitted_n[0][splitted_n[0].length - i - 1] + n;
            }
            v = n + decimal + (splitted_n.length == 1 ? '00' : splitted_n[1]);
            v = /*parseFloat(v).toFixed(2)+" " parte aggiunta da quintino... perch?*/v + moGlb.CURRENCY //EDIT BY Q;

        }
    } else
        v = '';//v = "0"+decimal+"00";
    //correggere come convertCurrency?
    fillText(v, xfield, yfield + 21);
};

moLibrary.currency_inside = function () {
    var thousand = moGlb.localization.f_thousand_separator, decimal = moGlb.localization.f_decimal_separator;

    if (v) {
        v = parseFloat(v).toFixed(2);
        if (!isNaN(v)) {
            var splitted_n = v.split('.'), n = '';
            for (var i = 0; i < splitted_n[0].length; i++) {
                if (i > 0 && (i % 3) == 0)
                    n = thousand + n;
                n = splitted_n[0][splitted_n[0].length - i - 1] + n;

            }
            v = n + decimal + (splitted_n.length == 1 ? '00' : splitted_n[1]);

        }
    } else
        v = "0" + decimal + "00";
    if (typeof (arrpd[f_cod]) == 'undefined') {
        fillText(v + " " + moGlb.CURRENCY, xfield, yfield + 21);
    }
};
moLibrary.progress = function () {
    if (v) {
        v = "" + v;
        for (var str_i = 0; str_i < 5 - v.length; i++)
            v = "0" + v;
        fillText(v, xfield, yfield + 21);
    }
};
//IGNORA
moLibrary.contract_status = function () {
    if (v) {
        var script_x = X1 + d + 10, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2)/*, img_name = 'bullet_'+(v==1?'orange':(v==2?'red':'green'))+'.png'*/;
        try {
            moCnvUtils.cnvIconFont(BDN, 58374, script_x, y + script_ts, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
    }
};

moLibrary.set_total_cost = function () {
    var downtime = txtfld_wo_edit_costs_for_downtime.getValue('double'), materials = txtfld_wo_edit_costs_for_material.getValue('double'),
            labors = txtfld_wo_edit_costs_for_labor.getValue('double'), tools = txtfld_wo_edit_costs_for_tool.getValue('double'),
            services = txtfld_wo_edit_costs_for_service.getValue('double');
    var scr_tot = (parseFloat(downtime) + parseFloat(materials) + parseFloat(labors) + parseFloat(tools) + parseFloat(services)).toFixed(2);
    txtfld_wo_edit_total_costs.set(scr_tot);
};
moLibrary.asset_opened_wo = function () {
    if (v > 0) {
        try {
            /*drawImage(moGlb.img['delegates/traffic_light.png'], xfield+6, yfield+6);*/moCnvUtils.cnvIconFont(BDN, 58646, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('workorders associated to this asset');
    }
};
moLibrary.asset_opened_std = function () {
    if (v > 0) {
        try {
            /*drawImage(moGlb.img['delegates/wo10.png'], xfield+6, yfield+6);*/moCnvUtils.cnvIconFont(BDN, 58644, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('standard workorders associated to this asset');
    }
};
moLibrary.asset_opened_pm = function () {
    if (v > 0) {
        try {
            /*drawImage(moGlb.img['delegates/periodic.png'], xfield+6, yfield+6);*/moCnvUtils.cnvIconFont(BDN, 58605, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('periodic maintenances associated to this asset');
    }
};
moLibrary.asset_opened_on_condition = function () {
    if (v > 0) {
        try {
            /*drawImage(moGlb.img['delegates/on_condition.png'], xfield+6, yfield+6);*/moCnvUtils.cnvIconFont(BDN, 58510, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('on condition workorders associated to this asset');
    }
};
//????
moLibrary.asset_downtime = function () {
    if (v > 0) {
        try {
            moCnvUtils.cnvIconFont(BDN, 58541, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('This asset has a unplanned downtime right now');
    }
};
moLibrary.asset_planned_downtime = function () {
    if (v > 0) {
        try {
            moCnvUtils.cnvIconFont(BDN, 58541, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('This asset has a planned downtime right now');
    }
};
moLibrary.asset_documents = function () {
    if (v > 0) {
        try {
            moCnvUtils.cnvIconFont(BDN, 58502, xfield + 6, yfield + 6, 16, colrs[2]);/*drawImage(moGlb.img['buttons/page.png'], xfield+6, yfield+6);*/
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('documents associated to this asset');
    }
};
moLibrary.asset_contracts = function () {
    if (v > 0) {
        try {
            /*drawImage(moGlb.img['buttons/contract.png'], xfield+6, yfield+6);*/moCnvUtils.cnvIconFont(BDN, 58374, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('contracts associated to this asset');
    }
};

moLibrary.mdl_wo_comment_tg_tg_script = function () {
    var scr_tot = 0;
    for (scr_k in childJStore) {
        scr_tot++;
    }

    mdl_wo_edit.data.fc_comments = scr_tot;
};

moLibrary.fc_comments = function () {
    v = parseInt(v);
    if (v) {
        moCnvUtils.cnvIconFont(BDN, 58511, xfield + 6, yfield + 6, 16, colrs[2]);
        var text = moGlb.langTranslate("There " + (v == 1 ? "is" : "are")) + " " + v + " " + moGlb.langTranslate("comment" + (v == 1 ? "" : "s"));
        if (moGlb.isset(JStore[f_cod].f_last_comment))
            text += '\n' + JStore[f_cod].f_last_comment;
        TIPtext = text;
    }
};
//IGNORA
moLibrary.wo_inventory = function () {
    if (v > 0) {
        try {
            moCnvUtils.cnvIconFont(BDN, 58578, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('parts associated to this workorder');
    }
};
//IGNORA
moLibrary.wo_labor = function () {
    if (v > 0) {
        try {
            moCnvUtils.cnvIconFont(BDN, 58544, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('resources associated to this workorder');
    }
};
//IGNORA
moLibrary.wo_tool = function () {
    if (v > 0) {
        try {
            moCnvUtils.cnvIconFont(BDN, 58576, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
        TIPtext = moGlb.langTranslate('There are') + ' ' + v + ' ' + moGlb.langTranslate('tools associated to this workorder');
    }
};
moLibrary.percent = function () {
    var pv = parseFloat(v);
    if (!isNaN(pv))
        v = pv.toFixed(2) + "%";
    fillText(v, xfield, yfield + 21);
};
//IGNORA
moLibrary.asset_planned_downtime_pm = function () {
    if (v == 1) {
        try {
            moCnvUtils.cnvIconFont(BDN, 58541, xfield + 6, yfield + 6, 16, colrs[2]);
        } catch (ex) {
            moDebug.log("Image not found");
        }
    }
};
//Icona in asset non viene messa!!!
moLibrary.icon = function () {

    if (moGlb.isset(v) && !moGlb.isempty(v)) {
        var av = v.split("|"),
                url = moGlb.baseUrl + "/document/get-file?session_id=" + av[0] + "&size=thumbnail&uploadType=image",
                script_ds = X1 + d + 3, script_hs = 24, script_ts = (parseInt(Yi - script_hs) / 2),
                script_ll = Hlw + cnvLS,
                script_lock = f_lock;

        moCnvUtils.draw_canvas_image(url, BDN, script_ds, y + script_ts, script_hs, script_hs, script_ll, script_lock);   // url, canvas_obj, left,top. width,height
    }

};

/*disegna cerchietto di status in treegrid*/
moLibrary.draw_circle = function (ctx, color, x, y, r) {
    with (ctx) {
        beginPath();
        fillStyle = color;
        arc(x, y, r, 0, Math.PI * 2, true);
        fill();
        closePath();
    }
}
//data.fc_comment_phase_id = mdl_wo_edit.data.f_phase_id;
moLibrary.translate = function () {
    if (v)
        v = moGlb.langTranslate(v);
    fillText(v, xfield, yfield + 21);
};
moLibrary.status = function () {
    v = v || 0;
    v = parseInt(v);
    if (v) {
        //moCnvUtils.cnvIconFont(BDN,58540,X1+d+15,y+(parseInt(Yi-14)/2)-1,16,img);
        var script_x = X1 + d + 10, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2)/*, img_name = 'bullet_'+(v==1?'green':(v>=2&&v<5?'orange':'red'))+'.png'*/;
        var img_color = (v == 1 ? '#080' : (v >= 2 && v < 5 ? '#E80' : '#B00'));
        try {
            moLibrary.draw_circle(BDN, img_color, script_x + 1, y + script_ts + 7.2, 4);
            if (JStore[f_cod]['f_name']) {
                TIPtext = moGlb.langTranslate(JStore[f_cod]['f_name']);
                fillText(moGlb.langTranslate(JStore[f_cod]['f_name']), xfield + 20, yfield + 20.6);
            }
            /*moCnvUtils.cnvIconFont(BDN,58638,script_x,y+script_ts,16,img_color);--drawImage(moGlb.img['delegates/'+img_name], script_x, y+script_ts);*/
        } catch (ex) {
            moDebug.log("Image " + img_name + " not found");
        }
    }
};




//???? IGNORA
moLibrary.priority_number = function () {
    vl = "";
    if (v == 3)
        vl = "A";
    else if (v == 2)
        vl = "B";
    else if (v == 1)
        vl = "C";
    fillText(vl, X1 + d, y + (parseInt(Yi - 11) / 2) + 11);
};
moLibrary.bkm_type = function () {
    var types = ["text", "numeric", "boolean", "date", "select", "image"];
    fillText(types[parseInt(v)], xfield, yfield + 21);
};
moLibrary.fc_dmn_process_type = function () {
    var img = {6: [58772, '#496670'], 7: [58773, '#a1c24d']}, script_x = X1 + d + 10, script_hs = 14, script_ts = (parseInt(Yi - script_hs) / 2), val = img[parseInt(v)];
    moCnvUtils.cnvIconFont(BDN, val[0], script_x + 20, y + script_ts - 1, 16, val[1]);
};
/**
 * Treegrid
 */
moLibrary.mdl_wo_inv_tg_tg_script = function () {
    var scr_tot = 0, ptot = 0;
    for (scr_k in childJStore) {
        ptot = parseFloat(childJStore[scr_k].fc_inv_cost);
        if (!isNaN(ptot))
            scr_tot += ptot;
    }
    txtfld_wo_edit_costs_for_material.set(scr_tot.toFixed(2));
    eval(moLibrary.getSource('set_total_cost'));
};
moLibrary.fc_inv_cost_pair_cross = function () {
    var tot = 0, ptot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++) {
        ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_inv_cost);
        if (!isNaN(ptot))
            tot += ptot;
    }
    fillStyle = '#000';
    fillText(tot.toFixed(2), xfield, yfield + 21);
};
//????
moLibrary.mdl_wo_tool_tg_tg_script = function () {
    var scr_tot = 0, ptot = 0;
    for (scr_k in childJStore) {
        ptot = parseFloat(childJStore[scr_k].fc_tool_cost);
        if (!isNaN(ptot))
            scr_tot += ptot;
    }
    txtfld_wo_edit_costs_for_tool.set(scr_tot.toFixed(2));
    eval(moLibrary.getSource('set_total_cost'));
};
//????
moLibrary.fc_tool_cost_pair_cross = function () {
    var tot = 0, ptot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++) {
        ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_tool_cost);
        if (!isNaN(ptot))
            tot += ptot;
    }
    fillStyle = '#000';
    fillText(tot.toFixed(2), xfield, yfield + 21);
};
//IGNORA
moLibrary.fc_po_material_total_price_pair_cross = function () {
    var tot = 0, ptot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++) {
        ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_po_material_total_price);
        if (!isNaN(ptot))
            tot += ptot;
    }
    fillStyle = '#000';
    fillText(tot.toFixed(2), xfield, yfield + 21);
};

moLibrary.mdl_std_rsc_tg_tg_script = function () {
    // Costs
    var scr_tot = 0, ptot = 0, owner = owner = {name: '', email: '', phone: ''};
    for (scr_k in childJStore) {
        ptot = parseFloat(childJStore[scr_k].fc_rsc_cost);
        if (!isNaN(ptot))
            scr_tot += ptot;
    }
    txtfld_std_edit_costs_for_labor.set(scr_tot.toFixed(2));
    eval(moLibrary.getSource('set_total_cost'));
};

moLibrary.mdl_wo_rsc_tg_tg_script = function () {
    // Costs
    var scr_tot = 0, ptot = 0, owner = owner = {name: '', email: '', phone: ''};
    for (scr_k in childJStore) {
        ptot = parseFloat(childJStore[scr_k].fc_rsc_cost);
        if (!isNaN(ptot))
            scr_tot += ptot;
    }
    txtfld_wo_edit_costs_for_labor.set(scr_tot.toFixed(2));
    eval(moLibrary.getSource('set_total_cost'));
};

moLibrary.fc_rsc_total_hours_pair_cross = function () {
    var tot = 0, ptot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++) {
        ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_rsc_total_hours);
        if (!isNaN(ptot))
            tot += ptot;
    }
    fillStyle = '#000';
    fillText(tot.toFixed(2), xfield, yfield + 21);
};

moLibrary.fc_rsc_cost_pair_cross = function () {
    var tot = 0, ptot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++) {
        ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_rsc_cost);
        if (!isNaN(ptot))
            tot += ptot;
    }
    //clearRect(xfield, yfield,x21, Yi);
    fillStyle = '#000';
    fillText(moLibrary.convertCurrency(tot), xfield, yfield + 21);
};

moLibrary.mdl_wo_supply_tg_tg_script = function () {
    var scr_tot = 0, ptot = 0;

    for (scr_k in childJStore) {
        ptot = parseFloat(childJStore[scr_k].fc_supply_total_cost);
        if (!isNaN(ptot)) {
            scr_tot += ptot;
        }
    }
    //console.log(scr_tot.toFixed(2));
    txtfld_wo_edit_costs_for_service.set(scr_tot.toFixed(2));
    /*for(scr_k1 in JStore) {
     JStore[scr_k1].fc_supply_total_cost = scr_tot.toFixed(2);
     }*/
    eval(moLibrary.getSource('set_total_cost'));
};

moLibrary.mdl_std_supply_tg_tg_script = function () {
    var scr_tot = 0, ptot = 0;

    for (scr_k in childJStore) {
        ptot = parseFloat(childJStore[scr_k].fc_supply_total_cost);
        if (!isNaN(ptot)) {
            scr_tot += ptot;
        }
    }
    //console.log(scr_tot.toFixed(2));
    txtfld_std_edit_costs_for_service.set(scr_tot.toFixed(2));
    /*for(scr_k1 in JStore) {
     JStore[scr_k1].fc_supply_total_cost = scr_tot.toFixed(2);
     }*/
    eval(moLibrary.getSource('set_total_cost'));
};
moLibrary.txtfld_cond_supply_edit_quantity_onchange = function (o) {
    moLibrary.txtfld_wo_supply_edit_quantity_onchange(o);
}
moLibrary.txtfld_pm_supply_edit_quantity_onchange = function (o) {
    moLibrary.txtfld_wo_supply_edit_quantity_onchange(o);
}
moLibrary.txtfld_std_supply_edit_quantity_onchange = function (o) {
    moLibrary.txtfld_wo_supply_edit_quantity_onchange(o);
}
moLibrary.txtfld_wo_supply_edit_quantity_onchange = function (o) {
    var mod = moLibrary.getModule(o.name);
    var quantity = parseFloat(o.get());
    //console.log(mdl_wo_supply_edit.data);
    var cost = eval('mdl_' + mod + '_edit').data.fc_supply_cost;

    eval('txtfld_' + mod + '_edit_total_cost').set(quantity * cost);
}
moLibrary.fc_supply_total_cost_pair_cross = function () {
    var tot = 0, ptot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++) {
        ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_supply_total_cost);
        if (!isNaN(ptot))
            tot += ptot;
    }
    fillStyle = '#000';
    fillText(moLibrary.convertCurrency(tot), xfield, yfield + 21);
};
/*moLibrary.fc_vendor_cost_pair_cross = function () {
 var tot = 0, ptot = 0;
 for(var scr_r=0; scr_r<arrpd[f_cod].length; scr_r++) {
 ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_vendor_cost);
 if(!isNaN(ptot)) tot += ptot;
 }
 fillStyle='#000';fillText(tot.toFixed(2), xfield, yfield+21);
 };*/

moLibrary.fc_material_cost_pair_cross = function () {
    var tot = 0, ptot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++) {
        ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_material_cost);
        if (!isNaN(ptot))
            tot += ptot;
    }
    //clearRect(xfield, yfield,x21, Yi);
    fillStyle = '#000';
    fillText(moLibrary.convertCurrency(tot), xfield, yfield + 21);
};

moLibrary.convertCurrency = function (v) {
    var thousand = moGlb.localization.f_thousand_separator, decimal = moGlb.localization.f_decimal_separator;

    if (v) {
        v = parseFloat(v).toFixed(2);
        if (!isNaN(v)) {
            var splitted_n = v.split('.'), n = '';
            for (var i = 0; i < splitted_n[0].length; i++) {
                if (i > 0 && (i % 3) == 0)
                    n = thousand + n;
                n = splitted_n[0][splitted_n[0].length - i - 1] + n;

            }
            v = n + decimal + (splitted_n.length == 1 ? '00' : splitted_n[1]);

        }
    } else
        v = "0" + decimal + "00";

    return v + " " + moGlb.CURRENCY;
}

//IGNORA
moLibrary.mdl_std_asset_tg_tg_script = function () {
    var scr_tot = 0, ptot = 0, wi = [];
    for (scr_k in childJStore) {
        ptot = parseFloat(childJStore[scr_k].fc_asset_downtime_cost);
        if (!isNaN(ptot))
            scr_tot += ptot;
    }
    for (scr_k in JStore)
        if (JStore[scr_k].fc_work_instructions)
            wi.push(JStore[scr_k].fc_work_instructions);
    txtfld_std_edit_costs_for_downtime.set(scr_tot.toFixed(2));
    if (moGlb.isempty(txtfld_std_edit_work_instructions.get()) && wi.length > 0)
        txtfld_std_edit_work_instructions.set('------------------------------------\r\n' + moGlb.langTranslate('Assets') + '\r\n------------------------------------\r\n- ' + wi.join('\r\n- '));
    eval(moLibrary.getSource('set_total_cost'));
};

//CONTROLLAREEEEEEEEEE
moLibrary.mdl_wo_asset_tg_tg_script = function () {
    var scr_tot = 0, ptot = 0, wi = [], dates = [];
    for (scr_k in childJStore) {
        ptot = parseFloat(childJStore[scr_k].fc_asset_downtime_cost);
        if (!isNaN(ptot))
            scr_tot += ptot;
    }
    txtfld_wo_edit_costs_for_downtime.set(scr_tot.toFixed(2));
    // Work instructions
    //console.log(JStore);
    for (scr_k in JStore)
        if (JStore[scr_k].fc_work_instructions != '') {
            //JStore[scr_k].fc_progress
            var v = JStore[scr_k].fc_progress;
            for (var str_i = 0; str_i < 5 - v.length; i++)
                v = "0" + v;
            wi.push('AssetID: ' + v + ' - ' + JStore[scr_k].f_title + '\r\n\r\n' + JStore[scr_k].fc_work_instructions + '\r\n------------------------------------\r\n');
        }
    if (mdl_wo_edit.data.fc_work_instructions_hidden != wi.join('\r\n')) {
        //txtfld_wo_edit_work_instructions_hidden.set(wi.join('\r\n'));

        mdl_wo_edit.setEditData('fc_work_instructions_hidden', wi.join('\r\n'));
        txtfld_wo_edit_work_instructions.set(txtfld_wo_edit_work_instructions.get() + ((txtfld_wo_edit_work_instructions.get() != '') ? "\r\n------------------------------------\r\n" : "") + wi.join('\r\n'));
    }

    eval(moLibrary.getSource('set_total_cost'));
};

moLibrary.mdl_wr_asset_tg_tg_script = function () {
    var scr_tot = 0, ptot = 0, wi = [], dates = [];

    // Work instructions
    //console.log(JStore);
    for (scr_k in JStore)
        if (JStore[scr_k].fc_work_instructions != '') {
            //JStore[scr_k].fc_progress
            var v = JStore[scr_k].fc_progress;
            for (var str_i = 0; str_i < 5 - v.length; i++)
                v = "0" + v;
            wi.push('AssetID: ' + v + ' - ' + JStore[scr_k].f_title + '\r\n\r\n' + JStore[scr_k].fc_work_instructions + '\r\n------------------------------------\r\n');
        }
    if (mdl_wr_edit.data.fc_work_instructions_hidden != wi.join('\r\n')) {
        //txtfld_wo_edit_work_instructions_hidden.set(wi.join('\r\n'));

        mdl_wr_edit.setEditData('fc_work_instructions_hidden', wi.join('\r\n'));
        txtfld_wr_edit_work_instructions.set(txtfld_wr_edit_work_instructions.get() + ((txtfld_wr_edit_work_instructions.get() != '') ? "\r\n------------------------------------\r\n" : "") + wi.join('\r\n'));
    }

    eval(moLibrary.getSource('set_total_cost'));
};

//IMPORTANTE (da propagare)
moLibrary.mdl_wo_edit_save = function (data) {
    // Overlap check for downtimes
    with (mdl_wo_asset_tg_tg) {
        for (var asset in arrpd) {
            var dts = [];
            for (var dti in arrpd[asset]) {
                var since = parseInt(childJStore[arrpd[asset][dti]].fc_asset_downtime_since),
                        till = parseInt(childJStore[arrpd[asset][dti]].fc_asset_downtime_till);
                if (!moGlb.isempty(dts)) { // first step
                    for (var i in dts)
                        if (!(dts[i].since > till || dts[i].till < since))
                            return "You cannot overlap downtimes for the same asset!";
                }
                dts.push({since: since, till: till});
            }
        }
    }
    return true;
};
//IGNORA
/*moLibrary.mdl_wr_asset_tg_tg_script = function () {
 // Work instructions
 var wi = [];
 for(scr_k in JStore) if(JStore[scr_k].fc_work_instructions) wi.push(JStore[scr_k].fc_work_instructions);
 if(moGlb.isempty(txtfld_wr_edit_work_instructions.get()) && wi.length > 0) txtfld_wr_edit_work_instructions.set('------------------------------------\r\n'+moGlb.langTranslate('Assets')+'\r\n------------------------------------\r\n- '+wi.join('\r\n- '));
 //
 };*/
moLibrary.fc_asset_downtime_cost_pair_cross = function () {
    var tot = 0, ptot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++) {
        ptot = parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_asset_downtime_cost);
        if (!isNaN(ptot))
            tot += ptot;
    }
    //clearRect(xfield, yfield,x21, Yi);
    fillStyle = '#000';
    fillText(moLibrary.convertCurrency(tot), xfield, yfield + 21);
};
moLibrary.fc_asset_downtime_hours_pair_cross = function () {
    var tot = 0;
    for (var scr_r = 0; scr_r < arrpd[f_cod].length; scr_r++)
        tot += parseFloat(childJStore[arrpd[f_cod][scr_r]].fc_asset_downtime_hours);
    fillStyle = '#000';
    if (isNaN(tot))
        tot = 0;
    fillText(tot.toFixed(2), xfield, yfield + 21);
};

/**
 * Supply
 */
moLibrary.pck_supply_edit_vendor_onchange = function (o) {
    var item = o.getResult();
    if (!moGlb.isempty(item)) {
        txtfld_supply_edit_resp_name.set(item.fc_vendor_web);
        txtfld_supply_edit_resp_phone.set(item.fc_vendor_phone);
        txtfld_supply_edit_resp_email.set(item.fc_vendor_email);
    }
};
moLibrary.pck_supply_edit_vendor_onclear = function (o) {
    txtfld_supply_edit_resp_name.set('');
    txtfld_supply_edit_resp_phone.set('');
    txtfld_supply_edit_resp_email.set('');
};
/**
 * Wo Task
 */
moLibrary.fc_task_issue_not_performed_pair_cross = function () {
    var fld, numsele = 'not performed', custscr = 'fc_task_issue_not_performed';
    var nfg = arrpd[f_cod].length, nuel = 0;
    for (var rr = 0; rr < nfg; rr++) {
        if (childJStore[ arrpd[f_cod][rr] ][custscr] == 1)
            nuel++;
    }
    fillStyle = "#4D494A";
    fillText(nuel + '/' + nfg, xfield + 8, yfield + 21);
};
moLibrary.fc_task_issue_performed_pair_cross = function () {
    var fld, numsele = 'positive', custscr = 'fc_task_issue_performed';
    var nfg = arrpd[f_cod].length, nuel = 0;
    for (var rr = 0; rr < nfg; rr++) {
        if (childJStore[ arrpd[f_cod][rr] ][custscr] == 1)
            nuel++;
    }
    fillStyle = "#047A3D";
    fillText(nuel + '/' + nfg, xfield + 8, yfield + 21);
};
moLibrary.fc_task_issue_not_executable_pair_cross = function () {
    var fld, numsele = 'not executable', custscr = 'fc_task_issue_not_executable';
    var nfg = arrpd[f_cod].length, nuel = 0;
    for (var rr = 0; rr < nfg; rr++) {
        if (childJStore[ arrpd[f_cod][rr] ][custscr] == 1)
            nuel++;
    }
    fillStyle = "#9C6508";
    fillText(nuel + '/' + nfg, xfield + 8, yfield + 21);
};
moLibrary.fc_task_quality_not_acceptable_pair_cross = function () {
    var fld, numsele = 'not acceptable', custscr = 'fc_task_quality_not_acceptable';
    var nfg = arrpd[f_cod].length, nuel = 0;
    for (var rr = 0; rr < nfg; rr++) {
        if (childJStore[ arrpd[f_cod][rr] ][custscr] == 1)
            nuel++;
    }
    fillStyle = "#9C6508";
    fillText(nuel + '/' + nfg, xfield + 8, yfield + 21);
};
moLibrary.fc_task_quality_acceptable_pair_cross = function () {
    var fld, numsele = 'acceptable', custscr = 'fc_task_quality_acceptable';
    var nfg = arrpd[f_cod].length, nuel = 0;
    for (var rr = 0; rr < nfg; rr++) {
        if (childJStore[ arrpd[f_cod][rr] ][custscr] == 1)
            nuel++;
    }
    fillStyle = "#4D494A";
    fillText(nuel + '/' + nfg, xfield + 8, yfield + 21);
};
moLibrary.fc_task_quality_good_pair_cross = function () {
    var fld, numsele = 'good', custscr = 'fc_task_quality_good';
    var nfg = arrpd[f_cod].length, nuel = 0;
    for (var rr = 0; rr < nfg; rr++) {
        if (childJStore[ arrpd[f_cod][rr] ][custscr] == 1)
            nuel++;
    }
    fillStyle = "#047A3D";
    fillText(nuel + '/' + nfg, xfield + 8, yfield + 21);
};
moLibrary.setTaskIssue = function (t) {
    var issues = ['not_performed', 'performed', 'not_executable'], task_issue = issues[t];
    with (mdl_wo_task$pm_tg_tg) {
        for (var s_chk in arrCHK) {
            var f_cod = arrCHK[s_chk];
            if (moGlb.isset(JStore[f_cod])) { // parent
                var num = arrpd[f_cod].length;
                for (var s_i in issues)
                    JStore[f_cod]["fc_task_issue_" + issues[s_i]] = "0/" + num;
                JStore[f_cod]["fc_task_issue_" + task_issue] = num + "/" + num;
                for (var str_j in childJStore[f_cod])
                    JStore[childJStore[f_cod][str_j]]["fc_task_issue_" + task_issue] = 1;
            } else { // child
                //childJStore[f_cod].fc_task_issue = task_issue.replace("_", " ");
                for (var s_i in issues)
                    childJStore[f_cod]["fc_task_issue_" + issues[s_i]] = 0;
                childJStore[f_cod]["fc_task_issue_" + task_issue] = 1;
            }
        }
        refresh();
    }
};
moLibrary.setTaskQuality = function (t) {
    var issues = ['good', 'acceptable', 'not_acceptable'], task_issue = issues[t];
    with (mdl_wo_task$pm_tg_tg) {
        for (var s_chk in arrCHK) {
            var f_cod = arrCHK[s_chk];
            if (moGlb.isset(JStore[f_cod])) { // parent
                var num = arrpd[f_cod].length;
                for (var s_i in issues)
                    JStore[f_cod]["fc_task_quality_" + issues[s_i]] = "0/" + num;
                JStore[f_cod]["fc_task_quality_" + task_issue] = num + "/" + num;
                for (var str_j in childJStore[f_cod])
                    JStore[childJStore[f_cod][str_j]]["fc_task_quality_" + task_issue] = 1;
            } else { // child
                if (childJStore[f_cod].fc_task_issue_performed != 1)
                    moGlb.alert.show(moGlb.langTranslate("You can set quality for performed tasks only"));
                else {
                    //childJStore[f_cod].fc_task_issue = task_issue.replace("_", " ");
                    for (var s_i in issues)
                        childJStore[f_cod]["fc_task_quality_" + issues[s_i]] = 0;
                    childJStore[f_cod]["fc_task_quality_" + task_issue] = 1;
                }
            }
        }
        refresh();
    }
};
moLibrary.set_task_result_onload = function (o) {
    with (o) {

        var mod = moLibrary.getModule(name), stype = o.name.split('_'), type = stype[stype.length - 1],
                issues = {issue: ['not_performed', 'performed', 'not_executable'], quality: ['good', 'acceptable', 'not_acceptable']};
        for (var i in issues[type])
            if (parseInt(eval('mdl_' + mod + '_edit').data['fc_task_' + type + '_' + issues[type][i]]) == 1)
                set(issues[type][i].replace('_', ' '));
        if (type == 'quality') {
            setProperties({v_on: (eval('slct_' + mod + '_edit_issue').get() == 'performed')});
            // if(eval('slct_'+mod+'_edit_issue').get() == 'performed') eval('slct_'+mod+'_edit_quality').set('good');
        }
        moLibrary.set_task_result_onchange(o);
    }
};
moLibrary.slct_wo_task_edit_issue_onload = moLibrary.set_task_result_onload;
moLibrary.slct_wo_task_edit_quality_onload = moLibrary.set_task_result_onload;
moLibrary.slct_wo_task$pm_edit_issue_onload = moLibrary.set_task_result_onload;
moLibrary.slct_wo_task$pm_edit_quality_onload = moLibrary.set_task_result_onload;
moLibrary.set_task_result_onchange = function (o) {
    with (o) {
        var v = get(), mod = moLibrary.getModule(name), stype = o.name.split('_'), type = stype[stype.length - 1],
                fields = {issue: ['not_performed', 'performed', 'not_executable'], quality: ['good', 'acceptable', 'not_acceptable']};
//                ms4trad = {issue:{'not_performed':0, 'performed':2, 'not_executable':1}, quality: {'good':0, 'acceptable':1, 'not_acceptable':2}};

        for (var i in fields[type]) {
            eval('mdl_' + mod + '_edit').data['fc_task_' + type + '_' + fields[type][i]] = 0;
        }
        eval('mdl_' + mod + '_edit').setEditData('fc_task_' + type + '_' + v.replace(' ', '_'), 1);
        if (type == 'issue') {
            eval('slct_' + mod + '_edit_quality').setProperties({v_on: (get() == 'performed')});
            if (get() != 'performed') {
                eval('mdl_' + mod + '_edit').data['fc_task_quality_good'] = 0;
                eval('mdl_' + mod + '_edit').data['fc_task_quality_acceptable'] = 0;
                eval('mdl_' + mod + '_edit').data['fc_task_quality_not_acceptable'] = 0;
            }
        }

    }
};
moLibrary.slct_wo_task_edit_issue_onchange = moLibrary.set_task_result_onchange;
moLibrary.slct_wo_task_edit_quality_onchange = moLibrary.set_task_result_onchange;
moLibrary.slct_wo_task$pm_edit_issue_onchange = moLibrary.set_task_result_onchange;
moLibrary.slct_wo_task$pm_edit_quality_onchange = moLibrary.set_task_result_onchange;
moLibrary.wo_task_since_till = function (o) {
    /*with(o) {
     var mod = moLibrary.getModule(o.name), dates = moLibrary.check_start_end_date(o, 'since', 'till'), hrs = 0;
     if(dates) hrs = Math.round((dates.end-dates.start)/(60*60));
     eval('txtfld_'+mod+'_edit_hours').set(hrs.toFixed(2));
     }*/
    with (o) {
        var mod = moLibrary.getModule(o.name), dates = moLibrary.check_start_end_date(o, 'since', 'till'), hrs = 0;
        if (dates)
            hrs = Math.round((dates.end - dates.start) / (60 * 60));
        if (hrs >= 0)
            eval('txtfld_' + mod + '_edit_hours').set(hrs.toFixed(2));

        eval('txtfld_' + mod + '_edit_since').setProperties({m_on: eval('txtfld_' + mod + '_edit_till').get('int') > 0 && eval('txtfld_' + mod + '_edit_since').get('int') == 0});
    }
};
moLibrary.txtfld_wo_task_edit_since_onchange = moLibrary.wo_task_since_till;
moLibrary.txtfld_wo_task_edit_till_onchange = moLibrary.wo_task_since_till;
moLibrary.txtfld_wo_task$pm_edit_since_onchange = moLibrary.wo_task_since_till;
moLibrary.txtfld_wo_task$pm_edit_till_onchange = moLibrary.wo_task_since_till;
moLibrary.wo_task_since_till_onload = function (o) {
    var mod = moLibrary.getModule(o.name);
    eval('txtfld_' + mod + '_edit_since').setProperties({m_on: o.get('int') > 0 && eval('txtfld_' + mod + '_edit_since').get('int') == 0});
};
moLibrary.txtfld_wo_task_edit_till_onload = moLibrary.wo_task_since_till_onload;
moLibrary.txtfld_wo_task$pm_edit_till_onload = moLibrary.wo_task_since_till_onload;
//moLibrary.txtfld_wo_task_edit_till_onload = function (o){txtfld_wo_task_edit_since.setProperties({m_on: txtfld_wo_task_edit_till.get('int') > 0 && txtfld_wo_task_edit_since.get('int') == 0});};
/**/


moLibrary.periodic_type = function () {
    v = parseInt(v);
    if (v == -1)
        v = "none";
    else if (v == 0)
        v = "weekly";
    else if (v == 1)
        v = "monthly";
    else if (v == 2)
        v = "periodic";
    else if (v == 3)
        v = "day of the month";
    else if (v == 4)
        v = "one time schedule";
    fillText(moGlb.langTranslate(v), xfield, yfield + 21);
};
moLibrary.cyclic_type = function () {
    v = parseInt(v);
    if (v == 0)
        v = "none";
    if (v == 1)
        v = "day";
    else if (v == 2)
        v = "week";
    else if (v == 3)
        v = "month";
    else if (v == 4)
        v = "year";
    fillText(moGlb.langTranslate(v), xfield, yfield + 21);
};
moLibrary.eng_sequence = function () {
    var dd = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'], mnths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    switch (parseInt(childJStore[f_cod]['fc_pm_periodic_type'])) {
        case 0:
            var vs = v.split(','), vst = [];
            for (var scr_i in vs)
                vst.push(moGlb.langTranslate(dd[vs[scr_i]]));
            v = vst.join(',');
            break;
        case 1:
            var vs = v.split(','), vst = [];
            for (var scr_i in vs)
                vst.push(moGlb.langTranslate(mnths[vs[scr_i]]));
            v = vst.join(',');
            break;
        case 4:
            v = new Date(v * 1000).format("{D} {d}/{m}/{Y}");
            break;
    }
    fillText(v, X1 + d, y + parseInt((Yi - 11) / 2) + 11);
};
/**
 * PM
 */
moLibrary.pm_periodic_type = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), edit = eval('mdl_' + mod + '_edit');

        if (edit.data.f_code > -1 && (value == -1 || value == 2))
            edit.data.fc_pm_sequence = '';
        if (value == 4)
            eval('txtfld_' + mod + '_edit_sequence_one_time').set('');
        eval('chkbx_' + mod + '_edit_sequence_weekly').setProperties({v_on: (value == 0), m_on: (value == 0)});
        eval('chkbx_' + mod + '_edit_sequence_monthly').setProperties({v_on: (value == 1), m_on: (value == 1)});
        eval('chkbx_' + mod + '_edit_sequence_day_of_month').setProperties({v_on: (value == 3), m_on: (value == 3)});
        eval('txtfld_' + mod + '_edit_sequence_one_time').setProperties({v_on: (value == 4), m_on: (value == 4)});
        eval('slct_' + mod + '_edit_cyclic_weekly').setProperties({v_on: (value == 0)});
        eval('slct_' + mod + '_edit_cyclic_monthly').setProperties({v_on: (value == 1)});
        eval('slct_' + mod + '_edit_cyclic_periodic').setProperties({v_on: (value == 2)});
        eval('slct_' + mod + '_edit_cyclic_day_of_the_month').setProperties({v_on: (value == 3)});
        eval('slct_' + mod + '_edit_cyclic_one_time_schedule').setProperties({v_on: (value == 4)});
        eval('txtfld_' + mod + '_edit_number').setProperties({v_on: !(value == -1), m_on: !(value == -1)});

        if (edit.data.fc_pm_periodic_type != value) {
            eval('chkbx_' + mod + '_edit_sequence_day_of_month').set('');
            eval('chkbx_' + mod + '_edit_sequence_weekly').set('');
            eval('chkbx_' + mod + '_edit_sequence_monthly').set('');
        } else {
            eval('chkbx_' + mod + '_edit_sequence_day_of_month').set(edit.data.fc_pm_sequence);
            eval('chkbx_' + mod + '_edit_sequence_weekly').set(edit.data.fc_pm_sequence);
            eval('chkbx_' + mod + '_edit_sequence_monthly').set(edit.data.fc_pm_sequence);
        }
    }
};

moLibrary.slct_pm_edit_cyclic_one_time_schedule_onload = function (o) {
    with (o) {
        txtfld_pm_edit_number.setProperties({m_on: (slct_pm_edit_cyclic_one_time_schedule.get('int') == 4)});
    }
}
moLibrary.slct_pm_edit_cyclic_one_time_schedule_onchange = function (o) {
    moLibrary.slct_pm_edit_cyclic_one_time_schedule_onload(o);
}

moLibrary.slct_pm_edit_periodic_type_onload = moLibrary.pm_periodic_type;
moLibrary.slct_pm_edit_periodic_type_onchange = moLibrary.pm_periodic_type;
moLibrary.slct_meter_edit_periodic_type_onload = moLibrary.pm_periodic_type;
moLibrary.slct_meter_edit_periodic_type_onchange = moLibrary.pm_periodic_type;
moLibrary.slct_inspc_edit_periodic_type_onload = moLibrary.pm_periodic_type;
moLibrary.slct_inspc_edit_periodic_type_onchange = moLibrary.pm_periodic_type;
//moLibrary.pm_rules_type_dayweek = function (o) {with(o) {var v = parseInt(value);eval('chkbx_'+moLibrary.getModule(o.name)+'_edit_rules_dayweek').setProperties({v_on:(v==1||v==2)});}};
//moLibrary.pm_rules_type_exception_group = function (o) {with(o) {var v = parseInt(value);eval('chkbx_'+moLibrary.getModule(o.name)+'_edit_rules_dayweek_exception_group').setProperties({v_on:(v==1||v==2)});}};
moLibrary.pm_cyclic_number = function (o) {
    with (o) {
        var v = parseInt(value);
        if (isNaN(v) || v <= 0)
            moGlb.alert.show(moGlb.langTranslate('You can set only a number greater than 0 for this field'));
    }
};
moLibrary.txtfld_pm_edit_number_onchange = moLibrary.pm_cyclic_number;
moLibrary.txtfld_meter_edit_number_onchange = moLibrary.pm_cyclic_number;
moLibrary.txtfld_inspc_edit_number_onchange = moLibrary.pm_cyclic_number;
/**
 * Import/export
 **/
moLibrary.txtfld_inout_edit_title_onload = function (o) {
    mdl_inout_edit.data.fc_inout_type = parseInt(mdl_inout_edit.data.t_selectors);
};
/**
 * Users and Profile
 **/
moLibrary.txtfld_up_edit_fc_usr_phone_onload = function (o) {
    o.set(moGlb.user.phone);
};
moLibrary.slct_up_edit_fc_usr_language_onload = function (o) {
    console.log(moGlb.user.language);
    o.set(moGlb.user.language);
};
moLibrary.slct_up_edit_fc_usr_gender_onload = function (o) {
    o.set((moGlb.user.gender == 'M') ? 0 : 1);
};
moLibrary.txtfld_up_edit_fc_usr_firstname_onload = function (o) {
    o.set(moGlb.user.firstname);
    mdl_up_edit.setSubtab();
};
moLibrary.txtfld_up_edit_fc_usr_lastname_onload = function (o) {
    o.set(moGlb.user.lastname);
};
moLibrary.txtfld_up_edit_fc_usr_mail_onload = function (o) {
    o.set(moGlb.user.email);
};
moLibrary.txtfld_up_edit_f_title_onload = function (o) {
    o.set(moGlb.user.fullname);
};
moLibrary.img_up_edit_avatar_onload = function (o) {
    o.set(moGlb.user.avatar);
};
moLibrary.txtfld_up_edit_fc_usr_usn_onload = function (o) {
    o.set(moGlb.user.username);
};
moLibrary.txtfld_up_edit_fc_usr_address_onload = function (o) {
    o.set(moGlb.user.address);
};
moLibrary.txtfld_up_edit_fc_usr_pwd_expiration_onload = function (o) {
    o.set(moGlb.user.pwdExpiration);
};
moLibrary.txtfld_up_edit_fc_usr_account_expiration_onload = function (o) {
    o.set(moGlb.user.accountExpiration);
};
moLibrary.txtfld_up_edit_fc_usr_creation_onload = function (o) {
    o.set(moGlb.user.accountCreation);
};
//moLibrary.txtfld_usr_edit_fc_usr_usn_registration_onload = function (o) { o.set(moGlb.user.accountCreation); };
//moLibrary.txtfld_usr_edit_fc_usr_pwd_registration_onload = function (o) { o.set(moGlb.user.accountExpiration); };
moLibrary.txtfld_up_edit_level_onload = function (o) {
    o.set(moGlb.user.level_text);
};
moLibrary.chkbx_up_edit_mobile_autoswitch_onload = function (o) {
    o.set(moGlb.user.fc_usr_mobile_autoswitch_desktop);
};
moLibrary.changePassword = function (o) {
    var mod = moLibrary.getModule(o.name), pwd = eval('txtfld_' + mod + '_edit_fc_usr_pwd'), r_pwd = eval('txtfld_' + mod + '_edit_fc_usr_repeat_pwd'),
            v_pwd = pwd.get(), v_r_pwd = r_pwd.get(), err = (v_pwd && v_pwd != v_r_pwd);
    r_pwd.setProperties({m_on: (v_pwd != '')});
    pwd.setError(err);
    r_pwd.setError(err);
};
moLibrary.txtfld_up_edit_fc_usr_pwd_onchange = function (o) {
    moLibrary.controlTypePassword(o.get(), o);
    moLibrary.changePassword(o);
}
moLibrary.txtfld_usr_edit_fc_usr_pwd_onchange = function (o) {
    moLibrary.controlTypePassword(o.get(), o);
    moLibrary.changePassword(o);
}
moLibrary.txtfld_up_edit_fc_usr_repeat_pwd_onchange = moLibrary.changePassword;


moLibrary.controlTypePassword = function (word, o) {
    //TYPE_PASSWORD is a string with syntax lunghezza(>4),maiuscola(0,1),obbl. num(0,1), obbl. car. spec(0,1)
    var type = moGlb.TYPE_PASSWORD;
    if (moGlb.isset(type) && type != '') {
        var arr_type = type.split(",");
        var no = 0;
        if (arr_type.length == 4) {
            if (word.length < parseInt(arr_type[0])) {
                moGlb.alert.show(moGlb.langTranslate("Password must be more than ") + arr_type[0] + moGlb.langTranslate(" characters"));
                o.set('');
                no = 1;
            }
            if (parseInt(arr_type[1]) == 1 && no == 0) {
                var espressione = new RegExp('[A-Z]', 'g');
                if (!espressione.test(word)) {
                    moGlb.alert.show(moGlb.langTranslate("Password it takes a capital letter"));
                    o.set('');
                    no = 1;
                }
            }
            if (parseInt(arr_type[2]) == 1 && no == 0) {
                var espressione = new RegExp('[0-9]');
                if (!espressione.test(word)) {
                    moGlb.alert.show(moGlb.langTranslate("Password it takes a number"));
                    o.set('');
                    no = 1;
                }
            }
            if (parseInt(arr_type[3]) == 1 && no == 0) {
                if (!moLibrary.caratteriSpeciali(word)) {
                    moGlb.alert.show(moGlb.langTranslate("Password you need to enter one of a special characters"));
                    o.set('');
                }
            }
        }
    }
}

moLibrary.caratteriSpeciali = function (word) {
    var iChars = "!@#£$%^&*()+=-[]\\\';,./{}|\":<>?_";

    for (var i = 0; i < word.length; i++) {
        if (iChars.indexOf(word.charAt(i)) != -1) {
            return true;
        }
    }
    return false;
}

moLibrary.txtfld_usr_edit_fc_usr_repeat_pwd_onchange = function (o) {
    moLibrary.changePassword(o);
    var to = new Date();
    if (o.get() != '')
        txtfld_usr_edit_fc_usr_pwd_registration.set(to.getTime() / 1000);
    else
        txtfld_usr_edit_fc_usr_pwd_registration.set(mdl_usr_edit.data.fc_usr_pwd_registration);
}
moLibrary.txtfld_usr_edit_fc_usr_repeat_pwd_onload = function (o) {
    var mod = moLibrary.getModule(o.name);

    eval('txtfld_' + mod + '_edit_fc_usr_repeat_pwd').setProperties({m_on: (eval('txtfld_' + mod + '_edit_fc_usr_pwd').get() != '')});
}



moLibrary.mdl_usr_lvl_tg_tg_script = function () {
    if (moGlb.isset(JStore)) {
        var lvl = 0, tlvl = [];
        for (scr_k in JStore) {
            lvl += parseInt((JStore[scr_k].fc_lvl_level));
            tlvl.push(JStore[scr_k].f_title);
        }
        mdl_usr_edit.data.fc_usr_level = (lvl ? lvl : -1);
        txtfld_usr_edit_level.set(tlvl.join(', '));
    }
};
/*
 moLibrary.fc_up_fullname_onload = function (o) {with(o) {set(moGlb.user.fullname);}};
 moLibrary.fc_up_firstname_onload = function (o) {with(o) {set(moGlb.user.firstname);}};
 moLibrary.fc_up_lastname_onload = function (o) {with(o) {set(moGlb.user.lastname);}};
 moLibrary.fc_up_address_onload = function (o) {with(o) {set(moGlb.user.address);}};
 moLibrary.fc_up_phone_onload = function (o) {with(o) {set(moGlb.user.phone);}};
 moLibrary.fc_up_mail_onload = function (o) {with(o) {set(moGlb.user.email);}};
 moLibrary.fc_up_username_onload = function (o) {with(o) {set(moGlb.user.username);}};
 moLibrary.fc_up_language_onload = function (o) {with(o) {set(moGlb.user.language);}};
 moLibrary.fc_up_gender_onload = function (o) {with(o) {set(moGlb.user.gender);}};
 moLibrary.fc_up_avatar_onload = function (o) {with(o) {set(moGlb.user.avatar);}};
 moLibrary.fc_up_creation_onload = function (o) {with(o) {set(moGlb.user.accountCreation);}};
 moLibrary.fc_up_account_expiration_onload = function (o) {with(o) {set(moGlb.user.accountExpiration);}};
 moLibrary.fc_up_pwd_expiration_onload = function (o) {with(o) {set(moGlb.user.pwdExpiration);}};
 moLibrary.txtfld_up_edit_level_onload = function (o) {o.set(moGlb.user.level_text);};
 moLibrary.fc_change_password = function (o) {with(o) {if(txtfld_usr_edit_fc_usr_pwd.get() != '') eval("txtfld_"+moLibrary.getModule(o.name)+"_edit_fc_usr_repeat_pwd").setProperties({m_on: !moGlb.isempty(value)});moLibrary.fc_repeat_pwd_check(o);}};
 moLibrary.fc_repeat_pwd_check = function (o) { // TO VERIFY
 with(o) {
 var mod = moLibrary.getModule(o.name), pwd = eval("txtfld_"+mod+"_edit_fc_usr_pwd"), r_pwd = eval("txtfld_"+mod+"_edit_fc_usr_repeat_pwd"),
 equal = (!moGlb.isempty(pwd.get()) && (pwd.get() != r_pwd.get()));
 with(pwd) {setProperties({error: equal});setStyle();}
 with(r_pwd) {setProperties({error: equal});setStyle();}
 }
 };    */
//fc_usr_group_level_onload = function (o) {with(o) {if(moGlb.isempty(value)) set(2);}};
moLibrary.mdl_usr_edit_save = function (data) {

    if (slct_usr_edit_fc_usr_language.get() == 1)
        mdl_usr_edit.setEditData('fc_usr_language_str', 'English');
    if (slct_usr_edit_fc_usr_language.get() == 2)
        mdl_usr_edit.setEditData('fc_usr_language_str', 'Italian');
    if (slct_usr_edit_fc_usr_language.get() == 3)
        mdl_usr_edit.setEditData('fc_usr_language_str', 'French');
    if (slct_usr_edit_fc_usr_language.get() == 4)
        mdl_usr_edit.setEditData('fc_usr_language_str', 'Spanish');
    //if(txtfld_usr_edit_fc_usr_usn_registration.get() == '')txtfld_usr_edit_fc_usr_usn_registration.set(new Date());
    //if(txtfld_usr_edit_fc_usr_pwd.get() != '')txtfld_usr_edit_fc_usr_pwd_registration.set(new Date());

    return true;
};

/**
 * Bookmarks
 */
moLibrary.mdl_bkm_edit_save = function () {
    with (mdl_bkm_eng_tg_tg) {
        var binds = [], fields = [];
        for (var i in childJStore) {
            binds.push(childJStore[i].fc_bkm_bind);
            fields.push(childJStore[i].fc_bkm_label);
        }
        mdl_bkm_edit.data.fc_bkm_fields_bind = binds.join(', ');
        mdl_bkm_edit.data.fc_bkm_fields = fields.join(', ');
    }
    return true;
};

/**
 * Data Manager
 */
moLibrary.mdl_dmn_edit_save = function (data) {
    mdl_dmn_edit.data.fc_dmn_process_type = mdl_dmn_edit.data.t_selectors;
    return true;
};
moLibrary.cron_starting_date_visibility = function () {
    txtfld_dmn_edit_cron_starting_date.setProperties({v_on: chkbx_dmn_edit_import_type.get() != 'instant'});
};
moLibrary.chkbx_dmn_edit_import_type_onchange = function (o) {
    moLibrary.cron_starting_date_visibility();
};
moLibrary.chkbx_dmn_edit_import_type_onload = function (o) {
    moLibrary.cron_starting_date_visibility();
};
moLibrary.txtfld_dmn_edit_exported_file_onload = function (o) {
    with (o) {
        var v = o.get();
        if (v)
            setProperties({description: v.split("|")[1]});
    }
};
moLibrary.createInstantImport = function (f_code, sheetname) {
    if (!mf$('importInstant')) {
        with (moGlb.createElement('importInstant', 'div', null, true).style) {
            padding = '4px';
            width = '500px';
            height = '300px';
            overflow = 'auto';
        }
        moComp.createComponent("importWindow_win", "moPopup", {
            width: 500, height: 300,
            contentid: "importInstant", title: moGlb.langTranslate("Import log")
        }, true);
    }
    //mf$('importInstant').innerHTML = moGlb.langTranslate('<b>Import log:</b><br/>');
    with (importWindow_win) {
        enableClose(false);
        show();
    }
    moLibrary.startInstantImport({response: 'continue', f_code: f_code, pos: 1, isFinish: 0, message: 'loading ' + sheetname});
};
moLibrary.startInstantImport = function (o) {
    if (typeof o != 'object') {
        try {
            o = JSON.parse(o);
        } catch (ex) {
            o = {message: o, response: 'ko'};
        }
    } else {
        mf$('importInstant').innerHTML = '<div class="data-loader">' + moGlb.langTranslate('Initializing loading') + '</div>';
    }
    if (!moGlb.isset(o.response))
        o = {message: o, response: 'ko'};
    if (o.response == 'ko') {
        with (mf$('importInstant').childNodes[mf$('importInstant').childNodes.length - 1]) {
            className = 'data-error';
            innerHTML = moCnvUtils.IconFont(58807, 16, '#ff0000') + ' ' + innerHTML;
        }
        mf$('importInstant').innerHTML += '<div class="data-error">' + moCnvUtils.IconFont(58807, 16, '#ff0000') + ' ' + moGlb.langTranslate(o.message) + '</div>';
    } else {
        with (mf$('importInstant').childNodes[mf$('importInstant').childNodes.length - 1]) {
            className = '';
            innerHTML = moCnvUtils.IconFont(58806, 16, '#496670') + ' ' + innerHTML;
        }
        if (o.response == 'ok')
            mf$('importInstant').innerHTML += '<div>' + moCnvUtils.IconFont(58806, 16, '#496670') + ' ' + moGlb.langTranslate('Data are successfully imported!') + '</div>';
        else
            mf$('importInstant').innerHTML += '<div class="data-loader">' + moGlb.langTranslate(o.message) + '</div>';
    }
    if (o.response == 'continue')
        Ajax.sendAjaxPost(moGlb.baseUrl + '/admin/datamanager/instant-data-loader', "f_code=" + o.f_code + "&pos=" + o.pos + "&isFinish=" + o.isFinish, "moLibrary.startInstantImport");
    else
        importWindow_win.enableClose(true);
};
moLibrary.exportCallback = function (o, mde, jc) {
    console.log(jc.data);
    if (!mde)
        mde = "";
    else
        mde += "&f_code=" + jc.data.f_code;
    if (!o) {
        o = "<!DOCTYPE html><html><head><meta charset='UTF-8'><meta name='viewport' content='width=device-width'><link rel='stylesheet' href='/mainsim3/export/template.css' /><script src='/mainsim3/export/jquery-1.10.2.min.js'></script></head><body><div id='templateContainer' class='container'><div class='title'>Exporter Tool</div><div class='mainInfo'><div class='mainInfoText'><b>output file</b> <span id='outputFile'>report.xlsx</span><br><b>config file</b> <span id='configFile'>exportTemplate.xml</span><br><b>total sheets</b> <span id='totalSheets'></span><br><b>time remaining</b> <span id='timeRemaining'></span><br></div><div id='mainInfoCanvas' class='mainInfoCanvas'><canvas id='canvas' width='100' height='100'></canvas></div></div><div class='sheetsInfo'><div id='sheetInfoText' class='sheetsInfoText'></div><div id='sheetInfoProgressBar' class='sheetInfoProgressBar'></div></div></div><div id='iframe'></div></body></html>";
    }

    var win = '';
    var container = mf$('exportFeedback');
    try {
        win = eval('exportFeedback_dm_win');
    } catch (ex) {
        if (!container) {
            var container = moGlb.createElement('exportFeedback', 'div', null, true);
            with (container) {
                style.width = '400px';
                style.height = '250px';
                style.overflowY = 'auto';
                style.overflowX = 'hidden';
            }
        }
        win = moComp.createComponent("exportFeedback_dm_win", "moPopup", {
            contentid: 'exportFeedback', title: moGlb.langTranslate('Export progress'),
            width: 400, height: 250
        }, true);
    }
    container.innerHTML = o;
    win.show();

    var url = moGlb.baseUrl;
    var filename = '';
    sheets = '';
    var progressTimer = null;

    var initCanvas = function () {
        var context = document.getElementById('canvas').getContext('2d');
        var centerX = context.canvas.width / 2;
        var centerY = context.canvas.height / 2;
        context.strokeStyle = '#000000';
        context.lineWidth = 1;
        context.beginPath();
        context.arc(centerX, centerY, 40, 0, Math.PI * 2, false);
        context.stroke();
        context.beginPath();
        context.arc(centerX, centerY, 20, 0, Math.PI * 2, false);
        context.stroke();
    };

    var updateCanvas = function (percent) {
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');

        // clear canvas
        context.clearRect(0, 0, canvas.width, canvas.height);

        // re init canvas
        initCanvas();

        // update arc
        endAngle = Math.PI * 2 * percent / 100 + 3 / 2 * Math.PI;
        startAngle = 3 / 2 * Math.PI;
        context.beginPath();
        context.strokeStyle = '#ff0000';
        context.lineWidth = 20;
        var centerX = context.canvas.width / 2;
        var centerY = context.canvas.height / 2;
        context.arc(centerX, centerY, 30, startAngle, endAngle, false);
        context.stroke();
        // write percent text
        context.font = "10px Verdana";
        if (percent == 100) {
            context.fillText(percent + "%", centerX - 16, centerY + 4);
        } else {
            context.fillText(percent + "%", centerX - 9, centerY + 4);
        }
    };

    var createProgressBars = function () {

        $.ajax({
            url: url + "/export/get-sheets-template"
        }).done(function (response) {
            sheets = jQuery.parseJSON(response);
            // create divs for sheets progress
            for (i = 0; i < sheets.length; i++) {

                // create sheet bar
                if (sheets[i].length > 25) {
                    sheetName = sheets[i].substr(0, 25) + '...';
                } else {
                    sheetName = sheets[i];
                }
                $('#sheetInfoText').append('<div id="txt_' + (i + 1) + '" class="sheetInfoTextDiv">' + sheetName + '</div>');
                $('#sheetInfoProgressBar').append('<div id="bar_' + (i + 1) + '" class="sheetInfoProgressBarDiv"></div>');
                // start sheet creation
                createSheet(sheets[i], i + 1);
            }
            // hide loader
            //$('#loader').hide();
            $('#totalSheets').html(sheets.length);

            // create timer for progress ajax calls
            progressTimer = setInterval(function () {
                progressFn(sheets)
            }, 3000);
        });
    };

    var createSheet = function (sheet, id) {
        $.ajax({
            url: url + "/export/create-sheet-template?filename=" + filename + "&sheet=" + sheet + "&sheetId=" + id
        }).done(function (response) {

        });
    }

    var progressFn = function progressFn(sheets) {
        // get progress
        $.ajax({
            url: url + "/export/progress-template?filename=" + filename,
            type: 'POST',
            data: 'sheets=' + JSON.stringify(sheets)
        }).done(function (response) {
            try {
                var obj = jQuery.parseJSON(response);
                var percentSum = 0;
                var endCounter = 0;
                var secondsLeft = 0;
                var calculateTime = true;

                for (key in obj) {


                    // sheet creation finished --> update progress bar to 100%
                    if (obj[key]['state'] == 'not started') {
                        var html = '<div style="position:relative;width:0%;background-color:' + backgroundColor + '">&nbsp;</div>' +
                                '<div style="position:relative;z-index:10;text-align:center;margin-top:-12px;">0%</div>';
                        calculateTime = false;
                        $('#bar_' + key).html(html);
                    } else if (obj[key]['state'] == 'end') {
                        backgroundColor = '#A1E3AA';
                        endCounter++;
                        var html = '<div style="position:relative;width:100%;background-color:' + backgroundColor + '">&nbsp;</div>' +
                                '<div style="position:relative;z-index:10;text-align:center;margin-top:-12px;">100%</div>';
                        $('#bar_' + key).html(html);
                        percentSum += 100;
                    }
                    // update progress bar
                    else if (obj[key]['counter'] > 0) {
                        percent = parseInt(obj[key]['counter'] / obj[key]['total'] * 100);
                        if (percent < 25) {
                            backgroundColor = '#D95050';
                        } else if (percent < 50) {
                            backgroundColor = '#F58251';
                        } else if (percent < 75) {
                            backgroundColor = '#F0EE7F';
                        } else {
                            backgroundColor = '#A1E3AA';
                        }

                        var html = '<div style="position:relative;width:' + percent + '%;background-color:' + backgroundColor + '">&nbsp;</div>' +
                                '<div style="position:relative;z-index:10;text-align:center;margin-top:-12px;">' + percent + '%</div>';

                        $('#bar_' + key).html(html);
                        percentSum += percent;
                        if (obj[key]['time'] != 'undefined') {
                            secondsLeft += (obj[key]['total'] - obj[key]['counter']) * obj[key]['time'] / obj[key]['counter'];
                        } else {
                            calculateTime = false;
                        }
                    }
                }


                // all sheets are created
                if (endCounter == sheets.length) {
                    // stop progress calls
                    clearInterval(progressTimer);

                    $('#timeRemaining').html('done');

                    updateCanvas(100);

                    // get xlsx
                    $.ajax({
                        url: url + "/export/finalize-excel-template?filename=" + filename,
                        type: 'POST',
                        data: 'sheets=' + JSON.stringify(sheets)
                    }).done(function (response) {
                        // get file from server
                        if (document.getElementById('iframe').innerHTML == '') {
                            document.getElementById('iframe').innerHTML = '<iframe onload="" style="display:none" src="' + url + '/export/download-excel-template?filename=' + response + '"></iframe>';
                        }
                    });
                } else {
                    updateCanvas(parseInt(percentSum / sheets.length));

                    if (calculateTime) {
                        if (secondsLeft > 0) {
                            if (secondsLeft / 60 > 1) {
                                var aux = Math.round(parseInt(secondsLeft) / 60);
                                $('#timeRemaining').html(aux + ' minutes left');
                            } else {
                                var aux = parseInt(secondsLeft);
                                $('#timeRemaining').html(aux + ' seconds left');
                            }
                        } else {
                            timeLeft = '';
                        }
                    } else {
                        $('#timeRemaining').html('not available');
                    }
                }
            } catch (e) {
                moDebug.log(e.message);
            }
        });
    }

    initCanvas();

    // start export call
    $.ajax({
        url: url + "/export/init-template" + mde
    }).done(function (response) {
        filename = response;
        createProgressBars();
    });
};
moLibrary.exportHistory = function (params) {
    var tempDate = '', inputDate = prompt(moGlb.langTranslate('Write here the date for the report. Accepted format is: \n gg-mm-yyyy-hh-ii'), '31-12-2013-12-30');
    if (!inputDate) {
        moGlb.alert.show(moGlb.langTranslate("Please set a date"));
        return exportData(expFormat, detailed, hy, costs);
    } else {
        try {
            var auxDate = inputDate.split('-'), dt = new Date(auxDate[2], auxDate[1] - 1, auxDate[0], auxDate[3], auxDate[4], 0, 0);
            tempDate = dt.format('{U}');
        } catch (ex) {
            moGlb.alert.show(moGlb.langTranslate("Date error"));
            return moLibrary.exportHistory(params);
        }
    }
    params.history = true;
    params.historyDate = tempDate;
    return params;
};


/*
 Calcolo data giorni lavorativi
 
 forma:
 OTTENGO IL TIMESTAMP DEL CAMPO DATA: 		var opening_date = eval('txtfld_'+mod+'_edit_opening_date').get('int');
 TRASFORMO IL VALORE TIMESTAMP IN UNA DATA: 	var date = new Date(opening_date*1000);
 RICHIAMO LA FUNZIONE scan_date():               var di = moLibrary.scan_date(date,10);
 SALVO IL VALORE NEL CAMPO:			eval('txtfld_'+mod+'_edit_fine_contratto').set(di.getTime()/ 1000);
 */
moLibrary.scan_date = function (start_date, day) {
    for (var a = 0; a < day; a++) {
        start_date = new Date(start_date.getTime() + (1000 * 3600));
        //start_date = moLibrary.holiday_control(start_date);
    }
    //start_date = moLibrary.holiday_control(start_date);
    return start_date;
}

//Funzione richiamata da scan_date()
moLibrary.holiday_control = function (end_date) {
    //struct for holiday
    var holiday = {
        Jan: ' 01 , 06 ',
        Feb: '',
        Mar: '',
        Apr: ' 25 ',
        May: ' 01 ',
        Jun: ' 02 ',
        Jul: '',
        Aug: ' 15 ',
        Sep: '',
        Oct: '',
        Nov: ' 08 ',
        Dec: ' 25 , 26 '
    };

    var year = new Date() /*(new Date()).getYear()*/;

    var pasqua = moGlb.easters[year.getFullYear()].split('-');

    var str_dat = end_date + '';
    var arr_d = str_dat.split(' ');
    var day_name = arr_d[0];
    var day = arr_d[2];
    var month = arr_d[1];
    var is = holiday[month];

    var day_num = parseInt(day);
    var pasqua_num = parseInt(pasqua[1]);
    var pasqua_num_add = pasqua_num + 1;

    switch (month) {
        case 'Feb':
            if (pasqua[0] == '1' && day_num == pasqua_num_add) /*return new Date(end_date.getTime()+1000*3600*24);*/
                end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
            if (pasqua[0] == '1' && day_num == pasqua_num) /*return new Date(end_date.getTime()+1000*3600*48);*/
                end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
        case 'Mar':
            if (pasqua[0] == '2' && day_num == pasqua_num_add) /*return new Date(end_date.getTime()+1000*3600*24);*/
                end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
            if (pasqua[0] == '2' && day_num == pasqua_num) /*return new Date(end_date.getTime()+1000*3600*48);*/
                end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
        case 'Apr':
            if (pasqua[0] == '3' && day_num == pasqua_num_add) {
                /*return new Date(end_date.getTime()+1000*3600*24);*/end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
            }
            if (pasqua[0] == '3' && day_num == pasqua_num) {
                /*return new Date(end_date.getTime()+1000*3600*48);*/end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
            }
        case 'May':
            if (pasqua[0] == '4' && day_num == pasqua_num_add) /*return new Date(end_date.getTime()+1000*3600*24);*/
                end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
            if (pasqua[0] == '4' && day_num == pasqua_num) /*return new Date(end_date.getTime()+1000*3600*48);*/
                end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
        default:
            break;
    }

    var str_dat = end_date + '';
    var arr_d = str_dat.split(' ');
    var day_name = arr_d[0];
    var day = arr_d[2];
    var month = arr_d[1];
    var is = holiday[month];

    if (day_name == 'Sat') {
        /*return new Date(end_date.getTime()+1000*3600*48);*/end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
    }
    if (day_name == 'Sun') {
        var str_dat = end_date + '';
        var arr_d = str_dat.split(' ');
        var day_name = arr_d[0];
        var day = arr_d[2];
        var month = arr_d[1];
        var is = holiday[month];
        /*return new Date(end_date.getTime()+1000*3600*24);*/end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
    }
    if (is.indexOf(' ' + day + ' ') != -1) {
        /*return new Date(end_date.getTime()+1000*3600*24);*/end_date = moLibrary.holiday_control(new Date(end_date.getTime() + 1000 * 3600 * 24));
    }

    return end_date;
};
moLibrary.slct_asset_edit_class_onload = function (o) {
    moLibrary.slct_asset_edit_class_onchange(o);
}
moLibrary.slct_asset_edit_class_onchange = function (o) {
    txtfld_asset_edit_latitude.setProperties({v_on: (o.get() == 'Plant')});
    txtfld_asset_edit_longitude.setProperties({v_on: (o.get() == 'Plant')});
    txtfld_asset_edit_address.setProperties({m_on: (o.get() == 'Plant'), v_on: (o.get() == 'Plant')});


    txtfld_asset_edit_quantity.setProperties({v_on: (o.get() == 'Equipment' || o.get() == 'Subunit' || o.get() == 'Component' || o.get() == 'Part')});
    txtfld_asset_edit_serial.setProperties({v_on: (o.get() == 'Equipment' || o.get() == 'Subunit' || o.get() == 'Component' || o.get() == 'Part')});
    txtfld_asset_edit_asset_uom.setProperties({v_on: (o.get() == 'Equipment' || o.get() == 'Subunit' || o.get() == 'Component' || o.get() == 'Part')});

    txtfld_asset_edit_area.setProperties({v_on: (o.get() == 'Location' || o.get() == 'Department' || o.get() == 'System/Sector' || o.get() == 'Floor')});
    txtfld_asset_edit_perimeter.setProperties({v_on: (o.get() == 'Location' || o.get() == 'Department' || o.get() == 'System/Sector' || o.get() == 'Floor')});

    if (o.get() != 'Plant') {
        txtfld_asset_edit_latitude.set('');
        txtfld_asset_edit_longitude.set('');
        txtfld_asset_edit_address.set('');
    }
    if (o.get() != 'Location' && o.get() != 'Department' && o.get() != 'System/Sector' && o.get() != 'Floor') {
        txtfld_asset_edit_area.set('');
        txtfld_asset_edit_perimeter.set('');
    }
    if (o.get() != 'Equipment' && o.get() != 'Subunit' && o.get() != 'Component' && o.get() != 'Part') {
        txtfld_asset_edit_quantity.set('');
        txtfld_asset_edit_serial.set('');
    }
}

moLibrary.txtfld_asset_edit_address_onchange = function (o) {
    if (o.get() == '') {
        txtfld_asset_edit_latitude.set('');
        txtfld_asset_edit_longitude.set('');
    } else if (slct_asset_edit_class.get() == 'Plant') {
        mainsimMap.get_coordinates_by_address(txtfld_asset_edit_address.get(), function (data) {
            txtfld_asset_edit_latitude.set(data[0].lat);
            txtfld_asset_edit_longitude.set(data[0].long);
        });
    }
}
moLibrary.slct_action_edit_priority_color_onload = function (o) {
    moLibrary.slct_action_edit_priority_color_onchange(o);
}
moLibrary.slct_action_edit_priority_color_onchange = function (o) {
    eval('txtfld_action_edit_priority_color_choose_comp').style.backgroundColor = o.get();
}

moLibrary.slct_wo_phases_onchange = function (o) {
    if (mdl_wo_edit.data.f_phase_id == 1)
        pck_wo_edit_owner_name.setProperties({m_on: (o.get('int') != 7 && o.get('int') != 0)});
    txtfld_wo_edit_ending_date.setProperties({m_on: (o.get('int') == 6)});
    txtfld_wo_edit_add_value_meter.setProperties({m_on: (mdl_wo_edit.data.fc_wo_linked_pm_oc == 1 && o.get('int') == 6)});
}
moLibrary.slct_wo_phases_onload = function (o) {
    moLibrary.slct_wo_phases_onchange(o);
    pck_wo_edit_owner_name.setProperties({m_on: (o.get('int') != 0)});
}
moLibrary.txtfld_wo_edit_owner_phone_onload = function (o) {
    if (mdl_wo_edit.data.f_phase_id == 1)
        pck_wo_edit_owner_name.setProperties({m_on: (slct_wo_phases.get('int') != 0)});
}

/*moLibrary.txtfld_wo_edit_generator_id_onload = function (o){
 txtfld_wo_edit_generator_id.setProperties({v_on: (o.get() != '')});
 if((mdl_wo_edit.data.f_category == 'PLANNED' || mdl_wo_edit.data.f_category == 'INSPECTION') && (o.get()).indexOf('ID') == -1){
 moLibrary.progress_fields(o);
 var raised = o.get();
 txtfld_wo_edit_generator_id.set('ID: ' + raised + ' - Category: ' + mdl_wo_edit.data.f_category);
 }
 }*/
/**
 * Set zeros before every progress field
 */
moLibrary.progress_fields = function (o) {
    var v = o.get();
    if (v) {
        for (var str_i = 0; str_i < 5 - v.length; i++)
            v = "0" + v;
    }
    o.set(v);
};
moLibrary.txtfld_wo_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_chain_edit_fc_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_cond_chain_edit_progre_onload = moLibrary.progress_fields;
moLibrary.txtfld_material_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_std_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_asset_edit_fc_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_contract_edit_fc_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_wr_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_rsc_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_slc_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_tool_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_fc_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_pm_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_cond_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_meter_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_task_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_tasklist_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_doc_edit_progress_onload = moLibrary.progress_fields;
moLibrary.txtfld_supply_edit_fc_progress_onload = moLibrary.progress_fields;
/**/


/*moLibrary.txtfld_asset_edit_f_field_wiz_onload = function (o){
 if(o.get() == '') txtfld_asset_edit_f_field_wiz.set("t_wares:'" + mdl_asset_edit.data.f_code + "'");
 }*/

moLibrary.txtfld_fc_edit_occurrence_onchange = function (o) {
    var v = txtfld_fc_edit_severity.getValue('int') * txtfld_fc_edit_occurrence.getValue('int') * txtfld_fc_edit_detection.getValue('int');
    txtfld_fc_edit_rpn.setProperties({value: v, description: v});
}

moLibrary.txtfld_fc_edit_detection_onchange = function (o) {
    var v = txtfld_fc_edit_severity.getValue('int') * txtfld_fc_edit_occurrence.getValue('int') * txtfld_fc_edit_detection.getValue('int');
    txtfld_fc_edit_rpn.setProperties({value: v, description: v});
}

moLibrary.txtfld_fc_edit_severity_onchange = function (o) {
    var v = txtfld_fc_edit_severity.getValue('int') * txtfld_fc_edit_occurrence.getValue('int') * txtfld_fc_edit_detection.getValue('int');
    txtfld_fc_edit_rpn.setProperties({value: v, description: v});
}

moLibrary.isEmpty_new = function (obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}


moLibrary.mdl_slc_edit_save = function (data) {
    //   var code = data.f_code;
    //   var codes = '10,11,12,13,14,15,16,17,18,19';    codes.indexOf(code) == -1

    var prnt = data.f_parent_code;

    if (prnt != "0" && moLibrary.isEmpty_new(mdl_slc_parent_tg_tg.JStore))
        return 'Error: missing parent';
    else
        return true;


}



moLibrary.phone_control = function (phone) {
    for (var i = 0; i < phone.length; i++) {
        var c = phone.charAt(i);

        if (!(parseInt(c) >= 0 && parseInt(c) <= 9) && c != ' ' && c != '.' && c != '+')
            return false;
        if (c == '+' && i > 0)
            return false;
        if (c == ' ' && i > 0 && !((parseInt(phone.charAt(i - 1)) >= 0 && parseInt(phone.charAt(i - 1)) <= 9) && (parseInt(phone.charAt(i + 1)) >= 0 && parseInt(phone.charAt(i + 1)) <= 9)))
            return false;
        if (c == '.' && !((parseInt(phone.charAt(i - 1)) >= 0 && parseInt(phone.charAt(i - 1)) <= 9) && (parseInt(phone.charAt(i + 1)) >= 0 && parseInt(phone.charAt(i + 1)) <= 9)))
            return false;
        if (i == 0 && (c == ' ' || c == '.'))
            return false;
    }
    return true;
}

moLibrary.txtfld_rsc_edit_phone_onchange = function (o) {
    if (!moLibrary.phone_control(o.get())) {
        o.set('');
        moGlb.alert.show(moGlb.langTranslate('Phone is wrong'));
    }
}
moLibrary.txtfld_vendor_edit_phone_onchange = function (o) {
    if (!moLibrary.phone_control(o.get())) {
        o.set('');
        moGlb.alert.show(moGlb.langTranslate('Phone is wrong'));
    }
}
moLibrary.txtfld_vendor_edit_resp_phone_onchange = function (o) {
    if (!moLibrary.phone_control(o.get())) {
        o.set('');
        moGlb.alert.show(moGlb.langTranslate('Phone is wrong'));
    }
}
moLibrary.txtfld_vendor_edit_alt_phone_onchange = function (o) {
    if (!moLibrary.phone_control(o.get())) {
        o.set('');
        moGlb.alert.show(moGlb.langTranslate('Phone is wrong'));
    }
}
moLibrary.txtfld_usr_edit_fc_usr_phone_onchange = function (o) {
    if (!moLibrary.phone_control(o.get())) {
        o.set('');
        moGlb.alert.show(moGlb.langTranslate('Phone is wrong'));
    }
}


moLibrary.txtfld_meter_edit_add_value_onchange = function (o) {
    var oggi = new Date;
    if (o.get() != '')
        txtfld_meter_edit_add_value_time.set(parseInt(oggi.getTime() / 1000));
    else
        txtfld_meter_edit_add_value_time.set('');
}

moLibrary.txtfld_asset_edit_bookable_minimum_hour_onchange = function (o) {
    moLibrary.max_min(o, txtfld_asset_edit_bookable_maximum_hour.get(), o.get());
}
moLibrary.txtfld_asset_edit_bookable_maximum_hour_onchange = function (o) {
    moLibrary.max_min(o, o.get(), txtfld_asset_edit_bookable_minimum_hour.get());
}
moLibrary.chkbx_asset_edit_bookable_onchange = function (o) {
    txtfld_asset_edit_bookable_minimum_hour.setProperties({m_on: (o.get('int') == 1)});
    txtfld_asset_edit_bookable_maximum_hour.setProperties({m_on: (o.get('int') == 1)});
}

moLibrary.chkbx_asset_edit_bookable_onload = function (o) {
    moLibrary.chkbx_asset_edit_bookable_onchange(o);
}

moLibrary.max_min = function (o, max, min) {
    if (Math.max(max, min) == min && max != '' && min != '') {
        moGlb.alert.show(moGlb.langTranslate("Minimum minutes are bigger than maximum minutes"));
        o.set('');
    }
}

moLibrary.txtfld_asset_edit_daily_usage_onchange = function (o) {
    if (!(o.get() >= 0 && o.get() <= 1) && o.get() != '') {
        moGlb.alert.show(moGlb.langTranslate("This field takes value between 0 and 1. It's calculated as (number of hour of usage /24). The default value is 1 (24h/24h)"));
        o.set('');
    }
}

moLibrary.mdl_cond_edit_save = function (data) {
    mdl_cond_edit.data.fc_cond_run_operation = 'OPENWORKORDER();';
    return true;
};

moLibrary.slct_prj_edit_adapter_onchange = function (o) {
    moLibrary.adapter_host_control(o.get(), txtfld_prj_edit_host.get());
}
moLibrary.txtfld_prj_edit_host_onchange = function (o) {
    moLibrary.adapter_host_control(slct_prj_edit_adapter.get(), o.get());
}

moLibrary.txtfld_prj_edit_username_onload = function (o) {
    moLibrary.adapter_host_control(slct_prj_edit_adapter.get(), txtfld_prj_edit_host.get());
}
moLibrary.txtfld_prj_edit_password_onload = function (o) {
    moLibrary.adapter_host_control(slct_prj_edit_adapter.get(), txtfld_prj_edit_host.get());
}

moLibrary.adapter_host_control = function (adapter, host) {
    txtfld_prj_edit_username.setProperties({v_on: !(adapter == 'PDO_MYSQL' && host == 'default')});
    txtfld_prj_edit_password.setProperties({v_on: !(adapter == 'PDO_MYSQL' && host == 'default')});
}


moLibrary.txtfld_usr_edit_fc_usr_firstname_onchange = function (o) {
    moLibrary.set_user_name(o);
}
moLibrary.txtfld_usr_edit_fc_usr_lastname_onchange = function (o) {
    moLibrary.set_user_name(o);
}
moLibrary.txtfld_up_edit_fc_usr_firstname_onchange = function (o) {
    moLibrary.set_user_name(o);
}
moLibrary.txtfld_up_edit_fc_usr_lastname_onchange = function (o) {
    moLibrary.set_user_name(o);
}

moLibrary.set_user_name = function (o) {
    var mod = moLibrary.getModule(o.name);
    if (eval('txtfld_' + mod + '_edit_fc_usr_firstname').get() != '' && eval('txtfld_' + mod + '_edit_fc_usr_lastname').get() != '') {
        eval('txtfld_' + mod + '_edit_f_title').set(eval('txtfld_' + mod + '_edit_fc_usr_firstname').get() + " " + eval('txtfld_' + mod + '_edit_fc_usr_lastname').get())
    }
}


//6 OTTOBRE 2015 DA METTERE SU MULTI
moLibrary.wo_rsc_since_till = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name), dates = moLibrary.check_start_end_date(o, 'since', 'till'), hrs = 0, mp = eval('txtfld_' + mod + '_edit_manpower').get('int'),
                uc = eval('txtfld_' + mod + '_edit_fee').get('double'), cost = '0.00';
        if (dates && (dates.end > dates.start))
            hrs = (dates.end - dates.start) / (60 * 60), cost = (hrs * mp * uc).toFixed(2);
        var e = dates.end, s = dates.start;
        if (dates.end == undefined)
            e = '';
        if (dates.start == undefined)
            s = '';

        if (e != eval('mdl_' + mod + '_edit').data.fc_rsc_till || s != eval('mdl_' + mod + '_edit').data.fc_rsc_since) {
            if (hrs == 0)
                hrs = "0.00";

            eval('txtfld_' + mod + '_edit_hours').set(hrs);
            eval('txtfld_' + mod + '_edit_total_hours').set((hrs * mp).toFixed(2));
            eval('txtfld_' + mod + '_edit_cost').set(cost);

            eval('txtfld_' + mod + '_edit_since').setProperties({m_on: eval('txtfld_' + mod + '_edit_till').get('int') > 0 && eval('txtfld_' + mod + '_edit_since').get('int') == 0});
        }
    }
};

moLibrary.wo_rsc_hours_manpower = function (o) {
    var mod = moLibrary.getModule(o.name), hrs = eval('txtfld_' + mod + '_edit_hours').get('double'), mp = eval('txtfld_' + mod + '_edit_manpower').get('int'),
            uc = eval('txtfld_' + mod + '_edit_fee').get('double'), th = (hrs * mp).toFixed(2);
    eval('txtfld_' + mod + '_edit_total_hours').set(th);
    eval('txtfld_' + mod + '_edit_cost').set((th * uc).toFixed(2));
};
/////////////////////////////////////////

//moLibrary.ownerFilter = function (o) {
//    var filter = {
/********************************************************************************************************************************************/
//        Example of filtering array, you can use only items you are interested in
//        Sort: [
//            { f: '<field 1 bind>', m: '<ASC|DESC>' }
//            { f: '<field n bind>', m: '<ASC|DESC>' }
//        ],
//        Filter: [
//            { f: '<field 1 bind>', v: '<value>', ty: 4 }
//        ],
//        Search: [
//            { f: '<field 1 bind>', v0: '<value>', Mm: <0 case insensitive | 1 case sensitive>, Sel: <type of search, it's a number and dipends on field type (text, numeric or date). See treegridSearch.js for more details>, type: <0 text | 1 numeric | 2 boolean | 3 date | 4 select | 5 static (image) > }
//        ],
//        Ands: [
//            { f: '<field 1 bind>', z: '<sql expression (es. > 1)>' },
//            { f: '<field n bind>', z: '<sql expression (es. IN (1, 2))>' },
//        ],
//        Selector: {
//             <name of module selector 1>: { s: <array of selector codes>, h: <0 plain | 1 hierarchy> }
//             <name of module selector n>: { s: <array of selector codes>, h: <0 plain | 1 hierarchy> }
//        }
/********************************************************************************************************************************************/
//        Search: [
//            { f: 'f_title', v0: 'mainsim', Mm: 0, Sel: 0, type: 0 }
//        ],
//    };
//    eval('mdl_'+o.picktype+'_pp_tg').filter(filter);
//};

//moLibrary.prova_owner = function (o){
//    var filter = {
//        Selector: {
//            mdl_wo_selector3: { s: ["5867"], h: 0 }
//        }

/* Selector: [
 { mdl_wo_selector3: { s: 10, h: 0 } }
 ]*/
//    }
//    console.log(filter);
//    eval('mdl_'+o.picktype+'_pp_tg').filter(filter);
//}

/*moLibrary.filter_owner = function (o){
 var filter = {
 Search: [
 { f: 'f_title', v0: 'mainsim', Mm: 0, Sel: 0, type: 0 }
 ]
 
 }
 
 eval('mdl_'+o.picktype+'_pp_tg').filter(filter);
 }*/



moLibrary.txtfld_doc_edit_tag_onload = function (o) {
    try {
        if (moGlb.isset(mn_doc_new)) {
            txtfld_doc_edit_attach.setProperties({v_on: (moGlb.isset(mdl_doc_edit.data.t_wares_parent_5) || (moGlb.isset(mdl_doc_edit.data.fc_doc_attach) && mdl_doc_edit.data.fc_doc_attach != '')), m_on: (moGlb.isset(mdl_doc_edit.data.t_wares_parent_5) || (moGlb.isset(mdl_doc_edit.data.fc_doc_attach) && mdl_doc_edit.data.fc_doc_attach != ''))});
            slct_doc_edit_type.setProperties({v_on: (moGlb.isset(mdl_doc_edit.data.t_wares_parent_5) || (moGlb.isset(mdl_doc_edit.data.fc_doc_attach) && mdl_doc_edit.data.fc_doc_attach != ''))});
            txtfld_doc_edit_author.setProperties({v_on: (moGlb.isset(mdl_doc_edit.data.t_wares_parent_5) || (moGlb.isset(mdl_doc_edit.data.fc_doc_attach) && mdl_doc_edit.data.fc_doc_attach != ''))});
            txtfld_doc_edit_version.setProperties({v_on: (moGlb.isset(mdl_doc_edit.data.t_wares_parent_5) || (moGlb.isset(mdl_doc_edit.data.fc_doc_attach) && mdl_doc_edit.data.fc_doc_attach != ''))});
            txtfld_doc_edit_tag.setProperties({v_on: (moGlb.isset(mdl_doc_edit.data.t_wares_parent_5) || (moGlb.isset(mdl_doc_edit.data.fc_doc_attach) && mdl_doc_edit.data.fc_doc_attach != ''))});
        }
    } catch (e) {
    }
}






//---------------------------------------------------------------------------------------------------------------------
/*
 Setting Inspections
 
 inspections_number          :   10    n. default of rows searched
 inspections_no_wf_phases    :   2_4,2_5,3_4,...    wfn_phasen,...
 inspections_wf_destinations :   2_3,3_6,...        wfn_phasen,...
 inspections_back_date       :   0 all else  n. gg     n. of day / month back from now
 inspections_table           :   t_workorders  / t_wares
 
 in t_scripts: getInspectionCodes
 
 */
moLibrary.f_inspections = function (modl) {      // modl = 'mdl_wo_tg'

    var Mname = modl + "_tg";
    moLibrary.Mod_inspection = window[Mname];


    if (!moGlb.inspections_number)
        moGlb.inspections_number = 0;

    //  moLibrary.iwin=null;
    //  moLibrary.iwin_end=null;


    // popup to get number of rows and confirm inspection (get default value from setting!)
    var cont_insp = mf$('id_cont_insp');
    try {
        moLibrary.iwin = eval('win_cont_insp');
    } catch (ex) {
        if (!cont_insp) {
            cont_insp = moGlb.createElement('id_cont_insp', 'div', null, true);
            with (cont_insp.style) {
                width = '240px';
                height = '160px';
                overflow = 'hidden';
            }
        }

        moLibrary.iwin = moComp.createComponent("win_cont_insp", "moPopup", {
            contentid: 'id_cont_insp', title: moGlb.langTranslate('Inspections'),
            width: 240, height: 160
        }, true);
    }


    var b, str = "",
            tx1 = moGlb.langTranslate("Number of items to be searched:"),
            tx2 = moGlb.langTranslate("Run your research?");

    b = "font:13px opensansR;color:#FFF;background-color:#A1C24D;border:0;border-radius:2px;cursor:pointer;' " +
            "onmouseover='this.style.backgroundColor=\"#E0E0E0\"' " +
            "onmouseout='this.style.backgroundColor=\"#A1C24D\"' " +
            "onmousedown='this.style.backgroundColor=\"#6F8D97\"' onmousemove='return false;'";


    str = "<div style='position:absolute;left:10px;top:18px;font:bold 13px opensansR;'>" + tx1 + "</div>" +
            "<input id='id_inspections_num' style='position:absolute;left:10px;top:40px;width:212px;height:27px;border:1px solid #436B75;border-radius:2px;font:13px opensansR;text-align:right;' value='" + moGlb.inspections_number + "' />" +
            "<div id='id_inspections_error_msg' style='position:absolute;left:10px;top:76px;width:216px;font:13px opensansR;color:#A00;text-align:right;'></div>" +
            "<div style='position:absolute;left:10px;top:100px;font:13px opensansR;'>" + tx2 + "</div>" +
            "<button style='position:absolute;right:0;bottom:0;width:60px;height:28px;" + b + " onclick='moLibrary.f_inspections_start()'>" + moGlb.langTranslate("Find") + "</button>" +
            "<button style='position:absolute;left:10;bottom:0;width:60px;height:28px;" + b + " onclick='moLibrary.f_inspections_close(0)'>" + moGlb.langTranslate("Cancel") + "</button>";


    cont_insp.innerHTML = str;
    moLibrary.iwin.show();
}



moLibrary.f_inspections_close = function (i) {
    if (i) {
        if (moLibrary.iwin_end)
            moLibrary.iwin_end.hide();
    } else {
        if (moLibrary.iwin)
            moLibrary.iwin.hide();
    }
}



moLibrary.f_inspections_start = function () {

    var ji = mf$("id_inspections_num"), n_insp = parseInt(ji.value);

    if (!n_insp || n_insp < 0 || n_insp > 1000) {

        var je = mf$("id_inspections_error_msg");

        je.innerHTML = moGlb.langTranslate("Invalid number!");
        setTimeout(function () {
            je.innerHTML = "";
        }, 2500);

        return;
    }

    moLibrary.f_inspections_close();
    moGlb.loader.show(moGlb.langTranslate("Looking for items..."));

    var Md = moLibrary.Mod_inspection;

    var stg, f, fltrs = Md.getData();
    f = fltrs.data;
    f["Hierarchy"] = Md.asHy;

    // settings
    stg = {
        "inspections_number": n_insp, // input from user
        "inspections_table": (moGlb.inspections_table ? moGlb.inspections_table : "t_workorders"),
        "ftype": Md.ftype, // categories: 1,4,13,...
        "inspections_back_date": (moGlb.inspections_back_date ? moGlb.inspections_back_date : 0),
        "inspections_wf_destinations": (moGlb.inspections_wf_destinations ? moGlb.inspections_wf_destinations : ""),
        "inspections_no_wf_phases": (moGlb.inspections_no_wf_phases ? moGlb.inspections_no_wf_phases : "")
    };

    //alert(JSON.stringify(f));  return;

    Ajax.sendAjaxPost(moGlb.baseUrl + "/script/execute", "script=getInspectionCodes&settings=" + JSON.stringify(stg) + "&name=" + Md.name + "&filters=" + JSON.stringify(f), "moLibrary.f_inspections_retCodes");
}


//---------------------------------------------------------------------------------------------------------------------

moLibrary.f_inspections_retCodes = function (a) {

    moGlb.loader.hide();
    //console.log(a);

    try {
        var jsn = JSON.parse(a);
    } catch (e) {
        alert(e + "\n\n" + a);
        return;
    }

    // popup esito

    var cont_insp_end = mf$('id_cont_insp_end');

    try {
        moLibrary.iwin_end = eval('win_cont_insp_end');
    } catch (ex) {

        if (!cont_insp_end) {
            cont_insp_end = moGlb.createElement('id_cont_insp_end', 'div', null, true);
            with (cont_insp_end.style) {
                width = '240px';
                height = '160px';
                overflow = 'hidden';
            }
        }

        moLibrary.iwin_end = moComp.createComponent("win_cont_insp_end", "moPopup", {
            contentid: 'id_cont_insp_end', title: moGlb.langTranslate('Inspection search result'),
            width: 240, height: 160
        }, true);

    }


    var b, str = "",
            tx1 = moGlb.langTranslate("N. of items to be searched: "),
            tx2 = moGlb.langTranslate("N. of items found: "),
            tx3 = moGlb.langTranslate("N. of items set to inspection: ");

    b = "font:13px opensansR;color:#FFF;background-color:#A1C24D;border:0;border-radius:2px;cursor:pointer;' " +
            "onmouseover='this.style.backgroundColor=\"#E0E0E0\"' " +
            "onmouseout='this.style.backgroundColor=\"#A1C24D\"' " +
            "onmousedown='this.style.backgroundColor=\"#6F8D97\"' onmousemove='return false;'";


    str = "<div style='position:absolute;left:10px;top:18px;font: 13px opensansR;'>" + tx1 + "<span style='font-weight:bold;'>" + jsn.wish + "</span></div>" +
            "<div style='position:absolute;left:10px;top:46px;font: 13px opensansR;'>" + tx2 + "<span style='font-weight:bold;'>" + jsn.find + "</span></div>" +
            "<div style='position:absolute;left:10px;top:74px;font: bold 13px opensansR;'>" + tx3 + jsn.changed + "</div>" +
            "<button style='position:absolute;left:10;bottom:0;width:60px;height:28px;" + b + " onclick='moLibrary.f_inspections_close(1)'>" + moGlb.langTranslate("Close") + "</button>" +
            "<button style='position:absolute;right:0;bottom:0;width:60px;height:28px;" + b + " onclick='moLibrary.f_inspections_show(\"" + jsn.codes.join(",") + "\")'>" + moGlb.langTranslate("Show") + "</button>";


    cont_insp_end.innerHTML = str;
    moLibrary.iwin_end.show();
}




moLibrary.f_inspections_show = function (cods) {

    var Md = moLibrary.Mod_inspection;
    moLibrary.f_inspections_close(1);

    var acods = cods.split(",");
    var tx = acods.join("||");

    Md.AND = [], Md.FILTER = [], Md.SEARCH = [], Md.SELECTOR = {}, Md.LIKE = {};
    if (tx) {
        Md.LIKE["Text"] = Base64.encode(tx);
        Md.LIKE["Fields"] = ["f_code"]
    }
    ;


    Md.ActRefresh();

}

///////  end inspections

/* force PM from asset */
moLibrary.force_pm_on_asset = function () {
    var codes_list = (mdl_asset_pm_tg_tg.arrCHK).join(",");
    var code_asset = mdl_asset_edit.data.f_code;
    Ajax.sendAjaxPost(moGlb.baseUrl + "/workorder/force-multi-pm/", 'code_asset=' + code_asset + '&codes_list=' + codes_list, 'moLibrary.print_result_pm_of_asset');

}

moLibrary.forcedPM = null;
moLibrary.print_result_pm_of_asset = function (result) {
    var obj = JSON.parse(result);
    moLibrary.forcedPM = obj.codes;
    moGlb.alert.show(moGlb.langTranslate(obj.message));
}

//Luca (preso da contarina.js da Cesare) 29/05/17
moLibrary.createChildFileCustom = function (data) {
    with (this) {
        // Solo se non ci sono elementi selezionati
        if (mdl_doc_tg.getSelected().length == 0) {
            data = {};
            data.f_type_id = 5;
            data['ischild'] = 1;
            data['t_wares_parent_5'] = [];
            mdl_doc_tg.createItem(data);
        } else {
            moGlb.alert.show("Per creare un documento nella root non ci devono essere elementi selezionati");
        }
    }
};
//Imposta automaticamente il title di quel modulo come il campo che lo richiama
moLibrary.setAutoTitle = function (o) {
    with (o) {
        var mod = moLibrary.getModule(o.name);
        var item = o.get();
        eval('txtfld_' + mod + "_edit_title").set(item);
    }

};


moLibrary.setOwnerData = function (d) {
    if (d.length == 0) {
        mdl_wo_edit.addCustomData('fc_owner_code', '');
        mdl_wo_edit.addCustomData('fc_owner_avatar', '');
        return;
    }
    data = JSON.parse(d);
    mdl_wo_edit.addCustomData('fc_owner_id', data.fc_owner_code);
    mdl_wo_edit.addCustomData('fc_owner_avatar', data.fc_owner_avatar);
}

popupCustom = function (title, file, width, height, callback) {
    /****aggiunto da elCarbo in data 10/10/2018****/
    //recupera eventuali parametri extra e eventuale callback
    var extra;
    var re = file.match(/^(.*)\?(.*)/);
    if (re && re.length == 3) { //es: 'popup_cdc?reverse=1&contabile=1'
        file = re[1];
        extra = re[2].replace(/&/g, ",");
    }
    extra = extra || '';
    callback = callback || ''; //eventuale callback del popup
    /*******************************************/

    Ajax.sendAjaxPost(moGlb.baseUrl + "/script/popup/", 'title=' + title + '&file=' + file + '&width=' + width + '&height=' + height + '&callback=' + callback + '&extra=' + extra, 'fillPopupCustom');
}

fillPopupCustom = function (data) {
    var d = JSON.parse(data);
    var popup = d.callback? 
        moComp.createComponent("custom_popup", "moPopup", {contentid: 'custom_popup', title: d.title, width: d.width, height: d.height, onunload:eval(d.callback)}):
        moComp.createComponent("custom_popup", "moPopup", {contentid: 'custom_popup', title: d.title, width: d.width, height: d.height});
    
    popup.show();
    var popupCont = document.getElementById('custom_popup_win_bd');
    popupCont.innerHTML = atob(d.content);
    popupCont.parentNode.style.marginLeft = 'auto';
    popupCont.parentNode.style.marginRight = 'auto';
    popupCont.parentNode.style.width = d.width + 'px';
    popupCont.parentNode.style.height = d.height + 'px';
}


moLibrary.new_picklist_treegrid_format = function () {
    try {
        var labels = JSON.parse(t)['labels'];
        t = labels.join(", ");
    } catch (e) {

    }
    if (moCnvUtils.measureText(t, f) > x21)
        t = moGlb.reduceText(t, f, x21);
    fillStyle = colr;
    if (t == "null" || t === null)
        t = "";
    fillText(t, X1 + d + 7, y + 8 + (Yi - 7) / 2);
    TIPtext = v;

}

moLibrary.new_picklist_treegrid_format = function () {
    try {
        var labels = JSON.parse(t)['labels'];
        t = labels.join(", ");
    } catch (e) {

    }
    if (moCnvUtils.measureText(t, f) > x21)
        t = moGlb.reduceText(t, f, x21);
    fillStyle = colr;
    if (t == "null" || t === null)
        t = "";
    fillText(t, X1 + d + 7, y + 8 + (Yi - 7) / 2);
    TIPtext = v;

}

moLibrary.pck_wo_edit_slc_4_onchange = moLibrary.pck_wo_edit_slc_4_onload = function (o) {
    try {
        pck_wo_edit_usr.removeBackendSelector();
        if (o.value != "") {
            pck_wo_edit_usr.addBackendSelector(JSON.parse(o.value).codes[0]);
        }
    } catch (e) {
        console.log(e);
    }
}

moLibrary.pck_wo_edit_slc_4_onclear = function (o) {
    try {
        pck_wo_edit_usr.removeBackendSelector();
    } catch (e) {
        console.log(e);
    }
}

// NICO: logout mobile
moLibrary.logoutMobile = function () {
    callbackPopupLogoutMobile = function (data) {
        clickedButton = function () {
            callbackUpdate = function (data) {
                data = JSON.parse(data);
                moGlb.loader.hide();
                if (data['msg'] == "SESSION_UPDATED") {
                    custom_popup.hide();
                } else {
                    console.log("ERR");
                }
            }
            var utente = document.getElementById("user1");
            var utenteId = utente.options[utente.selectedIndex].value;
            if (utenteId != "") {
                moGlb.loader.show();
                console.log(moGlb.baseUrl + "../api/debug/users/persist/" + utenteId + "/0");
                $.get(moGlb.baseUrl + "../api/debug/users/persist/" + utenteId + "/0", callbackUpdate);
                //Ajax.sendAjaxGet(moGlb.baseUrl + "../api/debug/users/persist/" + utenteId + "/0", "callbackUpdate");
            } else {
                document.getElementById("errorMessage").style.display = "block";
            }
        }
        var data = JSON.parse(data);
        var popup = moComp.createComponent("custom_popup", "moPopup", {contentid: "custom_popup", title: "Logout user", width: 300, height: 400});
        popup.show();
        var popupCont = document.getElementById('custom_popup_win_bd');
        popupCont.innerHTML = "<div style='margin-top:10px'>" +
                "<p class='label'>Selezionare utente</p><br>" +
                "<select id='user1' style='width:98%'><option value=''>Seleziona utente</option></select><br>" +
                "<br><div style='text-align: center'><button type='button' style='margin-left: auto;margin-right: auto;width:50px' onclick='clickedButton()'>Invia</button></div><br></div>" +
                "<p class='label' style='color:red; display:none' id='errorMessage'>Selezionare un utente</p>";
        popupCont.parentNode.style.marginLeft = 'auto';
        popupCont.parentNode.style.marginRight = 'auto';
        popupCont.parentNode.style.marginTop = 'auto';
        popupCont.parentNode.style.width = 300 + 'px';
        popupCont.parentNode.style.height = 200 + 'px';
        var select1 = document.getElementById("user1");
        var users = [];
        users[""] = "";
        for (var value in data['user']) {
            users[data['user'][value]['f_id']] = data['user'][value]['f_title'];
            var option = new Option(data['user'][value]['f_title'], data['user'][value]['f_id']);
            select1.appendChild(option);
        }
    }
    Ajax.sendAjaxPost(moGlb.baseUrl + "/script/execute", "script=get_popup_values", "callbackPopupLogoutMobile");
};