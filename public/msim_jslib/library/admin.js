/*
 * Stand by till modules will be activated
moLibrary.moduleSelection = function (o, comp) {
    var mod = moLibrary.getModule(o.name), md = eval('slct_'+mod+'_edit_module').get(), sb = eval('slct_'+mod+'_edit_submodule').get(),
    ps = eval('slct_'+mod+'_edit_position').get(), nm = eval('txtfld_'+mod+'_edit_name').get(), inm = eval('txtfld_'+mod+'_edit_instance_name');
    inm.set(comp+'_'+md+(sb?'_'+sb:'')+(ps?'_'+ps:'')+'_'+nm);
};
moLibrary.moduleSelection = function (o, comp) {
    var mod = moLibrary.getModule(o.name), md = eval('slct_'+mod+'_edit_module').get(), sb = eval('slct_'+mod+'_edit_submodule').get(),
    ps = eval('slct_'+mod+'_edit_position').get(), nm = eval('txtfld_'+mod+'_edit_name').get(), inm = eval('txtfld_'+mod+'_edit_instance_name');
    inm.set(comp+'_'+md+(sb?'_'+sb:'')+(ps?'_'+ps:'')+'_'+nm);
};
moLibrary.slct_button_edit_module_onchange = function (o) { moLibrary.moduleSelection(o, 'btn'); };
moLibrary.slct_button_edit_submodule_onchange = function (o) { moLibrary.moduleSelection(o, 'btn'); };
moLibrary.slct_button_edit_position_onchange = function (o) { moLibrary.moduleSelection(o, 'btn'); };
moLibrary.txtfld_button_edit_name_onchange = function (o) { moLibrary.moduleSelection(o, 'btn'); };
moLibrary.slct_button_edit_module_onload = function (o) { moLibrary.setModuleFields(o, 'btn'); };
*/

moLibrary.slct_component_edit_checktype_onload = function (o) {
    var exp = '';
    try { exp = mdl_component_edit.data.checkExp.split('_'); } catch(ex) { exp[0] = ''; }
    o.set(exp[0]);
    txtfld_component_edit_decimal.setProperties({v_on: exp[0] == 'decimal', m_on: exp[0] == 'decimal'});
    txtfld_component_edit_range_i.setProperties({v_on: exp[0] == 'range', m_on: exp[0] == 'range'});
    txtfld_component_edit_range_f.setProperties({v_on: exp[0] == 'range', m_on: exp[0] == 'range'});
    txtfld_component_edit_nchar_n.setProperties({v_on: exp[0] == 'nchar', m_on: exp[0] == 'nchar'});
    txtfld_component_edit_nchar_m.setProperties({v_on: exp[0] == 'nchar'});
    txtfld_component_edit_not_s.setProperties({v_on: exp[0] == 'not', m_on: exp[0] == 'not'});
    txtfld_component_edit_regexp_pattern.setProperties({v_on: exp[0] == 'regexp', m_on: exp[0] == 'regexp'});

//    switch(exp[0]) {
//        case 'decimal': txtfld_component_edit_decimal.set(exp[1]); break;
//        case 'range': txtfld_component_edit_range_i.set(exp[1]); txtfld_component_edit_range_f.set(exp[2]); break;
//        case 'nchar': txtfld_component_edit_nchar_n.set(exp[1]); txtfld_component_edit_nchar_m.set(exp[2]); break;
//        case 'not': txtfld_component_edit_not_s.set(exp[1]); break;
//        case 'regexp': txtfld_component_edit_regexp_pattern.set(exp[1]); break;
//        default: break;
//    }
};
moLibrary.txtfld_component_edit_decimal_onload = function (o) {
    var exp = mdl_component_edit.data.checkExp;
    if(exp && exp.indexOf('decimal') == 0) o.set(exp.split('_')[1]);
};
moLibrary.txtfld_component_edit_range_i_onload = function (o) {
    var exp = mdl_component_edit.data.checkExp;
    if(exp && exp.indexOf('range') == 0) o.set(exp.split('_')[1]);
};
moLibrary.txtfld_component_edit_range_f_onload = function (o) {
    var exp = mdl_component_edit.data.checkExp;
    if(exp && exp.indexOf('range') == 0) o.set(exp.split('_')[2]);
};
moLibrary.txtfld_component_edit_nchar_n_onload = function (o) {
    var exp = mdl_component_edit.data.checkExp;
    if(exp && exp.indexOf('nchar') == 0) o.set(exp.split('_')[1]);
};
moLibrary.txtfld_component_edit_nchar_m_onload = function (o) {
    var exp = mdl_component_edit.data.checkExp;
    if(exp && exp.indexOf('nchar') == 0 && exp.match(/_/g).length == 2) o.set(exp.split('_')[2]);
};
moLibrary.txtfld_component_edit_not_s_onload = function (o) {
    var exp = mdl_component_edit.data.checkExp;
    if(exp && exp.indexOf('not') == 0) o.set(exp.split('_')[1]);
};
moLibrary.txtfld_component_edit_regexp_pattern_onload = function (o) {
    var exp = mdl_component_edit.data.checkExp;
    if(exp && exp.indexOf('regexp') == 0) o.set(exp.split('_')[1]);
};
moLibrary.slct_component_edit_checktype_onchange = function (o) {
    var exp = o.get(), dec = txtfld_component_edit_decimal.get('int'), ri = txtfld_component_edit_range_i.get('int'),
    rf = txtfld_component_edit_range_f.get('int'), ncn = txtfld_component_edit_nchar_n.get('int'), ncm = txtfld_component_edit_nchar_m.get('int'),
    not = txtfld_component_edit_not_s.get(), rex = txtfld_component_edit_regexp_pattern.get();
    
    txtfld_component_edit_decimal.setProperties({v_on: exp == 'decimal', m_on: exp == 'decimal', value:'', description:''});
    txtfld_component_edit_range_i.setProperties({v_on: exp == 'range', m_on: exp == 'range', value:'', description:''});
    txtfld_component_edit_range_f.setProperties({v_on: exp == 'range', m_on: exp == 'range', value:'', description:''});
    txtfld_component_edit_nchar_n.setProperties({v_on: exp == 'nchar', m_on: exp == 'nchar', value:'', description:''});
    txtfld_component_edit_nchar_m.setProperties({v_on: exp == 'nchar', value:'', description:''});
    txtfld_component_edit_not_s.setProperties({v_on: exp == 'not', m_on: exp == 'not', value:'', description:''});
    txtfld_component_edit_regexp_pattern.setProperties({v_on: exp == 'regexp', m_on: exp == 'regexp', value:'', description:''});
    
    switch(exp) {
        case 'decimal': exp += '_'+dec; break;
        case 'range': exp += '_'+ri+'_'+rf; break;
        case 'nchar': exp += '_'+ncn+(ncm?'_'+ncm:''); break;
        case 'not': exp += '_'+not; break;
        case 'regexp': exp += '_'+rex; break;
        default: break;
    }
    
    mdl_component_edit.data.checkExp = exp;
};
moLibrary.txtfld_component_edit_decimal_onchange = function (o) {mdl_component_edit.data.checkExp = 'decimal_'+o.get('int');};
moLibrary.txtfld_component_edit_range_i_onchange = function (o) {mdl_component_edit.data.checkExp = 'range_'+o.get('int')+'_'+txtfld_component_edit_range_f.get('int'); };
moLibrary.txtfld_component_edit_range_f_onchange = function (o) {mdl_component_edit.data.checkExp = 'range_'+txtfld_component_edit_range_i.get('int')+'_'+o.get('int'); };
moLibrary.txtfld_component_edit_nchar_n_onchange = function (o) {mdl_component_edit.data.checkExp = 'nchar_'+o.get('int')+(txtfld_component_edit_nchar_m.get('int')?'_'+txtfld_component_edit_nchar_m.get('int'):''); };
moLibrary.txtfld_component_edit_nchar_m_onchange = function (o) {mdl_component_edit.data.checkExp = 'nchar_'+txtfld_component_edit_nchar_n.get('int')+'_'+o.get('int'); };
moLibrary.txtfld_component_edit_not_s_onchange = function (o) {mdl_component_edit.data.checkExp = 'not_'+o.get(''); };
moLibrary.txtfld_component_edit_regexp_pattern_onchange = function (o) {mdl_component_edit.data.checkExp = 'regexp_'+o.get(''); };
moLibrary.slct_component_edit_picktype_onchange = function (o) {
    var res = o.getItem();
    mdl_component_edit.data.table = res.table;
    mdl_component_edit.data.ftype = res.ftype;
    moLibrary.control_is_parent();
};

moLibrary.chkbx_component_edit_isparent_onchange = function (o){moLibrary.control_is_parent();}
moLibrary.txtfld_component_edit_instance_name_onchange = function(o){moLibrary.control_is_parent();}
moLibrary.control_is_parent = function (){
    if((chkbx_component_edit_isparent.get('int') == 1) && (slct_component_edit_picktype.get() != undefined) && (txtfld_component_edit_instance_name.get() != '')){
        if((txtfld_component_edit_instance_name.get()).split('_')[1] != slct_component_edit_picktype.get()){
            moGlb.alert.show(moGlb.langTranslate("For check 'pick a parent' module's title must equal pick type"));
            chkbx_component_edit_isparent.set(0);
        }
    }
}

moLibrary.txtfld_component_edit_label_onchange = function (o) { if(o.get() != '' && txtfld_component_edit_info.get() == '') txtfld_component_edit_info.set(o.get()); };

moLibrary.slct_component_edit_db_type_onchange = function (o){ 
    txtfld_component_edit_db_column_length.setProperties({m_on: ((o.get() == 'VARCHAR')&& (slct_component_edit_db_table.value != ''))});
}
moLibrary.slct_component_edit_db_type_onload = function (o){
    txtfld_component_edit_db_column_length.setProperties({m_on: ((o.description == 'VARCHAR') && (slct_component_edit_db_table.value != ''))});
}

moLibrary.slct_component_edit_db_table_onchange = function (o){ 
    txtfld_component_edit_db_column_length.setProperties({m_on: ((slct_component_edit_db_type.get() == 'VARCHAR')&& (o.value != ''))});
}

moLibrary.mdl_component_edit_save = function (data){
    var is_selected = false;
    if(data.f_type_id == 7){
        if((data.options).length > 0){
            for(var a = 0; a < (data.options).length; a++){
                if(data.options[a].selected == true && is_selected) return "Must be only one default selected option.";
                else if(data.options[a].selected == true && !is_selected)is_selected = true;
            }
        }
    }

    return true;
}