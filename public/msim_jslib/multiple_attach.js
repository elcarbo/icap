// attach multiple


moMultipleAttach={ J:null, atch:[], SEL:-1, ZEL:-1,
         SPA:"style='position:absolute;",
         OFL:[], old_atch:[],
         REQ:0, REFDOC:[], win: null, callback: '',
         imgPath: moGlb.baseUrl.replace("/admin", "")+"public/msim_images/default",

Start:function(container, req, refDoc, clbk){
with(moMultipleAttach){
  var r,str="",ftc,fc;
  
  REFDOC=refDoc;
  REQ=req;
  old_atch=CloneA(REFDOC);
  callback = clbk;
  atch=[];
  for(r=0;r<REFDOC.length;r++) {
    fc=REFDOC[r].f_code; 
    ftc=(!ftc) ? REFDOC[r].f_temp_code : null; 
    atch.push({ name:REFDOC[r].name, f_code:fc, f_temp_code:ftc });  
  }
  
  //J=mf$(container);
  
  str+="<div "+SPA+"left:10px;top:0;right:10px;bottom:11px;'>"+
      
   "<input id='idfocusOdlAttach' "+SPA+"left:20px;top:20px;width:10px;height:20px;' onblur='moMultipleAttach.f_evid_blur()'>"+
  
   "<img src='"+moMultipleAttach.imgPath+"/components/textfield/attach.png' "+SPA+"right:2px;top:14px;cursor:pointer;' title='Add file attachment list.' onclick='mf$(\"filesToUpload\").click()'>"+
   "<img src='"+moMultipleAttach.imgPath+"/buttons/cancel.png' "+SPA+"right:2px;top:52px;cursor:pointer;' title='Delete selected attachment' onmousedown='moMultipleAttach.f_sub_btn();' >"+
  
  
  "<iframe name='nmfrmattach' id='nmfrmattach' style='display:none;'></iframe>"+
  "<form name='frmattach' action='' method='post' target='nmfrmattach' enctype='multipart/form-data' >"+       
  
  "<div "+SPA+"right:0px;top:10px;width:28px;height:24px;overflow:hidden;z-index:100;opacity:0.0;cursor:pointer;'>"+
  "<input name='filesToUpload[]' id='filesToUpload' type='file' multiple onchange='moMultipleAttach.loadUpldFiles()' "+SPA+"left:-130px;top:0;z-index:100;' />"+
  "</div>"+
  "</form>"+

   
  "<div class='clattachdiv' id='idAttachDoc' "+SPA+"left:0px;top:12px;right:28px;height:50px;overflow:auto;background-color:#FFF;'></div>"+
         
  "<button "+SPA+"left:0;bottom:0;width:70px;font:11px Arial;cursor:pointer;' onclick='moMultipleAttach.Cancel()'>" + moGlb.langTranslate("Cancel") + "</button>"+
  "<button "+SPA+"right:0;bottom:0;width:70px;font:11px Arial;cursor:pointer;' onclick='moMultipleAttach.Send()'>" + moGlb.langTranslate("Attach") + "</button>"+
         
  "</div>";
        

        if(!mf$(container)) {
            J = moGlb.createElement(container, 'div', '', true);
            win = moComp.createComponent("multiple_attach_win", "moPopup", {contentid: container, title: moGlb.langTranslate('Multiple attach')});        
        } else J=mf$(container);

        J.innerHTML=str;

f_attach_to_buttons(); // create button for attach
win.show();

}},

loadUpldFiles:function() {
with(moMultipleAttach){
  
  //mf$("idloadbox").style.display="block";
  moGlb.loader.show(moGlb.langTranslate("Loading, please wait..."));
  document.forms["frmattach"].action="uploads/multiple-uploads?request="+REQ+"&rnd="+parseInt(10000*(Math.random()));  
  //document.forms["frmattach"].action="uploads.php?request="+REQ+"&rnd="+parseInt(10000*(Math.random()));
  document.forms["frmattach"].submit();
  
}},


retUpldFiles:function(v) {
with(moMultipleAttach){
  
  //mf$("idloadbox").style.display="none";
  moGlb.loader.hide();
  
  var r, jsn=JSON.parse(v);
  for(r=0;r<jsn.length;r++){
    atch.push({ "name":jsn[r].name, "f_code":jsn[r].f_code, "f_temp_code":jsn[r].f_temp_code  }); 
  }

  f_attach_to_buttons();
}},





f_attach_to_buttons:function(){   // m:  0=odl attach   1=other attach
with(moMultipleAttach){
  SEL=-1;
  var r,s="";
  for(r=0;r<atch.length;r++) s+=f_add_btn( atch[r].name, r);
  mf$("idAttachDoc").innerHTML=s;
}},


 


f_add_btn:function(tx,i){
with(moMultipleAttach){

  var s="<span id='idatch"+i+"' style='font:bold 11px Arial;color:#888;margin:2px 8px 2px 0;"+
        "text-decoration:underline;cursor:pointer;' onclick='moMultipleAttach.f_evid("+i+")' > "+tx+" </span>";
  return s;
}},



f_evid:function(i){
with(moMultipleAttach){

  var j=mf$("idatch"+i);
  j.style.backgroundColor="#CCC";
 
  mf$("idfocusOdlAttach").focus();
  SEL=i;
}},



f_evid_blur:function(){
with(moMultipleAttach){
  if(SEL==-1) return;
  mf$("idatch"+SEL).style.backgroundColor="#FFF";
  SEL=-1;
}},




f_sub_btn:function(){
with(moMultipleAttach){
  if(SEL==-1) return;
     atch.splice(SEL,1);
   f_attach_to_buttons();
   SEL=-1;  
}},



CloneA:function(a) {
with(moMultipleAttach){
        if(!a) return;
        var cln = (typeof a.length == "undefined" ? {} : []);
        for(var i in a) {
            if(typeof a[i]=="object") cln[i]=CloneA(a[i]);
            else cln[i]=a[i];
        }
        return cln;
}},


//------------------------------------------------------------------------------

Send:function(){
with(moMultipleAttach){
  
  var r;

  for(r=0;r<old_atch.length;r++) REFDOC.pop();
  for(r=0;r<atch.length;r++) REFDOC.push(atch[r]);

  Close();
}},



Cancel:function(){
with(moMultipleAttach){
  Close();
}},


Close:function(){
with(moMultipleAttach){
  J.innerHTML="";
//  var pj=J.parentNode;
//  pj.style.display="none";
if(callback) callback(REFDOC);
win.hide();
}}


}  //