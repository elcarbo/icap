var moModuleTimeLine = function () {
    this.noheader = true, this.editMode = 1, this.dataMode = 0, this.noscroll = true;
    var tlName = "";
    this.modResize = function (l0, t0, w0, h0, layoutid, anim) {
        with(this) {
            if(!tlName) {
                tlName = name+"_tl", tgName = tlName.multiReplace({tl: "tg"});
                eval(tlName+" = new moTimeline('"+tlName+"', {modTG: '"+tgName+"', EditMode: "+editMode+", DataMode: "+dataMode+"});");
            }
            baseResize(l0, t0, w0, h0, layoutid);
        
            var mbpos = moGlb.getPosition(mbody);
            eval(tlName+".resize"+(anim ? "_animation" : "")+"(0, 0, "+(mbpos.w-borderWidth*2)+", "+(mbpos.h-borderWidth*2)+", '"+mbody.id+"');");            
        }
    };
    this.modResizeAnimation = function (l, t, w, h, layoutid) {this.modResize(l, t, w, h, layoutid, true);};
    //this.refresh = function (fc) {console.log(this.name+": "+fc); if(this.active) eval(tlName).retoLD(fc); };
};
moModuleTimeLine.prototype = new moModule();