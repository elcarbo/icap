var moModuleSelector = function () {
    this.slcName = '', this.stype = 0, this.orderby = 'f_title', this.order = 'ASC', this.smode = 0, this.hidemenu = 0, this.noscroll = true, this.lighter = true;
    var defaultLabel = 'Selector', init = true;
    this.create = function (props) {
        with(this) {
            if(moGlb.isempty(label)) label = defaultLabel;
            baseCreate(props);
            slcName = name+'_slc';
            new moComp.createComponent(slcName, 'moSelector', {
                f_type_id: stype, orderby: orderby, order: order, ModSel: smode, noMenuMode: hidemenu, mod: name
            }, true);
            ReverseAjax.register(name, "t_selectors", stype);
        }
    };
    this.refresh = function () { };
    this.reset = function () { eval(this.slcName).Reset(); };
    this.modResize = function (l, t, w, h, layoutid, anim) {
        this.baseResize(l, t, w, h, layoutid);
        with(this) { if(init) { eval(slcName).Start(mbody.id); init = false; } else eval(slcName).Resize(); }
    };
    this.modOut = function () { return eval(this.slcName).GetSelected().Selected; };
    this.setMode = function (mode) { this.smode = mode; };
    // Event dispatcher
    this.sender = function () {with(this) {EventDispatcher.send(name, "selector", {hierarchy: smode > 1?1:0, selectors: modOut()});}};
    this.setInheritedLabel = function (lbl) { with(this) { if(!label && lbl) setLabel(lbl); } };
};

moModuleSelector.prototype = new moModule();