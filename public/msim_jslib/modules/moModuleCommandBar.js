var moModuleCommandBar = function () {
    this.elements = "", this.bpadding = 3, this.minsz = 25, this.padding = 0, this.borderWidth = 0,
    this.noheader = true, this.noframe = true, this.noscroll = true, this.margin = {t: 4, l: 4, r: 4, b: 0};
    this.draw = function () {
        with(this) {
            mbody.style.backgroundColor = "transparent";
            mbody_frame.style.backgroundColor = "transparent";
            var fpos = moGlb.getPosition(mbody);
            
            // box blank if closed
            var bj = moGlb.createElement("id_blankbox"+name, "div", mbody);
            if(fpos.w < minsz + margin.l) {
                with(bj.style){
                    left = 0; top = 0; width = "28px"; height = "30px";
                    backgroundColor = "#FFF";
                    display = "block";
                    zIndex = 1000;
                }
            } else bj.style.display = "none";
            
            if(moGlb.isempty(elements)) return;
            var el = null, t_els = elements.split("|"), els = {left: (moGlb.isset(t_els[0]) ? t_els[0].split(","): [] ), right: (moGlb.isset(t_els[1]) ? t_els[1].split(","): [] )};

            with(els) {
                if(moGlb.isset(left) && left.length > 0) {
                    for(var i in left) {
                        if(eval(left[i]).isVisible()) {
                            var mbpos = moGlb.getPosition(mbody), pos = el ? el.getPosition() : {l: mbpos.l-padding, w: -bpadding};
                            try {el = eval(left[i]);el.display(pos.l-mbpos.l+pos.w+bpadding, 0, mbody.id);
                            } catch(ex) {moDebug.log("Element "+left[i]+" doesn't exist");}
                        }
                    }
                }

                if(moGlb.isset(right) && right.length > 0) {
                    var eleft = moGlb.getPosition(mbody).w+bpadding;
                    for(var i=right.length-1; i>=0; i--) {
                        if(eval(right[i]).isVisible()) {
                            try {                            
                                el = eval(right[i]);
                                eleft += -(bpadding+(el.props ? el.props.w : el.w));
                                el.display(eleft, 0, mbody.id);
                            } catch(ex) { moDebug.log("Element "+right[i]+" doesn't exist"); }
                        }
                    }
                }
            }
        }
    };
    this.getElements = function () {
        with(this) {
            if(!moGlb.isempty(elements)) return elements.multiReplace({"|": ","}).split(",");
            return [];
        }
    };
};
moModuleCommandBar.prototype = new moModule();