var moModuleEdit = function () {
    this.tabbar = "", this.commandsl = "", this.etype = "", this.componentm = [], this.unique = '', this.askConfirm = false, this.commands = '',
    this.defaultLabel = moGlb.langTranslate('Code')+" {fc_progress} - "+moGlb.langTranslate('Title')+" {f_title} - "+moGlb.langTranslate('Phase')+" {f_phase_id}",
    this.tContainer = "", // Tab container
    this.aContainer = "", // Actions container (print, message, ...)
    this.lContainer = "", // Layout container
    this.iContainer = "", // Info container (edit no. or new)
    this.cContainer = "", // Commands container (cancel, change phase, save)
    this.actions = {}, this.icw = 2, this.table = "", this.cheight = 34, this.noscroll = true, this.editability = -1,
    this.buttons = [], this.data = {f_code: 0}, this.isBatch = false;
    var f_type_id = 0, phaseFieldId = '', inprogress = [], tgn = 0;
    this.create = function (props) {
        with(this) {
            baseCreate(props);
            phaseFieldId = 'slct_'+name.multiReplace({'mdl_': '', '_edit': ''})+'_phases';
            try { eval(phaseFieldId).setProperties({v_value: [{f_codes: "!=''"}]}); } catch(ex) {} // hide phases when batch
            if(!tabbar) tabbar = name.replace('mdl', 'tbbr');
            if(!commandsl) commandsl = (name.replace('mdl', 'lyt'))+"_cb";
            if(!commands) {
                var mod = name.multiReplace({'mdl_': '', '_edit': ''});
                commands = 'btn_'+mod+'_cancel|slct_'+mod+'_phases,btn_'+mod+'_'+(mod.split('_').length > 1?'set':'save');
            }
            try { // Creating commands layout
                moComp.createComponent(commandsl, "moLayout", {layout:[{idp:-1,ov:0,tp:0,wh:36,level:-1,module:"",lock:0,minv:36,acc:0},{idp:0,ov:1,tp:0,wh:36,level:-1,module:name+"_cb_bl",lock:0,minv:36,acc:0}]}, true);
            } catch(ex) { moDebug.log("Problems during loading "+commandsl+": check it - "+ex); }
            try {
                moComp.createComponent(name+"_cb_bl", 'moModuleCommandBar', {type:"moModuleCommandBar",noshadow:true,elements:commands}, true);
            } catch(ex) { moDebug.log("Problems during loading "+name+"_cb_bl"+": check it - "+ex); }
            enableSave(name.split('_')[1] == 'up');
        }
    }; 
    this.draw = function (idata) {
        with(this) {
            if(!mbody) return;
            var mbpos = moGlb.getPosition(mbody);
            //iContainer = moGlb.createElement(name+"_icntnr", "div", mbody);
            
            tContainer = moGlb.createElement(name+"_tbcntnr", "div", mbody);
            aContainer = moGlb.createElement(name+"_actcntnr", "div", mbody);
            lContainer = moGlb.createElement(name+"_lytcntnr", "div", mbody);
            cContainer = moGlb.createElement(name+"_ccntnr", "div", mbody);


            aContainer.setAttribute("style", "top:"+padding+"px;right:"+padding+"px;z-index:"+moGlb.zmodule+2+";");
            aContainer.setAttribute("class", "aContainer");

            var tb = eval(tabbar);
            tContainer.setAttribute("style", "position:absolute;top:"+padding+"px;left:"+(padding-tb.tabL+icw)+"px;height:"+tb.tabH+"px;width:"+(mbpos.w-padding*2-icw-moGlb.getPosition(aContainer).w)+"px;z-index:"+moGlb.zmodule+2+";");
            //iContainer.setAttribute("style", "position:absolute;top:"+padding+"px;left:"+padding+"px;height:"+tb.tabH+"px;width:"+icw+"px;z-index:"+moGlb.zmodule+2+";padding-top:4px;padding-left:3px;font-weight:bold;");
            //iContainer.innerHTML = "New";

            var tpos = moGlb.getPosition(tContainer);
            lContainer.setAttribute("class", "lContainer");
            lContainer.setAttribute("style", "top:"+(padding+tpos.h-1)+"px;left:"+padding+"px;width:"+(mbpos.w-padding*2-borderWidth*4)+"px;height:"+(mbpos.h-padding*2-tpos.h-borderWidth*4-cheight)+"px;z-index:"+moGlb.zmodule+1+";");

            var lpos = moGlb.getPosition(lContainer);
            cContainer.setAttribute("style", "position:absolute;top:"+(padding+lpos.h+lpos.t-mbpos.t)+"px;left:"+padding+"px;width:"+(mbpos.w-padding*2-borderWidth*4)+"px;height:"+cheight+"px;z-index:"+moGlb.zmodule+1+";");
            eval(commandsl).display(true, name+"_ccntnr");

            tb.pname = name;
            tb.display(tContainer.id, lContainer.id);
            tb.resize();

            // get components sub modules
            //if(moGlb.isempty(componentm)) setSubtab();
        }
    };
    this.setSubtab = function () {
        with(this) {
        
        try{ 
        
          if(this.name=="mdl_wo_edit"){
            DBL.query("TRUNCATE t_mdl_wo_prl1_tg_tg");       // pricelist clear tables
            DBL.query("TRUNCATE t_mdl_wo_prl2_tg_tg");
            mdl_wo_prl1_tg_tg.JStore={};
            mdl_wo_prl2_tg_tg.JStore={};
            
          }
           
         //  console.log("try"+Math.random()+" | "+this.name );
           
        }catch(e){}
        
            //console.log(mdl_component_edit.data);
            //data = mdl_component_edit.data;
            componentm = [], tgn = 0;
            var list = eval(tabbar).getLayoutList(moInit.admin);
            for(var i in list) {
                for(var j in eval(list[i]).LY) {
                    var md = eval(list[i]).LY[j].module;
                    if(md) {
                        try {
                            eval(md).pname = name;
                            //if(eval(md).type == "moModuleComponent" || eval(md).type == "moModuleTextAdminLayout" || eval(md).type == "moModuleTextAdminTabbar" || eval(md).type == "moModuleTextAdminSelectOptions" || eval(md).type == "moModuleTextAdminCheckbox") componentm.push(md);
                            if(eval(md).type == "moModuleComponent" || eval(md).type == "moModuleFrame" || eval(md).type.indexOf("moModuleAdmin") == 0) componentm.push(md);
                            else if(eval(md).type == "moModuleCrud") {
                                for(var k in eval(eval(md).innerLayout).LY) {
                                    if(eval(eval(md).innerLayout).LY[k].module.indexOf("tg") >= 0) {
                                        var mod = eval(eval(md).innerLayout).LY[k].module
                                        componentm.push(mod);
                                        if(mod.indexOf('_tg') > 0) tgn++;
                                    }
                                }
                            }
                        } catch(ex) {}
                    }
                }
            }
            
        }
    };
    
    this.prepPrint = function (a) {

      setTimeout(function(){ eval(this.name+".print()"); },1200);
    
    };
    
    this.print = function (params,sub_tg) {
        if(typeof(sub_tg) == "undefined") sub_tg = 0;
        
        with(this) {     

            if(sub_tg == 1){
                var change = pname.replace("crud","tg_tg");
                var codess = eval(change).arrCHK;
            }
            if (moGlb.isset(params.newPrint)) {
                 var url = moGlb.baseUrl+"/pdf/print-pdf/f_code/"; 
                 var codes = ((sub_tg == 1) ? codess : eval(eval(pname).treeGrid).getSelectedIds());
                 window.open(url+codes[0], '_blank');
                 return;
            }
            if(!moGlb.isset(params.script_name)) return;
            
            // console.log(params); console.log(sub_tg);
 
            var url = moGlb.baseUrl+"/pdf-creator/print-pdf/f_code/", codes = ((sub_tg == 1) ? codess : eval(eval(pname).treeGrid).getSelectedIds()), prs = '';
          
            if(moGlb.isset(params.multiple) && params.multiple) {
            
                delete(params.multiple);
            
                if((!moGlb.isset(params.nocode) || !params.nocode) && codes.length == 0) {
                   
                    if(!moGlb.isset(params.filter) || !params.filter) { 
                    
                      moGlb.alert.show(moGlb.langTranslate("Select items to print first")); 
                      return false; 
                    
                    } else {
                      
                      Ajax.sendAjaxPost(moGlb.baseUrl+"/pdf-creator/pdf-params", "filter="+JSON.stringify(eval(name.replace('edit', 'tg')).getFilters()), name+".prepPrint", true);
                    }
                }//modificato matteo 10 ottobre 2016
                if((codes.length > 150 && sub_tg != 1) || (codes.length > 10 && sub_tg == 1)) { moGlb.alert.show(moGlb.langTranslate("Too many items to print (max "+ ((sub_tg == 1) ? '10' : '150') +")")); return; }
            
            } else codes = ((sub_tg == 1) ? codess : [ data.f_code ]);
           
           
            for(var i in params) prs += "/"+i+"/"+params[i];
            if(codes.length > 0) {
                if(moGlb.isset(params.onesheet) && params.onesheet) { delete(params.onesheet); window.open(url+codes.join(',')+prs, '_blank'); }
                else for(var i in codes) {
                    //window.open(url+codes[i]+prs, "Pdf "+codes[i], "width="+(moGlb.getClientWidth()-100)+", height="+(moGlb.getClientHeight()-20));
                    window.open(url+codes[i]+prs, '_blank');
                }
            } else window.open(url+"0"+prs, '_blank');
        }
    };
    this.sendEmail = function (o, params) {
        with(this) {
            if(!isNaN(parseInt(o))) { // First step
                if(!moGlb.isset(params)) params = {};
                params.action = o; params.f_code = getEditData('f_code');
                moGlb.loader.show();
                Ajax.sendAjaxPost(moGlb.baseUrl+"/mail/send-custom-mail", "params="+JSON.stringify(params), name+".sendEmail", true);
            } else { // Second step
                try {
                    o = JSON.parse(o);
                    if(moGlb.isset(o.message)) moGlb.alert.show(moGlb.langTranslate(o.message));
                } catch(ex) {
                    moGlb.alert.show(moGlb.langTranslate("Error in parsing send email response"));
                }
                moGlb.loader.hide();                
            }
        }
    };
    this.setData = function (idata,pnm) {
        with(this) {
            inprogress = [];
            //data = mdl_component_edit.data;
          // if(moInit.admin) moComponent_adm.Edit(mdl_component_edit.data.options,mbody.id);
            if(!moGlb.isempty(idata) && (idata.f_code <= 0 || idata.f_code != data.f_code)) {
                isBatch = moGlb.isset(idata.f_codes); data = idata; editability = moGlb.isset(data.f_editability)?data.f_editability:-1; delete(data.f_editability);
                if(!isBatch) {  // Phases
                    if(!moGlb.isset(data.f_phase_id)) data.f_phase_id = "";
                    try { eval(phaseFieldId).getOptions("workflows/wf-phase-exit", "f_wf_id="+data.f_wf_id+"&f_phase_id="+data.f_phase_id); } catch(ex) { }
                }
                var v_params = {};
                if(moGlb.isset(data.f_type_id)) v_params.category = data.f_type_id.toString(); else v_params.category = "";
                if(moGlb.isset(data.t_selectors)) v_params.t_selector = data.t_selectors.toString(); else v_params.t_selector = "";
                if(moGlb.isset(data.f_wf_id) && moGlb.isset(data.f_phase_id)) v_params.wf = data.f_wf_id+"_"+data.f_phase_id; else v_params.wf = "";                
                eval(tabbar).setVisibility(v_params);
                setSubtab();
                
               //if(pnm=="mdl_bkm_eng_crud") alert(JSON.stringify(data)); 
                
                for(var i in componentm) { try { eval(componentm[i]).setData(data,pnm); } catch(ex) { /*moDebug.log(componentm[i]+" error - "+ex);*/ } }
                setLabel(data.f_code == 0?'':label);
                checkCompleteness();
            }
        }
    };
    /**
     * Add custom key value pair data.
     * @param k string
     * @param v mixed
     */
    this.addCustomData = function(k, v) {
        with(this) {
            data[k] = v;
        }
    }
    
    
    //NICO: creata per aggiungere param data[] alle pck dal multiedit 
    this.addPckData = function (code, tbl, ftp, picktype) {
    	with (this) {
            try {
                addCrossData(code, tbl, ftp, picktype);
                data[tbl+"_"+ftp].pck = true;
            } catch(e) {
                
            }
    	}
    }
    
    
    /**
     * Create a cross with the code in input.
     * @param int code
     * @param string tbl
     * @param int ftp
     */
    this.addCrossData = function (code, tbl, ftp, pick_type) {
        with(this) {
            var module_name = name.replace('edit', 'tg'), f_code = [];
            
            // check if exists tab and delete its elements
            if(code == ""){
                var aux = name.split('_');
                try{
                    mdl = eval('mdl_' + aux[1] + '_' + pick_type + '_tg_tg');
                    for(el in mdl.JStore){
                        delete mdl.rifY[el];
                        delete mdl.JStore[el];
                    }
                    mdl.Ypos = [];
                }
                catch(e){
                    data[tbl+"_"+ftp] = {
                        f_code : [],
                        f_pair : [],
                        f_type : ftp,
                        Ttable : tbl
                    };
                }
            }
            else{
                if(data[tbl+"_"+ftp] != undefined){
                	var index = data[tbl+"_"+ftp].f_code.indexOf(code);
                	if (!(index > -1)) {
              			data[tbl+"_"+ftp].f_code.push(code);
            		}
                    
                }
                else{
                    if(code) f_code = [ code ];
                    data[tbl+"_"+ftp] = {
                        f_code: f_code, 
//                        f_pair: [{ f_code: f_code, f_code_main: data.f_code, f_type: ftp, Ttable: tbl, f_module_name: module_name}],
                        f_code_main: data.f_code, f_type: ftp, Ttable: tbl, pairCross: 0, f_module_name: module_name
                    };
                }
            }

        }
    };
    
    /**
     * Remove a cross with the code in input.
     * @param int code
     * @param string tbl
     * @param int ftp
     */
    this.removeCrossData = function (code, tbl, ftp, picktype) {
        with(this) {
            var module_name = this.name.replace('edit', 'tg'), f_code = [];
            var aux = this.name.split('_');
         
            try{
                if (picktype.indexOf('slc') != -1) {
                    var split = picktype.split("_");
                    var mdl = eval('mdl_' + aux[1] + '_' + split[0] + '_tg_tg');
                }
                else{
                    var mdl = eval('mdl_' + aux[1] + '_' + picktype + '_tg_tg');
                }
                delete mdl.rifY[code];
                delete(mdl.JStore[code]);
                // delete from Ypos array
                for(var i = 0; i < mdl.Ypos.length; i++){
                    if(mdl.Ypos[i].fcode == code){
                        mdl.Ypos.splice(i, 1);
                        break;
                    }
                }
                mdl.updateMenuNewJob();
                mdl.draw2();
            }
            catch(e){ 
            }
            
            var index = data[tbl+"_"+ftp].f_code.indexOf(code);
            if (index > -1) {
              data[tbl+"_"+ftp].f_code.splice(index, 1);
            }
        }
    };
    
    //NICO: creata per eliminare data[] delle pck dal multiedit 
    this.removePckData = function (code, tbl, ftp, picktype) {
    	with (this) {
            try {
                removeCrossData(code, tbl, ftp, picktype);
                delete data[tbl+"_"+ftp];
            } catch(e) {
                
            }
    	}
    }
    
    this.getDataRaw = function() {
        return this.data;
    };
    this.getEditData = function (k) { return this.data[k]; };
    this.setEditData = function (k, v) { with(this) { if(moGlb.isset(data[k])){data[k] = v;}else if(data['f_code'] == 0){data[k] = v;} } };
    this.save = function () {
        with(this) {
            var tp = name.split('_')[1], mnd = [], err = [], attachments = [];            
            data.f_module_name = (tp=='up'?'mdl_usr_tg':eval(pname+".treeGrid")); if(!moInit.admin) data.unique = []; data.attachfields = [];
            //editability = data.f_editability; delete(data.f_editability);
            if(tp == 'up') data.f_code = moGlb.user.code;
            var d = [], url = moGlb.baseUrl+"/"+etype+"/"+(data.f_code_main ? "save-pair-cross" : (data.f_code != 0 ? "edit"+(moInit.admin || data.f_code>0 ?"":"-batch") : "new"));
            for(var i in componentm) {
                try {                    
                    if(moGlb.isset(eval(componentm[i]).attach) && eval(componentm[i]).attach) attachfields.push(eval(componentm[i]).bind);
                    var values = eval(componentm[i]).getData();
                    if(values) {
                        if(!moGlb.isempty(values.mandatory)) mnd = mnd.concat(values.mandatory);
                        if(!moGlb.isempty(values.error)) err = err.concat(values.error);
                        if(!moGlb.isempty(values.attachments)) attachments = attachments.concat(values.attachments);
                        //moGlb.alert.show(moGlb.langTranslate("Cannot save: set all the mandatory fields and check the values of all the constrained fields"));return;
                        
                        //NICO multiedit: controllo se data ha già un qualche valore (c'è una nuova pck)
                        if (!(data.f_code > 0) && isBatch) {
                            for(var ii in data) {
                                for (var kk in values) {
                                    if (ii == kk) {
                                        values.kk.pck = data.ii.pck;
                                    }
                                }
                            }    
                        }   
                        d.push(values);
                    }
                    try {
                        if(!moGlb.isempty(unique)) data.unique.push(unique);
                        var unique_list = eval(componentm[i]).getUnique();
                        if(!moGlb.isempty(unique_list)) data.unique = data.unique.concat(unique_list);
                    } catch(ex) {moDebug.log(ex);}                    
                } catch(ex) { moDebug.log(ex); }
            }
            var msg = '';
            if(!moGlb.isempty(mnd)) msg += moGlb.langTranslate("The following fields have to be filled before saving: ")+mnd.join(', ');
            if(!moGlb.isempty(err)) msg += (msg!=''?'\n':'')+moGlb.langTranslate("The following fields have to be corrected before saving: ")+err.join(', ');
            if(msg) { moGlb.alert.show(moGlb.langTranslate(msg)); return; }
            //try{if(data.f_code > 0) data.f_phase_id = eval(phaseFieldId).get("int");} catch(ex) {}
            for(var i in d) for(var j in d[i]) data[j] = d[i][j];
            var cansave = (moGlb.isset(moLibrary[name+"_save"]) ? moLibrary[name+"_save"](data) : true);
            if(cansave === true) {
                var params = data;
                if(moGlb.isset(data[""])) delete(data[""]);
                if(moInit.admin) { // Administrator
                    params = { f_code: data.f_code, f_type_id: data.f_type_id, f_instance_name: data.f_instance_name, f_module_name: eval(pname+".treeGrid") };
                    delete(data.fpdr); delete(data.nch); delete(data.selected); delete(data.fpos); delete(data.rtree); delete(data.f_code);
                    delete(data.f_instance_name); delete(data.f_visibility); delete(data.f_type_id); delete(data.isedit); delete(data.f_phase_id);
                    delete(data.f_module_name); /*delete(data.unique);*/ delete(data.mandatory); delete(data.error);                    
                    if(data.f_properties) params.f_properties = data.f_properties;
                    else params.f_properties = JSON.stringify(data);
                }
                try{if(data.f_code > 0) data.f_phase_id = eval(phaseFieldId).get("int");} catch(ex) {}

				// add forced PM to params
				if(moLibrary.forcedPM !== null){
					for(var i = 0; i < moLibrary.forcedPM.length; i++){
						var found = false;
						for(j = 0; j < params.t_workorders_4.f_code.length; j++){
							if(moLibrary.forcedPM[i] == params.t_workorders_4.f_code[j]){
								found = true;
								break;
							}
						}
						if(found == false){
							params.t_workorders_4.f_code.push(moLibrary.forcedPM[i]);
						}
					}
				}
                if(params.t_wares_parent_1) {
                    if(params.t_wares_parent_1.f_module_name && params.t_wares_parent_1.f_code) {
                        if(params.t_wares_parent_1.f_module_name == 'mdl_asset_parent_tg' && params.t_wares_parent_1.f_code.length > 1) {
                            moGlb.alert.show("Non e' ammessa parentela multipla per asset selezionati!");
                            return;
                        }
                    }
                }
                Ajax.sendAjaxPost(url, "params="+JSON.stringify(params), name+".result", true);
                moGlb.loader.show();
            } else moGlb.alert.show(moGlb.langTranslate(cansave));
        }
    };
    this.setPairCross = function () {
        with(this) {
            var mnd = [], err = [];
            if(!moGlb.isset(moLibrary[name+"_save"]) || moLibrary[name+"_save"](data)) {
                var d = [];
                for(var i in componentm) {
                    //try {d.push(eval(componentm[i]).getData());} catch(ex) {moDebug.log(ex);}
                    try {
                        var values = eval(componentm[i]).getData();
                        values.usr_level = moGlb.user.level;
                        if(!moGlb.isempty(values.mandatory)) mnd = mnd.concat(values.mandatory);
                        if(!moGlb.isempty(values.error)) err = err.concat(values.error);
                        var msg = '';
                        if(!moGlb.isempty(mnd)) msg += moGlb.langTranslate("The following fields have to be filled before saving: ")+mnd.join(', ');
                        if(!moGlb.isempty(err)) msg += (msg!=''?'\n':'')+moGlb.langTranslate("The following fields have to be corrected before saving: ")+err.join(', ');
                        if(msg) { moGlb.alert.show(moGlb.langTranslate(msg)); return; }
                        //if(values == false) {moGlb.alert.show(moGlb.langTranslate("Cannot save: set all the mandatory fields and check the values of all the constrained fields"));return;}
                        d.push(values);
                        try {
                            if(!moGlb.isempty(unique)) data.unique.push(unique);
                            var unique_list = eval(componentm[i]).getUnique();
                            if(!moGlb.isempty(unique_list)) data.unique = data.unique.concat(unique_list);
                        } catch(ex) { moDebug.log(ex); }
                    } catch(ex) { moDebug.log(ex); }
                }
                for(var i in d) for(var j in d[i]) data[j] = d[i][j];
                eval(eval(pname+".treeGrid")).setPairCross(data); cancel(true);
            }
        }
    };
    this.cancel = function (fromsave, canc) {
        with(this) {
            if(moGlb.isset(canc)) {
                var treeGridName = eval(pname+".treeGrid");/*, timeLineName = eval(pname+".timeLine");*/
                editability = -1; reset();
                eval(pname).closeEdit();
                eval(treeGridName).closeEdit(isBatch); checkCompleteness(); enableSave(false);
                eval(tabbar).reset();
            } else if(fromsave || !moGlb.isset(data.f_type_id) || !askConfirm) {
                cancel(fromsave, 1);
            } else if(askConfirm) {
                moGlb.confirm.show(moGlb.langTranslate("Are you sure you want to exit without saving?"), function () {      
                    cancel(fromsave, 1);
                });
            }
            if(name.split("_").length == 3){
                moGlb.lockEdit = false; // not cross nor pair cross
                moGlb.changeLockedElement(false,name);
            }
        }
		
		moLibrary.forcedPM = null;
    };
    this.reset = function () {
        with(this) {
            data = {f_code: 0}; isBatch = false; inprogress = [];
            try { eval(phaseFieldId).reset(true); } catch(ex) {}
            try { for(var i in componentm) eval(componentm[i]).reset(); setLabel(); } catch(ex) {/*moDebug.log("Resetting "+componentm[i]+": "+ex);*/}            
        }
    };
    this.result = function (o) {
        with(this) {
            moGlb.loader.hide();
            try { o = JSON.parse(o); } catch(ex) { moDebug.log(ex); moGlb.alert.show(moGlb.langTranslate(o)); return; }
            if(moGlb.isset(o.message)) moGlb.alert.show(moGlb.langTranslate(o.message));
            else if(moGlb.isset(o.error)) moGlb.alert.show(moGlb.langTranslate(o.error));
            else {
                // edit ok --> delete global forcedPM for next saves
				moLibrary.forcedPM = null;
				if(name.split('_')[1] == 'dmn') {
                    if(data.f_code == 0 && parseInt(data.t_selectors) == 7) { // Data Manager export
                        Ajax.sendAjaxPost(moGlb.baseUrl+"/export/start-template", "template="+data.fc_dmn_export_settings.split('|')[0]+"&f_code="+o.f_code, "moLibrary.exportCallback");
                    } else if(moGlb.isset(o.startImport)) { // Data Manager import instant
                        moLibrary.createInstantImport(o.f_code, o.sheet);
                    }
                }
                data.f_code = 0;
                if(name.split("_")[1] != 'up') cancel(true);
                else { moGlb.alert.show(moGlb.langTranslate("Logout from the system to see your changes")); }
                ReverseAjax.count = ReverseAjax.delay - 2;
            }
            if(moInit.admin) eval(eval(pname+".treeGrid")).refresh();
        }
    };
    this.enableSave = function (enabled) {
        with(this) {
            try {
                var mod = name.multiReplace({'mdl_': '', '_edit': ''});
                if(mod.indexOf('_') < 0) eval('btn_'+mod+'_save').setProperties({disabled: !enabled});
            } catch(ex){ if(!moInit.admin) moDebug.log(ex); }
        }
    };
    this.setLabel = function (clabel) {
        with(this) {
            if(!moGlb.isset(clabel)) clabel = '';
            for(var i in data) clabel = clabel.replace('{'+i+'}', moGlb.langTranslate(data[i]));
            clabel = moGlb.langTranslate(!clabel || (clabel.indexOf("{") >= 0 && clabel.indexOf("}") >= 0)?"Edit":clabel);
            try {
                if(mf$(name+"_header_lbl")) mf$(name+"_header_lbl").innerHTML = clabel;
                var hctx = mf$(name+"_hcnv").getContext('2d');
                with(hctx) {save();translate(14, h-padding*2-10);rotate(270 * Math.PI / 180);
                moCnvUtils.write(hctx, clabel, 0.5, 0.5, "#ffffff", moGlb.fontBold, "left");restore();}
            } catch(ex) {}
        }
    };
    this.checkCompleteness = function (tg) {
        with(this) {
            if(tg) inprogress.push(tg);
            //console.log(inprogress.length+" == "+tgn);            
            enableSave(editability && (data.f_code == 0 || tgn == 0 || inprogress.length >= tgn));
        }
    };
//    this.modResize = function (l0, t0, w0, h0, layoutid) {
//        this.baseResize(l0, t0, w0, h0, layoutid);
//        with(this) {
//            try {
//                for(var i in componentm) with(eval(componentm[i])) { if(type == 'moModuleComponent') { console.log(componentm[i]); scroll(0); setScrollbar(); } }
//            } catch(ex) {/*moDebug.log("Resetting "+componentm[i]+": "+ex);*/}
//        }
//    };

// NICO 08/10/2018
/**
     * Remove a cross with the name in input.
     * @param int code
     * @param string tbl
     * @param int ftp
     */
    this.removeCrossDataFromName = function (selectorname, tbl, ftp, picktype) {
        with(this) {
            var module_name = this.name.replace('edit', 'tg'), f_code = [];
            var aux = this.name.split('_');
            var code = "";
            try{
                if (picktype.indexOf('slc') != -1) {
                    var split = picktype.split("_");
                    var mdl = eval('mdl_' + aux[1] + '_' + split[0] + '_tg_tg');
                }
                else{
                    var mdl = eval('mdl_' + aux[1] + '_' + picktype + '_tg_tg');
                }
                
                for (var j in mdl.JStore) {
                    if (ftp > 0) {
                        if (ftp != mdl.JStore[j].f_type_id) {
                            continue;
                        }
                    }
                    if (mdl.JStore[j].f_title == selectorname) {
                       code = mdl.JStore[j].f_code;
                       delete(mdl.rifY[code]);
                       delete(mdl.JStore[code]);
                       
                       break;
                   } 
                }
                for(var i = 0; i < mdl.Ypos.length; i++){
                    if(mdl.Ypos[i].fcode == code){
                        mdl.Ypos.splice(i, 1);
                        break;
                    }
                }

                /*mdl.updateMenuNewJob();
                mdl.draw2();*/
            }
            catch(e){ 
            }
            
            var index = data[tbl+"_"+ftp].f_code.indexOf(code);
            if (index > -1) {
              data[tbl+"_"+ftp].f_code.splice(index, 1);
            }
        }
    };



};
moModuleEdit.prototype = new moModule();