var moModuleCrud = function () {
    this.innerLayout = "", this.pinned = false, this.commandBar = "", this.treeGrid = "", this.edit = "", this.timeLine = "",
    this.inspectionField = "", this.noscroll = true;
    var unsetted = true, data = "";
    this.create = function (props) {
        with(this) {
            baseCreate(props);
            if(!innerLayout) innerLayout = name.replace('mdl', 'lyt');
            if(!commandBar) commandBar = name.replace('crud', 'cb'); try {eval(commandBar);} catch(ex) {commandBar = '';}
            if(!treeGrid) treeGrid = name.replace('crud', 'tg'); try {eval(treeGrid);} catch(ex) {treeGrid = '';}
            if(!edit) edit = name.replace('crud', 'edit'); try {eval(edit);} catch(ex) {edit = '';}
            if(!timeLine) timeLine = name.replace('crud', 'tl'); try {eval(timeLine);} catch(ex) {timeLine = '';}
        }
    },
    this.draw = function () {
        with(this) {
            if(mbody && w > minsz && h > minsz && !moGlb.isempty(innerLayout)) {
                try {eval(innerLayout).display(true, mbody.id);}
                catch(ex) {moDebug.log("moModuleCrud.draw");moDebug.log(ex);return;}
            }
        }
    };
    this.modResize = function (l, t, w, h, layoutid) {
        this.baseResize(l, t, w, h, layoutid);
        with(this) {
            if(moGlb.isempty(innerLayout)) return;
            if(unsetted) {
                unsetted = false;
                var l = eval(innerLayout);
                try { commandBar = name.replace('crud', 'cb'); eval(commandBar).setParent(name); } catch(ex) { commandBar = ''; }
                try { treeGrid = name.replace('crud', 'tg'); eval(treeGrid).setParent(name); } catch(ex) { treeGrid = ''; }
                try { edit = name.replace('crud', 'edit'); eval(edit).setParent(name); } catch(ex) { edit = ''; }
                try { timeLine = name.replace('crud', 'tl'); eval(timeLine).setParent(name); } catch(ex) { timeLine = ''; }
            }
            if(treeGrid && data) { eval(treeGrid).setData(data); data = ""; }            
        }
    };
    this.setData = function (data) { this.data = data; with(this) if(treeGrid) { eval(treeGrid).setData(data); data = ""; } };
    this.getData = function () { with(this) { if(treeGrid) return eval(treeGrid).getData(); } };
    this.display = function (d) {
        with(this) {
            baseDisplay();
            if(mbody && !moGlb.isempty(innerLayout)) {
                try{eval(innerLayout).display(d, mbody.id);}
                catch(ex){moDebug.log("Warning: "+name+", innerLayout "+innerLayout+" doesn't exist");return;}
            }
        }
    };
    this.pin = function (moduleName, direction) { // direction: <= 0 1 =>
        with(this) {
            if(moGlb.isempty(innerLayout)) return;
            pinned = eval(innerLayout).getProperty(moduleName, "acc") > 0;
            //try {eval(innerLayout).setProperty(moduleName, "acc", (eval(innerLayout).getProperty(moduleName, "acc") == 0 ? 1 : 0));} catch(ex){moDebug.log("Warning: "+name+", innerLayout "+innerLayout+" doesn't exist");return;}            
            try {eval(innerLayout).setProperty(moduleName, "acc", (!pinned ? 1 : 0));} catch(ex){moDebug.log("Warning: "+name+", innerLayout "+innerLayout+" doesn't exist");return;}            
            //l.expand(i, direction);
        }
    };
    this.openEdit = function () {
        with(this) {
            var nm = name.split('_');
            if(moGlb.isempty(eval(edit).componentm)) eval(edit).draw();
            //if(nm[2] == 'crud' || (nm[2] == 'pp' && nm[3] == 'crud')) {
            if(nm[2] == 'crud') {
                try {
                    if(!pinned){eval(innerLayout).expand(treeGrid, edit, 0, 1);}
                    //eval(innerLayout).expand(treeGrid, timeLine?timeLine:edit, 0, 1);
                    //if(timeLine) eval(innerLayout).expand(timeLine, edit, 0, 1);
                } catch(ex) { moDebug.log(ex); }
            } else { // Pair cross
                var wname = edit+'_win', win = '';                
                try { win = eval(wname);
                } catch(ex) {
                    try { // Creating edit layout
                        moComp.createComponent(edit+'_lyt', "moLayout", {layout:[{idp:-1,ov:0,tp:0,wh:100,level:-1,module:edit,lock:0,minv:36,acc:0},{idp:0,ov:1,tp:0,wh:100,level:-1,module:edit,lock:0,minv:36,acc:0}]}, true);
                    } catch(ex) { moDebug.log("Problems during loading "+edit+'_lyt'+": check it - "+ex); }
                    win = moComp.createComponent(wname, 'moPopup', {
                        contentid: edit+'_lyt', title: moGlb.langTranslate('Properties'), width: 750, height: 350
                    }, true);
                    win.enableClose(false);
                }
                win.show();
            }
        }
    };
    this.closeEdit = function () {
        with(this) {
            try { // Pair cross
                eval(edit+'_win').hide();
            } catch(ex) {
                //if(eval(innerLayout).getProperty(edit, "acc") == 1 && eval(innerLayout).open) {
                if(!pinned) {
                    eval(innerLayout).expand(edit, treeGrid, 1, 1); // pin == 0
                    //eval(innerLayout).expand(edit, (timeLine?timeLine:treeGrid), 1, 1); // pin == 0
                    //if(timeLine) eval(innerLayout).expand(timeLine, treeGrid, 1, 1); // pin == 0                    
                }
            }
        }
    };
    this.reset = function () { with(this) { if(edit) eval(edit).reset(); } };
    //this.setInspectionLabel = function (inspectionName) { with(this) { var lbl = label.split(" - "); setLabel(lbl[0]+(inspectionName=="default"?"":" - "+inspectionName)); } };    
    this.setInspectionLabel = function (iName) {
        with(this) {
            var s_lbl = label.split(" - ");
            setLabel(s_lbl[0] + (iName.toLowerCase() != 'default' ? ' - '+iName : ''));
        }
    };
    this.importData = function () { Importer.Start(this.treeGrid); };
    this.setActive = function (b) { with(this) { active = b; try { eval(treeGrid).setActive(b); } catch(ex) { } } };    
};
moModuleCrud.prototype = new moModule();