var moModule = function (name) {
    this.name = "", this.mid = "", this.label = "", this.type = "", this.timestamp = "", this.active = false, this.lighter = false,
    this.t = 0, this.l = 0, this.w = 0, this.h = 0, this.minsz = 38, this.display = true, this.eventExclude = {},
    this.hdH = 28, this.noheader = false, this.noframe = false, this.noshadow = false, this.noscroll = false,
    this.margin = 6, this.padding = 4, this.borderWidth = 0, this.color = moColors.text, this.font = moGlb.fontNormal,
    this.frame = null, this.mbody_frame = null, this.mbody = null, this.layout = "", this.pname = "", this.help = '',
    this.vscroll = '', this.dy = 0;
    this.baseCreate = function (props) {
        this.json = JSON.stringify(props);
        moGlb.mergeProperties(this, props);
        this.label = moGlb.langTranslate(this.label);
    };
    this.create = function (props) {this.baseCreate(props);};
    this.draw = function () {};
    this.drawHeader = function () {
        with(this) {
            var header = mf$(name+"_header"), lbl = moGlb.reduceText(label, moGlb.fontBold, (w > minsz ? w : h)-14)/*, hcnv = mf$(name+"_hcnv"), hctx = (hcnv ? hctx = hcnv.getContext('2d') : null)*/;
            if(!header) {
                header = moGlb.createElement(name+"_header", "div", frame);
                header.setAttribute("style", "z-index:"+(moGlb.zmodule+2));
                header.innerHTML = "<span id='"+name+"_header_lbl'></span>";                
            }
            setLabel(label);
            header.setAttribute("class", "hd"+(w <= minsz ? " hor" : "")+(lighter ? " lighter" : ""));
            header.style.display = (noheader && w > minsz && h > minsz ? "none" : "block");
            header.style.height = (w <= minsz ? (h-padding*2-borderWidth*4-margin*2) : hdH)+"px";
            if(help) with(moGlb.createElement(name+"_help", "div", header)) {
                setAttribute("class", "tip"); innerHTML = String.fromCharCode(58630);
                onclick = function (e) { Help.Start(help); };
            }
        }
    };
    this.createFrame = function (containerid) {
        with(this) {
            if(!frame) frame = moGlb.createElement(name, "div", mf$(containerid));
            frame.setAttribute("class", "module_box"+(noframe ? "" : " frame")+(noshadow ? "" : " shadow"));
            if(!noframe) { frame.setAttribute("style", "border-width:"+borderWidth+"px;z-index:"+moGlb.zmodule); drawHeader(); }            
            drawBody();
        }
    };
    this.drawBody = function () {
        with(this) {
            mbody_frame = moGlb.createElement(name+"_mbody_frame", "div", mf$(frame.id));
            mbody_frame.setAttribute("class", "mbody_frame"+(w <= minsz ? " hor" : "")+(noheader ? " full" : ""));
            mbody_frame.setAttribute("style", "border-width:"+(noframe ? 0 : borderWidth)+"px;z-index:"+moGlb.zmodule+";padding:"+padding+"px;");
            
            mbody = moGlb.createElement(name+"_mbody", "div", mbody_frame);
            mbody.setAttribute("class", "mbody"+(w <= minsz ? " hor" : "")+(noheader ? " full" : ""));
            mbody.setAttribute("style", "z-index:"+moGlb.zmodule+";padding:"+padding+"px;");
        }
    };
    this.setActive = function (b) { with(this) { active = b; if(!noscroll) setScrollbar(); }};
    this.modIn = function (res) {};
    this.modOut = function () {};
    this.setParent = function (pname) { this.pname = pname; };
    this.baseResize = function (l0, t0, w0, h0, layoutid, anim) {
        with(this) {
            if(!active || (l == l0 && t == t0 && w == w0 && h == h0)) return;
            l = l0, t = t0, w = w0, h = h0, layout = layoutid;
            if(!anim) createFrame(layoutid);
            with(frame) style.top = (t+getMargin("t"))+"px", style.left = (l+getMargin("l"))+"px", style.width = (w <= minsz ? hdH : (w-(getMargin("l")+getMargin("r"))-borderWidth*2))+"px", style.height = (h-(getMargin("t")+getMargin("b"))-borderWidth*2)+"px";
            if(!anim && mbody && w > minsz && h > minsz) draw();
            if(typeof displayBody == "function") displayBody((w > minsz && h > minsz));
            //setScrollbar();            
        }
    };
    this.displayBody = function (d) {
        with(this) {
            var mbody_cover = mf$(name+"_mbody_cover");
            if(!mbody_cover) {
                mbody_cover = moGlb.createElement(name+"_mbody_cover", "div", mf$(frame.id));
                mbody_cover.setAttribute("style", "border-width:"+(noframe ? 0 : borderWidth)+"px;z-index:"+(moGlb.zmodule+10)+";");                
            }
            mbody_cover.setAttribute("class", "mbody"+(w <= minsz ? " hor" : "")+(noheader ? " full" : ""));
            mbody.style.display = (d ? "block" : "none");
            mbody_cover.style.display = (d ? "none" : "block");
            setScrollbar();
        }
    }
    this.baseDisplay = function (d) { this[(d ? "start" : "stop")+"Listening"](); };
    this.display = function (d) { this.baseDisplay(d); };
    this.modResize = function (l0, t0, w0, h0, layoutid) { this.baseResize(l0, t0, w0, h0, layoutid); };
    this.modResizeAnimation = function (l0, t0, w0, h0, layoutid) {this.baseResize(l0, t0, w0, h0, layoutid, true);};
    this.getPosition = function () {return moGlb.getPosition(this.frame);};
    this.sender = function () {
//        EventDispatcher.dispatch(this.name, "greet", "Hi all! I'm "+this.name);
    };
//    this.listener = function (module, key, values) {
//        moDebug.log(values+" - Hi "+module+", I'm "+this.name);
//    };
    this.listener = function () {};
    this.startListening = function () {EventDispatcher.register(this.name, this.eventExclude);};
    this.stopListening = function () {EventDispatcher.unregister(this.name);};
    this.getMargin = function (pos) { with(this) { return (typeof margin == "object" ? margin[pos] : parseInt(margin)); } };
    this.setLabel = function (clabel) {
        with(this) {
            label = clabel; //var hctx = mf$(name+"_hcnv").getContext('2d');
            mf$(name+"_header_lbl").innerHTML = clabel;
//            with(hctx) {clearRect(0, 0, 1000, 1000);save();translate(14, h-padding*2-10);rotate(270 * Math.PI / 180);
//            moCnvUtils.write(hctx, clabel, 0.5, 0.5, "#ffffff", moGlb.fontBold, "left");restore();}
        }
    };
    this.getUnique = function () {};
    this.scroll = function (y0) {with(this) {if(!noscroll) {dy = -y0; if(mbody) mbody.style.top = dy+"px"; }}};    
    this.getScrollbarPosition = function () {
        with(this) {
            var fpos = moGlb.getPosition(frame), hdh = (noheader?0:hdH), bw = (noframe?0:borderWidth);
            return { hdh: hdh, top: hdh+padding, left: fpos.w-padding-(vscroll?vscroll.W:0), height: fpos.h-padding*2-hdh-bw*2 };
        }
    };
    this.setScrollbar = function (hh, resize) {
        with(this) {
            if(noscroll || !mbody) return;
            if(!hh) hh = parseInt(mbody.scrollHeight);
            if(!vscroll) {
                vscroll = new OVscroll(name+".vscroll", {zi: moGlb.zmodule+10, pid: frame.id, Fnc: name+".scroll"});
                var o = this;
                frame.onmouseover = function (e) {WHEELOBJ = o.name+".vscroll"; };
                frame.onmouseout = function (e) {WHEELOBJ = ""; };
            }
            var pos = getScrollbarPosition(), display_on = w > minsz && h > minsz && hh > h-pos.hdh;
            vscroll.Resize({T: pos.top, L: pos.left, H: pos.height, HH: hh});
            vscroll.Display(display_on);
            mbody.style.right = (display_on?vscroll.W:0)+"px";
        }
    };    
};