var moModuleComponent = function () {
    this.fields = "", this.noheader = true, this.noframe = true, this.lblH = 20, this.componentH = 85, this.groups = ["msim-default"],
            this.gfields = {"msim-default": []}, this.openFieldset = false, this.dt = {}, this.uniquefields = [], this.margin = 0, this.padding = 0;
    var alreadySet = false, visibility = {wf: {id: 0, phase: 0}, type_id: 0, selectors: []};
    this.draw = function () {
        with (this) {
            if (mbody && !alreadySet) {
                if (typeof fields == "string")
                    groupFields();
                for (var i in groups)
                    createFieldset(gfields[groups[i]], groups[i]);
                alreadySet = true;
                setData(dt);
                if (moGlb.user.level == -1) {
                    with (moGlb.createElement("", "span", mbody)) { // Mock up
                        setAttribute("class", "component settings mockup");
                        setAttribute("title", "Print mock-up");
                        innerHTML = String.fromCharCode(58488);
                        onclick = function (e) {
                            eval(pname).print({script_name: "print_mockup", module_name: name, mockup: 1});
                        };
                    }
                    with (moGlb.createElement("", "span", mbody)) { // List
                        setAttribute("class", "component settings");
                        setAttribute("title", "Print field list");
                        innerHTML = String.fromCharCode(58442);
                        onclick = function (e) {
                            eval(pname).print({script_name: "print_mdl_info", module_name: name, mockup: 1});
                        };
                    }
                }
            }
        }
    };
    this.createFieldset = function (fieldlist, flabel) {
        with (this) {
            var fds = moGlb.createElement(createFieldsetName(flabel), "div", mbody, true),
                    lbl = moGlb.createElement(fds.id + "_label", "div", fds);

            fds.setAttribute("class", "fieldset" + (flabel == "msim-default" ? " hidden" : (openFieldset ? "" : " empty")));
            lbl.setAttribute("class", "labelling");

            lbl.innerHTML = '<span class="open">' + String.fromCharCode(58789) + '</span><span class="close">' + String.fromCharCode(58788) + '</span>' + moGlb.langTranslate(flabel);
            lbl.onclick = function () {
                if (!moGlb.removeClass(fds, "empty"))
                    moGlb.addClass(fds, "empty");
                setScrollbar();
            };
            for (var i = 0; i < fieldlist.length; i++) {
                if (fieldlist[i] != "|") {
                    if (i > 0 && fieldlist[i - 1] == "|") {
                        with (moGlb.createElement(name + "_" + fieldlist[i] + "_newline", "div", fds, true)) {
                            setAttribute("class", "component newline");
                        }
                    }
                    createComponent(fieldlist[i], fds);
                }
            }
        }
    };
    this.createComponent = function (field, fieldset) {
        with (this) {//console.log(field);console.log('****************'); console.log(fieldset);
            var lbl_width = eval(field).w > 175 ? (eval(field).w - 13) + 'px' : null;
            var comp = moGlb.createElement(name + "_" + field + "_cnt", "div", fieldset, true),
                    info = moGlb.createElement(field + "_info", "div", comp, true),
                    lbl = moGlb.createElement(field + "_label", "span", comp, true, lbl_width),
                    batch = moGlb.createElement(field + "_batch", "div", comp, true),
                    /*lock_close = 58404    lock_open = 58405*/
                    unique = moGlb.createElement(field + "_unique", "div", comp, true);
            comp.setAttribute("class", "component");
            if (eval(field).getProperty("type") != "moButton") {
                info.setAttribute("class", "info");
                //info.setAttribute("tabindex", "0");
                lbl.setAttribute("class", "label");
                //batch.setAttribute("class", "batch "+(isBatch && eval(field).isBatchable() ?"off":"hide"));
                batch.setAttribute("class", "batch hide");
                unique.setAttribute("class", "unique hide");

                info.innerHTML = String.fromCharCode(58542);
                info.onclick = function (e) {
                    var icloud = mf$("info_cloud"), infoLabel = moGlb.langTranslate(eval(field).getProperty("info"));
                    if (!icloud) {
                        icloud = moGlb.createElement("info_cloud", "div");
                        icloud.setAttribute("class", "info_cloud hide");
                        document.addEventListener("click", function (e) {
                            var icloud = mf$("info_cloud");
                            if (icloud)
                                icloud.style.display = 'none';
                        });
                    }
                    icloud.innerHTML = infoLabel + (moGlb.DEBUG_ON && moGlb.user.level == -1 ? '<div class="fieldname"><b>field:</b> ' + field + '</div><div class="fieldname"><b>bind:</b> ' + eval(field).bind + '</div>' + (eval(field).onload ? '<div class="fieldname"><b>on load:</b> ' + eval(field).onload + '</div>' : '') + (eval(field).onchange ? '<div class="fieldname"><b>on change:</b> ' + eval(field).onchange + '</div>' : '') : '');
                    comp.appendChild(icloud);
                    icloud.style.display = 'block';
                    e.stopPropagation();
                };
                lbl.innerHTML = moGlb.langTranslate(eval(field).getProperty("label"));
                batch.innerHTML = '<span class="on">' + String.fromCharCode(58806) + '</span><span class="off">' + String.fromCharCode(58807) + '</span>';
                batch.onclick = function (e) {
                    var mod = ((field).split("_"))[1];
                    //var elem_id = eval(field + "_unique").id;
                    if ((eval("mdl_" + mod + "_edit").unique).indexOf(eval(field).bind) != -1 || eval(field).unique != '') {
                        if (!setUnique(field)) {
                            if (!moGlb.replaceClass(this, "on", "off")) {
                                moGlb.replaceClass(this, "off", "on");
                                eval(field).batch = true;
                                if (eval("mdl_" + mod + "_edit").unique != '')
                                    document.getElementById(field + "_unique").className = "unique on2";
                            } else {
                                eval(field).batch = false;
                                document.getElementById(field + "_unique").className = "unique on";
                            }
                        }
                    } else {
                        if (!moGlb.replaceClass(this, "on", "off")) {
                            moGlb.replaceClass(this, "off", "on");
                            eval(field).batch = true;
                            if (eval(field).type == "moPicklist") {
                                eval(field).removeAll();
                                eval("mdl_" + mod + "_edit").addPckData("", eval(field).table + (eval(field).isparent ? '_parent' : ''), eval(field).ftype, eval(field).picktype);
                            }
                        } else {
                            if (eval(field).type == "moPicklist") {
                                eval(field).removeAll();
                                eval("mdl_" + mod + "_edit").removePckData("", eval(field).table + (eval(field).isparent ? '_parent' : ''), eval(field).ftype, eval(field).picktype);
                            }
                            eval(field).batch = false;
                        }
                            
                    }
                };

                unique.innerHTML = '<span class="on" title = "Field unique on module">' + String.fromCharCode(58406) + '</span><span class="on2" title = "Field unique on module">' + String.fromCharCode(58405) + '</span><span class="off" title = "Field unique on element">' + String.fromCharCode(58406) + '</span>';
            }
            eval(field).setProperties({componentBox: name + "_" + field + "_cnt", pname: name});
            eval(field).display(0, padding, comp.id, true);
        }
    };

    this.setUnique = function (field)
    {
        var mod = ((field).split("_"))[1];
        with (this) {
            var elem_id = eval(field).unique;
            if (eval(field).unique != '') {
                return true;
            } else if (eval("mdl_" + mod + "_edit").unique != '') {
                var all_fields = eval("mdl_" + mod + "_edit_c1").fields;
                for (var fi in all_fields) {
                    var obj_f = eval(all_fields[fi]);
                    if (((eval("mdl_" + mod + "_edit").unique).indexOf(obj_f.bind) != -1) && ((document.getElementById(all_fields[fi] + "_unique").className).indexOf("on2") != -1) && all_fields[fi] != field)
                        return true;
                }
            }
            return false;
        }

    };
    this.groupFields = function () {
        with (this) {
            var f = fields.split("|"), err = ['Warning: there are some errors in module ' + name + ":"];
            for (var i in f) {
                f[i] = f[i].split(",");
                for (var j = 0; j < f[i].length; j++) {
                    if (!f[i][j])
                        err.push("- Module has an empty field specifications, check your configuration");
                    else {
                        try {
                            var groupName = eval(f[i][j]).groupName;
                            if (!moGlb.isset(gfields[groupName])) {
                                groups.push(groupName);
                                gfields[groupName] = [];
                            }
                            gfields[groupName].push(f[i][j]);
                            if (eval(f[i][j]).unique)
                                uniquefields.push(eval(f[i][j]).bind);
                        } catch (ex) {
                            //moDebug.log("Problems with field "+f[i][j]+": check your configuration");
                            err.push("- Field " + f[i][j] + " is not defined");
                        }
                    }
                }
                try {
                    gfields[groupName].push("|");
                } catch (ex) {
                }
            }
            fields = (fields.multiReplace({"\\|": ","})).split(",");
            if (err.length > 1)
                moGlb.alert.show(err.join("\n"));
        }
    };
    this.setData = function (data, pnm) {
        with (this) {

            var focus = 0, efldi, fldi;
            if (typeof fields == "string")
                dt = data;
            else {

                for (var i in fields) {   // alert(fields[i]+" | "+data[fields[i]])
                    fldi = fields[i];
                    efldi = eval(fldi);

                    if (efldi) {
                        var batchn = false;
                        if (eval(pname)) {
                            batchn = eval(pname).isBatch?true:false;                       
                             }
                        with (efldi) {
                            if (!focus && !isReadonly() && !isDisabled()) {
                                setFocus();
                                focus = 1;
                            }

                            if (moGlb.isset(efldi.bind)) {
//                            setData({value: (moGlb.isset(data[bind]) ? data[bind] : ""), v_wf: data.f_wf_id, v_phase: data.f_phase_id,
//                                v_type_id: data.f_type_id, v_selectors: data.t_selectors, isbatch: eval(pname).isBatch});

                                try {
                                    setData({value: ((moGlb.isset(data[bind]) && data[bind]) ? data[bind] : ""), isbatch: batchn});
                                    //setData({value: ((moGlb.isset(data[bind]) && data[bind]) ? data[bind] : ""), isbatch: eval(pname).isBatch});
                                } catch (e) {
                                }

                            }
                            display();
                        }
                        if (eval(pname) && mf$(fldi + "_batch")) {   
                            mf$(fldi + "_batch").setAttribute("class", "batch " + (eval(pname).isBatch && efldi.isBatchable() ? "off" : "hide"));
                            eval(fldi).batch = false;
//                            if (eval(pname).isBatch && !efldi.isBatchable()) {
//                                efldi.reset();
//                                mf$(efldi.componentBox).setAttribute("class", "component hide");
//                            }
                        }
                    }
                }

            }


        }
    };
    this.getData = function () {
        with (this) {
            var data = {mandatory: [], error: [], attachments: []}, isBatch = eval(pname).isBatch;
            if (typeof fields == "string")
                return;
            for (var i in fields) {
                with (eval(fields[i])) {
                    if (fields[i] != "|" && eval(fields[i]).type != "moButton") {
                        try {
                            if (!consistencyCheck(isBatch)) {
                                if (!mandatoryCheck())
                                    data.mandatory.push(moGlb.langTranslate(eval(fields[i]).label));
                                if (!errorCheck())
                                    data.error.push(moGlb.langTranslate(eval(fields[i]).label));
                            }
                            if (eval(fields[i]).type == 'moImage' || (moGlb.isset(eval(fields[i]).attach) && eval(fields[i]).attach))
                                data.attachments.push(bind);
                        } catch (ex) {
                            moDebug.log(fields[i] + " exception: " + ex);
                        }
                        if (getProperty("visible") && !getProperty("disabled") && (!isBatch || getProperty("batch"))) {
                            data[bind] = get();
                        }
                    }
                }
            }
            return data;
        }
    };
    this.reset = function () {
        with (this) {
            if (typeof fields == 'string')
                return;
            for (var i in fields) {
                try {
                    eval(fields[i]).reset();
                } catch (ex) {
                    moDebug.log("Problems in resetting field " + fields[i] + ": " + ex);
                }
            }
            vscroll.Resize({ptr: 0});
        }
    };
    this.setBatch = function (fieldId) {
        with (this) {
            alert('setBatch');
            mf$(fieldId + "_batch").setAttribute("class", "batch " + (eval(pname).batchEdit ? "off" : "hide"));
        }
    };
    this.setFieldsetVisibility = function (fname) {
        with (this) {
            var fieldset = mf$(createFieldsetName(fname));
            var cmp = fieldset.getElementsByClassName("component hide"), ncomp = 0;
            for (var i in gfields[fname])
                if (gfields[fname][i] != '|')
                    ncomp++;
            fieldset.style.display = (cmp.length == ncomp ? "none" : "block");
        }
    }
    this.createFieldsetName = function (lbl) {
        return this.name + "_fieldset_" + lbl.replace(" ", "-");
    }
    this.getUnique = function () {
        return this.uniquefields;
    };
    //this.cancel = function (o) { this.scroll(0); };
};
moModuleComponent.prototype = new moModule();