summaryShell = [];
summaryShellType = [];
summarySelected = [];
var moModuleSummary = function () {
    this.items = [], this.scriptName = "", /*this.params = "", */this.tgName = "", /*this.ui = [], */this.selected = '', this.lighter = true, this.summaryFrom = null, resetSummary = 0;
    this.draw = function (n) {

        with(this) {
            if(tgName && items.length == 0) {
                var filters = eval(tgName).getFilters();
                filters.Prnt = -1;
                filters.summaryFrom = summaryFrom;
                if(resetSummary == 1) {
                    filters.Ands = [];
                    resetSummary = 0;
                }
                if(scriptName == 'wo-all' && filters.Ands.length > 0) {
                        filters.Ands = [];
                }
                var params = JSON.stringify(filters);
                Ajax.sendAjaxPost(moGlb.baseUrl+"/summary/"+scriptName, "params="+params, name+".result");
                //ui = [];                
            }
        }
    };
    this.result = function (o) {
        with(this) {
            try {
              items = JSON.parse(o); 
            
              var sep="", ovrall={ 
                          "value":"'Show overall'",  
                          "count":0,
                          "label":moGlb.langTranslate('Show overall'),
                          "description":"",
                          "types":[],
                          "key": "t_wf_phases.f_summary_name"
                        };
               
           this.queryAll=""; 
              
           if(moGlb.stgSummaryAll){           
              for(var i in items) { 
                if(items[i].value == "'Deleted'" || items[i].value == "'Deleting'" || items[i].value == "'Cancellation'") { continue; }
                queryAll+=(sep+items[i].value);   
                ovrall.count+=items[i].count;
                sep=","; 
              }           
              items.push(ovrall);
            }
            
              drawList();

            }
            catch(ex) {moDebug.log(ex);}
        }
    };
    this.drawList = function () {
        with(this) {
            var createItem = function(name, params, cnt) {
                with(params) {
                    var item = moGlb.createElement("", "div", mbody, true);
                    item.setAttribute("class", "summary "+value);
                    
                    if(value=="inbox") {
                        var avatar = moGlb.createElement("", "div", item, true);
                        avatar.setAttribute("class", "icon"+(value=="inbox"?" avatar":""));
                        avatar.setAttribute("style", "background-image:url("+moGlb.getAvatarPath(icon.split("|")[0], moGlb.user.gender)+")");
                        avatar.innerHTML = "&nbsp;";
                    }

                    var lbl = moGlb.createElement(name+"_"+value, "div", item, true);
                    lbl.setAttribute("class", "label");
                    lbl.innerHTML = moGlb.langTranslate(label);
    
                    if(description) {
                        var desc = moGlb.createElement("", "div", lbl, true);
                        desc.setAttribute("class", "desc");
                        desc.innerHTML = moGlb.langTranslate(description);
                    }

                    if(moGlb.isset(count) && count>-1) {
                        var cnt = moGlb.createElement("", "div", item, true);
                        cnt.setAttribute("class", "count");
                        cnt.innerHTML = "("+count+")";
                    }
                    
                    if(moGlb.isset(params.types) && !moGlb.isempty(types)) {
                        var dtypes = moGlb.createElement("", "div", item, true);
                        if(cnt == 0)  {
                            dtypes.style.display = 'none';
                        }
                        dtypes.setAttribute("class", "types");
                        for(var i in types) {
                            var dtype = moGlb.createElement(name+"_"+value+"_"+i, "div", dtypes, true);
                            with(dtype) {
                                setAttribute("class", "type");
                                var countLabel = types[i].count > -1 ? " ("+types[i].count+")" : "";
                                innerHTML = types[i].label + countLabel;
                            }
                            //ui.push(dtype);
                        }
                    }                    
                }
            };
            summaryShell[name] = [];
            summaryShellType[name] = [];
            if(mbody && w > minsz && h > minsz) {
                mbody.innerHTML = "";
                for(var i in items) {
                    var k=items[i].key, v=items[i].value, qq=queryAll;
                 /*   if(document.getElementById(name+"_"+v)) {
                        continue;
                    }*/
                    createItem(name, items[i], items[i].count);
                    summaryShell[name][i] = v;
                    mf$(name+'_'+v).onclick = function(e) {
                        summaryFrom = 1;
                        var sql, sp=this.id.split("_"), vsp=sp[sp.length-1];
                            if(!vsp.match(/\'/)) {
                                vsp = "'"+vsp+"'";
                            }
                            if(!qq.match(/\'/)) {
                                qq = "'"+qq+"'";
                            }
                        sql=' IN ('+( (vsp!="'Show overall'")?vsp:qq  )+')';

                        sender([ { field:k, sql:sql }, { field:'f_type_id', sql:' IN ('+eval(tgName).ftype+')' } ]);
                        select(this.id);
                        if(name == 'mdl_wo_summary_inbox') {
                            if(typeof summaryShell['mdl_wo_summary'] != 'undefined') {
                                for(var m in summaryShell['mdl_wo_summary']) {
                                    moGlb.removeClass(mf$('mdl_wo_summary_'+summaryShell['mdl_wo_summary'][m]), 'selected');   
                                }
                            }
                            if(typeof summaryShellType['mdl_wo_summary'] != 'undefined') {
                                for(var o in summaryShellType['mdl_wo_summary']) {
                                    moGlb.removeClass(mf$('mdl_wo_summary_'+summaryShellType['mdl_wo_summary'][o]), 'selected');   
                                }
                            }
                            
                            summarySelected['mdl_wo_summary'] = '';
                            summaryShellType['mdl_wo_summary'] = '';
                            
                        }
                        if(name == 'mdl_wo_summary') {
                            if(typeof summaryShell['mdl_wo_summary_inbox'] != 'undefined') {
                                for(var m in summaryShell['mdl_wo_summary_inbox']) {
                                    moGlb.removeClass(mf$('mdl_wo_summary_inbox_'+summaryShell['mdl_wo_summary_inbox'][m]), 'selected');  
                                }
                            }
                            summarySelected['mdl_wo_summary_inbox'] = '';
                        }
                        if(name == 'mdl_asset_summary') {
                                select();
                            }
                    }

                    for(var j in items[i].types) {
                        if(items[i].types[j].count == 0) {
                            document.getElementById(name+"_"+v+"_"+j).style.display = 'none';
                        }
                        var kt = items[i].types[j].key; 
                        if(typeof summaryShellType['mdl_wo_summary'] != 'undefined') {
                            summaryShellType['mdl_wo_summary'][j] = v+"_"+j;   
                        }
                        mf$(name+"_"+v+"_"+j).onclick = function (e) {
                            summaryFrom = 2;
                            var sp = this.id.split("_");
                            sender([{field: k, sql: ' IN ('+sp[sp.length-2]+')'}, {field: kt, sql: ' IN ('+sp[sp.length-1]+')'}]);
                            select(this.id);
                            if(name == 'mdl_wo_summary') {
                                if(typeof summaryShell['mdl_wo_summary_inbox'] != 'undefined') {
                                    for(var n in summaryShell['mdl_wo_summary_inbox']) {
                                        moGlb.removeClass(mf$('mdl_wo_summary_inbox_'+summaryShell['mdl_wo_summary_inbox'][n]), 'selected');   
                                    }
                                    summarySelected['mdl_wo_summary_inbox'] = '';
                                }
                            }
                            if(name == 'mdl_asset_summary') {
                                select();
                            }
                        }
                    }
                }
            }
            setScrollbar();
            select();
        }
    };
    this.refresh = function (gridName) { with(this) {items = [];tgName = gridName;draw();}}
    this.reset = function (gridName) { with(this) { resetSummary = 1; summarySelected[name]; refresh(gridName);}}
    // Event dispatcher
    //this.sender = function (v) {with(this) {EventDispatcher.send(name, "summary", {field: "t_wf_groups.f_id", sql: "="+v, hierarchy: 0});}};
    this.sender = function (v) {with(this) {EventDispatcher.send(name, "summary", v);}};
    //this.select = function (id) {with(this) { for(var i in ui) {if(ui[i].id == id) moGlb.addClass(ui[i], 'selected'); else moGlb.removeClass(ui[i], 'selected'); }}};
    this.select = function (id) { 
        with(this) { 
            if(id){
                if(summarySelected[name] != undefined)
                    moGlb.removeClass(mf$(summarySelected[name]), 'selected');
                summarySelected[name] = id; 
                moGlb.addClass(mf$(summarySelected[name]), 'selected'); 
            }
        }
    };
};

moModuleSummary.prototype = new moModule();
