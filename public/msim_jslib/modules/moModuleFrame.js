var moModuleFrame = function () {
    this.src = "", this.params = {}, this.iframe = "", this.padding = 0;
    this.draw = function () {
        with(this) {
            if(mbody && w > minsz && h > minsz) {
                var iframe = mf$(name+"_iframe");                
                if(!iframe) {
                    iframe = moGlb.createElement(name+"_iframe", "iframe", mbody);                    
                    iframe.setAttribute("name", name+"_iframe");
                    iframe.setAttribute("style", "position:absolute;border-width:0px;");
                    iframe.src = src.replace('BASEURL', moGlb.baseUrl);
                }
                var mbpos = moGlb.getPosition(mbody);                
                with(iframe.style) {top = padding+"px", left = padding+"px", width = (mbpos.w-padding*2)+"px", height = (mbpos.h-padding*2)+"px";}
                mbody_frame.style.backgroundColor = "#dddddd";
            }
        }
    };
    this.setData = function (data) {
        with(this) {
            params = data;
            if(mf$(name+"_iframe")) mf$(name+"_iframe").src = src.replace('BASEURL', moGlb.baseUrl)+"?"+Math.random();
        }
    };
    this.getData = function () { try { return mf$(this.name+"_iframe").contentWindow.getData(); } catch(ex) { } };
    this.reset = function () { this.params = {}; };
};
moModuleFrame.prototype = new moModule();