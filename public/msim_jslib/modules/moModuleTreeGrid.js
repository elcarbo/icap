var moModuleTreeGrid = function () {
    this.label = "Data", this.table = "t_workorders", this.ftype = "1", this.gridName = "", this.hierarchy = true, this.tgType = "moTreeGrid",
    this.listenTo = "", this.noheader = true, this.treeOn = "f_title", this.rowHeight = 30, this.tlName = "", this.ref_fault_field = '', this.skipwf = false,
    this.orderby = "f_code", this.order = "DESC", this.pairCross = 0, this.history = 0, this.self = 0, this.ignoreSelector = false, this.hiddenCross = false,
    this.minCross = 0, this.maxCross = 0, this.noscroll = true, this.completed = false;
    this.reloadSummary = true;
    var format = "", selectors = {}, activeInspection = {name: "default", shared: 1}, imenuName = "", jobName = "Job", parentCodes = [];
    this.create = function (props) {
        with(this) {
            baseCreate(props);startListening();
            if(tgType == "moTreeGrid") {
                ReverseAjax.register(name, table, ftype);
                moInit.Locked.register(name);
            }
            if(listenTo && typeof listenTo == "string") listenTo = listenTo.split(",");
            if(!gridName) {
                gridName = name+"_tg";
                var data = {
                    moduleName: name, Hy: hierarchy, Ttable: table, ftype: ftype, crud: name, ispopup: (name.indexOf('_pp_') > 0),
                    ref_fault_field: ref_fault_field, rowH: parseInt(rowHeight), fieldTree: treeOn, pairCross: pairCross,
                    history: (history?1:0), jobTitle: jobName, self: self, ignoreSelector: ignoreSelector, skipwf: skipwf,
                    hCross: (hiddenCross?1:0),
                    SORT: [ {f: orderby, m: order} ]
                };
                data.modTM = gridName.multiReplace({tg: "tl"});
                new moComp.createComponent(gridName, tgType, data, true);
            }
            imenuName = "mn_"+(name.replace("mdl_", "").replace("_tg", ""))+"_inspect";
        }
    };
    this.modResize = function (l, t, w, h, layoutid, anim) {
        this.baseResize(l, t, w, h, layoutid);
        with(this) {
            createPhaseMenu(); createInspectionMenu();
            var mbpos = moGlb.getPosition(mbody);
            eval(gridName+".resize"+(anim ? "_animation" : "")+"(0, 0, "+(mbpos.w-borderWidth*2)+", "+(mbpos.h-borderWidth*2)+", '"+mbody.id+"');");            
        }
    };
    this.modResizeAnimation = function (l, t, w, h, layoutid) {this.modResize(l, t, w, h, layoutid, true);};
    this.createItem = function (data) {
        with(this) { if(tgType == 'moTreeGrid'){
                            moGlb.lockEdit = true;
                            var a = name.split("_");
                            a[2] = "edit";
                            moGlb.lockEditName = a.join("_");
                            moGlb.changeLockedElement(true,name);
                    }
            if(!data) data = {};data.f_code = 0;
            if(typeof data.f_type_id == "undefined") data.f_type_id = ftype;
//            if(moGlb.isset(data.t_selectors) && !moGlb.isempty(data.t_selectors)) eval(eval(pname).edit).addCrossData(data.t_selectors, 't_selectors', '1,2,3,4,5,6,7,8,9,10'); // cross with selectors                
            dataHandler(data);
        }
    };
    this.createChild = function (data) {
        with(this) {
            var items = getSelected();
            //aggiunta primo if per gerarchia wo
            if(data && moGlb.isset(data.childwo) && !data.childwo && items.length < 1){ createItem(data);}
            else if(items.length < 1) moGlb.alert.show(moGlb.langTranslate("Select item(s) first"));
            else if(/*name.indexOf('_slc') > 0 &&*/ items.length > 1) moGlb.alert.show(moGlb.langTranslate("You can select only one parent"));
            else { if(!data) data = {}; data[table+'_parent_'+ftype] = items; if(moGlb.isset(data.childwo) && !data.childwo) data['ischild'] = 1; createItem(data); }
        }
    };
    this.batchEdit = function () {
        with(this) {
            var selected = getSelected(), codes = [], filters = '';
            var type = "";
            if(selected.length > 0) {
                // NICO: controllo che i tipi siano tutti uguali
                var tgcodes = eval(gridName).JStore;
                type = tgcodes[selected[0].f_code].f_type_id;
                for(var i in selected) {
                    if (tgcodes[selected[i].f_code].f_type_id != type) {
                        moGlb.alert.show(moGlb.langTranslate("Item selected are different"));
                        eval(gridName).jaxULock(getSelectedIds(),0,0);
                        return;
                    }
                    codes.push(selected[i].f_code);
                } 
                //dataHandler({f_codes: codes.join(","), f_code: -1, f_editability: 1});
            } else {
                // NICO: in caso non siano selezionati blocco il multiedit
                /*filters = getFilters();
                moGlb.alert.show(moGlb.langTranslate("No item selected, all displayed rows will be modified"));*/
                moGlb.alert.show(moGlb.langTranslate("Select at least an item"));
                return;
            }
 
            dataHandler({f_codes: codes.join(","), f_code: -1, f_editability: 1, f_filters: filters, f_type_id: type});
        }
    };
//    this.unlockRecord = function () {
//        with(this) {
//            moGlb.loader.show(); var codes = getSelectedIds();
//            if(codes.length > 0) Ajax.sendAjaxPost(moGlb.baseUrl+"/treegrid/unlock-records","f_code="+code);
//            else Ajax.sendAjaxPost(moGlb.baseUrl+"/treegrid/unlock-records","f_module_name="+name);
//        }
//    };
    this.deleteUi = function (o)  {
        with(this) {
            if(o) { moGlb.loader.hide();unlock();refresh();return; }
            else {
                var codes = getSelectedIds();
                if(codes.length == 0) moGlb.alert.show(moGlb.langTranslate("No elements selected"));
                else {
                    moGlb.confirm.show(moGlb.langTranslate("Are you sure you want to proceed?"), function () {
                        moGlb.loader.show();
                        Ajax.sendAjaxPost(moGlb.baseUrl+"/ui/delete", "f_module_name="+name+"&f_code="+codes.join(','), name+".deleteUi");
                    });
                }
            }            
//            if(confirm(moGlb.langTranslate("Are you sure you want to proceed?"))) {
//                moGlb.loader.show();
//                var codes = getSelectedIds();
//                if(codes.length == 0) moGlb.alert.show(moGlb.langTranslate("No elements selected"));
//                else Ajax.sendAjaxPost(moGlb.baseUrl+"/ui/delete", "f_module_name="+name+"&f_code="+codes.join(','), name+".deleteUi");
//            }
        }
    };
    this.openPO = function (singlePo) {
        with(this) {
            moGlb.loader.show(); var codes = getSelectedIds();
            if(codes.length > 0) Ajax.sendAjaxPost(moGlb.baseUrl+"/workorder/open-po","f_codes="+codes+"&ftype="+this.ftype+"&singlePo="+singlePo);            
            else {moGlb.alert.show(moGlb.langTranslate("No elements selected"));moGlb.loader.hide();}
            moGlb.loader.hide();
        }
    };
    this.opportunisticWo = function (o) {
        with(this) {
            if(moGlb.isset(o)) {
                var res = JSON.parse(o);
                if(moGlb.isset(res.message)) {
                    //if(!isNaN(res.message)) res.message = res.message+" "+moGlb.langTranslate("workorders generated");
                    //moGlb.alert.show(moGlb.langTranslate(res.message));
					moGlb.alert.show(res.message);
                }
                ReverseAjax.count = ReverseAjax.delay - 1;
                moGlb.loader.hide(); unlock();
            } else {
                var codes = getSelectedIds();
                if(codes.length > 0) {                
                    moGlb.confirm.show(moGlb.langTranslate("Are you sure you want to proceed?"), function () {
                        moGlb.loader.show(); 
                        Ajax.sendAjaxPost(moGlb.baseUrl+"/workorder/opportunistic-wo", "f_codes="+codes, name+".opportunisticWo");
                    });
                } else moGlb.alert.show(moGlb.langTranslate("Select items to generate first"));
            }
        }        
    };
    this.move = function (up) { eval(this.gridName)['chgOrder'+(up?'UP':'DOWN')](); };
    this.edit = function (code) {
        with(this) {
            if(tgType == 'moTreeGrid'){
                moGlb.lockEdit = true;
                var a = name.split("_");
                a[2] = "edit";
                moGlb.lockEditName = a.join("_");
                moGlb.changeLockedElement(true,name);
            }
            if(typeof code == "object") dataHandler(code);
            else {
                moGlb.loader.show(moGlb.langTranslate("Loading..."));
                Ajax.sendAjaxPost(moGlb.baseUrl+"/treegrid/get-code-properties", "f_code="+code+"&Tab="+table+"&module_name="+name, name+".dataHandler");
            }
        }
    };
    this.dataHandler = function (data) {
        with(this) {
            if(typeof data == "string") data = JSON.parse(data);
            try {
                eval(pname).openEdit();
                
             // alert("pname "+pname+"\n"+eval(pname).edit)
                
                eval(eval(pname).edit).setData(data,pname);
            } catch(ex) { moDebug.log(ex); }
            moGlb.loader.hide();
            
            //Per i wo, possibilità alla creazione di aggiuingere la gerarchia ad un wo impostando nel menu mn_wo_new la action: mdl_wo_tg.createChild({childwo:true});
            if(data && moGlb.isset(data.childwo) && (data.childwo || (moGlb.isset(data.ischild) && data.ischild == 1))){
                    //if(moGlb.isset(mdl_wo_edit.data.f_type_id) && (typeof mdl_wo_edit.data.f_type_id == "string" || moGlb.isset(data.ischild))){
                            //Parent
                            for(var obj in mdl_wo_edit.data){
                                    if(obj.indexOf('workorders')){
                                            if(typeof (mdl_wo_edit.data)[obj][0] == 'object'){
                                                    var ob = (mdl_wo_edit.data)[obj][0];
                                                    pck_wo_edit_parent.set(ob.f_title);
                                                    mdl_wo_edit.addCrossData(ob.f_code, 't_workorders_parent', ob.f_type_id);
                                            }

                                    }

                            }
                    //}
            }
        }
    };
    this.closeEdit = function (batch) {with(this) {try{eval(gridName).closeEdit();/*if(batch) eval(gridName).deCheck();data = {};*/}catch(ex){}}};
    this.unlock = function () {eval(this.gridName).unlockElements();};
    this.setLocked = function (list) { eval(this.gridName).retotherLock(list); };
    this.listener = function (module, key, value) {
        with(this) {
            this.reloadSummary = true;
            if(moArray.hasElement(listenTo, module)) {
                if(key == "selector") {
                    eval(gridName).manageSelector(module, value, hierarchy?1:0);
                    for(var i in listenTo) {
                        if(eval(listenTo[i]).type == "moModuleSummary") {
                            eval(listenTo[i]).refresh(name);
                        }
                    }
                } else if(key == "summary") {
                    this.reloadSummary = false;
                    with(eval(gridName)) {
                        for(var i in value) {                            
                            setSummaryCondition(value[i], i);
                        }
                        ActRefresh();
                    }
                }
            }
        }
    };
    this.setActive = function (b) { with(this) { active = b; if(b) updateListeners(); } };
    //this.filter = function (filters) { with(eval(this.gridName)) { f_filter_button(filters); } };
    this.filter = function (filters) {
        with(this) {
            with(eval(gridName)) {
                if(moGlb.isset(filters.Sort)) SORT = filters.Sort;
                if(moGlb.isset(filters.Filter)) FILTER = filters.Filter;
                if(moGlb.isset(filters.Search)) {
                    for(var j in filters.Search) filters.Search[j].v0 = Base64.encode(filters.Search[j].v0);
                    SEARCH = filters.Search;
                }
                if(moGlb.isset(filters.Ands)) AND = filters.Ands;
                if(moGlb.isset(filters.Selector)) SELECTOR = filters.Selector;
                if(moGlb.isset(filters.Like)) LIKE = filters.Like;
            }
        }
    };
    this.setData = function (data) {        
        with(this) {
            with(eval(gridName)) { arrCHK = [];NT = 0;f_code = data.f_code;typeid_main=data.f_type_id;loadroot(); }
            if(moGlb.isset(data[table+'_'+ftype])) parentCodes = data[table+'_'+ftype];
        }
    };
    //this.getData = function () {with(this) {var d = {};d[table+"_"+ftype] = getSelected().join(",");return d;}};
    this.getData = function () {
        with(this) {
            if(history) return {};
            var d = {}; d[table+"_"+ftype] = eval(this.gridName).saveList();
            d[table+"_"+ftype].f_module_name = name;
            if(minCross && d[table+"_"+ftype].f_code.length < parseInt(minCross)) return { error: eval(name.replace("tg", "crud")).label+" "+moGlb.langTranslate("table need to have a minimum of")+" "+minCross+" "+moGlb.langTranslate("items") };
            return d;
        }
    };
    this.getSelected = function () {return eval(this.gridName).getSelectedRows();};
    this.setSelected = function (items) {    
        with(this) {
        
            if(moGlb.QoF) {
            
              gridName="mdl_wo_prl"+moGlb.QoF+"_tg_tg";
              moGlb.QoF=0;
              
            } else {
        
        
                var rn = eval(gridName).rowNumber();
                if(maxCross && items.length + rn > maxCross) {
                    moGlb.alert.show(moGlb.langTranslate("Maximum number of rows excedeed, data will be truncated"));
                    items = items.slice(0, maxCross - items.length - rn);
                }
            
            }
            
            eval(gridName).updateChecked(items);
        }
    };
    this.getSelectedIds = function () {
        with(this) {
            var codes = [], selected = getSelected();
            if(!moInit.admin) for(var i in selected) codes.push(selected[i].f_code);
            else codes = selected;
        }
        return codes;
    };   
    /**
     * Open a popup to select items to add to cross treegrids
     * @param {type} filter
     * @param {type} onselect
     * @param {type} oncancel
     * @returns {undefined}
     */
    this.addElements = function (filter, onselect, oncancel) {
        with(this) {
            var b_mod = name.multiReplace({'mdl_': '', '_tg': ''}).split('_');
            var mod = (b_mod[1] == 'parent'?b_mod[0]:b_mod[1]), ov_name = 'ov_'+mod.split("$")[0], overlay = '';
            moGlb.openPopup(ov_name, b_mod[0], b_mod[1], filter, onselect, oncancel);
        }
    };    
    this.removeSelected = function (pair_cross_only) { eval(this.gridName).deleteSelected(pair_cross_only, this.name.split('_')[2] == 'comment'); };
    this.resume = function (o) {
        with(this) {
            if(!o) {                
                var codes = getSelectedIds(), count = codes.length;
                if(count == 0) moGlb.alert.show(moGlb.langTranslate("Select one or more items"));
                else {
                    moGlb.loader.show();
                    Ajax.sendAjaxPost(moGlb.baseUrl+"/workorder/reopened-close", "f_ids="+codes.join(",")+"&f_module_name="+name+"&f_count="+count, name+".resume");
                    eval(gridName).jaxULock(codes, 0, 0);
                }
            } else {
                moGlb.loader.hide();o = JSON.parse(o);
                moGlb.alert.show(moGlb.langTranslate("There are")+" "+o.f_changed+"/"+o.f_count+" "+moGlb.langTranslate("elements modified")+(o.f_message?'\n'+o.f_message:''));
            }
        }
    };
    this.setPairCross = function (rows) { eval(this.gridName).saveJob(rows); };
    this.setDefaultData = function (o) {
        with(this) {
            var d = JSON.parse(o), q = [];
            if(d.length > 0) {
                with(eval(gridName)) {
                    AND.push({f: "f_code", z: "IN ("+d.join(",")+")"});asHy = 0;
                    ActRefresh();
                }
            }
        }
    };
    this.refresh = function () {
        with(this) {
            //completed = false;
            try { eval(gridName).ActRefresh(); } catch(ex) {eval(gridName).loadroot();}
            //updateListeners();
        }
    };
    this.reset = function () {
        with(this) {
            completed = false; selectors = {};
            unlock();
            with(eval(gridName)) {
                if(tgType == "moTreeGrid") asHy = (hierarchy?1:0);
                //try { actclearfilter(tgType == "moTreeGrid_light" ? 0 : 2); } catch(ex) {}
                if(!moInit.admin) {
                    //activeInspection = {name: "default", shared: 1}; eval(pname).setInspectionLabel('default');            
                    if(activeInspection.name != 'default') selectInspection(activeInspection.name, activeInspection.shared);
                    else try { actclearfilter(tgType == "moTreeGrid_light" ? 0 : 2); } catch(ex) {}
                } else { try { eval(gridName).actclearfilter(0); } catch(ex) {} }
            }            
            updateListeners(true);
        }
    };

    /**
     * force listeners to update their data or to reset them if clear is true
     * @param {type} clear
     * @returns {undefined}
     */
    this.updateListeners = function (clear) { 
        with(this) { 
            if(active && listenTo){ 
                for(var i in listenTo){
                    if(listenTo[i].indexOf('summary') !== -1 && !this.reloadSummary) continue;
                    try { 
                        eval(listenTo[i])[clear?'reset':'refresh'](name);
                    }  
                    catch(ex) {} 
                } 
            }
        }
        this.reloadSummary = true;
    }

    this.exportData = function (expFormat, detailed, params) {
        with(this) {
            if(!moGlb.isset(params)) params = {};
            if(!moGlb.isempty(params) && moGlb.isset(params.script)) params = moLibrary.exec(params.script, params);
            params.data = (eval(gridName).getData()).data;
            params.detailed = moGlb.isset(detailed) && detailed == 1,
            params.view = (eval(gridName).getView()).data,
            params.Tab = [ table, ftype ]; params.moduleName = name; params.format = expFormat;    
            try {
                var win = eval('exportFeedback_win');
            } catch(ex) {
                var container = mf$('exportFeedback');
                if(!container) {
                    var container = moGlb.createElement('exportFeedback', 'div', null, true);
                    with(container) { style.width = '400px'; style.height = '250px'; }            
                }
                moComp.createComponent("exportFeedback_win", "moPopup", {
                    contentid: 'exportFeedback', title: moGlb.langTranslate('Exportation progress'),
                    width: 400, height: 250,
                    onunload: function () {
                    //if(exportFeedback_win.isVisible())
                        moGlb.progressEnd();
                }}, true);
            }
            mf$('exportFeedback').innerHTML = "<div class='progress'></div>";
            eval('exportFeedback_win').show();
            Ajax.sendAjaxPost(moGlb.baseUrl+"/export/start", "params="+JSON.stringify(params), name+".exportCallback", true);
        }
    };
    this.exportCallback = function (o) { 
        mf$('exportFeedback').innerHTML = o; 
        moGlb.progressStart();
        Ajax.sendAjaxPost(moGlb.baseUrl+"/export/create", "", ""); 
    };
    this.setBookmark = function (clr, items) { with(this) { eval(gridName).setBookmark(getSelected(), moGlb.bookmarkColors[clr]); } };
    this.setPhaseBatch = function (wf, exit) {
        with(this) {
            if(!exit) {
                moGlb.loader.hide(); 
                try{
                var o = JSON.parse(wf);
                moGlb.alert.show(moGlb.langTranslate("There are")+" "+o.f_changed+"/"+o.f_count+" "+moGlb.langTranslate("elements modified")+(o.f_message?'\n'+o.f_message:''));
                }catch(ex){
                moGlb.alert.show(wf);
            }
            } else {                
                var codes = getSelectedIds();
                if(codes.length == 0) moGlb.alert.show(moGlb.langTranslate("Select one or more items"));
                else {
                    moGlb.loader.show();
                    //Ajax.sendAjaxPost(moGlb.baseUrl+"/"+eval(eval(pname).edit).etype+"/change-phase-batch", "f_ids="+codes.join(",")+"&wf_id="+wf+"&f_phase_id="+exit+"&f_table="+table+"&f_module_name="+name+"&f_count="+count, name+".setPhaseBatch");
                    Ajax.sendAjaxPost(moGlb.baseUrl+"/"+eval(eval(pname).edit).etype+"/change-phase-batch", "f_codes="+codes.join(",")+"&f_wf_id="+wf+"&f_exit="+exit+"&f_module_name="+name, name+".setPhaseBatch");
                    eval(gridName).jaxULock(codes, 0, 0);
                    ReverseAjax.count = ReverseAjax.delay - 1;
                }
            }
        }
    };
    this.result = function () {window.open(moGlb.baseUrl+"/export/"+format+"-exporter", "Export", "width=450,height=300,location=0,titlebar=0,status=0,scrollbars=0,resizable=0,menubar=0,toolbar=0");};
    this.modIn = function (res) {with(this) {eval(gridName).Reversajax(res); updateListeners();}};
    //this.summaryRefresh = function () {with(this) {if(active && listenTo) for(var i in listenTo) try {if(eval(listenTo[i]).type == "moModuleSummary") eval(listenTo[i]).refresh(name);} catch(ex) {moDebug.log(name+": "+ex);}}}
    this.newJob = function () {eval(this.gridName).newJob();};
    //this.newComment = function (data) {eval(this.gridName).newComment(data);};
    this.clone = function (recursive) {
        with(this) {
            if(typeof recursive != "boolean") {
                moGlb.loader.hide();unlock();refresh();
                var o = JSON.parse(recursive);
                if(moGlb.isset(o.message)) moGlb.alert.show(moGlb.langTranslate(o.message));
                return;
            }
            var etype = eval(eval(pname).edit).etype, codes = getSelectedIds();
            if(codes.length > 0) {
                moGlb.loader.show();
                Ajax.sendAjaxPost(moGlb.baseUrl+"/"+etype+"/clone-"+etype, "f_code="+codes.join(",")+"&clone_type="+(recursive?1:0)+"&f_module_name="+name, name+".clone");
            } else moGlb.alert.show(moGlb.langTranslate("Select items to clone first"));            
        }
    };
    this.createWo = function () {
        var nm = this.name;
        with(eval(this.gridName)) {
            if(arrCHK.length < 1) moGlb.alert.show(moGlb.langTranslate('You can to select at least one element'));
            else Ajax.sendAjaxPost(moGlb.baseUrl+"/workorder/create-wo", "f_code="+arrCHK.join(",")+"&f_module_name="+nm);
        }
    };
    this.selectInspection = function (iname, shared) {
        with(this) {
            activeInspection.name = iname;activeInspection.shared = (shared?1:0);
            moGlb.loader.show(moGlb.langTranslate("Setting up view, please wait..."));
            Ajax.sendAjaxPost(moGlb.baseUrl+"/inspection/get-inspection", "f_name="+iname+"&f_module_name="+name.replace('pp_', '')+"&shared="+shared, name+".setInspection");            
        }
    };
    this.setInspection = function (o) {
        with(this) {
            var insp = JSON.parse(o);
            moGlb.loader.hide();
            if(!moGlb.isset(insp.message)) {
                with(eval(gridName)) { SEL=-1;
                    with(insp) {
                    
                        //setData({id: f_id, name: f_name, data: insp.data});
                        if(!moGlb.isempty(bookmark)) for(var i in bookmark) setBookmark([{f_code: i}], bookmark[i], true);
                        else setBookmark([], "");
//                        setData({id: f_id, name: f_name, data: (insp.f_name.toLowerCase() == 'default' ? { Sort: [ {f: orderby, m: order} ], Filter: [], Search: [], Ands: [], Selector: {} } : insp.data)});
                        setData({id: f_id, name: f_name, data: insp.data});
                        setView({id: f_id, name: f_name, data: insp.view});                        
                    }
                }
                eval(pname).setInspectionLabel(activeInspection.name);
                updateListeners();
            }
        }
    };
    this.createInspection = function () {
        with(this) {
            try {eval("inspectionWindow");} catch (ex) {
                var div = moGlb.createElement("inspectionNameContainer", "div");
                with(div) {style.display = "none";setAttribute("class", "inspection");}
                var frm = moGlb.createElement("inspectionForm", "form", div, true);
                frm.onsubmit = function (e) {return false;}
                with(moGlb.createElement("inspectionNameField", "input", frm)) {setAttribute("type", "text");setAttribute("class", "default");}
                if(moGlb.user.level & moGlb.SHARED_VIEWS_CREATOR_LEVEL) with(moGlb.createElement("inspectionShared", "input", frm)) {setAttribute("type", "checkbox");
                frm.innerHTML += "<div>"+moGlb.langTranslate("set as shared view")+"</div>";}
                with(moGlb.createElement("inspectionNameBtn", "button", frm)) {                    
//                    onclick = function (e) {o.saveInspection(mf$("inspectionNameField").value, (mf$("inspectionShared")?mf$("inspectionShared").checked:0));}
                    innerHTML = moGlb.langTranslate("Save");
                }
                inspectionWindow = moComp.createComponent("inspectionWindow", "moPopup", {contentid: "inspectionNameContainer",
                    title: moGlb.langTranslate("Save as..."), width: 250, height: 100}, true);
                mf$(inspectionWindow.getContainerId()).appendChild(div);
            }
            var o = this;
            mf$("inspectionNameBtn").onclick = function (e) {o.saveInspection(mf$("inspectionNameField").value, (mf$("inspectionShared")?mf$("inspectionShared").checked:0));}
            mf$("inspectionNameContainer").style.display = "block";
            eval("inspectionWindow").show();
        }
    };
    this.saveInspection = function (iname, shared) {
        with(this) {
            if(!iname) moGlb.alert.show(moGlb.langTranslate("Insert a name for your view"));
            else if(iname.toLowerCase() == "default") moGlb.alert.show(moGlb.langTranslate("Default is reserved, choose another name"));
            else {
                eval("inspectionWindow").hide();
                moGlb.loader.show(moGlb.langTranslate("Saving view, please wait..."));
                var data = {f_module_name: name, f_name: iname, view: JSON.stringify(eval(gridName).getView().data),
                    data: JSON.stringify(eval(gridName).getData().data),
                    bookmark: JSON.stringify((eval(gridName).getBookmark()).properties), shared: (shared && ((moGlb.user.level & moGlb.SHARED_VIEWS_CREATOR_LEVEL) != 0))};                
                Ajax.sendAjaxPost(moGlb.baseUrl+"/inspection/save", "params="+JSON.stringify(data), name+".addInspection", true);
                mf$("inspectionForm").reset();
            }
        }
    };
    this.addInspection = function (o) { 
        with(this) {
            var insp = JSON.parse(o), index = parseInt(insp.shared)?0:1;
            if(!moGlb.isset(insp.message)) {
                eval(imenuName)[index].Sub.push({Txt: insp.name, Type: 0, Status: 1, Group: 1, Icon: "", Fnc: name+".selectInspection('"+insp.name+"', "+(insp.shared?1:0)+");"});
                if(eval(imenuName)[index].Sub.length > 0) {
                    eval(imenuName)[index].Type = 1;
                    eval(imenuName)[index].Disable = 0;
                }
                selectInspection(insp.name, insp.shared);
            } else moGlb.alert.show(moGlb.langTranslate(insp.message));
            moGlb.loader.hide();
        }
    };
    this.deleteInspection = function () {
        with(this) {            
            if(activeInspection.shared && moGlb.user.level > -1) moGlb.alert.show(moGlb.langTranslate("You cannot delete shared view"));
            else if(activeInspection.name.toLowerCase() == "default" && activeInspection.shared) moGlb.alert.show(moGlb.langTranslate("You cannot delete default views"));
            else {
                moGlb.confirm.show(moGlb.langTranslate("Are you sure you want to proceed?"), function () {
                    moGlb.loader.show(moGlb.langTranslate("Trying to delete view")+" '"+activeInspection.name+"'");
                    Ajax.sendAjaxPost(moGlb.baseUrl+"/inspection/delete", "f_module_name="+name+"&f_name="+activeInspection.name+"&shared="+activeInspection.shared, name+".removeInspection");
                });
            }
        }
    };
    this.removeInspection = function (o) {
        with(this) {
            o = JSON.parse(o); moGlb.loader.hide();
            if(!o.message) {
                moGlb.alert.show(moGlb.langTranslate("View deleted successfully!"));
                var j = (o.shared == 1 ? 0 : 1);
                for(var i in eval(imenuName)[j].Sub) {
                    if(eval(imenuName)[j].Sub[i].Txt == o.name) {
                        eval(imenuName)[j].Sub.splice(i, 1);
                        if(eval(imenuName)[j].Sub.length == 0) {
                            eval(imenuName)[j].Type = 0;
                            eval(imenuName)[j].Disable = 1;
                        }
                    }
                }
                selectInspection("default", 1);
            }
        }
    };
    this.getUnique = function () {return [];};
    this.count = function () {return eval(name+"_tg").count();};
    this.getFilters = function () {
        with(this) {
            with(eval(gridName)) {
                // AND sets summary filters to itself
                return { Sort: SORT, Filter: FILTER, Search: SEARCH, Ands: AND, Selector: SELECTOR, Like: LIKE, Hierarchy: asHy,
                    Tab: [Ttable, ftype, fname], module_name: crud, self: self, ignoreSelector: ignoreSelector//, Prnt: pp
                };
            }
        }
    };
    // Creating change phase and inspection menu
    this.createPhaseMenu = function (o) { this.createMenu(o, 'extras', 'createPhaseMenu'); };
    this.createInspectionMenu = function (o) { this.createMenu(o, 'inspect', 'createInspectionMenu'); };    
    this.createMenu = function (o, tp, fn) {
        with(this) {  
            if(tgType != "moTreeGrid") return;
            var sname = name.split('_'), mnname = 'mn_'+sname[1]+(sname.length>3?'_'+sname[2]:'')+'_'+tp;

            if(!moGlb.isset(o)) { // First step: get menu data                
                try { 
                    eval(mnname);
                    Ajax.sendAjaxPost(moGlb.baseUrl+"/menu/create-"+tp, "tg="+name+"&table="+table+"&type="+ftype+"&menu="+mnname, name+"."+fn);
                } catch(ex) { return; }                
            } else { window[mnname] = JSON.parse(o); } // Second step: fill menu with data
        }
    };
    
    this.createMenuChangePhase = function (data) {
        with(this) {
          
            if(tgType != "moTreeGrid") return;
            var sname = name.split('_'); 
        
            var code = eval('mdl_' + sname[1] + '_tg_tg').arrCHK;
            if(!moGlb.isempty(code)){
                var prms = "code="+code+"&tg="+name;
                if(typeof data.excludewfphases !== 'undefined') {
                     try { var jsn=JSON.stringify(data.excludewfphases); prms = prms+"&excludewfphases="+jsn; }catch(e){}
                }
                Ajax.sendAjaxPost(moGlb.baseUrl+"/menu/create-"+data['name'], prms, name+".createMenuChangePhaseDraw");
            }else{
                moGlb.alert.show(moGlb.langTranslate("No elements selected"));
            }
        }
    };
    
    this.createMenuChangePhaseDraw = function (data){
        if(data == "[]"){
            moGlb.alert.show(moGlb.langTranslate("Workorders' workflow are different"));
        }else{
            var jsn, sname=this.name.split('_'), pos=moGlb.getPosition(mf$('btn_'+sname[1]+"_chg_ph_cnv"));    // mdl_wo_tg => btn_wo_chg_ph_cnv   
            try { jsn=JSON.parse(data); }catch(e){ moGlb.alert.show(moGlb.langTranslate("Json error")+ ' ' +data); return;   }
            moMenu.Start(jsn, pos ,2);
        }
    }
    
    
    this.oncompletion = function () {
        with(this) {
            completed = true;
            var nm = name.split('_');
            eval('mdl_'+nm[1]+'_edit').checkCompleteness(name);
            if(parentCodes) { setSelected(parentCodes); parentCodes = []; }
        }
    };    
    /* Works on bookmarks only */
    this.copyRows = function () {
        with(this) {
            var rows = getSelected();
            if(!moGlb.isset(moGlb.notes)) moGlb.notes = [];
            if(rows.length == 0) moGlb.alert.show(moGlb.langTranslate("No elements selected"));
            else {
                moGlb.notes = [];
                for(var i in rows) moGlb.notes[moGlb.notes.length] = rows[i];
                moGlb.alert.show(moGlb.langTranslate("Rows copied"));
            }
        }
    };
    this.pasteRows = function () {
        with(this) {
            with(eval(gridName)) {
                if(moGlb.notes.length == 0) moGlb.alert.show(moGlb.langTranslate("No elements to paste"));            
                else { for(var i in moGlb.notes) { moGlb.notes[i].f_code = 0; setPairCross(moGlb.notes[i]); } }
            }
        }
    };
    this.setEditingCode = function (editingCode) { eval(this.gridName).editing = editingCode; };
    
    
    this.addNewAttach = function (){
    
      // multiAttach(this.name+"_tg");
    
    }
    
    
};
moModuleTreeGrid.prototype = new moModule();