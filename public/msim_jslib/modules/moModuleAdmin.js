var moModuleAdmin = function () {
    this.padding = 0;
    var props = '';    
};
moModuleAdmin.prototype = new moModule();

var moModuleAdminLayout = function () {
    this.noscroll = 1;
    var empty = '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":0,"wh":1307,"level":-1,"module":"module0","lock":0,"minv":36,"acc":0}]}';
    this.draw = function () { with(this) { if(mbody) { LAYOUT_cut.Edit(props?props:empty, mbody.id); } } };
    this.setData = function (data) { props = data.f_properties; };
    this.getData = function () { return { f_properties: LAYOUT_cut.Save() }; };
    this.reset = function () { props = ''; };
    this.modResize = function (l0, t0, w0, h0, layoutid) { this.baseResize(l0, t0, w0, h0, layoutid); this.draw(); };
};
moModuleAdminLayout.prototype = new moModuleAdmin();

var moModuleAdminComponent = function () {
    this.noscroll = 0;
    var uiType = '';
    this.draw = function () {
        with(this) {
            if(mbody) {
                if(uiType != 7 && uiType != 8) mbody.innerHTML = "<div id='admin-msg'>"+moGlb.langTranslate('Nor options nor buttons are supported for this type')+"</div>";
                else moComponent_adm.Edit(/*props*/mdl_component_edit.data.options, mbody.id);
            }            
        }
    };
    this.setData = function (data) {uiType = data.f_type_id; props = data[(uiType == 7 || uiType == 8) ? "options" : "buttons"]; };
    this.getData = function () { var res = {}; res[uiType == 7 ? "options" : "buttons"] = mdl_component_edit.data.options; return res; };
    this.reset = function () { props = ''; };
    this.modResize = function (l0, t0, w0, h0, layoutid) { this.baseResize(l0, t0, w0, h0, layoutid); this.draw(); };
};
moModuleAdminComponent.prototype = new moModuleAdmin();

var moModuleAdminMenu = function () {
    this.noscroll = 0;
    this.draw = function () { with(this) { if(mbody) { moMenu_adm.Start(mbody.id, (props?JSON.parse(props).menu:[])); } } };
    this.setData = function (data) { props = data.f_properties; };
    this.getData = function () { return { f_properties: moMenu_adm.gridtojsn() }; };
    this.reset = function () { props = ''; draw(); };
    this.modResize = function (l0, t0, w0, h0, layoutid) { this.baseResize(l0, t0, w0, h0, layoutid); this.draw(); };
};
moModuleAdminMenu.prototype = new moModuleAdmin();

var moModuleAdminTabbar = function () {
    this.noscroll = 0;
    var is_tbbr_main = false;
    this.draw = function () { with(this) { if(mbody) { moTabbar_adm.Edit(props, mbody.id, is_tbbr_main); } } };
    this.setData = function (data) { props = data.f_properties; is_tbbr_main = data.f_instance_name; };
    this.getData = function () { return { f_properties: moTabbar_adm.Save() }; };
    this.reset = function () { props = ''; };
    this.modResize = function (l0, t0, w0, h0, layoutid) { this.baseResize(l0, t0, w0, h0, layoutid); this.draw(); };
};
moModuleAdminTabbar.prototype = new moModuleAdmin();