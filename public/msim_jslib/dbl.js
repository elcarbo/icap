// local database  v 1.02
 
Dbl={ db:{}, active_db:"", last_tab:"",
      CMD:["CREATE_TABLE","DROP_TABLE","INSERT","UPDATE","DELETE","SELECT","TRUNCATE","SETVAR","GETVAR","DELVAR","SHOWFIELDS","SHOWTABLES","SHOWVARS"],
      AUTOSAVE:1,

list_db:function(){
  var l=Dbl.Get("Dbl.list_db");
  if(!l) l=[];
  return l;
},


reset_db:function(db_name){ 
with(Dbl){
  var r,i=1;
  if(db_name) i=del_db(db_name); 
  else { r,l=list_db();
         for(r=0;r<l.length;r++) del_db(l[r]);  } 
  return i;         
}},

del_db:function(db_name){
with(Dbl){
  if(!exist_db(db_name)) return false;  
  
  var r,m,l, atv=Get("Dbl."+db_name);
  if(atv==null) return false;
  for(r=0;r<atv.length;r++){ m=atv[r];
    Del("Dbl."+db_name+"."+m);
  }
  Del("Dbl."+db_name); 
  l=list_db();
  for(r=0;r<l.length;r++) { if(l[r]==db_name) { l.splice(r,1); break; } }
  Set("Dbl.list_db",l); 
}},

exist_db:function(db_name){
  if(!db_name) return false;
  var e=0, l=Dbl.list_db();
  if(l.indexOf(db_name)!=-1) e=1;  
  return e;
},


create_db:function(db_name){
with(Dbl){
  if(exist_db(db_name)) return false;
  var l=list_db();
  l.push(db_name);  
    Set("Dbl.list_db",l); 
    Set("Dbl."+db_name,[]); 
    db={};   
  return 1;
}},


save_db:function(db_name){
with(Dbl){
  if(!db_name || db_name==active_db) {
    var k, atv=[];
    for(k in db) { 
        Set("Dbl."+active_db+"."+k,db[k]); 
        atv.push(k);
    }
    Set("Dbl."+active_db,atv); 
  } 
}},


save_tab:function(tab_name){
with(Dbl){   
   if(!active_db) return false;
   if(!tab_name) tab_name=last_tab;
   if(!tab_name || !active_db) return false;  
   Set("Dbl."+active_db+"."+tab_name,db[tab_name]);  
   return 1;
}},



connect_db:function(db_name){
with(Dbl){
  last_tab=""; 
  if(!db_name) {            // disconnect
    active_db="", db={}; 
    return 1;            
  } 
  if(!exist_db(db_name)) return false;  
  
  var r,m,t, atv=Get("Dbl."+db_name);
  if(atv==null) return false;
  
  db={};
  for(r=0;r<atv.length;r++){
    m=atv[r];
    t=Get("Dbl."+db_name+"."+m);
    if(!t) t={};
    db[m]=t;
  }
active_db=db_name;
return 1;
}},



query:function(s,j){
with(Dbl){
  if(!active_db) { alert(moGlb.langTranslate("database not connected!")); return null; }   
  if(!s) {alert(moGlb.langTranslate("Query error!")); return null; } 

  s=jsTrim(s);
  
  var a=s.split(" "), b=a.shift(), c=b.toUpperCase(), t=a.shift();
  if(CMD.indexOf(c)==-1) { alert(moGlb.langTranslate("Query command error!")); return null; }
 
  return Dbl[c](t,a.join(" "),j);
}},


//-----------------------------------------------------------------------------

CREATE_TABLE:function(t,s){
with(Dbl){ 
 
  if(active_db && typeof(db[t])=="undefined") {  db[t]={};
                      
     var r,a,b,v,i=0;
         s=f_Replace(s, {'\\(':'','\\)':'',', ':','} );                       
         a=s.split(",");
 
         db[t]={"field":[],"blank":[],"fan":{},"fna":{},"type":[],"data":[],"count":0,"erased":[] };            
         
         for(r=0;r<a.length;r++){
         
          b=(a[r]).split(" ");
          if(b.length<2) { alert(moGlb.langTranslate("field information error")); delete db[t]; return false; }
           
          v=""; 
          if(typeof(b[2])=="undefined"){
            if(b[1]=="int") v=0;
            if(b[1]=="real") v=0.0;
          } else { 
              v=b[2];
              if(b[1]!="string") v=eval(v);
          }
          
          if(b[1]=="inc"){ if(i) { alert(moGlb.langTranslate("auto increment fields error!")); delete db[t]; return false; } i++; }
 
          db[t]["field"].push(b[0]);
          db[t]["type"].push(b[1]);  
          db[t]["blank"].push(v);        
        } 
        
      f_index_fields(t);
                            
   } else return false; 
 
a=[];  
for(k in db) a.push(k);   // save table list in locastorage for the  active_db
Set("Dbl."+active_db,a);    
   
last_tab=t;   
save_tab(t);
return 1;  
}},


SHOWFIELDS:function(t){
with(Dbl){ 
  if(!t && last_tab) t=last_tab;
  if(!f_verify(t)) return false;
return CloneA(db[t].field);  
}},


SHOWTABLES:function(){
with(Dbl){ 
  if(!active_db) return [];
    var k, atv=[];
    for(k in db) { if(k!="vars") atv.push(k); } 
  return atv;
}},

DROP_TABLE:function(t){
with(Dbl){ 
  if(!f_verify(t)) return false;
  delete db[t];   
  Del("Dbl."+active_db+"."+t);
  
var r,l=Get("Dbl."+active_db);  
for(r=0;r<l.length;r++) { if(l[r]==t) { l.splice(r,1); break; } }
Set("Dbl."+active_db,l);

return 1;   
}},


TRUNCATE:function(t){
with(Dbl){ 
   if(!f_verify(t)) return false;      
   with(db[t]) data=[], count=0, erased=[]; 
  // f_index_fields(t);
   
last_tab=t;
if(AUTOSAVE) save_tab(t);    
return 1;  
}},



INSERT:function(t,s,j){
with(Dbl){
  
  if(!f_verify(t)) return false; 
  if(typeof(j)=="undefined") {  
    s=jsTrim(s); 
    v=s.substring(0,4);
    if(v.toLowerCase()=="set ") s=s.substring(4);   
    j=f_setToJson(t,s);    // string to json 
  }
    var r,m,typ=3;
    if(typeof(j.length)=="undefined") typ=4;
    else { if(typeof(j[0].length)!="undefined") typ=2;
           else  if(typeof(j[0])!='object') typ=1;  }

    m=(typ>2)?1:0;
    if(typ&2) { for(r=0;r<j.length;r++) f_insert_row(t,j[r],m);  }
    else f_insert_row(t,j,m);
    
last_tab=t;
if(AUTOSAVE) save_tab(t);     
return 1;
}},


DELETE:function(t,s){ 
with(Dbl){ 
  if(!f_verify(t)) return false;  
  last_tab=t; 
  
  var a,p,n,v,k,h,i,Dd;  
  if(s==""){           // delete all and preserve count 
    n=db[t].count;
    TRUNCATE(t);          
    db[t].count=n; 
    if(AUTOSAVE) save_tab(t);         
    return 1;
  }

  s=f_Replace(s,{ "\\n":"" } );

  v=(s.substring(0,6)).toLowerCase();
  if(v!="where ") { alert(moGlb.langTranslate("delete where error!")); return false; }
  s=s.substring(6);
 
  a=f_where(t,s);
  
    Dd=db[t]["data"];
    for(n=0;n<a.length;n++){  i=a[n];       
      db[t].erased.push(i);
      Dd[i]=null;
   }
   
if(AUTOSAVE) save_tab(t);   
return a.length;
}},


UPDATE:function(t,s,j){
with(Dbl){ 

  if(!f_verify(t)) return false;
    
  var D,v,y,w,a,p,k,i,z, m=0,
      st="", wh="", tj=(typeof(j)=="undefined");

  s=f_Replace(s,{ "\\n":"" } );
  s=jsTrim(s);
  
  w=(s.substring(0,6)).toLowerCase();
  v=w.substring(0,4);
  
  if(w=="where ") m=1;
  else {  m=2;
  if(v=="set " ) s=s.substring(4); 
      if(!tj) { alert(moGlb.langTranslate("update SET/json value together.")); return false; }
  }
  
  if(m==1 && tj) { alert(moGlb.langTranslate("update no json found.")); return false; }                              
  if(m==2){  // SET   
    z=s.toLowerCase();
    p=z.indexOf("where ");
    
    st=s; 
    if(p>0) { st=s.substring(0,p);
              wh=s.substring(p+6); }              
    j=f_setToJson(t,st);          
  }
  
  if(m==1) wh=s.substring(6);        
  a=f_where(t,wh);                                                          
  D=db[t]["data"];  
  for(r=0;r<a.length;r++){ p=a[r];           // p=pos row
    for(k in j){                             //  k=field name
      i=AtoN(t,k);                           // i=pos col     
      y=db[t]["type"][i];
      if(y=="inc") continue;       //  no update if auto increment 
      v=j[k];
      if(y=="int") v=parseInt(v);
      D[p][i]=v;
    }        
  }   
last_tab=t; 
if(AUTOSAVE) save_tab(t);
return a.length;
}},



SELECT:function(t,s,mode){   // mode NUMERIC NUM N    default ASSOC
with(Dbl){ 
 
  if(!f_verify(t)) return false;
  s=f_Replace(s,{ "\\n":"" } );
  
  var r,i,ss,n=0,f="",fields=[], wh="", a=[], md,
      rw, k, Dd=db[t]["data"], resp=[], rs=[];
  
  if(!mode) mode="";
  mode=mode.toUpperCase().substring(0,1);
  md=(mode=="N")?0:1;
   
  ss=s.toLowerCase();
  n=ss.indexOf(" where ");
  if(n==-1) { alert(moGlb.langTranslate("no WHERE in query!")); return []; }
  
  f=s.substring(0,n);
  k=f.substring(0,4).toLowerCase();
  if(k=="get ") f=f.substring(4);
  
  f=f_Replace(f,{" ":""});
  
  if(f=="*") fields=SHOWFIELDS(t);    //   * = all fields
  else fields=f.split(",");
 
  wh=s.substring(n+7); 
  a=f_where(t,wh,1);     // a[...] result
  
  if(fields[0]!="count(*)"){
      n=fields.length;
      for(r=0;r<a.length;r++){
        rw=Dd[a[r]];
        rs=[];
        for(i=0;i<n;i++){       // put value in rs array
          k=AtoN(t,fields[i]);
          if(typeof(k)=="undefined") { alert(moGlb.langTranslate("field name error!")); return []; }
          rs.push(rw[k]);
        }      
        rw=md?{}:[];
        for(i=0;i<n;i++){       // NUMERIC / ASSOCIATIVE   
          if(md) rw[ fields[i] ]=rs[i]; else rw[i]=rs[i];    
        }
        resp.push(rw);   
      }
  } else resp=a.length; 
  
  
last_tab=t; 
return resp;
}},



SETVAR:function(k,v){ 
  if(!Dbl.active_db) return false;
  if(typeof(Dbl.db["vars"])=="undefined") Dbl.db["vars"]={};
  if(typeof(v)=="undefined") { 
    var a=k.split("="); 
    k=a[0], v=a[1];
  }
  Dbl.db["vars"][k]=v;
  Dbl.save_tab("vars"); 
},

GETVAR:function(k){
    if(!Dbl.active_db) return false;
      var t=Dbl.Get("Dbl."+Dbl.active_db+".vars");
      if(t==null) return null;
      Dbl.db["vars"]=t;
    if(typeof(Dbl.db["vars"][k])=="undefined") return null; 
    return Dbl.db["vars"][k];
},


DELVAR:function(k){
    if(!Dbl.active_db) return false;
    if(typeof(Dbl.db["vars"])=="undefined") return null;
    if(typeof(Dbl.db["vars"][k])=="undefined") return null; 
    delete Dbl.db["vars"][k];
    Dbl.save_tab("vars");
},


SHOWVARS:function(){
with(Dbl){
    if(!active_db) return false;
    if(typeof(db["vars"])=="undefined") return null;
    var k,a=[];
    for(k in db["vars"]) a.push({ "key":k, "value":db["vars"][k] } ); 
    return a;    
}},

//-------------------------------
// index  fAN  fNA  

f_index_fields:function(t){
with(Dbl){
    var r,v, D=db[t];     
    for(r=0;r<db[t]["field"].length;r++) { 
        v=D["field"][r];
        D.fan[v]=r;
        D.fna[r]=v;   
    }   
}},

AtoN:function(t,A){ return Dbl.db[t].fan[A]; },        // position field    field Name to pos number
NtoA:function(t,N){ return Dbl.db[t].fna[N]; },



f_verify:function(t){
with(Dbl){
   return (active_db && typeof(db[t])!="undefined")?1:0;  
}},



f_insert_row:function(t,a,m){   // a=array row,   m 0=array  1=associative 
with(Dbl){

  var k,n,y,v,i,p, ii=0, D,Dd;
      D=db[t], Dd=D["data"];
  
  if(D.erased.length) i=D.erased.shift();
  else i=D["data"].length;
                                                                              
  n=parseInt(D.count)+1;
      
  Dd[i]=CloneA(D["blank"]); 
              
    for(k in a){           // k field name  
     p=m?AtoN(t,k):k;     // field pos 
     y=D["type"][p];     // field type
     v=a[k];            // value
     
     if(y=="inc" || y=="int") { v=parseInt(v); if(!v) v=0; }   
     if(y=="inc") { v=(v<n)?n:v; D.count=v; ii=1; }    // auto increment correction
     Dd[i][p]=v;
    }
  
    p=D["type"].indexOf("inc");                   // auto increment if not specify 
    if(p!=-1 && !ii) { D.count=n; Dd[i][p]=n; } 
}},

 
  
// parsing WHERE -> return array of index true    // md=1 SELECT(order by, limit)   0=UPDATE-DELETE
f_where:function(t,s,md){    s=Dbl.jsTrim(s);

  var v,r,str=[],as=[],st="",m=0,i=0, ss="", 
      where="", orderby="", limit="";

  // substitute string 'string' with #
  for(r=0;r<s.length;r++) { v=s.charAt(r);  
    if(v=="'"){ 
      if(m) { str.push( [st,i,r+1] ); st=""; m=0;  } else m=1,i=r;      
    continue; }  
    if(m) st+=v;
  }
  
  if(str.length){ i=0; 
    for(r=0;r<str.length;r++) {
      ss+=s.substring(i,str[r][1])+"#";
      i=str[r][2];      
    }
    ss+=s.substring(i);
    s=ss; 
  }
     
if(md){   // SELECT
    
  s=s.replace(/\s{2,}/g, ' '); // toglie spazi doppi
  ss=s.toLowerCase();
  
  as=ss.split(" limit ");
  if(as.length>1) limit=as.pop();
  ss=as.join(" limit ");
  
  as=ss.split(" order by ");
  if(as.length>1) orderby=as.pop();
  
  i=as[0].length, m=orderby.length, r=limit.length;
  
  where=s.substring(0,i);
  orderby=s.substring(i+10,i+10+m);
  limit=s.substring(s.length-r);
  
  where=Dbl.f_Replace(where,{"not in":"notin", "not like":"notlike", "NOT IN":"notin", "NOT LIKE":"notlike"});
  
} else where=s;
 
 
// calculate f o v
 s=where+" ";
var c,l, a=[{}], ao=[], n=0,m=0, fov="", vi=0, b="=><!&| ",k=0;                
 
  for(i=0;i<s.length;i++){ c=s.charAt(i);             // m 0 = field   1=OP   2= value  3 AND / OR      // alert(c+" | "+r+" | fov: "+fov)
    if(!fov && c==" ") continue;   
         
    if(m==3) { c=c.toUpperCase();  
             if(c!=" ") { if(c=="A" || c=="N" || c=="D") fov="AND"; else fov="OR"; }    
             if(fov && c==" ") { ao.push(fov); fov=""; m=0; n++; a[n]={}; }
             continue;    
    }     
        
    if(m==2){ if(c=="(" || c==")") vi++;
              if(fov=="#") { fov="'"+str[k][0]+"'"; k++; }
              if(c=="#") {  fov+="'"+str[k][0]+"'"; k++; continue; } 
              if(c==" " && (!vi || vi>1)) { a[n]["val"]=fov; m=3; fov="";  } else fov+=c;
              continue; }

    r=b.match(c);
   
    if(m==1) { if(c==" " || c=="#" || (c>="0" && c<="9") || c=="-" || c=="+") {  
                    a[n]["op"]=fov; fov=(c!=" ")?c:"";  m=2; vi=0;
               } else fov+=c;  }
     
    if(!m) { if(r){ a[n]["field"]=fov; m=1; fov=(c!=" ")?c:""; } else fov+=c; }          
  } 
 
 
//for(r=0;r<a.length;r++) alert(r+"\n"+a[r].field+"\n"+a[r].op+"\n"+a[r].val);
 
return Dbl.find_where(t,a,ao, orderby, limit);     
},  


/* parsing WHERE clausole

a  = ["AND", "OR", ...]
ao = [ { field:field0, op:OP0, val:Value0 },
       { field:field1, op:OP1, val:Value1 },
     ...]    
*/

find_where:function(t,a,ao, orderby,limit){   
with(Dbl){

  var r,n,k,f,o,v, b, S0,S1=[], rr=a.length,
      or=["",""], lm=[],lmi,lmn;

  if(orderby) { or=orderby.split(" "); if(or.length>1) or[1]=or[1].toLowerCase(); else or[1]=""; }
 
      S0=""; n=ao.length;      
      for(r=1;r<rr;r++) S0+="(";  
      
      for(r=0;r<rr;r++){
          f=a[r].field;
          
          if(typeof(a[r].op)=="undefined") S0+=f;
          else {
            o=a[r].op.toLowerCase(),
            v=a[r].val;
            k=AtoN(t,f);
            if(typeof(k)=="undefined") { alert(moGlb.langTranslate("field name error!")); return []; }
            f="RW["+k+"]";
            S0+=f_forsolve(f,o,v);
          }

          if(r) S0+=")";  
          if(n>r) S0+=(ao[r]=="OR")?" || ":" && ";  
      }
      
     //alert(S0);
     S1=f_ifsearch(t,S0, or[0]); 
    
  // ASC DESC or[1]
  if(or[1]=="desc") S1.reverse();
  
  // limit 
    if(limit) { lm=limit.split(","); lmi=parseInt(lm[0]); n=S1.length;
          if(lm.length>1) lmn=parseInt(lm[1]); else lmn=n;
          if((lmi+lmn)>n) lmn=n-lmi;        
    S1=S1.slice(lmi, (lmi+lmn) );  
    }

return S1;
}},

 

f_ifsearch:function(t,s, or){   // or = field to order by
with(Dbl){

  var r,D=db[t], Dd=D["data"], RW, resp=[], vresp=[], k=0, o=-1, p,v;
  
  s="if("+s+") k=1";
  if(or) o=AtoN(t,or);  // order by field
try{ 
  for(r=0;r<Dd.length;r++){ RW=Dd[r]; 
    if(RW==null) continue;
    k=0; eval(s);
    if(k) { 
      if(o==-1) resp.push(r);
      else { v=RW[o];                       // ORDER BY              
             p=f_binSearch(vresp,v);           
             if(!p[1]) while(vresp[ p[0]]==v) p[0]++;  // if v exist splice after equal         
             vresp.splice(p[0],0,v);
             resp.splice(p[0],0,r);
    }}} 
}catch(e){ alert(moGlb.langTranslate("query error!")); return []; }
 return resp;
}},


f_forsolve:function(f,o,v){
  var b,s,n,cnd="",sao;
  if(o=="=") o="==";
  if(o=="in" || o=="notin") {     
    
      cnd="(";   
          v=v.substring(1,v.length-1);
          b=v.split(",");
          if(o=="in") { p="==", sao=" || ";  }
          else { p="!=", sao=" && "; }
          s="";
          for(n=0;n<b.length;n++) { 
                cnd+=s+f+p+b[n]; s=sao; }     
     cnd+=")";
     
 } else {
      
     if(o=="like" || o=="notlike") {        
         sao=(o=="like")?">":"==";
        cnd="("+f+".indexOf("+v+")"+sao+"-1)";
 
     } else cnd+=f+o+v; 
 }
return cnd;
},

f_setToJson:function(t,s){  
with(Dbl){           
      var p,f,v,i,c,n,k, j={};         
      s=jsTrim(s);         
      while(s.indexOf("=")!=-1){
      
             p=s.indexOf("=");  if(!p) return j;
             f=s.substring(0,p);
             f=jsTrim(f);
             
             k=AtoN(t,f);
             if(typeof(k)=="undefined") { alert(moGlb.langTranslate("field name error!")); return {}; }
             
             s=s.substring(p+1);
             
             // value number | string
             v="",n=0; 
             for(i=0;i<s.length;i++){ c=s.charAt(i);
                if(c=="'" && !n) { n=1; continue; }
                if(c==" " && !v) continue;
                if(c=="'" && n) { n++; continue };
                if(c==","){  break; }                                  
                v+=c; 
             }          
             
             
          v=jsTrim(v);
          if(db[t]["type"][k]!="string") j[f]=eval(v);
          else j[f]=v;
          
          s=s.substring(i+1);                    
      }
  return j;
}},

 
 
//------------------------------------------------------------------------------
// localstorage

Set:function(k, val){
    var s;
    if(typeof(val)=="string") s=val;
    else s=JSON.stringify(val);
    
    try { localStorage.setItem(k,s); } catch (e){
	 	 if(e==QUOTA_EXCEEDED_ERR) alert(moGlb.langTranslate('Quota exceeded!')); }    
},

Get:function(k){
    var l=localStorage.getItem(k);
    if(l==null) return l; 
    return JSON.parse(l);
},

Del:function(k){
  var g=localStorage.getItem(k);
  if(g==null) return g;  
  localStorage.removeItem(k);
},


//------------------------------------------------------------------------------
// functions

f_Replace:function(str,s){  var k; 
  for(k in s) { str=str.replace(new RegExp(k,'g'), s[k] );   }
return str;  
},

CloneA:function(A){  var C=[];
for(D in A) C[D]=A[D];
return C;
},


f_binSearch:function(A,x) {  
  var i, p=0, q=A.length-1; 
  if(q<0) return [0,1];      // p[0]=pos in index_sort | p[1] 0=exist  1=not found 
  while(p<=q) { 
    i=parseInt((q+p)/2);
    if(A[i]==x) return [i,0];
    if(A[i]>x) q=i-1; else p=i+1; 
  }      
if(A[i]<x) i++;      
return [i,1];
},

jsTrim:function(s){
  return s.replace(/^\s+|\s+$/g,""); 
}

} // end Dbl

 
 

/* Dbl     (local dabase)

--------------------------------------------------------------------------------
properties:
- AUTOSAVE               //  1 active at (INSERT, UPDATE, DELETE)
- active_db              // connected database name (without Dbl.)
- last_tab               //  table of last query

method:

-list_db();               // return array of db in localstorage
-reset_db(db_name);       // if !db_name  reset all databases in localstorage!
-exist_db(db_name);       // return false if not exist
-create_db(db_name);      // if not exist create new database
-save_db();               //  save connected db in local storage
-save_tab(last_tab);      //  save last_tab or indicated table in own localstorage db  
-connect_db(db_name);     //  restore db_name (if exist)  return 1 if restored  | if no params=disconnect

-query(string, json);

-SHOWFIELDS(table)    // return array with fields name
-SHOWTABLES()        // return array with tables name

-SETVAR(key,value);
-GETVAR(key);
-DELVAR(key)

--------------------------------------------------------------------------------

CREATE_TABLE table (if not exist or different!)
DROP_TABLE table
TRUNCATE table
INSERT table | json   

DELETE table WHERE field op value 
UPDATE table WHERE field op value | json

SELECT table field1, field2, ... WHERE field op value ORDER BY field ASC/DESC LIMIT f,n


SETVAR var_name = value; 
GETVAR var_name;
DELVAR var_name;

INSERT table SET field=value, field=value, ...
UPDATE table SET field=value, field=value, ... WHERE field op value

 

db={

    tablename:{
          
              field:[.....],
                fan:{},             // field => assoc to numb
                fna:{},   
                  
              type:[],             // int, string, real, inc (auto increment)
              
              blank:[],           // riga default
              
              data:[
                      [ f1, f2, ... fn ],
                      [ row2 ],
                      ...    
              ],

              count:0                 // counter auto increments
              
              erased:[]
          },
          
         
     vars:{     
              varname1: value1,
          
              varname2: value2
     }      
          
 
}


CREATE_TABLE tony (progre int, f_code int, f_parent int, f_title string, f_description string 'default', f_active int 999)


INSERT JSON type:
                  jsn1=[1, 1, 0, "root", "first tree element",1];
                  
                  jsn2=[
                    [1, 1, 0, "root", "first tree element",1],
                    [2, 2, 1, "ch2", "",0 ],
                    [3, 3, 1, "ch3", "ciao" ,1],
                    [4, 4, 3, "sub3", "zz"] 
                  ];
                  
                  jsn3 = [ { f_code:5, f_parent:2, f_title:"sub2", f_description:"angy"  },
                           { f_code:6, f_parent:3, f_title:"sub3", f_description:"casa"  }
                          ];
                  
                  jsn4 ={ f_code:5, f_parent:2, f_title:"sub2", f_description:""   };




field Operators: '>=','<=','!=','=','>','<','&','|','not in','in','like','not like'


ALTER_TABLE table ADD f_session_id int 0 AFTER f_active   // default to the end ||  AFTER  field   ||   FIRST     
ALTER_TABLE table CHANGE f_session_id f_session_id string  
ALTER_TABLE table DROP f_session_id



//-----------------------------
fix

features
- ALTER_TABLE aggiungere / togliere field in tabella
- inserire parentesi in where
- distinct


*/