/*Inclusione librerie di google.maps
 * Principale
 * MarkerCluster
 * InfoBubble
 * */
//GOOGLE API KEY LEGATA ALL'ACCOUNT unp.tablet@gmail.com
document.write('<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAabIw1jfJoHhDvgiF38VxbSh6fu6TXhE4"></script>');
document.write('<script type="text/javascript" src="/public/msim_jslib/map_js/lib_g_map/markerclusterer.js"></script>');
document.write('<script type="text/javascript" src="/public/msim_jslib/map_js/lib_g_map/infobubble.js"></script>');

//var script = '<script type="text/javascript" src="http://google-maps-' + 'utility-library-v3.googlecode.com/svn/trunk/infobubble/src/infobubble';
/*if (document.location.search.indexOf('compiled') !== -1) {
  script += '-compiled';
}
script += '.js"><' + '/script>';
document.write(script);*/
    

/************************************/
/*RICORDA: in google.maps ['D'] = long e ['k'] = lat*/
var mainsimMap = {
    map: null,
    markers: [],
    markerCluster: null,
    directionsService: null,
    directionDisplay: null,
    
    checkConnection: function(){
        if (navigator.onLine) {
            return 'ok';
        } else {
            return 'no';
        }
    },
    
    //type(div) = string; 
    //type(zoom) = int; 
    //type(mapTypeId) = {google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBIRD, google.maps.MapTypeId.TERRAN}; 
    //opzionale - type(center) = array[float] lenght(2)   [0]latitudine - [1]longitudine
    generate_map: function (div, zoom, center, typeId) {
        //Controlli----------------------------------
        if(center == undefined) center = [0,0];
        if(typeId == undefined) typeId = google.maps.MapTypeId.ROADMAP;
        
        if(center.length != 2 || (isNaN(center[0]) || isNaN(center[1]))) {
            console.log('Error: in generate_map function, the parameter "center" is not an array of float');
            return;
        }
        if(isNaN(zoom)){
            console.log('Error: in generate_map function, the parameter "zoom" is not an integer');
            return;
        }
        //--------------------------------------------
        var styles = [{
            stylers: [
              { lightness: 10 },
              {saturation: 10},
              {gamma: 1.3}
            ]
          }];
         var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
         //mainsimMap.map.setOpacity(6);
        var myOptions = { 
            zoom:zoom,
            mapTypeId: typeId,
            center: new google.maps.LatLng(center[0],center[1]),
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
              }
        }
       mainsimMap.map = new google.maps.Map(document.getElementById(div), myOptions);
       mainsimMap.map.mapTypes.set('map_style', styledMap);
       mainsimMap.map.setMapTypeId('map_style');
      // mainsimMap.map.MapTypeStyler.saturation = -100;
      //console.log(mainsimMap.map);
       mainsimMap.display_map();
    },
    
        display_map: function (){
            mainsimMap.directionsDisplay = new google.maps.DirectionsRenderer();
            mainsimMap.directionsDisplay.setMap(mainsimMap.map);
        },
    
    /*dalla lista di coordinate genera i rispettivi marker sulla mappa
     * array_markers = [{ "fcode":"-", "loc":"genova", "lat":44.4056499, "lon":8.946255999999948, "job":"", "tm_job":0 },{ "fcode":"0001", "loc":"milano","lat":45.4654219, "lon":9.18592430000001, "job":"electrician", "tm_job":1800 },
     * OP - type(bubble) = boolean;
     * OP - type(icon) = boolean;
     * OP - type(draggable) = boolean;
     * OP - type(bubble_aspect) = [{"style":"color:green;","shadow":0,"padding":0,"backgroundColor":"transparent","borderWidth":0,"borderColor":"#2c2c2c","arrowSize":40,"minHeight":40,"disableAutoPan":false,"hideCloseButton":true,"arrowPosition":25,"class":"phoney"}]
     * */
    generate_list_of_markers: function(array_markers, bubble, icon, draggable,bubble_aspect){
        mainsimMap.clear_element_on_map();

        
        //Controlli----------------------------------
        if(bubble == undefined) bubble = false;
        if(icon == undefined) icon = '';
        if(draggable == undefined) draggable = false;
        if(bubble_aspect == undefined) bubble_aspect = '';
        
        if(typeof(bubble) != 'boolean' || typeof(icon) != 'string' || typeof(draggable) != 'boolean'){
            console.log("Error: in generate_list_of_markers function, one of optional parameters hasn't a correct type");
            return;
        }
        //-------------------------------------------
        
        var marker;
        for(var x = 0; x < array_markers.length; x++){
            marker = mainsimMap.marker_generator('marker_' + x,new google.maps.LatLng(parseFloat(array_markers[x].lat), parseFloat(array_markers[x].lon)),array_markers[x].loc,bubble, /*icon,*/ draggable,array_markers[x].text,bubble_aspect,array_markers[x].max_priority);
            mainsimMap.markers.push(marker);
        }
    },
    
        /*generatore di un singolo marker*/
        marker_generator: function(id,latlng,title,bubble/*,icon*/,draggable,text,bubble_aspect,priority){
            var color = mainsimMap.define_color_of_priority(priority);
          
            var marker = new google.maps.Marker({ position: latlng,
                map: mainsimMap.map, 
                title: title,
                icon: color[0]['icon'],
                draggable: draggable
            });
            if(bubble) mainsimMap.add_cloud_on_marker(marker,id,text,bubble_aspect,color[0]['color'])
            return marker;
        },
    
            /*collega ad un marker la sua rispettiva nuvoletta*/
            add_cloud_on_marker: function(marker,id,text,bubble_aspect,text_color){
                var boxText = document.createElement("div");
                boxText.id = id;
                boxText.innerHTML = text;
                var myOptions = null;
                
                if(bubble_aspect == ''){
                    boxText.style.cssText = "color:red;";
                    myOptions = new InfoBubble({
                            content: boxText
                        });
                    }
                 else{
                     if(parseInt(text)<10)var arrowPosition = 25;
                    else var arrowPosition = 45;

                    bubble_aspect[0].style = "color:" + text_color + ";";
                    
                    
                     boxText.style.cssText = bubble_aspect[0].style;
                     
                     
                     myOptions = new InfoBubble({
                            content: boxText,
                            shadowStyle: bubble_aspect[0].shadowStyle,
                            padding: bubble_aspect[0].padding,
                            backgroundColor: bubble_aspect[0].backgroundColor,
                            borderWidth: bubble_aspect[0].borderWidth,
                            borderColor: bubble_aspect[0].borderColor,
                            arrowSize: bubble_aspect[0].arrowSize,
                            minHeight: bubble_aspect[0].minHeight,
                            disableAutoPan: bubble_aspect[0].disableAutoPan,
                            hideCloseButton: bubble_aspect[0].hideCloseButton,
                            arrowPosition: (bubble_aspect[0].arrowPosition != '') ? bubble_aspect[0].arrowPosition : arrowPosition,
                            backgroundClassName: bubble_aspect[0].class
                        });
                 }        
                myOptions.open(mainsimMap.map,marker);
            },
    
    /*generatore di cluster in base alla lista di marker*/
    cluster_generator: function(zoom_remove_bubble){
        //Controlli----------------------------------
        if(zoom_remove_bubble == undefined) zoom_remove_bubble = false;
        if(typeof(zoom_remove_bubble) != 'boolean'){
            console.log("Error: in cluster_generator function, the parameter 'zoom' is not a boolean");
            return;
        }
        //-------------------------------------------
        
        mainsimMap.markerCluster = new MarkerClusterer(mainsimMap.map, mainsimMap.markers, {
            gridSize:40,
            minimumClusterSize: 2,
            calculator: function(markers, numStyles) {
                return {
                    text: markers.length,
                    index: numStyles
                };
            }
        });
        
        if(zoom_remove_bubble) mainsimMap.zoom_event();
    },
    
        /*in base allo zoom applicato sulla mappa vengono resi visibili o invisibili le nuvolette*/
        zoom_event: function(){
            google.maps.event.addListener(mainsimMap.map, 'zoom_changed', function() {
                var zoomLevel = mainsimMap.map.getZoom();
                
                if (zoomLevel <= 6) {
                    mainsimMap.disabilited_infobubble(true);
                } else {
                    mainsimMap.disabilited_infobubble(false);
                }
            });
        },
            /*funzione che rendere visibili/invisibili le nuvolette*/
            disabilited_infobubble: function(disable){
                if(disable) var dis = 'none';
                else var dis = 'block'
                for(var m = 0; m < mainsimMap.markers.length; m++){
                    document.getElementById('marker_'+m).style.display= dis;
                }
            },
    
    /*ottenere un marker in base a
     * position
     * coordinates [0]lat, [1]lon
     * loc
     * 
     * filter = 'position' || 'coordinates' || 'loc'
     * value => if filter = position => number
     *          if filter = coordinates => array[2] of numeric
     *          if filter = loc => string 
     * */
    get_markers_filter: function(filter, value){
        var markers_filter = [];
        for(var i = 0; i < mainsimMap.markers.length; i++){
            if(filter == 'position' && i == parseInt(value)) markers_filter.push(mainsimMap.markers[i]);
            if(filter == 'coordinates' && (mainsimMap.markers[i]['position']['D'] == value[1] && mainsimMap.markers[i]['position']['k'] == value[0])) markers_filter.push(mainsimMap.markers[i]);
            if(filter == 'loc' && mainsimMap.markers[i].title == value) markers_filter.push(mainsimMap.markers[i]);
        }       
        return markers_filter;
    },
    
    /*
     * Generare un percorso tra due punti
     * marker_1 = primo marker di google.maps
     * marker_2 = secondo marker di google.maps
     * fun = funzione che orriene il distanza e durata del percorso
     * OP - draw_path = booleano che determina la rappresentazione grafica del tracciato
     * OP - text = booleano che stabilisce se il risultato deve essere sottoforma di stringa (dur: 1h e 30 min || dist: 1km) o un numero (dur: secondi || dist: metri)
     */
    generate_path_from_to: function(marker_1, marker_2,fun,draw_path,text,see_from_to){
        //Controlli----------------------------------
        if(text == undefined) text = false;
        if(draw_path == undefined) draw_path = true;
        if(see_from_to == undefined) see_from_to = false;
        if(typeof(draw_path) != 'boolean' || typeof(draw_path) != 'boolean' || typeof(fun) != 'function'){
            console.log("Error: in generate_path_from_to function, one of optional parameters hasn't a correct type");
            return;
        }
        //-------------------------------------------
        
        
        mainsimMap.directionsService = new google.maps.DirectionsService();
        var request = {
            origin:marker_1.position, 
            destination:marker_2.position,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        mainsimMap.directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                if(draw_path) {
                    mainsimMap.directionsDisplay.setDirections(response);
                    mainsimMap.directionsDisplay.setOptions( { suppressMarkers: see_from_to } );
                }
                if(text) fun([{"distance":response['routes'][0]['legs'][0]['distance'].text, "duration":response['routes'][0]['legs'][0]['duration'].text}]);
                else fun([{"distance":response['routes'][0]['legs'][0]['distance'].value, "duration":response['routes'][0]['legs'][0]['duration'].value}]);
                
            }
        });
    },
   
    /*Ottenere le coordinate conoscendo l'indirizzo
     * type(address) = string
     * type(fun) = function
     * */
    get_coordinates_by_address: function (address,fun){
        //Controlli----------------------------------
        if(address == '' || typeof(address) != 'string'){
            console.log("Error: in get_coordinates_by_address function, the 'address' parameter is wrong");
            return;
        }if(typeof(fun)!='function' || fun == undefined){
            console.log("Error: in get_coordinates_by_address function, the 'fun' parameter is wrong");
            return;
        }
        //-------------------------------------------
        
        
        var geocoder = new google.maps.Geocoder();
        
        geocoder.geocode( {'address': address}, function(results,status) {
            if (status == google.maps.GeocoderStatus.OK) {
                fun([{"long":results[0].geometry.location.lng(),"lat":results[0].geometry.location.lat()}]);
            } else {
                alert(moGlb.langTranslate("Problem during the search of the address: ") + status);
            }
        });
        
    },
    
    //Cancella tutti i marker e oggetti collegati sulla mappa
    clear_element_on_map: function(){
        if(mainsimMap.markers.length > 0){
            for(var i=0; i < mainsimMap.markers.length; i++){
                document.getElementById("marker_"+i).remove();
                mainsimMap.markerCluster.clearMarkers();
                mainsimMap.markers[i].setMap(null);
             }
             mainsimMap.markers = new Array();
         }
    },
    
    //priority è un int
    define_color_of_priority: function(priority){
        moGlb.priorities[priority]
        
        var i1 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDSYh/hwqgQAAAs5JREFUSMe9VsFKG1EUPTPNphKGCYKl3RXk4aqkIApiMUJXSjFBsbRBMqKi4MbgB8R8gOgm0FLFkRIlISEGMSuhKYWAEki1G3lVcNVSaWsIpV2VdBVNZu6bzETp3c2dmXe4955z7pPQJFJ5qIoKza1gIBZjfuqb+Xm+86uC95Uy9DEfylbnSVZA9x5g5dVrpsFBzM1y/dsXhEXAJODeIfxbCZbBDeLlcx4Y7sGOMS8bE/vHWLopGABsJVhm/xhLlhXuH2NpY5NFRIeEJnj4z298rM/dbYN38y1bEf0zGeLRp4+ugSU7bRwf5YMjfchbVZQtwJdMs3fN2ivVCJLZZZfGDzu7PIjOHEhOWvmBY4MiWuAZ94z5UJYB4NPn3lsBA4AnDJNzs1w35msYcioP9fTEhNcSWD2oMXd6colUHqqsqNComd2UpdQZigrN5VYwYHwhIghFLJHeRvqQDy425twKBmSjXXV2eeDEDLYSLLN3CL9IRvXPsRjzm4QvSX+/ioRsJXIqb9Qs6TQiX8UthYz/HLYAm60cUbhc5s6YAKvVO/dF9mS1Gah8m9usANnoCpQJAMBwD3aog0WyAIC1dbZg3JWuShlZoFH8uSK0oW7oFCjAGxwovkxXnStCi2835n5+x6YEAMFFVhWZbSuzEy2D+DKXZAAIvuAm72vvQKRVJlLLoDYOGQCo9q2tswWRg1hFrgiN4kFtzlcsHQvwh5SDZAvw2gXLFuCNb7MNk6zqzr4CDPTjnGptMs1KdpwmW4A3mWYlqpWBfpyTOhzqhk6Zd2aXXVpVKgILTfCwUTLkko286a1Scxgf5Y9H+hoNWQQ2N8t1ahGT1hadOZCoSpNpVqonUq4IzQmY5c3b6to4PcVXKSepSYxivS3Aa8cwM8/JbdsRoNWcms23ZcCaXbV3IGJs4/QUX/1xgWirNminWt/RBc6OLnCWLcDn9P9/XVQ9AZw0kAkAAAAASUVORK5CYII=';
        var i2 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDSYQr8IquwAAAppJREFUSMe9VkFrGlEQ/nbx0iCLJZBAbj00UEJbDdEHGtktFQrJQYX8gPXQe/wF6i8w9xyyvQfMHiIEDN0QFJ4VrQYvDYGcCpW2ESntsT1touu8dVdD5+asMx8z8833RsIMO7YQUkLQgwrUs3OWof7z7i0/+TXCxWgIY0/D0C2f5Aa0uoZy7YLp8GEplRvfviIvAiYBT5vINLusggUs9ppnd2M4cfplp6PWQ3FRMABodlml1kPRtcJaD8VLzgqiJIkoz//5jc/jvidLCNc/sbIoJsl4KfXqAVjy0sbNDf4mHYflVpHZgNbus4+z2ivZBLm6ZncUAZLryPlp5eUXHFFEe/mcP93TMJQBYHUN5ccAA4DkOnIplRtOv40hiaorvefSIqQpHLK/VJWyEoJOzWxRllI5lBD0QFCB6vwgIghFLNG+peOwCoeTvqACVXbKVSLK837EoNllldMmMqI1Gv99ds4yU4vv3LPxxG5LTvmpXLKXeRxbCOGRTMZ/Nk+As54ckQUC052RCeqmRfLk9jJQ/qXg9AbITlWw6myfCt6N4YRKLFoLKldK5UZgNIQJTC5/tQV9ZwsGBQp4U6BqCzrvTPp+fscHyU2G5p2dm1zKAMAifEqkl1dQmJeJ1GNgj0MGAKp9Vp3tixRkViup58me8z1Lwy/4M0pBzAbCXsHMBsK8w46c/vHc94DZbdxSrW33WceL0pgNhNt91qFamd3GLbmHO1swtAQ/cAZdXbM7t0pFYIkozztXRvJzJmxu8Eg6PinIIjDRxSD7ORPafdYZJ1K1Bd0PmOvl7XY22m2nVIlFeI5ivSfAB8WYZp6fa9sXoNucZs13bkBbrpZXUHC2UUvwgx8DlOaVQS/Vat0BbroD3JgNaH7j/wEUKTcR/f/7FwAAAABJRU5ErkJggg==';
        var i3 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDSMdrASiQwAAAqNJREFUSMe9VkFrGlEQ/nYVikG3pkKWpjlIA+kttRBykIiW9vBIDirkWlj/Qf0Fsb/A/INYcg3oHiLvIMQieAiC0VsDBg9Ni9IYuykVD0t7qamu7z13NXRuzvrmY2a++WYkzLCTMvyKH5pXQbR1TRKs/6w/o4WfBj4ZfeT2Y+iL4kkiIHUV2XaHaHBgQZXmOl+R5gEzAU/PkegNSR4L2JNHNLm3jYLVL1sdpSYyi4IBQG9I8qUmMsIMS01kvv0gB7wgqo+mB79wMe7zLCHUuSNZ3punj+mHt5v/gCU7ZVRc9HU8jLIoI72KmGGSs1nllUYEGbjILYsAkQ2knJSycokjFtE8Jl3ej6EvA4C6iuxDgAFAZAOpoEpzU+34iyHxsnsXodIipDmukN+sLGXFD43Vs0VZyoqh+KG5vQqinbvJDzyCsIjFm7d4GOXjyqTPqyAqW+VK9dG0EzHoDUn+9BwJ3hiN/25dk8TU4FvnbDywaMhZflYs2U4/Tsrw44FMxn82W4CzVg7P3O7pysgM6sZ58iTaDCz/khfRKUCrKny5Ie9Zj/e2UWAF5o0FK1ZQpTm30YcOTA5/sQZtdws5FihgT4GKNWg3Awubv+OjJJKheXsnkksZAAIeOiXSgRUczMtE1jIYteO+PI0uWs3P5Lnd/ohLSY7GfZsv6NXLFaxPsPTqEm9YCqJXEbILplcRsoJZY98DJnfQZpXWMEndjtLoVYQMk9RZpUzuoM2cw90t5NYC9HBKE13kVpQpD0z10bS1JZKTM0Fx0Vfx8KQg88B4F4Ps5EwwTFIfX0XFGjQnYMLLW3Q2jsrOUqWAh6ZYomELkEdzp9e2I0BRn2b1d+71FA/jwmPSZRaD1wL00GPSZTtgc5leRazRRavRRUuvIub0/R/LzDwoahnN3QAAAABJRU5ErkJggg==';
        var i4 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDSI7ZxIW/wAAAdFJREFUSMe9Vs9KAkEc/ly8JLIsBAWd7AEqr500qnMGPcD2BvYE2RPsI9gDLGwGGpjhhrKCGbgGHoS1+55Eoo52Cmxnfu7MrPXBXHZnft/8vvn9SyEGtgtDN2BmdRQOd1Hi7em94+5jjuf5DLcXRcygAtuF0ZmgulhgIbM6E1RtF4YUWb2PkixRdNX7fDUYtEaoJCX7Wa0RKlH7qSjZ8R6uqcs8+rj6+sRw+dtGBvnTA1jUmac33Jzss8QrZax5KMYpU/NQFJbXdmFQASAbbFSg/Qok3iYVMiF7lHdICMpLTTdgRjff93CUlJBnQzdgwpvCEfWOF1ir8i2615vCYT42hyjLFgOKtDlEmXFGNAXiklw0VTTRuoo1QcM/Q4hQteWk06wyGid0z3iHGy84pwxT/zJZFISqwl+lRWeCKtdIY8AWA1k0BjDJDIgttgrTApU6GgA8vOIyemhzi+6LcdjeYfsj885+iEB5TIiR0g8RMBudLnJE882LktU85Hk2nC5ywrcTfU+KLFal9hiWrKcUGdUIhMcEHilFJj0xUKTLElFPoDyeUDNqewyLkj5x0aC8SDRtq0agavoIlyuejO0xrHU2aO7Y4IcI/BCByEQexTeb6aokOrEZIQAAAABJRU5ErkJggg==';
        var i5 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDSEltjB4XwAAAmFJREFUSMdjZCAA5k+RF+DlYU7g4WGyd9e7E4BNzc5LKhu+fPl38POXvwsScx5+wGceIz6LxERZ+r0M7yYwkAC2nVde8Or1n0JcFmO1cPkcxYAIu/vrGSgAKw4pBkam3N+ALs6MLrBukVJDqPX96QwUAh35DxH6pkqMK9e/P4DTh+sWKTUEWtyrx2XI2mNKhT9+/ruALMbBzmQQbHWvH5ee9SeUGoPi7jVgDcb/txj+Y8NLZio4EPLRkpkKDrj0L5+jGICRQLAp3LpSeT6pQbl1pfJ8bGbNnyIvwMDAwMDEwMDAICbK0o8ttXmH300k1ULv8LuJ284rL0AXh9nBOH+KvECC28P3GMlXDXeWIQb8v8XwH11swS55QSZeHmaMfLZ0v4IjpakUmxm8PMwJTDw8TPboEjHpDw7gyp8EEwQeM3h4mOwZ0A1Ys0CpgFjLCFm6ZoFSAbpaJnRF6PkMBvCVPLjksJnFREx8wJI0NQATA50BURYSqnJwAWZmRgGCFvLyMPvjKv3x1QzYxLk4MXMA1qIIl8GkZAtY5scoKrEZsnKeYgKlcbVynmICzkoAX2FLDsBVGcDjcNURRYxCWkiQpZ5cC7FVBhjxfP2Y4F1S4oeUoLx+TPAuRio9fZHfGVsJsmSmggGxli2ZqWAQZnMfow7FZjZO1xEbn0tmKhiQUs7Cwcalyv04mhkGpFqGqyIgupmAzVJclpHcPMFlKXIQ4YoCctpC8GYjNgM3LlXuxxX0FBcauHxBVgIhJbkTsoyU7EN0cYUtGDcuVe6nZgWNtYV9/Zjg3evHBO8S0yJHBwA1BpTQFhsl0wAAAABJRU5ErkJggg==';
        var i6 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDRwR/WzEVAAAAxpJREFUSMe9Vl9IU2EUP/dupItNZ5RYFPfeXaletA37Y0Jo9GIkuP5QOhTvDKShDw586E19FzRKNCG3uGEaszlQ6i0jMP9M5gikB+e96yXpoV2tsCJdT4v57fvu7lXpwF7O9+387jnf7/zOoSCLDbcwVovJIJhz6MrqkhUn7s7rD8UT339tv/22ueVvHooravEoNaDCPGPvtTMxAXTYVJT3f9n44yUBYwFHWjlnfbkUhD3Y81nuuqtfmkD9BtQx3m7run1eGoA9Wslxpa6UsVEvZhPTxAzH221dN8pWO0lBAmGb9+fv7aV0X+4B2n7r7Gov6T8vF23dN/tWuzIA1cr4bIa93DggT6tlJHrYqoYK+U228lIpgrgvxRM4AtT0xNx6SjnZwftwRPO9Ywqah+IKDQBQmGfs3Q8wAICanph7Ksr7UX8KgyJlRzWSW0aLJUVI4rKkLSaDgHuzvbIUF8NiMgi0OYeuRA9IBBlp5ZxJEZLpv5FWDqs+uBjmHLqSRuUqELZ59YhBfbkUJIGisapLVpw0egnts/TApPKRznCxaC3vMdzCWGGfjIb/bJoAs40ckhloypoV0GIy1JLkSW0y4PwHMR1A4aSI1PQ4ppLGEK75p6K837ixuR0CgB2Ao22cUPdIypAnV7804epHP0bCZj3axgnoWeLH1lNKTYZ2+3ZqckkDAIzNcRkifchs7NwtE3HDIOOdl7sLYlplS81G2zgBjbPcXRDLYOnC5/wrOAURPaxdK5joYe13Lkg+1J8e+x9g06As40rbUCFHtCiN6GHtDRVyBFfKpkFZxi5RgXllycHx1tNHE+Xpfgezfr+4iA0Fw8qaHrBA2Oate5jJduyagL5DUoQkrryih7Xj7k528D6s+mAbfCYROsfz7MmixA6A0hPKvVPHuOj4gvIxRRDXRfmVnvVEdY0grY2hCN8HAFDriLWjZ2NznBsnGpoAU1ngmKdn29YFqEYKZIdxNA7IS1kniBbAYFhZY44wDz59PZyLMjgU4fver+RdFR7HZS2xdK+CooetKitafwIAsLiWfzfbRo7aX0coj2XOQzQ3AAAAAElFTkSuQmCC';
        var i7 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDRstnUIuFAAAAutJREFUSMe9Vk1IG1EQnn0b1JRNNG2RnprdTQ9toSWlFHqLpRdLBdMfWpWIGwUhmEMCvat3QQuKVlBTIjYWrQoVeqs9tdAWPbUXk914qRRk408bDcb0FInPeZtdlQ7sZd6+72N+3jfDQRkb73TXOOy8IlQSX/3Gqh/758OFK/M7eweftrP5WPtoOmOExxkR1Tpt/Q/1pAIWbNHlif3e2o+yiFHCqS7J37yrzsEp7E2V9KhlSJ2n/TztmI3IPc/+qsNwSruxn2m66ZO5t1/0JWaEsxG55/F2qpsFMiPI0d3cwUqpr6qCeJ/upPpZd9455N4nA6meY4RGaZysEO+1DmtLRhHFQ2JdIKd9LJdertggwXxaxxqgoS8ZtJLK9y88E1ijTfBuV/toOkMAAGqdtv6zIAMAaOhLBhddnhjtL3JwrOi4MfaTMWOFDihgURKHnVewmp22SzEMh51XiFBJfPQBq0GmuiR/oQMKpd9Ul4SqD4YhVBIfoeVqRpCjVsSgeVedY5HSWPUbq35C/0S/s1JgVvpYZxgWMVOP8U53DZyREfjPZoqw3MhhGU+4mrKEDjvfyJIno8mA+c8hL4DQqtC4mYxgl1uG1HkMmDWGMKxFlydm28oeLADAkcefCEtK06Aaw0hbjs1QFY06EZYUyB490//kX3NGMnTS2hnJJQEAmLZLx0T6vGDrPmknYsOgWI7D9PzodSWvremy2fqwLBGWlOdZdaLU9/OyK3W9W/cc6dKvv6rvYwoSD4les2TxkOilyWjsQ8K2EU3DUhvIactmlCYeEr2BnLaMpbJtRNPQd9g0qMYWqj0D9KVgPq0bRcoimxHkKF0SzsqaMFkh3mod1lbMkLE2BmJlTQjktOXSUZQIS4oVMsPN22htLKYdU6VpuxTERMMUIavNrW7blgiN6lSuvugEMUM49y2z7r7jfrnmvFh1dU+/S6f3877zgfIqrZnBsrwKxkNi3e1Lm2MAAN/XqzvKbeS0/QPEtmnw581EywAAAABJRU5ErkJggg==';
        var i8 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDRsFqPeG7gAAAr9JREFUSMe9Vk1oE0EUnp0EYyRp0hyCIjSzuxePEW+9tOK1YERBDcRuowRCCybSe5J7ITlYWqQmqwtpi5ZG6F299CLSnoqXTTaBUukhtbVSlbbpqSXMvpndTcQHe3k7833M+/neE5CFlVORoN/rUnwePBJY0GPQmf1ncu3wz+nnn0cnavJV8wcPT+ARhQfcxVC5riAH1k5K6u7BcZZFDBJWJ8UYWTRWUR9mPCb34rONGu3HtGMlI+X7JUMIIbJorK5kpDz3hSsZKX/tbSPHAtlOiNnff083u32XL+Hoda1RZN3ZeSIW7pfqeRMhL4z6w8jtxJzxifciLU1G5eXmR1vhLaciwfWQ0KG/tWm54jSUa9NyBcIqpyLBixyGB9xFqNrGZvQJp4RjM/pEOymptP+cQyinIsEb71t79IHhdkfop2jWQ0KH9n17MDSI/V6XAuWs3yqFMPxel4J9HjxC/2AVSHVSjNG5qU6KoPpAGD4PHsG0XG0nxKwTMSCLxiqLlMYKLOgxU+PTfdYNzGtyyA9hYTv5OC/pf2EY/WezRWg1cljmwkLQktDvdd1lyRNvMkD+K0AHCGvTcoWeeaymhyqVNYag5m8nJRUfHJ1+oA8uTYng0I3PNmrD7Y7Q/bHIIIy9XydvBJ4M9Zo7nlxihBBqxYlJpEM+d67XSoSGgSnPW4VB3a5s8WxpSlRonK3CoG6q0i87gTuQgmhpErVLpqVJdKhqmGZoN/YF4fi8YUChlZebG3aURkuTqLzc3IBCOT5vGMyLtRdyEZrYvJdqaRKF7rx7LmX6WhMgUhaZ4/WERdpdSFCBWJFx1wjW2rirSCWEEAqrdVPIWnEy8ehlQ+2J8PwVUOU52bYdEfIqkNphbibmjE0rLMGJXIV87hwdxl1FKrUPjwt2ZdDxKqilyeitq/uvEULo6/fAU6uNnLYzbryXQtwgKPMAAAAASUVORK5CYII=';
        var i9 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDRofTI5O1QAAArVJREFUSMe9Vs9rGlEQfvt2s6tVo/YQPASi5taToX+ALb0Gq6SgWG3WFAKSHBR6j7kH9JBgCDRZYiMWuqyE3pueS9FTb8YNFBp60KRtqOvPngzm+fbtrobOzVnffMzMN98MBTTscH3BYTPTvJWD/sZxK4j7z8NXpvIfpf/599+esHZwcUWKR5GA5maZ7PU7hQcGzB7jhJ+/umk1YCxgccMTHIgdCUxh1MpMKLpXL6N+iDrElDczLRgAAAzEjiSmvBlihmLKm2mV2ltqQdgwm261+9VRn4mFvvb7dlbtjSnCbq/kzjNjgKQy9kPM03hePiNlVEi6n0Cp+0mrvNSQINxpr4kjwPJOLWGklB/fLB7hiKYEaOfawcUVBACAuVkmex9gAACwvFNL2GOcgPqHGJRadi8vv1PTkObENT/AZQltZprH9WxaluJi2Mw0z1g56G8gH9QIgiOW2rzF8/LZiWv+js/KQT9E5YoNs2kjYjAQO1JxwxNUG6PR343jVnBs8NE5Gw1MGnKcHxcL6unH4fqCA9yTQfCfTReg1spRMxpSDk1Am5l+riZPpM2A8z/goH8MEFWFm6KSwj2O7tXLuMBqY4GLZY9xAoWjO/1iJhHZrQvT9Kq06eF7HzpHqBhQJBmatHckuYTDjMbuFCuzNWl2uGUwbMetQH/bdtYqeYtXb3+MlHIpeXP+aKu5eIelX37Yn+EUpJB0+/SCFZJuHwqGxr4FXN2XZVxpodSt6FGaQtLtg1K3givl6r4sY+cwslsXLFEuhz7iTntNUqZqYGyYTaMtoYycCf0QsxTPy1U9YGoXAzRyJkCpWxldRaVND28EjHh5k87GYdlxqqQlGtQkNDd6bRsCJPVJq78Tr6d4Xq4qAdqJY7AlyuWUAO3UA6Y7Q/TCfuy6fgsAAF8v7a+1LnLU/gEx8FqqxlUMpgAAAABJRU5ErkJggg==';
        var i10 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwPDRk7W6D5xwAAAt5JREFUSMdjZCAA5qXJC/ByMifwsDPZH574JACbGtt8mQ1ffv47+Pn73wVJsx5+wGceIz6LxPhY+o9NfprAQAKwypVe8OrTn0JcFmO1cFm2YsCVOc/XM1AAdFIkA6Om3t+ALs6ELrC2QKmBUssYGBgYrsx5vn5tgVIDXh+uLVBqODv9WT0uQwwzpAp//Pp3AVmMg43J4PyMZ/249BhnSjUGT7jXgGEhvmDUTJJwjJ3+4AA+Hy3OVHC4Pu/FfkLBywhLIHcWvnyPLQH49NxNJCUot5Qoz8eW0FTixQWTZj38wMTAwMAgxsfSTw3LGBgYGHx67iZa5UovQBeH2cGIy3dtP38wUpJoqtg5/mPzJRMvJ3MCtjijNJViM4OXkzmBiYedyR5dAlcCWZatGFDFzvEfGS/LVsRa+mAzg4edyZ4JvbgyzJAqJKUwuDLn+XpclqKbdXjikwCMjI+ez5ANxpfJsYljM4uJmPiYlyYvwEAlwMRAZ0CUhYSqHFyAmYlRgKCFvJzM/riKJ3w1AzZxLiw5gBFbUYQr02NLqbiqIWyZ3ypXegHLp+//NjIwMKBYuCJHMSFiyn2M4glqMKpjpt7H6usVOYoJl2Y/RxF7//XvQkZ8xRC5cYevuGRiYGBg0EuVxCikhXhY6slNidgqA1g8w4PnWqPg3SVt35WIjR9cABqU85HFYqo472nVv1dGSaWnn/M7YytBFmcqGBBr2eJMBQN0y9DNhlsYP+PBA2xBe33ei/PElDSLMxUMrs97cR5bUMbPePAAaz6MmHJ/gVm29AR0TXcWvnyPz6e4LDPMkCpEjxJGUpoJmkkShrHTH1wgxjJcLQYmUpoJ1+e9OI9cFa3IUUwgxTK8LW98zUZYsJ+a+rQAXU4vVTIRW6FBlIW4kjmprW2SLMQXT4Til2wLYcWVEA9LPXowmmVLT3j35U8jscUgyU3BxZkKDsYSH+cyMDAwnH3Bn0yoRY4OAPDkaGhc3tXqAAAAAElFTkSuQmCC';
        //console.log(priority);
        var color = {'#5b7326':i1,'#7f9b37':i2,'#a3c348':i3,'#ffff00':i4,'#ffda00':i5,'#ffa300':i6,'#ff6300':i7,'#c61201':i8,'#a5181c':i9,'#7a0708':i10};
        
        return [{"color":moGlb.priorities[priority].color,"icon":color[moGlb.priorities[priority].color]}];
    }
    
};
