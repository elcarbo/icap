/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
window.onload = function (){
    
    //console.log(parent.moGlb.GET_MAP);
    if(parent.moGlb.GET_MAP == 1){
        mainsimMap.generate_map('map', 7, [44.4125,8.944980]);

        get_type_wo();
        get_priority_wo();
        get_owner_wo();

        filter();
    }
}


function get_type_wo() {
    Ajax.sendAjaxPost(moGlb.baseUrl + "get-type-for-filter",'','result_types');
}

function get_priority_wo() {
    //Ajax.sendAjaxPost(moGlb.baseUrl + "get-priority-for-filter",'','result_priority');
   
    select_priority.options[select_priority.options.length] = new Option(parent.moGlb.langTranslate("Priority (All)"),'');
    for(var pr in moGlb.priorities){ 
        select_priority.options[select_priority.options.length] = new Option(parent.moGlb.langTranslate(moGlb.priorities[pr].label),pr);
    }
}

function get_owner_wo() {
    Ajax.sendAjaxPost(moGlb.baseUrl + "get-owner-for-filter",'','result_owner');
}

function result_types(x){
    var obj = JSON.parse(x);
    select_type.options[select_type.options.length] = new Option(parent.moGlb.langTranslate('Type (All)'),'');
    
    for(var t = 0; t < obj.length; t++){
        select_type.options[select_type.options.length] = new Option(parent.moGlb.langTranslate(obj[t]['f_type']),obj[t]['f_id']);
    }
}

/*function result_priority(x){
    var obj = JSON.parse(x);
    select_priority.options[select_priority.options.length] = new Option("Priority (All)",'');
    
    for(var t = 0; t < obj['options'].length; t++){
        select_priority.options[select_priority.options.length] = new Option(moGlb.langTranslate(obj['options'][t]['label']),obj['options'][t]['value']);
    }
}*/

function result_owner(x){
    var obj = JSON.parse(x);
    select_owner.options[select_owner.options.length] = new Option(parent.moGlb.langTranslate('Owner (All)'),'');
    
    if(obj!=null){
        for(var t = 0; t < obj.length; t++){
            select_owner.options[select_owner.options.length] = new Option(parent.moGlb.langTranslate(obj[t]['f_owner']),obj[t]['f_owner']);
        }
    }
}

function filter(){
    var type = '';
    var priority = '';
    var owner = '';
    
    if(mf$('select_type').options[mf$('select_type').selectedIndex] != undefined){
        type = mf$('select_type').options[mf$('select_type').selectedIndex].value;
        priority = mf$('select_priority').options[mf$('select_priority').selectedIndex].value;
        owner = mf$('select_owner').options[mf$('select_owner').selectedIndex].value;
    }
    
    Ajax.sendAjaxPost(moGlb.baseUrl + "filter-wo",'f_type_id='+type+'&f_priority='+priority+'&fc_owner_name='+owner,'draw_wo_on_map');
}

function draw_wo_on_map(result){   
    var dati = JSON.parse(result);
    
    var bubble_aspect = [{"style":"","shadowStyle":0,"padding":0,"backgroundColor":"transparent","borderWidth":0,"borderColor":"#2c2c2c","arrowSize":10,"minHeight":55,"disableAutoPan":false,"hideCloseButton":true,"arrowPosition":"","class":"phoney"}];
    
    mainsimMap.generate_list_of_markers(dati,true,'',false,bubble_aspect);
    mainsimMap.cluster_generator(true);
}
