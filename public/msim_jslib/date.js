/**
 * Return the number of the year week for the date instance
 * @param void
 * @return int week number
 */
Date.prototype.getWeek = function () {
    var M=[0,31,59,90,120,151,181,212,243,273,304,334], 
    g=this.getDate(),m=this.getMonth(),y=this.getFullYear(),
    b=((!(y%4)&&y%100)||!(y%400))?1:0,    
    n=g+M[m]+((b&&m>1)?1:0), 
    j=(new Date(y,0,1)).getDay();
    if(!j) j=7;      
    var k=(j>4)?(j-2):(j+5), w=parseInt((n+k)/7);     
    if(!w) return (new Date(y-1,11,31)).getWeek();     
    var d=(new Date(y,11,31)).getDay();     
    if(d<4 && n>(365+b-d)) return 1;
    return w;
}

/**
 * Format the date following the format in input
 * // Week day
 *	"{l}" A full textual representation of the day of the week Sunday through Saturday
 *	"{D}" A textual representation of a day, three letters Sun through Sat
 *	"{D2}" A textual representation of a day, two letters Su through Sa
 *	"{N}" ISO-8601 numeric representation of the day of the week, 1 (for Monday) through 7 (for Sunday)
 *	"{S}" English ordinal suffix for the day of the month, 2 characters, st, nd, rd or th. Works well with j
 *	"{w}" Numeric representation of the day of the week, 0 (for Sunday) through 6 (for Saturday)
 *	"{z}" The day of the year (starting from 0), 0 through 365
 *
 *	// Month day
 *	"{j}" Day of the month without leading zeros 1 to 31
 *	"{d}" Day of the month, 2 digits with leading zeros 01 to 31
 *
 *	// Week
 *	"{W}" ISO-8601 week number of year, weeks starting on Monday
 *
 *	// Month
 *	"{F}" A full textual representation of a month, such as January or March January through December
 *	"{m}" Numeric representation of a month, with leading zeros 01 through 12
 *	"{M}" A short textual representation of a month, three letters Jan through Dec
 *	"{n}" Numeric representation of a month, without leading zeros 1 through 12
 *	"{t}" Number of days in the given month 28 through 31
 *
 *	// Year
 *	"{L}" Whether it's a leap year true if it is a leap year, false otherwise.
 *	"{o}" ISO-8601 year number. This has the same value as Y, except that if the ISO week number (W) belongs to the previous or next year, that year is used instead. Examples: 1999 or 2003
 *	"{Y}" A full numeric representation of a year, 4 digits Examples: 1999 or 2003
 *	"{y}" A two digit representation of a year Examples: 99 or 03
 *
 *	// Time
 *	"{a}" Lowercase Ante meridiem and Post meridiem am or pm
 *	"{A}" Uppercase Ante meridiem and Post meridiem AM or PM
 *	"{B}" Swatch Internet time 000 through 999
 *	"{g}" 12-hour format of an hour without leading zeros 1 through 12
 *	"{G}" 24-hour format of an hour without leading zeros 0 through 23
 *	"{h}" 12-hour format of an hour with leading zeros 01 through 12
 *	"{H}" 24-hour format of an hour with leading zeros 00 through 23
 *	"{i}" Minutes with leading zeros 00 to 59
 *	"{s}" Seconds, with leading zeros 00 through 59
 *	"{u}" Microseconds Example: 654321
 *
 *	// Timezone
 *	"{e}" Timezone identifier Examples: UTC, GMT, Atlantic/Azores
 *	"{I}" Whether or not the date is in daylight saving time 1 if Daylight Saving Time, 0 otherwise.
 *	"{O}" Difference to Greenwich time (GMT) in hours Example: +0200
 *	"{P}" Difference to Greenwich time (GMT) with colon between hours and minutes Example: +02:00
 *	"{T}" Timezone abbreviation Examples: EST, MDT ...
 *	"{Z}" Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive. -43200 through 50400
 *
 *	// Full Date/Time
 *	"{c}" ISO 8601 date 2004-02-12T15:19:21+00:00
 *	"{r}" RFC 2822 formatted date Example: Thu, 21 Dec 2000 16:01:07 +0200
 *	"{U}" Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
 *	"{ts}" Milliseconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
 * @param format string date format
 * @return string the formatted date
 */
Date.prototype.format = function (format) {
    function oneTwoDigits(n) {return (n < 10 ? "0" + n : n);}
    function amPm(h, capital) {return (h < 12 ? (capital ? "AM" : "am") : (capital ? "PM" : "pm"));}
    function getEnglishOrdinalSuffix(day) {
        switch(day % 10) {
            case 1:return "st";
            case 2:return "nd";
            case 3:return "rd";
            default:return "th";
        }
    }
    function h24to12(h) {
        if(h == 0) return 12;
        if(h < 13) return h;
        return h - 12;
    }
    
    with(moGlb) {
        return format.multiReplace({
            // Week day
            "{l}": weekDays[this.getDay()],                  // A full textual representation of the day of the week Sunday through Saturday
            "{D}": weekDays[this.getDay()].substr(0, 3),     // A textual representation of a day, three letters Sun through Sat
            "{D2}": weekDays[this.getDay()].substr(0, 2),    // A textual representation of a day, two letters Su through Sa
            "{N}": "",                                                 // ISO-8601 numeric representation of the day of the week, 1 (for Monday) through 7 (for Sunday)
            "{S}": getEnglishOrdinalSuffix(this.getDate()),                                                 // English ordinal suffix for the day of the month, 2 characters, st, nd, rd or th. Works well with j
            "{w}": this.getDay(),                                      // Numeric representation of the day of the week, 0 (for Sunday) through 6 (for Saturday)
            // "{z}": this.getDayOfYear(),                                // The day of the year (starting from 0), 0 through 365

            // Month day
            "{j}":  this.getDate(),   // Day of the month without leading zeros 1 to 31
            "{d}": oneTwoDigits(this.getDate()),  // Day of the month, 2 digits with leading zeros 01 to 31

            // Week
            "{W}": this.getWeek(),    // ISO-8601 week number of year, weeks starting on Monday

            // Month
            "{F}": months[this.getMonth()], // A full textual representation of a month, such as January or March January through December
            "{m}": oneTwoDigits(this.getMonth() + 1), // Numeric representation of a month, with leading zeros 01 through 12
            "{M}": months[this.getMonth()].substr(0, 3), // A short textual representation of a month, three letters Jan through Dec
            "{n}": this.getMonth() + 1, // Numeric representation of a month, without leading zeros 1 through 12
            "{t}": this.getMonthDaysNo(), // Number of days in the given month 28 through 31

            // Year
            // "{L}": this.isLeapYear(), // Whether it's a leap year true if it is a leap year, false otherwise.
            "{o}": "", // ISO-8601 year number. This has the same value as Y, except that if the ISO week number (W) belongs to the previous or next year, that year is used instead. Examples: 1999 or 2003
            "{Y}": this.getFullYear(), // A full numeric representation of a year, 4 digits Examples: 1999 or 2003
            "{y}": this.getFullYear().toString().substr(this.getFullYear().toString().length - 2), // A two digit representation of a year Examples: 99 or 03

            // Time
            "{a}": amPm(this.getHours()), // Lowercase Ante meridiem and Post meridiem am or pm
            "{A}": amPm(this.getHours(), true), // Uppercase Ante meridiem and Post meridiem AM or PM
            "{B}": "", // Swatch Internet time 000 through 999
            "{g}": h24to12(this.getHours()), // 12-hour format of an hour without leading zeros 1 through 12
            "{G}": this.getHours(), // 24-hour format of an hour without leading zeros 0 through 23
            "{h}": oneTwoDigits(h24to12(this.getHours())), // 12-hour format of an hour with leading zeros 01 through 12
            "{H}": oneTwoDigits(this.getHours()), // 24-hour format of an hour with leading zeros 00 through 23
            "{i}": oneTwoDigits(this.getMinutes()), // Minutes with leading zeros 00 to 59
            "{s}": oneTwoDigits(this.getSeconds()), // Seconds, with leading zeros 00 through 59
            "{u}": "", // Microseconds Example: 654321

            // Timezone
            "{e}": "", // Timezone identifier Examples: UTC, GMT, Atlantic/Azores
            "{I}": "", // Whether or not the date is in daylight saving time 1 if Daylight Saving Time, 0 otherwise.
            "{O}": "", // Difference to Greenwich time (GMT) in hours Example: +0200
            "{P}": "", // Difference to Greenwich time (GMT) with colon between hours and minutes Example: +02:00
            "{T}": "", // Timezone abbreviation Examples: EST, MDT ...
            "{Z}": "", // Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive. -43200 through 50400

            // Full Date/Time
            "{c}": this.toISOString(), // ISO 8601 date 2004-02-12T15:19:21+00:00
            "{r}": this.toUTCString(), // RFC 2822 formatted date Example: Thu, 21 Dec 2000 16:01:07 +0200
            "{U}": this.getTime() / 1000, // Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
            "{ts}": this.getTime() // Milliseconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
        });
    }
};

/**
 * Return true id date is the same as d
 * @param d date the date to compare
 * @return boolean
 */
Date.prototype.hasSameDate = function (d) {
    return this.getFullYear() == d.getFullYear() &&
        this.getMonth() == d.getMonth() &&
        this.getDate() == d.getDate();
};

/**
 * Return true if date and d has the same number of year week
 * @param d date the date to compare
 * @return boolean
 */
Date.prototype.hasSameWeek = function (d) {
    return this.getWeek() == d.getWeek();
};

/**
 * Return true if date and d has the same month
 * @param d date the date to compare
 * @return boolean
 */
Date.prototype.hasSameMonth = function (d) {
    return this.getFullYear() == d.getFullYear() &&
        this.getMonth() == d.getMonth();
};

/**
 * Return the number of days in the date month
 * @param void
 * @return int
 */
Date.prototype.getMonthDaysNo = function () {
    var d = new Date(this.getFullYear(), this.getMonth() + 1, 1);
    d.setDate(0);
    return d.getDate();
};

/**
 * Set the hour based on the 12-hour system
 * @param h int hours
 * @param pm boolean pm/am
 * @return void
 */
Date.prototype.set12Hours = function (h, pm) {
    function h12to24(h, pm) {
        return ((h == 12) ? 0 : h) + ((pm ? 1 : 0) * 12);
    }
    
    if(h.length == 1) h = parseInt(h);
    else h = parseInt(h.substr(0, 1)) * 10 + parseInt(h.substr(1, 1));
    
    this.setHours(h12to24(h, pm));
};

/**
 * Store the delta between client and server date
 * @param void
 * @return void
 */
Date.prototype.setDelta = function () {
    moGlb.deltaDt = this.getTime() - new Date().getTime();
};

/**
 * Return the client data adding the delta
 * @param void
 * @return the server aligned timestamp in seconds (like php)
 */
Date.prototype.toServerTs = function () {
    return ((this.getTime() - moGlb.deltaDt)/1000).toFixed();
};