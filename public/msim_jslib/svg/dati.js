// dati


tree=[
{ idp:0, idf:0, lbl:'Clear All' },

{ idp:0, idf:1, lbl:'Destinazione uso' },
  { idp:1, idf:2, lbl:'Ascensore', col:'#7f7f7f', ids:['svg_5467', 'svg_5466', 'svg_5465', 'svg_5462','svg_5464'] },
  { idp:1, idf:3, lbl:'Camere (2494,13 mq)', col:'#003f7f', ids:['svg_1' ,'svg_2782','svg_5161','svg_5162','svg_5172','svg_5179','svg_5181','svg_5182','svg_5185','svg_5191','svg_5193','svg_5197','svg_5491','svg_5494','svg_5497','svg_5498'] },
  { idp:1, idf:4, lbl:'Corridoio (124,66 mq)', col:'#7fff00', ids:['svg_5472','svg_5460','svg_5457','svg_5452','svg_5217','svg_5450','svg_5449','svg_5479'] },
  { idp:1, idf:5, lbl:'Cucina (21,55 mq)', col:'#007f3f', ids:['svg_5476','svg_5477','svg_5478'] },                                                     
  { idp:1, idf:6, lbl:'Deposito (3,71 mq)', col:'#ffd4aa', ids:['svg_5480'] },
  { idp:1, idf:7, lbl:'Deposito attrezzature (5 mq)', col:'#ff0000', ids:['svg_5178'] } ,   
  { idp:1, idf:8, lbl:'Deposito Pulito (4 mq)', col:'#00007f', ids:['svg_5475'] } ,
  { idp:1, idf:9, lbl:'Deposito Sporco (4,35 mq)', col:'#7f7f00', ids:['svg_5484'] } , 
  { idp:1, idf:10, lbl:'Sala attesa (54,73 mq)', col:'#ff7f00', ids:['svg_5207','svg_5461','svg_5209'] } ,  
  { idp:1, idf:11, lbl:'Locale infermieri (23,61 mq)', col:'#7f003f', ids:['svg_5483','svg_5481'] },
  { idp:1, idf:12, lbl:'Sala visite (14,65 mq)', col:'#7f3f00', ids:['svg_5471','svg_5173','svg_5174','svg_5175'] },
  { idp:1, idf:13, lbl:'Sbarco ascensori (55,91 mq)', col:'#3f7f00', ids:['svg_5213','svg_5212'] },
  { idp:1, idf:14, lbl:'Scale (66,76 mq)', col:'#ffff00', ids:['svg_5198','svg_5201','svg_5199'] },
  { idp:1, idf:15, lbl:'Servizi igenici (37,17 mq)', col:'#007fff', ids:['svg_5163','svg_5164','svg_5165','svg_5169','svg_5170','svg_5183','svg_5184','svg_5186','svg_5194','svg_5195','svg_5485','svg_5492','svg_5493','svg_5495'] },
  { idp:1, idf:16, lbl:'Studio medico (11,68 mq)', col:'#7f0000', ids:['svg_5468'] },
  { idp:1, idf:17, lbl:'Vuotatoio (2,89 mq)', col:'#ff00ff', ids:['svg_5473'] },
  { idp:1, idf:18, lbl:'Wc personale (7,48 mq)', col:'#00ffff', ids:['svg_5474','svg_5176'] },

{ idp:0, idf:19, lbl:'Reparto/Centro di costo' },
  { idp:19, idf:20, lbl:'Aree comuni', col:'#B20000', ids:['svg_5472','svg_5460','svg_5457','svg_5452','svg_5217','svg_5450','svg_5449','svg_5479','svg_5213','svg_5212'] },
  { idp:19, idf:21, lbl:'Cardiologia', col:'#DC143C', ids:['svg_1','svg_2782','svg_5161','svg_5162','svg_5172'] },
  { idp:19, idf:22, lbl:'Chirurgia generale 1', col:'#ff0000', ids:['svg_5179','svg_5181','svg_5182','svg_5185','svg_5191','svg_5193'] },
  { idp:19, idf:23, lbl:'Ginecologia', col:'#ff00ff', ids:['svg_5197','svg_5491','svg_5494','svg_5497','svg_5498'] },




{ idp:0, idf:24, lbl:'Controsoffitti' },
  { idp:24, idf:25, lbl:'Assente', col:'#003f7f', ids:['svg_5480','svg_5178','svg_5475'] },
  { idp:24, idf:26, lbl:'Cartongesso', col:'#7fff00', ids:['svg_5476','svg_5477','svg_5478','svg_5484'] },
  { idp:24, idf:27, lbl:'Lamellare', col:'#007f3f', ids:['svg_5472','svg_5460','svg_5457','svg_5452','svg_5217','svg_5450','svg_5449','svg_5479'] },  
  { idp:24, idf:28, lbl:'Metallico anticorrosione', col:'#ffd4aa', ids:['svg_1','svg_2782','svg_5161','svg_5162','svg_5172','svg_5179','svg_5181','svg_5182','svg_5185','svg_5191','svg_5193','svg_5197','svg_5491','svg_5494','svg_5497','svg_5498'] },  
  { idp:24, idf:29, lbl:'Pannelli mobili', col:'#00007f', ids:['svg_5207','svg_5461','svg_5209'] },  
  
  
  
 { idp:0, idf:30, lbl:'Pavimentazione' },
  { idp:30, idf:31, lbl:'Industriale', col:'#003f7f', ids:['svg_5467','svg_5466','svg_5465','svg_5462','svg_5464'] },
  { idp:30, idf:32, lbl:'Lineolum', col:'#7fff00', ids:['svg_5198','svg_5201','svg_5199'] },
  { idp:30, idf:33, lbl:'Marmo', col:'#007f3f', ids:['svg_5163','svg_5164','svg_5165','svg_5169','svg_5170','svg_5183','svg_5184','svg_5186','svg_5194','svg_5195','svg_5485','svg_5492','svg_5493','svg_5495','svg_5474','svg_5176'] },  
  { idp:30, idf:34, lbl:'Nessuno', col:'#ffd4aa', ids:['svg_5473'] }, 
  { idp:30, idf:35, lbl:'Piastrelle', col:'#ff0000', ids:['svg_5476','svg_5477','svg_5478','svg_5207','svg_5461','svg_5209','svg_5471','svg_5173','svg_5174','svg_5175'] }, 
  { idp:30, idf:36, lbl:'PVC', col:'#faf', ids:['svg_5468','svg_5472','svg_5460','svg_5457','svg_5452','svg_5217','svg_5450','svg_5449','svg_5479','svg_5213','svg_5212','svg_5483','svg_5481'] } ,
    

{ idp:0, idf:0, lbl:'Aree di trattamento ' },
{ idp:0, idf:38, lbl:' Unita di Estrazione Aria',ids:[] },
{ idp:38, idf:39, lbl:'ET6 (5,540 mc/h)', col:'#E8112D',ids:[] },
{ idp:38, idf:40, lbl:'ET7 (1,870 mc/h)', col:'#9E6B05',ids:[] },
{ idp:38, idf:41, lbl:'ET8 (2,160 mc/h)', col:'#51BFE2',ids:[] },  
{ idp:38, idf:42, lbl:'Da TR esistente (1,060 mc/h)', col:'#00386B',ids:[] },
    
    
{ idp:0, idf:43, lbl:'Unita di Trattamento Aria ',ids:[]},

  { idp:43, idf:44, lbl:'UTA MANDATA', col:'#96938E' ,ids:['svg_1','svg_2782','svg_5161','svg_5162','svg_5172','svg_5179','svg_5181','svg_5182','svg_5213','svg_5185','svg_5191','svg_5193','svg_5197','svg_5491','svg_5494','svg_5497','svg_5498','svg_5472','svg_5460','svg_5457','svg_5452','svg_5217','svg_5450','svg_5449','svg_5479','svg_5476','svg_5477','svg_5478','svg_5480','svg_5480','svg_5178','svg_5475','svg_5484','svg_5207','svg_5461','svg_5209','svg_5483','svg_5481','svg_5471','svg_5173','svg_5174','svg_5175','svg_5163','svg_5164','svg_5165','svg_5169','svg_5170','svg_5183','svg_5184','svg_5186','svg_5194','svg_5195','svg_5485','svg_5492','svg_5493','svg_5495','svg_5468','svg_5473','svg_5474','svg_5176']},
  { idp:43, idf:45, lbl:'UTA TR6 (6,130 mc/h)', col:'#AFA80A' ,ids:[]},
  { idp:43, idf:46, lbl:'UTA TR7 (3,310 mc/h)', col:'#5BBF21',ids:[]},
  { idp:43, idf:47, lbl:'UTA TR7 (3,310 mc/h)', col:'#CCC693',ids:[]},  
  { idp:43, idf:48, lbl:'Da TR esistente TR3 (3,310 mc/h)', col:'#BC3877',ids:[]},
  
 
      
{ idp:0, idf:49, lbl:'Altezze Controsoffitti' },
  { idp:49, idf:50, lbl:'Cartongesso (h 2,70 mt)', col:'#00AACC',ids:[]},
  { idp:49, idf:51, lbl:'Cartongesso (h 2,40 mt)', col:'#0C1975',ids:[]},
  { idp:49, idf:52, lbl:'A tenuta in laminato (h 2,70 mt)', col:'#FF7F1E',ids:[] },  
  { idp:49, idf:53, lbl:'A tenuta in laminato (h 2,90 mt)', col:'#FFD816',ids:[] },  
  { idp:49, idf:54, lbl:'Area non soggetta ad intervento', col:'#BCC1B2',ids:[] },
  { idp:49, idf:55, lbl:'A quadrotti (h 2,70 mt)', col:'#2B2B28',ids:[] }
   
];

