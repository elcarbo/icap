// JavaScript Document



Tree = {  N:0, J:null, OP:-1, All:[], AC:[], img_path: moGlb.baseUrl+"/public/msim_images/default/_svg/",
          
Start:function(){
with(Tree){

  J=js$("idtree");
  N=tree.length;
  
  var r,g,s="",hg;
  
  for(r=0;r<N;r++){ fi=tree[r].idf;
          if(tree[r].idp) continue;
          s+="<div id='idrow"+fi+"' class='rowtree' onclick='Tree.Active("+fi+","+fi+");'>";
          
          if(fi) s+="<img id='idnode"+fi+"' src="+img_path+"'/strutturap0.gif' style='cursor:pointer;' onclick='Tree.OpenClose(event,"+fi+");' />";
          
          s+=tree[r].lbl+"</div>";
      chld="";   tree[r].ids=[]; nf=0;
    if(fi) { AC[fi]=[];  
      for(n=0;n<N;n++) { 
        if(tree[n].idp==fi) { nf++; 
           chld+="<div id='idrow"+tree[n].idf+"' class='rowtree' onclick='Tree.Active("+tree[n].idf+","+tree[n].idp+");'><img id='idnode"+
              fi+"' src='"+img_path+"/struttura2.gif' style='margin-left:18px;' />"+
              "<span style='border:1px solid #aaa;width:16px;height:16px;background-color:"+tree[n].col+";'>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>&nbsp;&nbsp;"+
              tree[n].lbl+"</span></div>";    
        
        
        if(nf) for(g=0;g<tree[n].ids.length;g++) { hg=tree[n].ids[g];
                                                   tree[r].ids.push(hg);  
                                                   All.push(hg); 
                                                   AC[fi][hg]=tree[n].col;   
                                                 }  // padre con tutti gli id
      }  }  
          s+="<div id='idnodechild"+tree[r].idf+"' style='display:none;'>"+chld+"</div>";  
  }}
  J.innerHTML=s;
  
  
Active(0,0);  
}},



OpenClose:function(e,p){
with(Tree){

  var o=1, j=js$("idnodechild"+p), ji=js$("idnode"+p), sr=ji.src;
  
  if(sr.indexOf("turap0")>-1) o=0;
  
  j.style.display=(o?"none":"block");
  ji.src=(o?img_path+"/strutturap0.gif":img_path+"/strutturap1.gif");
  
  cancelEvent(e);
}},



Active:function(i,b){   // p=padre
with(Tree){

var P,I,r,j; 

// root deseleziona tutti
I=tree[i].ids

js$("idrow"+i).style.fontWeight="bold";
if(OP>-1) js$("idrow"+OP).style.fontWeight="normal";

for(r=0;r<All.length;r++) {
  j=js$(All[r]);  if(!j) return false;
  j.setAttribute("fill","rgba(255,255,255,0.1)");
}


for(r=0;r<I.length;r++) {
  j=js$(I[r]);  
  idcol=AC[b][I[r]];
  j.setAttribute("fill",idcol);
  j.setAttribute("opacity",0.5); 
}

OP=i;

}}






} // end Tree





function cancelEvent(e){
  e.returnValue=false;
  if(e.preventDefault) e.preventDefault();
  e.cancelBubble=true;
  if(e.stopPropagation) e.stopPropagation();
}

function cloneArray(A){
  var C = new Array();
  for(D in A) if(A[D].Constructor==Array) CloneArray(A[D]); else C[D]=A[D];
  return C;
}    






