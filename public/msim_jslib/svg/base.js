// JavaScript
/*

js$(string)   return object
jsBrowser()   isTouch 0|1    BROWSER = 'MSIE','FIREFOX','CHROME','SAFARI','OPERA', 'ALTRO'    Browser -1=altro  0=explorer  1=firefox ...

jsW()         return page W
jsH()         return page H

jsGetPos_ltwh(j)           return { l , t , w, h }
jsCreateElem(nm,tpe,pdr)     return obj   nm=nome id  tpe=tag    pdr=oggetto padre (default document.body)
jsgetTrasf()

jsCoords(e)         return { x, y }
jsTCoords(e)      i=n. esimo touch return { x, y }

jsOpacity(oj,op)

jsMisuraTxt(t,f)    return larghezza testo  t  e font f (11px Arial)
jsReduceTxt(tx,l,f)   return testo tx nella larghezza l e font f (11px Arial)
jsHfont(v)          return altezza maiuscola del font alto v pixel 

 
jsIsNum(num)
indexOf             array per compatibilità

array.clone()
string.multiReplace(s)        s= { "a":"b", "c":"d"  }
sortNumber(a,b)
cancelEvent(event)
addEvent(o,t,f)
subEvent(o,t,f)


WHEELOBJ="";      // nome funzione da eseguire se rotella mouse es.  onmouseover="WHEELOBJ='Ruota(d)'" 

*/



function js$(l){ return document.getElementById(l)}


Global={ WW:0, HH:0, OW:0, OH:0, Wait:0, Fnc:"",

Start:function(f){

  if(f) Fnc=f;
  Global.Size();
  addEvent(window,'onorientationchange' in window ? 'orientationchange' : 'resize', Global.Resize);
},
  
Resize:function(a){   
  with(Global){  if(typeof(a)!="number" && Wait) return;
  var W=jsW(), H=jsH(); 
  if(OW!=W || OH!=H) { OW=W, OH=H, Wait=1; setTimeout("Global.Resize(1)",500); return; }
  Wait=0;

  // azioni resize 
  WW=W, HH=H;
  
  try{ if(Fnc) eval(Fnc); } catch(e) {}                                       // inserire gestore resize
   
  //js$("debug").innerHTML=WW+" | "+HH;                                   
  } 
},


Size:function(){
  with(Global){
     WW=jsW();
     HH=jsH();  
}}


} //



function jsBrowser(){
var NAV=navigator.userAgent.toUpperCase(), NAP=navigator.appVersion;

isIphone = (/iphone/gi).test(NAP),
isIpad = (/ipad/gi).test(NAP),
isAndroid = (/android/gi).test(NAP),
isTouch = isIphone || isIpad || isAndroid;

BROWSER="ALTRO";  Browser=-1;
var BS=['MSIE','FIREFOX','CHROME','SAFARI','OPERA'];
for(var r=0;r<BS.length;r++) if(NAV.indexOf(BS[r])>-1) { BROWSER=BS[r]; Browser=r; break; } 
} //



function jsH(){
if (window.innerWidth!=window.undefined) return window.innerHeight;
if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;
if (document.body) return document.body.clientHeight;
}  
  
  
function jsW(){
if (window.innerWidth!=window.undefined) return window.innerWidth;
if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth;
if (document.body) return document.body.clientWidth;
} 


function jsGetPos_ltwh(j){ 
var L=0,T=0,W=j.offsetWidth,H=j.offsetHeight;
while(j.offsetParent){ 
L+=j.offsetLeft; T+=j.offsetTop;
j=j.offsetParent; } 
L+=j.offsetLeft; T+=j.offsetTop;
return {l:L,t:T,w:W,h:H};
} //



WHEELOBJ="";

if(window.addEventListener) window.addEventListener('DOMMouseScroll',jsWheel,false);
window.onmousewheel=document.onmousewheel=jsWheel;

function jsWheel(e) { e=e||window.event;
var d=0; if(e.wheelDelta){ d=e.wheelDelta/120;
if(window.opera) d=-d;} else if(e.detail) d=-e.detail/3;
if(d && WHEELOBJ) eval(WHEELOBJ+"(d)"); 
cancelEvent(e);
} //






function jsCreateElem(nm,tpe,pdr){ 
if(!js$(nm)) { 
if(!pdr) pdr=document.body;
var j=document.createElement(tpe);
j.id=nm;  pdr.appendChild(j);
j.style.position="absolute";
return j;
}
else return js$(nm);
} 


function jsCoords(e){ e=e||window.event;  if(e.pageX || e.pageY) return {x:e.pageX, y:e.pageY}; 
return { x:e.clientX+document.body.scrollLeft-document.body.clientLeft, 
         y:e.clientY+document.body.scrollTop-document.body.clientTop }; 
}

function jsTCoords(e){ 
return { x:e.changedTouches[0].pageX,
			   y:e.changedTouches[0].pageY }			   
}


function jsgetTrasf() {
   var j=jsCreateElem("idtrasformtestbox","div");
   j.style.display="none";

    var prop = [
        'transform',
        'WebkitTransform',
        'msTransform',
        'MozTransform',
        'OTransform'
    ];
    var p;
    while (p=prop.shift()) {
        if(typeof j.style[p] != 'undefined') {
            return p;
        }
    }
    return "zoom";
}

function getElClassName(j, cln){

 if(document.getElementsByClassName) return j.getElementsByClassName(cln);

 var a=[], re = new RegExp('\\b' + cln + '\\b'),
     el=j.getElementsByTagName("*");	
	for(var r=0;r<el.length; r++)
		if (re.test(el[r].className)) a.push(el[r]);		
	return a;
} 


/*
if(!Array.indexOf) {
  Array.prototype.indexOf=function(obj,s) {
    for (var i=(s || 0); i<this.length; i++) 
      if(this[i]==obj) return i; 
    return -1;
}}
*/






function jsOpacity(oj,op){
op=(op==100)?99.999:op;
oj.style.filter="alpha(opacity:"+op+")";
oj.style.MozOpacity=op/100;
oj.style.opacity=op/100;
}


function jsMisuraTxt(t,f){ if(!f) f="11px Arial"; 
j=jsCreateElem("tempcanvas","canvas");
with(j.style) left="-200px", top="-200px";
j.width="10",j.height="10"; 
with(js$('tempcanvas').getContext('2d')){ font=f;
return measureText(t).width;
}}


function jsReduceTxt(tx,l,f){   if(!f) f="11px Arial"; 
var j=jsCreateElem('tempchar',"canvas");
var cnv=j.getContext('2d'); cnv.font=f;
var pt=cnv.measureText(".").width,t="",tt=t, m=0, ca;
for(var r=0;r<3;r++) { m+=pt; if(m>l) return t; t+="."; } 
for(r=0;r<tx.length;r++) { ca=tx.charAt(r); 
m+=cnv.measureText(ca).width; if(m>l) return tt+t; tt+=ca;  }
}


function jsHfont(v){return parseInt(v*0.72+((v>28)?0.55:0.7))}


 

Object.prototype.clone=function(){
  var A=(this instanceof Array)?[]:{};
  for(var i in this){ if(i!='clone'){
    if(this[i] && typeof this[i]=="object"){
     A[i]=this[i].clone(); } else A[i]=this[i];
  }} return A;
}

function jsIsNum(num) {
   return (!isNaN(num) && !isNaN(parseFloat(num))); }


String.prototype.multiReplace = function (s) {  
  var str=this,k;  
  for (k in s) str=str.replace(new RegExp(k,'g'), s[k] ); 
return str;  
}

function sortNumber(a,b) {return a - b; }


function cancelEvent(e){ e=e||window.event;
e.returnValue=false;
if(e.preventDefault) e.preventDefault();
e.cancelBubble=true;
if(e.stopPropagation) e.stopPropagation();
}


function addEvent(o,t,f){     
var d='addEventListener';
if(o[d]) o[d](t,f,false);  
else o['attachEvent']('on'+t,f);
}

function subEvent(o,t,f){ 
var d='removeEventListener';
if(o[d]) return o[d](t,f,0);
return o['detachEvent']('on'+t,f);
} 


// da #000 o #000000  to  rfb o rgba
function colorToRGBa(c,a){ 
  var A="a"; if(typeof(a)=="undefined") A="";
  var n=c.length; d=(n>4)?2:1, dd=0, v="", s="", rgb="rgb"+A+"(";
  for(var r=1;r<n;r++) { v+=c.charAt(r); 
      dd++; if(dd==d) { rgb+=s+(parseInt((d>1)?v:v+v,16)); 
                        v=""; dd=0; s=","; } }
  if(A) A=","+a;
  return rgb+A+")";
} 

// c = colore in formato #000   o   #000000
// t = tonalità    0 <= t < 1
// m = formato  0=#000000  1=rgb(00,00,00)
function colorTone(c,t,m){ 
  if(t<0 || t>=1) return c;
    if(!m) m=0; else m=1;
    var n=c.length, d=(n>4)?2:1, dd=0, s=[], v="", v0, v1;
      for(var r=1;r<n;r++) { v += c.charAt(r);
      dd++; if(dd==d) { v0= parseInt((d>1)?v:(v+v), 16);
               v1=parseInt(v0*t); if(!m) v1=v1.toString(16);
               s.push(v1); v=""; dd=0; } }
    if(m) v0="rgb("+s.join(",")+")";
    else v0="#"+s.join("");
  return v0;
} 