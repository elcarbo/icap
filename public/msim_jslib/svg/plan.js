// panning e zoom event on SVG


Plan = {  TX:0,TY:0, sxy:1, SXY:1,INI_TX:0, INI_TY:0, tx:0,ty:0, svj:null, cntj:null, SVJ:null,
          LT:[],FD:[], DIS:0, dis:1, LT:{}, CX:0,CY:0, 
          aClk:[], CLK:"", aDrg:[], DRG:"",
          WW:0, HH:0,

Start:function(cnt){
  with(Plan){

  // div container
  cntj=js$(cnt);
  LT=jsGetPos_ltwh(cntj);    
  svj=js$("idsvg");


  var aj=document.getElementsByTagName("svg");
  SVJ=aj[0];
  
  
  SVJ.setAttribute("width", LT.w);
  SVJ.setAttribute("height", LT.h);

  // unita di misura metro SCALE = moltiplicatore rispetto al metro
// dotpitch dimensione pixel del monitor
  var rect=js$("base_rect"),
      wrectj=js$("view_rect"),   // rect visualizzazione iniziale opzionale
      unitj=js$("base_unit"),    // 1 => 1 metro
      scalej=js$("base_scale");  //  1:50
      
  if(rect && unitj && scalej){ 
  
     var dotpj=js$("base_dotpitch"), dotpitch=0.25;
     if(dotpj) dotpitch=parseFloat(dotpj.textContent);

     var um=parseFloat(unitj.textContent),
         sa=(scalej.textContent).split(":"), s1=parseInt(sa[0]), s2=parseInt(sa[1]);
        
     if(wrectj){   
     var vl=parseInt(wrectj.getAttribute("x")), vt=parseInt(wrectj.getAttribute("y")),
         vw=parseInt(wrectj.getAttribute("width")), vh=parseInt(wrectj.getAttribute("height"));
         
         var rk=LT.w/vw, rry=rk*vh;
            if(rry>LT.h) rk=LT.h/vh;
          
         TX=(LT.w-vw*rk)/2 - vl*rk; INI_TX=TX;
         TY=(LT.h-vh*rk)/2 - vt*rk; INI_TY=TY; 
             
         INI_SXY=rk;     
         SXY=INI_SXY;
         
         //alert(INI_TX+" | "+INI_SXY+" | "+vw)
         
     } else {
        
        var rx=parseInt(rect.getAttribute("width")), ry=parseInt(rect.getAttribute("height")),
            rk=LT.w/rx, rry=rk*ry;
            
            if(rry>LT.h) rk=LT.h/ry;
          
         TX=(LT.w-rx*rk)/2; INI_TX=TX;
         TY=(LT.h-ry*rk)/2; INI_TY=TY; 
             
         INI_SXY=rk;     
         SXY=INI_SXY;
     }   

         svj.setAttribute("transform","translate("+INI_TX+","+INI_TY+") scale("+INI_SXY+","+INI_SXY+")");
         
         // SCALE 
         if(s1>0 && s2>0) { SCALE=s2/s1*um; 
          }
          
  } else {
  
   // alert("scala non fornita");
  
  }
  
  
  
  
  // eventi
  var EVT=["mousedown","mousemove","mouseup","touchstart","touchmove","touchend"], r=0,n;
  if(!isTouch) WHEELOBJ="Plan.Wheel"; else r=3; 
  for(n=r;n<r+3;n++) eval("addEvent(cntj,EVT[n],Plan.f_"+EVT[n-r]+");");
  
  
  var ARR=document.getElementsByClassName("cl_over");
  for(r=0;r<ARR.length;r++) {  
      addEvent(ARR[r],"mouseover",Plan.f_over);
      addEvent(ARR[r],"mouseout",Plan.f_out); }
  
  ARR=document.getElementsByClassName("cl_edit");
  for(r=0;r<ARR.length;r++)  aClk.push(ARR[r].id);
 
  ARR=document.getElementsByClassName("cl_drag");
  for(r=0;r<ARR.length;r++)  aDrg.push(ARR[r].id); 
  
  
  vediscala(); 
}},
  
 

f_over:function(e){
  with(Plan){
   var j=e.target || e.srcElement;
   j.setAttribute("opacity",0.3);  
   js$("debug").innerHTML="OVER: "+j.id;
}},


f_out:function(e){
  with(Plan){

  var j=e.target || e.srcElement;
  j.setAttribute("opacity",1);
}},



f_mousedown:function(e){
  with(Plan){

  var j=e.target || e.srcElement, ii=j.id, lx=0, ly=0, md=0, tr="",rr,rx=0,ry=0,trn="",
        pcls=j.parentNode.getAttribute("class");
        
  if(!pcls) { cancelEvent(e); return false; }
  
  if(pcls.indexOf("cl_drag")>-1) { j=j.parentNode; ii=j.id; }
  
 
  
  CLK=""; if(aClk.indexOf(ii)>-1) CLK=ii;
  j.parentNode.appendChild(j);
  
  
  if(aDrg.indexOf(ii)>-1) { DRG=ii; var j=js$(DRG);  
  
  if(!j) { cancelEvent(e); return false; }
   
        lx=parseInt(j.getAttribute("x")); ly=parseInt(j.getAttribute("y"));     
        if(isNaN(lx)) { md=1;
                        tr=j.getAttribute("transform"); 
                        rr=extractTranslate(tr);  trn=rr[3];
                          
                        if(!rr[2]) { j.setAttribute("transform","translate(0,0)"+trn);
                                     rx=0, ry=0;
                        } else  rx=rr[0], ry=rr[1]; 
                      }
  }
  

  var r,t, nt=FD.length;
  if(isTouch) { var tch=e.changedTouches;
                for(r=0;r<tch.length;r++) {
                t=tch[r], x=t.pageX, y=t.pageY, i=t.identifier;
                FD.push( { f:i, ix:x, iy:y, x:x, y:y, lx:lx, ly:ly, rx:rx, ry:ry, md:md, trn:trn });  
                if(nt && !DIS) { DIS=Distz(x, y, FD[nt-1].x, FD[nt-1].y); dis=DIS;  }
                }
  } else { t=jsCoords(e);
         FD.push( { f:0, ix:t.x, iy:t.y, x:t.x, y:t.y, lx:lx, ly:ly, rx:rx, ry:ry, md:md, trn:trn });
  } 
  tx=TX,ty=TY;
  
  cancelEvent(e);
}},  
        


f_mousemove:function(e){
  with(Plan){ 
  
  var nt=FD.length, tch, t,x,y,i,n=1,g;
  if(nt) { if(isTouch) tch=e.changedTouches, n=tch.length; 
      for(r=0;r<n;r++) {  
        if(isTouch) { t=tch[r], i=t.identifier;
          for(g=0;g<nt;g++) { if(FD[g].f==i)  break; }
          x=t.pageX, y=t.pageY;    
         } else { t=jsCoords(e), x=t.x, y=t.y, g=0;  CX=x, CY=y; } 
          FD[g].x=x, FD[g].y=y;
          if(nt==1) {  var dx=x-FD[0].ix, dy=y-FD[0].iy;
          
              if(DRG){  var j=js$(DRG);
                        if(FD[0].md) lx=FD[0].rx, ly=FD[0].ry; else lx=FD[0].lx, ly=FD[0].ly;
                        
                        lx+=dx/SXY, ly+=dy/SXY;
                       
                        if(FD[0].md) j.setAttribute("transform","translate("+lx+","+ly+")"+FD[0].trn);
                        else j.setAttribute("x",lx), j.setAttribute("y",ly);
                        
                      //  js$("idquote").innerHTML="x: "+lx.toFixed(2)+" | y: "+ly.toFixed(2);
              
              } else { TX=tx+dx, TY=ty+dy; 
                       svj.setAttribute("transform","translate("+TX+","+TY+") scale("+SXY+","+SXY+")");  } 
              }             
      }
  // se gesture zoom                              
  if(nt>1) { DIS=Distz(FD[0].x, FD[0].y, FD[1].x, FD[1].y);
             CX=(FD[1].x+FD[0].x)/2, CY=(FD[1].y+FD[0].y)/2;
             var osxy=SXY, x=CX-LT.l-TX, y=CY-LT.t-TY;
              SXY=sxy+(DIS/dis-1); if(SXY<0.2) SXY=0.2; 
     var k=SXY/osxy, X=k*x, Y=k*y; TX+=x-X, TY+=y-Y;
     svj.setAttribute("transform","translate("+TX+","+TY+") scale("+SXY+","+SXY+")");
     vediscala();
     } else DIS=0, sxy=SXY; 
           
  } else { t=jsCoords(e); CX=t.x, CY=t.y; }
      
  cancelEvent(e);
}},



f_mouseup:function(e){
  with(Plan){ 
  
  if(FD.length){
  
  var t,tch,x,y, nt=FD.length, ix=FD[0].ix, iy=FD[0].iy;
   if(isTouch){ t=jsTCoords(e); x=t.x, y=t.y; 
                DIS=0, sxy=SXY;
   } else t=jsCoords(e), x=t.x, y=t.y;
  
   
  // if(nt==1 && CLK && inRange(ix,x,iy,y) ) { js$("debug").innerHTML="CLICK: "+CLK;  }
   FD=[]; DRG=""; CLK="";
  } 
   
  cancelEvent(e);
}},




Wheel:function(d){
  with(Plan){ 

  var osxy=SXY, x=CX-LT.l-TX, y=CY-LT.t-TY;
  
  var kk=d/10; if(SXY>2) kk*=parseInt(SXY);
  SXY+=kk; if(SXY<0.2) SXY=0.2;  
  
  var k=SXY/osxy, X=k*x, Y=k*y;
  TX+=x-X, TY+=y-Y;
  svj.setAttribute("transform","translate("+TX+","+TY+") scale("+SXY+","+SXY+")"); 
  
  vediscala();  
}},


vediscala:function(){

// js$("idscale").innerHTML="Scala: "+(parseInt(Plan.SXY*100))+"%";


},


Reset:function(l){
  with(Plan){ 

  TX=0, tx=0, TY=0, ty=0, SXY=1, sxy=1, DIS=0, dis=1;
  svj.setAttribute("transform","translate("+INI_TX+","+INI_TY+") scale("+INI_SXY+","+INI_SXY+")");

  vediscala();
}},


showhide:function(l){
  with(Plan){ 
  var j=js$("level"+l), d=j.style.display;
  j.style.display=(d=="none")?"block":"none";
}},


extractTranslate:function(tr){ 
   if(tr==null) return [0,0,0,""]; 
   if(tr.indexOf("translate(")<0 ) return [0,0,0,tr]; 
   tr=tr.substring(10);
   var atr=tr.split(")"), aa=atr.shift(), trn=atr.join(")");
   atr=aa.split(","); 
   return [parseInt(atr[0]), parseInt(atr[1]), 1, trn];
},

inRange:function(x1,x2,y1,y2){
  var d=4; if( Math.abs(x1-x2)<d && Math.abs(y1-y2)<d ) return 1;
  return 0;
},
  
  
Distz:function(x1,y1,x2,y2){
var x=Math.abs(x2-x1), y=Math.abs(y2-y1);
return Math.sqrt(x*x+y*y);
}  
  
}