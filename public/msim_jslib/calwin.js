// floating window


calWin={ W:520, H:420, oL:0, oT:0, oW:0,oH:0, BS:-1, OH:0, OW:0, MV:0,
         J:null, MHJ:null, zi:11000,
         S:"style='position:absolute;", dwn:0, PB:{},Ix:0,Iy:0, Tdown:0,
         EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove',
         colrs:["#F0F0F0","#A1C24D","#4A6671","#6F8D97","#E2EECA","#E0E0E0","#A0A0A0","#DDDDDD"],
         SRC:"calendar/calendar.html?mode=1",
         
Show:function(){
with(calWin){

  if(!J){
   J=moGlb.createElement('idCalBox','div');
   with(J.style){
    left="128px";
    top="128px";
    width=W+"px";
    height=H+"px";
    boxSizing="border-box";
    border="2px Solid "+colrs[2];
    borderRadius="2px";
    backgroundColor="#FFF";
    boxShadow="4px 4px 12px "+colrs[2];
    zIndex=zi;
    overflow="hidden";
  }
  if(isTouch) EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';
  Draw();
 } else {
   J.style.display="block";
 }
 f_updOld(); 
}},

Draw:function(){
with(calWin){

var s="<div id='idCalBar' "+S+"left:6px;top:6px;right:6px;height:28px;background-color:"+colrs[2]+";cursor:default;border-radius:2px;'>"+
        "<div "+S+"left:8px;top:4px;font:bold 14px opensansR;text-align:left;color:#FFF;'>Calendar</div>"+
        "<div id='idCalBarIcons' "+S+"right:8px;top:3px;font:16px FlaticonsStroke;text-align:right;color:#FFF;cursor:pointer;'>"+
          "<span id='idCalBSIcon'>"+String.fromCharCode(58705)+"</span>&nbsp;&nbsp;<span>"+String.fromCharCode(58827)+"</span></div>"+
        "</div>"+
        
        "<iframe id='idCalFrame' src='"+SRC+"' "+S+"left:6px;top:36px;width:"+(W-16)+"px;height:"+(H-46)+"px;' frameborder=0></iframe>"+
       
        "<canvas id='idCalRes' "+S+"right:0;bottom:0;cursor:nw-resize;' width='32' height='32'></canvas>";
        

  J.innerHTML=s;
  J.style.display="block";
  
  MHJ=moGlb.createElement("maskBkmDragScroll_2","div",document.body);
  MHJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(zi+10)+";");
  
  JB=mf$("idCalBar");
  JC=mf$("idCalRes");
  var cj=JC.getContext("2d");
  
  with(cj){
    fillStyle=colrs[2];
    beginPath(); 
    moveTo(17,29); lineTo(29,29); lineTo(29,17);  
    closePath();
    fill();
  }
  
    // events
  eval("JB."+EVTDOWN+"=function(e){ calWin.sel_down(e); }");
  eval("JC."+EVTDOWN+"=function(e){ calWin.msel_down(e); }");
  
  if(isTouch){ 
    eval("JB."+EVTMOVE+"=function(e){calWin.sel_move(e); }");
    eval("JB."+EVTUP+"=function(e){calWin.sel_up(e); }");  
    eval("JC."+EVTMOVE+"=function(e){calWin.msel_move(e); }");
    eval("JC."+EVTUP+"=function(e){calWin.msel_up(e); }"); 
  }
  

  

}},

//---------------------------------------------------------------------------------
// events

sel_down:function(e){
with(calWin){ moEvtMng.cancelEvent(e); 
                       
  if(dwn) return false;
  
  var w,h,t=new Date().getTime();   
  if((t-Tdown)<200) {
    h=J.offsetHeight;
    if(h>44) {    // collapse
      OH=h,OW=J.offsetWidth;
      h=44,w=160;
      d="none";  
    } else {      // expand
      h=OH, w=OW;
      oW=OW,oH=OH;
      W=oW,H=oH;
      d="block";  
    }
  
    JC.style.display=d;
    with(J.style) height=h+"px",width=w+"px";
    if(w>160) { f_updtWH(); }
    return false;
  }
  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e); 
  Ix=P.x, Iy=P.y;
  PB=moGlb.getPosition(J);
 
  if(!isTouch){
    MHJ.style.display="block";
    eval("MHJ."+EVTMOVE+"=function(e){calWin.sel_move(e); }");
    eval("MHJ."+EVTUP+"=function(e){calWin.sel_up(e); }");
  }
  dwn=1; MV=0;
}},

sel_move:function(e){
with(calWin){ moEvtMng.cancelEvent(e); 
  MV++;
  var fx,fy, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);   
  fx=P.x-Ix, fy=P.y-Iy; 
  with(J.style){  
    left=(PB.l+fx)+"px";
    top=(PB.t+fy)+"px";
    opacity="0.85";
  } 
}},


sel_up:function(e){
with(calWin){ moEvtMng.cancelEvent(e);  
  dwn=0;
  Tdown=new Date().getTime(); 
  if(!isTouch) MHJ.style.display="none";
  J.style.opacity="1";
  // click 
  if(MV<4){
    var x,w=PB.w, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);   
    x=P.x-PB.l;
    
    if(x>(w-64) && x<(w-36) ) { f_big(e); }
    if(x>(w-36) ) { Hide(e);  }
  
    return false;
  }
   
  f_updOld();   
}},



//-----------------------


msel_down:function(e){
with(calWin){ moEvtMng.cancelEvent(e); 
  
  if(dwn) return false;
  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e); 
  Ix=P.x, Iy=P.y;
  PB=moGlb.getPosition(J);

  if(!isTouch){
    MHJ.style.display="block";
    eval("MHJ."+EVTMOVE+"=function(e){calWin.msel_move(e); }");
    eval("MHJ."+EVTUP+"=function(e){calWin.msel_up(e); }");
  }
  dwn=1;
}},

msel_move:function(e){
with(calWin){ moEvtMng.cancelEvent(e); 
  
  var x,y, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);   

  x=P.x-Ix, y=P.y-Iy;
  W=PB.w+x, H=PB.h+y;
  if(W<230) W=230;
  if(H<250) H=250;
  
  with(J.style){  
    width=W+"px";
    height=H+"px";
  }
}},


msel_up:function(e){
with(calWin){ moEvtMng.cancelEvent(e);  
  dwn=0; 
  if(!isTouch) MHJ.style.display="none";
  f_updOld();
  JC.style.display="block";
  f_updtWH();
}},



f_updOld:function(){
with(calWin){
  BS=-1;
  PB=moGlb.getPosition(J);
  oL=PB.l, oT=PB.t, oW=PB.w, oH=PB.h;
  mf$("idCalBSIcon").innerHTML=String.fromCharCode(58705);
}},



f_updtWH:function(){
with(calWin){  
  with(mf$("idCalFrame").style){
    width=(W-16)+"px";
    height=(H-46)+"px"; 
  }
}},








Hide:function(e){ moEvtMng.cancelEvent(e); 
with(calWin){
  J.style.display="none";
}},



f_big:function(e){  moEvtMng.cancelEvent(e); 
with(calWin){
  BS*=-1;
  if(BS>0){
   var ll=16, tt=16, ww=moGlb.getClientWidth(), hh=moGlb.getClientHeight();
   W=ww-32, H=hh-32;
   mf$("idCalBSIcon").innerHTML=String.fromCharCode(58704);
   JC.style.display="block";
   
  } else {
   var ll=oL, tt=oT, W=oW, H=oH;
   mf$("idCalBSIcon").innerHTML=String.fromCharCode(58705);
  }


  with(J.style) left=ll+"px",top=tt+"px",width=W+"px",height=H+"px";
  f_updtWH();
}}

} //
