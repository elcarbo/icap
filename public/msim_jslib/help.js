// Help  320x280




Help={ J:null, JB:null, JC:null, JW:null, MHJ:null, zi:12000, VscrHelp:null,
       PTR:[], TAG:"", PAGE:0, DATA:[], NPGmax:0, NPG:0, nobtn:0,
       S:"style='position:absolute;", dwn:0,PB:{},Ix:0,Iy:0,Tdown:0,
       EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove',
       colrs:["#F0F0F0","#A1C24D","#4A6671","#6F8D97","#E2EECA","#E0E0E0","#A0A0A0","#DDDDDD","rgba(161,194,77,0.3)","#FFF"],
       phpcall:moGlb.baseUrl+"help/xml-to-jsn",     // "docs/xml_to_jsn.php",
       

Start:function(path){  // path = [ { Menu:"FlaticonsStroke.58648 Work activities" }, { Modules:"Work requests" }, { How_do_it:'How create a new work request' }, { page:2 } ];
with(Help){

if(!J){
 J=moGlb.createElement('idHelpBox', 'div');
 with(J.style){
  left="64px";
  top="64px";
  width="460px";
  height="280px";
  boxSizing="border-box";
  border="2px Solid "+colrs[2];
  borderRadius="2px";
  backgroundColor=colrs[9];
  boxShadow="4px 4px 12px "+colrs[2];
  zIndex=zi;
  overflow="hidden";
}}


 if(isTouch) EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';
 
 
 // call ajax
 if(!path) path=[]; else path=JSON.parse(path);       
 iniPTR=path.length; 
 PTR=path;         
 f_call();
 
}}, 
 
f_call:function(){   // alert(a)
with(Help){ 
 
 Ajax.sendAjaxPost(phpcall,"param="+JSON.stringify(PTR),"Help.retXmlPage");
 
}},


retXmlPage:function(a){    // alert(a)   //{"data":["Description","How create a new work request"],"page":0,"tag":"How_do_it"}
with(Help){

  try{ var axml=JSON.parse(a);  }catch(e){  alert(moGlb.langTranslate("xml parsing error!")); return; }

  PAGE=parseInt(axml.page);       // 0 list           >0 page to start default 1
  TAG=axml.tag;
  DATA=axml.data;
 
  nobtn=0; NPG=0; NPGmax=DATA.length;
  
  if(typeof (NPGmax)== "undefined" ) return;
  
  if(!PAGE) nobtn=2; else {
    NPG=PAGE-1;
    if(PAGE>=NPGmax) NPG=NPGmax-1;
  }
  if(TAG!="page" && PTR.length==iniPTR) nobtn=3;    // if no parents

  Draw();
}},






Draw:function(){
with(Help){

  var s="<div id='idHelpBar' "+S+"left:6px;top:6px;right:6px;height:28px;background-color:"+colrs[2]+";cursor:default;border-radius:2px;'>"+
        "<div id='idHelpBarTitle' "+S+"left:8px;top:4px;font:bold 14px opensansR;text-align:left;color:#FFF;'>Title</div>"+        
        "<div id='idHelpBarCounter' "+S+"right:32px;top:4px;font:bold 14px opensansR;text-align:left;color:#FFF;'></div>"+
        "<div "+S+"right:8px;top:3px;cursor:pointer;font:16px FlaticonsStroke;color:#FFF;' onmousedown='Help.f_close(); moEvtMng.cancelEvent(event); return false;'></div>"+
        "</div>"+
        "<div id='idHelpPageCont' "+S+"left:6px;top:40px;right:6px;bottom:40px;overflow:hidden;'></div>"+
        "<div id='idHelpPageScrol' "+S+"right:6px;top:40px;width:24px;bottom:40px;overflow:hidden;display:none;'></div>"+ 
        f_Button("left:6", 58807, "Close", "f_close",4)+
        f_Button("right:6", 58815, "Next", "f_next",2)+
        f_Button("right:90", 58814, "Back", "f_back",1);
        

  J.innerHTML=s;
  J.style.display="block";
  
  MHJ=moGlb.createElement("maskBkmDragScroll_4","div",document.body);
  MHJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(zi+10)+";");
  
  JB=mf$("idHelpBar");
  JC=mf$("idHelpPageCont");
  
  
  // btn disabled?
  if(nobtn&1) f_btn_disable(1,"Back");
  if(nobtn&2) f_btn_disable(1,"Next");
  
  // events
  eval("JB."+EVTDOWN+"=function(e){ Help.sel_down(e); }");
  
  if(isTouch){ 
    eval("JB."+EVTMOVE+"=function(e){Help.sel_move(e); }");
    eval("JB."+EVTUP+"=function(e){Help.sel_up(e); }");  
  }
  
  // scrollbar
  Help.VscrHelp=new OVscroll('Help.VscrHelp',{ displ:0, L:-100, T:0, H:0, HH:0, Fnc:'Help.Vbarpos', pid:'idHelpPageScrol' });
  mf$("idHelpPageScrol").style.display="none";
 
  Page();
}},


f_btn_disable:function(m,b,g){
with(Help){

  if(!g){ setTimeout("Help.f_btn_disable("+m+",'"+b+"',1)",260);  return; }

  var j=mf$("idHelpBtn"+b);
  with(j.style){ 
    backgroundColor=m?"#D0D0D0":"#A1C24D";
    color=m?"#888":"#FFF";
  }
}},


Vbarpos:function(i){
with(Help){
  if(!JW) return;
  JW.style.top=-i+"px";
}},



Page:function(){
with(Help){

  var r,n,s="",bk,onm,tx,a,hh,hw,ttl="";
      
  if(!PAGE){   // list of argument / module
 
       ttl="FlaticonsStroke.58386&nbsp;"+(TAG?TAG:"List");
       mf$("idHelpBarTitle").innerHTML=f_text_icon(ttl,"#FFF",1);
 
       for(r=0;r<DATA.length;r++){ 
          bk=(r&1)?colrs[0]:"#FFF";
          tx=f_text_icon(DATA[r]); 
          onm="onmouseover='this.style.backgroundColor=\""+colrs[7]+"\"' onmouseout='this.style.backgroundColor=\""+bk+"\"' onclick='Help.f_choose(\""+DATA[r]+"\")' ";
          s+="<div style='font:13px opensansR;padding:8px;background-color:"+bk+";cursor:pointer;' "+onm+">"+tx+"</div>";
       }
        
        JC.innerHTML=s;
        hh=JC.scrollHeight;
        s="<div id='idHelpPageWrap' "+S+"left:0;top:0;width:100%;height:"+hh+"px;'>"+s+"</div>";
        JC.innerHTML=s;
        JW=mf$("idHelpPageWrap");
        
        hw=JC.offsetHeight;
        if(hh>hw){
          mf$("idHelpPageScrol").style.display="block";
          VscrHelp.Display(1); VscrHelp.Resize( { L:0, T:0, H:hw, HH:hh } );
      }else mf$("idHelpPageScrol").style.display="none";
      
      if(!isTouch) JC.onmouseover=function(e){ WHEELOBJ="Help.VscrHelp"; }
      
  } else {
  
    WHEELOBJ="";
  
    if(PAGE==-1 || !DATA) {
       nobtn|=2;  f_btn_disable(1,'Next');
      mf$("idHelpPageCont").innerHTML="<div style='font:13px opensansR;color:#B00;margin:4px;'>Data not found in XML!</div>";
      return;
    }

     a=DATA[NPG];
     mf$("idHelpBarTitle").innerHTML=f_text_icon(a.title,"#FFF",1);
     mf$("idHelpBarCounter").innerHTML=(NPG+1)+"/"+NPGmax;
     
     var mg=4;
     if(a.info) s+="<div "+S+"left:"+mg+"px;right:"+mg+"px;top:"+mg+"px;font:14px opensansR;color:#000;'>"+a.info+"</div>";
     
     if(a.goodtoknow) s+="<div "+S+"left:"+mg+"px;right:"+mg+"px;bottom:"+mg+"px;font:13px opensansR;color:#888;'>"+
                         "<div style='margin-bottom:4px;'>"+moCnvUtils.IconFont(58630,16,colrs[1])+" &nbsp;Good to know</div>"+a.goodtoknow+"</div>";

     mf$("idHelpPageCont").innerHTML=s;
  }

  
  
}},


f_text_icon:function(tx,cc,rel){
  var a,n,cod;
  if(!cc) cc="#666";
  if(!rel) rel=0;
  a=tx.split("FlaticonsStroke.");   //  test FlaticonsStroke.58500 testo FlaticonsStroke.58344
    if(a.length>1){
      for(n=1;n<a.length;n++){
        cod=a[n].substring(0,5);
        a[n]=moCnvUtils.IconFont(cod,16,cc,rel) + "&nbsp;<span style='position:relative;top:-"+(rel+1)+"px;'>"+a[n].substring(5)+"</span>";
     }
     tx=a.join("");
   }
   
   if(rel) tx=tx.multiReplace( {"_":" " } );
   
  return tx;
},



f_choose:function(tx){
with(Help){
 
   eval("PTR.push( { "+TAG+":\""+tx+"\" } )");
   f_call();   

}},




f_Button:function(lr, ico, label, fnc, i){
with(Help){
 
  var s,stl="padding:3px;border-radius:2px;border:0px;background-color:#A1C24D;font:13px opensansR;color:#FFF;cursor:pointer;";       
 
  s="<div class='cl_button' id='idHelpBtn"+label+"' "+S+lr+"px;top:242px;width:"+((label!="Back")?70:78)+"px;text-align:center;"+stl+"' "+
    " onmouseover='Help.f_buttonOvr(this,1,"+i+")' onmouseout='Help.f_buttonOvr(this,0,"+i+")' onclick='Help."+fnc+"(\""+label+"\")'>"+
    moCnvUtils.IconFont(ico,16,"#FFF")+"&nbsp;&nbsp;<span style='position:relative;top:-2px;'>"+moGlb.langTranslate(label)+"&nbsp;&nbsp;</span></div>";

  return s;
}},

f_buttonOvr:function(j,m,i){ 
with(j.style){
  if(Help.nobtn&i) return;
  backgroundColor=m?"#D0D0D0":"#A1C24D";
  color=m?"#888":"#FFF";
}},



// nobtn 2
f_next:function(lbl){
with(Help){
  if(nobtn&2) return;
  f_btn_click(lbl);
  
   NPG++; if(NPG==(NPGmax-1)) { nobtn|=2;  f_btn_disable(1,'Next');  }
   Page(); 
}},


// nobtn 1
f_back:function(lbl){
with(Help){
  if(nobtn&1) return;
  f_btn_click(lbl);
  
  
  if(!PAGE) {  // list
    if(PTR.length>iniPTR){ PTR.pop(); f_call(); }
  } else {
  
    if(nobtn&2) { nobtn-=2; Help.f_btn_disable(0,'Next'); }
    NPG--; 
    if(NPG<0) { 
      NPG=0;
      if(PTR.length>iniPTR){ PTR.pop(); f_call(); return; }   
    }
  
    Page();
  }
  
  
  
}},




f_btn_click:function(lbl,m,i){
with(Help){
 
  var cc=colrs[2];
  if(!m) { setTimeout("Help.f_btn_click('"+lbl+"',1)",250); } else cc=colrs[1];
  
  with(mf$("idHelpBtn"+lbl).style){
   backgroundColor=cc;
   color="#FFF";
  }
  
}},


//---------------------------------------------------------------------------------
// events

sel_down:function(e){
with(Help){ moEvtMng.cancelEvent(e); 
  
  if(dwn) return false;
  
  var h,t=new Date().getTime();
  if((t-Tdown)<200) {
    h=J.offsetHeight;
    if(h>100) h=44; else h=280;
    J.style.height=h+"px";
    return false;
  }
  
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e); 
  Ix=P.x, Iy=P.y;
  PB=moGlb.getPosition(J);

  if(!isTouch){
    MHJ.style.display="block";
    eval("MHJ."+EVTMOVE+"=function(e){Help.sel_move(e); }");
    eval("MHJ."+EVTUP+"=function(e){Help.sel_up(e); }");
  }
  dwn=1;
}},

sel_move:function(e){
with(Help){ moEvtMng.cancelEvent(e); 
 
  var fx,fy, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e);   

  fx=P.x-Ix, fy=P.y-Iy;
  
  with(J.style){  
    left=(PB.l+fx)+"px";
    top=(PB.t+fy)+"px";
    opacity="0.85";
  }
  
  
}},


sel_up:function(e){
with(Help){ moEvtMng.cancelEvent(e);  
  dwn=0;
  J.style.opacity="1";
  Tdown=new Date().getTime(); 
  if(!isTouch) MHJ.style.display="none";
}},




f_close:function(){
with(Help){
 J.style.display="none";
}}

} // end help
