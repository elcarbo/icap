var moInit = {
    admin: false,
    STATUS: 1,
    Nline: 0, 
    timeTest: 12000,   // x * 1000  time to check offline
    DT: 250,
    LastUpdate: 0,   

    start: function (stime, lang, admin) {
        moInit.admin = (admin?true:false);
        moDebug.start(); // Debug

        // Popup
        moGlb.loader = moComp.createComponent("loader", "moLoader", true);     // Loader
        moGlb.alert = moComp.createComponent("alertpopup", "moAlert", true);     // Alert
        moGlb.confirm = moComp.createComponent("confirmpopup", "moConfirm", true);     // Confirm
        moGlb.offline = moComp.createComponent("offlinepopup", "moOffline", true);     // Offline
        moGlb.loader.show(moGlb.langTranslate("Loading application, please wait..."));

        Ajax.sendAjaxPost("mainpage/maininit", "", "moInit.init");

        serverDt = new Date(stime);
        serverDt.setDelta();

        moGlb.getBrowser();

        // Init credits animation
        with(moGlb) { with(slideCredits) { mf$("credits").onclick = function (e) { DD=(DD<0)?10:-10; slide(DD); moEvtMng.cancelEvent(e); } } }

        // Settings for touch devices
        with(moGlb) { if(isTouch) zoom = 1.2; }

        // Languages
        moGlb.LANG = lang;
        
        // global search treegrid
        moGlb.SetSearchFields();
        
        // Disable selection on double click
        if(!moGlb.isTouch) document.ondblclick = function (e) { 
          
          var actEl=document.activeElement, tnm=actEl.tagName.toLowerCase();
          if(tnm=="input" || tnm=="textarea") return;
          
          mf$("iddblclickfocus").focus();   
          moEvtMng.cancelEvent(e); 
          return false; 
        }

        // Update online users and bulletins and lock-user treegrid
//        if(!moInit.admin) {
            moInit.UpdateTime(); //  Alive
            Ajax.fnc_offline="moInit.evtFncOffline";
            // Ajax.fnc_check="moInit.evtCheckOffline";    // freeze
//        }        
    },

UpdateTime:function() {
with(moInit) {    
  if(!STATUS) return;
  if(moGlb.isset(moGlb.updateTimeUrl)) Ajax.sendAjaxPost(moGlb.updateTimeUrl, "Timestamp="+LastUpdate, "moInit.retUpdateTime");   //  Alive + offline
  setTimeout("moInit.UpdateTime()", timeTest);
}},


retUpdateTime:function(par){
with(moInit){

  LastUpdate=new Date().getTime();
  Nline++; 
  
  // online users and bulletins and lock-user treegrid 
  moGlb.updateTime(par);
  moInit.Locked.dispatch(par);  
}},




evtFncOffline: function (fs,ff) {   // if Ajax !readyState 3 
with(moInit) {  

  //moDebug.log("ajax call params error:\nfile: "+fs+"\nreturn function: "+ff); 

     if(!STATUS) return;
     STATUS=0;
     evtOffline(); 
  }
},


// freeze disabled
evtCheckOffline: function () {  
with(moInit) {
   if(Nline<5 || !STATUS) return;
   var tt=new Date().getTime();
   if( (tt-LastUpdate)>(timeTest*1500) ) {
    STATUS=0;
    evtOffline();
  }
}},


evtOffline: function () {  
with(moInit) {     
//  if(!moGlb.isset(moGlb.offline)) {
//     with(moGlb.createElement("offline", "div", "", true)) {
//          innerHTML = "<div>"+moGlb.langTranslate("You are currently offline")+"</div><br/>"+moGlb.langTranslate("<b>main</b>sim can't estabilish a connection to the server.<br/>Attempting to restore network connection.<b>main</b>sim will<br/>resume once the connection has been re-estabilished. Please wait.");
//     }
//     moGlb.offline = moComp.createComponent("popupOffline", "moPopup", {
//        width: 375, height: 170, zindex: 10000, hdH: 0, bdWidth: 0, cssClass: '', contentid: "offline" },
//        true);
//  }
     moGlb.offline.show();
     evtOnline(); 
}},


evtOnline: function () {  
   with(moInit) {
   Ajax.sendAjaxPost("login/update-time", "Timestamp="+LastUpdate, "moInit.retEvtOnline");
   if(STATUS) return;
   setTimeout("moInit.evtOnline()",timeTest);
   }
},

retEvtOnline: function () {  
  STATUS=1;
  setTimeout("Ajax.f_Resume();",1000);  // wait 2 seconds 
},


evtRestart: function(mode) {    // 0=continue  1=refresh
  moGlb.offline.hide();
  
  if(mode){
    window.onbeforeunload = function () { }
    location.href = moGlb.baseUrl;  
  }
},






 

init: function (res) {    
        try { res = JSON.parse(res); } catch(ex) {            
            var uiname = '', i = res.length-1;
            while(res[i] != '>') { uiname = res[i]+uiname; i--; }
            moGlb.alert.show(moGlb.langTranslate("You have an error in your JSON syntax for the ui ")+uiname);
            return;
        }
        if(!moInit.admin) { ReverseAjax.regs = res.global.revajax; delete(res.revajax); ReverseAjax.start(); } // reverse ajax
        for(var i in res.global) moGlb[i] = res.global[i]; delete(res.global);
        moDebug.debug = moGlb.debugOn;

      //  moPreload.load(moGlb.imgToLoad, function () {
          //  moDateViewer.create(); // Date viewer
            moInit.setUserPanel(moGlb.user, moGlb.online);
            with(moGlb) {
                with(res) {
                    for(var id in menus) eval(menus[id].name+" = menus["+id+"].menu;");
                    for(var id in textfields) try { moComp.createComponent(textfields[id].name, "moTextField", textfields[id], true); } catch(ex) { moDebug.log("Problems during loading "+textfields[id].name+": check it - "+ex); }
                    for(var id in picklists) try { moComp.createComponent(picklists[id].name, "moPicklist", picklists[id], true); } catch(ex) { moDebug.log("Problems during loading "+picklists[id].name+": check it - "+ex); }
                    for(var id in checkboxes) try { moComp.createComponent(checkboxes[id].name, "moCheckbox", checkboxes[id], true); } catch(ex) { moDebug.log("Problems during loading "+checkboxes[id].name+": check it - "+ex); }
                    for(var id in selects) try { moComp.createComponent(selects[id].name, "moSelect", selects[id], true); } catch(ex) { moDebug.log("Problems during loading "+selects[id].name+": check it - "+ex); }
                    for(var id in buttons) try { moComp.createComponent(buttons[id].name, "moButton", buttons[id], true); } catch(ex) { moDebug.log("Problems during loading "+buttons[id].name+": check it - "+ex); }
                    for(var id in images) try { moComp.createComponent(images[id].name, "moImage", images[id], true); } catch(ex) { moDebug.log("Problems during loading "+images[id].name+": check it - "+ex); }
//                    //for(var id in layouts) try { moComp.createComponent(layouts[id].name, "moLayout", { lay: layouts, ind: id }, true); } catch(ex) { moDebug.log("Problems during loading "+layouts[id].name+": check it - "+ex); }
                    for(var id in layouts) try { moComp.createComponent(layouts[id].name, "moLayout", layouts[id], true); } catch(ex) { moDebug.log("Problems during loading "+layouts[id].name+": check it - "+ex); }
                    for(var id in modules) { modules[id].mid = id; try { moComp.createComponent(modules[id].name, modules[id].type, modules[id], true); } catch(ex) { moDebug.log("Problems during loading "+modules[id].name+": check it - "+ex); } }
                    for(var id in tabbars) {     
                    
                    
                      if(tabbars[id].name == "tbbr_main") { 
                        moTabbarmenu.Start(tabbars[id].tabs,"tabcontainer");  moTabbarmenu.f_clicksel(-1,0);  
                        continue; 
                      }
                        try { moComp.createComponent(tabbars[id].name, "moTabbar", { Elm: tabbars[id].tabs }, true); } catch(ex) { moDebug.log("Problems during loading "+tabbars[id].name+": check it - "+ex); }
                    }
                }

                moGlb.loader.hide();
                if(user.defaultPassword) moGlb.alert.show(langTranslate("Please change your default password"));

                moEvtMng.addEvent(document, "mousemove", f_auto_logoff.Move);                
                f_auto_logoff.Move(); f_auto_logoff.f_Inc();            
            }
       // });
    },

setUserPanel: function (user, online) {
       with(user) {
           if(mf$("bulletins")) mf$("bulletins").innerHTML = bulletins;
           mf$("userFullname").innerHTML = fullname;
         //  mf$("userIcon").innerHTML = '<img id="icon" src="'+moGlb.getAvatarPath(avatar, gender)+'" width="36" height="36" alt="'+fullname+'" />';
       }
       if(mf$("online")) mf$("online").innerHTML = online;
    },
    
Locked: {
        regs: [],
        register: function (name) { moInit.Locked.regs.push(name); },
        unregister: function (name) {with(moInit.Locked) {for(var i in regs) if(regs[i] == name) {delete(regs[i]); return;}}},
        dispatch: function (data) { var par=JSON.parse(data);  with(moInit.Locked) for(var i in regs) { with(eval(regs[i])) { if(active) setLocked(par.locked); }} } 
    }
};






var f_auto_logoff = {
    counter_logoff:0, nc: 10,
    
    Move: function () { 
      with(f_auto_logoff) { 
        nc++; 
        if(nc > 5) { 
          counter_logoff = new Date().getTime(); 
          
          //if((counter_logoff-moInit.LastUpdate) > (moInit.timeTest*1.4) ) {  moInit.evtRestart();  } // back to freeze offline ... need restart       

          nc = 0; 
        } 
      } 
    },
    
    f_Inc: function () {
        with(f_auto_logoff) {            
            var dta = new Date(), Now = parseInt(dta.getTime());  
            if(Now-counter_logoff > (parseInt(moGlb.AUTO_LOGOFF) * 60 * 1000)) {
                Ajax.sendAjaxPost(moGlb.baseUrl.replace("admin/", "")+"/login/logout", "", "f_auto_logoff.callback", true);
                window.onbeforeunload = function () {};
            } else setTimeout("f_auto_logoff.f_Inc()", 30 * 1000);   
        }
    },
    callback: function (o) {
		try{
			var obj = JSON.parse(o);
			window.onbeforeunload = null;
			location.href = moGlb.baseUrl + '/' + obj.url;
		}
		catch(e){
			location.href = moGlb.baseUrl;
		} 
	}
    
};
