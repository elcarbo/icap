var ReverseAjax = {
    server: "treegrid/reverse-ajax",  sid: "", regs: {t_selectors: {}, t_workorders: {}, t_wares: {}, t_systems: {}}, // ts: 0,
    delay: 60, count: 0,
    setSessionId: function (sessionId) {with(ReverseAjax) {sid = sessionId;}},
    register: function (name, type, category) {
        with(ReverseAjax) {
            if(!moGlb.isset(category) || moGlb.isempty(category) || category == "0") {
                category = []; for(var i in regs[type]) category.push(i);
            }
            else category = category.toString().split(",");
            for(var i in category) {
                if(moGlb.isset(regs[type][category[i]]) && !moArray.hasElement(regs[type][category[i]], name)) regs[type][category[i]].push(name);
            }
        }
    },
    dispatch: function (res) {
        with(ReverseAjax) {
            //ts = res.ts;
            for(var type in res) {
                for(var category in res[type]) {
                    var listeners = (moGlb.isset(regs[type][-1]) ? moArray.merge(regs[type][-1], regs[type][category]) : regs[type][category]);
                    for(var i in listeners) {
                        if(listeners[i]) {
                            try {eval(listeners[i]).modIn(res[type][category]);} catch(ex) {moDebug.log(ex);}
                        }
                    }
                }
            }
            // for(var event in res) for(var i in regs[event]) { eval("var mod = " + regs[event][i]); mod.modIn(res[event]); }
        }
    },
    unregister: function (name) {with(ReverseAjax) {for(var i in regs) if(regs[i] == name) {delete(modules[module]);break;}}},
    start: function () {
        with(ReverseAjax) {
            count++; if(count == delay) { count = 0; Ajax.sendAjaxPost(server, "", "ReverseAjax.receive"); }
            setTimeout("ReverseAjax.start()", 1000);
        }
    },
    receive: function (res) {with(ReverseAjax) { try { if(res) dispatch(JSON.parse(res)); } catch(ex) {}}}
};