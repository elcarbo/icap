// kpi with flotr2


KPI={ cntj:null, pcntj:null, boxj:null, headj:null, menuj:null, niSel:0,
      W:0,H:0, RESP:"", graph:null, SERIES:null, drgStart:null, ZD:0, PX:0,
      Params:{}, defaultMin:null, defaultMax:null, PAR:{},
      QueryCall:"", 
      startFNC:{},       // function to run at start
      PATH:"",          // path for images   
      
      
      graphType:"Bars_stacked",    // Line | Bars | Pie | Bars_stacked | Radar | Bubble
      
      
      Query:{},        // query and action in php
      updateQuery:0,   // second between ajax call of Query  
 
      Zoom:true,
      Drag:true,
 
      labelGraph:[],     // example for priority: "Normal","Urgent","Immediate"
      ticksGraph:[],                                            // "ticks"  for Radar

      Rotate:false,
      
      
//------------------------
//**   option setting   **//
//------------------------

  
optview:{
  title:null,
  subtitle:null,
  HtmlText: false,
  colors: ['#00A8F0', '#C0D800', '#CB4B4B', '#4DA74D', '#9440ED'],
  mode: 'normal',
  xaxis: { showLabels: true, min:null, max:null, noTicks:5 },      // mode:'time'   'normal'
  yaxis: { showLabels:true, min:null, max:null, noTicks:5, margin:true },
  grid: {  verticalLines: true, horizontalLines: true }, 
  points: { show:true }, 
  shadowSize: 4, 
  mouse: { track:true, relative:true  },
  pie: { explode: 10 },
  lines: { fill:true }, 
  bars: { barWidth:0.8 },
  legend: { position: 'ne' }
},


//----------------------------------------
//**   button, select, check setting   **//
//----------------------------------------

VAL:{},
ValVar:[],    //  value from da select e check

// header
headDispl:1,
downloadDispl:1,   // 0=none   1=view in header   2=view in menu
selbtnH:[],


// menu
menuDipl:0,
menuWidth:120,

selbtnM:[],


//------------------------------------------------------------------------------------------------------------------------------


Start:function(z){ // z=config object
with(KPI){

var r,k,s,t,b;

  // set custom options
  if(!z) z={};
  for(k in z){
    if(k=="optview"){
      for(r in z[k]) KPI[k][r]=z[k][r];   
    } else KPI[k]=z[k];
  }

 
boxj=mf$("idboxkpi");
headj=mf$("idheadkpi");
menuj=mf$("idmenukpi");
pcntj=mf$("idpcntkpi");
cntj=mf$("idcntkpi");

menuj.style.display=menuDipl?"block":"none";
headj.style.display=headDispl?"block":"none";


// header
if(headDispl) f_buildHeader();

// menu => select & button & check 
if(menuDipl && menuWidth) f_buildMenu();

 

// run function for init
for(r in startFNC) startFNC[r]();



 
 
Resize();
window.addEventListener("resize", KPI.Resize, false);

DIRX="x";
if(Rotate && graphType.substring(0,4)=="Bars") { DIRX="y"; optview.legend.position='se'; }  
  
if(Zoom) ZD|=1;
if(Drag) ZD|=2;  
  
var xyaxis=(DIRX=="x")?"xaxis":"yaxis";

defaultMin=optview[xyaxis].min;
defaultMax= optview[xyaxis].max; 
if(typeof(defaultMin)=="undefined") defaultMin=null;
if(typeof(defaultMax)=="undefined") defaultMax=null;
  
if(ZD==1){ // solo ZOOM 
  optview.selection={ mode: 'x', fps: 25 } 
  Flotr.EventAdapter.observe(cntj, 'flotr:select', KPI.f_zoomSelect);
  Flotr.EventAdapter.observe(cntj, 'flotr:click', KPI.f_defaultDraw );  
}  
 
if(ZD==3){
  cntj.addEventListener('mousemove', KPI.f_overMove, false);
  cntj.addEventListener('mouseout', KPI.f_outMove, false);
  Flotr.EventAdapter.observe(cntj, 'flotr:click', KPI.f_defaultDraw ); 
} 
 
 
 
// call ajax Query
UpdateSeries();
}},



f_buildHeader:function(){
with(KPI){
  niSel=0; var sh="";
   // download
  if(downloadDispl==1) sh="<div style='position:absolute;right:10px;bottom:5px;text-decoration:underline;cursor:pointer;' onclick='KPI.f_download()'>download</div>";
  
  // header => select & button 
  for(r=0;r<selbtnH.length;r++){
    s=selbtnH[r];
    t=s.type.toLowerCase();
    if(t=="select") sh+=buildSelectH(s);
    if(t=="button") sh+=buildButtonH(s);
  }
  if(sh) headj.innerHTML="<div style='margin:6px 10px 0 0'>"+sh+"</div>";
}},



f_buildMenu:function(){
with(KPI){
  var sh="";
  if(downloadDispl==2) sh="<div style='position:absolute;bottom:5px;text-decoration:underline;cursor:pointer;' onclick='KPI.f_download()'>download</div>";
  
  for(r=0;r<selbtnM.length;r++){
    s=selbtnM[r];
    t=s.type.toLowerCase();
    b=r?"<br>":"";
    if(t=="select") sh+=b+buildSelectM(s);
    if(t=="button") sh+=b+buildButtonM(s);
    if(t=="check") sh+=b+buildCheckM(s);
  }
  if(sh) menuj.innerHTML="<div style='margin:10px 6px 6px 6px'>"+sh+"</div>";
}},




f_overMove:function(e) {
with(KPI){
  
  if(!Zoom) return false;
  var p=graph.getEventPosition(e);
  WHEELOBJ="KPI";

  PX=p[DIRX];
}},


f_outMove:function(e){ WHEELOBJ=""; },


Wheel:function(d) {
with(KPI){
  
  var maX=graph.axes[DIRX].max, miN=graph.axes[DIRX].min, 
      ml=PX-miN, mr=maX-PX, dl,dr, k=8, xyaxis,V;
  
  dl=ml/k, dr=mr/k;
  
  xyaxis=(DIRX=="x")?"xaxis":"yaxis";
  V=optview[xyaxis];
  
  V.min=miN+dl*d, V.max=maX-dr*d;
            
  graph=Flotr.draw( cntj, SERIES, optview );
  
  //graph.axes.x.datamax+" | "+graph.axes.x.datamin+" | "+graph.axes.x.min+" | "+graph.axes.x.max)
}},








f_downDrag:function(e) {
with(KPI){
  drgStart=graph.getEventPosition(e);
  Flotr.EventAdapter.observe(cntj, 'mousemove', KPI.f_moveDrag);
  Flotr.EventAdapter.observe(cntj, 'mouseup', KPI.f_upDrag);
}},


f_moveDrag:function(e) {
with(KPI){

  var p=graph.getEventPosition(e), xyaxis,V,
      xa=graph.axes[DIRX],
      dx=drgStart[DIRX]-p[DIRX];

  xyaxis=(DIRX=="x")?"xaxis":"yaxis";
  V=optview[xyaxis];
  V.min=xa.min+dx, V.max=xa.max+dx;
            
  graph=Flotr.draw( cntj, SERIES, optview );          
}},


f_upDrag:function(e) {
with(KPI){

  Flotr.EventAdapter.stopObserving(cntj, 'mousemove', KPI.f_moveDrag);
}},








f_zoomSelect:function(area) {
with(KPI){

  var xyaxis=(DIRX=="x")?"xaxis":"yaxis";

    optview[xyaxis].min=area[DIRX+"1"];
    optview[xyaxis].max=area[DIRX+"2"];      
  graph=Flotr.draw( cntj, SERIES, optview );       
}},


f_defaultDraw:function() {
with(KPI){
    
  var xyaxis=(DIRX=="x")?"xaxis":"yaxis";
    
    optview[xyaxis].min=defaultMin;
    optview[xyaxis].max=defaultMax;
  graph=Flotr.draw( cntj, SERIES, optview );
}},






Resize:function(){
with(KPI){

  W=boxj.offsetWidth;
  H=boxj.offsetHeight;

  var bw=W-12, bh=H-12, bt=6, bl=6;
  if(menuDipl) { bw-=menuWidth; bl+=menuWidth;  with(menuj.style) width=menuWidth+"px", height=(H-30)+"px";  }
  if(headDispl) bh-=30, bt+=30;
  
  with(pcntj.style) left=bl+"px", width=bw+"px", height=bh+"px", top=bt+"px";
  with(cntj.style) width=(bw-16)+"px", height=(bh-16)+"px";
  
  if(RESP) eval("KPI."+graphType+"('"+RESP+"',1)");
}},





buildSelectH:function(o){
with(KPI){
  var r,s="<span style='margin:0 5px 0 10px;'>",slc,spec,ff;
  
  if(o.specvar) spec=o.specvar;
  ff=(o.act)?o.act:"f_ValVar";
  
  if(o.label) s+=o.label+" ";
  s+="<select id='idselkpi"+niSel+"' class='cl_selbtn' style='padding:0' onchange='KPI."+ff+"(this.value,\""+spec+"\")'>";
  for(r=0;r<o.opt.length;r++) with(o.opt[r]) {
    slc=""; if(o.opt[r].selected) slc="selected";
    s+="<option value='"+value+"' "+slc+">"+label+"</option>";
  }
  s+="</select></span>";
  niSel++;
  return s;
}},

buildButtonH:function(o){
with(KPI){
  var r,s="<span style='margin:0 5px 0 10px;'>",slc,ff;
  ff=(o.act)?o.act:"";
  if(o.label) lbl=o.label; else lbl="&nbsp;";
  s+="<button id='idselkpi"+niSel+"' class='cl_selbtn' onclick='KPI.f_btnpress(this,\""+ff+"\");'>&nbsp;"+lbl+"&nbsp;</button></span>"; 
  niSel++;
  return s;
}},




buildSelectM:function(o){
with(KPI){
  var r,s="",slc,dw=menuWidth-12,spec,ff;
  
  if(o.specvar) spec=o.specvar;
  ff=(o.act)?o.act:"f_ValVar";
  if(o.label) s+=o.label+"<br>";
  s+="<select class='cl_selbtn' style='padding:0;width:"+dw+"px;' onchange='KPI."+ff+"(this.value,\""+spec+"\")'>";
  for(r=0;r<o.opt.length;r++) with(o.opt[r]) {
    slc=""; if(o.opt[r].selected) slc="selected";
    s+="<option value='"+value+"' "+slc+">"+label+"</option>";
  }
  s+="</select><br>";
  return s;
}},


buildButtonM:function(o){
with(KPI){
  var r,s="",slc,dw=menuWidth-12,ff; 
  ff=(o.act)?o.act:"";
  if(o.label) lbl=o.label; else lbl="&nbsp;";
  s+="<button class='cl_selbtn' style='width:"+dw+"px;' onclick='KPI.f_btnpress(this,\""+ff+"\")'>&nbsp;"+lbl+"&nbsp;</button><br>"; 
  return s;
}},


buildCheckM:function(o){
with(KPI){
  var r,s="",slc,dw=menuWidth-12,spec;
  
  if(o.specvar) spec=o.specvar;
  if(o.label) s+=o.label+"<br>";
  
  VAL[spec]=[];
  for(r=0;r<o.opt.length;r++) with(o.opt[r]) {
  
    slc=0; if(o.opt[r].selected) slc=1; 
    VAL[spec][r]=slc;
    
    s+="<div onclick='KPI.f_CheckVar("+r+",\""+value+"\",\""+spec+"\")' style='cursor:pointer;margin-top:2px;font:10px Arial;'>"+
       "<img id='idcheck_"+spec+"_"+value+"' src='"+PATH+"check"+slc+".png'>&nbsp;"+label+"</div>";
  }
  s+="<br>";
  
  return s;
}},




// from button
f_btnpress:function(bj,act){
with(KPI){
  bj.style.backgroundColor="#999";
  setTimeout(function(){ bj.style.backgroundColor="#EEE"; },300);
  
  if(act) eval(act);  // action 
    
UpdateSeries();
}},


// from select
f_ValVar:function(v,spec){ // v=value   spec= field (name) 
with(KPI){
 
  VAL[spec]=v;
 
UpdateSeries(); 
}},


// from checkbox
f_CheckVar:function(r,v,spec){
with(KPI){
 
  var i=(VAL[spec][r])?0:1; 
  VAL[spec][r]=i?v:"";
  mf$("idcheck_"+spec+"_"+v).src=PATH+"check"+i+".png";

UpdateSeries();
}},


// from 
f_ActVar:function(v,spec){
with(KPI){

  // custom code

UpdateSeries();
}},


 

/*

autodetect of argument   (v = m | dt)

dt={ Year, Month, Day, Hours, Minutes, Seconds, milliseconds }


return:
  m=0   now in millisecond   
  m=1   actual day  (at hour: 01:00 )
  m=2   tomorrow (at hour: 01:00 )
*/
DateTime:function(v){   

  var m,dt,k;
  if(!v) v=0;
  if(typeof(v)=="number") m=v, dt=null;
  else m=null, dt=v; 
  
  var d=new Date(), 
      DD=d.getDate(),
      MM=parseInt(d.getMonth()),
      YY=d.getFullYear();
                  
  if(dt) {
    var DT={ Year:YY, Month:MM, Day:DD, Hours:1, Minutes:0, Seconds:0, Milliseconds:0 };
    for(k in dt) DT[k]=dt[k];
    d=new Date(DT.Year, DT.Month, DT.Day, DT.Hours, DT.Minutes, DT.Seconds, DT.Milliseconds);
  } 
  
  if(m) {
    if(m==2) DD++;    
    d=new Date(YY,MM,DD,1,0,0);
  }
  
return d.getTime();
},

//------------------------------------------------------------------------------


UpdateSeries:function(){
with(KPI){

  for(var r=0;r<paramFNC.length;r++) paramFNC[r]();  // exec function to find value to Ajax
   Ajax.sendAjaxPost(QueryCall,"params="+JSON.stringify(Params), "KPI.RetParam" );    
}},


RetParam:function(a){
with(KPI){

try{ PAR=JSON.parse(a); } catch(e){  alert(moGlb.langTranslate(a));  }
 

for(var r=0;r<retqueryFNC.length;r++) retqueryFNC[r](); 

KPI[graphType](a);
}},





f_download:function(a){
with(KPI){
  
  if(graph) graph.download.saveImage("jpeg");

}},

Line:function(a,md){
with(KPI){

  RESP=a;
  var dd,series=[],i;
  dd=JSON.parse(a);

  optview.lines.show=true;

  // label for legend?
  if(labelGraph.length){
    for(r=0;r<dd.length;r++) {
      lbl=""; if(labelGraph[r]) lbl=labelGraph[r];
      series.push({ data:dd[r], label:lbl  });
    }
  } else series=dd;

  SERIES=series;

  optview.mouse.trackFormatter=function(o) { return o.y; };

  // draw
  graph=Flotr.draw( cntj, series, optview );
  
  if(ZD&2){ Flotr.EventAdapter.observe(graph.overlay, 'mousedown', KPI.f_downDrag); } 

  // if update 
  if(!md && updateQuery) setTimeout("KPI.UpdateSeries()",updateQuery*1000);  
}},





Bars:function(a,md){
with(KPI){

  RESP=a;
  var r,n,dd,i,series=[],V,v0, HRZ=false;
  dd=JSON.parse(a);

  if(Rotate){ HRZ=true;
    for(r=0;r<dd.length;r++) {
      for(n=0;n<dd[r].length;n++){ V=dd[r][n];
         v0=V[0]; V[0]=V[1]; V[1]=v0;    
      }
    }
  }


  optview.bars={ show:true , horizontal:HRZ };
  if(optview.yaxis.min===null) optview.yaxis.min=0;

  // label for legend?
  if(labelGraph.length){
    for(r=0;r<dd.length;r++) {
      lbl=""; if(labelGraph[r]) lbl=labelGraph[r];
      series.push({ data:dd[r], label:lbl  });
    }
  } else series=dd;
  
  SERIES=series;
  
  // draw
  graph=Flotr.draw( cntj, series, optview );
  
  if(ZD&2){ Flotr.EventAdapter.observe(graph.overlay, 'mousedown', KPI.f_downDrag); } 

  // if update
  if(!md && updateQuery) setTimeout("KPI.UpdateSeries()",updateQuery*1000);  
}},



Bars_stacked:function(a,md){
with(KPI){

  RESP=a;
  var r,n,dd,i,series=[],V,v0, HRZ=false;
  dd=JSON.parse(a);

  
  if(Rotate){ HRZ=true;
    for(r=0;r<dd.length;r++) {
      for(n=0;n<dd[r].length;n++){ V=dd[r][n];
         v0=V[0]; V[0]=V[1]; V[1]=v0;    
      }
    }
  }

  V=optview.bars;
  V.show=true;
  V.stacked=true;
  V.horizontal=HRZ;
  V.shadowSize=0;
  
  with(optview){  
    points.show=false; 
    mouse.track=false;
  }  
  if(optview.yaxis.min===null) optview.yaxis.min=0;

  // label for legend?
  if(labelGraph.length){
    for(r=0;r<dd.length;r++) {
      lbl=""; if(labelGraph[r]) lbl=labelGraph[r];
      series.push({ data:dd[r], label:lbl  });
    }
  } else series=dd;
  

  SERIES=series;
  
  // draw
  graph=Flotr.draw( cntj, series, optview );
 
  if(ZD&2){ Flotr.EventAdapter.observe(graph.overlay, 'mousedown', KPI.f_downDrag); } 

  // if update
  if(!md && updateQuery) setTimeout("KPI.UpdateSeries()",updateQuery*1000);  
}},



Pie:function(a,md) { 
with(KPI){

  RESP=a;
  var r,vv,lbl,series=[],i,t=0;
  vv=JSON.parse(a);


  optview.pie.show=true;
  with(optview){
    grid={ verticalLines: false, horizontalLines: false };
    xaxis.showLabels=false;
    optview.yaxis.showLabels=false;
    points.show=false;
    mouse.trackFormatter=function(o) { return o.y; };
 }
 
  
  for(r=0;r<vv.length;r++) t+=vv[r];
  if(!t) for(r=0;r<vv.length;r++) vv[r]=1; 
 
  for(r=0;r<vv.length;r++) {
    lbl=""; if(labelGraph[r]) lbl=labelGraph[r];
    series.push({ data:[ [0,vv[r]] ], label:lbl  });
  
  }
  
  SERIES=series;

  // draw
  graph=Flotr.draw( cntj, series, optview );
  
    // if update 
  if(!md && updateQuery) setTimeout("KPI.UpdateSeries()",updateQuery*1000);  
}},


Radar:function(a,md) {
with(KPI){

  RESP=a;
  var r,vv,lbl,series=[],i,tks=[],vi,vtot=0;
  vv=JSON.parse(a);

  if(!vv) vv=[];
 
  for(r=0;r<vv.length;r++) {
    lbl=""; if(labelGraph[r]) lbl=labelGraph[r];
    vi=[]; for(i=0;i<vv[r].length;i++) { 
                vi.push([i,vv[r][i]]); 
                vtot+=parseInt(vv[r][i]);
          }
    series.push({ data:vi, label:lbl  });
  }

  if(!vtot) series=[];

  // ticks
  for(r=0;r<ticksGraph.length;r++) tks.push([0,ticksGraph[r]] );
  
  optview["radar"]={ show:true };
  with(optview){
    grid={ circular:true, minorHorizontalLines:true };
    xaxis={ showLabels:true, ticks:tks };
    optview.yaxis.showLabels=true;
    points.show=false;
    mouse.relative=false;
    mouse.trackFormatter=function(o) {  var s=KPI.ticksGraph[parseInt(o.x)]+": "+o.y; return s;  };
 }

  SERIES=series;

  // draw
  graph=Flotr.draw( cntj, series, optview );


  // if update 
  if(!md && updateQuery) setTimeout("KPI.UpdateSeries()",updateQuery*1000);
}},


Bubble:function(a,md){
with(KPI){

  RADIUS=10;

  RESP=a;
  var dd,series=[],i;
  dd=JSON.parse(a);

  optview.bubbles={ show: true, baseRadius:RADIUS, };

  // label for legend?
  series=dd;

  SERIES=series;

  // draw
  graph=Flotr.draw( cntj, series, optview );
  
  if(ZD&2){ Flotr.EventAdapter.observe(graph.overlay, 'mousedown', KPI.f_downDrag); } 

  // if update 
  if(!md && updateQuery) setTimeout("KPI.UpdateSeries()",updateQuery*1000);  
}}



} // end KPI

/*

Flotr.defaultOptions = {
  colors: ['#00A8F0', '#C0D800', '#CB4B4B', '#4DA74D', '#9440ED'], //=> The default colorscheme. When there are > 5 series, additional colors are generated.
  ieBackgroundColor: '#FFFFFF', // Background color for excanvas clipping
  title: null,             // => The graph's title
  subtitle: null,          // => The graph's subtitle
  shadowSize: 4,           // => size of the 'fake' shadow
  defaultType: null,       // => default series type
  HtmlText: true,          // => wether to draw the text using HTML or on the canvas
  fontColor: '#545454',    // => default font color
  fontSize: 7.5,           // => canvas' text font size
  resolution: 1,           // => resolution of the graph, to have printer-friendly graphs !
  parseFloat: true,        // => whether to preprocess data for floats (ie. if input is string)
  xaxis: {
    ticks: null,           // => format: either [1, 3] or [[1, 'a'], 3]
    minorTicks: null,      // => format: either [1, 3] or [[1, 'a'], 3]
    showLabels: true,      // => setting to true will show the axis ticks labels, hide otherwise
    showMinorLabels: false,// => true to show the axis minor ticks labels, false to hide
    labelsAngle: 0,        // => labels' angle, in degrees
    title: null,           // => axis title
    titleAngle: 0,         // => axis title's angle, in degrees
    noTicks: 5,            // => number of ticks for automagically generated ticks
    minorTickFreq: null,   // => number of minor ticks between major ticks for autogenerated ticks
    tickFormatter: Flotr.defaultTickFormatter, // => fn: number, Object -> string
    tickDecimals: null,    // => no. of decimals, null means auto
    min: null,             // => min. value to show, null means set automatically
    max: null,             // => max. value to show, null means set automatically
    autoscale: false,      // => Turns autoscaling on with true
    autoscaleMargin: 0,    // => margin in % to add if auto-setting min/max
    color: null,           // => color of the ticks
    mode: 'normal',        // => can be 'time' or 'normal'
    timeFormat: null,
    timeMode:'UTC',        // => For UTC time ('local' for local time).
    timeUnit:'millisecond',// => Unit for time (millisecond, second, minute, hour, day, month, year)
    scaling: 'linear',     // => Scaling, can be 'linear' or 'logarithmic'
    base: Math.E,
    titleAlign: 'center',
    margin: true           // => Turn off margins with false
  },
  x2axis: {},
  yaxis: {
    ticks: null,           // => format: either [1, 3] or [[1, 'a'], 3]
    minorTicks: null,      // => format: either [1, 3] or [[1, 'a'], 3]
    showLabels: true,      // => setting to true will show the axis ticks labels, hide otherwise
    showMinorLabels: false,// => true to show the axis minor ticks labels, false to hide
    labelsAngle: 0,        // => labels' angle, in degrees
    title: null,           // => axis title
    titleAngle: 90,        // => axis title's angle, in degrees
    noTicks: 5,            // => number of ticks for automagically generated ticks
    minorTickFreq: null,   // => number of minor ticks between major ticks for autogenerated ticks
    tickFormatter: Flotr.defaultTickFormatter, // => fn: number, Object -> string
    tickDecimals: null,    // => no. of decimals, null means auto
    min: null,             // => min. value to show, null means set automatically
    max: null,             // => max. value to show, null means set automatically
    autoscale: false,      // => Turns autoscaling on with true
    autoscaleMargin: 0,    // => margin in % to add if auto-setting min/max
    color: null,           // => The color of the ticks
    scaling: 'linear',     // => Scaling, can be 'linear' or 'logarithmic'
    base: Math.E,
    titleAlign: 'center',
    margin: true           // => Turn off margins with false
  },
  y2axis: {
    titleAngle: 270
  },
  grid: {
    color: '#545454',      // => primary color used for outline and labels
    backgroundColor: null, // => null for transparent, else color
    backgroundImage: null, // => background image. String or object with src, left and top
    watermarkAlpha: 0.4,   // => 
    tickColor: '#DDDDDD',  // => color used for the ticks
    labelMargin: 3,        // => margin in pixels
    verticalLines: true,   // => whether to show gridlines in vertical direction
    minorVerticalLines: null, // => whether to show gridlines for minor ticks in vertical dir.
    horizontalLines: true, // => whether to show gridlines in horizontal direction
    minorHorizontalLines: null, // => whether to show gridlines for minor ticks in horizontal dir.
    outlineWidth: 1,       // => width of the grid outline/border in pixels
    outline : 'nsew',      // => walls of the outline to display
    circular: false        // => if set to true, the grid will be circular, must be used when radars are drawn
  },
  mouse: {
    track: false,          // => true to track the mouse, no tracking otherwise
    trackAll: false,
    position: 'se',        // => position of the value box (default south-east)
    relative: false,       // => next to the mouse cursor
    trackFormatter: Flotr.defaultTrackFormatter, // => formats the values in the value box
    margin: 5,             // => margin in pixels of the valuebox
    lineColor: '#FF3F19',  // => line color of points that are drawn when mouse comes near a value of a series
    trackDecimals: 1,      // => decimals for the track values
    sensibility: 2,        // => the lower this number, the more precise you have to aim to show a value
    trackY: true,          // => whether or not to track the mouse in the y axis
    radius: 3,             // => radius of the track point
    fillColor: null,       // => color to fill our select bar with only applies to bar and similar graphs (only bars for now)
    fillOpacity: 0.4       // => opacity of the fill color, set to 1 for a solid fill, 0 hides the fill 
  }
}


*/

/*

Line & Bars
points_series: [ [x0,y0], [x1,y1], [x2,y2], ...  ]

Pie
[ x0,x1,x2,x3,...]

Radar
[
  [ x0,x1,x2,x3,...],
  [ x0,x1,x2,x3,...],
  ...
]

Bubble
[
  [ [x0,y0,r0], [x1,y1,r1], ... ],
  [ [x0,y0,r0], [x1,y1,r1], ... ],
  ...
]

...


selbtnM:[

  { type:"select", label:"Nave: ", specvar:"asset",
         opt:[ { label:"Selezionare", value:0 },
               { label:"Nave1", value:1 },
               { label:"Nave2", value:2 }
         ] 
  },
  { type:"select", label:"Priority: ", specvar:"f_priority",
          opt:[ { label:"Selezionare", value:0 },
                { label:"Normal", value:1 },
                { label:"Urgent", value:2 },
                { label:"Immediate", value:3 }
          ] 
  },
  { type:"button", label:"Update", act:"" },
  { type:"button", label:"Reset", act:"" },
  { type:"check", label:"Choose Ships: ", specvar:"asset",
          opt:[ { label:"Ship 1", value:1 },
                { label:"Ship 2", value:2 },
                { label:"Ship 3", value:3 },
                { label:"Ship 4", value:4 }         
        ]
  }

],





Head
  { type:"select", label:"Nave: ", specvar:"asset",    // act:"f_ActVar",  
         opt:[ { label:"Selezionare", value:0 },
               { label:"Nave1", value:1 },
               { label:"Nave2", value:2 }
         ] 
  },
  { type:"select", label:"Time: ", specvar:"f_priority",
          opt:[ { label:"oggi", value:0 },
                { label:"ieri", value:1 },
                { label:"settimana", value:2 },
                { label:"Immediate", value:3 }
          ] 
  },
  { type:"button", label:"Update", act:"" }  // UpdateSeries()



Menu
  { type:"select", label:"Nave: ", specvar:"asset",
         opt:[ { label:"Selezionare", value:0 },
               { label:"Nave1", value:1 },
               { label:"Nave2", value:2 }
         ] 
  },
  { type:"select", label:"Priority: ", specvar:"f_priority",
          opt:[ { label:"Selezionare", value:0 },
                { label:"Normal", value:1 },
                { label:"Urgent", value:2 },
                { label:"Immediate", value:3 }
          ] 
  },
  { type:"button", label:"Update", act:"" },
  { type:"button", label:"Reset", act:"" },
  { type:"check", label:"Choose Ships: ", specvar:"asset",
          opt:[ { label:"Ship 1", value:1 },
                { label:"Ship 2", value:2 },
                { label:"Ship 3", value:3 },
                { label:"Ship 4", value:4 }         
        ]
  }


*/
