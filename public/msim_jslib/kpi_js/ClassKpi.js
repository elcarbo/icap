//document.write('<script type="text/JavaScript" src="../public/msim_jslib/chart.js"></script>');

var mainsimKpi = {
    //data = [{label:"",value:""}]
    createSelect: function(data,div,title){
        var select = '<b>'+title+':</b> <select id = "kpi_select" onchange = "get_data()">';
        for(var d = 0; d < data.length; d++){
            select += '<option value="'+data[d]['value']+'">'+data[d]['label']+'</option>'
        }
        select += '</select>';
        document.getElementById(div).innerHTML = select;
    },
    
    //data = [{value: 12,color:"#67bcd1",highlight: "#86cee0",label: "WR"}]
    createGraphPie: function(data,option_percent,canvas){
        // Ottengo il contesto 2D del Canvas in cui mostrare il grafico
        var ctx = document.getElementById(canvas).getContext("2d");
        // Crea il grafico e visualizza i dati
        var myLineChart = new Chart(ctx).Pie(data,{percentageInnerCutout : option_percent});
    },
    
    createGraphLine: function(data,canvas){
        // Ottengo il contesto 2D del Canvas in cui mostrare il grafico
        var ctx = document.getElementById(canvas).getContext("2d");
        // Crea il grafico e visualizza i dati
        var myLineChart = new Chart(ctx).Line(data,{scaleShowGridLines : true});
    },
    
    createGraphBar: function(data,canvas){
        // Ottengo il contesto 2D del Canvas in cui mostrare il grafico
        var ctx = document.getElementById(canvas).getContext("2d");
        // Crea il grafico e visualizza i dati
        var myLineChart = new Chart(ctx).Bar(data);
    },
    
    //data = [{value: 12,color:"#67bcd1",highlight: "#86cee0",label: "WR"}]
    createLegend: function(data,div){
        var cont  = 0;
        for(var c = 0; c < data.length; c++) cont += data[c]['value'];

        var legend = '<table>';
        for(var a = 0; a < data.length; a++){
            var percent = (data[a]['value'] * 100)/cont;
            legend += '<tr><td ><div class = "square" id="square'+a+'"></div></td><td>'+data[a]['value']+'</td><td><div class="txt">'+data[a]['label']+'</div></td><td>'+percent.toFixed(2)+'%</td></tr>';
            //set_square(10,y,a);
        }
        document.getElementById(div).innerHTML = legend+'</table>';

        for(var b = 0; b < data.length; b++) document.getElementById("square"+b).style.backgroundColor = data[b]['color'];

    }
}


