/*window.onload = */function onload_data(){
    get_select();
    
    get_data();  
    /*PIE: var data = [
 
  {
        value: 12,
        color:"#67bcd1",
        highlight: "#86cee0",
        label: "WR"
    },
    {
        value: 11,
        color: "#f09912",
        highlight: "#f9b03d",
        label: "CO"
    },
    {
        value: 4,
        color: "#6ecc9e",
        highlight: "#86deb4",
        label: "PM"
    },
    {
        value: 10,
        color: "#eb4a17",
        highlight: "#f46538",
        label: "INSP"
    },
    {
        value: 8,
        color: "#9942ac",
        highlight: "#b25fc4",
        label: "MT"
    },
    {
        value: 8,
        color: "#f749a2",
        highlight: "#f96fb7",
        label: "COND"
    },
    {
        value: 3,
        color: "#2b7c90",
        highlight: "#4697ab",
        label: "RSV"
    }
        
];*/
    
    //LINE:
    /*var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [28, 48, 40, 19, 86, 27, 90]
        }
    ]
};*/
    //BAR
   /* var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)",
            data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.5)",
            strokeColor: "rgba(151,187,205,0.8)",
            highlightFill: "rgba(151,187,205,0.75)",
            highlightStroke: "rgba(151,187,205,1)",
            data: [28, 48, 40, 19, 86, 27, 90]
        }
    ]
};*/
     
    //mainsimKpi.createGraphPie(data,90,'myLineChart');
    //mainsimKpi.createGraphLine(data,'myLineChart');
    //mainsimKpi.createGraphBar(data,'myLineChart');
    //mainsimKpi.createLegend(data,'legend');
}

/*function get_type_wo() {
    Ajax.sendAjaxPost(moGlb.baseUrl + "kpi_data",'type','result_types');
}

function result_types(x){
    var obj = JSON.parse(x);
    select_type.options[select_type.options.length] = new Option(moGlb.langTranslate('Type (All)'),'');
    
    for(var t = 0; t < obj.length; t++){
        select_type.options[select_type.options.length] = new Option(moGlb.langTranslate(obj[t]['f_type']),obj[t]['f_id']);
    }
}*/

function get_select(){
    Ajax.sendAjaxPost(moGlb.baseUrl + "../../select",'type='+'openedwo','get_data_select');
    
}

function get_data(){
    var period = document.getElementById('kpi_select').value;
    
    /*if(mf$('select_type').options[mf$('select_type').selectedIndex] != undefined){
        type = mf$('select_type').options[mf$('select_type').selectedIndex].value;
    }*/
    
    Ajax.sendAjaxPost(moGlb.baseUrl + "../../data",'type='+'openedwo'+'&period='+period,'get_data_wo');
}

function get_data_wo(data){
    eval("var json = " + data);
    mainsimKpi.createGraphPie(json,90,'myLineChart');
    mainsimKpi.createLegend(json,'legend');
}

function get_data_select(data){
    eval("var json = " + data);
    //data: [{value:"today",label:"TODAY"},{value:"yesterday",label:"YESTERDAY"},{value:"this week",label:"THIS WEEK"}]
    mainsimKpi.createSelect(json,'sel_cont','Periodo');
}