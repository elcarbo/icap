/* localStorage  

LStore
.Set(id, value)     return 0|1
.Get(id)
.Del(id)
.Reset()

.nT   totale
.nL   attuale


*/

LStore = {  nL:0, nT:2600000,

Set:function(id, value){
  with(LStore){
    var s=JSON.stringify(value), n=s.length;
    nL+=n; if(nL<nT) localStorage.setItem(id,s); 
           else { nL-=n; return 0; } 
  }
  return 1;
},

Get:function(id){
  with(LStore){ 
    var l=localStorage.getItem(id);
    if(l==null) return l; 
    return JSON.parse(l);
  }
},

Del:function(id){
  with(LStore){
  var g=localStorage.getItem(id);
  if(g==null) return g;
  nL-=g.length;   
  var l=localStorage.removeItem(id);
  }
},

Reset:function(){
  with(LStore){
    localStorage.clear(); 
    nL=0;
  }
}

} // end 