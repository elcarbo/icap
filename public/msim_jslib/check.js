/*
int                 solo numeri interi
decimal_d           d = n. cifre decimali
decim-_d          d = n massimo di cifre decimali 
decim+_d          d = n. minimo di cifre decimali
range_i_f           i < numero < f (m=-inf M=+inf)
nchar_n             numero esatto di caratteri
nchar_m_n           numero caratteri min m max n
piva                partita iva
cfisc               cod fisc
not_s               s stringa caratteri esclusi
mail                mail
regexp_/pattern/    /pattern/ espressione regolare javascript    es. regexp_/[0-9]{4}-[a-zA-Z]{2}-[0-9]{2}/  1539-Ak-74 

- le condizioni multiple sono separate da uno spazio

es. 
 CAP (5 caratteri) 
 /^\d{5}$/
*/

var moCheck = {
    check: function (v,c){ // v=valore   c= tipo di controllo
        with(moCheck) {
            var risp=1, arrc=c.split(" "), na=arrc.length,cc,pp,app,ss,s,pi,pf,vl;

            vl=v.length; 

            // più controlli?
            for(var r=0;r<na;r++){  

                cc=arrc[r];   

                if(cc=="piva")  risp=checkPIva(v);
                if(cc=="cfisc") risp=checkCF(v);
                if(cc=="mail" && v)  risp=checkMail(v);
                if(cc=="int")   risp=checkInt(v);

                app=cc.split("_");


                if(vl){
                    if(cc.substring(0,5)=="nchar") {
                        if(app.length>2) {
                            pi=parseInt(app[1]);
                            pf=parseInt(app[2]);
                            if(vl<pi || vl>pf) risp=0;
                        } else {
                            if(vl!=parseInt(app[1])) risp=0;
                        } 
                    }

                    else if(cc.substring(0,3)=="not") {
                        ss=app[1];
                        if(app.length>2)  ss+=app[2]+"_";
                        for(var n=0;n<ss.length;n++) {
                            s=ss.charAt(n); 
                            if(v.indexOf(s)>-1) {
                                risp=0;
                                break;
                            }
                        }
                    }

                    else if(cc.substring(0,5)=="range") {
                        if(!isNum(v)) risp=0;
                        else {
                            v=parseFloat(v);
                            if(app[1]!="m" || app[2]!="M") {
                                var i=app[1], f=app[2], mM=1;
                                if(i=="m" && v>f) {
                                    risp=0;
                                    mM=0;
                                }
                                if(f=="M" && v<i) {
                                    risp=0;
                                    mM=0;
                                }
                                if(mM && (v<i || v>f)) risp=0;  
                            }
                        }
                    }
 
                    else if(cc.substring(0,7)=="decimal") {
                        if(!isNum(v)) risp=0;
                        else { 
                            pp=v.split(".");
                            s=pp.length;
                            if(s>1){ if(pp[1].length>app[1]) risp=0; }
                        }
                    }
                    
                    else if(cc.substring(0,6)=="decim-") {
                        if(!isNum(v)) risp=0;
                        else { 
                            pp=v.split(".");
                            s=pp.length;
                            if(s>1){ 
                             if(pp[1].length>app[1]) risp=0;
                            }
                        }
                    }

                   else if(cc.substring(0,6)=="decim+") {
                        if(!isNum(v)) risp=0;
                        else { 
                            pp=v.split(".");
                            s=pp.length;
                            if(s==1 || pp[1].length<app[1]) risp=0;   
                        }
                    }
                       
                    else if(cc == 'username') {
                        //
                    } else if(cc == 'password') {
                        //
                    }
                                                                  
                } 
                if(!risp) return risp;
            } //
            return 1;
            }
    }, //



    checkInt: function (v){
        if(v=="") return 1;
        var rsp=0;
        if(moCheck.isNum(v) && v.indexOf(".")==-1) rsp=1;
        return rsp;
    }, //


    isNum: function (num) {
        return (!isNaN(num) && !isNaN(parseFloat(num)));
    },



    checkCF: function (cf){
        if(cf=="") return 1;
        var validi, i, s, set1, set2, setpari, setdisp;
        if( cf == '' )  return 0;
        cf = cf.toUpperCase();
        if( cf.length != 16 ) return 0;
        validi = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
        for( i = 0; i < 16; i++ ){
            if( validi.indexOf( cf.charAt(i) ) == -1 ) return 0;
        }
		
        set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
        setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
        s = 0;
        for( i = 1; i <= 13; i += 2 )
            s += setpari.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));
        for( i = 0; i <= 14; i += 2 )
            s += setdisp.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));
        if( s%26 != cf.charCodeAt(15)-'A'.charCodeAt(0) ) return 0;
	
        return 1;
    },


    checkPIva: function (pi){
        if(pi=="") return 1;
        if( pi == '' )  return 0;
        if( pi.length != 11 )
            return 0;
        validi = "0123456789";
        for( i = 0; i < 11; i++ ){
            if( validi.indexOf( pi.charAt(i) ) == -1 ) return 0;
        }
        s = 0;
        for( i = 0; i <= 9; i += 2 )
            s += pi.charCodeAt(i) - '0'.charCodeAt(0);
        for( i = 1; i <= 9; i += 2 ){
            c = 2*( pi.charCodeAt(i) - '0'.charCodeAt(0) );
            if( c > 9 )  c = c - 9;
            s += c;
        }
        if( ( 10 - s%10 )%10 != pi.charCodeAt(10) - '0'.charCodeAt(0) ) return 0;
        return 1;
    },

    checkMail: function (emailStr)  {
        var emailPat = /^(.+)@(.+)$/;
        var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
        var validChars = "[^\\s" + specialChars + "]";
        var quotedUser = "(\"[^\"]*\")";
        var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
        var atom = validChars + "+";
        var word = "(" + atom + "|" + quotedUser + ")";
        var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
        var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
        var matchArray = emailStr.match(emailPat);
        if (matchArray == null) { return false; }
        var user = matchArray[1];
        var domain = matchArray[2];
        if (user.match(userPat) == null) {
            return false;
        }
        var IPArray = domain.match(ipDomainPat);
        if (IPArray != null) {
            for (var i = 1; i <= 4; i++) {
                if (IPArray[i] > 255) { return false; }
            }
            return true;
        }
        var domainArray = domain.match(domainPat);
        if (domainArray == null) { return false; }
        var atomPat = new RegExp(atom, "g");
        var domArr = domain.match(atomPat);
        var len = domArr.length;
        if (domArr[domArr.length - 1].length < 2 ||
            domArr[domArr.length - 1].length > 6) { return false; }
        if (len < 2) { return false; }
        return true;
    }
};