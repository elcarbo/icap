// JavaScript Document

/*

odl_Attachments

atch=[

  { name:"filename", type:0|1, f_code:    }      // type   0=attach odl   1=new attach
  {   }
]
*/

moSendMail={J:null, atch:[], SEL:-1, ZEL:-1, REQ: 0,
           SPA:"style='position:absolute;",
           OFL:[], win: null,
           imgPath: moGlb.baseUrl.replace("/admin", "")+"/public/msim_images/default/",
           sendEvent: "", add_params: {},

Start: function(params) {
    with(moSendMail){
        var r, str="", container = 'idEmailContainer';

        atch=[];
        if(moGlb.isset(params.odl_Attachments)) {
            for(r=0;r<params.odl_Attachments.length;r++) {  
                atch.push({name:params.odl_Attachments[r].name, type:0, f_code:params.odl_Attachments[r].f_code});  
            }
        }
        
        if(params.sendEvent) sendEvent = params.sendEvent;
        if(params.params) add_params = params.params;        

        //str+="<div "+SPA+"left:10px;top:0;right:10px;bottom:11px;'>"+
        str+="<div "+SPA+"left:10px;top:5px;font:11px Arial;'>" + "To:" + "</div>"+
        "<input id='idmailTo' type='text' "+SPA+"left:10px;top:23px;width:560px;' value='"+params.mail_To+"'>"+

        "<div "+SPA+"left:10px;top:55px;font:11px Arial;'>" + "Cc:" + "</div>"+
        "<input id='idmailCc' type='text' "+SPA+"left:10px;top:73px;width:560px;' value='"+params.mail_Cc+"'>"+

        "<div "+SPA+"left:10px;top:105px;font:11px Arial;'>" + "Object:" + "</div>"+
        "<input id='idmailObj' type='text' "+SPA+"left:10px;top:123px;width:560px;' value='"+params.title_Object+"'>"+

        "<input id='idfocusOdlAttach' "+SPA+"left:20px;top:255px;width:10px;height:20px;' onblur='moSendMail.f_evid_blur()'>"+

        //"<img src='"+moSendMail.imgPath+"/components/textfield/attach.png' "+SPA+"left:258px;top:175px;cursor:pointer;' title='"+moGlb.langTranslate("Add file attachment list")+"' onclick='mf$(\"filesToUpload\").click()'>"+
        "<img src='"+moSendMail.imgPath+"/buttons/cancel.png' "+SPA+"left:565px;top:175px;cursor:pointer;' title='"+moGlb.langTranslate("Delete selected attachment")+"' onmousedown='moSendMail.f_sub_btn();' >"+

        "<iframe name='nmfrmattach' id='nmfrmattach' style='display:none;'></iframe>"+
        "<form name='frmattach' action='' method='post' target='nmfrmattach' enctype='multipart/form-data' >"+       

        "<div "+SPA+"left:252px;top:173px;width:28px;height:24px;overflow:hidden;z-index:100;opacity:0.0;'>"+
        "<input name='filesToUpload[]' id='filesToUpload' type='file' multiple onchange='moSendMail.loadUpldFiles()' "+
        SPA+"left:-130px;top:0;z-index:100;' />"+
        "</div>"+
        "</form>"+

        "<div "+SPA+"left:10px;top:155px;font:11px Arial;'>" + "Attachments:" + "</div>"+
        "<div class='clattachdiv' id='idmailAttach0' "+SPA+"left:10px;top:173px;width:537px;height:40px;overflow:auto;'></div>"+

        //"<div "+SPA+"left:290px;top:155px;font:11px Arial;'>" + "Other Attachments:" + "</div>"+
        //"<div class='clattachdiv' id='idmailAttach1' "+SPA+"left:290px;top:173px;width:280px;height:40px;overflow:auto;'></div>"+

        "<div "+SPA+"left:10px;top:231px;font:11px Arial;'>" + "Body:" + "</div>"+
        "<textarea id='idmailBody' "+SPA+"left:10px;top:249px;width:560px;height:94px;background-color:#FFF;'>"+params.body+"</textarea>"+

        "<button "+SPA+"left:10px;bottom:5px;width:80px;font:11px Arial;cursor:pointer;' onclick='moSendMail.Cancel()'>" + "Cancel" + "</button>"+
        "<button "+SPA+"right:10px;bottom:5px;width:80px;font:11px Arial;cursor:pointer;' onclick='moSendMail.Send()'>" + "Send email" + "</button>";        

        if(!mf$(container)) {
            J = moGlb.createElement(container, 'div', '', true);
            win = moComp.createComponent("email_win", "moPopup", {contentid: container, title: moGlb.langTranslate('Send e-mail')});        
        } else J=mf$(container);

        J.innerHTML=str;
        f_attach_to_buttons(); // create button for attach

        win.show();
    }
},

loadUpldFiles:function() {
with(moSendMail){
  
  moGlb.loader.show();
  document.forms["frmattach"].action="mail/uploads?rnd="+parseInt(10000*(Math.random()));
  document.forms["frmattach"].submit(); 
  
}},


retUpldFiles:function(v) {
with(moSendMail){
  
  moGlb.loader.hide();
  
  var r, jsn=JSON.parse(v);
  for(r=0;r<jsn.length;r++){
    atch.push({name:jsn[r].name, type:1, f_code:jsn[r].f_code}); 
  }

  f_attach_to_buttons();
}},





f_attach_to_buttons:function(){   // m:  0=odl attach   1=other attach
with(moSendMail){
  SEL=-1;
  var r,s=["",""];
  for(r=0;r<atch.length;r++) {
    s[atch[r].type]+=f_add_btn( atch[r].name, r);
  }  
  mf$("idmailAttach0").innerHTML=s[0];
  //mf$("idmailAttach1").innerHTML=s[1];
}},


 


f_add_btn:function(tx,i){
with(moSendMail){

  var s="<span type='text' id='idatch"+i+"' style='font:bold 11px Arial;color:#888;margin:2px 8px 2px 0;"+
        "text-decoration:underline;cursor:pointer;' onclick='moSendMail.f_evid("+i+")' >"+tx+" </span>";
  return s;
}},



f_evid:function(i){
with(moSendMail){

  var j=mf$("idatch"+i);
  j.style.backgroundColor="#CCC";
 
  mf$("idfocusOdlAttach").focus();
  SEL=i;
}},



f_evid_blur:function(){
with(moSendMail){
  if(SEL==-1) return;
  mf$("idatch"+SEL).style.backgroundColor="#FFF";
  SEL=-1;
}},




f_sub_btn:function(){
with(moSendMail){

  if(SEL==-1) return;
  ZEL=SEL;
  if(atch[SEL]["type"]){
    Ajax.sendAjaxPost(moGlb.baseUrl+"/mail/remove-attach","request="+REQ+"&f_code="+atch[SEL]["f_code"], "moSendMail.retAtchRemove" ); 
  } else retAtchRemove();
  
}},


retAtchRemove:function(){
with(moSendMail){

   atch.splice(ZEL,1);
   f_attach_to_buttons();
   SEL=-1;
   
}},

f_remove_all:function(){
with(moSendMail){

  var listcode="",r, sep="";
  
  for(r=0;r<atch.length;r++){
    if(atch[r]["type"]){       
       listcode+=sep+atch[r]["f_code"];
       sep=",";
    }
  }

  if(listcode) {
   // WAITING POPUP
   moGlb.loader.show();
   Ajax.sendAjaxPost("remove_attach.php","request="+REQ+"&f_code="+listcode, "mpSendMail.retAtchRemoveAll");
  
  } else Close();

}},


retAtchRemoveAll:function(v){
with(moSendMail){
 
  // stop waiting
  moGlb.loader.hide();
  Close();
   
}},

//------------------------------------------------------------------------------

Send:function(){
with(moSendMail){

  var to, cc, ob, bd;
  
  to=mf$("idmailTo").value;
  cc=mf$("idmailCc").value;
  ob=mf$("idmailObj").value;
  bd=mf$("idmailBody").value;  
  
  if(!to || !ob) {moGlb.alert.show(moGlb.langTranslate(('Compile field "To"')));return;}
  
  // verify mails
  a=to.split(";");
  for(r=0;r<a.length;r++) {if(!moCheck.checkMail(a[r])) {moGlb.alert.show(moGlb.langTranslate(("Email format error!")));return;}}
  if(cc) {a=cc.split(";");
    for(r=0;r<a.length;r++) {if(!moCheck.checkMail(a[r])) {moGlb.alert.show(moGlb.langTranslate(("Email format error!")));return;}} 
  }

  par=JSON.stringify( {"Attach":atch, "To":to, "Cc":cc, "Object":ob, "Body":Base64.encode(bd), "add_params": add_params} );
  Ajax.sendAjaxPost(moGlb.baseUrl+"/mail/send-mail", "params="+par, "moSendMail.retEmailSend");
  
  moSendMail.Close();
  if(sendEvent) sendEvent();
  // LOADING POPUP
  moGlb.loader.show();
}},




retEmailSend:function(v){

  //  close LOADING POPUP
  //mf$("idloadbox").style.display="none";
  moGlb.loader.hide();
  
  if(v!="ok") {moGlb.alert.show(moGlb.langTranslate(("Trasmission Error!\n")+v));return;}   
},

Cancel:function(){
with(moSendMail){ 

  f_remove_all();  
}},

Close:function(){
with(moSendMail){
//  J.innerHTML="";
//  var pj=J.parentNode;
  //pj.style.display="none";
  win.hide();
}}
}  //