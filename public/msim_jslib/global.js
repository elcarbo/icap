/*
 * Global properties and functions
 */
var moGlb = {
    // Format parameters
    language: "", // User language
    weekDays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    weekLabel: "Week", weekStartsOn: 0,
    hd: "mainsim_hd", bd: "mainsim_bd",
    oldw: 0, oldh: 0,
    minWinWidth: 400, minWinHeight: 400, // Minimum size for the window
    fontNormal: "13px opensansR", fontBold: "13px opensansR", lineHeight: 1.25, // Style info
    isTouch: 0, zoom: 1, // For mobile and touch devices
    zpopup: 1000, zmask: 900, zmenu: 400, zdrag: 1100, zscroll: 250, zmodule: 200, zlayout: 100, zloader: 1700, zdtmpicker: 1800, zattach: 950, // Z-index
    moduleMinWH: 36, userLevel: null, userId: 1,
    menusPrefixId: "msim_mn", buttonsPrefixId: "msim_btn", modulePrefixId: "msim_mdl",
    layoutPrefixId: "msim_lyt", tabbarPrefixId: "msim_tbr", textfieldPrefixId: "msim_tbr",
    menus: [], buttons: [], modules: [], layouts: [], tabbars: [], imgToLoad: [], img: {},
    imgPath: "", imgDefaultPath: "", baseUrl: "./", maskId: "mask", attachPath: "",
    defaultKeyword: "MSIM:DEFAULT", lockEdit: false, actual_cb_btn: {},
    signature: [], bookmarkColors: ["", "#EEB7B2", "#FBDE68", "#B6D4E6"],
    defaultSearchFields: {},
    stgSearch: "",
    COPY: {},
    /**
     * Get the browser type
     * @param void
     * @return void
     */
    getBrowser: function () {
        var NAV = navigator.userAgent.toUpperCase(), NAP = navigator.appVersion;

        isIphone = (/iphone/gi).test(NAP),
                isIpad = (/ipad/gi).test(NAP),
                isAndroid = (/android/gi).test(NAP),
                isMobile = (/mobile/gi).test(NAV),
                isTouch = isIphone || isIpad || isAndroid || isMobile;

        moGlb.isTouch = isTouch;

        BROWSER = "ALTRO";
        Browser = -1;
        var BS = ['MSIE', 'FIREFOX', 'CHROME', 'SAFARI', 'OPERA'];
        for (var r = 0; r < BS.length; r++)
            if (NAV.indexOf(BS[r]) > -1) {
                BROWSER = BS[r];
                Browser = r;
                break;
            }
    },
    changeLockedElement: function (lock, name) {
        with (moGlb) {
            //console.log("lock: " + lock +" | "+name);

            try {
                GlobalSearch.f_enable_disable(lock, name);
            } catch (e) {
            }

            // get command bar name
            var r, b, p, ae, oe, oo, o = name.split("_");
            o.pop();
            o.push("cb");
            oo = o.join("_");

            try {
                eval("oe=" + oo);
                ae = oe.elements.split(",");

                if (typeof (actual_cb_btn[oo]) == "undefined")
                    actual_cb_btn[oo] = {};

                for (r = 0; r < ae.length; r++) {
                    if (ae[r].indexOf("_pin") != -1)
                        continue;
                    eval("b=" + ae[r]);

                    if (lock) { // true => disable

                        p = b.getProperty("disabled");
                        actual_cb_btn[oo][ae[r]] = p;
                        if (p)
                            continue;

                    } else {  // false => enable

                        p = actual_cb_btn[oo][ae[r]];
                        if (p)
                            continue;
                    }
                    b.setProperties({disabled: lock});
                }
                if (!lock)
                    delete(actual_cb_btn[oo]);

            } catch (e) {
            }

        }
    },
    /**
     * Get the width for the useful space in the page
     * @param void
     * @return float
     */
    getClientWidth: function () {
        if (window.innerWidth != window.undefined)
            return window.innerWidth;
        if (document.compatMode == 'CSS1Compat')
            return document.documentElement.clientWidth;
        if (document.body)
            return document.body.clientWidth;
    },
    /**
     * Get the height for the useful space in the page
     * @param void
     * @return float
     */
    getClientHeight: function () {
        if (window.innerWidth != window.undefined)
            return window.innerHeight;
        if (document.compatMode == 'CSS1Compat')
            return document.documentElement.clientHeight;
        if (document.body)
            return document.body.clientHeight;
    },
    /**
     * Get an object square (x, y, width and height)
     * @param j object object
     * @return { float, float, float, float }
     */
    getPosition: function (j) {
        var L = 0, T = 0, W = j.offsetWidth, H = j.offsetHeight, lboff = 0, tboff = 0;
        while (j.offsetParent) {
            if (j.style.borderLeftWidth)
                lboff = parseInt(j.style.borderLeftWidth.substr(0, j.style.borderLeftWidth.length - 2));
            if (j.style.borderTopWidth)
                tboff = parseInt(j.style.borderTopWidth.substr(0, j.style.borderTopWidth.length - 2));
            L += j.offsetLeft + lboff;
            T += j.offsetTop + tboff;
            j = j.offsetParent;
        }
        L += j.offsetLeft;
        T += j.offsetTop;
        return {l: L, t: T, w: W, h: H};
    },
    verticalAlign: function (cj) {
        var d = document.body, j = moGlb.createElement('', 'div', d, true);
        //d.appendChild(j);
        with (j.style) {
            position = "absolute";
            top = "-1000px";
        }
        var s = cj.innerHTML;
        j.innerHTML = s;
        var h = j.scrollHeight, H = parseInt(cj.scrollHeight), m = parseInt((H - h) / 2);
        cj.innerHTML = "<div style='margin-top:" + m + "px;'>" + s + "</div>";
        d.removeChild(j);
    },
    /**
     * Create new element and append it to the dom
     * @param nm string element name
     * @param tpe string element type, the element tag
     * @param pdr object element parent, where it will be appended
     * @return float
     */
    createElement: function (nm, tpe, pdr, rPosition, width) {
        if (!mf$(nm)) {
            if (!pdr)
                pdr = document.body;
            var j = document.createElement(tpe);
            if (nm)
                j.id = nm;
            pdr.appendChild(j);

            if (width)
                j.style.width = width;

            j.style.position = (rPosition ? "relative" : "absolute");
            return j;
        } else
            return mf$(nm);
    },
    /**
     * Set the opacity value for an object
     * @param oj object object
     * @param op float opacity value
     * @return void
     */
    setOpacity: function (oj, op) {
        /*
         op=(op==100)?99.999:op;
         oj.style.filter="alpha(opacity:"+op+")";
         oj.style.KHTMLOpacity=op/100;
         oj.style.MozOpacity=op/100;
         */
        oj.style.opacity = op / 100;
    },
    /**
     * DEPRECATED
     * Get a reduced version of a long text that ends with ...
     * @param l int text max length
     * @param text string the text to reduce
     * @param width int text weight
     * @param height int text height in pixel
     * @param family string text style
     * @return string
     */
    /*_reduceText: function (l, text, width, height, family) {
     var cnv=mf$('tempchar');
     if(!cnv) {cnv = moGlb.createElement("tempchar", "canvas");cnv.setAttribute("style", "display:none;");}
     with(cnv.getContext('2d')) {
     //var ctx=cnv.getContext('2d');
     font=width+" "+height+"px "+family;
     var pt=measureText(".").width,t="",tt=t, m=0, ca, tx=text;
     for(var r=0;r<3;r++) {
     m+=pt;
     if(m>l) return t;
     t+=".";
     } 
     for(r=0;r<text.length;r++) {
     ca=tx.charAt(r); 
     m+=measureText(ca).width;
     if(m>l) return tt+t;
     tt+=ca;
     }
     }
     },*/

    /**
     * Get a reduced version of a long text that ends with ...
     * @param text string the text to reduce
     * @param style string text style
     * @param maxlength int text max length
     * @return string
     */
    reduceText: function (text, style, maxlength) {
        if (!text)
            return;
        var cnv = mf$("tmpcnv");

        if (text.length > 80)
            text = text.substr(0, 80) + "...";

        if (!cnv) {
            cnv = moGlb.createElement("tmpcnv", "canvas");
            cnv.setAttribute("style", "display: none;");
        }
        with (cnv.getContext('2d')) {
            font = "bold 11px opensansR";
            if (measureText("...").width > maxlength)
                return "";
            if (measureText(text).width <= maxlength)
                return text;
            else {
                for (var i = 1; i < text.length; i++) {
                    var t = text.substr(0, text.length - i - 1) + "...";
                    if (measureText(t).width <= maxlength)
                        return t;
                }
                return "...";
            }
        }
    },
    tgReduceText: function (th, l) {
//        var cnv = mf$("tmpcnv");
//        if(!cnv) {cnv = moGlb.createElement("tmpcnv", "canvas");cnv.setAttribute("style", "display: none;");}
//        var ctx=cnv.getContext("2d");
//        ctx.font=th.FontW+" "+th.FontH+"px "+th.FontFam;
//        var pt=ctx.measureText(".").width,t="",tt=t, m=0, ca, tx=th.txt;
//        for(var r=0;r<3;r++) { m+=pt; if(m>l) return t; t+="."; } 
//        for(r=0;r<th.txt.length;r++) { ca=tx.charAt(r); 
//        m+=ctx.measureText(ca).width; if(m>l) return tt+t; tt+=ca;  }
    },
    /**
     * Get the height for a text
     * @param v float font height
     * @return int
     */
    getFontHeight: function (v) {
        return parseInt(v * 0.72 + ((v > 28) ? 0.55 : 0.7));
    },
    /**
     * Convert an exadecimal color into rgb or rgba if the opacity value exists
     * @param c int color
     * @param a float opacity value
     * @return the rgb color string
     */
    colorToRGBa: function (c, a) {
        var A = "a", n = c.length;
        if (typeof (a) == "undefined")
            A = "";
        var d = (n > 4) ? 2 : 1, dd = 0, v = "", s = "", rgb = "rgb" + A + "(";
        for (var r = 1; r < n; r++) {
            v += c.charAt(r);
            dd++;
            if (dd == d) {
                rgb += s + (parseInt((d > 1) ? v : v + v, 16));
                v = "", dd = 0, s = ",";
            }
        }
        if (A)
            A = "," + a;
        return rgb + A + ")";
    },
    /**
     * Hide the tab bar menu
     * @param void
     * @return void
     */
    hideMenuTab: function () {
        mf$("listabhidden").style.display = "none";
        if (moGlb.isTouch)
            subEvent(document, 'touchend', moGlb.hideMenuTab);
    },
    showMask: function (show, transparent) {
        with (mf$(moGlb.maskId).style) {
            display = (show ? "block" : "none");
            backgroundColor = (transparent ? "transparent" : "#ffffff");
        }
    },
    /**
     * Show the overlay mask
     * @param void
     * @return void
     */
//    showMask: function () {
//        var mask = moGlb.createElement("mask", "div");
//        mask.setAttribute("style", "z-index: " + moGlb.zmask + ";");
//        mask.setAttribute("class", "mask");
//        mask.onmousedown = function (e) {moEvtMng.cancelEvent(e);return false;};
//    },

    /**
     * Hide the overlay mask
     * @param void
     * @return void
     */
//    hideMask: function () {
//        var mask = mf$("mask");
//        if(mask) document.body.removeChild(mask);
//    },

    /**
     * DEPRECATED
     * Load images in dom
     * @param path string the path to images
     * @param urls array of strings contains the names of the images files
     * @param images array of images an output array which stores the new images
     * @param fn function callback function called on final load
     * @param i int index of the image that is loading right now
     * @return void
     */
    loadImages: function (path, urls, images, fn, i) {
        if (typeof i == "undefined")
            i = 0;
        if (urls.length == i)
            return fn;
        images[i] = new Image();
        images[i].src = path + urls[i];
        images[i].onload = moGlb.loadImages(path, urls, images, fn, i + 1);
    },
    openPopup: function (ov_name, m_mod, c_mod, filter, onselect, onunload) {
        var overlay = '';
        try {
            overlay = eval(ov_name);
        } catch (ex) {
            overlay = moComp.createComponent(ov_name, 'moOverlay', {
                filter: filter, m_mod: m_mod, c_mod: c_mod,
                onselect: onselect, onunload: onunload
            });
        }
        overlay.show(eval('mdl_' + (c_mod == 'parent' ? m_mod : c_mod) + '_edit').data.f_code, m_mod, c_mod);
    },
    /**
     * Merge the props of two structures
     * @param base structure the base structure to overwrite
     * @param props structure the new properties
     * @return void
     */
    mergeProperties: function (base, props) {
        if (props) {
            for (var name in props) {
                //if(!moGlb.isempty(props[name])) {
                if (props[name] === true || props[name] == "true")
                    base[name] = true;
                else if (props[name] === false || props[name] == "false")
                    base[name] = false;
                else {
                    var v = parseFloat(props[name]);
                    base[name] = (isNaN(v) || v.length != props[name].length ? props[name] : v);
                }
                //}
            }
        }
    },
    /**
     * Look if a point is within the square
     * @param x int x coordinates of the point
     * @param y int y coordinates of the point
     * @param range structure (xi, yi, xf, yf) the square
     * @return boolean true if the point is within the square
     */
    isIn: function (x, y, range) {
        return (x > range.xi && x < range.xf) && (y > range.yi && y < range.yf);
    },
    /**
     * Duplicates a structure
     * @param A array the structure to clone
     * @return array the copied structure
     */
    cloneArray: function (A) {
//        var C = new Array();
//        for(D in A) if(A[D].Constructor==Array) CloneArray(A[D]); else C[D]=A[D];
//        return C;
        return moArray.clone(A);
    },
    drag: {J: null, X: 0, Y: 0, dx: 0, dy: 0, W: 0, H: 0, c: '', O: {}, Om: '', Ou: '', u: '', p: "px", maskj: null, Pmaskj: null,
        Start: function (e, z, ck) {
            if (!ck)
                ck = 7;
            if (!(ck & 8))
                moEvtMng.cancelEvent(e);
            var b = (document.all) ? 1 : 0, k = e.button;    //if(!b&&k!=2)k=3*k+1;

            //if(!(ck&k) && !isTouch) return;  
            moEvtMng.cancelEvent(e);

            with (moGlb.drag) {

                if (J)
                    Stop(e);
                else {
                    J = e.target || e.srcElement;

                    if (!moGlb.isTouch) {
                        Om = document.onmousemove;
                        Op = document.onmouseup;
                        document.onmousemove = Move;
                        document.onmouseup = Stop;
                    } else {
                        J.ontouchmove = moGlb.drag.Move;
                        J.ontouchend = moGlb.drag.Stop;
                    }

                    if (k & 2 && !moGlb.isTouch)
                        J.oncontextmenu = function () {
                            return false;
                        }

                    var P = (moGlb.isTouch) ? moEvtMng.getTouchXY(e) : moEvtMng.getMouseXY(e);
                    X = P.x;
                    Y = P.y;
                    if (!z)
                        z = {};
                    O = {Pr: 0, Vinc: 0, Lim: 0, Lm0: 0, Lm1: 0, Lm2: 0, Lm3: 0, Lmj: null, Fnc: ''}
                    for (var i in O)
                        O[i] = (typeof z[i] == 'undefined') ? O[i] : z[i];
                    with (O) {
                        if (Pr)
                            J = J.parentNode;


                        Pmaskj = J.parentNode;
                        maskj = moGlb.createElement("maskdrag", "div", Pmaskj);
                        with (maskj.style)
                            zIndex = moGlb.zdrag - 1, left = 0, top = 0, width = '100%', height = '100%';

                        var a = 0;
                        with (J.style) {
                            if (!position) {
                                position = 'absolute';
                                a = 1
                            }
                            P = moGlb.getPosition(J);
                            if (a) {
                                left = P.l + p;
                                top = P.t + p
                            }
                            W = P.w;
                            H = P.h;
                            X -= P.l;
                            Y -= P.t;
                            dx = P.l - parseInt(left);
                            dy = P.t - parseInt(top)
                        }
                        c = J.style.cursor;
                        u = 'move';
                        if (Vinc & 1)
                            u = 'w-resize';
                        if (Vinc & 2)
                            u = 'n-resize';
                        if (Lim == 2) {
                            Lm1 = mfWW() - Lm1;
                            Lm3 = mfHH() - Lm3
                        }
                        if (Lim == 3) {
                            P = mfGetPosition_ltwh(mf$(Lmj));
                            Lm0 += P.l;
                            Lm1 = P.l + P.w - Lm1;
                            Lm2 += P.t;
                            Lm3 = P.t + P.h - Lm3
                        }
                    }
                }
            }
            moEvtMng.cancelEvent(e);
            return false;
        },
        Move: function (e) {
            e = e || window.event;
            with (moGlb.drag) {
                var P = (moGlb.isTouch) ? moEvtMng.getTouchXY(e) : moEvtMng.getMouseXY(e), L = P.x - X, T = P.y - Y;
                with (O) {
                    if (Lim) {
                        if (L < Lm0)
                            L = Lm0;
                        if (T < Lm2)
                            T = Lm2;
                        if ((L + W) > Lm1)
                            L = Lm1 - W;
                        if ((T + H) > Lm3)
                            T = Lm3 - H
                    }
                    with (J.style) {
                        cursor = u;
                        if (!(Vinc & 2))
                            left = (L - dx) + p;
                        if (!(Vinc & 1))
                            top = (T - dy) + p;
                    }
                }
            }
            moEvtMng.cancelEvent(e);
            return false;
        },
        Stop: function (e) {
            with (moGlb.drag) {
                J.style.cursor = c;
                J = null;

                if (!moGlb.isTouch) {
                    document.onmousemove = Om;                                                        //  <---------
                    document.onmouseup = Ou;
                }

                Pmaskj.removeChild(maskj);

                if (O.Fnc)
                    eval(O.Fnc)
            }
            moEvtMng.cancelEvent(e);
            return false;
        }
    },
    updateTime: function (o) {
        try {
            o = JSON.parse(o);
            if (moGlb.isset(o.overwrite_session) && Boolean(o.overwrite_session) === true)
                Ajax.sendAjaxPost(moGlb.baseUrl + "/login/logout", "", "f_auto_logoff.callback", true);
            else {
                mf$("online").innerHTML = o.online;
                if (mf$("bulletins") != null) {
                    mf$("bulletins").innerHTML = o.bulletins;
                }
                if (o.onmaintenance)
                    location.href = moGlb.baseUrl;
            }
        } catch (ex) {
            moDebug.log(ex);
        }
    },
    showLayout: function (i) {
        with (moGlb) {
            var ly = null;
//            for(var j in layouts) {
//                eval("ly = "+layoutPrefixId+j+";");
//                ly.display(bd, 0);
//            }
            eval("ly = " + layoutPrefixId + i + ";");
            ly.display(bd, 1);
        }
    },
    addClass: function (el, className) {
        var classNames = el.getAttribute("class").split(" ");
        for (var i in classNames)
            if (classNames[i] == className)
                return false;
        el.setAttribute("class", classNames.join(" ") + " " + className);
        return true;
    },
    removeClass: function (el, className) {//console.log(el);
        var classNames = el.getAttribute("class").split(" ");//console.log(classNames);
        for (var i in classNames)
            if (classNames[i] == className) {
                delete(classNames[i]);
                el.setAttribute("class", classNames.join(" "));
                return true;
            }
        return false;
    },
    replaceClass: function (el, oldClass, newClass) {
        with (moGlb) {
            if (removeClass(el, oldClass))
                return addClass(el, newClass);
            return false;
        }
    },
    getPrefixByType: function (type) {
        with (moGlb) {
            if (type.substr(0, 8) == "moModule")
                return modulePrefixId;
            switch (type) {
                case "moMenu":
                    return menusPrefixId;
                case "moButton":
                    return buttonsPrefixId;
                case "moLayout":
                    return layoutPrefixId;
                case "moTabbar":
                    return tabbarPrefixId;
                case "moTextField":
                    return textfieldPrefixId;
                default:
                    "";
            }
        }
    },
//    addslashes: function (str) { return str.multiReplace({ "/\\/g": '\\\\', "/\'/g": '\\\'', '/\"/g': '\\"', "/\0/gg": '\\0' }); },
//    stripslashes: function (str) { return str.multiReplace({ "/\\'/g": '\'', '/\\"/g': '"', '/\\0/g': '\0', "/\\\\/g": '\\' }); },

    // Init credits animation
    slideCredits: {
        ANM: 0, DD: 10, oldov: "",
        slide: function (d) {
            with (moGlb.slideCredits) {
                if (DD > 0 && d < 0) {
                    oldov = document.onmouseover;
                    document.onmouseover = function () {
                        moGlb.slideCredits.slide(10);
                    };
                } else if (DD < 0 && d > 0)
                    document.onmouseover = oldov;
                DD = d;
                if (!ANM)
                    tmapcl();
            }
        },
        tmapcl: function () {
            with (moGlb.slideCredits) {
                var j = mf$("credits"), ml = j.style.marginLeft ? parseInt(j.style.marginLeft) : -16, en = 0;
                ml += DD;
                DD = parseInt(DD * 1.5);
                if (ml > -16) {
                    ml = -16;
                    en = 1;
                }
                if (ml < -326) {
                    ml = -326;
                    en = 1;
                }
                j.style.marginLeft = ml + "px";
                if (en) {
                    ANM = 0;
                    return;
                }
                ANM = 1;
                window.setTimeout("moGlb.slideCredits.tmapcl()", 40);
            }
        }
    },
    isempty: function (o) {
        var obj = {};
        if (typeof o == "undefined" || o == null)
            return true;
        if (typeof o == "string" || (typeof o == "object" && o.constructor != obj.constructor))
            return o.length == 0;
        if (typeof o == "object") {
            for (var i in o)
                return false;
            return true;
        }
        return false;
    },
    isset: function (v) {
        return typeof v != "undefined";
    },
    //isDefault: function (v) {return v.toLowerCase() == moGlb.defaultKeyword.toLowerCase();},    
    parseBool: function (v) {
        return !(!v);
    },
    openDoc: function (v, fc) {
        var url = v;

        // alert(v+" | "+fc+"\n"+url);
        //if(v.indexOf("|") > 0) url = moGlb.baseUrl+"/getDocument.php?session_id="+v.split("|")[0];
        if (v.indexOf("|") > 0)
            url = moGlb.baseUrl + "/document/get-file?session_id=" + v.split("|")[0];
        
        if (fc && (url.indexOf(fc + "") == -1))
            url += "_" + fc;
        
        else if (v.indexOf("|") <0)
            url = moGlb.baseUrl + "/document/get-file?f_code=" + v.split(",")[0];
        
        window.open(url, moGlb.langTranslate("See attachment"));
    },
    langTranslate: function (search) {
        return (moGlb.isset(moGlb.LANG) && moGlb.LANG[search] ? moGlb.LANG[search] : search);
    },
    getAvatarPath: function (avatar, gender) {
        var session_id = avatar.split("|")[0];
        if (!avatar)
            return moGlb.imgDefaultPath + "thumbnails/" + (gender == "F" ? "wo" : "") + "man.png";
        //return moGlb.baseUrl.replace("/admin", "")+"/getDocument.php?session_id="+session_id+"&size=thumbnail";
        return moGlb.baseUrl.replace("/admin", "") + "/document/get-file?session_id=" + session_id + "&size=thumbnail";
    },
    padString: function (string, where, length, pad) {
        var padString = string;
        while (padString.length < length) {
            if (where == 'L') {
                padString = pad + padString;
            } else if (where == 'R') {
                padString = padString + pad;
            } else {
                break;
            }
        }
        return padString;
    },
    progressStart: function () {
        // prepare draw bar
        jbar = document.getElementById("idbarprg");
        wB = 260;
        jtxt = document.getElementById("idtxtprg");
        jmsg = document.getElementById("idmsg");
        jmsg.style.display = "block";
        timerProgress = setInterval(moGlb.progressSend, 500);
    },
    progressSend: function () {
        Ajax.sendAjaxPost(moGlb.baseUrl + '/export/progress', null, 'moGlb.progressCallback', false);
    },
    progressCallback: function (response) {
        try {
            var objResponse = JSON.parse(response);

            // get percent
            var w = wB / objResponse.tot * (objResponse.counter), perc = parseInt((objResponse.counter) / objResponse.tot * 100);
            if (objResponse.end == 'true')
                perc = 100;
            // get time left
            var secondsLeft = ((objResponse.tot - objResponse.counter) * objResponse.time / objResponse.counter);
            if (secondsLeft > 0) {
                if (secondsLeft / 60 > 1) {
                    var aux = Math.round(parseInt(secondsLeft) / 60);
                    timeLeft = '(' + aux + ' minutes left)';
                } else {
                    var aux = parseInt(secondsLeft);
                    timeLeft = '(' + aux + ' seconds left)';
                }
            } else {
                timeLeft = '';
            }



            //  #3C6A74
            jbar.innerHTML = "<div style='position:absolute;left:0;top:0;height:11px;width:" + w + "px;background-color:#ADD139;box-shadow: inset 1px 3px 6px rgba(0,0,0,.40);'></div>";

            if (objResponse.end == 'true' && moGlb.downloadStatus != 'done') {
                //jmsg.style.display= "none";
                moGlb.downloadStatus = 'done';
                document.getElementById('iframe').innerHTML = '<iframe onload="" style="display:none" src="' + moGlb.baseUrl + '/export/download?file=' + objResponse.filename + '"></iframe>';
                document.getElementById('idtxtprg').style.textAlign = 'center';
                document.getElementById('idtxtprg').innerHTML = 'download completed (close the window)';
            } else {
                jtxt.innerHTML = "exporting " + perc + "% " + timeLeft;
            }
            //}
        } catch (e) {
        }
    },
    progressEnd: function () {
        clearInterval(timerProgress);
        if (moGlb.downloadStatus != 'done') {
            Ajax.sendAjaxPost(moGlb.baseUrl + '/export/abort', "");
        }
        moGlb.downloadStatus = '';
    },
    // treegrid default field for global search    

    // prepare array from SETTING  wo:f_title,f_priority,... ; ... 

    // set
    SetSearchFields: function () {
        with (moGlb) {
            defaultSearchFields = {};
            if (!stgSearch)
                return;
            var v, r, n, nav, av, s = {" ": "", "\n": ""};
            v = stgSearch.multiReplace(s);   // remove space and \n
            nav = v.split(";");                        // split rows
            for (r = 0; r < nav.length; r++) {
                av = nav[r].split(":");                // split module from fields
                if (av.length != 2)
                    continue;
                defaultSearchFields[ av[0] ] = av[1].split(",");   // wo:[field1,field2,...];
            }
        }
    },
    // get 
    getSearchFields: function (name) {  // name   mdl_wo_tg
        with (moGlb) {
            var mdl, a;
            a = name.split("_");
            mdl = a[1];             // mdl = wo  
            if (!defaultSearchFields[mdl])
                return ["f_title", "f_description"];
            else
                return defaultSearchFields[mdl];
        }
    }

};

var moComp = {
    createComponent: function (name, type, props, noresize) {
        eval(name + " = new " + type + "('" + name + "');");
        if (!moGlb.isset(props))
            props = {};
        props.name = name;
        window[name].create(props ? props : {});
        //delete(props.name); window[name].json = props;
        if (!noresize)
            moResize.add(name);
        return moComp.getComponent(name);
    },
    getComponent: function (name) {
        try {
            return eval(name);
        } catch (ex) {
            return null;
        }
    },
    removeComponent: function (obj) {
        delete window[typeof obj == "string" ? obj : obj.name];
    }
}

/**
 * List of the colors used in the application
 */
var moColors = {
    text: "#000000", textDisabled: "#A0A0A0", textOver: "#000000", textError: "#ff0000", textGray: "#808080",
    /*bg: "rgba(240,240,240,1)",*/bg: "#f2f2f2", bgOver: "#DEE9BB", bgOverGray: "#dfdfdf", bgSelected: "#C2DB70", bgDisabled: "#F2F2F2",
    bgHighlightColor: "#D7E3C3", bgNone: "#ffffff", bgError: "#FAF9ED", bgHighlightModColor: "#9DC3EB",
    separator: "#E2E3E3", separatorBevel: "#FFFFFF",
    /*border: "#72A0FF",*/ border: "#60828D", borderError: "#F25C5C", btnBorder: "#9d9d9d", btnReflection: "rgba(223, 223, 223, 0.85)",
    shadow: "rgba(0, 0, 0, 0.5)", shadowError: moGlb.colorToRGBa("#F25C5C", 0.5),
    windowBg: "rgba(240,240,240,1)", windowBorder: "",
    canvasSymbol: "#808080",
    fgTabTxtSel: "#1D4D5C", fgTabTxtNosel: "#868686", fgTabTxtOver: "#ffffff", fgTabTxtDisable: '#C0C0C0',
    bgTabSel: "#ffffff", bgTabNosel: "#D0D0D0", bgTabOver: "#ADD139",
    pxTabBord: "#808080", pxTabNosel: "#404040",
    // Tree grid
    ROWBKCOL: ["#FFFFFF", "#FAFAFA", "#FEFF9E"], ROWBDCOL: "#EDEDED",
    Col0: ["#F6F6F6", '#ECEDEF', "#aaa", "#444"], Col1: ["#DFEBEF", '#C8D9DE', "#72A0AC", "#444"], Col2: ["#EBF2F4", '#DAE6E9', "#aaa", "#444"],
    tgArrow: "#888888", maskMoveStroke: "#808080", maskMoveFill: "rgba(224,224,224,0.5)", reverseFlashFill: "rgba(255,0,0,0.1)"
};


/**
 * Get the dom object with the id in input
 * @param l string object id
 * @return object
 */
function mf$(l) {
    return document.getElementById(l);
}

/**
 * Add to the String object a method to make multiple replaces
 * @param subst object replacements array
 * @return str string the string with replacements
 */
String.prototype.multiReplace = function (replace) {
    var str = this, key;
    for (key in replace)
        str = str.replace(new RegExp(key, 'g'), replace[key]);
    return str;
};

/**
 * Add to the String object a method to make multiple replaces
 * @param subst object replacements array
 * @return str string the string with replacements
 */
String.prototype.multiReplace = function (replace) {
    var str = this, key;
    for (key in replace)
        str = str.replace(new RegExp(key, 'g'), replace[key]);
    return str;
};

/**
 * Add to the Float object a method to get the number conversion into decimal format
 * @param void
 *
 Number.prototype.toDecimalLocale = function (d) {
 if(isNaN(parseInt(d))) return false;
 return this.toFixed(d).replace('.', moGlb.localization.f_decimal_separator);
 };*/

/**
 * Add to the Float object a method to get the number conversion into currency format
 * @param void
 *
 Number.prototype.toCurrencyLocale = function () {
 var splitted_n = this.toFixed(2).split('.'), n = '';
 for(var i=0; i<splitted_n[0].length; i++) {
 if(i> 0 && (i%3) == 0) n = moGlb.localization.f_thousand_separator+n;
 n = splitted_n[0][splitted_n[0].length-i-1]+n;
 }
 return n+moGlb.localization.f_decimal_separator+(splitted_n.length == 1?'00':splitted_n[1]);
 };*/

/**
 * Return the number of the year week for the date instance
 * @param void
 * @return int week number
 */
Date.prototype.getWeek = function () {
    var M = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334],
            g = this.getDate(), m = this.getMonth(), y = this.getFullYear(),
            b = ((!(y % 4) && y % 100) || !(y % 400)) ? 1 : 0,
            n = g + M[m] + ((b && m > 1) ? 1 : 0),
            j = (new Date(y, 0, 1)).getDay();
    if (!j)
        j = 7;
    var k = (j > 4) ? (j - 2) : (j + 5), w = parseInt((n + k) / 7);
    if (!w)
        return (new Date(y - 1, 11, 31)).getWeek();
    var d = (new Date(y, 11, 31)).getDay();
    if (d < 4 && n > (365 + b - d))
        return 1;
    return w;
}

/**
 * Format the date following the format in input
 * // Week day
 *	"{l}" A full textual representation of the day of the week Sunday through Saturday
 *	"{D}" A textual representation of a day, three letters Sun through Sat
 *	"{D2}" A textual representation of a day, two letters Su through Sa
 *	"{N}" ISO-8601 numeric representation of the day of the week, 1 (for Monday) through 7 (for Sunday)
 *	"{S}" English ordinal suffix for the day of the month, 2 characters, st, nd, rd or th. Works well with j
 *	"{w}" Numeric representation of the day of the week, 0 (for Sunday) through 6 (for Saturday)
 *	"{z}" The day of the year (starting from 0), 0 through 365
 *
 *	// Month day
 *	"{j}" Day of the month without leading zeros 1 to 31
 *	"{d}" Day of the month, 2 digits with leading zeros 01 to 31
 *
 *	// Week
 *	"{W}" ISO-8601 week number of year, weeks starting on Monday
 *
 *	// Month
 *	"{F}" A full textual representation of a month, such as January or March January through December
 *	"{m}" Numeric representation of a month, with leading zeros 01 through 12
 *	"{M}" A short textual representation of a month, three letters Jan through Dec
 *	"{n}" Numeric representation of a month, without leading zeros 1 through 12
 *	"{t}" Number of days in the given month 28 through 31
 *
 *	// Year
 *	"{L}" Whether it's a leap year true if it is a leap year, false otherwise.
 *	"{o}" ISO-8601 year number. This has the same value as Y, except that if the ISO week number (W) belongs to the previous or next year, that year is used instead. Examples: 1999 or 2003
 *	"{Y}" A full numeric representation of a year, 4 digits Examples: 1999 or 2003
 *	"{y}" A two digit representation of a year Examples: 99 or 03
 *
 *	// Time
 *	"{a}" Lowercase Ante meridiem and Post meridiem am or pm
 *	"{A}" Uppercase Ante meridiem and Post meridiem AM or PM
 *	"{B}" Swatch Internet time 000 through 999
 *	"{g}" 12-hour format of an hour without leading zeros 1 through 12
 *	"{G}" 24-hour format of an hour without leading zeros 0 through 23
 *	"{h}" 12-hour format of an hour with leading zeros 01 through 12
 *	"{H}" 24-hour format of an hour with leading zeros 00 through 23
 *	"{i}" Minutes with leading zeros 00 to 59
 *	"{s}" Seconds, with leading zeros 00 through 59
 *	"{u}" Microseconds Example: 654321
 *
 *	// Timezone
 *	"{e}" Timezone identifier Examples: UTC, GMT, Atlantic/Azores
 *	"{I}" Whether or not the date is in daylight saving time 1 if Daylight Saving Time, 0 otherwise.
 *	"{O}" Difference to Greenwich time (GMT) in hours Example: +0200
 *	"{P}" Difference to Greenwich time (GMT) with colon between hours and minutes Example: +02:00
 *	"{T}" Timezone abbreviation Examples: EST, MDT ...
 *	"{Z}" Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive. -43200 through 50400
 *
 *	// Full Date/Time
 *	"{c}" ISO 8601 date 2004-02-12T15:19:21+00:00
 *	"{r}" RFC 2822 formatted date Example: Thu, 21 Dec 2000 16:01:07 +0200
 *	"{U}" Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
 *	"{ts}" Milliseconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
 * @param format string date format
 * @return string the formatted date
 */
Date.prototype.format = function (frmt) {
    function oneTwoDigits(n) {
        return (n < 10 ? "0" + n : n);
    }
    function amPm(h, capital) {
        return (h < 12 ? (capital ? "AM" : "am") : (capital ? "PM" : "pm"));
    }
    function getEnglishOrdinalSuffix(day) {
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }
    function h24to12(h) {
        if (h == 0)
            return 12;
        if (h < 13)
            return h;
        return h - 12;
    }

    return frmt.multiReplace({
        // Week day
        "{l}": moGlb.langTranslate(moGlb.weekDays[this.getDay()]), // A full textual representation of the day of the week Sunday through Saturday
        "{D}": moGlb.langTranslate(moGlb.weekDays[this.getDay()]).substr(0, 3), // A textual representation of a day, three letters Sun through Sat
        "{D2}": moGlb.langTranslate(moGlb.weekDays[this.getDay()]).substr(0, 2), // A textual representation of a day, two letters Su through Sa
        "{N}": "", // ISO-8601 numeric representation of the day of the week, 1 (for Monday) through 7 (for Sunday)
        "{S}": getEnglishOrdinalSuffix(this.getDate()), // English ordinal suffix for the day of the month, 2 characters, st, nd, rd or th. Works well with j
        "{w}": this.getDay(), // Numeric representation of the day of the week, 0 (for Sunday) through 6 (for Saturday)
        // "{z}": this.getDayOfYear(),                                // The day of the year (starting from 0), 0 through 365

        // Month day
        "{j}": this.getDate(), // Day of the month without leading zeros 1 to 31
        "{d}": oneTwoDigits(this.getDate()), // Day of the month, 2 digits with leading zeros 01 to 31

        // Week
        "{W}": this.getWeek(), // ISO-8601 week number of year, weeks starting on Monday

        // Month
        "{F}": moGlb.langTranslate(moGlb.months[this.getMonth()]), // A full textual representation of a month, such as January or March January through December
        "{m}": oneTwoDigits(this.getMonth() + 1), // Numeric representation of a month, with leading zeros 01 through 12
        "{M}": moGlb.langTranslate(moGlb.months[this.getMonth()]).substr(0, 3), // A short textual representation of a month, three letters Jan through Dec
        "{n}": this.getMonth() + 1, // Numeric representation of a month, without leading zeros 1 through 12
        "{t}": this.getMonthDaysNo(), // Number of days in the given month 28 through 31

        // Year
        // "{L}": this.isLeapYear(), // Whether it's a leap year true if it is a leap year, false otherwise.
        "{o}": "", // ISO-8601 year number. This has the same value as Y, except that if the ISO week number (W) belongs to the previous or next year, that year is used instead. Examples: 1999 or 2003
        "{Y}": this.getFullYear(), // A full numeric representation of a year, 4 digits Examples: 1999 or 2003
        "{y}": this.getFullYear().toString().substr(this.getFullYear().toString().length - 2), // A two digit representation of a year Examples: 99 or 03

        // Time
        "{a}": amPm(this.getHours()), // Lowercase Ante meridiem and Post meridiem am or pm
        "{A}": amPm(this.getHours(), true), // Uppercase Ante meridiem and Post meridiem AM or PM
        "{B}": "", // Swatch Internet time 000 through 999
        "{g}": h24to12(this.getHours()), // 12-hour format of an hour without leading zeros 1 through 12
        "{G}": this.getHours(), // 24-hour format of an hour without leading zeros 0 through 23
        "{h}": oneTwoDigits(h24to12(this.getHours())), // 12-hour format of an hour with leading zeros 01 through 12
        "{H}": oneTwoDigits(this.getHours()), // 24-hour format of an hour with leading zeros 00 through 23
        "{i}": oneTwoDigits(this.getMinutes()), // Minutes with leading zeros 00 to 59
        "{s}": oneTwoDigits(this.getSeconds()), // Seconds, with leading zeros 00 through 59
        "{u}": "", // Microseconds Example: 654321

        // Timezone
        "{e}": "", // Timezone identifier Examples: UTC, GMT, Atlantic/Azores
        "{I}": "", // Whether or not the date is in daylight saving time 1 if Daylight Saving Time, 0 otherwise.
        "{O}": "", // Difference to Greenwich time (GMT) in hours Example: +0200
        "{P}": "", // Difference to Greenwich time (GMT) with colon between hours and minutes Example: +02:00
        "{T}": "", // Timezone abbreviation Examples: EST, MDT ...
        "{Z}": "", // Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive. -43200 through 50400

        // Full Date/Time
        "{c}": this.toISOString(), // ISO 8601 date 2004-02-12T15:19:21+00:00
        "{r}": this.toUTCString(), // RFC 2822 formatted date Example: Thu, 21 Dec 2000 16:01:07 +0200
        "{U}": Math.round(this.getTime() / 1000), // Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
        "{ts}": this.getTime() // Milliseconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
    });
};

/**
 * Return true id date is the same as d
 * @param d date the date to compare
 * @return boolean
 */
Date.prototype.hasSameDate = function (d) {
    return this.getFullYear() == d.getFullYear() &&
            this.getMonth() == d.getMonth() &&
            this.getDate() == d.getDate();
};

/**
 * Return true if date and d has the same number of year week
 * @param d date the date to compare
 * @return boolean
 */
Date.prototype.hasSameWeek = function (d) {
    return this.getWeek() == d.getWeek();
};

/**
 * Return true if date and d has the same month
 * @param d date the date to compare
 * @return boolean
 */
Date.prototype.hasSameMonth = function (d) {
    return this.getFullYear() == d.getFullYear() &&
            this.getMonth() == d.getMonth();
};

/**
 * Return the number of days in the date month
 * @param void
 * @return int
 */
Date.prototype.getMonthDaysNo = function () {
    var d = new Date(this.getFullYear(), this.getMonth() + 1, 1);
    d.setDate(0);
    return d.getDate();
};

/**
 * Set the hour based on the 12-hour system
 * @param h int hours
 * @param pm boolean pm/am
 * @return void
 */
Date.prototype.set12Hours = function (h, pm) {
    function h12to24(h, pm) {
        return ((h == 12) ? 0 : h) + ((pm ? 1 : 0) * 12);
    }

    if (h.length == 1)
        h = parseInt(h);
    else
        h = parseInt(h.substr(0, 1)) * 10 + parseInt(h.substr(1, 1));

    this.setHours(h12to24(h, pm));
};

/**
 * Store the delta between client and server date
 * @param void
 * @return void
 */
Date.prototype.setDelta = function () {
    moGlb.deltaDt = this.getTime() - new Date().getTime();
};

/**
 * Return the client data adding the delta
 * @param void
 * @return the server aligned timestamp in seconds (like php)
 */
Date.prototype.toServerTs = function () {
    return ((this.getTime() + moGlb.deltaDt) / 1000).toFixed();
};

var moEvtMng = {
    /**
     * Get the mouse x and y
     * @param e event event
     * @return { float, float }
     */
    getMouseXY: function (e, obj, dx, dy) {
        if (!dx)
            dx = 0;
        if (!dy)
            dy = 0;
        var pos = {l: 0, t: 0};
        if (obj)
            pos = moGlb.getPosition(obj);

        return {
            x: (e.pageX ? e.pageX : e.clientX + document.body.scrollLeft - document.body.clientLeft) - pos.l - dx,
            y: (e.pageY ? e.pageY : e.clientY + document.body.scrollTop - document.body.clientTop) - pos.t - dy
        };
    },
    /**
     * Get the mouse x and y adding an offset
     * @param e event event
     * @param ox int x offset
     * @param oy int y offset
     * @return { float, float }
     */
//    getMouseXYOffset: function (e, ox, oy){
//        var coords = moEvtMng.getMouseXY(e);
//        return {x: coords.x-ox, y: coords.y-oy};
//    },


    /**
     * Get the mouse x and y for touch devices
     * @param e event event
     * @param i int multi touch
     * @return { float, float }
     */
    getTouchXY: function (e, i, obj) {
        if (!i)
            i = 0;
        var pos = {l: 0, t: 0};
        if (obj)
            pos = moGlb.getPosition(obj);
        return {
            x: e.changedTouches[i].pageX - pos.l,
            y: e.changedTouches[i].pageY - pos.t
        }
    },
    /**
     * Cancel the event propagation
     * @param e event event
     * @return void
     */
    cancelEvent: function (e) {
        e.returnValue = false;
        if (e.preventDefault)
            e.preventDefault();
        e.cancelBubble = true;
        if (e.stopPropagation)
            e.stopPropagation();
        return false;
    },
    /**
     * Add an event to an object
     * @param o object the object on which the event has to be attached
     * @param the type of event (without on)
     * @param f function the function to be executed on event rising
     * @return void
     */
    addEvent: function (o, t, f) {
        var d = 'addEventListener';
        if (o[d])
            o[d](t, f, false);
        else
            o['attachEvent']('on' + t, f);
    },
    /**
     * Cancel the event propagation
     * @param o object the object on which the event has to be detached
     * @param t the type of event (without on)
     * @param f function the function to be executed on event rising
     * @return void
     */
    subEvent: function (o, t, f) {
        var d = 'removeEventListener';
        if (o[d])
            return o[d](t, f, 0);
        return o['detachEvent']('on' + t, f);
    },
    /**
     * Cancel the event propagation
     * @param o object the object on which the event has to be detached
     * @param t the type of event (without on)
     * @param f function the function to be executed on event rising
     * @return void
     */
    onWheel: function (event) {
        var d = 0;
        if (!event)
            event = window.event;
        if (event.wheelDelta) {
            d = event.wheelDelta / 120;
            if (window.opera)
                d = -d;
        } else if (event.detail) {
            d = -event.detail / 3;
        }
        if (d && WHEELOBJ)
            eval("if(" + WHEELOBJ + ") " + WHEELOBJ + ".Wheel(d)");

        for(var i = 0; i < event.path.length; i++){
            if(event.path[i].id != undefined && event.path[i].id.indexOf('wizard_asset_') != -1) return;
        }

        if (event.preventDefault)
            event.preventDefault();
        event.returnValue = false;
    }
};

WHEELOBJ = "";
if (window.addEventListener)
    window.addEventListener('DOMMouseScroll', moEvtMng.onWheel, false);
window.onmousewheel = document.onmousewheel = moEvtMng.onWheel;

/**
 * @object moResize static
 * Manage the components resize on page resize
 */
var moResize = {
    els: [], // array of string to store the name of the object to be resized
    wait: false, // time to wait before firing the resize event

    /**
     * Add an object name to the elements list
     * @param name string object name
     * @return void
     */
    add: function (name) {
        moArray.pushOnce(moResize.els, name);
    },
    /**
     * Remove an object from the list: the object will not be able to be resized on page resize event anymore
     * @param name string object name
     * @return void
     */
    sub: function (name) {
        delete(moResize.els[name]);
    },
    /**
     * Call the resize method for every object in the list, if it exists. Called by the window.onresize event
     * @param void
     * @return void
     */
    onResize: function (resize) {
        with (moResize) {
            if (typeof (resize) != "number" && wait)
                return;
            if (moGlb.getClientWidth() != moGlb.oldw || moGlb.getClientHeight() != moGlb.oldh) {
                with (moGlb) {
                    oldw = getClientWidth(), oldh = getClientHeight();
                }
                wait = 1;
                setTimeout("moResize.onResize(1)", 500);
                return;
            }

            wait = 0;
            for (var i = 0; i < els.length; i++)
                try {
                    eval(els[i] + ".resize()");
                } catch (e) { /* moDebug.log(el + " has not any resize method"); */
                }
        }
    }
};

//moEvtMng.addEvent(window, 'onorientationchange' in window ? 'orientationchange' : 'resize', moResize.onResize);
moEvtMng.addEvent(window, 'resize', moResize.onResize);
//window.onresize = moResize.onResize;

// resize and rotation ipad-iphone-android
//window.addEventListener('onorientationchange' in window ? 'orientationchange' : 'resize', moResize.onResize, false);

var moCnvUtils = {
    /**
     * Get the width for a text
     * @param t string text
     * @param f string font size and family
     * @return float
     */
    measureText: function (t, f) {
        if (!f)
            f = moGlb.fontNormal;
        j = moGlb.createElement("tempcanvas", "canvas");
        j.width = "10", j.height = "10";
        with (j.getContext('2d')) {
            font = f;
            return measureText(t).width;
        }
    },
    measureDivText: function (t, f) {
        if (!f)
            f = moGlb.fontNormal;
        var nm = "tempmeasurediv", j = mf$(nm);
        if (!j) {
            var j = document.createElement("div");
            j.id = nm;
            document.body.appendChild(j);
            with (j.style) {
                position = "absolute";
                top = "-500px";
                whiteSpace = "nowrap";
            }
        }
        j.style.font = f;
        j.innerHTML = t;
        return j.scrollWidth;
    },
    // FlaticonStroke view in html 
    IconFont: function (cod, height, color) {
        if (!height)
            height = 16;
        if (!color)
            color = "#4A6671";
        return "<span style='font:" + height + "px FlaticonsStroke;color:" + color + ";'>" + String.fromCharCode(cod) + "</span>";
    },
    // FlaticonStroke view in canvas 
    cnvIconFont: function (cj, cod, x, y, height, color) {
        if (!height)
            height = 16;
        if (!color)
            color = "#4A6671";
        with (cj) {
            save();
            fillStyle = color;
            font = height + "px FlaticonsStroke";
            textBaseline = "alphabetic";
            textAlign = "left";
            fillText(String.fromCharCode(cod), x, y + height);
            restore();
        }
    },
    /*drawRect: function (ctx, x, y, w, h, fillColor, borderOnly) {
     with(ctx) {
     if(borderOnly) {
     strokeStyle = fillColor;
     strokeRect(x, y, w, h);
     } else {
     fillStyle = fillColor;
     fillRect(x, y, w, h);
     }
     }
     },*/

    /**
     * Draws a rounded-angle rectangle
     * @param ctx object canvas context
     * @param w int rectangle width
     * @param h int rectangle height
     * @param bg string rectangle background color
     * @param border string rectangle border color
     * @param borderWidth int rectangle border width
     * @param borderRadius int rectangle border radius
     * @return void
     */
    drawRoundedRect: function (ctx, w, h, bg, border, borderWidth, borderRadius) {
        with (ctx) {
            var b = parseInt(borderRadius / 3 * 2);
            fillStyle = bg;
            if (borderWidth > 0)
                strokeStyle = border, lineWidth = borderWidth;
            beginPath();
            moveTo(0.5, borderRadius);
            bezierCurveTo(0.5, b, borderRadius - b, 0.5, borderRadius, 0.5);
            lineTo(w - borderRadius, 0.5);
            bezierCurveTo(w - borderRadius + b, 0.5, w - 0.5, b, w - 0.5, borderRadius);
            lineTo(w - 0.5, h - borderRadius);
            bezierCurveTo(w - 0.5, h - borderRadius + b, w - borderRadius + b, h - 0.5, w - borderRadius, h - 0.5);
            lineTo(borderRadius, h - 0.5);
            bezierCurveTo(borderRadius - b, h - 0.5, 0.5, h - borderRadius + b, 0.5, h - borderRadius);
            closePath();
            fill();
            if (borderWidth > 0)
                stroke();
        }
    },
    roundRect: function (cj, x, y, w, h, r) {
        with (cj) {
            beginPath();
            moveTo(x + r, y);
            lineTo(x + w - r, y);
            lineTo(x + w, y + r);
            lineTo(x + w, y + h - r);
            lineTo(x + w - r, y + h);
            lineTo(x + r, y + h);
            lineTo(x, y + h - r);
            lineTo(x, y + r);
            closePath();
        }
    },
    /**
     * Writes a string in a canvas
     * @param ctx object canvas context
     * @param text string the string to write
     * @param x int text x coordinate
     * @param y int text y coordinate
     * @param color string text color
     * @param fontStyle string text style
     * @param align string text alignment (default center)
     * @return void
     */
    write: function (ctx, text, x, y, color, fontStyle, align) {
        with (ctx) {
            fillStyle = color ? color : moColors.text;
            font = fontStyle ? fontStyle : moGlb.fontNormal;
            textBaseline = "alphabetic";
            textAlign = align ? align : "center";
            try {
                fillText(text, x, y);
            } catch (ex) {
            }
        }
    },
    writeDiv: function (tx, x, y, colr, fntStyle, algn, idiv) {
        var cL = colr ? colr : moColors.text,
                fN = fntStyle ? fntStyle : moGlb.fontNormal,
                tA = algn ? algn : "center";
        idiv = idiv ? "id='" + idiv + "'" : "";
        return "<div " + idiv + " style='position:absolute;left:" + x + "px;top:" + y + "px;font:" + fN + ";color:" + cL + ";text-align:" + tA + ";'>" + tx + "</div>";
    },
    /**
     * Creates a new canvas
     * @param name string canvas id
     * @param w int canvas width
     * @param h int canvas height
     * @param style string canvas style
     * @param className string the name of a css class
     * @param container object the dom object that contains the new canvas
     * @return object the new canvas
     */
    createCanvas: function (name, w, h, style, className, container, rPosition) {
        cnv = moGlb.createElement(name, "canvas", (container ? container : document.body));
        if (style)
            cnv.setAttribute("style", "position:" + (rPosition ? "relative" : "absolute") + ";" + style);
        if (w)
            cnv.setAttribute("width", w);
        if (h)
            cnv.setAttribute("height", h);
        if (className)
            cnv.setAttribute("class", className);
        return cnv;
    },
    arrayCnvImg: {}, // [ img_obj, canvas_imageData  ]   index: url   value: canvas image / avatar

    draw_canvas_image: function (url, cnvj, l, t, w, h, ll, flk) {    // url, canvas_obj, left,top. width,height, ll for clip()

        if (typeof (moCnvUtils.arrayCnvImg[url]) == "undefined") {

            moCnvUtils.arrayCnvImg[url] = [];

            var cnv = moGlb.createElement("id_draw_canvas_image", "canvas"),
                    cj = cnv.getContext("2d"),
                    im = moCnvUtils.arrayCnvImg[url];

            with (cnv.style)
                left = 0, top = 0;
            cnv.width = w, cnv.height = h;

            im[0] = new Image();
            im[0].src = url;

            im[0].onload = function () {
                cj.drawImage(im[0], 0, 0, w, h);
                im[1] = cj.getImageData(0, 0, w, h);
                moCnvUtils.clip_canvas_putimage(cnvj, im[1], l, t, ll, flk);
            }

        } else
            moCnvUtils.clip_canvas_putimage(cnvj, moCnvUtils.arrayCnvImg[url][1], l, t, ll, flk);

    },
    clip_canvas_putimage: function (cnvj, uj, l, t, ll, flk) {

        // check l<ll
        // console.log(l+" | "+ll+" locl: "+flk)
        try {
            if (flk || l > ll)
                cnvj.putImageData(uj, l, t);
        } catch (e) {
        }

    }


};

/**
 * Static object of array utilities
 */
var moArray = {
    /**
     * Push the element el in the array a in the pos position only if
     * the element is not already in it
     * @param a array the array
     * @param el any the element to push
     * @param pos int the position
     * @return el on success otherwise false
     */
    pushOnce: function (a, el, pos) {
        if (!moArray.hasElement(a, el)) {
            if (!pos && pos != 0)
                pos = a.length;
            a.splice(pos, 0, el);
            return el;
        }
        return false;
    },
    /**
     * Removes the element el from the array a
     * @param a array the array
     * @param el any the element to remove
     * @return void
     */
    remove: function (a, el) {
        for (var i in a)
            if (a[i] == el)
                a.splice(i, 1);
    },
    /**
     * Replace an element with another
     * @param a array the array
     * @param oldel any the element to replace
     * @param newel any the new element
     * @return void
     */
    replace: function (a, oldel, newel) {
        for (var i in a) {
            if (a[i] == oldel)
                if (moArray.pushOnce(a, newel, i) == newel)
                    moArray.remove(a, oldel);
            return true;
        }
        return false;
    },
    /**
     * Return true if the array contains the element
     * @param a array the array
     * @param el any the element to find
     * @return boolean
     */
    hasElement: function (a, el) {
        for (var i = 0; i < a.length; i++)
            if (a[i] == el)
                return true;
        return false;
    },
    compare: function (a, b, lookOrder) {
        if (a.length != b.length)
            return false;
        eval("var c = a, d = b;");
        if (!lookOrder) {
            c.sort();
            d.sort();
        }
        for (var i in c)
            if (c[i] != d[i])
                return false;
        return true;
    },
    merge: function (a1, a2, once) {
        with (moArray) {
            if (!once)
                return a1.concat(a2);
            var merged = clone(a1);
            for (var i in a2)
                pushOnce(merged, a2[i]);
            return merged;
        }
    },
    clone: function (a) {
        if (!a)
            return;
        var clone = (typeof a.length == "undefined" ? {} : []);
        for (var i in a) {
            if (typeof a[i] == "object")
                clone[i] = moArray.clone(a[i]);
            else
                clone[i] = a[i];
        }
        return clone;
    },
    contains: function (needle, haystack) {
        for (var i in haystack)
            if (haystack[i] == needle)
                return true;
        return false;
    },
    intersect: function (a1, a2) {
        var result = [];
        for (var i in a1)
            if (moArray.contains(a1[i], a2))
                result.push(a1[i]);
        return result;
    },
    /**
     * Apply a user function to every member of an array
     * @param a array the array
     * @param f function function to be applied
     * @return array
     */
    walk: function (a, f, clone) {
        var c = a;
        if (clone)
            c = moArray.clone(a);
        for (var i in c)
            c[i] = f(c[i]);
        return c;
    }
};

var moKeyMng = {
    SHIFT: 16,
    CTRL: 17,
    ESC: 27,
    BACKSPACE: 8,
    TAB: 9,
    ENTER: 13,
    pressed: [],
    onkeydown: function (e) {
        with (moKeyMng) {
            if (!e)
                e = window.event;
            var key = (e.which) ? e.which : e.keyCode;
            if (!moArray.hasElement(pressed, key))
                moArray.pushOnce(pressed, key);
        }
    },
    onkeyup: function (e) {
        with (moKeyMng) {
            if (!e)
                e = window.event;
            var key = (e.which) ? e.which : e.keyCode;
            if (moArray.hasElement(pressed, key))
                moArray.remove(pressed, key);
        }
    },
    hasKey: function (key) {
        with (moKeyMng) {
            return moArray.hasElement(pressed, key);
        }
    }
};

document.onkeydown = moKeyMng.onkeydown;
document.onkeyup = moKeyMng.onkeyup;