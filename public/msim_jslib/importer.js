// Importer
Importer={pth: "", module: "", 
           alabl:{},abook:{}, slabl:[], sbook:[], BKM:0, SEQUE:[], DISEQ:[],
           labl: [],book:[], heads:[], seque:[], diseq:[], sheets:[], keyshe:[], proj:[], 
            aKey:[], Nb:0, Nh:0, I:0, filen:"",


Start:function(mdl) {
with(Importer){
    pth = moGlb.imgDefaultPath+"_importer/";
    module = mdl;
var s=//"<div style='position:absolute;left:50%;top:50%;width:540px;height:520px; margin-left:-270px;margin-top:-260px; border:1px solid #AAA;background-color:#FFF;'>"+
 
"<!-- choose xls file -->"+
"<div style='position:absolute;left:10px;top:10px;font:bold 11px Arial;'>"+moGlb.langTranslate("Choose xls/xlsx file")+"</div>"+
"  <iframe name='ifrmimport' src='' style='display:none;'></iframe>"+
"      <form name='frmimport' method='post' ENCTYPE='multipart/form-data' target='ifrmimport'>"+
"        <input name='fileattach' type='file' style='position:absolute;left:10px;top:24px;font:11px Arial;width:190px;height:22px;' />"+
"      </form>"+
"  <button onclick='Importer.Read()' style='position:absolute;left:10px;top:56px;font:11px Arial;width:84px;height:26px;line-height:26px;overflow:hidden;'>"+
"  <img src='"+pth+"attach.png' /><span style='position:relative;top:-3px;margin-left:8px;'>"+moGlb.langTranslate("Upload")+"</span>"+
"  </button>"+

"<!-- select sheet -->"+
"<div style='position:absolute;left:380px;top:10px;font:bold 11px Arial;'>"+moGlb.langTranslate("Select the Sheet")+"</div>"+
"  <div id='idDivSheet' style='position:absolute;left:380px;top:26px;width:160px;'>"+
"  <select id='idSelSheet' style='position:absolute;left:0px;width:150px;height:22px;padding:3px;font:11px Arial;border:1px solid #496670;' onchange=''><option value=0>"+moGlb.langTranslate("Choose Sheet")+"</option></select>"+
"  </div>"+

"<!-- select project -->"+
"<div style='position:absolute;left:380px;top:60px;font:bold 11px Arial;display:none'>"+moGlb.langTranslate("Select the Project")+"</div>"+
"  <div id='idDivPrj' style='position:absolute;left:380px;top:76px;width:160px;display:none'>"+
"  <select id='idSelPrj' style='position:absolute;left:0px;width:150px;font:11px Arial;height:22px;padding:3px;border:1px solid #496670;'><option value=0>Default</option></select>"+
"  </div>"+

"<!-- select bookmark -->"+
"<div style='position:absolute;left:380px;top:60px;font:bold 11px Arial;display:block'>"+moGlb.langTranslate("Select the Bookmark")+"</div>"+
"  <div id='idDivBkj' style='position:absolute;left:380px;top:76px;width:160px;display:block'>"+
"  <select id='idSelBkj' style='position:absolute;left:0px;width:150px;font:11px Arial;height:22px;padding:3px;border:1px solid #496670;'><option value=0>none</option></select>"+
"  </div>"+

"<!-- select type_id -->"+
"<div style='position:absolute;left:220px;top:60px;font:bold 11px Arial;display:none'>"+moGlb.langTranslate("Choose type_id")+"</div>"+
"  <div id='idDivTyp' style='position:absolute;left:220px;top:76px;width:160px;display:none'>"+
"  <select id='idSelTyp' style='position:absolute;left:0px;width:140px;font:11px Arial;height:22px;padding:3px;border:1px solid #496670;'><option value=0>"+moGlb.langTranslate("Choose Type_id")+"</option></select>"+
"  </div>"+

"<!-- Description -->"+
"<div style='position:absolute;left:10px;top:100px;width:520px;font:11px Arial;'>"+
"<b>"+moGlb.langTranslate("Customize Mapping")+"</b><br>"+moGlb.langTranslate("The column headers in the xls/xlsx file are displayed in the fields below. Kindly choose the appropriate column header for the drop down boxes below.")+ 
"</div>"+

"<!-- select/deselect all -->"+
"<div style='position:absolute;left:10px;top:476px;width:520px;text-align:right;'>"+
"<img src='"+pth+"arrow_right1.png' style='cursor:pointer;' onclick='Importer.seldesel(1)' />&nbsp;&nbsp;"+
"<img src='"+pth+"arrow_right0.png' style='cursor:pointer;' onclick='Importer.seldesel(0)' />&nbsp;&nbsp;"+
"</div>"+

"<!-- Associations -->"+
"<div id='idAssoc' style='position:absolute;left:10px;top:148px;width:520px;height:324px;font:11px Arial;overflow:auto;border:1px solid #aaa;border-radius:5px;'></div>"+

"<!-- Cancel -->"+
"<button onclick='Importer.Cancel()' style='position:absolute;left:10px;bottom:10px;font:11px Arial;width:84px;height:26px;line-height:26px;overflow:hidden;'>"+
"<img src='"+pth+"cancel.png' /><span style='position:relative;top:-3px;margin-left:8px;'>"+moGlb.langTranslate("Cancel")+"</span>"+
"</button>"+

"<!-- Action -->"+
"<div style='position:absolute;left:278px;bottom:15px;font:bold 11px Arial;text-align:right;'>Action</div>"+
"<select id='idSelAction' style='position:absolute;left:320px;width:120px;height:22px;padding:3px;bottom:12px;font:11px Arial;border:1px solid #496670;'>"+
"<option value=0>"+moGlb.langTranslate("Import")+"</option>"+
"<option value=1>"+moGlb.langTranslate("Update")+"</option>"+
"<option value=2>"+moGlb.langTranslate("Recover")+"</option>"+
"</select>"+

"<!-- Import -->"+
"<button onclick='Importer.ActImport()' style='position:absolute;left:450px;bottom:10px;font:11px Arial;width:80px;height:26px;line-height:26px;overflow:hidden;'>"+
"<img src='"+pth+"save.png' /><span style='position:relative;top:-3px;margin-left:8px;'>"+moGlb.langTranslate("Import")+"</span>"+
"</button>"+

"<!-- waiting upload -->"+
"<div id='idmaskimport' style='display:none;'>"+
"<div style='position:absolute;left:0;top:0;width:540px;height:545px;background-color:#FFF;opacity:0.6;'></div>"+
"<img src='"+pth+"loader.gif' style='position:absolute;left:246px;top:40%;' >"+
"</div>"+

"<!-- form iframe -->"+
"<iframe name='imfrm' src='' style='display:none;' ></iframe>"+
"<form name='frmimp' src='' method='post' target='imfrm'>"+
"<input name='param' type='hidden' value='' />"+
"</form>";

//+
//"</div>";


mf$("idCntImport").innerHTML=s;

    var win = null, div = mf$("idCntImport");    
    //mf$(win.getContainerId()).appendChild(div);
    div.style.display = "block";
    try {win = eval("idCntImport_win");win.name;} catch (ex) {win = moComp.createComponent("idCntImport_win", "moPopup", {contentid: div.id, width: 540, height: 540}, true);}
    win.show({title: moGlb.langTranslate("Embedded Importer")});
}},

seldesel:function(m){
with(Importer){  

 if(typeof(heads[I])=="undefined") return; 
 var n,v,dj, nhe=heads[I].length;     
 diseq[I]=[];
 for(n=0;n<nhe;n++){ 
    v=heads[I][n];dj=mf$("idSelBook"+n);
    if(m) {dj.removeAttribute("disabled");             
    } else {diseq[I].push(v); 
             dj.setAttribute("disabled",true);
    }  
    mf$("idDiSeq"+n).src=pth+"arrow_right"+m+".png"; 
 }
}},

Read:function(){
with(Importer){              
  var fj=document.forms["frmimport"], NM=fj.elements["fileattach"].value;
  if(!NM) {moGlb.alert.show(moGlb.langTranslate("Choose xls/xlsx File!"));return;} 
  
  // verifico estensione file .xls
  var ext=NM.split(".").pop();
  if(ext!="xls" && ext!="xlsx") {moGlb.alert.show(moGlb.langTranslate("Incorrect file type!"));return;}
  
  fj.action = moGlb.baseUrl+"/module-importer/upload?fnc=Importer.UpldRisp&module="+module;
  fj.submit();
  
  // waiting
  mf$("idmaskimport").style.display='block'; 
   
}},





UpldRisp:function(risp,err){  // err=1   error
with(Importer){  
 
  mf$("idmaskimport").style.display='none';
  if(err) {moGlb.alert.show(moGlb.langTranslate(risp));return;}

  var a, ns,np,r,str,nty,k;
  try { a=JSON.parse(risp); } catch(e){  alert(moGlb.langTranslate(e)+"\n"+moGlb.langTranslate(risp)); return;  }
  
  // bookmark   headers     sheetname
  abook=a.bookmark;
  alabl=a.labels;
  
  sbook=[], slabl=[];
  
  for(k in abook) {  sbook.push(k); }
  for(k in alabl) slabl.push(k);
 
  
  book = abook[ sbook[0] ]; Nb=book.length;
  labl = alabl[ slabl[0] ];
  heads = a.headers; Nh=heads.length;
  
  seque = a.sequence;
  SEQUE = moGlb.cloneArray(seque);
  diseq = a.disabledsequence;
  DISEQ = moGlb.cloneArray(diseq);
  
  sheet = a.sheetname; ns=sheet.length;
  keyshe = a.keysheet;
  proj = a.projects;   np=proj.length;
  filen= a.filename;    
 
  typls = a.typelist; nty=0;
 
 
  if(!Nb || !Nh) {moGlb.alert.show(moGlb.langTranslate("Number of field error!"));return;}
  if(ns!=Nh) {sheet=[]; for(r=0;r<Nh;r++) sheet.push("Sheet"+r);}     // if sheet name doesn't exist
  
   // create select of sheet
   str="<select id='idSelSheet' style='position:absolute;left:0px;width:150px;height:22px;padding:3px;font:11px Arial;border:1px solid #496670;' onchange='Importer.ChangeSheet(this.value,0)'>";
   for(r=0;r<Nh;r++) str+="<option value='"+r+"'>"+sheet[r]+"</option>";
   str+="</select>";
   
   mf$("idDivSheet").innerHTML=str;
  
  
   // create select of bookmark
   str="<select id='idSelBkj' style='position:absolute;left:0px;width:150px;font:11px Arial;height:22px;padding:3px;border:1px solid #496670;' onchange='Importer.ChangeBook(this.value)'>";
   for(r=0;r<sbook.length;r++) str+="<option value='"+r+"'>"+sbook[r]+"</option>";
   str+="</select>";
  
   mf$("idDivBkj").innerHTML=str;
  
  
  // create select of type_id
   str="<select id='idSelTyp' style='position:absolute;left:0px;width:140px;height:22px;padding:3px;font:11px Arial;border:1px solid #496670;'>";
   
   for(k in typls){ nty++;   
    str+="<option value='"+k+"'>"+typls[k]+"</option>";
   } 
   str+="</select>";
   
   mf$("idDivTyp").innerHTML=str;
 
   BKM=0;
   ChangeSheet(0);
}},



ChangeBook:function(b){  
with(Importer){
  
  BKM=b;
    
  book = abook[ sbook[b] ]; 
  Nb=book.length;
  labl = alabl[ slabl[b] ];
  ChangeSheet(I,1);
}},




ChangeSheet:function(i,m){  
with(Importer){
 
    I=i;
    // if change sheet set bookmark default 0
    if(!m) { mf$("idSelBkj").options[0].selected=true; ChangeBook(0); return; }
    
    
    var k,v,r,n,b,sv, slt, Ky, str="", HE=heads[i], nhe=HE.length, Sq,br,    // numero colonne header sheet i
        selfld="", selOpt="";
    
    
    if(nhe>Nb) {moGlb.alert.show(moGlb.langTranslate("The number of fields of xls/xlsx file exceeds the list on the drop down box!"));}
    

 if(!BKM){
  if(typeof(diseq[i])!="undefined") { diseq[i]=moGlb.cloneArray(DISEQ[i]); }
  if(typeof(seque[i])!="undefined") { seque[i]=moGlb.cloneArray(SEQUE[i]); }
 } else{
  diseq=[];
  seque=[]; 
 }
 
 
    // se non esiste una disabled sequence diseq
    if(typeof(diseq[i])=="undefined") {
      diseq[i]=[];
    }
    
    
    // se non esiste una sequenza registrata
    if(typeof(seque[i])=="undefined") {
      seque[i]={};
      for(r=0;r<nhe;r++){
        v=heads[i][r];
        br=book[r];  
        if(typeof(br)=="undefined") br=book.length-1; 
        seque[i][v]=br;                                   // ordine default
      }                                   
    }
    Sq=seque[i];
    
 
    
    
    // se non esiste key (keyshe[i])
    Ky=[];
    if(typeof(aKey[i])!="undefined"){ 
      Ky=aKey[i];  
    } else {
      if(typeof(keyshe[i])=="undefined") keyshe[i]=[];
      else {            
        for(r=0;r<nhe;r++){
          v=heads[i][r]; 
          if(typeof(keyshe[i][v])!="undefined") { Ky.push(r); break; }
        }
      }
    }
   
    aKey[i]=Ky;
    
    for(r=0;r<nhe;r++){
      
      v=heads[i][r];
      sv=Sq[v];
      selOpt="";

          // built select row
          for(n=0;n<Nb;n++) {
            slt="";
            b=book[n];
            lb=labl[n]; 
            if(b==sv) slt="selected"; 
            selOpt+="<option value='"+b+"' "+slt+">"+lb+" ("+b+")</option>"; 
          }   
                   
          k=0;   if(Ky.indexOf(r)>-1) k=1;
          enb=1; if(diseq[i].indexOf(v)>-1) enb=0;                   
                                 
          selfld="<select id='idSelBook"+r+"' "+(enb?"":"disabled")+" style='position:absolute;width:210px;height:22px;font:11px Arial;border:1px solid #819BA3;padding:3px;'"+
                 " onchange='Importer.ChgRowAssoc("+r+","+i+")'>"+selOpt+"</select>";
 

         // row
          str+="<div style='position:absolute;;left:8px;top:"+(8+30*r)+"px;width:490px;height:28px;'>"+
               "<img id='idKeySheet"+r+"' src='"+pth+"key"+k+".png' style='position:absolute;left:0;top:6px;cursor:pointer;' onclick='Importer.ChgKeySheet("+r+","+i+")' />"+
               "<div style='position:absolute;left:24px;top:4px;width:210px;height:20px;border:1px solid #819BA3;font:11px Arial;overflow:hidden;'>"+
               
               "<div style='margin:3px;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;'>"+v+"</div></div>"+
               
               "<img id='idDiSeq"+r+"' src='"+pth+"arrow_right"+enb+".png' style='position:absolute;left:248px;top:6px;cursor:pointer;' onclick='Importer.ChgDisabSeq("+r+","+i+")' />"+
               "<div style='position:absolute;left:275px;top:4px;'>"+selfld+"</div>"+

               "</div>";  
    }
    
    mf$("idAssoc").innerHTML=str;
}},


ChgDisabSeq:function(r,i){  // n = N. row |i = sheet
with(Importer){

    var b=1, v=heads[i][r], n, nhe=heads[i].length, dj=mf$("idSelBook"+r);
    if(diseq[i].indexOf(v)>-1) b=0; 
    
    b=b?0:1; // inverto stato enable
    mf$("idDiSeq"+r).src=pth+"arrow_right"+b+".png";
  
    if(b) {  // tolgo v da array
             for(n=0;n<diseq[i].length;n++) {  
                   if(diseq[i][n]==v) {diseq[i].splice(n,1);break;}
             }
             dj.removeAttribute("disabled"); 
              
    } else { // aggiungo v  disabled
             diseq[i].push(v); 
             dj.setAttribute("disabled",true);       
    }

 
// alert(diseq[i].join())

}},


ChgRowAssoc:function(n,i){  // n = N° select bookmarc | i = sheet
with(Importer){

    var k,v, Sq=seque[i];
    var sj=mf$("idSelBook"+n),
        key=heads[i][n],
        oldval=Sq[key],
        newval=sj.value;

    for(k in Sq) {if(k==key) continue;
      v=Sq[k];  
      if(v==newval) {moGlb.alert.show(moGlb.langTranslate("Value already assigned!"));break;}
    }

    Sq[key]=newval;

}},


ChgKeySheet:function(n,i){  // n = N° row | i = sheet
with(Importer){

    var r, nhe=heads[i].length, Ky=aKey[i];
    
    //alert(n+" | "+typeof(n)+"\n"+JSON.stringify(Ky) )
    
    for(r=0;r<nhe;r++) mf$("idKeySheet"+r).src=pth+"key0.png";
    
    // remove
    if(Ky.indexOf(n)>-1) { 
      r=Ky.indexOf(n);
      Ky.splice(r,1);
    } else {
      Ky.push(n);
    }
    
    for(r=0;r<Ky.length;r++) mf$("idKeySheet"+Ky[r]).src=pth+"key1.png";
    
    aKey[i]=Ky;
}},


Cancel:function(){
    mf$("idCntImport").innerHTML="";
    eval("idCntImport_win").hide();
},


 


ActImport:function(){
with(Importer){
  if(Nh){
    
    var r,v, nhe=heads[I].length, ar=[], Sq=seque[I], er=0;
 
    for(k in Sq) { 
      v=Sq[k];
       if(diseq[I].indexOf(k)>-1) continue;  // if disabled
       
      if( ar.indexOf(v)>-1 ) {moGlb.alert.show(moGlb.langTranslate("Field:")+" "+v+" "+moGlb.langTranslate("Assignment duplicated!"));return;}
      
      if(/*v=="f_type_id" ||*/ v=="f_parent_code") er++;
      ar.push(v);
    } // 
    
    var Ky=aKey[I], aj=mf$("idSelAction"), mode=parseInt(aj.value);   // se 0: add / 1: add value                   // replace / 2: add value
    
  // se nessuna chiave indicata per add value e rollback  
  if(mode>=1 && !Ky.length) {moGlb.alert.show(moGlb.langTranslate("Primary key is not defined!"));return;}     // 1 = ex mode=2

    
  var asq={},z; for(k in Sq) {z=Sq[k]; asq[k]=z;} 
    
  //if(er<1) {moGlb.alert.show(moGlb.langTranslate("Choose the field 'f_parent_code' in the list"));return;}  
    
   var f_type_id=mf$("idSelTyp").value;
    
  // start importing ajax   
  var rnd=parseInt(10000*(Math.random())), param={"f_type_id":f_type_id, "sequence":asq, "disabledsequence":diseq[I], "mode":mode, "keysheet":Ky, "module":module, "sheetname":sheet[I], "filename":filen}; 

  param=JSON.stringify(param); 
  
  
  
  Ajax.sendAjaxPost(moGlb.baseUrl+"/module-importer/elaborate", "param="+param, "Importer.EndImport", true);
  
  /*
  var frj=document.forms["frmimp"];
  frj.elements["param"].value=param;  
  frj.action=moGlb.baseUrl+"/module-importer/elaborate?rnd="+rnd;
  frj.submit();
  
    eval("idCntImport_win").hide();
    moGlb.loader.show(moGlb.langTranslate("Importing: please wait..."));
    
  */  
  }
  moGlb.loader.show(moGlb.langTranslate("Importing: please wait..."));
    
}},


EndImport: function(a) {
        moGlb.loader.hide();
        try { a = JSON.parse(a); if(a && moGlb.isset(a.message)) moGlb.alert.show(moGlb.langTranslate(a.message)); } catch(ex) {}    
        Importer.Cancel();
        eval(Importer.module).refresh();
        eval("idCntImport_win").hide();
    }
} // end Import