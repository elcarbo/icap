
u="undefined", q="px"; 

function isNum(num) { return (!isNaN(num) && !isNaN(parseFloat(num))); }

function js$(l){ return document.getElementById(l)}


Global={ WW:0, HH:0, OW:0, OH:0, Wait:0, Fnc:"",

Start:function(f){
with(Global){
  Size();
  addEvent(window,'onorientationchange' in window ? 'orientationchange' : 'resize', Resize);
  if(f) { Fnc=f; Resize(); }
}},
  
Resize:function(a){   
with(Global){ 
  if(typeof(a)!="number" && Wait) return;
  var W=jsW(), H=jsH(); 
  if(OW!=W || OH!=H) { OW=W, OH=H, Wait=1; setTimeout("Global.Resize(1)",500); return; }
  Wait=0;

  // azioni resize 
  WW=W, HH=H;
  try{ if(Fnc) eval(Fnc); } catch(e) {}                                       // Fnc gestore resize                                  
  } 
},


Size:function(){
  with(Global){
     WW=jsW();
     HH=jsH();  
}}


} //


function jsGetPos_ltwh(j){ 
var L=0,T=0,W=j.offsetWidth,H=j.offsetHeight;
while(j.offsetParent){ 
L+=j.offsetLeft; T+=j.offsetTop;
j=j.offsetParent; } 
L+=j.offsetLeft; T+=j.offsetTop;
return {l:L,t:T,w:W,h:H};
} //


function jsCreateElem(nm,tpe,pdr){ 
if(!js$(nm)) { 
if(!pdr) pdr=document.body;
var j=document.createElement(tpe);
j.id=nm; pdr.appendChild(j);
j.style.position="absolute";
return j;
}
else return js$(nm);
} 


function jsBrowser(){
var NAV=navigator.userAgent.toUpperCase(), NAP=navigator.appVersion;

isIphone = (/iphone/gi).test(NAP),
isIpad = (/ipad/gi).test(NAP),
isAndroid = (/android/gi).test(NAP),
isMobile =  (/mobile/gi).test(NAV),
isTouch = isIphone || isIpad || isAndroid || isMobile;

BROWSER="ALTRO";  Browser=-1;
var BS=['MSIE','FIREFOX','CHROME','SAFARI','OPERA'];
for(var r=0;r<BS.length;r++) if(NAV.indexOf(BS[r])>-1) { BROWSER=BS[r]; Browser=r; break; } 
} //



function jsH(){
if (window.innerWidth!=window.undefined) return window.innerHeight;
if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;
if (document.body) return document.body.clientHeight;
}  
  
  
function jsW(){
if (window.innerWidth!=window.undefined) return window.innerWidth;
if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth;
if (document.body) return document.body.clientWidth;
} 




function jsCoords(e){ e=e||window.event;  if(e.pageX || e.pageY) return {x:e.pageX, y:e.pageY}; 
return { x:e.clientX+document.body.scrollLeft-document.body.clientLeft, 
         y:e.clientY+document.body.scrollTop-document.body.clientTop }; 
}

function jsTCoords(e){ 
return { x:e.changedTouches[0].pageX,
			   y:e.changedTouches[0].pageY }			   
}



function jsCloneArray(A){ var C=(A.length)?[]:{};
for(D in A) C[D]=(typeof(A[D])=='object')?jsCloneArray(A[D]):A[D];
return C;
}

 
function jsTrim(s){
  return s.replace(/^\s+|\s+$/g,""); 
}




function jsOpacity(oj,op){
op=(op==100)?99.999:op;
oj.style.filter="alpha(opacity:"+op+")";
oj.style.MozOpacity=op/100;
oj.style.opacity=op/100;
}




String.prototype.multiReplace = function (s) {  
  var str=this,k;  
  for (k in s) str=str.replace(new RegExp(k,'g'), s[k] ); 
return str;  
}

function sortNumber(a,b) {return a - b; }


function cancelEvent(e){ e=e||window.event;
e.returnValue=false;
if(e.preventDefault) e.preventDefault();
e.cancelBubble=true;
if(e.stopPropagation) e.stopPropagation();
}


function addEvent(o,t,f){     
var d='addEventListener';
if(o[d]) o[d](t,f,false);  
else o['attachEvent']('on'+t,f);
}

function subEvent(o,t,f){ 
var d='removeEventListener';
if(o[d]) return o[d](t,f,0);
return o['detachEvent']('on'+t,f);
} 




function fdate(e,t){
var n=this,r,i,
s=["Sun","Mon","Tues","Wednes","Thurs","Fri","Satur","January","February","March","April","May","June","July","August","September","October","November","December"],
o=/\\?(.?)/gi,u=function(e,t){return i[e]?i[e]():t},a=function(e,t){e=String(e);while(e.length<t){e="0"+e}return e};i={d:function(){return a(i.j(),2)},D:function(){return i.l().slice(0,3)},j:function(){return r.getDate()},l:function(){return s[i.w()]+"day"},N:function(){return i.w()||7},S:function(){var e=i.j(),t=e%10;if(t<=3&&parseInt(e%100/10,10)==1){t=0}return["st","nd","rd"][t-1]||"th"},w:function(){return r.getDay()},z:function(){var e=new Date(i.Y(),i.n()-1,i.j()),t=new Date(i.Y(),0,1);return Math.round((e-t)/864e5)},W:function(){var e=new Date(i.Y(),i.n()-1,i.j()-i.N()+3),t=new Date(e.getFullYear(),0,4);return a(1+Math.round((e-t)/864e5/7),2)},F:function(){return s[6+i.n()]},m:function(){return a(i.n(),2)},M:function(){return i.F().slice(0,3)},n:function(){return r.getMonth()+1},t:function(){return(new Date(i.Y(),i.n(),0)).getDate()},L:function(){var e=i.Y();return e%4===0&e%100!==0|e%400===0},o:function(){var e=i.n(),t=i.W(),n=i.Y();return n+(e===12&&t<9?1:e===1&&t>9?-1:0)},Y:function(){return r.getFullYear()},y:function(){return i.Y().toString().slice(-2)},a:function(){return r.getHours()>11?"pm":"am"},A:function(){return i.a().toUpperCase()},B:function(){var e=r.getUTCHours()*3600,t=r.getUTCMinutes()*60,n=r.getUTCSeconds();return a(Math.floor((e+t+n+3600)/86.4)%1e3,3)},g:function(){return i.G()%12||12},G:function(){return r.getHours()},h:function(){return a(i.g(),2)},H:function(){return a(i.G(),2)},i:function(){return a(r.getMinutes(),2)},s:function(){return a(r.getSeconds(),2)},u:function(){return a(r.getMilliseconds()*1e3,6)},e:function(){throw"Not supported (see source code of date() for timezone on how to add support)"},I:function(){var e=new Date(i.Y(),0),t=Date.UTC(i.Y(),0),n=new Date(i.Y(),6),r=Date.UTC(i.Y(),6);return e-t!==n-r?1:0},O:function(){var e=r.getTimezoneOffset(),t=Math.abs(e);return(e>0?"-":"+")+a(Math.floor(t/60)*100+t%60,4)},P:function(){var e=i.O();return e.substr(0,3)+":"+e.substr(3,2)},T:function(){return"UTC"},Z:function(){return-r.getTimezoneOffset()*60},c:function(){return"Y-m-d\\TH:i:sP".replace(o,u)},r:function(){return"D, d M Y H:i:s O".replace(o,u)},U:function(){return r/1e3|0}};this.date=function(e,t){n=this;r=t===undefined?new Date:t instanceof Date?new Date(t):new Date(t*1e3);return e.replace(o,u)};return this.date(e,t)
}

function toCapitalize(s){
return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
}
