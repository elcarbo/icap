/* tool_db

DTB
"db_host"=>"192.168.0.12", "db_name"=>"mainsim3_development", "db_user"=>"root", "db_passw"=>"1881", "type"=>"enterprise", "disabled"=>0

ACT
"table"=>"t_creation_date", "act_name"=>"act_settings", "act_description"=>"aggiornamento settings", "option"=>"", "nomaster"=>0, "disabled"=>0

Table: t_update
f_id	f_master	f_slave	[f_table	f_action]	f_status
*/



ToolDB={
  DTB:[], ACT:[], I:-1, ERR:0,
  cycle_prcess:0, Nupd:0, Iupd:0, Fupd:0, 
  TOT:0, POS:0, COUNT:50, listREPL:[], IDU:0, CO:0, SELJSN:[],
  msgprogress:"Update in progress!&nbsp;&nbsp;<img src='../public/msim_images/default/popup/loader.gif' style='position:relative;top:2px;'width='16px' />",
  
  php_read_save_ini:"read_save_ini.php",
  php_update:"update.php",
  php_replace:"replace.php",
  php_add_db_list:"add_db_list.php",

Start:function(){
with(ToolDB){ 

with(Dbl){
 create_db("db_mainsim_update_dbs");
 connect_db("db_mainsim_update_dbs");
}

  // read  select_ini.jsn
  Ajax.sendAjaxPost("db-updater/read-ini?par=select_ini.jsn", "par=0", "ToolDB.f_RetReadSelectIni",0);


}},


f_RetReadSelectIni:function(a){
with(ToolDB){ 
  try{ SELJSN=JSON.parse(a);  }catch(e){ alert(a); return; }  // ["ini.jsn","ini_copy.jsn"]
  
  var i,r, sj=js$("id_selini"), n=sj.options.length;

  for(i=n;i>=0;i--) sj.options[i]=null; 
  for(r=0;r<SELJSN.length;r++)  {
    b=(SELJSN[r].split(".jsn"));
    sj.options[r]=new Option(b[0],r);
  }
  sj.options[0].selected=true;
  
  f_ReadIni();
}},



f_ReadIni:function(f){
with(ToolDB){ 

  if(!f) f="ini.jsn";
  Ajax.sendAjaxPost("db-updater/read-ini?par="+f, "par=0", "ToolDB.f_RetReadIni",0); 
   
}},


f_RetReadIni:function(a){
with(ToolDB){
  try{ var jsn=JSON.parse(a);  }catch(e){ alert(a); return; }
 
  DTB=jsn.databases;
  ACT=jsn.actions;
  
  if(!DTB) DTB=[];
  if(!ACT) ACT=[];
  
  var t=Dbl.query("CREATE_TABLE t_update (f_id inc, f_master int, f_slave int, f_act int, f_status int)");
        Dbl.query("CREATE_TABLE t_errors (f_id inc, f_code_master int, f_code_slave int, f_description string, id_update int, f_replaced int)");

 
  /* already exist
  if(t===false){
     if(!f_test_end_update()) {   
        if(confirm("Do you want to resume the update?")) { f_Resume(); f_Draw_update(); return; }    
  }}
  */
  
  f_btn_class("update",1);
  f_btn_class("resume",0);
  f_btn_class("stop",0);
  
  f_reset_update();
  f_Draw_update();
  
  if(CO) {
    f_Draw_db();
    f_Draw_act();
  }
  CO=0;
  
}},





f_Reset:function(){
with(ToolDB){
  cycle_prcess=0;
  f_reset_update();
  f_Draw_update();

  f_btn_class("update",1);
  f_btn_class("resume",0);
  f_btn_class("stop",0);
  
  f_set_message("Update reset!",1);
}},




f_Update:function(){
with(ToolDB){

  if(js$("id_btn_update").className=="cl_btn_disable") return false;
  
  // reset table
  f_reset_update();
  
  // disable update enable stop disable resume
  f_btn_class("update",0);
  f_btn_class("resume",0);
  f_btn_class("stop",1);
  
  
  // insert actions for each db slave with status 0
  var r,n,jn=[],mst;
  
  for(d=1;d<DTB.length;d++){
    if(parseInt(DTB[d].disabled)) continue;
    
    for(n=0;n<ACT.length;n++){
      if(parseInt(ACT[n].disabled)) continue;  
      mst=parseInt(ACT[n].nomaster)?-1:0;      // -1 no master db
      jn.push( {'f_master':mst, 'f_slave':d, 'f_act':n, 'f_status':0} );       
    } 
  }
  
  Nupd=jn.length; Iupd=0;
  if(Nupd) Dbl.query("INSERT t_update",jn);
  f_Draw_update();

  if(Nupd) { 
    Iupd=0; 
    Fupd=Iupd+1; 
    POS=0;
  
    cycle_prcess=1; 
    f_set_message(msgprogress,0); 
    f_cycle_prcess(); 
  }
}},



f_cycle_prcess:function(){
with(ToolDB){
  if(!cycle_prcess) return;

  // mark actual and previous rows
  f_mark();
 
  
  // end of process
  if(Iupd>=Nupd) {
    cycle_prcess=0;
    f_set_message("Update completed!",0);
    
    f_btn_class("update",1);
    f_btn_class("resume",0);
    f_btn_class("stop",0);
    
    return;
  }
  
  
  var a,rw,dm,ds,da, dbm; 
  a=Dbl.query("SELECT t_update GET * WHERE f_id="+Fupd);
  if(a.length!=1) { alert("error job not found!"); return; } 
  rw=a[0];
  
  dm=parseInt(rw.f_master);
  ds=parseInt(rw.f_slave); 
  da=parseInt(rw.f_act);   

  if(dm<0) dbm={}; else  dbm=DTB[dm];
 
  var par={   
    "db_master":dbm,
    "db_slave": DTB[ds],
    "action": ACT[da],
    "limit": [POS, COUNT, TOT]
  }

  Ajax.sendAjaxPost("db-updater/begin-works","params="+JSON.stringify(par), "ToolDB.f_RetUpdt",0); 
  
}},



f_RetUpdt:function(a){
with(ToolDB){

  try{ var r,ae,v,jn=JSON.parse(a);  }catch(e){ alert("error "+a);  }
 
  ERR=jn.flag_error;
  if(!ERR) ERR=0; else ERR=parseInt(ERR);
  if(ERR) js$("id_err_"+(Fupd-1)).value|=2;

  if(jn.status=="next"){    // next  
    f_next();
    return;
  }
  
  TOT=parseInt(jn.params.total); 
  
  // alert(jn.status)
  
  if(jn.status=="start"){
    if(!TOT) { f_next(); return; }
   
  }
  
  if(jn.status=="progress"){
  
    POS+=COUNT;
    
    // if errors
    if(jn.error.length){
      ae=[];
      for(r=0;r<jn.error.length;r++){
        ae.push({ "f_code_master":jn.error[r][0], "f_code_slave":jn.error[r][1], "f_description":jn.error[r][2], "id_update":Fupd, "f_replaced":0 }); 
      }
      Dbl.query("INSERT t_errors",ae); 
      js$("id_err_"+(Fupd-1)).value|=1;
    }
    
    v=parseInt(POS/TOT*100);
    if(v>=100) { f_next(); return; }   // end progress
   
    f_update_status(v); 
  }
  
  f_cycle_prcess();
}},



f_next:function(v){
with(ToolDB){

  // update 100%
    f_update_status(100);  
      Iupd++;  
      Fupd=Iupd+1;                    
      POS=0;
 
    f_cycle_prcess();
}},



f_update_status:function(v){
with(ToolDB){
  
  Dbl.query("UPDATE t_update SET f_status="+v+" WHERE f_id="+Fupd);
  var s, jt=js$("id_status_"+Fupd);
  
  s="<div style='position:relative;left:0;top:0;width:100%;height:22px;'>"+
    "<div style='position:relative;left:0;top:0;width:"+v+"%;height:100%;background-color:#A1C24D;opacity:0.5;'></div>"+
    "<div style='position:absolute;left:0;top:3px;width:100%;text-align:center;'>"+v+"%</div>"+
    "</div>";
  
  jt.innerHTML=s;
}},




f_mark:function(){
with(ToolDB){
  var r,c,n;  
  for(r=0;r<Nupd;r++){
    c="#BBB";
    if(r<Iupd) c="#404040";
    if(r==Iupd) c="#4A6671";    
    n=parseInt(js$("id_err_"+r).value);
    if(n==1) c="#2060E0";
    if(n==2) c="#B00";
    if(n==3) c="#883CD0";
    js$("id_tr_"+r).style.color=c;
  }
}},




f_Draw_update:function(){
with(ToolDB){

  var rw,r,s,c,dm,ds,ac,fid;

  rw=Dbl.query("SELECT t_update GET * WHERE 1");
  
  s="<table cellpadding='2px' cellspacing='2px' class='cl_resTab'>"+
     "<tr><th>N.</th><th>Master</th><th>Slave</th><th>table</th><th>Action</th><th>Status</th></tr>";
  
  for(r=0;r<rw.length;r++){
    c=(r&1)?"class='cl_disp'":"class='cl_par'";  
    dm=parseInt(rw[r].f_master);
    ds=parseInt(rw[r].f_slave);
    da=parseInt(rw[r].f_act);   
    fid=parseInt(rw[r].f_id);
          
    col="#BBB";      
     
    s+="<tr id='id_tr_"+r+"' "+c+" style='color:"+col+";' onclick='ToolDB.f_viewErr("+fid+")'>"+
        "<td style='text-align:center;'>"+(r+1)+"<input id='id_err_"+r+"' type='hidden' value=0 /></td>"+
        "<td>"+((dm<0)?"-":DTB[0].db_name)+"</td>"+
        "<td>"+DTB[ds].db_name+"</td>"+
        "<td>"+ACT[da].table+"</td>"+
        "<td>"+ACT[da].act_name+"</td>"+
        "<td id='id_status_"+fid+"' style='text-align:center;font-weight:bold;'>"+rw[r].f_status+"%</td>"+
        "</tr>"; 
  }
  
  s+="</table>";
  
  js$("id_main_tab").innerHTML=s;

}},



f_viewErr:function(idu){
with(ToolDB){

  if(!idu) {
    if(!IDU) { alert("id update error"); return false; }
    idu=IDU;
  }
  
  IDU=idu;
  
  var s="", rw,r,c,fcm,fcs,v; 
 
  v=parseInt(js$("id_err_"+(idu-1)).value);  
  if(!v || cycle_prcess) return;


  // build table
  rw=Dbl.query("SELECT t_errors GET * WHERE id_update="+idu);
  
  s="<table cellpadding='2px' cellspacing='2px' class='cl_resTab'>"+
     "<tr><th>N.</th><th>f_code master</th><th>f_code slave</th><th>Description</th><th style='text-align:center;text-decoration:underline;cursor:pointer;'"+
     " onclick='ToolDB.f_replaceErrAll("+idu+")'>replace all</th></tr>";
  
  for(r=0;r<rw.length;r++){
    c=(r&1)?"class='cl_disp'":"class='cl_par'";  
       
    fcm=rw[r].f_code_master;
    fcs=rw[r].f_code_slave; 
    rpl=rw[r].f_replaced; 
     
    s+="<tr "+c+" style='color:"+(rpl?"#444":"#2060E0")+";'>"+
        "<td style='text-align:center;cursor:default;'>"+(r+1)+"</td>"+
        "<td style='cursor:default;'>"+fcm+"</td>"+
        "<td style='cursor:default;'>"+fcs+"</td>"+
        "<td style='cursor:default;'>"+rw[r].f_description+"</td>"+
        "<td style='text-align:center;text-decoration:underline;cursor:pointer;' "+(rpl?"":"onclick='ToolDB.f_replaceErr("+fcm+","+fcs+","+idu+","+rw[r].f_id+")'")+" >replace"+(rpl?"d":"")+"</td>"+
        "</tr>"; 
  }
  
  s+="</table>";
  
  
  js$("id_err_cont").innerHTML=s;
  js$("id_err_page").style.display="block";

}},


f_closeErr:function(fid){
with(ToolDB){
  js$("id_err_cont").innerHTML="";
  js$("id_err_page").style.display="none";
}},



f_getDBs:function(idu){
with(ToolDB){
  var a,rw,dm,ds,da, dbm; 
  
  a=Dbl.query("SELECT t_update GET f_master,f_slave,f_act WHERE f_id="+idu);
  if(a.length!=1) { alert("error job not found2!"); return; } 
  rw=a[0];
  
  dm=parseInt(rw.f_master);
  ds=parseInt(rw.f_slave); 
  da=parseInt(rw.f_act);   

  if(dm<0) dbm={}; else  dbm=DTB[dm];
 
  return {   
    "db_master":dbm,
    "db_slave": DTB[ds],
    "action": ACT[da]
  }

}},


f_replaceErrAll:function(idu){
with(ToolDB){

  if(!confirm("Confirm all replace?") ) return false;
  
  var r,a,fcm=[],fcs=[],par;
  
  a=Dbl.query("SELECT t_errors GET * WHERE id_update="+idu+" AND f_replaced=0");
  
  listREPL=[];
  for(r=0;r<a.length;r++){
      fcm.push(a[r].f_code_master);
      fcs.push(a[r].f_code_slave);
      listREPL.push(a[r].f_id);
  }
  
  par=f_getDBs(idu);
  par["f_code_master"]=fcm;
  par["f_code_slave"]=fcs;  
  
  
  
  f_waiting(1);
  Ajax.sendAjaxPost(php_replace,"params="+JSON.stringify(par), "ToolDB.f_RetReplErr",0);
}},


f_replaceErr:function(fcm,fcs,idu,fid){
with(ToolDB){

  var par=f_getDBs(idu);
      par["f_code_master"]=[fcm];
      par["f_code_slave"]=[fcs];  
   
  listREPL=[fid];
   
/*
db_master
db_slave
action
f_code_master
f_code_slave
*/

  f_waiting(1);
  Ajax.sendAjaxPost(php_replace,"params="+JSON.stringify(par), "ToolDB.f_RetReplErr",0); 
}},


f_RetReplErr:function(a){
with(ToolDB){
  f_waiting(0);
  if(a!="ok") { alert(a); return false; }

  if(!listREPL.length) { alert("no update f_id to replace!"); return; }
  Dbl.query("UPDATE t_errors SET f_replaced=1 WHERE f_id IN ("+listREPL.join(",")+")");

  listREPL=[];
  f_viewErr();
}},






f_reset_update:function(){
with(ToolDB){
  POS=0;
 
  Dbl.query("TRUNCATE t_update");
  Dbl.query("TRUNCATE t_errors");
}},


f_test_end_update:function(){  // return false if no end
with(ToolDB){
  var u=Dbl.query("SELECT t_update count(*) WHERE f_status<100");
    
  if(u) {
    f_btn_class("resume",1); 
    return false;
  }
  
  f_btn_class("resume",0); 
  return true;
}},





f_Config:function(){
with(ToolDB){

  var jcnf=js$("id_config_page");
  jcnf.style.display="block";
  
  f_Draw_db();
  f_Draw_act();
}},




// select ini jsn --------------------------------------------------------------------

 



f_addIni:function(m){
with(ToolDB){

  var s=prompt("Enter name (without extension): ","");
  if(!s) return;

  var b,c="{}", ss=s.split(" ");
  s=ss.join("_");
  s+=".jsn";
  
  if( SELJSN.indexOf(s)!= -1 ) { alert("name exist!");  return; }
  
  SELJSN.push(s);
  if(m) c=JSON.stringify( { databases:DTB, actions:ACT } );
  
  Ajax.sendAjaxPost("db-updater/save-ini?par="+s, "params="+c, "ToolDB.f_RetSaveIni",0);                 // create file ini
  
  b=JSON.stringify(SELJSN);
  Ajax.sendAjaxPost("db-updater/save-ini?par=select_ini.jsn", "params="+b, "ToolDB.f_RetSaveIni",0);     // update select 
  
  CO=1;
  f_RetReadSelectIni(b); 
}},

 
 



f_delIni:function(){
with(ToolDB){

  if(!confirm("Confirm deleting of selected setting?")) return;
  var v,s,b, ji=js$("id_selini"); 
  v=parseInt(ji.value);
  if(!v) { alert("You can not delete default setting!"); return;  }

  s=ji.options[ji.selectedIndex].text + ".jsn";  // filename
  SELJSN.splice(v,1);
  
  // delete file and update select
  Ajax.sendAjaxPost("db-updater/save-ini?par="+s, "params=#delete#", "ToolDB.f_RetSaveIni",0);                 // create file ini
  
  b=JSON.stringify(SELJSN);
  Ajax.sendAjaxPost("db-updater/read-ini?par=select_ini.jsn", "params="+b, "ToolDB.f_RetSaveIni",0);     // update select 

  CO=1;
  f_RetReadSelectIni(b); 
}},


f_iniChange:function(){
with(ToolDB){
  var v, ji=js$("id_selini");  
  v=ji.options[ji.selectedIndex].text + ".jsn";
  CO=1;
  f_ReadIni(v);
}},



// database --------------------------------------------------------------------

f_Draw_db:function(){
with(ToolDB){
   var r,s,rw,cl,j;  
   s="<table cellpadding='2px' cellspacing='2px' class='cl_resTab'>"+
     "<tr><th>N.</th><th>Database</th></tr>";
 
  for(r=0;r<DTB.length;r++){
    rw=(r&1)?"class='cl_disp'":"class='cl_par'";  
 
    cl=( parseInt(DTB[r].disabled) )?"#AAA":"#333"; 
    s+="<tr "+rw+" onclick='ToolDB.f_edit_db("+r+")'><td style='color:"+cl+";text-align:center;'>"+r+"</td><td style='color:"+cl+
    ";'>"+DTB[r].db_name+(r?"":"&nbsp;&nbsp;&nbsp;(Master)")+"</td></tr>"; 
  }
  s+="</table>";

  j=js$("id_body_database")
  j.innerHTML=s;
  f_Table(j);
}},


f_edit_db:function(i){
with(ToolDB){
  f_mask_pop(1);
  js$("id_popup_database").style.display="block";


  var v,r,l=["db_host","db_name","db_user","db_passw","type","disabled"];
  for(r=0;r<l.length;r++) {
    v="";
    if(r==4) v="enterprise";
    if(r==5) v=0;    
    js$("id_db"+r).value=(i>-1)?DTB[i][l[r]]:v;  
  }

  I=i;
}},

f_edit_db_close:function(i){
with(ToolDB){

  f_mask_pop(0);
  js$("id_popup_database").style.display="none";
}},





f_edit_db_save:function(){
with(ToolDB){

  var V={},v,r,l=["db_host","db_name","db_user","db_passw","type","disabled"];
  
  if(I>-1){
      for(r=0;r<l.length;r++) {
        v=js$("id_db"+r).value; 
        if(v=="" && (r==1 || r>3)) { alert("Compile all fields!"); return false; }
        DTB[I][l[r]]=v; 
      }
  } else {
       for(r=0;r<l.length;r++) {
        v=js$("id_db"+r).value; 
        if(v=="" && (r==1 || r>3)) { alert("Compile fields!"); return false;  }
        V[l[r]]=v; 
      }
    DTB.push(V);
  }
  f_save_ini();
  f_edit_db_close();
  f_Draw_db();
}},





f_ADD_dbs:function(i){
with(ToolDB){

  var dbtp="FREE";
  if(i==1) dbtp="PRO";
  if(i==2) dbtp="ENTERPRISE";

  if(!confirm("Confirm to add databases ("+dbtp+") in list?")) return;

  f_waiting(1);
  Ajax.sendAjaxPost("db-updater/get-db-list","dbtype="+dbtp, "ToolDB.f_RetAddDBS",0);  
}},


f_RetAddDBS:function(a){
with(ToolDB){

   try{ var dblist=JSON.parse(a);  }catch(e){ alert(a); return; }
   
   
   var k=0, r,dbN=[], d={}, nm, n=dblist.length;
   if(!n) { f_waiting(0); return; }
    
   for(r=0;r<DTB.length;r++) dbN.push(DTB[r].db_name);
   
   for(r=0;r<n;r++){
    nm=dblist[r].db_name;
    if(dbN.indexOf(nm)!=-1) continue;
    k++;
    d=dblist[r];
    d["db_host"]="";
    d["db_user"]="";
    d["db_passw"]="";
      
    DTB.push(d);
   } 
    
  if(k) {
    f_save_ini();
    f_Draw_db();
  }else{
  
    f_waiting(0);  
  
  }
}},




f_edit_db_delete:function(){
with(ToolDB){

  if(!confirm("Delete database from list?")) return;
  
  DTB.splice(I,1);
  f_save_ini();
  f_edit_db_close();
  f_Draw_db();
}},


// mask
f_mask_pop:function(m){
  js$("id_popup_mask").style.display=(m?"block":"none");
},


// actions ----------------------------------------------------------------------

f_Draw_act:function(){
with(ToolDB){
   var r,s,rw,cl,j;  
   s="<table cellpadding='2px' cellspacing='2px' class='cl_resTab'>"+
     "<tr><th>N.</th><th>Table</th><th>Action</th></tr>";
 
  for(r=0;r<ACT.length;r++){
    rw=(r&1)?"class='cl_disp'":"class='cl_par'";  
 
    cl=(parseInt(ACT[r].disabled))?"#AAA":"#333"; 
    s+="<tr "+rw+" onclick='ToolDB.f_edit_act("+r+")'><td style='color:"+cl+";text-align:center;'>"+r+"</td><td style='color:"+cl+
    ";' style='color:"+cl+"'>"+ACT[r].table+"</td><td style='color:"+cl+"'>"+ACT[r].act_description+"</td></tr>"; 
  }
  s+="</table>";

  j=js$("id_body_actions")
  j.innerHTML=s;
  f_Table(j);
}},


f_edit_act:function(i){
with(ToolDB){

  f_mask_pop(1);
  js$("id_popup_actions").style.display="block";  

  var v,r,l=["table","act_name","act_description","option","nomaster","disabled"];
  for(r=0;r<l.length;r++) {
    v="";
    if(r>3) v=0;
    js$("id_act"+r).value=(i>-1)?ACT[i][l[r]]:v; 
  } 
  I=i;
}},



f_edit_act_close:function(i){
with(ToolDB){
  f_mask_pop(0);
  js$("id_popup_actions").style.display="none";
}},



f_edit_act_save:function(i){
with(ToolDB){
  var V={},v,r,l=["table","act_name","act_description","option","nomaster","disabled"];
  
  if(I>-1){
      for(r=0;r<l.length;r++) {
        v=js$("id_act"+r).value;
        if(v=="" && r!=3) { alert("Compile all fields!"); return false;  }
        ACT[I][l[r]]=v 
      }
  } else {
       for(r=0;r<l.length;r++) {
        v=js$("id_act"+r).value; 
        if(v=="" && r!=3) { alert("Compile all fields!"); return false;  }
        V[l[r]]=v; 
      }
    ACT.push(V);
  }

  f_save_ini();
  f_edit_act_close();
  f_Draw_act();
}},



f_edit_act_delete:function(i){
with(ToolDB){

  if(!confirm("Delete action from list?")) return;
  
  ACT.splice(I,1);
  f_save_ini();
  f_edit_act_close();
  f_Draw_act();
}},




//-------------------

f_save_ini:function(){
with(ToolDB){
  var par={ databases:DTB, actions:ACT };
  
  var v, ji=js$("id_selini");
  v=ji.options[ji.selectedIndex].text + ".jsn";
  
  Ajax.sendAjaxPost("db-updater/save-ini?par="+v, "params="+JSON.stringify(par), "ToolDB.f_RetSaveIni",0); 
}},

f_RetSaveIni:function(t){
with(ToolDB){
  if(t!="ok"){ alert(t);  }
  f_waiting(0);
}},

// ----------------------------------------------------------------------



f_Config_close:function(){
with(ToolDB){

  var jcnf=js$("id_config_page");
  jcnf.style.display="none";
  
  

}},







f_Stop:function(){
with(ToolDB){
  if(js$("id_btn_stop").className=="cl_btn_disable") return;
  f_btn_class("stop",0);
  f_btn_class("update",1);
  f_btn_class("resume",1);
  f_set_message("Update stopped!",0);
  cycle_prcess=0;
}},


f_Resume:function(){
with(ToolDB){
  if(js$("id_btn_resume").className=="cl_btn_disable") return;
  
  var a,c=f_test_end_update();
 
  
 
  if(c) {  // no resume
    f_btn_class("stop",0);
    f_btn_class("update",1);
    f_btn_class("resume",0);
    
    f_set_message("Update con not be resumed!",2);  
    return;  
  } 
  
   // resume
  f_btn_class("stop",1);
  f_btn_class("update",0);
  f_btn_class("resume",0);
 
  f_set_message("Update resumed!",1,msgprogress);
  
  Nupd=Dbl.query("SELECT t_update count(*) WHERE 1");
  a=Dbl.query("SELECT t_update f_id WHERE f_status<100 order by f_id limit 0,1","NUM");
  if(!a.length) { alert("resume error!"); return false; }
  
    Fupd=a[0][0]; 
    Iupd=Fupd-1;
    POS=0;  
 
  setTimeout( function(){ cycle_prcess=1; ToolDB.f_cycle_prcess(); },800 ) 
}},




f_btn_class:function(cl,st){
with(ToolDB){ 
  js$("id_btn_"+cl).className=(st?"cl_btn":"cl_btn_disable");
}},



f_Table:function(j){
  if(!j) j=document;
  var row = j.getElementsByTagName('tr');
  for(i=1; i<row.length; i++) {
	row[i].onmouseover = function() { 
		this.className += ' secondcolor';  	}
	row[i].onmouseout = function() {
		this.className = this.className.replace('secondcolor', ''); }
  } //

},


f_set_message:function(msg,t,msg2){
with(ToolDB){ 
  js$("id_message").innerHTML=msg;
  if(t){ 
    if(!msg2) msg2="";
    setTimeout(function(){ ToolDB.f_set_message(msg2,0); },t*1000);
  }
}},


f_waiting:function(m){
with(ToolDB){ 
  if(m) f_actwaiting(1);
  else {
    setTimeout(function(){ ToolDB.f_actwaiting(0); },500);
  }
}},

f_actwaiting:function(m){
with(ToolDB){ 
  js$("id_wait_mask").style.display=m?"block":"none";
  js$("id_loader").style.display=m?"block":"none";
}},


} // end