/* 
  Selector

properties: 
methods:
-Resize() 
-GetSelected(f_code)


            0       1          2             3             4                  5
Ypos = [ f_code, f_title, f_unique, openclose:-1|0|1, is last sibling 0|1, bookable ]      // openclose  0=leaf   >0 closed  <0 opened
Rfp: unique => position in Ypos
*/


AssetSel={ 
        
        FontH:11, FontF:"opensansR", mod: '',
        J:null, JO:null, JE:null, JC:null, cj:null, TGJ:null, Wc:0, Hc:0, marg:6,
        EVTDOWN:'onmousedown', EVTUP:'onmouseup', EVTMOVE:'onmousemove', dwn:0, PE:{}, Ix:0,Iy:0, IMV:0, noevt:0,
        Yi:0, Yt:0, vptr:0, Ivptr:0, VW:12, UNQ:"", Rfp:{}, 
        tBox:null, oBox:null, nBox:null, Cj:null, cCj:null,
        Ypos:[], Sel:[], SelU:[], aBD:[], VscrAssetSel:null, DB:[],
        colrs:["#FFFFFF",
               "#4A6671","#4A6671",   // green
               "#E0E0E0",             // vertical
               "#888888","#000000"],  // black


Draw:function(){
with(AssetSel){
  var r,n,nn,y,ry,c,py,
          
  nar=Ypos.length;
  if(!nar) return;    // no rows
  Yt=vptr%Yi, n=Hc+Yt;
  y=vptr-Yt;
  ry=y/Yi;
 
   
  cj.clearRect(0,0,Wc,Hc);
  py=-Yt-Yi;
  aBD=[]; 
    for(r=ry;r<nar;r++){
      Drow(r,py,0); 
      py+=Yi;
      if(py>Hc) break;
    }
}},



Drow:function(i,y,m){
with(AssetSel){
  var r,oc,xx,yy,uq,nq,f,yf,cc,aq,le,jj,pb,upb;
    uq=Ypos[i][2]; aq=uq.split("|"); nq=aq.length-1;  // number of nesting
    xx=nq*18; yy=y+(Yi-16)/2;
    oc=Ypos[i][3];  
    
  jj=m?cCj:cj;    
    cc=(Ypos[i][5])?5:4;  //grey

    
    // folders
      f=oc?((oc>0)?58364:58369):58355;
      moCnvUtils.cnvIconFont(jj,f,xx+18,yy,16,colrs[cc-3]);
       
  // if leaf
  le=(i<Ypos.length-1 && uq.length==Ypos[i+1][2].length)?1:2;  
  
with(jj){  
    // vertical line
    fillStyle=colrs[3];
    
    // parent is last element?
    upb=aq[0]; 
    for(r=0;r<nq-1;r++) {
       upb+="|"+aq[r+1]; 
       pb=Rfp[upb];
       if(!Ypos[pb][4]) fillRect((r+1)*18+7,y,1,Yi);
    }
    
    fillRect(nq*18+7,y,1,Yi/le);
    fillRect(nq*18+9,y+Yi/2,8,1);
    
    if(oc) {
      fillStyle=colrs[0];
      fillRect(xx,yy+2,14,14);
      
      // plus/minus
        f=(oc>0)?58824:58825;   // 58804:58805;
        moCnvUtils.cnvIconFont(jj,f,xx,yy+1,14,colrs[4]);
      
    }    
    // title
    fillStyle=colrs[cc];
    font=FontH+"px "+FontF; 
    textBaseline="alphabetic"; 
    textAlign="left"; 
    yf=y+FontH+(Yi-FontH)/2;
    fillText(Ypos[i][1], xx+40, yf); 
  } 
  aBD.push({y:y+Yi, x:xx, idy:i });
}},


 
sel_down:function(e){  
with(AssetSel){ moEvtMng.cancelEvent(e); 
  
  moMenu.Close();
  
  if(noevt) return false;
  if(dwn) return false; 
  var P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e); 
  Ix=P.x,Iy=P.y,Ivptr=vptr;
  PE=moGlb.getPosition(JE);
  IMV=0;
  if(!isTouch){
    JO.style.display="none";
    TGJ.style.display="block";
    eval("TGJ."+EVTMOVE+"=function(e){AssetSel.sel_move(e); }");
    eval("TGJ."+EVTUP+"=function(e){AssetSel.sel_up(e); }");
  }
  dwn=1;
}},

sel_move:function(e){ 
with(AssetSel){ moEvtMng.cancelEvent(e); if(noevt) return false;
 
  var i,y,v,h, P=(isTouch)?moEvtMng.getTouchXY(e):moEvtMng.getMouseXY(e,JE);
 
  if(!dwn){
    i=OvrClick(P.y);
    if(i<0) JO.style.display="none";
    else with(JO.style) top=(marg+aBD[i].y-Yi)+"px", display="block";
  } else {  
    IMV++;    
    if(IMV&1) return false;  
    if(isTouch) P.y-=PE.t;
      h=Ypos.length*Yi;
      if(h<=Hc) v=0; else  {
        v=Ivptr+Iy-P.y-PE.t;  
        if(v+Hc>h) v=h-Hc;
      }
      if(v<0) v=0;
      vptr=v; 
      VscrAssetSel.Resize( { ptr:vptr } ); 
      Draw();   
  } 
}},


sel_up:function(e){
with(AssetSel){ moEvtMng.cancelEvent(e); if(noevt) return false;  
  var r,i,f,x,y,ox; 
  // click
  if(IMV<4){
    r=OvrClick(Iy-PE.t);
    if(r>-1){
      i=aBD[r].idy; f=Ypos[i][0];
      x=aBD[r].x; ox=Ix-PE.l;
      if(Ypos[i][3] && ox>x && ox<x+26) OpenClose(i);            
      else addSEL(f,i);
     }
  }
  dwn=0;
  TGJ.style.display="none";
}},
 

OvrClick:function(y){
with(AssetSel){
  var r,y1=0,y2,v=-1;
  for(r=0;r<aBD.length;r++){
    y2=aBD[r].y;
    if(y>=y1 && y<y2) { v=r; break; }
    y1=y2;
  }
  return v;
}},


addSEL:function(fc,i){
with(AssetSel){
  if(fc==Ypos[0][0]) {  return; } // select root with children => Sel[]
  var r,n,uq,uc,f,p;
 
    if(Sel.indexOf(fc)!=-1) { 
      Sel=[];
      SelU=[];
    } else {
      Sel=[fc];
      SelU.push(Ypos[i][2]);
    }
  
  Draw();
  outAction(fc,Ypos[i][5]);
}},


OpenClose:function(i){
with(AssetSel){ 
  var r,y=0,hh,n=0, f=Ypos[i][0], oc=Ypos[i][3];
  Ypos[i][3]*=-1; 
  if(oc<0) { // close   
    for(r=i+1;r<Ypos.length;r++){
      uq=Ypos[r][2];
      if(uq.indexOf(f+"")==-1) break;
      n++; 
    }
    for(r=0;r<aBD.length;r++){
      if(aBD[r].idy==i) { y=aBD[r].y; break; }
    }
    tBox=cj.getImageData(0,0,Wc,y);
    hh=getOtherRow(i+1,n);
    Ypos.splice(i+1,n);
    UpdateUnique();
    AnimOC(y,0,3,n*Yi,hh,-1);    
  }else{ // open
    Load(Ypos[i][0],Ypos[i][2]);    
  }
}},


Vbarpos:function(i){
with(AssetSel){
  vptr=i;
  Draw();  
  if(!isTouch) JO.style.display="none";
}},


Resize:function(){
with(AssetSel){
  Wc=J.offsetWidth - marg*2, Hc=J.offsetHeight - marg*2;
  JC.height=Hc;
  JC.width=Wc-VW; 
 
  with(JE.style) width=Wc+"px", height=Hc+"px";
 
  if(VscrAssetSel) UpdateVscr(1);
  vptr=0;
  Draw();
}},


UpdateVscr:function(m){
with(AssetSel){
  var h=Ypos.length*Yi;
  if(m)  vptr=0; else { if(h>Hc && vptr+Hc>h) vptr=h-Hc; }
  if(Hc>=h) VscrAssetSel.Display(0); else { if(!VscrAssetSel.displ) VscrAssetSel.Display(1); VscrAssetSel.Resize( { L:Wc-marg, T:marg, H:Hc, HH:h, ptr:vptr } ); }
}},

 

GetSelected:function(){
with(AssetSel){
  return { Selected:Sel  };
}},


 
 
Load:function(p,unq){  // p = parent f_code   unq = univoce code
with(AssetSel){
   noevt=1; UNQ=unq;    
   
    var a,D,r,i;
   
// query children
  a={ data:[], parent:p };
  
        
  
  for(r=0;r<DB.length;r++){   // DB:  0 fcode, 1 title, 2 parent, 3 bookable, 4 nchild
    if(DB[r][2]==p) {
      a.data.push( [DB[r][0], DB[r][1], DB[r][4], DB[r][3] ] );  
    }
  }
    
  AssetSel.retLoad(a);    // a.data  [ [ 0=f_code 1=f_title  2=nchild  (0=leaf) ], 3=bookable  ]   
}},


retLoad:function(a){
with(AssetSel){

  if(a.data.length==0) return; 
  
  // alert(a.parent+"\ndata: "+JSON.stringify(a.data) )
  
  var r,fc,rw,pd,nr,uq,ps=0,p,y=0,hh,sib;  
  pd=a.parent;
  rw=a.data;                                  // rw[r]  [ 0=f_code 1=f_title  2=nchild  (0=leaf)  ]
  if(UNQ) { ps=Rfp[UNQ]+1; UNQ+="|"; }
  nr=rw.length;
 
  for(r=0;r<nr;r++){      
    fc=rw[r][0];  
    uq=UNQ+fc;
    sib=(r==nr-1)?1:0;
    Ypos.splice(ps+r,0,[ fc, rw[r][1], uq, rw[r][2], sib, rw[r][3] ]); // Ypos  0 fcode   1 title   2 unique    3 nchild    4 sib 
  }   
  UpdateUnique(); 
 
  
  // animate
  p=ps-1; 
  if(p>=0) {
    for(r=0;r<aBD.length;r++){ if(aBD[r].idy==p) { y=aBD[r].y+1; break; }}
    tBox=cj.getImageData(0,0,Wc,y-1 );
    oBox=cj.getImageData(0,y,Wc,(Hc-y) );
    hh=getNewRow(p+1,nr);
    AnimOC(y,0,3,nr*Yi,hh,1);  
  } else {
    UpdateVscr();
    Draw();
    noevt=0;
  }  
}},


UpdateUnique:function(){
with(AssetSel){
  var r,i,uq; 
  Rfp={};
  for(r=0;r<Ypos.length;r++){
    uq=Ypos[r][2];
    Rfp[uq]=r;
  }
}},


// open
getNewRow:function(p,nr){
with(AssetSel){ 
 if(!Cj) {
  Cj=moGlb.createElement("idcnvhiddAssetSel","canvas");
  Cj.style.display="none";
  
  cCj=Cj.getContext("2d");
 } 
  var r,h,i=0,k=Hc,hh;  
  Cj.width=Wc, Cj.height=Hc;
  cCj.clearRect(0,0,Wc,Hc);   
  h=nr*Yi;
  nc=parseInt(Hc/Yi)-1; 
  if(nr>nc) i=nr-nc;
  for(r=nr-1;r>=i;r--){
    k-=Yi;
    Drow(p+r,k,1);
  }
  hh=Hc-k; 
  try { nBox=cCj.getImageData(0,k,Wc,hh); }catch(e){ nBox=null; }
  return hh;
}},


// close
getOtherRow:function(p,nr){
with(AssetSel){   
  var r,h,i,n,k=0,nn; 
  Cj.width=Wc, Cj.height=Hc;
  cCj.clearRect(0,0,Wc,Hc);   
  h=nr*Yi;
  nc=parseInt(Hc/Yi)-1; 
  i=(nr>nc)?nc:nr;
  for(r=0;r<i;r++){  
    Drow(p+r,k,1);
    k+=Yi;
  }   
  try {nBox=cCj.getImageData(0,0,Wc,k); }catch(e){ nBox=null; }
  
  // oBox
  cCj.clearRect(0,0,Wc,Hc);
  n=Ypos.length;
  nn=p+nr+nc; if(nn>n) nn=n;
  h=0;
  for(r=p+nr;r<nn;r++){  
    Drow(r,h,1);
    h+=Yi;
  } 
  oBox=h?oBox=cCj.getImageData(0,0,Wc,h):null;
  return k;
}},



AnimOC:function(y,t,k,tf,df,d){
with(AssetSel){  
  cj.clearRect(0,y,Wc,(Hc-y));
  t+=k; k+=5; if(t>=tf || t>(Hc-Yi) ) t=tf; 
  var yy=y+d*t;
  if(nBox)cj.putImageData(nBox,0,yy-((d>0)?df:0));
  cj.putImageData(tBox,0,0);
  if(oBox) cj.putImageData(oBox,0,yy+((d<0)?df:0));
  if(t==tf){ UpdateVscr(); Draw(); noevt=0;  return; }
  
setTimeout("AssetSel.AnimOC("+y+","+t+","+k+","+tf+","+df+","+d+")",40);
}},
 
 

 


outAction:function(fc,i){
with(AssetSel){
    
 // console.log( "Selez: "+fc+" | "+i) 
 
 // bookable
 if(i){
  resForm.f_close_asset_tree(fc);
 }
  
}},
 
 
 
// Start -------------------------------------------- 
Start:function(container){ 
with(AssetSel) {

Ypos=[];
Sel=[];   // array of fcode
SelU=[];  // array of unique selected
aBD=[];

  Yi=isTouch?28:22;  // row height
  FontH=11;
  
  if(container) J=mf$(container); else return;
  Wc=J.offsetWidth - marg*2, Hc=J.offsetHeight - marg*2;
 
  var s="<canvas id='AssetSelCnv' style='position:absolute; width='"+(Wc-VW)+"' height='"+Hc+"'></canvas>"+
        
        "<div id='AssetSelOvr' style='position:absolute;left:0px;top:0;width:100%;height:"+Yi+"px;background-color:#A0A0A0;opacity:0.1;display:none;'></div>"+   // over
        "<div id='AssetSelEvt' style='position:absolute;left:0;top:"+marg+"px;width:100%;height:"+Hc+"px;background-color:#FFF;opacity:0.01;'></div>";           // events
      
        
  J.innerHTML=s;
  JC=mf$("AssetSelCnv");
  JO=mf$("AssetSelOvr");
  JE=mf$("AssetSelEvt");
 
  cj=JC.getContext("2d");
  
  if(isTouch) { EVTDOWN='ontouchstart', EVTUP='ontouchend', EVTMOVE='ontouchmove';  }

  
  if(VscrAssetSel) delete(AssetSel.VscrAssetSel); 
  AssetSel.VscrAssetSel=null;

  // scrollbar  
  VscrAssetSel=new OVscroll('AssetSel.VscrAssetSel',{ displ:0, L:0, T:0, H:Hc, HH:1, Fnc:'AssetSel.Vbarpos', pid:J.id });     // VW=VscrAssetSel.W;
 

  TGJ=moGlb.createElement("maskTreegridDragScroll","div",document.body);
  TGJ.setAttribute("style","position:absolute;left:0;top:0;width:100%;height:100%;background-color:#FF4;opacity:0.0;display:none;z-index:"+(moGlb.zloader+500));

 
  // events
  eval("JE."+EVTDOWN+"=function(e){AssetSel.sel_down(e); }");
  
  if(isTouch){ 
    eval("JE."+EVTMOVE+"=function(e){AssetSel.sel_move(e); }");
    eval("JE."+EVTUP+"=function(e){AssetSel.sel_up(e); }");  
  } else {    
    JE.onmouseover=function(e){ WHEELOBJ="AssetSel.VscrAssetSel";  }
    JE.onmouseout=function(e){ WHEELOBJ=""; JO.style.display="none"; }
    eval("JE."+EVTMOVE+"=function(e){AssetSel.sel_move(e); }");   
  }


  JC.width=Wc-VW;
  DB=[];
  DB.push([0,"Assets Bookable","",0,0]);   //  DB:  fcode, title, parent, bookable, nchild  // AST => V:  0:fcode, 1:title, 2:min , 3:max, 4:fparent, 5:bookable
  
  var r,V,i,n,nc,fc;
  
  V=resForm.AST; n=V.length;
  for(r=0;r<n;r++){    
    DB.push( [ parseInt(V[r][0]), V[r][1], parseInt(V[r][4]), (+V[r][5]), 0 ]  );   // DB:  0:fcode, 1:title, 2:parent, 3:bookable, 4:nchild
  }
  
  
  n=DB.length;
  for(r=0;r<n;r++){ 
    fc=DB[r][0];
    nc=0;
    for(i=0;i<n;i++){
      if(DB[i][2]==fc) nc++;
    }
    DB[r][4]=nc;
  }
  
  //alert(JSON.stringify(DB) )
  Resize();
  Load(0,""); 
 
}}

} // end selector