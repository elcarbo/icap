// calendarietto


miniCal={ jm:null, jb:null, jmb:null, I:0, LBL:"", FNC:"", wcell:28, mg:2, W:0, H:0,
          S:"style='position:absolute;",
          fnt:"13px OpensansR",
          colrs:["#4A6671","#A9BCC2","#E8E8E8","rgba(161,194,77,0.5)","#A1C24D"],
          sweek:["Mon","Tue","Wed","Thu","Fri","Sat","Sun"],
          amonth:["January","February","March","April","May","June","July","August","September","October","November","December"],
          smonth:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"],
          Day:{}, NOW:{}, SEL:{}, Dt1:{}, Dt2:{}, MTH:0,

Show:function(i,lbl,fnc,val){
with(miniCal){

  jm=mf$("id_cal_mask");
  jb=mf$("id_box_mcal");
  
  if(isTouch) wcell=48;
  
  W=wcell*7+mg*6+16;
  H=W+56;
  with(jb.style){
    width=W+"px";
    height=H+"px";
    margin="-"+(H/2)+"px 0 0 -"+(W/2)+"px";  
  }
  
  I=i; LBL=lbl; FNC=fnc;
    
  Day=f_now();
  NOW=f_now(); 
  if(val) SEL=f_now(val); else SEL={};
    
  Draw();
  
  try{ parent.document.getElementById("id_mask_popup").style.display="block"; }catch(e){}
  jm.style.display="block";
  jb.style.display="block";
}},


Draw:function(){
with(miniCal){

  var r,x,s,y=80;
  
  s="<div "+S+"left:8px;top:8px;right:8px;height:28px;background-color:"+colrs[0]+";border-radius:2px;'>"+
    "<div "+S+"left:8px;top:5px;font:bold "+fnt+";color:#FFF;'>"+LBL+"</div>"+
    "<div "+S+"right:8px;top:3px;font:16px FlaticonsStroke;color:#FFF;cursor:pointer;' onclick='miniCal.Close()'>"+String.fromCharCode(58827)+"</div>"+
    "</div>";
    
  // month year  
  s+="<div "+S+"left:14px;top:48px;font:16px FlaticonsStroke;color:"+colrs[0]+";cursor:pointer;' onclick='miniCal.f_nextprev(-1)'>"+String.fromCharCode(58790)+"</div>"+
     "<div "+S+"right:14px;top:48px;font:16px FlaticonsStroke;color:"+colrs[0]+";cursor:pointer;' onclick='miniCal.f_nextprev(1)'>"+String.fromCharCode(58791)+"</div>"+
     "<div "+S+"left:32px;right:32px;top:49px;font:bold 14px OpensansR;color:"+colrs[0]+";text-align:center;cursor:pointer;' onclick='miniCal.f_htitle()'>"+LANG(amonth[ Day.MM ])+" "+Day.YY+"</div>";
   

  // day week
  for(r=0;r<7;r++){
     x=8+(wcell+mg)*r;
     s+="<div "+S+"left:"+x+"px;top:"+y+"px;width:"+wcell+"px;text-align:center;font:"+fnt+";'>"+LANG(sweek[r])+"</div>";  
  }  
  
  y+=18;
  s+="<div id='id_bdmcal' "+S+"left:8px;top:"+y+"px;right:8px;height:"+((wcell+mg)*6)+"px;overflow:hidden;'></div>";
    
    
    // animate box month 
  s+="<div id='id_months_cont' style='position:absolute;left:8px;top:"+y+"px;right:8px;bottom:8px;overflow:hidden;display:none;'>"+
     "<div id='id_months' style='position:absolute;left:0px;top:0px;width:100%;height:100%;background-color:#FFF;overflow:hidden;'></div></div>";  

  jb.innerHTML=s;
  
  jmb=mf$("id_bdmcal");
  DrawMonth();
    
}},


DrawMonth:function(){
with(miniCal){
   var r,s="",x=0,y=0, onc, D,dn,nr,cc,bk;
   
 D=f_Dt1Dt2();
 Dt1=D.di, Dt2=D.df, nr=D.nr;  
 dn=moGlb.cloneArray(Dt1);  
   
   
 for(n=0;n<6;n++){  
    for(r=0;r<7;r++){ 
     
    cc=(dn.MM!=Day.MM)?"#AAA":"#000"; 
    if(dn.DD==NOW.DD && dn.MM==NOW.MM && dn.YY==NOW.YY) bk="#CCC"; else bk=colrs[2]; 
    if(dn.DD==SEL.DD && dn.MM==SEL.MM && dn.YY==SEL.YY) bk=colrs[4]; 
    
    
   
     x=(wcell+mg)*r;     
     onc=" onmouseover='this.style.backgroundColor=\""+colrs[3]+"\";' onmouseout='this.style.backgroundColor=\""+bk+"\";' onclick='miniCal.SelDay("+dn.DD+","+dn.MM+","+dn.YY+")' ";     
     s+="<div "+S+"left:"+x+"px;top:"+y+"px;width:"+wcell+"px;height:"+wcell+"px;;text-align:center;font:"+fnt+
        ";background-color:"+bk+";cursor:pointer;' "+onc+">"+
        "<div style='margin-top:"+(parseInt((wcell-18)/2))+"px;color:"+cc+";'>"+dn.DD+"</div>"+
        "</div>";  
   
    dn=f_inc(1,0,0,dn);  
    }
   y+=wcell+mg; 
   
  }
  jmb.innerHTML=s;

}},



f_nextprev:function(i){
with(miniCal){
  Day.DD=1;
  Day=f_inc(0,i,0);
  Draw();
}},









f_htitle:function(i){
with(miniCal){
 
   if(MTH) { f_htitle_close(); return false; }
   MTH=1;
   
   var j,jmm,ww,hh,r,lr,llr,cgx,cgy,rcgx,mh,x,y,rx,ty,i=0,
       s="", g=4;
   
   ww=W-16, hh=jmb.offsetHeight;
 
   cgx=parseInt((ww-g*2)/3); rcgx=(ww-g*2)%3; 
   cgy=parseInt((hh-g*4)/5);

   
   Hbt=(cgy+g)*5;
   jmm=mf$("id_months")
   jmm.style.top=(-Hbt)+"px";
   jmm.style.height=Hbt+"px";
   
   
   mh=(cgx<120)?smonth:amonth;
   ty=(cgy-16)/2;
   
   lr=parseInt(cgx-g)/2; // (cgx&1)
   x=rcgx, y=0;
   s+=f_mbt("f_today()",x,y,lr,cgy,ty-2,"<span style='font:16px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58486)+"</span>");     
   
   x+=g+lr;  llr=lr+((cgx&1)?1:0);
   s+=f_mbt("f_chg_year(-1)",x,y,llr,cgy,ty-2,"<span style='font:16px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58790)+"</span>");
   
   x+=g+llr;
   s+=f_mbt("",x,y,cgx,cgy,ty,"<span id='idyearmonth' style='font:bold 14px opensansR;color:#000;'>"+Day.YY+"</span>");     // year
   
   x+=g+cgx;
   s+=f_mbt("f_chg_year(1)",x,y,llr,cgy,ty-2,"<span style='font:16px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58791)+"</span>");
   
   x+=g+llr;
   s+=f_mbt("f_htitle_close()",x,y,lr,cgy,ty-2,"<span style='font:16px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58827)+"</span>",1);
   
   // btn
   for(r=1;r<5;r++){   
    y=r*(g+cgy);
    for(rx=0;rx<3;rx++){ 
      x= rcgx+rx*(g+cgx);  
      s+=f_mbt("f_set_monthY("+i+")",x,y,cgx,cgy,ty,mh[i],(rx==2)?1:0);
      i++;
    }
   }

   jmm.innerHTML=s;
   mf$("id_months_cont").style.display="block";
   
   f_anim(-Hbt,8);
}},


f_anim:function(hh,k){
  hh+=k;  
  if(hh>0) hh=0;  
  mf$("id_months").style.top=hh+"px";
  if(!hh) return;
  k+=k/4;
setTimeout(function(){ miniCal.f_anim(hh,k); },40);
},


f_mbt:function(fnc,l,t,w,h,dt,txt){
with(miniCal){
 if(fnc) fnc="onclick='miniCal."+fnc+";' class='cl_mbtn' "; else fnc="class='cl_nombtn'";   
 return "<div "+S+"left:"+l+"px;top:"+t+"px;width:"+w+"px;height:"+h+"px;' "+fnc+">"+
        "<div "+S+"left:0;top:"+dt+"px;width:100%;font:"+fnt+";text-align:center;color:#000;'>"+txt+"</div></div>";
}},


f_chg_year:function(i){
   var v=parseInt(mf$("idyearmonth").innerHTML)
   v+=i;
   mf$("idyearmonth").innerHTML=v; 
   return false;  
},

f_set_monthY:function(m){
with(miniCal){
    var y=parseInt(mf$("idyearmonth").innerHTML);  
    Day.YY=y, Day.MM=m; 
    Draw();
    f_htitle_close();
}},

f_today:function(m){
with(miniCal){
    Day.YY=NOW.YY, Day.MM=NOW.MM; 
    Draw();
    f_htitle_close();
}},


f_htitle_close:function(h,k){
with(miniCal){   
   if(!h) h=0,k=8;  
   h-=k;
   if(h<-Hbt){
    mf$("id_months_cont").style.display="none";
    MTH=0;
    return;
   }
   mf$("id_months").style.top=h+"px";
   k+=k/4;
setTimeout(function(){ miniCal.f_htitle_close(h,k); },40);
}},


 

SelDay:function(d,m,y){
with(miniCal){

  var tms=day_to_tms({ DD:d, MM:m, YY:y });
  if(FNC) eval(FNC+"("+I+","+tms+")");
  
  Close();
}},


Close:function(){
with(miniCal){

   try{ parent.document.getElementById("id_mask_popup").style.display="none"; }catch(e){}
   jm.style.display="none";
   jb.style.display="none";
}},



//--------------------------------------------------------------------------------
// functions

f_Dt1Dt2:function(){
with(miniCal){
  var nr=0,df,di,dn,w;
  df=new Date(Day.YY,Day.MM,1,0,0,0,0);     // first day of month
  w=df.getDay();  
  if(!w) w=6; else w--;                     
     
  di=f_inc(-w,0,0,f_now(df));               // first day/row
  df=f_inc(6,0,0,di);

  for(;;){ 
    nr++;
    if(df.MM!=Day.MM) { 
      dn=f_inc(-6,0,0,df); 
      if(dn.MM!=Day.MM) { nr--; df=f_inc(-7,0,0,df); }
      break;
    }
    df=f_inc(7,0,0,df);
  } 
  
  return { di:di, df:df, nr:nr };
}},


f_inc:function(d,m,y,dd){    // dd = { YY, MM, DD, hh, mm, ss, W  }
with(miniCal){
  if(!dd) dd=Day;
  var D=dd.DD+d,
      M=dd.MM+m,
      Y=dd.YY+y,
  dt=new Date(Y,M,D,dd.hh,dd.mm,dd.ss);   
  return f_now(dt);
}},



f_now:function(dt){ 
  var d;
  if(!dt) d=new Date();                              // now
  else {
    if(typeof(dt)=="number") d=new Date(dt*1000);  // from timestamp
    else d=dt;                                       // drom Date
  }      
  var h=d.getHours(),
  m=d.getMinutes(),
  s=d.getSeconds(), 
  D=d.getDate(),
  M=parseInt(d.getMonth()),
  Y=d.getFullYear(),
  W=d.getDay();           // day of week

  return { DD:D, MM:M, YY:Y, hh:h, mm:m, ss:s, W:W };
},

day_to_tms:function(d){
  var dta=new Date(d.YY,d.MM,d.DD,0,0,0);
  return parseInt((dta.getTime())/1000);
}


} // end
