/* Calendar

button 
left-right circle: 58814-58815
printer:           58414

props:
MW:     0=Month    1=Week
                                                                                                                           
                                                                                                                              
to Translate
"Month","Week","Options",
"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"
"Sun","Mon","Tue","Wed","Thu","Fri","Sat"
"January","February","March","April","May","June","July","August","September","October","November","December"


todo:
resize left top
collapse no fullpage

NORANGE     view range of WO

CNFG
  ASSIGN
  TYPERANGE
  PRIORITY    "" All   "1,2,3"   "1,2"   "3"
   menu.
    priority
    wotype
    assignment
    typerange
  ptr  [YYYYMMDD]=[ [fcode, (N. first)],... ]
  ref  [fcode]=row_data           (fcode, progress, type, title, priority, from_date, to_date, code_assignment, asset )
  

workhour:[],    mark work hours to left of week table


labels:

f_title,  fc_progress, f_creation_date
f_type_id, f_priority, f_code,

date: planned, effective

responsabile, richiedente

categoria (tipo workorder)
 
 
popup 
+-  click
-  assignment
- w size of popup

- loading sync update reverse ajax


Mode:   0 from module   1 from popup (different table name in localstorage)

*/






moCal={ Mode:0, MW:0, NORANGE:0, conn:null, JM:null, JP:null, MDRJ:null, CTRL:0, 
        W:0,H:0, JB:null, jhl:null, OscrCal:null, VscrCal:null, 
        VscrEdCal:null, jsb:null, jso:null, jsedc:null, sceH:0, sceh:0,
        Day:{}, Dt1:null, Dt2:null, NOW:{}, odrg:{}, PAR:{}, REVERSE:0, UPDT:0,
        LcolW:0, RcolW:0, colW:0, mg:1, COL:[], ROW:[],
        celm:40, celw:0, fit:1, wmg:10, Hbt:0, MTH:0, RESZ:0, BTNEDIT:0,
        jsc:null, jsch:null, jscl:null, scl:0, scW:0, scw:0, sct:0, scH:0, sch:0,
        S:"style='position:absolute;",
        bkhcell:"#EAEAEA", bkcell:"#F4F4F4",
        tabName:"t_datacal",
        workhour:[9,10,11,12,14,15,16,17],                              // 9,10,11,12,14,15,16,17

  CallStart: "calendar-options",       //"cal_start.php",   
  CallWo: "calendar-workorders",             //"cal_wo.php",       
  CallEditWo:"calendar-workorder",
  CallReverse:"calendar-reverse",

  Lbw:[],Btw:[], fnt:"13px opensansR", fnth:"18px opensansL",
  aweek:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
  sweek:["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sun"],
  amonth:["January","February","March","April","May","June","July","August","September","October","November","December"],
  smonth:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"],
  
  Labels:{ 
    f_title:"Short description",
    f_description:"Description",
    fc_progress:"WO ID",
    f_creation_date:"Creation date",
    f_type_id:"Category",
    f_priority:"Priority",
    
    wf_phase:"Workflow phase",
    asset:"Asset",
    
    f_owner:"Owner",
    f_requestor:"Requestor",
    
    f_start_date:"Planned starting date", 
    f_end_date:"Planned ending Date", 
    fc_wo_starting_date:"Starting date",
    fc_wo_ending_date:"Ending date"
  },

  
  CNFG:{},

  menufb:[],
  
  
 
 
Start:function(m){  
with(moCal){
  
  tabName=m?"t_datacal_win":"t_datacal";
  
  Lbw=[ LANG("Month"), LANG("Week"), LANG("Options") ];   
  Btw=[ moCnvUtils.measureDivText(Lbw[0],fnt)+10, moCnvUtils.measureDivText(Lbw[1],fnt)+10, moCnvUtils.measureDivText(Lbw[2],fnt)+10];
 
  JB=mf$("id_body");
  JM=mf$("id_cal_mask");
  JP=mf$("id_cal_popup");
  MDRJ=mf$("id_drg_mask");
  
  Day=f_now();
  NOW=f_now();
 
  // database
  Dbl.create_db("db_mainsim_calendar");
  conn=Dbl.connect_db("db_mainsim_calendar");
  if(!conn) { alert("no db"); return; }
  
  // TABLE tabName                      
  Dbl.query("CREATE_TABLE "+tabName+" (fcode int, progress int, type int, title string, priority int, from_date int, to_date int, code_assignment int, asset string)");
 
  DrawHead();
  Ajax.sendAjaxPost(CallStart,"mode="+Mode,"moCal.retCallStart");
}},


  
retCallStart:function(a){
with(moCal){  
  CNFG:{};
  try{ CNFG=JSON.parse(a); }catch(e){ alert("no params: "+a); return; } 
  CNFG.ASSIGN=0, CNFG.TYPERANGE=0; CNFG.PRIORITY=""; CNFG.WOTYPE="";
  
  var k,sep="";
  
  if(typeof(CNFG.labels)!="undefined") {
    for(k in CNFG.labels) Labels[k]=CNFG.labels[k];
  }

  for(k in CNFG.menu.priority) { CNFG.PRIORITY+=sep+k; sep=","; }
  CNFG.nPri=CNFG.PRIORITY.length;
  sep="";
  for(k in CNFG.menu.wotype) { CNFG.WOTYPE+=sep+k; sep=","; }
  CNFG.nWty=CNFG.WOTYPE.length;
  buildMenu();
 
  
  Resize();    // and draw body val
  
  f_update(1);
  f_reverse();
}},



f_reverse:function(){
with(moCal){
  REVERSE++;
  if(REVERSE>15) {  REVERSE=0;
      PAR["f_codes"]=[];
      for(k in CNFG.ifc) { PAR["f_codes"].push(k); }
      
      Ajax.sendAjaxPost(CallReverse,"param="+JSON.stringify(PAR),"moCal.retCallReverse");
      return;
  }
  setTimeout(function(){ moCal.f_reverse();},2000);
}},


retCallReverse:function(a){
with(moCal){

  UPDT+=+a;
  f_setUpdate();

  REVERSE=0;
  f_reverse();
}},


f_setUpdate:function(){
  mf$("id_btn_update").className="cl_btn"+(moCal.UPDT?" cl_btn_red":""); 
},


buildMenu:function(){
with(moCal){  

    var r,asgm=[], dtrng=[], C=CNFG.menu;
    
    asgm.push( {Txt:LANG('All'), Type:3, Group:2, Status:1, Icon:['','radio'], Fnc:'moCal.f_chg_assignment(0)' } );
    for(r=0;r<C.assignment.length;r++){
      asgm.push( {Txt:LANG(C.assignment[r][1]), Type:3, Group:2, Icon:['','radio'], Fnc:'moCal.f_chg_assignment('+C.assignment[r][0]+')' } );
      if(!r) C.ASSIGN=C.assignment[0][0];
    }
    
    for(r=0;r<C.typerange.length;r++){
      dtrng.push( {Txt:LANG(C.typerange[r][1]), Type:3, Status:(r?0:1), Group:3, Icon:['','radio'], Fnc:'moCal.f_chg_typerange('+C.typerange[r][0]+')' } );
      if(!r) C.TYPERANGE=C.typerange[0][0];
    }
 
    menufb=[
        {Txt:LANG(Labels.f_type_id), Type:0, Status:0, Icon:['FlaticonsStroke.58648'], Fnc:'moCal.f_wotype()' },
        {Txt:LANG('Priority'), Type:0, Status:0, Icon:['FlaticonsStroke.58540'], Fnc:'moCal.f_priority()' },
        {Txt:LANG(Labels.f_owner), Type:1,  Icon:['FlaticonsStroke.58544'], Sub:asgm },
        {Txt:LANG('Date range'), Type:1,  Icon:['FlaticonsStroke.58648'], Sub:dtrng },
        {Type:4},
        {Txt:LANG('Show WO range'), Type:2, Status:0, Icon:['checked',''], Fnc:'moCal.chgNORANGE()' },
        {Txt:LANG('Print'), Type:0, Status:0, Icon:['FlaticonsStroke.58414'], Fnc:'' },
        {Type:4},
        {Txt:LANG('Work Range'), Type:0, Status:0, Icon:[''], Fnc:'moCal.f_WorkRange()' },
        {Txt:LANG('Clear Filters'), Type:0, Icon:['FlaticonsStroke.58456'], Fnc:'moCal.f_ClearAllFilter()' },
        {Txt:LANG('Debug'), Type:0, Status:0, Icon:[''], Fnc:'Debug.Open()' },
      ]
 
}},




f_chg_assignment:function(i){
with(moCal){
   CNFG.ASSIGN=i;
   f_showhide_filter();
   Resize();
}},



f_chg_typerange:function(i){
with(moCal){

   CNFG.TYPERANGE=i;
   f_update(1);
}},





f_update:function(i){      // i=1   force update
with(moCal){
                   
  var D=f_Dt1Dt2();             
  
  if(!i && MW && CNFG.D1){
    var d1=D.di.YY+"/"+D.di.MM+"/"+D.di.DD;   
    if(d1==CNFG.D1) { Resize(); return; }
  }
  
  Dt1=D.di, Dt2=D.df;
  f_waiting(1);
              
  PAR={ mode:Mode, daterange:{ from:[Dt1.YY,Dt1.MM,Dt1.DD], to:[Dt2.YY,Dt2.MM,Dt2.DD] }, assignment:CNFG.ASSIGN, typerange:CNFG.TYPERANGE };
  Ajax.sendAjaxPost(CallWo,"param="+JSON.stringify(PAR),"moCal.retUpdateMonth");
  REVERSE=0;
}},

retUpdateMonth:function(a){
with(moCal){  
  
  try{ var jsn=JSON.parse(a); }catch(e){ alert(a); return; } 
/*  
  var jsn={
    "data": [
        [
            "19073",
            "9",
            "13",
            "Richiesta di preventivo -> ZINGONIA -> PROCESSI -> GLATT -> Compila il form",
            "1",
            "1462453682",
            null,
            ""
        ],
        [
            "19075",
            "11",
            "13",
            "Richiesta di preventivo -> ZINGONIA -> Compila il form",
            "1",
            "1462454894",
            null,
            ""
        ],
        [
            "19084",
            "12",
            "13",
            "Richiesta di preventivo -> NEMBRO -> EDIFICIO A -> PRODUZIONE -> BICONO 2  (LOC. 55) -> Compila il form",
            "1",
            "1462458168",
            null,
            ""
        ],
        [
            "19085",
            "13",
            "13",
            "Richiesta di preventivo -> NEMBRO -> EDIFICIO A -> PRODUZIONE -> BICONO 1  (LOC. 67) -> Compila il form",
            "1",
            "1462458211",
            null,
            ""
        ],
        [
            "19086",
            "14",
            "13",
            "Richiesta di preventivo -> NEMBRO -> EDIFICIO A -> PRODUZIONE -> Compila il form",
            "1",
            "1462458353",
            null,
            ""
        ],
        [
            "19074",
            "10",
            "13",
            "Richiesta di preventivo -> BREMBATE -> Compila il form",
            "1",
            "1462490372",
            "0",
            ""
        ],
        [
            "11337",
            "525",
            "4",
            "11.2 Z",
            "0",
            "1463698800",
            "1467867660",
            ""
        ],
        [
            "11339",
            "527",
            "4",
            "34.3 Z",
            "0",
            "1463698800",
            "1467867660",
            ""
        ],
        [
            "11340",
            "528",
            "4",
            "29.2 Z",
            "0",
            "1463698800",
            "1467867660",
            ""
        ],
        [
            "19076",
            "1788",
            "4",
            "15.4 N",
            "1",
            "1465966800",
            "0",
            ""
        ],
        [
            "19078",
            "1790",
            "4",
            "34.2 N",
            "1",
            "1469250000",
            "0",
            ""
        ],
        [
            "19082",
            "1794",
            "4",
            "15.2 N",
            "1",
            "1467781200",
            "0",
            ""
        ]
    ]
};
*/  
           
  with(Dbl){
    query("TRUNCATE "+tabName);
    if(jsn.data.length) {
      query("INSERT "+tabName,jsn.data);
      query("UPDATE "+tabName+" SET priority=1 WHERE priority=0" );
    }
  }
  
   
 
  doTabPtr();
 
  f_waiting(0);
  Resize();    // and draw body val
  REVERSE=0; UPDT=0;
  f_setUpdate();
}},


doTabPtr:function(){           // fcode, progress, type, title, priority, from, to
with(moCal){
  
  var r,i,D,P,t1,t2,fd,td,fc,a,idt;
  
  CNFG.ptr={}; P=CNFG.ptr;
  CNFG.ref={}; R=CNFG.ref;
  CNFG.ifc={};
  CNFG.D1=Dt1.YY+"/"+Dt1.MM+"/"+Dt1.DD;
 
  t1=date_to_tms(Dt1), t2=date_to_tms(Dt2);
  
  D=Dbl.query("SELECT "+tabName+" * WHERE 1 ORDER BY from_date");
  for(r=0;r<D.length;r++){
     fc=D[r]["fcode"];
     R[fc]=moGlb.cloneArray(D[r]);
     
     fd=D[r]["from_date"];  if(fd<t1) fd=t1;
     td=D[r]["to_date"];  if(td>t2) td=t2;
        
     a=f_now(fd);
     a.hh=0,a.mm=0,a.ss=0;
    
     CNFG.ifc[fc]=0;
    
     for(i=0;;i++){
       CNFG.ifc[fc]++;
       idt=""+a.YY+f_zero(a.MM)+f_zero(a.DD);
       
       if(typeof(P[idt])=="undefined") P[idt]=[];
       P[idt].push([fc,i]);
       
       a=f_inc(1,0,0,a);
       ta=date_to_tms(a);
       if(ta>td) break;  
     }     
  }

  // alert(JSON.stringify(P) )
}},


f_waiting:function(i,k){
with(moCal){

k=+k;

if(tabName=="t_datacal_win" || k) { 
  JM.style.display=i?"block":"none";
  mf$("id_cal_loader").style.display=i?"block":"none";
  return;
}


try{
  if(i) parent.moGlb.loader.show(moGlb.langTranslate("Loading..."));
  else parent.moGlb.loader.hide();
} catch(e){ f_waiting(i,1);  }
  


}},




Resize:function(){
with(moCal){
  if(MTH) f_htitle_close(-Hbt,1,0);
  var rr=5, a="right", hb=33, 
      sw=Btw[0]+Btw[1]+Btw[2]+47, 
      hw=moCnvUtils.measureDivText(jhl.innerHTML,fnth);
  
  f_close_popup();
  
  W=moCalPage.WW,  H=moCalPage.HH;
  
  // header button title
  if((sw+hw+74)<W) rr=sw, a="center", hb=0;
 
  jhl.style.right=rr+"px"; 
  jhl.style.textAlign=a; 
  
  mf$("id_rbtn").style.top=hb+"px"; 
  JB.style.top=(hb+38)+"px"; 


  var Sw, bH=JB.offsetHeight, ll=0,ww, ct=28+mg+mg, cH=bH-ct;
  
  // scroll vars
  sct=0, scH=cH, sch=0;
  scl=0, scW=0, scw=0;
  
  // MW; month week
  if(!MW){
    sb=drawhMonth();
    sb+="<div "+S+"left:0px;right:0;top:"+ct+"px;height:"+cH+"px;overflow:hidden'>"+
      "<div id='id_wrap_bd' "+S+"left:0;top:0px;width:100%;'></div></div>"+
      "<div id='id_vscroll_bd' "+S+"right:2px;top:"+ct+"px;width:"+(isTouch?32:24)+"px;bottom:0px;overflow:hidden;display:none;'></div>"; 
  
  } else {
  
    Sw=cellWeek(); // build header & cells
    
    ll=LcolW+mg*2;

       // l head
    sb="<div "+S+"left:"+mg+"px;top:"+mg+"px;width:"+LcolW+"px;height:28px;background-color:"+bkhcell+";overflow:hidden;text-align:center;'>"+
          "<div style='font:16px FlaticonsStroke;color:#888;margin-top:3px;cursor:pointer;' onclick='moCal.fitWexpand()'>"+String.fromCharCode(58770)+"</div></div>"+
    
       // l body
       "<div "+S+"left:"+mg+"px;top:"+ct+"px;bottom:"+mg+"px;width:"+LcolW+"px;overflow:hidden;'>"+
         "<div id='id_lcolw_bd' "+S+"left:0;top:0px;width:100%;bottom:0;overflow:hidden;'>"+Sw.lcolw_bd+"</div>"+
       "</div>"+
    
       // r head
       "<div "+S+"left:"+ll+"px;right:"+mg+";top:"+mg+"px;height:28px;overflow:hidden'>"+
         "<div id='id_rcolw_hd' "+S+"left:0;right:0;top:0px;height:28px;overflow:hidden'>"+Sw.rcolw_hd+"</div>"+
       "</div>"+ 
       
       // r body
       "<div "+S+"left:"+ll+"px;right:"+mg+";top:"+ct+"px;bottom:"+mg+"px;overflow:hidden;'>"+
         "<div id='id_rcolw_bd' "+S+"left:0;top:0px;bottom:0;right:0;overflow:hidden;'>"+Sw.rcolw_bd+"</div>"+
       "</div>"+  

      "<div id='id_vscroll_bd' "+S+"right:2px;top:"+ct+"px;width:"+(isTouch?32:24)+"px;bottom:0px;overflow:hidden;display:none;'></div>"+
      "<div id='id_oscroll_bd' "+S+"left:"+ll+"px;right:0;bottom:4px;height:"+(isTouch?32:24)+"px;overflow:hidden;display:none;'></div>"; 
  }
 
  JB.innerHTML=sb;
  if(MW){
    
    jsc=mf$("id_rcolw_bd");
    jscl=mf$("id_lcolw_bd");
    jsch=mf$("id_rcolw_hd");
  
  }else jsc=mf$("id_wrap_bd");  // wrappers x scroll

    
  // scrollbars                    
  if(VscrCal) { delete(moCal.VscrCal); moCal.VscrCal=null; }
  if(OscrCal) { delete(moCal.OscrCal); moCal.OscrCal=null; }
  
  moCal.VscrCal=new OVscroll('moCal.VscrCal',{ displ:0, L:-100, T:0, H:0, HH:0, Fnc:'moCal.Vbarpos', pid:'id_vscroll_bd' });
  if(MW) moCal.OscrCal=new OVscroll('moCal.OscrCal',{ displ:0, OV:1, L:0, T:-100, H:0, HH:0, Fnc:'moCal.Obarpos', pid:'id_oscroll_bd' });
 
  if(!isTouch) {
    JB.onmouseover=function(e){ 
      WHEELOBJ="moCal.VscrCal"; 
      moEvtMng.addEvent(document,"keydown", moCal.keyDown); 
      moEvtMng.addEvent(document,"keyup", moCal.keyUp);      
    }
 
    
  } 
   
  // draw cells 
  if(typeof(CNFG.ptr)!="undefined"){
  
    if(!MW) { 
      sb=cellMonth();  
      jsc.innerHTML=sb;  
    }else{
      jso=mf$("id_oscroll_bd"); 
    }
    
    jsb=mf$("id_vscroll_bd"); 
    
    if(sch>scH) {
      jsb.style.display="block";
      VscrCal.Display(1); VscrCal.Resize( { L:0, T:0, H:scH, HH:sch } );
      if(sct) VscrCal.Resize( { ptr:sct } );
      
    } else jsb.style.display="none"; 
    
    if(MW){   
      if(scw>scW) {
        jso.style.display="block";
        OscrCal.Display(1); OscrCal.Resize( { L:0, T:0, H:scW, HH:scw } );
        //if(scl) OscrCal.Resize( { ptr:scl } );
      } else jso.style.display="none"; 
    }
    
  } 
  RESZ=0;
  
}},







//-----------------------------------------------------------------------------
// Cells

cellMonth:function(){
with(moCal){

  var r,i,b,c,g,C, rx=0,ry=0,dh, ch,hc,cr,hh,D,dn,hmax,tt,
      s="", nr=0, gg=[], cc=[], bkc=[], Scell=[];

  D=f_Dt1Dt2();
  Dt1=D.di, Dt2=D.df, nr=D.nr;
  
      // Total Height
      hh=scH-nr*mg;
      cr=hh%nr;       
 
  dn=moGlb.cloneArray(Dt1);
  ROW=[]; 


  hmax=0; tt=0;
  for(;;){
  
    gg.push(dn.DD); 
    cc.push( ((dn.MM!=Day.MM)?"#AAA":"#000") ); 
    if(dn.DD==NOW.DD && dn.MM==NOW.MM && dn.YY==NOW.YY) bkc.push("#E2E2E2"); else bkc.push(bkcell); 
    
      C=wrapMCell(dn,COL[rx].w);    // inside Month cell
      hc=C.h; if(!hc) hc=celm;
      Scell.push(C.s);
    
    if(hc>hmax) hmax=hc;             // hmax row

    rx++; if(rx>6) {  
      rx=0,ry++; if(ry>nr) break;     // change row / exit
    
      ROW.push( { t:tt, h:hmax } );
      tt+=hmax+mg; 
      hmax=0;  
    }   
      
    dn=f_inc(1,0,0,dn);
  }
  
  // adjust height
  sch=tt; 
  if(tt<hh){  
    dh=hh-tt+mg*nr;
    ch=parseInt(dh/nr), cr=dh%nr;
    tt=0;
    for(r=0;r<nr;r++){   
      dh=(r==(nr-1))?(ch+cr):ch;  
      ROW[r].h+=dh;
      ROW[r].t+=tt;  
      tt+=dh;   
    }
    sch=scH; 
  }

  rx=0,ry=0;
  for(i=0;;i++){  g=gg[i], c=cc[i];
 
    s+="<div id='idcell_"+i+"' class='cl_cell' "+S+"left:"+(COL[rx].l)+"px;top:"+ROW[ry].t+"px;width:"+COL[rx].w+"px;height:"+ROW[ry].h+"px;background-color:"+bkc[i]+";'>"+
       "<div "+S+"left:3px;top:3px;font:"+fnt+";color:"+c+";'>"+g+"</div>"+
       Scell[i]+
       "<div id='idovrcell_"+i+"' "+S+"left:2px;top:28px;right:2px;height:44px;border:2px solid #666;box-sizing:border-box;display:none;'></div>"+
       "</div>";
    
    rx++; if(rx>6) rx=0,ry++; 
    if(ry>=nr) break;  
  }

  return s;
}},




wrapMCell:function(d,w){   // d=date
with(moCal){

  var r, p, t, f, G, s="", h=0, d, aw,ap,
      P=CNFG.ptr, R=CNFG.ref;
  
  p=""+d.YY+f_zero(d.MM)+f_zero(d.DD);

  if(typeof(P[p])!="undefined"){  
    t=28;
    
  //  alert(P[p].length+" \ "+p)
    
    for(r=0;r<P[p].length;r++) {
      f=P[p][r];
      if(NORANGE && f[1]) continue; 
      d=R[f[0]];
      
      aw=CNFG.WOTYPE.split(",");
      ap=CNFG.PRIORITY.split(",");
      
      if( aw.indexOf(d.type+"")==-1 ) continue;
      if( ap.indexOf(d.priority+"")==-1 ) continue;
      
      if(CNFG.ASSIGN && CNFG.ASSIGN != d.code_assignment ) continue;  
      
      G=f_BoxWO(t,w, d, f[1]);
      t+=G.h; 
      s+=G.s;
      t+=2;
    } 
    h=t;   
  }
     
  return { h:h, s:s };
}},



f_BoxWO:function(t,w,d,x){   // d=data fcode     x = n. box >0  70% color
with(moCal){             

  var c,cc,nf="",s,g=2,k,onm;
  h=44;
     
  c=d.priority;            
  cc=CNFG.menu.priority[c][1]; 
        
  if(x) cc=colorTone(cc,0.35);
  else {
     k=58372;
     if(CNFG.menu.wotype[d.type]!="undefined"){
       k=CNFG.menu.wotype[d.type][1]; 
       if(!k) k=58372;
     }
     nf="<span "+S+"left:4px;top:9px;font:16px FlaticonsStroke;color:#FFF;overflow:hidden;'>"+String.fromCharCode(k)+"</span>";
     
   if(w>72){  
     nf+="<div "+S+"left:27px;top:2px;right:4px;height:20px;font:13px opensansR;color:#FFF;overflow:hidden;'>WO "+f_zero(d.progress,5)+"</div>";
     nf+="<div "+S+"left:27px;top:21px;right:2px;height:18px;font:13px opensansR;color:#FFF;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;'>"+d.title+"</div>";
   }
  }

  onm="onmouseover='moCal.f_ovrout(1,"+d.fcode+")' onmouseout='moCal.f_ovrout(0,"+d.fcode+")' onclick='moCal.f_editwo("+d.fcode+")' ";
  
  s="<div id='id_"+d.fcode+"_"+x+"' "+S+"left:"+g+"px;top:"+t+"px;width:"+(w-g*2)+"px;height:"+h+"px;border-radius:2px;border:2px solid "+cc+
    ";box-sizing:border-box;background-color:"+cc+
    ";overflow:hidden;cursor:pointer;' title='WO "+f_zero(d.progress,5)+"\n"+d.title+"' "+onm+">"+nf+"</div>";

  return { h:h, s:s };
}},


f_ovrout:function(m,f){
with(moCal){

 var r,j,d, n=CNFG.ifc[f];
 
 for(r=0;r<n;r++){
   j=mf$("id_"+f+"_"+r); 
   if(!j) continue;  
   d=j.style.backgroundColor; c=d;
   if(m) c="rgba(0,0,0,"+(MW?"0.7":"0.4")+")"; 
   j.style.borderColor=c;
   if(NORANGE) break;
 }
 
 if(NORANGE) viewRange(m,f,j,n,d);
}},



viewRange:function(m,f,j,n,c){
with(moCal){

  var r,d=colorTone(c,0.35), pj=j.parentNode, i=parseInt((pj.id.split("_")).pop());  
  for(r=1;r<n;r++){
    i++;
    fj=mf$("idovrcell_"+i);
    if(!fj) continue;
    with(fj.style){
      backgroundColor=d;
      opacity="0.7";
      display=m?"block":"none";  
    }  
  }

}},




f_Dt1Dt2:function(){
with(moCal){
  var nr=0,df,di,dn,w;
  df=new Date(Day.YY,Day.MM,1,0,0,0,0);     // first day of month
  w=df.getDay();  
  if(!w) w=6; else w--;                     
     
  di=f_inc(-w,0,0,f_now(df));               // first day/row
  df=f_inc(6,0,0,di);

  for(;;){ 
    nr++;
    if(df.MM!=Day.MM) { 
      dn=f_inc(-6,0,0,df); 
      if(dn.MM!=Day.MM) { nr--; df=f_inc(-7,0,0,df); }
      break;
    }
    df=f_inc(7,0,0,df);
  } 
  
  return { di:di, df:df, nr:nr };
}},


 

DrawHead:function(){
with(moCal){
 
  var sh,sb, E=f_head_label(), 
      rr=5, a="right", hb=33, 
      sw=Btw[0]+Btw[1]+Btw[2]+47; 
  
  sh="<div class='cl_btn' "+S+"left:0;top:0;width:27px;font:16px FlaticonsStroke;' onclick='moCal.f_next_prev(-1)'><span "+S+"left:5px;top:3px;'>"+String.fromCharCode(58814)+"</span></div>"+
     "<div class='cl_btn' "+S+"left:32px;top:0;width:27px;font:16px FlaticonsStroke;' onclick='moCal.f_next_prev(1)'><span "+S+"left:5px;top:3px;'>"+String.fromCharCode(58815)+"</span></div>"+
     "<div id='id_filter_onoff' "+S+"left:64px;top:4px;font:16px FlaticonsStroke;color:#AAA;cursor:default;display:none;'>"+String.fromCharCode(58456)+"</div>"+

      "<div id='id_head_label' "+S+"left:64px;top:2px;right:"+rr+"px;font:"+fnth+";text-align:"+a+";cursor:default;' title=''>"+E+"</div>"+

     "<div id='id_rbtn' "+S+"right:0;width:"+sw+"px;top:"+hb+"px;height:30px;'>"+
      "<div id='id_btn0' class='cl_btn_press' "+S+"right:0;top:0px;width:"+Btw[0]+"px;font:"+fnt+";' onclick='moCal.f_btn01(0)'><span "+S+"left:5px;top:5px;'>"+Lbw[0]+"</span></div>"+
      "<div id='id_btn1' class='cl_btn' "+S+"right:"+(Btw[0]+5)+"px;top:0px;width:"+Btw[1]+"px;font:"+fnt+";' onclick='moCal.f_btn01(1)'><span "+S+"left:5px;top:5px;'>"+Lbw[1]+"</span></div>"+
      "<div class='cl_btn' "+S+"right:"+(Btw[0]+Btw[1]+10)+"px;top:0px;width:"+Btw[2]+"px;font:"+fnt+";' onclick='moCal.f_menuFilter(this)'><span "+S+"left:5px;top:5px;'>"+Lbw[2]+"</span></div>"+
      "<div id='id_btn_update' class='cl_btn"+(UPDT?" cl_btn_red":"")+"' "+S+"right:"+(Btw[0]+Btw[1]+Btw[2]+15)+"px;top:0px;width:27px;font:16px FlaticonsStroke;' onclick='moCal.f_update(1)'><span "+S+"left:5px;top:3px;'>"+String.fromCharCode(58762)+"</span></div>"+
     "</div>";
 
  mf$("id_head").innerHTML=sh;
  JB.style.top=(hb+38)+"px";
  jhl=mf$("id_head_label");
}},





drawhMonth:function(){
with(moCal){  
  var bW=JB.offsetWidth, r,l,w, wk, s="", ww=bW-8*mg;

  LcolW=0,
  RcolW=ww%7,
  colW=parseInt(ww/7);
  wk=(colW<120)?sweek:aweek;  
  COL=[];

  for(r=0;r<7;r++){
   l=mg+(colW+mg)*r;
   w=colW+((r<6)?0:RcolW);
   
   COL.push({ l:l, w:w });
   
   s+="<div "+S+"left:"+l+"px;top:"+mg+"px;width:"+w+"px;height:28px;background-color:"+bkhcell+";overflow:hidden;text-align:center;'>"+
      "<div style='font:bold "+fnt+";color:#606060;margin:5px 2px 0 2px;'>"+LANG(wk[r+1])+"</div></div>"; 
  }
  jhl.title="";
 
  return s;
}},

 

                                     
f_head_label:function(){
with(moCal){

  var s=""
  if(MW) {
    var m1,g1,a1="", m2="",g2,a2, w=Day.W;
    
    if(!w) w=6; else w--;  // 0:mon - 6:sun 
    Dt1=f_inc(-w,0,0);
    Dt2=f_inc(6-w,0,0);                      
          
    m1=Dt1.MM, g1=Dt1.DD, g2=Dt2.DD, a2=Dt2.YY;
    if(a2!=Dt1.YY) a1=" "+Dt1.YY;
    if(m1!=Dt2.MM) {
      m2=LANG(smonth[Dt2.MM])+" ";      
      m1=LANG(smonth[m1]);
    } else m1=LANG(amonth[m1]);  
                              
    s=m1+" "+"<strong>"+g1+"</strong>"+a1+" - "+m2+"<strong>"+g2+"</strong>"+" "+a2;
    
  } else {
    s="<span style='cursor:pointer;text-decoration:underline;' onclick='moCal.f_htitle()'>"+LANG(amonth[Day.MM])+" "+Day.YY+"</span>"; 
    
  }  
    
  return s;
}},




 








//------------------------------------------------------------------------------------------- 
// week


cellWeek:function(){
with(moCal){

  var r,rx,ry,ch,cr,hh, bW=JB.offsetWidth, ww=bW-9*mg, 
      sh="",sl="",sr="",s, 
      w,dl=0,dn,gm,wk, bkc, wwmin=64, nr=24, l=0;

  LcolW=(ww<380)?24:40,
  RcolW=0, colW=0; 
  scW=bW-LcolW-mg*3; 
  
  // Total Height  fit +1 / -1
   hh=scH-nr*mg;
   ch=parseInt(hh/nr), cr=hh%nr;
   if(fit<0) {
    if(ch<28) ch=28,cr=0; 
   }else{
    if(ch<96) ch=96,cr=0;
   }   
   celw=ch;
      
    ROW=[];
    for(r=0;r<nr;r++) {
      // if(r==(nr-1)) ch+=cr; 
      ROW.push( { t:sch, h:ch } );
      sch+=ch+mg; 
    }
  
  // hours  lcolw_bd
  for(ry=0;ry<nr;ry++){
    if(workhour.indexOf(ry)!=-1) bkc="#D2D2D2"; else  bkc=bkcell;
    sl+="<div "+S+"left:0;top:"+ROW[ry].t+"px;width:"+LcolW+"px;height:"+ROW[ry].h+"px;background-color:"+bkc+";'>"+
        "<div "+S+"left:0;right:0;height:1px;top:"+(parseInt(ROW[ry].h/2))+"px;overflow:hidden;background-color:#DDD;'></div>"+
        "<div "+S+"left:0;right:0;top:1px;height:20px;font:"+fnt+";color:#666;text-align:center;'>"+((ry<10)?("0"+ry):ry)+((LcolW<40)?"":":00")+"</div>"+
        "</div>";  
  }      
  
  w=Day.W;
  if(!w) w=6; else w--;                         
  Dt1=f_inc(-w,0,0,Day);            // first day/row     
  dn=moGlb.cloneArray(Dt1);

  // Cols width
  COL=[]; 
  for(r=0;r<7;r++){
   C=wrapWCell(dn);       // return width  & fcodes
   w=C.w;
   if(!w) w=wwmin;
   COL.push({ l:l, w:w, f:C.f, d:dn });
   l+=w+mg;
   dn=f_inc(1,0,0,dn);
  }

   l=scW-l+mg; 
   if(l>0){
    colW=parseInt(l/7);
    RcolW=l%7;
   }

  dn=moGlb.cloneArray(Dt1);
  for(r=0;r<7;r++){
   COL[r].w+=colW+((r==6)?RcolW:0);
   COL[r].l+=dl;
   
   w=COL[r].w;
   l=COL[r].l;
   gm=(w>80)?" "+dn.DD+"/"+(dn.MM+1):"";
   wk=(w<160)?sweek:aweek;

   // header
   sh+="<div "+S+"left:"+l+"px;top:0;width:"+w+"px;height:28px;background-color:"+bkhcell+";overflow:hidden;text-align:center;'>"+
       "<div style='font:bold "+fnt+";color:#606060;margin:5px 2px 0 2px;'>"+LANG(wk[r+1])+gm+"</div></div>";  
   
    dn=f_inc(1,0,0,dn);
    dl+=colW;
  }
 
  dn=moGlb.cloneArray(Dt1);
  
  //cells
  for(ry=0;ry<nr;ry++){ 
  
   for(rx=0;rx<7;rx++){ 
   
      if(dn.DD==NOW.DD && dn.MM==NOW.MM && dn.YY==NOW.YY) bkc="#E2E2E2"; else bkc=bkcell;
      if(workhour.indexOf(ry)!=-1) bkc="#E8E8E8"; 
       
      sr+="<div class='cl_cell' "+S+"left:"+COL[rx].l+"px;top:"+ROW[ry].t+"px;width:"+COL[rx].w+"px;height:"+ROW[ry].h+"px;background-color:"+bkc+";'>"+
         "<div "+S+"left:0;right:0;height:1px;top:"+(parseInt(ROW[ry].h/2))+"px;overflow:hidden;background-color:#DDD;'></div>"+
         "</div>";
  
      dn=f_inc(1,0,0,dn);
   } 
 
    dn=f_inc(-7,0,0,dn);  
  } 

  if(cr) {
    s="<div "+S+"left:0;bottom:0;right:0;height:"+(cr-mg)+"px;background-color:"+bkcell+";overflow:hidden;'></div>"; 
    sr+=s;
    sl+=s;
  }
  
  
   //.... draw column COL[ry].f   
  for(rx=0;rx<7;rx++){    
    if(COL[rx].f.length) sr+=f_WBoxWO(COL[rx]);             
  } 
  
  jhl.title=LANG("Week")+" n. "+f_getWeek(Dt1); 
  jhl.style.cursor="default";
  
  if(fit>0){                        // Vscrollbar at 8:00 AM
    sct=ROW[8].t;
    if( (sct+scH)>sch ) sct=0;
  }else{
    sct=0; 
  }  
                 
  scw=COL[6].l+COL[6].w;     // Oscroll width
 
  return { rcolw_hd:sh, lcolw_bd:sl, rcolw_bd:sr }

}},





wrapWCell:function(d){  
with(moCal){
   var fc=[], w=0, r,f,p,n,aw,ap,dd, minWw=80,
       P=CNFG.ptr, R=CNFG.ref;
   p=""+d.YY+f_zero(d.MM)+f_zero(d.DD);
   
   if(typeof(P[p])!="undefined"){ 
 
    for(r=0;r<P[p].length;r++) {
      f=P[p][r];
      if(NORANGE && f[1]) continue; 
      dd=R[f[0]];   
      aw=CNFG.WOTYPE.split(",");
      ap=CNFG.PRIORITY.split(",");
      if( aw.indexOf(dd.type+"")==-1 ) continue;
      if( ap.indexOf(dd.priority+"")==-1 ) continue; 

      if(CNFG.ASSIGN && CNFG.ASSIGN != dd.code_assignment ) continue;   
      
      fc.push(f);
   }
    n=fc.length;
    w=(n*minWw+2)-2+wmg*2;     
  } 
   return { w:w, f:fc }
}},




f_WBoxWO:function(C){  
with(moCal){

  var r,k,s="", di,df,tdi,tdf,R, d,si,sf,tw,ss,tl="",tdmt,onm,x,
      n,l,w,rw,t,h, c,cc,nf, dfo,dto; 
  R=CNFG.ref;

  di=C.d; di.hh=0,di.mm=0,di.ss=0
  df={ YY:di.YY, MM:di.MM, DD:di.DD, hh:23, mm:59, ss:59 }; 
  di=date_to_tms(di);
  df=date_to_tms(df);
  
  n=C.f.length;
  tw=C.w-(2*(n-1))-wmg*2;
  w=parseInt(tw/n);
 
  if(w>128) {
    w=128;
    l=C.l+parseInt( (C.w - (w*n+2*(n-1)) )/2 );
  }else{
    l=C.l+wmg;
  }
  
  for(r=0;r<n;r++){
   fc=C.f[r][0]; x=C.f[r][1];
   d=R[fc];
                                                                
   si=(d.from_date>di)?d.from_date:di;
   sf=(d.to_date<df)?d.to_date:df; 
  
   si=f_now(si);
   sf=f_now(sf);
   
   t=ROW[ si.hh ].t + parseInt(si.mm/60*celw);
   h=ROW[ sf.hh ].t-t + parseInt(sf.mm/60*celw);
   if(t<2) t=2,h-=2;

   if(h<20) h=20;
  
   c=d.priority;            
   cc=CNFG.menu.priority[c][1];
   
   if(x) { 
    cc=colorTone(cc,0.35);
    ss="";
   }else {

       k=58372;
       if(CNFG.menu.wotype[d.type]!="undefined"){
         k=CNFG.menu.wotype[d.type][1]; 
         if(!k) k=58372;
       }
       ss="<span "+S+"left:4px;top:9px;font:16px FlaticonsStroke;color:#FFF;overflow:hidden;'>"+String.fromCharCode(k)+"</span>";
       
     if(w>72){  
       ss+="<div "+S+"left:27px;top:2px;right:4px;height:20px;font:13px opensansR;color:#FFF;overflow:hidden;'>"+f_zero(d.progress,5)+"</div>";
       ss+="<div "+S+"left:27px;top:21px;right:2px;font:13px opensansR;color:#FFF;overflow:hidden;text-overflow:ellipsis;'>"+d.title+"</div>";
     }
   }
   
   dfo=f_now(d.from_date), dto=f_now(d.to_date);
   tdmt="\nfrom: "+dfo.DD+"/"+(dfo.MM+1)+"/"+dfo.YY+" - "+f_zero(dfo.hh)+":"+f_zero(dfo.mm)+"\nto: "+dto.DD+"/"+(dto.MM+1)+"/"+dto.YY+" - "+f_zero(dto.hh)+":"+f_zero(dto.mm);
   tl=f_zero(d.progress,5)+"\n"+d.title+tdmt;
   
   onm="onclick='moCal.f_editwo("+d.fcode+")' onmouseover='moCal.f_ovrout(1,"+d.fcode+")' onmouseout='moCal.f_ovrout(0,"+d.fcode+")' ";    
   
   s+="<div id='id_"+d.fcode+"_"+x+"' "+S+"left:"+l+"px;top:"+t+"px;width:"+w+"px;height:"+h+"px;border-radius:2px;border:2px solid "+cc+
      ";box-sizing:border-box;background-color:"+cc+";opacity:0.8;overflow:hidden;' title='"+tl+"' "+onm+">"+ss+"</div>";
   
   l+=w+2;
  }
  

 return s;
}},



fitWexpand:function(){
with(moCal){
  fit*=-1;
  Resize();
}},




f_WorkRange:function(){
with(moCal){
   
   
}},




//------------------------------------------------------------------------------
// events


keyDown:function(e){ if(!e) e = window.event; 
with(moCal){
   
  if(e.ctrlKey) CTRL=1; 
  MDRJ.style.display="block"; 
}},

keyUp:function(e){ if(!e) e = window.event;
with(moCal){     
  if(e.ctrlKey) CTRL=0; 
  MDRJ.style.display="none"; 
}},



f_panDown:function(e){ 
with(moCal){
  var P=moEvtMng.getMouseXY(e);
  odrg.ix=P.x, odrg.iy=P.y,
  odrg.x=P.x, odrg.y=P.y,
  odrg.mv=1;   
  odrg.vy=VscrCal?VscrCal.ptr:0; 
  odrg.vx=OscrCal?OscrCal.ptr:0;
    
  moEvtMng.cancelEvent(e);
}},

f_panMove:function(e){ 
with(moCal){
  if(odrg.mv){
    var P=moEvtMng.getMouseXY(e);
    with(odrg){
      x=P.x-ix, y=P.y-iy;  
      if(VscrCal) VscrCal.PosPtr(vy-y);
      if(OscrCal) OscrCal.PosPtr(vx-x);  
  }}
  moEvtMng.cancelEvent(e);
}},

f_panUp:function(e){ 
  moCal.odrg.mv=0;
  moEvtMng.cancelEvent(e);
},




f_btn01:function(i){   // switch month / week
with(moCal){
  if(MW==i || MTH) return false;
  mf$("id_btn"+MW).className="cl_btn";
  MW=i;
  mf$("id_btn"+MW).className="cl_btn_press";
  
  if(NOW.MM==Day.MM && NOW.YY==Day.YY) {  // if month == now  go in week of now
    Day.DD=NOW.DD; 
    Day.W=NOW.W;
  }
  
  JB.style.backgroundColor=MW?"#888":"#AAA";
  
  jhl.innerHTML=f_head_label();

  Resize();
  noFocus();
}},


f_next_prev:function(i){   // i: +1 -1
with(moCal){
  if(MTH) return false;
  var d,m;
  
  if(MW) { 
    d=7*i, m=0;  
  } else {
   Day.DD=1;
   d=0, m=1*i;
  } 
  
  Day=f_inc(d,m,0);
  jhl.innerHTML=f_head_label();
  noFocus();
  
  f_update(0); 
}},


f_menuFilter:function(fj){
with(moCal){
   if(MTH) return false;
   var p=moGlb.getPosition(fj);
   moMenu.Start(menufb,{l:p.l, t:p.t, w:p.w, h:p.h},2);
   noFocus();
}},




Vbarpos:function(i){
with(moCal){

  if(!jsc) return;
  jsc.style.top=-i+"px";
  if(MW) jscl.style.top=-i+"px";
}},

Obarpos:function(i){
with(moCal){
  if(!jsc) return;
  jsc.style.left=-i+"px";
  jsch.style.left=-i+"px";
}},



f_htitle:function(i){
with(moCal){
 
   if(MW) return false;
   if(MTH) { f_htitle_close(); return false; }
   MTH=1;
   var j,jm,ww,hh,r,lr,llr,cgx,cgy,rcgx,mh,x,y,rx,ty,i=0,
       s="", g=5;
   
   ww=JB.offsetWidth, hh=JB.offsetHeight;
 
   cgx=parseInt(ww/3-g); rcgx=0;
   if(cgx>260) { cgx=260; rcgx=parseInt((ww-((cgx+g)*3))/2);  }
   
   cgy=parseInt((hh-g*6)/5);
   if(cgy>100) cgy=100;
   
   Hbt=cgy*5;
   jm=mf$("id_months")
   jm.style.top=(-Hbt)+"px";
   jm.style.height=Hbt+"px";
   
   
   mh=(cgx<120)?smonth:amonth;
   ty=(cgy-22)/2;
   
   lr=parseInt(cgx-g)/2; // (cgx&1)
   x=rcgx, y=0;
   s+=f_mbt("f_today()",x,y,lr,cgy,ty+2,"<span style='font:"+fnt+";'>"+LANG("Today")+"</span>");
   
   x+=g+lr;  llr=lr+((cgx&1)?1:0);
   s+=f_mbt("f_chg_year(-1)",x,y,llr,cgy,ty-6,"<span style='font:24px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58802)+"</span>");
   
   x+=g+llr;
   s+=f_mbt("",x,y,cgx,cgy,ty-4,"<span id='idyearmonth' style='font:bold 22px opensansR;color:#000;'>"+Day.YY+"</span>");     // year
   
   x+=g+cgx;
   s+=f_mbt("f_chg_year(1)",x,y,llr,cgy,ty-6,"<span style='font:24px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58803)+"</span>");
   
   x+=g+llr;
   s+=f_mbt("f_htitle_close()",x,y,lr,cgy,ty-6,"<span style='font:24px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58827)+"</span>",1);
   
   // btn
   for(r=1;r<5;r++){   
    y=r*(g+cgy);
    for(rx=0;rx<3;rx++){ 
      x= rcgx+rx*(g+cgx);  
      s+=f_mbt("f_set_monthY("+i+")",x,y,cgx,cgy,ty,mh[i],(rx==2)?1:0);
      i++;
    }
   }

   jm.innerHTML=s;
   j=mf$("id_months_mask");
   j.style.opacity="0.1";
   j.style.display="block";
   mf$("id_months_cont").style.display="block";
   
   f_anim(-Hbt,8,0.1);
   VscrCal.Display(0);
}},


f_anim:function(hh,k,o){
  hh+=k;  
  o+=0.06;if(o>0.9) o=0.9;
  if(hh>0) hh=0,o=0.9;  
  mf$("id_months_mask").style.opacity=o;
  mf$("id_months").style.top=hh+"px";
  if(!hh) return;
  k+=k/4;
setTimeout(function(){ moCal.f_anim(hh,k,o); },40);
},



f_mbt:function(fnc,l,t,w,h,dt,txt){
with(moCal){
 if(fnc) fnc="onclick='moCal."+fnc+";' class='cl_mbtn' "; else fnc="class='cl_nombtn'";
 return "<div "+S+"left:"+l+"px;top:"+t+"px;width:"+w+"px;height:"+h+"px;' "+fnc+">"+
        "<div "+S+"left:0;top:"+dt+"px;width:100%;font:"+fnth+";text-align:center;color:#000;'>"+txt+"</div></div>";
}},


f_chg_year:function(i){
   moCal.noFocus();
   var v=parseInt(mf$("idyearmonth").innerHTML)
   v+=i;
   mf$("idyearmonth").innerHTML=v; 
   return false;  
},

f_set_monthY:function(m){
with(moCal){
    var y=parseInt(mf$("idyearmonth").innerHTML);  
    Day.YY=y, Day.MM=m; 
    jhl.innerHTML=f_head_label();
    RESZ=1;
    f_htitle_close();
}},

f_today:function(m){
with(moCal){
    Day.YY=NOW.YY, Day.MM=NOW.MM; 
    jhl.innerHTML=f_head_label();
    RESZ=1;
    f_htitle_close();
}},


f_htitle_close:function(h,k,o){
with(moCal){   
   if(!h) h=0,k=8,o=0.9;  
   h-=k; o-=0.1;
   if(h<-Hbt){
    mf$("id_months_mask").style.display="none";
    mf$("id_months_cont").style.display="none";
    MTH=0;
    VscrCal.Display(1);
    if(RESZ) f_update(0); 
    return;
   }
   mf$("id_months_mask").style.opacity=o;
   mf$("id_months").style.top=h+"px";
   k+=k/4;
setTimeout(function(){ moCal.f_htitle_close(h,k,o); },40);
}},


chgNORANGE:function(){
with(moCal){ 

  NORANGE=+!NORANGE;
  Resize();

}},



// popup
f_editwo:function(fc){
with(moCal){ 
   var par={ f_code:fc  };
   Ajax.sendAjaxPost(CallEditWo,"param="+JSON.stringify(par),"moCal.retEditWo"); 
}},

/*
"Title",                    //  0 title
"description"               //  1 Descrizione
1,                          //  2 wo type"
2,                          //  3 priority
"responsabile",             //  4 responsabile
"richiedente",              //  5 richiedente
"asset",                    //  6 asset
"28/07/1969",               //  7 date_creation
"22/12/2014",               //  8 date_planned_from
"24/12/2014",               //  9 date_planned_to
"23/12/2014",               // 10 date_effective_from
"25/12/2014"                // 11 date_effective_to
*/

retEditWo:function(a){
with(moCal){ 

  var jsn,d;
  
  try{ jsn=JSON.parse(a); }catch(e){ alert(a); return; } 

  var fc,x,y,dy=66,dw=240,v,dd, df=null,dt=null, def=null,det=null, dcr=null,
      s,w,h,bt,col;
  
  d=jsn.data, fc=jsn.fcode, dd=CNFG.ref[fc];
  
  bt=LANG("Workorder:")+" "+f_zero(dd.progress,5); 
   
  w=(W>506)?506:266;
  h=(H>580)?580:(H-40);   
   
  col=(W>506)?1:0;
   
  if(d[7]) dcr=f_now(d[7]); 
   
  if(d[8])  df=f_now(d[8]);
  if(d[9])  dt=f_now(d[9]);
  
  if(d[10])  def=f_now(d[10]);
  if(d[11])  det=f_now(d[11]); 
  
   
  // form 
  s="<div "+S+"left:10px;top:0;right:20px;bottom:48px;font:"+fnt+";overflow:hidden;'>"+
    "<div id='id_wrap_Edbd' "+S+"left:0;top:0;right:0px;font:"+fnt+";'>";
  
    
  // title
  x=0;y=0;
  s+=f_input(x,y,LANG(Labels.f_title), LANG(d[0]) );
  
  // description
  if(col) x+=dw; else y+=dy; 
  s+=f_textarea(x,y,LANG(Labels.f_description), LANG(d[1]) );
    

    
  // type
  x=0; y+=dy+28;
 
 try{ 
  v=CNFG.menu.wotype[ d[2] ][0];
 }catch(e){ return; }
 
  s+=f_input(x,y,LANG(Labels.f_type_id), LANG(v) );
  
  // priority
  if(col) x+=dw; else y+=dy; 
  v=CNFG.menu.priority[ d[3] ][0];
  s+=f_input(x,y,LANG(Labels.f_priority), LANG(v) );
    
    
  // asset
  x=0;y+=dy;
  s+=f_input(x,y,LANG(Labels.asset), LANG(d[6]) );  
    
    
   // Workflow phase
  if(col) x+=dw; else y+=dy; 
  s+=f_input(x,y,LANG(Labels.wf_phase), LANG(d[12]) );    
    
    
  // responsabile
  x=0; y+=dy;
  s+=f_input(x,y,LANG(Labels.f_owner), LANG(d[4]) );
  
  // richiedente
  if(col) x+=dw; else y+=dy; 
  s+=f_input(x,y,LANG(Labels.f_requestor), LANG(d[5]) );
   

    
  // date creation    
  x=0;y+=dy;
  v=f_lang_date(dcr);
  s+=f_input(x,y,LANG(Labels.f_creation_date), v );
  

  // date planned from    
  x=0;y+=dy;
  v=f_lang_date(df);
  s+=f_input(x,y,LANG(Labels.f_start_date), v );
   
  // date planned to    
  if(col) x+=dw; else y+=dy;
  v=f_lang_date(dt);
  s+=f_input(x,y,LANG(Labels.f_end_date), v );
  


  // date effective from    
  x=0;y+=dy;
  v=f_lang_date(def);
  s+=f_input(x,y,LANG(Labels.fc_wo_starting_date), v );
   
  // date effective to    
  if(col) x+=dw; else y+=dy; 
  v=f_lang_date(det);
  s+=f_input(x,y,LANG(Labels.fc_wo_ending_date), v );
  
  s+="</div></div>"+
     "<div id='id_vscroll_Edbd' "+S+"right:2px;top:0;width:"+(isTouch?20:12)+"px;bottom:48px;overflow:hidden;'></div>";
  
  // buttons
  s+="<div class='cl_btn' "+S+"left:0;bottom:8px;'><div style='font:13px opensansR;padding:5px 6px 0 6px;' onclick='moCal.f_close_popup()'>"+LANG("Cancel")+"</div></div>"+
     "<div class='cl_btn_disab' id='id_btn_edtwolnk' "+S+"right:0;bottom:8px;'><div style='font:13px opensansR;padding:5px 6px 0 6px;' onclick='moCal.btnEditLink("+fc+")'>"+LANG("Edit Workorder")+"</div></div>";
 
 
  f_open_popup(w,h,bt,s,y+48);
}},




f_lang_date:function(d){
with(moCal){
  
  if(!d) return "";
  var v, l=1, 
  DD=f_zero(d.DD),
  MM=f_zero(d.MM+1),
  YY=d.YY,
  hh=f_zero(d.hh),
  mm=f_zero(d.mm);
  
  try{ l=parent.moGlb.user.language;  }catch(e){}

  v=MM+"/"+DD+"/"+YY+", "+hh+":"+mm;  // default ENG
  
  if(l==2) v=DD+"/"+MM+"/"+YY+", "+hh+":"+mm;  // italian

  return v;
}},




f_input:function(x,y,lbl,v){
with(moCal){

  var s,ww=220;

  s="<div class='cl_label' style='left:"+x+"px;top:"+y+"px;'>"+lbl+"</div>"+
      "<div class='cl_inpdiv' style='left:"+x+"px;top:"+(y+18)+"px;width:"+ww+"px;'>"+
      "<input class='cl_inp' type='text' value='"+v+"' readonly style='width:"+ww+"px;' /></div>";

  return s;
}},



f_textarea:function(x,y,lbl,v){
with(moCal){

  var s,ww=220;
    
    s="<div class='cl_label' style='left:"+x+"px;top:"+y+"px;width:"+ww+"px;'>"+lbl+"</div>"+
      "<textarea class='cl_inpdiv' style='left:"+x+"px;top:"+(y+18)+"px;height:60px;width:"+ww+
      "px;padding:2px 0 0 5px;font:"+fnt+";background-color:#FFF;' readonly>"+v+"</textarea>";

  return s;
}},


 
f_ClearAllFilter:function(){
with(moCal){ 

  var k,sep="",Sb;
  CNFG.tempPRIORITY="";
  CNFG.PRIORITY="";
  CNFG.tempWOTYPE="";
  CNFG.WOTYPE="";
  
  for(var k in CNFG.menu.priority) { CNFG.PRIORITY+=sep+k;  CNFG.tempPRIORITY+=sep+k;  sep=","; }
  sep="";
  for(k in CNFG.menu.wotype) { CNFG.WOTYPE+=sep+k;  CNFG.tempWOTYPE+=sep+k;  sep=","; }

  Sb=menufb[2].Sub;
  for(k=0;k<Sb.length;k++) Sb[k].Status=k?0:1;

  f_chg_assignment(0);
}},

f_showhide_filter:function(){
with(moCal){ 

  var p=0;
  if( typeof(CNFG.tempPRIORITY)=="undefined" || CNFG.tempPRIORITY.length==CNFG.nPri) p++;
  if( typeof(CNFG.tempWOTYPE)=="undefined" || CNFG.tempWOTYPE.length==CNFG.nWty) p++;
  if(!CNFG.ASSIGN) p++;
 
  mf$("id_filter_onoff").style.display=(p==3)?"none":"block"; 
}},

f_wotype:function(){
with(moCal){ 
   
   CNFG.tempWOTYPE=CNFG.WOTYPE;
   var k, bt, y=0 ,s="", p, a=CNFG.tempWOTYPE.split(",");
  
   for(k in CNFG.menu.wotype){ 
    
    p=58783; 
    if(a.indexOf(k+"")!=-1) p=58726; // checked
       
    s+="<div "+S+"left:10px;width:32px;top:"+y+"px;font:16px FlaticonsStroke;color:#4A6671;cursor:pointer;' onclick='moCal.chg_Pwotype("+k+",this)'>"+String.fromCharCode(p)+"</div>"+ 
       "<div "+S+"left:32px;top:"+(y+4)+"px;font:13px opensansR;color:#000;'>"+CNFG.menu.wotype[k][0]+"</div>";    

    y+=28;
   }
  
   y+=2;
   s+="<div id='id_msgs_popup' "+S+"left:0;top:"+y+"px;font:13px opensansR;color:C00;display:none;'>"+LANG("select at least one element")+"</div>";
 
   y+=26;
   s+="<div class='cl_btn' "+S+"left:0;top:"+y+"px;'><div style='font:13px opensansR;padding:5px 6px 0 6px;' onclick='moCal.f_close_popup()'>"+LANG("Cancel")+"</div></div>"+
      "<div class='cl_btn' "+S+"right:0;top:"+y+"px;'><div style='font:13px opensansR;padding:5px 6px 0 6px;' onclick='moCal.set_Pwotype()'>"+LANG("Save")+"</div></div>";
 
   bt="WO Type";
   f_open_popup(200,y+84,bt,s,0); 
}},


chg_Pwotype:function(v,vj){
with(moCal){ 
   var i,a=CNFG.tempWOTYPE?CNFG.tempWOTYPE.split(","):[]; 
   i=a.indexOf(v+"")   
    if(i==-1) {   
      p=58726;            // checked
      a.push(v+""); 
    }else{ 
      p=58783;
      a.splice(i,1);
    }  
   CNFG.tempWOTYPE=a.join(",");
   vj.innerHTML=String.fromCharCode(p);  
   mf$("id_msgs_popup").style.display="none";
   
   f_showhide_filter();
}},


set_Pwotype:function(){
with(moCal){
  var tp=CNFG.tempWOTYPE; 
  if(!tp) { mf$("id_msgs_popup").style.display="block"; return false; }
  CNFG.WOTYPE=tp;
  f_close_popup();
  Resize();
}},




// popup priority
f_priority:function(){
with(moCal){ 
   
   CNFG.tempPRIORITY=CNFG.PRIORITY;
   var k, bt, y=0 ,s="", p, a=CNFG.tempPRIORITY.split(",");
  
   for(k in CNFG.menu.priority){
    
    p=58783; 
    if(a.indexOf(k+"")!=-1) p=58726; // checked
  
    s+="<div "+S+"left:10px;width:32px;top:"+y+"px;font:16px FlaticonsStroke;color:#4A6671;cursor:pointer;' onclick='moCal.chg_Ppriority("+k+",this)'>"+String.fromCharCode(p)+"</div>"+ 
       "<div "+S+"left:32px;top:"+(y+4)+"px;font:13px opensansR;color:#000;'>"+CNFG.menu.priority[k][0]+"</div>";      
    y+=28;
   }
 
   y+=2;
   s+="<div id='id_msgs_popup' "+S+"left:0;top:"+y+"px;font:13px opensansR;color:C00;display:none;'>"+LANG("select at least one element")+"</div>";
 
   y+=26;
   s+="<div class='cl_btn' "+S+"left:0;top:"+y+"px;'><div style='font:13px opensansR;padding:5px 6px 0 6px;' onclick='moCal.f_close_popup()'>"+LANG("Cancel")+"</div></div>"+
      "<div class='cl_btn' "+S+"right:0;top:"+y+"px;'><div style='font:13px opensansR;padding:5px 6px 0 6px;' onclick='moCal.set_Ppriority()'>"+LANG("Save")+"</div></div>";
 
   bt="WO Priority";
   f_open_popup(200,y+84,bt,s,0); 
}},



chg_Ppriority:function(v,vj){
with(moCal){ 
   var i,a=CNFG.tempPRIORITY?CNFG.tempPRIORITY.split(","):[]; 
   i=a.indexOf(v+"")   
    if(i==-1) {   
      p=58726;            // checked
      a.push(v+""); 
    }else{ 
      p=58783;
      a.splice(i,1);
    }  
   CNFG.tempPRIORITY=a.join(",");
   vj.innerHTML=String.fromCharCode(p);  
   mf$("id_msgs_popup").style.display="none";
   
   f_showhide_filter();
}},


set_Ppriority:function(){
with(moCal){
  var tp=CNFG.tempPRIORITY; 
  if(!tp) { mf$("id_msgs_popup").style.display="block"; return false; }
  CNFG.PRIORITY=tp;
  f_close_popup();
  Resize();
}},






f_open_popup:function(w,h,bt,s,scr){
with(moCal){
   
   with(JM.style) opacity="0.8", backgroundColor="#4A6671", display="block";  
   with(JP.style){
    if(w){
      width=w+"px";
      height=h+"px";
      marginLeft=(-w/2)+"px";
      marginTop=(-h/2)+"px";
      if(!bt) bt="";
      mf$("id_bartitle_popup").innerHTML=bt; 
      if(!s) s="";
      mf$("id_body_popup").innerHTML=s;
    } 
    VscrCal.Display(0);
    display="block";
   }
   
   
   
   // vertical scroll 
   if(scr){
    sceh=scr;

    jsedc=mf$("id_wrap_Edbd");
    sceH=mf$("id_vscroll_Edbd").offsetHeight;
 
    if(sceh>sceH){
      moCal.VscrEdCal=new OVscroll('moCal.VscrEdCal',{ displ:0, L:-100, T:0, H:0, HH:0, Fnc:'moCal.VbarEdpos', pid:'id_vscroll_Edbd' });
      VscrEdCal.Display(1); 
      VscrEdCal.Resize( { L:0, T:0, H:sceH, HH:sceh } ); 
      jsedc.onmouseover=function(){ WHEELOBJ="moCal.VscrEdCal"; }
    }
    
    BTNEDIT=0;
    pollingCheckEditBtn();
 
    
    // js$("id_btn_edtwolnk")
    
   }
   
}},



pollingCheckEditBtn:function(){
with(moCal){

  var act, lke;
  
  try { 
      var tgj=parent.mdl_wo_tg_tg, crud=tgj.crud;
      
      act=eval("parent.eval(crud).active"); 
      lke=parent.mdl_wo_tg_tg.lkEdit; 
    
      BTNEDIT=(!lke && act)?1:0;
      
  }catch(e){ return; }
  
  
  mf$("id_btn_edtwolnk").className=(BTNEDIT?"cl_btn":"cl_btn_disab");
  
setTimeout( function(){ pollingCheckEditBtn();  },1000 );
}},



btnEditLink:function(fc){
with(moCal){

  if(!BTNEDIT) return;

  try { 
      var tgj=parent.mdl_wo_tg_tg;

      if(BTNEDIT) {
          tgj.aLoad=[];   
          tgj.SEARCH=[{"f":"f_code","v0":Base64.encode(fc+""),"Mm":0,"Sel":"0","type":1}];
          tgj.FILTER=[];
          tgj.AND=[];
          tgj.SELECTOR={};
          tgj.SORT=[];
          tgj.ActRefresh();
      }
    
    }catch(e){}

}},




VbarEdpos:function(i){
with(moCal){
  if(!jsedc) return;
  jsedc.style.top=-i+"px";
}},




f_close_popup:function(){
with(moCal){
 try{
   VscrCal.Display(1);
   JP.style.display="none";
   with(JM.style) opacity="0.2", backgroundColor="#FFF", display="none";
   if(VscrEdCal) { delete(moCal.VscrEdCal); moCal.VscrEdCal=null; }
 }catch(e){}  
}},














// functions

f_inc:function(d,m,y,dd){    // dd = { YY, MM, DD, hh, mm, ss, W  }
with(moCal){
  if(!dd) dd=Day;
  var D=dd.DD+d,
      M=dd.MM+m,
      Y=dd.YY+y,
  dt=new Date(Y,M,D,dd.hh,dd.mm,dd.ss);   
  return f_now(dt);
}},



f_now:function(dt){ 
  var d;
  if(!dt) d=new Date();                              // now
  else {
    if(typeof(dt)=="number") d=new Date(dt*1000);  // from timestamp
    else d=dt;                                       // drom Date
  }      
  var h=d.getHours(),
  m=d.getMinutes(),
  s=d.getSeconds(), 
  D=d.getDate(),
  M=parseInt(d.getMonth()),
  Y=d.getFullYear(),
  W=d.getDay();           // day of week

  return { DD:D, MM:M, YY:Y, hh:h, mm:m, ss:s, W:W };
},


f_getWeek:function(dt){  
  var date=new Date(dt.YY,dt.MM,dt.DD,0,0,0,0);
  date.setDate(dt.DD+3-(date.getDay()+6)%7); 
  var week1=new Date(date.getFullYear(),0,4); 
  return 1+Math.round(((date.getTime()-week1.getTime())/86400000-3+(week1.getDay()+6)%7)/7); 
},

noFocus:function(){
  if(!isTouch) setTimeout(function(){ mf$('idinputnofocus').focus(); },200);
  return false;
},


date_to_tms:function(d){
  var dta=new Date(d.YY,d.MM,d.DD,d.hh,d.mm,d.ss);
  return parseInt((dta.getTime())/1000);
},

f_zero:function(v,n){ if(!n) n=2;
  var r,s=v+"", i=s.length;
  for(r=i;r<n;r++) s="0"+s;
  return s;
},

/*
 c = colore in formato #000   o   #000000   o rgb(r,g,b)
 t = tonalità    0 <= t < 1
 m = formato  0=#000000  1=rgb(00,00,00)
 a = alpha    0<a<1   return rgba()
*/
colorTone:function(c,t,m,a){ 
  if(t<0 || t>=1) return c;
    if(!m) m=0; else m=1;
    
    var r,v,f=c.split("(");
    if(f.length>1){
            f=f[1].substring(0,f[1].length-1);
            f=f.split(",");
            c="#"; 
            for(r=0;r<3;r++) {
              v=(parseInt(f[r])).toString(16);
              c+=moCal.f_zero(v); 
            } 
    }
    
    var n=c.length, d=(n>4)?2:1, dd=0, s=[], v="", v0, v1;
      for(r=1;r<n;r++) { v+=c.charAt(r);
      dd++; if(dd==d) { v0= parseInt((d>1)?v:(v+v), 16);
               v1=parseInt(  v0+((255-v0)*(1-t)) ); if(!m) v1=v1.toString(16);
               if(v1.length<2) v1="0"+v1;
               s.push(v1); v=""; dd=0; } }
    if(m) { v0="rgb"+(a?"a":"")+"("+s.join(",")+(a?(","+a):"")+")"; }
    else v0="#"+s.join("");
  return v0;
}, 

colorToneDark:function(c,t,m,a){ 
  if(t<0 || t>=1) return c;
    if(!m) m=0; else m=1;
    var r,n=c.length, d=(n>4)?2:1, dd=0, s=[], v="", v0, v1;
      for(r=1;r<n;r++) { v+=c.charAt(r);
      dd++; if(dd==d) { v0= parseInt((d>1)?v:(v+v), 16);
               v1=parseInt(v0*(1-t)); if(!m) v1=v1.toString(16);
               if(v1.length<2) v1="0"+v1;
               s.push(v1); v=""; dd=0; } }
    if(m) { v0="rgb"+(a?"a":"")+"("+s.join(",")+(a?(","+a):"")+")"; }
    else v0="#"+s.join("");
  return v0;
} 


} //      end






function LANG(w){
  var r=w, b=0;
  try{ r=parent.moGlb.langTranslate(w); }catch(e){ b=1; }    // desk
  if(b) try{ r=parent.LANG(w); }catch(e){}                   // mobile
  return r;
}







// debug
//mf$("debug").innerHTML+=hh+"<br>";
  
  