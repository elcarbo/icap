/* Calendar

button 
left-right circle: 58814-58815
printer:           58414

props:
MW:     0=Month    1=Week
                                                                                                                           
                                                                                                                              
to Translate
"Month","Week","Options",
"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"
"Sun","Mon","Tue","Wed","Thu","Fri","Sat"
"January","February","March","April","May","June","July","August","September","October","November","December"


todo:
resize left top
collapse no fullpage

NORANGE     view range of WO

CNFG
  ASSIGN
  TYPERANGE
  PRIORITY   
   menu.
    priority
    wotype
    assignment
    typerange
  ptr  [YYYYMMDD]=[ [fcode, (N. first)],... ]
  ref  [fcode]=row_data           (fcode, progress, type, title, priority, from_date, to_date, code_asset, asset )
  

workhour:[],    mark work hours to left of week table

*/






moRese={ Mode:0, USID:1, MW:0, NORANGE:0, conn:null, JM:null, JP:null, MDRJ:null, CTRL:0, 
        W:0,H:0, JB:null, jhl:null, OscrCal:null, VscrCal:null, jsb:null, jso:null,
        Day:{}, Dt1:null, Dt2:null, NOW:{}, odrg:{}, PAR:"", REVERSE:0, UPDT:0,
        LcolW:0, RcolW:0, colW:0, mg:1, COL:[], ROW:[], CELLTMS:[], EditVal:{},
        celm:40, celw:0, fit:1, wmg:10, Hbt:0, MTH:0, RESZ:0,
        jsc:null, jsch:null, jscl:null, scl:0, scW:0, scw:0, sct:0, scH:0, sch:0,
        COLORBOX:"#4BA8C6", COLORMY:"#DAA920", MY:0, WND:null,
        S:"style='position:absolute;",
        bkhcell:"#EAEAEA", bkcell:"#F4F4F4",
        workhour:[9,10,11,12,14,15,16,17],                              // 9,10,11,12,14,15,16,17

  CallStart:"res_start.php", 
  CallWo:"reservation-workorders",     // CallWo:"rese_wo.php",
 
  CallReverse:"cal_reverse.php",
  CallSave:"reservation-save",            // reservation-save     rese_save.php
  
  Lbw:[],Btw:[], fnt:"13px opensansR", fnth:"18px opensansL",
  aweek:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
  sweek:["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sun"],
  amonth:["January","February","March","April","May","June","July","August","September","October","November","December"],
  smonth:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"],
  
  CNFG:{},

  menufb:[],
  
  
 
Start:function(m){    //   m:  0=module  1=wizard
with(moRese){
  
  // user id
  if(!m){
    try{ USID=parseInt(parent.moGlb.user.code);  }catch(e){ USID=1; }
  }else{
    try{ USID=parseInt(parent.parent.parent.moGlb.user.code); }catch(e){ USID=1; }
  }
 
  
  Lbw=[ LANG("Month"), LANG("Week"), LANG("Options") ];   
  Btw=[ moCnvUtils.measureDivText(Lbw[0],fnt)+10, moCnvUtils.measureDivText(Lbw[1],fnt)+10, moCnvUtils.measureDivText(Lbw[2],fnt)+10];
 
  JB=mf$("id_body");
  JM=mf$("id_cal_mask");
  JP=mf$("id_cal_popup");
  MDRJ=mf$("id_drg_mask");
 
  Day=f_now();
  NOW=f_now();
 
  // database
  Dbl.create_db("db_mainsim_calendar");
  conn=Dbl.connect_db("db_mainsim_calendar");
  if(!conn) { alert("no db"); return; }
  
  // TABLE t_datares
  Dbl.query("CREATE_TABLE t_datares (fcode int, progress int, user_id int, title string, description string, from_date int, to_date int, code_asset int, asset string, reservation_id string)");
   
 
  DrawHead();
  Ajax.sendAjaxPost(CallStart,"mode="+Mode,"moRese.retCallStart");
}},


  
retCallStart:function(a){
with(moRese){  
  CNFG:{};
  try{ CNFG=JSON.parse(a); }catch(e){ alert(a); return; }  
  
  buildMenu();
   
  Resize();    // and draw body val
  f_update(1);
  f_reverse();
}},



f_reverse:function(){
with(moRese){
  REVERSE++;
  if(REVERSE>15) {  REVERSE=0;
      Ajax.sendAjaxPost(CallReverse,"param="+PAR,"moRese.retCallReverse");
      return;
  }


  setTimeout(function(){ moRese.f_reverse();},2000);
}},


retCallReverse:function(a){
with(moRese){

  UPDT=+a;
  f_setUpdate();

  REVERSE=0;
  f_reverse();
}},


f_setUpdate:function(){
  mf$("id_btn_update").className="cl_btn"+(moRese.UPDT?" cl_btn_red":""); 
},


buildMenu:function(){
with(moRese){ 

    var r,asgm=[], dtrng=[], C=CNFG.menu;
 
    menufb=[
        {Txt:LANG('My reservation'), Type:2, Status:0, Icon:['','checked'], Fnc:'moRese.f_MYrese()' },
        {Txt:LANG('Print'), Type:0, Status:0, Icon:['FlaticonsStroke.58414'], Fnc:'' },
        {Type:4},
        {Txt:LANG('New reservation'), Type:0, Status:0, Icon:[''], Fnc:'moRese.f_cellClick(0,-1,-1)' },
        {Txt:LANG('Show WO range'), Type:2, Status:0, Icon:['checked',''], Fnc:'moRese.chgNORANGE()' },
        {Txt:LANG('Debug'), Type:0, Status:0, Icon:[''], Fnc:'Debug.Open()' },
      ]

}},


f_MYrese:function(){     
with(moRese){

  MY=menufb[0].Status;
  Resize();

}},


f_update:function(i){      // i=1   force update
with(moRese){
                   
  var D=f_Dt1Dt2();             
  
  if(!i && MW && CNFG.D1){
    var d1=D.di.YY+"/"+D.di.MM+"/"+D.di.DD;   
    if(d1==CNFG.D1) { Resize(); return; }
  }
  
  Dt1=D.di, Dt2=D.df;
  f_waiting(1);
 
  PAR=JSON.stringify({ daterange:{ mode:Mode, from:[Dt1.YY,Dt1.MM,Dt1.DD], to:[Dt2.YY,Dt2.MM,Dt2.DD] } } );
  Ajax.sendAjaxPost(CallWo,"param="+PAR,"moRese.retUpdateMonth");
  REVERSE=0;
}},

retUpdateMonth:function(a){
with(moRese){  
  
  try{ var jsn=JSON.parse(a); }catch(e){ alert(a); return; } 
  if(!jsn.data) jsn.data=[];
  
  with(Dbl){
    query("TRUNCATE t_datares");
    if(jsn.data.length) query("INSERT t_datares",jsn.data);
  } 
  

  
 
  doTabPtr();
 
  f_waiting(0);
  Resize();    // and draw body val
  REVERSE=0; UPDT=0;
  f_setUpdate();
}},


doTabPtr:function(){  
with(moRese){
  
  var r,i,D,P,t1,t2,fd,td,fc,a,idt;
  
  CNFG.ptr={}; P=CNFG.ptr;
  CNFG.ref={}; R=CNFG.ref;
  CNFG.ifc={};
  CNFG.D1=Dt1.YY+"/"+Dt1.MM+"/"+Dt1.DD;
 
  t1=date_to_tms(Dt1), t2=date_to_tms(Dt2);
  
  D=Dbl.query("SELECT t_datares * WHERE 1 ORDER BY from_date");
  for(r=0;r<D.length;r++){
     fc=D[r]["fcode"];
     R[fc]=moGlb.cloneArray(D[r]);
     
     fd=D[r]["from_date"];  if(fd<t1) fd=t1;
     td=D[r]["to_date"];  if(td>t2) td=t2;
        
     a=f_now(fd);
     a.hh=0,a.mm=0,a.ss=0;
    
     CNFG.ifc[fc]=0;
    
     for(i=0;;i++){
       CNFG.ifc[fc]++;
       idt=""+a.YY+f_zero(a.MM)+f_zero(a.DD);
       
       if(typeof(P[idt])=="undefined") P[idt]=[];
       P[idt].push([fc,i]);
       
       a=f_inc(1,0,0,a);
       ta=date_to_tms(a);
       if(ta>td) break;  
     }     
  }

  // alert(JSON.stringify(P) )
}},


f_waiting:function(i){
with(moRese){
  JM.style.display=i?"block":"none";
  mf$("id_cal_loader").style.display=i?"block":"none";
}},




Resize:function(){
with(moRese){
  if(MTH) f_htitle_close(-Hbt,1,0);
  var rr=5, a="right", hb=33, 
      sw=Btw[0]+Btw[1]+Btw[2]+47, 
      hw=moCnvUtils.measureDivText(jhl.innerHTML,fnth);
  
  W=moResePage.WW,  H=moResePage.HH;
  
  // header button title
  if((sw+hw+74)<W) rr=sw, a="center", hb=0;
  
  jhl.style.right=rr+"px"; 
  jhl.style.textAlign=a; 
  
  mf$("id_rbtn").style.top=hb+"px"; 
  JB.style.top=(hb+38)+"px"; 


  var Sw, bH=JB.offsetHeight, ll=0,ww, ct=28+mg+mg, cH=bH-ct;
  
  // scroll vars
  sct=0, scH=cH, sch=0;
  scl=0, scW=0, scw=0;
  
  // MW; month week
  if(!MW){
    sb=drawhMonth();
    sb+="<div "+S+"left:0px;right:0;top:"+ct+"px;height:"+cH+"px;overflow:hidden'>"+
      "<div id='id_wrap_bd' "+S+"left:0;top:0px;width:100%;'></div></div>"+
      "<div id='id_vscroll_bd' "+S+"right:2px;top:"+ct+"px;width:"+(isTouch?32:24)+"px;bottom:0px;overflow:hidden;display:none;'></div>"; 
  
  } else {
  
    Sw=cellWeek(); // build header & cells
    
    ll=LcolW+mg*2;

       // l head
    sb="<div "+S+"left:"+mg+"px;top:"+mg+"px;width:"+LcolW+"px;height:28px;background-color:"+bkhcell+";overflow:hidden;text-align:center;'>"+
          "<div style='font:16px FlaticonsStroke;color:#888;margin-top:3px;cursor:pointer;' onclick='moRese.fitWexpand()'>"+String.fromCharCode(58770)+"</div></div>"+
    
       // l body
       "<div "+S+"left:"+mg+"px;top:"+ct+"px;bottom:"+mg+"px;width:"+LcolW+"px;overflow:hidden;'>"+
         "<div id='id_lcolw_bd' "+S+"left:0;top:0px;width:100%;bottom:0;overflow:hidden;'>"+Sw.lcolw_bd+"</div>"+
       "</div>"+
    
       // r head
       "<div "+S+"left:"+ll+"px;right:"+mg+";top:"+mg+"px;height:28px;overflow:hidden'>"+
         "<div id='id_rcolw_hd' "+S+"left:0;right:0;top:0px;height:28px;overflow:hidden'>"+Sw.rcolw_hd+"</div>"+
       "</div>"+ 
       
       // r body
       "<div "+S+"left:"+ll+"px;right:"+mg+";top:"+ct+"px;bottom:"+mg+"px;overflow:hidden;'>"+
         "<div id='id_rcolw_bd' "+S+"left:0;top:0px;bottom:0;right:0;overflow:hidden;'>"+Sw.rcolw_bd+"</div>"+
       "</div>"+  

      "<div id='id_vscroll_bd' "+S+"right:2px;top:"+ct+"px;width:"+(isTouch?32:24)+"px;bottom:0px;overflow:hidden;display:none;'></div>"+
      "<div id='id_oscroll_bd' "+S+"left:"+ll+"px;right:0;bottom:4px;height:"+(isTouch?32:24)+"px;overflow:hidden;display:none;'></div>"; 
  }
 
  JB.innerHTML=sb;
  if(MW){
    
    jsc=mf$("id_rcolw_bd");
    jscl=mf$("id_lcolw_bd");
    jsch=mf$("id_rcolw_hd");
  
  }else jsc=mf$("id_wrap_bd");  // wrappers x scroll

    
  // scrollbars                    
  if(VscrCal) { delete(moRese.VscrCal); moRese.VscrCal=null; }
  if(OscrCal) { delete(moRese.OscrCal); moRese.OscrCal=null; }
  
  moRese.VscrCal=new OVscroll('moRese.VscrCal',{ displ:0, L:-100, T:0, H:0, HH:0, Fnc:'moRese.Vbarpos', pid:'id_vscroll_bd' });
  if(MW) moRese.OscrCal=new OVscroll('moRese.OscrCal',{ displ:0, OV:1, L:0, T:-100, H:0, HH:0, Fnc:'moRese.Obarpos', pid:'id_oscroll_bd' });
 
  if(!isTouch) {
    JB.onmouseover=function(e){ 
      WHEELOBJ="moRese.VscrCal"; 
      moEvtMng.addEvent(document,"keydown", moRese.keyDown); 
      moEvtMng.addEvent(document,"keyup", moRese.keyUp);      
    }  
  } 
   
  // draw cells 
  if(typeof(CNFG.ptr)!="undefined"){
  
    if(!MW) { 
      sb=cellMonth();  
      jsc.innerHTML=sb;  
    }else{
      jso=mf$("id_oscroll_bd"); 
    }
    
    jsb=mf$("id_vscroll_bd"); 
    
    if(sch>scH) {
      jsb.style.display="block";  
      VscrCal.Display(1); VscrCal.Resize( { L:0, T:0, H:scH, HH:sch } );
      if(sct) VscrCal.Resize( { ptr:sct } );
      
    } else jsb.style.display="none"; 
    
    if(MW){   
      if(scw>scW) {
        jso.style.display="block";
        OscrCal.Display(1); OscrCal.Resize( { L:0, T:0, H:scW, HH:scw } );
        //if(scl) OscrCal.Resize( { ptr:scl } );
      } else jso.style.display="none"; 
    }
    
  } 
  RESZ=0; 
}},







//-----------------------------------------------------------------------------
// Cells

cellMonth:function(){
with(moRese){

  var r,i,b,c,g,C, rx=0,ry=0,dh, ch,hc,cr,hh,D,dn,hmax,tt,onc,
      s="", nr=0, gg=[], cc=[], bkc=[], Scell=[];

  D=f_Dt1Dt2();
  Dt1=D.di, Dt2=D.df, nr=D.nr;
  
      // Total Height
      hh=scH-nr*mg;
      cr=hh%nr;       
 
  dn=moGlb.cloneArray(Dt1);
  ROW=[]; 

  CELLTMS=[];
  hmax=0; tt=0;
  for(;;){
    CELLTMS.push(dn);
    gg.push(dn.DD); 
    cc.push( ((dn.MM!=Day.MM)?"#AAA":"#000") ); 
    if(dn.DD==NOW.DD && dn.MM==NOW.MM && dn.YY==NOW.YY) bkc.push("#E2E2E2"); else bkc.push(bkcell); 
    
      C=wrapMCell(dn,COL[rx].w);    // inside Month cell
      hc=C.h; if(!hc) hc=celm;
      Scell.push(C.s);
    
    if(hc>hmax) hmax=hc;             // hmax row

    rx++; if(rx>6) {  
      rx=0,ry++; if(ry>nr) break;     // change row / exit
    
      ROW.push( { t:tt, h:hmax } );
      tt+=hmax+mg; 
      hmax=0;  
    }   
      
    dn=f_inc(1,0,0,dn);
  }
  
  // adjust height
  sch=tt; 
  if(tt<hh){  
    dh=hh-tt+mg*nr;
    ch=parseInt(dh/nr), cr=dh%nr;
    tt=0;
    for(r=0;r<nr;r++){   
      dh=(r==(nr-1))?(ch+cr):ch;  
      ROW[r].h+=dh;
      ROW[r].t+=tt;  
      tt+=dh;   
    }
    sch=scH; 
  }

  rx=0,ry=0;
  for(i=0;;i++){  g=gg[i], c=cc[i];
    onc="onclick='moRese.f_cellClick(event,"+i+",-1)'";
    s+="<div id='idcell_"+i+"' class='cl_cell' "+onc+" "+S+"left:"+(COL[rx].l)+"px;top:"+ROW[ry].t+"px;width:"+COL[rx].w+"px;height:"+ROW[ry].h+"px;background-color:"+bkc[i]+";'>"+
       "<div "+S+"left:3px;top:3px;font:"+fnt+";color:"+c+";'>"+g+"</div>"+
       Scell[i]+
       "<div id='idovrcell_"+i+"' "+S+"left:2px;top:28px;right:2px;height:44px;border:2px solid #666;box-sizing:border-box;display:none;'></div>"+
       "</div>";
    
    rx++; if(rx>6) rx=0,ry++; 
    if(ry>=nr) break;  
  }

  return s;
}},




wrapMCell:function(d,w){   // d=date
with(moRese){

  var r, p, t, f, G, s="", h=0, z,
      P=CNFG.ptr, R=CNFG.ref;
  
  p=""+d.YY+f_zero(d.MM)+f_zero(d.DD);

  if(typeof(P[p])!="undefined"){  
    t=28;
    
  //  alert(P[p].length+" \ "+p)
    
    for(r=0;r<P[p].length;r++) {
      f=P[p][r];
      
      if(NORANGE && f[1]) continue; 
      z=R[f[0]];

      if(MY && USID!=z.user_id) continue;
  
      G=f_BoxWO(t,w, z, f[1]);
      t+=G.h; 
      s+=G.s;
      t+=2;
    } 
    h=t;   
  }
     
  return { h:h, s:s };
}},



f_BoxWO:function(t,w,d,x){   // d=data fcode     x = n. box >0  70% color
with(moRese){             

  var c,nf="",s,g=2,k,onm;
  h=44;

  c=(d.user_id==USID)?COLORMY:COLORBOX; 
        
  if(x) c=colorTone(c,0.35);
  else {
     k=58412;
     nf="<span "+S+"left:4px;top:9px;font:16px FlaticonsStroke;color:#FFF;overflow:hidden;'>"+String.fromCharCode(k)+"</span>";
     
   if(w>72){  
     nf+="<div "+S+"left:27px;top:2px;right:4px;height:20px;font:11px opensansR;color:#FFF;overflow:hidden;'>"+f_zero(d.progress,4)+" - <span style='font-size:12px;'>"+d.title+"</span></div>";
     nf+="<div "+S+"left:27px;top:21px;right:2px;height:18px;font:12px opensansR;color:#FFF;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;'>"+d.asset+"</div>";
   }
  }

  onm="onmouseover='moRese.f_ovrout(1,"+d.fcode+")' onmouseout='moRese.f_ovrout(0,"+d.fcode+")' onclick='moRese.f_cellClick(event,"+d.fcode+")' ";
  
  s="<div id='id_"+d.fcode+"_"+x+"' "+S+"left:"+g+"px;top:"+t+"px;width:"+(w-g*2)+"px;height:"+h+"px;border-radius:2px;border:2px solid "+c+
    ";box-sizing:border-box;background-color:"+c+
    ";overflow:hidden;cursor:pointer;' title='nr. "+f_zero(d.progress,4)+" - "+d.title+"\nasset: "+d.asset+"' "+onm+">"+nf+"</div>";

  return { h:h, s:s };
}},


f_ovrout:function(m,f){
with(moRese){

 var r,j,d, n=CNFG.ifc[f];
 
 for(r=0;r<n;r++){
   j=mf$("id_"+f+"_"+r); 
   if(!j) continue;  
   d=j.style.backgroundColor; c=d;
   if(m) c="rgba(0,0,0,"+(MW?"0.7":"0.4")+")"; 
   j.style.borderColor=c;
   if(NORANGE) break;
 }
 
 if(NORANGE) viewRange(m,f,j,n,d);
}},



viewRange:function(m,f,j,n,c){
with(moRese){

  var r,d=colorTone(c,0.35), pj=j.parentNode, i=parseInt((pj.id.split("_")).pop());  
  for(r=1;r<n;r++){
    i++;
    fj=mf$("idovrcell_"+i);
    if(!fj) continue;
    with(fj.style){
      backgroundColor=d;
      opacity="0.7";
      display=m?"block":"none";  
    }  
  }

}},




f_Dt1Dt2:function(){
with(moRese){
  var nr=0,df,di,dn,w;
  df=new Date(Day.YY,Day.MM,1,0,0,0,0);     // first day of month
  w=df.getDay();  
  if(!w) w=6; else w--;                     
     
  di=f_inc(-w,0,0,f_now(df));               // first day/row
  df=f_inc(6,0,0,di);

  for(;;){ 
    nr++;
    if(df.MM!=Day.MM) { 
      dn=f_inc(-6,0,0,df); 
      if(dn.MM!=Day.MM) { nr--; df=f_inc(-7,0,0,df); }
      break;
    }
    df=f_inc(7,0,0,df);
  } 
  
  return { di:di, df:df, nr:nr };
}},


 

DrawHead:function(){
with(moRese){
  var sh,sb, E=f_head_label(), 
      r=5, a="right", hb=33, 
      sw=Btw[0]+Btw[1]+Btw[2]+47;

  
  sh="<div class='cl_btn' "+S+"left:0;top:0;width:27px;font:16px FlaticonsStroke;' onclick='moRese.f_next_prev(-1)'><span "+S+"left:5px;top:3px;'>"+String.fromCharCode(58814)+"</span></div>"+
     "<div class='cl_btn' "+S+"left:32px;top:0;width:27px;font:16px FlaticonsStroke;' onclick='moRese.f_next_prev(1)'><span "+S+"left:5px;top:3px;'>"+String.fromCharCode(58815)+"</span></div>"+
    
     "<div id='id_head_label' "+S+"left:64px;top:2px;right:"+r+"px;font:"+fnth+";text-align:"+a+";cursor:default;' title=''>"+E+"</div>"+
    
     "<div id='id_rbtn' "+S+"right:0;width:"+sw+"px;top:"+hb+"px;height:30px;'>"+
      "<div id='id_btn0' class='cl_btn_press' "+S+"right:0;top:0px;width:"+Btw[0]+"px;font:"+fnt+";' onclick='moRese.f_btn01(0)'><span "+S+"left:5px;top:5px;'>"+Lbw[0]+"</span></div>"+
      "<div id='id_btn1' class='cl_btn' "+S+"right:"+(Btw[0]+5)+"px;top:0px;width:"+Btw[1]+"px;font:"+fnt+";' onclick='moRese.f_btn01(1)'><span "+S+"left:5px;top:5px;'>"+Lbw[1]+"</span></div>"+
      "<div class='cl_btn' "+S+"right:"+(Btw[0]+Btw[1]+10)+"px;top:0px;width:"+Btw[2]+"px;font:"+fnt+";' onclick='moRese.f_menuFilter(this)'><span "+S+"left:5px;top:5px;'>"+Lbw[2]+"</span></div>"+
      "<div id='id_btn_update' class='cl_btn"+(UPDT?" cl_btn_red":"")+"' "+S+"right:"+(Btw[0]+Btw[1]+Btw[2]+15)+"px;top:0px;width:27px;font:16px FlaticonsStroke;' onclick='moRese.f_update(1)'><span "+S+"left:5px;top:3px;'>"+String.fromCharCode(58762)+"</span></div>"+
     "</div>";
 
  mf$("id_head").innerHTML=sh;
  JB.style.top=(hb+38)+"px";
  jhl=mf$("id_head_label");
}},





drawhMonth:function(){
with(moRese){  
  var bW=JB.offsetWidth, r,l,w, wk, s="", ww=bW-8*mg;

  LcolW=0,
  RcolW=ww%7,
  colW=parseInt(ww/7);
  wk=(colW<120)?sweek:aweek;  
  COL=[];

  for(r=0;r<7;r++){
   l=mg+(colW+mg)*r;
   w=colW+((r<6)?0:RcolW);
   
   COL.push({ l:l, w:w });
   
   s+="<div "+S+"left:"+l+"px;top:"+mg+"px;width:"+w+"px;height:28px;background-color:"+bkhcell+";overflow:hidden;text-align:center;'>"+
      "<div style='font:bold "+fnt+";color:#606060;margin:5px 2px 0 2px;'>"+LANG(wk[r+1])+"</div></div>"; 
  }
  jhl.title="";
 

  return s;
}},

 

                                     
f_head_label:function(){
with(moRese){

  var s=""
  if(MW) {
    var m1,g1,a1="", m2="",g2,a2, w=Day.W;
    
    if(!w) w=6; else w--;  // 0:mon - 6:sun 
    Dt1=f_inc(-w,0,0);
    Dt2=f_inc(6-w,0,0);                      
          
    m1=Dt1.MM, g1=Dt1.DD, g2=Dt2.DD, a2=Dt2.YY;
    if(a2!=Dt1.YY) a1=" "+Dt1.YY;
    if(m1!=Dt2.MM) {
      m2=LANG(smonth[Dt2.MM])+" ";      
      m1=LANG(smonth[m1]);
    } else m1=LANG(amonth[m1]);  
                              
    s=m1+" "+"<strong>"+g1+"</strong>"+a1+" - "+m2+"<strong>"+g2+"</strong>"+" "+a2;
    
  } else {
    s="<span style='cursor:pointer;text-decoration:underline;' onclick='moRese.f_htitle()'>"+LANG(amonth[Day.MM])+" "+Day.YY+"</span>"; 
    
  }  
    
  return s;
}},




 








//------------------------------------------------------------------------------------------- 
// week


cellWeek:function(){
with(moRese){

  var r,rx,ry,ch,cr,hh, bW=JB.offsetWidth, ww=bW-9*mg, 
      sh="",sl="",sr="",s,onc, 
      w,dl=0,dn,gm,wk, bkc, wwmin=64, nr=24, l=0;

  LcolW=(ww<380)?24:40,
  RcolW=0, colW=0; 
  scW=bW-LcolW-mg*3; 
  
  // Total Height  fit +1 / -1
   hh=scH-nr*mg;
   ch=parseInt(hh/nr), cr=hh%nr;
   if(fit<0) {
    if(ch<28) ch=28,cr=0; 
   }else{
    if(ch<96) ch=96,cr=0;
   }   
   celw=ch;
      
    ROW=[];
    for(r=0;r<nr;r++) {
      // if(r==(nr-1)) ch+=cr; 
      ROW.push( { t:sch, h:ch } );
      sch+=ch+mg; 
    }
  
  // hours  lcolw_bd
  for(ry=0;ry<nr;ry++){
    if(workhour.indexOf(ry)!=-1) bkc="#D2D2D2"; else  bkc=bkcell;
    sl+="<div "+S+"left:0;top:"+ROW[ry].t+"px;width:"+LcolW+"px;height:"+ROW[ry].h+"px;background-color:"+bkc+";'>"+
        "<div "+S+"left:0;right:0;height:1px;top:"+(parseInt(ROW[ry].h/2))+"px;overflow:hidden;background-color:#DDD;'></div>"+
        "<div "+S+"left:0;right:0;top:1px;height:20px;font:"+fnt+";color:#666;text-align:center;'>"+((ry<10)?("0"+ry):ry)+((LcolW<40)?"":":00")+"</div>"+
        "</div>";  
  }      
  
  w=Day.W;
  if(!w) w=6; else w--;                         
  Dt1=f_inc(-w,0,0,Day);            // first day/row     
  dn=moGlb.cloneArray(Dt1);

  // Cols width
  COL=[]; 
  for(r=0;r<7;r++){
   C=wrapWCell(dn);       // return width  & fcodes
   w=C.w;
   if(!w) w=wwmin;
   COL.push({ l:l, w:w, f:C.f, d:dn });
   l+=w+mg;
   dn=f_inc(1,0,0,dn);
  }

   l=scW-l+mg; 
   if(l>0){
    colW=parseInt(l/7);
    RcolW=l%7;
   }

  CELLTMS=[];
  dn=moGlb.cloneArray(Dt1);
  for(r=0;r<7;r++){
   CELLTMS.push(dn);
   COL[r].w+=colW+((r==6)?RcolW:0);
   COL[r].l+=dl;
   
   w=COL[r].w;
   l=COL[r].l;
   gm=(w>80)?" "+dn.DD+"/"+(dn.MM+1):"";
   wk=(w<160)?sweek:aweek;

   // header
   sh+="<div "+S+"left:"+l+"px;top:0;width:"+w+"px;height:28px;background-color:"+bkhcell+";overflow:hidden;text-align:center;'>"+
       "<div style='font:bold "+fnt+";color:#606060;margin:5px 2px 0 2px;'>"+LANG(wk[r+1])+gm+"</div></div>";  
   
    dn=f_inc(1,0,0,dn);
    dl+=colW;
  }
 
  dn=moGlb.cloneArray(Dt1);
  
  //cells
  for(ry=0;ry<nr;ry++){ 
  
   for(rx=0;rx<7;rx++){ 
   
      if(dn.DD==NOW.DD && dn.MM==NOW.MM && dn.YY==NOW.YY) bkc="#E2E2E2"; else bkc=bkcell;
      if(workhour.indexOf(ry)!=-1) bkc="#E8E8E8"; 
      onc="onclick='moRese.f_cellClick(event,"+rx+","+ry+")'"; 
      sr+="<div class='cl_cell' "+onc+" "+S+"left:"+COL[rx].l+"px;top:"+ROW[ry].t+"px;width:"+COL[rx].w+"px;height:"+ROW[ry].h+"px;background-color:"+bkc+";'>"+
         "<div "+S+"left:0;right:0;height:1px;top:"+(parseInt(ROW[ry].h/2))+"px;overflow:hidden;background-color:#DDD;'></div>"+
         "</div>";
  
      dn=f_inc(1,0,0,dn);
   } 
 
    dn=f_inc(-7,0,0,dn);  
  } 

  if(cr) {
    s="<div "+S+"left:0;bottom:0;right:0;height:"+(cr-mg)+"px;background-color:"+bkcell+";overflow:hidden;'></div>"; 
    sr+=s;
    sl+=s;
  }
  
  
   //.... draw column COL[ry].f   
  for(rx=0;rx<7;rx++){    
    if(COL[rx].f.length) sr+=f_WBoxWO(COL[rx]);             
  } 
  
  jhl.title=LANG("Week")+" n. "+f_getWeek(Dt1); 
  jhl.style.cursor="default";
  
  if(fit>0){                        // Vscrollbar at 8:00 AM
    sct=ROW[8].t;
    if( (sct+scH)>sch ) sct=0;
  }else{
    sct=0; 
  }  
                 
  scw=COL[6].l+COL[6].w;     // Oscroll width
 
  return { rcolw_hd:sh, lcolw_bd:sl, rcolw_bd:sr }

}},





wrapWCell:function(d){  
with(moRese){
   var fc=[], w=0, r,f,p,n,dd, minWw=80,
       P=CNFG.ptr, R=CNFG.ref;
   p=""+d.YY+f_zero(d.MM)+f_zero(d.DD);
   
   if(typeof(P[p])!="undefined"){ 
 
    for(r=0;r<P[p].length;r++) {
      f=P[p][r];
      if(NORANGE && f[1]) continue; 
      dd=R[f[0]];   
   
      fc.push(f);
   }
    n=fc.length;
    w=(n*minWw+2)-2+wmg*2;     
  } 
   return { w:w, f:fc }
}},




f_WBoxWO:function(C){  
with(moRese){

  var r,k,s="", di,df,tdi,tdf,R, d,si,sf,tw,ss,tl="",tdmt,onm,x,
      n,l,w,rw,t,h, c,nf, dfo,dto; 
  R=CNFG.ref;

  di=C.d; di.hh=0,di.mm=0,di.ss=0
  df={ YY:di.YY, MM:di.MM, DD:di.DD, hh:23, mm:59, ss:59 }; 
  di=date_to_tms(di);
  df=date_to_tms(df);
  
  n=C.f.length;
  tw=C.w-(2*(n-1))-wmg*2;
  w=parseInt(tw/n);
 
  if(w>128) {
    w=128;
    l=C.l+parseInt( (C.w - (w*n+2*(n-1)) )/2 );
  }else{
    l=C.l+wmg;
  }
  
  for(r=0;r<n;r++){
   fc=C.f[r][0]; x=C.f[r][1];
   d=R[fc];
              
   if(MY && USID!=d.user_id) continue;           
                                                                
   si=(d.from_date>di)?d.from_date:di;
   sf=(d.to_date<df)?d.to_date:df; 
  
   si=f_now(si);
   sf=f_now(sf);
   
   t=ROW[ si.hh ].t + parseInt(si.mm/60*celw);
   h=ROW[ sf.hh ].t-t + parseInt(sf.mm/60*celw);
   if(t<2) t=2,h-=2;

   if(h<20) h=20;
  
   c=(d.user_id==USID)?COLORMY:COLORBOX; 
   
   if(x) { 
    c=colorTone(c,0.35);
    ss="";
   }else {
       k=58412;
       ss="<span "+S+"left:4px;top:9px;font:16px FlaticonsStroke;color:#FFF;overflow:hidden;'>"+String.fromCharCode(k)+"</span>";
       
     if(w>72){  
       ss+="<div "+S+"left:27px;top:2px;right:4px;height:20px;font:11px opensansR;color:#FFF;overflow:hidden;'>"+f_zero(d.progress,4)+" - <span style='font-size:12px;'>"+d.title+"</span></div>";
       ss+="<div "+S+"left:27px;top:21px;right:2px;font:12px opensansR;color:#FFF;overflow:hidden;text-overflow:ellipsis;'>"+d.asset+"</div>";
     }
   }
   
   dfo=f_now(d.from_date), dto=f_now(d.to_date);
   tdmt="\nfrom: "+dfo.DD+"/"+(dfo.MM+1)+"/"+dfo.YY+" - "+f_zero(dfo.hh)+":"+f_zero(dfo.mm)+"\nto: "+dto.DD+"/"+(dto.MM+1)+"/"+dto.YY+" - "+f_zero(dto.hh)+":"+f_zero(dto.mm);
   tl=f_zero(d.progress,4)+" - "+d.title+"\n"+d.asset+tdmt;
   
   onm="onclick='moRese.f_cellClick(event,"+d.fcode+")' onmouseover='moRese.f_ovrout(1,"+d.fcode+")' onmouseout='moRese.f_ovrout(0,"+d.fcode+")' ";    
   
   s+="<div id='id_"+d.fcode+"_"+x+"' "+S+"left:"+l+"px;top:"+t+"px;width:"+w+"px;height:"+h+"px;border-radius:2px;border:2px solid "+c+
      ";box-sizing:border-box;background-color:"+c+";opacity:0.8;overflow:hidden;' title='"+tl+"' "+onm+">"+ss+"</div>";
   
   l+=w+2;
  }
  

 return s;
}},



fitWexpand:function(){
with(moRese){
  fit*=-1;
  Resize();
}},




f_WorkRange:function(){
with(moRese){
   
   
}},




//------------------------------------------------------------------------------
// events


keyDown:function(e){ if(!e) e = window.event; 
with(moRese){
   
  if(e.ctrlKey) CTRL=1; 
  MDRJ.style.display="block"; 
}},

keyUp:function(e){ if(!e) e = window.event;
with(moRese){     
  if(e.ctrlKey) CTRL=0; 
  MDRJ.style.display="none"; 
}},



f_panDown:function(e){ 
with(moRese){
  var P=moEvtMng.getMouseXY(e);
  odrg.ix=P.x, odrg.iy=P.y,
  odrg.x=P.x, odrg.y=P.y,
  odrg.mv=1;   
  odrg.vy=VscrCal?VscrCal.ptr:0; 
  odrg.vx=OscrCal?OscrCal.ptr:0;
    
  moEvtMng.cancelEvent(e);
}},

f_panMove:function(e){ 
with(moRese){
  if(odrg.mv){
    var P=moEvtMng.getMouseXY(e);
    with(odrg){
      x=P.x-ix, y=P.y-iy;  
      if(VscrCal) VscrCal.PosPtr(vy-y);
      if(OscrCal) OscrCal.PosPtr(vx-x);  
  }}
  moEvtMng.cancelEvent(e);
}},

f_panUp:function(e){ 
  moRese.odrg.mv=0;
  moEvtMng.cancelEvent(e);
},




f_btn01:function(i){   // switch month / week
with(moRese){
  if(MW==i || MTH) return false;
  mf$("id_btn"+MW).className="cl_btn";
  MW=i;
  mf$("id_btn"+MW).className="cl_btn_press";
  
  if(NOW.MM==Day.MM && NOW.YY==Day.YY) {  // if month == now  go in week of now
    Day.DD=NOW.DD; 
    Day.W=NOW.W;
  }
  
  JB.style.backgroundColor=MW?"#888":"#AAA";
  
  jhl.innerHTML=f_head_label();

  Resize();
  noFocus();
}},


f_next_prev:function(i){   // i: +1 -1
with(moRese){
  if(MTH) return false;
  var d,m;
  
  if(MW) { 
    d=7*i, m=0;  
  } else {
   Day.DD=1;
   d=0, m=1*i;
  } 
  
  Day=f_inc(d,m,0);
  jhl.innerHTML=f_head_label();
  noFocus();
  
  f_update(0); 
}},


f_menuFilter:function(fj){
with(moRese){
   if(MTH) return false;
   var p=moGlb.getPosition(fj);
   moMenu.Start(menufb,{l:p.l, t:p.t, w:p.w, h:p.h},2);
   noFocus();
}},




Vbarpos:function(i){
with(moRese){

  if(!jsc) return;
  jsc.style.top=-i+"px";
  if(MW) jscl.style.top=-i+"px";
}},

Obarpos:function(i){
with(moRese){
  if(!jsc) return;
  jsc.style.left=-i+"px";
  jsch.style.left=-i+"px";
}},






f_htitle:function(i){
with(moRese){
 
   if(MW) return false;
   if(MTH) { f_htitle_close(); return false; }
   MTH=1;
   var j,jm,ww,hh,r,lr,llr,cgx,cgy,rcgx,mh,x,y,rx,ty,i=0,
       s="", g=5;
   
   ww=JB.offsetWidth, hh=JB.offsetHeight;
 
   cgx=parseInt(ww/3-g); rcgx=0;
   if(cgx>260) { cgx=260; rcgx=parseInt((ww-((cgx+g)*3))/2);  }
   
   cgy=parseInt((hh-g*6)/5);
   if(cgy>100) cgy=100;
   
   Hbt=cgy*5;
   jm=mf$("id_months")
   jm.style.top=(-Hbt)+"px";
   jm.style.height=Hbt+"px";
   
   
   mh=(cgx<120)?smonth:amonth;
   ty=(cgy-22)/2;
   
   lr=parseInt(cgx-g)/2; // (cgx&1)
   x=rcgx, y=0;
   s+=f_mbt("f_today()",x,y,lr,cgy,ty+2,"<span style='font:"+fnt+";'>"+LANG("Today")+"</span>");
   
   x+=g+lr;  llr=lr+((cgx&1)?1:0);
   s+=f_mbt("f_chg_year(-1)",x,y,llr,cgy,ty-6,"<span style='font:24px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58802)+"</span>");
   
   x+=g+llr;
   s+=f_mbt("",x,y,cgx,cgy,ty-4,"<span id='idyearmonth' style='font:bold 22px opensansR;color:#000;'>"+Day.YY+"</span>");     // year
   
   x+=g+cgx;
   s+=f_mbt("f_chg_year(1)",x,y,llr,cgy,ty-6,"<span style='font:24px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58803)+"</span>");
   
   x+=g+llr;
   s+=f_mbt("f_htitle_close()",x,y,lr,cgy,ty-6,"<span style='font:24px FlaticonsStroke;color:#888;'>"+String.fromCharCode(58827)+"</span>",1);
   
   // btn
   for(r=1;r<5;r++){   
    y=r*(g+cgy);
    for(rx=0;rx<3;rx++){ 
      x= rcgx+rx*(g+cgx);  
      s+=f_mbt("f_set_monthY("+i+")",x,y,cgx,cgy,ty,mh[i],(rx==2)?1:0);
      i++;
    }
   }

   jm.innerHTML=s;
   j=mf$("id_months_mask");
   j.style.opacity="0.1";
   j.style.display="block";
   mf$("id_months_cont").style.display="block";
   
   f_anim(-Hbt,8,0.1);
   VscrCal.Display(0);
}},


f_anim:function(hh,k,o){
  hh+=k;  
  o+=0.06;if(o>0.9) o=0.9;
  if(hh>0) hh=0,o=0.9;  
  mf$("id_months_mask").style.opacity=o;
  mf$("id_months").style.top=hh+"px";
  if(!hh) return;
  k+=k/4;
setTimeout(function(){ moRese.f_anim(hh,k,o); },40);
},



f_mbt:function(fnc,l,t,w,h,dt,txt){
with(moRese){
 if(fnc) fnc="onclick='moRese."+fnc+";' class='cl_mbtn' "; else fnc="class='cl_nombtn'";
 return "<div "+S+"left:"+l+"px;top:"+t+"px;width:"+w+"px;height:"+h+"px;' "+fnc+">"+
        "<div "+S+"left:0;top:"+dt+"px;width:100%;font:"+fnth+";text-align:center;color:#000;'>"+txt+"</div></div>";
}},


f_chg_year:function(i){
   moRese.noFocus();
   var v=parseInt(mf$("idyearmonth").innerHTML)
   v+=i;
   mf$("idyearmonth").innerHTML=v; 
   return false;  
},

f_set_monthY:function(m){
with(moRese){
    var y=parseInt(mf$("idyearmonth").innerHTML);  
    Day.YY=y, Day.MM=m; 
    jhl.innerHTML=f_head_label();
    RESZ=1;
    f_htitle_close();
}},

f_today:function(m){
with(moRese){
    Day.YY=NOW.YY, Day.MM=NOW.MM; 
    jhl.innerHTML=f_head_label();
    RESZ=1;
    f_htitle_close();
}},


f_htitle_close:function(h,k,o){
with(moRese){   
   if(!h) h=0,k=8,o=0.9;  
   h-=k; o-=0.1;
   if(h<-Hbt){
    mf$("id_months_mask").style.display="none";
    mf$("id_months_cont").style.display="none";
    MTH=0;
    VscrCal.Display(1);
    if(RESZ) f_update(0); 
    return;
   }
   mf$("id_months_mask").style.opacity=o;
   mf$("id_months").style.top=h+"px";
   k+=k/4;
setTimeout(function(){ moRese.f_htitle_close(h,k,o); },40);
}},


chgNORANGE:function(){
with(moRese){ 

  NORANGE=+!NORANGE;
  Resize();

}},




// popup  new | edit             EditVal={};
f_cellClick:function(e,i,y){   // y=undefined => new|edit    reservation i = asset code
with(moRese){

   //alert(e+" | "+i+" | "+y)

  if(e) moEvtMng.cancelEvent(e);
  
  var w,h,d,s,tm,dt,tit,md;
   
  if(i<0 && y<0){  // new from menu
  
      tm=f_click_now();
      
  }else {   // new | edit

    if(typeof(y)!="undefined"){   // new
   
         tm=0, dt=CELLTMS[i];
         if(f_lessnow(dt)) {
           tm=f_click_now(); 
         } else { 
           if(y<0) { d=f_now(), y=d.hh; if(d.mm>30 && y<23) y++;  }
           dt.hh=y, dt.mm=0; 
           tm=date_to_tms(dt);
         }
   
         tit="New reservation";
         md=0;
      
    } else {
    
      var rw=CNFG.ref[i], ttt,ttf, Dtf,Dtt, dtf=f_now(rw.from_date), dtt=f_now(rw.to_date);
  
      Dtf=day_to_tms(dtf);
      Dtt=day_to_tms(dtt);
      
      ttf=dtf.hh*2+((dtf.mm>15)?1:0);
      ttt=dtt.hh*2+((dtt.mm>15)?1:0);  
      
      if(Dtt==Dtf) Dtt=0;
   
        EditVal={
            f_code:rw.fcode,
            title:rw.title,
            description:rw.description,
            asset:-1,
            asset_code:rw.code_asset,       
            asset_title:rw.asset,
            datefrom:Dtf,
            dateto:Dtt,
            
            timefrom:ttf,    
            timeto:ttt,
            c_type:0,         
            c_freq:1,
            c_step:1,
            c_seq:0,
            
            e_text0:"",
            e_ck0:0,
            e_text1:"",
            e_ck1:1,
            
            l_tablist:[],
            f_reservation_id:rw.reservation_id,
            userid:rw.user_id
        };
       
        tit="Edit reservation";
        md=1;
    }  
  }  
   
   w=(W>506)?506:266;
   h=(H>540)?540:(H-40);   // 48
   
   s="<iframe src='reservation_form.html?"+tm+"|"+md+"' "+S+"left:0;top:0;width:"+(w-16)+"px;height:"+(h-56)+"px;background-color:#FFF;' frameborder=0></iframe>";
   
   f_open_popup(w,h,tit,s);
}},



f_click_now:function(){
with(moRese){
    var dt=f_now(), y=dt.hh; if(dt.mm>30 && y<23) y++;  
    dt.hh=y, dt.mm=0; dt.ss=0;
    return date_to_tms(dt);
}},



// popup  edit

f_window:function(wnd){
  moRese.WND=wnd;
},
 

f_open_popup:function(w,h,bt,s){
with(moRese){
   
   with(JM.style) opacity="0.8", backgroundColor="#4A6671", display="block";  
   with(JP.style){
    if(w){
      width=w+"px";
      height=h+"px";
      marginLeft=(-w/2)+"px";
      marginTop=(-h/2)+"px";
      if(!bt) bt="";
      mf$("id_bartitle_popup").innerHTML=LANG(bt); 
      if(!s) s="";
      mf$("id_body_popup").innerHTML=s;
    } 
    VscrCal.Display(0);
    display="block";
   }
   
}},


f_close_popup:function(){
with(moRese){
   VscrCal.Display(1);
   JP.style.display="none";
   with(JM.style) opacity="0.2", backgroundColor="#FFF", display="none";
   mf$("id_mask_popup").style.display="none";
}},




/*
receive:
{ f_code:fcode,
  title:"",
  description:"",
  asset:-1,        // SA[0]
  datefrom:DTI,
  dateto:0,
    
  timefrom:tti,    // select  SA[] 1,2,3
  timeto:ttt,
  
  c_type:0,   
  c_freq:1,
  c_step:1,
  c_seq:0,
    
  e_text0:"",
  e_ck0:0,
  e_text1:"",
  e_ck1:1 
  
  userid:1
  f_reservation_id:reservation_id,
  l_tablist:[
  
    [date_from, date_to, status],
    ...
  
  ]
}

*/

f_get_new_reservation:function(vl,md){  //  md: 0=new   1=edit
with(moRese){

  var vs,val=moGlb.cloneArray(vl);

  val["f_reservation_fieldset"]=[
    val.c_type,
    val.c_freq,
    val.c_step,
    val.c_seq,
    val.e_text0,
    val.e_ck0,
    val.e_text1,
    val.e_ck1  
  ];

  val.datefrom += val.timefrom*1800; 
  vs=JSON.stringify(val);   
  
  
  // alert( vs ); return;
  
  Ajax.sendAjaxPost(CallSave,"mode="+md+"&val="+vs,"moRese.retCallSave");      
}},


retCallSave:function(a){   
with(moRese){

 try{ var jsn=JSON.parse(a); }catch(e){ alert(a); return; } 
 
 if(!jsn.result){ // table list
  if(jsn.tablist){
            
    if(!jsn.tablist.length) {  // close form  not exist tablist
      f_close_form();
    } else {
    
        
      WND.resForm.f_ShowFieldsetList(jsn.tablist);
    }  
    
   
  }else{
  
    alert("save error!");
  
  }
    
 } else {  // close form
      
    f_close_form();
 }


}},



f_close_form:function(){   
with(moRese){
  WND.resForm.f_close();
  f_update(1);
}},



// functions

f_inc:function(d,m,y,dd){    // dd = { YY, MM, DD, hh, mm, ss, W  }
with(moRese){
  if(!dd) dd=Day;
  var D=dd.DD+d,
      M=dd.MM+m,
      Y=dd.YY+y,
  dt=new Date(Y,M,D,dd.hh,dd.mm,dd.ss);   
  return f_now(dt);
}},



f_now:function(dt){ 
  var d;
  if(!dt) d=new Date();                              // now
  else {
    if(typeof(dt)=="number") d=new Date(dt*1000);  // from timestamp
    else d=dt;                                       // drom Date
  }      
  var h=d.getHours(),
  m=d.getMinutes(),
  s=d.getSeconds(), 
  D=d.getDate(),
  M=parseInt(d.getMonth()),
  Y=d.getFullYear(),
  W=d.getDay();           // day of week

  return { DD:D, MM:M, YY:Y, hh:h, mm:m, ss:s, W:W };
},


f_getWeek:function(dt){  
  var date=new Date(dt.YY,dt.MM,dt.DD,0,0,0,0);
  date.setDate(dt.DD+3-(date.getDay()+6)%7); 
  var week1=new Date(date.getFullYear(),0,4); 
  return 1+Math.round(((date.getTime()-week1.getTime())/86400000-3+(week1.getDay()+6)%7)/7); 
},

noFocus:function(){
  if(!isTouch) setTimeout(function(){ mf$('idinputnofocus').focus(); },200);
  return false;
},


date_to_tms:function(d){
  var dta=new Date(d.YY,d.MM,d.DD,d.hh,d.mm,d.ss);
  return parseInt((dta.getTime())/1000);
},


day_to_tms:function(d){
  var dta=new Date(d.YY,d.MM,d.DD,0,0,0);
  return parseInt((dta.getTime())/1000);
},


f_zero:function(v,n){ if(!n) n=2;
  var r,s=v+"", i=s.length;
  for(r=i;r<n;r++) s="0"+s;
  return s;
},



f_lessnow:function(dt){
  var t,d, now=parseInt(((new Date()).getTime())/1000);
  d=new Date(dt.YY, dt.MM, dt.DD, dt.hh, dt.mm, dt.ss, 0);
  t=parseInt((d.getTime())/1000);
  if(t>now) return false; 
  return true;
},



/*
 c = colore in formato #000   o   #000000   o rgb(r,g,b)
 t = tonalità    0 <= t < 1
 m = formato  0=#000000  1=rgb(00,00,00)
 a = alpha    0<a<1   return rgba()
*/
colorTone:function(c,t,m,a){ 
  if(t<0 || t>=1) return c;
    if(!m) m=0; else m=1;
    
    var r,v,f=c.split("(");
    if(f.length>1){
            f=f[1].substring(0,f[1].length-1);
            f=f.split(",");
            c="#"; 
            for(r=0;r<3;r++) {
              v=(parseInt(f[r])).toString(16);
              c+=moRese.f_zero(v); 
            } 
    }
    
    var n=c.length, d=(n>4)?2:1, dd=0, s=[], v="", v0, v1;
      for(r=1;r<n;r++) { v+=c.charAt(r);
      dd++; if(dd==d) { v0= parseInt((d>1)?v:(v+v), 16);
               v1=parseInt(  v0+((255-v0)*(1-t)) ); if(!m) v1=v1.toString(16);
               if(v1.length<2) v1="0"+v1;
               s.push(v1); v=""; dd=0; } }
    if(m) { v0="rgb"+(a?"a":"")+"("+s.join(",")+(a?(","+a):"")+")"; }
    else v0="#"+s.join("");
  return v0;
}, 

colorToneDark:function(c,t,m,a){ 
  if(t<0 || t>=1) return c;
    if(!m) m=0; else m=1;
    var r,n=c.length, d=(n>4)?2:1, dd=0, s=[], v="", v0, v1;
      for(r=1;r<n;r++) { v+=c.charAt(r);
      dd++; if(dd==d) { v0= parseInt((d>1)?v:(v+v), 16);
               v1=parseInt(v0*(1-t)); if(!m) v1=v1.toString(16);
               if(v1.length<2) v1="0"+v1;
               s.push(v1); v=""; dd=0; } }
    if(m) { v0="rgb"+(a?"a":"")+"("+s.join(",")+(a?(","+a):"")+")"; }
    else v0="#"+s.join("");
  return v0;
} 


} //      end






function LANG(w){
  var r=w, b=0;
  try{ r=parent.moGlb.langTranslate(w); }catch(e){ b=1; }    // desk
  if(b) try{ r=parent.LANG(w); }catch(e){}                   // mobile
  return r;
}







// debug
//mf$("debug").innerHTML+=hh+"<br>";
  
  