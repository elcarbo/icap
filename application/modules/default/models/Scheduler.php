<?php

class Mainsim_Model_Scheduler
{
    private $db;

    protected $asset_code = null;

    protected static $days_of_the_week = [
        1 => 'monday',
        2 => 'tuesday',
        3 => 'wednesday',
        4 => 'thursday',
        5 => 'friday',
        6 => 'saturday',
        7 => 'sunday'
    ];

    protected
      $periodic_rows = [],
      $periodic_table_schema = [
        'f_code',
        'f_forewarning',
        'f_start_date',
        'f_end_date',
        'f_timestamp',
        'f_mode',
        'f_available_type',
        'f_executed',
        'f_duration',
        'f_asset_code'
      ];
    
    public function __construct($db = null) {                
        $this->db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
		$this->timestamp=time();
    }

    public function getAssetCode()
    {
        return $this->asset_code;
    }

    public function setAssetCode($asset_code)
    {
        $this->asset_code = $asset_code;
    }
    
    /**
     *Create weekly schedule
     * @param type $periodic
     * @param type $f_code_wo 
     */
    public function weekly($periodic,$f_code_wo)
    {
        $increment = strtotime('monday midnight', $periodic['fc_pm_start_date']);
        $end_date = $periodic['fc_pm_end_date'];
        $count = 0;
        //get sequence of DoW
        $days_otw = explode(',',str_replace(".", ",", $periodic['fc_pm_sequence']));
        //get week cyclic
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic = $periodic['fc_pm_cyclic_type'];
        $now = time();
        //while till the end_date 
        while($increment <= $end_date) {
            if($count > 5000) {
                $this->db->delete("t_periodics","f_executed = 0 and f_code = ".$f_code_wo);
                return false;
            }                        
            foreach($days_otw as $dow) {
                $dom_ts = strtotime(self::$days_of_the_week[$dow], $increment);
                //COMMENTATO DA BALDINI IL 8/10/17. Allineato il comportaento alle "normali" periodic
                if($dom_ts < $periodic['fc_pm_start_date'])
                    continue;

                //if last periodic is over the end date, continue
                if(($dom_ts + $periodic['fc_pm_at_time']) > $end_date)
                {
                    break;
                }

                $this->createPeriodics($periodic, $f_code_wo,($dom_ts + $periodic['fc_pm_at_time']),($dom_ts + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']));
                $count++;
            }            
            $increment = $this->increment($type_cyclic, $increment,$num_cyclic);
        }
        $this->savePeriodics();
        return true;
    }
    
    
    public function monthly($periodic,$f_code_wo)
    {
        $increment = strtotime('midnight', $periodic['fc_pm_start_date']);
        $day_otm = date('d', $increment);
        $end_date = $periodic['fc_pm_end_date'];
        $months = explode(',',str_replace(".", ",", $periodic['fc_pm_sequence']));
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic =$periodic['fc_pm_cyclic_type'];        
        while(date('Y',$increment) <= date('Y',$end_date)) {
            foreach($months as $month) {
                $dom_ts = strtotime(sprintf('%s/%s',$month,$day_otm), $increment);
                //COMMENTATO DA BALDINI IL 8/10/17. Allineato il comportaento alle "normali" periodic
                if($dom_ts < $periodic['fc_pm_start_date'])
                    continue;
                //if last periodic is over the end date, continue
                if(($dom_ts + $periodic['fc_pm_at_time']) > $end_date)
                {
                    break;
                }
                $this->createPeriodics($periodic, $f_code_wo, $dom_ts + $periodic['fc_pm_at_time'], $dom_ts + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);
            }            
            $increment = $this->increment($type_cyclic, $increment,$num_cyclic);                 
        }
        $this->savePeriodics();
        return true;
    }    
    /**
     * Periodic generatore with date of the month
     * @param type $periodic
     * @param type $f_code_wo
     * @return boolean 
     */
    public function dayofthemonth($periodic,$f_code_wo)
    {
        $increment = strtotime('midnight',$periodic['fc_pm_start_date']);
        $end_date = $periodic['fc_pm_end_date'];
        $dom_array =  explode(',',str_replace('.', ',', $periodic['fc_pm_sequence']));

        $count = 0;
        $now = time();
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic = $periodic['fc_pm_cyclic_type'];
        while($increment <= $end_date) {
            if($count > 5000) {
                $this->db->delete("t_periodics","f_executed = 0 and f_code = ".$f_code_wo);
                return false;
            }
            //filter day of the month
            $month = date('m', $increment);
            foreach($dom_array as $dom) {
                $dom_ts = strtotime(sprintf('%s/%s',$month, $dom), $increment);
                //COMMENTATO DA BALDINI IL 8/10/17. Allineato il comportaento alle "normali" periodic
                if($dom_ts < $periodic['fc_pm_start_date'])
                {
                    continue;
                }
                //if last periodic is over the end date, continue
                if(($dom_ts + $periodic['fc_pm_at_time']) > $end_date) {
                    break;
                }

                $this->createPeriodics($periodic, $f_code_wo, $dom_ts + $periodic['fc_pm_at_time'], $dom_ts + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);
                $count++;
            }
            $increment = $this->increment($type_cyclic, $increment,$num_cyclic);
        }
        $this->savePeriodics();
        return true;
    }  
    
    public function ontimescheduled($periodic,$f_code_wo)
    {
        $increment = strtotime('midnight', $periodic['fc_pm_start_date']);
        //$increment = $periodic['fc_pm_sequence'];
        $end_date = $periodic['fc_pm_end_date'];        
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic = $periodic['fc_pm_cyclic_type'];
        $count = 0;
        while($increment <= $end_date) {
            if($count > 5000) {
                $this->db->delete("t_periodics","f_executed = 0 and f_code = ".$f_code_wo);
                return false;
            }                        

            //if last periodic is over the end date, stop generation
            if(($increment + $periodic['fc_pm_at_time']) > $end_date)
            {
                break;
            }

            $this->createPeriodics($periodic, $f_code_wo, $increment  + $periodic['fc_pm_at_time'], $increment + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);
            $count++;

            $increment = $this->increment($type_cyclic, $increment,$num_cyclic);

            if(!$increment)
            {
                break;
            }
        }

        $this->savePeriodics();
        return true;
    }
    
    public function periodic($periodic,$f_code_wo, $floating_shift = false)
    {     //Maoli ciclati sto cazzo   
        $increment = strtotime('midnight', $periodic['fc_pm_start_date']);
        $end_date = $periodic['fc_pm_end_date'];        
        $num_cyclic = $periodic['fc_pm_cyclic_number'];
        $type_cyclic =$periodic['fc_pm_cyclic_type'];
        $count = 0;
        if(empty($num_cyclic)){
            throw new Exception($this->trsl->_('Set Cyclic number'));
        }

        while($increment <= $end_date) {
            if($count > 5000) {                
                $this->db->delete("t_periodics","f_executed = 0 and f_code = ".$f_code_wo);
                return false;
            }                                   

            //if last periodic is over the end date, continue
            if(($increment + $periodic['fc_pm_at_time']) > $end_date)
            {
                break;
            }
            
            if(!$floating_shift || $count > 0){
                $this->createPeriodics($periodic, $f_code_wo, $increment + $periodic['fc_pm_at_time'], $increment + $periodic['fc_pm_at_time'] + $periodic['fc_pm_duration']);
            }
            $count++;
            $increment = $this->increment($type_cyclic, $increment,$num_cyclic);
        }
        $this->savePeriodics();
        return true;
    }
    
    public function increment($type,$timestamp,$num_cyclic)
    {
        $cyclic_type = '';
        switch($type) {
            case 0://case 'none' : //none
                return 0;
                break;
            case 1://case 'day' : //day
                $cyclic_type = 'days';
                break;
            case 2 ://case 'week' :  //week
                $cyclic_type = 'weeks';
                break;
            case 3://case 'month' : //month
                $cyclic_type = 'months';
                break;
            case 4://case 'year' : //year
                $cyclic_type = 'years';
                break;
            default : throw new InvalidArgumentException($this->trsl->_("Unknown Cyclic type"));
                break;
        }

        return strtotime(sprintf("+%d %s",$num_cyclic, $cyclic_type), $timestamp);
    }
    
    /**
     *Create periodic record in t_periodics table
     * @param type $periodic
     * @param type $f_code
     * @param type $start_date
     * @param type $end_date 
     */
    private function createPeriodics($periodic,$f_code,$start_date,$end_date)
    {
        $asset_cond = "";
        if($asset_code = $this->getAssetCode())
        {
            $asset_cond = " AND f_asset_code = ".$asset_code;
        }

        if(isset($this->periodic_rows[$start_date])) return;
        $checkExists = $this->db->query("SELECT count(f_code) as num from t_periodics where f_code = {$f_code}
            and f_start_date = {$start_date} ".$asset_cond)->fetch();
        
        if($checkExists['num'] > 0){
            //Aggiunto da Baldini. Server a garantire che tutte le righe di t_periodics abbiano lo stesso timestamp se fanno parte del'ultima modifica alla PM

            $this->db->update('t_periodics', array("f_timestamp" => $this->timestamp), 'f_code =' . $f_code.' AND f_start_date ='.$start_date.$asset_cond );
            return;
			
        } 
        
        $data = array(
            'f_code' => $f_code,
            'f_forewarning'=>$periodic['fc_pm_forewarning'],
            'f_start_date'=>$start_date,
            'f_end_date'=>$end_date,                
            'f_timestamp'=>$this->timestamp,
            'f_mode'=>1,
            'f_available_type'=>1,
            'f_executed'=>$start_date <= time()?1:0,
            'f_duration'=>$periodic['fc_pm_duration']
        );

        //se non passato, rimuovo la colonna asset_code
        if(($asset_code = $this->getAssetCode()))
        {
            $data['f_asset_code'] = $asset_code;
        }

        $this->periodic_rows[$start_date] = $data;
    }

    public function clearPeriodicRows()
    {
        $this->periodic_rows = [];
    }

    /**
     * Salviamo a blocchi le periodiche su database
     * per evitare di 'intasare' il db 
     */
    protected function savePeriodics()
    {
        if(!$this->periodic_rows) return;
        $table_schema = $this->periodic_table_schema;
        //se non ci sono asset, rimuovo la colonna
        if($this->getAssetCode() === null || $this->getAssetCode() === false)
        {
            unset($table_schema[array_search('f_asset_code', $table_schema)]);
        }

        $default_insert = "INSERT INTO t_periodics (".implode(', ',$table_schema).") VALUES ";
        foreach(array_chunk($this->periodic_rows, 1000) as $rows)
        {
            $values = [];
            $insert = $default_insert;
            foreach($rows as $row)
            {
                $values[] = "(".implode(', ',$row).")";
            }

            $insert.=implode(',', $values);
            $this->db->query($insert);
        }
    }
}
