<?php
class Mainsim_Model_Ldap{
    
    private $db;
    private $conf;
    
    public function __construct($db = null) {
        if($db){
            $this->db = $db;
        }
        else{
            $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        }
    }
    
    public function authenticate($user, $password, $conf){
        
       $this->conf = $conf;
       include(APPLICATION_PATH . "/../" . $conf->script); // esempio : ../scripts/iit/ldap.php
       return $result;
    }
	
	public function authenticateADFS($conf){
		$this->conf = $conf;
		require_once('../simplesamlphp-1.15.4/lib/_autoload.php');
		
		$as = new \SimpleSAML\Auth\Simple($this->conf->sp);

		$as->requireAuth();
		$attributes = $as->getAttributes(); 
		$session = SimpleSAML_Session::getSessionFromRequest();
		$session->cleanup();

		// custom script
		// ie : ../scripts/<PROJECT>/adfs.php 
		// do something with ldap user data, ie fill $this->conf->userProfile to create new user
		try{
			include(APPLICATION_PATH . "/../" . $this->conf->script);
			if($result === false) return false;
		}
		catch(Exception $e){
			die("ADFS custom script '" . APPLICATION_PATH . "/../" . $this->conf->script ."' not found");
		}
		
		$loginObj = new Mainsim_Model_Login($this->db);

		// check if user exists
			if($existingUser = $this->userExists($this->conf->userProfile['fc_usr_usn'])){
				// check if user is already logged
				$loginObj = new Mainsim_Model_Login($this->db);
				//$result_check = $loginObj->checkLogin($existingUser[0]['f_code']);
				/*if(!$result_check) {
                    return false;
                }*/
				$this->db->delete('t_access_registry', "f_user_id = " . $existingUser[0]['f_code']);
				//$adfsObj->updateUser($existingUser[0]['f_code'],$confLDAP,$userInfo);
				$userInfo = $loginObj::getUSerInfo($existingUser[0]['f_code'], $this->db);
				$loginObj->saveUserSession($userInfo);
				return true;
				//die();
				
			}
			// create new user
			else{
				
				$pw = $loginObj->crypt(md5($pw));  
				if($newUser = $this->createUser($us,$pw)){
					$userInfo = $loginObj::getUSerInfo($newUser['f_code'], $this->db);
					$loginObj->saveUserSession($userInfo);
					return true;
				}
			}
		
		//$this->db->update('t_access_registry', array('f_session_id' => session_id()), 'f_user_id =' . $userInfo->f_id);
	}
	
    
    public function updateUser($userCode, $conf,$userInfo){
        $this->conf = $conf;
       include(APPLICATION_PATH . "/../" . $conf->script_update); // esempio : ../scripts/iit/ldap_update_user.php       
       return $result;
    }


    
    public function userExists($us){
        
        $select = new Zend_Db_Select($this->db);
        $select->from(array("t1"=>"t_users"), array('f_code'))
                ->join(array("t2"=>"t_creation_date"),"t1.f_code = t2.f_id",array("f_id"))
                ->where("t2.f_phase_id = 1")
                ->where("fc_usr_usn = ?", $us);
				
        $res = $select->query()->fetchAll();
        if(count($res) > 0)
            return $res;
        else
            return false;
    }
    
    public function createUser($us,$psw = null){
        
        //auto signup
        /*if (!file_exists(APPLICATION_PATH.'/configs/defaultUser.config.php')){
            $this->view->errorMessage =
                'Source error: No defaultUser.config is avaiable for LDAP auto-signin';
                                    $logger->info("Error: No defaultUser.config");
            return -1;
        }
        $params = require APPLICATION_PATH.'/configs/defaultUser.config.php';
                            $logger->info("Registering...");*/

        /*
        //$params['fc_usr_firstname'] = $nameUser;
        //$params['fc_usr_lastname'] = $surnameUser;
        $params['f_title'] = $us;
        //$params['fc_usr_mail'] = $mailUser;
        $params['fc_usr_usn'] = $us;
        $params['fc_usr_password'] = '1234';
        $params['fc_usr_level_text'] = $this->conf->userLevelText;
        $params['f_type_id'] = 16;
        $params['f_module_name'] = 'mdl_usr_tg';
        $params['fc_usr_language'] = $this->conf->userLanguage; //'2'
        $params['fc_usr_level'] = $this->conf->userLevel; //-1
        $params['t_systems_4'] = (array)$this->conf->userRole;
        */
        
        $profile = $this->conf->userProfile;
        $profile['t_systems_4'] = (array)$profile['t_systems_4'];
        foreach ($profile as $key => $value) {
            $params[$key]=$value;
        }
        if(isset($psw)) $params['fc_usr_password'] = $psw;
            else $params['fc_usr_password'] = '1234';
		
        $wares = new Mainsim_Model_Wares();
        try {
            $result = $wares->newWares($params, [], true, true);  
  
        }catch(Exception $e) {
			
            print($e->getMessage());   die();
            return false;
        }
        return $result;
    }
    
    public function update($params)
    {
        $wares = new Mainsim_Model_Wares();
        try {         
            $result = $wares->editWares($params, [], true, true);  
  
        }catch(Exception $e) {
            print($e->getMessage());   die();
            return false;
        }  
    }
    
}
