<?php

class Mainsim_Model_Installer
{
    /**
     *
     * @var Zend_Db 
     */
    private $db;  
    private $encrypt_method = "AES-256-CBC";
    private $salt = 'a23der';
    public $captchaDir = 'public/msim_images/captcha';
    private $captchaObj;

    
    public function __construct($db = null) {
       $this->db = !is_null($db) ? $db : Zend_Db::factory(Zend_Registry::get('db'));
       $this->captchaObj = new Zend_Captcha_Image();
       $this->captchaObj->setWordLen('4')
                        ->setTimeout(300)
                        ->setHeight('60')
                        ->setFont('public/msim_css/font/OpenSans-Regular.ttf')
                        ->setImgDir($this->captchaDir)
                        ->setDotNoiseLevel('5') 
                        ->setLineNoiseLevel('5');
    }
    
    public function control_name_installation($name){
        $select = new Zend_Db_Select($this->db);
        
        $inst = $select->from(array("t1"=>"t_wares"), array('f_code'))
                ->join(array('t2'=>'t_creation_date'), 't1.f_code = t2.f_id')
                ->where("t2.f_title= ? ", $name)->where("t2.f_type= 'WARES'")->where("t2.f_phase_id <> 3")->query()->fetch(); 
    
        if(!emprty($inst['f_code'])){
            return $name . "_go";
        }
        return $name;
        
    }
    

      
}