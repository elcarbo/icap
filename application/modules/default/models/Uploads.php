<?php

class Mainsim_Model_Uploads
{
    private $db;
    
    public function __construct($db = null) 
    {
        if($db){
            $this->db = $db;
        }
        else{
            $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        }
    }
    
    /**
     * save uploaded files in temp dir until user save object
     * @return string js callback 
     */
    public function uploads()
    {
        $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
        $callback = $_POST["callback"];
        $attachmentFolder = Zend_Registry::get('attachmentsFolder');
        if($_POST['f_linkname'] == '') {
            $result = "";            
            //check if there is some upload's error
            if(!isset($_FILES['f_data']) or $_FILES['f_data']['error'] != UPLOAD_ERR_OK) {
                return "'error', '$callback', '0'";
            }
            
            if(isset($_FILES['f_data']['tmp_name'])) 
                $fileattach = $_FILES['f_data']['tmp_name'];
            else 
                $fileattach = "";
            $mime = $_FILES['f_data']['type'];

            $sizes = array(                
                "thumbnail" => array( "w" => 50, "h" => 50 ),
                "medium" => array( "w" => 400, "h" => 400 ),
                "high" => array( "w" => 800, "h" => 600 )
            );
            
            $path_temp = realpath($attachmentFolder.'/temp/');
            $ext = substr($_POST["f_filename"],strrpos($_POST["f_filename"],".") + 1 );
            $filename = substr($_POST['f_filename'],0,strrpos($_POST["f_filename"],"."));            
            $time = microtime(true)*10000;
            $session_id = $time.'_'.$uid;            
            $imgInfo = getimagesize($fileattach);
            $data = array(                    
                "session_id"=>$session_id,"fieldname" => $_POST["f_fieldname"],
                "filename" => $filename,"ext" => $ext,"mime" => $mime
            );
            if(($imgInfo[2] == 1 || $imgInfo[2] == 2 || $imgInfo[2] == 3) && $_POST['uploadType'] == 'image' ) {                                
                foreach($sizes as $k => $v) {
                    $file_container = $this->resize($fileattach, $sizes[$k]["w"], $size[$k]["h"], $_POST["f_code"]);                    
                    $data["type"] = $k;
                    ob_start();
                    switch ($imgInfo[2]) {
                        case 1: imagegif($file_container); break;
                        case 2: imagejpeg($file_container);  break;
                        case 3: imagepng($file_container); break;
                        default:  trigger_error('Failed resize image!', E_USER_WARNING);  
                            break;
                    }
                    $file = ob_get_contents();
                    ob_end_clean();
                    
                    $savePath = realpath($path_temp.'/').'/'.$session_id."_$k.".$ext;
                    $result = $this->saveFile($savePath, $file, $data);
                    if(!empty($result)) {
                        foreach($sizes as $k => $v) {
                            @unlink(realpath($path_temp.'/').'/'.$session_id."_$k.".$ext);
                            $this->db->delete("t_attachments","f_session_id = '$session_id' and f_type = '$k'");
                        }
                        break;
                    }
                }
            }
            //check if try to insert an image of different format
            elseif(($imgInfo[2] != 1 && $imgInfo[2] != 2 && $imgInfo[2] != 3) && $_POST['uploadType'] == 'image') {
                $result = 'Yuo cannot upload images of format different from jpg,gif or png';
            }
            else {
                $data["type"] = 'doc';
                $result = $this->saveFile(realpath($path_temp.'/').'/'.$session_id.'.'.$ext, file_get_contents($fileattach), $data);
            }
            return "'$result', '$callback', '$session_id','".addslashes($filename).".{$ext}'";
        }
        else {
            return "'', '$callback', '', '".addslashes($_POST['f_linkname'])."'";
        }
    }
    
    
    /**
     * Save file and create row in t_attachments
     * @param string $savePath : path to save file
     * @param type $file : file
     * @param array $options array with filename,ext,fieldname,type,session_id,mime. All keys in array are mandatory
     */
    public function saveFile($savePath,$file,$options = [])
    {
		$result = "";
        try {                                
            if(file_put_contents($savePath, $file)){
                $data = array(
                    "f_code" => 0,
                    "f_session_id"=>$options['session_id'],		
                    "f_timestamp" => time(),
                    "f_active" => 0,
                    "f_fieldname" => $options["fieldname"],
                    "f_file_name" => $options['filename'],
                    "f_file_ext" => $options['ext'],
                    "f_type" => $options['type'],
                    "f_mime" => $options['mime'],
                    "f_path"=>  $savePath 
                );            
                $this->db->insert("t_attachments", $data);
            }
            else {// message of error
                $result = "There was an error during the upload of your file. Please check privilegies of upload folder.";
            }
        }catch(Exception $e){  
            $error = $e;
            try{
                unlink($savePath);    
            }catch(Exception $ex){}
              $result = "error: ".$error->getMessage(); 
            }
        return $result;
    }
    
    public function removeAttach($f_code, $fieldname)
    {        
        try {
            $this->db->update("t_attachments", array("f_active" => -1), "f_code = $f_code and f_fieldname = '{$fieldname}'");
        } catch(Exception $e) {}        
    }
    
    public static function resize($img, $w, $h) 
    {
       //Get Image size info
       $imgInfo = getimagesize($img);
       switch ($imgInfo[2]) {
           case 1: $im = imagecreatefromgif($img); 
               break;
           case 2: $im = imagecreatefromjpeg($img);
               break;
           case 3: $im = imagecreatefrompng($img); 
               break;
           default:  trigger_error('Unsupported filetype!', E_USER_WARNING);  
               break;
       }
      
       //If image dimension is smaller, do not resize
       if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
           $nHeight = $imgInfo[1];
           $nWidth = $imgInfo[0];
       }
       else{
           if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
               $nWidth = $w;
               $nHeight = $imgInfo[1]*($w/$imgInfo[0]);
           }
           else{
               $nWidth = $imgInfo[0]*($h/$imgInfo[1]);
               $nHeight = $h;
           }
       }
       $nWidth = round($nWidth);
       $nHeight = round($nHeight);
       
       $newImg = imagecreatetruecolor($nWidth, $nHeight);

       /* Check if this image is PNG or GIF, then set if Transparent*/  
       if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
           imagealphablending($newImg, false);
           imagesavealpha($newImg,true);
           $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
           imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
       }
       imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);

       return $newImg;
    }
    
    public function getDocument($params) 
    {
        $select = new Zend_Db_Select($this->db);                
        $select->from("t_attachments");
        if(isset($params['session_id'])) {
            $select->where("f_session_id = ?",$params['session_id']);
        }
        else {
            $select->where("f_code = ?", $params['f_code']);
        }
        $res_img = $select->where("f_type = ?", $params['size'])->query()->fetchAll();                        
        return $res_img;        
    }
	
    public function multipleUploads()
    {        
        $uid = Zend_Auth::getInstance()->getIdentity()->f_id;                        
        //check if there is some upload's error  		
        if(!isset($_FILES['filesToUpload'])) {
            return "error', '', '0";
        }
        $tot_a = count($_FILES['filesToUpload']['error']);        
        for($a = 0;$a < $tot_a;++$a) {
            if($_FILES['filesToUpload']['error'][$a] != UPLOAD_ERR_OK) {				
                return "error', '', '0";
            }
        }
        
        $tot_a = count($_FILES['filesToUpload']['name']);
        $atch = array();
        $time = time();
        $wares = new Mainsim_Model_Wares();
        
        //fields for return
        $cr_tab = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
        $wa_tab = Mainsim_Model_Utilities::get_tab_cols("t_wares");
        $cf_tab = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");     
        $select = new Zend_Db_Select($this->db);
        
        for($a = 0;$a < $tot_a;++$a) {            
            if(isset($_FILES['filesToUpload']['tmp_name'][$a])) 
                $fileattach = $_FILES['filesToUpload']['tmp_name'][$a];
            else 
                $fileattach = "";
            $mime = $_FILES['filesToUpload']['type'][$a];
            
            $data = array();  
            $filename =  $_FILES['filesToUpload']['name'][$a];
            $path_temp = realpath($attachmentFolder.'/temp/');
            $ext = substr($filename,strrpos($filename,".") + 1 );
            $filename = substr($filename,0,strrpos($filename,"."));
            $session_id = $time.'_'.$uid;                        
            if(file_put_contents(realpath($path_temp.'/').'/'.$session_id.'.'.$ext, file_get_contents($fileattach))){
                $data = array(
                    "f_code" => 0,
                    "f_session_id"=>$session_id,		
                    "f_timestamp" => time(),
                    "f_active" => 0,
                    "f_fieldname" => 'fc_doc_attach',
                    "f_file_name" => $filename,
                    "f_file_ext" => $ext,
                    "f_type" => 'doc',
                    "f_mime" => $mime, // TODO: add
                    "f_path"=>  realpath($path_temp).'/'.$session_id.'.'.$ext //temp path                        
                );
                $this->db->insert("t_attachments", $data);
                $params = array(
                    'f_module_name'=>'mdl_doc_tg',
                    'f_code'=>0,
                    'f_type_id'=>5
                );
                
                $params['fc_doc_attach'] = $session_id.'|'.$filename.'.'.$ext;                
                $sel = isset($_GET['sel'])? array($_GET['sel']):array();
                $params['t_selectors_6'] = array(                            
                        "f_code"=>$sel,
                        "f_code_main"=> 0,
                        "f_type"=> "6",
                        "Ttable"=> "t_selectors",
                        "f_module_name"=> "mdl_doc_slc_tg"                            
                );
                $params['f_title'] = $filename.'.'.$ext;                
                $res_wa = $wares->newWares($params);
                if(isset($res_wa['f_code'])) {
                    $mdl_doc = "mdl_{$_GET['mod']}_doc_tg";
                    $bm_doc = Mainsim_Model_Utilities::getBookmark($mdl_doc);                    
                    if(!empty($bm_doc)) {
                        $bm_prop = json_decode(Mainsim_Model_Utilities::chg($bm_doc['f_properties']),true);
                        $cr_fields = array();                        
                        $wa_fields = array();                        
                        $cf_fields = array();                        
                        $tot_b = count($bm_prop);
                        for($b = 0;$b < $tot_b;++$b) {
                            if(in_array($bm_prop[$b][0],$cr_tab)) {
                                $cr_fields[] = $bm_prop[$b][0];
                            }
                            elseif(in_array($bm_prop[$b][0],$wa_tab)) {
                                $wa_fields[] = $bm_prop[$b][0];
                            }
                            elseif(in_array($bm_prop[$b][0],$cf_tab)) {
                                $cf_fields[] = $bm_prop[$b][0];
                            }                            
                        }
                        $res_doc = $select->from(array("t1"=>"t_creation_date"),$cr_fields)
                                ->join(array("t2"=>"t_wares"),"t1.f_id = t2.f_code",$wa_fields)
                                ->join(array("t3"=>"t_custom_fields"),"t1.f_id = t3.f_code",$cf_fields)
                                ->where("t1.f_id = ?",$res_wa['f_code'])->query()->fetch();                        
                        foreach($res_doc as &$line_doc) {
                            $line_doc = is_null($line_doc)?'':$line_doc;
                        }
						$res_doc["rtree"] = "root";
                        $atch[] = $res_doc;                        
                        $time++;
                    }
                }                     
            }                 
        }
        $jsn=json_encode($atch);
        return $jsn;
    }
    
    
    
   /**
     * Upload files from drag drop treegrids
     * @param array $files list of files in $_FILES['attachments']
     * create new doc in file & links     
     * @return array list of uploaded files properties
     */
    public function multipleUploadsFromDragDrop($files, $mode=0)
    {
      $tot = count($files);        
      $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
      $attachmentFolder = Zend_Registry::get('attachmentsFolder');
      $path_temp = realpath($attachmentFolder.'/temp/');
	  $result = [];
      for($i=0;$i<$tot;$i++) {        
        $file = $files[$i];            
        if($file['error'] != UPLOAD_ERR_OK || empty($file['tmp_name'])) {continue;}            
        $fileattach = $file['tmp_name'];            
        $data['mime'] = $file['type'];                        
        $data['ext'] = $ext = substr($file["name"],strrpos($file["name"],".") + 1 );
        $data['filename'] = substr($file["name"],0,strrpos($file["name"],"."));
        $time = microtime(true)*10000;
        $data['session_id'] = $session_id = $time.'_'.$uid;
        $data['fieldname'] = "txtfld_doc_edit_attach";            
        $data["type"] = 'doc';    
        
        if($mode) $fgc=$fileattach; else $fgc=file_get_contents($fileattach);
        
        
        try {
            $check = $this->saveFile($path_temp.DIRECTORY_SEPARATOR.$session_id.'.'.$ext, $fgc, $data);
        }catch(Exception $e) {$check = $e->getMessage();}         
        if(!empty($check)){ continue; }
        
        $result[] = ["name"=>$file['name'], "size"=>$file['size'],  "url"=>"{$session_id}|{$file['name']}" ];
      } 

      $res=$this->saveNewDocFromDragDrop($result);

      return $res;
    }
    
    
    
    public function saveNewDocFromDragDrop($list)
    {
		$wa = new Mainsim_Model_Wares($this->db);
        $select = new Zend_Db_Select($this->db);
        $risp = [];
        $tot = count($list);
        $params = ['f_code'=>0,'f_type_id'=>5,'f_module_name'=>'mdl_doc_tg','attachments'=>['fc_doc_attach'],
                   'f_visibility'=> -1, 'f_editability'=>-1, 'f_phase_id'=> 1, 'f_name'=>'Active', 'f_edit'=>-1, 'f_wf_id'=>'9', 'rtree'=>'root', 'fc_progress'=>0, 'selected'=>2, 'fc_doc_type'=>'generic'];
        for($i = 0;$i < $tot;++$i) {
            $params['f_title'] = $list[$i]['name'];
            $params['fc_doc_attach'] = $list[$i]['url'];
            $params['size'] = $list[$i]['size'];
            $res = $wa->newWares($params, [], false);
            if(!isset($res['f_code'])) { print_r($res);die('error'); }
            $arisp=$params;
            $arisp['f_code']=$res['f_code'];
            
          $select->reset();
          $select->from('t_creation_date', 'fc_progress')
                 ->where("f_id = '".$res['f_code']."'");
          $rsp = $select->query()->fetchAll();

          if(count($rsp)) $arisp['fc_progress'] = $rsp[0]['fc_progress'];    
                  
          $risp[]=$arisp;
        }  
        return $risp;
    }
    
    
    
    
    
    
    /**
     * Upload files from wizard (dd and form)
     * @param array $files list of files in $_FILES['attachments']
     * @return array list of uploaded files
     */
    public function multipleUploadsFromWiz($files)
    {
        $tot = count($files);        
        $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
		if(!$uid){
			$userInfo = Zend_Auth::getInstance()->getIdentity();
			$uid = $userInfo['f_code'];
		}
        $attachmentFolder = Zend_Registry::get('attachmentsFolder');
        $path_temp = realpath($attachmentFolder.'/temp/');
		$result = [];
        for($i = 0;$i < $tot;$i++) {        
            $file = $files[$i];            
            if($file['error'] != UPLOAD_ERR_OK || empty($file['tmp_name'])) {continue;}            
            $fileattach = $file['tmp_name'];            
            $data['mime'] = $file['type'];                        
            $data['ext'] = $ext = substr($file["name"],strrpos($file["name"],".") + 1 );
            $data['filename'] = substr($file["name"],0,strrpos($file["name"],"."));
            $time = microtime(true)*10000;
            $data['session_id'] = $session_id = $time.'_'.$uid;
            $data['fieldname'] = "txtfld_doc_edit_attach";            
            $data["type"] = 'doc';    
            try {
                $check = $this->saveFile($path_temp.DIRECTORY_SEPARATOR.$session_id.'.'.$ext, file_get_contents($fileattach), $data);
            }catch(Exception $e) {$check = $e->getMessage();}         
            if(!empty($check)){ continue; }
            $result[] = ["name"=>$file['name'], "size"=>$file['size'],  "url"=>"{$session_id}|{$file['name']}" ];
        } 
        return $result;
    }
    
    /**
     * remove temp file attached from wizard
     * @param string $session_id temporary key of file
     */
    public function removewizAttachment($session_id)
    {
        $exp_si = explode('|',$session_id);
        $si = $exp_si[0];        
        $filename = $si.'.'.substr($exp_si[1],strrpos($exp_si[1],".") + 1 );
        $attachmentFolder = Zend_Registry::get('attachmentsFolder');
        $path_temp = realpath($attachmentFolder.'/temp/');        
        $this->db->delete("t_attachments","f_session_id = '$si'");
        unlink($path_temp.DIRECTORY_SEPARATOR.$filename);
    }
}