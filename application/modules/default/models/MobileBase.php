<?php
class Mainsim_Model_MobileBase{
    
    protected $db;
    
    public function __construct($db = null) {
        
        if(!$db){
            //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
            //$this->db = Zend_Db::factory($config->database);
            $conf = Zend_Registry::get('db');            
            $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        }
        else{
            $this->db = $db;
        }
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    protected function dbSelect($select, $mode = null, $debug = false){
        try{
            // debug
            if($debug){
                echo "<hr>";
                echo $select . "<br>";
                echo "<hr>";
            }
            $res = $select->query();
            if($mode){
                $res->setFetchMode($mode);
            }
            
            if(CHG_MOBILE){
                $aux = $res->fetchAll();
                for($i = 0; $i < count($aux); $i++){
                    foreach($aux[$i] as $key => $value){
                        $aux2 = Mainsim_Model_Utilities::chg($aux[$i][$key]);
                        if($aux2){
                            $aux[$i][$key] = $aux2;
                        }
                    }
                }
                return $aux;
            }
            else{
                return $res->fetchAll();
            }
        }
        catch(Exception $e){
            print("<br>" . $select . "<br>");
            print($e->GetMessage());
        }
    }
    
    protected function dbUpdate($table, $data, $where = null){
        
        if($where){
            $whereSQL = '1=1 ';
            foreach($where as $k => $v){
                $whereSQL .= ' AND ' . $k . '=\'' . $v . '\'';
            }
        }
        try{
            $this->db->update($table, $data, $whereSQL);
        }
        catch(Exception $e){
            print($e->GetMessage());
        }
    }
    
    protected function dbInsert($table, $data){
        try{
            $this->db->insert($table, $data);
            return $this->db->lastInsertId();
        }
        catch(Exception $e){
            print($e->GetMessage());
        }
    }
    
    protected function dbDelete($table, $where){
        
        try{
            $this->db->delete($table, $where);
        }
        catch(Exception $e){
            print($e->GetMessage());
        }
    }
    
    protected function dbBeginTransaction(){
        $this->db->beginTransaction();
    }
    
    protected function dbEndTransaction(){
        $this->db->commit();
    }
    
    protected function dbRollbackTransaction(){
        $this->db->rollBack();
    }
    
    
    public function createResponseObject($result, $status, $sessionId = null, $params = null, $message = null, $headerMessage = null, $version = null, $custom = null){
        
        $object = array(
            'result' => $result,
            'status' => $status,
            'sessionId' => $sessionId,
            'params' => $params,
            'message' => $message,
            'headerMessage' => $headerMessage,
            'version' => $version
        );
        
        // add custom data
        if(is_array($custom)){
            foreach($custom as $key => $value){
                $object[$key] = $value;
            }
        }
        
        return $object;
    }
    
    public function translate($string){
        return $string;
    }
}
?>
