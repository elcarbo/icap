<?php

class Mainsim_Model_Mail
{
    private $db;
    private $smtp;     
    private $smtpParams;
    
    public function __construct($db = null) {
        $this->db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $resSmtpParams = $this->db->query("SELECT f_title,f_description"
                . " from t_creation_date where f_type = 'SYSTEMS' AND f_category = 'SETTING' "
                . " AND f_title IN ('SMTP_ANONYMOUS','SMTP_SSL','SMTP_PORT','SMTP_SENDER_MAIL',"
                . "'SMTP_SENDER_NAME','SMTP_NOREPLY','SMTP_PASSWORD','SMTP_USERNAME','SMTP_ADDRESS','SEND_MAIL', 'SEND_MAIL_CRON', 'MAIL_TEST')")
            ->fetchAll();
        $this->prepareSmtpParams($resSmtpParams);
        //Zend_Debug::dump($this->smtpParams);die; 
        $smtp_params = array();    
        if(!$this->smtpParams['SMTP_ANONYMOUS']) { //if not anonymous login
            $smtp_params = array(                                       
                'auth'=>'login',                    
                'username'=>$this->smtpParams['SMTP_USERNAME'],
                'password'=>$this->smtpParams['SMTP_PASSWORD']
            );
            if($this->smtpParams['SMTP_PORT'] != '') {
                $smtp_params['port'] = $this->smtpParams['SMTP_PORT'];
            }
            if($this->smtpParams['SMTP_SSL'] != '') {
                $smtp_params['ssl'] = $this->smtpParams['SMTP_SSL'];
            }
        }
        $this->smtp = new Zend_Mail_Transport_Smtp($this->smtpParams['SMTP_ADDRESS'],$smtp_params);        
        Zend_Mail::setDefaultTransport($this->smtp);
    }
    
    public function __destruct() {        
        Zend_Mail::clearDefaultFrom();
        Zend_Mail::clearDefaultReplyTo();
    }
    
    private function prepareSmtpParams($resSmtpParams)
    {
        $tot = count($resSmtpParams);
        for($i = 0;$i < $tot;++$i) {
            $this->smtpParams[$resSmtpParams[$i]['f_title']] = $resSmtpParams[$i]['f_description'];
        }
    }
        
    /**
     * 
     * @param array $mail_list : list of mails receiver divides in To,Cc,Bcc 
     * @param string $object : Object of mail
     * @param string $body : Body of mail
     * @param array $attachments : list of attachment. Every attachment is an array with : path,filename,mime
     * @param array $additional_params : list of additional params like From,replyTo
     */
    public function sendMail($mail_list,$object,$body,$attachments = array(),$additional_params = array()) 
    {

        //if($this->smtpParams['SEND_MAIL'] == 'false') return;
        if(!isset($this->smtpParams['SEND_MAIL'])) $this->smtpParams['SEND_MAIL'] = 1;
        $sm = (int)$this->smtpParams['SEND_MAIL'];
        if(!$sm) return; 

        if(!isset($this->smtpParams['SEND_MAIL_CRON']) || (int)$this->smtpParams['SEND_MAIL_CRON'] == 0){
                $mail = new Zend_Mail();   
                //add To,Cc and Bcc
                $mailListClone = json_encode($mail_list);

				$select = new Zend_Db_Select($this->db);
                    $r = $select->from('t_creation_date')->where("f_category LIKE '%SETTING%' AND f_title LIKE '%MAIL_TEST%'")->query()->fetchAll();
                    if(sizeof($r) > 0 && $r[0]['f_description'] != '') {
                        $mail_list = [];
                        $m = json_decode($r[0]['f_description'], true);
                        if($m['to'] != '') $mail_list['To'] = [trim($m['to'])];
                        if(isset($m['bcc']) && $m['bcc'] != '') $mail_list['Bcc'] = [trim($m['bcc'])];
                            else unset($mail_list['Bcc']);
                        if(isset($m['cc']) && $m['cc'] != '' ) $mail_list['Cc'] = [trim($m['cc'])];
                            else unset($mail_list['cc']);
                    }

                foreach($mail_list as $addType => $lists) { 
                    $type = "add{$addType}";
					foreach($lists as $mail_addr => $mail_name) {
						if(strpos($mail_addr,'@') !== false) {
							$mail->$type($mail_addr,$mail_name);
						}
						else {                    
							$mail->$type($mail_name);
						}
					}
				} 

                //add from and reply To
                if(isset($additional_params['from'])) {
                    $mail->setFrom($additional_params['from']['mail'],$additional_params['from']['name']);
                }
                else {
                   Zend_Mail::setDefaultFrom($this->smtpParams['SMTP_SENDER_MAIL'],$this->smtpParams['SMTP_SENDER_NAME']);
                }
                if(isset($additional_params['replyTo'])) {
                    $mail->setReplyTo($additional_params['replyTo']['mail'],$additional_params['replyTo']['name']);
                }
                else {
                   Zend_Mail::setDefaultReplyTo($this->smtpParams['SMTP_NOREPLY'],'');        
                }

                //add attachments     

                if(!empty($attachments)) {
                    foreach($attachments as $atch)  {
                        if(isset($atch['disposition']) && $atch['disposition'] == Zend_Mime::DISPOSITION_INLINE) {                    
                            $mail->setType(Zend_Mime::MULTIPART_RELATED);
                        }

                        if(isset($atch['content'])) $content=$atch['content']; 
                        else $content = file_get_contents($atch['path']);                  

                        $at = $mail->createAttachment($content);
                        $at->filename    = $atch['filename'];
                        $at->type        = $atch['mime'];
                        $at->disposition = isset($atch['disposition'])?$atch['disposition']:Zend_Mime::DISPOSITION_ATTACHMENT;
                        $at->encoding    = isset($atch['encoding'])?$atch['encoding']:Zend_Mime::ENCODING_BASE64;
                        $at->id = isset($atch['id'])?$atch['id']:"cid_".md5($content);
                    }
                }        
                $mail->setSubject($object);    

                if(isset($this->smtpParams['MAIL_TEST']) && $this->smtpParams['MAIL_TEST'] != '') {
                    $body .= '<br>-------------<br>'.$mailListClone;
                }                   

                if(isset($additional_params['content-type']) && $additional_params['content-type'] == 'text/plain'){
                                $mail->setBodyText($body);
                        }
                        else{
                                $mail->setBodyHtml($body);
                        }
                $mail->send();
        }
        // send by cron
        else{
            foreach($mail_list as $key => $value){
                foreach($value as $key2 => $value2){
                    $aux[] = $value2;
                } 
            }   
            $insert = array(
                'f_address' => implode(',', $aux),
                'f_time_sent' => time(),
                'f_subject' => $object,
                'f_message' => $body,
                'f_status' => 0,
                'f_attempts' => 0,
                'f_attachments' => ''
            );
            $this->db->insert('t_mail_queue', $insert);
            
        }
    }
    
    public function removeAttach($path) {
        $where = $this->db->quoteInto("f_path = ?",$path);
        $this->db->delete("t_attachments",$where);
        unlink($path);
    }
    
    /**
     * @paran int $f_code f_code to attach
     * @param array $files list of $_FILES to attach
     * @return array array with element attached
     */
    public function addAttachment()
    {        
        $path_dir = realpath(APPLICATION_PATH.'/../attachments/temp')."/";        
        $uid = Zend_Auth::getInstance()->getIdentity()->f_id;        
        $atch=array();
        if(!is_dir($path_dir)) {
            if(!mkdir($path_dir)){
                $atch['error'] = 'Impossibile scrivere nella cartella "attachment". Controllare permessi.';
            }
        }        
        if(empty($atch)) {
            $n=time();                                
            if(count($_FILES['filesToUpload']['tmp_name'])) {
                for($r=0;$r<count($_FILES['filesToUpload']['tmp_name']);$r++) {
                    $prefix = $uid.'_'.$n.'_';
                    $name=$_FILES['filesToUpload']['name'][$r];
                    $ext = substr($name,strrpos($name,".") + 1 );
                    $filename = substr($name,0,strrpos($name,"."));
                    $new_name = $prefix.$name;
                    $Path=$path_dir.$new_name;
                    
                    if(move_uploaded_file($_FILES['filesToUpload']['tmp_name'][$r], $Path)){       
                        $data = array(
                            'f_code'=>0,
                            'f_session_id'=>$new_name,
                            'f_timestamp'=>time(),
                            'f_active'=>0,
                            'f_fieldname'=>'',
                            'f_file_name'=>$filename,
                            'f_file_ext'=>$ext,
                            'f_type'=>'doc',
                            'f_mime'=>$_FILES['filesToUpload']['type'][$r],
                            'f_path'=>$Path                            
                        );
                        $this->db->insert("t_attachments",$data);
                        
                        $atch[]=array("name"=>$name, "f_code"=>$new_name);
                    }
                    $n++;
                }        
            }
        }
        return json_encode($atch);
    }
    
    public function getAttachInfo($f_code_doc,$field = 'f_code')
    {
        $select = new Zend_Db_Select($this->db);
        $res_atch_file = $select->from("t_attachments")->where("$field = ?",$f_code_doc)
                ->query()->fetch();
        $attach = array();
        if(!empty($res_atch_file)) {
            $attach = array(
                'mime'=>$res_atch_file['f_mime'],
                'path'=>$res_atch_file['f_path'],
                'size'=>filesize($res_atch_file['f_path']),
                'filename'=>$res_atch_file['f_file_name'].'.'.$res_atch_file['f_file_ext'],
                'session_id'=>$res_atch_file['f_session_id']
            );                                
        }
        return $attach;
    }
    
    public function sendCustomMail($params) {
         
         // get workorder info
         $select = new Zend_Db_Select($this->db);
         $select->from('t_creation_date')
                ->join('t_workorders', 't_workorders.f_code = t_creation_date.f_id')
                ->join('t_custom_fields', 't_workorders.f_code = t_custom_fields.f_code')
                ->where('t_creation_date.f_id = ' . $params->f_code);
         $res = $select->query()->fetchAll();
         
         // get mail template
         $select->reset();
         $select->from('t_creation_date')
                ->join('t_wares', 't_creation_date.f_id = t_wares.f_code')
                ->where('t_wares.f_code = ' . $params->action);
         $res2 = $select->query()->fetchAll();
         
         $body = $res2[0]['f_description'];
         foreach($res[0] as $key => $value){

             if(is_numeric($value) && strlen($value) == 10){
                 $value = date('Y-m-d', $value);
             }
             
             $body = str_replace("::" . $key . "::", $value, $body);
         }
         
         if($body == null){
             $result['message'] .= "Email body is empty\n";
         }
         
         if($res2[0]['fc_action_email_to'] != null){
             
            $aux = explode(";", $res2[0]['fc_action_email_to']);
            for($i = 0; $i < count($aux); $i++){
                
                if(filter_var($aux[$i], FILTER_VALIDATE_EMAIL)) { 
                    $mailing_list['To'][$aux[$i]] = $aux[$i]; 
                } 
                else{ 
                    $notValidEmail .= "-" . $aux[$i] . "\n";
                }
            }
         }
         
         if(isset($res2[0]['fc_action_email_cc'])){
            
            $aux = explode(";", $res2[0]['fc_action_email_cc']);
            for($i = 0; $i < count($aux); $i++){
                if(filter_var($aux[$i], FILTER_VALIDATE_EMAIL)) { 
                    $mailing_list['Cc'][$aux[$i]] = $aux[$i]; 
                } 
                else{ 
                    $notValidEmail .= "-" . $aux[$i] . "\n";
                }
            }
         }
         
         if(isset($res2[0]['fc_action_email_ccn'])){
           
            $aux = explode(";", $res2[0]['fc_action_email_ccn']);
            for($i = 0; $i < count($aux); $i++){
                if(filter_var($aux[$i], FILTER_VALIDATE_EMAIL)) { 
                    $mailing_list['To'][$aux[$i]] = $aux[$i]; 
                } 
                else{ 
                    $notValidEmail .= "-" . $aux[$i] . "\n";
                } 
            }
         }
         
         if(!$result['message'] && is_array($mailing_list['To'])){
            try{ 
               $this->sendMail($mailing_list, $res2[0]['f_title'], $body);
               $result['message'] = 'Email sent successfully'; 
               if($notValidEmail){
                   $result['message'] .= "\nbut there are some email addresses not valid\n" . $notValidEmail;
               }
            }
            catch(Exception $e){
               print($e->getMessage());
            }
         }
         
         if(!is_array($mailing_list['To'])){
             $result['message'] = 'No email addresses specified';
         }
         
         return $result;
    }
    
    /*Funzione aggionta da Lara G. - 14/06/2016*/
    /*$_POST['params'] -> nome dello script in t_scripts, dati in json*/
    public function createLinkMail($script_name_old,$data_json_old, $frm = '') {
       // $params = json_decode(Mainsim_Model_Utilities::chg($_POST['params']),true);
        
        $password = '';

        $possibleChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $i = 0; 

        while ($i < 10) { 
          $char = substr($possibleChars, mt_rand(0, strlen($possibleChars)-1), 1);
          if (!strstr($password, $char)) { 
            $password .= $char;
            $i++;
          }
        }
        
        $code = $password;
        $time = time();
        $data_json = $data_json_old;
        $script_name = $script_name_old;
        
        $this->db->insert("t_script_mail",['f_code_mail' => $code, 'f_data_json' => $data_json, 'f_status' => 0, 'f_script_name' => $script_name, 'f_timestamp' => $time]);
       
        return 'http://'.$_SERVER['HTTP_HOST'].'/mail/operation-link-mail?ns=1&cd='.$code.(!empty($frm) ? '&frm='.$frm : '');
    }
    
    public function setDataLink($code, $frm){
        $select = new Zend_Db_Select($this->db);
        $select->from('t_script_mail')
               ->where("f_code_mail = '". $code ."'");
               #echo $select; die;
        $res = $select->query()->fetch();
        if(!empty($res)) {
            if($res['f_status'] == 0) {
                $re = $this->executeScriptMailLink($res['f_code_mail'],$res['f_script_name'], $res['f_data_json'],$res['f_timestamp']);
                $this->db->update('t_script_mail', array('f_status' => 1), "f_code_mail = '" . $res['f_code_mail']."'");
                if(empty($frm)) {
                    return "Operazione andata a buon fine.";
                } else {
                    return $re;
                }
            } return "Operazione già effettuata.";
        } else {
            return 'Link non ancora attivo, si prega di riprovare più tardi.';
        }
    }
    
    public function executeScriptMailLink($code_d,$script_name, $data_scr, $timestamp){
        $data = [];
        $params['f_code_mail'] = $code_d;
        $params['dati_link_mail'] = $data_scr;
        $params['timestamp_link_mail'] = $timestamp;
        $select = new Zend_Db_Select($this->db);
        $select->from('t_scripts')
                ->where('f_name = ?', $script_name)
                ->where("f_type = 'php' ");
        $d = $select->query()->fetchAll();
        if(count($d) == 0) return false;        
            eval($d[0]["f_script"]);        
        return $data;
    }
   
} 