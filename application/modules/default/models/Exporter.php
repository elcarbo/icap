<?php
require_once 'ExcelStreamer.php';
class Mainsim_Model_Exporter{
    
    private $exportURL;
    private $filename; 
    private $lang;
    
    public function __construct($exportURL = null, $exportConfigURL = null) {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        if($exportURL){
            $this->exportURL = $exportURL;
        }
        else{
            $this->exportURL = 'export/temp/'; 
        }
        
        if($exportConfigURL){
            $this->exportConfigURL = $exportConfigURL;
        }
        else{
            $this->exportConfigURL = 'export/';
        }
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    // ********************************************************************
    // get fields to be exported from bookmarks
    // 
    // $obj = config object for the specific data (ware, workorder, etc...)
    // ********************************************************************
    private function getFieldsFromBookmarks2(&$obj, $bookmarks = null, $phase = null){

        if(!$bookmarks){
            // get custom fields from bookmarks
            // new way to get bookmarks
            $objSys = new Mainsim_Model_System();
            $bookmarks = $objSys->getBookmark($obj['module'], 'Default');
            
            $bookmarks = json_decode(utf8_encode(Mainsim_Model_Utilities::chg($bookmarks['view']['f_properties'])));
        }
        // fields to field list
        for($i = 0; $i < count($bookmarks); $i++){
            // convert images fields
            if($bookmarks[$i][0] == 'fc_creation_user_avatar'){
                $bookmarks[$i][0] = 'fc_creation_user_name';
            }
            
            // visibility property set to '1' ---> add field to list 
            if($bookmarks[$i][4] == 1){
            // title
                $obj['fields'][$bookmarks[$i][0]]['title'] = $bookmarks[$i][1];
                // width
                $obj['fields'][$bookmarks[$i][0]]['width'] = $bookmarks[$i][5];
                // data type
                switch($bookmarks[$i][2]){
                    // number
                    case '1' : 
                        $obj['fields'][$bookmarks[$i][0]]['type'] = 'Number';
                        break;
                    // boolean
                    case '2' : 
                        $obj['fields'][$bookmarks[$i][0]]['type'] = 'Boolean';
                        break; 
                    // date
                    case '3' : 
                        $obj['fields'][$bookmarks[$i][0]]['type'] = 'DateTime';
                        break;
                    // default
                    default :
                        $obj['fields'][$bookmarks[$i][0]]['type'] = 'String';
                        break;
                }
                
                // get map from db actions for f_priority field
                if($bookmarks[$i][0] == 'f_priority'){
                    
                    $select = new Zend_Db_Select($this->db);
                    $select->from('t_creation_date', array('f_title'))
                           ->join('t_wares', 't_creation_date.f_id = t_wares.f_code', array('fc_action_priority_value'))
                           ->where('fc_action_priority_value IS NOT NULL');
                    try{
                        $res = $this->db->query($select)->fetchAll();
                        for($r = 0; $r < count($res); $r++){
                            $map[$res[$r]['fc_action_priority_value']] = $res[$r]['f_title'];
                        }
                    }
                    catch(Exception $e){
                        file_put_contents($this->exportURL . $this->filename . '_log.txt', $select . "\n" . $e->getMessage());
                        die();
                    } 
                }
                else if($bookmarks[$i][0] == 'f_phase_id'){
                    
                    $select = new Zend_Db_Select($this->db);
                    if($phase[0] == 't_wares'){
                        $select->from('t_wares_types', array())
                                ->join('t_wf_phases', 't_wares_types.f_wf_id = t_wf_phases.f_wf_id', array('f_number', 'f_name'))
                                ->where('t_wares_types.f_id = ' . $phase[1]);
                        try{
                            $res = $this->db->query($select)->fetchAll();
                            for($r = 0; $r < count($res); $r++){
                                $map[$res[$r]['f_number']] = $res[$r]['f_name'];
                            }
                        }  
                        catch(Exception $e){
                            file_put_contents($this->exportURL . $this->filename . '_log.txt', $select . "\n" . $e->getMessage());
                            die();
                        }
                    }
                }
                else if(is_object($bookmarks[$i][6])){
                   $map = get_object_vars($bookmarks[$i][6]);
                }
                else{
                   $map = $bookmarks[$i][6];
                }

                if($bookmarks[$i][9] == 'datetime'){
                    if($this->lang == 'it_IT'){
                        //$eval = "\$value = \$value ?  date(\"d/m/Y H:i:s\", (int)\$value) : '';";
                        $eval = "\$value = \$value ?  25569 + (((int)\$value)+date('Z',\$value)) /86400: '';";
                    }
                    else{
                        $eval = "\$value = \$value ?  25569 + (((int)\$value)+date('Z',\$value)) /86400: '';";
                    }
                    $obj['fields'][$bookmarks[$i][0]]['format'] = array(     
                        'eval' => $eval
                    );
                    //die(var_dump($obj['fields'])); 
                }

                if($bookmarks[$i][9] == 'date'){
                    
                    if($this->lang == 'it_IT'){
                        $eval = "\$value = \$value ?  25569 + (((int)\$value)+date('Z',\$value)) /86400: '';";
                    }
                    else{
                        $eval = "\$value = \$value ?  25569 + (((int)\$value)+date('Z',\$value)) /86400: '';";
                    }
                    $obj['fields'][$bookmarks[$i][0]]['format'] = array(     
                        'eval' => $eval
                    );
                }

                if(count($map) > 0 && $bookmarks[$i][0] != 'f_name'){
                    $obj['fields'][$bookmarks[$i][0]]['format']['mapData'] = $map;
                    $obj['fields'][$bookmarks[$i][0]]['format']['eval'] = "\$value = \$mapData[\$value];";
                }
            }
        }
    }
    
    // ********************************************************************
    // get fields to be exported from bookmarks
    // 
    // $obj = config object for the specific data (ware, workorder, etc...)
    // ********************************************************************
    private function getFieldsFromBookmarks(&$obj, $bookmarks = null){
        if(!$bookmarks){
            // get custom fields from bookmarks
            // new way to get bookmarks
            $objSys = new Mainsim_Model_System();
            $bookmarks = $objSys->getBookmark($obj['module'], 'Default');
            $bookmarks = json_decode(utf8_encode($bookmarks['view']['f_properties']));

            /* old way from t_bookmarks */
            /*
            $selectBookmarks = "SELECT f_properties 
                                FROM t_bookmarks
                                WHERE f_module_name = '" . $obj['module'] . "'
                                    AND f_name = 'Default'";
            try{
                $resBookmarks = $this->db->query($selectBookmarks)->fetchAll();
                $bookmarks = json_decode(utf8_encode($resBookmarks[0]['f_properties']));
            }
            catch(Exception $e){
                file_put_contents($this->exportURL . $this->filename . '_log.txt', $selectBookmarks . "\n" . $e->getMessage());
                die();
            }*/
        }
        // fields to field list
        for($i = 0; $i < count($bookmarks); $i++){
            // visibility property set to '1' ---> add field to list 
            if($bookmarks[$i][4] == 1){
            // title
                $obj['fields'][$bookmarks[$i][0]]['title'] = $bookmarks[$i][1];
                // width
                $obj['fields'][$bookmarks[$i][0]]['width'] = $bookmarks[$i][5];
                // data type
                switch($bookmarks[$i][2]){
                    // number
                    case '1' : 
                        $obj['fields'][$bookmarks[$i][0]]['type'] = 'Number';
                        break;
                    // boolean
                    case '2' : 
                        $obj['fields'][$bookmarks[$i][0]]['type'] = 'Boolean';
                        break; 
                    // date
                    case '3' : 
                        $obj['fields'][$bookmarks[$i][0]]['type'] = 'String';
                        break;
                    // default
                    default :
                        $obj['fields'][$bookmarks[$i][0]]['type'] = 'String';
                        break;
                }

                if(is_object($bookmarks[$i][6])){
                   $map = get_object_vars($bookmarks[$i][6]);
                }
                else{
                   $map = $bookmarks[$i][6];
                }

                if($bookmarks[$i][9] == 'datetime'){
                    $obj['fields'][$bookmarks[$i][0]]['format'] = array(
                        'eval' => "\$value = \$value ?  date(\"Y-m-d\", (int)\$value) : '';"
                    );
                }

                if($bookmarks[$i][9] == 'date'){
                    
                    if($obj['lang'] == 'it_IT'){
                        $eval = "\$value = \$value ?  date(\"Y-m-d\", (int)\$value) : '';";
                    }
                    else{
                        $eval = "\$value = \$value ?  date(\"Y-m-d\", (int)\$value) : '';";
                    }
                    $obj['fields'][$bookmarks[$i][0]]['format'] = array(     
                        'eval' => $eval
                    );
                }

                if(count($map) > 0 && $bookmarks[$i][0] != 'f_name'){
                    $obj['fields'][$bookmarks[$i][0]]['format']['mapData'] = $map;
                    $obj['fields'][$bookmarks[$i][0]]['format']['eval'] = "\$value = \$mapData[\$value];";
                }
            }
        }
    }
    
    // *********************************************************
    // prepare fields in SQL language for query used to get data
    // RETURN array of fields to use in query ($selectFields)
    // 
    // $obj = config object for the specific data (ware, workorder, etc...)
    // $type = type of data (ware/workorder)
    // *********************************************************
    private function createFieldsSelect(&$obj, $type, $history = null){           
        if($type == 'ware' || $type == 'workorder'){
            $selectFields[] = 'cd.f_id AS reportCode';
        }
        foreach($obj['fields'] as $key => $value){ 
            $field = $key;
            switch($type){
                // *******************************
                // wares fields
                // *******************************
                case 'ware' :

                    // disambiguate 'f_code' field
                    if($field == 'f_code'){
                        $selectFields[] = 'cd.f_id';
                    }
                    else if($field == 'f_title'){
                        $selectFields[] = 'cd.f_title';
                    }
                    else if($field == 'f_phase_id'){
                        $selectFields[] = 'cd.f_phase_id';
                    }
                    else if($field == 'f_description'){
                        $selectFields[] = 'cd.f_description';
                    }
                    else if($field == 'f_timestamp'){
                        $selectFields[] = 'wa.f_timestamp';
                    }
                    // get parent
                    else if($field == 'f_parent_code'){
                        $selectFields[] = "(SELECT GROUP_CONCAT(CAST(wp.f_parent_code AS CHAR) SEPARATOR ',')
                                        FROM t_wares_parent wp
                                        WHERE wp.f_code = cd.f_id
                                           AND wp.f_active = 1
                                        GROUP BY wp.f_code) as parents";
                    }
                    else if($field == 'f_name'){
                        if($history){
                            $table = 't_creation_date_history';
                            $field_creation = 'f_code';
                        }
                        else{
                            $table = 't_creation_date';
                            $field_creation = 'f_id';
                        }
                        $selectFields[] = "(SELECT wp.f_name
                                            FROM " . $table . " cd1
                                                JOIN t_wf_phases wp ON (cd1.f_wf_id = wp.f_wf_id) 
                                            WHERE cd1.f_phase_id = wp.f_number
                                                AND cd1." . $field_creation . " = wa.f_code AND cd1.f_timestamp = wa.f_timestamp) AS " . $field;
                    }
                    // selector associated to ware
                    else if(strpos($field, 'imp_cross_selector') > 0){
                        $aux = explode("_", $field);
                        $selectFields[] = "(SELECT sw.f_selector_id
                                            FROM t_selector_ware sw
                                                JOIN t_selectors s ON sw.f_selector_id = s.f_code
                                            WHERE sw.f_ware_id = cd.f_id
                                                AND sw.f_nesting_level = 1
                                                AND s.f_type_id = " . $aux[4] . "
                                            ORDER BY sw.f_id DESC
                                            LIMIT 0, 1) as $field";
                    }
                    else if(in_array($field, array('f_percentage', 'f_phase_name', 'f_creation_user_avatar', 'f_creation_user_name', 'f_creation_date', 'f_creation_user_email', 'f_creation_user_phone', 'f_editor_user_name'))){
                        unset($obj['fields'][$field]);
                    }
                    else{
                        $selectFields[] = $field;
                    }
                    break;

                // *******************************
                // workorder fields
                // *******************************
                case 'workorder' :
                    // disambiguate 'f_code' field
                    if($field == 'f_code'){
                        $selectFields[] = 'cd.f_id';
                    }
                    else if($field == 'f_title'){
                        $selectFields[] = 'cd.f_title';
                    }
                    else if($field == 'f_phase_id'){
                        $selectFields[] = 'cd.f_phase_id';
                    }
                    else if($field == 'f_description'){
                        $selectFields[] = 'cd.f_description';
                    }
                    else if($field == 'f_timestamp'){
                        $selectFields[] = 'wo.f_timestamp';
                    }
                    // get parent
                    else if($field == 'f_parent_code'){
                        $selectFields[] = "(SELECT GROUP_CONCAT(CAST(wp.f_parent_code AS CHAR) SEPARATOR ',')
                                        FROM t_workorders_parent wp
                                        WHERE wp.f_code = cd.f_id
                                           AND wp.f_active = 1
                                        GROUP BY wp.f_code) as parents";
                    }
                    else if($field == 'f_name'){
                        if($history){
                            $table = 't_creation_date_history';
                            $field_creation = 'f_code';
                        }
                        else{
                            $table = 't_creation_date';
                            $field_creation = 'f_id';
                        }
                        $selectFields[] = "(SELECT wp.f_name
                                            FROM " . $table . " cd1
                                                JOIN t_wf_phases wp ON (cd1.f_wf_id = wp.f_wf_id) 
                                            WHERE cd1.f_phase_id = wp.f_number
                                                AND cd1." . $field_creation . " = wo.f_code AND cd1.f_timestamp = wo.f_timestamp) AS " . $field;
                    }                    
                    else if(in_array($field, array(
                        'fc_f_percentage', 
                        'f_phase_name', 
                        'f_creation_user_avatar', 
                        'f_creation_user_name', 
                        'f_creation_date', 
                        'f_creation_user_email', 
                        'f_creation_user_phone', 
                        'f_editor_user_name',
                        'fc_owner_name',
                        'fc_owner_email',
                        'fc_owner_phone'))){
                        unset($obj['fields'][$field]);
                    }
                    else{
                        $selectFields[] = $field;
                    }
                    break;
                // *******************************
                // paircross fields
                // *******************************
                case 'paircross':
                    // disambiguate 'f_code' field
                    if($field == 'f_code'){
                        //$selectFields[] = 'pc.f_code';
                        unset($obj['fields'][$field]);
                    }
                    else if($field == 'fc_asset_planned_downtime'){
                         $selectFields[] = 'pc.fc_asset_planned_downtime';
                    }
                    else if($field == 'fc_asset_downtime_hours'){
                         $selectFields[] = 'pc.fc_asset_downtime_hours';
                    }
                    else if(in_array($field, array('fc_rsc_standard', 'fc_rsc_overtime', 'fc_rsc_overnight', 'fc_rsc_holiday'))){
                        unset($obj['fields'][$field]);
                    }
                    else if($field == 'f_title'){
                        $selectFields[] = 'pc.f_title';
                    }
                    else{
                        $pcCols = Mainsim_Model_Utilities::get_tab_cols('t_pair_cross');
                        if(in_array($field, $pcCols)){ 
                            $selectFields[] = 'pc.' . $field;
                        }
                        else{
                            $selectFields[] = $field;
                        }
                    }
                    break;
            }

        }

        return $selectFields;
    }
    
    public function getProgress($filename){
        $aux = explode(".", $filename);
        //print($this->exportURL . $aux[0] . '.txt');
        if(file_exists($this->exportURL . $aux[0] . '.txt')){
            $file = file_get_contents($this->exportURL . $aux[0] . '.txt');
            file_put_contents($this->exportURL . $aux[0] . '_time.txt', time());
            $aux2 = json_decode($file);
            return json_encode($aux2);
        }
        else{
            return "*";
        }
        
    }
    
    public function abort($filename){
        $aux = json_decode(file_get_contents($this->exportURL . $filename . '.txt'));
        
        if($aux->end == 'true'){
            unlink($this->exportURL . $filename . '.xls');
            unlink($this->exportURL . $filename . '.txt');
            unlink($this->exportURL . $filename . '_time.txt');
        }
        else{
            file_put_contents($this->exportURL . $filename . '_abort.txt', 'abort');
        }
    }
    
    public function delete($filename, $format){
        
        @unlink($this->exportURL . $filename . '.' . $format);
        /*if($format == 'xlsx'){
                $this->removeDir($this->exportURL . $filename);
        }*/
        @unlink($this->exportURL . $filename . '.txt');
        @unlink($this->exportURL . $filename . '_time.txt');
    }
	
    public function removeDir($dir){
        foreach(glob($dir . '/*') as $file) {
                if(is_dir($file)){
                        removeDir($file);
                }
                else{
                        unlink($file);
                }
        }
        rmdir($dir);
    }
    
    public function deleteTpl($filename){
        unlink($filename);
        $this->removeDir(str_replace(".xlsx", "", $filename));
    }
    
    /* row data array :
     * 0 : value
     * 1 : type
     * 2 : row offset
     * 3 : col offset
     * 4 : style
     * 5 : merge horizontal
     * 6 : merge vertical
     */
    private function formatExcelData2($data, $fieldsFormat, $fieldsFilter = null, $columnOffset = null, $excelFormat = 'xls'){

        unset($data['RowNum']);
        unset($data['system_code']);
        unset($data['reportCode']);
        $style = $data['_style_'];
        unset($data['_style_']);

        if($excelFormat == 'xlsx'){
                $types['Number'] = 'n';
                $types['Boolean'] = 'b';
                $types['String'] = 'inlineStr';
                $types['DateTime'] = 'date';
        }
        else if($excelFormat == 'xls'){
                $types['Number'] = 'Number';
                $types['Boolean'] = 'Boolean';
                $types['String'] = 'String';
                $types['DateTime'] = 'String';
        }
        if($fieldsFilter){
            foreach($fieldsFilter as $k => $v){
                $value = $data[$k];
                if(key_exists($k, $fieldsFormat) && isset($value)){
                    $mapData = $fieldsFormat[$k]['format']['mapData'];
                    if($v != null && $v != '' && $v != 0){
                        eval($fieldsFormat[$k]['format']['eval']);
                    }
                }
                if($value == '0'){
                    $value = '0';
                }
                if(!$formattedData && $columnOffset){
                    $formattedData[] = array($this->translate($value), $types[$fieldsFormat[$k]['type']], null, $columnOffset, $style, null, null);
                }
                else{
                    $formattedData[] = array($this->translate($value), $types[$fieldsFormat[$k]['type']], null, null, $style, null, null);
                }
            }
        }
        else{
           // die(var_dump($fieldsFormat, "\n", $data));
            foreach($fieldsFormat as $k => $v){
                if(key_exists($k, $data)){
                    $mapData = json_decode(json_encode($fieldsFormat[$k]['format']['mapData']), 1);
                    
                    //  check if value is a new picklist json format
                    $aux = json_decode($data[$k]);
                    if($aux != null && isset($aux->codes) && isset($aux->labels)){
                        $value = implode(",", $aux->labels);
                    }
                    else{
                        $value = $data[$k];
                    }

                    if($value != null && $value != ''){
                        eval($fieldsFormat[$k]['format']['eval']);
                    }
                }
                if($value == '0'){
                    $value = '0';
                }
                if($columnOffset){
                    
                    $formattedData[] = array($this->translate($value), $types[$fieldsFormat[$k]['type']], null, $columnOffset, $style, null, null);
                    $columnOffset++;
                }
                else{
                    $formattedData[] = array($this->translate($value), $types[$fieldsFormat[$k]['type']], null, null, $style, null, null);    
                }
            }
        }
        return $formattedData;
    }
    
    
    
    /* row data array :
     * 0 : value
     * 1 : type
     * 2 : row offset
     * 3 : col offset
     * 4 : style
     * 5 : merge horizontal
     * 6 : merge vertical
     */
    private function formatExcelData($data, $fieldsFormat, $fieldsFilter = null, $columnOffset = null, $excelFormat = 'xls'){

        unset($data['RowNum']);
		unset($data['system_code']);
        unset($data['reportCode']);
        $style = $data['_style_'];
        unset($data['_style_']);

		if($excelFormat == 'xlsx'){
			$types['Number'] = 'n';
			$types['Boolean'] = 'b';
			$types['String'] = 'inlineStr';
			$types['DateTime'] = 'inlineStr';
		}
		else if($excelFormat == 'xls'){
			$types['Number'] = 'Number';
			$types['Boolean'] = 'Boolean';
			$types['String'] = 'String';
			$types['DateTime'] = 'String';
		}
        
        if($fieldsFilter){
            foreach($fieldsFilter as $k => $v){
				$value = $data[$k];
				if(key_exists($k, $fieldsFormat) && isset($value)){
					$mapData = $fieldsFormat[$k]['format']['mapData'];
					if($v != null && $v != '' && $v != 0){
						eval($fieldsFormat[$k]['format']['eval']);
					}
				}
				if($value == '0'){
					$value = '';
				}
				if(!$formattedData && $columnOffset){
					$formattedData[] = array($this->translate($value), $types[$fieldsFormat[$k]['type']], null, $columnOffset, $style, null, null);
				}
				else{
					$formattedData[] = array($this->translate($value), $types[$fieldsFormat[$k]['type']], null, null, $style, null, null);
				}
            }
        }
        else{
            foreach($data as $k => $v){
                $value = $v;
                if(key_exists($k, $fieldsFormat)){
                        $mapData = $fieldsFormat[$k]['format']['mapData'];
                        if($v != null && $v != '' && $v != 0){
                                eval($fieldsFormat[$k]['format']['eval']);
                        }
                }
                if($value == '0'){
                        $value = '';
                }
                if($columnOffset){
                        $formattedData[] = array($this->translate($value), $types[$fieldsFormat[$k]['type']], null, $columnOffset, $style, null, null);
                        $columnOffset++;
                }
                else{
                        $formattedData[] = array($this->translate($value), $types[$fieldsFormat[$k]['type']], null, null, $style, null, null);
                    
                }
            }
        }  
        return $formattedData;
    }
    
    /*
     * prepare data for excel
     * 
     * @param data : row data in associative array way ('field' => 'value')
     * @params fieldConfig : config array of all fields used in the current worksheet
     */
    private function formatExcelDataTPL($data, $operationData, $configFields, $counter = null){
        $types = array(
            'n' => 'n',
            's' => 'inlineStr'
        );
        
        // loop data to format fields
        foreach($configFields as $k => $v){
            if($v['type'] == 'autoincrement'){
                $value = $counter;
                $v['type'] = 'n';
            }
            else if(isset($v['constant'])){
                $value = $v['constant'];
            }
            else{
                $value = $data[$k];
                // data operation
                if($v['operation']){
                    $operation = $operationData[$v['operation']];
                    if($operation['map']){
                        $value = $operation['map'][$value];
                    }

                    if($operation['transform']){
                        eval($operation['transform']);
                    }
                }
            }

            $formattedData[] = array($value, $v['type'] ? $types[$v['type']] : 'inlineStr', null, null, $v['style'] ? $v['style'] : null, null, null);
        }
        return $formattedData;
    }

    // *******************************************************************************************************************************
    // get data from db 
    // RETURN resultset of data
    //
    // $selectFields = fields to get
    // $type = type of data (ware/workorder)
    // $id = 
    // $f_code =
    // *******************************************************************************************************************************
    private function getData($database, $selectFields, $type, $id = null, $f_code = null, $f_code2 = null, $limit, $offset, $numeric = false, $filterParams = null, $history = null, $costs = null){
        // convert to SQL where clause
        if($filterParams->Search){
            for($i = 0; $i < count($filterParams->Search); $i++){
                // disambiguate f_code field
                if($filterParams->Search[$i]->f == 'f_code' && $type == 'ware'){
                    $filterParams->Search[$i]->f = 'wa.f_code';
                }
                else if($filterParams->Search[$i]->f == 'f_code' && $type == 'workorder'){
                    $filterParams->Search[$i]->f = 'wo.f_code';
                }
                
                $value = base64_decode($filterParams->Search[$i]->v0);
                
                if($filterParams->Search[$i]->f == 'fc_progress'){
                    $value = ltrim($value, '0');
                }
                
                $searchFilter .= " AND " . $filterParams->Search[$i]->f . " ";
                if($filterParams->Search[$i]->type == 0){
					
                    switch($filterParams->Search[$i]->Sel){
                            // contain
                            case 0:
                                    $searchFilter .= " LIKE '%" . $value . "%' ";
                                    break;
                            // not contain
                            case 1:
                                    $searchFilter .= " NOT LIKE '%" . $value . "%' ";
                                    break;
                            // start with
                            case 2:
                                    $searchFilter .= " LIKE '" . $value . "%' ";
                                    break;
                            // end width
                            case 3:
                                    $searchFilter .= " LIKE '%" . $value . "' ";
                                    break;
                            // equal to
                            case 4:
                                    $searchFilter .= " = '" . $value . "' ";
                                    break;
                            // not equal to
                            case 5:
                                    $searchFilter .= " <> '" . $value . "' ";
                                    break;
                    }
                }
                else if($filterParams->Search[$i]->type == 1){

                    switch($filterParams->Search[$i]->Sel){
                            // equals
                            case 0:
                                    $searchFilter .= " = " . $value . " ";
                                    break;
                            // not equals
                            case 1:
                                    $searchFilter .= " <> " . $value . " ";
                                    break;
                            // greater than
                            case 2:
                                    $searchFilter .= " > " . $value . " ";
                                    break;
                            // less than
                            case 3:
                                    $searchFilter .= " < " . $value . " ";
                                    break;

                    }
                }
            }
        }
        
        // convert to SQL where clause
        if($filterParams->Filter){
             for($i = 0; $i < count($filterParams->Filter); $i++){ 
                 if($filterParams->Filter[$i]->f == 'f_type_id'){
                     $id = $filterParams->Filter[$i]->v;
                 }
                 else if($filterParams->Filter[$i]->ty == 3){
                     
                     switch($filterParams->Filter[$i]->v) {
                        // today
                        case 0:     
                            $interval = mktime(0,0,0) . " AND ". mktime(23,59,59);
                            break;
                        
                        // yesterday
                        case 1:
                            $interval = mktime(0,0,0,date('n'),date('j')-1) . " AND " . mktime(23,59,59,date('n'),date('j')-1);   
                            break;
                        
                        // tomorrow
                        case 2: 
                            $interval = mktime(0,0,0,date('n'),date('j')+1) . " AND " . mktime(23,59,59,date('n'),date('j')+1);
                            break;
                        
                        // this week
                        case 3: 
                            $interval = mktime(0,0,0,date('n'),date('j')-date('N')+1) . " AND " . mktime(23,59,59,date('n'),date('j')-date('N')+7);
                            break;
                        
                        // previous week
                        case 4:  
                            $interval = mktime(0,0,0,date('n'),date('j')-date('N')+1 -7) . " AND " . mktime(23,59,59,date('n'),date('j')-date('N'));
                            break;
                        
                        // next week
                        case 5:
                            $interval = mktime(23,59,59,date('n'),date('j')-date('N') + 8) . " AND " . mktime(0,0,0,date('n'),date('j')-date('N') +14);
                            break;
                        
                        // this month
                        case 6:        
                            $interval = mktime(0,0,0,date('n'),1) . " AND " . mktime(0,0,0,date('n')+1,0);
                            break;
                        
                        // previous month
                        case 7: 
                            $interval = mktime(0,0,0,date('n') - 1,1) . " AND " . mktime(23,59,59,date('n'),0);
                            break;
                        
                        // this year
                        case 8: 
                            $interval = mktime(0,0,0,1,1,date('Y')) . " AND " . mktime(23,59,59,12,31,date('Y'));
                            break;
                        
                        // previous year
                        case 9:
                            $interval = mktime(0,0,0,1,1,date('Y')-1) . " AND ". mktime(23,59,59,12,31,date('Y')-1);
                            break;                    
                    }
					
                    if($filterParams->Filter[$i]->f == 'f_timestamp'){
                        if($type == 'ware'){
                                $whereSelector .= " AND (wa.f_timestamp between " . $interval . ")";
                        }
                        else if($type == 'workorder'){
                                $whereSelector .= " AND (wo.f_timestamp between " . $interval . ")";
                        }
                    }
                    else{
                        $whereSelector .= " AND (" . $filterParams->Filter[$i]->f . " between "  . $interval . ")";
                    }
                 }
                 else if($filterParams->Filter[$i]->ty == 4){
                        $whereSelector .= " AND (" . $filterParams->Filter[$i]->f . " = '"  . $filterParams->Filter[$i]->v . "')";
                 }
             }
        }
        
        // convert to SQL order clause
        if($filterParams->Sort){
            for($i = 0; $i < count($filterParams->Sort); $i++){
                if($filterParams->Sort[$i]->f == 'f_code' || $filterParams->Sort[$i]->f == ''){
                        $filterParams->Sort[$i]->f = 'cd.f_id';
                }
                $aux[]= $filterParams->Sort[$i]->f . ' ' . $filterParams->Sort[$i]->m;
            }
            $orderBy = implode(',', $aux);
        }
        
        // convert to SQL join 
        if($filterParams->Selector && count((array)$filterParams->Selector)){
			if($type == 'ware'){
                $joinSelector = " JOIN t_selector_ware sw ON sw.f_ware_id = wa.f_code ";
            }
            else if($type == 'workorder'){
                $joinSelector = " JOIN t_selector_wo sw ON sw.f_wo_id = wo.f_code ";
            }
			
			foreach($filterParams->Selector as $key1 => $value1){
				foreach($value1 as $key2 => $value2){
					if($key2 == 's'){
						$selectors .= implode(',', $value2);
					}
				}
			}
            $whereSelector = " AND sw.f_selector_id IN (" . $selectors . ") AND sw.f_nesting_level = 1 ";
        }
		
		// Ands
		if($filterParams->Ands){
			for($i = 0; $i < count($filterParams->Ands); $i++){
				if($filterParams->Ands[$i]->f == 't_wf_groups.f_id'){

					$joinSelector .= " JOIN t_wf_phases wp ON (wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id)";
					if(in_array($filterParams->Ands[$i]->z, array('=all', '=inbox'))){
						$filterParams->Ands[$i]->z = ' not in (6,7)';
					}
					$whereSelector .= " AND wp.f_group_id " . $filterParams->Ands[$i]->z;
				}
                                else{
                                    $whereSelector .= " AND " . $filterParams->Ands[$i]->f . $filterParams->Ands[$i]->z;
                                }
			}
		}
		else{
			$joinSelector .= " JOIN t_wf_phases wp ON (wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id)";
			$whereSelector .= " AND wp.f_group_id not in (6, 7)";
		}
        // query for SQL SERVER
		if(strtolower($database) == 'sqlserver'){
			switch($type){
				// wares query
				case 'ware':
					
                                        if($history){
                                            $selectData = "SELECT *
                                                            FROM (" . "SELECT cd.f_id AS system_code, " . implode(",", $selectFields) . ", ROW_NUMBER() OVER (ORDER BY " . (((isset($orderBy) && $orderBy != 'f_code')) ? $orderBy : "cd.f_id" ) . ") AS RowNum
                                                                    FROM t_creation_date_history cd JOIN t_wares_history wa ON cd.f_code = wa.f_code 
                                                                    JOIN t_custom_fields_history cf ON wa.f_code = cf.f_code"
                                                                    . (isset($joinSelector) ? $joinSelector : '') .
                                                                    " WHERE 1 = 1 " 
                                                                     . (isset($searchFilter) ? str_replace('_table_', 'wa', $searchFilter) : '') .
                                                                     "AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                                                     . " AND cd.f_timestamp = (SELECT MAX(cd2.f_timestamp) from t_creation_date_history cd2 where cd.f_code = cd2.f_code AND cd2.f_timestamp < " . $history . ")" 
                                                                     . (isset($whereSelector) ? $whereSelector : '') .
                                                            ") AS MyDerivedTable
                                                            WHERE MyDerivedTable.RowNum BETWEEN " . $offset  . " AND " . ($offset + $limit);
                                        }
                                        else{
                                            $selectData = "SELECT *
                                                            FROM (" . "SELECT cd.f_id AS system_code, " . implode(",", $selectFields) . ", ROW_NUMBER() OVER (ORDER BY " . (((isset($orderBy) && $orderBy != 'f_code')) ? $orderBy : "cd.f_id" ) . ") AS RowNum
                                                                    FROM t_creation_date cd JOIN t_wares wa ON cd.f_id = wa.f_code 
                                                                    JOIN t_custom_fields cf ON wa.f_code = cf.f_code"
                                                                    . (isset($joinSelector) ? $joinSelector : '') .
                                                                    " WHERE 1 = 1 " 
                                                                     . (isset($searchFilter) ? str_replace('_table_', 'wa', $searchFilter) : '') .
                                                                     "AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                                                     . (isset($whereSelector) ? $whereSelector : '') .
                                                            ") AS MyDerivedTable
                                                            WHERE MyDerivedTable.RowNum BETWEEN " . $offset  . " AND " . ($offset + $limit);
                                            
                                        }
					break;
                                // workorders query
				case 'workorder':
					if($history){
                                            $selectData = "SELECT *
                                            FROM (" . "SELECT cd.f_code AS system_code, " . implode(",", $selectFields) . ", ROW_NUMBER() OVER (ORDER BY " . (((isset($orderBy) && $orderBy != 'f_code')) ? $orderBy : "cd.f_id" ) . ") AS RowNum
                                                        FROM t_creation_date_history cd JOIN t_workorders wo ON cd.f_id = wo.f_code 
                                                        JOIN t_custom_fields_history cf ON wo.f_code = cf.f_code"
                                                        . (isset($joinSelector) ? $joinSelector : '') .
                                                        " WHERE 1 = 1 "
                                                        . (isset($searchFilter) ? str_replace('_table_', 'wo', $searchFilter) : '') .
                                                         "AND wo.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                                         . " AND cd.f_timestamp = (SELECT MAX(cd2.f_timestamp) from t_creation_date_history cd2 where cd.f_code = cd2.f_code AND cd2.f_timestamp < " . $history . ")" 
                                                         . (isset($whereSelector) ? $whereSelector : '') .
                                            ") AS MyDerivedTable
                                            WHERE MyDerivedTable.RowNum BETWEEN " . $offset  . " AND " . ($offset + $limit);
                                        }
                                        else{
                                            $selectData = "SELECT *
                                            FROM (" . "SELECT cd.f_id AS system_code, " . implode(",", $selectFields) . ", ROW_NUMBER() OVER (ORDER BY " . (((isset($orderBy) && $orderBy != 'f_code')) ? $orderBy : "cd.f_id" ) . ") AS RowNum
                                                        FROM t_creation_date cd JOIN t_workorders wo ON cd.f_id = wo.f_code 
                                                        JOIN t_custom_fields cf ON wo.f_code = cf.f_code"
                                                        . (isset($joinSelector) ? $joinSelector : '') .
                                                        " WHERE 1 = 1 "
                                                                . (isset($searchFilter) ? str_replace('_table_', 'wo', $searchFilter) : '') .
                                                                 "AND wo.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                                                 . (isset($whereSelector) ? $whereSelector : '') .
                                            ") AS MyDerivedTable
                                            WHERE MyDerivedTable.RowNum BETWEEN " . $offset  . " AND " . ($offset + $limit);
                                        }
					break;
				// workorder-ware query
				case 'workorder-ware':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_ware_wo ww JOIN t_wares wa ON ww.f_ware_id  = wa.f_code
										JOIN t_creation_date cd ON cd.f_id = wa.f_code
										JOIN t_custom_fields cf ON wa.f_code = cf.f_code
									WHERE 1 = 1
										 AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id) . "
										 AND ww.f_wo_id = " . $f_code . "
									ORDER BY wa.f_code";
					break;
				// ware-ware query
				case 'ware-ware':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_wares_relations wr JOIN t_wares wa ON wr.f_code_ware_slave  = wa.f_code
										JOIN t_creation_date cd ON cd.f_id = wa.f_code
										JOIN t_custom_fields cf ON wa.f_code = cf.f_code
									WHERE 1 = 1
										 AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id) . "
										 AND wr.f_code_ware_master = " . $f_code . "
									ORDER BY wa.f_code";
					break;
				// ware-workorder query
				case 'ware-workorder':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_ware_wo ww JOIN t_workorders wo ON ww.f_wo_id  = wo.f_code
										JOIN t_creation_date cd ON cd.f_id = wo.f_code
										JOIN t_custom_fields cf ON wo.f_code = cf.f_code
									WHERE 1 = 1
										 AND wo.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id) . "
										 AND ww.f_ware_id = " . $f_code . "
									ORDER BY wo.f_code";
					break;
				// paircross
				case 'ware-paircross':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_pair_cross pc JOIN t_custom_fields cf ON pc.f_code_cross = cf.f_code
                                        JOIN t_creation_date cd ON cd.f_id = cf.f_code
                                        JOIN t_wares wa ON wa.f_code = cf.f_code
									WHERE 1 = 1
										 AND pc.f_code_main = " . $f_code . "
										 AND pc.f_code_cross = " . $f_code2 . "
									ORDER BY pc.f_id DESC";
					break;
                                // paircross
				case 'workorder-paircross':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_pair_cross pc JOIN t_custom_fields cf ON pc.f_code_cross = cf.f_code
                                        JOIN t_creation_date cd ON cd.f_id = cf.f_code
                                        JOIN t_workorders wo ON wo.f_code = cf.f_code
									WHERE 1 = 1
										 AND pc.f_code_main = " . $f_code . "
										 AND pc.f_code_cross = " . $f_code2 . "
									ORDER BY pc.f_id DESC";
					break;
			}
		}
		// query for MYSQL
		else if(strtolower($database) == 'mysql'){
                        switch($type){
				// wares query
				case 'ware':
					if($costs){
                                            $selectData = "SELECT " . implode(",", $selectFields) . "
                                                            FROM (SELECT cd.* FROM t_creation_date_history cd 
                                                                              JOIN t_wf_phases wp ON (wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id) 
                                                                              WHERE 1 = 1 AND cd.f_category = 'ASSET'
                                                                                AND cd.f_timestamp = (SELECT MAX(f_timestamp) FROM t_creation_date_history WHERE f_code = cd.f_code AND f_timestamp <= 1388534400) 
                                                                                AND wp.f_group_id NOT IN (6, 7)
                                                                              GROUP BY cd.f_code) AS t1 
                                                            LEFT JOIN (SELECT cf.fc_asset_maintenance_costs,cf.fc_asset_total_costs_ytd, cf.f_code, cf.f_timestamp 
                                                                        FROM t_wares_history cf 
                                                                        WHERE cf.f_timestamp > 1388448000) AS t2
                                                                 ON (t1.f_timestamp = t2.f_timestamp AND t1.f_code = t2.f_code)";
                                        }
                                        else if($history){
                                            
                                            $selectData = "SELECT cd.f_code AS system_code, " . implode(",", $selectFields) . "
                                                            FROM t_creation_date_history cd JOIN t_wares_history wa ON (cd.f_code = wa.f_code AND cd.f_timestamp = wa.f_timestamp)
                                                                    JOIN t_custom_fields_history cf ON (wa.f_code = cf.f_code AND wa.f_timestamp = cf.f_timestamp)"
                                                                    . (isset($joinSelector) ? $joinSelector : '') .
                                                            " WHERE 1 = 1 " 
                                                                    . " AND cd.f_timestamp = (SELECT MAX(cd2.f_timestamp) from t_creation_date_history cd2 where cd.f_code = cd2.f_code AND cd2.f_timestamp < " . $history . ")"  
                                                                    . (isset($searchFilter) ? str_replace('_table_', 'wa', $searchFilter) : '') 
                                                                    . "AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                                                    . (isset($whereSelector) ? $whereSelector : '')
                                                                    . ((isset($orderBy) && $orderBy != 'f_code') ? " ORDER BY " . $orderBy : " ORDER BY wa.f_code");
                                        }
                                        else{
                                            $selectData = "SELECT cd.f_id AS system_code, " . implode(",", $selectFields) . "
                                                            FROM t_creation_date cd JOIN t_wares wa ON cd.f_id = wa.f_code 
                                                                    JOIN t_custom_fields cf ON wa.f_code = cf.f_code"
                                                                    . (isset($joinSelector) ? $joinSelector : '') .
                                                            " WHERE 1 = 1 " 
                                                                    . (isset($searchFilter) ? str_replace('_table_', 'wa', $searchFilter) : '') 
                                                                    . " AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                                                    . (isset($whereSelector) ? $whereSelector : '')
                                                                    . ((isset($orderBy) && $orderBy != 'f_code') ? " ORDER BY " . $orderBy : " ORDER BY wa.f_code");
                                            
                                        }
					break;
			   // workorders query
				case 'workorder':
                                        if($history){
                                            $selectData = "SELECT cd.f_code AS system_code, " . implode(",", $selectFields) . "
                                                            FROM t_creation_date_history cd JOIN t_workorders_history wo ON (cd.f_code = wo.f_code AND cd.f_timestamp = wo.f_timestamp) 
                                                                    JOIN t_custom_fields_history cf ON (wo.f_code = cf.f_code AND wo.f_timestamp = cf.f_timestamp)"
                                                                    . (isset($joinSelector) ? $joinSelector : '') .
                                                            " WHERE 1 = 1 "
                                                                     . " AND cd.f_timestamp = (SELECT MAX(cd2.f_timestamp) from t_creation_date_history cd2 where cd.f_code = cd2.f_code AND cd2.f_timestamp < " . $history . ")" 
                                                                     . (isset($searchFilter) ? str_replace('_table_', 'wo', $searchFilter) : '') .
                                                                     " AND wo.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                                                     . (isset($whereSelector) ? $whereSelector : '')
                                                                     . ((isset($orderBy) && $orderBy != 'f_code') ? " ORDER BY " . $orderBy : " ORDER BY wo.f_code");
                                        }
                                        else{
                                            $selectData = "SELECT cd.f_id AS system_code, " . implode(",", $selectFields) . "
                                                            FROM t_creation_date cd JOIN t_workorders wo ON cd.f_id = wo.f_code 
                                                                    JOIN t_custom_fields cf ON wo.f_code = cf.f_code"
                                                                    . (isset($joinSelector) ? $joinSelector : '') .
                                                            " WHERE 1 = 1 "
                                                                     . (isset($searchFilter) ? str_replace('_table_', 'wo', $searchFilter) : '') .
                                                                     "AND wo.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                                                     . (isset($whereSelector) ? $whereSelector : '')
                                                                     . ((isset($orderBy) && $orderBy != 'f_code') ? " ORDER BY " . $orderBy : " ORDER BY wo.f_code");
                                        }
	
					break;
				// workorder-ware query
				case 'workorder-ware':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_ware_wo ww JOIN t_wares wa ON ww.f_ware_id  = wa.f_code
										JOIN t_creation_date cd ON cd.f_id = wa.f_code
										JOIN t_custom_fields cf ON wa.f_code = cf.f_code
									WHERE 1 = 1
										 AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id) . "
										 AND ww.f_wo_id = " . $f_code . "
									ORDER BY wa.f_code";
					break;
				// ware-ware query
				case 'ware-ware':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_wares_relations wr JOIN t_wares wa ON wr.f_code_ware_slave  = wa.f_code
										JOIN t_creation_date cd ON cd.f_id = wa.f_code
										JOIN t_custom_fields cf ON wa.f_code = cf.f_code
									WHERE 1 = 1
										 AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id) . "
										 AND wr.f_code_ware_master = " . $f_code . "
									ORDER BY wa.f_code";
					break;
				// ware-workorder query
				case 'ware-workorder':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_ware_wo ww JOIN t_workorders wo ON ww.f_wo_id  = wo.f_code
										JOIN t_creation_date cd ON cd.f_id = wo.f_code
										JOIN t_custom_fields cf ON wo.f_code = cf.f_code
									WHERE 1 = 1
										 AND wo.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id) . "
										 AND ww.f_ware_id = " . $f_code . "
									ORDER BY wo.f_code";
					break;
				// paircross
				case 'ware-paircross':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_pair_cross pc JOIN t_custom_fields cf ON pc.f_code_cross = cf.f_code
                                        JOIN t_creation_date cd ON cd.f_id = cf.f_code
                                        JOIN t_wares wa ON wa.f_code = cf.f_code
									WHERE 1 = 1
										 AND pc.f_code_main = " . $f_code . "
										 AND pc.f_code_cross = " . $f_code2 . "
									ORDER BY pc.f_id DESC";
					break;
                                // paircross
				case 'workorder-paircross':
					$selectData = "SELECT " . implode(",", $selectFields) . "
									FROM t_pair_cross pc JOIN t_custom_fields cf ON pc.f_code_cross = cf.f_code
                                        JOIN t_creation_date cd ON cd.f_id = cf.f_code
                                        JOIN t_workorders wo ON wo.f_code = cf.f_code
									WHERE 1 = 1
										 AND pc.f_code_main = " . $f_code . "
										 AND pc.f_code_cross = " . $f_code2 . "
									ORDER BY pc.f_id DESC";
					break;
			}
			
			$selectData .= ' LIMIT ' . $offset . ',' . $limit;
		}
        
        // execute query
        try{
            $query = $this->db->query($selectData);
            //file_put_contents($this->exportURL . $this->filename . '_query_log.txt', $type . "\n" . $selectData . "\n", FILE_APPEND);
            if($numeric){
                $query->setFetchMode(Zend_Db::FETCH_NUM);
            }
            $resData = $query->fetchAll();
        }
        catch(Exception $e){
            file_put_contents($this->exportURL . $this->filename . '_log.txt', $type . "\n" . $selectData . "\n" . $e->getMessage());
        }
        return $resData;
    }
    
    private function getCountData($type, $id, $filterParams = null, $history = null, $costs = null){
        // convert to SQL where clause
        if($filterParams->Search){
            for($i = 0; $i < count($filterParams->Search); $i++){
                // disambiguate f_code field
                if($filterParams->Search[$i]->f == 'f_code' && $type == 'ware'){
                    $filterParams->Search[$i]->f = 'wa.f_code';
                }
                else if($filterParams->Search[$i]->f == 'f_code' && $type == 'workorder'){
                    $filterParams->Search[$i]->f = 'wo.f_code';
                }
                
                $value = base64_decode($filterParams->Search[$i]->v0);
                
                if($filterParams->Search[$i]->f == 'fc_progress'){
                    $value = ltrim($value, '0');
                }
                
                $searchFilter .= " AND " . $filterParams->Search[$i]->f . " ";
                if($filterParams->Search[$i]->type == 0){
					
                    switch($filterParams->Search[$i]->Sel){
                            // contain
                            case 0:
                                    $searchFilter .= " LIKE '%" . $value . "%' ";
                                    break;
                            // not contain
                            case 1:
                                    $searchFilter .= " NOT LIKE '%" . $value . "%' ";
                                    break;
                            // start with
                            case 2:
                                    $searchFilter .= " LIKE '" . $value . "%' ";
                                    break;
                            // end width
                            case 3:
                                    $searchFilter .= " LIKE '%" . $value . "' ";
                                    break;
                            // equal to
                            case 4:
                                    $searchFilter .= " = '" . $value . "' ";
                                    break;
                            // not equal to
                            case 5:
                                    $searchFilter .= " <> '" . $value . "' ";
                                    break;
                    }
                }
                else if($filterParams->Search[$i]->type == 1){

                    switch($filterParams->Search[$i]->Sel){
                            // equals
                            case 0:
                                    $searchFilter .= " = " . $value . " ";
                                    break;
                            // not equals
                            case 1:
                                    $searchFilter .= " <> " . $value . " ";
                                    break;
                            // greater than
                            case 2:
                                    $searchFilter .= " > " . $value . " ";
                                    break;
                            // less than
                            case 3:
                                    $searchFilter .= " < " . $value . " ";
                                    break;

                    }
                }
            }
        }
        
        // convert to SQL where clause
        if($filterParams->Filter){
             for($i = 0; $i < count($filterParams->Filter); $i++){ 
                 if($filterParams->Filter[$i]->f == 'f_type_id'){
                     $id = $filterParams->Filter[$i]->v;
                 }
                 else if($filterParams->Filter[$i]->ty == 3){
                     
                     switch($filterParams->Filter[$i]->v) {
                        // today
                        case 0:     
                            $interval = mktime(0,0,0) . " AND ". mktime(23,59,59);
                            break;
                        
                        // yesterday
                        case 1:
                            $interval = mktime(0,0,0,date('n'),date('j')-1) . " AND " . mktime(23,59,59,date('n'),date('j')-1);   
                            break;
                        
                        // tomorrow
                        case 2: 
                            $interval = mktime(0,0,0,date('n'),date('j')+1) . " AND " . mktime(23,59,59,date('n'),date('j')+1);
                            break;
                        
                        // this week
                        case 3: 
                            $interval = mktime(0,0,0,date('n'),date('j')-date('N')+1) . " AND " . mktime(23,59,59,date('n'),date('j')-date('N')+7);
                            break;
                        
                        // previous week
                        case 4:  
                            $interval = mktime(0,0,0,date('n'),date('j')-date('N')+1 -7) . " AND " . mktime(23,59,59,date('n'),date('j')-date('N'));
                            break;
                        
                        // next week
                        case 5:
                            $interval = mktime(23,59,59,date('n'),date('j')-date('N') + 8) . " AND " . mktime(0,0,0,date('n'),date('j')-date('N') +14);
                            break;
                        
                        // this month
                        case 6:        
                            $interval = mktime(0,0,0,date('n'),1) . " AND " . mktime(0,0,0,date('n')+1,0);
                            break;
                        
                        // previous month
                        case 7: 
                            $interval = mktime(0,0,0,date('n') - 1,1) . " AND " . mktime(23,59,59,date('n'),0);
                            break;
                        
                        // this year
                        case 8: 
                            $interval = mktime(0,0,0,1,1,date('Y')) . " AND " . mktime(23,59,59,12,31,date('Y'));
                            break;
                        
                        // previous year
                        case 9:
                            $interval = mktime(0,0,0,1,1,date('Y')-1) . " AND ". mktime(23,59,59,12,31,date('Y')-1);
                            break;                    
                    }
					
                    if($filterParams->Filter[$i]->f == 'f_timestamp'){
                        if($type == 'ware'){
                                $whereSelector .= " AND (wa.f_timestamp between " . $interval . ")";
                        }
                        else if($type == 'workorder'){
                                $whereSelector .= " AND (wo.f_timestamp between " . $interval . ")";
                        }
                    }
                    else{
                        $whereSelector .= " AND (" . $filterParams->Filter[$i]->f . " between "  . $interval . ")";
                    }
                 }
                 else if($filterParams->Filter[$i]->ty == 4){
                        $whereSelector .= " AND (" . $filterParams->Filter[$i]->f . " = '"  . $filterParams->Filter[$i]->v . "')";
                 }
             }
        }
        
        // convert to SQL order clause
        if($filterParams->Sort){
            for($i = 0; $i < count($filterParams->Sort); $i++){
                if($filterParams->Sort[$i]->f == 'f_code' || $filterParams->Sort[$i]->f == ''){
                        $filterParams->Sort[$i]->f = 'cd.f_id';
                }
                $aux[]= $filterParams->Sort[$i]->f . ' ' . $filterParams->Sort[$i]->m;
            }
            $orderBy = implode(',', $aux);
        }
        
        // convert to SQL join 
        if($filterParams->Selector && count((array)$filterParams->Selector)){
            if($type == 'ware'){
                $joinSelector = " JOIN t_selector_ware sw ON sw.f_ware_id = wa.f_code ";
            }
            else if($type == 'workorder'){
                $joinSelector = " JOIN t_selector_wo sw ON sw.f_wo_id = wo.f_code ";
            }
			
            foreach($filterParams->Selector as $key1 => $value1){
                foreach($value1 as $key2 => $value2){
                    if($key2 == 's'){
                        $selectors .= implode(',', $value2);
                    }
                }
            }
            $whereSelector = " AND sw.f_selector_id IN (" . $selectors . ") AND sw.f_nesting_level = 1 ";
        }
		
        // Ands
        if($filterParams->Ands){
            for($i = 0; $i < count($filterParams->Ands); $i++){
                if($filterParams->Ands[$i]->f == 't_wf_groups.f_id'){

                    $joinSelector .= " JOIN t_wf_phases wp ON (wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id)";
                    if(in_array($filterParams->Ands[$i]->z, array('=all', '=inbox'))){
                            $filterParams->Ands[$i]->z = ' not in (6,7)';
                    }
                    $whereSelector .= " AND wp.f_group_id " . $filterParams->Ands[$i]->z;
                }
                else{
                    $whereSelector .= " AND " . $filterParams->Ands[$i]->f . $filterParams->Ands[$i]->z;
                }
            }
        }
        else{
            $joinSelector .= " JOIN t_wf_phases wp ON (wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id)";
            $whereSelector .= " AND wp.f_group_id not in (6, 7)";
        }
        
        switch($type){
            // wares query
            case 'ware':
                
                if($costs){
                    $selectData = "SELECT count(*)
                                    FROM (SELECT cd.* FROM t_creation_date_history cd 
                                                      JOIN t_wf_phases wp ON (wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id) 
                                                      WHERE 1 = 1 AND cd.f_category = 'ASSET'
                                                        AND cd.f_timestamp = (SELECT MAX(f_timestamp) FROM t_creation_date_history WHERE f_code = cd.f_code AND f_timestamp <= 1388534400) 
                                                        AND wp.f_group_id NOT IN (6, 7)
                                                      GROUP BY cd.f_code) AS t1 
                                    LEFT JOIN (SELECT cf.fc_asset_maintenance_costs,cf.fc_asset_total_costs_ytd, cf.f_code, cf.f_timestamp 
                                                FROM t_wares_history cf 
                                                WHERE cf.f_timestamp > 1388448000) AS t2
                                         ON (t1.f_timestamp = t2.f_timestamp AND t1.f_code = t2.f_code)";
                }
                else if($history){
                    
                    $selectData = "SELECT COUNT(DISTINCT(cd.f_code)) AS tot
                                FROM t_creation_date_history cd JOIN t_wares_history wa ON (cd.f_code = wa.f_code AND cd.f_timestamp = wa.f_timestamp) 
                                JOIN t_custom_fields_history cf ON (wa.f_code = cf.f_code AND wa.f_timestamp = cf.f_timestamp)"
                                . (isset($joinSelector) ? $joinSelector : '') .
                                " WHERE 1 = 1 " 
                                     . (isset($searchFilter) ? $searchFilter : '') .
                                     "AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                     . "AND cd.f_timestamp < " . $history 
                                     . (isset($whereSelector) ? $whereSelector : '');
                }
                else{
                    $selectData = "SELECT COUNT(*) AS tot
                                FROM t_creation_date cd JOIN t_wares wa ON cd.f_id = wa.f_code
                                JOIN t_custom_fields cf ON wa.f_code = cf.f_code"
                                . (isset($joinSelector) ? $joinSelector : '') .
                                " WHERE 1 = 1 " 
                                     . (isset($searchFilter) ? $searchFilter : '') .
                                     "AND wa.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                     . (isset($whereSelector) ? $whereSelector : '');
                }
                break;
            // workorders query
            case 'workorder':
                if($history){
                    $selectData = "SELECT COUNT(DISTINCT(cd.f_code)) AS tot
                                FROM t_creation_date_history cd JOIN t_workorders_history wo ON (cd.f_code = wo.f_code AND cd.f_timestamp = wo.f_timestamp) 
                                                                JOIN t_custom_fields_history cf ON (wo.f_code = cf.f_code AND wo.f_timestamp = cf.f_timestamp)"
                                . (isset($joinSelector) ? $joinSelector : '') .
                                " WHERE 1 = 1 "
                                    . (isset($searchFilter) ? str_replace('_table_', 'wa', $searchFilter) : '') .
                                     "AND wo.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                     . "AND cd.f_timestamp < " . $history
                                     . (isset($whereSelector) ? $whereSelector : '');
                }
                else{
                    $selectData = "SELECT COUNT(*) AS tot
                                FROM t_creation_date cd JOIN t_workorders wo ON cd.f_id = wo.f_code 
                                                                JOIN t_custom_fields cf ON wo.f_code = cf.f_code"
                                . (isset($joinSelector) ? $joinSelector : '') .
                                " WHERE 1 = 1 "
                                    . (isset($searchFilter) ? str_replace('_table_', 'wa', $searchFilter) : '') .
                                     "AND wo.f_type_id " . (is_array($id) ? (' in (' . implode(',', $id) . ')') : ' = ' . $id)
                                     . (isset($whereSelector) ? $whereSelector : '');
                }
                break;
        }
        
        // execute query
        try{
            $query = $this->db->query($selectData);
            
            if($numeric){
                $query->setFetchMode(Zend_Db::FETCH_NUM);
            }
            $resData = $query->fetchAll();
        }
        catch(Exception $e){
            file_put_contents($this->exportURL . $this->filename . '_log.txt', $type . "\n" . $selectData . "\n" . $e->getMessage());
            die();
        }
        return $resData;
    }
    
    
    private function getConfig($filename, $module){ 
        if (file_exists($filename)) {
            $xml = simplexml_load_file($filename);
         } 
         // config file not exists
         else{
            file_put_contents($this->exportURL . $this->filename . '_log.txt', 'Failed to open ' . $filename);
         }
         
         // loop config
         foreach($xml as $key => $value){
            // search data config by module
            if($key == 'items'){
                
                foreach($value as $key2 => $value2){
                    if((string)$value2->module == $module){
                        $config['module'] = (string)$value2->module;
                        $config['type'] = (string)$value2->type;
                        $config['title'] = (string)$value2->title;
                        $config['id'] = explode(',', (string)$value2->id);
                        $config['widthConvFactor'] = (string)$value2->widthConvFactor;
                        if($value2->subItems){
                            for($i = 0; $i < count($value2->subItems->subItem); $i++){
                                $aux = array(
                                    'module' => (string)$value2->subItems->subItem[$i]->module,
                                    'type' => (string)$value2->subItems->subItem[$i]->type,
                                    'title' => (string)$value2->subItems->subItem[$i]->title,
                                    'id' => (string)$value2->subItems->subItem[$i]->id,

                                );
                                if((string)$value2->subItems->subItem[$i]->pairCross){
                                    $aux['pairCross']['module'] = (string)$value2->subItems->subItem[$i]->pairCross;

                                }
                                $config['subData'][] = $aux;
                            }
                        }
                    }
                }
            }
            else if($key == 'styles'){
                foreach($value as $key2 => $value2){
                    $styleArray = array();
                    foreach($value2 as $key3 => $value3){
                        if($key3 == 'name'){
                            $styleName = (string)$value3;
                        }
                        else{
                            $styleArray[$key3] = array();
                            if(is_object($value3)){
                                foreach($value3 as $key4 => $value4){
                                    $styleArray[$key3][$key4] = (string)$value4;
                                }
                            }
                            else{
                                
                            }
                        }   
                    }
                    $config['styles'][$styleName] = $styleArray;
                }
            }
            else if($key == 'database'){
                $config['database'] = (string)$value;
            }
            else if($key == 'lang'){
                $config['lang'] = (string)$value;
            }
                
         }
         return $config;
    }
    
    /*
     * get the sheets configuration to export
     * 
     * @param filename : config file
     */
    public function getConfigSheetsTPL($filename){
        
        if (file_exists($filename)) {
            $xml = simplexml_load_file($filename);
        } 
        // config file not exists
        else{
            file_put_contents($this->exportURL . $this->filename . '_log.txt', 'Failed to open ' . $filename);
        }
        
       

        // read xml part for excel sheets
        foreach($xml->sheets as $sheet){
            foreach($sheet as $data){
                if($data->export == 'yes'){
                    $sheetArray = null;
                    $sheetArray['fields'] = array();
                    $sheetArray['name'] = (string)$data->name;
                    $sheetArray['type'] = (string)$data->type;
                    
                    
                    if($data['type'][0] == 'append'){
                        $sheetArray['append'] = true; 
                    }
                    
                    if($data->parents){
                        $sheetArray['parents'] = array(
                            'childCode' => (string)$data->parents['childCode'],
                            'field' => (string)$data->parents,
                            'label' => $data->parents['alias'] ? (string)$data->parents['alias'] : (string)$data->parents
                        );
                        
                        $sheetArray['fields'][] = array(
                            'field' => (string)$data->parents,    
                            'label' => $data->parents['alias'] ? (string)$data->parents['alias'] : (string)$data->parents,
                            'position' => (int)$data->parents['pos'],
                            'table' => '',
                            'style' => $data->parents['style'] ? (string)$data->parents['style'] : null,
                            'styleTitle' => $data->parents['styleTitle'] ? (string)$data->parents['styleTitle'] : null,
                            'operation' => $field[0]['operation'] ? (string)$field[0]['operation'] : null,
                            'type' => $field[0]['type'] ? (string)$field[0]['type'] : 's'
                        );
                    }
                    
                    if($data->constants){
                        foreach($data->constants->constant as $k => $v){

                            $sheetArray['fields'][] = array(
                                'field' => (string)$v['alias'][0],
                                'label' => (string)$v['alias'][0],
                                'position' => (string)$v['pos'][0],
                                'table' => '',
                                'style' => (string)$v['style'][0],
                                'styleTitle' => (string)$v['styleTitle'][0],
                                'constant' => (string)$v,
                                'operation' => $v['operation'][0] ? (string)$v['operation'][0] : null,
                                'type' => $v['type'][0] ? (string)$v['type'][0] : 's'
                            );
                        }
                    }
                    

                    if(isset($data->autoincrement)){
                        $sheetArray['autoincrement'] = (string)$data->autoincrement;
                        $sheetArray['fields'][] = array(
                            'field' => '',
                            'label' => (string)$data->autoincrement,
                            'position' => (string)$data->autoincrement['pos'][0],
                            'table' => '',
                            'type' => 'autoincrement'
                        );
                    }
                    // loop queries
                    $queryNum = 1;
                    foreach($data->queries->query as $key => $q){

                        $query = array();
                        $auxFields = array();
                        $query['id'] = 'q' . $queryNum;
                        $query['type'] = (string)$q['type'];
                        
                        
                        // eventually add custom fields for the main query
                        if($queryNum == 1){
                            $auxCustomFields = null;
                            $auxCustomFields2 = null;
                            if($xml->custom){
                                foreach($xml->custom->relations->rel as $rel){
                                    if((string)$rel->name == (string)$data->name){
                                        foreach($rel->fields->field as $field){
                                            $auxCustomFields[] = array(
                                                'field' => (string)$field[0],
                                                'label' => $field[0]['alias'] ? (string)$field[0]['alias'] : (string)$field,
                                                'position' => '',
                                                'table' => 't_custom_fields',
                                                'style' => $field[0]['style'] ? (string)$field[0]['style'] : null,
                                                'styleTitle' => $field[0]['styleTitle'] ? (string)$field[0]['styleTitle'] : null,
                                                'query' => 'q1',
                                                'operation' => $field[0]['operation'] ? (string)$field[0]['operation'] : null,
                                                'type' => $field[0]['type'] ? (string)$field[0]['type'] : 's'
                                            ); 
                                            $query['fields']['q1.t_custom_fields.'  . (string)$field] = 't_custom_fields.' . (string)$field;
                                            $auxCustomFields2['t_custom_fields.' . (string)$field] = (string)$field;
                                        }        
                                    }
                                }
                            }
                        }

                        foreach($q->from->fields->field as $field){
							//Aggiunta alias table in quanto non funzionavano le join con alias
							 $alias_table=(string)$q->from->table['alias'] ? (string)$q->from->table['alias'] : (string)$q->from->table;
							
                            $auxFields[ $alias_table . '.'  . (string)$field] = (string)$field;
                            
                            if($field[0]['flat']){
                                $query['fields']['q' . $queryNum . '.'  .  $alias_table . '.' . (string)$field] = new Zend_Db_Expr('GROUP_CONCAT(' .  $alias_table . '.'  . (string)$field . ')');
                            }
                            else{
                                $query['fields']['q' . $queryNum . '.'  .  $alias_table . '.' . (string)$field] = (string)$q->from->table . '.'  . (string)$field;
                            }
                            
                            if($field[0]['show'] != "n"){
                                $sheetArray['fields'][] = array(
                                    'field' => (string)$field,
                                    'label' => $field[0]['alias'] ? (string)$field[0]['alias'] : (string)$field,
                                    'position' => (string)$field['pos'],
                                    'table' =>  $alias_table,
                                    'style' => $field[0]['style'] ? (string)$field[0]['style'] : null,
                                    'styleTitle' => $field[0]['styleTitle'] ? (string)$field[0]['styleTitle'] : null,
                                    'query' => 'q' . $queryNum,
                                    'operation' => $field[0]['operation'] ? (string)$field[0]['operation'] : null,
                                    'type' => $field[0]['type'] ? (string)$field[0]['type'] : 's'
                                ); 
                            }
                        }

                        $query['from'] = array(
                            'table' =>  $alias_table,
                            'fields' => $auxFields
                        );
                        
                        // eventually add custom field
                        if((string)$q->from->table == 't_custom_fields' && isset($auxCustomFields2)){
                            $query['fields'] += $auxCustomFields2;
                        }
                        $sheetArray['tables'][] =  $alias_table;

                        if($q->joins){
							 
                            foreach($q->joins->join as $join => $value){
								//2 Aggiunta alias table in quanto non funzionavano le join con alias
								$alias_table=(string)$value->table['alias'] ? (string)$value->table['alias'] : (string)$value->table;
                                $auxFields = array();
                                if($value->fields){
                                    foreach($value->fields->field as $field){
                                        $auxFields[(string)$value->table . '.' . (string)$field] = (string)$field;
                                        if($field[0]['flat']){
                                            $query['fields']['q' . $queryNum . '.'  .  $alias_table . '.' . (string)$field] = new Zend_Db_Expr('GROUP_CONCAT(' .  $alias_table . '.'  . (string)$field . ')');
                                        }
                                        else{
                                            $query['fields']['q' . $queryNum . '.' .  $alias_table . '.' . (string)$field] =  $alias_table. '.' . (string)$field;
                                        }
                                    }
                                }

                                if($value->fields){
                                    foreach($value->fields->field as $f){
                                        
                                        if($f[0]['show'] != 'n'){
                                            $sheetArray['fields'][] = array(
                                                'field' => (string)$f[0],
                                                'label' => $f[0]['alias'] ? (string)$f[0]['alias'] : (string)$f[0],
                                                'position' => (string)$f['pos'],
                                                'table' => $value->table['alias'] ? (string)$value->table['alias'] : (string)$value->table,
                                                'style' => $f[0]['style'] ? (string)$f[0]['style'] : null,
                                                'styleTitle' => $f[0]['styleTitle'] ? (string)$f[0]['styleTitle'] : null,
                                                'query' => 'q' . $queryNum,
                                                'operation' => $f[0]['operation'] ? (string)$f[0]['operation'] : null,
                                                'type' => $f[0]['type'] ? (string)$f[0]['type'] : ''
                                            );
                                        }
                                    }
                                }

                                // eventually add custom fields info 
                                if((string)$value->table == 't_custom_fields' && isset($auxCustomFields2)){
                                    $auxFields = $auxFields + $auxCustomFields2;
                                }

                                $query['joins'][] = array(
                                    'table' => (string)$value->table,
                                    'on' => (string)$value->on,
                                    'fields' => $auxFields,
                                    'alias' => (string)$value->table['alias']
                                );
                                
                                $query['tables'][] = (string)$value->table;
                            }
                        }

                        if($q->wheres){
                             foreach($q->wheres->where as $where => $value){
                                 $query['wheres'][] = (string)$value;
                                 if($value[0]['type'] == 'mainQueryRelation'){
                                     $query['mainQueryRelation'][] = 'q1.' . (string)$value[0]['field'];
                                 }
                             }
                        }
                        
                        if($q->orders){
                             foreach($q->orders->order as $order => $value){
                                 $query['orders'][] = (string)$value;
                             }
                        }
                        
                        $sheetArray['query'][] = $query;
                        $queryNum++;
                    }
                    
                    // order fields by position
                    /*
                    $positionArray = array_fill(0, count($sheetArray['fields']), '');
                    $noPositionArray = null;
                    $lastIndex = 0;
                    for($k = 0; $k < count($sheetArray['fields']); $k++){
                        if($sheetArray['fields'][$k]['position']){
                            $positionArray[$sheetArray['fields'][$k]['position'] - 1] = $sheetArray['fields'][$k];
                            $lastIndex++;
                        }
                        else{
                            $noPositionArray[] = $sheetArray['fields'][$k];
                        }
                    }
                    // add custom fields in tail of position array
                    for($k = 0; $k < count($noPositionArray); $k++){
                        $positionArray[$lastIndex++] = $noPositionArray[$k];
                    }
                    $sheetArray['fields'] = $positionArray;
                    
                    $config[] = $sheetArray;
                     * 
                     * 
                     */
                    $orderedFields = null;
                    for($i = 0; $i < count($sheetArray['fields']); $i++){
                        for($j = 0; $j < count($sheetArray['fields']); $j++){
                            if($sheetArray['fields'][$j]['position'] == ($i + 1)){
                                if($sheetArray['fields'][$j]['type'] == 'autoincrement'){
                                    $orderedFields['autoincrement'] = $sheetArray['fields'][$j];
                                }
                                else{
                                    $orderedFields[$sheetArray['fields'][$j]['query'] . '.' . $sheetArray['fields'][$j]['table'] . '.' .$sheetArray['fields'][$j]['field']] = $sheetArray['fields'][$j];
                                }
                                break;
                            }
                        }
                    }
                    
                    // eventually add custom fields
                    $position = count($orderedFields) + 1;
                    for($i = 0; $i < count($auxCustomFields); $i++){
                        $auxCustomFields[$i]['position'] = $position;
                        $position++;
                        $orderedFields[$auxCustomFields[$i]['query'] . '.' . $auxCustomFields[$i]['table'] . '.' . $auxCustomFields[$i]['field']] = $auxCustomFields[$i];
                    }
                    
                    $sheetArray['fields'] = $orderedFields;
                    $config[] = $sheetArray;
                } 
            }
        }//var_dump($sheetArray['query']);
        return $config;
    }
    
    /*
     * prepare the query to retrieve data for a sheet
     * 
     * @param 
     */
    public function createSelectTPL($queryConfig, $params = null){
        if(is_array($params)){
            array_walk_recursive($queryConfig, function(&$item, $key) use(&$params) {
                for($i = 0; $i < count($params); $i++){
                    $item = str_replace("[param" . $i . "]", "'" . $params[$i] . "'", $item);
                }
            });
        }
        
        // from
        $selectData = $this->db->select()
                                   ->from($queryConfig['from']['table'], array()/*$configSheet['from']['fields']*/);
        $selectCount = $this->db->select()
                                    ->from($queryConfig['from']['table'], array('num' => 'COUNT(*)'), array());
        // join
        for($i = 0; $i < count($queryConfig['joins']); $i++){
            if($queryConfig['joins'][$i]['type'] == 'left'){
                $selectData->joinLeft($queryConfig['joins'][$i]['alias'] ? array($queryConfig['joins'][$i]['alias'] => $queryConfig['joins'][$i]['table']) : $queryConfig['joins'][$i]['table'], $queryConfig['joins'][$i]['on'], array()/*$configSheet['joins'][$i]['fields']*/);
                $selectCount->joinLeft($queryConfig['joins'][$i]['alias'] ? array($queryConfig['joins'][$i]['alias'] => $queryConfig['joins'][$i]['table']) : $queryConfig['joins'][$i]['table'], $queryConfig['joins'][$i]['on'], array());
            }
            else{
                $selectData->join($queryConfig['joins'][$i]['alias'] ? array($queryConfig['joins'][$i]['alias'] => $queryConfig['joins'][$i]['table']) : $queryConfig['joins'][$i]['table'], $queryConfig['joins'][$i]['on'], array()/*$configSheet['joins'][$i]['fields']*/);
                $selectCount->join($queryConfig['joins'][$i]['alias'] ? array($queryConfig['joins'][$i]['alias'] => $queryConfig['joins'][$i]['table']) : $queryConfig['joins'][$i]['table'], $queryConfig['joins'][$i]['on'], array());
            }
        }
        
        // where
        for($i = 0; $i < count($queryConfig['wheres']); $i++){
            $selectData->where($queryConfig['wheres'][$i]);
            $selectCount->where($queryConfig['wheres'][$i]);
        }
        
        // order
        for($i = 0; $i < count($queryConfig['orders']); $i++){
            $selectData->order($queryConfig['orders'][$i]);
            //$selectCount->order($queryConfig['orders'][$i]);
        }
        
        // relations with main query
        $select['mainQueryRelation'] = $queryConfig['mainQueryRelation'];
        $select['type'] = $queryConfig['type'];

        
        // get select fields
        /*
        for($i = 0; $i < count($queryConfig['fields']); $i++){
            if(is_array($queryConfig['fields'][$i])){
                if($queryConfig['fields'][$i]['table']){
                    /*
                    if($queryConfig['fields'][$i]['label']){
                        $selectFields[$queryConfig['fields'][$i]['table'] . '.' . $queryConfig['fields'][$i]['label']] = $queryConfig['fields'][$i]['table'] . '.' . $queryConfig['fields'][$i]['field'];
                    }
                    else{
                        $selectFields[$queryConfig['fields'][$i]['table'] . '.' . $queryConfig['fields'][$i]['field']] = $queryConfig['fields'][$i]['table'] . '.' . $queryConfig['fields'][$i]['field'];
                    }
                    $selectFields[$queryConfig['fields'][$i]['table'] . '.' . $queryConfig['fields'][$i]['field']] = $queryConfig['fields'][$i]['table'] . '.' . $queryConfig['fields'][$i]['field'];
                }
                elseif($queryConfig['fields'][$i]['constant']){
                    $selectFields[] = new Zend_Db_Expr ('"' . $queryConfig['fields'][$i]['constant'] . '" AS ' . $queryConfig['fields'][$i]['label']);
                }
                else{
                    $selectFields[] = new Zend_Db_Expr ('"0" AS ' . str_replace(array("/", " "), "_", $queryConfig['fields'][$i]['label']));
                }
            }
        }*/
        
        /*
        if($configSheet['parents']){
            $selectFields['childCode'] =  $configSheet['parents']['childCode'];
        }*/
        /*
        foreach($queryConfig['fields'] as $key => $value){
           $selectFields[] = $key;  
        }
        */
        $selectData->columns($queryConfig['fields']);
        echo $selectData;
        $select['data'] = $selectData;
        $select['count'] = $selectCount;
        return $select;
    }
    
    /*
     * get the config styles for the excel
     * 
     * @param filename : config file
     */
    public function getConfigStylesTPL($filename){
        
        if (file_exists($filename)) {
            $xml = simplexml_load_file($filename);
        } 
        // config file not exists
        else{
            file_put_contents($this->exportURL . $this->filename . '_log.txt', 'Failed to open ' . $filename);
        }
        
        // read xml part for style definition
        foreach($xml->styles->style as $style){

            $styleArray = array();
            foreach($style as $key => $value){
                if($key == 'name'){
                    $styleName = (string)$value;
                }
                else{
                    if(is_object($value)){
                        $styleArray[(string)$key] = (array)$value;
                    }
                    else{
                        $styleArray[(string)$key] = (string)$value;
                    }
                }   
            }
            $config[$styleName] = $styleArray;
        }
        return $config;
    }


    /*
     * get the field configuration to export
     * 
     * @param filename : config file
     * @param tables : array containing the tables from which retrieve fields config
     */
    public function getConfigOperationDataTPL($filename){
        
        if (file_exists($filename)) {
            $xml = simplexml_load_file($filename);
        } 
        // config file not exists
        else{
            file_put_contents($this->exportURL . $this->filename . '_log.txt', 'Failed to open ' . $filename);
        }
        
        if($xml->operations){
            foreach($xml->operations->operation as $op){


                if($op->map){
                    $map = null;
                    foreach($op->map->value as $v){
                        $map[(string)$v['old']] = (string)$v;
                    }
                    $operations[(string)$op->name]['map'] = $map;
                }

                if($op->transform){
                    $operations[(string)$op->name]['transform'] = (string)$op->transform->function;
                }
            }
        }
        return $operations;
    }
    
    public function getParentsTPL($f_code, $field, $type, $name){
        
        if($type == 'ware'){
            $table = 't_wares_parent';
        }
        else if($type == 'workorder'){
            $table = 't_workorders_parent';
        }
        else if($type == 'selector'){
            $table = 't_selectors_parent';
        }
        
        try{
            $select = $this->db->select()
                                ->from($table, array())
                                ->join('t_creation_date', 't_creation_date.f_id = ' . $table . '.f_code', array())
                                ->join('t_custom_fields', 't_creation_date.f_id = t_custom_fields.f_code', array())
                                ->where($table . '.f_code=' . $f_code)
                                ->where('f_active = 1');
            $select->columns(array($field));
            $aux = $select->query()->fetchAll();
        }
        catch(Exception $e){
            file_put_contents($this->exportURL . $this->filename . '_log.txt', "\n" . $name . "\n" . $e->getMessage() . "\n" . $select, FILE_APPEND);
        }
        
        $parents = array();
        $aux2 = explode(".", $field); 
        for($i = 0; $i < count($aux); $i++){
           $parents[] = $aux[$i][$aux2[1]];
        }

        return $parents;
    }
    
    public function custom($filename, $template, $debug = false){
        
        $this->filename = $filename;
        
        // init excel library
        $objExcel = new ExcelStreamer();
        $objExcel->createXLSX($this->filename);
        
        // get styles config
        $configStyles = $this->getConfigStylesTPL($template);
        // add style to excel
        $objExcel->addStylesXLSX($configStyles);
        // get sheets config to export
        $configSheets = $this->getConfigSheetsTPL($template);
        // get operation data
        $operationData = $this->getConfigOperationDataTPL($template);
        
        // loop thorugh sheets retrieved by configuration file
        for($i = 0; $i < count($configSheets); $i++){
            
            $selectArray = null;
            // create select queries array ( index 0 = main query);
            for($q = 0; $q < count($configSheets[$i]['query']); $q++){
                $selectArray[] = $this->createSelectTPL($configSheets[$i]['query'][$q]);
            }
            
            if(!$configSheets[$i]['append']){
                // create new work sheet
                $objExcel->addSheetXLSX(null, $configSheets[$i]['name']);
                // write excel columns row
                $columnsData = array();
                foreach($configSheets[$i]['fields'] as $value){
                    $columnsData[] = array($value['label'], 'inlineStr', 0, 0 , $value['styleTitle'] ? $value['styleTitle'] : null, 0, 0);
                    //$columns[] = array('width' => round($value['width'] / ($config['widthConvFactor'] * 6.5)));
                }
                $objExcel->writeRowXLSX($columnsData, 1, 'columns');
                $rowCounter = 2;
            }
            $startQueryRow = 0;
            
            // debug
            if($debug){
                $limit = 100;
                $countData = 100;
            }
            else{
                $limit = 1000;
            }
            
            while(true){
                // get data from database
                try{
                    $rawData = $selectArray[0]['data']->limit($limit, $startQueryRow)->query()->fetchAll();
                }
                catch(Exception $e){
                    file_put_contents($this->exportURL . $this->filename . '_log.txt', "\n-------------------------------------\n" . $configSheets[$i]['name'] . "\n-------------------------------------\n" . $e->getMessage() . "\n" . $selectArray[0]['data'], FILE_APPEND);
                }
                if(count($rawData) == 0){
                    break;
                }
                
                for($j = 0; $j < count($rawData); $j++){
                    
                    if(count($selectArray) > 1){
                        $maxSubRow = 0;
                        $subRawData = null;
                        for($z = 1; $z < count($selectArray); $z++){
                            $query = $selectArray[$z]['data'];
                            for($h = 0; $h < count($selectArray[$z]['mainQueryRelation']); $h++){
                                $query = str_replace('[field' . ($h + 1) . ']', $rawData[$j][$selectArray[$z]['mainQueryRelation'][$h]], $query);
                            }
                            
                            try{
                                $subRawData[$z - 1] = $this->db->query($query)->fetchAll();
                                if(count($subRawData[$z - 1]) > $maxSubRow){
                                    $maxSubRow = count($subRawData[$z - 1]);
                                }
                            }
                            catch(Exception $e){
                                file_put_contents($this->exportURL . $this->filename . '_log.txt', $configSheets[$i]['name'] . " subquery\n" . $e->getMessage() . "\n" . $query . "\n", FILE_APPEND);
                            }
     
                        }  
                        
                        if($maxSubRow > 0){
                            for($z = 0; $z < $maxSubRow; $z++){
                                $unformattedData = $rawData[$j];
                                for($t = 0; $t < count($subRawData); $t++){
                                    if(is_array($subRawData[$t][$z])){
                                        $unformattedData += $subRawData[$t][$z];
                                    }
                                }

                                // format data for the excel
                                $formattedData = $this->formatExcelDataTPL($unformattedData, $operationData, $configSheets[$i]['fields']);
                                // put data into datasheet
                                $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                                // increment number of rows processed
                                $rowCounter++;
                            }
                        }
                        else{
                            // format data for the excel
                            $formattedData = $this->formatExcelDataTPL($rawData[$j], $operationData, $configSheets[$i]['fields']);
                            // put data into datasheet
                            $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                            // increment number of rows processed
                            $rowCounter++;
                            
                        }
                        
                        $progressCounter++;
                       
                    }
                    else{
                        // format data for the excel
                        $formattedData = $this->formatExcelDataTPL($rawData[$j], $operationData, $configSheets[$i]['fields']);
                        // put data into datasheet
                        $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                        // increment number of rows processed
                        $rowCounter++;
                        $progressCounter++;
                    }
                    
                    // eventually add counter field
                    if(isset($configSheets[$i]['autoincrement'])){
                       $rawData[$j] =  array('autoincrement' => $rowCounter) + $rawData[$j];
                    }
                   
                    // output
                    if($mode == "shell"){
                       echo "\r" . $progressCounter . "/" . $countData;
                    }
                }
                // debug
                if($debug){
                    break;
                }
                $startQueryRow = $startQueryRow + $limit;
            }
        }
        
    }
    
    
    public function template($filename, $tplType = 'mainsim', $mode = "shell", $debug = false, $tplFile = null){
        $this->filename = $filename;
        
        // output
        if($mode == "shell"){
            echo "\n---------------------------------\n";
            echo "TEMPLATE EXPORT";
            echo "\n---------------------------------\n";
            echo "start : " . date("H:i:s");
        }

        // init excel library
        $objExcel = new ExcelStreamer();
        $objExcel->createXLSX($this->filename);
        
        // get styles config
        $configStyles = $this->getConfigStylesTPL($this->exportConfigURL . ($tplFile ? $tplFile : 'exportTemplate.xml'));
        // add style to excel
        $objExcel->addStylesXLSX($configStyles);
        // get sheets config to export
        $configSheets = $this->getConfigSheetsTPL($this->exportConfigURL . ($tplFile ? $tplFile : 'exportTemplate.xml'));
        // get operation data
        $operationData = $this->getConfigOperationDataTPL($this->exportConfigURL . ($tplFile ? $tplFile : 'exportTemplate.xml'));
        
        // loop thorugh sheets retrieved by configuration file
        for($i = 0; $i < count($configSheets); $i++){
            $selectArray = null;
            // create select queries array ( index 0 = main query);
            for($q = 0; $q < count($configSheets[$i]['query']); $q++){
                $selectArray[] = $this->createSelectTPL($configSheets[$i]['query'][$q]);
            }
            
            if(!$configSheets[$i]['append']){
                // create new work sheet
                $objExcel->addSheetXLSX(null, $configSheets[$i]['name']);
                // write excel columns row
                $columnsData = array();
                foreach($configSheets[$i]['fields'] as $value){
                    $columnsData[] = array($value['label'], 'inlineStr', 0, 0 , $value['styleTitle'] ? $value['styleTitle'] : null, 0, 0);
                    //$columns[] = array('width' => round($value['width'] / ($config['widthConvFactor'] * 6.5)));
                }
                $objExcel->writeRowXLSX($columnsData, 1, 'columns');
                $rowCounter = 2;
                
            }
            $startQueryRow = 0;
            
            

            // debug
            if($debug){
                $limit = 100;
                $countData = 100;
            }
            else{
                // get count data from database
                /*
                try{

                    $aux = $selectArray[0]['count']->query()->fetch();
                }
                catch(Exception $e){
                    file_put_contents($this->exportURL . $this->filename . '_log.txt', $configSheets[$i]['name'] . "\n" . $e->getMessage() . "\n" . $select['count'], FILE_APPEND);
                }
                
                if(!$configSheets[$i]['append']){
                    $countData = $aux['num'];
                }
                else{
                    $countData += $aux['num'];
                }
                */
                $limit = 1000;
            }
            
            
            // output            
            if($mode == "shell"){
                if(!$configSheets[$i]['append']){
                    echo "\n\n" . $configSheets[$i]['name'] . " (" . $countData . " items)\n";
                }
                else{
                    echo "\n\n" . $configSheets[$i]['name'] . " append (" . $countData . " items)\n";
                }
            }
            else if($mode == "ajax"){
                $progressAjax['sheets'] = array(
                    'counter' => $i + 1,
                    'total' => count($configSheets)
                );
                $progressAjax['filename'] = $this->exportURL . $this->filename;
                file_put_contents($this->exportURL . $this->filename . '.txt', json_encode($progressAjax));
            }
            $progressCounter  = 0;
            while(true){
                // get data from database
                try{
                    //echo $selectArray[0]['data']; die();
                    //file_put_contents($this->exportURL . 'query_log.txt', $selectArray[0]['data']);
                    $rawData = $selectArray[0]['data']->limit($limit, $startQueryRow)->query()->fetchAll();
                }
                catch(Exception $e){
                    file_put_contents($this->exportURL . $this->filename . '_log.txt', "\n-------------------------------------\n" . $configSheets[$i]['name'] . "\n-------------------------------------\n" . $e->getMessage() . "\n" . $selectArray[0]['data'], FILE_APPEND);
                }
                if(count($rawData) == 0){
                    break;
                }
                
                // init params for parents retrieving
                /*
                if(isset($configSheets[$i]['parents'])){
                    
                    print_r($configSheets[$i]); die();
                    if($configSheets[$i]['type'] == 'ware'){
                         if($configSheets[$i]['name'] == 't_wares_users'){
                            $parentCodeField = 't_users.f_code';
                         }
                         else{
                             $parentCodeField = 't_wares.f_code';
                         }
                     }
                     else if($configSheets[$i]['type'] == 'workorder'){
                         $parentCodeField = 't_workorders.f_code';
                     }
                     else if($configSheets[$i]['type'] == 'selector'){
                         $parentCodeField = 't_selectors.f_code';
                     }
                }*/
                for($j = 0; $j < count($rawData); $j++){
                    
                    // evanetually get parents
                    /*
                    if(isset($configSheets[$i]['parents'])){
                        $parents = implode(',' ,$this->getParentsTPL($rawData[$j]['childCode'], $configSheets[$i]['parents']['field'], $configSheets[$i]['type'], $configSheets[$i]['name']));
                        $rawData[$j][str_replace(array("/", " "), "_", $configSheets[$i]['parents']['label'])] = $parents;
                    }*/
                    if(count($selectArray) > 1){
                        $maxSubRow = 0;
                        $subRawData = null;
                        for($z = 1; $z < count($selectArray); $z++){
                            $query = $selectArray[$z]['data'];
                            for($h = 0; $h < count($selectArray[$z]['mainQueryRelation']); $h++){
                                $query = str_replace('[field' . ($h + 1) . ']', $rawData[$j][$selectArray[$z]['mainQueryRelation'][$h]], $query);
                            }
                            
                            try{
                                $subRawData[$z - 1] = $this->db->query($query)->fetchAll();
                                if(count($subRawData[$z - 1]) > $maxSubRow){
                                    $maxSubRow = count($subRawData[$z - 1]);
                                }
                            }
                            catch(Exception $e){
                                file_put_contents($this->exportURL . $this->filename . '_log.txt', $configSheets[$i]['name'] . " subquery\n" . $e->getMessage() . "\n" . $query . "\n", FILE_APPEND);
                            }
     
                        }  
                        
                        if($maxSubRow > 0){
                            for($z = 0; $z < $maxSubRow; $z++){
                                $unformattedData = $rawData[$j];
                                for($t = 0; $t < count($subRawData); $t++){
                                    if(is_array($subRawData[$t][$z])){
                                        $unformattedData += $subRawData[$t][$z];
                                    }
                                }

                                // format data for the excel
                                $formattedData = $this->formatExcelDataTPL($unformattedData, $operationData, $configSheets[$i]['fields']);
                                // put data into datasheet
                                $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                                // increment number of rows processed
                                $rowCounter++;
                            }
                        }
                        else{
                            // format data for the excel
                            $formattedData = $this->formatExcelDataTPL($rawData[$j], $operationData, $configSheets[$i]['fields']);
                            // put data into datasheet
                            $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                            // increment number of rows processed
                            $rowCounter++;
                            
                        }
                        
                        $progressCounter++;
                       
                    }
                    else{
                        // format data for the excel
                        $formattedData = $this->formatExcelDataTPL($rawData[$j], $operationData, $configSheets[$i]['fields']);
                        // put data into datasheet
                        $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                        // increment number of rows processed
                        $rowCounter++;
                        $progressCounter++;
                    }
                    
                    // eventually add counter field
                    if(isset($configSheets[$i]['autoincrement'])){
                       $rawData[$j] =  array('autoincrement' => $rowCounter) + $rawData[$j];
                    }
                    // format data for the excel
                    //$formattedData = $this->formatExcelDataTPL($rawData[$j], $configFields['fields']);
                    
                    
                    // output
                    if($mode == "shell"){
                       echo "\r" . $progressCounter . "/" . $countData;
                    }
                }
                // debug
                if($debug){
                    break;
                }
                $startQueryRow = $startQueryRow + $limit;
            }
            
        }
        
        
        // ***************************************
        // add mainsim tpl sheets
        // ***************************************
        if($tplType == 'mainsim'){
            
            // ***************************************
            // create custom field sheet
            // ***************************************
            $objExcel->addSheetXLSX(null, 'Custom');
            $xml = simplexml_load_file($this->exportConfigURL . ($tplFile ? $tplFile : 'exportTemplate.xml'));
            $columnsData = array();
            // columns
            foreach($xml->custom->columns->column as $c => $v){
                $columnsData[] = array($v, 'inlineStr', 0, 0 , null, 0, 0);
            }
            $rowOffset = 1;
            $objExcel->writeRowXLSX($columnsData, $rowOffset, 'columns');
            // data 
            foreach($xml->custom->fields->field as $k => $v){
                $aux = (array)$v->mainsim;
                $rowData = array();
                foreach($aux as $k1 => $v1){
                    $rowData[] = array((string)$v1, 'inlineStr', 0, 0, null, 0, 0); 
                }
                $rowOffset++;
                $objExcel->writeRowXLSX($rowData, $rowOffset, 'data');
            }
        }
        
        // output
        if($mode == "shell"){
            echo "\n\nfinalizing...\n";
        }
        
        // ***************************************
        // finalize xlsx
        // ***************************************
        $objExcel->closeXLSX();

        // output
        if($mode == "shell"){
            echo "\nend : " . date("H:i:s") . "\n";
        }
    }
    
    public function excel2($filename, $params, $excelFormat){
        
        $objTranslate = new Mainsim_Model_Translate();
        $t_sst = (microtime(true)*10000).'_'.Zend_Auth::getInstance()->getIdentity()->f_id.'_'.rand(0,9);
        $tsstCode = new Zend_Session_Namespace("tsst_{$params->Tab[0]}_{$params->Tab[1]}");
        $treegrid = new Mainsim_Model_Treegrid();
        $params->data->Tab = $params->Tab;
        $params->data->f_module_name = $params->moduleName;
        $this->getFieldsFromBookmarks2($config, $params->view, $params->data->Tab);
        $this->filename = $filename;	
	// init time file to check if the connection is active after a while
        $startTime = time();
        file_put_contents($this->exportURL . $filename . '_time.txt', time() + 60); 
        $config = Zend_Registry::get('config');

        $multidatabase = $config->multiDatabase;

        $exportCfg = $this->exportConfigURL; // Default XML file
        
        // Get project name to check whether there is a custim xml exporting file 
        $sel = new Zend_Db_Select($this->db);
	$sel->from('t_creation_date', 'f_description')
                ->where("f_title = 'PROJECT_NAME'");
        // If there is, overwrite the default path with the custom one
        $projectName = $sel->query()->fetch()['f_description'];
        if(file_exists($exportCfg . 'custom/' . $projectName . '/exportExcel.xml')) {         
            $exportCfg .= 'custom/'. $projectName . '/';           
        }
            
        // get config from xml
        $config = $this->getConfig($exportCfg . 'exportExcel.xml', $params->moduleName);

        // load translations
        if($config['lang'] || true){
            include(APPLICATION_PATH . '/configs/lang/it_IT.php');
            $this->lang = $config['lang'];
            $this->langArray = ${$config['lang']};
        }

        // overwrite config types with dynamic type passed by web client
        if($params->Tab){
            $config['id'] = explode(",", $params->Tab[1]);
        }
        
        // get fields from db bookmark
        $this->getFieldsFromBookmarks2($config, $params->view, $params->data->Tab);
        // get additional fields from db bookmarks
        if($params->detailed){
            try{
            for($i = 0; $i < count($config['subData']); $i++){
                $this->getFieldsFromBookmarks2($config['subData'][$i]);
                $config['subData'][$i]['selectFields'] = $this->createFieldsSelect($config['subData'][$i], $config['subData'][$i]['type']);
                if($config['subData'][$i]['pairCross']){
                    $this->getFieldsFromBookmarks2($config['subData'][$i]['pairCross'], null, $params->data->Tab);
                    $config['subData'][$i]['pairCross']['selectFields'] = $this->createFieldsSelect($config['subData'][$i]['pairCross'], 'paircross');
                }
            }
            }
            catch(Exception $e){
            }

        }
        // get excel columns headers
        foreach($config['fields'] as $key => $value){ 
            if($excelFormat == 'xlsx'){

            //$headerData[] = array($this->translate($value['title']), 'inlineStr', 0, 0 , 'colsHeader', 0, 0);
                    $headerData[] = array(utf8_decode($objTranslate->_($value['title'])), 'inlineStr', 0, 0 , 'colsHeader', 0, 0);
            $columns[] = array('width' => round($value['width']/5 /*/ (isset($config['widthConvFactor'])?$config['widthConvFactor']:1 * 6.5))*/));
            }
            else if($excelFormat == 'xls'){
                    //$headerData[] = array($this->translate($value['title']), 'String', 0, 0 , 'colsHeader', 0, 0);
                    $headerData[] = array(utf8_decode($objTranslate->_($value['title'])), 'String', 0, 0 , 'colsHeader', 0, 0);
                    $columns[] = array('width' => round($value['width'] / $config['widthConvFactor']));
            }	
        }
		// get excel title 
        if($excelFormat == 'xlsx'){
            $titleOffset = count($headerData) - 1;
                $titleData[] = array($config['title'], 'inlineStr', 0, 0, 'colsTitle', $titleOffset, 0);
        }
        else if($excelFormat == 'xls'){
            $titleOffset = count($headerData) - 1;
            $titleData[] = array($config['title'], 'String', 0, 0, 'colsTitle', $titleOffset, 0);
        }
        
        // get excel column headers and titles for detailed report
        if($params->detailed){
            for($i = 0; $i < count($config['subData']); $i++){
                $subConfig = $config['subData'][$i];  
                $numFields = 0;
                if($subConfig['pairCross']){
                    $subHeadersFields = $subConfig['pairCross']['fields'];
                }
                else{
                    $subHeadersFields = $subConfig['fields'];
                }
                foreach($subHeadersFields as $key => $value){
                    if($key != 'f_code' || ($key == 'f_code' && $this->config['showSystemCode'] == 'y')){
                        if($excelFormat == 'xlsx'){
                            //$headerData[] = array($this->translate($value['title']), 'inlineStr', 0, 0, 'colsHeader', 0, 0);
                            $headerData[] = array(utf8_decode($objTranslate->_($value['title'])), 'inlineStr', 0, 0, 'colsHeader', 0, 0);
                            $columns[] = array('width' => round($value['width'] / ($config['widthConvFactor'] * 6.5)));
                        }
                        else if($excelFormat == 'xls'){
                            //$headerData[] = array($this->translate($value['title']), 'String', 0, 0, 'colsHeader', 0, 0);
                            $headerData[] = array(utf8_decode($objTranslate->_($value['title'])), 'String', 0, 0, 'colsHeader', 0, 0);
                            $columns[] = array('width' => round($value['width'] / ($config['widthConvFactor'])));
                        }  
                    }
                    $numFields++;
                }
                if($excelFormat == 'xlsx'){
                    $titleData[] = array($config['subData'][$i]['title'], 'inlineStr',0, ($i == 0) ? ($titleOffset + 1) : $titleOffset, 'colsTitle', $numFields - 1);
                }
                else if($excelFormat == 'xls'){
                    $titleData[] = array($config['subData'][$i]['title'], 'String',0, $titleOffset + 1, 'colsTitle', $numFields - 1);
                }
                $titleOffset = count($headerData);
            }
        }
		
        // total number of fields in the excel
        $totalFields = count($columns);

        $objExcel = new ExcelStreamer();
        if($excelFormat == 'xlsx'){
                $objExcel->createXLSX($filename);
                $objExcel->addStylesXLSX($config['styles']);
                $objExcel->addSheetXLSX($columns);
                $objExcel->writeRowXLSX($titleData, 1, 'colsTitle');
                $objExcel->writeRowXLSX($headerData, 2, 'colsHeader');
        }
        else if($excelFormat == 'xls'){
                $objExcel->createXLS($filename . '.xls');
                $objExcel->addStylesXLS($config['styles']);
                $objExcel->addSheetXLS('1', null);
                $objExcel->setColumnsXLS($columns);
                $objExcel->writeRowXLS($titleData, 1, 'colsTitle');
                $objExcel->writeRowXLS($headerData, 2, 'colsHeader');
        }

        // number of items retrived per query
        $queryLimit = 50000;
        
        $queryOffset = 0;
        $sheetNumber = 1;
        $sheetRows = 0;
        foreach($config['fields'] as $k => $v){
            $table = Mainsim_Model_MobileUtilities::getFieldTable($k, array('t_creation_date', $params->data->Tab[0], 't_custom_fields', 't_pair_cross', 't_wf_phases', 't_users'));
            $fields['query_list'][] = $table . "." . $k;
        }

   
        // get total number of items (query without offset)

        if($treegrid->useNewSelectors()) {

            $fields['query_list'][] = new Zend_Db_Expr('t_creation_date.f_id as system_code');
            $fields['query_list'][] = new Zend_Db_Expr('t_creation_date.f_id as reportCode');
            
            // check if exists selector filter from request
            if(isset($params->data->Selector)){
                $selectorFilterIds = array();
                foreach($params->data->Selector as $key => $value){
                    foreach($value as $key2 => $value2){
                        if($key2 == 's'){
                            $selectorFilterIds = array_merge($selectorFilterIds, $value2);
                        }
                    } 
                }
            }

            try {
		$p['_category'] = $treegrid->getCategoriesByModule($params->data->f_module_name, $params->data->Tab[0]);
                $p['_selectors'] = is_array($selectorFilterIds) ? $selectorFilterIds : null;
                $p['_filters']['_and'] = $params->data->Ands;
				$p['_filters']['_order'] = $params->data->Sort;
                $p['_filters']['_search'] = $params->data->Search;
                $p['_filters']['_like'] = $params->data->Like;
                $p['_filters']['_filter'] = $params->data->Filter;
                $p['_hierarchy'] = $params->data->Hierarchy;
                $p['_module'] = $params->data->module_name;
				$p['_parent'] = -1;
                $tot  = count($treegrid->getCodes($p));
            }catch(Exception $e) {
                print($e->GetMessage());die();
            }
        }

        // initialize variable for even/odd item style
        $even = 1;
        $rows = 3;
        
        // process data set until query result is empty
        while(true){
            $p['_limit']=array();
            $p['_limit'] = array($queryOffset, $queryLimit);
            $p['_parent'] = -1;

            $codes = $treegrid->getCodes($p);
            // get fields showed in treegrid
            //$fields = $treegrid->getFields($params->data->f_module_name, $params->Tab[0]);
            // get data
			
			$chunks = array_chunk($codes, 1000);
			
			$data = [];
			for($w = 0; $w < count($chunks); $w++){
				$res = $treegrid->getData($chunks[$w], $fields, $p,$params->data->Tab[0]);
				$data = array_merge($data, $res);
			}
            
            // format data for treegrid
            //$result = $tree->formatData($fields, $data, $params);
            
            // there are no items to process --> close the excel file
            if(count($data) == 0){
                if($excelFormat == 'xlsx'){
                    $objExcel->closeXLSX();
                }
                else if($excelFormat == 'xls'){
                    $objExcel->closeXLS();
                }
                file_put_contents($this->exportURL . $filename . '.txt', json_encode(array('end' => 'true', 'counter' => $items, 'tot' => $tot, 'time' => time() - $startTime, 'filename' => $this->exportURL . $filename . '.' . $excelFormat)));
                die();
                break;
            }
            
            // loop main data
            for($i = 0; $i < count($data); $i++){
                
                // array containing all data rows for item 'i' (only 1 row if report is not detailed)
                $dataRow = array();

                // counter to excel rows number to be used for the item 'i'
                $itemRows = 0;

                // detailed report --> get cross data and find rows used from item 'i'
                $addFill = false;
                
                if($params->detailed){
                    for($pi = 0; $pi < count($config['subData']); $pi++){
                        $subConfig = $config['subData'][$pi];
                        $subData[$pi] = $this->getData($config['database'], $subConfig['selectFields'], $config['type'] . "-" . $subConfig['type'], $subConfig['id'], $data[$i]['system_code'], null, $queryLimit, $queryOffset);    
                        if($subConfig['pairCross']){
                            $aux = $subData[$pi];
                            $subData[$pi] = array();
                            for($g = 0; $g < count($aux); $g++){
                                if($even){
                                    $aux[$g]['_style_'] = 'pairCrossEven';
                                    $subStyle = 'even';
                                }
                                else{
                                    $aux[$g]['_style_'] = 'pairCrossOdd';
                                    $subStyle = 'odd';
                                }
                                $subData[$pi][] = $aux[$g];
                                $pairData = $this->getData($config['database'], $subConfig['pairCross']['selectFields'], $subConfig['type'] . '-paircross', $subConfig['id'], $data[$i]['reportCode'], $aux[$g]['reportCode'], $queryLimit, $queryOffset);    
                                for($h = 0; $h < count($pairData); $h++){ 
                                    $pairData[$h]['_style_'] = $subStyle;
                                    $subData[$pi][] = $pairData[$h];
                                }   
                            } 
                        }
                        
                        if(count($subData[$pi]) > $itemRows){
                            $itemRows = count($subData[$pi]);
                        }
                    }
					
                    if($itemRows == 0){
                            $itemRows = 1;
                    }
                }
                // no detailed report --> rows used for item 'i' = 1
                else{
                    $itemRows = 1;
                }
                
                // loop through rows data of item 'i' to format in excel params
                $firstRow = true;
                for($r = 0; $r < $itemRows; $r++){
                    // array containing row data to export in excel
                    $excelRow = null;
                    
                    // add main data ($data) of item 'i' to excel row (only for the first row)
                    if($firstRow){
                        $excelRow = $this->formatExcelData2($data[$i], $config['fields'], null, null, $excelFormat);  
                    }
                    else{
                        $excelRow = array_fill(0, count($config['fields']), null);
                    }
                    $offsetCell = count($config['fields']);
                    // detailed report --> add to excel rows secondary data 
                    if($params->detailed){
                        for($pi = 0; $pi < count($config['subData']); $pi++){
                            $arrayToMerge = null;
                            if($config['subData'][$pi]['pairCross']){
                                $fields = $config['subData'][$pi]['pairCross']['fields'];
                                $filter = $config['subData'][$pi]['pairCross']['fields'];
                            }
                            else{
                                $fields = $config['subData'][$pi]['fields'];
                                $filter = null;
                            }
                            if(is_array($subData[$pi][$r]) && (count($subData[$pi][$r]) > 0)){ 
                                $arrayToMerge = $this->formatExcelData2($subData[$pi][$r], $fields, $filter, $offsetCell, $excelFormat);
                            }
                            else{
                                $arrayToMerge = array_fill(0, count($fields), null);
                            }
                            
                            $excelRow = array_merge($excelRow, $arrayToMerge);
                            $offsetCell += count($fields);
                        }
                    }
                    
                    if($firstRow){
                        $firstRow = false;
                    }
                    // array containing all excel rows of item 'i'
                    $excelRows[] = $excelRow;
                }
                
				
				// add new sheet
                if($rows > 65533){
                    $sheetNumber++;
                    if($excelFormat == 'xls'){
                            $objExcel->addSheetXLS($sheetNumber, null);
                            $objExcel->setColumnsXLS($columns);
                            $objExcel->writeRowXLS($titleData, null, 'colsTitle', $config['styles']['colsTitle']['height']['size']);
                            $objExcel->writeRowXLS($headerData, null, 'colsHeader', $config['styles']['colseader']['height']['size']);
                    }
                    else if($excelFormat == 'xlsx'){
                            $objExcel->addSheetXLSX($columns);
                            //$objExcel->setColumnsXLS($columns);
                            $objExcel->writeRowXLSX($titleData, 1, 'colsTitle');
                            $objExcel->writeRowXLSX($headerData, 2, 'colsHeader');
                    }

                    // reset rows counter for the new sheet
                    $rows = 3;
                    // reassign style 'even' for the first item of the new sheet
                    $even = 1;
                }

                // set the style of item
                if($even){
                    $style = 'even';
                }
                else{
                    $style = 'odd';
                }
                // write item to excel file
                if($excelFormat == 'xls'){
                    $objExcel->writeRowsXLS($excelRows, $rows, $style);
                }
                else{
                    $objExcel->writeRowsXLSX($excelRows, $rows, $style);
                }
				
                // alternate item style
                $even = 1 - $even;
                
                // update excel rows counter
                $rows += count($excelRows);
                
                // reset array for the next item (i + 1)
                unset($excelRows);
                // update processed items counter 
                $items++;
                
                // update file progress for client
                // send end process to client
                if($items == $tot){
					
                    if($excelFormat == 'xls'){
                        $objExcel->closeXLS();
                    }
                    else if($excelFormat == 'xlsx'){
                        $objExcel->closeXLSX();
                    }
					
                    file_put_contents($this->exportURL . $filename . '.txt', json_encode(array('end' => 'true', 'counter' => $items, 'tot' => $tot, 'time' => time() - $startTime, 'filename' => $this->exportURL . $filename . '.' . $excelFormat)));
                    die();
                    break;
                }
                // send update process to client 
                else{
                    $timeClient = file_get_contents($this->exportURL . $filename . '_time.txt');
//                    file_put_contents($this->exportURL . $filename . '.txt', json_encode(array('end' => 'false', 'counter' => $items, 'tot' => $tot, 'time' => time() - $startTime, 'filename' => $this->exportURL . $filename . '.' . $excelFormat, 'progressTime' => time() - $timeClient)));
                }
                
                // client disconneted --> delete files
                if($timeClient && (time() - $timeClient > 60)){
                    if($excelFormat == 'xlsx'){
                        $objExcel->closeXLSX();
                    }
                    else if($excelFormta == 'xls'){
                        $objExcel->closeXLS();
                    }
                    unlink($this->exportURL . $filename . '.' . $excelFormat);
                    unlink($this->exportURL . $filename . '.txt');
                    unlink($this->exportURL . $filename . '_time.txt');
                    @unlink($this->exportURL . $filename . '_abort.txt');
                    die();
                }  
            }
            
            // update offset for next data set query
            $queryOffset += $queryLimit;
        }
        
        
        die();
    }

    public function excel($filename, $params, $excelFormat){
        $this->filename = $filename;
 
		
		// init time file to check if the connection is active after a while
        $startTime = time();
        file_put_contents($this->exportURL . $filename . '_time.txt', time() + 60); 
        
        // get config from xml
        $config = $this->getConfig($this->exportConfigURL . 'exportExcel.xml', $params->moduleName);

        // load translations
        if($config['lang'] || true){
            include(APPLICATION_PATH . '/configs/lang/it_IT.php');
            $this->lang = $$config['lang'];
        }
        
        // overwrite config types with dynamic type passed by web client
        if($params->Tab){
            $config['id'] = explode(",", $params->Tab[1]);
        }
        
	// get fields from db bookmark
        $this->getFieldsFromBookmarks($config, $params->view);
        
        
	// get additional fields from db bookmarks
        if($params->detailed){
            for($i = 0; $i < count($config['subData']); $i++){
                $this->getFieldsFromBookmarks($config['subData'][$i]);
                $config['subData'][$i]['selectFields'] = $this->createFieldsSelect($config['subData'][$i], $config['subData'][$i]['type']);
                if($config['subData'][$i]['pairCross']){
                    $this->getFieldsFromBookmarks($config['subData'][$i]['pairCross']);
                    $config['subData'][$i]['pairCross']['selectFields'] = $this->createFieldsSelect($config['subData'][$i]['pairCross'], 'paircross');
                }
            }
        }
		
	// create query 'select' part from the bookmarks previously retrieved 
        if($params->history == true){
            $config['selectFields'] = $this->createFieldsSelect($config, $config['type'], true);
        }
        else{
            $config['selectFields'] = $this->createFieldsSelect($config, $config['type']);
        }
        // get excel columns headers
        foreach($config['fields'] as $key => $value){ 
            if($excelFormat == 'xlsx'){

            $headerData[] = array($this->translate($value['title']), 'inlineStr', 0, 0 , 'colsHeader', 0, 0);
                    $columns[] = array('width' => round($value['width'] / ($config['widthConvFactor'] * 6.5)));
            }
            else if($excelFormat == 'xls'){
                    $headerData[] = array($this->translate($value['title']), 'String', 0, 0 , 'colsHeader', 0, 0);
                    $columns[] = array('width' => round($value['width'] / $config['widthConvFactor']));
            }	
        }
		// get excel title 
        if($excelFormat == 'xlsx'){
            $titleOffset = count($headerData) - 1;
                $titleData[] = array($config['title'], 'inlineStr', 0, 0, 'colsTitle', $titleOffset, 0);
        }
        else if($excelFormat == 'xls'){
            $titleOffset = count($headerData) - 1;
            $titleData[] = array($config['title'], 'String', 0, 0, 'colsTitle', $titleOffset, 0);
        }
        
        // get excel column headers and titles for detailed report
        if($params->detailed){
            for($i = 0; $i < count($config['subData']); $i++){
                $subConfig = $config['subData'][$i];  
                $numFields = 0;
                if($subConfig['pairCross']){
                    $subHeadersFields = $subConfig['pairCross']['fields'];
                }
                else{
                    $subHeadersFields = $subConfig['fields'];
                }
                foreach($subHeadersFields as $key => $value){
                    if($key != 'f_code' || ($key == 'f_code' && $this->config['showSystemCode'] == 'y')){
                        if($excelFormat == 'xlsx'){
                            $headerData[] = array($value['title'], 'inlineStr', 0, 0, 'colsHeader', 0, 0);
                            $columns[] = array('width' => round($value['width'] / ($config['widthConvFactor'] * 6.5)));
                        }
                        else if($excelFormat == 'xls'){
                            $headerData[] = array($value['title'], 'String', 0, 0, 'colsHeader', 0, 0);
                            $columns[] = array('width' => round($value['width'] / ($config['widthConvFactor'])));
                        }  
                    }
                    $numFields++;
                }
                if($excelFormat == 'xlsx'){
                    $titleData[] = array($config['subData'][$i]['title'], 'inlineStr',0, ($i == 0) ? ($titleOffset + 1) : $titleOffset, 'colsTitle', $numFields - 1);
                }
                else if($excelFormat == 'xls'){
                    $titleData[] = array($config['subData'][$i]['title'], 'String',0, $titleOffset + 1, 'colsTitle', $numFields - 1);
                }
                $titleOffset = count($headerData);
            }
        }
		
        // total number of fields in the excel
        $totalFields = count($columns);

        $objExcel = new ExcelStreamer();

        if($excelFormat == 'xlsx'){
                $objExcel->createXLSX($filename);
                $objExcel->addStylesXLSX($config['styles']);
                $objExcel->addSheetXLSX($columns);
                $objExcel->writeRowXLSX($titleData, 1, 'colsTitle');
                $objExcel->writeRowXLSX($headerData, 2, 'colsHeader');
        }
        else if($excelFormat == 'xls'){
                $objExcel->createXLS($filename . '.xls');
                $objExcel->addStylesXLS($config['styles']);
                $objExcel->addSheetXLS('1', null);
                $objExcel->setColumnsXLS($columns);
                $objExcel->writeRowXLS($titleData, 1, 'colsTitle');
                $objExcel->writeRowXLS($headerData, 2, 'colsHeader');
        }

        // number of items retrived per query
        $queryLimit = 50000;
	
        $queryOffset = 0;
        $sheetNumber = 1;
        $sheetRows = 0;
        
        // get total number of items (query without offset)
        if($params->costs == true){
            $aux = $this->getCountData($config['type'], $config['id'], $params->data, null, $params->historyDate);
        }
        else if($params->history == true){
            $aux = $this->getCountData($config['type'], $config['id'], $params->data, $params->historyDate);
        }
        else{
            $aux = $this->getCountData($config['type'], $config['id'], $params->data);
        }
        $tot = $aux[0]['tot'];
        
        // initialize variable for even/odd item style
        $even = 1;
        $rows = 3;
        
        // process data set until query result is empty
        while(true){
            // get data from db (based on $queryLimit quantity)
            if($params->costs == true){
                $data = $this->getData($config['database'], $config['selectFields'], $config['type'], $config['id'], null, null, $queryLimit, $queryOffset, null, $params->data, null, $params->historyDate);
            }
            else if($params->history == true){
                $data = $this->getData($config['database'], $config['selectFields'], $config['type'], $config['id'], null, null, $queryLimit, $queryOffset, null, $params->data, $params->historyDate);
            }
            else{
                $data = $this->getData($config['database'], $config['selectFields'], $config['type'], $config['id'], null, null, $queryLimit, $queryOffset, null, $params->data);
            }
			
            // there are no items to process --> close the excel file
            if(count($data) == 0){
                
                if($excelFormat == 'xlsx'){
                        $objExcel->closeXLSX();
                }
                else if($excelFormat == 'xls'){
                        $objExcel->closeXLS();
                }
                file_put_contents($this->exportURL . $filename . '.txt', json_encode(array('end' => 'true', 'counter' => $items, 'tot' => $tot, 'time' => time() - $startTime, 'filename' => $this->exportURL . $filename . '.' . $excelFormat)));
                die();
                break;
            }
            
            // loop main data
            for($i = 0; $i < count($data); $i++){
                
                // array containing all data rows for item 'i' (only 1 row if report is not detailed)
                $dataRow = array();

                // counter to excel rows number to be used for the item 'i'
                $itemRows = 0;

                // detailed report --> get cross data and find rows used from item 'i'
                $addFill = false;
                
                if($params->detailed){
                    for($pi = 0; $pi < count($config['subData']); $pi++){
                        $subConfig = $config['subData'][$pi];
                        $subData[$pi] = $this->getData($config['database'], $subConfig['selectFields'], $config['type'] . "-" . $subConfig['type'], $subConfig['id'], $data[$i]['system_code'], null, $queryLimit, $queryOffset);    
                        if($subConfig['pairCross']){
                            $aux = $subData[$pi];
                            $subData[$pi] = array();
                            for($g = 0; $g < count($aux); $g++){
                                if($even){
                                    $aux[$g]['_style_'] = 'pairCrossEven';
                                    $subStyle = 'even';
                                }
                                else{
                                    $aux[$g]['_style_'] = 'pairCrossOdd';
                                    $subStyle = 'odd';
                                }
                                $subData[$pi][] = $aux[$g];
                                $pairData = $this->getData($config['database'], $subConfig['pairCross']['selectFields'], $subConfig['type'] . '-paircross', $subConfig['id'], $data[$i]['reportCode'], $aux[$g]['reportCode'], $queryLimit, $queryOffset);    
                                for($h = 0; $h < count($pairData); $h++){ 
                                    $pairData[$h]['_style_'] = $subStyle;
                                    $subData[$pi][] = $pairData[$h];
                                }   
                            } 
                        }
                        
                        if(count($subData[$pi]) > $itemRows){
                            $itemRows = count($subData[$pi]);
                        }
                    }
					
                    if($itemRows == 0){
                            $itemRows = 1;
                    }
                }
                // no detailed report --> rows used for item 'i' = 1
                else{
                    $itemRows = 1;
                }
                
                // loop through rows data of item 'i' to format in excel params
                $firstRow = true;
                for($r = 0; $r < $itemRows; $r++){
                    // array containing row data to export in excel
                    $excelRow = null;
                    
                    // add main data ($data) of item 'i' to excel row (only for the first row)
                    if($firstRow){
                        $excelRow = $this->formatExcelData($data[$i], $config['fields'], null, null, $excelFormat);  
                    }
                    else{
                        $excelRow = array_fill(0, count($config['fields']), null);
                    }
                    $offsetCell = count($config['fields']);
                    // detailed report --> add to excel rows secondary data 
                    
                    if($params->detailed){
                        for($pi = 0; $pi < count($config['subData']); $pi++){
                            $arrayToMerge = null;
                            if($config['subData'][$pi]['pairCross']){
                                $fields = $config['subData'][$pi]['pairCross']['fields'];
                                $filter = $config['subData'][$pi]['pairCross']['fields'];
                            }
                            else{
                                $fields = $config['subData'][$pi]['fields'];
                                $filter = null;
                            }
                            if(is_array($subData[$pi][$r]) && (count($subData[$pi][$r]) > 0)){ 
                                $arrayToMerge = $this->formatExcelData($subData[$pi][$r], $fields, $filter, $offsetCell, $excelFormat);
                            }
                            else{
                                $arrayToMerge = array_fill(0, count($fields), null);
                            }
                            
                            $excelRow = array_merge($excelRow, $arrayToMerge);
                            $offsetCell += count($fields);
                        }
                    }
                    
                    if($firstRow){
                        $firstRow = false;
                    }
                    // array containing all excel rows of item 'i'
                    $excelRows[] = $excelRow;
                }
                
				
				// add new sheet
                if($rows > 65533){
                    $sheetNumber++;
                    if($excelFormat == 'xls'){
                            $objExcel->addSheetXLS($sheetNumber, null);
                            $objExcel->setColumnsXLS($columns);
                            $objExcel->writeRowXLS($titleData, null, 'colsTitle', $config['styles']['colsTitle']['height']['size']);
                            $objExcel->writeRowXLS($headerData, null, 'colsHeader', $config['styles']['colseader']['height']['size']);
                    }
                    else if($excelFormat == 'xlsx'){
                            $objExcel->addSheetXLSX($columns);
                            //$objExcel->setColumnsXLS($columns);
                            $objExcel->writeRowXLSX($titleData, 1, 'colsTitle');
                            $objExcel->writeRowXLSX($headerData, 2, 'colsHeader');
                    }

                    // reset rows counter for the new sheet
                    $rows = 3;
                    // reassign style 'even' for the first item of the new sheet
                    $even = 1;
                }

				// set the style of item
                if($even){
					$style = 'even';
                }
                else{
                    $style = 'odd';
                }
                // write item to excel file
                if($excelFormat == 'xls'){
                        $objExcel->writeRowsXLS($excelRows, $rows, $style);
                }
                else{
                        $objExcel->writeRowsXLSX($excelRows, $rows, $style);
                }
				
                // alternate item style
                $even = 1 - $even;
                
                // update excel rows counter
                $rows += count($excelRows);
                
                // reset array for the next item (i + 1)
                unset($excelRows);
                // update processed items counter 
                $items++;
                

                // update file progress for client
                // send end process to client
                if($items == $tot){
					
                    if($excelFormat == 'xls'){
                        $objExcel->closeXLS();
                    }
                    else if($excelFormat == 'xlsx'){
                        $objExcel->closeXLSX();
                    }
					
                    file_put_contents($this->exportURL . $filename . '.txt', json_encode(array('end' => 'true', 'counter' => $items, 'tot' => $tot, 'time' => time() - $startTime, 'filename' => $this->exportURL . $filename . '.' . $excelFormat)));
                    die();
                    break;
                }
                // send update process to client 
                else{
                    $timeClient = file_get_contents($this->exportURL . $filename . '_time.txt');
                    file_put_contents($this->exportURL . $filename . '.txt', json_encode(array('end' => 'false', 'counter' => $items, 'tot' => $tot, 'time' => time() - $startTime, 'filename' => $this->exportURL . $filename . '.' . $excelFormat, 'progressTime' => time() - $timeClient)));
                }
                
                // client disconneted --> delete files
                if($timeClient && (time() - $timeClient > 60)){
                    if($excelFormat == 'xlsx'){
                        $objExcel->closeXLSX();
                    }
                    else if($excelFormta == 'xls'){
                        $objExcel->closeXLS();
                    }
                    unlink($this->exportURL . $filename . '.' . $excelFormat);
                    unlink($this->exportURL . $filename . '.txt');
                    unlink($this->exportURL . $filename . '_time.txt');
                    @unlink($this->exportURL . $filename . '_abort.txt');
                    die();
                }  
            }
            // update offset for next data set query
            $queryOffset += $queryLimit;
        }
    }
    
    public function translate($value){
		return ((isset($this->langArray[$value])) ? $this->langArray[$value] : $value);
    }
    
    public function getCustomFieldsTPL(){
        $objExcel = new ExcelStreamer();
        $objExcel->openXLSX('input_template.xlsx');
        
        // get custom_fields association for all sheets
        // sheets to check
        $sheetsToCheck = array(
            't_workorders_workorder',
            't_wares_asset',
            't_workorders_standard_workorder',
            't_workorders_periodic_maint',
            't_wares_tasks',
            't_workorders_on_condition',
            //'t_wares_meter',
            't_wares_action',
            //'t_wares_schedule',
            't_wares_inventory',
            //'t_workorders_purchaseorders',
            't_wares_labor',
            't_wares_tools',
            't_wares_contracts',
            't_wares_failurecodes',
            't_wares_vendors',
            't_wares_documents',
            't_wares_users'
        );
        
        // extract custom fields relations from template sheets
        for($i = 0; $i < count($sheetsToCheck); $i++){
            $objExcel->openSheetXLSX($sheetsToCheck[$i]);
            $fieldList = $objExcel->readRowXLSX();
            
            $fieldsXML = null;
            // extract custom fields from field list
            for($j = 0; $j < count($fieldList); $j++){
                if($fieldList[$j] && strpos($fieldList[$j], 'f_') === false && strpos($fieldList[$j], 'fc_') === false){
                    $fieldsXML .= "\t\t\t\t\t<field>" . $fieldList[$j] . "</field>\n";
                }
            }
            
            if($fieldsXML){
                $relsXML .= "\t\t\t<rel>\n" .
                            "\t\t\t\t<name>" . $sheetsToCheck[$i] . "</name>\n" .
                            "\t\t\t\t<fields>\n" . 
                            $fieldsXML .
                            "\t\t\t\t</fields>\n" .
                            "\t\t\t</rel>\n";
  
            }
            $objExcel->closeSheetXML();
        }

        // extract custom fields definitions from 'Custom' sheet
        $objExcel->openSheetXLSX('Custom');
        $columns = $objExcel->readRowXLSX();
        for($i = 1; $i < count($columns); $i++){
            $columnsXML .= "\t\t\t<column>" . $columns[$i] . "</column>\n";
        }
        
        while($row = $objExcel->readRowXLSX()){
            if($row[2]){
                $fieldsXML .= "\t\t\t<field>\n";
                // name
                $fieldsXML .= "\t\t\t\t<name>" . $row[2] . "</name>\n";
                // label
                $fieldsXML .= "\t\t\t\t<label>" . $row[3] . "</label>\n";
                // type
                switch($row[5]){
                    case '3 : TEXT':
                    case '5 : DATE':
                    case '0 : INT':
                    case '2 : VARCHAR':
                        $fieldsXML .= "\t\t\t\t<type>inlineStr</type>\n";
                        break; 
                }

                // save mainsim info
                $fieldsXML .= "\t\t\t\t<mainsim>\n";
                for($i = 1; $i < count($columns); $i++){
                    $fieldsXML .= "\t\t\t\t\t<" . $columns[$i] . ">" . $row[$i] . "</" . $columns[$i] . ">\n";
                }
                $fieldsXML .= "\t\t\t\t</mainsim>\n";
                
                
                $fieldsXML .= "\t\t\t</field>\n";
            }
        }    
        
        // add custom part to export XML file
        $customXML = "\n\t<custom>\n" .
                        "\t\t<relations>\n" .
                        $relsXML .
                        "\t\t</relations>\n" .
                        "\t\t<fields>\n" .
                        $fieldsXML . 
                        "\t\t</fields>\n" . 
                        "\t\t<columns>\n" .
                        $columnsXML .
                        "\t\t</columns>\n" .
                     "\t</custom>\n" .
                     "</root>";
        $fileHandle = fopen('exportTemplate.xml', 'r+');
        fseek($fileHandle, -8, SEEK_END);
        fwrite($fileHandle, $customXML);
        fclose($fileHandle);
    }
    
    //************************************
    // tool
    //************************************
    public function createTempDirTool(){
        $dir = $this->exportURL . time();
        mkdir($dir);
        return $dir;
    }
    
    public function getSheetsTool($template = null){
        if($template){
            $configSheets = $this->getConfigSheetsTPL($template);
        }
        else{
            $configSheets = $this->getConfigSheetsTPL($this->exportConfigURL . 'exportTemplate.xml');
        }
        $result = array();
        for($i = 0; $i < count($configSheets); $i++){
            if(!in_array($configSheets[$i]['name'], $result)){
                $result[] = $configSheets[$i]['name'];
            }
        }
        return $result;
    }
    
    public function getConfigSheetTool($configFile, $sheetName){
        $configSheets = $this->getConfigSheetsTPL($configFile);
        for($i = 0; $i < count($configSheets); $i++){
            if($configSheets[$i]['name'] == $sheetName){
                $result[] = $configSheets[$i];
            }
        }
        return $result;
    }
    
    public function getConfigOperationDataTool($configFile){
        return $this->getConfigOperationDataTPL($configFile);
    }

    public function createSheetTool($dir, $sheetName, $sheetId, $template = null, $params = null){    // get sheet config
        $startTime = time();
        if($template){
            $configSheets = $this->getConfigSheetTool($template, $sheetName);
        }
        else{
            $configSheets = $this->getConfigSheetTool($this->exportConfigURL . 'exportTemplate.xml', $sheetName);
        }
        // get total count data
        $countData = 0;
        for($i = 0; $i < count($configSheets); $i++){
            $selectArray = null;
            for($j = 0; $j < count($configSheets[$i]['query']); $j++){
                $select = $this->createSelectTPL($configSheets[$i]['query'][$j]);
                $selectArray[] = $select;
                if($select['type'] != 'sub'){
                try{
                    $aux = $select['count']->query()->fetch();
                    $countData += $aux['num'];
                }
                catch(Exception $e){
                    file_put_contents($dir . '/' . $sheetName . '_log.txt', $sheetName . "\n" . $e->getMessage() . "\n" . $selectArray[0]['count'], FILE_APPEND);
                }
                }
            }
        }
        // get operation data
        if($template){
            $operationData = $this->getConfigOperationDataTool($template, $sheetName);
        }
        else{
            $operationData = $this->getConfigOperationDataTool($this->exportConfigURL . 'exportTemplate.xml', $sheetName);
        }
        
        $objExcel = new ExcelStreamer();
        $objExcel->ms_createSheetXLSX($sheetId, $dir, null, null);  
        
        // get styles config
        if($template){
            $configStyles = $this->getConfigStylesTPL($template);
        }
        else{
            $configStyles = $this->getConfigStylesTPL($this->exportConfigURL . 'exportTemplate.xml');
        }
        // add style to excel
        $objExcel->ms_addStylesXLSX($configStyles);
        
        $progressCounter = 0;
        for($i = 0; $i < count($configSheets); $i++){
            $configSheet = $configSheets[$i];
            
            $selectArray = null;
            // create select queries array ( index 0 = main query);
            for($q = 0; $q < count($configSheet['query']); $q++){
                $selectArray[] = $this->createSelectTPL($configSheet['query'][$q], isset($params) ? $params : null);
            }
            
            if(!$configSheet['append']){
                // write excel columns row
                $columnsData = array();
                foreach($configSheet['fields'] as $value){
                    $columnsData[] = array($value['label'], 'inlineStr', 0, 0 , $value['styleTitle'] ? $value['styleTitle'] : null, 0, 0);
                    //$columns[] = array('width' => round($value['width'] / ($config['widthConvFactor'] * 6.5)));
                }
                $objExcel->writeRowXLSX($columnsData, 1, 'columns');
                $rowCounter = 2;  
            }

            $startQueryRow = 0;  

            $limit = 10000;
            
            while(true){
                // get data from database
                try{
                    $rawData = $selectArray[0]['data']->limit($limit, $startQueryRow)->query()->fetchAll();
                }
                catch(Exception $e){
                    file_put_contents($dir . '/' . $sheetName . '_log.txt', "\n-------------------------------------\n" . $configSheet['name'] . "\n-------------------------------------\n" . $e->getMessage() . "\n" . $select['data'], FILE_APPEND);
                }
                if(count($rawData) == 0){
                    break;
                }

                for($j = 0; $j < count($rawData); $j++){

                    if(count($selectArray) > 1){
                        $maxSubRow = 0;
                        $subRawData = null;
                        for($z = 1; $z < count($selectArray); $z++){
                            $query = $selectArray[$z]['data'];
                            for($h = 0; $h < count($selectArray[$z]['mainQueryRelation']); $h++){
                                $query = str_replace('[field' . ($h + 1) . ']', $rawData[$j][$selectArray[$z]['mainQueryRelation'][$h]], $query);
                            }
                            try{
                                $subRawData[$z - 1] = $this->db->query($query)->fetchAll();
                                if(count($subRawData[$z - 1]) > $maxSubRow){
                                    $maxSubRow = count($subRawData[$z - 1]);
                                }
                            }
                            catch(Exception $e){
                                file_put_contents($dir . '/' . $sheetName . '_log.txt', $configSheet['name'] . " subquery\n" . $e->getMessage() . "\n" . $query . "\n", FILE_APPEND);
                            }
                        }  

                        if($maxSubRow > 0){
                            for($z = 0; $z < $maxSubRow; $z++){
                                $unformattedData = $rawData[$j];
                                for($t = 0; $t < count($subRawData); $t++){
                                    if(is_array($subRawData[$t][$z])){
                                        $unformattedData += $subRawData[$t][$z];
                                    }
                                }

                                // format data for the excel
                                $formattedData = $this->formatExcelDataTPL($unformattedData, $operationData, $configSheet['fields'], $progressCounter + 1);
                                // put data into datasheet
                                $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                                // increment number of rows processed
                                $rowCounter++;
                            }
                        }
                        else{
                            // format data for the excel
                            $formattedData = $this->formatExcelDataTPL($rawData[$j], $operationData, $configSheet['fields'], $progressCounter + 1);
                            // put data into datasheet
                            $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                            // increment number of rows processed
                            $rowCounter++;
                        }
                        $progressCounter++;
                    }
                    else{
                        // format data for the excel
                        $formattedData = $this->formatExcelDataTPL($rawData[$j], $operationData, $configSheet['fields'], $progressCounter + 1);
                        // put data into datasheet
                        $objExcel->writeRowXLSX($formattedData, $rowCounter, 'data');
                        // increment number of rows processed
                        $rowCounter++;
                        $progressCounter++;
                    }

                    // eventually add counter field
                    if(isset($configSheet['autoincrement'])){
                       $rawData[$j] =  array('autoincrement' => $rowCounter) + $rawData[$j];
                    }

                    // update progress file
                    $progress['counter'] = $progressCounter;
                    $progress['total'] = $countData;
                    $progress['state'] = 'processing';
                    $progress['time'] =  time() - $startTime;
                    file_put_contents($dir . '/' . $sheetName . '_progress.txt', json_encode($progress));
                }

                $startQueryRow = $startQueryRow + $limit;
            }
        }
        
        // close sheet
        $objExcel->closeSheetXLSX();
        // update progress file
        $progress['counter'] = $progressCounter;
        $progress['total'] = $countData;
        $progress['state'] = 'end';
        file_put_contents($dir . '/' . $sheetName . '_progress.txt', json_encode($progress));
    }
    
    public function getProgressTool($dir, $sheets){
        for($i = 0; $i < count($sheets); $i++){
            if(file_exists($dir . '/' . $sheets[$i] . '_progress.txt')){
                $progress[$i + 1] = json_decode(file_get_contents($dir . '/' . $sheets[$i] . '_progress.txt'));
            }
            else{
                $progress[$i + 1] = array('state' => 'not started');
            }
        }
        return $progress;
    }
    
    public function finalizeXLSXTool($dirTemp, $sheets){
        
        $objExcel = new ExcelStreamer();
        $objExcel->ms_setTempDir($dirTemp);
        return $objExcel->ms_finalizeXLSX($dirTemp, $sheets);
    }
    
    public function createXMLConfigTool($dirTemp, $template = null){
        $configSheets = $this->getSheetsTool($template);
        if($template){
            $configStyles = $this->getConfigStylesTPL($template);
        }
        else{
            $configStyles = $this->getConfigStylesTPL($this->exportConfigURL . 'exportTemplate.xml');
        }
        $objExcel = new ExcelStreamer();
        $objExcel->ms_setTempDir($dirTemp);
        $objExcel->addStylesXLSX($configStyles);
        $objExcel->ms_createConfigXML($configSheets, $configStyles);
    }
    
    public function saveConfigFileTool($f_code, $configFile){
        // custom config file
        $attachmentFolder = Zend_Registry::get('attachmentsFolder');
        if($configFile){
            $aux = explode("/", $configFile);
            $destinationFile = array_pop($aux);
            $aux2 = explode(".", $destinationFile);
            $session_id = $aux2[0];
            copy($configFile, $attachmentFolder. '/doc/' . $destinationFile);
            //unlink($configFile);
            
            try{
                // update config file position in wo db data
                $query = "update t_workorders set fc_dmn_export_settings = '" . $session_id . "|exportTemplate.xml' where f_code =" . $f_code;
                $this->db->query($query);
                $query2 = "update t_attachments set f_path = '" . addslashes($attachmentFolder . '/doc/' . $destinationFile) . "' where f_session_id = '" . $session_id . "'";
                $this->db->query($query2);
            }
            catch(Exception $e){
                print("saveConfigTool\n" . $e->getMessage());
            }
        }
    }
    
    public function getFilename($template, $code = null){
        $xml = simplexml_load_file($template);
        if(isset($xml->file->title->database)){
            $query = "SELECT " . $xml->file->title->database->field . " AS title FROM " . $xml->file->title->database->table . " WHERE " . $xml->file->title->database->id_field . " = " . $code; 
            try{
                $res = $this->db->query($query)->fetchAll();
            }
            catch(Exception $e){
                print($e->getMessage());
            }
            return $res[0]['title'];
        }
        else{
            return null;
        }
        
    }
    
    public function excelScript($filename, $params, $excelFormat)
    {
//        $f_code = $params->f_code;
        $objExcel = new ExcelStreamer();
//        die(json_encode($params));
        require(SCRIPTS_PATH.$params->scriptCustom.'.php');
//        $select = new Zend_Db_Select($this->db);
//        $data = $select->from("t_scripts")
//            ->where("f_name = ?", $params->scriptCustom)->where("f_type = 'php'")        
//            ->query()->fetch();
//        
//        
//        if(!empty($data["f_script"])){
//            $objExcel = new ExcelStreamer();
//            eval($data["f_script"]);
//        }
    }
    
}