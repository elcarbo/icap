<?php
class Mainsim_Model_Kpi{
    
    private $filterName;
    private $filterID;
    private $kpiID;
    private $kpiName;
    private $kpiFrequency;
    private $kpiType;
    private $db;
    
    public function __construct($kpi = null){
        
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));

        // get kpi info
        if($kpy){
            $this->kpiName = $kpi;
            $select = new Zend_Db_Select($this->db);
            $select->from('t_kpi')
                   ->where("f_name = '" . $kpi . "'");
            $res = $select->query()->fetchAll();
            $this->kpiID = $res[0]['f_id'];
            $this->kpiType = $res[0]['f_type'];
            $this->kpiFrequency = $res[0]['f_frequency'];
        }
    }
    
    public function initFilter($filter){
        // get filter info
        $select = new Zend_Db_Select($this->db);
        $select->from('t_kpi_filters')
               ->where('f_kpi=' . $this->kpiID)
               ->where("f_filter='" . $filter . "'");
        $res = $select->query()->fetchAll();
        if(count($res) == 1){
            $this->filterID = $res[0]['f_id'];
        }
        // filter not exist --> create new 
        else{
            $this->db->insert('t_kpi_filters', array('f_kpi' => $this->kpiID, 'f_filter' => $filter));
            // get new filterID
            $select->reset();
            $select->from('t_kpi_filters')
                   ->where('f_kpi = ' . $this->kpiID)
                   ->where('f_filter = "' . $filter . '"');
            $res = $select->query()->fetchAll();
            $this->filterID = $res[0]['f_id'];
        }
        $this->filterName = $filter;
    }
    
    public function get_ui(){
        // get filter info
        $select = new Zend_Db_Select($this->db);
        $select->from('t_kpi_ui')
               ->where('f_kpi=' . $this->kpiID);
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $result['opt' . $i] = json_decode($res[$i]['f_values']);
        }
        return $result;
    }
    
    
    
    public function get_data($from = null, $to = null){
        // get filter info
        $select = new Zend_Db_Select($this->db);
        $select->from('t_kpi_values')
               ->where('f_filter=' . $this->filterID);
        // time period
        if($from && $to){
            $select->where('f_time >= ' . $from  . ' AND f_time <= ' . $to);
        }
        elseif($from){
            $select->where('f_time >= ' . $from);
        }
        elseif($to){
            $select->where('f_time <= ' . $to);
        }
        
        $select->order('f_time DESC');
        $res = $select->query()->fetchAll();
		$result = array();
        // format data for javascript graph
        for($i = 0; $i < count($res); $i++){
            if($res[$i]['f_values'] != "null"){  
                $aux = get_object_vars(json_decode($res[$i]['f_values']));
                foreach($aux as $key => $value){
                    $result[$key][] = array($res[$i]['f_time'] * 1000, $value);
                    //print($key . " " . $value . "<br>");
                }
            }
        }
        foreach($result as $r){
            $result1[] = $r;
        }
        return $result1;
    }
    
    public function update($filters){
        $startTime = strtotime(date("Y-m-d"));
        $endTime = mktime(0, 0, 0, 1, 1, 2012);
        $select = new Zend_Db_Select($this->db);
        while($startTime > $endTime){
            
            // check if exists kpi values for this date
            $select->reset();
            $select->from('t_kpi_values')
                   ->where('f_time = ' . $startTime)
                   ->where('f_filter = ' . $this->filterID);
            $res = $select->query()->fetchAll();
            // if exists values for this date stop update
            if(count($res) == 1){
                break;
            }
            // else calculate kpi values
            else{
                $value = $this->{$this->kpiName}($startTime, $filters);
                // print(date("Y-m-d", $startTime) . "<br>");
                // update database
                try{
                    //print(date("Y-m-d", $startTime) . "<br>");
                    $this->db->insert('t_kpi_values', array('f_time' => $startTime, 'f_filter' => $this->filterID, 'f_values' => json_encode($value)));
                }
                catch(Exception $e){
                    print($e->getMessage());
                }
            }
            $startTime -= 86400;
        }
    }
    
    public function wos_trends($timestamp, $filters){
        if(date('Ymd') == date('Ymd', $timestamp)){
            $select = new Zend_Db_Select($this->db);
            $select->from('t_workorders', array())
                   ->join('t_creation_date', 't_workorders.f_code = t_creation_date.f_id', array('f_phase_id', new Zend_Db_Expr('count(*) AS tot')))
                   ->join('t_custom_fields', 't_creation_date.f_id = t_custom_fields.f_code', array());
            // filter by type
            if($filters->f_type_id){
                $select->where('t_workorders.f_type_id = ' . $filters->f_type_id);
            }
            else{
                $select->where('t_workorders.f_type_id in (1,4)');
            }
            // filter by discipline
            if($filters->discipline){
                $select->where("t_custom_fields.discipline = '" . $filters->discipline . "'");
            }
            $select->group('t_creation_date.f_phase_id');
            $res = $select->query()->fetchAll();
            for($i = 0; $i < count($res); $i++){
                switch($res[$i]['f_phase_id']){
                    // open
                    case '1':
                        $open += $res[$i]['tot'];
                        break;
                    /*
                    // issued
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    //case '7':
                        $issued += $res[$i]['tot'];
                        break;
                     * 
                     */
                    // closed
                    case '6':
                        $closed += $res[$i]['tot'];
                        break;
                }
                $result['open'] = $open ? $open : 0;
                //$result['issued'] = $issued ? $issued : 0;
                $result['issued'] = $open + $closed;
                $result['closed'] = $closed ? $closed : 0;
            }
        }
        else{
            // get workorders created before $timestamp
            $selectWo = new Zend_Db_Select($this->db);
            $selectWo->from('t_workorders', array('f_code'))
                    ->join('t_custom_fields', 't_workorders.f_code = t_custom_fields.f_code', array());
            // filter by type
            if($filters->f_type_id){
                $selectWo->where('t_workorders.f_type_id = ' . $filters->f_type_id);
            }
            else{
                $selectWo->where('t_workorders.f_type_id IN (1,4)');
            }
            // filter by discipline
            if($filters->discipline){
                $selectWo->where("t_custom_fields.discipline = '" . $filters->discipline . "'");
            }
            $selectWo->where('t_workorders.f_timestamp <= ' . $timestamp);
           
            $resWo = $selectWo->query()->fetchAll();
            
            // get value for timestamp
            for($i = 0; $i < count($resWo); $i++){
                $status = $this->get_timestamp_value('t_creation_date', 'f_phase_id', $timestamp, $resWo[$i]['f_code']);
                switch($status){
                    // open
                    case '1':
                        $open++;
                        break;
                    // issued
                    /*
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '7':
                        $issued++;
                        break;
                     * 
                     */
                    // closed
                    case '6':
                        $closed++;
                        break;
                }
            }
            $result['open'] = $open ? $open : 0;
            //$result['issued'] = $issued ? $issued : 0;
            $result['issued'] = $open + $closed;
            $result['closed'] = $closed ? $closed : 0;
        }
        return $result;      
    }
    
    public function get_timestamp_value($table, $field, $timestamp, $f_code){
        // get value for timestamp closest to timestamp required
        $select = new Zend_Db_Select($this->db);
        $select->from($table . '_history', array($field, 'f_code'))
               ->where('f_timestamp > ' . $timestamp)
               ->where('f_code = ' . $f_code)
               ->limit(1);
        $res = $select->query()->fetchAll();
        // if there are no old values ​​get the current
        if(count($res) == 0){
            $select->reset();
            $select->from($table, array($field))
                   ->where(($table != 't_creation_date' ? 'f_code = ' : 'f_id = ') . $f_code);
            $res = $select->query()->fetchAll();
        }
        //print_r($res);
        return $res[0][$field];
    }
    
    public function trend($type, $params = null){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        
        $select = new Zend_Db_Select($this->db);
        $additionalWhere = array(
            'open-wo'=>array($this->db->quoteInto('t3.f_type_id IN (?)', array(1,4,5,6,7,10,13,20))),
            'work-request'=>array($this->db->quoteInto('t3.f_type_id IN (?)', array(12))),
            'periodic'=>array($this->db->quoteInto('t3.f_type_id IN (?)', array(4,5))),
            'emergency'=>array($this->db->quoteInto('t3.f_type_id IN (?)', array(13))),
            'priority'=>array($this->db->quoteInto('t3.f_type_id IN (?)', array(1,4,5,6,7,10,13,20))),
            
        );
        
        
        $select->from(array('t1'=>'t_creation_date'), array("f_id"))
            ->join(array('t2'=>'t_wf_phases'), 't1.f_phase_id = t2.f_number AND t1.f_wf_id = t2.f_wf_id', array())
            ->join(array('t3'=>'t_workorders'), 't3.f_code = t1.f_id', array())
            ->join('t_custom_fields', 't1.f_id = t_custom_fields.f_code', array())
            ->join('t_workorders_parent', 't1.f_id = t_workorders_parent.f_code', array())
            ->join(array("wp" => "t_wf_phases"), "t1.f_phase_id = wp.f_number AND t1.f_wf_id = wp.f_wf_id", array())
            ->join('t_wf_groups', 't2.f_group_id = t_wf_groups.f_id', array())
            ->joinLeft(array("sw" => "t_selector_wo"), 't3.f_code = sw.f_wo_id', array())
            ->joinLeft(array("st" => "t_selectors_types"), 'sw.f_selector_id = st.f_id', array())
            ->where("t_wf_groups.f_id NOT IN (6,7,8)")/*f_group ('Closing', 'Deleting','Cloned')*/
            ->where("t1.f_visibility & " . $userData->f_level . " != 0")
            ->where("t1.f_visibility & " . $userData->f_group_level . " != 0")
            ->where("t2.f_visibility & " . $userData->f_level . " != 0")
            ->where("t_wf_groups.f_visibility & " . $userData->f_level . " != 0")
            ->where("t_workorders_parent.f_active = 1");  
     
        if(isset($additionalWhere[$type])) {
            $tot = count($additionalWhere[$type]);
            for($i = 0;$i < $tot;++$i) {
                $select->where($additionalWhere[$type][$i]);
            }
        }
        
        //Control on user
        if($userData->f_level == 8 ){
             $select->where('t1.f_creation_user = ' . $userData->f_id );
          }
          
          if($userData->f_level == 32 ){ 
            $select->join('t_ware_wo', 't_ware_wo.f_wo_id = t3.f_id', array());
            $select->where('t_ware_wo.f_ware_id = ' . $userData->f_id);  
          }
          
          $selectorsList=array( $userData->selectors );

          for($k=0; $k<count($selectorsList); $k++){
            if(!empty($selectorsList[$k])) $where[] =  'sw.f_selector_id IN (' . $selectorsList[$k]. ')';
        }
        
        if($where){
            $select->where('' . implode(' AND ', $where) );  //. ' OR sw.f_selector_id IS NULL)');
        }
        
        $select->distinct();
           
        if($type == 'priority'){
            
            $selectPriority = new Zend_Db_Select($this->db);
            $selectPriority->from('t_creation_date', array())
                   ->join('t_wares', 't_creation_date.f_id = t_wares.f_code', new Zend_Db_Expr('max(fc_action_priority_value) AS max'))
                   ->where("fc_action_type = 'priority'")->where("t_creation_date.f_phase_id = 1");
            $resPriority = $selectPriority->query()->fetchAll();
            $maxPriority = $resPriority[0]['max'];
            $select->where('t3.f_priority = ' . $maxPriority);
        }
       // var_dump($select);
       // echo $select;
        $res = $select->query()->fetchAll();
        return array(
            'value' => count($res),
            'index' => (int)$params->index
        );
    }
    
    /*****************************************
     * NEW KPI
     *
    *****************************************/
    public function data($type, $period){
        
        $select = new Zend_Db_Select($this->db);
        $select->from('t_kpi', array('f_data'))
               ->where("f_type ='" . $type . "'")
               ->where("f_period ='" . $period . "'");
        $res = $select->query()->fetchAll();
        return $res[0]['f_data'];
        
    }
    
    public function updateOpenedWo(){
        
        $to = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
        $from = mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));
        
        // get all opened wo
        $select = new Zend_Db_Select($this->db);
        $select->from('t_creation_date', array('f_category', 'count(*) AS tot'))
               ->where("f_type ='WORKORDERS'")
               ->where('f_creation_date >= '  . $from . ' AND f_creation_date < ' . $to)
               ->group('f_category');
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            if($i % 2 == 0){
                $color = '#86deb4';
            }
            else{
                $color = '#eb4a17';
            }
            $data[] = array(
                'value' => $res[$i]['tot'],
                'color' => $color,
                'highlight' => '#f46538',
                'label' => $res[$i]['f_category']
            );
        }
        
        $updateDataYesterday = array(
            'f_type' => 'openedwo',
            'f_period' => 'yesterday',
            'f_data' => json_encode($data)
        );
        
        // ******************************
        // update yesterday
        // ******************************
        $select->reset();
        $select->from('t_kpi')
               ->where("f_type = 'openedwo' && f_period = 'yesterday'");
        $res = $select->query()->fetchAll();
        // update
        if(count($res) > 0){
            $this->db->update('t_kpi', $updateDataYesterday, "f_type = 'openedwo' AND f_period = 'yesterday'");
        }
        // first insert
        else{
            $this->db->insert('t_kpi', $updateDataYesterday);
        }
        
        // ******************************
        // update week
        // ******************************
        $select->reset();
        $select->from('t_kpi')
               ->where("f_type = 'openedwo' && f_period = 'this week'");
        $resWeek = $select->query()->fetchAll();
        // update
        if(count($resWeek) > 0){
            
            $dataToUpdate = json_decode($resWeek[0]['f_data']);
            for($i = 0; $i < count($data); $i++){
                $found = false;
                for($j = 0; $j < count($dataToUpdate); $j++){
                    if($data[$i]['label'] == $dataToUpdate[$j]->label){
                        $found = true;
                        $dataToUpdate[$j]->value += $data[$i]['value'];
                    }
                }
                // new category added
                if(!$found){
                    $dataToUpdate[] = (object)$data[$i];
                }
            }
            // update to db
            // new week -> reset current week e update last week
            if($this->changeWeek()){
                $select->reset();
                $select->from('t_kpi')
                       ->where("f_type = 'openedwo' && f_period = 'last week'");
                $resLastWeek = $select->query()->fetchAll();
                // update last week
                if(count($resLastWeek) > 0){
                    $this->db->update('t_kpi', array('f_data' => json_encode($dataToUpdate)), "f_type = 'openedwo' AND f_period = 'last week'");
                }
                // first insert last week
                else{
                    $this->db->insert('t_kpi', array("f_type" => "openedwo", "f_period" => "last week", "f_from" => $resWeek[0]['f_from'], "f_to" => $resWeek[0]['f_to'], "f_data" => json_encode($dataToUpdate)));
                }
                // reset 'this week'
                $this->db->update('t_kpi', array('f_data' => '', 'f_from' => mktime(0, 0, 0, date('n'), 1, date('Y')), 'f_to' => mktime(0, 0, 0, date('n'), date('t'), date('Y'))), "f_type = 'openedwo' AND f_period = 'this week'");
            }
            // week not changed --> update 'this week'
            else{
                $this->db->update('t_kpi', array('f_data' => json_encode($dataToUpdate)), "f_type = 'openedwo' && f_period = 'this week'");
            }
        }
        // first insert
        else{
            $updateDataWeek = array(
                'f_type' => 'openedwo',
                'f_period' => 'this week',
                'f_from' => mktime(0, 0, 0, date('n'), 1, date('Y')),
                'f_to' => mktime(0, 0, 0, date('n'), date('t'), date('Y')),
                'f_data' => json_encode($data)
            );
            $this->db->insert('t_kpi', $updateDataWeek);
        }

        // ******************************
        // update month
        // ******************************
        $select->reset();
        $select->from('t_kpi')
               ->where("f_type = 'openedwo' && f_period = 'this month'");
        $resMonth = $select->query()->fetchAll();
        // update
        if(count($resMonth) > 0){
            
            $dataToUpdate = json_decode($resMonth[0]['f_data']);
            for($i = 0; $i < count($data); $i++){
                $found = false;
                for($j = 0; $j < count($dataToUpdate); $j++){
                    if($data[$i]['label'] == $dataToUpdate[$j]->label){
                        $found = true;
                        $dataToUpdate[$j]->value += $data[$i]['value'];
                    }
                }
                // new category added
                if(!$found){
                    $dataToUpdate[] = (object)$data[$i];
                }
            }
            // update to db
            // new month -> reset current month e update last month
            if($this->changeMonth()){
                $select->reset();
                $select->from('t_kpi')
                       ->where("f_type = 'openedwo' && f_period = 'last month'");
                $resLastMonth = $select->query()->fetchAll();
                // update last month
                if(count($resLastMonth) > 0){
                    $this->db->update('t_kpi', array('f_data' => json_encode($dataToUpdate)), "f_type = 'openedwo' AND f_period = 'last month'");
                }
                // first insert last month
                else{
                    $this->db->insert('t_kpi', array("f_type" => "openedwo", "f_period" => "last month", "f_from" => $resMonth[0]['f_from'], "f_to" => $resMonth[0]['f_to'], "f_data" => json_encode($dataToUpdate)));
                }
                // reset 'this month'
                $this->db->update('t_kpi', array('f_data' => '', 'f_from' => mktime(0, 0, 0, date('n'), 1, date('Y')), 'f_to' => mktime(0, 0, 0, date('n'), date('t'), date('Y'))), "f_type = 'openedwo' AND f_period = 'this month'");
            }
            // month not changed --> update 'this month'
            else{
                $this->db->update('t_kpi', array('f_data' => json_encode($dataToUpdate)), "f_type = 'openedwo' && f_period = 'this month'");
            }
        }
        // first insert
        else{
            $updateDataMonth = array(
                'f_type' => 'openedwo',
                'f_period' => 'this month',
                'f_from' => mktime(0, 0, 0, date('n'), 1, date('Y')),
                'f_to' => mktime(0, 0, 0, date('n'), date('t'), date('Y')),
                'f_data' => json_encode($data)
            );
            $this->db->insert('t_kpi', $updateDataMonth);
        }
        
        // ******************************
        // update year
        // ******************************
        $select->reset();
        $select->from('t_kpi')
               ->where("f_type = 'openedwo' && f_period = 'this year'");
        $resYear = $select->query()->fetchAll();
        // update
        if(count($resYear) > 0){
            
            $dataToUpdate = json_decode($resYear[0]['f_data']);
            for($i = 0; $i < count($data); $i++){
                $found = false;
                for($j = 0; $j < count($dataToUpdate); $j++){
                    if($data[$i]['label'] == $dataToUpdate[$j]->label){
                        $found = true;
                        $dataToUpdate[$j]->value += $data[$i]['value'];
                    }
                }
                // new category added
                if(!$found){
                    $dataToUpdate[] = (object)$data[$i];
                }
            }
            // update to db
            // new year -> reset current year e update last year
            if($this->changeYear()){
                $select->reset();
                $select->from('t_kpi')
                       ->where("f_type = 'openedwo' && f_period = 'last year'");
                $resLastMonth = $select->query()->fetchAll();
                // update last month
                if(count($resLastMonth) > 0){
                    $this->db->update('t_kpi', array('f_data' => json_encode($dataToUpdate)), "f_type = 'openedwo' AND f_period = 'last year'");
                }
                // first insert last year
                else{
                    $this->db->insert('t_kpi', array("f_type" => "openedwo", "f_period" => "last year", "f_from" => $resYear[0]['f_from'], "f_to" => $resYear[0]['f_to'], "f_data" => json_encode($dataToUpdate)));
                }
                // reset 'this year'
                $this->db->update('t_kpi', array('f_data' => '', 'f_from' => mktime(0, 0, 0, date('n'), 1, date('Y')), 'f_to' => mktime(0, 0, 0, date('n'), date('t'), date('Y'))), "f_type = 'openedwo' AND f_period = 'this year'");
            }
            // year not changed --> update 'this year'
            else{
                $this->db->update('t_kpi', array('f_data' => json_encode($dataToUpdate)), "f_type = 'openedwo' && f_period = 'this year'");
            }
        }
        // first insert
        else{
            $updateDataYear = array(
                'f_type' => 'openedwo',
                'f_period' => 'this year',
                'f_from' => mktime(0, 0, 0, date('n'), 1, date('Y')),
                'f_to' => mktime(0, 0, 0, date('n'), date('t'), date('Y')),
                'f_data' => json_encode($data)
            );
            $this->db->insert('t_kpi', $updateDataYear);
        }
        
        // ******************************
        // update history
        // ******************************
        $yesterdayTimestamp = mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));
        $yesterdayNumber = date("j", $yesterdayTimestamp);
        $month = date("m", $yesterdayTimestamp);
        $dayEndMonth = date("t", $yesterdayTimestamp);
        $from = mktime(0, 0, 0, $month, 1, date("Y"));
        $to = mktime(0, 0, 0, $month, $dayEndMonth, date("Y"));
        $select->reset();
        $select->from('t_kpi')
               ->where("f_type = 'openedwo' AND f_period = 'history' AND f_from = " . $from . " AND f_to = " . $to);
        $resHistory = $select->query()->fetchAll();
        if(count($resHistory) > 0){
  
            $dataToUpdate = json_decode($resHistory[0]['f_data']);
            //print_r($dataToUpdate); die();
            $aux = (array)$dataToUpdate[0];
            foreach($dataToUpdate[0] as $key => $value){
                $found = false;
                if($k == $yesterdayNumber){
                        $found = true;
                        $value = $data;
                }
                // new category added
                if(!$found){
                    $dataToUpdate[0][$yesterdayNumber] = (object)$data;
                }
            }

            // update to db
            // new year -> reset current year e update last year
            if($this->changeYear()){
                $select->reset();
                $select->from('t_kpi')
                       ->where("f_type = 'openedwo' && f_period = 'last year'");
                $resLastMonth = $select->query()->fetchAll();
                // update last month
                if(count($resLastMonth) > 0){
                    $this->db->update('t_kpi', array('f_data' => json_encode($dataToUpdate)), "f_type = 'openedwo' AND f_period = 'last year'");
                }
                // first insert last year
                else{
                    $this->db->insert('t_kpi', array("f_type" => "openedwo", "f_period" => "last year", "f_from" => $resYear[0]['f_from'], "f_to" => $resYear[0]['f_to'], "f_data" => json_encode($dataToUpdate)));
                }
                // reset 'this year'
                $this->db->update('t_kpi', array('f_data' => '', 'f_from' => mktime(0, 0, 0, date('n'), 1, date('Y')), 'f_to' => mktime(0, 0, 0, date('n'), date('t'), date('Y'))), "f_type = 'openedwo' AND f_period = 'this year'");
            }
            // year not changed --> update 'this year'
            else{
                $this->db->update('t_kpi', array('f_data' => json_encode($dataToUpdate)), "f_type = 'openedwo' && f_period = 'this year'");
            }
        }
        // first insert
        else{
            
            if(count($data) == 0){
                $data = "{}";
            }
            
            $insertDataHistory = array(
                'f_type' => 'openedwo',
                'f_period' => 'history',
                'f_from' => $from,
                'f_to' => $to,
                'f_data' => json_encode(array(
                    array( $yesterdayNumber => $data)
                ))
            );
            $this->db->insert('t_kpi', $insertDataHistory);
        } 
    }
    
    private function changeMonth(){
        return !(date('n') == date('n', mktime(0, 0, 0, date('m'), date('d') - 1, date("Y"))));
    }
    
    private function changeWeek(){
        return !(date('W') == date('W', mktime(0, 0, 0, date('m'), date('d') - 1, date("Y"))));
    }
    
    private function changeYear(){
        return !(date('Y') == date('Y', mktime(0, 0, 0, date('m'), date('d') - 1, date("Y"))));
    }
    
    public function selectOptions($type){
        
        switch($type){
            
            case 'openedwo':
                $result = array(
                    array(
                        'value' => 'today',
                        'label' => 'TODAY'
                    ),
                    array(
                        'value' => 'yesterday',
                        'label' => 'YESTERDAY'
                    ),
                    array(
                        'value' => 'this week',
                        'label' => 'THIS WEEK'
                    ),
                    array(
                        'value' => 'last week',
                        'label' => 'LAST WEEK'
                    ),
                    array(
                        'value' => 'this month',
                        'label' => 'THIS MONTH'
                    ),
                    array(
                        'value' => 'this year',
                        'label' => 'THIS YEAR'
                    ),
                    array(
                        'value' => 'last year',
                        'label' => 'LAST YEAR'
                    )
                );
        }
        
        return $result;
    }
    
    /*kpi custom hpg - Galanti - 03/05/2016*/
    public function trendhpg($type, $params = null){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        
        $select = new Zend_Db_Select($this->db);
        $additionalWhere = array(
            'open-wohpg'=>array('t3.f_type_id IN (1) and t_wf_groups.f_id NOT IN (6,7,8)'),
            'closed-hpg'=>array('t3.f_type_id IN (1) and t1.f_phase_id = 7'),
            'collaudo-hpg'=>array('t3.f_type_id IN (13) and t_wf_groups.f_id NOT IN (6,7,8)'),
            'attesa-hpg'=>array('t3.f_type_id IN (1) and t1.f_phase_id = 4'),
            
        );
        
        
        $select->from(array('t1'=>'t_creation_date'), array("f_id"))
            ->join(array('t2'=>'t_wf_phases'), 't1.f_phase_id = t2.f_number AND t1.f_wf_id = t2.f_wf_id', array())
            ->join(array('t3'=>'t_workorders'), 't3.f_code = t1.f_id', array())
            ->join('t_custom_fields', 't1.f_id = t_custom_fields.f_code', array())
            ->join('t_workorders_parent', 't1.f_id = t_workorders_parent.f_code', array())
            ->join(array("wp" => "t_wf_phases"), "t1.f_phase_id = wp.f_number AND t1.f_wf_id = wp.f_wf_id", array())
            ->join('t_wf_groups', 't2.f_group_id = t_wf_groups.f_id', array())
            ->joinLeft(array("sw" => "t_selector_wo"), 't3.f_code = sw.f_wo_id', array())
            ->joinLeft(array("st" => "t_selectors_types"), 'sw.f_selector_id = st.f_id', array())
            /*->where("t1.f_visibility & " . $userData->f_level . " != 0")
            ->where("t1.f_visibility & " . $userData->f_group_level . " != 0")
            ->where("t2.f_visibility & " . $userData->f_level . " != 0")
            ->where("t_wf_groups.f_visibility & " . $userData->f_level . " != 0")*/
            ->where('t1.f_creation_user = ' . $userData->f_id )
            ->where("t_workorders_parent.f_active = 1");  
     
        if(isset($additionalWhere[$type])) {
            $tot = count($additionalWhere[$type]);
            for($i = 0;$i < $tot;++$i) {
                $select->where($additionalWhere[$type][$i]);
            }
        }
        
        //Control on user
      /*  if($userData->f_level == 8 ){
             $select->where('t1.f_creation_user = ' . $userData->f_id );
          }
          
          if($userData->f_level == 32 ){ 
            $select->join('t_ware_wo', 't_ware_wo.f_wo_id = t3.f_id', array());
            $select->where('t_ware_wo.f_ware_id = ' . $userData->f_id);  
          }*/
          
          $selectorsList=array( $userData->selectors );

          for($k=0; $k<count($selectorsList); $k++){
            if(!empty($selectorsList[$k])) $where[] =  'sw.f_selector_id IN (' . $selectorsList[$k]. ')';
        }
        
        if($where){
            $select->where('' . implode(' AND ', $where) );  //. ' OR sw.f_selector_id IS NULL)');
        }
        
        $select->distinct();
           
        /*if($type == 'priority'){
            
            $selectPriority = new Zend_Db_Select($this->db);
            $selectPriority->from('t_creation_date', array())
                   ->join('t_wares', 't_creation_date.f_id = t_wares.f_code', new Zend_Db_Expr('max(fc_action_priority_value) AS max'))
                   ->where("fc_action_type = 'priority'")->where("t_creation_date.f_phase_id = 1");
            $resPriority = $selectPriority->query()->fetchAll();
            $maxPriority = $resPriority[0]['max'];
            $select->where('t3.f_priority = ' . $maxPriority);
        }*/
       // var_dump($select);
       // echo $select;
        //echo $select; die;
        $res = $select->query()->fetchAll();
        return array(
            'value' => count($res),
            'index' => (int)$params->index
        );
    }
    
}
