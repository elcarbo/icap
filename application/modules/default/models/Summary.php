<?php

class Mainsim_Model_Summary
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function extra($summary = '',$params = array())
    {
        $RETURN = array();        
        $msmdb = new Zend_Db_Select($this->db);
        $res_summary = $msmdb->from("t_scripts","f_script")
            ->where("f_name = ?",$summary)->query()->fetch();
        if(!empty($res_summary['f_script'])) {
            eval($res_summary['f_script']);            
        }
        return $RETURN;         
    }

    /**
     * Get summary for treegrid parameter passed
     * @param type $params
     * @return string json with summary
     */
    public function getSummary($params)
    {
        $params=json_decode($_POST["params"]);

        $sel = new Zend_Db_Select($this->db);
        $set = "DISABLE_FUNC";
        $setting = $sel ->from("t_creation_date", ["f_description"])
                        ->where("f_title = ?", $set)
                        ->query()->fetch();
        $sel->reset();
        //die(var_dump($setting));
        if (!empty($setting)) {
            try {
                $setting = json_decode($setting["f_description"],1);
                if($setting["woAll"]["enabled"]) {
                    if(in_array($params->module_name,$setting["woAll"]["modules"])) {
                        $text = $setting["woAll"]["options"];
                        print(json_encode($text));
                        die();
                    } 
                }
            } catch (Exception $e) {
                //Silently fail
            }
        }

        $tree = new Mainsim_Model_Treegrid();
        
        // check if exists selector filter from request
        if(isset($params->Selector)){
            $selectorFilterIds = array();
            foreach($params->Selector as $key => $value){
                foreach($value as $key2 => $value2){
                    if($key2 == 's'){
                        $selectorFilterIds = array_merge($selectorFilterIds, $value2);
                    }
                } 
            }
        }
        

        try {
            //$p['_category'] = $tree->getCategoriesByModule($params->module_name, $params->Tab[0]);
            $p['_category'] = $tree->getCategoriesByTypeId($params->Tab[1], $params->Tab[0]);
            $p['_rtree'] = $params->rtree;
            $p['_selectors'] = is_array($selectorFilterIds) ? $selectorFilterIds : null;
            $p['_filters']['_and'] = $params->Ands;
            $p['_filters']['_order'] = $params->Sort;
            $p['_filters']['_search'] = $params->Search;
            $p['_filters']['_like'] = $params->Like;
            $p['_filters']['_filter'] = $params->Filter;
            $p['_hierarchy'] = $params->Hierarchy;
            $p['_module'] = $params->module_name;
            $p['_parent'] = $params->Prnt;
            $p['_self'] = $params->self;
            $p['_ignoreSelector'] = $params->ignoreSelector;
            $p['_summary'] = true;

          

            $data = $tree->getCodes($p);
            
            $lastPhase = '';
            if(isset($p['_filters']['_and'][0]->z) && !preg_match('/inbox/', $p['_filters']['_and'][0]->z)) { 
                $crumbs = explode("\'", $p['_filters']['_and'][0]->z);
                $lastPhase = $crumbs[0];
            }

            if(preg_match('/inbox/', $p['_filters']['_and'][0]->z)) {
                $p['_filters']['_and'] = [];  
            }

            $lastType = 0;
            if(isset($p['_filters']['_and'][1]->z)) { 
                $lastType = preg_replace('/(^.*\(|\).*$)/', '', $p['_filters']['_and'][1]->z);  
            }

            $summary = new Zend_Session_Namespace('summarydata');
            $newPhase = ''; 

            if(!empty($summary->phase)) {
                $sel = new Zend_Db_Select($this->db);
                $res = $sel->from(array("t_wf_phases"), array("f_name"))->where("f_wf_id = ".$summary->phase[0]." AND f_number = ".$summary->phase[1])->query()->fetch();
                $newPhase = $res['f_name'];
                $summary->phase = '';
            }
            
            $fromRefresh = 0;
            if(sizeof($p['_filters']['_and']) == 0) {
                $fromRefresh = 1;
            }

            // format data for summary
            print(json_encode($this->formatSummaryData($data, $params->Tab, $params->module_name, $params->summaryFrom, $lastPhase, $lastType, $newPhase, $fromRefresh)));
            die();
        }
        catch(Exception $e) {
            print($e->getMessage());
            $result['message'] = utf8_decode($e->getMessage());
        }
    }
    
    
    public function getDataByCodes($codes,$table='t_workorders'){
        if(empty($codes)) return;
        $chunks = array_chunk($codes, 5000);
        $select = new Zend_Db_Select($this->db);
        $result = array();
        for($i = 0; $i < count($chunks); $i++){
            $select->reset();
            $select->from($table, array('f_type_id'))
                   ->join('t_creation_date', 't_creation_date.f_id = '.$table.'.f_code', array())
                   ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array('f_summary_name'))
                   ->where('t_creation_date.f_id IN (' . implode(',', $chunks[$i]) . ')');
            $res = $select->query()->fetchAll();
            $result = array_merge($result, $res);
        }
        return $result;
    }
    
    public function getPhasesOrder($types){
        $select = new Zend_Db_Select($this->db);
        $select->distinct()->from($types[0].'_types', array())
               ->join('t_wf_phases', $types[0].'_types.f_wf_id = t_wf_phases.f_wf_id', array('f_summary_name', 'f_order'))
               ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
               ->where($types[0].'_types.f_id IN (' . $types[1] . ')')
               ->where("t_wf_phases.f_summary_name <> ''")
               ->order('f_order');
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $result[] = $res[$i]['f_summary_name'];
        }
        return array_values(array_unique($result));
    }
    
    public function getWoSummaryLabelsByTypes($types){
        $select = new Zend_Db_Select($this->db);
        $select->from($types[0].'_types', array('f_id AS wo_type_id', 'f_summary_name AS wo_type_name'))
            ->join('t_wf_phases', $types[0].'_types.f_wf_id = t_wf_phases.f_wf_id', array('f_summary_name AS phase_name'))
            ->where($types[0].'_types.f_id IN (' . $types[1] . ')')
            ->where("t_wf_phases.f_summary_name <> ''");
        $res = $select->query()->fetchAll();
        return $res;
    }
    
    public function formatSummaryData($data, $types, $module, $summaryFrom, $lastPhase, $lastType, $newPhase, $fromRefresh) {
        $select = new Zend_Db_Select($this->db);
        $res = $this->getWoSummaryLabelsByTypes($types);
        $phasesOrder = $this->getPhasesOrder($types);
        
        for($i = 0; $i < count($res); $i++) {
            // create return object skeleton (wo phase labels and wo type labels)
            if(!isset($result[$res[$i]['phase_name']])) {
                $result[$res[$i]['phase_name']] = array(
                    "value" => "'" . Mainsim_Model_Utilities::chg($res[$i]['phase_name']) . "'",
                    "count" => 0,
                    "label" => $res[$i]['phase_name'],
                    "description" => "",
                    "types" => array(
                        $res[$i]['wo_type_id'] => [
                            "label" => $res[$i]['wo_type_name'],
                            "count" => 0,
                            "key" => $types[0].".f_type_id",
                            "value" => $res[$i]['wo_type_id']
                        ]
                    ),
                    "key" => "t_wf_phases.f_summary_name"
                );
            } else if(!isset($result[$res[$i]['phase_name']]['types'][$res[$i]['wo_type_id']])) {
                $result[$res[$i]['phase_name']]['types'][$res[$i]['wo_type_id']] = [
                    "label" => $res[$i]['wo_type_name'],
                    "count" => 0,
                    "key" => $types[0].".f_type_id",
                    "value" => $res[$i]['wo_type_id']
                ];
            }
        }

        $summary = new Zend_Session_Namespace('summarydata');
    
        if(!isset($summary->module[$module]) || strlen($summary->module[$module]) == 0) {
            $refresh = 0;
            $summary->module[$module] = $module;
            $summary->data[$module] = $data;
        } else {
            $refresh = 1;
            if($summary->module[$module] != $module) {
                $refresh = 0;
                $summary->module[$module] = $module;
                $summary->data[$module] = $data;
            }
        }
        
        
        /*
        if($refresh == 1 && $summaryFrom == 1 && is_array($data)) {
            for($i = sizeof($summary->data[$module]) - 1; $i >= 0; --$i) {
                for($j = 0; $j < sizeof($data); ++$j) {
                    if($summary->data[$module][$i]['f_summary_name'] == $data[$j]['f_summary_name']) {
                        array_splice($summary->data[$module], $i, 1);
                    } 
                }                  
            } 
            $summary->data[$module] = array_merge($summary->data[$module], $data);
        }

        if($refresh == 1 && $summaryFrom == 2 && is_array($data)) {
            for($i = sizeof($summary->data[$module]) - 1; $i >= 0; --$i) {
                for($j = 0; $j < sizeof($data); ++$j) {
                    if($summary->data[$module][$i]['f_summary_name'] == $data[$j]['f_summary_name'] && $summary->data[$module][$i]['f_type_id'] == $lastType) {
                        array_splice($summary->data[$module], $i, 1);
                    } 
                }                  
            } 
            $summary->data[$module] = array_merge($summary->data[$module], $data);
        }*/

        if(!is_array($data) && strlen($data) == 0) {
            $loopFlag = false;
            for($i = sizeof($summary->data[$module]) - 1; $i >= 0; --$i) {
                if($summary->data[$module][$i]['f_summary_name'] == $lastPhase && $summary->data[$module][$i]['f_type_id'] == $lastType && !$loopFlag) {
                    array_splice($summary->data[$module], $i, 1);
                    $loopFlag = true;
                }                
            } 
        }

        if(!empty($newPhase)) {
            array_push($summary->data[$module], ['f_type_id' => $lastType, 'f_summary_name' => $newPhase]);    
        }

        if($refresh == 1 && $fromRefresh == 1) {
            $summary->data[$module] = $data;   
        }

        for($i = 0; $i < count($summary->data[$module]); $i++) {
            $result[$summary->data[$module][$i]['f_summary_name']]['count'] += $summary->data[$module][$i]['total'];
            $result[$summary->data[$module][$i]['f_summary_name']]['types'][$summary->data[$module][$i]['f_type_id']]['count'] = $summary->data[$module][$i]['total'];
        }

        // result ordered by group order
        for($i = 0; $i < count($phasesOrder); $i++){
            $resultOrdered[] = $result[$phasesOrder[$i]];
        }
        return $resultOrdered;
    }
    
    public function getInbox($params)
    {
      /* 
        $user = Zend_Auth::getInstance()->getIdentity();
        $uid = $user->f_id;        
        $user_group_level = $user->f_group_level;
        $user_level = $user->f_level;                
        if($user_group_level != -1) {
            $user_level+=$user_group_level;
        }
        $tab = $params->Tab[0];        
        $f_type = $params->Tab[1];
        $treegrid = new Mainsim_Model_Treegrid();        
        $t_sst = (microtime(true)*10000).'_'.$uid.'_'.rand(0,9);                
        $resParams= $treegrid->prepareArrayTreegrid($params,$t_sst);        
        $tgdParamsToMe = $resParams['params'];        
        $tgdParamsFromMe = $resParams['params'];        
        $tgdParamsToMe["join"][] = array(array("t3"=>"t_ware_wo"),"t1.f_code = t3.f_wo_id",array());
        $tgdParamsToMe["where"][] = "t3.f_ware_id = $uid";
        //$tgdParamsToMe['where'][] = "(t_creation_date.f_editability & $user_level) != 0";
        $tgdParamsFromMe['where'][] = "(t_creation_date.f_creation_user = $uid)";
        $cols = array('mainTableCols'=>array('count'=>'count(t1.f_code)'));                        
        $gToMe = $treegrid->treegridQuery($tab, $f_type, $cols, $tgdParamsToMe);       
        $gFromMe = $treegrid->treegridQuery($tab, $f_type, $cols, $tgdParamsFromMe);     
       */

        $user = Zend_Auth::getInstance()->getIdentity();

        $tree = new Mainsim_Model_Treegrid();

        $p1['_category'] =  $tree->getCategoriesByTypeId($params->Tab[1], $params->Tab[0]);
        $p1['_rtree'] = $params->rtree;
        $p1['_selectors'] = null;
        $p1['_filters']['_and'] = [
                                0 => (object)[
                                    'f' => 't_wf_phases.f_summary_name',
                                    'z' => " IN ('inbox')"
                                ],
                                1 => (object)[
                                    'f' => 'f_type_id',
                                    'z' => " IN (".$params->Tab[1].")"
                                ]
        ];
        $p1['_filters']['_order'] = $params->Sort;
        $p1['_filters']['_search'] = $params->Search;
        $p1['_filters']['_like'] = $params->Like;
        $p1['_filters']['_filter'] = $params->Filter;
        $p1['_hierarchy'] = $params->Hierarchy;
        $p1['_module'] = $params->module_name;
        $p1['_parent'] = $params->Prnt;
        $p1['_self'] = $params->self;
        $p1['_ignoreSelector'] = $params->ignoreSelector;

        $codes1 = $tree->getCodes($p1);
        //$data1 = $this->getDataByCodes($codes1, $params->Tab[0]);
        $count1 = sizeof($codes1);

        $p2['_category'] = $tree->getCategoriesByTypeId($params->Tab[1], $params->Tab[0]);
        $p2['_rtree'] = $params->rtree;
        $p2['_selectors'] =null;
        $p2['_filters']['_and'] = [
                                0 => (object)[
                                    'f' => 't_wf_phases.f_summary_name',
                                    'z' => " IN ('inboxFromMe')"
                                ],
                                1 => (object)[
                                    'f' => 'f_type_id',
                                    'z' => " IN (".$params->Tab[1].")"
                                ]
        ];
        $p2['_filters']['_order'] = $params->Sort;
        $p2['_filters']['_search'] = $params->Search;
        $p2['_filters']['_like'] = $params->Like;
        $p2['_filters']['_filter'] = $params->Filter;
        $p2['_hierarchy'] = $params->Hierarchy;
        $p2['_module'] = $params->module_name;
        $p2['_parent'] = $params->Prnt;
        $p2['_self'] = $params->self;
        $p2['_ignoreSelector'] = $params->ignoreSelector;

        $codes2 = $tree->getCodes($p2);
        //$data2 = $this->getDataByCodes($codes2, $params->Tab[0]);
        $count2 = sizeof($codes2);

        $inbox = array(array(
            "value" => "inbox",
            "count" => $count1,
            "label" => "",
            "description" => "WO assigned to me",
            "key" => "t_wf_phases.f_summary_name",
            "icon" => $user->fc_usr_avatar
        ), array(
            "value" => "inboxFromMe",
            "count" => $count2,
            "label" => "",
            "description" => "WO requested by me",
            "key" => "t_wf_phases.f_summary_name",
            "icon" => $user->fc_usr_avatar
        ));
        return $inbox;
    }
    
    /**
     * Create group for summary
     * @param type $tab table of object
     * @param type $f_type type(s) of object
     */
    private function createGroups($tab,$f_type)
    {
        $select = new Zend_Db_Select($this->db);
        $wf_groups = array();
        //get wf list order by number of phase
        $resWf = $select->from(array("t2"=>"t_wf_phases"),array('f_wf_id','num'=>'count(*)'))
            ->join(array("t3"=>$tab."_types"),"t2.f_wf_id = t3.f_wf_id",array())
            ->where("t3.f_id IN ($f_type)")->group('t2.f_wf_id')
            ->order("count(*) DESC")->query()->fetchAll();
        $tot = count($resWf);
        for($a = 0;$a < $tot;++$a) {
            $select->reset();
            $resPhase = $select->from('t_wf_phases',array('f_order','f_summary_name'))
                ->where('f_wf_id = ?',$resWf[$a]['f_wf_id'])->query()->fetchAll();
            $totB = count($resPhase);
            for($b = 0;$b < $totB;++$b) {
                if(empty($resPhase[$b]['f_summary_name']) || array_key_exists($resPhase[$b]['f_summary_name'], $wf_groups)) {
                    continue;
                }
                $wf_groups[$resPhase[$b]['f_summary_name']] = $resPhase[$b]['f_order'];
            }
        }
        asort($wf_groups);
        $wf_groups = array_keys($wf_groups);
        return $wf_groups;
    }
}