<?php
//ini_set("display_errors","On");
//error_reporting(E_ALL);
class Mainsim_Model_PdfCreator
{    
   /**
    *
    * @var Zend_Db 
    */
   private $db;
   public function __construct() {
       $this->db = Zend_Db::factory(Zend_Registry::get('db'));
   }
   
   public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function printPdf(&$params) 
    {       
        $select = new Zend_Db_Select($this->db);
        $data = $select->from("t_scripts")
            ->where("f_name = ?", $params["script_name"])->where("f_type = 'php'")        
            ->query()->fetch();
        $f_code = $params["f_code"];
        $fcode_asset = [];          
        $codes = explode(",", $f_code); 
        if((count($codes) == 1 && $codes[0] == 0 && empty($params['mockup'])) && !isset($params['nocodepdf'])) { /* if no codes has been passed */	            
             if(isset($_SESSION['pdfParams'])){ /**execute treegrid query */ 
                $sessionParams = $_SESSION['pdfParams'];
                //$treegrid = new Mainsim_Model_Treegrid();
                //$t_sst = (microtime(true)*10000).'_'.Zend_Auth::getInstance()->getIdentity()->f_id.'_'.rand(0,9);
                //$cols = ['mainTableCols'=>['f_code']];
                
                //$resParams= $treegrid->prepareArrayTreegrid($sessionParams,$t_sst);
                //$treegridParams = $resParams['params'];        		
                //$g = $treegrid->treegridQuery($sessionParams->Tab[0], $sessionParams->Tab[1], $cols, $treegridParams); 
                //$codes = array_map(function($line){ return (int)$line['f_code'];}, $g);
                //$this->db->delete("t_selectors_support","f_call like '{$t_sst}%'");
                               
                $tree = new Mainsim_Model_Treegrid();
                if(isset($sessionParams->Selector)){
                    $selectorFilterIds = array();
                    foreach($sessionParams->Selector as $key => $value){
                        foreach($value as $key2 => $value2){
                            if($key2 == 's'){
                                $selectorFilterIds = array_merge($selectorFilterIds, $value2);
                            }
                        } 
                    }
                }
                $p['_fromPicklist'] = false;
                $p['_category'] = $tree->getCategoriesByTypeId($sessionParams->Tab[1], $sessionParams->Tab[0]);                
                //$p['_category'] = $tree->getCategoriesByModule($sessionParams->module_name, $sessionParams->Tab[0]);
                $p['_rtree'] = $sessionParams->rtree;
                $p['_selectors'] = is_array($selectorFilterIds) ? $selectorFilterIds : null;
                $p['_filters']['_and'] = $sessionParams->Ands;
                $p['_filters']['_order'] = $sessionParams->Sort;
                $p['_filters']['_search'] = $sessionParams->Search;
                $p['_filters']['_like'] = $sessionParams->Like;
                $p['_filters']['_filter'] = $sessionParams->Filter;
                $p['_hierarchy'] = $sessionParams->Hierarchy;
                $p['_self'] = $sessionParams->self;
                $p['_module'] = $sessionParams->module_name;
                $p['_ignoreSelector'] = $sessionParams->ignoreSelector;
                $p['_limit'] = null; // Limit used to send only data that would be displayed in view
                $p['_parent'] = $sessionParams->Prnt > 0 ? $sessionParams->Prnt : -1; // Level of hierarchy to show
                $codes = $tree->getCodes($p);
            }
            else {
                $trsl = new Mainsim_Model_Translate(null,$this->db);            
                $trsl->setLang($trsl->getUserLang($userinfo->f_language)); 
                throw new Exception($trsl->_('No filter found'));
            }
        }
        eval($data["f_script"]);
        return $pdf;     
    }
}