<?php

class Mainsim_Model_Wares 
{
    private $db,$table;    
    
    public function __construct($db = null) {        
        $this->db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $this->table = 't_wares';
        $this->obj = new Mainsim_Model_MainsimObject();
        $this->obj->type = $this->table;
    }
    
    /**
     * Edit existing ware
     * @param type $params params to edit 
     * @param type $edit_batch true if called from editBatch Method, otherwise false
     * @param type $userinfo unset by default(null),set only if come from 'updateUser' method in 'Login.php' model
     * @return string|array array empty if ok or array with message of error
     * @throws Exception 
     */
    public function editWares($params, $edit_batch = false,$commit = true,$userinfo = null)
    {          

        $error = [];
        $params = Mainsim_Model_Utilities::clearEmptyValuesParams($params);        
        unset($params['f_id']);
        //print_r($params); die(); 
        $new_ware = []; $new_creation = [];
        $custom_fields = []; $users = [];
        $time = time();
        if(Zend_Registry::isRegistered('time')) {
            $time = Zend_Registry::get('time');
        }
        else {
            Zend_Registry::set('time', $time);
        }
        try {
                       
            if(isset($params['utente_ex'])) {
                $this->db->query('DELETE FROM t_wares_relations WHERE f_code_ware_master = '.$params['f_code'].' AND f_code_ware_slave = '.$params['utente_ex']);
                $this->db->query('DELETE FROM t_wares_relations WHERE f_code_ware_slave = '.$params['f_code'].' AND f_code_ware_master = '.$params['utente_ex']);
            }
            //$userinfo unset by default(null),set only if come from 'updateUser' method in 'Login.php' model
            if(!isset($userinfo))$userinfo = Zend_Auth::getInstance()->getIdentity(); 
             //if is a normal new wo, start transaction
            if($commit){ 
                $this->db->beginTransaction();                
            } 
            Zend_Registry::set('db_connection_editor',$this->db);
            $sel = new Zend_Db_Select($this->db);
            //check if this ware is your or not
            if($params['f_code'] != $userinfo->f_id) {
                //print_r($params); die();   
                $check_lock = $sel->from("t_locked")->where("f_code = ?",$params['f_code'])->where("f_user_id = ?",$userinfo->f_id)
                        ->query()->fetch();
                if(empty($check_lock)) {
                    throw new Exception("Element locked by another user.");
                }            
            } else { if(!$params['f_type_id']) $params['f_type_id']=16; }
            // end mod 04/02/13 AC     
            $columns = Mainsim_Model_Utilities::get_tab_cols("t_wares");
            $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");           
            $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");           
            $users_columns = Mainsim_Model_Utilities::get_tab_cols("t_users");
                        
            // mod 04/02/13 AC
            $params['fc_editor_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;            
            $params['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_editor_user_gender'] = $userinfo->f_gender;
            $params['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_editor_user_mail'] = $userinfo->fc_usr_mail;
            // update asset balance = budget - total costs
            $params['fc_asset_balance'] = $params['fc_asset_budget_ytd'] - $params['fc_asset_total_costs_ytd'];
            // update asset maintanance cost on replacement value
            $params['fc_asset_m_cost_on_rep_value'] = $params['fc_asset_replacement_costs'] > 0 ? round($params['fc_asset_maintenance_costs'] / $params['fc_asset_replacement_costs'], 2) : '';            
            // end mod 04/02/13 AC            
            $module_name = $params['f_module_name'];            
            //get old wares,creation and custom fields of this f_code
            $olds = array(
                'old_table' => Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],"t_wares"), 
                'old_creation' => Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],"t_creation_date","f_id"),             
                'old_custom' => Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],'t_custom_fields'),  
                'old_user' => Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],'t_users'),
                'old_ware_wo_cross' => Mainsim_Model_Utilities::getCross('t_ware_wo',$params['f_code'],'f_ware_id'),                        
                'old_ware_rel' => Mainsim_Model_Utilities::getCross('t_wares_relations',$params['f_code'],'f_code_ware_master')                        
            );
            if(!$params['f_phase_id']) {
                unset($params['f_phase_id']);
            } 
            if(!isset($params['f_wf_id'])) {
                $params['f_wf_id'] = $olds['old_creation']['f_wf_id'];
            }

            $f_code = $params['f_code']; 
            $params = Mainsim_Model_Utilities::clearChars($params);
            foreach ($params as $key => $param) { //distribute params value between array
                if(in_array($key, $columns)) { $new_ware[$key] = $param; }
                elseif(in_array($key, $creation_columns)) { $new_creation[$key] = $param; }            
                elseif(in_array($key, $custom_columns)) { $custom_fields[$key] = $param; }
                elseif(in_array($key, $users_columns)) { $users[$key] = $param; }
            }  
            //check if there are any unique to check
            if(isset($params['unique']) && !empty($params['unique'])) {                
                foreach($params['unique'] as $line_unique) {
                    Mainsim_Model_Utilities::isUnique($line_unique,$params,$params['f_module_name'], $this->db);                    
                }
            }
            $new_ware['f_timestamp'] = $time; 
            $new_creation['f_timestamp'] = $time;
            $new_ware['f_user_id'] = $userinfo->f_id; 
            // unset fc_asset_wo --> previously updated in waresWokorderIconAssociation
            unset($new_ware['fc_asset_wo']);            
            //update t_wares
            #print_r($new_ware);
            #print_r($new_creation); exit;
            $config = parse_ini_file (APPLICATION_PATH.'/configs/application.ini',true);
            $adapter = $config['production']['database.adapter'];
            try {
                $new_ware = Mainsim_Model_Utilities::setParamsNull($adapter, $new_ware);
                $this->db->update("t_wares",$new_ware,"f_code = $f_code");
            } catch(Exception $e) {
               print "first: ".$e->getMessage();
            }
            //update t_creation_date
            try {
                $new_creation = Mainsim_Model_Utilities::setParamsNull($adapter, $new_creation);
                $this->db->update("t_creation_date",$new_creation,"f_id = $f_code");
            } catch(Exception $e) {
                print "second: ".$e->getMessage();
            }
            $custom_fields['f_code'] = $f_code;            
            $custom_fields['f_timestamp'] = $time;
            try {
                $custom_fields = Mainsim_Model_Utilities::setParamsNull($adapter, $custom_fields);
                $this->db->update("t_custom_fields",$custom_fields,"f_code = $f_code");
            } catch(Exception $e) {
               print "third: ".$e->getMessage();
            }
            if(!empty($users)) {                
                unset($users["f_id"]);
               // var_dump($users); die();
               // if($users['fc_usr_password'] != '') $users['fc_usr_pwd_registration'] = time();
                try {
                    $users = Mainsim_Model_Utilities::setParamsNull($adapter, $users);
                    $this->db->update("t_users",$users,"f_code = $f_code");
                } catch(Exception $e) {
                   print "fourth: ".$e->getMessage();
                }
            }
            /*
             * saved wares, create cross with other wares and wo and parent(s)            
             */ 

            $scriptModule = new Mainsim_Model_Script($this->db,"t_wares",$f_code,$params,$olds,$edit_batch);
            try {
                $this->saveCrossAndPair($f_code,$params,$scriptModule,$edit_batch);
            } catch(Exception $e) {
                   print "fifth: ".$e->getMessage();
            }
            //check if there are attachments and add into the system
            Mainsim_Model_Utilities::saveAttachments($f_code, 't_wares', $params, $scriptModule);     
            if(isset($new_creation['f_phase_id']) && $new_creation['f_phase_id'] != $olds['old_creation']['f_phase_id']) {                                    
                $sel->reset();            
                $res_phase = $sel->from(['t1'=>"t_wf_exits"],'f_script')
                    ->join(['t2'=>'t_wf_phases'],"t1.f_wf_id = t2.f_wf_id and t1.f_exit = t2.f_number",['f_group_id'])    
                    ->where("f_phase_number = ?",$olds['old_creation']['f_phase_id'])->where("f_exit = ?",$new_creation['f_phase_id'])
                    ->where("t1.f_wf_id = ?",$new_creation['f_wf_id'])->query()->fetch();                
                if(in_array($res_phase['f_group_id'],[6,7])) {
                    $activeChildren = Mainsim_Model_Utilities::checkActiveChildren("t_wares",$f_code,$this->db);
                    if($activeChildren) { throw new Exception("Close or delete all children elements before"); }
                }
                if(!empty($res_phase['f_script'])){ eval($res_phase['f_script']); }
            }
                        
            if(!empty($module_name)){
                $res_bm = Mainsim_Model_Utilities::getBookmark($module_name);            
                $bm = json_decode(Mainsim_Model_Utilities::chg($res_bm['f_properties']),true);
                foreach($bm as $line_bm) {
                    if(!array_key_exists($line_bm[0], $params)) $params[$line_bm[0]] = '';
                }
            }
            $fields_params = array_keys($params);
            foreach($fields_params as $field) {                   
                if($field == "t_wares_".$params['f_type_id']) continue;                
                
                $sel->reset();                       
                $res_scr = $sel->from("t_scripts")->where("f_name = '$field-t_wares'")->where("f_type = 'php'")->query()->fetch();     
                if(!empty($res_scr['f_script'])) { eval($res_scr['f_script']); }
                elseif(method_exists($scriptModule,$field) && is_callable(array($scriptModule,$field))) {//if not exist try to check if is a custom script in script model                                                            
                    $err_script = $scriptModule->$field();
                    if(isset($err_script['message'])) 
                        throw new Exception($err_script['message']);
                } 
            }  
            if(!empty($params['attachments']) && !($edit_batch)) {
                Mainsim_Model_Utilities::clearTempAttachments($params,$this->db);
            } 
            // NICO: ricreo $params[t_selectors_1,2,3,4,5,6,7,8,9,10] in caso non ci sia
            // Non c'è se il tab dei selettori non è visibile
            if (!isset($params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']) && !isset($params['t_selectors_1,2,3,4,5,6,7,8,9,10'])) {
                $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'] = [];
                $s = new Zend_Db_Select($this->db);
                $s->reset();
                $results = $s->from(["t1"=>"t_selector_ware"])
                        ->join(["t2"=>"t_selectors"],"t1.f_selector_id = t2.f_code", ["f_type_id"])
                        ->where("f_ware_id = ".$f_code)
                        ->query()->fetchAll();   
                $selettori = [];
                foreach ($results as $indice=>$selettore) {
                    $selettori[$selettore['f_type_id']][] = $selettore['f_selector_id'];
                }  
                foreach ($params as $indice=>$par) {
                    if (strpos($indice, 't_selectors_') !== false) {
                        if($indice == 't_selectors_1,2,3,4,5,6,7,8,9,10,11')
                            continue;
                        $selettori[substr($indice,12 )] = $par['f_code'];
                    }
                }
                foreach ($selettori as $k=>$arr) {
                    foreach ($arr as $k2=>$code) {
                        $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'][] = $code;
                    }
                }
                
                $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'] = array_unique($params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code']);
                $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code_main'] = $f_code;
            }   
            // update auxiliary fields for selectors
            $selectors = [];
            foreach ($params as $key => $value) {
                if (strpos($key, 't_selectors_') !== false) {
                    $selectors = array_merge($selectors, $params[$key]['f_code']);
                }
            }
            Mainsim_Model_Utilities::updateAuxSelectorFields('wares', $f_code, $this->db, array_values(array_unique($selectors)));
          
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", ["f_timestamp"=>$time], "Edit");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_wares", ["f_timestamp"=>$time], "Edit");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", ["f_timestamp"=>$time], "Edit");  
            Mainsim_Model_Utilities::saveReverseAjax("t_wares_parent",$f_code,[],"upd",$module_name,$this->db);
            if($commit) {             
                if(empty($error['message'])){
                    $this->db->commit(); 
                    
                }
                elseif(!empty($error['message'])){ $this->db->rollBack(); return $error; }
            }
            
        }catch(Exception $e) {
            if($commit) { 
                $this->db->rollBack();
            }
                $this->db->insert("t_logs",['f_timestamp'=>time(),'f_log'=>$e->getMessage(),'f_type_log'=>0]);
		
			/*
			$prefix = '';
            $trsl = new Mainsim_Model_Translate(null,$this->db);            
            $trsl->setLang($trsl->getUserLang($userinfo->f_language));            
            if($commit) { $this->db->rollBack(); $prefix = $trsl->_("Error").":";}
            $error['message'] = $prefix.' '.$trsl->_($e->getMessage());
            $this->db->insert("t_logs",['f_timestamp'=>time(),'f_log'=>"Error in ".__METHOD__." : ".$e->getMessage(),'f_type_log'=>0]);
            return $error;    */        
        }
       
        return [];
    }   
    
    /**
     * Save cross and pair cross
     * @param int $f_code
     * @param array $params
     * @param Mainsim_Model_Script $scriptModule
     * @param boolean $editBatch
     */
    private function saveCrossAndPair($f_code,&$params,$scriptModule,$editBatch = false)
    {
        $time = Zend_Registry::get('time');
        $pairCols = Mainsim_Model_Utilities::get_tab_cols('t_pair_cross');
        $found = false;
        foreach($params as $key => $value) {
            if($key == "t_wares_".$params['f_type_id'] || !is_array($value) || (empty($value['f_code']) && $editBatch)){
                // NICO: lascio eseguire per i selettori soltanto
                if (!($editBatch && strpos($key, "t_selectors_") !== false))
                    continue;
                
            }
            $ftype = $value['f_type'];
            if(strpos($key,"t_wares_") !== false && strpos($key,"parent") === false  && $ftype > 0 ) {                    
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($value['f_code'],'t_wares_relations',$f_code,'f_code_ware_master',[],'f_code_ware_slave','t_wares', $ftype);
                if(!$checkIdenticalCross) {
                    $this->db->delete("t_wares_relations","f_code_ware_master = $f_code AND f_type_id_slave = $ftype");
                    $this->db->delete("t_wares_relations","f_code_ware_slave = $f_code AND f_type_id_master = $ftype");
                    $this->createCross($f_code, $value['f_code'],$params['f_type_id'],$ftype);
                    $totWaresRelations = count($value['f_code']);
                    for($iWR = 0;$iWR < $totWaresRelations;++$iWR) {
                        $this->createCross($value['f_code'][$iWR],[$f_code],$ftype,$params['f_type_id']);
                    }
                }
            }
            if(strpos($key,"t_workorders_") !== false) {
                $ftype_exp = explode(',',$ftype);
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($value['f_code'],'t_ware_wo',$f_code,'f_ware_id',[],'f_wo_id','t_workorders', $ftype);
                if(!$checkIdenticalCross) {
                    $this->db->delete("t_ware_wo","f_ware_id = $f_code AND f_wo_id IN (SELECT f_code from t_workorders where f_type_id IN ({$ftype}) )");                    
                    Mainsim_Model_Utilities::createCross($this->db, 't_ware_wo', $f_code, 'f_ware_id', $value['f_code'], 'f_wo_id');
                    //se sto modificando la relazione PM/ASSET, lancio controllo su schedulazione pm-per-asset
                    if($params['f_type_id'] == 1 && count(array_intersect($ftype_exp, [3,11,15,19]))) {
                        $this->reschedulePmPerAsset($f_code, $value['f_code']);
                    }
                }
            }
            if(strpos($key,"t_systems_") !== false) {                    
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($value['f_code'],'t_wares_systems',$f_code,'f_ware_id',[],'f_system_id','t_systems', $ftype);
                if(!$checkIdenticalCross) {
                    $this->db->delete("t_wares_systems","f_ware_id = $f_code AND f_system_id IN (SELECT f_code from t_systems where f_type_id IN ({$ftype}) )");
                    Mainsim_Model_Utilities::createCross($this->db, 't_wares_systems', $f_code, 'f_ware_id', $value['f_code'], 'f_system_id');
                }                    
            }

            if(strpos($key,"t_selectors_") !== false) {
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($value['f_code'],'t_selector_ware',$f_code,'f_ware_id',[],'f_selector_id','t_selectors', $ftype);
                if(!$checkIdenticalCross) {                        
                    //if asset, update all workorders with new asset
                    if($params['f_type_id'] == 1) {
						$this->changeWoSelectorFromAsset($f_code,$value['f_code']);
                        //aggiungo/rimuovo l'asset da tutte le pm a cui è associato lo stesso selettore di tipo 11
                        $this->updateAssetsMaintenancePlan($f_code, $value['f_code']);
                    }
                    $this->db->delete("t_selector_ware","f_ware_id = $f_code and f_selector_id IN (SELECT f_code from t_selectors where f_type_id IN ({$ftype}) )");
                    Mainsim_Model_Utilities::createSelectorCross($this->db, $f_code, $value['f_code'], "t_selector_ware", "f_ware_id");
                }                    
            }
            if(strpos($key,"t_wares_parent") !== false) { 
                $found = true;
                $parentCodes = empty($value['f_code'])?[0]:$value['f_code'];                    
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($parentCodes,'t_wares_parent',$f_code,'f_code',[],'f_parent_code');
                if(!$checkIdenticalCross) {
                    $this->db->delete("t_wares_parent", "f_code = $f_code");
                    Mainsim_Model_Utilities::createParents('t_wares_parent',$f_code,$value['f_code'],$this->db);
                }
            }

            if(isset($value['f_pair']) && is_array($value['f_pair'])) {                    
                if($ftype > 0 || strpos($ftype,',')!==false) {                         
                    $this->db->delete("t_pair_cross","f_code_main = {$f_code} and f_code_cross IN (SELECT f_code from {$value['Ttable']} where f_type_id IN({$value['f_type']}) )");                        
                }
                elseif($ftype < 0) { // delete pair cross for periodic maintenance                                                
                    $this->db->delete("t_pair_cross","f_code_main = {$f_code} and f_code_cross = {$value['f_type']}");
                }

                foreach($value['f_pair'] as $f_pair) {
                    if(is_null($f_pair)) continue;
                    $f_pair_params = [];
                    foreach($f_pair as $key_pair => $line_pair) {
                        if($key == '' || !in_array($key_pair, $pairCols)) continue;
                        $f_pair_params[$key_pair] = $line_pair;
                    }                        
                    $f_pair_params['f_code_main'] = $f_code;                        
                    Mainsim_Model_Utilities::createPairCross($this->db,$f_pair_params,$value['pairCross']);                            
                }                    
            }
        }
        //if not found, create workorders_parent array list        
        if(!$found) { 
            if($params['f_code'] == 0){
                $this->db->insert('t_wares_parent',['f_code'=>$f_code,'f_parent_code'=>0,'f_active'=>1,'f_timestamp'=>$time]);
            }
        }
    }

    public function updateAssetsMaintenancePlan($asset_code, $selectors_code)
    {
        $select = new Zend_Db_Select($this->db);
        $wo = new Mainsim_Model_Workorders($this->db);
        $script = new Mainsim_Model_Script($this->db);
        $current_selectors = $select->from(["t1" => "t_selector_ware"],[])
          ->join(["t2"=>"t_selectors"], "t1.f_selector_id = t2.f_code", ['f_code'])
          ->where("f_type_id = 11")->where("f_ware_id = ?",$asset_code)->query()->fetchAll(Zend_Db::FETCH_COLUMN);

        $select->reset();
        $new_selectors = !$selectors_code? [] : $select->from("t_selectors", "f_code")
          ->where("f_type_id = 11")->where("f_code IN (?)", $selectors_code)
          ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

        //aggiorniamo eventuali selettori rimossi
        $selectors_to_remove = array_diff($current_selectors, $new_selectors);

        $select->reset();
        $select->from(["t1" => "t_workorders"], "f_code")
          ->join(["t2"=>"t_selector_wo"], "t1.f_code = t2.f_wo_id", []);
        foreach($selectors_to_remove as $selector)
        {
            $select->reset(Zend_Db_Select::WHERE);
            $res_generators = $select->where("f_selector_id = ?", $selector)
              ->where("f_type_id IN (3,9,11,15,19)") // FUNZIONA PER TUTTI I GENERATORI
              ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

            foreach($res_generators as $generator_code)
            {
                $this->db->delete("t_periodics", sprintf("f_code = %d and f_asset_code = %d", $generator_code, $asset_code));
                $this->db->delete("t_ware_wo", sprintf("f_wo_id = %d and f_ware_id = %d", $generator_code, $asset_code));
                $wo->addNextDueDate($generator_code);
                $script->fc_wo_asset($generator_code);
            }
        }

        //aggiungiamo eventuali nuovi selettori
        $selectors_to_add = array_diff($new_selectors, $current_selectors);

        if(!$selectors_to_add) return;
        $select->reset();
        $asset_installation_date = $select->from("t_wares","fc_asset_installation_date")
          ->where("f_code = ?",$asset_code)
          ->query()->fetch(Zend_Db::FETCH_COLUMN);

        $select->reset();
        $select->from(["t1" => "t_workorders"], ["f_code","f_type_id"])
          ->join(["t2"=>"t_selector_wo"], "t1.f_code = t2.f_wo_id", []);
        foreach($selectors_to_add as $selector)
        {
            $select->reset(Zend_Db_Select::WHERE);
            $res_generators = $select->where("f_selector_id = ?", $selector)
              ->where("f_type_id IN (3,9,11,15,19)") // FUNZIONA PER TUTTI I GENERATORI
              ->where("not exists (select * from t_ware_wo where f_ware_id = ? and f_wo_id = t1.f_code)", $asset_code)
              ->query()->fetchAll();

            foreach($res_generators as $generator)
            {
                $this->db->insert("t_ware_wo", [
                    "f_wo_id" => $generator['f_code'],
                    "f_ware_id" => $asset_code,
                    "f_timestamp"=> time()
                ]);
                if($generator['f_type_id'] != 9)
                {
                    $asset_list = [$asset_code => $asset_installation_date];
                    $wo->createPeriodics($generator['f_code'], 0 ,$asset_list);
                }
                $script->fc_wo_asset($generator['f_code']);
            }
        }
    }
    
        
    /**
     * Create new Wares
     * @param array $params array with params to insert
     * @param Zend_Auth $userinfo if not logged or cron method, $userinfo must be set with user info in object
     * @param bool $commit : true if at the end of request you have to commit the insert, otherwise
     * @return string
     * @throws Exception
     */
    public function newWares($params,$userinfo = [],$commit = true, $force = false)
    {     
        $error = [];   
        $params = Mainsim_Model_Utilities::clearEmptyValuesParams($params);        
        $time = time();//print_r($params); die();
        if(Zend_Registry::isRegistered('time')) {
            $time = Zend_Registry::get('time');
        }
        else {
            Zend_Registry::set('time', $time);
        }
        try {            
            if($commit) {
                $this->db->beginTransaction();
            }            
            Zend_Registry::set('db_connection_editor',$this->db);
            $columns = Mainsim_Model_Utilities::get_tab_cols("t_wares");
            $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");      
            $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");      
            $users_columns = Mainsim_Model_Utilities::get_tab_cols("t_users");      
            //generate fc_progress code            
            $fc_progress = Mainsim_Model_Utilities::getFcProgress("t_wares",$params['f_type_id'],$this->db);
            
            $module_name = $params['f_module_name'];
            if(empty($userinfo)) {
                $userinfo = Zend_Auth::getInstance()->getIdentity();
                // transform userinfo array to object for mobile  users
                if(is_array($userinfo)){
                    $aux = $userinfo;
					$userinfo = new stdClass();
                    foreach($aux as $key => $value){
                        if($key == 'f_code'){
                            $userinfo->f_id = $value;
                        }
                        else if($key == 'fc_usr_level'){
                            $userinfo->f_level = $value;
                        }
                        else{
                            $userinfo->{$key} = $value;
                        }
                    }
                }
            }            
            // mod 04/02/13 AC
            //user info
            $params['fc_editor_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;            
            $params['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_editor_user_gender'] = $userinfo->f_gender;
            $params['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_editor_user_mail'] = $userinfo->fc_usr_mail;
            // -- creator info
            $params['fc_creation_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;
            $params['fc_creation_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_creation_user_gender'] = $userinfo->f_gender;
            $params['fc_creation_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_creation_user_mail'] = $userinfo->fc_usr_mail;
                        
            //recupero il nome del tipo di wo da inserire nel creation_date
            $sel = new Zend_Db_Select($this->db);
            $result_ware = $sel->from("t_wares_types")->where("f_id = ?",$params['f_type_id'])->query()->fetch();  
            $sel->reset();
            $res_order = $sel->from("t_creation_date","f_order")->where("f_type = 'WARES'")
                    ->where("f_category = '{$result_ware['f_type']}'")
                    ->limit(1)->order("f_order DESC")->query()->fetch();
            $f_order = !empty($res_order)?(int)$res_order['f_order']:0;
            $f_order++;
            // Insert into t_creation_date
            $new_cd = [];
            // Insert into t_wares        
            $custom_fields = [];
            $users = [];
            $new_ware = [];
            $params = Mainsim_Model_Utilities::clearChars($params);
            foreach($params as $key => $value) {
                if(in_array($key, $creation_columns)){ $new_cd[$key] = $value; }
                elseif(in_array($key, $columns)) { $new_ware[$key] = $value; }
                elseif(in_array($key,$custom_columns)) { $custom_fields[$key] = $value; }
                elseif(in_array($key,$users_columns)) { $users[$key] = $value; }
            }
            
            $new_cd["f_type"] = "WARES";
            $new_cd["f_category"] = $result_ware['f_type'];
            $new_cd["f_order"] = $f_order;            
            $new_cd["f_timestamp"] = $time;
            if ($force) {
                $new_cd['f_creation_user'] = 1;
            }else{
                $new_cd["f_creation_user"] = $userinfo->f_id;   
            }
            
            $custom_fields['f_main_table'] = 't_wares';
            $custom_fields['f_main_table_type'] = $params['f_type_id'];
            $new_cd["f_creation_date"] = $time;
            $new_cd['f_wf_id'] = isset($params['f_wf_id'])?$params['f_wf_id'] : $result_ware['f_wf_id'];
            if($result_ware['f_wf_id']) $new_cd['f_wf_id'] = $result_ware['f_wf_id'];
            $new_cd['f_phase_id'] = isset($params['f_phase_id'])?$params['f_phase_id'] : 1;
            if($result_ware['f_wf_phase'] && (!isset($params['f_phase_id']) || empty($params['f_phase_id'])) ) $new_cd['f_phase_id'] = $result_ware['f_wf_phase'];
            $new_cd['f_visibility'] = isset($params['f_visibility'])?$params['f_visibility'] : -1;
            $new_cd['f_editability'] = isset($params['f_editability'])?$params['f_editability'] : -1; 
            $new_cd['fc_progress'] = $fc_progress;
            $config = parse_ini_file (APPLICATION_PATH.'/configs/application.ini',true);
            $adapter = $config['production']['database.adapter'];
            try {
                $this->db->insert("t_creation_date", $new_cd);
            } catch (Exception $e) {
                print "first: ".$e->getMessage();
            }

            $f_code = $this->db->lastInsertId();

            //check if there are any unique to check
            if(isset($params['unique']) && !empty($params['unique'])) {
                foreach($params['unique'] as $line_unique) {                    
                    Mainsim_Model_Utilities::isUnique($line_unique,$params,$params['f_module_name'], $this->db);
                }
            }
            
            if ($force) {
                $new_ware['f_user_id'] = 1;
            }else{
                $new_ware['f_user_id'] = $userinfo->f_id;
            }
            $new_ware['f_code'] = $f_code;                    
            $new_ware['f_timestamp'] = $time;        
            //RECUPERO IL WF_ID
            
            if(!isset($new_ware['f_start_date']) || empty($new_ware['f_start_date'])) {
                $new_ware['f_start_date'] = time();
            }
            //RECUPERO L'ULTIMO F_ORDER            
            $new_ware['f_order'] = isset($params['f_order'])?$params['f_order']:$f_order;
            
            try {
                $new_ware = Mainsim_Model_Utilities::setParamsNull($adapter, $new_ware);
                $this->db->insert("t_wares", $new_ware);                
            } catch (Exception $e) {
                print "second: ".$e->getMessage();
            }

            $custom_fields['f_code'] = $f_code;            
            $custom_fields['f_timestamp'] = $time;
            try {
                $this->db->insert("t_custom_fields", $custom_fields); 
            } catch(Exception $e) {
                print "third: ".$e->getMessage();
            }            
            if(!empty($users)) {                
                $users['f_code'] = $f_code;
                $users['fc_usr_usn_registration'] = time();
                $users['fc_usr_pwd_registration'] = time();
                $this->db->insert("t_users", $users); 
            }
            $scriptModule = new Mainsim_Model_Script($this->db,"t_wares",$f_code,$params,[],false,$userinfo);   
            $this->saveCrossAndPair($f_code, $params, $scriptModule);            
            
            //check if there are attachments and add into the system
            Mainsim_Model_Utilities::saveAttachments($f_code, 't_wares', $params, $scriptModule);
                        
            $res_bm = Mainsim_Model_Utilities::getBookmark($module_name,'view',$userinfo->f_id);
            $bm = json_decode(Mainsim_Model_Utilities::chg($res_bm['f_properties']),true);
            foreach($bm as $line_bm) {
                if(!array_key_exists($line_bm[0], $params)) $params[$line_bm[0]] = '';
            }
            $fields_params = array_keys($params);    
            
            // change phase scripts
            $sel->reset();
            $res_phase = $sel->from("t_wf_exits")->where("f_phase_number = 0")
                    ->where("f_exit = ?",$new_cd['f_phase_id'])->where("f_wf_id = ?",$new_cd['f_wf_id'])->query()->fetchAll();                            
            if(!empty($res_phase) && $res_phase[0]['f_script'] != "") eval($res_phase[0]['f_script']);  
           
            // fields scripts
            foreach($fields_params as $field) {                
                if($field == "t_wares_".$params['f_type_id']) continue;            
                $sel->reset(); 
                $res_scr = $sel->from("t_scripts")->where("f_name LIKE '".$field."-t_wares' AND f_type = 'php'")->query()->fetch();     
                if(isset($res_scr['f_script'])) { eval($res_scr['f_script']); }
                elseif(method_exists($scriptModule,$field) && is_callable(array($scriptModule,$field))) {//if not exist try to check if is a custom script in script model                                                            
                    $err_script = $scriptModule->$field();
                    if(isset($err_script['message'])) throw new Exception($err_script['message']);
                } 
            }                         
                          
            if(!empty($params['attachments'])) {
                Mainsim_Model_Utilities::clearTempAttachments($params,$this->db);
            }
            //print_r($params); die();
            // update auxiliary fields for selectors
            $selectors = [];
            foreach ($params as $key => $value) {
                if (strpos($key, 't_selectors_') !== false) {
                    $selectors = array_merge($selectors, $params[$key]['f_code']);
                }
            }
            //print_r($params[$selectorKey]); die();
            Mainsim_Model_Utilities::updateAuxSelectorFields('wares', $f_code, $this->db, array_values(array_unique($selectors)));
            //create reverse ajax row
            Mainsim_Model_Utilities::saveReverseAjax("t_wares_parent",$f_code,isset($parent_codes)?$parent_codes:NULL,"add", $module_name,$this->db);
            // create history row
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", array("f_timestamp"=>$time), "New Record");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_wares", array("f_timestamp"=>$time), "New Record");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", array("f_timestamp"=>$time), "New Record");
            if(empty($error) && $commit) {
                $this->db->commit();
            }
            elseif(!empty($error)){
                throw new Exception($error['message']);
            }            
        }catch(Exception $e) {    
            $this->db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>"Error in ".__METHOD__." : ".$e->getMessage(),'f_type_log'=>0));            
            if($commit) { $this->db->rollBack(); }     
            
            // registration doesn't have user infos
            if($userinfo->f_language != null){
                $trsl = new Mainsim_Model_Translate(null,$this->db);
                $trsl->setLang($trsl->getUserLang($userinfo->f_language));
                $error['message'] = $trsl->_("Error").": ".$trsl->_($e->getMessage());  
            }
            else{
                $error['message'] = $e->getMessage();  
            } 
            return $error;
        }
        return array('f_code'=>$f_code);
    }        
   
    /**
     * CREO LE CROSS 
     */
    private function createCross($f_code_master,$f_code_slave,$f_type_master,$f_type_slave) 
    {        
        $f_ware_code_array = $f_code_slave;
        foreach($f_ware_code_array as $ware) {
            if(!$ware) continue;
            $this->db->insert('t_wares_relations', array(
                'f_code_ware_master'=>$f_code_master,
                'f_code_ware_slave'=>$ware,
                'f_type_id_master'=>$f_type_master,
                'f_type_id_slave'=>$f_type_slave,                
                'f_timestamp'=>time()
            ));
        }
    }
    
    //aggiornamento dei contatori 'fc_asset_wo' e 'fc_asset_opened_wo' dei wares, a cui sono asociati una icona    
    public function waresWorkorderIconAssociation($newWorkorders = [],$f_type = [], $ware){        
//        // get old workorder asset association
//        $select = new Zend_Db_Select($this->db);
//        $select->from('t_ware_wo', array('f_ware_id', 'f_wo_id'))
//            ->join(array("cd" => "t_creation_date"), "t_ware_wo.f_wo_id = cd.f_id", [])
//            ->join(array("wp" => "t_wf_phases"), "cd.f_phase_id = wp.f_number AND cd.f_wf_id = wp.f_wf_id", [])
//            ->join(array("wo" => "t_workorders"), "wo.f_code = cd.f_id", []) 
//            ->where('wp.f_group_id NOT IN (6,7,8)');
//        if(!empty($f_type)) {
//            $select->where("wo.f_type_id IN (?)",$f_type);
//        }
//        $oldWorkorders = $select->where('f_ware_id =' . $ware)->query()->fetchAll();
//        $select->reset();                
//        if(count($newWorkorders) > 0){
//            $newWorkorders = $select->from(array("cd" => "t_creation_date"), array('f_id'))
//                ->join(array("wp" => "t_wf_phases"), "cd.f_phase_id = wp.f_number AND cd.f_wf_id = wp.f_wf_id", [])                
//                ->where('wp.f_group_id NOT IN (6,7,8)')
//                ->where('cd.f_id IN (' . implode(",", $newWorkorders). ')')
//                ->query()->fetchAll();
//        }
//        $updateWoCounter = count($newWorkorders) - count($oldWorkorders);  
//        if($updateWoCounter != 0){
//            
//            //aggiorno fc_asset_wo del ware
//            $this->db->update('t_wares', array('fc_asset_wo' => count($newWorkorders)), 'f_code = '.$ware);
//
//            //aggiorno fc_asset_opende_wo del ware e della sua parentela
//            $parents = [];
//            $parents = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $ware,$parents,$this->db);
//            unset($parents[count($parents) - 1]);
//            $parents[] = $ware;
//            $function = 'isnull';
//            if(Mainsim_Model_Utilities::isMysql()) {
//                $function = 'ifnull';
//            }
//            $this->db->update('t_wares', array('fc_asset_opened_wo' => 0), 'f_code IN (' . implode(',', $parents) . ") and $function(fc_asset_opened_wo,0) = 0");
//            if(count($parents) > 0){
//                $this->db->update('t_wares', array('fc_asset_opened_wo' => new Zend_Db_Expr('fc_asset_opened_wo + '. $updateWoCounter)), 'f_code IN (' . implode(',', $parents) . ')');
//            }
//        }        
    }
    
    public function changePhaseBatch($f_codes, $f_wf_id, $f_exit, $f_module_name) {
        $result = $this->obj->changePhase($f_codes, $f_wf_id, $f_exit, $f_module_name);
        //se ho eliminato un documento aggiorno il campo fc_documents degli asset a cui era associato(direttamente e non)
        if($f_module_name =='mdl_doc_tg' && $f_wf_id = 9 && $f_exit == 3 && $result['f_changed']>0)
        {
            $select = new Zend_Db_Select($this->db); 
            $docs_id = explode(',', $f_codes);
            foreach($docs_id as $doc_id) 
            {
                $f_code_ware_master = $doc_id;
                //ricavo gli asset associati al documento
                $select->reset();
                $res_assets = $select->from(array("t1" => "t_wares_relations"), array("asset_code" =>"t1.f_code_ware_slave"))                
                ->where("t1.f_code_ware_master = {$f_code_ware_master}")->where("t1.f_type_id_slave = 1")->query()->fetchAll();
                $select->reset();
                //decremento di 1 il campo fc_documents degli asset e dei suoi parents
                $scriptModule = new Mainsim_Model_Script($this->db,'t_wares');
                foreach ($res_assets as $asset) 
                {
                   $id=$asset['asset_code'];
                   $parents = $scriptModule->getParents($select,$id); 
                   foreach ($parents as $f_code) 
                   {
                       $select->reset();
                       $res = $select->from('t_creation_date',array('fc_documents'))
                           ->where('f_id=?', $f_code)->query()->fetch();
                       $num = $res['fc_documents']-1;
                       $select -> reset();
                       $this->db->update("t_creation_date", array("fc_documents" => $num), "f_id = {$f_code}");
                   }
                   $select->reset();
                }            
            } 
        }
        return $result;
    }     
    
    /**
     * Update cross selector_wo for wo associate with asset
     * @param int $f_code f_code of asset
     */
    protected function changeWoSelectorFromAsset($f_code, $newSelectors)
    {
        $select = new Zend_Db_Select($this->db);
        //recupero i selettori dell'asset
        $selectors = $select->from("t_selector_ware",'f_selector_id')
          ->join('t_selectors', "f_selector_id = f_code", [])
          ->where('f_ware_id = ?',$f_code)->where("f_type_id != 11")//escludo il selettore 11 perché è un caso a parte
          ->query()->fetchAll();
        if($selectors)
        $selectors=array_values($selectors);
        $totSelectors = count($selectors);
        $implodeSel = implode(',',$selectors);

        //recupero i workorder su cui applicare i nuovi selettori ( i generatori sono esclusi)
        $select->reset();
        $resWoAsset = $select->from(["t1"=>"t_ware_wo"],["f_wo_id"])
          ->join(['t3' => 't_workorders'], "t1.f_wo_id = t3.f_code", [])
          ->where("t3.f_type_id NOT IN (3,9,11,15,19)")
          ->where("f_ware_id = ?",$f_code)
          ->query()->fetchAll();
		// se l'asset aveva dei selettori associati prima del salvataggio li rimuovo anche dai workorders ad esso associati prima di inserire le nuove relazioni wo-selectors
		if($totSelectors > 0){
			foreach($resWoAsset as $line){
				$select->reset();
				//primo check: il wo ha tutti i selettori dell'asset
				$res_num_sel_wo = $select->from("t_selector_wo",["count(*)"])
				  ->where("f_selector_id IN (?)", $selectors)->where("f_wo_id = ?",$line['f_wo_id'])
				  ->group("f_wo_id")->query()->fetch(); 
                                if($res_num_sel_wo)
                                $res_num_sel_wo=array_values($res_num_sel_wo);
				if($res_num_sel_wo == $totSelectors){ // cancello le vecchie
					$this->db->delete("t_selector_wo","f_wo_id = {$line['f_wo_id']} and f_selector_id IN ($implodeSel)");
				}
				else { // verifico che non abbia alcuna cross
					$select->reset(Zend_Db_Select::WHERE);
					$res_num_sel_wo = $select->where("f_wo_id = ?",$line['f_wo_id'])
					  ->query()->fetch();
                                        if($res_num_sel_wo)
                                        $res_num_sel_wo=array_values($res_num_sel_wo);
					if($res_num_sel_wo > 0){ continue; } // questo wo ha dei selettori diversi da quelli dell'asset quindi li lascio li
				}
				//... e scrivo le nuove
				Mainsim_Model_Utilities::createSelectorCross($this->db, $line['f_wo_id'], $newSelectors, 't_selector_wo', 'f_wo_id');
			}
		}
    }

    /**
     * Reschedule pm-per-asset
     * @param $f_code
     * @param $pm_codes
     */
    protected function reschedulePmPerAsset($f_code, $pm_codes)
    {
        $select = new Zend_Db_Select($this->db);
        $res_asset = $select->from("t_wares","fc_asset_installation_date")
          ->where("f_code = ?", $f_code)->where("f_type_id = 1")->query()->fetch();
        if(!$res_asset){ //controllo che non sia arrivato un wares sbagliato
            throw new InvalidArgumentException("reschedulePmPerAsset: f_code must be an asset code");
        }
        $asset_list = [$f_code => $res_asset['fc_asset_installation_date']];

        $wo = new Mainsim_Model_Workorders($this->db);
        $select->reset();
        $res_codes_to_reschedule = $select->from("t_workorders", "f_code")
          ->where("f_code IN (?)", $pm_codes)->where("fc_pm_type = 'pm-per-asset'")
          ->query()->fetchAll(Zend_Db::FETCH_COLUMN);
        foreach($res_codes_to_reschedule as $pm_code){
            $wo->createPeriodics($pm_code, null, $asset_list);
        }
    }
}