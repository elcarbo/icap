<?php
class Mainsim_Model_Pricelist
{
    private $db;
    
    public function __construct($db = null, $rootPath = '') {
       $this->db = !is_null($db) ? $db : Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function getPricelistByWo($woCode, $type){
        
        // get bookmark for module 'mdl_prl_tg'
        $bookmark = $this->getBookMark('mdl_wo_prl' . $type . '_tg');
        
        // get table fields
        $tableFields = $this->getTableFields();
        $data[] = $tableFields;
        
        // get data
        $select = new Zend_Db_Select($this->db);
        $select->from('t_pricelist')
               ->where('fc_code_wo =' . $woCode)
               ->where('fc_type_prl =' . $type);
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $data[] = array_values($res[$i]);
        }
        
        $response = array(
            'bookmark' => $bookmark,
            'data' => $data
        );
        return json_encode($response);
    }
    
    private function savePricelistByType($woCode, $data, $type){
        
        // map negative ids with database ids
        $mappedNegativeIds = $this->mapNegativeIds($data);
        
        // delete oldData;
        $this->deletePricelistByWoType($woCode, $type);
        // insert data
        if(count($data) > 0){
            $this->insertPricelist($mappedNegativeIds, $data, $type, $woCode);    
        }
    }
    
    private function deletePricelistByWoType($woCode, $type){
        
        $this->db->delete("t_pricelist", "fc_code_wo = " . $woCode . " AND " . "fc_type_prl = " . $type);
    }
    
    private function insertPricelist($mappedNegativeIds, $data, $type, $woCode){
        
        $tableFields = $this->getTableFields();
        $timestamp = time();
        
        foreach($data as $key => $value){
            $insertArray = array_fill_keys($tableFields, '');
            $insertArray['fc_wo_prl_timestamp'] = $timestamp;
            $insertArray['fc_code_wo'] = $woCode;
            
            // replace negative ids
            if($key < 0){
                $value['f_id'] = $mappedNegativeIds[$value['f_id']];
            }

            // replace negative parent ids
            if($value['fc_code_parent'] < 0){
                $value['fc_code_parent'] = $mappedNegativeIds[$value['fc_code_parent']];
            }

            // replace negative values in rtree field
            $aux = explode("|", $value['rtree']);
            for($i = 0; $i < count($aux); $i++){
                if($aux[$i] < 0){
                    $aux[$i] = $mappedNegativeIds[$aux[$i]];
                }
            }
            $value['rtree'] = implode("|", $aux);
            
            foreach($value as $key2 => $value2){
                if(isset($insertArray[$key2]) && $key2 != 'fc_code_wo'){
                    $insertArray[$key2] = $value2;
                }
            }
            
            $inserts[] = "('" . implode("','", $insertArray) . "')";
        }
        $tableFields = $this->getTableFields();
        $insertStatement = "INSERT INTO t_pricelist(" . implode(",", $tableFields) . ") VALUES " . implode(",", $inserts);
        try{
            $this->db->query($insertStatement);
        }
        catch(Exception $e){
            print($e->getMessage());
            die();
        }
    }
    
    private function mapNegativeIds($data){
        
        $mappedNegativeIds = array();
        $lastInsertId = $this->getLastInsertId();
        foreach($data as $key => $value){
            if($value['f_id'] < 0){
                $mappedNegativeIds[$key] = ++$lastInsertId; 
            }
        }
        return $mappedNegativeIds;
    }
    
    private function getLastInsertId(){
        
        $select = new Zend_Db_Select($this->db);
        $select->from('t_pricelist', 'max(f_id) AS max');
        $res = $select->query()->fetchAll();
        return $res[0]['max'];
    }
    
    public function savePricelist($data){
        
        // save quote
   
        $this->savePricelistByType($data['f_code_main'], $data['f_pair1'], 1);
        // save final
        $this->savePricelistByType($data['f_code_main'], $data['f_pair2'], 2);
    }
    
    private function getBookMark($module){
        
        // get bookmarks
        $objSystem = new Mainsim_Model_System();
        $res = $objSystem->getBookmark($module);
        return json_decode(Mainsim_Model_Utilities::chg($res['view']['f_properties']), true);
    }
    
    private function getTableFields(){
        
        $query = "DESCRIBE t_pricelist";
        
        $res = $this->db->query($query)->fetchAll();;
        $tableFields = array();
        for($i = 0; $i < count($res); $i++){
            $tableFields[] = $res[$i]['Field'];
        }
        return $tableFields;
    }
      
}

