<?php

class Mainsim_Model_System extends Mainsim_Model_MainsimObject
{
    private $db, $table, $obj,$trsl;
      
    public function __construct($db = null) {
        $this->db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $this->obj = new Mainsim_Model_MainsimObject();
        $this->table = 't_systems';
        $this->obj->type = $this->table;
    }
        
    /**
     * Edit existing system element
     * @param type $params
     * @param type $edit_batch
     * @return string|array
     * @throws Exception 
     */
    public function editSystem($params,$edit_batch = false,$commit = true)
    {           
        $error = [];
        $time = time();
        if(Zend_Registry::isRegistered('time')) {
            $time = Zend_Registry::get('time');
        }
        else {
            Zend_Registry::set('time', $time);
        }      
        try {
            $userinfo = Zend_Auth::getInstance()->getIdentity();
            if($commit){
                $this->db->beginTransaction();                
            } 
            Zend_Registry::set('db_connection_editor',$this->db);
            $select = new Zend_Db_Select($this->db);
            
            // end mod 04/02/13 AC            
            $columns = Mainsim_Model_Utilities::get_tab_cols("t_systems");
            $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");           
            $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");  
            $pair_columns = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
            $bm_columns = Mainsim_Model_Utilities::get_tab_cols("t_bookmarks");
                        
            $userinfo = Zend_Auth::getInstance()->getIdentity();
            // mod 04/02/13 AC
            $params['fc_editor_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;            
            $params['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_editor_user_gender'] = $userinfo->f_gender;
            $params['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_editor_user_mail'] = $userinfo->fc_usr_mail;
            
            // end mod 04/02/13 AC
            $select = new Zend_Db_Select($this->db);                                   
            $module_name = $params['f_module_name'];            
            //get old wares,creation and custom fields of this f_code
            $old_sys = Mainsim_Model_Utilities::getOld($this->db, $params['f_code'],"t_systems");
            $old_creation = Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],"t_creation_date","f_id");             
            $old_sys_custom = Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],'t_custom_fields');         
            
            //olds array for script
            $olds = array(
                'old_table'=>$old_sys,
                'old_custom'=>$old_sys_custom,
                'old_creation'=>$old_creation                
            );
             
            if(!$params['f_phase_id']) {
                unset($params['f_phase_id']);
            }
            //unset($params['f_id']);
            $f_code = $params['f_code'];
                        
            $new_sys = $old_sys;       
            $new_creation = $old_creation;
            $custom_fields = $old_sys_custom;
            $params = Mainsim_Model_Utilities::clearChars($params);
            $params = Mainsim_Model_Utilities::clearEmptyValuesParams($params);
            foreach ($params as $key => $param) {
                if(in_array($key, $columns)) {
                    $new_sys[$key] = $param;
                }
                elseif(in_array($key, $creation_columns)) {
                    $new_creation[$key] = $param;
                }            
                elseif(in_array($key, $custom_columns)) {
                    $custom_fields[$key] = $param;
                }                
            }
            
            //check if there are any unique to check           
            if(isset($params['unique']) && !empty($params['unique'])) {                
                foreach($params['unique'] as $line_unique) {
                    Mainsim_Model_Utilities::isUnique($line_unique,$params,$params['f_module_name'], $this->db);                    
                }
            }
            
            $new_sys['f_timestamp'] = $time; 
            $new_creation['f_timestamp'] = $time;
            $new_sys['f_user_id'] = $userinfo->f_id;
            
            //Disable old sys element and insert new
            Mainsim_Model_Utilities::newRecord($this->db,$f_code,"t_systems",$new_sys); 
            
            //Disable old creation and insert new
            Mainsim_Model_Utilities::newRecord($this->db,$f_code, "t_creation_date", $new_creation,"Edit");
                        
            $custom_fields['f_code'] = $f_code;            
            $custom_fields['f_timestamp'] = $time;
            Mainsim_Model_Utilities::newRecord($this->db,$f_code,"t_custom_fields",$custom_fields);
           
            foreach($params as $key => $value) {
                if(strpos($key,"t_selectors_") !== false  && (!$edit_batch || !empty($value['f_code'])) ) {                    
                    $this->db->delete("t_selector_system","f_system_id = $f_code");
                    //create (if passed) new cross
                    if(!empty($value['f_code'])){
                        Mainsim_Model_Utilities::createSelectorCross($this->db, $f_code, $value['f_code'], "t_selector_system", "f_system_id");                                                
                    }                    
                }               
                if(strpos($key,"t_wares_") !== false  && (!$edit_batch || !empty($value['f_code']))) {                                        
                    //create (if passed) new cross
                    if($value['f_type'] != -1) {
                        if(!empty($value['f_code'])){
                            $tot_a = count($value['f_code']);
                            for($a = 0;$a < $tot_a;++$a) {
                                $this->db->insert('t_wares_systems',array(
                                    'f_system_id'=>$f_code,
                                    'f_ware_id'=>$value['f_code'][$a],
                                    'f_timestamp'=>$time
                                ));
                            }
                        }     
                    }                    
                }
				
                if(strpos($key,"t_systems_")!== false  && (!$edit_batch || !empty($value['f_code']))) {
                    if(isset($value['f_pair'])) {
                        //delete olds pair cross
                        if($value['f_type'] > 0) {
                            if(!empty($value['f_code']) || !$edit_batch) { // check if is a batch edit
                                $this->db->delete("t_pair_cross","f_code_main = {$params['f_code']} and f_code_cross IN (SELECT f_code from t_wares where f_type_id = {$value['f_type']})");
                            }
                        }
                        else { // delete pair cross for periodic maintenance
                            if(!empty($value['f_code']) || !$edit_batch) { // check if is a batch edit
                                if($params['f_type_id'] == 3) {
                                    $this->db->delete("t_bookmarks","f_code = {$params['f_code']}");
                                }
                                else {
                                    $this->db->delete("t_pair_cross","f_code_main = {$params['f_code']} and f_code_cross = {$value['f_type']}");
                                }
                            }
                        }
                        
                        /* Check fields to insert in t_pair_cross or t_bookmarks */
                        foreach($value['f_pair'] as $f_pair) {
                            if(is_null($f_pair)) continue;
                            $f_pair_params = [];
                            foreach($f_pair as $key_pair => $line_pair) {
                                if($params['f_type_id'] != 3) {
                                    if(!in_array($key_pair, $pair_columns)) continue;
                                    $f_pair_params[$key_pair] = $line_pair;
                                }
                                else {
                                    if(!in_array($key_pair, $bm_columns)) continue;
                                    $f_pair_params[$key_pair] = $line_pair;
                                }
                            }
                            $f_pair_params = Mainsim_Model_Utilities::clearChars($f_pair_params);
                            $f_pair_params = Mainsim_Model_Utilities::clearEmptyValuesParams($f_pair_params);
                            
                            if($params['f_type_id'] != 3) {
                                $f_pair_params['f_code_main'] = $f_code;
                                Mainsim_Model_Utilities::newPairCross($this->db,$f_pair_params,$value['pairCross']);                            
                            }
                            else {
                                $f_pair_params['f_code'] = $f_code;                                
                                $this->db->insert("t_bookmarks",$f_pair_params);
                            }
                            
                        }                    
                    } 
                }
				
                if(strpos($key,"t_systems_parent") !== false  && (!$edit_batch || !empty($value['f_code'])) ) {
                    $parentCodes = empty($value['f_code'])?array(0):$value['f_code'];                    
                    $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($parentCodes,'t_systems_parent',$f_code,'f_code',[],'f_parent_code');
                    if(!$checkIdenticalCross) {
                        $this->db->delete("t_systems_parent", "f_code = $f_code");
                        Mainsim_Model_Utilities::createParents('t_systems_parent',$f_code,$value['f_code'],$this->db);
                    }
                }
            }
			
            if($params['f_type_id'] == 1 && !empty($params['t_selectors'])) {                
                $sels = explode(',',$params['t_selectors']);                
                foreach($sels as $sel) {
                    $this->db->insert("t_selector_system",array("f_system_id"=>$f_code,"f_selector_id"=>$sel,'f_nesting_level'=>1,'f_timestamp'=>time()));
                }
            }
            
            $res_bm = $this->getBookmark($module_name);
            $bm = json_decode(Mainsim_Model_Utilities::chg($res_bm['view']['f_properties']),true);
            foreach($bm as $line_bm) {
                if(!array_key_exists($line_bm[0], $params)) $params[$line_bm[0]] = '';
            }
            $fields_params = array_keys($params);    
            //lancio i vari script
            $scriptModule = new Mainsim_Model_Script($this->db,"t_systems",$f_code,$params,$olds);            
             
            foreach($fields_params as $field) {                     
                if($field == "t_systems_".$params['f_type_id']) continue;                                
				
                $select->reset();                      
                $res_scr = $select->from("t_scripts")->where("f_name = '$field-t_systems'")->where("f_type = 'php'")->query()->fetch();                
                if(!empty($res_scr['f_script'])) { eval($res_scr['f_script']); }
                    
                elseif(method_exists($scriptModule,$field) && is_callable(array($scriptModule,$field))) {//if not exist try to check if is a custom script in script model                    
					$err_script = $scriptModule->$field();
                    if(isset($err_script['message'])) throw new Exception($err_script['message']);
                } 
            }              
                       
            if($new_creation['f_phase_id'] != $old_creation['f_phase_id']) {                    
                $select->reset();            
                $res_phase = $select->from("t_wf_exits")->where("f_phase_number = ?",$old_creation['f_phase_id'])
                        ->where("f_exit = ?",$new_creation['f_phase_id'])->where("f_wf_id = ?",$new_creation['f_wf_id'])->query()->fetch();                                
                if(!empty($res_phase) && $res_phase['f_script'] != "")  {                                            
                    eval($res_phase['f_script']);                                        
                }
            }
            Mainsim_Model_Utilities::saveAttachments($f_code, 't_systems', $params, $scriptModule);              
            //update reverse ajax
            $parent_list = array(0);
            foreach($params as $key =>$value) {
                if(strpos($key,"t_systems_parent") !== false && !empty($value['f_code'])) {
                    $parent_list = $value['f_code'];
                    break;                
                }            
            }
            if($commit){$this->db->commit(); }            
            Mainsim_Model_Utilities::saveReverseAjax("t_systems_parent", $f_code, [], "upd", $module_name,$this->db);
        }catch(Exception $e) {  
            if($commit){ $this->db->rollBack(); } 
            $trsl = new Mainsim_Model_Translate(null,$this->db);            
            $trsl->setLang($trsl->getUserLang($userinfo->f_language));
            $error['message'] = $trsl->_("Error").": ".$trsl->_($e->getMessage());
            $this->db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>"Error in ".__METHOD__." : ".$e->getMessage(),'f_type_log'=>0));
            return $error;            
        }                        
        $resp = array('f_code'=>$f_code); 
        if($params['f_type_id'] == 6 && $new_creation['f_phase_id'] == 1 && $params['fc_dmn_import_type'] == 'instant') {
            $expSheets = explode(',',$params['fc_dmn_objects']);
            $resp['sheet'] = $expSheets[0];
            $resp['startImport'] = 1;
        }
        return $resp;
    }   
        
    
    /* ADDED BY ALESSANDRA 13/03/2012 */
    public function newSystem($params,$userinfo = [])
    {
        $error = [];   
        $time = time();
        if(Zend_Registry::isRegistered('time')) {
            $time = Zend_Registry::get('time');
        }
        else {
            Zend_Registry::set('time', $time);
        }    
        try {            
            $this->db->beginTransaction();
            Zend_Registry::set('db_connection_editor',$this->db);
            $columns = Mainsim_Model_Utilities::get_tab_cols("t_systems");            
            $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");      
            $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
            $pair_columns = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
            $bm_columns = Mainsim_Model_Utilities::get_tab_cols("t_bookmarks");            
            $fc_progress = Mainsim_Model_Utilities::getFcProgress("t_systems",$params['f_type_id'],$this->db);
            
            $module_name = $params['f_module_name'];
            if(empty($userinfo)) {
                $userinfo = Zend_Auth::getInstance()->getIdentity();   
            }
            $sel = new Zend_Db_Select($this->db);            
            // mod 04/02/13 AC
            //user info
            $params['fc_editor_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;            
            $params['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_editor_user_gender'] = $userinfo->f_gender;
            $params['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_editor_user_mail'] = $userinfo->fc_usr_mail;
            // -- creator info
            $params['fc_creation_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;
            $params['fc_creation_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_creation_user_gender'] = $userinfo->f_gender;
            $params['fc_creation_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_creation_user_mail'] = $userinfo->fc_usr_mail;
            
            
            //get type of sys to insert in t_creation_date
            
            $result_sys = $sel->from("t_systems_types")->where("f_id = ?",$params['f_type_id'])->query()->fetch();  
            $sel->reset();
            $res_order = $sel->from("t_creation_date","f_order")->where("f_type = 'SYSTEMS'")
                    ->where("f_category = '{$result_sys['f_type']}'")
                    ->limit(1)->order("f_order DESC")->query()->fetch();
            $f_order = !empty($res_order)?(int)$res_order['f_order']:1;
            
            // Insert into t_creation_date
            $new_cd = [];
            $new_cd["f_type"] = "SYSTEMS";
            $new_cd["f_category"] = $result_sys['f_type'];
            $new_cd["f_order"] = $f_order;
            $new_cd["f_creation_date"] = $time;
            $new_cd["f_timestamp"] = $time;
            $new_cd["f_creation_user"] = $userinfo->f_id;                    
            // Insert into t_systems        
            $custom_fields = [];            
            $new_system = [];
            $params = Mainsim_Model_Utilities::clearChars($params);
            $params = Mainsim_Model_Utilities::clearEmptyValuesParams($params);        
            foreach($params as $key => $value) {                
                if(in_array($key, $creation_columns)){                    
                    $new_cd[$key] = $value;
                }
                elseif(in_array($key, $columns)) {                    
                    $new_system[$key] = $value;
                }
                elseif(in_array($key,$custom_columns)) {                    
                    $custom_fields[$key] = $value;
                }                
            }
            
            //check if there are any unique to check            
            if(isset($params['unique']) && !empty($params['unique'])) {                
                foreach($params['unique'] as $line_unique) {
                    Mainsim_Model_Utilities::isUnique($line_unique,$params,$params['f_module_name'], $this->db);                    
                }                
            }            
                        
            
            $new_cd['f_wf_id'] = isset($params['f_wf_id'])?$params['f_wf_id'] : $result_sys['f_wf_id'];
            if($result_sys['f_wf_id']) $new_cd['f_wf_id'] = $result_sys['f_wf_id'];
            $new_cd['f_phase_id'] = isset($params['f_phase_id'])?$params['f_phase_id'] : 1;
            if($result_sys['f_wf_phase'] && (!isset($params['f_phase_id']) || empty($params['f_phase_id'])) ) 
                $new_cd['f_phase_id'] = $result_sys['f_wf_phase'];
            $new_cd['f_visibility'] = $userinfo->f_level;
            $new_cd['f_editability'] = $userinfo->f_level; 
            $new_cd['fc_progress'] = $fc_progress; 
            $this->db->insert("t_creation_date", $new_cd);
            $f_code = $this->db->lastInsertId();
                      
            $new_system['f_user_id'] = $userinfo->f_id;        
            $new_system['f_code'] = $f_code;                    
            $new_system['f_timestamp'] = $time;        
            $sel->reset();        
            
            //RECUPERO L'ULTIMO F_ORDER
            $sel->reset();                        
            $f_order++;
            $this->db->insert("t_systems", $new_system);                            
            
            $custom_fields['f_code'] = $f_code;            
            $custom_fields['f_timestamp'] = $time;
            $this->db->insert("t_custom_fields", $custom_fields); 
             
            foreach($params as $key => $value) {                
                if(strpos($key,"t_selectors_") !== false) {                                        
                    //create (if passed) new cross
                    if(!empty($value['f_code']) && $value != '' && isset($value['f_code'])){
                        Mainsim_Model_Utilities::createSelectorCross($this->db, $f_code, $value['f_code'], "t_selector_system", "f_system_id");                        
                    }                
                }      
                if(strpos($key,"t_wares_") !== false ) {                                        
                    //create (if passed) new cross
                    if($value['f_type'] != -1) {
                        if(!empty($value['f_code'])){
                            $tot_a = count($value['f_code']);
                            for($a = 0;$a < $tot_a;++$a) {
                                $this->db->insert('t_wares_systems',array(
                                    'f_system_id'=>$f_code,
                                    'f_ware_id'=>$value['f_code'][$a],
                                    'f_timestamp'=>$time
                                ));
                            }
                        }     
                    }                    
                }
                
                /* Check fields to insert in t_pair_cross or t_bookmarks */
                if(strpos($key,"t_systems_")!== false) {                    
                    foreach($value['f_pair'] as $f_pair) {
                        if(is_null($f_pair)) continue;
                        $f_pair_params = [];
                        foreach($f_pair as $key_pair => $line_pair) {
                            if($params['f_type_id'] != 3) {
                                if(!in_array($key_pair, $pair_columns)) continue;
                                $f_pair_params[$key_pair] = $line_pair;
                            }
                            else {
                                if(!in_array($key_pair, $bm_columns)) continue;
                                $f_pair_params[$key_pair] = $line_pair;
                            }
                        }
                        $f_pair_params = Mainsim_Model_Utilities::clearChars($f_pair_params);
                        $f_pair_params = Mainsim_Model_Utilities::clearEmptyValuesParams($f_pair_params);
                        
                        if($params['f_type_id'] != 3) {
                            $f_pair_params['f_code_main'] = $f_code;
                            Mainsim_Model_Utilities::newPairCross($this->db,$f_pair_params,$value['pairCross']);                            
                        }
                        else {
                            $f_pair_params['f_code'] = $f_code;                                
                            $this->db->insert("t_bookmarks",$f_pair_params);
                        }
                    }                                        
                }
            }
            
            if($params['f_type_id'] == 1 && !empty($params['t_selectors'])) {
                $sels = explode(',',$params['t_selectors']);
                foreach($sels as $sel_line) {
                    $this->db->insert("t_selector_system",array("f_system_id"=>$f_code,"f_selector_id"=>$sel_line,'f_nesting_level'=>1,'f_timestamp'=>time()));
                }
            }
                        
            $res_bm = $this->getBookmark($module_name);            
            $bm = json_decode(Mainsim_Model_Utilities::chg($res_bm['view']['f_properties']),true);
            foreach($bm as $line_bm) {
                if(!array_key_exists($line_bm[0], $params)) $params[$line_bm[0]] = '';
            }
            $fields_params = array_keys($params);  
            //lancio i vari script
            $scriptModule = new Mainsim_Model_Script($this->db,"t_systems",$f_code,$params);
            foreach($fields_params as $field) {                     
                if($field == "t_systems_".$params['f_type_id']) continue;                
                
                $sel->reset();                      
                $res_scr = $sel->from("t_scripts")->where("f_name = '$field-t_systems'")->where("f_type = 'php'")->query()->fetch();                            
                if(!empty($res_scr['f_script'])) { eval($res_scr['f_script']); }
                
                elseif(method_exists($scriptModule,$field) && is_callable(array($scriptModule,$field))) {//if not exist try to check if is a custom script in script model                    
                    $err_script = $scriptModule->$field();                    
                    if(isset($err_script['message'])) throw new Exception($err_script['message']);                    
                } 
            }
            
            Mainsim_Model_Utilities::saveAttachments($f_code, 't_systems', $params, $scriptModule);  
            if(empty($error)) $this->db->commit();
            else return $error;
            
        }catch(Exception $e) {                
            $this->db->rollBack();
            $this->db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>"Error in ".__METHOD__." : ".$e->getMessage(),'f_type_log'=>0));
            $trsl = new Mainsim_Model_Translate(null,$this->db);            
            $trsl->setLang($trsl->getUserLang($userinfo->f_language));
            $error['message'] = $trsl->_("Error").": ".$trsl->_($e->getMessage());
            return $error;
        }
        
        //update reverse ajax
        $parent_list = array(0);
        foreach($params as $key =>$value) {
            if(strpos($key,"t_systems_parent") !== false && !empty($value['f_code'])) {
                $parent_list = $value['f_code'];
                break;                
            }            
        }        
        Mainsim_Model_Utilities::createParents('t_systems_parent',$f_code,$parent_list,$this->db);
        Mainsim_Model_Utilities::saveReverseAjax("t_systems_parent",$f_code,[],"add", $module_name,$this->db);
        $resp = array('f_code'=>$f_code); 
        if($params['f_type_id'] == 6 && $new_cd['f_phase_id'] == 1 && $params['fc_dmn_import_type'] == 'instant') {
            $expSheets = explode(',',$params['fc_dmn_objects']);
            $resp['sheet'] = $expSheets[0];
            $resp['startImport'] = 1;
        }
        return $resp;
    }
        
    public function cloneSystem($clone_type = 0, $f_codes, $module_name, $fields = "") {
        if(is_string($f_codes)) $codes = explode(",", $f_codes);
        else $codes = $f_codes; $count = count($codes); $res = "";
        for($i=0; $i<$count; $i++) {
            // Cloning object
            $f_ncode = $this->obj->cloneObject($codes[$i], $module_name, $fields); if(is_array($f_ncode)) return $f_ncode; // Error
            $res = $this->obj->cloneCross("t_wares_systems", "f_system_id = {$codes[$i]}", array("f_system_id" => $f_ncode)); if(is_array($res)) return $res; // Error
            $res = $this->obj->cloneCross("t_selector_system", "f_system_id = {$codes[$i]}", array("f_system_id" => $f_ncode)); if(is_array($res)) return $res; // Error
            $res = $this->obj->cloneBookmarks($codes[$i], $f_ncode); if(is_array($res)) return $res; // Error
            
            // Cloning children recursively
            if($clone_type > 0) {
                $children = $this->obj->getChildren($codes[$i]); $c_count = count($children);                
                $fields["t_item_parent"]["f_parent_code"] = $f_ncode;
                for($j=0; $j<$c_count; $j++) {
                    $res = $this->cloneSystem($clone_type, $children[$j], $module_name, $fields);
                    if(is_array($res)) return $res; // Error
                }
            }
        }
        return $res;
    }
    
        
    public function getFavourite($mod, $user) {
        $q = new Zend_Db_Select($this->db);
        $bkm = $q->from(array("c" => "t_creation_date"), array("f_id", "f_title"))->join(array("s" => "t_systems"), "c.f_id = s.f_code")
            ->where("s.fc_bkm_module = '$mod'")->where("c.f_title = 'bookmark'")->where("s.fc_bkm_user_id = ?", $user)->where("s.fc_bkm_bookmark != ''")
            ->query()->fetch();
        return array("bookmark" => array(
            "f_id" => $bkm["f_code"],
            "f_name" => $bkm["f_title"],
            "f_properties" => json_encode($bkm["fc_bkm_bookmark"]?$bkm["fc_bkm_bookmark"]:[])
        ));
    }
    
    public function getBookmark($mod, $name = 'Default', $user = 0) {
        $name = Mainsim_Model_Utilities::clearChars($name);
        $q = new Zend_Db_Select($this->db);        
        $bkm = $q->from(array("c" => "t_creation_date"), array("f_id", "f_title"))->join(array("s" => "t_systems"), "c.f_id = s.f_code")
            ->where("s.fc_bkm_module = ?",$mod)->where("c.f_title = ?",$name)->where("s.fc_bkm_user_id IN (?)", $user)->where("c.f_phase_id = 1")
            ->query()->fetch();
        
        if(empty($bkm)) return [];
 
        array_walk_recursive($bkm, function(&$t,&$k){ $tt=$t; if(gettype($tt)=="string") $t=Mainsim_Model_Utilities::chg($tt); } );        
       
        $res = array( 
            "f_id" => $bkm["f_code"], "f_module_name" => $mod, "f_name" => $name, "f_user_id" => $bkm['fc_bkm_user_id'],
            "view" => [], 
            "data" => $bkm["fc_bkm_data"]?$bkm["fc_bkm_data"]:'[]',
            "bookmark"=>$bkm["fc_bkm_bookmark"]?$bkm["fc_bkm_bookmark"]:'[]'
        );
        
        $q->reset();
        $cols = $q->from("t_bookmarks", array("fc_bkm_bind", "fc_bkm_label", "fc_bkm_type", "fc_bkm_locked", "fc_bkm_visible", "fc_bkm_width",
            "fc_bkm_filter", "fc_bkm_range", "fc_bkm_prange", "fc_bkm_script","fc_bkm_level","fc_bkm_group"))->where("f_code = ?",$bkm["f_code"])
                ->query()->fetchAll();    
        
        array_walk_recursive($cols, function(&$t,&$k){ $tt=$t; if(gettype($tt)=="string") $t=Mainsim_Model_Utilities::chg($tt); } );
                        
        $n = count($cols);
        for($i=0; $i<$n; $i++) {            
            $res["view"]["f_properties"][] = array($cols[$i]["fc_bkm_bind"], $cols[$i]["fc_bkm_label"], intval($cols[$i]["fc_bkm_type"]), intval($cols[$i]["fc_bkm_locked"]),
                intval($cols[$i]["fc_bkm_visible"]), intval($cols[$i]["fc_bkm_width"]),
                (!is_null($cols[$i]["fc_bkm_filter"])?json_decode($cols[$i]["fc_bkm_filter"]):[]),
                (!is_null($cols[$i]["fc_bkm_range"])?json_decode($cols[$i]["fc_bkm_range"]):[]),
                (!is_null($cols[$i]["fc_bkm_prange"])?json_decode($cols[$i]["fc_bkm_prange"]):[]),
                $cols[$i]["fc_bkm_script"],intval($cols[$i]["fc_bkm_level"]),(!is_null($cols[$i]["fc_bkm_group"])?$cols[$i]["fc_bkm_group"]:''));
        }        
 
        $res["view"]["f_properties"] = json_encode($res["view"]["f_properties"]);                    
        return $res;
    }
    
    public function deleteUI($f_code = 0)
    {
        if($f_code > 0 && ctype_digit($f_code)) {
            $this->db->delete("t_reverse_ajax","f_code = $f_code");
            $this->db->delete("t_locked","f_code = $f_code");
            $this->db->delete("t_systems_parent","f_code = $f_code");
            $this->db->delete("t_custom_fields_history","f_code = $f_code");
            $this->db->delete("t_custom_fields","f_code = $f_code");
            $this->db->delete("t_selector_system","f_system_id = $f_code");
            $this->db->delete("t_wares_systems","f_system_id = $f_code");
            $this->db->delete("t_pair_cross","f_code_main = $f_code");
            $this->db->delete("t_bookmarks","f_code = $f_code");
            $this->db->delete("t_systems_history","f_code = $f_code");
            $this->db->delete("t_systems","f_code = $f_code");
            $this->db->delete("t_creation_date_history","f_id = $f_code");
            $this->db->delete("t_creation_date","f_id = $f_code");
        }
    }
    
    /**
     * Load sheet to import 
     */
    public function getSheets()
    {
        
        //MODIFICA DA MATTEO il 24 02 2017
        $sheets = [];
        $cross_sheets = array(
            array('value'=>'t_pair_cross','label'=>'PAIR CROSS'.' [t_pair_cross]','selected'=>false),
            array('value'=>'t_cross_wares_selectors','label'=>'CROSS WARES - SELECTORS'.' [t_cross_wares_selectors]','selected'=>false),
            array('value'=>'t_cross_workorders_wares','label'=>'CROSS WORKORDERS - WARES'.' [t_cross_workorders_wares]','selected'=>false),
            array('value'=>'t_cross_wares_wares','label'=>'CROSS WARES - WARES'.' [t_cross_wares_wares]','selected'=>false), 
            array('value'=>'t_cross_workorders_selectors','label'=>'CROSS WORKORDERS - SELECTORS'.' [t_cross_workorders_selectors]','selected'=>false),
            array('value'=>'t_cross_workorders_workorders','label'=>'CROSS WORKORDERS - WORKORDERS'.' [t_cross_workorders_workorders]','selected'=>false)
        );
        
        $res_sheets = $this->db->query("SELECT f_type, f_sheet_name from t_selectors_types")->fetchAll();
        $tot_a = count($res_sheets); 
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_sheets[$a]['f_sheet_name']) && !is_null($res_sheets[$a]['f_sheet_name'])){
                $label=$res_sheets[$a]['f_type']." [".$res_sheets[$a]['f_sheet_name']."]";
                $sheets[] = array('value'=>$res_sheets[$a]['f_sheet_name'],'label'=>$label,'selected'=>false);
            }
        }

        $res_sheets = $this->db->query("SELECT f_type, f_sheet_name from t_wares_types")->fetchAll();
        $tot_a = count($res_sheets);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_sheets[$a]['f_sheet_name']) && !is_null($res_sheets[$a]['f_sheet_name'])){
                   $label=$res_sheets[$a]['f_type']." [".$res_sheets[$a]['f_sheet_name']."]";
                $sheets[] = array('value'=>$res_sheets[$a]['f_sheet_name'],'label'=>$label,'selected'=>false);
            }
        }

        $res_sheets = $this->db->query("SELECT f_type, f_sheet_name from t_workorders_types")->fetchAll();
        $tot_a = count($res_sheets);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_sheets[$a]['f_sheet_name']) && !is_null($res_sheets[$a]['f_sheet_name'])){
                $label=$res_sheets[$a]['f_type']." [".$res_sheets[$a]['f_sheet_name']."]";
                $sheets[] = array('value'=>$res_sheets[$a]['f_sheet_name'],'label'=>$label,'selected'=>false);
            }
        }

        $sheets = array_merge($sheets,$cross_sheets);
        return $sheets;
    }
    
    public function changePhaseBatch($f_codes, $f_wf_id, $f_exit, $f_module_name) {
        return $this->obj->changePhase($f_codes, $f_wf_id, $f_exit, $f_module_name);
    }
}