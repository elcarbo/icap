<?php

class Mainsim_Model_Autocomplete
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));        
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    /**
     * @param type $field
     * @param type $table
     * @param type $cond
     * @param type $order
     * @param type $groupby
     * @param type $having
     * @return type Deprecated
     */
    public function _getList($field, $table, $cond, $order, $groupby, $having) {
        $data = array();
        $q = new Zend_Db_Select($this->db);
        if($groupby) $groupby .= ", "; $groupby .= "$field";
        $q->from($table, array($field));
        if($cond) $q->where($cond);
        if($order) $q->order($order);
        if($groupby) $q->group($groupby);
        if($having) $q->having($having);
        $results = $q->query()->fetchAll();
        foreach($results as $res) $data[] = $res[$field];
        return $data;
    }
    
    public function getList($scriptName, $value) {        
        /*
        $data = array();
        $q = new Zend_Db_Select($this->db);
        $results = $q->distinct()->from('t_creation_date', array('f_title'))->where("f_title LIKE '$value%'")
            ->where("f_type = 'WARES'")->where("f_category = 'ASSET'")->query()->fetchAll();        
        foreach($results as $res) $data[] = $res["f_title"];
        return $data;*/
        #print APPLICATION_PATH . '/../scripts/' . PROJECT_NAME . "/" . $scriptName . ".php";
         if(file_exists(APPLICATION_PATH . '/../scripts/' . PROJECT_NAME . "/" . $scriptName . ".php")){
            include(APPLICATION_PATH . '/../scripts/' . PROJECT_NAME . "/" . $scriptName . ".php");
        }
        else{
            echo "[]";
        }
    }

    public function getPicklistData($codes, $picklistName, $picklistType, $table){
        // get treegrid fields
        if(strpos($picklistType, 'slc') !== false){
            $picklistType = "slc";
        };
        $select = new Zend_Db_Select($this->db);
        $select->from('t_systems')
            ->where('fc_bkm_module = ?', 'mdl_' . $picklistType . '_tg');
        $res = $select->query()->fetchAll();

        $select = "SELECT 'root' AS rtree, '" . $picklistName . "' AS picklistName, t_creation_date.f_id AS value, t_creation_date.f_title AS label, " . str_replace(["f_code", "f_name"], [$table . ".f_code", "f_phase_id"], $res[0]['fc_bkm_fields_bind']) 
            . " FROM t_creation_date"
            . " JOIN " . $table . " ON " . $table . ".f_code = t_creation_date.f_id"
            . " JOIN t_custom_fields ON t_creation_date.f_id = t_custom_fields.f_code"
            . " WHERE t_creation_date.f_id IN (" . implode(',', $codes) . ")";
        $res = $this->db->query($select)->fetchAll();
        return $res;  
    }
}