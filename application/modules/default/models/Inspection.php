<?php

class Mainsim_Model_Inspection
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));        
    }
    
    public function create($params) {
        $sys = new Mainsim_Model_System();
        $res = $sys->newSystem($params);        
        $message = isset($res['message'])?$res['message']:'';                 
        $return = array(
            'name' => $params['f_title'],
            'shared' => $params['fc_bkm_user_id'] == 0?1:0,
            'message' => $message        
        );
        if(empty($return['message'])) {
            unset($return['message']);    
        }
        return $return;
    }
    
    public function delete($module_name, $name, $uid) 
    {
        $q = new Zend_Db_Select($this->db);
        $codes = $q->from(array('c' => 't_creation_date'), array())->join(array('s' => 't_systems'), 'c.f_id = s.f_code', array('s.f_code'))
            ->where('c.f_title = ?', $name)->where('s.fc_bkm_module = ?', $module_name)->where('s.fc_bkm_user_id = ?', $uid)
            ->query()->fetchAll();
        $sys = new Mainsim_Model_System();
        foreach($codes as $code) {
            $res = $sys->editSystem(array("f_code" => $code["f_code"],'f_module_name'=>'mdl_sys_tg','f_type_id'=>3, "f_phase_id" => 3));
            if(isset($res["message"])) return $res;
        }
        return array("shared" => ($uid == 0), "name" => $name);
    }

    public function getInspection($f_name, $f_module_name, $shared) 
    {
        $uid = ($shared == 1 ? 0 : Zend_Auth::getInstance()->getIdentity()->f_id);        
        $f_name = ($f_name != 'undefined' ? utf8_encode($f_name) : 'Default');
        $f_name = iconv('UTF-8', 'CP1252', $f_name);
        $sys = new Mainsim_Model_System();
        $data = $sys->getBookmark($f_module_name, $f_name, $uid);
        if(empty($data)) {
            $data = array(
                "f_id" => 0,
                "f_name" => "",                    
                "data" => array("Sort" => array(), "Filter" => array(), "Search" => array(), "Ands" => array(), "Selector" => array()),
                "view" => array(
                    array("f_code", "Code", 1, 1, 0, 80, array(), array(), array(), ""),
                    array("f_title", "Title", 0, 0, 1, 200, array(), array(), array(), ""),
                    array("f_description", "Description", 0, 0, 1, 300, array(), array(), array(), "")                    
                ),
                "bookmark" => array()
            );   
        }
        else {               
            $data['view'] = json_decode(Mainsim_Model_Utilities::chg($data['view']['f_properties']));
            $data['data'] = json_decode(Mainsim_Model_Utilities::chg($data['data']));            
            $data['bookmark'] = json_decode(Mainsim_Model_Utilities::chg($data['bookmark']));
        }        
        return $data;
    }
}