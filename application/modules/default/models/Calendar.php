<?php

class Mainsim_Model_Calendar
{
    private $db;
    private $smtp;     
    private $smtpParams;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function checkReverseAjax($params){ 
        $time = time() - 30;
        $select = new Zend_Db_Select($this->db);
        $select->from('t_reverse_ajax')
               ->where('f_timestamp > ' . $time)
               ->where('f_code IN (' . implode(",", $params['f_codes']) . ')');
        $res = $select->query()->fetchAll();
        if(count($res) > 0){
            return 1;
        }
        else{
            return 0;
        }
            

    }
    
    public function getCalendarWorkorder($params){

        $select = new Zend_Db_Select($this->db);
        $select->from('t_creation_date', array('f_title', 'f_description', 'f_creation_date', 'fc_creation_user_name'))
               ->join('t_workorders', 't_creation_date.f_id = t_workorders.f_code', array('f_priority', 'f_start_date', 'f_end_date', 'fc_wo_starting_date', 'fc_wo_ending_date', 'fc_owner_name', 'f_type_id'))
               ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array('f_summary_name')) 
               ->where('f_code = ' . $params['f_code']);
        $res = $select->query()->fetchAll();
        
        $select->reset();
        $select->from('t_ware_wo')
               ->join('t_creation_date', 't_ware_wo.f_ware_id = t_creation_date.f_id')
               ->where('t_ware_wo.f_wo_id = ' . $params['f_code'])
               ->where('t_creation_date.f_category = ?','ASSET');
        $res2 = $select->query()->fetchAll();
        $assets = array();
        if(count($res2) > 0){
            for($i = 0; $i < count($res2); $i++){
                $assets[] = $res2[$i]['f_title'];
            }           
        }
        
        $result = array(
            is_null($res[0]['f_title']) ? '' : $res[0]['f_title'],
            is_null($res[0]['f_description']) ? '' : $res[0]['f_description'],
            is_null($res[0]['f_type_id']) ? '' : (int)$res[0]['f_type_id'],
            is_null($res[0]['f_priority']) ? '' : (int)$res[0]['f_priority'],
            is_null($res[0]['fc_owner_name']) ? '' : $res[0]['fc_owner_name'],
            is_null($res[0]['fc_creation_user_name']) ? '' : $res[0]['fc_creation_user_name'],
            implode(", ", $assets),
            is_null($res[0]['f_creation_date']) ? '' : (int)$res[0]['f_creation_date'],
            is_null($res[0]['f_start_date']) ? '' : (int)$res[0]['f_start_date'],
            is_null($res[0]['f_end_date']) ? '' : (int)$res[0]['f_end_date'],
            is_null($res[0]['fc_wo_starting_date']) ? '' : (int)$res[0]['fc_wo_starting_date'],
            is_null($res[0]['fc_wo_ending_date']) ? '' : (int)$res[0]['fc_wo_ending_date'],
            is_null($res[0]['f_summary_name']) ? '' : $res[0]['f_summary_name']
        );
        
        return $result;
    }
    
    public function getCalendarOptions(){
        
        // get wo icons
        $result['menu']['wotype'] = array(
            "1"=>array("Corrective","58595"),
            "4"=>array("Periodic","58608"),
            "6"=>array("On Condition","58460"), 
            "7"=>array("Meter Reading","58611"),  
            "10"=>array("Corrective Task","58595"),
            "13"=>array("Emergency","58583")
        );
        
 
        // get priority
        $aux = null;
        $select = new Zend_Db_Select($this->db);
        $select->from('t_creation_date', array('f_title'))
               ->join('t_wares', 't_creation_date.f_id = t_wares.f_code', array('fc_action_priority_color', 'fc_action_priority_value'))
               ->where("t_creation_date.f_category = 'ACTION'")
               ->where("t_wares.fc_action_type = 'priority'");
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $aux[$res[$i]['fc_action_priority_value']] = array($res[$i]['f_title'], $res[$i]['fc_action_priority_color']);
        }
        $result['menu']['priority'] = $aux;

  
        // get users
        $aux = null;
        $select->reset();
        
        $select->from('t_users', array('f_code', 'fc_usr_firstname', 'fc_usr_lastname'));
        $res = $select->query()->fetchAll();
        
        
        
        for($i = 0; $i < count($res); $i++){
            $aux[] = array($res[$i]['f_code'], $res[$i]['fc_usr_firstname'] . ' ' . $res[$i]['fc_usr_lastname']);
            //  $aux[] = array($res[$i]['f_code'], $res[$i]['fc_usr_firstname'] );
        if($i>4) break;
       }
        $result['menu']['assignment'] = $aux;


              
        // get type range
        $result['menu']['typerange']= array(
            array("0","Planned"),
            array("1","Effective"),
            array("2","Open")    
        );
        
        $labels = array(
            'f_title',
            'f_description',
            'fc_progress',
            'f_creation_date',
            'f_priority',
            'fc_owner_name',    
            'fc_creation_user_name',
            'fc_wo_asset',
            'f_start_date', 
            'f_end_date',
            'fc_wo_starting_date',
            'fc_wo_ending_date'
        );
        
        for($k = 0; $k < count($labels); $k++){
            $select->reset();
            $select->from('t_ui_object_instances')
                   ->where("f_instance_name like '%wo_edit%' and f_type_id in (6,7,8,13,14) and f_properties like '%\"bind\":\"" . $labels[$k] . "\"%'");
            $res = $select->query()->fetchAll();
            $aux = json_decode($res[0]['f_properties']);
            
            $result['labels'][$labels[$k]] = $aux->label;
        }
        $result['labels']['f_category'] = 'Category';
        require APPLICATION_PATH . '/configs/lang/en_GB.php';
                   
        // get workorders shortnames
        $aux = null;
        $select->reset();
        $select->from('t_workorders_types', array('f_id', 'f_summary_name'));
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $aux[$res[$i]['f_id']] = $res[$i]['f_summary_name'];
        }
        $result['wo_summary_name'] = $aux;
        
        return $result;
    }
    
    public function getCalendarWorkorders($params){
        
        $startDate = mktime (0,0,0, $params['daterange']["from"][1]+1, $params['daterange']["from"][2], $params['daterange']["from"][0]);
        $endDate = mktime (23,59,59, $params['daterange']["to"][1]+1, $params['daterange']["to"][2], $params['daterange']["to"][0]);

        // get user selectors
        $userData = Zend_Auth::getInstance()->getIdentity();
 
        
        //$session = new Zend_Session_Namespace('session');
        //$userData = $session->data;
        
      /*  
        $selectors = Mainsim_Model_MobileUtilities::getSelectors($userData->f_id, 'workorders');
        // group selector by type
        $selectorsList = array();
        for($i = 0; $i < count($selectors); $i++){
            if(!$selectorsList[$selectors[$i]['f_type_id']]){
                $selectorsList[$selectors[$i]['f_type_id']] = $selectors[$i]['f_selector_id'];
                $selectorsTypeList[] = $selectors[$i]['f_type_id'];
            }
            else{
                $selectorsList[$selectors[$i]['f_type_id']] .= ',' . $selectors[$i]['f_selector_id'];
            }
        }
    */                       
        
        switch($params['typerange']){
            
            case 0:
                //wo query fcode, progress, type, title, priority, from_date, to_date,   
                $select = new Zend_Db_Select($this->db);
                $select->from(array("wo" => "t_workorders"), array('f_type_id', 'f_priority', 'f_start_date', 'f_end_date'))
                       ->join(array("cd" => "t_creation_date"), "wo.f_code = cd.f_id", array('f_id', 'fc_progress', 'f_title', 'f_creation_date'))
                       ->join(array("wp" => "t_wf_phases"), "cd.f_phase_id = wp.f_number AND cd.f_wf_id = wp.f_wf_id", array('f_name'))
                       ->join(array("wg" => "t_wf_groups"), "wp.f_group_id = wg.f_id", array())
                       ->joinLeft(array("sw" => "t_selector_wo"), 'wo.f_code = sw.f_wo_id', array())
                       ->joinLeft(array("st" => "t_selectors_types"), 'sw.f_selector_id = st.f_id', array())
                       //->where("(sw.f_nesting_level = 0 || sw.f_nesting_level is null)")
                       ->where("cd.f_visibility & " . $userData->f_level . " != 0")
                       ->where("cd.f_visibility & " . $userData->f_group_level . " != 0")
                       ->where("wp.f_visibility & " . $userData->f_level . " != 0")
                       ->where("wg.f_visibility & " . $userData->f_level . "!= 0");
                $select->where("NOT(wo.f_start_date > " . $endDate . " OR wo.f_end_date < " . $startDate . ") OR ( (wo.f_start_date IS NULL OR wo.f_start_date=0) AND (wo.f_end_date=0 OR wo.f_end_date IS NULL) AND cd.f_creation_date > " . $startDate . " AND cd.f_creation_date < " . $endDate . ")"
                        . "OR ( (wo.f_start_date IS NOT NULL AND wo.f_start_date!=0)  AND (wo.f_end_date=0 OR wo.f_end_date IS NULL) AND cd.f_creation_date > " . $startDate . " AND cd.f_creation_date < " . $endDate . ")"
                        . "OR ( (wo.f_start_date IS NULL OR wo.f_start_date=0) AND wo.f_end_date IS NOT NULL AND wo.f_end_date > " . $startDate . " AND wo.f_end_date < " . $endDate . ")");
                break;
            
            case 1:
                $select = new Zend_Db_Select($this->db);
                $select->from(array("wo" => "t_workorders"), array('f_type_id', 'f_priority', 'fc_wo_starting_date', 'fc_wo_ending_date'))
                       ->join(array("cd" => "t_creation_date"), "wo.f_code = cd.f_id", array('f_id', 'fc_progress', 'f_title', 'f_creation_date'))
                       ->join(array("wp" => "t_wf_phases"), "cd.f_phase_id = wp.f_number AND cd.f_wf_id = wp.f_wf_id", array())
                       ->join(array("wg" => "t_wf_groups"), "wp.f_group_id = wg.f_id", array())      
                       ->joinLeft(array("sw" => "t_selector_wo"), 'wo.f_code = sw.f_wo_id', array())
                       ->joinLeft(array("st" => "t_selectors_types"), 'sw.f_selector_id = st.f_id', array())
                       //->where("(sw.f_nesting_level = 0 || sw.f_nesting_level is null)")
                       ->where("cd.f_visibility & " . $userData->f_level . " != 0")
                       ->where("cd.f_visibility & " . $userData->f_group_level . " != 0")
                       ->where("wp.f_visibility & " . $userData->f_level . " != 0")
                       ->where("wg.f_visibility & " . $userData->f_level . "!= 0");
                $select->where("NOT(wo.fc_wo_starting_date > " . $endDate . " OR wo.fc_wo_ending_date < " . $startDate . ") OR ( (wo.fc_wo_starting_date IS NULL OR wo.fc_wo_starting_date=0) AND (wo.fc_wo_ending_date IS NULL OR wo.fc_wo_ending_date=0) AND cd.f_creation_date > " . $startDate . " AND cd.f_creation_date < " . $endDate . ")"
                . "OR ( (wo.fc_wo_starting_date IS NOT NULL AND wo.fc_wo_starting_date!=0) AND (wo.fc_wo_ending_date IS NULL OR wo.fc_wo_ending_date=0) AND wo.fc_wo_starting_date > " . $startDate . " AND wo.fc_wo_starting_date < " . $endDate . ")"
                . "OR ( (wo.fc_wo_starting_date IS NULL OR wo.fc_wo_starting_date=0) AND wo.fc_wo_ending_date IS NOT NULL AND wo.fc_wo_ending_date > " . $startDate . " AND wo.fc_wo_ending_date < " . $endDate . ")");        
                break;
            
            case 2:
                $select = new Zend_Db_Select($this->db);
                $select->from(array("wo" => "t_workorders"), array('f_type_id', 'f_priority', 'f_start_date', 'f_end_date'))
                       ->join(array("cd" => "t_creation_date"), "wo.f_code = cd.f_id", array('f_id', 'fc_progress', 'f_title', 'f_creation_date'))
                       ->join(array("wp" => "t_wf_phases"), "cd.f_phase_id = wp.f_number AND cd.f_wf_id = wp.f_wf_id", array())
                       ->join(array("wg" => "t_wf_groups"), "wp.f_group_id = wg.f_id", array())      
                       ->joinLeft(array("sw" => "t_selector_wo"), 'wo.f_code = sw.f_wo_id', array())
                       ->joinLeft(array("st" => "t_selectors_types"), 'sw.f_selector_id = st.f_id', array())
                       //->where("(sw.f_nesting_level = 0 || sw.f_nesting_level is null)")
                       ->where("cd.f_visibility & " . $userData->f_level . " != 0")
                       ->where("cd.f_visibility & " . $userData->f_group_level . " != 0")
                       ->where("wp.f_visibility & " . $userData->f_level . " != 0")
                       ->where("wg.f_visibility & " . $userData->f_level . "!= 0");
                $select->where("cd.f_creation_date  >" . $startDate . " AND cd.f_creation_date < " . $endDate);
                break;
        }
        
        
     
       
          // Service Requestor  "self"
          if($userData->f_level == 8 ){
             $select->where('cd.f_creation_user = ' . $userData->f_id );
          }
          
          // External Mantainer  "cross wo user"   
          if($userData->f_level == 32 ){ 
            $select->join('t_ware_wo', 't_ware_wo.f_wo_id = wo.f_id', array());
            $select->where('t_ware_wo.f_ware_id = ' . $userData->f_id);  
          }
      
 
        
        //$select->where("wo.f_type_id IN (1,4,6,7,10,13)");
        
        if(!$userData->selectors) $selectorsList=array();
        else $selectorsList=array( $userData->selectors );
        
        // print_r($selectorsList); die;
      
        
        // filter by selectors
        for($k=0; $k<count($selectorsList); $k++){
            $where[] =  'sw.f_selector_id IN (' . $selectorsList[$k]. ')';
        }
        
     
        if($where){
            $select->where('' . implode(' AND ', $where) );  //. ' OR sw.f_selector_id IS NULL)');
        }
       
       
        // filter by group phases
        if($params['group_phases']){
            $select->where("wg.f_id IN (" . $params['group_phases'] . ")");
        }
        
        
        
        
   
            $result = array();
            $select->distinct();

         //  echo($select); die;
            
            $res = $select->query()->fetchAll();
            
            
            for($i = 0; $i < count($res); $i++){
   
                $tle=$res[$i]["f_title"];
                $tle=utf8_decode( $res[$i]["f_title"] );
                $res[$i]["f_title"]=str_replace("'","",$tle);
                
                
                // search if exists a owner
                $select->reset();
                $select->from(array("ww" => "t_ware_wo"), array())
                       ->join(array("cd2" => "t_creation_date"), 'ww.f_ware_id = cd2.f_id', array('f_id as owner_code'))
                       ->where('cd2.f_category = ?', 'USER')
                       ->where('ww.f_wo_id = ' . $res[$i]['f_id']);
                $resOwner = $select->query()->fetchAll();
                
                switch($params['typerange']){
                    case 0:
                        if($res[$i]['f_start_date'] != null && $res[$i]['f_end_date'] != null){
                            $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['f_start_date'], $res[$i]['f_end_date'], $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        }
                        else if($res[$i]['f_start_date'] != null && $res[$i]['f_end_date'] == null){
                            $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['f_start_date'], $res[$i]['f_start_date'], $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        }
                        else if($res[$i]['f_start_date'] == null && $res[$i]['f_end_date'] != null){
                            $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['f_end_date'], $res[$i]['f_end_date'], $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        }
                        else{
                            $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['f_creation_date'], null, $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        }
                        break;
                    
                    case 1:
                        if($res[$i]['fc_wo_starting_date'] != null && $res[$i]['fc_wo_ending_date'] != null){
                            $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['fc_wo_starting_date'], $res[$i]['fc_wo_ending_date'], $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        }
                        else if($res[$i]['fc_wo_starting_date'] != null && $res[$i]['fc_wo_ending_date'] == null){
                            $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['fc_wo_starting_date'], $res[$i]['fc_wo_starting_date'], $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        }
                        else if($res[$i]['fc_wo_starting_date'] == null && $res[$i]['fc_wo_ending_date'] != null){
                            $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['fc_wo_ending_date'], $res[$i]['fc_wo_ending_date'], $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        }
                        else{
                            $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['f_creation_date'], null, $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        }
                        break;
                    
                    case 2:
                        $result[] = array($res[$i]['f_id'], $res[$i]['fc_progress'], $res[$i]['f_type_id'], $res[$i]['f_title'], $res[$i]['f_priority'], $res[$i]['f_creation_date'], null, $resOwner[0]['owner_code'] ? $resOwner[0]['owner_code'] : "");
                        break;
                        
                }
                
            }
            return $result;
    
    }
} 