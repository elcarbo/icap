<?php

class Mainsim_Model_Moduleimporter
{
    private $db,$class,$impField,$newMethod,$editMethod;
    //max num allow of row inside a sheet, after this, you have to use cron system
    private $num_row_controll = 3000;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    /**
     * Set import attribute by table
     * @param type $table
     */
    private function setImportAttrByTable($table)
    {
        if($table == 't_wares') {
            $this->class = 'Mainsim_Model_Wares';
            $this->newMethod = 'newWares';
            $this->editMethod = 'editWares';
            $this->impField = 'fc_imp_code_wares_excel';
        }
        elseif($table == 't_workorders') {
            $this->class = 'Mainsim_Model_Workorders';
            $this->newMethod = 'newWorkOrder';
            $this->editMethod = 'editWorkOrder';
            $this->impField = 'fc_imp_code_wo_excel';
        }
        elseif($table == 't_selectors') {
            $this->class = 'Mainsim_Model_Selectors';
            $this->newMethod = 'newSelector';
            $this->editMethod = 'editSelector';
            $this->impField = 'fc_imp_code_sel_excel';
        }
        else{
            throw new Exception("Undefined table");
        }
    }
    
    public function upload($files,$module)
    {
        $result = array();
        $select = new Zend_Db_Select($this->db);
        if(!isset($files['fileattach']) or $files['fileattach']['error'] != UPLOAD_ERR_OK) { 
           $result['err']=1; 
           $result['risp']="File Error!"; 
           return $result;
        }
        //if file uploaded grater than 3Mb : ERROR
        if(!$files['fileattach']['size'] > 3145728){ 
            $result['err']=1; 
            $result['risp']="File Size Error!File cannot be grater than 3Mb";
            return $result;
        }
        
        //save file
        $ext = explode('.',$files["fileattach"]["name"]);
        $ext = array_pop($ext); 
        $folder = Zend_Registry::get('attachmentsFolder').DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
        if($ext == 'xlsx' || $ext == 'xls') {    
            $filename = str_replace(".$ext", '_1_'.$_SERVER['REQUEST_TIME'].".$ext",$files["fileattach"]["name"] );//filename will create using filename_uid_timestamp            
            move_uploaded_file($files["fileattach"]["tmp_name"],$folder . $filename);
        }
        else {
            $result['err']=1; $result['risp']="File Error!";
            return $result;
        }

        // Read sheets and headers of xls file                        
        $excelFile = realpath($folder . $filename);
        $file = $this->IterateExcel($excelFile,1,1);
        $sheetname = $file->getSheetNames();            
        //get header and row for every sheet
        $tot = count($sheetname);
        for($i = 0;$i < $tot;++$i) {                
            $key_sheet = array();
            $rowIterator = $file->setActiveSheetIndex($i)->getRowIterator();    
            $this->rowIteratorImport($rowIterator,$key_sheet,true);            
            $headers[] = $key_sheet;
        }
        
        $labels = array();
        $bookmarks = array();
        $this->getBookmarks($module, $bookmarks, $labels);
        
        //get type(s) id             
        $res_type = $select->from("t_ui_object_instances","f_properties")->where("f_instance_name = ?",$module)->query()->fetch();
        $prop = json_decode(Mainsim_Model_Utilities::chg($res_type['f_properties']),true);            
        $f_types = array();
        $table = $prop['table'].'_types';

        $select->reset();        
        $res_types = $select->from($table)->where("f_id IN ({$prop['ftype']})")->query()->fetchAll();       
        $tot_types = count($res_types);
        for($t = 0;$t < $tot_types;++$t) {
            $f_types[$res_types[$t]['f_id']] = $res_types[$t]['f_type'];
        }

        $keysheet=array(); $project=array();
        $sequences=array(); $disabledsequence=array();

        $result['risp']=json_encode(array("bookmark"=>$bookmarks,"filename"=>$filename,"labels"=>$labels,"disabledsequence"=>$disabledsequence,"headers"=>$headers, "sequence"=>$sequences, "sheetname"=>$sheetname, "keysheet"=>$keysheet, "projects"=>$project,"typelist"=>$f_types));            
        $result['err'] = 0;
        return $result;
     //
    }
    
    /**
     * Get all bookmarks for passed module name
     * @param type $moduleName
     */
    private function getBookmarks($moduleName,&$bookmarks,&$labels)
    {
        $sys = new Mainsim_Model_System();        
        $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
        $select = new Zend_Db_Select($this->db);
        $resBkmList = $select->from(["t1"=>"t_creation_date"],"f_title")
            ->join(['t2'=>'t_systems'],"t1.f_id = t2.f_code",[])
            ->where("t2.f_type_id = 3")->where("f_phase_id = 1")
            ->where("fc_bkm_module = ?",$moduleName)->query()->fetchAll();        
        $tot = count($resBkmList);
        for($i = 0;$i < $tot;++$i) {            
            $bkmName = $resBkmList[$i]['f_title'];
            $res_bm = $sys->getBookmark($moduleName, $bkmName,[0,$uid]);
            $res_bm = $res_bm['view'];            
            $bm = json_decode(Mainsim_Model_Utilities::chg($res_bm['f_properties']));
            if(is_null($bm)) { continue; }
            array_walk($bm, function($el) use(&$bookmarks,&$labels,$bkmName){
                $bookmarks[$bkmName][] = $el[0];
                $labels[$bkmName][] = trim(preg_replace('/\r|\n/',' ', $el[1]));                     
            });            
        }
    }
    
    public function elaborate($params)
    {          
        $select = new Zend_Db_Select($this->db);
        $params = json_decode(Mainsim_Model_Utilities::chg($params),true);
        $f_type_id = (int)$params['f_type_id'];
        $sheet = $params["sheetname"];
        $filename = $params["filename"];
        $folder = Zend_Registry::get('attachmentsFolder').DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
        $excelFile = $folder.$filename;
        $module = $params["module"];        
		$keyColumn = isset($params['keysheet'])?$params['keysheet']:false;
        //edit        
        //mode : 0 = Add, 1=Add Value        
        $sequence = $params['sequence'];
        $disabledsequence = $params['disabledsequence'];//params excluded from import                
        $resTg = $select->from("t_ui_object_instances","f_properties")
            ->where("f_instance_name like ?",$module)->query()->fetch();
        $prop_tg = json_decode(Mainsim_Model_Utilities::chg($resTg['f_properties']),true);                
        //get table and set import attributes
        $table = $prop_tg['table'];		        
		$this->setImportAttrByTable($table);
        
        //get moduleComponent and get UI bind and expression (if setted before)	        
        $moduleComponent = str_replace("_tg","_edit_c1",$module);
        $uiFields = $this->getCheckFromUi($moduleComponent);        
        
        //get keyColumn for addValue type        
        $keys = array_keys($sequence);		
        if($keyColumn !== false) {
            $newKeyColumn = [];
            foreach($keyColumn as $lineColumn) {
                $newKeyColumn[] =  $sequence[$keys[$lineColumn]];
            }
            $keyColumn = $newKeyColumn;
        } 
        
        //get excel header
        $headers = [];
        $file = $this->IterateExcel($excelFile,1,1);
        $sheetnames = $file->getSheetNames();        
        $pos = array_search($sheet,$sheetnames);        
        $rowIterator = $file->setActiveSheetIndex($pos)->getRowIterator();    
        $this->rowIteratorImport($rowIterator,$headers,true);		        
        
        //get data from excel
		$position = 1; $inc = 1000; $data = [];
        do {
            $file = $this->IterateExcel($excelFile,$position,$inc,$sheet);
            $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
            $objects = $this->rowIteratorImport($rowIterator,$headers,false);             
            $file->disconnectWorksheets();
            unset($file);
            
            $position+=$inc;
            $tot_b = count($objects);
            for($b = 0;$b <$tot_b;++$b) {
                $line = $objects[$b];
                $row = [];
                //remove now allow columns
                $keys = array_diff(array_keys($line),$disabledsequence);
                array_walk($keys,function($el) use(&$row,$sequence,$line){ 
                    $row[$sequence[$el]] = $line[$el];
                });
                $data[] = $row;
            }
            if(count($objects)<$inc-1){ break;}
        }while(true);           
        
        
        $res = [];  
        try {
            //check key error
            $notUniqueKeys = $this->checkUniqueKey($data, $keyColumn, $table, $f_type_id, $params['mode']);
            if(!empty($notUniqueKeys)){
                $res['message'] = $this->writeResponseMessage("Some values are duplicated.",$params['mode'],
                    0,$notUniqueKeys);
                unlink($excelFile);return $res;
            }
            //check any error         
            $this->checkError($data,$uiFields);
            $this->db->beginTransaction();
            $time = time();  
            
            Zend_Registry::set('time',$time);
            if($params['mode'] == 0) { //import
                $logs = $this->addRow($data, $table,$f_type_id,$module,$keyColumn);			
                $this->createFather($f_type_id, $table);
            }
            elseif($params['mode'] == 1) { //update
                $logs = $this->addValueRow($data, $table,$f_type_id,$keyColumn,$module);
            }
            elseif($params['mode'] == 2) { //rollback
                $logs = $this->rollBack($data, $table,$f_type_id,$keyColumn,$module);
            }
            $this->db->commit();
            $res['message'] = $this->writeResponseMessage('Sheet imported successfully.',$params['mode'], count($data),$logs);
        }catch(Exception $e) {                 
            try{$this->db->rollBack();}catch(Exception $ex){};            
            $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
            $this->db->delete("t_locked","f_user_id = {$uid}");
            $res['message'] = $this->writeResponseMessage($e->getMessage(),$params['mode']);            
        }        
        unlink($excelFile);
        return $res;
    }
    
    
    private function writeResponseMessage($message,$type = 0,$totRows = 0,$logsLine = [])
    {
        $fileLog = date('d-m-Y H-i')."_moduleImportError.log";        
        $fileLogContent = $totRows > 0?$message:"Error occurred: ".$message;
        $workedRows = $totRows == 0?'0/0':($totRows-count($logsLine)).'/'.$totRows;
        $workType = $type == 0?"imported":($type == 1?"updated":"recovered");
        $fileLogContent.=PHP_EOL."Row $workType: ".$workedRows.PHP_EOL;    
        $returnMessage = $fileLogContent;
        if(count($logsLine)){
            $fileLogContent.="List of incorrect elements in your excel:".PHP_EOL.' - ';
            $fileLogContent.=implode(PHP_EOL.' - ',$logsLine);
        }        
        file_put_contents(APPLICATION_PATH.'/../temp/'.$fileLog, $fileLogContent, FILE_APPEND);
        $returnMessage .= "See log file $fileLog located in temp directory for more informations.";
        $returnMessage = nl2br($returnMessage);
        return $returnMessage;
    }
    
    /**
     * 
     * @param array $data: dirty data from excel
     * @param array $uiFields: controll list
     * @param int $mode: 0: insert,1: update
     * @param array $keyColumn: for update only, list of key column to check
     */
    private function checkError($data,$uiFields)
    {
        $tot_b = count($data);
        $line_keys = $tot_b > 0 ?array_keys($data[0]):array();
        $errors = array();
        for($b = 0; $b < $tot_b; ++$b) {
            $line = $data[$b];
            $tot_c = count($line_keys);
            for($c = 0; $c < $tot_c;++$c) {
                if(isset($uiFields[$line_keys[$c]]['validation'])) {
                    if( !in_array($line[$keys[$c]], $uiFields[$line_keys[$c]]['validation'])
                            && (!is_null($line[$keys[$c]]) || !empty($line[$keys[$c]]) ) ) {
                        $errors[] = "Value not valid for field {$line_keys[$c]} at line $b";
                    }
                }
                /* next soon
                 * if(!empty($uiFields[$line_keys[$c]]['exp']) ) {
                    if(!$this->check($line[$keys[$c]],$uiFields[$line_keys[$c]]['exp'],$keys[$c],$table,$f_type_id,$data)) 
                        return array('message'=>'Value not valid for field '.$line_keys[$c].' at line '.$b);
                }*/
            }
        }
        return $errors;
    }


    /**
     * return all validation and check expression from
     * UI in passed module
     * @param type $editModule
     */
    private function getCheckFromUi($editModule)
    {
        $select = new Zend_Db_Select($this->db);
        $res_c1 = $select->from("t_ui_object_instances","f_properties")
            ->where("f_instance_name like ?",$editModule)->query()->fetch();
        $prop_c1 = json_decode(Mainsim_Model_Utilities::chg($res_c1['f_properties']),true);                
        $c1_fields_str = explode(',',  str_replace('|', ',', $prop_c1['fields']));
        //get UI bind and expression (if setted before)
        $select->reset();
        $res_ui = $select->from("t_ui_object_instances",array("f_instance_name","f_properties"))
                ->where("f_instance_name IN (?)",$c1_fields_str)->query()->fetchAll();				        
        $tot_ui = count($res_ui);
        $uiFields = array();        
        for($ui = 0;$ui < $tot_ui;++$ui) {
            //if UI is a select or check box looking for option for validation table
            $prop = json_decode(Mainsim_Model_Utilities::chg($res_ui[$ui]['f_properties']),true);   			            
            $uiFields[$prop['bind']] = array('exp'=>isset($prop['checkExp'])?$prop['checkExp']:'');                    
            if(strpos($res_ui[$ui]['f_instance_name'],'slct_') !== false ) {
				if(!empty($prop['options'])) { //get category
                    $tot_opt = count($prop['options']);
					for($opt = 0;$opt < $tot_opt;++$opt) {                        
						$uiFields[$prop['bind']]['validation'][] = $prop['options'][$opt]['value'];
					}
				}
            }
            elseif(strpos($res_ui[$ui]['f_instance_name'],'chkbx_') !== false) {                
                $tot_bt = count($prop['buttons']);
                for($bt = 0;$bt < $tot_bt;++$bt) {
                    $uiFields[$prop['bind']]['validation'][] = $prop['buttons'][$bt]['value'];
                }                
            }
        }
        return $uiFields;
    }
    
    private function addRow($rows,$table,$f_type_id,$module_name,$keyColumn)
    {        
        $class = $this->class;
        $method = $this->newMethod;         
        $time = Zend_Registry::get('time');
        $obj = new $class($this->db);        
        $tot = count($rows);        
        //get type to insert in t_creation_date            
        if($tot) {
            $keys = array_keys($rows[0]);
        }
        $tot_keys = count($keys);        
        for($i = 0; $i < $tot;++$i) {            
            $row = $rows[$i];
            $params = array();
            $codeValue = array();
            for($k = 0;$k < $tot_keys;++$k) {                
                if($keys[$k] == 'f_parent_code'){
                    $params['fc_imp_parent_code'] = $row[$keys[$k]];                    
                }     
                if(in_array($keys[$k], $keyColumn)) {                    
                    $codeValue[] = $row[$keys[$k]];                                        
                }
                $params[$keys[$k]] = $row[$keys[$k]];
            }            
            $params['f_code'] = 0;
            $params['f_type_id'] = $f_type_id;
            $params['f_module_name'] = $module_name;            
            if($table == 't_wares' || $table == 't_selectors') {                
                $res = $obj->$method($params,array(),false);
            }
            else {                
                $res = $obj->$method($params,false,array());
            }
            
            if(!isset($res['f_code'])) {                
                $linerr = $i+1;
                //throw new Exception("Error occurred at line {$linerr}: ".$res['message']);
                throw new Exception("Error occurred at line {$linerr}: ".$res['message']);
            }      
            $this->db->update("t_creation_date_history",array('fc_type_history'=>'Element added from moduleImporter system'),
              "f_code = {$res['f_code']} and f_timestamp = {$time}");
        }      
        return array();
    }
    
    private function addValueRow($rows,$table,$f_type_id,$key_search,$module)
    {
        $tot_a = count($rows);		
		if($tot_a == 0) return;
		$class = $this->class;
        $time = Zend_Registry::get('time');
        $obj = new $class($this->db);
		$method = $this->editMethod;		        
        $select = new Zend_Db_Select($this->db);
        $lineMissed = [];
        
        for($a = 0; $a < $tot_a; ++$a) {
            $line = $rows[$a];
			$params = $line;
			$params['f_type_id'] = $f_type_id;
			$params['f_module_name'] = $module;
			
			//get f_code			
            $select->reset();
            $totks = count($key_search);
            $select->from(["t1"=>"t_creation_date"],[])
                ->join(["t2"=>$table],"t1.f_id = t2.f_code",["f_code"])
                ->join(["t3"=>"t_custom_fields"],"t2.f_code = t3.f_code",[])
                ->where("f_type_id = ?",$f_type_id)->order("t1.f_timestamp DESC");            
            $keyArray = [];
            for($ks = 0;$ks < $totks; ++$ks) {
                $keyArray[] = $key_search[$ks].': "'.$rows[$a][$key_search[$ks]].'"';
                $prefix = in_array($key_search[$ks],['f_code','f_timestamp','f_id'])?"t2.":"";
                $select->where("{$prefix}$key_search[$ks] = ?",$rows[$a][$key_search[$ks]]);
            }
            
            $res_f_code = $select->query()->fetch();				
            if(empty($res_f_code)){ $lineMissed[] = "Record not found for key/s ".  implode(', ', $keyArray); continue; }
            $params['f_code'] = $res_f_code['f_code'];
			
			$uid = Zend_Auth::getInstance()->getIdentity()->f_id;            
			$this->db->insert("t_locked",['f_code'=>$params['f_code'],'f_user_id'=>$uid,'f_timestamp'=>time()]);
			$res_check = $obj->$method($params,false,false);            
			$this->db->delete("t_locked","f_code = {$params['f_code']} and f_user_id = $uid");									
            if(isset($res_check['message'])) {      
                $pos = $a+1;
                throw new Exception("Error at line {$pos} : ".$res_check['message']);
            }
            $this->db->update("t_creation_date_history",['fc_type_history'=>'Element modified from moduleImporter system'],
              "f_code = {$params['f_code']} and f_timestamp = {$time}");
        }
        return $lineMissed;
    }
    
    /**
     * Rollback passed record to the previous version (not over)
     */
    private function rollBack($rows,$table,$f_type_id,$key_search)
    {
        $tot_a = count($rows);
        $time = Zend_Registry::get('time');        
        $select = new Zend_Db_Select($this->db);
        
        $creationTable = Mainsim_Model_Utilities::get_tab_cols('t_creation_date');
        $defaultTable = Mainsim_Model_Utilities::get_tab_cols($table);
        $customTable = Mainsim_Model_Utilities::get_tab_cols('t_custom_fields');
        
        $uid = Zend_Auth::getInstance()->getIdentity()->f_id;            
        $userinfo = Mainsim_Model_Login::getUSerInfo($uid,$this->db);
        
        $lineMissed = [];
                
        for($a = 0; $a < $tot_a; ++$a) {            
			//get f_code			
            $select->reset();
            $totks = count($key_search);
            $select->from(["t1"=>"t_creation_date"],[])
                ->join(["t2"=>$table],"t1.f_id = t2.f_code",["f_code"])
                ->join(["t3"=>"t_custom_fields"],"t2.f_code = t3.f_code",[])
                ->where("f_type_id = ?",$f_type_id);     
            $keyArray = [];
            for($ks = 0;$ks < $totks; ++$ks) {
                $keyArray[] = $key_search[$ks].': "'.$rows[$a][$key_search[$ks]].'"';
                $prefix = in_array($key_search[$ks],['f_code','f_timestamp','f_id'])?"t2.":"";
                $select->where("{$prefix}$key_search[$ks] = ?",$rows[$a][$key_search[$ks]]);
            }
            
            $res_f_code = $select->order("t1.f_timestamp desc")
                ->limit(1)->query()->fetch();
            if(empty($res_f_code)){
                $lineMissed[] = "Record not found for key/s ".  implode(', ', $keyArray); continue; 
            }
            $f_code = $res_f_code['f_code'];
            
            $select->reset();
            $resValueToRestore = $select->from(['t1'=>'t_creation_date_history'])
                ->join(["t2"=>"{$table}_history"],"t1.f_code = t2.f_code and t1.f_timestamp = t2.f_timestamp")
                ->join(["t3"=>"t_custom_fields_history"],"t1.f_code = t3.f_code and t1.f_timestamp = t3.f_timestamp")
                ->where("t1.f_code = ?",$f_code)->order("t1.f_timestamp desc")->limit(1,1)->query()->fetch();
			if(empty($resValueToRestore)){
                $lineMissed[] = "Record with key/s ".  implode(', ', $keyArray)." cannot be recover because is a new record."; continue; 
            }
            
            $this->db->insert("t_locked",[ //lock record to avoid override from users
				'f_code'=>$f_code,'f_user_id'=>$uid,'f_timestamp'=>time()
			]);
            
            $creation = [];$defTable = [];$custom = [];
            foreach($resValueToRestore as $key => $value) {
                if(in_array($key,$creationTable)){
                    $creation[$key] = $value;
                }elseif(in_array($key,$defaultTable)){
                    $defTable[$key] = $value;
                }elseif(in_array($key,$customTable)){
                    $custom[$key] = $value;
                }
            }            
            //add user info
            $creation['fc_editor_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;            
            $creation['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
            $creation['fc_editor_user_gender'] = $userinfo->f_gender;
            $creation['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
            $creation['fc_editor_user_mail'] = $userinfo->fc_usr_mail;
			$defTable['f_user_id'] = $userinfo->f_id;
            //set time info
            $creation['f_timestamp'] = $time;
            $defTable['f_timestamp'] = $time;
            $custom['f_timestamp'] = $time;
            //remove f_id and set f_code
            unset($creation['f_id']);unset($creation['f_code']);
            unset($defTable['f_id']);unset($custom['f_id']);
            $defTable['f_code'] = $f_code;$custom['f_code'] = $f_code;            
            //update row and write new history
            $this->db->update("t_creation_date",$creation,"f_id = $f_code");
            $this->db->update($table,$defTable,"f_code = $f_code");
            $this->db->update("t_custom_fields",$custom,"f_code = $f_code");            
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", $creation, 'Element modified with rollback action from moduleImporter system');
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, $table, $defTable);
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", $custom);            
			$this->db->delete("t_locked","f_code = {$f_code} and f_user_id = {$userinfo->f_id}");
        }
        return $lineMissed;        
    }    
    
    private function createFather($type_id,$table)
    {       
        $time = Zend_Registry::get('time');
        $custom_code = '';        
        if($table == 't_wares') { $custom_code = 'fc_imp_code_wares_excel'; }
        elseif($table == 't_workorders') { $custom_code = 'fc_imp_code_wo_excel'; }
        elseif($table == 't_selectors') { $custom_code = 'fc_imp_code_sel_excel'; }
        else { return; }
        $table_parent = $table."_parent";        
        $select = new Zend_Db_Select($this->db);         
        $rows = $select->from(array("t1"=>"$table"),array('f_code',$custom_code))  
            ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array("fc_imp_parent_code"))                    
            ->where("f_type_id = $type_id")->where("t1.f_timestamp = ?",$time)->query()->fetchAll();            
             
        $tot_a = count($rows);
        for($a = 0;$a < $tot_a;++$a) {
            $row = $rows[$a];
            $f_parent_code_excel = $row['fc_imp_parent_code'];
            //cerco f_code originale in custom_fields
            $f_code = $row['f_code'];
            if(!empty($f_parent_code_excel)) {
                $parent_array = "";
                if(strpos($f_parent_code_excel,",")!== false) {
                    $parent_array = "'".str_replace(",","','",$f_parent_code_excel)."'";
                }
                else {
                    $parent_array = "'".str_replace(".","','",$f_parent_code_excel)."'";
                }
                $select->reset();
                $res_custom = $select->from($table,"f_code")->where("$custom_code IN ($parent_array)")
                    ->where("f_type_id = ?",$type_id)->query()->fetchAll();
                $tot_rc = count($res_custom);
                if($tot_rc > 0){//if not empty, delete default father and add new
                    $this->db->delete($table_parent,"f_code = $f_code");
                }
                for($rc = 0;$rc < $tot_rc;++$rc) {
                    $parent = array(
                        'f_parent_code'=>$res_custom[$rc]['f_code'],
                        'f_code'=>$f_code,
                        'f_active'=>1,
                        'f_timestamp'=>time()
                    );
                    $this->db->insert($table_parent,$parent);
                }
            }
        }
    }
    
    /**
     * exec check inside ui object
     * @param type $v value
     * @param type $c condition
     * @return int 1 = ok, 0 = ko
     */
    private function check($v,$c,$col = '',$table = '',$f_type_id = 0,$data = array())
    {           
        $risp=1; 
        $arrc=explode(" ",$c);  
        $na=count($arrc);   // ,cc,pp,app,ss,s,pi,pf,vl;

        $vl=strlen($v); 

        for($r=0;$r<$na;$r++){
            $cc=$arrc[$r]; 
            if($cc=="mail")  $risp=$this->checkMail($v);
            if($cc=="int")   $risp=$this->checkInt($v);
            if($cc=="unique")   $risp=$this->checkUnique($v,$col,$table,$data,$f_type_id);

            $app=explode("_",$cc); 
            //if value exist, check condition
            if($vl){
                if(substr($cc,0,5)=="nchar") {
                    if(count($app)>2) {
                        $pi=(int) ($app[1]);
                        $pf=(int) ($app[2]);
                        if($vl<$pi || $vl>$pf) $risp=0;
                    } else {
                        if($vl != ((int) ($app[1])) ) $risp=0;
                    } 
                }

                if(substr($cc,0,3)=="not") {
                    $ss=$app[1];
                    if(count($app)>2)  $ss.=$app[2]+"_";
                    for($n=0;$n<strlen($ss);$n++) {
                        $s=substr($ss,$n,1); 
                        if(strpos($v,$s)!==false) {
                            $risp=0;
                            break;
                        }
                    }
                }

                if(substr($cc,0,5)=="range") {
                    if(!is_numeric($v)) $risp=0;
                    else {
                        $v=(double) $v;

                        if($app[1]=="m" && $app[2]=="M") {  }
                        else {
                            $i=$app[1]; $f=$app[2]; $mM=1;

                            if($i=="m") { $f=(double) $f;  
                                if($v>$f) $risp=0;
                                $mM=0;
                            } else if($f=="M") { $i=(double) $i;
                                if($v<$i) $risp=0; 
                                $mM=0; 
                            }

                            if($mM) { $f=(double) $f; $i=(double) $i;
                                if($v<$i || $v>$f) $risp=0;  }
                        }
                    }
                }

                if( substr($cc,0,7)=="decimal") {   
                    if(!is_numeric($v)) $risp=0;
                    else { 
                        $pp=explode(".",$v);
                        $s=count($pp);                            
                        if($s && strlen($pp[1])>$app[1]) $risp=0; 
                    }
                }
                if(strpos($cc,"regexp")!== false) {
                    $risp = 0;
                    if(preg_match_all($app[1],$v)) {
                        $risp = 1;
                    }
                }
            } 
            if(!$risp) return 0;
        } //
        return 1;
    }

    /**
     * Check if passed keys are valid
     * @param array $keys     
     * @param string $table
     * @param int $type_id
     * @param int $checkValue
     * @param array $dataList
     */
    private function checkUniqueKey($data,$keys,$table,$type_id,$checkValue)
    {
        //if insert, set as keys import code
        if($checkValue == 0){ $keys = array($this->impField); }
        //if empty are not unique        
        if(empty($keys)) throw new Exception('Keys cannot be empty');
        $errorKeys = array();
        
        $keysList = array_map(function($el) use($keys){
            return implode('|',array_map(function($el2) use($el){
              return $el[$el2];  
            },$keys));            
        },$data);
        //check if f_code are uniques        
        $valueCheckList = array_count_values($keysList);
        $filteredvalue = array_filter($valueCheckList, function($el){
            if($el > 1) return $el;
        });        
        if(!empty($filteredvalue)) {
            return array_keys($filteredvalue);
        }        
        $tot = count($data);        
        for($i = 0;$i < $tot;++$i){    
            //check multiple occurrence on db            
            $unique = $this->checkUnique($data[$i],$keys, $table, $type_id);
            if($checkValue < 2 && $unique>$checkValue )  {//rollback not included
                $errorKeys[] = $keysList[$i];
            }
        }
        if(!empty($errorKeys)) {
            return $errorKeys;            
        }
    }

    /**
     * check if passed value is an int
     * @param type $v = value
     * @return int 1 = ok, 0= ko
     */
    private function checkInt($v)
    {
        if($v=="" || is_long($v)) return 1;
        $rsp=1; 
        for($r=0;$r<strlen($v);$r++){
            $c=substr($v,$r,1); $o=ord($c);
            if($o<48 || $o>57) return 0;
        }
        return 1;
    }
    
    /**
     * check if the value is unique inside the system
     * @param type $v value to check
     * @param type $col colum to check
     * @param type $table table to check
     * @param type $f_type_id other data passed from excel
     */
    private function checkUnique($v,$col,$table,$f_type_id)
    {
        $select = new Zend_Db_Select($this->db);        
        //now check inside the system		
        $select->from(["t1"=>$table],['num'=>'count(*)'])
            ->join(["t2"=>"t_custom_fields"],"t1.f_code = t2.f_code",[])
            ->join(["t3"=>"t_creation_date"],"t1.f_code = t3.f_id",[])
            ->where("f_type_id = ?",$f_type_id);
        $t3 = ['f_id','f_timestamp']; $t2 = ['f_code'];
        $tot = count($col);
        for($i = 0;$i < $tot;++$i){
            $prefix = in_array($col[$i],$t3)?"t3.":(in_array($col[$i],$t2)?"t2.":"");
            $select->where("{$prefix}{$col[$i]} = ?",$v[$col[$i]]);        
        }             
        $res_unique = $select->query()->fetch();        
        return $res_unique['num'];
    }

    /**
     * check if is a valid mail
     * @param type $email 
     * @return boolean 
     */
    private function checkMail($email) {
        // First, we check that there's one @ symbol, and that the lengths are right
        if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
            // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
            return false;
        }
        // Split it into sections to make life easier
        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
                return false;
            }
        }
        if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
            $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
                if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
                    return false;
                }
            }
        }
        return true;
    } 
    
    function rowIteratorImport($rowIterator,&$keys,$get_keys = false)
    {
        $res = array();  		
        foreach($rowIterator as $row){                
            $i = 0;
            $cellIterator = $row->getCellIterator();            
            $cellIterator->setIterateOnlyExistingCells(false);                         
            $rowIndex = $row->getRowIndex ();                
            
            $array = array();
            /**
             *@var PHPExcel_Cell $cell 
             */
            foreach ($cellIterator as  $cell) {              
                if($rowIndex == 1 && $get_keys) {                          
                    $key = trim(preg_replace('/\r|\n/',' ', $cell->getCalculatedValue()));                     
                    if(strlen($key)){ $keys[] = $key; }
                }        
                else {                    
                    if(isset($keys[$i])){                    
                        if($cell->getDataType() != 'f' && $cell->getDataType() != 'n') {
                            $cell->setDataType(PHPExcel_Cell_DataType::TYPE_STRING); 
                        }
                        
                        if($cell->getDataType() != 'n') {
                            $array[$keys[$i]] = $cell->getCalculatedValue();                            
                        }
                        else {    
                            if(PHPExcel_Shared_Date::isDateTime($cell)) {                                 
                                 $array[$keys[$i]] = PHPExcel_Shared_Date::ExcelToPHP($cell->getValue());//PHPExcel_Style_NumberFormat::toFormattedString($cell->getValue(), "M/D/YYYY");                                 
                            }
                            else {
                                $array[$keys[$i]] = $cell->getCalculatedValue();                                
                            }                            
                        }                        
                    }
                    $i++;
                    if($i > count($keys)) break;                    
                }
            }      
            //check if is the header
            if($rowIndex == 1) continue;   
            elseif($array[$keys[0]] == '' || is_null($keys[0])) continue;
            
            $res[] = $array;        
        }
        
        return $res;
    }

    private function IterateExcel($filename,$position,$inc,$sheet = false)
    {
        $ext = explode('.',$filename);
        $ext = array_pop($ext);
        
        if($ext == 'xls') {            
            $excelReader = new PHPExcel_Reader_Excel5();    
        }
        else {            
            $excelReader = new PHPExcel_Reader_Excel2007();
        }
        $excelReader->setReadDataOnly(false);
        $chunkFilter = new Mainsim_Model_ChunkReadFilter();
        $excelReader->setReadFilter($chunkFilter);    
        $chunkFilter->setRows($position, $inc);
        if($sheet !== false) {
            $excelReader->setLoadSheetsOnly($sheet);
        }
        $file = $excelReader->load($filename);
        return $file;
    }
    
    private function numRows($filename,$position)
    {
        $ext = explode('.',$filename);
        $ext = array_pop($ext);
        
        if($ext == 'xls') {            
            $excelReader = new PHPExcel_Reader_Excel5();    
        }
        else {            
            $excelReader = new PHPExcel_Reader_Excel2007();
        }
        $excelReader->setReadDataOnly(true);        
        $file = $excelReader->load($filename);
        $num_rows = $file->setActiveSheetIndex($position)->getHighestRow();
        unset($file);unset($excelReader);
        return ($num_rows - 1) > $this->num_row_controll?false:true;
    }
}