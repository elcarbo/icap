<?php

class Mainsim_Model_OptionSelect
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));        
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function getOptions($scriptName)
    {
        if(file_exists(APPLICATION_PATH . '/../scripts/' . PROJECT_NAME . "/select." . $scriptName . ".php")){
            include(APPLICATION_PATH . '/../scripts/' . PROJECT_NAME . "/select." . $scriptName . ".php");
        }
        else{
            if(file_exists(APPLICATION_PATH . "/../scripts/default/select." . $scriptName . ".php")){
            include(APPLICATION_PATH . "/../scripts/default/select." . $scriptName . ".php");
            }
        }
        return array();
    }
    
    public function getGroupsExceptions()
    {
//        $select = new Zend_Db_Select($this->db);
//        $res = $select->from("t_groups_exceptions",array('value'=>'f_id','label'=>'f_name'))->query()->fetchAll();
//        if(count($res) == 1) $res[0]['selected'] = true;
//        return $res;
        return array(
            array("label" => "uno", "value" => 1, "selected" => true),
            array("label" => "due", "value" => 2, "selected" => false),
            array("label" => "tre", "value" => 3, "selected" => false)
        );
    }
    
    public function getDaysException($f_code)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from("t_days_exceptions")->where("f_code = ?", $f_code)->query()->fetchAll();
        return $res;
    }
    
    public function getSelectorTypes() {
        $q = new Zend_Db_Select($this->db);
        return $q->from("t_selectors_types", array("value" => "f_id", "label" => "f_type"))
        ->where("f_id > 0")->order("f_id ASC")->query()->fetchAll();
    }
    
    public function getWarehouses($f_code)
    {
//        $select = new Zend_Db_Select($this->db);
//        $res = $select->from("t_days_exceptions")->where("f_code = ?", $f_code)->query()->fetchAll();
//        return $res;
        return array(
            array("label" => "Select", "value" => "", "selected" => true),
            array("label" => "WH1", "value" => "wh1", "selected" => false),
            array("label" => "WH2", "value" => "wh2", "selected" => false),
            array("label" => "WH3", "value" => "wh3", "selected" => false),
            array("label" => "WH4", "value" => "wh4", "selected" => false),
            array("label" => "WH5", "value" => "wh5", "selected" => false),
            array("label" => "WH6", "value" => "wh6", "selected" => false),
            array("label" => "WH7", "value" => "wh7", "selected" => false),
            array("label" => "WH8", "value" => "wh8", "selected" => false)
        );
    }
    
    public function getDataByCategory($table, $f_type, $cond, $orderby, $order) {
        $q = new Zend_Db_Select($this->db);
        $q->from("t_creation_date")->join($table, "t_creation_date.f_id = $table.f_code")
        ->join("t_custom_fields", "$table.f_code = t_custom_fields.f_code")->where("$table.f_type_id = ?", $f_type);
        if($cond) $q->where($cond);
        $q->order("$orderby $order");
        return $q->query()->fetchAll();
    }
    
    public function getWoPriorities() {
        $options = array();
        $main = new Mainsim_Model_Mainpage();
        $priorities = $main->getPriorities();
        foreach($priorities as $k => $v) {
            $options[] = array(
                "label" => $v["label"],
                "value" => $k,
                "selected" => $v["selected"]
            );
        }
        return $options;
    }
}