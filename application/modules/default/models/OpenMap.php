<?php

class Mainsim_Model_OpenMap
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));        
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
  
    public function getAssets() {
        
        $selectAssets = $this->db->select()
                                 ->from(array('tw' => 't_wares'), array('code' => 'f_code', 'title' => 'f_title', 'description' => 'tw.f_description', 'code' => 'f_code'))
                                 ->join(array('tcf' => 't_custom_fields'), 'tw.f_code = tcf.f_code', array('lat' => 'tcf.fc_latitude', 'lng' =>  'tcf.fc_longitude', 'icon' => 'tcf.fc_icon'))
                                 ->where('tw.f_type_id = 1')                                 
                                 ->where('tcf.fc_latitude <> 0')
                                 ->where('tcf.fc_longitude <> 0');
        $result = $this->db->query($selectAssets)->fetchAll();
        
        // retrieve active workorders associated to asset 'i'
        for($i = 0; $i < count($result); $i++){
            $selectWorkOrders = $this->db->select()
                                         ->from(array('tw' => 't_workorders'), array('code' => 'f_code', 'title' => 'f_title', 'description' => 'f_description', 'type' => 'f_type_id', 'priority' => 'f_priority'))
                                         ->join(array('tww' => 't_ware_wo'), 'tw.f_code = tww.f_wo_id')                                         
                                         ->where('tww.f_ware_id = ?', $result[$i]['code']);
            $aux = $this->db->query($selectWorkOrders);
            $result[$i]['work_orders'] = $aux;
        }
        return $result;
    }
    
    public function getWorkorders() {
        
        $selectWorkOrders = $this->db->select()
                                     ->from(array('tw' => 't_workorders'), array('id' => 'f_code', 'priority' => 'f_priority', 'title' => 'f_title', 'description' => 'f_description', 'categoryId' => 'f_category'))
                                     ->join(array('tcf' => 't_custom_fields'), 'tw.f_code = tcf.f_code', array('lat' => 'fc_latitude', 'lng' => 'fc_longitude', 'code' => 'fc_progress', 'icon' =>'fc_icon'))
                                     ->joinLeft(array('twt' => 't_workorders_types'), 'twt.f_id = tw.f_category', array('category' => 'f_type'))
                                     ->joinLeft(array('twp' => 't_wf_phases'), 'tw.f_phase_id = twp.f_number AND tw.f_wf_id = twp.f_wf_id', array('phase' => 'f_name'))                                     
                                     ->where('tw.f_type_id IN (1, 4, 6, 7, 10, 13)');
        $aux = $this->db->query($selectWorkOrders)->fetchAll();
        $n = 0;
        for($i = 0; $i < count($aux); $i++){
            // workorder without coordinates --> there are assets associated
            if($aux[$i]['lat'] == 0 && $aux[$i]['lng'] == 0){
               $selectAssets = $this->db->select()
                                        ->from(array('tww' => 't_ware_wo'), array('id' => 'f_ware_id'))
                                        ->join(array('tcf' => 't_custom_fields'), 'tww.f_ware_id = tcf.f_code', array('lat' => 'tcf.fc_latitude', 'lng' => 'tcf.fc_longitude'))
                                        ->where('tww.f_wo_id= ?', $aux[$i]['id'])
                                        ->where('tcf.fc_longitude <> 0')
                                        ->where('tcf.fc_latitude <> 0')                                        
                                        ->limit(1);
               $aux2 = $this->db->query($selectAssets)->fetchAll();
               if(count($aux2) > 0){
                   $result[$n] = $aux[$i];
                   $result[$n]['assets'] = true;
                   $result[$n]['lat'] = $aux2[0]['lat'];
                   $result[$n]['lng'] = $aux2[0]['lng'];
                   $n++;
               }
            }
            else{
                $result[$n] = $aux[$i];
                $result[$n]['assets'] = false;
                $n++;
            }
        }
        return $result;
    }
    
    public function getWorkorderAssets($id) {
        
        $select = $this->db->select()
                           ->from(array('tw' => 't_wares'), array('id' => 'f_code', 'title' => 'f_title', 'description' => 'f_description'))
                           ->join(array('tww' => 't_ware_wo'), 'tw.f_code = tww.f_ware_id')
                           ->join(array('tcf' => 't_custom_fields'), 'tw.f_code = tcf.f_code', array('lat' => 'fc_latitude', 'lng' => 'fc_longitude', 'icon' => 'fc_icon'))
                           ->join(array('two' => 't_workorders'), 'two.f_code = tww.f_wo_id', array('priority' => 'f_priority'))
                           ->where('tcf.fc_latitude <> 0 ')
                           ->where('tcf.fc_longitude <> 0')
                           ->where('tww.f_wo_id = ?', $id);
        $result = $this->db->query($select)->fetchAll();
        return $result;
    }
    
    public function searchLocation($location) {
        //$r = new HttpRequest('http://nominatim.openstreetmap.org/search.php?format=json&q=' . urlencode($location), HTTP_METH_GET);
        //$r->send();
        return file_get_contents('http://nominatim.openstreetmap.org/search.php?format=json&q=' . urlencode($location));
    }
}