<?php

class Mainsim_Model_PMReader
{
    private $db;
    private $create_user_info = array();
    
    public function __construct($cli = false) {                
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));               
        //create default user        
        if(!$cli) {                        
            $this->create_user_info = Zend_Auth::getInstance()->getIdentity();
        }
        else {            
            $this->create_user_info = Mainsim_Model_Login::getUSerInfo(1, $this->db);  //INSERIRE CASO NO UTENTE F_ID 1              
        }        
        //Zend_session::writeClose();
    }
    
    /**
     * Periodic Maintenance Generator
     */
    public function readPeriodics($start = -1,$end = 0,$number = 15,$f_code = false,$f_type_id = 0, $id = null)
    {
        $select = new Zend_Db_Select($this->db);
        $objUtils = new Mainsim_Model_Utilities();
        $admusr = Mainsim_Model_Login::getUSerInfo(1, $this->db);
        $admusrinfo = array(
            "fc_editor_user_name"=>$admusr->fc_usr_firstname . ' ' . $admusr->fc_usr_lastname
            ,"fc_editor_user_avatar"=>$admusr->fc_usr_avatar,"fc_editor_user_gender" => $admusr->f_gender
            ,"fc_editor_user_phone" => $admusr->fc_usr_phone,"fc_editor_user_mail" => $admusr->fc_usr_mail
        );
        $periodics_closing_groups = [6];
        $addingGroups = $objUtils->getSettings('periodics_floating_groups');
        $addingGroups = explode(",", $addingGroups);
        $periodics_closing_groups = array_unique(array_merge($periodics_closing_groups,$addingGroups));
        
        $days_before = 1;
        $generated = 0;
		
        if($id){
            $select->reset();
            $select->from(array("t1"=>"t_periodics"))
                   ->join(array("t2"=>"t_workorders"), 't1.f_code = t2.f_code', array('f_type_id','fc_pm_type','fc_pm_fixed'))
                   ->join(array('t3'=>"t_creation_date"), 't3.f_id = t2.f_code', array('f_phase_id'))
                   ->join('t_wf_phases', 't3.f_wf_id = t_wf_phases.f_wf_id AND t3.f_phase_id = t_wf_phases.f_number', array())
                   ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                   ->where('t1.f_id = ' . $id)
                   ->where('t_wf_groups.f_id NOT IN (6,7)')
                   ->where('f_executed = 0');
        }
        else{
            //get delay (in days)
            $res_delay = $select->from("t_creation_date",array('f_days_before_check'=>"f_description"))
                    ->where("f_category = 'SETTING'")->where("f_title = 'DAYS_BEFORE_CHECK'")
                    ->query()->fetch();
            $days_before = $res_delay && $res_delay['f_days_before_check'] > 0?$res_delay['f_days_before_check']:1;

            //convert delay in second
            $days_before = $days_before*86400;
            //-----
            $select->reset();
            $start = $start == -1?time() - $days_before:$start; 
            $end = $end==0?time() + 400:$end;
            $select->from(array("t1"=>"t_periodics"))
                    ->join(array("t2"=>"t_workorders"),"t1.f_code = t2.f_code",array('f_type_id', 'fc_pm_type','fc_pm_fixed'))
                    ->join(array('t3'=>"t_creation_date"),"t2.f_code = t3.f_id",array('f_phase_id'))
                    ->where("t3.f_phase_id IN (1,5,6)")->where("t2.f_type_id IN (3,11,19)")->where("f_executed = 0");        
            if($number) { $select->limit($number);}
            if($f_type_id) {$select->where("t2.f_type_id = ?",$f_type_id);}
            //if method called from opportunistic wo
            $select->where("t1.f_executed = 0 AND t1.f_start_date - t1.f_forewarning > $start")
            ->order('t2.f_priority DESC')->order('(t1.f_start_date - t1.f_forewarning) ASC');
            if($f_code) {
                    $select->where("t1.f_code = ?",$f_code);
            }
            else {
                    $select->where("t1.f_start_date - t1.f_forewarning < $end");
            }
        }
        //die($select->__toString());
        $resToGen = $select->query()->fetchAll();
		//print_r($resToGen);
        
        //se è una PM per asset, verifico se ci sono altre pm da generare lo stesso momento ma per diversi asset, tranne che per le force su singolo asset
        if($f_code && $resToGen && $resToGen[0]['fc_pm_type'] == 'pm-per-asset' && $id == null){
            $select->reset(Zend_Db_Select::WHERE)
                ->reset(Zend_Db_Select::LIMIT_COUNT)
                ->reset(Zend_Db_Select::LIMIT_OFFSET);
            $select->where("t3.f_phase_id IN (1,5)",['f_phase_id'])
              ->where("t2.f_type_id IN (3,11,19)")
              ->where("f_executed = 0")
              ->where("t1.f_start_date = ?", $resToGen[0]['f_start_date'])
              ->where("t1.f_code = ?",$f_code);
            $resToGen = $select->query()->fetchAll();
			
        }
        $woCreator = new Mainsim_Model_Workorders($this->db);
        $finalToGen = array();
        
        foreach($resToGen as $line) {
            //se il generatore è in fase 'Attivo' o 'In Corso'
            if(in_array($line['f_phase_id'], [1,6])) {
                $select->reset();
                //aggiungo il gruppo Deleting(7) 
                $close_groups = array_merge([7],$periodics_closing_groups);
                $resNotClosedWo = $select->from(['t1' => 't_workorders'], ['f_code'])
                    ->join(['t2' => 't_creation_date'], 't2.f_id = t1.f_code ', [])
                    ->join(['t3' => 't_wf_phases'], 't3.f_wf_id = t2.f_wf_id and t3.f_number = t2.f_phase_id', [])
                    ->where('t1.f_code_periodic = ?', $line['f_code'])
                    ->where('t1.f_type_id IN (4,5,7)')
                    ->where('t3.f_group_id NOT IN ('. implode(',',$close_groups).')')
                    ->query()->fetch();
                //se ci sono odl generati non chiusi sposta in fase 'Bloccato'
                if(!empty($resNotClosedWo)){
                    $line['f_phase_id'] = 5;
                    //cambio fase e inserisco il log
                    $timestamp = time();
                    $updt = array_merge($admusrinfo,["f_timestamp" => $timestamp, "f_phase_id" => 5]);
                    $this->db->update('t_creation_date', $updt, "f_id = {$line['f_code']}");
                    $objUtils->newRecord($this->db, $line['f_code'], "t_creation_date", array("f_timestamp" => $timestamp), "PMReader",$timestamp);
                    $objUtils->newRecord($this->db, $line['f_code'], "t_workorders", array("f_timestamp" => $timestamp), "PMReader",$timestamp);
                    $objUtils->newRecord($this->db, $line['f_code'], "t_custom_fields", array("f_timestamp" => $timestamp), "PMReader",$timestamp);
                }
            }
            
            //se il generatore è in fase 'Bloccato'
            if($line['f_phase_id'] == 5){ 
                $this->db->update("t_periodics",array("f_executed"=>2),"f_id = '{$line['f_id']}'");
                $woCreator->addNextDueDate($line['f_code']);
            }
            
            array_push($finalToGen, $line);
        }

        //filtro le periodiche di generatori non bloccati
        $finalToGen = array_filter($finalToGen, function ($v){ 
             return $v['f_phase_id'] == 1 || $v['f_phase_id'] == 6;
        });        
        //re-indicizzazione dell'array ( 0,1,2 ...)
        $finalToGen = array_values($finalToGen);
        
        // 1: check if father have an engagement and in this case the children cannot be executed
        $select->reset();
        $select->from(array("t1"=>"t_workorders_parent"),array("num"=>"count(t1.f_code)"))
            ->join(array("t2"=>"t_periodics"),"t1.f_parent_code = t2.f_code",array());
        $tot = count($finalToGen);

        for($i = 0;$i < $tot;++$i) {
            $line = $finalToGen[$i];
            $select->reset(Zend_Db_Select::WHERE);             
            $res_father = $select->where("t1.f_code = ?",$line['f_code'])->query()->fetch();            
            if($res_father['num'] > 0){
                continue;
            }            
            $res = $woCreator->initializeWoGeneration($line,$this->create_user_info);

            if(is_int($res)){
                $generated +=$res;
                
                $this->db->update("t_periodics",array("f_executed"=>1),"f_id = '{$line['f_id']}'");
                //and increment next due date and subsequent due date
                $woCreator->addNextDueDate($line['f_code']);
                
                //se in fase 'Attivo' setto il generatore in fase 'in 'In Corso' +log
                if($line['f_phase_id'] == 1){
                    $timestamp = time();
                    $updt = array_merge($admusrinfo,["f_timestamp" => $timestamp, "f_phase_id" => 6]);
                    $this->db->update('t_creation_date', $updt, "f_id = {$line['f_code']}");
                    $objUtils->newRecord($this->db, $line['f_code'], "t_creation_date", array("f_timestamp" => $timestamp), "PMReader",$timestamp);
                    $objUtils->newRecord($this->db, $line['f_code'], "t_workorders", array("f_timestamp" => $timestamp), "PMReader",$timestamp);
                    $objUtils->newRecord($this->db, $line['f_code'], "t_custom_fields", array("f_timestamp" => $timestamp), "PMReader",$timestamp);
                    //$this->db->update('t_creation_date', ['f_phase_id' =>6], "f_id = {$line['f_code']}");
                }
            }
            elseif(isset($res['message'])){
                for($j = $i;$j < $tot;++$j) {
                    $this->db->update("t_periodics",array("f_executed"=>0),"f_id = '{$finalToGen[$j]['f_id']}'");
                    //$this->db->update('t_creation_date', ['f_phase_id' =>$finalToGen[$j]['f_phase_id']], "f_id = {$finalToGen[$j]['f_code']}");
                }
                $generated = $res;
                $this->db->insert("t_logs",array("f_log"=>'Error during periodic generating at '.date('m-d-Y H:i:s')." MESSAGE: ".$res['message'],'f_type_log'=>1,'f_timestamp'=>time()));
                break;
            }
        }
        
        
        $this->db->insert("t_logs",array("f_log"=>count($finalToGen).' periodic maintenances has been generated at '.date('m-d-Y H:i:s'),'f_type_log'=>1,'f_timestamp'=>time()));
        if($id){
            return count($finalToGen);
        }
        else{
            //controllo se ci sono periodiche con f_executed 4 da generare
            $select->reset();
            $select->from(array("t1"=>"t_periodics"))
                    ->join(array("t2"=>"t_workorders"),"t2.f_code = t1.f_code",array('f_type_id', 'fc_pm_type','fc_pm_fixed','fc_pm_cyclic_number','fc_pm_cyclic_type'))
                    ->join(array('t3'=>"t_creation_date"),"t3.f_id = t2.f_code",array('f_phase_id'))
                    ->where("t3.f_phase_id IN (1,6)")->where("t2.f_type_id IN (3,11,19)")
                    ->where("f_executed = 4")->where("f_generated_code IS NULL");        
            if($number) { $select->limit($number);}
            if($f_type_id) {$select->where("t2.f_type_id = ?",$f_type_id);}
            //if method called from opportunistic wo
            $select->order('t2.f_priority DESC')->order('(t1.f_start_date - t1.f_forewarning) ASC');
            if($f_code) {
                    $select->where("t1.f_code = ?",$f_code);
            }
            else {
                    $select->where("t1.f_start_date - t1.f_forewarning < $end");
            }
            //die($select->__toString());
            $resToGen4 = $select->query()->fetchAll();

            $sch = new Mainsim_Model_Scheduler($this->db); $generated4 = 0;
            foreach($resToGen4 as $line) {
                $reslpm = $woCreator->getLastPm($line['f_code'], $line['f_asset_code'] , $line['f_start_date'], null, 'asc');
                unset($reslpm[0]); 
                
                //rimuovo periodiche con f_executed = 1 non generate
                $reslpm = array_filter($reslpm, function ($v){ 
                         return $v['f_executed'] != 1 || !empty($v['f_generated']);
                }); 
                
                $reslpm = array_values($reslpm);
                
                //se la periodica successiva a quella bloccata è la prossima da generare (f_executed = 0) e 
                //dista temporalmente di un periodo >= a quello scandito dal generatore genero la periodica bloccata
                if(empty($reslpm) || $reslpm[0]['f_executed'] == 0){
                    $from = strtotime('midnight', $line['f_start_date']);
                    $num_cyclic = $line['fc_pm_cyclic_number'];
                    $type_cyclic = $line['fc_pm_cyclic_type'];
                    $increment = $sch->increment($type_cyclic, $from,$num_cyclic);

                    if(empty($reslpm) || $increment <= $reslpm[0]['f_start_date']){
                        $res = $woCreator->initializeWoGeneration($line,$this->create_user_info);
                        if(is_int($res)){
                            $generated4 +=$res;
                        }
                    }
                }
            }
            
            $this->db->insert("t_logs",array("f_log"=>$generated4.' blocked periodic maintenances has been generated at '.date('m-d-Y H:i:s'),'f_type_log'=>1,'f_timestamp'=>time()));

            return $generated;
        }
    }  

	public function isMulti($codes){
		
		$select = new Zend_Db_Select($this->db);
		$select->from('t_workorders', array('f_code', 'fc_pm_type'))
			->where('f_code IN (' . $codes . ')');
		$res = $select->query()->fetchAll();
		for($i = 0; $i < count($res); $i++){
			$result[$res[$i]['f_code']] = $res[$i]['fc_pm_type'];
		}
		return $result;
	}
}