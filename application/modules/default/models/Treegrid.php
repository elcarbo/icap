<?php

 class Mainsim_Model_Treegrid {

     /**
      *
      * @var Zend_Db 
      */
     private $db, $tab_custom, $tab_users, $tab_creation, $db_config;

     public function __construct($db = null) {
         if ($db != null) {
             $this->db = $db; 
         } else {
             $this->db = Zend_Db::factory(Zend_Registry::get('db'));
         }
         $this->tab_custom = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
         $this->tab_users = Mainsim_Model_Utilities::get_tab_cols("t_users");
         $this->tab_creation = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
         $this->db_config = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOption('database');
     }

     public function __destruct() {
         $this->db->closeConnection();
     } 

     //******************************************
     // new selectors
     //******************************************
     public function useNewSelectors() {

         $select = new Zend_Db_Select($this->db);
         $select->from('t_creation_date', array('f_description'))
                 ->where("f_title = 'NEW_SELECTORS'");

         $res = $select->query()->fetchAll();
         if (isset($res[0]['f_description'])) {
             return (bool) $res[0]['f_description'];
         } else {
             return false;
         }
     }

     public function getCategoriesByModule($module, $what) {
         // format popup modules
         if (strpos($module, 'slc') !== FALSE) {
             $select = new Zend_Db_Select($this->db);
             $select->from("t_selectors_types", array('f_type'));
             $res = $select->query()->fetchAll();
         } else {
             $module = str_replace("_pp", "", $module);
             $select = new Zend_Db_Select($this->db);
             $select->from($what . "_types", array('f_type'))
                     ->where("f_module_name = '" . $module . "'");
             $res = $select->query()->fetchAll();
         }
         $result = array();
         for ($i = 0; $i < count($res); $i++) {
             $result[] = $res[$i]['f_type'];
         }
         return $result;
     }

     public function getCategoriesByTypeId($id, $what) {
         // format popup modules
//         if (strpos($what, 'selectors') !== FALSE) {
//             $select = new Zend_Db_Select($this->db);
//             $select->from("t_selectors_types", array('f_type'));
//             $res = $select->query()->fetchAll();
//         } else {

             $select = new Zend_Db_Select($this->db);
             $select->from($what . "_types", array('f_type'))
                     ->where("f_id in (" . $id . ")");
             $res = $select->query()->fetchAll();
//         }
         $result = array();
         for ($i = 0; $i < count($res); $i++) {
             $result[] = $res[$i]['f_type'];
         }
         return $result;
     }

     public function getCodes($params, $debug = false) {

        //print_R($params); die();
         // get user Selectors ordered by type
         $userSelectors = (!$params['_ignoreSelector']) ? $this->getUserSelectors($params['_selectors'], $debug) : null;
         
         // t_selectors --> parent_code = 0 
         if (strpos($params['_module'], 'slc') > 0 && $params['_parent'] == -1 && count($params['_filters']['_search']) == 0) {
             $parent = 0;
         }
         // if a search filter has passed by client the reasearch is extended to all items, not only to parent level
         else if (count($params['_filters']['_search']) > 0 || count($p['_filters']['_filter'])>0) {
             $parent = -1;
         } else if ($params['_parent'] == 0 && count($userSelectors['_all']['_flat']) > 0) {
             $parent = -1;
         }
         else {
             $parent = $params['_parent'];
         }

         // excpetion for selectors picklist (hide root element)
         //var_dump($params['_fromPicklist']);die();
         if(strpos($params['_module'], '_slc_pp_') !== false && $params['_fromPicklist'] && $params['_parent'] == -1 && count($params['_filters']['_search']) == 0){
            $select = new Zend_Db_Select($this->db);
            $select->from('t_creation_date', ['f_id'])
                ->where("f_category = '" . $params['_category'][0] . "'")
                ->where("f_phase_id = 1")
                ->where("f_type = 'SELECTORS'")
                ->limit(1);
            $res = $select->query()->fetch();
            $parent = $res['f_id'];
         }

         $selectorsData = $this->getItemsBySelectors($params['_category'], $userSelectors, $parent, $params['_filters'], $params['_summary'], $params['_self']);

         if($params['_summary']){
            return $selectorsData;
         }

         // computation of items visible to user by intersection of items grouped by selector categories
         if (count($params['_filters']['_search']) > 0 || $params['_hierarchy'] == 0) {
             $result = $this->computeVisibleItems2($selectorsData, $userSelectors, true, $params['_summary']);
         } else {
             $result = $this->computeVisibleItems2($selectorsData, $userSelectors, false, $params['_summary']);
         }
          //die(json_encode([$result,$params['_limit']]));
         // result offset
         if (isset($params['_limit'])) {
             return array_slice($result, $params['_limit'][0], $params['_limit'][1]);
         } else {
             return $result;
         }
     }

     public function getUserSelectors($ids = null, $debug = false,$ignore_selector=false) {

         
         
          // init return structure
         $result = array(
             '_all' => array(
                 '_category' => array(),
                 '_categories' => array(),
                 '_flat' => array(),
             ),
             '_user' => array(
                 '_category' => array(),
                 '_categories' => array(),
                 '_flat' => array(),
             ),
             '_client' => array(
                 '_category' => array(),
                 '_categories' => array(),
                 '_flat' => array(),
             )
         );
         
         if($ignore_selector)
             return $result;
         
         if ($_GET['ns']) {
             $userData = Zend_Auth::getInstance()->getIdentity();
             $f_code = $userData['f_code'];
         } else {
             $f_code = Zend_Auth::getInstance()->getIdentity()->f_id;
         }


         $userData = Zend_Auth::getInstance()->getIdentity();

         $select = new Zend_Db_Select($this->db);

        
         // get user selectors
         $select->from('t_selector_ware', array())
                 ->join('t_creation_date', 't_selector_ware.f_selector_id = t_creation_date.f_id', array('f_id', 'f_category'))
                 ->where('f_ware_id = ' . $f_code);
         $resUserSelectors = $select->query()->fetchAll();
        
         for ($i = 0; $i < count($resUserSelectors); $i++) {
             $result['_all']['_category'][$resUserSelectors[$i]['f_category']][] = $resUserSelectors[$i]['f_id'];
             $result['_user']['_category'][$resUserSelectors[$i]['f_category']][] = $resUserSelectors[$i]['f_id'];
             if (!in_array($resUserSelectors[$i]['f_category'], $result['_all']['_categories'])) {
                 $result['_all']['_categories'][] = $resUserSelectors[$i]['f_category'];
             }
             if (!in_array($resUserSelectors[$i]['f_category'], $result['_user']['_categories'])) {
                 $result['_user']['_categories'][] = $resUserSelectors[$i]['f_category'];
             }
             $result['_all']['_flat'][] = $resUserSelectors[$i]['f_id'];
             $result['_user']['_flat'][] = $resUserSelectors[$i]['f_id'];
             $result['_user']['_category_selector'][] = $resUserSelectors[$i]['f_category'] . ":" . $resUserSelectors[$i]['f_id'];
             $result['_all']['_category_selector'][] = $resUserSelectors[$i]['f_category'] . ":" . $resUserSelectors[$i]['f_id'];
         }

         // get client selectors
         if (is_array($ids) && count($ids) > 0) {
             
             $select->reset();
             $select->from('t_creation_date', array('f_id', 'f_category'))
                     ->where('f_id IN (' . implode(",", $ids) . ')');
             $resClientSelectors = $select->query()->fetchAll();

             for ($i = 0; $i < count($resClientSelectors); $i++) {
                 if (!is_array($result['_all']['_category'][$resClientSelectors[$i]['f_category']]) || !in_array($resClientSelectors[$i]['f_id'], $result['_all']['_category'][$resClientSelectors[$i]['f_category']])) {
                     $result['_all']['_category'][$resClientSelectors[$i]['f_category']][] = $resClientSelectors[$i]['f_id'];
                 }
                 if (!is_array($result['_client']['_category'][$resClientSelectors[$i]['f_category']]) || !in_array($resClientSelectors[$i]['f_id'], $result['_client']['_category'][$resClientSelectors[$i]['f_category']])) {
                     $result['_client']['_category'][$resClientSelectors[$i]['f_category']][] = $resClientSelectors[$i]['f_id'];
                 }
                 if (!is_array($result['_all']['_categories']) || !in_array($resClientSelectors[$i]['f_category'], $result['_all']['_categories'])) {
                     $result['_all']['_categories'][] = $resClientSelectors[$i]['f_category'];
                 }
                 if (!is_array($result['_client']['_categories']) || !in_array($resClientSelectors[$i]['f_category'], $result['_client']['_categories'])) {
                     $result['_client']['_categories'][] = $resClientSelectors[$i]['f_category'];
                 }
                 if (!is_array($result['_all']['_flat']) || !in_array($resClientSelectors[$i]['f_id'], $result['_all']['_flat'])) {
                     $result['_all']['_flat'][] = $resClientSelectors[$i]['f_id'];
                 }
                 if (!is_array($result['_client']['_flat']) || !in_array($resClientSelectors[$i]['f_id'], $result['_client']['_flat'])) {
                     $result['_client']['_flat'][] = $resClientSelectors[$i]['f_id'];
                 }
                 $result['_client']['_category_selector'][] = $resClientSelectors[$i]['f_category'] . ":" . $resClientSelectors[$i]['f_id'];
                 $result['_all']['_category_selector'][] = $resClientSelectors[$i]['f_category'] . ":" . $resClientSelectors[$i]['f_id'];
             }
         }

         if ($debug) {
             $this->debug($result);
         }
         return $result;
     }

     private function getTablesInfo($category) {
         $select = new Zend_Db_Select($this->db);
         // ware types
         $select->from('t_wares_types', array('f_type'));
         $res = $select->query()->fetchAll();
         for ($i = 0; $i < count($res); $i++) {
             $wareTypes[] = $res[$i]['f_type'];
         }
         // workorder types
         $select->reset();
         $select->from('t_workorders_types', array('f_type'))
                 ->where("f_type <> 'DATA MANAGER'");
         $res = $select->query()->fetchAll();
         for ($i = 0; $i < count($res); $i++) {
             $workorderTypes[] = $res[$i]['f_type'];
         }
         // selector types
         $select->reset();
         $select->from('t_selectors_types', array('f_type'))
                 ->where("f_type <> 'DATA MANAGER'");
         $res = $select->query()->fetchAll();
         for ($i = 0; $i < count($res); $i++) {
             $selectorTypes[] = $res[$i]['f_type'];
         }
         // system types
         $select->reset();
         $select->from('t_systems_types', array('f_type'));
         $res = $select->query()->fetchAll();
         for ($i = 0; $i < count($res); $i++) {
             $systemTypes[] = $res[$i]['f_type'];
         }

         if (count(array_intersect($workorderTypes, $category)) > 0) {

             $result['_selectorTable'] = 't_selector_wo';
             $result['_itemTable'] = 't_workorders';
             $result['_parentTable'] = 't_workorders_parent';
             $result['_selectorField'] = 'f_wo_id';
         } else if (count(array_intersect($wareTypes, $category)) > 0) {
             $result['_selectorTable'] = 't_selector_ware';
             $result['_itemTable'] = 't_wares';
             $result['_parentTable'] = 't_wares_parent';
             $result['_selectorField'] = 'f_ware_id';
         } else if (count(array_intersect($selectorTypes, $category)) > 0) {
             $result['_itemTable'] = 't_selectors';
             $result['_parentTable'] = 't_selectors_parent';
         } else if (count(array_intersect($systemTypes, $category)) > 0) {
             $result['_itemTable'] = 't_systems';
             $result['_parentTable'] = 't_systems_parent';
             $result['_selectorTable'] = 't_selector_system';
             $result['_selectorField'] = 'f_system_id';
         }
         return $result;
     }
     
     private function getTablesInfoByMainTable($main_table) {
         
         $table_reference_array=[
             't_workorders'=>[
             '_selectorTable' => 't_selector_wo',
             '_itemTable' => 't_workorders',
             '_parentTable' => 't_workorders_parent',
             '_selectorField' => 'f_wo_id'],
             
             't_wares'=>[
             '_selectorTable' => 't_selector_ware',
             '_itemTable' => 't_wares',
             '_parentTable' => 't_wares_parent',
             '_selectorField' => 'f_ware_id'],
             
             "t_systems"=>[
             '_selectorTable' => 't_selector_system',
             '_itemTable' => 't_systems',
             '_parentTable' => 't_systems_parent',
             '_selectorField' => 'f_system_id'],
             
             "t_selectors"=>[
             '_itemTable' => 't_selectors',
             '_parentTable' => 't_selectors_parent']];
             
         $result=$table_reference_array[$main_table];
         
         return $result;
     }


     private function addQueryFilters(&$select, $filters, $tablesInfo, $summary = false, $parent = -1) {

         $userInfo = Zend_Auth::getInstance()->getIdentity();
         //print_r($filters); die("filtro");
         // filter
         for ($i = 0; $i < count($filters['_filter']); $i++) {

             if ($filters['_filter'][$i]->f != null) {

                 $table = $this->getFieldTable($filters['_filter'][$i]->f, array('t_creation_date', $tablesInfo['_itemTable'], 't_selectors', 't_custom_fields', 't_users', 't_wf_phases'));

                 switch ($filters['_filter'][$i]->ty) {
                     // null-not null
                     case 2:
                         if ($filters['_filter'][$i]->v == 1) {
                             $select->where($table . "." . $filters['_filter'][$i]->f . " IS NULL");
                         } else {
                             $select->where($table . "." . $filters['_filter'][$i]->f . " IS NOT NULL");
                         }
                         break;
                     // field filter
                     case 4:
                         $select->where($table . "." . $filters['_filter'][$i]->f . "='" . $filters['_filter'][$i]->v . "'");
                         break;
                     // default
                     default:
                         switch ($filters['_filter'][$i]->v) {
                             case 0: // TODAY
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0) . " AND " . mktime(23, 59, 59));
                                 break;
                             case 1: // YESTERDAY
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, date('n'), date('j') - 1) . " AND " . mktime(23, 59, 59, date('n'), date('j') - 1));
                                 break;
                             case 2: //TOMORROW
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, date('n'), date('j') + 1) . " AND " . mktime(23, 59, 59, date('n'), date('j') + 1));
                                 break;
                             case 3: //THIS WEEK
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, date('n'), date('j') - date('N') + 1) . " AND " . mktime(23, 59, 59, date('n'), date('j') - date('N') + 7));
                                 break;
                             case 4: //LAST WEEK
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, date('n'), date('j') - date('N') + 1 - 7) . " AND " . mktime(23, 59, 59, date('n'), date('j') - date('N')));
                                 break;
                             case 5: //NEXT WEEK                        
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(23, 59, 59, date('n'), date('j') - date('N') + 8) . " AND " . mktime(0, 0, 0, date('n'), date('j') - date('N') + 14));
                                 break;
                             case 6: //THIS MONTH
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, date('n'), 1) . " AND " . mktime(0, 0, 0, date('n') + 1, 0));
                                 break;
                             case 7: //LAST MONTH
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, date('n') - 1, 1) . " AND " . mktime(23, 59, 59, date('n'), 0));
                                 break;
                             case 8: //NEXT MONTH
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, date('n') + 1, 1) . " AND " . mktime(23, 59, 59, date('n') + 2, 0));
                                 break;
                             case 9: //THIS YEAR
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, 1, 1, date('Y')) . " AND " . mktime(23, 59, 59, 12, 31, date('Y')));
                                 break;
                             case 10: //LAST YEAR
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . mktime(0, 0, 0, 1, 1, date('Y') - 1) . " AND " . mktime(23, 59, 59, 12, 31, date('Y') - 1));
                                 break;
                             case 11: //LAST 2 gg
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . (mktime(0, 0, 0)-(86400*2)) . " AND " . mktime(23, 59, 59));
                                 break;
                             case 12: //LAST 30 GG
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . (mktime(0, 0, 0)-(86400*30)) . " AND " . mktime(23, 59, 59));
                                 break;
                             case 13: //LAST 365 gg
                                 $select->where($table . "." . $filters['_filter'][$i]->f . " BETWEEN " . (mktime(0, 0, 0)-(86400*365)) . " AND " . mktime(23, 59, 59));
                                 break;
                         }
                         break;
                 }
             }
         }

         // where
         for ($i = 0; $i < count($filters['_and']); $i++) {

             if ($filters['_and'][$i]->f == 't_wf_phases.f_summary_name') {

                 if (strpos($filters['_and'][$i]->z, "inboxFromMe") !== false) {
                     $select->where('t_creation_date.f_creation_user =' . (is_object($userInfo) ? $userInfo->f_id : $userInfo['f_code']));
                 } elseif (strpos($filters['_and'][$i]->z, "inbox") !== false) {
                     $select->join('t_ware_wo', 't_ware_wo.f_wo_id = t_creation_date.f_id', array())
                     ->where('t_ware_wo.f_ware_id = ' . (is_object($userInfo) ? $userInfo->f_id : $userInfo['f_code']));
                 } else {
                     //IN ('Apertura','Closing','Presa in carico','Verifica conformita'','Non conforme')"
                     if($parent == -1 || $tablesInfo['_itemTable'] !== 't_workorders'){
                        $filters['_and'][$i]->z = ltrim($filters['_and'][$i]->z, "IN (");
                        $filters['_and'][$i]->z = rtrim($filters['_and'][$i]->z, ")");
                        #print $filters['_and'][$i]->z;
                        // escape characters 
                        $aux = explode(",", $filters['_and'][$i]->z);
                       # print_r($aux);exit;
                        for ($z = 0; $z < count($aux); $z++) {
                            $aux[$z] = "'".str_replace("'", "\'", (preg_replace("/^\'|\'$/","", $aux[$z])))."'";
                        }
                        $filter = "IN (" . implode(",", $aux) . ")";
                      #print $filter;
                        $select->where($filters['_and'][$i]->f . " " . $filter);
                        $phaseFilter = true;
                     }
                 }
             } else {
                 $select->where($filters['_and'][$i]->f . " " . $filters['_and'][$i]->z);
             }
         }
         // order
        if(!$summary){
            for ($i = 0; $i < count($filters['_order']); $i++) {
                if ($filters['_order'][0]->f != null) {
                    $table = $this->getFieldTable($filters['_order'][$i]->f, array('t_creation_date', $tablesInfo['_itemTable'], 't_selectors', 't_custom_fields', 't_users', 't_wf_phases'));
                    if($filters['_order'][$i]->f!='f_name'){
                    $select->order($table . "." . $filters['_order'][$i]->f . " " . $filters['_order'][$i]->m);
                    }else{
                    $select->order("t_wf_phases.f_order " . $filters['_order'][$i]->m); 
                    $select->columns("t_wf_phases.f_order");
                    }
                    // check that columns aren't already in select before add it
                    $columns = $select->getPart('columns');
                    $found = false;
                    for ($j = 0; $j < count($columns); $j++) {
                        if ($columns[$j][0] == $table && $columns[$j][1] == $filters['_order'][$i]->f) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $select->columns($table . "." . $filters['_order'][$i]->f);
                    }
                }
            }
            // default order
            if ((count($filters['_order']) == 1 && $filters['_order'][0]->f == '') || count($filters['_order']) == 0) {
                $select->order('t_creation_date.f_id DESC');
            }
        }

         // phase not found -> default 
         if (!$phaseFilter && !$summary) {
               if($tablesInfo['_itemTable'] !== 't_workorders' || $parent <= 0)
                    $select->where('t_wf_groups.f_id NOT IN (6,7)');
         }
         
        //se la query sta recuperando i figli di un workorders guarda se esiste il setting 'WO_SUMMARY_CHILDREN_GROUPS_FILTER'
        //e utilizzalo per stabilire quali gruppi di fase escludere dei figli
        if($tablesInfo['_itemTable'] == 't_workorders' &&  $parent > 0){
               $utls = new Mainsim_Model_Utilities();
               $wscnf = $utls->getSettings("WO_SUMMARY_CHILDREN_GROUPS_FILTER");
               if(!empty($wscnf)) $select->where('t_wf_groups.f_id NOT IN ('.$wscnf.')');
               else $select->where('t_wf_groups.f_id NOT IN (6,7)');
        }
           
         // search
         for ($i = 0; $i < count($filters['_search']); $i++) {

             $value = base64_decode($filters['_search'][$i]->v0);
             $value2 = isset($filters['_search'][$i]->v1)?base64_decode($filters['_search'][$i]->v1):NULL;
             $table = $this->getFieldTable($filters['_search'][$i]->f, array('t_creation_date', $tablesInfo['_itemTable'], 't_selectors', 't_custom_fields', 't_users','t_wf_phases'));
             /*
               if($filterParams->Search[$i]->f == 'fc_progress'){
               $value = ltrim($value, '0');
               } */

             if ($filters['_search'][$i]->type == 0) {

                 switch ($filters['_search'][$i]->Sel) {

                     // contain
                      case 0:
                         if($filters['_search'][$i]->Mm==1){
                             if(!Mainsim_Model_Utilities::isMysql()){
                                $resCollate = $this->db->query("SELECT cast(SERVERPROPERTY ('Collation') as varchar(50)) as coll")->fetch();
                                $collation = str_replace("_CI_", "_CS_", $resCollate['coll']);
                                $f = "$f COLLATE {$collation}";
                                $select->where($table . "." . $filters['_search'][$i]->f . " COLLATE {$collation} LIKE '%" . addslashes(utf8_encode($value)) . "%'");
                             } else 
                                $select->where("BINARY(".$table . "." . $filters['_search'][$i]->f . ") LIKE '%" . addslashes(utf8_encode($value)) . "%'");
                            
                         }else{
                         $select->where($table . "." . $filters['_search'][$i]->f . " LIKE '%" . addslashes(utf8_encode($value)) . "%'");
                         }
                         break;
                     // not contain
                     case 1:
                         $select->where($table . "." . $filters['_search'][$i]->f . " NOT LIKE '%" . addslashes(utf8_encode($value)) . "%'");
                         break;
                     // start with
                     case 2:
                         $select->where($table . "." . $filters['_search'][$i]->f . " LIKE '" . addslashes(utf8_encode($value)) . "%' ");
                         break;
                     // end width
                     case 3:
                         $select->where($table . "." . $filters['_search'][$i]->f . " LIKE '%" . addslashes(utf8_encode($value)) . "' ");
                         break;
                     // equal to
                     case 4:
                         $select->where($table . "." . $filters['_search'][$i]->f . " = '" . addslashes(utf8_encode($value)) . "'");
                         break;
                     // not equal to
                     case 5:
                         $select->where($table . "." . $filters['_search'][$i]->f . " <> '" . addslashes(utf8_encode($value)) . "'");
                         break;
                     // IS EMPTY
                     case 6:
                         $select->where($this->createEmptyWhere($table . "." . $filters['_search'][$i]->f, "''"));
                         break;                     
                     // IS NOT EMPTY
                     case 7:
                         $select->where($this->createEmptyWhere($table . "." . $filters['_search'][$i]->f, "''",false));
                         break;                      
                 }
             } else if ($filters['_search'][$i]->type == 1) {

                 switch ($filters['_search'][$i]->Sel) {
                     // equals
                     case 0:
                         $select->where($table . "." . $filters['_search'][$i]->f . " = " . addslashes(utf8_encode($value)));
                         break;
                     // not equals
                     case 1:
                         $select->where($table . "." . $filters['_search'][$i]->f . " <> " . utf8_encode($value));
                         break;
                     // greater than
                     case 2:
                         $select->where($table . "." . $filters['_search'][$i]->f . " > " . addslashes(utf8_encode($value)));
                         break;
                     // less than
                     case 3:
                         $select->where($table . "." . $filters['_search'][$i]->f . " < " . addslashes(utf8_encode($value)));
                         break;
                 }
             } else if ($filters['_search'][$i]->type == 3) {
                 // data
                 /*
                   if($typ==3 && isset($date[$sel])){
                   if($date[$sel]['cond'] == 'empty') {$asrc[] = $this->createEmptyWhere($f,0);}
                   elseif($date[$sel]['cond'] == 'notempty') {$asrc[] = $this->createEmptyWhere($f,0,false);}
                   elseif($date[$sel]['cond'] == 'between') {$asrc[]="$f between $v0 and $v1";}
                   else { $asrc[] = $this->db->quoteInto("$f {$date[$sel]['cond']} ? ",$v0);}
                   } */
                $date = [['cond' => '<'], ['cond' => '>'], ['cond' => '='], ['cond' => '!='], ['cond' => 'between'], ['cond' => 'empty'], ['cond' => 'notempty']];
                $f= $table . "." .$filters['_search'][$i]->f;
                $sel=$filters['_search'][$i]->Sel;
                       
                        
                 if (isset($date[$sel])) {
                 if ($date[$sel]['cond'] == 'empty') {
                      $select->where($this->createEmptyWhere($f, 0));
                 } elseif ($date[$sel]['cond'] == 'notempty') {
                      $select->where($this->createEmptyWhere($f, 0, false));
                 } elseif ($date[$sel]['cond'] == 'between') {
                      $select->where("$f between $value and $value2");
                 }elseif ($date[$sel]['cond'] == '=') {
                     $select->where("$f between ". mktime(0, 0, 0, date('n',$value), date('j',$value),date('Y',$value)) . " and " . mktime(0, 0, 0, date('n',$value), date('j',$value)+1,date('Y',$value)));
                 } else {
                      $select->where($this->db->quoteInto("$f {$date[$sel]['cond']} ? ", $value));
                 }
             }
            }
         }
//         echo $select;
//         $sql = $select->__toString();
//         var_dump($sql ); die;
         // like
         if (isset($filters['_like']->Fields) && count($filters['_like']->Fields) > 0) {
             $aux = base64_decode($filters['_like']->Text);
             $values = explode("||", $aux);
             for ($i = 0; $i < count($filters['_like']->Fields); $i++) {
                 $table = $this->getFieldTable($filters['_like']->Fields[$i], array('t_creation_date', $tablesInfo['_itemTable'], 't_selectors', 't_custom_fields', 't_users'));
                 for ($j = 0; $j < count($values); $j++) {
                     $likeQuery[] = $table . '.' . $filters['_like']->Fields[$i] . " LIKE '%" . $values[$j] . "%'";
                 }
             }
             $select->where(implode(" OR ", $likeQuery));
         }
         #echo $select;
     }

     public function getFieldTable($field, $tables) {

         // get onte time tables definition from db if not exist  
         for ($i = 0; $i < count($tables); $i++) {
             if (!isset($this->tableDefinition[$tables[$i]])) {

                 $tableDefinition = $this->db->describeTable($tables[$i]);
                 foreach ($tableDefinition as $key => $value) {
                     $this->tableDefinition[$tables[$i]][] = $value['COLUMN_NAME'];
                 }
             }
         }
         // serach fields in tables
         foreach ($this->tableDefinition as $key => $value) {
             if (in_array($field, $value)) {
                 return $key;
             }
         }
     }

     public function formatData($fields, $data, $params) {
        // count result
         if ($params->Prnt < 0) {
             $result['ntot'] = $data;
             $result['data'] = array();
         }
         // data result
         else {
             
             // count value
             $result['ntot'] = $params->rtree;
             // header row
             $result['data'][] = array_keys($fields['list']);

             // format data row by row
             //print_r($fields['list']);die();
             for ($i = 0; $i < count($data); $i++) {
                 
                 $row = [];
                 foreach($data[$i] as $k => $v){
                     if(in_array($k, array('f_visibility', 'f_editability','f_percentage','f_wf_id','f_type_id','f_priority'))){
                        $row[] = (int)$v;
                     }
                     else if(isset($fields['list'][$k])){
                         //print($fields['list'][$k][2] . "\n");
                         switch($fields['list'][$k][2]){
                            // string
                             case 0: 
                                 //print("string : " . $k . "\n");
                                 $row[] = (string)$v;
                                 break;
                             // int, date
                             case 1:
                             case 2:
                             case 3:
                                 //print("int : " . $k . "\n");
                                 $row[] = $v;
                                 break;
                             case 4:
                                 $row[] = $v;
                                 break;
                             default:
                                 $row[] = $v;
                                 break;
                         }
                     }
                     else{
                         //print("null : " . $k . "\n");
                         //if(in_array($k, array('fpdr','nch','selected','fpos','rtree'))){
                         if(in_array($k, array('nch','fpdr','selected','fpos','f_timestamp'))){
                             $row[] = (int)$v;
                         }
                         else{
                             $row[] = (string)$v;
                         }
                    
                    }
                    /*
                    if($k == 'nch'){
                        $row[] = (int)$v;
                    }
                    else{
                      $row[] = $v;
                    }*/
                 }
                 //$result['data'][] = array_values($data[$i]);
                 $result['data'][] = $row;  
             }
         }
         //print_r($result); die();
         // other values
         $result['other'] = array(
             'timestamp' => time(),
             'modify' => 0,
                 /* 'nch' => 1 */
         );

         return $result;
     }

     public function getFields($module, $tableType) {

         $obj = new Mainsim_Model_System();

         $res = $obj->getBookmark(str_replace("_pp", "", $module), 'Default', 0);
         $res = json_decode($res['view']['f_properties']);
         for ($i = 0; $i < count($res); $i++) {
             $table = $this->getFieldTable($res[$i][0], array('t_creation_date', $tableType, 't_custom_fields', 't_users', 't_wf_phases'));
             if (!$table) {
                 $table = 'virtual';
                 switch ($res[$i][0]) {
                     default:
                         $fields['query_list'][] = new Zend_Db_Expr("'placeholder' AS " . $res[$i][0]); //"'placeholder' AS " . $res[$i][0];
                         break;
                 }
                 if (!isset($fields[$table]) || !in_array($res[$i][0], $fields[$table])) {
                     $fields[$table][] = $res[$i][0];
                     $fields['list'][$res[$i][0]] = $res[$i];
                 }
             } else {
                 if (!isset($fields[$table]) || !in_array($res[$i][0], $fields[$table])) {
                     $fields[$table][] = $res[$i][0];
                     $fields['query_list'][] = $table . '.' . $res[$i][0];
                     $fields['list'][$res[$i][0]] = $res[$i];
                 }
             }
         }

         // add system fields
         $systemFields = array('f_visibility', 'f_editability', 'f_wf_id', 'f_type_id');
         for ($i = 0; $i < count($systemFields); $i++) {
             if (!in_array($systemFields[$i], $fields['list'])) {
                 if ($systemFields[$i] == 'f_type_id') {
                     $fields[$tableType][] = 'f_type_id';
                     $fields['query_list'][] = $tableType . '.f_type_id';
                 } else {
                     if (!in_array($systemFields[$i], $fields['t_creation_date'])) {
                         $fields['t_creation_date'][] = $systemFields[$i];
                         $fields['query_list'][] = 't_creation_date.' . $systemFields[$i];
                     }
                 }
                 if (!in_array($systemFields[$i], $fields['list'])) {
                     $fields['list'][$systemFields[$i]] = null;
                 }
             }
         }

         // add treegrid virtual fields $fields['list']
         $fields['list'] = array_merge(array("fpdr" => null, "nch" => null, "selected" => null, "fpos" => null, "rtree" => null), $fields['list']);

         $fields['query_list'][] = 't_wf_phases.f_percentage';
         $fields['list']['f_percentage'] = null;
         return $fields;
     }

     public function getData($codes, $fields, $params,$main_table=NULL) {

         $userInfo = Zend_Auth::getInstance()->getIdentity();
         
         //controllo da rimuuovere non appena si è certi che tutte le funzioni che chiamano getData abbiano i parametri corretti.
             
         if(!$main_table){
            $tablesInfo = $this->getTablesInfo($params['_category']);
         }
         else{
             $tablesInfo= $this->getTablesInfoByMainTable($main_table);
         }

         $selectData = new Zend_Db_Select($this->db);
         /* if($this->db_config['adapter'] != 'PDO_MYSQL'){
           $this->db->query('SET CHARACTER SET utf8');
           } */
         $selectData->from('t_creation_date', array())
                 ->join($tablesInfo['_itemTable'], $tablesInfo['_itemTable'] . '.f_code = t_creation_date.f_id', array())
                 ->join('t_custom_fields', 't_custom_fields.f_code = ' . $tablesInfo['_itemTable'] . '.f_code', array())
                 ->join('t_wf_phases', 't_wf_phases.f_number = t_creation_date.f_phase_id AND t_wf_phases.f_wf_id = t_creation_date.f_wf_id', array('f_editability AS wf_phases_editability'))
                 ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array());

         if (in_array('USER', $params['_category'])) {
             $selectData->join('t_users', 't_users.f_code = t_custom_fields.f_code', array());
         }

         if (isset($codes)) {
             $selectData->where('t_creation_date.f_id IN (' . implode(',', $codes) . ')');
         }
         $selectData->columns($fields['query_list']);
         if ($params['_filters'] != null) {
             $this->addQueryFilters($selectData, $params['_filters'], $tablesInfo,$params['_parent']);
         }
         $res = $selectData->query()->fetchAll();
         // get children
         if (count($codes) > 0) {

             $selectChildren = new Zend_Db_Select($this->db);
             $selectChildren->from($tablesInfo['_parentTable'], array('f_parent_code', 'f_code'))
                     ->join('t_creation_date', 't_creation_date.f_id = ' . $tablesInfo['_parentTable'] . '.f_code', array('fc_hierarchy_selectors'))
                     ->join($tablesInfo['_itemTable'], $tablesInfo['_itemTable'] . '.f_code = t_creation_date.f_id', array())
                     ->join('t_wf_phases', 't_wf_phases.f_number = t_creation_date.f_phase_id AND t_wf_phases.f_wf_id = t_creation_date.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->where('f_parent_code IN (' . implode(",", $codes) . ')');

             if (in_array('USER', $params['_category'])) {
                 $selectChildren->join('t_users', 't_users.f_code = t_creation_date.f_id', array());
             }

             // where
             for ($i = 0; $i < count($params['_filters']['_and']); $i++) {

                 if (strpos($params['_filters']['_and'][$i]->z, "inboxFromMe") !== false) {

                     $selectChildren->where('t_creation_date.f_creation_user =' . (is_object($userInfo) ? $userInfo->f_id : $userInfo['f_code']));
                 } elseif (strpos($params['_filters']['_and'][$i]->z, "inbox") !== false) {
                     $selectChildren->join('t_ware_wo', 't_ware_wo.f_wo_id = t_creation_date.f_id', array())
                             ->where('t_ware_wo.f_ware_id = ' . $userInfo->f_id);
                 } elseif ($params['_filters']['_and'][$i]->f == 't_wf_phases.f_summary_name') {
                     //Dava errore durante gli ezport con più di 1000 elementi. Comunque da verificare
//                     $params['_filters']['_and'][$i]->z = ltrim($params['_filters']['_and'][$i]->z, "IN ('");
//                     $params['_filters']['_and'][$i]->z = rtrim($params['_filters']['_and'][$i]->z, "')");
                     // escape characters 
                     
                     if($tablesInfo['_itemTable'] !== 't_workorders')
                     {
                         $aux = explode(",", $params['_filters']['_and'][$i]->z);
                        for ($z = 0; $z < count($aux); $z++) {
                            if(is_numeric($aux[$z])) {
                               $aux[$z] = $aux[$z];
                           } else {
                               $aux[$z] = "'".str_replace("'", "\'", (preg_replace("/^\'|\'$/","", $aux[$z])))."'";
                           }
                        }
                        $filter = "IN (" . implode(",", $aux) . ")";
                     }
                   }else {
                     $filter= $params['_filters']['_and'][$i]->z;
                   }
                    
                   if(!empty($filter)){
                    $selectChildren->where($params['_filters']['_and'][$i]->f . " " . $filter);
                    $phaseFilter = true;
                   }
                 
             }

             // phase not found -> default 
             if (!$phaseFilter) {
                if($tablesInfo['_itemTable'] != 't_workorders')
                    $selectChildren->where('t_wf_groups.f_id NOT IN (6,7)');
             }
             
            //la query sta recuperando i figli di un workorders perciò se esiste il setting 'WO_SUMMARY_CHILDREN_GROUPS_FILTER'
            //viene utilizzato per stabilire quali gruppi di fase escludere dei figli
           if($tablesInfo['_itemTable'] == 't_workorders'){
                  $utls = new Mainsim_Model_Utilities();
                  $wscnf = $utls->getSettings("WO_SUMMARY_CHILDREN_GROUPS_FILTER");
                  if(!empty($wscnf)) $selectChildren->where('t_wf_groups.f_id NOT IN ('.$wscnf.')');
                  else $selectChildren->where('t_wf_groups.f_id NOT IN (6,7)');
           }
               
//             die($selectChildren->__toString());
             $resChildren = $selectChildren->query()->fetchAll();

             $children = array();
             for ($i = 0; $i < count($resChildren); $i++) {
                 $children[$resChildren[$i]['f_parent_code']][$resChildren[$i]['f_code']] = array(
                     'fc_hierarchy_selectors' => $resChildren[$i]['fc_hierarchy_selectors'],
                     'fc_hierarchy_codes' => $resChildren[$i]['fc_hierarchy_codes']
                 );
             }
         } else {
             $resChildren = array();
         }

         $userSelectors = $this->getUserSelectors();

         for ($i = 0; $i < count($res); $i++) {
             // parent code (fpdr)
             $fpdr = $params['_parent'];

             //$nch = count($this->getCodes($res[$i]['f_code'], $selectorFilters, true));
             $nch = isset($children[$res[$i]['f_code']]) ? count($this->computeVisibleItems($children[$res[$i]['f_code']], $userSelectors)) : 0;
             // compute editabilities from user level, wo editability and workorder phase editability
             $userInfo = Zend_Auth::getInstance()->getIdentity();
             $res[$i]['f_editability'] = ((int) ($res[$i]['f_editability']) & (int) ($res[$i]['wf_phases_editability']) & (int) ($userInfo->f_level));
             unset($res[$i]['wf_phases_editability']);
             // selected
             $selected = 0;
             // position in treegrid block (fpos)
             $fpos = $params['_limit'][0] + $i;
             // rtree
             $rtree = $params['_rtree'];

             $res[$i] = array('fpdr' => $fpdr, 'nch' => (int) $nch, 'selected' => $selected, 'fpos' => $fpos, 'rtree' => $rtree) + $res[$i];
         }

         return $res;
     }

     public function computeVisibleItems2(&$selectorsData, $userSelectors, $search = false, $summary = false) {
        //print_r($selectorsData);die();
        if (!isset($selectorsData))
            return array();

        if (isset($userSelectors['_user']['_category_selector'])) {
            $skip = false;
            $allSelectors = array_flip($userSelectors['_all']['_category_selector']);
        } else {
            $skip = true;
        }
        $auxSelectorsData = $selectorsData;
        foreach ($selectorsData as $code => $value) {

           if (!$value['fc_hierarchy_codes'] || $search) {
               $result[] = $code;
           } else {
               $aux = array_flip(explode(",", $value['fc_hierarchy_codes']));
               //print_r($selectorsData);
               //print_r(array_intersect_key($aux, $selectorsData));
               // print("a: " . $code . " " . count(array_intersect_key($aux, $selectorsData)) . "\n");
               if (count(array_intersect_key($aux, $auxSelectorsData)) == 0) {

                   $result[] = $code;
               }
           }  
        }

        if (is_array($result)) {
            return array_unique($result);
        } else
            return null;
    }

     public function computeVisibleItems(&$selectorsData, $userSelectors, $search = false, $summary = false) {
         if (!isset($selectorsData))
             return array();
         /*
           if(isset($userSelectors['_client']['_category_selector'])){
           $allSelectors = array_flip($userSelectors['_client']['_category_selector']);
           }
           else if(isset($userSelectors['_user']['_category_selector'])){
           $allSelectors = array_flip($userSelectors['_user']['_category_selector']);
           } */

         if (isset($userSelectors['_user']['_category_selector'])) {
             $skip = false;
             $allSelectors = array_flip($userSelectors['_all']['_category_selector']);
         } else {
             $skip = true;
         }

         //print("\nselettori utente \n");
         //print_r($allSelectors); 
         // first calculate items that appears in all categories, others will have a second chance
         $auxSelectorsData = $selectorsData;
         foreach ($selectorsData as $code => $value) {

             // if($code == 8480) {
             $validCode = true;
             if (!$skip) {
                 $selectorsArray = array();
                 $aux = explode(",", $value['fc_hierarchy_selectors']);
                 for ($j = 0; $j < count($aux); $j++) {
                     $selectorsArray[$aux[$j]] = $aux[$j];
                 }
                 //print("\nselettori asset \n");
                 //print_r($selectorsArray);

                 $selectorsNotOwnedByUser = array_diff_key($selectorsArray, $allSelectors);

                 if (count($selectorsNotOwnedByUser) > 0) {
                     $selectorsIntersection = array_intersect_key($selectorsArray, $allSelectors);
                     //print("\nselettori non appartenenti all'utente \n");
                     //print_r($selectorsNotOwnedByUser);
                     //print("\nintersezione \n");
                     //print_r($selectorsIntersection);
                     $selectorsOwnedByUser = array_diff_key($allSelectors, $selectorsArray);

                     if (count($selectorsOwnedByUser) > 0) {

                         $selectorsNotOwnedByUserString = implode(",", $selectorsNotOwnedByUser);
                         $selectorsIntersectionString = implode(",", $selectorsIntersection);
                         foreach ($selectorsOwnedByUser as $k => $v) {
                             $category = explode(":", $k);
                             if (strpos($selectorsNotOwnedByUserString, $category[0]) !== false && strpos($selectorsIntersectionString, $category[0]) === false) {
                                 $validCode = false;
                                 break;
                             }
                         }
                     }

                     /*
                       foreach($selectorsNotOwnedByUser as $key => $value2){

                       if(isset($userSelectors['_client']['_category_selector'])){

                       if(!in_array($value2, $userSelectors['_user']['_category_selector'])){
                       $category = explode(":", $value2);

                       if(in_array($category[0], $userSelectors['_user']['_categories'])){
                       $validCode = false;
                       break;
                       }
                       }
                       }
                       else{
                       $category = explode(":", $value2);
                       if(in_array($category[0], $userSelectors['_user']['_categories'])){
                       $validCode = false;
                       break;
                       }
                       }
                       } */
                 }
             }

             if ($validCode) {
                 if (!$value['fc_hierarchy_codes'] || $search || $summary) {
                     $result[] = $code;
                 } else {
                     $aux = array_flip(explode(",", $value['fc_hierarchy_codes']));
                     //print_r($selectorsData);
                     //print_r(array_intersect_key($aux, $selectorsData));
                     // print("a: " . $code . " " . count(array_intersect_key($aux, $selectorsData)) . "\n");
                     if (count(array_intersect_key($aux, $auxSelectorsData)) == 0) {

                         $result[] = $code;
                     }
                 }
             }


             //}
         }

         // if parent = 0 and exist items without parent they are valid
         /*
           if(isset($selectorsData['___']) && count($selectorsData['___']['_id']) > 0 ){
           if(!is_array($result)){
           $result = $selectorsData['___']['_id'];
           }
           else{
           $result = array_merge($result, $selectorsData['___']['_id']);
           }
           }
           // move each second chance item to the categories of selectors which is associated
           $secondChanceByCategories = array();

           for($i = 0; $i < count($secondChance); $i++){
           // init array that will contain types associated with i-th element
           $elementTypes = array();
           // loop through types elements
           foreach($selectorsData as $key => $value){
           // check if i-th element of second chance array is contained in $value (array of elements grouped by type)
           if(in_array($secondChance[$i], $value['_id'])){
           // add type to element
           $elementTypes[] = $key;
           }
           }
           $typeKey = implode(",", $elementTypes);
           // add element to correct type array. (i.e if $elementTypes = 'SELECTOR1,SELECTOR2' the element will be added to $secondChanceByType[SELECTOR1,SELECTOR2] array
           if(!isset($secondChanceByCategories[$typeKey])){
           $secondChanceByCategories[$typeKey] = array($secondChance[$i]);
           }
           else{
           $secondChanceByCategories[$typeKey][] = $secondChance[$i];
           }
           }

           // delete second chance items that have a selector of a categroy owned by user
           foreach($secondChanceByCategories as $categories => $items){

           $categoriesOwnedByItems = explode(",", $categories);

           // find categories owned by user but not associated with this items
           $categoryNotOwnedByItems = array_values(array_diff($userSelectors['_user']['_categories'], $categoriesOwnedByItems));

           // if a second chance element is related with selectors of category owned by user but different id it's a bad element
           $aux = $this->checkItemsOwnedBySelectorCategories($categoryNotOwnedByItems, $items);
           if(is_array($aux['out'])){
           $result = array_merge($result, $aux['out']);
           }
           }


           // check if there are selectors passed by client to filter result
           if(count($userSelectors['_client']['_flat']) > 0){
           $this->filterItemsByClientSelectors($result, $userSelectors, $selectorsData);
           }
          * 
          */
         //print_r($result); die();
         if (is_array($result)) {
             return array_unique($result);
         } else
             return null;
     }

     public function getItemsBySelectors($category, $selectors, $parent = -1, $filters = null, $summary = false, $self = false) {

         $userInfo = Zend_Auth::getInstance()->getIdentity();

         if ($userInfo->f_level) {
             $user_level['f_level'] = $userInfo->f_level;
             $user_level['f_group_level'] = $userInfo->f_group_level;
         } else {
             $user_level['f_level'] = $userInfo['fc_usr_level'];
             $user_level['f_group_level'] = $userInfo['fc_usr_group_level'];
         }
         // get table info needed to get items
         $tablesInfo = $this->getTablesInfo($category);
         /* if($this->db_config['adapter'] != 'PDO_MYSQL') {
           $this->db->query('SET CHARACTER SET utf8');
           } */

         $select = new Zend_Db_Select($this->db);
        $select->from('t_creation_date', array());
        $select ->join('t_wf_phases', 't_wf_phases.f_number = t_creation_date.f_phase_id AND t_wf_phases.f_wf_id = t_creation_date.f_wf_id', array())
                ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array());
        if(is_array($filters) && (count($filters['_filter']) > 0 || count($filters['_search']) > 0 || count($filters['_order']) > 0)){
        $select->join($tablesInfo['_itemTable'], $tablesInfo['_itemTable'] . '.f_code = t_creation_date.f_id', array())
                ->join('t_custom_fields', 't_custom_fields.f_code = ' . $tablesInfo['_itemTable'] . '.f_code', array());
        }
        if ($parent != -1) {
            $select->join($tablesInfo['_parentTable'], $tablesInfo['_parentTable'] . '.f_code = ' . 't_creation_date.f_id', array());
        }

        $select->where("t_wf_phases.f_visibility & " . $user_level['f_level'] . " != 0")
        ->where("t_wf_groups.f_visibility & " . $user_level['f_level'] . "!= 0")
        ->where("t_creation_date.f_visibility & " . $user_level['f_level'] . " != 0")
        ->where("t_creation_date.f_visibility & " . $user_level['f_group_level'] . " != 0")
        ->where("t_creation_date.f_category IN ('" . implode("','", $category) . "')");

         //AGGIUNTO DA BALDINI IL 27 04 2017 - compatibilita' con logica external manteiner
         //MODIFICATO DA BALDINI IL 2 10 2017 - validità solo per WO
         $settings = Zend_Registry::get('settings');
         if ($tablesInfo['_itemTable'] == 't_workorders' && isset($user_level['f_level']) && isset($settings['ENABLE_EXTERNAL_MANTAINER']) && $settings['ENABLE_EXTERNAL_MANTAINER'] == 'On' && ($user_level['f_level'] != -1 && $user_level['f_level'] & 32) != 0) {

             $tableCross = 't_ware_wo';
             $fieldCross = 'f_wo_id';
             $fieldWhere = 'f_ware_id';
             $select->join($tableCross, "t_creation_date.f_id =" . $fieldCross, array())->where($fieldWhere . "=" . (is_object($userInfo) ? $userInfo->f_id : $userInfo['f_code']));
         }

         // filter to show only workorders created by logged user (self) 
         if (isset($self) && (((int)$self & $userInfo->f_level && $userInfo->f_level>0) || ((int)$self==-1))) {
             $select->where('t_creation_date.f_creation_user = ' . (is_object($userInfo) ? $userInfo->f_id : $userInfo['f_code']));
         }
        
         if (in_array('USER', $category)) {
             $select->join('t_users', 't_users.f_code = t_custom_fields.f_code', array());
         }

         if ($filters != null) {
             $this->addQueryFilters($select, $filters, $tablesInfo, $summary, $parent);
         }
        // add selectors 'SQL where' from client
        if (count($selectors['_client']['_flat']) > 0) {
            $select->where($this->addClientSelectorsWhere($selectors['_client']));  
        }
        // add user's selectors 'SQL where'
        elseif(count($selectors['_user']['_flat']) > 0){
            $select->where($this->addValidCodesWhere($selectors['_user']));
        }
        // add filter for objects not matching user's selectors if there are
        if(count($selectors['_user']['_flat']) > 0){
            $select->where("t_creation_date.f_id NOT IN (" . $this->addNotValidCodesWhere($selectors['_user'], $category) . ")");
        }

        // filter by parent
        if ($parent != -1) {
            $select->where($tablesInfo['_parentTable'] . ".f_parent_code = " . (int) $parent);

        }

        if($summary){
            $select->group(['t_wf_phases.f_summary_name', $tablesInfo['_itemTable'] . '.f_type_id']);
            $select->columns(['t_wf_phases.f_summary_name', $tablesInfo['_itemTable'] . '.f_type_id', "COUNT(*) AS total"]);
        }
        else{
            $select->columns(['fc_hierarchy_selectors', 'fc_hierarchy_codes', 't_creation_date.f_id']);
        }
        
        // execute query
        $res = $select->query()->fetchAll();
//        die($select->__toString());
        // for summary data is ok
        if($summary){
            return $res;
        }

        // format data
        $result = array();
        for ($i = 0; $i < count($res); $i++) {
            $result[$res[$i]['f_id']] = array(
                'fc_hierarchy_selectors' => $res[$i]['fc_hierarchy_selectors'],
                'fc_hierarchy_codes' => $res[$i]['fc_hierarchy_codes']
            );
        }
        return $result;
    }

    private function addValidCodesWhere($selectors){
        // tutti gli elementi che hanno uno dei codici selettori posseduti dall'utente o che non hanno selettori associati
        $where = "((fc_hierarchy_selectors LIKE '%" . implode("%' OR fc_hierarchy_selectors LIKE '%", $selectors['_flat']) . "%' OR fc_hierarchy_selectors IS NULL)";
        // oppure 
        $where .= " OR ";
        // tutti gli elementi che non hanno categorie di selettori dell'utente (inclui anche gli elementi che hanno fc_hierarchy_selectors vuoto)
        $where .= "(fc_hierarchy_selectors NOT LIKE '%" . implode("%' AND fc_hierarchy_selectors NOT LIKE '%", $selectors['_categories']) . "%'))";
        return $where;
    }

    private function addNotValidCodesWhere($selectors, $objCategory){
        // i codici non validi sono quelli che hanno una categoria selettori che ha anche l'utente ma un codice selettore diverso
        foreach($selectors['_category'] as $category => $codes){
            $where[] = "(fc_hierarchy_selectors LIKE '%" . $category . "%' AND fc_hierarchy_selectors NOT LIKE '%" . implode("%' AND fc_hierarchy_selectors NOT LIKE '%", $codes)  . "%')"; 
        }
        $select = "SELECT f_id FROM t_creation_date WHERE f_category IN('" . implode("','", $objCategory) . "') AND (" . implode(" OR ", $where) . ")";
        return $select;
    }
     
    private function addClientSelectorsWhere($selectors){
        foreach ($selectors['_category'] as $key => $value) {
            $or = array();
            for ($j = 0; $j < count($value); $j++) {
                $or[] = "fc_hierarchy_selectors LIKE '%" . $value[$j] . "%'";
            }
            $and[] = implode(" OR ", $or);
        }
        return "(" . implode(") AND (", $and) . ")";
    }

    //******************************************
    // end new selectors
    //******************************************


     public function jax_read_root($params) {
         //Zend_session::writeClose();
         //carico i parametri di visibilità  dell'utente
         $qu = new Zend_Db_Select($this->db);
         $user = Zend_Auth::getInstance()->getIdentity();
         $user_level = $user->f_level;
         $user_group_level = $user->f_group_level;
         $uid = $user->f_id;
         $tab = $params->Tab[0];
         $tabp = $tab . "_parent";
         $f_type = $params->Tab[1];
         $f_name = "";

         //IN CASO DI PASSAGGIO DI GRUPPO WF DA SOMMARIO, DETERMINO LA VISUALIZZAZIONE IN BASE A QUELLI RICHIESTI 
         // E NON IN BASE ALLA VISIBILITA' DI BASE


         $n1 = 0;
         $n0 = 0;

         /*
           $qu->from(array('t2'=>$tabp),array('num'=>'count(*)'))
           ->join(array("t1"=>$tab),"t1.f_code = t2.f_code",array())
           ->join(array("t_creation_date"),"t_creation_date.f_id = t2.f_code",array())
           ->join("t_wf_phases","t_creation_date.f_phase_id = t_wf_phases.f_number and t_creation_date.f_wf_id = t_wf_phases.f_wf_id",array())
           ->join("t_wf_groups","t_wf_phases.f_group_id = t_wf_groups.f_id",array());
           if($f_type > 0) {
           $qu->where("f_type_id IN ($f_type)");
           }
           $ris =  $qu->where("t2.f_active = 1")
           ->where("f_parent_code = 0")
           ->where("(t_creation_date.f_visibility & $user_group_level) != 0")
           ->where("(t_creation_date.f_visibility & $user_level) != 0")
           ->where("(t_wf_phases.f_visibility & $user_level) != 0")
           ->where("(t_wf_groups.f_visibility & $user_level) != 0")->query()->fetch();
           $n0=(int) $ris['num'];

           $n1=$n0;
           $qu->reset();
           // se lista piatta conto elementi in $tab
           if(!$n0) {
           $qu->from($tab,array('num'=>'count(*)'));
           if($f_type) {
           $qu->where("f_type_id IN ($f_type)");
           }
           $ris = $qu->query()->fetch();
           $n1=(int) $ris['num'];
           $qu->reset();
           }
          */



         // Recupero asset per wo        
         $book = $this->getBookmark($f_name, 'view', $params->module_name);
         $favourite = $this->getBookmark($f_name, 'bookmark', $params->module_name);
         $favourite_id = 0;
         $favourite_bookmark = array();
         if (!is_null($favourite)) {
             $favourite_bookmark = json_decode(Mainsim_Model_Utilities::chg($favourite['f_properties']));
             $favourite_id = json_decode($favourite['f_id']);
         }

         return array("n0" => $n0, "n1" => $n1, 'bookmark' => json_decode(Mainsim_Model_Utilities::chg($book['f_properties']), true), 'bookmark_name' => $book['f_name'],
             'bookmark_id' => $book['f_id'], 'aFavorite' => $favourite_bookmark, 'FavoriteId' => $favourite_id, 'checkCross' => array());
     }

     /**
      * A.B. sei una paraculo
      * @param type $params
      * @return type 
      */
     public function jax_read_root_chk_history($params) {
         $qu = new Zend_Db_Select($this->db);
         //Zend_session::writeClose();
         $module_name = $params->module_name;
         $tab = $params->Tab[0];
         $type = $params->Tab[1];
         $tab_columns = Mainsim_Model_Utilities::get_tab_cols($tab);
         $tab_custom = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
         $f_name = "";
         $result_col = $this->getBookmark($f_name, 'view', $module_name);
         $cols_name = array();
         $result_col = json_decode(Mainsim_Model_Utilities::chg($result_col['f_properties']), true);

         $black_list = array(
             'f_creation_date',
             'f_percentage',
             'f_name'
         );
         foreach ($result_col as $line) {
             if (!in_array($line[0], $tab_columns) && in_array($line[0], $tab_custom) && !in_array($line[0], $black_list)) {
                 $cols_name[] = $line[0];
             }
         }

         $f_code = $params->f_code;
         if ($params->Tab[0] != "t_meters_history") {
             //recupero tutta la storia di questo f_code.
             $res = $qu->from(array("t1" => $tab . '_history'))
                             ->join("t_creation_date_history", "t1.f_code = t_creation_date_history.f_code and t1.f_timestamp = t_creation_date_history.f_timestamp")
                             ->join(array("t2" => "t_custom_fields_history"), "t_creation_date_history.f_code = t2.f_code AND t_creation_date_history.f_timestamp = t2.f_timestamp", $cols_name)
                             ->join("t_wf_phases", "t_creation_date_history.f_phase_id = t_wf_phases.f_number and t_creation_date_history.f_wf_id = t_wf_phases.f_wf_id", array('f_edit' => 't_wf_phases.f_editability', 'f_percentage', 'f_name'))->where("t1.f_code = $f_code")->order("t1.f_timestamp ASC")
                             ->query()->fetchAll();
         } else {
             $res = $qu->from(array("t1" => "t_meters_history"))
                             ->where("t1.f_code = $f_code")
                             ->query()->fetchAll();
         }
         $data = array();
         $oth = array();

         $book = $this->getBookmark($f_name, 'view', $module_name);

         $i = 0;
         $head = array();
         $arr = array();
         foreach ($res as $data) {
             $cmp = array();
             foreach ($data as $k => $v) {
                 $cmp[] = Mainsim_Model_Utilities::chg($v);
                 if (!$i)
                     $head[] = $k;
             }
             if (!$i)
                 $arr[] = $head;
             $arr[] = $cmp;
             $pos++;
             $i++;
         }
         $data = $arr;

         // $jarbkm=Mainsim_Model_Utilities::chg($book['f_properties']);
         $jarbkm = $book['f_properties'];


         return array('bookmark' => json_decode($jarbkm, true), 'bookmark_name' => $book['f_name'], 'bookmark_id' => $book['f_id'], 'data' => $data, 'oth' => $oth);
     }

     /**
      * @param mixed $params
      * @return type 
      */
     public function jax_read_root_chk($params) {
         $qu = new Zend_Db_Select($this->db);
         $module_name = $params->module_name;
         $tab = $params->Tab[0];
         $tabp = $tab . "_parent";
         $f_name = "";

         $sort = $params->Sort;
         $order = "t2.f_id ASC";
         $aord_field = array("t3.f_id", "t1.f_code", "t3.f_creation_date", "t3.f_title", "t3.f_description", "t3.f_timestamp", "t3.fc_progress", "t1.f_start_date", "t1.f_end_date", "t1.fc_wo_starting_date", "t1.fc_wo_ending_date");


         $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
         $elementList = array(
             'WARES' => array(
                 't_wares' => array('table' => 't_wares_relations', 'column' => 'f_code_ware_slave', 'whereColumn' => 'f_code_ware_master'),
                 't_selectors' => array('table' => 't_selector_ware', 'column' => 'f_selector_id', 'whereColumn' => 'f_ware_id'),
                 't_workorders' => array('table' => 't_ware_wo', 'column' => 'f_wo_id', 'whereColumn' => 'f_ware_id'),
                 't_systems' => array('table' => 't_wares_systems', 'column' => 'f_system_id', 'whereColumn' => 'f_ware_id')
             ),
             'SELECTORS' => array(
                 't_wares' => array('table' => 't_selector_ware', 'column' => 'f_ware_id', 'whereColumn' => 'f_selector_id'),
                 't_workorders' => array('table' => 't_selector_wo', 'column' => 'f_wo_id', 'whereColumn' => 'f_selector_id'),
                 't_systems' => array('table' => 't_selector_system', 'column' => 'f_system_id', 'whereColumn' => 'f_selector_id')
             ),
             'WORKORDERS' => array(
                 't_wares' => array('table' => 't_ware_wo', 'column' => 'f_ware_id', 'whereColumn' => 'f_wo_id'),
                 't_workorders' => array('table' => 't_wo_relations', 'column' => 'f_code_wo_slave', 'whereColumn' => 'f_code_wo_master'),
                 't_selectors' => array('table' => 't_selector_wo', 'column' => 'f_selector_id', 'whereColumn' => 'f_wo_id')
             ),
             'SYSTEMS' => array(
                 't_wares' => array('table' => 't_wares_systems', 'column' => 'f_ware_id', 'whereColumn' => 'f_system_id'),
                 't_selectors' => array('table' => 't_selector_system', 'column' => 'f_selector_id', 'whereColumn' => 'f_system_id')
             )
         );

         if (strpos($params->Tab[0], "_parent") !== false) {
             $tab = substr($params->Tab[0], 0, strpos($params->Tab[0], "_parent"));
             $tabp = $params->Tab[0];
         }

         $data = array();
         $oth = array();

         $f_type = $params->Tab[1];
         $f_code = $params->f_code;

         if ($tab != "t_meters_history") {
             $f_codes = array();
             if (strpos($params->Tab[0], "_parent") === false) {
                 // get cross element
                 if ($f_code > 0) {
                     $qu->reset();
                     $resultType = $qu->from("t_creation_date", "f_type")
                                     ->where("f_id = ?", $f_code)->query()->fetch();

                     if (isset($elementList[$resultType['f_type']][$tab])) {
                         $qu->reset();
                         //get f_codes crossed                        
                         $crossTable = $elementList[$resultType['f_type']][$tab]['table'];
                         $crossColumn = $elementList[$resultType['f_type']][$tab]['column'];
                         $whereColumn = $elementList[$resultType['f_type']][$tab]['whereColumn'];
                         $qu->from(array('t1' => $tab), array('f_code'))
                                 ->join(array('t2' => $crossTable), "t2.$crossColumn = t1.f_code", array())
                                 ->join(array('t3' => "t_creation_date"), "t1.f_code = t3.f_id", array());

                         if ($params->skipwf == false) {

                             $qu->join(array("t4" => "t_wf_phases"), "t3.f_phase_id = t4.f_number and t3.f_wf_id = t4.f_wf_id", array())
                                     ->join(array("t5" => "t_wf_groups"), "t4.f_group_id = t5.f_id", array())
                                     ->where("t5.f_id != 7");
                         }

                         // sort  
                         if (isset($sort[0])) {

                             $sort_field = $sort[0]->f;
                             $sort_direc = $sort[0]->m;

                             if ($sort_field && $sort_direc) {

                                 for ($r = 0; $r < count($aord_field); $r++) {
                                     if (strpos($aord_field[$r], "." . $sort_field) !== false) {
                                         $sfield = $aord_field[$r];
                                         if (strpos($sfield, "t1") !== false && $tab != "t_workorders" && $sort_field != "f_code") {    // f_code = f_id 
                                         } else {
                                             $order = $sfield . " " . $sort_direc;
                                         }
                                         break;
                                     }
                                 }
                             }
                         }


                         // echo $order." | ".$module_name; die(" ");


                         $qu->where("t2.$whereColumn = ?", $f_code);
                         if ($resultType['f_type'] == 'SELECTORS') {
                             $qu->where("t2.f_nesting_level = 1"); //if this is a selector cross
                         }
                         if ($f_type) {
                             $qu->where("t1.f_type_id IN ($f_type)");
                         }
                         $qu->order($order);
                         $data = $qu->query()->fetchAll();

                         foreach ($data as $line) {
                             $f_codes[] = (int) $line['f_code'];
                         }
                     }
                 }
             } else {
                 $result_codes = $qu->from($tabp, array('f_parent_code'))
                                 ->where("f_code = ?", $f_code)
                                 ->where("f_active = 1")->query()->fetchAll();
                 foreach ($result_codes as $line) {
                     $f_codes[] = (int) $line['f_parent_code'];
                 }
             }
             if (!empty($f_codes)) {
                 if ($params->hiddenCross == 1 && strpos($params->Tab[0], "_parent") === false) {
                     $params->Prnt = -1;
                     //if not parent, and hiddenCross enable, apply treegrid default filter
                     $t_sst = (microtime(true) * 10000) . '_' . $uid . '_' . rand(0, 9);
                     $resParams = $this->prepareArrayTreegrid($params, $t_sst);
                     $treegridParams = $resParams['params'];
                     $treegridParams['where'][] = $this->db->quoteInto("t1.f_code IN (?)", $f_codes);
                     $cols['mainTableCols'][] = 'f_code';
                     $resCodes = $this->treegridQuery($params->Tab[0], $params->Tab[1], $cols, $treegridParams);
                     $this->db->delete("t_selectors_support", "f_call = '{$t_sst}'");
                     $totrC = count($resCodes);
                     $f_codes_visible = array(); //list of code visible for this user
                     for ($rc = 0; $rc < $totrC; ++$rc) {
                         $f_codes_visible[] = (int) $resCodes[$rc]['f_code'];
                     }
                 }
                 $params_check = json_encode(array("Tab" => $params->Tab, "Ands" => implode(',', $f_codes), 'module_name' => $module_name, "Type" => $resultType['f_type'], "SourceCode" => $f_code));
                 $res_check = $this->jax_read_check(json_decode($params_check));
                 $data = $res_check['data'];
                 $oth = $res_check['oth'];
             }
         } else {
             $qu->reset();
             $res_data = $qu->from($tab, array("f_code", "f_value", "f_timestamp"))
                             ->where("f_code = ?", $f_code)
                             ->query()->fetchAll();
             if (!empty($res_data)) {
                 $data[0] = array_keys($res_data[0]);
             }
             $tot = count($res_data);
             for ($i = 0; $i < $tot; ++$i) {
                 $data[] = array_values($res_data[$i]);
             }
         }
         $book = $this->getBookmark($f_name, 'view', $module_name);
         if (!empty($data)) {
             $data[0][] = 'hiddenCross';
             $f_code_pos = array_search("f_code", $data[0]);
         }
         $totData = count($data);
         for ($d = 1; $d < $totData; ++$d) {
             if ($f_code_pos === false) {
                 $data[$d][] = 0;
                 continue;
             } elseif ($params->hiddenCross == 1 && strpos($params->Tab[0], "_parent") === false && !in_array($data[$d][$f_code_pos], $f_codes_visible)) {
                 $data[$d][] = 1;
             } else {
                 $data[$d][] = 0;
             }
         }
         if (is_null($oth)) {
             $oth = array();
         }
         return array('bookmark' => json_decode(Mainsim_Model_Utilities::chg($book['f_properties']), true), 'bookmark_name' => $book['f_name'], 'bookmark_id' => $book['f_id'], 'data' => $data, 'oth' => $oth);
     }

     //----------------------
     public function jax_read_root_chk_pair($params) {

         $qu = new Zend_Db_Select($this->db);
         //Zend_session::writeClose();
         $f_code = $params->f_code;
         $module_name = $params->module_name;

         $sort = $params->Sort;
         $order = "t2.f_id ASC";
         $aord_field = array("t3.f_id", "t1.f_code", "t3.f_creation_date", "t3.f_title", "t3.f_description", "t3.f_timestamp", "t3.fc_progress", "t1.f_start_date", "t1.f_end_date", "t1.fc_wo_starting_date", "t1.fc_wo_ending_date");


         //rec f_type_id main        
         if ($f_code > 0) {
             $res_tab = $qu->from("t_creation_date")->where("f_id = ?", $f_code)->query()->fetch();
             $table = "t_" . strtolower($res_tab['f_type']);
             $table .= $table == 't_systems' ? "_settings" : "";
             $qu->reset();
             $res_ftype = $qu->from($table)->where("f_code = ?", $f_code)->query()->fetchAll();
             $f_type_main = $res_ftype[0]['f_type_id'];
         } else {
             $f_type_main = $params->f_typeid_wo;
         }

         //ottengo gli f_type di code e code_cross        
         $qu->reset();
         $res = array();
         if ($params->Tab[1] > 0) {

             // pair_cross inverted
             if ($params->pcross_inv == 1) {
                 $res = $qu->from(array("t1" => "t_pair_cross"))
                                 ->join(array("t2" => $params->Tab[0]), "t1.f_code_main = t2.f_code", array())
                                 ->where("f_code_cross = ?", $f_code)
                                 ->where("f_type_id IN ({$params->Tab[1]})")
                                 ->query()->fetchAll();
             } else {
                 $res = $qu->from(array("t1" => "t_pair_cross"))
                                 ->join(array("t2" => $params->Tab[0]), "t1.f_code_cross = t2.f_code", array())
                                 ->where("f_code_main = ?", $f_code)
                                 ->where("f_type_id IN ({$params->Tab[1]})")
                                 ->query()->fetchAll();
             }
         } elseif ($f_code > 0) { // if is an engagement            
             $res = $qu->from("t_pair_cross")->where("f_code_main = ?", $f_code)->where("f_code_cross = ?", $params->Tab[1])->query()->fetchAll();
         }

         $qu->reset();
         $fields = array('f_code' => 'f_id', 'f_code_main', 'f_code_cross', 'f_group_code');
         $pair_cols = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
         $book = $this->getBookmark('', 'view', $module_name);
         $properties = json_decode(Mainsim_Model_Utilities::chg($book['f_properties']), true);
         
         // NICO 18/10/2018
         // Filtro bookmark per userrole
         $userrole = Zend_Auth::getInstance()->getIdentity()->f_level;
         $bookmarks = [];
         foreach ($properties as $key=>$value) {
            if ($userrole & $value[10]) {
                $bookmarks[] = $value;
            }
        }
        $properties = $bookmarks;
        
         foreach ($properties as $line) {
             if (in_array($line[0], $pair_cols) || $params->pairCross == 1) {
                 $fields[] = $line[0];
             }
         }
         $f_ids = array();
         $f_code_cross = array();
         $groups_code = array();
         foreach ($res as $line) {
             $f_ids[] = $line['f_id'];
             $groups_code[] = $line['f_group_code'];
             if (!in_array($line['f_code_cross'], $f_code_cross))
                 $f_code_cross[] = $line['f_code_cross'];
         }
         $data = array();
         $data_cross = array();
         $oth = array();
         if (!empty($f_code_cross)) {
             $params_str = json_encode(array("f_id" => $f_ids));
             $res_pair = $this->jax_read_chk_pair(json_decode($params_str), $fields);
             $data = $res_pair['data'];
             if ($params->pairCross > 0 && $params->pairCross != 2) {
                 for ($i = 1; $i < count($data); $i++) {
                     $code_cross = $data[$i][2]; //salvo il code_cross
                     $data[$i][2] = $data[$i][3]; //sposto il group_code
                     $data[$i][3] = $code_cross;
                 }
             }
             $oth = $res_pair['oth'];
         }
         $qu->reset();
         $res_f_codes = array();
         $res_check = array();

         $tab_cols_bm = array("f_id", "f_code", "f_timestamp");
         $creation_cols_bm = array();
         $custom_cols_bm = array();

         $book_wo = $this->getBookmark("", 'view', $module_name);
         
         // NICO: Filtro bookmarks
         $bookmarks = [];
         foreach (json_decode(Mainsim_Model_Utilities::chg($book_wo['f_properties']), true) as $key=>$value) {
            if ($userrole & $value[10]) {
                $bookmarks[] = $value;
            }
        }
        $book_wo['f_properties'] = json_encode($bookmarks);
         
         
         
         $custom_cols = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
         $creation_cols = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
         $tab_cols = Mainsim_Model_Utilities::get_tab_cols($params->Tab[0]);

         $properties = json_decode(Mainsim_Model_Utilities::chg($book_wo['f_properties']), true);
         foreach ($properties as $line) {
             if (in_array($line[0], $creation_cols) && !in_array($line[0], $creation_cols_bm)) {
                 $creation_cols_bm[] = $line[0];
             }
             if (in_array($line[0], $tab_cols) && !in_array($line[0], $tab_cols_bm)) {
                 $tab_cols_bm[] = $line[0];
             }
             if (in_array($line[0], $custom_cols) && !in_array($line[0], $custom_cols_bm)) {
                 $custom_cols_bm[] = $line[0];
             }
         }

         $tab = $params->Tab[0];


         if (!$params->pairCross) {
             $qu->from(array('t1' => $tab), $tab_cols_bm)
                     ->join("t_creation_date", "t1.f_code = t_creation_date.f_id", $creation_cols_bm)
                     ->join(array("t2" => "t_custom_fields"), "t_creation_date.f_id = t2.f_code", $custom_cols_bm);
             if ($table == 't_workorders') {
                 if ($params->pcross_inv) {
                     $qu->join(array("t3" => "t_ware_wo"), "t1.f_code = t3.f_ware_id", array())->where("t3.f_ware_id = $f_code");
                 } else {
                     $qu->join(array("t3" => "t_ware_wo"), "t1.f_code = t3.f_ware_id", array())->where("t3.f_wo_id = $f_code");
                 }
             } else {
                 if ($params->Tab[0] == 't_workorders') {
                     $qu->join(array("t3" => "t_ware_wo"), "t1.f_code = t3.f_wo_id", array())
                             ->where("t3.f_ware_id = $f_code");
                 } else {
                     $qu->join(array("t3" => "t_wares_relations"), "t1.f_code = t3.f_code_ware_slave", array())
                             ->where("t3.f_code_ware_master = $f_code");
                 }
             }
             $qu->join(array("t4" => "t_wf_phases"), "t_creation_date.f_phase_id = t4.f_number AND t_creation_date.f_wf_id = t4.f_wf_id", array('f_name', 'f_percentage'));
             $qu->where("t1.f_type_id IN ({$params->Tab[1]})")
                     ->order("t3.f_id ASC");


             $res_check = $qu->query()->fetchAll();
         } elseif (!empty($groups_code) && $params->pairCross != 2) {
             $res_check = $qu->from(array('t1' => $tab), $tab_cols_bm)
                             ->join("t_creation_date", "t1.f_code = t_creation_date.f_id", $creation_cols_bm)
                             ->join(array("t2" => "t_custom_fields"), "t_creation_date.f_id = t2.f_code", $custom_cols_bm)
                             ->where("t1.f_code IN (" . implode(',', $groups_code) . ")")->query()->fetchAll();
         }

         $i = 0;
         if (empty($data)) {
             $data[0][] = "f_code";
             $data[0] = array_merge($data[0], Mainsim_Model_Utilities::get_tab_cols("t_pair_cross"));
             $data[0][] = 'f_description';
         }

         if ($params->pcross_inv) {
             $data[0][1] = 'f_code_cross';
             $data[0][2] = 'f_code_main';
         }

         if ($params->pairCross != 2) {
             $data_cross[0] = $data[0];
             $fields = array();
             $properties = json_decode(Mainsim_Model_Utilities::chg($book_wo['f_properties']), true);
             foreach ($properties as $line) {
                 if (!in_array($line[0], $data_cross[0])) {
                     $data_cross[0][] = $line[0];
                 }
             }

             $data_cross[0][] = 'f_percentage';
         }

         foreach ($res_check as $line) {
             foreach ($data_cross[0] as $key) {
                 if (array_key_exists($key, $line)) {
                     $res_f_codes[$i][] = $line[$key];
                 } else
                     $res_f_codes[$i][] = "";
             }
             $i++;
         }

         if ($params->pairCross != 2) {
             foreach ($res_f_codes as $line) {
                 $chg_line = $line;
                 foreach ($chg_line as &$chg_value) {
                     $chg_value = Mainsim_Model_Utilities::chg($chg_value);
                 }
                 $data_cross[] = $chg_line;
             }
         } else {
             $data_cross = array();
         }


         $data_cross_copy = array(
             $data_cross[0]
         );
         $qu->reset();
         $type_id = $params->pairCross == 1 ? 1 : $params->Tab[1];

         if ($tab == 't_workorders') {
             $qu->from(array('t1' => "t_workorders"), "f_code");
         } else {
             $qu->from(array('t1' => "t_wares"), "f_code");
         }


         if ($table == 't_workorders') {
             $qu->join(array("t2" => "t_ware_wo"), "t1.f_code = t2.f_ware_id", array())
                     ->where("f_wo_id = ?", $f_code);
         } else {
             if ($params->Tab[0] == 't_workorders') {
                 $qu->join(array("t2" => "t_ware_wo"), "t1.f_code = t2.f_wo_id", array())
                         ->where("f_ware_id= ?", $f_code);
             } else {
                 $qu->join(array("t2" => "t_wares_relations"), "t1.f_code = t2.f_code_ware_slave", array())
                         ->where("f_code_ware_master = ?", $f_code);
             }
         }



         // sort  
         if (isset($sort[0])) {

             $sort_field = $sort[0]->f;
             $sort_direc = $sort[0]->m;

             if ($sort_field && $sort_direc) {

                 for ($r = 0; $r < count($aord_field); $r++) {
                     if (strpos($aord_field[$r], "." . $sort_field) !== false) {
                         $sfield = $aord_field[$r];
                         if (strpos($sfield, "t1") !== false && $tab != "t_workorders" && $sort_field != "f_code") {    // f_code = f_id 
                         } else {
                             $order = $sfield . " " . $sort_direc;
                         }
                         break;
                     }
                 }
             }
         }


         //      echo $order." | ".$module_name; die(" "); 



         $res_order = $qu->join(array('t3' => "t_creation_date"), "t1.f_code = t3.f_id", array())
                         ->where("f_type_id IN ({$type_id})")
                         ->order($order)->query()->fetchAll();

         $tot_a = count($res_order);
         $tot_b = count($data_cross);
         for ($a = 0; $a < $tot_a; ++$a) {
             for ($b = 1; $b < $tot_b; ++$b) {
                 if ($data_cross[$b][0] == $res_order[$a]['f_code']) {
                     $data_cross_copy[] = $data_cross[$b];
                     break;
                 }
             }
         }
         $data_cross = $data_cross_copy;
         return array('bookmark' => $properties, 'bookmark_name' => $book['f_name'], 'bookmark_id' => $book['f_id'], 'data' => $data, 'oth' => $oth, 'data_cross' => $data_cross);
     }

     //----------------------
     public function jax_read_chk_pair($params, $fields) {
         $q = new Zend_Db_Select($this->db);
         //Zend_session::writeClose();
         $an = $params->f_id;
         if ($an == "")
             $an = "0";
         else
             $an = implode(",", $params->f_id);
         $pairCols = Mainsim_Model_Utilities::get_tab_cols('t_pair_cross');
         $tabCols = Mainsim_Model_Utilities::get_tab_cols('t_wares');
         $customCols = Mainsim_Model_Utilities::get_tab_cols('t_custom_fields');
         $creationCols = Mainsim_Model_Utilities::get_tab_cols('t_creation_date');
         unset($fields['f_code']);
         $pair_columns = array('f_code' => 'f_id');
         $tab_columns = array();
         $custom_columns = array();
         $creation_columns = array();

         foreach ($fields as $key => $line) {
             if ($line == 'f_code')
                 continue;
             if (in_array($line, $pairCols)) {
                 $pair_columns[$key] = $line;
             } elseif (in_array($line, $tabCols)) {
                 $tab_columns[$key] = $line;
             } elseif (in_array($line, $creationCols)) {
                 $creation_columns[$key] = $line;
             } elseif (in_array($line, $customCols)) {
                 $custom_columns[$key] = $line;
             }
         }
         $ris = $q->from("t_pair_cross", $pair_columns)
                         ->where("t_pair_cross.f_id IN ($an)")->query()->fetchAll();

         $i = 0;
         $head = array();
         $arr = array();
         foreach ($ris as &$data) {
             if ($data['f_code_cross'] > 0) {
                 $q->reset();
                 $res_desc = $q->from("t_creation_date", array('f_description'))
                                 ->where("f_id = ?", $data['f_code_cross'])
                                 ->query()->fetch();
                 $data['f_description'] = $res_desc['f_description'];
             }
             if ($data['f_group_code'] > 0 && (!empty($creation_columns) && !empty($tab_columns) && !empty($custom_columns))) {
                 $q->reset();
                 $res_task = $q->from(array('t1' => "t_creation_date"), $creation_columns)
                                 ->join(array('t2' => 't_wares'), 't1.f_id = t2.f_code', $tab_columns)
                                 ->join(array('t3' => 't_custom_fields'), 't1.f_id = t3.f_code', $custom_columns)
                                 ->where("t1.f_id = ?", $data['f_code_cross'])->query()->fetch();

                 $data = array_merge($data, $res_task);
             }
             $cmp = array();
             foreach ($data as $k => $v) {
                 $cmp[] = Mainsim_Model_Utilities::chg($v);
                 if (!$i)
                     $head[] = $k;
             }
             if (!$i)
                 $arr[] = $head;
             $arr[] = $cmp;
             $pos++;
             $i++;
         }
         return array("data" => $arr, "other" => array());
     }

     //----------------------
     public function jax_read_root_chk_bm($params) {
         $qu = new Zend_Db_Select($this->db);
         //Zend_session::writeClose();
         $f_code = $params->f_code;
         $module_name = $params->module_name;
         $book = Mainsim_Model_Utilities::getBookmark($module_name, 'view');
         $properties = json_decode(Mainsim_Model_Utilities::chg($book['f_properties']), true);

         $qu->reset();
         $res = $this->db->query("SELECT f_id as f_code, f_code as f_code_main,'-1' as f_code_cross,'Field' as f_title,'0' as f_group_code,
                f_timestamp, fc_bkm_bind, fc_bkm_label, fc_bkm_type, fc_bkm_locked, fc_bkm_visible, fc_bkm_width, fc_bkm_filter, fc_bkm_range, fc_bkm_prange, fc_bkm_script, fc_bkm_level, fc_bkm_group 
            FROM t_bookmarks WHERE f_code = $f_code");
         $res->setFetchMode(Zend_Db::FETCH_NUM);
         $res = $res->fetchAll();

         $qu->reset();
         $fields = array(
             "f_code",
             "f_code_main",
             "f_code_cross",
             "f_title",
             "f_group_code",
             "f_timestamp",
             "fc_bkm_bind",
             "fc_bkm_label",
             "fc_bkm_type",
             "fc_bkm_locked",
             "fc_bkm_visible",
             "fc_bkm_width",
             "fc_bkm_filter",
             "fc_bkm_range",
             "fc_bkm_prange",
             "fc_bkm_script",
             "fc_bkm_level",
             "fc_bkm_group"
         );

         //  $res = Mainsim_Model_Utilities::clearChars($res);
         array_walk_recursive($res, function(&$t, &$k) {
             $tt = $t;
             if (gettype($tt) == "string")
                 $t = Mainsim_Model_Utilities::chg($tt);
         });

         $data = array();
         $data[0] = array_values($fields);
         $data = array_merge($data, $res);

         return array(
             'bookmark' => $properties,
             'bookmark_name' => $book['f_name'],
             'bookmark_id' => $book['f_id'],
             'data' => $data,
             'oth' => null,
             'data_cross' => array(null)
         );
     }

     public function jax_read_check($params) {
         //carico i parametri di visibilità  dell'utente        
         $q = new Zend_Db_Select($this->db);
         //Zend_session::writeClose();
         if (strpos($params->Tab[0], "_parent") !== false) {
             $tab = substr($params->Tab[0], 0, strpos($params->Tab[0], "_parent"));
         } else {
             $tab = $params->Tab[0];
         }
         $tab_custom = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
         $tab_creation = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
         $tab_user = Mainsim_Model_Utilities::get_tab_cols("t_users");
         $tab_columns = Mainsim_Model_Utilities::get_tab_cols($tab);

         $an = $params->Ands;
         if ($an == "")
             $an = "0";
         $f_type = $params->Tab[1];
         $result_col = $this->getBookmark($f_name, 'view', $params->module_name);

         $cols_name = array();
         $creation_cols = array();
         $main_cols = array();
         $user_cols = array();

         $result_col = json_decode(Mainsim_Model_Utilities::chg($result_col['f_properties']), true);
         foreach ($result_col as $line) {
             if (in_array($line[0], $tab_creation)) {
                 $creation_cols[] = $line[0];
             } elseif (in_array($line[0], $tab_columns)) {
                 $main_cols[] = $line[0];
             } elseif (in_array($line[0], $tab_custom)) {
                 $cols_name[] = $line[0];
             } elseif (in_array($line[0], $tab_user)) {
                 $user_cols[] = $line[0];
             }
         }
         if (!in_array("f_code", $main_cols)) {
             $main_cols[] = 'f_code';
         }
         $tab_explode = explode("_", $params->Tab[0]);
         $q->reset();
         if (strpos($tab, "_parent") === false) {
             $q->from(array('t1' => $tab), $main_cols)
                     ->join("t_creation_date", "t1.f_code = t_creation_date.f_id", $creation_cols)
                     ->join("t_wf_phases", "t_wf_phases.f_wf_id = t_creation_date.f_wf_id and t_wf_phases.f_number = t_creation_date.f_phase_id", array("f_name", "f_percentage"))
                     ->join("t_custom_fields", "t_custom_fields.f_code = t1.f_code", $cols_name);
             if ($f_type == 16 && $params->Tab[0] == "t_wares") {
                 $q->join("t_users", "t_users.f_code = t1.f_code", $user_cols);
             }
             if (count($tab_explode) < 3 && $f_type) {
                 $q->where("f_type_id IN ($f_type)");
             }
             $q->where("t1.f_code in ($an)");
         } else {
             $q->from(array('t1' => $tab), $main_cols)
                     ->join("t_creation_date", "t1.f_code = t_creation_date.f_id", $creation_cols)
                     ->join("t_wf_phases", "t_wf_phases.f_wf_id = t_creation_date.f_wf_id and t_wf_phases.f_number = t_creation_date.f_phase_id", array("f_name", "f_percentage"))
                     ->join("t_custom_fields", "t_custom_fields.f_code = t1.f_code", $cols_name);
             if ($f_type == 16 && $params->Tab[0] == "t_wares") {
                 $q->join("t_users", "t_users.f_code = t1.f_code", $user_cols);
             }
             $q->where("t1.f_code in ($an)");
         }

         $ris = $q->query()->fetchAll();
         $i = 0;
         $head = array();
         $arr = array();

         $codes_list = explode(',', $params->Ands);
         $tot_a = count($ris);
         $ris_copy = array();
         for ($a = 0; $a < $tot_a; ++$a) {

             // if it's a cross WARES-WORKORDERS of multipm type substitute fc_pm_next_due_date and fc_pm_subsequent_due_date with ware specific dates
             if (isset($ris[$a]['fc_pm_type']) && $ris[$a]['fc_pm_type'] == 'pm-per-asset' && (isset($ris[$a]['fc_pm_next_due_date']) || isset($ris[$a]['fc_pm_subsequent_due_date']))) {
                 $q->reset();
                 $q->from('t_periodics', array('f_start_date'))
                         ->where('f_asset_code =' . $params->SourceCode)
                         ->where('f_code = ' . $ris[$a]['f_code'])
                         ->where('f_executed = 0')
                         ->order('f_start_date')
                         ->limit(2);
                 $resPeriodicAssetDates = $q->query()->fetchAll();
                 // set fc_pm_next_due_date if exists
                 if (isset($ris[$a]['fc_pm_next_due_date'])) {
                     if (isset($resPeriodicAssetDates[0]['f_start_date'])) {
                         $ris[$a]['fc_pm_next_due_date'] = $resPeriodicAssetDates[0]['f_start_date'];
                     } else {
                         $ris[$a]['fc_pm_next_due_date'] = '';
                     }
                 }

                 // set fc_pm_next_subsequent_due_date if exists
                 if (isset($ris[$a]['fc_pm_subsequent_due_date'])) {
                     if (isset($resPeriodicAssetDates[1]['f_start_date'])) {
                         $ris[$a]['fc_pm_subsequent_due_date'] = $resPeriodicAssetDates[1]['f_start_date'];
                     } else {
                         $ris[$a]['fc_pm_subsequent_due_date'] = '';
                     }
                 }
             }

             $ris_copy[$ris[$a]['f_code']] = $ris[$a];
         }
         $ris = $ris_copy;
         $tot_a = count($codes_list);


         for ($a = 0; $a < $tot_a; ++$a) {
             $cmp = array();
             if (!isset($ris[$codes_list[$a]]))
                 continue;
             $data = $ris[$codes_list[$a]];
             foreach ($data as $k => $v) {
                 $cmp[] = Mainsim_Model_Utilities::chg($v);
                 if (!$i)
                     $head[] = $k;
             }
             if (!$i)
                 $arr[] = $head;
             $arr[] = $cmp;
             $i++;
         }
         if (strpos($tab, "_parent") !== false) {
             $array_code = explode(',', $an);
             $cmp = array();
             if (in_array(0, $array_code)) {
                 $q->reset();
                 //Recupero la configurazione                 
                 if (empty($head)) {
                     $head = $this->getBookmark($f_name, 'view', $params->module_name);
                     $res_head = json_decode(Mainsim_Model_Utilities::chg($head['f_properties']), true);
                     $head = array();
                     foreach ($res_head as $value) {
                         $head[] = $value[0];
                     }
                 }
                 $head_json = array();
                 foreach ($head as $key) {
                     $head_json[] = $key;
                     if ($key == 'f_title') {
                         $cmp[] = 'Root';
                     } elseif ($key == 'f_code') {
                         $cmp[] = 0;
                     } else {
                         $cmp[] = "";
                     }
                 }
                 if (empty($arr)) {
                     $arr[] = $head_json;
                 }
                 //$head
                 $arr[] = $cmp;
             }
         }
         return array("data" => $arr, "other" => array());
     }

     /**
      * Merge User selectors and params selectors
      * @param array $selectors list of selector in $params->Selector
      * @return array list of selectors merged
      */
     private function getSelectorsFromParamsAndUser(array $selectors) {
         $AllSels = !empty(Zend_Auth::getInstance()->getIdentity()->selectors) ? explode(',', Zend_Auth::getInstance()->getIdentity()->selectors) : [];
         foreach ($selectors as $value) {
             $AllSels = array_merge($AllSels, $value->s);
         }
         return $AllSels;
     }

     /**
      * Return objects in a tree grid
      * @global type $lang
      * @param type $params
      * @return type 
      */
     public function jax_read_child($params, $nobkm = false) {
         //prepare user params
         $user = Zend_Auth::getInstance()->getIdentity();
         $user_group_level = $user->f_group_level;
         $user_level = $user->f_level;
         if ($user_group_level != -1) {
             $user_level+=$user_group_level;
         }
         $treegridParams = array();
         //check if this user can see only his element        
         $fpdr = $params->Prnt;
         $u_selectors = $user->selectors ? explode(',', $user->selectors) : array();
         $uid = $user->f_id;
         $tab = $params->Tab[0];
         $f_type = $params->Tab[1];
         $rtree = $params->rtree;
         $tsstCode = new Zend_Session_Namespace("tsst_{$tab}_{$f_type}");
         $selectorList = $this->getSelectorsFromParamsAndUser((array) $params->Selector);
         //new! key to user selector support table
         if ($fpdr < 0) {
             $t_sst = isset($tsstCode->t_sst) ? $tsstCode->t_sst : 1;
             unset($_SESSION["tsst_{$tab}_{$f_type}"]['t_sst']);
             unset($_SESSION["tsst_{$tab}_{$f_type}"]['selectorClause']);
             $this->db->delete('t_selectors_support', "f_call = '{$t_sst}'");
             $t_sst = (microtime(true) * 10000) . '_' . $uid . '_' . rand(0, 9);
         } else {
             $t_sst = $tsstCode->t_sst;
         }
         $q = new Zend_Db_Select($this->db);
         $resParams = $this->prepareArrayTreegrid($params, $t_sst, $tsstCode);
         $treegridParams = $resParams['params'];
         $defaultTreegridParams = $resParams['defaultParams'];
         $hy = $resParams['hy'];
         $oth = array(
             'timestamp' => time(),
             'modify' => 0
         );

         $module_name = $params->module_name;

         $and = array();
         $f_timestamp = $params->timestamp;

         if ($hy && $f_timestamp) {
             $select_reverse = "SELECT COUNT(f_code) AS num FROM t_reverse_ajax 
                WHERE f_timestamp > $f_timestamp AND (f_old_parent = $fpdr OR f_parent = $fpdr)";
             $result_reverse = $this->db->query($select_reverse)->fetch();
             if ($result_reverse['num']) {
                 $oth['modify'] = 1;
             }
             $cols = array('mainTableCols' => array('num' => 'count(t1.f_code)'));
             $result_nch = $this->treegridQuery($tab, $f_type, $cols, $treegridParams);
             $oth['nch'] = $result_nch[0]['num'];
             $and[] = "t2.f_parent_code=" . $fpdr;
         } elseif ($hy) {
             $and[] = "t2.f_parent_code=" . $fpdr;
         }

         //if fpdr is not root and search array is empty, add parent
         $search = (array) $params->Search;
         $selector = (array) $params->Selector;
         $like = (array) $params->Like;
         $countlike = isset($like["Fields"]) ? count($like["Fields"]) : 0;

         if (($fpdr >= 0 && count($search) == 0 && count($selector) == 0 && $countlike == 0) && empty($u_selectors) || $fpdr > 0) {
             $treegridParams['where'] = array_merge($treegridParams['where'], $and);
         } elseif ($fpdr >= 0 && !empty($u_selectors) && $tab == 't_selectors') {//case t_selectors
             $treegridParams['where'] = array_merge($treegridParams['where'], $and);
         }
         //------------------------------------------------------------------------------        
         $arr = array();
         // if is not hierarchy or if padre==root return all elements per scrollbar e nch padre
         $ntot = $rtree;
         if ($fpdr < 0) {
             if ($hy) {
                 if ((empty($selector) && empty($u_selectors)) && empty($search) && (!$countlike)) {
                     $treegridParams['where'][] = "t2.f_parent_code = 0";
                 } elseif (!empty($u_selectors) && $tab == 't_selectors') {
                     $treegridParams['where'][] = "t2.f_parent_code = 0";
                 }
             }
             $cols = array('mainTableCols' => array('num' => 'count(t1.f_code)'));
             //Zend_Debug::dump($treegridParams,$hy);die; 
             $ris = $this->treegridQuery($tab, $f_type, $cols, $treegridParams);
             $ntot = $ris[0]['num'];
         } else {
             $q->reset();


             if (!$nobkm) {
                 $result_col = $this->getBookmark($f_name, 'view', $module_name);
                 $result_col = json_decode(Mainsim_Model_Utilities::chg($result_col['f_properties']), true);
             }

             $tab_columns = Mainsim_Model_Utilities::get_tab_cols($tab);
             $cols = array(
                 'mainTableCols' => array('f_code', 'f_type_id'),
                 'creationTableCols' => array('f_visibility', 'f_editability', 'f_wf_id'),
                 'wfPhasesTableCols' => array('f_edit' => 't_wf_phases.f_editability', 'f_percentage', 'f_name'),
                 'customTableCols' => array(),
                 'wfGroupsTableCols' => array(),
                 'userTableCols' => array(),
                 'mainParentTableCols' => array()
             );


             if (!$nobkm) {
                 foreach ($result_col as $line) {
                     if ($line[0] == 'f_code')
                         continue;
                     //check creation date column
                     if (in_array($line[0], $this->tab_creation) && !in_array($line[0], $cols['creationTableCols'])) {
                         $cols['creationTableCols'][] = $line[0];
                     }
                     //check default column
                     elseif (in_array($line[0], $tab_columns) && !in_array($line[0], $cols['mainTableCols'])) {
                         $cols['mainTableCols'][] = $line[0];
                     }
                     //check custom  column
                     elseif (in_array($line[0], $this->tab_custom) && !in_array($line[0], $cols['customTableCols'])) {
                         $cols['customTableCols'][] = $line[0];
                     }
                     //check user column
                     elseif (in_array($line[0], $this->tab_users) && !in_array($line[0], $cols['userTableCols'])) {
                         $cols['userTableCols'][] = $line[0];
                     }
                 }


                 // Sort            
                 if (count($params->Sort)) {
                     for ($r = 0; $r < count($params->Sort); $r++) {
                         if (empty($params->Sort[$r]->f))
                             continue;
                         $prefix = "";
                         if (in_array($params->Sort[$r]->f, $tab_columns)) {
                             $prefix = "t1.";
                         }
                         $treegridParams['order'][] = $prefix . $params->Sort[$r]->f . " " . $params->Sort[$r]->m;
                     }
                 }
             } // nobkm 

             if (!isset($treegridParams['order'])) {
                 $treegridParams['order'] = array("t1.f_code DESC");
             }


             $treegridParams['limit'] = array($params->Limit[1], $params->Limit[0]);
             $ris = $this->treegridQuery($tab, $f_type, $cols, $treegridParams);

             $pos = $params->Limit[0];
             $arr = array();
             if (!empty($ris)) {
                 $arr = $this->prepareResponseTreegrid($tab, $f_type, $fpdr, $hy, $pos, $rtree, $user_level, $ris, $defaultTreegridParams);
             }
         }

         if ($nobkm)
             return $arr;

         return array("ntot" => $ntot, "data" => $arr, "other" => $oth);
     }

     /**
      * If ENABLE_EXTERNAL_MANTAINER is 'On' and user level = 32, limit
      * visibility to only what is crossed for
      * @param type $treegridParams
      * @param type $userinfo
      */
     private function setExternalMantainer($tab, &$treegridParams, $userinfo) {
         $settings = Zend_Registry::get('settings');
         if (isset($settings['ENABLE_EXTERNAL_MANTAINER']) && $settings['ENABLE_EXTERNAL_MANTAINER'] == 'On' && ($userinfo->f_level != -1 && $userinfo->f_level & 32) != 0) {
             //limit visibility
             $tableCross = 't_ware_wo';
             $fieldCross = 'f_wo_id';
             $fieldWhere = $tab == 't_wares' ? 'f_code_ware_slave' : 'f_ware_id';
             if ($tab == 't_wares') {
                 $tableCross = 't_wares_relations';
                 $fieldCross = 'f_code_ware_master';
             } elseif ($tab == 't_selectors') {
                 $tableCross = 't_selector_ware';
                 $fieldCross = 'f_selector_id';
             } elseif ($tab == 't_systems') {
                 $tableCross = 't_wares_systems';
                 $fieldCross = 'f_system_id';
             }
             $treegridParams['join'][] = array(array("ext_cross" => $tableCross), "t1.f_code = ext_cross.{$fieldCross}", array());
             $treegridParams['where'][] = "ext_cross.{$fieldWhere} = {$userinfo->f_id}";
         }
     }

     /**
      * Prepare array with where clause for treegridQuery method
      * @param object $params : object with params from treegrid
      * @param string $sessionCode : code used as key in t_selectors_support if any Selector clause is passed
      * @param Zend_Session_Namespace $sessionCodeObject : session object used only in treegrid
      * @return array : return array with 3 keys : 
      *  1) hy (int) : 0 is not hierarchy otherwise 1
      *  2) params (array) : perché D. sorride sempre? array with the where clause for treegridQuery method. All clause are inside key 'where'
      *  3) defaultParams (array) : array used in jax_read_child only with partial where clause
      */
     public function prepareArrayTreegrid($params, $sessionCode = null, $sessionCodeObject = null) {
         //prepare user params        
         $user = Zend_Auth::getInstance()->getIdentity();
         $user_group_level = $user->f_group_level;
         $user_level = $user->f_level;
         $treegridParams = array();
         $defaultTreegridParams = array(); //contain default filter used during array preparation
         //check if this user can see only his element
         $self = isset($params->self) && $user_level != -1 ? $params->self : 0;
         $fpdr = !isset($params->Prnt) ? -1 : $params->Prnt;
         if ($user_group_level != -1) {
             $user_level+=$user_group_level;
         }
         $u_selectors = $user->selectors ? explode(',', $user->selectors) : array();
         $tab = $params->Tab[0];
         $f_type = $params->Tab[1];

         //new! key to user selector support table        
         $hy = $params->Hierarchy;
         //start to add default where clause
         $defaultTreegridParams['where'] = $this->createDefaultWhereTg($f_type, $self);
         $treegridParams['where'] = $defaultTreegridParams['where'];
         //check external mantainer flag setted and user level = 32        
         $this->setExternalMantainer($tab, $treegridParams, $user);
         $invisible = false; //Variable used to see invisible element (Closed and deleted group);
         $filter = array();
         if (count($params->Filter)) {
             //    $hy = 0;
             $filter = $this->createFilterArray($params->Filter, $tab);
         }
         //merge default where with filter array
         $treegridParams['where'] = array_merge($treegridParams['where'], $filter);

         // Ands  
         $ands = array();
         if (count($params->Ands)) {
             $resAnds = $this->createAndsArray($params->Ands, $tab);
             $ands = $resAnds['andsList'];
             $invisible = $resAnds['invisible'];
         }
         //merge default where with Ands array
         $treegridParams['where'] = array_merge($treegridParams['where'], $ands);

         //if invisile variable is true, show element with f_visibility in t_wf_grouos = 0
         if ($invisible) {
             $treegridParams['where'][] = "(t_wf_groups.f_visibility & $user_level) != 0 OR (t_wf_groups.f_visibility = 0)";
             $defaultTreegridParams['where'][] = "(t_wf_groups.f_visibility & $user_level) != 0 OR (t_wf_groups.f_visibility = 0)";
         } else { //else check f_visibility with user level
             $treegridParams['where'][] = "(t_wf_groups.f_visibility & $user_level) != 0";
             $defaultTreegridParams['where'][] = "(t_wf_groups.f_visibility & $user_level) != 0";
         }

         $search = array();
         if (count($params->Search) && $fpdr <= 0) {
             //    $hy = 0;
             $search = $this->createSearchArray($params->Search, $tab, $user);
         }

         //merge default where with Searc array
         $treegridParams['where'] = array_merge($treegridParams['where'], $search);


         $like = array();
         if (count($params->Like) && $fpdr <= 0) {
             //  $hy = 0;
             $like = $this->createLikeArray($params->Like, $tab, $user);
         }

         //merge default where with Searc array
         $treegridParams['where'] = array_merge($treegridParams['where'], $like);


         //Selectors    
         $checkSel = (array) $params->Selector;
         $selectors = !empty($checkSel) ? $params->Selector : array();
         $selector_clause = array();
         if ($tab != 't_wares' && $f_type != '16') {
             if (!is_null($sessionCode) && (!empty($selectors) || !empty($u_selectors)) && $fpdr < 0 && $tab != 't_selectors') {
                 $ins_t_sst = $this->db->quote($sessionCode);
                 $resSelector = $this->createSelectorsArray($tab, $ins_t_sst, $fpdr, $hy, $f_type, $selectors, $u_selectors);
                 if (isset($resSelector['clause'])) {
                     $selector_clause = $resSelector['clause'];
                     if (!is_null($sessionCodeObject)) {
                         $sessionCodeObject->t_sst = $sessionCode;
                         $sessionCodeObject->selectorClause = $resSelector['clause'];
                     }
                 }
             } elseif ((!empty($selectors) || !empty($u_selectors)) && $fpdr <= 0 && $tab != 't_selectors' && !is_null($sessionCodeObject)) {
                 $selector_clause = is_array($sessionCodeObject->selectorClause) ? $sessionCodeObject->selectorClause : array();
             }
             $treegridParams['where'] = array_merge($treegridParams['where'], $selector_clause);
         }
         $resParams = array('params' => $treegridParams, 'hy' => $hy, 'defaultParams' => $defaultTreegridParams);
         return $resParams;
     }

     /**
      * Parse Selectors array and user_selector (if passed) from treegrid params and return where clause to use in treegrid query
      * @param string $tab
      * @param int $t_sst
      * @param int $fpdr
      * @param string $f_type
      * @param object $selectors
      * @param array $u_selectors     
      * @return type
      */
     public function createSelectorsArray($tab, $t_sst, $fpdr, $hy, $f_type, $selectors = [], $u_selectors = []) {
         $tabp = $tab . '_parent';
         $select = new Zend_Db_Select($this->db);
         $selector_clause = array();
         //set user selectors
         $selector_types = array();
         $code_cross = array();
         $select->from("t_selectors", 'f_type_id');
         foreach ($u_selectors as $sel_line) {
             $select->reset(Zend_Db_Select::WHERE);
             $res_sel = $select->where("f_code = ?", $sel_line)->query()->fetch();
             $selector_types[$res_sel['f_type_id']][$sel_line] = (int) $sel_line;
         }

         $selectors_where = "";
         $t_selector_cross = '';
         $f_selector_cross = '';
         if ($tab == "t_wares") {
             $t_selector_cross = 't_selector_ware';
             $f_selector_cross = 'f_ware_id';
         } elseif ($tab == "t_workorders") {
             $t_selector_cross = 't_selector_wo';
             $f_selector_cross = 'f_wo_id';
         } elseif ($tab == "t_systems") {
             $t_selector_cross = 't_selector_system';
             $f_selector_cross = 'f_system_id';
         }

         $uSelRes = [];
         foreach ($selector_types as $key_type => $line_type) {
             $uSelRes[$key_type] = [];
             $select->reset();
             //get all elements crossed with selectors in $line_type or not crossed with selector of type in $key_type
             $select->from(['t1' => $tab], ['f_code', 'children' => "(SELECT count(*) from $tabp where f_parent_code = t1.f_code)"])
                     ->join(['t2' => $tabp], "t1.f_code = t2.f_code", ['f_parent_code'])
                     ->join(['t3' => $t_selector_cross], "t1.f_code = t3.{$f_selector_cross}", ['f_selector_id'])
                     ->where("f_type_id IN ({$f_type})")->where("f_selector_id IN (?)", $line_type)
                     ->where("f_nesting_level = 1")->where('f_active = 1')->order('f_selector_id desc')->order('f_parent_code asc');
             $resAssoc = $select->query()->fetchAll();
             $resAssocClear = array_values(array_unique(array_map(function($el) {
                                 return (int) $el['f_code'];
                             }, $resAssoc)));
             $totA = count($resAssoc);
             $notAllow = [];
             for ($a = 0; $a < $totA; ++$a) {
                 if (in_array($resAssoc[$a]['f_code'], $notAllow)) {
                     continue;
                 }
                 $uSelRes[$key_type][$resAssocClear[$a]] = $resAssocClear[$a];
                 $children = [];
                 if ($resAssoc[$a]['children'] > 0) {
                     $children = Mainsim_Model_Utilities::getChildCode($tabp, $resAssoc[$a]['f_code'], $children, $this->db, true);
                 }
                 if ($hy == 0) {
                     $uSelRes[$key_type] = array_merge($uSelRes[$key_type], $children);
                 }
                 $notAllow = array_merge($notAllow, $children);
             }
         }
         //print_r($uSelRes);
         $fCodeList = [];
         $uSelRes = array_values($uSelRes);
         //print_r($uSelRes); 
         $totB = count($uSelRes);
         //print("\n------------\n");
         if ($totB > 0) {
             $fCodeList = $uSelRes[0];
             for ($b = 0; $b < $totB; ++$b) {
                 //$fCodeList = array_intersect($fCodeList, $uSelRes[$b]);
                 $fCodeList = array_merge($fCodeList, $uSelRes[$b]);
                 //print_r($fCodeList);
             }
         }

         // get items without selectors associated

         $select->reset();
         $select->from(['t1' => $tab], ['f_code', 'children' => "(SELECT count(*) from $tabp where f_parent_code = t1.f_code)"])
                 ->join(['t2' => $tabp], "t1.f_code = t2.f_code", ['f_parent_code'])
                 ->where('t1.f_code NOT IN (SELECT DISTINCT(' . $f_selector_cross . ') FROM ' . $t_selector_cross . ")")
                 ->where("f_type_id IN ({$f_type})")
                 ->where("f_parent_code = 0")
                 ->order('f_parent_code asc');
         $resNoSel = $select->query()->fetchAll();
         if (is_array($resNoSel)) {
             for ($z = 0; $z < count($resNoSel); $z++) {
                 $fCodeList[] = $resNoSel[$z]['f_code'];
             }
         }
         //print_r($fCodeList);
         //$resNoSel = $select->query()->fetchAll();

         $this->fillSelectorsSupport($t_sst, $fCodeList);
         $complete_code_list = []; //list of element associate to selected selector                
         //------------------------------------------------------------------
         //selectors
         $selector_types = [];
         $select->reset();
         $select->from("t_selectors", 'f_type_id');
         foreach ($selectors as $key_type => $sel_line) {
             if (!empty($sel_line->s)) {
                 $select->reset(Zend_Db_Select::WHERE);
                 $res_sel = $select->where("f_code = ?", $sel_line->s[0])->query()->fetch();
                 $selector_types[$res_sel['f_type_id']]['f_codes'] = array_map(function($el) {
                     return (int) $el;
                 }, $sel_line->s);
                 $selector_types[$res_sel['f_type_id']]['hy'] = $sel_line->h;
             }
         }
         foreach ($selector_types as $key_type => $line_type) {
             $code_list = $line_type['f_codes'];
             if ($line_type['hy'] == 0) { //if hy == 0, explode selector tree and get children
                 $code_list = Mainsim_Model_Utilities::getChildCode("t_selectors_parent", $code_list, $code_list, $this->db);
             }

//             $select->reset();
//             //get elements crossed with passed selectors
//             $select->from(array("t1" => $t_selector_cross), array("f_code" => $f_selector_cross))
//                     ->join(array("t2" => $tab), "t2.f_code = $f_selector_cross", array())
//                     ->join(array("t4" => $tabp), "t4.f_code = t2.f_code", array('f_parent_code'))
//                     ->where("f_selector_id IN ( ? )", $code_list)
//                     ->group($f_selector_cross)->group('f_parent_code')->where("f_type_id IN ($f_type)")
//                     ->order("$f_selector_cross ASC");
//             if (!empty($uSelRes)) {
//                 $select->join(array('t3' => 't_selectors_support'), "t2.f_code = t3.f_code", array())
//                         ->where("f_call = $t_sst");
//             }
//             $res_sel_cross = $select->query()->fetchAll();
             $select->reset();
             $select->from(["t1"=>"t_creation_date"], [])
                     ->join(["t2"=>$tab], "t2.f_code = t1.f_id", ['f_code'])
                     ->join(array("t4" => $tabp), "t4.f_code = t2.f_code", array('f_parent_code'))
                     ->where("t1.fc_hierarchy_selectors like '%".$line_type['f_codes'][0]."%'")
                     ->where("t2.f_type_id IN ($f_type)");
             if (!empty($uSelRes)) {
                 $select->join(array('t3' => 't_selectors_support'), "t2.f_code = t3.f_code", array())
                         ->where("f_call = $t_sst");
             }
             $res_sel_cross = $select->query()->fetchAll();
             $res_sel_cross_list = array_map(function($el) {
                 return (int) $el['f_code'];
             }, $res_sel_cross);
             for ($b = 0; $b < $totB; ++$b) {
                 if ($res_sel_cross[$b]['f_parent_code'] == 0) {
                     continue;
                 }
                 $parentList = Mainsim_Model_Utilities::getParentCode($tabp, $res_sel_cross[$b]['f_code'], array(), $this->db);
                 //remove f_parent_code = 0
                 array_pop($parentList);
                 $notInList = array_diff($res_sel_cross_list, $parentList);
                 if (count($notInList) != count($res_sel_cross)) {
                     unset($res_sel_cross[$b]);
                 }
             }

             if (count($res_sel_cross_list) > 0 && $hy == 0) {
                 $pos_trunk = 0;
                 $length = 1000;
                 do {
                     $not_end = true;
                     $trunk = array_slice($res_sel_cross_list, $pos_trunk, $length);
                     $tot_c = count($trunk);
                     if ($tot_c < $length)
                         $not_end = false;
                     $search_parent_trunk = array();
                     for ($c = 0; $c < $tot_c; ++$c) {
                         $line_code_list = $trunk[$c];
                         $code_cross[$key_type][$line_code_list] = $line_code_list;
                         $search_parent_trunk[$line_code_list] = $line_code_list;
                         $select->reset();
                     }
                     if (!empty($search_parent_trunk)) {
                         $code_cross[$key_type] = Mainsim_Model_Utilities::getChildCode($tabp, $search_parent_trunk, $code_cross[$key_type], $this->db, true);
                     } else {// create empty row in array to check with other selector                                									
                         $code_cross[$key_type] = array();
                     }
                     $pos_trunk += $length;
                 } while ($not_end);
             } else {
                 $code_cross[$key_type] = $res_sel_cross_list;
             }
             /* $res_sel_cross = $select->query()->fetchAll();            
               $totB = count($res_sel_cross);
               for($b = 0;$b < $totB;++$b) {
               if($res_sel_cross[$b]['f_parent_code'] == 0) { continue;}
               $parentList = Mainsim_Model_Utilities::getParentCode($tabp, $res_sel_cross[$b]['f_code'], array(), $this->db);
               //remove f_parent_code = 0
               array_pop($parentList);
               $notInList = array_diff($res_sel_cross,$parentList);
               if(count($notInList) != count($res_sel_cross)){
               unset($res_sel_cross[$b]);
               }
               }

               $res_sel_cross = array_map(function($el){return (int)$el['f_code'];}, $res_sel_cross);

               if(count($res_sel_cross) > 0 && $hy == 0) {
               $pos_trunk = 0;$length = 1000;
               do {
               $not_end = true;
               $trunk = array_slice($res_sel_cross, $pos_trunk,$length);
               $tot_c = count($trunk);
               if($tot_c < $length) $not_end = false;
               $search_parent_trunk = array();
               for($c = 0;$c < $tot_c;++$c) {
               $line_code_list = $trunk[$c];
               $code_cross[$key_type][$line_code_list] = $line_code_list;
               $search_parent_trunk[$line_code_list] = $line_code_list;
               $select->reset();
               }
               if(!empty($search_parent_trunk)) {
               $code_cross[$key_type] = Mainsim_Model_Utilities::getChildCode($tabp, $search_parent_trunk, $code_cross[$key_type],$this->db,true);
               }
               else {// create empty row in array to check with other selector
               $code_cross[$key_type] = array();
               }
               $pos_trunk += $length;
               }while($not_end);
               }
               else {
               $code_cross[$key_type] = $res_sel_cross;
               } */
         }

         if (!empty($selector_types)) {
             $this->db->delete("t_selectors_support", "f_call = $t_sst");
         }
         if (!empty($code_cross)) {//intersect array of different selector's types.
             if (count($code_cross) == 1) { // if only one selector
                 $complete_code_list = array_pop($code_cross);
             } else { // search common f_code           
                 $code_cross = array_values($code_cross);
                 $totC = count($code_cross);
                 $complete_code_list = isset($code_cross[0]) ? $code_cross[0] : array();
                 for ($c = 1; $c < $totC; ++$c) {
                     $complete_code_list = array_intersect($complete_code_list, $code_cross[$c]);
                 }
             }
             $this->fillSelectorsSupport($t_sst, $complete_code_list);
         }
         $clause = " t1.f_code in (select f_code from t_selectors_support where f_call = $t_sst )";
         if ($hy == 0) {
             $clause = "t2.f_parent_code in (select f_code from t_selectors_support where f_call = $t_sst) OR " . $clause;
         }
         if (!empty($uSelRes) || !empty($selector_types)) {
             $selector_clause['clause'][] = $clause;
         }
         return $selector_clause;
     }

     /**
      * Massive f_code insert into t_selectors_support (for treegrid)
      * @param string $t_sst id for inserted line
      * @param array $complete_code_list list of f_code
      */
     private function fillSelectorsSupport($t_sst, $complete_code_list) {
         $pos_trunk = 0;
         $length = 950;
         $time = time();
         do {
             $t_sst_list = array();
             $insert_query = "INSERT INTO t_selectors_support(f_code,f_call,f_timestamp) VALUES ";
             $not_end = true;
             $trunk = array_slice($complete_code_list, $pos_trunk, $length);
             $tot_cw = count($trunk);
             if ($tot_cw < $length)
                 $not_end = false;
             for ($cw = 0; $cw < $tot_cw; ++$cw) {
                 if (empty($trunk[$cw])) {
                     continue;
                 }
                 $t_sst_list[] = "(" . $trunk[$cw] . "," . $t_sst . ",$time)";
             }
             $t_sst_insert = implode(',', $t_sst_list);
             $insert_query .= $t_sst_insert;
             $pos_trunk += $length;
             if (!empty($t_sst_list)) {
                 $this->db->query($insert_query);
             }
         } while ($not_end);
     }

     /**
      * Parse Ands array from treegrid params and return where clause to use in treegrid query
      * @param type $andsList
      * @param type $tab
      * @return string
      */
     public function createAndsArray($andsList, $tab) {
         $user = Zend_Auth::getInstance()->getIdentity();
         $user_group_level = $user->f_group_level;
         $user_level = $user->f_level;
         if ($user_group_level != -1) {
             $user_level+=$user_group_level;
         }
         $select = new Zend_Db_Select($this->db);
         $tab_columns = Mainsim_Model_Utilities::get_tab_cols($tab);
         $ands = array('invisible' => false);
         $tot = count($andsList);
         for ($r = 0; $r < $tot; $r++) {
             if (strpos($andsList[$r]->f, '.') !== false) {
                 if ($andsList[$r]->f == 't_wf_phases.f_summary_name') {
                     /**
                      * All and inbox are two different cases
                      */
                     if (strpos($andsList[$r]->z, "all") === false && strpos($andsList[$r]->z, "inbox") === false && strpos($andsList[$r]->z, "inboxFromMe") === false) {
                         $select->reset();
                         $res_group = $select->from(array('t1' => "t_wf_groups"), 'f_visibility')
                                         ->join(array('t2' => 't_wf_phases'), 't1.f_id = t2.f_group_id', array())
                                         ->where("t2.f_summary_name {$andsList[$r]->z}")->query()->fetchAll();
                         $totRG = count($res_group);
                         for ($iRG = 0; $iRG < $totRG; ++$iRG) {
                             if ($res_group[$iRG]['f_visibility'] == 0) {
                                 $ands['invisible'] = true;
                                 break;
                             }
                         }
                         $ands['andsList'][] = $andsList[$r]->f . " " . $andsList[$r]->z;
                     } elseif (strpos($andsList[$r]->z, "inboxFromMe") !== false) {
                         $ands['andsList'][] = "(t_creation_date.f_creation_user = {$user->f_id})";
                     } elseif (strpos($andsList[$r]->z, "inbox") !== false) {
                         $ands['andsList'][] = "EXISTS(select f_id from t_ware_wo where f_wo_id = t1.f_code and f_ware_id = {$user->f_id})"; //"(t_creation_date.f_editability & $user_level) != 0";
                     }
                 } else {
                     $ands['andsList'][] = $andsList[$r]->f . " " . $andsList[$r]->z;
                 }
             } elseif (in_array($andsList[$r]->f, $this->tab_creation)) { // check if is a field of t_creation_date
                 $ands['andsList'][] = "t_creation_date." . $andsList[$r]->f . " " . $andsList[$r]->z;
             } elseif (in_array($andsList[$r]->f, $tab_columns)) {
                 $ands['andsList'][] = "t1." . $andsList[$r]->f . " " . $andsList[$r]->z;
             } elseif (in_array($andsList[$r]->f, $this->tab_custom)) {
                 $ands['andsList'][] = "t_custom_fields." . $andsList[$r]->f . " " . $andsList[$r]->z;
             } elseif (in_array($andsList[$r]->f, $this->tab_users)) {
                 $ands['andsList'][] = "t_users." . $andsList[$r]->f . " " . $andsList[$r]->z;
             }
         }
         return $ands;
     }

     /**
      * Parse Search array from treegrid params and return where clause to use in treegrid query
      * @param array $search
      * @param string $tab
      * @param Zend_Auth $user
      * @return array
      */
     public function createSearchArray($search, $tab, $user) {
         $q = new Zend_Db_Select($this->db);
         $tab_columns = Mainsim_Model_Utilities::get_tab_cols($tab);
         $string = [
             ['cond' => 'like', 'before' => '%', 'after' => '%'], ['cond' => 'not like', 'before' => '%', 'after' => '%'], ['cond' => 'like', 'before' => '', 'after' => '%'],
             ['cond' => 'like', 'before' => '%', 'after' => ''], ['cond' => '=', 'before' => '', 'after' => ''], ['cond' => '!=', 'before' => '', 'after' => ''],
             ['cond' => 'empty'], ['cond' => 'notempty']
         ];
         $numeric = [['cond' => '='], ['cond' => '!='], ['cond' => '>'], ['cond' => '<'], ['cond' => 'empty'], ['cond' => 'notempty']];
         $date = [['cond' => '<'], ['cond' => '>'], ['cond' => '='], ['cond' => '!='], ['cond' => 'between'], ['cond' => 'empty'], ['cond' => 'notempty']];
         $asrc = array();
         $tot = count($search);
         $translator = new Mainsim_Model_Translate();
         for ($r = 0; $r < $tot; $r++) {
             $sel = $search[$r]->Sel;
             $typ = $search[$r]->type;
             $mn = $search[$r]->Mm;
             $v1 = mb_convert_encoding($search[$r]->v1, 'CP1252', 'BASE64');
             $v0 = mb_convert_encoding($search[$r]->v0, 'CP1252', 'BASE64');
             $f = $search[$r]->f;
             //check if is a phase name
             if ($f == 'f_name') {
                 $lang = $translator->getLang();

                 $resLang = $translator->getLanguage($lang);
                 $possible_res = array_filter($resLang, function($el) use ($v0) {
                     return ( strpos(strtolower($el), strtolower($v0)) !== false );
                 });

                 $possible_res[$v0] = $v0;
                 if (is_array($possible_res)) {
                     $possible_res = array_keys($possible_res);
                     $f_name_where = array();
                     foreach ($possible_res as $line_poss) {
                         $q->reset();
                         $res_exist = $q->from("t_wf_phases")->where("f_name like '%$line_poss%'")
                                         ->query()->fetchAll();
                         if (count($res_exist)) {
                             if ($typ == 0) {
                                 $f_name_where[] = $this->db->quoteInto("($f {$string[$sel]['cond']} ?) ", "{$string[$sel]['before']}{$line_poss}{$string[$sel]['after']}");
                             }
                         }
                     }
                     if (empty($f_name_where)) {
                         $asrc[] = "( 0=1)";
                     } else {
                         $asrc[] = "( " . implode(" OR ", $f_name_where) . " )";
                     }
                 }
                 continue;
             }
             if (in_array($f, $tab_columns)) {
                 $f = 't1.' . $f;
             } elseif (in_array($f, $this->tab_creation)) {
                 $f = 't_creation_date.' . $f;
             } elseif (in_array($f, $this->tab_custom)) {
                 $f = 't_custom_fields.' . $f;
             } elseif (in_array($f, $this->tab_users)) {
                 $f = 't_users.' . $f;
             }

             if ($mn && Mainsim_Model_Utilities::isMysql()) {
                 $f = "BINARY(" . $f . ")";
             } elseif ($mn && !Mainsim_Model_Utilities::isMysql()) {
                 $resCollate = $this->db->query("SELECT cast(SERVERPROPERTY ('Collation') as varchar(50)) as coll")->fetch();
                 $collation = str_replace("_CI_", "_CS_", $resCollate['coll']);
                 $f = "$f COLLATE {$collation}";
             }
             // testo
             if ($typ == 0 && isset($string[$sel])) {
                 if ($string[$sel]['cond'] == 'empty') {
                     $asrc[] = $this->createEmptyWhere($f, "''");
                 } elseif ($string[$sel]['cond'] == 'notempty') {
                     $asrc[] = $this->createEmptyWhere($f, "''", false);
                 } else {
                     $asrc[] = $this->db->quoteInto("($f {$string[$sel]['cond']} ?) ", "{$string[$sel]['before']}{$v0}{$string[$sel]['after']}");
                 }
                 continue;
             }

             // numeric
             if ($typ == 1 && isset($numeric[$sel])) {
                 if ($numeric[$sel]['cond'] == 'empty') {
                     $asrc[] = $this->createEmptyWhere($f, -123456789);
                 } elseif ($numeric[$sel]['cond'] == 'notempty') {
                     $asrc[] = $this->createEmptyWhere($f, -123456789, false);
                 } else {
                     $asrc[] = $this->db->quoteInto("$f {$numeric[$sel]['cond']} ? ", $v0);
                 }
                 continue;
             }

             // data
             if ($typ == 3 && isset($date[$sel])) {
                 if ($date[$sel]['cond'] == 'empty') {
                     $asrc[] = $this->createEmptyWhere($f, 0);
                 } elseif ($date[$sel]['cond'] == 'notempty') {
                     $asrc[] = $this->createEmptyWhere($f, 0, false);
                 } elseif ($date[$sel]['cond'] == 'between') {
                     $asrc[] = "$f between $v0 and $v1";
                 } elseif ($date[$sel]['cond'] == '=') {
                     $asrc[] = "$f between ". mktime(0, 0, 0, date('n',$vo), date('j',$vo),date('Y',$vo)) . " and " . mktime(0, 0, 0, date('n',$vo), date('j',$vo)+1,date('Y',$vo));
                 } else {
                     $asrc[] = $this->db->quoteInto("$f {$date[$sel]['cond']} ? ", $v0);
                 }
             }
         }
         return $asrc;
     }

     /**
      * Parse Search array from treegrid params and return where clause to use in treegrid query
      * @param array $search
      * @param string $tab
      * @param Zend_Auth $user
      * @return array
      */
     public function createLikeArray($like, $tab, $user) {

         $q = new Zend_Db_Select($this->db);
         $tab_columns = Mainsim_Model_Utilities::get_tab_cols($tab);

         $asrc = array();
         $aor = array();
         $tot = count($like->Fields);

         $translator = new Mainsim_Model_Translate();
         $v0 = mb_convert_encoding($like->Text, 'CP1252', 'BASE64');
         $av0 = explode("||", $v0);

         for ($i = 0; $i < count($av0); $i++) {
             $v0 = $av0[$i];
             if ($v0 == "")
                 continue;
             for ($r = 0; $r < $tot; $r++) {

                 $f = $like->Fields[$r];

                 //check if is a phase name
                 if ($f == 'f_name') {
                     $lang = $translator->getLang();
                     $resLang = $translator->getLanguage($lang);

                     $possible_res = array_filter($resLang, function($el) use ($v0) {
                         return ( strpos(strtolower($el), strtolower($v0)) !== false );
                     });

                     $possible_res[$v0] = $v0;
                     if (is_array($possible_res)) {
                         $possible_res = array_keys($possible_res);
                         $f_name_where = array();
                         foreach ($possible_res as $line_poss) {
                             $q->reset();
                             $res_exist = $q->from("t_wf_phases")->where("f_name like '%$line_poss%'")
                                             ->query()->fetchAll();
                             if (count($res_exist)) {

                                 $f_name_where[] = $this->db->quoteInto("($f like ?) ", "%{$line_poss}%");
                             }
                         }
                         if (empty($f_name_where)) {
                             $aor[] = "( 0=1)";
                         } else {
                             $aor[] = "( " . implode(" OR ", $f_name_where) . " )";
                         }
                     }
                     continue;
                 }

                 if (in_array($f, $tab_columns)) {
                     $f = 't1.' . $f;
                 } elseif (in_array($f, $this->tab_creation)) {
                     $f = 't_creation_date.' . $f;
                 } elseif (in_array($f, $this->tab_custom)) {
                     $f = 't_custom_fields.' . $f;
                 } elseif (in_array($f, $this->tab_users)) {
                     $f = 't_users.' . $f;
                 }

                 $aor[] = $this->db->quoteInto("($f like ?) ", "%{$v0}%");
             }
         }

         if (count($aor)) {
             $asrc[] = implode(" OR ", $aor);
         }
         return $asrc;
     }

     /**
      * Create clause for empty value for MySQL and
      * SQL Server
      */
     private function createEmptyWhere($field, $refValue, $empty = true) {
         $isMysql = Mainsim_Model_Utilities::isMysql();
         $operator = $empty ? '=' : '<>';
         $query = "IFNULL($field,$refValue) $operator $refValue";
         if (!$isMysql) {
             $query = "ISNULL($field,$refValue) $operator $refValue";
         }//sql server
         return $query;
     }

     /**
      * Parse filter array from treegrid and return where clause
      * @param type $filter
      * @param type $tab
      * @return string
      */
     
     public function createFilterArray($filter, $tab) {
         $tab_columns = Mainsim_Model_Utilities::get_tab_cols($tab);
         $aflt = array();
         $tot = count($filter);
         for ($r = 0; $r < $tot; $r++) {
             $prefix = "";
             if (in_array($filter[$r]->f, $tab_columns)) {
                 $prefix = "t1.";
             } elseif (in_array($filter[$r]->f, $this->tab_custom)) {
                 $prefix = "t_custom_fields.";
             } elseif (in_array($filter[$r]->f, $this->tab_creation)) {
                 $prefix = "t_creation_date.";
             } elseif (in_array($filter[$r]->f, $this->tab_users)) {
                 $prefix = "t_users.";
             }
             if ($filter[$r]->ty == 2) {
                 $operator = $filter[$r]->v == 1 ? '=' : '<>';
                 //if database is Mysql use ifnull function, otherwise isnull
                 $function = Mainsim_Model_Utilities::isMysql() ? "ifnull" : "isnull";
                 $aflt[] = $function . "(" . $prefix . $filter[$r]->f . ",0) $operator 1";
             } elseif ($filter[$r]->ty == 4) {
                 $aflt[] = $prefix . $filter[$r]->f . "='" . $filter[$r]->v . "'";
             } else {
                 switch ($filter[$r]->v) {
                     case 0: //TODAY
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(0, 0, 0) . " and " . mktime(23, 59, 59);
                         break;
                     case 1: //YESTERDAY
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(0, 0, 0, date('n'), date('j') - 1) . " and " . mktime(23, 59, 59, date('n'), date('j') - 1);
                         break;
                     case 2: //TOMORROW
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(0, 0, 0, date('n'), date('j') + 1) . " and " . mktime(23, 59, 59, date('n'), date('j') + 1);
                         break;
                     case 3: //THIS WEEK
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(0, 0, 0, date('n'), date('j') - date('N') + 1) . " and " . mktime(23, 59, 59, date('n'), date('j') - date('N') + 7);
                         break;
                     case 4: //LAST WEEK
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(0, 0, 0, date('n'), date('j') - date('N') + 1 - 7) . " and " . mktime(23, 59, 59, date('n'), date('j') - date('N'));
                         break;
                     case 5: //NEXT WEEK                        
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(23, 59, 59, date('n'), date('j') - date('N') + 8) . " and " . mktime(0, 0, 0, date('n'), date('j') - date('N') + 14);
                         break;
                     case 6: //THIS MONTH
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(0, 0, 0, date('n'), 1) . " and " . mktime(0, 0, 0, date('n') + 1, 0);
                         break;
                     case 7: //LAST MONTH
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(0, 0, 0, date('n') - 1, 1) . " and " . mktime(23, 59, 59, date('n'), 0);
                         break;
                     case 8: //NEXT MONTH
                         $aflt[] = "$prefix" . $filter[$r]->f . " between " . mktime(0, 0, 0, date('n') + 1, 1) . " and " . mktime(23, 59, 59, date('n') + 2, 0);
                         break;
                     case 9: //THIS YEAR
                         $aflt[] = $prefix . $filter[$r]->f . " between " . mktime(0, 0, 0, 1, 1, date('Y')) . " and " . mktime(23, 59, 59, 12, 31, date('Y'));
                         break;
                     case 10: //LAST YEAR
                         $aflt[] = $prefix . $filter[$r]->f . " between " . mktime(0, 0, 0, 1, 1, date('Y') - 1) . " and " . mktime(23, 59, 59, 12, 31, date('Y') - 1);
                         break;
                     case 11: //LAST 30 GG
                         $aflt[] = $prefix . $filter[$r]->f .  " >= " . (mktime(0, 0, 0)-(86400*30));
                          break;
                     case 12: //LAST 365 gg
                         $aflt[] = $prefix . $filter[$r]->f .  " >= " . (mktime(0, 0, 0)-(86400*365));
                          break;
                 }
             }
         }
         return $aflt;
     }

     /**
      * 
      * @param string $tab
      * @param string $f_type
      * @param int $fpdr
      * @param int $hy
      * @param int $pos
      * @param string $rtree
      * @param array $ris
      * @param array $treegridParams : array with filter for treegrid query
      */
     private function prepareResponseTreegrid($tab, $f_type, $fpdr, $hy, $pos, $rtree, $user_level, $ris, $treegridParams) {
         $head = array("fpdr", "nch", "selected", "fpos", "rtree");
         $defaultParams = $treegridParams;
         $tot = count($ris);
         for ($i = 0; $i < $tot; ++$i) {
             $treegridParams = $defaultParams;
             //foreach($ris as $data){
             $data = $ris[$i];
             $cmp = array($fpdr, 0, 0, $pos, $rtree);
             foreach ($data as $k => &$v) {
                 if ($k == "f_editability") {
                     $f_wf_edit = $data['f_edit'];
                     if (!(((int) $v & (int) $f_wf_edit & (int) $user_level))) {
                         $v = 0;
                     } else {
                         $v = 1;
                     }
                     $cmp[] = $v;
                     if (!$i)
                         $head[] = $k;
                     continue;
                 }
                 $cmp[] = Mainsim_Model_Utilities::chg($v);
                 if (!$i) {
                     $head[] = $k;
                 }
             }

             // nch: 0 foglia  >0 numero child 
             if ($hy) {
                 /* $q->reset();
                   $q->from(array("t2"=>$tabp),array("num"=>"count(*)"))
                   ->join(array("t1"=>$tab),"t1.f_code = t2.f_code",array())
                   ->join("t_creation_date","t_creation_date.f_id = t1.f_code",array())
                   ->join("t_wf_phases","t_creation_date.f_phase_id = t_wf_phases.f_number and t_creation_date.f_wf_id = t_wf_phases.f_wf_id",array())
                   ->join("t_wf_groups","t_wf_phases.f_group_id = t_wf_groups.f_id",array())
                   ->join(array("t3"=>"t_custom_fields"),"t3.f_code = t_creation_date.f_id",array())
                   ->where("t2.f_parent_code=".$data["f_code"])
                   ->where("t2.f_active=1")
                   ->where("(t_creation_date.f_visibility & $user_group_level) != 0");
                   if($user_group_level != -1) {
                   $q->where("(t_creation_date.f_visibility & $user_level) != 0");
                   }
                   if($self & $user_level) {
                   $q->where("(t_creation_date.f_creation_user = $uid)");
                   }
                   $q->where("(t_wf_phases.f_visibility & $user_level) != 0");
                   if($invisible) {
                   $q->where("(t_wf_groups.f_visibility & $user_level) != 0 OR (t_wf_groups.f_visibility = 0)");
                   }
                   else {
                   $q->where("(t_wf_groups.f_visibility & $user_level) != 0");
                   }
                   $zis = $q->query()->fetchAll();
                  */
                 $treegridParams['where'][] = "t2.f_parent_code = " . $data["f_code"];
                 $cols = array('mainParentTableCols' => array('num' => 'count(*)'));
                 $zis = $this->treegridQuery($tab, $f_type, $cols, $treegridParams);
                 $cmp[1] = (int) $zis[0]['num'];
                 //$treegridParams = $defaultParams;
                 //$cmp[1]=(int) $zis[0]['num'];                  
                 // multi padre!
                 $pdr = array();
                 /* $q->reset();
                   $q->from(array("t2"=>$tabp),array("f_parent_code"))
                   ->join(array("t1"=>$tab),"t1.f_code = t2.f_code",array())
                   ->join("t_creation_date","t_creation_date.f_id = t1.f_code",array())
                   ->join("t_wf_phases","t_creation_date.f_phase_id = t_wf_phases.f_number and t_creation_date.f_wf_id = t_wf_phases.f_wf_id",array())
                   ->join("t_wf_groups","t_wf_phases.f_group_id = t_wf_groups.f_id",array())
                   ->join(array("t3"=>"t_custom_fields"),"t3.f_code = t_creation_date.f_id",array())
                   ->where("t2.f_parent_code=".$data["f_code"])
                   ->where("t1.f_code=".$data["f_code"])
                   ->where("t2.f_active=1")
                   ->where("(t_creation_date.f_visibility & $user_group_level) != 0");
                   if($user_group_level != -1){
                   $q->where("(t_creation_date.f_visibility & $user_level) != 0");
                   }
                   if($self & $user_level) {
                   $q->where("(t_creation_date.f_creation_user = $uid)");
                   }
                   $q->where("(t_wf_phases.f_visibility & $user_level) != 0");

                   if($invisible) {
                   $q->where("(t_wf_groups.f_visibility & $user_level) != 0 OR (t_wf_groups.f_visibility = 0)");
                   }
                   else {
                   $q->where("(t_wf_groups.f_visibility & $user_level) != 0");
                   } */
                 //$zis = $q->query()->fetchAll();
                 $treegridParams['where'][] = "t1.f_code = " . $data["f_code"];
                 $cols = array('mainParentTableCols' => array('f_parent_code'));
                 $zis = $this->treegridQuery($tab, $f_type, $cols, $treegridParams);
                 $totZ = count($zis);
                 //foreach($zis as $value_zis) {                    
                 for ($z = 0; $z < $totZ; ++$z) {
                     $pdr[] = $zis[$z]['f_parent_code'];
                 }
                 $cmp[] = $pdr;
                 if (!$i) {
                     $head[] = "f_parent_code";
                 }
             }
             if (!$i) {
                 $arr[] = $head;
             }
             $arr[] = $cmp;
             $pos++;
             //$i++;                
         }
         return $arr;
     }

     /**
      * Create default array with params for treegrid query
      * @param string $f_type : list if type_ids
      * @param int $self : check if user can see only request generate from itself
      * @return array : array of where clause
      */
     public function createDefaultWhereTg($f_type, $self) {
         $user = Zend_Auth::getInstance()->getIdentity();
         $user_group_level = $user->f_group_level;
         $user_level = (int) $user->f_level;
         $self = (int) $self;
         $uid = $user->f_id;

         $defaultTreegridParams = array();
         //if f_type > 0 , create where clause
         if ($f_type > 0) {
             $defaultTreegridParams[] = "f_type_id IN($f_type)";
         }
         $defaultTreegridParams[] = "t2.f_active = 1";
         //if sel option is setted, show only what has been created by user
         if ($self & $user_level) {
             $defaultTreegridParams[] = "(t_creation_date.f_creation_user = $uid)";
         }
         $defaultTreegridParams[] = "(t_creation_date.f_visibility & $user_level) != 0";
         $defaultTreegridParams[] = "(t_wf_phases.f_visibility & $user_level) != 0";
         //if group level is not all, add user group level visibility
         if ($user_group_level != -1) {
             $defaultTreegridParams[] = "(t_creation_date.f_visibility & $user_group_level) != 0";
         }
         return $defaultTreegridParams;
     }

     /**
      * 
      * @param string $tab : name of main table (t_wares,t_workorders,t_selectors,t_systems)
      * @param string $f_type_id : f_type_id main table 
      * @param array $cols : list of array with columns for each table in query. The key names are : 
      *  1) mainTableCols : array with column of main table
      *  2) creationTableCols : array with column of t_creation_date
      *  3) customTableCols : array with column of t_custom_fields
      *  4) wfPhasesTableCols : array with column of t_wf_phases
      *  5) wfGroupsTableCols : array with column of t_wf_groups
      *  6) userTableCols : array with column of t_users
      *  7) mainParentTableCols : array with column of main table parent (t_wares_parent, etc)
      *  if one of these not exists or value is not an array, system will change it in an empty array
      * @param array $params : array with all params for the query. the key names are : 
      *  1) where : array with all where clause
      *  2) group : array with all group clause
      *  3) sort  : array with all sort clause
      *  4) limit : array with all limit clause (usually one)
      *  5) having : array with all having clause
      * @return array rows extracted from database
      */
     public function treegridQuery($tab, $f_type_id, $cols, $params = array()) {
         $select = new Zend_Db_Select($this->db);
         $tabp = $tab . '_parent';
         $main_table_cols = isset($cols['mainTableCols']) && is_array($cols['mainTableCols']) ? $cols['mainTableCols'] : array();
         $creation_cols = isset($cols['creationTableCols']) && is_array($cols['creationTableCols']) ? $cols['creationTableCols'] : array();
         $custom_cols = isset($cols['customTableCols']) && is_array($cols['customTableCols']) ? $cols['customTableCols'] : array();
         $wf_phases_cols = isset($cols['wfPhasesTableCols']) && is_array($cols['wfPhasesTableCols']) ? $cols['wfPhasesTableCols'] : array();
         $wf_groups = isset($cols['wfGroupsTableCols']) && is_array($cols['wfGroupsTableCols']) ? $cols['wfGroupsTableCols'] : array();
         $user_cols = isset($cols['userTableCols']) && is_array($cols['userTableCols']) ? $cols['userTableCols'] : array();
         $main_table_parent_cols = isset($cols['mainParentTableCols']) && is_array($cols['mainParentTableCols']) ? $cols['mainParentTableCols'] : array();
         $select->from(array('t1' => $tab), $main_table_cols);
         $select->join(array('t2' => $tabp), 't1.f_code = t2.f_code', $main_table_parent_cols);
         $select->join(array("t_creation_date"), "t_creation_date.f_id = t1.f_code", $creation_cols)
                 ->join("t_wf_phases", "t_creation_date.f_phase_id = t_wf_phases.f_number and t_creation_date.f_wf_id = t_wf_phases.f_wf_id", $wf_phases_cols)
                 ->join("t_wf_groups", "t_wf_phases.f_group_id = t_wf_groups.f_id", $wf_groups)
                 ->join("t_custom_fields", "t_custom_fields.f_code = t_creation_date.f_id", $custom_cols);
         if ($f_type_id > 0) {
             if ($f_type_id == 16 && $tab == 't_wares') { //add table to query for users and wizard
                 $select->join("t_users", "t_users.f_code = t1.f_code", $user_cols);
             }
         }


         /* add all params query  */
         foreach ($params as $method => $values) {
             $tot = count($values);
             for ($i = 0; $i < $tot; ++$i) {
                 if (strpos($method, "join") !== false) {
                     $select->$method($values[$i][0], $values[$i][1], $values[$i][2]);
                 } elseif ($method != 'limit') {
                     $select->$method($values[$i]);
                 } else {
                     $select->limit($values[0], $values[1]);
                 }
             }
         }
         $ris = $select->query()->fetchAll();
         return $ris;
     }

     public function jax_lock($params) {
         // tempo di rilascio automatico uscita da mainsim forzata                
         $mode = addslashes($params->mode);
         $afcode = $params->fcode;    // array di fcode        
         $user = Zend_Auth::getInstance()->getIdentity();
         //Zend_session::writeClose();
         $uid = $user->f_id;
         $tt = time();
         $sel = new Zend_Db_Select($this->db);
         
         //aggiunto da elcarbo il 23/01/2019
         //per propagare il lock del workorder ai parents del wo e ai suo discendenti
         if(!empty($afcode)){
            $tmp = $sel->from("t_creation_date", ["f_type"])
               ->where("f_id = ?", $afcode[0])->query()->fetch();
            $f_type = $tmp["f_type"];
            

           if($f_type == 'WORKORDERS'){
                $tolock = [];
                foreach ($afcode as $line) {
                    $sel->reset();
                    $temp = [];
                    $hierarchy = $sel->from("t_creation_date as cd", ["fc_hierarchy_codes"])
                        ->joinLeft("t_creation_date as cd2","cd2.fc_hierarchy_codes LIKE '".$line.",%' OR cd2.fc_hierarchy_codes LIKE '%,".$line.",%' ","f_id as child_code")
                       ->where("cd.f_id = ".$line)->query()->fetchAll();

                    $parents_hie = (!empty($hierarchy))?explode(",",$hierarchy[0]["fc_hierarchy_codes"]):[];
                    $childs_hie = (!empty($hierarchy))?array_column($hierarchy, 'child_code'):[];
                    $temp = array_unique(array_merge($parents_hie,$childs_hie));
                    $temp = array_filter($temp, function ($v){ //elimino valori non numerici o minori di 0
                        return $v > 0;
                    });
                    
                    if(!empty($temp)) $tolock[$line] = implode(",",$temp);
               }
           }
         }
         
         // unlock        
         if (!$mode && !empty($afcode)) {
             //modifiche e aggiunte di elcarbo del 23/01/2019
             //per propagare il lock del workorder ai parents del wo e ai suo discendenti  
             if($f_type == 'WORKORDERS'){ 
                 foreach ($afcode as $code) {
                     $temp = [$code];
                     if(!empty($tolock[$code])){
                         $temp = array_merge($temp,explode(",",$tolock[$code]));
                     }
                     
                    $sel->reset();
                    $f_ids = $sel->from("t_locked as tl", ["f_id"])
                       ->where( "tl.f_user_id = $uid". " and tl.f_code IN (". implode(",",$temp) .")")
                        ->query()->fetchAll();

                    if(!empty($f_ids)){
                        $f_ids = implode(",",array_column($f_ids,'f_id'));
                        $this->db->delete("t_locked", "f_id IN (" . $f_ids . ")");
                    }
                 }
             }
             else  $this->db->delete("t_locked", "f_code in (" . implode(',', $afcode) . ") and f_user_id = $uid");
             
             return array();
         }
         // lock sempre singolo
         if ($mode == 1) {
             $risp = array();
             $this->db->beginTransaction();
             //check if already locked   
             foreach ($afcode as $line) {
                 $sel->reset();
                 $ris = $sel->from("t_locked", array("num" => "count(*)"))
                                 ->where("f_code = ?", $line)->query()->fetch();
                 
                //$ris = $this->db->query("SELECT count(*) AS num FROM t_locked where f_code = $line FOR UPDATE")->fetch(); 
                $nc = (int) $ris['num'];
  
                 if (!$nc) {
                     $this->db->insert("t_locked", array(
                         "f_code" => $line,
                         "f_user_id" => $uid,
                         "f_timestamp" => $tt));
                     
                    //aggiunto da elcarbo il 23/01/2019
                    //per propagare il lock del workorder ai parents del wo e ai suo discendenti                     
                     if($f_type == 'WORKORDERS' && !empty($tolock[$line])){ 
                         $temp = explode(",",$tolock[$line]);
                        foreach ($temp as $wo_code) {
                            $this->db->insert("t_locked", array(
                                "f_code" => $wo_code,
                                "f_user_id" => $uid,
                                "f_timestamp" => $tt));                            
                        }
                     }
                 }
             }
             $this->db->commit();
             return $risp;
         }
     }

     /**
      * @deprecated 3.0.5.1
      * @return type
      */
     public function jax_lockotheruser() {
         $user = Zend_Auth::getInstance()->getIdentity();
         //Zend_session::writeClose();
         $uid = $user->f_id;
         $arrlk = array();
         $qu = "SELECT f_code FROM t_locked where f_user_id != $uid";
         $ris = $this->db->query($qu)->fetchAll();
         foreach ($ris as $line)
             $arrlk[] = $line['f_code'];
         return $arrlk;
     }

     public function getBookmark($f_name, $f_type = 'view', $module_name, $uid = 0) {
         $sys = new Mainsim_Model_System();
         if (!$uid) {
             $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
         }
         if ($f_type == "bookmark") { // Looking for bookmark
             $res = $sys->getFavourite($module_name, $uid);
         } else {
             $res = $sys->getBookmark($module_name, 'Default', 0);
         }
         if ($f_type == 'view' && empty($res)) {
             $m = explode("$", $module_name);
             $mod = explode("_", $m[0]);
             $res = $sys->getBookmark("mdl_" . ($mod[2] == 'parent' || $mod[2] == 'history' || $mod[2] == 'pp' ? $mod[1] : $mod[2]) . "_tg", 'default');
             if (empty($res)) {
                 return array(
                     "f_properties" => '[["f_title","Title",0,1,1,280,[],[],[],""],["f_code","Code",0,0,1,80,[],[],[],""],["f_description","Description",1,0,1,180,[],[],[],""]]'
                 );
             }
         }
         return (empty($res) ? array() : $res[$f_type]);
     }

     /**
      * Metodo per la modifica delle periodiche
      * @param type $data 
      */
     public function editPeriodic($params) {
         //Cancello la vecchia periodica
         if (!$params['fid'])
             return;
         $dt = $_POST["dt"];
         $t0 = $_POST["t0"];
         $t1 = $_POST["t1"];
         if (!$dt && !$t0 && !$t1) {
             $this->db->update("t_periodics", array("f_forewarning" => "0", "f_start_date" => "0", "f_end_date" => "0"), "f_id = " . $params['fid']);
         } else {
             $sel = new Zend_Db_Select($this->db);
             $res = $sel->from("t_periodics")->where("f_id = ?", $params['fid'])->query()->fetchAll();
             $data = $res[0];
             $data['f_start_date'] = $t0;
             $data['f_end_date'] = $t0 + $t1;
             $data['f_forewarning'] = $dt;
             unset($data['f_id']);
             $this->db->update("t_periodics", $data, "f_id = " . $params['fid']);
         }
     }

     public function deleteBookmark($data) {
         $sel = new Zend_Db_Select($this->db);
         $sel->from("t_bookmarks");
         foreach ($data as $key => $line)
             $sel->where("$key = ?", $line);
         $res_check = $sel->query()->fetchAll();
         $response = array();
         if ($res_check[0]['f_user_id'] == 0) {
             $response['msg'] = "Impossibile cancellare il bookmark condiviso";
         } else {
             foreach ($res_check as $res_line)
                 $this->db->delete("t_bookmarks", "f_id = {$res_line['f_id']}");
             $response['msg'] = 'ok';
         }
         return $response;
     }

     /*
      * get timebar data for labors
      */

     public function jax_timebar_labors($f_code, $year = null) {
         if (!$year) {
             $year = date('Y');
         }
         $select = new Zend_Db_Select($this->db);
         $select->from('t_pair_cross', array('fc_rsc_since', 'fc_rsc_till'))
                 ->join('t_workorders', 't_pair_cross.f_code_main = t_workorders.f_code')
                 ->where('f_code_cross = ' . $f_code)
                 ->where('fc_rsc_since >= ' . strtotime($year . '-01-01') . ' AND fc_rsc_till < ' . strtotime(($year + 1) . '-01-01'))
                 ->where('f_type_id IN (1,4,5,6,7,10,12,13)');
         $res = $select->query()->fetchAll();
         for ($i = 0; $i < count($res); $i++) {
             $data1[] = array(
                 't0' => $res[$i]['fc_rsc_since'],
                 't1' => $res[$i]['fc_rsc_till'] - $res[$i]['fc_rsc_since'],
                 'dt' => 0,
                 'nb' => 1,
                 'n' => 0,
                 'title' => ''
             );
         }
         $result['fcode'] = $f_code;
         $data2 = array();
         $result['data'][] = $data2;
         $result['data'][] = $data1 ? $data1 : array();
         return $result;
     }

     public function jax_timebar($f_code, $data_mode, $rsv = false) {
         //Zend_session::writeClose();
         /*
           JTM[1]=[
           // disponibilitÃ 
           [ {t0:1327964400, t1:864000*2, type:0 }, // type=0   colore '#c5dc97'
           {t0:1332253883, t1:864000*3, type:2 },  // type=1   colore '#fb9999'
           {t0:1334928683, t1:864000*7, type:1 }
           ],

           [              // impegnativa
           {t0:(1327964400+180000), t1:100000, dt:60000, tp:0, nb:2, n:0 },
           {t0:(1327964400+200000), t1:150000, dt:30000, tp:1, nb:2, n:1 },
           {t0:(1333464415+380000), t1:140000, dt:20000, tp:0, nb:1, n:0 }
           ]
           ];

          */
         //PREPARAZIONE ARRAY DATI PER DISPONIBILITA' E IMPEGNATIVA
         $arr = array(array(), array());
         $uid = Zend_Auth::getInstance()->getIdentity()->f_level;
         $select = new Zend_Db_Select($this->db);

         $f_code_query = $f_code;
         if ($rsv) { // if this is a reservation
             $f_codes = array($f_code_query);
             $select->reset();
             $res_code = $select->from(array("t1" => "t_workorders_parent"), array("f_code"))
                             ->join(array("t2" => "t_creation_date"), "t1.f_code = t2.f_id", array())
                             ->join(array("t3" => "t_wf_phases"), "t2.f_phase_id = t3.f_number and t2.f_wf_id = t3.f_wf_id", array())
                             ->join(array("t4" => "t_wf_groups"), "t4.f_id = t3.f_group_id", array())
                             ->where("f_parent_code = ?", $f_code)
                             ->where("t1.f_active = 1")
                             ->where("t4.f_visibility != 0")
                             ->query()->fetchAll();
             foreach ($res_code as $code_child) {
                 $f_codes[] = $code_child['f_code'];
             }
             $f_code_query = implode(',', $f_codes);
         }
         $select->reset();
         $select->from(array("t1" => "t_periodics"));
         if ($data_mode) {
             $select->reset();
             $select->from(array("t1" => "t_workorders"), array('f_start_date', 'f_end_date', 'f_id' => 'f_code', "fc_wo_starting_date", "fc_wo_ending_date"))
                     ->join(array("t2" => "t_creation_date"), "t1.f_code = t2.f_id", array("f_creation_date", 'f_title'))->where("(f_visibility & $uid) != 0");
         }

         $select->where("t1.f_code IN ($f_code_query)")->order("t1.f_start_date");
         $ris = $select->query()->fetchAll();

         foreach ($ris as $data) {

             //recupero i dati che mi servono
             $mode = !$data_mode ? $data["f_mode"] : 1;
             $t0 = $data["f_start_date"];
             $t1 = $data["f_end_date"] - $t0;   // delta
             if ($data_mode && !$rsv) { // if is not a reservation and not a generator, create a generator
                 $data['f_forewarning'] = 0; //$t0 - $data['f_creation_date']; 
             }


             // disponibilita'  
             if ($mode == 0 && !$data_mode) {
                 $c = (int) $data["f_available_type"];
                 $arr[0][] = array("t0" => $t0, "t1" => $t1, "type" => $c);
             }

             // impegnativa
             //tp,nb,n => per consuntivazione dalla preventivazione
             //tp = preventivazione
             //nb => numero elementi
             //n => posizione di disegno
             if ($mode == 1) {
                 $res = array();
                 $select->reset();
                 if (!$data_mode) {
                     //verifico se esiste la consuntivazione
                     $res = $select->from("t_workorders")->where("f_code_periodic = ?", $data['f_code'])
                                     ->where("f_start_date = {$data['f_start_date']}")->where("f_end_date != 0")
                                     ->query()->fetchAll();
                 } else {
                     //verifico se esiste la consuntivazione
                     if ($data['fc_wo_starting_date'] != 0 && $data['fc_wo_ending_date'] != 0) {
                         $res[0]['f_start_date'] = $data['fc_wo_starting_date'];
                         $res[0]['f_end_date'] = $data['fc_wo_ending_date'];
                     }
                 }
                 $f_title = Mainsim_Model_Utilities::chg($data['f_title']);
                 if (!empty($res)) {
                     $arr[1][] = array("t0" => $t0, "t1" => $t1, "dt" => $data["f_forewarning"], "tp" => 0, "nb" => 2, "n" => 0, "title" => $f_title, 'fid' => $data['f_id']); //preventiva
                     $arr[1][] = array("t0" => $res[0]['f_start_date'], "t1" => $res[0]['f_end_date'] - $res[0]['f_start_date'], "dt" => 0, "tp" => 0, "nb" => 2, "n" => 1, "title" => $f_title, 'fid' => $data['f_id']); //consuntiva
                 } else {
                     $arr[1][] = array("t0" => $t0, "t1" => $t1, "dt" => $data["f_forewarning"], "tp" => 0, "nb" => 1, "n" => 0, "title" => $f_title, 'fid' => $data['f_id']);
                 }
             }
         }
         return array("fcode" => $f_code, "data" => $arr);
     }

     public function getReverseAjax() {
         $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
         $u_selectors = Zend_Auth::getInstance()->getIdentity()->selectors;
         $user_level = Zend_Auth::getInstance()->getIdentity()->f_level;
         $sel = new Zend_Db_Select($this->db);

         // NICO: modifica per Revet.
        // In caso ci sia il setting DISABLE_FUNC con reverseAjax true
        // rispondo sempre un array vuoto
        $sel->reset();
        $set = "DISABLE_FUNC";
        $setting = $sel ->from("t_creation_date", ["f_description"])
                        ->where("f_title = ?", $set)
                        ->query()->fetch();
        if (!empty($setting)) {
            try {
                $setting = json_decode($setting["f_description"],1);      
                if($setting["reverseAjax"]["enabled"]){
                    return [];
                }
            } catch (Exception $e) {
                //Silently fail
            }
        }
        $sel->reset();

         $response = array();
         $res = $sel->from("t_reverse_ajax")
                         ->where("f_user_id = ?", $uid)
                         ->query()->fetchAll();
         if (sizeof($res) > 0) {
             foreach ($res as $line) {
                 $sel->reset();
                 $res_code = $sel->from("t_creation_date")->where("f_id = ?", $line['f_code'])->query()->fetch();
                 $module_name = $line['f_module_name'];
                 $sel->reset();
                 $node = array();
                 $table = "t_" . strtolower($res_code['f_type']);
                 if (!empty($res_code)) {
                     //check if i can see this code
                     if (!empty($u_selectors) && in_array($res_code['f_type'], ['WARES', 'WORKORDERS'])) {
                         $sel->reset();
                         $res_sel = $sel->from("t_selectors", "f_type_id")->where("f_code IN ($u_selectors)")
                                         ->group("f_type_id")->query()->fetchAll();
                         $type_ids = array_map(function($el) {
                             return $el['f_type_id'];
                         }, $res_sel);
                         $sel->reset();
                         $tableCross = 't_selector_wo';
                         $field = 'f_wo_id';
                         if ($res_code['f_type'] == 'WARES') {
                             $tableCross = 't_selector_ware';
                             $field = "f_ware_id";
                         }
                         $res_check = $sel->from($table, ['num' => 'count(*)'])
                                         ->where("f_code = ?", $line['f_code'])
                                         ->where("exists(select f_id from $tableCross where $field = {$line['f_code']} and f_selector_id in ($u_selectors)"
                                                 . "or not exists (select t1.f_id from $tableCross as t1 "
                                                 . "join t_selectors as t2 on t1.f_selector_id = t2.f_code where $field = {$line['f_code']} and f_type_id IN (?)))", $type_ids)
                                         ->query()->fetch();
                         if (empty($res_check['num']))
                             continue;
                     }
                     $bookmark = $this->getBookmark("", 'view', $module_name);
                     $bookmark_array = json_decode(Mainsim_Model_Utilities::chg($bookmark['f_properties']));
                     if ($res_code['f_type'] == 'SYSTEM') {
                         $table = 't_systems';
                     }
                     $main_table = Mainsim_Model_Utilities::get_tab_cols($table);
                     $creation_table = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
                     $custom_table = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
                     $user_table = Mainsim_Model_Utilities::get_tab_cols("t_users");
                     $main_cols = array('f_type_id');
                     $creation_cols = array('f_visibility', 'f_editability');
                     $custom_cols = array();
                     $user_cols = array();
                     foreach ($bookmark_array as $line_book) {
                         if (in_array($line_book[0], $main_table)) {
                             $main_cols[] = $line_book[0];
                         } elseif (in_array($line_book[0], $creation_table)) {
                             $creation_cols[] = $line_book[0];
                         } elseif (in_array($line_book[0], $custom_table)) {
                             $custom_cols[] = $line_book[0];
                         } elseif (in_array($line_book[0], $user_table)) {
                             $user_cols[] = $line_book[0];
                         }
                     }

                     $sel->reset();
                     $sel->from(array("t1" => $table), $main_cols)
                             ->join(array("t_creation_date"), "t_creation_date.f_id = t1.f_code", $creation_cols)
                             ->join(array("t2" => "t_custom_fields"), "t_creation_date.f_id = t2.f_code", $custom_cols)
                             ->join("t_wf_phases", "t_creation_date.f_phase_id = t_wf_phases.f_number and t_creation_date.f_wf_id = t_wf_phases.f_wf_id", array('f_edit' => 't_wf_phases.f_editability', 'f_percentage', 'f_name'));
                     if ($res_code['f_category'] == 'USER') {
                         $sel->join("t_users", "t_users.f_code = t1.f_code", $user_cols);
                     }
                     $res_node = $sel->where("t1.f_code = ?", $line['f_code'])->query()->fetch();
                     if (!empty($res_node)) {
                         foreach ($res_node as $key_node => $line_node) {
                             if ($key_node == "f_editability") {
                                 $f_wf_edit = $res_node['f_edit'];
                                 $v = !(((int) $line_node & (int) $f_wf_edit & (int) $user_level)) ? 0 : 1;
                                 $node[$key_node] = $v;
                             } else {
                                 $node[$key_node] = Mainsim_Model_Utilities::chg($line_node);
                             }
                         }
                     }
                     $response[$table][$res_node['f_type_id']][] = array(
                         'code' => $line['f_code'],
                         'opid' => explode(',', $line['f_old_parent']),
                         'pid' => explode(',', $line['f_parent']),
                         'action' => $line['f_action'],
                         'node' => $node
                     );
                 }
                 $this->db->delete("t_reverse_ajax", "f_id = " . $line['f_id']);
             }
         }
         return $response;
     }

     /**
      * Get all information of passed f_code from main table(wares/workorders/selectors/systems), custom_fields and creation_date
      * @param type $params
      * @return list of column for passed code
      */
     public function getCodeProperties($params) {
         $tab_columns = Mainsim_Model_Utilities::get_tab_cols($params['Tab']);
         $tab_custom = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
         $tab_creation = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
         //remove f_timestamp and f_id from main table and custom 
         unset($tab_columns['f_timestamp']);
         unset($tab_columns['f_id']);
         unset($tab_custom['f_timestamp']);
         unset($tab_custom['f_id']);
         unset($tab_custom['f_code']);
         unset($tab_creation['f_id']);
         $user_level = Zend_Auth::getInstance()->getIdentity()->f_level;

         $sel = new Zend_Db_Select($this->db);
         $resp = array();
         if (strpos($params['module_name'], "usr_tg") !== false || strpos($params['module_name'], "usr_pp_tg") !== false) {
             $res = $sel->from(array('t1' => 't_users'));
         } else {
             $res = $sel->from(array('t1' => $params['Tab']), $tab_columns);
         }
         $sel->join(array("t_creation_date"), "t_creation_date.f_id = t1.f_code", $tab_creation)
                 ->join("t_wf_phases", "t_creation_date.f_phase_id = t_wf_phases.f_number and t_creation_date.f_wf_id = t_wf_phases.f_wf_id", array('f_edit' => 't_wf_phases.f_editability', 'f_percentage', 'f_name')
                 )
                 ->join(array("t2" => "t_custom_fields"), "t_creation_date.f_id = t2.f_code", $tab_custom)
                 ->join(array("t4" => "{$params['Tab']}_parent"), "t1.f_code = t4.f_code", array("f_parent_code"));
         $sel->where("t1.f_code = ?", $params['f_code'])->where('t4.f_active = 1');

         $res = $sel->query()->fetch();

         //RECUPERO IL MODULO        
         foreach ($res as $key => $line) {
             $resp[$key] = Mainsim_Model_Utilities::chg($line);
         }

         if (!(((int) $resp['f_editability'] & (int) $resp['f_edit'] & (int) $user_level))) {
             $resp['f_editability'] = 0;
         } else {
             $resp['f_editability'] = 1;
         }

         $sel->reset();
         //RIMOSSO IL 3 MARZO DA BALDINI - CON QUESTA MODIFICA GLI OGGETTI CHIUSI E CANCELLATI DIVENTANO MODIFICABILI

         /* $res_edit = $sel->from(array("t1"=>"t_wf_phases"),array())
           ->join(array("t2"=>"t_wf_groups"),"t1.f_group_id = t2.f_id",array("f_visibility"))
           ->where("t1.f_wf_id = ?",$resp['f_wf_id'])
           ->where("t1.f_number = ?",$resp['f_phase_id'])->query()->fetch();
           if(!empty($res_edit) && ($res_edit['f_visibility']) == 0) {
           $resp['f_editability'] = 0;
           } */

         $arr_lock = $this->jax_lockotheruser();
         if (in_array($resp['f_code'], $arr_lock)) {
             $resp['f_editability'] = 0;
         }
         $selectors = array();
         $sel->reset();
         //add selectors
         if ($params['Tab'] == 't_wares') {
             $res_selectors = $sel->from("t_selector_ware", array("f_selector_id"))
                             ->where("f_ware_id = ?", $params['f_code'])
                             ->query()->fetchAll();

             foreach ($res_selectors as $line_selectors) {
                 $selectors[] = $line_selectors['f_selector_id'];
             }
         } elseif ($params['Tab'] == 't_workorders') {
             $res_selectors = $sel->from("t_selector_wo", array("f_selector_id"))
                             ->where("f_wo_id = ?", $params['f_code'])
                             ->query()->fetchAll();

             foreach ($res_selectors as $line_selectors) {
                 $selectors[] = $line_selectors['f_selector_id'];
             }
         } elseif ($params['Tab'] == 't_systems') {
             $res_selectors = $sel->from("t_selector_system", array("f_selector_id"))
                             ->where("f_system_id = ?", $params['f_code'])
                             ->query()->fetchAll();

             foreach ($res_selectors as $line_selectors) {
                 $selectors[] = $line_selectors['f_selector_id'];
             }
         }
         $resp['t_selectors'] = implode(",", $selectors);
         //chek if this request(if this is a request) is locked from a mobile device
         if ($params['Tab'] == 't_workorders') {
             if ($res[0]['f_phase_mobile_status'] == 1) {
                 $resp['f_editability'] = 0;
             }
         }
         return $resp;
     }

     /**
      * unlock records of a specific module 
      * @param $f_codes mixed could be module name (string) or f_code list(array)
      */
     public function unlockRecords($f_codes) {
         if (empty($f_codes))
             return;
         $select = new Zend_Db_Select($this->db);
         if (is_array($f_codes) && !empty($f_codes)) {
             $tot_a = count($f_codes);
             for ($a = 0; $a < $tot_a; ++$a) {
                 $line = $f_codes[$a];
                 $this->db->delete("t_locked", "f_code = " . $line);
             }
         } elseif (!empty($f_codes)) {
             $res_mod = $select->from("t_ui_object_instances", array("f_properties"))
                             ->where("f_instance_name = ?", $f_codes)->query()->fetch();
             if (empty($res_mod))
                 return;
             $prop = json_decode(Mainsim_Model_Utilities::chg($res_mod['f_properties']), true);
             $types = $prop['ftype'];
             $table = $prop['table'];
             $select->reset();
             $res_codes = $select->from(array("t1" => "t_locked"), array("f_code"))
                             ->join(array("t2" => $table), "t1.f_code = t2.f_code", array())
                             ->where("t2.f_type_id IN ($types)")->query()->fetchAll();
             $tot_a = count($res_codes);
             for ($a = 0; $a < $tot_a; ++$a) {
                 $line = $res_codes[$a];
                 $this->db->delete("t_locked", "f_code = " . $line['f_code']);
             }
         }
     }

     public function setRequestedFields($fcode,$custom_fields = null) {
                
        $sel = new Zend_Db_Select($this->db);
        $sel->from("t_creation_date")->where("t_creation_date.f_id = ".$fcode);
        if($custom_fields){
            $t_creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
            $t_custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
            $t_custom_columns = array_diff($t_custom_columns, $t_creation_columns);
            $sel->join(array("t_custom_fields"), "t_custom_fields.f_code = t_creation_date.f_id", $t_custom_columns);
        }

        $res = $sel->query()->fetch();
        $res['f_title'] = utf8_encode($res['f_title']);
        $res['f_description'] = utf8_encode($res['f_description']);
//        var_dump($res); die();
        return $res;
     }

 }
 
