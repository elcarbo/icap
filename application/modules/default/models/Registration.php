<?php

class Mainsim_Model_Registration
{
    /**
     *
     * @var Zend_Db 
     */
    private $db;  
    private $encrypt_method = "AES-256-CBC";
    private $salt = 'a23der';
    public $captchaDir = 'public/msim_images/captcha';
    public $Regcf="";
    private $captchaObj;

    
    public function __construct($db = null) {
       $this->db = !is_null($db) ? $db : Zend_Db::factory(Zend_Registry::get('db'));
       $this->captchaObj = new Zend_Captcha_Image();
       $this->captchaObj->setWordLen('4')
                        ->setTimeout(300)
                        ->setHeight('60')
                        ->setFont('public/msim_css/font/OpenSans-Regular.ttf')
                        ->setImgDir($this->captchaDir)
                        ->setDotNoiseLevel('5') 
                        ->setLineNoiseLevel('5');
    
      // registration_custom_fields
      $select = new Zend_Db_Select($this->db);
      $select->from("t_creation_date",array("f_description"))->where("f_title='registration_custom_fields'");
      $res = $select->query()->fetch();

      if(count($res)) {
        $this->Regcf=$res['f_description'];
      }
 
      
    }
    

    public function get_translations($language){
      
        $translations = array(
            'en' => array(
                "title" => "Signup",
                "description" => "",
                "name" => "First name",
                "surname" => "Last name",
                "email" => "E-mail",
				"phone" => "Phone",
                "sex" => "Sex",
                "male" => "Male",
                "female" => "Female",
                "language" => "Language",
                "capcha"=>"Type the characters below",
                "send" => "Send",
                "successful_registration_title" => "Registration Successful",
                "successful_registration_header" => "Congratulations your event registration has been successful!",
                "successful_registration_message" => "An email has been sent to your address confirming your registration.<br>If you do not receive the email please check your spam filter or contact your supervisor",
                "successful_registration_message_approval" => "Your registration is under approval by your supervisor.<br>You’ll be notified by email that your account is ready",
                "error_message" => "An error has occurred during the registration. Please retry",
                "text_email" => "Congratulations!<br>your event registration has been successful.<br>Your account access is:<br><br>username : <b><username></b><br>password : <b><password></b><br><br>You can login using the following link <b><link></b><br><br>After you logged in we suggest you to change the provided password using the 'User profile' window with a unique one you should use for the <b>main</b>sim login only. It will also be better to edit your profile with your personal infos.",
                "text_email_approval" => "Congratulations!<br>Your registration is under approval by your <b>main</b>sim supervisor.<br>You'll be notified by email that your <b>main</b>sim account is ready.",
                "object_email" =>"Confirmation of registration", // "Welcome to mainsim!",
                "error_email_message" => "The email address you entered is already used",
                "back" => "back",
                "email_not_valid" => 'email address not valid',
                "no_check_privacy"=> 'Check for privacy',
                "name_not_valid" => 'first name not vaild',
                "surname_not_valid" => 'last name not valid',
                "error_captcha_message" => 'captcha not valid',
                "privacy" => "In sending this request, I confirm that I have read the privacy policy statement, and I agree for my data to be processed in order for the requested service (contact request) to be provided",
                "mail_not_sent" => "Email notification not sent!"
            ),
            'it' => array(
                "title" => "Registrati",
                "description" => "",
                "name" => "Nome",
                "surname" => "Cognome",
                "email" => "E-mail",
				"phone" => "Telefono",
                "sex" => "Sesso",
                "male" => "Maschio",
                "female" => "Femmina",
                "language" => "Lingua",
                "capcha"=> "Scrivi i caratteri indicati",
                "send" => "Invia",
                "successful_registration_title" => "Registrazione avvenuta con successo",
                "successful_registration_header" => "Congratulazioni, la tua registrazione e' andata a buon fine!",
                "successful_registration_message" => "Verrà inviata un'e-mail al tuo indirizzo di posta per confermare la tua registrazione.<br>Se non ricevi l'e-mail ti preghiamo di controllare la cartella di spam o di contattare il tuo responsabile.",
                "successful_registration_message_approval" => "La tua registrazione è in attesa di approvazione dal responsabile.<br>Riceverai un'e-mail di notifica all'attivazione del tuo account",
                "error_message" => "Errore durante la registrazione",
                "text_email" => "Congratulazioni!<br>la tua registrazione e' andata a buon fine.<br>Le tue credenziali di accesso sono:<br><br>username : <b><username></b><br>password : <b><password></b><br><data_custom><br><br>Puoi entrare nel sistema attraverso il seguente link <b><link></b><br><br>Dopo aver fatto accesso al sistema ti consigliamo caldamente di modificare la password fornita attraverso la finestra del 'Profilo utente' con una password unica usata esclusivamente per il tuo accesso. Ti suggeriamo inoltre di modificare il tuo profilo inserendo le informazioni personali richieste.",
                "text_email_approval" => "Congratulazioni!<br>La tua registrazione e' in attesa di approvazione dal responsabile <b>main</b>sim.<br>Riceverai un'e-mail di notifica all'attivazione del tuo account <b>main</b>sim.",
                "object_email" => "Conferma Registrazione", //"Benvenuto in mainsim",
                "error_email_message" => "L'indirizzo mail inserito è già utilizzato",
                "back" => "indietro",
                "email_not_valid" => 'email non valida',
                "no_check_privacy"=> 'Check per privacy',
                "name_not_valid" => 'nome non valido',
                "surname_not_valid" => 'cognome non valido',
                "error_captcha_message" => 'captcha non valido',
                "privacy" => "Con l'invio della richiesta dichiaro di aver preso visione dell'informativa privacy e acconsento al trattamento dei dati per l'erogazione del servizio richiesto",
                "mail_not_sent" => "Email di notifica non inviata!"
            )
        );

        return $translations[$language];
    }
    
    public function get_privacy(){
        $select = new Zend_Db_Select($this->db);
        $res = $select->from(array("t1"=>"t_creation_date"),array("f_description"))
                ->where("f_title = 'NEWUSR_PRIVACY'")->query()->fetch(); 
        return $res['f_description'];
    }
    
    public function get_language(){
        //$db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $select = new Zend_Db_Select($this->db);
        $lang = $select->from(array("t1"=>"t_creation_date"),array("f_description"))
                ->where("f_title = 'NEWUSR_LANGUAGE'")->query()->fetch(); 

        switch($lang['f_description']){
            case '2':
                return 'it';
            case '1':
                return 'en';
        }
        
    }
    
    public function get_link(){
        //$db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $select = new Zend_Db_Select($this->db);
        $link = $select->from(array("t1"=>"t_creation_date"),array("f_description"))
                ->where("f_title = 'NEWUSR_LINK'")->query()->fetch(); 
        
        return $link["f_description"];
    }
    
    public function get_active(){
        //$db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $select = new Zend_Db_Select($this->db);
        $link = $select->from(array("t1"=>"t_creation_date"),array("f_description"))
                ->where("f_title = 'NEWUSR_ACTIVE'")->query()->fetch(); 
        
        return $link["f_description"];
    }
    
    public function get_role(){
        //$db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $select = new Zend_Db_Select($this->db);
        $link = $select->from(array("t1"=>"t_creation_date"),array("f_description"))
                ->join(array("t2" => "t_systems"), "t2.fc_lvl_level = t1.f_description", array("f_code"))
                ->join(array("t3" => "t_creation_date"), "t3.f_id = t2.f_code", array("f_title"))
                ->where("t1.f_title = 'NEWUSR_ROLE'")->where("t3.f_category = 'LEVEL'")->query()->fetch(); 
        if(empty($link)){
            $select->reset(Zend_Db_Select::where);
            $link = $select->where("t1.f_id = 9")->query()->fetch(); 
        }
        return $link;
    }
    
    public function sendMail($language, $username, $password, $link, $email, $active, $data_custom="" ){
        $translations = $this->get_translations($language);
        if($active == "true"){
            //$text = str_replace(array('<username>', '<password>', '<link>'), array($username, $password, '<a href="http://' . $link . '/registration/autologin?usr=' . $username . '&pwd=' . $this->encryptPassword($password) . '">' . $link . '/registration/autologin?usr=' . $username . '&pwd=' . $this->encryptPassword($password) . '</a>'), $translations["text_email"]);
            $text = str_replace(array('<username>', '<password>', '<link>','<data_custom>'), array($username, $password, '<a href="http://' . $link . '">' . $link . '</a>',$data_custom), $translations["text_email"]);
        }
        else{
            $text = $translations["text_email_approval"];
        }
        $objMail = new Mainsim_Model_Mail();
        $objMail->sendMail(array('To'=>array($email)),$translations["object_email"], $text, null, array('content-type' => 'text/html'));
    }
    
    public function deleteUser($userCode){
        $this->db->delete('t_wares_systems', 'f_ware_id = ' . $userCode);
        $this->db->delete('t_wares_parent', 'f_code = ' . $userCode);
        $this->db->delete('t_custom_fields', 'f_code = ' . $userCode);
        $this->db->delete('t_custom_fields_history', 'f_code = ' . $userCode);
        $this->db->delete('t_users', 'f_code = ' . $userCode);
        $this->db->delete('t_wares_history', 'f_code = ' . $userCode);
        $this->db->delete('t_wares', 'f_code = ' . $userCode);
        $this->db->delete('t_creation_date', 'f_id = ' . $userCode);
        $this->db->delete('t_creation_date_history', 'f_code = ' . $userCode);
    }
    
    private function checkCaptcha($captchaData){
        if(!$this->captchaObj->isValid($captchaData)){
            $this->deleteCaptchaPNG($captchaData['id']);
            return false;
        }
        else{
            $this->deleteCaptchaPNG($captchaData['id']);
            return true;
        }
    }
    
    public function get_selectors_list(){
        //$db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $select = new Zend_Db_Select($this->db);
        $link = $select->from(array("t1"=>"t_creation_date"),array("f_description"))
                ->where("t1.f_title = 'CUSTOM_FIELDS_REG'")->where("t1.f_category = 'SETTING'")->where("t1.f_type = 'SYSTEMS'")->query()->fetch(); 
       
        return (($link)? $link["f_description"] : '');
    }
    
    
    public function newUser($data)
    {
        $dict = $this->get_translations($data['language']);
        if(!$this->checkCaptcha($data['captcha'])){
            return array(
                'code' => 'KO',
                'message' => $dict['error_captcha_message']
            );
        }
        
        $role = $this->get_role();
        $selectors = $this->get_selectors_list();
        //$array_selectors = explode(',', $selectors);
        $unique = array("fc_usr_usn");
        $t_wares_parent_16 = array("f_code"=>[],"f_code_main"=>0,"f_type"=>16,"Ttable"=>"t_wares_parent","f_module_name"=>"mdl_usr_parent_tg");
        $t_systems_4 = array("f_code"=>[$role["f_code"]],"f_code_main"=>0,"f_type"=>"4","Ttable"=>"t_systems","f_module_name"=>"mdl_usr_lvl_tg");
        $t_selectors_1_2_3_4_5_6_7_8_9_10 = array("f_code"=>(($selectors != '') ? [$selectors] : []),"f_code_main"=>0,"f_type"=>"1,2,3,4,5,6,7,8,9,10","Ttable"=>"t_selectors","f_module_name"=>"mdl_usr_slc_tg");    
        
        $e_mail=strtolower($data['email']);
        
        $active = $this->get_active();
        $password = $this->createPassword();
        $username = $e_mail;
        $email = $e_mail;
        $language = $data['language'];
 
        $data_wares = array(
            "fc_usr_level"=> $role["f_description"],
            "f_code"=>0,
            "f_type_id"=>16,
            "f_phase_id"=> ($active == "true") ? 1 : 2,
            "f_module_name"=>"mdl_usr_tg",
            "unique"=>$unique,
            "f_title"=> $data['name'] . " " . $data['surname'],
            "fc_usr_firstname"=>$data['name'],
            "fc_usr_lastname"=>$data['surname'],
            "fc_usr_gender"=>$data['sex'],
            "fc_usr_address"=>"",
            "fc_usr_phone"=>$data['phone'],
            "fc_usr_mail"=> $e_mail,
            "fc_usr_usn"=> $username,
            "fc_usr_password"=> $password['password'],
            "fc_usr_repeat_pwd"=> $password['password'],
            "fc_usr_language"=>($data['language'] == 'en') ? 1 : (($data['language'] == 'it')? 2 : ""),
            "fc_usr_language_str"=>($data['language'] == 'en') ? "English" : (($data['language'] == 'it')? "Italian" : ""),
            "fc_usr_level_text"=>$role["f_title"],
            "fc_usr_avatar"=>"",
            "t_wares_parent_16"=>$t_wares_parent_16,
            "t_systems_4"=>$t_systems_4,
            "t_selectors_1,2,3,4,5,6,7,8,9,10"=>$t_selectors_1_2_3_4_5_6_7_8_9_10
        );
        
        if(isset($data['add_field_custom']) ){
            $cf=explode(",",$data['add_field_custom']);
            for($r=0;$r<count($cf);$r++){
              $data_wares[ $cf[$r] ]=$data[ $cf[$r] ];
            }
        }
        
        $select = new Zend_Db_Select($this->db);
        $admin = $select->from(array("t1"=>"t_users"))
                ->join(array('t2'=>'t_creation_date'), 't1.f_code = t2.f_id')
                ->where("t2.f_title='Mainsim Administrator'")->query()->fetchObject(); 
        $objWares = new Mainsim_Model_Wares();
        $result = $objWares->newWares($data_wares,$admin);
 
 
 
        if(isset($result['message'])){
            if(strpos($result['message'], 'usr_usn') > 0){
                return array(
                    'code' => 'error_email_message',
                    'message' => $dict["error_email_message"]
                );
            }
            else{
                return array(
                    'code' => 'KO',
                    'message' => $dict["error_message"]
                );
            }
        }
        else{
            $userCode = $result['f_code'];
            try{
                $this->sendMail($language, $username, $password['password'], $_SERVER['HTTP_HOST'], $email, $active);
                $sendmail="";
            }
            catch(Exception $e){
            
                // if(strpos($e->getMessage(), "5.1.1") !== false){ $this->deleteUser($userCode); }
                $sendmail = $dict["mail_not_sent"];
                
                
                // $sendmail=$language." | ".$username." | ".$password['password']." | ".$_SERVER['HTTP_HOST']." | ".$email." | ".$active;
              //  $sendmail.=" - ".$e->getMessage();
               
            }
           
            return array(
                    'code' => 'OK',
                    'successful_registration_title' => $dict['successful_registration_title'],
                    'successful_registration_header' => $dict['successful_registration_header'],
                    'successful_registration_message' => $active == "true" ? $dict['successful_registration_message'] : $dict['successful_registration_message_approval'],
                    'back' => $dict['back'],
                    'sendmail' => $sendmail
            ); 
            
           /* for($sel = 0; $sel < count($array_selectors); $sel++){
                $select->reset();
                $asset_for_sel = $select->from(array("t1"=>"t_selector_ware"), array('f_ware_id'))
                           ->join(['t3'=>'t_wares'],'t3.f_code = t1.f_ware_id',['f_type_id'])
                           ->where("t1.f_selector_id=" . $array_selectors[$sel])->query()->fetchAll(); 

               $this->db->insert('t_selector_ware', ['f_selector_id' => $array_selectors[$sel], 'f_ware_id' => $userCode,'f_nesting_level' => 1,'f_timestamp' => time()]);
               for($sel_ware = 0; $sel_ware < count($asset_for_sel); $sel_ware++)
                   $this->db->insert('t_ware_relations', ['f_code_master' => $asset_for_sel[$sel_ware]['f_ware_id'], 'f_code_ware_slave' => $userCode,'f_type_id_master'=>$asset_for_sel[$sel_ware]['f_type_id'], 'f_type_id_slave'=>16, 'f_timestamp' => time()]); 
            }*/
           
        }
        return $result;
    }
    
    public function connect_new_db($name){
        
        $db = Zend_Db::factory('Pdo_Mysql', array(
    'host'     => 'localhost',
    'username' => 'root',
    'password' => '1881',
    'dbname'   => 'mainsim3_' . $name/*"mainsim3_lara6"*/
));
        
        $select = new Zend_Db_Select($db);
       // $admin = $select->from(array("t1"=>"t_users"))->query()->fetchAll();
       // var_dump($admin);die();
        return $db;
    }
    
    public function newUsermailchimp($data)
    {
        //$dict = $this->get_translations($data['language']);
        //$this->db->closeConnection();
        $conn = $this->connect_new_db($data['db']);
        //$this->db = $conn;
        
        $role = $data["role"];/*$this->get_role();*/
        $unique = array("fc_usr_usn");
        $t_wares_parent_16 = array("f_code"=>[],"f_code_main"=>0,"f_type"=>16,"Ttable"=>"t_wares_parent","f_module_name"=>"mdl_usr_parent_tg");
        $t_systems_4 = array("f_code"=>[$role["f_code"]],"f_code_main"=>0,"f_type"=>"4","Ttable"=>"t_systems","f_module_name"=>"mdl_usr_lvl_tg");
        $t_selectors_1_2_3_4_5_6_7_8_9_10 = array("f_code"=>[],"f_code_main"=>0,"f_type"=>"1,2,3,4,5,6,7,8,9,10","Ttable"=>"t_selectors","f_module_name"=>"mdl_usr_slc_tg");    
       
        $active = 1/*$this->get_active()*/;
        $password = $this->createPassword();
        $username = $data["username"];
		$phone = $data["phone"];
        $email = strtolower($data['email']);
        $language = $data['language'];
        
        $data_wares = array(
            "fc_usr_level"=> $role["num"],
            "f_code"=>0,
            "f_type_id"=>16,
            "f_phase_id"=> $active,
            "f_module_name"=>"mdl_usr_tg",
            "unique"=>$unique,
            "f_title"=> $data["f_title"],
            "fc_usr_firstname"=>$data["fc_usr_firstname"],
            "fc_usr_lastname"=>$data["fc_usr_lastname"],
            "fc_usr_gender"=>0,
            "fc_usr_address"=>"",
            "fc_usr_phone"=>$phone,
            "fc_usr_mail"=> $email,
            "fc_usr_usn"=> $data["username"],
            "fc_usr_password"=> $password['password'],
            "fc_usr_repeat_pwd"=> $password['password'],
            "fc_usr_language"=>($data['language'] == 'en') ? 1 : (($data['language'] == 'it')? 2 : ""),
            "fc_usr_language_str"=>($data['language'] == 'en') ? "English" : (($data['language'] == 'it')? "Italian" : ""),
            "fc_usr_level_text"=>$role["name"],
            "fc_usr_avatar"=>"",
            "t_wares_parent_16"=>$t_wares_parent_16,
            "t_systems_4"=>$t_systems_4,
            "t_selectors_1,2,3,4,5,6,7,8,9,10"=>$t_selectors_1_2_3_4_5_6_7_8_9_10
        );
        
        $select = new Zend_Db_Select($conn);
        $admin = $select->from(array("t1"=>"t_users"))
                ->join(array('t2'=>'t_creation_date'), 't1.f_code = t2.f_id')
                ->where("t2.f_title='Mainsim Administrator'")->query()->fetchObject(); 
        $objWares = new Mainsim_Model_Wares();
        $result = $objWares->newWares($data_wares,[],true,$conn);
       // var_dump($result);
        $testo = 'Hi ' . $data["f_title"] . "\n\n" . "Here you have credentials to your mainsim application." . "\n" . "Installation: " . $data["db"] . ".mainsim.com" . "\n" . "Username: " . $data["username"] . "\n" . "Password: " . $password["password"] . "\n\n\n" . "Have a great day," . "\n" . "mainsim Support Team";
        $conn->closeConnection();
        $mail = new Mainsim_Model_Mail();
        $mail->sendMail(array('To'=>array($email)),Mainsim_Model_Utilities::chg("Your mainsim installation is ready!"),$testo, null, array('content-type' => 'text/plain'));
        /*if(isset($result['message'])){
            if(strpos($result['message'], 'usr_usn') > 0){
                return array(
                    'code' => 'KO',
                    'message' => $dict["error_email_message"]
                );
            }
            else{
                return array(
                    'code' => 'KO',
                    'message' => $dict["error_message"]
                );
            }
        }
        else{
            $userCode = $result['f_code'];
            try{
                $this->sendMail($language, $username, $password['password'], $_SERVER['HTTP_HOST'], $email, $active);
                return array(
                    'code' => 'OK',
                    'successful_registration_title' => $dict['successful_registration_title'],
                    'successful_registration_header' => $dict['successful_registration_header'],
                    'successful_registration_message' => $active == 1 ? $dict['successful_registration_message'] : $dict['successful_registration_message_approval'],
                    'back' => $dict['back']
                );
            }
            catch(Exception $e){
                if(strpos($e->getMessage(), "5.1.1") !== false){
                    $this->deleteUser($userCode);
                    return array(
                        'code' => 'KO',
                        'message' => $dict["email_not_valid"]
                    );
                }
            }
        }*/die();
    }
    
    public function get_data_custom(){
        $select = new Zend_Db_Select($this->db);
        $link = $select->from(array("t1"=>"t_creation_date"),array("f_description"))
                ->where("t1.f_title = 'MAIL_CUSTOM_REG_DATA'")->where("t1.f_category = 'SETTING'")->where("t1.f_type = 'SYSTEMS'")->query()->fetch(); 
       
        return (($link)? $link["f_description"] : '');
    }
    
    public function get_text_custom(){
        $select = new Zend_Db_Select($this->db);
        $link = $select->from(array("t1"=>"t_creation_date"),array("f_description"))
                ->where("t1.f_title = 'MAIL_CUSTOM_REG_TEXT'")->where("t1.f_category = 'SETTING'")->where("t1.f_type = 'SYSTEMS'")->query()->fetch(); 
       
        return (($link)? $link["f_description"] : '');
    }
    
    public function activateUserMail($userCode){
        $select = new Zend_Db_Select($this->db);
        $select->from('t_users')
               ->where('f_code = ' . $userCode);
        $res = $select->query()->fetchAll();
        $password = $this->createPassword();
        $data_custom = $this->get_data_custom();
        $data_result = '';
        $arr_sup = array();
        if($data_custom != ''){
            $custom_array = json_decode($data_custom,true);
            foreach ($custom_array as $key => $value) {
                $select->reset();
				
				//SOLO NEL CASO DI FC_HIERARCHY
				if($key == 't_selector_ware'){
					$data_query = $select->from(['t1'=>"t_selector_ware"], array('f_selector_id')) 	
						->join(['t2'=>"t_selector_ware"],'t2.f_selector_id = t1.f_selector_id', array('f_ware_id')) 
						->join(['t3'=>"t_creation_date"],'t3.f_id = t2.f_ware_id', array('fc_hierarchy','f_title')) 						
						->where('t1.f_ware_id = ' . $userCode)->where('t3.f_category = "ASSET"')->query()->fetch(); 
				}else{
					$data_query = $select->from(['t1'=>$key] ,$value)  
							->where((($key == 't_creation_date') ? "f_id = " : "f_code = ") . $userCode)
							->query()->fetch();	
				}
               
                for($dq = 0; $dq < count($value); $dq++){
					if($key == 't_selector_ware')  $arr_sup[$value[$dq]] = $data_query[$value[$dq]] . " -> " .  $data_query['f_title'];
                    else $arr_sup[$value[$dq]] = $data_query[$value[$dq]];
                }
            }
        }
		
        $text_custom = $this->get_text_custom();
        if($text_custom != ''){
            $translations = $this->get_translations($language);
            $text_custom_result = $text_custom;
            if(count($arr_sup) > 0){
				foreach($arr_sup as $k => $as){
					$text_custom_result = str_replace(array('<#'.$k . '#>'),array($as),$text_custom_result);
				}
            }
            $data_result = $text_custom_result;
        }
        
        $this->db->update('t_users', array('fc_usr_pwd' => $password['crypted']), 'f_code = ' . $userCode);
        switch($res[0]['fc_usr_language']){
            case 1:
                $language = 'en';
                break;
            
            case 2:
                $language = 'it';
                break;
        }   
        $this->sendMail($language, $res[0]['fc_usr_usn'], $password['password'], $_SERVER['HTTP_HOST'], $res[0]['fc_usr_mail'], "true",$data_result);
    }
    
    public function createPassword(){
        
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $password = implode($pass);
        $objLogin = new Mainsim_Model_Login();
        $crypted = $objLogin->crypt(md5($password));
        return array(
            'password' => $password,
            'crypted' => $crypted
        );
    }
    
    public function createCaptcha(){
        return $this->captchaObj->generate();
    }
    
    private function deleteCaptchaPNG($id){
        try{
            unlink($this->captchaDir . "/" . $id . ".png");
        } 
        catch(Exception $ex){
            print($ex->getMessage());
        }
    }
    
    public function encryptPassword($password){
        
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->salt, $password, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }
    
    public function decryptPassword($password){
        
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->salt, base64_decode($password), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }  
}