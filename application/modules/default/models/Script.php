<?php

class Mainsim_Model_Script
{
    private $db, $table, $f_code, $params, $olds,$edit_batch,$userinfo,$trsl,$skipScript;
    
    public function __construct($db = null,$table = '',$f_code = 0,$params = [],$olds = [],$edit_batch = false,$userinfo = null) {
        if(is_null($db)) {$this->db = Zend_Db::factory(Zend_Registry::get('db'));}
        else {$this->db = $db;}
        $this->table = $table;$this->f_code = $f_code;
        $this->params = $params;$this->olds = $olds;
        
        $settings = Zend_Registry::get('settings');
            if(isset($settings['BANNED_SCRIPT']))
                $this->skipScript = explode(',',$settings['BANNED_SCRIPT']);
            else 
                $this->skipScript = array();
            
        $this->edit_batch = $edit_batch;
        if($userinfo) {
            $this->userinfo = $userinfo;
        }  
        else {
            $this->userinfo = Zend_Auth::getInstance()->getIdentity();
        }        
        // registration doesn't need userinfo
        if(isset($this->userinfo) && isset($this->userinfo->f_language) && $this->userinfo->f_language != null){
            $this->trsl = new Mainsim_Model_Translate('en_GB', $this->db);
            $this->trsl->setLang($this->trsl->getLangNameFromCode($this->userinfo->f_language));
        }
    }
    
    /**
     * Set f_code
     */
    public function setFcode($f_code)
    {
        $this->f_code = $f_code;
    }
    
    /**
     * Set params object
     * @param array $params 
     */
    public function setParams($params)
    {
        $this->params = $params;
    }
    
    public function skipScript($script){
        $this->skipScript[] = $script;
    }
    
    public function execute($params) {
        
        $data = [];
        $select = new Zend_Db_Select($this->db);
        $select->from('t_scripts')
                ->where('f_name = ?', $params["script"])
                ->where("f_type = 'php' ");
        $d = $select->query()->fetch();
        
            if(!$d) {
                  if (file_exists(SCRIPTS_PATH . $params["script"] . '.php')) {
                      require(SCRIPTS_PATH . $params["script"].'.php');
                  }else if (file_exists(SCRIPTS_PATH_DEFAULT . $params["script"] . '.php')){
                       require(SCRIPTS_PATH_DEFAULT . $params["script"].'.php');
                  }else{
                      return false;
                  }
            }else{
               eval($d["f_script"]);               
            }
            return $data;
        
    } 
    
    public function datadbactionemail($params){
        $wf = (int)$params['wf'];

        if($wf > 0){
            $selOp = new Zend_Db_Select($this->db);
            $selOp->reset();

            $resQty = $selOp->from('t_workflows',array('f_name'))
                ->join('t_wf_exits', 't_wf_exits.f_wf_id = t_workflows.f_id',array('f_phase_number','f_exit','f_description','f_id'))
                ->where('t_workflows.f_id =' . $wf)->query()->fetchAll();
        
            $data[] = array(
                "label" => "Selezionare", 
                "value" => "", 
                "selected" => true,
                "wf" => (!empty($resQty[0]['f_name'])) ? $resQty[0]['f_name'] : 'No workflow'
            );
            for($i = 0; $i < count($resQty); $i++){
                $data[] = array(
                    'label' => $resQty[$i]['f_phase_number'] . ' -> ' . $resQty[$i]['f_exit'],    
                    'value' => /*$resQty[$i]['f_id']*/$wf . '-' . $resQty[$i]['f_phase_number'] . '-' . $resQty[$i]['f_exit'],
                    'wf' => $resQty[$i]['f_name'],
                    'ph_name' => $resQty[$i]['f_description']
                );
            }
            
        }else{
            $data[] = array(
                "label" => "Selezionare", 
                "value" => "", 
                "selected" => true,
                "wf" => 'No workflow'
            );
        }
        
        return $data;
    }
    
    /*
Su MainsimObject.php c'è il codice
Ciao Cristian, mi hai obbligato a mettere questo commento per poter in futuro, nel caso ci fossero dei problemi, dare la colpa a me u.u
if($codes[$i]['f_type_id'] == 11) $scriptModule->fc_action_email_ph();
 */
    public function fc_action_email_ph(){
        $selOp = new Zend_Db_Select($this->db);
        $selOp->reset();
        
        
        $get_email_act = $selOp->from('t_wares',array('fc_action_email_cc','fc_action_email_to','fc_action_email_ph','f_action_code','name_script_link','code_link_script'))
                ->join(array('t4' => 't_creation_date'),"t_wares.f_code = t4.f_id",array('f_title','f_description'))
                ->where('t_wares.f_code = ' . $this->f_code)->query()->fetch();
        
        if(!empty($get_email_act['fc_action_email_to'])){
        $id_phase = $get_email_act['fc_action_email_ph'];
        $data_phase = explode("-",$id_phase);
        
        $selOp->reset();

        $resQty = $selOp->from('t_wf_exits',array('f_script'))
                ->where("f_wf_id = " . $data_phase[0] . " and f_phase_number = " . $data_phase[1] . " and f_exit = " . $data_phase[2])->query()->fetch();

        
        //$ccn = ",'Ccn' => array('". str_replace(";","','",$this->params['fc_action_email_ccn'])  ."')";
        
        //obj
        if(strpos($get_email_act['f_title'], '$traductor') !== FALSE){
        $control_dynamic_text2 = str_replace('$traductor', '".$traductor', $get_email_act['f_title']);
        $get_email_act['f_title'] = str_replace('")', '")."', $control_dynamic_text2);
        }
        $control_dynamic_text_0 = str_replace("{", '".', $get_email_act['f_title']);
        $control_dynamic_text = str_replace("}", '."', $control_dynamic_text_0);
        
        //body
        if(strpos($get_email_act['f_description'], '$traductor') !== FALSE){
        $control_dynamic_bd_2 = str_replace('$traductor', '".$traductor', $get_email_act['f_description']);
        $get_email_act['f_description'] = str_replace('")', '")."', $control_dynamic_bd_2);
        }
        $control_dynamic_bd_0 = str_replace("{", '".', $get_email_act['f_description']);
        $control_dynamic_bd = str_replace("}", '."', $control_dynamic_bd_0);
        
        //to
        if($get_email_act['fc_action_email_to'] == '{$to}'){
            $control_dynamic_to_0 = str_replace("{", "", $get_email_act['fc_action_email_to']);
            $control_dynamic_to = str_replace("}", "", $control_dynamic_to_0);
        }else $control_dynamic_to = "array('" . str_replace(";","','",$get_email_act['fc_action_email_to'])  . "')";
        
        //cc
        if($get_email_act['fc_action_email_cc'] == '{$cc}'){
            $control_dynamic_cc_0 = str_replace("{", "", $get_email_act['fc_action_email_cc']);
            $control_dynamic_cc = str_replace("}", "", $control_dynamic_cc_0);
        }else $control_dynamic_cc = "array('" . str_replace(";","','",$get_email_act['fc_action_email_cc'])  . "')";
        
        $cc = ",'Cc' => " . $control_dynamic_cc;
        
        if(strpos($get_email_act['f_action_code'], 'LASCIA QUESTO COMMENTO') !== FALSE){
            $testo_m = "\r\n\r\n\$mail_action = new Mainsim_Model_Mail(\$this->db);
        \r\n\$mail_action->sendMail(array('To'=> array(\$to[\$single_to])" . (($get_email_act['fc_action_email_cc'] != '') ? $cc : '') /*. (($this->params['fc_action_email_ccn'] != '') ? $ccn : '')*/ . ")," . '"' . $control_dynamic_text . '"' . "," . '"' . str_replace("\n" ,"<br>", $control_dynamic_bd) . '"' . ",null,array('content-type'=>'text/html'));\r\n";
            $rs = str_replace('/*LASCIA QUESTO COMMENTO*/', $testo_m,$get_email_act['f_action_code']);
            $mail_custom_default = "/*Aggiunto da ActionMail*/\r\n" . $rs . "/*Aggiunto da ActionMail*/";
       
        }else{
            $mail_custom_default = "/*Aggiunto da ActionMail*/\r\n" . $get_email_act['f_action_code'] . "\r\n\r\n\$mail_action = new Mainsim_Model_Mail(\$this->db);
        \r\n\$mail_action->sendMail(array('To'=> " . $control_dynamic_to . (($get_email_act['fc_action_email_cc'] != '') ? $cc : '') /*. (($this->params['fc_action_email_ccn'] != '') ? $ccn : '')*/ . ")," . '"' . $control_dynamic_text . '"' . "," . '"' . str_replace("\n" ,"<br>", $control_dynamic_bd) . '"' . ",null,array('content-type'=>'text/html'));\r\n/*Aggiunto da ActionMail*/";
       
        }
        
        
        $selOp->reset();
        $element_act = $selOp->from('t_creation_date',array('f_phase_id'))
                ->where('t_creation_date.f_id = ' . $this->f_code)->query()->fetch();
        
        $str_simple_s = '';
        if(strpos($resQty['f_script'], "/*Aggiunto da ActionMail*/") !== FALSE){
            $str_simple = explode("/*Aggiunto da ActionMail*/",$resQty['f_script']);
            unset($str_simple[1]);  
            $str_simple_s = implode("",$str_simple);   
        }else{
            $str_simple_s = $resQty['f_script'];
        }
        
        $new_script = trim($str_simple_s) . (($element_act['f_phase_id'] != 3 && $element_act['f_phase_id'] != 2) ? (((($str_simple_s != '') ? "\r\n\r\n" : ''). $mail_custom_default)) : "" );
        
        $this->db->update('t_wf_exits', array('f_script' => str_replace("#@#","%",$new_script)), "f_wf_id = " . $data_phase[0] . " and f_phase_number = " . $data_phase[1] . " and f_exit = " . $data_phase[2]);
        if($get_email_act['name_script_link'] != '')
            $this->script_mail_on_t_scripts($selOp,$get_email_act['code_link_script'],$get_email_act['name_script_link']);
        
        }
    }
    
    public function script_mail_on_t_scripts($selOp,$code_script,$code_name){
        $selOp->reset();
        
        
        $thereisScriptName = $selOp->from('t_scripts',array('f_id'))
                ->where('t_scripts.f_name = "' . $code_name . '"')->query()->fetch();
        
        if($thereisScriptName['f_id'] > 0)
            $this->db->update('t_scripts', array('f_script' => $code_script), "f_name = '" . $code_name . "'");
        else $this->db->insert("t_scripts",array(
                'f_name'=>$code_name,
                'f_script'=>$code_script,
                'f_type'=>'php'
            ));

    }
    
    public function fc_action_priority_value(){
        if($this->params['f_type_id'] != 11) { return; }
        $selOp = new Zend_Db_Select($this->db);
        $selOp->reset();

        $resQty = $selOp->from('t_wares',array('fc_action_priority_value'))
                ->join('t_creation_date', 't_wares.f_code = t_creation_date.f_id',array('f_title'))
                ->where('t_wares.f_type_id = 11')->where('t_creation_date.f_phase_id <> 3')->query()->fetchAll();

        $priority = '{';
        $arr = [];
        foreach($resQty as $pri){
                if(empty($pri['f_title'])) continue;
                $str = '"'.$pri['fc_action_priority_value'].'":"'.$pri['f_title'].'"';
                array_push($arr,$str);
        }
        $text = implode(",",$arr);
        $priority .= $text . '}';

        $this->db->update('t_bookmarks', array('fc_bkm_filter' => $priority), "fc_bkm_bind = 'f_priority'");
        return [];
    }
    
    /*control for user's role*/
    public function t_systems_4(){
        $code = $this->f_code;
        $select = new Zend_Db_Select($this->db);
        $select->reset();
        $res_role = $select->from(array("t1"=>"t_users"),array('fc_usr_level_text'))
            ->where("t1.f_code =" . $code)->query()->fetch();
        if(empty($res_role['fc_usr_level_text'])) throw new Exception($this->trsl->_("Set a role for the user"));
        
    }
    
    /**
     * Script for check during insert in t_selectors 
     */
    public function t_selector_parent() 
    {
        $select = new Zend_Db_Select($this->db);
        $keys = array_keys($this->params);        
        foreach($keys as $key) {
            if(strpos($key, "t_selectors_parent_") !== false) {
                if(!empty($this->params[$key]['f_code'])) { //check if there is a father                    
                    $select->reset();
                    $res_types = $select->from(array("t1"=>"t_selectors"),[])
                        ->join(array("t2"=>"t_selectors_types"),"t1.f_type_id = t2.f_id",array("f_id","f_type"))
                        ->where("t1.f_code IN (?)",$this->params[$key]['f_code'])
                        ->group("t2.f_id")->group("t2.f_type")->query()->fetchAll();
                    if(count($res_types) == 1 ) { // change type of selector with the type of the father
                        //check if father have type 0 (error)
                        if($res_types[0]['f_id'] == -1) {
                            throw new Exception($this->trsl->_("Wrong father assigned"));
                        }                        
                        $this->db->update("t_creation_date",array("f_category"=>$res_types[0]['f_type']),"f_id = {$this->f_code}");
                        $this->db->update("t_selectors",array("f_type_id"=>$res_types[0]['f_id']),"f_code = {$this->f_code}");                        
                    }
                    else { //error
                        throw new Exception($this->trsl->_("Wrong father assigned"));
                    }                            
                }
                break;
            }
        }
    }
    
    
    public function overlapLabors(){
        $select = new Zend_Db_Select($this->db);
        
        // check if overlap control is set
        $select->from('t_creation_date')
               ->where("f_title = 'overlap_labors'");
        $res = $select->query()->fetchAll();
        
        
        if(count($res) > 0 && !is_null($res[0]['f_description'])){
            $aux = array(); 
            if(isset($this->params['t_wares_2']['f_code'])){
                $wareKey = 'f_title';
                $woKey = $res[0]['f_description'];
                $title = 'Sovrapposizione Dipendenti:';
                
                // get paircross already created
                for($i = 0; $i < count($this->params['t_wares_2']['f_pair']); $i++){
                    if($this->params['t_wares_2']['f_pair'][$i]['f_code'] > 0){
                        $alreadyExistent[] = $this->params['t_wares_2']['f_pair'][$i]['f_code'];
                    }
                }
                
                for($i = 0; $i < count($this->params['t_wares_2']['f_pair']); $i++){
                    
                    if($this->params['t_wares_2']['f_pair'][$i]['f_code'] < 0){
                        // search overlap in other workorders
                        $select->reset();
                        $select->from('t_pair_cross', array())
                               ->join('t_workorders', 't_pair_cross.f_code_main = t_workorders.f_code', array())
                               ->join('t_custom_fields', 't_custom_fields.f_code = t_workorders.f_code', array())
                               ->join('t_creation_date', 't_creation_date.f_id = t_pair_cross.f_code_cross', array('f_title'))
                               ->where('f_code_cross =' . $this->params['t_wares_2']['f_pair'][$i]['f_code_cross'])
                               ->where('t_workorders.f_code <> ' . $this->f_code)
                               ->where('((t_pair_cross.fc_rsc_since <= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_since'] . ' AND t_pair_cross.fc_rsc_till >= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_till'] . ') OR ' .
                                        '(t_pair_cross.fc_rsc_since <= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_since'] . ' AND t_pair_cross.fc_rsc_till >= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_since'] . ') OR ' .
                                        '(t_pair_cross.fc_rsc_since <= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_till'] . ' AND t_pair_cross.fc_rsc_till >= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_till'] . ') OR ' .
                                        '(t_pair_cross.fc_rsc_since >= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_since'] . ' AND t_pair_cross.fc_rsc_till <= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_till'] . '))');
                        $select->columns($woKey . ' AS wo_key');
                        $res = $select->query()->fetchAll();
                        $aux = array_merge($aux, $res);

                        // search overlap in same workoder
                        if(is_array($alreadyExistent)){
                            $select->reset();
                            $select->from('t_pair_cross', array())
                                   ->join('t_workorders', 't_pair_cross.f_code_main = t_workorders.f_code', array())
                                   ->join('t_custom_fields', 't_custom_fields.f_code = t_workorders.f_code', array())
                                   ->join('t_creation_date', 't_creation_date.f_id = t_pair_cross.f_code_cross', array('f_title'))
                                   ->where('f_code_cross =' . $this->params['t_wares_2']['f_pair'][$i]['f_code_cross'])
                                   ->where('t_workorders.f_code = ' . $this->f_code)
                                   ->where('((t_pair_cross.fc_rsc_since <= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_since'] . ' AND t_pair_cross.fc_rsc_till >= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_till'] . ') OR ' .
                                            '(t_pair_cross.fc_rsc_since <= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_since'] . ' AND t_pair_cross.fc_rsc_till >= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_since'] . ') OR ' .
                                            '(t_pair_cross.fc_rsc_since <= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_till'] . ' AND t_pair_cross.fc_rsc_till >= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_till'] . ') OR ' .
                                            '(t_pair_cross.fc_rsc_since >= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_since'] . ' AND t_pair_cross.fc_rsc_till <= ' . $this->params['t_wares_2']['f_pair'][$i]['fc_rsc_till'] . '))')
                                   ->where('t_pair_cross.f_id IN (' . implode(',', $alreadyExistent) . ')');
                            $select->columns($woKey . ' AS wo_key');
                            $res = $select->query()->fetchAll();
                            $aux = array_merge($aux, $res);     
                        }
                    }
                }
            }
            if(count($aux) > 0){
                $result = array();
                for($i = 0; $i < count($aux); $i++){
                    if(!isset($result[$aux[$i][$wareKey]])){
                        $result[$aux[$i][$wareKey]] = array(
                            $aux[$i]['wo_key']
                        );
                    }
                    else{
                        if(!in_array($aux[$i]['wo_key'], $result[$aux[$i][$wareKey]])){
                            $result[$aux[$i][$wareKey]][] = $aux[$i]['wo_key'];
                        }
                    }
                }
                $message = $title . "<br>";
                $message .= "------------------------------<br>";
                foreach($result as $key => $value){
                    $message .="<br>" . $key . "<br>";
                    for($j = 0; $j < count($value); $j++){
                        $message .= "    - workorder " . $value[$j] . "<br>";
                    }

                }
                
                return $message;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
            
    /**
     * Insert number of contracts associate inside wo
     * @return type 
     */
    public function fc_contracts()
    {
        $select = new Zend_Db_Select($this->db);   
        if($this->table == 't_workorders') {
            $res_doc = $select->from(array("t1" => "t_ware_wo"), array('num'=>'count(*)'))
                ->join(array("t2" => "t_wares"), "t1.f_ware_id = t2.f_code",[])
                ->where("t1.f_wo_id = {$this->f_code}")->where("t2.f_type_id = 7")->query()->fetch();
        }
        elseif($this->table == 't_wares') {
            $res_doc = $select->from(array("t1" => "t_wares_relations"), array('num'=>'count(*)'))                
                ->where("t1.f_code_ware_master = {$this->f_code}")->where("t1.f_type_id_slave = 7")->query()->fetch();
        }
        else return [];
        $this->db->update("t_creation_date", array("fc_contracts" => $res_doc['num']), "f_id = {$this->f_code}");
        return [];
    }
    
    /**
     * Insert number of documents associate inside wo
     * @return type 
     */
    public function fc_documents()
    {
        $select = new Zend_Db_Select($this->db);   
        if($this->table == 't_workorders') {
            $res_doc = $select->from(array("t1" => "t_ware_wo"), array('num'=>'count(*)'))
                ->join(array("t2" => "t_wares"), "t1.f_ware_id = t2.f_code",[])
                ->join(array("cd" => "t_creation_date"), "cd.f_id = t1.f_ware_id and cd.f_wf_id = 9 and cd.f_phase_id in (1,2,4)",[])
                ->where("t1.f_wo_id = {$this->f_code}")->where("t2.f_type_id = 5")->query()->fetch();
        }
        elseif($this->table == 't_wares') 
        {   
            $select->reset();
            $res_doc = $select->from(array("t1" => "t_wares_relations"), array('num'=>'count(*)'))                
                ->where("t1.f_code_ware_master = {$this->f_code}")
                ->where("t1.f_type_id_slave = 5")
                ->join(array("cd" => "t_creation_date"),"cd.f_id = t1.f_code_ware_slave and cd.f_wf_id = 9 and cd.f_phase_id in (1,2,4)",[])
                ->query()->fetch();
        }
        else return [];
        
        $this->db->update("t_creation_date", array("fc_documents" => $res_doc['num']), "f_id = {$this->f_code}");
        return [];
    }


    /**
     * update icon about resource allocated to wo
     * @return type return error if present
     */
    public function fc_wo_rsc()
    {        
        $select = new Zend_Db_Select($this->db);
        $res_wo_rsc = $select->from("t_ware_wo",array('num'=>'count(*)'))
            ->join("t_creation_date","f_wo_id = t_creation_date.f_id",[])
            ->join("t_wares","t_wares.f_code = t_ware_wo.f_ware_id",[])
            ->join(array("t4"=>"t_wf_phases"),"t_creation_date.f_phase_id = t4.f_number and t_creation_date.f_wf_id = t4.f_wf_id",[])
            ->join(array("t5"=>"t_wf_groups"),"t5.f_id = t4.f_group_id",[])            
            ->where("t5.f_id NOT IN (6,7) ")->where("f_wo_id = {$this->f_code}")
            ->where("t_wares.f_type_id = 2")->query()->fetch();            
        $this->db->update("t_workorders",array("fc_wo_rsc"=>$res_wo_rsc['num']),"f_code = {$this->f_code}");	   
        return [];
    }
    
    /**
     * update icon about resource allocated to wo
     * @return type return error if present
     */
    public function fc_wo_tool()
    {        
        $select = new Zend_Db_Select($this->db);
        $res_wo_tool = $select->from("t_ware_wo",array('num'=>'count(*)'))
            ->join("t_creation_date","f_wo_id = t_creation_date.f_id",[])
            ->join("t_wares","t_wares.f_code = t_ware_wo.f_ware_id",[])
            ->join(array("t4"=>"t_wf_phases"),"t_creation_date.f_phase_id = t4.f_number and t_creation_date.f_wf_id = t4.f_wf_id",[])
            ->join(array("t5"=>"t_wf_groups"),"t5.f_id = t4.f_group_id",[])            
            ->where("t5.f_id NOT IN (6,7) ")->where("f_wo_id = {$this->f_code}")
            ->where("t_wares.f_type_id = 4")->query()->fetch();            
        $this->db->update("t_workorders",array("fc_wo_tool"=>$res_wo_tool['num']),"f_code = {$this->f_code}");	   
        return [];
    }
    
    /**
     * update icon about resource allocated to wo
     * @return type return error if present
     */
    public function fc_wo_inventory()
    {        
        $select = new Zend_Db_Select($this->db);
        $res_wo_inv = $select->from("t_ware_wo",array('num'=>'count(*)'))
            ->join("t_creation_date","f_wo_id = t_creation_date.f_id",[])
            ->join("t_wares","t_wares.f_code = t_ware_wo.f_ware_id",[])
            ->join(array("t4"=>"t_wf_phases"),"t_creation_date.f_phase_id = t4.f_number and t_creation_date.f_wf_id = t4.f_wf_id",[])
            ->join(array("t5"=>"t_wf_groups"),"t5.f_id = t4.f_group_id",[])            
            ->where("t5.f_id NOT IN (6,7) ")->where("f_wo_id = {$this->f_code}")
            ->where("t_wares.f_type_id = 3")->query()->fetch();            
        $this->db->update("t_workorders",array("fc_wo_inventory"=>$res_wo_inv['num']),"f_code = {$this->f_code}");	   
        return [];
    }
    
    
    /**
     * Create corrective taqsk if one of the task assign is not executable 
     * or Asset quality controll is not acceptable
     * @return array 
     */
    public function t_wares_10()
    {
        //check if one of the tasks assigned is not executable or not acceptable
        if(is_array($this->params['t_wares_10']['f_pair'])){
            $wo = new Mainsim_Model_Workorders($this->db);
            $trslt = new Mainsim_Model_Translate(null, $this->db);
            $userLang = $trslt->getUserLang($this->userinfo->f_language);
            
            $trslt->setLang($userLang);

            $settings = Zend_Registry::get('settings');
            $COT_title=(isset($settings['COT_TITLE']) && $settings['COT_TITLE'])?$trslt->_($settings['COT_TITLE'],false):$trslt->_("Corrective task raised from",false);
                    
            $select_task = new Zend_Db_Select($this->db);
            //get fc_progress of wo
            $resFcProgressFather = $select_task->from('t_creation_date',array('fc_progress'))
                ->where('f_id = ?',$this->f_code)->query()->fetch();
            $tot = count($this->params['t_wares_10']['f_pair']);
            
            // evaluate if a corrective workorder must be generated
            
            $json = isset($settings['CORRECTIVE_AUTOGEN'])?$settings['CORRECTIVE_AUTOGEN']:null;
            //$json = $json?$json:'{"lock":1}';
            $lock = 0;
            $autogen = $json?json_decode($json):null;
            if($autogen && isset($autogen->lock)) {
                $lock = $autogen->lock;
            }
            for($i = 0;$i < $tot;++$i) {
                $line_task = $this->params['t_wares_10']['f_pair'][$i];            
                if( !$lock && ( (!empty($line_task['fc_task_issue_not_executable' ]))
                    || (!empty($line_task['fc_task_quality_not_acceptable' ]))
                )) {
                    // new check with field f_code_task
                    $select_task->reset();                    
                    $res_check = $select_task->from(array('t1'=>'t_workorders'))
                        ->join(array('t2'=>'t_ware_wo'), 't1.f_code = t2.f_wo_id', [])
                        ->where('t1.f_type_id = 10')->where('t2.f_ware_id = ' . $line_task['f_code_cross'])
                        ->where('t1.f_code_periodic = ' . $line_task['f_code_main'])
                        ->where('t1.f_code_task = ' . $line_task['f_group_code'])->query()->fetch();
                    if(!empty($res_check)){continue;}
                    $statusTask = $trslt->_('Not performed',false);
                    if(!empty($line_task['fc_task_issue_performed'])) {
                        $statusTask = $trslt->_('Performed',false);
                    }elseif(!empty($line_task['fc_task_issue_not_executable'])){
                        $statusTask = $trslt->_('Not executable',false);
                    }
                    $statusAsset = $trslt->_('Good',false);                    
                    if(!empty($line_task['fc_task_quality_acceptable' ])) {
                        $statusAsset = $trslt->_('Acceptable',false);
                    }elseif(!empty($line_task['fc_task_quality_not_acceptable'])){                        
                        $statusAsset = $trslt->_('Not acceptable',false);
                    }

                    $new_wo_params = [];
                    $new_wo_params['f_type_id'] = 10;
                    $new_wo_params['f_code_periodic'] = $line_task['f_code_main'];        
                    $new_wo_params['f_code_task'] = $line_task['f_group_code'];
                    $new_wo_params['f_module_name'] = "mdl_wo_tg";                    
                    $new_wo_params['fc_wo_generator_id'] = $resFcProgressFather['fc_progress'];
                    $new_wo_params['f_code'] = 0;
                    $new_wo_params['f_title'] = $COT_title." ".$this->params['f_title'];
                    
                    $new_wo_params['f_description'] = $trslt->_("Task")." : {$line_task['f_title']}".PHP_EOL;  
                    $new_wo_params['f_description'] .= $trslt->_("Status task",false)." : {$statusTask}".PHP_EOL;  
                    $new_wo_params['f_description'] .= $trslt->_("Status asset",false)." : {$statusAsset}".PHP_EOL;
                    $new_wo_params['f_description'] .= $trslt->_("Notes",false)." : {$line_task['fc_task_notes']}".PHP_EOL;
                    array_walk($new_wo_params,function($val){$val = Mainsim_Model_Utilities::chg($val);});
                    //add asset
                    $new_wo_params['t_wares_1'] = array(
                        'f_code_main'=>0,
                        'f_code'=>array($line_task['f_code_cross']),
                        'f_type'=>1,
                        'Ttable'=>'t_wares',
                        "f_module_name"=>"mdl_wo_asset_tg"
                    );                   
                    $res_ct = $wo->newWorkOrder($new_wo_params,false,$this->userinfo);
                    
                    if (isset($res_ct['message']) ) { throw new Exception($res_ct['message']); }
                }
            }            
        }              
    }
    
    
    /**
     * Create asset hierarchy 
     * @return array 
     */
    public function t_wares_1()
    {    
        if($this->table == 't_workorders' && (!empty($this->params['t_wares_1']['f_code']) || !$this->edit_batch) ) {
            $this->fc_wo_asset();
            //other script linked to t_wares_1                        
            $this->fc_asset_opened_std();            
            $this->fc_asset_opened_wo();    
            $this->fc_asset_opened_pm();
            $this->fc_asset_opened_on_condition(); 
            $this->fc_asset_wo();
            $this->linkWaresFromAsset();
            //$this->attachDocFromAsset();
            //$this->t_wares_1_priority();
            
            /*
            Mainsim_Model_Utilities::newRecord($this->db, $resAsset[$i]['f_code'], "t_creation_date", $creationData,'Automatic fc_asset_downtime_total_hours update.');
            Mainsim_Model_Utilities::newRecord($this->db, $resAsset[$i]['f_code'], "t_wares", $waresData,'Automatic fc_asset_downtime_total_hours update.');
            Mainsim_Model_Utilities::newRecord($this->db, $resAsset[$i]['f_code'], "t_custom_fields", $customData,'Automatic fc_asset_downtime_total_hours update.');                
             * */             
        }
        else if($this->params['f_type_id'] == 5 && $this->table == 't_wares' && (!empty($this->params['t_wares_1']['f_code']) || !$this->edit_batch) )
        {
            //se dal modulo "File e Link" ho rimosso o aggiunto al documento dei assets , aggiorno il campo fc_documents dei asset e dei suoi parents
            $select = new Zend_Db_Select($this->db); 
            $array = $this->params['t_wares_1']['f_code'];
            $res_asset = [];
            if(count($array)>0){
                $str = '('.implode(",", $array).')';
                $res_asset = $select->from(array("cd" => "t_creation_date"), array('cd.f_id'))
                            ->where("cd.f_id in {$str} and cd.f_wf_id = 33 and cd.f_phase_id in (1,2,4)",[])
                    ->query()->fetchAll();
                $select->reset();
            }
            $now = array();
            foreach ($res_asset as $value) {
                if(in_array($value['f_id'],$array))
                    array_push ($now, $value['f_id']);
            }
             // $now = array di asset attivi associati attualmente al documento
            $old = [];
            if(isset($this->olds['old_ware_rel'])) $old = $this->olds['old_ware_rel'];
            $old_asset_f_codes = [];
            foreach($old as $ware){
                if( $ware['f_code_ware_master'] == $this->f_code && $ware['f_type_id_slave'] == 1 ) 
                    array_push($old_asset_f_codes,$ware['f_code_ware_slave']);
            }
            $before = array();
            if(count($old_asset_f_codes)>0)
            {
                $comma_string = '('.implode(",", $old_asset_f_codes).')';
                $res_asset = $select->from(array("cd" => "t_creation_date"), array('cd.f_id'))                
                    ->where("cd.f_id in {$comma_string}")
                    ->where("cd.f_wf_id = 33 and cd.f_phase_id in (1,2,4)")
                    ->query()->fetchAll();
                $select->reset();            
                foreach ($res_asset as $value) {
                    if(in_array($value['f_id'],$old_asset_f_codes))
                        array_push ($before, $value['f_id']);
                }
            }    
            //before = array di asset attivi associati precedentemente al documento
            //print_r($before);
            //print_r($now);
            $removed_wo_wares = array();
            $added_wo_wares = array();
            foreach($before as $f_ware_code){
                if(count($now) == 0 || !in_array($f_ware_code,$now)) array_push($removed_wo_wares,$f_ware_code);
            }
            foreach($now as $f_ware_code){
                 if(count($before)== 0 || !in_array($f_ware_code,$before)) array_push($added_wo_wares,$f_ware_code);
            }
            
            $to_decrease= array();
            $select = new Zend_Db_Select($this->db);
             foreach ($removed_wo_wares as $f_code){
                 $parents=array();
                 $parents = $this->getParents($select,$f_code);
                 $to_decrease = array_merge($to_decrease,$parents);
             }
             $this -> updateNumFc_DocumentsBy($to_decrease,-1);

            $to_increase= array();
            $select = new Zend_Db_Select($this->db);
             foreach ($added_wo_wares as $f_code){
                 $parents=array();
                 $parents = $this->getParents($select,$f_code);
                 $to_increase = array_merge($to_increase,$parents);
             }
             $this -> updateNumFc_DocumentsBy($to_increase,1);    
        }
        return [];
    }
    
    private function updateNumFc_DocumentsBy($parents,$operator)
    {
        $array = array_count_values($parents);
        $select = $this->db->select();
        foreach ($array as $f_code_asset => $value) {
            $select->reset();
            $res = $select->from('t_creation_date',array('fc_documents'))
                ->where('f_id=?', $f_code_asset)->query()->fetch();
            $res_count = $res['fc_documents'] + ($value*$operator);
            $this->db->update("t_creation_date", array("fc_documents" => $res_count), "f_id = {$f_code_asset}");
            $select -> reset();         
        }
    }
    
    public function fc_wo_asset($f_code = 0){
    
        if(in_array('fc_wo_asset', $this->skipScript))return;
        
        if($f_code == 0) {
            $f_code = $this->f_code;
        }
        $select_hy = new Zend_Db_Select($this->db);
        $asset_hierarchies = [];
        $res_ware_wo = $select_hy->from(array("t1"=>"t_ware_wo"),[])
                ->join(array("t2"=>"t_creation_date"),"t1.f_ware_id = t2.f_id",array('f_title','fc_hierarchy'))
                ->where("t2.f_category = 'ASSET'")->where("t1.f_wo_id = ?",$f_code)
                ->query()->fetchAll();        
        $tot_ww = count($res_ware_wo);
        for($ww = 0;$ww < $tot_ww;++$ww) {            
            $asset_hierarchies[$res_ware_wo[$ww]['f_title']] = $res_ware_wo[$ww]['fc_hierarchy'];                
        }
        $new_fc_asset_hy = [];
        foreach ($asset_hierarchies as $key_hy =>$value_hy) {                            
            $new_fc_asset_hy[] = !empty($value_hy)?"$key_hy ($value_hy)":"$key_hy";
        }
        $fc_hierarchy = implode(", ",$new_fc_asset_hy);   
        $this->db->update("t_workorders",array("fc_wo_asset"=>$fc_hierarchy),"f_code = {$f_code}");
    }
    
        
    /**
     * insert any file or image into the system
     * @return []
     */
    public function saveFile($f_code,$table,$column,$value,$special_folder = '')
    {        
        $error = [];
        $field = 'f_code';
        if($table == 't_creation_date'){
            $field = 'f_id';
        }
        //check if there is a new icon and manage change batch
        $sel_doc = new Zend_Db_Select($this->db);        
        if(($this->edit_batch && empty($value) ) ) {
            return;        
        }        
        $attachmentFolder = Zend_Registry::get('attachmentsFolder');
        //recupero il setting, per sapere se devo salvare o no i vecchi attachments
        $sel_doc->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
                ->join(array("s" => "t_systems"), "c.f_id = s.f_code")
                ->where("c.f_type = 'SYSTEMS'")
                ->where("c.f_category = 'SETTING'")
                 ->where("c.f_title = 'save_old_attachments'");

        $settings = $sel_doc->query()->fetch();
        $save_old_attachments = (empty($settings))?0:$settings['f_description'];
        $sel_doc->reset();
        
        if($save_old_attachments!= '1'){
               //remove old attachment with f_active = -1  
               $resDelAttach = $sel_doc->from('t_attachments',array('f_id','f_path'))
                   ->where('f_active = -1')->query()->fetchAll();        
               $totDA = count($resDelAttach);
               for($i_oa = 0;$i_oa < $totDA;++$i_oa) {            
                   $this->db->delete('t_attachments',"f_id = {$resDelAttach[$i_oa]['f_id']}");
                   @unlink($resDelAttach[$i_oa]['f_path']);
               }
        } 
        
        //if value is equal to remove, clear field and set attachment read to be deleted next time
        if($value == 'remove') {
            $sel_doc->reset();            
            $res_si = $sel_doc->from($table.'_history',$column)->where("f_code = ?",$f_code)->order('f_id desc')->limit(1)
                ->query()->fetch();            
            $si_exp = explode('|',$res_si[$column]);        
            $si = isset($si_exp[0])?$si_exp[0]:0;               
            $sel_doc->reset();
            $resAttach = $sel_doc->from("t_attachments","f_id")->where("f_session_id = ?",$si)->query()->fetchAll();                    
            $totRa = count($resAttach);
            for($ra = 0;$ra < $totRa;++$ra) {
                $this->db->update("t_attachments",array("f_active"=>-1),"f_id = {$resAttach[$ra]['f_id']}");
            }
            $this->db->update($table,array($column=>''),"$field = {$f_code}");
            return;
        }        
        //remove old attach
        $si_exp = explode('|',$value);        
        $si = isset($si_exp[0])?$si_exp[0]:0;   
        $sel_doc->reset();
        $resAttach = $sel_doc->from("t_attachments")->where("f_session_id = ?",$si)->query()->fetchAll();        
        
        if(empty($resAttach)){            
            return;
        }
        //if f_code = 0 is a new file. find and delete old attachment
        if($resAttach[0]['f_code'] == 0) {            
            $sel_doc->reset();
            $resOldAttach = $sel_doc->from('t_attachments',array('f_id'))
                ->where('f_code = ?',$f_code)->where("f_fieldname = ?",$resAttach[0]['f_fieldname'])
                ->query()->fetchAll();            
            $totOA = count($resOldAttach);
            for($i_oa = 0;$i_oa < $totOA;++$i_oa) {
                $this->db->update('t_attachments',array("f_active"=>-1),"f_id = {$resOldAttach[$i_oa]['f_id']}");                
            }
        } 
        else {
            return;
        }        
        
        //check if exist folder with f_code name to put new attach 
        $type = $resAttach[0]['f_type'] == 'doc'?'doc':'img';
        $type = $special_folder != '' ? $special_folder:$type;        
        $dir = realpath($attachmentFolder.'/'.$type)."/$f_code";                            
        if(!is_dir($dir)) {//check if dir exist, if not create it
            if(!mkdir($dir, 0777, true)) { // if is impossible to create folder, don't create and send error message
                    $dir = '';
            }                                
        }
        if(!empty($dir)) {            
            foreach($resAttach as $line_doc) {
                $id = $line_doc['f_id'];
                //change session id adding f_code
                $new_session_id = $line_doc['f_session_id'].'_'.$f_code;
                $new_path = $dir.'/'.$new_session_id.'_'.$line_doc['f_type'].'.'.$line_doc['f_file_ext'];                
                if(file_put_contents($new_path, file_get_contents(realpath($line_doc['f_path'])))) { //if ok, remove temp and change params in db
                    $new_doc = $line_doc;
                    $new_doc['f_code'] = $f_code;
                    $new_doc['f_path'] = $new_path; 
                    $new_doc['f_session_id'] = $new_session_id;
                    unset($new_doc['f_id']);
                    $this->db->insert("t_attachments",$new_doc);                    
                }
                else { // in case of error
                    foreach($resAttach as $line_doc) {
                        $new_path = $dir.'/'.$line_doc['f_session_id'].'_'.$line_doc['f_type'].'.'.$line_doc['f_file_ext'];
                        unlink($line_doc['f_path']); // remove temp file
                        unlink($new_path);// remove new file,
                        $this->db->delete("t_attachments","f_code = 0 and f_session_id = '{$line_doc['f_session_id']}'");//remove temp row on db
                        $this->db->delete("t_attachments","f_code = $f_code and f_session_id = '{$line_doc['f_session_id']}'");//remove file row on db
                    }
                    for($i_oa = 0;$i_oa < $totOA;++$i_oa) {//re-enable old attachment
                        $this->db->update('t_attachments',array("f_active"=>0),"f_id = {$resOldAttach[$i_oa]['f_id']}");                
                    }
                    throw new Exception($this->trsl->_('Cannot save file, contact administrator'));                    
                }
            }                    
            $this->db->update($table,array($column=>$new_session_id.'|'.$si_exp[1]),"$field = {$f_code}");
        }
        else {
            throw new Exception($this->trsl->_('Cannot save file, contact administrator'));
        }
        return $error;
    }
    
    public function fc_asset_available($f_code_asset)
    {
        if($this->params['f_type_id'] == 1 || $this->params['f_type_id'] == 10 || $this->params['f_type_id'] == 13) {            
            $select_udt = new Zend_Db_Select($this->db);	            
            //add downtime hour                            
            $select_udt->reset();
            $res_dt = $select_udt->from("t_wares",array("fc_asset_downtime_total_hours","fc_asset_operational_time"))
                    ->where("f_code = ?",$f_code_asset)->query()->fetch();
            $udt = (int)($res_dt['fc_asset_downtime_total_hours']);
            $opt = (int)$res_dt['fc_asset_operational_time'];            
            $av = 0;
            if($opt){
                $av = number_format((($opt-$udt) * 100)/$opt,3);
            }                    
            $this->db->update("t_wares",array("fc_asset_availability"=>$av),"f_code = {$f_code_asset}");
        }
    }
    
    /**
     * update number of failure of an asset if the wo is closed 
     */
    private function wo_fc_assetno_failures_wo($f_code_asset)
    {        
        if($this->params['f_type_id'] == 1 || $this->params['f_type_id'] == 10 || $this->params['f_type_id'] == 13) {
            $select_udt = new Zend_Db_Select($this->db);                        
            //add downtime hour  
            $res_dt = $select_udt->from(array('t1'=>"t_wares"),array("num"=>"count(*)"))
                ->join(array('t2'=>'t_ware_wo'), "t1.f_code = t2.f_ware_id",[])
                ->join(array('t3'=>'t_workorders'), "t3.f_code = t2.f_wo_id",[])
                ->join(array('t4'=>'t_creation_date'), "t3.f_code = t4.f_id",[])
                ->join(array('t5'=>'t_wf_phases'), "t4.f_wf_id = t5.f_wf_id and t4.f_phase_id = t5.f_number",[])
                ->where("t1.f_code = ?",$f_code_asset)->where("t3.f_type_id IN (1,10,13) ")->where("t5.f_group_id = 6")
                ->query()->fetch();
            $no_fail = (int)$res_dt['num'];            
            $this->db->update("t_wares",array("fc_asset_no_failures"=>$no_fail),"f_code = {$f_code_asset}");
        }
        return [];
    }
       
    
    /**
     * update number of on condition(type 6) associate to an asset 
     */
    public function fc_asset_opened_on_condition()
    {
        if($this->table == 't_workorders') 
        {
            if(in_array($this->params['f_type_id'], array(6))) 
            {	                
                if(isset($this->f_code) && $this->f_code != 0) 
                {
                    $select = new Zend_Db_Select($this->db);
                    $resWo = $select->from(array("cd" => "t_creation_date"), [])
                                    ->join(array("wp" => "t_wf_phases"), "wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id", [])
                                    ->join(array("wg" => "t_wf_groups"), "wg.f_id = wp.f_group_id", ['f_id as group'])
                                    ->where('cd.f_id =' . $this->params['f_code'])
                                    ->query()->fetch();
                    $select->reset();
                    $removed_wo_wares = array(); $added_wo_wares = array();
                    
                    //se il wo non è stato cancellato , chiuso o clonato
                    if(!in_array($resWo['group'], array(6,7,8))){
                        //ottengo gli asset non più associati al wo(removed_wo_wares) e quelli di nuova associazione (added_wo_wares)
                        if(isset($this->olds['old_ware_wo_cross']) && isset($this->params['t_wares_1']['f_code']))
                            $this->getWoWaresCrossDifferences($this->olds['old_ware_wo_cross'],$this->params['t_wares_1']['f_code'],$removed_wo_wares,$added_wo_wares);            
                    }
                    else{
                        if(!empty($this->olds['old_ware_wo_cross'])) $removed_wo_wares =  array_column($this->olds['old_ware_wo_cross'], 'f_ware_id');
                    }
                    
                    if(count($removed_wo_wares) > 0) {
                        $this->db->update('t_wares', array('fc_asset_opened_on_condition' => new Zend_Db_Expr("fc_asset_opened_on_condition - 1")), 'f_code IN (' . implode(',', $removed_wo_wares) . ')');
                    }
                    
                    if(count($added_wo_wares) > 0) {
                        $this->db->update('t_wares', array('fc_asset_opened_on_condition' => new Zend_Db_Expr("fc_asset_opened_on_condition + 1")), 'f_code IN (' . implode(',', $added_wo_wares) . ')');
                    }
                    
                    //Mainsim_Model_Utilities::saveReverseAjax("t_wares_parent",$f_ware_code, $parent_1, "upd", "mdl_asset_tg",$this->db);
                }            
            }
        }
        elseif($this->table == 't_wares' && $this->params['f_type_id'] == 1 && $this->f_code != 0){ 
            $select = new Zend_Db_Select($this->db);
            //recupero il valore di fc_asset_openend_on_condition
            $res = $select->from('t_ware_wo as ww',array('count(*) as fc_asset_opened_on_condition'))
                ->join(array("cd" => "t_creation_date"), "cd.f_id =  ww.f_wo_id", [])
                ->join(array("wo" => "t_workorders"), "wo.f_code = cd.f_id", [])
                ->join(array("wp" => "t_wf_phases"), "wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id", [])
                ->where('ww.f_ware_id = ?', $this->f_code)
                ->where('wo.f_type_id IN (6)')
                ->where('wp.f_group_id NOT IN (6,7,8)')
                ->query()->fetch();
            $fc_asset_opened_on_condition = $res['fc_asset_opened_on_condition'];  
            $this->db->update('t_wares', array('fc_asset_opened_on_condition' =>$fc_asset_opened_on_condition), 'f_code = '.$this->f_code);
        }
    }
    
    /**
     * update number of standard wo associate to an asset 
     */
    public function fc_asset_opened_std()
    {        
        $select_ware_1 = new Zend_Db_Select($this->db);
        if($this->table == 't_workorders'  && (!$this->edit_batch || !empty($this->params['t_wares_1']['f_code']))) {
            if($this->params['f_type_id'] == 15) {                
                if($this->params['f_code'] != 0) {
                    $tot_a = count($this->olds['old_ware_wo_cross']);
                    for($a = 0;$a < $tot_a;++$a) {
                        if(isset($this->olds['old_ware_wo_cross'][$a]['f_ware_id']) && isset($this->olds['old_ware_wo_cross'][$a]['f_wo_id'])) {
                            $select_ware_1->reset();
                            $res_check = $select_ware_1->from("t_wares")->where("f_type_id = 1")->where("f_code  = ?",$this->olds['old_ware_wo_cross'][$a]['f_ware_id'])
                                    ->query()->fetch();
                            if(!empty($res_check)) {
                                $this->db->update('t_wares',array('fc_asset_opened_std'=>0),"f_code = {$this->olds['old_ware_wo_cross'][$a]['f_ware_id']}");
                            }
                        }
                    }
                }            
                foreach($this->params['t_wares_1']['f_code'] as $f_ware_code) {
                    $select_ware_1->reset();
                    //check how many corrective wo are opened on this ware
                    $res_count = $select_ware_1->from(array("t1"=>"t_ware_wo"),array('num'=>'count(*)'))				
                        ->join(array("t2"=>"t_workorders"),"t1.f_wo_id = t2.f_code",[])
                        ->join("t_creation_date","t_creation_date.f_id = t2.f_code",[])
                        ->join(array("t3"=>"t_wf_phases"),"t_creation_date.f_phase_id = t3.f_number and t_creation_date.f_wf_id = t3.f_wf_id",[])
                        ->join(array("t4"=>"t_wf_groups"),"t4.f_id = t3.f_group_id",[])
                        ->where("t1.f_ware_id = ?",$f_ware_code)->where("t2.f_type_id = ?",$this->params['f_type_id'])				
                        ->where("t4.f_id NOT IN (5,6) ")->query()->fetch();
                    $wo_asset = $res_count['num'];
                    $this->db->update("t_wares",array("fc_asset_opened_std"=>$wo_asset), "f_code = {$f_ware_code}");
                    $select_ware_1->reset();
                    $res_parent_1 = $select_ware_1->from("t_wares_parent",array('f_parent_code'))
                            ->where("f_active = 1")
                            ->where("f_code = ?",$f_ware_code)->query()->fetchAll();
                    $parent_1 = [];
                    foreach($res_parent_1 as $line_1) {
                        $parent_1[] = $line_1['f_parent_code'];
                    }
                    Mainsim_Model_Utilities::saveReverseAjax("t_wares_parent",$f_ware_code, $parent_1, "upd", "mdl_asset_tg",$this->db);
                }                            
                unset($select_ware_1);
            }     
        }
        elseif($this->table == 't_wares') {
            $res_std = $select_ware_1->from("t_ware_wo",array('num'=>'count(*)'))
                ->join("t_workorders","f_wo_id = f_code",[])
                ->join("t_creation_date","f_wo_id = t_creation_date.f_id",[])
                ->join(array("t4"=>"t_wf_phases"),"t_creation_date.f_phase_id = t4.f_number and t_creation_date.f_wf_id = t4.f_wf_id",[])
                ->join(array("t5"=>"t_wf_groups"),"t5.f_id = t4.f_group_id",[])
                ->where("t4.f_id NOT IN (5,6) ")
                ->where("f_ware_id = ?",$this->f_code)
                ->where("f_type_id = 15")
                ->query()->fetch();
            $this->db->update("t_wares",array("fc_asset_opened_std"=>$res_std['num']),"f_code = ".$this->f_code); 
        }
    }
    
    /**
     * update number of standard wo (type 4,5,7) associate to an asset 
     */
    public function fc_asset_opened_pm() 
    {
        if($this->table == 't_workorders') 
        {
            if(in_array($this->params['f_type_id'], array(4, 5, 7))) 
            {	                
                if(isset($this->f_code) && $this->f_code != 0) 
                {
                    $select = new Zend_Db_Select($this->db);
                    $resWo = $select->from(array("cd" => "t_creation_date"), [])
                                    ->join(array("wp" => "t_wf_phases"), "wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id", [])
                                    ->join(array("wg" => "t_wf_groups"), "wg.f_id = wp.f_group_id", ['f_id as group'])
                                    ->where('cd.f_id =' . $this->params['f_code'])
                                    ->query()->fetch();
                    $select->reset();
                    $removed_wo_wares = array(); $added_wo_wares = array();
                    
                    //se il wo non è stato cancellato , chiuso o clonato
                    if(!in_array($resWo['group'], array(6,7,8))){
                        //ottengo gli asset non più associati al wo(removed_wo_wares) e quelli di nuova associazione (added_wo_wares)
                        if( isset($this->olds['old_ware_wo_cross']) && isset($this->params['t_wares_1']['f_code']) ) 
                            $this->getWoWaresCrossDifferences($this->olds['old_ware_wo_cross'],$this->params['t_wares_1']['f_code'],$removed_wo_wares,$added_wo_wares);            
                    }
                    else{
                        if(isset($this->olds['old_ware_wo_cross']) && !empty($this->olds['old_ware_wo_cross'])) $removed_wo_wares =  array_column($this->olds['old_ware_wo_cross'], 'f_ware_id');
                    }
                    
                    if(count($removed_wo_wares) > 0) {
                        $this->db->update('t_wares', array('fc_asset_opened_pm' => new Zend_Db_Expr("fc_asset_opened_pm - 1")), 'f_code IN (' . implode(',', $removed_wo_wares) . ')');
                    }
                    
                    if(count($added_wo_wares) > 0) {
                        $this->db->update('t_wares', array('fc_asset_opened_pm' => new Zend_Db_Expr("fc_asset_opened_pm + 1")), 'f_code IN (' . implode(',', $added_wo_wares) . ')');
                    }
                }            
            }
        }
        elseif($this->table == 't_wares' && $this->params['f_type_id'] == 1 && $this->f_code != 0){ 
            $select = new Zend_Db_Select($this->db);
            //recupero il valore di fc_asset_openend_pm
            $res = $select->from('t_ware_wo as ww',array('count(*) as fc_asset_opened_pm'))
                ->join(array("cd" => "t_creation_date"), "cd.f_id =  ww.f_wo_id", [])
                ->join(array("wo" => "t_workorders"), "wo.f_code = cd.f_id", [])
                ->join(array("wp" => "t_wf_phases"), "wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id", [])
                ->where('ww.f_ware_id = ?', $this->f_code)
                ->where('wo.f_type_id IN (4, 5, 7)')
                ->where('wp.f_group_id NOT IN (6,7,8)')
                ->query()->fetch();
            $fc_asset_opened_pm = $res['fc_asset_opened_pm'];  
            $this->db->update('t_wares', array('fc_asset_opened_pm' =>$fc_asset_opened_pm), 'f_code = '.$this->f_code);
        }
    }
    
    /**
     * update number of standard wo ( type NOT IN (3,4,5,6,7,9,11,14,16,18,19,20) ) associate to an asset 
     */
    public function fc_asset_opened_wo() 
    {      
        if($this->table == 't_workorders') 
        {
            if(!in_array($this->params['f_type_id'], array(3, 4, 5, 6, 7, 9, 11, 14, 16, 18, 19, 20))) 
            {	                
                if(isset($this->f_code) && $this->f_code != 0) 
                {
                    $select = new Zend_Db_Select($this->db);
                    $resWo = $select->from(array("cd" => "t_creation_date"), [])
                                    ->join(array("wp" => "t_wf_phases"), "wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id", [])
                                    ->join(array("wg" => "t_wf_groups"), "wg.f_id = wp.f_group_id", ['f_id as group'])
                                    ->where('cd.f_id =' . $this->f_code)
                                    ->query()->fetch();
                    $select->reset();
                    $removed_wo_wares = array(); $added_wo_wares = array();
                    
                    //se il wo non è stato cancellato , chiuso o clonato
                    if(!in_array($resWo['group'], array(6,7,8))){
                        //ottengo gli asset non più associati al wo(removed_wo_wares) e quelli di nuova associazione (added_wo_wares)
                        if( isset($this->olds['old_ware_wo_cross']) && isset($this->params['t_wares_1']['f_code']) )
                            $this->getWoWaresCrossDifferences($this->olds['old_ware_wo_cross'],$this->params['t_wares_1']['f_code'],$removed_wo_wares,$added_wo_wares);            
                    }
                    else{
                        if(isset($this->olds['old_ware_wo_cross']) && !empty($this->olds['old_ware_wo_cross'])) $removed_wo_wares =  array_column($this->olds['old_ware_wo_cross'], 'f_ware_id');
                    }
                    
                    if(count($removed_wo_wares) > 0) {
                        $this->db->update('t_wares', array('fc_asset_opened_wo' => new Zend_Db_Expr("fc_asset_opened_wo - 1")), 'f_code IN (' . implode(',', $removed_wo_wares) . ')');
                    }
                    
                    if(count($added_wo_wares) > 0) {
                        $this->db->update('t_wares', array('fc_asset_opened_wo' => new Zend_Db_Expr("fc_asset_opened_wo + 1")), 'f_code IN (' . implode(',', $added_wo_wares) . ')');
                    }
                    //Mainsim_Model_Utilities::saveReverseAjax("t_wares_parent",$f_ware_code, $parent_1, "upd", "mdl_asset_tg",$this->db);
                }            
            }
        }
        elseif($this->table == 't_wares' && $this->params['f_type_id'] == 1 && $this->f_code != 0){ 
            $select = new Zend_Db_Select($this->db);
            //recupero il valore di fc_asset_openend_wo
            $res = $select->from('t_ware_wo as ww',array('count(*) as fc_asset_opened_wo'))
                ->join(array("cd" => "t_creation_date"), "cd.f_id =  ww.f_wo_id", [])
                ->join(array("wo" => "t_workorders"), "wo.f_code = cd.f_id", [])
                ->join(array("wp" => "t_wf_phases"), "wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id", [])
                ->where('ww.f_ware_id = ?', $this->f_code)
                ->where('wo.f_type_id NOT IN (3,4,5,6,7,9,11,14,16,18,19,20)')
                ->where('wp.f_group_id NOT IN (6,7,8)')
                ->query()->fetch();
            $fc_asset_opened_wo = $res['fc_asset_opened_wo'];  
            $this->db->update('t_wares', array('fc_asset_opened_wo' =>$fc_asset_opened_wo), 'f_code = '.$this->f_code);
        }
    }

    /**
     * update number of standard wo(opened_wo,opened_pm,opened_on_condiction) associate to an asset and his childrens
     */
    
    public function fc_asset_wo() 
    {
        if($this->table == 't_workorders') 
        {
            if(!in_array($this->params['f_type_id'], array(3, 9, 11, 14, 16, 18, 19, 20))) 
            {	                
                if(isset($this->f_code) && $this->f_code != 0) 
                {
                    $select = new Zend_Db_Select($this->db);
                    $resWo = $select->from(array("cd" => "t_creation_date"), [])
                                    ->join(array("wp" => "t_wf_phases"), "wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id", [])
                                    ->join(array("wg" => "t_wf_groups"), "wg.f_id = wp.f_group_id", ['f_id as group'])
                                    ->where('cd.f_id =' . $this->f_code)
                                    ->query()->fetch();
                    $select->reset();
                    $removed_wo_wares = array(); $added_wo_wares = array();
                    
                    //se il wo non è stato cancellato , chiuso o clonato
                    if(!in_array($resWo['group'], array(6,7,8))){
                        //ottengo gli asset non più associati al wo(removed_wo_wares) e quelli di nuova associazione (added_wo_wares)
                        if( isset($this->olds['old_ware_wo_cross']) && isset($this->params['t_wares_1']['f_code']) )
                            $this->getWoWaresCrossDifferences($this->olds['old_ware_wo_cross'],$this->params['t_wares_1']['f_code'],$removed_wo_wares,$added_wo_wares);            
                    }
                    else{
                        if(isset($this->olds['old_ware_wo_cross']) && !empty($this->olds['old_ware_wo_cross'])) $removed_wo_wares =  array_column($this->olds['old_ware_wo_cross'], 'f_ware_id');
                    }
                    
                    if(count($removed_wo_wares) > 0) 
                    {
                        foreach($removed_wo_wares as $ware) {
                            $parents = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $ware, array(), $this->db);
                            unset($parents[count($parents) - 1]);
                            $parents [] = $ware;

                            // fc_asset_wo
                            if (count($parents) > 0) {
                                $this->db->update('t_wares', array('fc_asset_wo' => new Zend_Db_Expr('fc_asset_wo - 1')), 'f_code IN (' . implode(',', $parents) . ')');
                            }
                        }           
                    }
                    
                    if(count($added_wo_wares) > 0) {
                        
                        foreach($added_wo_wares as $ware) {
                            $parents = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $ware, array(), $this->db);
                            unset($parents[count($parents) - 1]);
                            $parents [] = $ware;

                            // fc_asset_wo
                            if (count($parents) > 0) {
                                $this->db->update('t_wares', array('fc_asset_wo' => new Zend_Db_Expr('fc_asset_wo + 1')), 'f_code IN (' . implode(',', $parents) . ')');
                            }
                        } 
                    }
                    //Mainsim_Model_Utilities::saveReverseAjax("t_wares_parent",$f_ware_code, $parent_1, "upd", "mdl_asset_tg",$this->db);
                }            
            }
        }
        elseif($this->table == 't_wares' && $this->params['f_type_id'] == 1 && $this->f_code != 0){ 
            $select = new Zend_Db_Select($this->db);
            
            //recupero i figli del asset
            $child_codes = array();
            Mainsim_Model_Utilities::getChildCode("t_wares_parent",$this->f_code,$child_codes,$this->db);
            $child_codes = array_values($child_codes);
            array_push($child_codes, $this->f_code);
                        
            //recupero il valore di fc_asset_wo
            $res = $select->from('t_ware_wo as ww',[])
                ->join(array("cd" => "t_creation_date"), "cd.f_id =  ww.f_wo_id", [])
                ->join(array("wo" => "t_workorders"), "wo.f_code = cd.f_id", 'count(*) as fc_asset_wo')
                ->join(array("wp" => "t_wf_phases"), "wp.f_number = cd.f_phase_id AND wp.f_wf_id = cd.f_wf_id", [])
                ->where('ww.f_ware_id IN ('. implode(",", $child_codes).")")
                ->where('wo.f_type_id NOT IN (3,9,11,14,16,18,19,20)')
                ->where('wp.f_group_id NOT IN (6,7,8)')
                ->query()->fetch();

            $fc_asset_wo = $res['fc_asset_wo'];
            $old_fc_asset_wo = empty($this->olds['old_table']['fc_asset_wo'])?0:$this->olds['old_table']['fc_asset_wo'];
            $olds_parents = [];
            if(!empty($this->olds['old_creation']['fc_hierarchy_codes']))
            {
                $olds_parents = $this->olds['old_creation']['fc_hierarchy_codes'];
                $olds_parents = explode(",", $olds_parents);
            }
            $parents = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $this->f_code, array(), $this->db);
            
            if(in_array(0,$olds_parents)){
                unset($olds_parents[count($olds_parents) - 1]); //elimino lo 0
            }
            if(in_array(0,$parents)){
                unset($parents[count($parents) - 1]); //elimino lo 0
            }
            
            $flagHc = ((count(array_diff($olds_parents,$parents)) + count(array_diff($parents,$olds_parents)))>0)?true:false;
            //se è cambiata la parentela
            if($flagHc){
                if(!empty($olds_parents) && $old_fc_asset_wo > 0)
                    $this->db->update('t_wares', array('fc_asset_wo' => new Zend_Db_Expr("fc_asset_wo - $old_fc_asset_wo")), 'f_code IN (' . implode(',', $olds_parents) . ')');
                if(!empty($parents) && $fc_asset_wo > 0)
                    $this->db->update('t_wares', array('fc_asset_wo' => new Zend_Db_Expr("fc_asset_wo + $fc_asset_wo")), 'f_code IN (' . implode(',', $parents) . ')');
            }
            else if($fc_asset_wo != $old_fc_asset_wo){
                $fc_aw_diff = $fc_asset_wo - $old_fc_asset_wo ;
                if(!empty($parents)) $this->db->update('t_wares', array('fc_asset_wo' => new Zend_Db_Expr("fc_asset_wo + $fc_aw_diff")), 'f_code IN (' . implode(',', $parents) . ')');
            }
            $this->db->update('t_wares', array('fc_asset_wo' => $fc_asset_wo), 'f_code = ' . $this->f_code);
        }
    }
    
    private function updateNumWoOpenPerAssetBy($parents,$operator)
    {
        $array = array_count_values($parents);
        $sel = $this->db->select();
        foreach ($array as $f_code_asset => $value) {
            $sel->reset();
            $res = $sel->from('t_wares',array('fc_asset_opened_wo'))
                    ->where('f_code=?', $f_code_asset)->query()->fetch();
            $res_count = $res['fc_asset_opened_wo'] + ($value*$operator);
            $sel->reset();
            $this->db->update('t_wares',array('fc_asset_opened_wo'=>$res_count),"f_code = {$f_code_asset}");
        }
    }

    public function getParents($sel, $fcode, $leveledFCode = []) 
    {
            $collect = $leveledFCode;
            array_push($collect, $fcode);
            $sel->reset();
            $res = $sel->from('t_wares_parent')->where('f_code=?', $fcode)->query()->fetch();
            if($res['f_parent_code'] != 0) {		
                    return $this->getParents($sel, $res['f_parent_code'], $collect);
            } else {
                    return $collect;
            }
    }

    private function getWoWaresCrossDifferences($olds,$now,&$removed_wo_wares,&$added_wo_wares)
    {
        $before = array(); $f_wares_types = array(1);
        for($i=0; $i < count($olds); ++$i) array_push($before, $olds[$i]['f_ware_id']);
        $select = new Zend_Db_Select($this->db);
        $res=[];
        if(count($before)!=0){
            $res = $select->from(array('t1'=>"t_wares"),array('t1.f_code'))
                    ->where("t1.f_code IN (?)",$before)
                    ->where("t1.f_type_id IN (?)",$f_wares_types)->query()->fetchAll();
        }
        $before = array();
        foreach ($res as $key => $value) {
            array_push($before, $value['f_code']);
        }
//        foreach($before as $f_ware_code){
//            if(count($now) == 0 || !in_array($f_ware_code,$now)) array_push($removed_wo_wares,$f_ware_code);
//        }
        $removed_wo_wares = array_diff($before, $now);
        $added_wo_wares = array_diff($now, $before);
                
//        foreach($now as $f_ware_code){
//             if(count($before)== 0 || !in_array($f_ware_code,$before)) array_push($added_wo_wares,$f_ware_code);
//        }
    }
    
    private function getNumWoOpenPerAsset($f_code_asset,$f_wo_types)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from(array('t1'=>"t_ware_wo"),array('num'=>'count(*)'))
            ->join(array('t2'=>"t_creation_date"),"t2.f_id = t1.f_wo_id",[])
            ->join(array('t3'=>"t_workorders"),"t2.f_id = t3.f_code",[])
            ->join(array("t4"=>"t_wf_phases"),"t2.f_phase_id = t4.f_number and t2.f_wf_id = t4.f_wf_id",[])            
            ->where("t4.f_group_id NOT IN (6,7,8) ")->where("t3.f_type_id IN (?)",$f_wo_types)
            ->where("f_ware_id = ?",$f_code_asset)->query()->fetch();
        return $res['num'];
    }
    
    private function t_wares_1_priority()
    {
        $select = new Zend_Db_Select($this->db);
        $prio = $select->from("t_workorders","f_priority")->where("f_code = ?",$this->f_code)->query()->fetch();
        if(!isset($this->params['f_priority']) || $prio['f_priority'] == 0){
            $select_pr = new Zend_Db_Select($this->db);        
            $priority = 0;
            foreach($this->params['t_wares_1']['f_code'] as $f_code_wares_1) {        
                //recupero f_title
                $select_pr->reset();
                $res_prio = $select_pr->from("t_wares",array('f_priority'))->where("f_code = ?",$f_code_wares_1)->query()->fetch();
                if(empty($res_prio)) continue;	
                if($res_prio['f_priority'] > $priority) $priority = $res_prio['f_priority'];
            }
            unset($select_pr);$this->db->update('t_workorders',array('f_priority'=>$priority),"f_code = {$this->f_code}");        
        }
    }
    
    /**
     * Create hierarchy of any object
     */    
    public function fc_hierarchy($f_code = 0, $hierarchy = null, $f_title = null)
    {   
        if(in_array('fc_hierarchy', $this->skipScript)) return;
        $select = new Zend_Db_Select($this->db);

        if($f_code == 0) {
            $f_code = $this->f_code;
        }
        // get parents
        $parentsCodes = array();
        $parentsCodes = Mainsim_Model_Utilities::getParentCode($this->table."_parent", $f_code, [], $this->db);
        // first time 
        if(!$hierarchy){             
            // there is at least one element beyond the root element
            $hierarchy = "";
            if(count($parentsCodes) > 1) {
                // unset root element
               // unset($parentsCodes[count($parentsCodes) - 1]);
                foreach($parentsCodes as $k => $v) {
                    if($v == 0) {
                        unset($parentsCodes[$k]);
                    }
                }
                
                //$parentsCodes = array_reverse($parentsCodes);
                // get parents titles
                $select->from('t_creation_date', array('f_id','f_title'))
                        ->where('f_id IN (' . implode(',', $parentsCodes) . ')');
                $res = $select->query()->fetchAll();
                
                //bugfix ordine gerarchia
                $res = array_column($res,'f_title','f_id');
                for($i = 0; $i < count($parentsCodes); $i++){
                    $hierarchyArray[] =  $res[$parentsCodes[$i]];
                }

                $hierarchy = implode(", ", $hierarchyArray);
                // check if hierarchy is changed, otherwise exit
                if(isset($this->olds['old_creation']['fc_hierarchy']) && $hierarchy == $this->olds['old_creation']['fc_hierarchy']) return;   
            }
        }
        
        $this->db->update('t_creation_date', array('fc_hierarchy' => $hierarchy), 'f_id = ' . $f_code);

        // get children
        $select->reset();
        $select->from('t_wares_parent', array('f_code'))
               ->join('t_creation_date', 't_wares_parent.f_parent_code = t_creation_date.f_id', array('f_title'))
                ->where('t_wares_parent.f_parent_code = ' . $f_code);
        $res = $select->query()->fetchAll();
        // for each child invoke function for his children
        for($i = 0; $i < count($res); $i++){
            $this->fc_hierarchy($res[$i]['f_code'], $hierarchy != '' ? $res[$i]['f_title'] . ", " . $hierarchy : $res[$i]['f_title']);
        }
    }
    
    /**
     *  Check if f_title changed. if true
     * recalculate children's hierarchy
     */
    public function f_title()
    {
        if(in_array('f_title', $this->skipScript))return;
		$select = new Zend_Db_Select($this->db); 
        if($this->params['f_code'] == 0) { //create unique code for import/update from module importer
            $this->createImportCode();
        }
        $oldTitle = isset($this->olds['old_creation']['f_title'])?$this->olds['old_creation']['f_title']:'';
        $time = Zend_Registry::isRegistered('time')?Zend_Registry::get('time'):time();
        $resTitle = $select->from(array("t2"=>"t_creation_date"),array('f_title','fc_hierarchy'))->where("f_id = ?",$this->f_code)
            ->query()->fetch();
        $select->reset();
        $resChilds = $select->from(array('t1'=>$this->table."_parent"),array("num"=>"count(f_code)"))                
                ->where("f_parent_code = ?",$this->f_code)
            ->query()->fetch();                

        if($resChilds['num'] > 0 && $oldTitle != $resTitle['f_title']){
            //if there are children and title is changed, propagate it to children
            $this->fc_hierarchy($this->f_code,$resTitle['fc_hierarchy']);
        }                
    }
    
    /**
     * Create unique code to put in fc_imp_code_*_excel
     */
    private function createImportcode() 
    {
        if($this->table == 't_systems') return;
        $params = $this->params;
        $select = new Zend_Db_Select($this->db);        
        $unique = isset($this->params['unique'])?array_unique($this->params['unique']):[];
        $field = "fc_imp_code_sel_excel";
        if($this->table == 't_wares'){
            $field = "fc_imp_code_wares_excel";
        }elseif($this->table == 't_workorders'){
            $field = "fc_imp_code_wo_excel";
        }
        $uniqueCode = !empty($this->params[$field])?$this->params[$field]:$this->f_code;
        if(!empty($unique)) {
            $creationPrefixField = array("f_id","f_code","f_timestamp");
            $select->from(array("t1"=>"t_creation_date"),["num"=>"count(*)"])
                ->join(array('t2'=>$this->table),"t1.f_id = t2.f_code",[])
                ->join(array('t3'=>"t_custom_fields"),"t1.f_id = t3.f_code",[])
                ->joinLeft(array('t4'=>"t_users"),"t1.f_id = t4.f_code",[]);
            $tot = count($unique);$values = [];            
            for($i = 0;$i < $tot;++$i){
                $uniqueList = explode(',',$unique[$i]);
                array_walk($uniqueList, function($el) use($params,$creationPrefixField,&$values,&$select){
                    $prefix = in_array($el,$creationPrefixField)?"t1.":"";
                    $select->where("{$prefix}{$el} = ?",$params[$el]);
                    $values[] = $params[$el];
                });
            }                   
            $res = $select->query()->fetch();            
            if($res['num'] == 1) {//passed unique array is truly unique, use it
                $uniqueCode = implode('|',$values);
            } 
        }        
        $this->db->update($this->table,array($field=>$uniqueCode),"f_code = {$this->f_code}");
    }
            
    
    public function fc_usr_password() 
    {
        $pwd = '';        
        if($this->params['fc_usr_password'] != '') {
            $pv = md5($this->params['fc_usr_password']);
            $xc0=substr($pv,0,2);
            $xc1=substr($pv,2,6);
            $xc2=substr($pv,8,16);
            $xc3=substr($pv,24,8);
            $pv = $xc1.$xc3.$xc0.$xc2;

            $xc0=substr($pv,0,5);
            $xc1=ord(substr($pv,5,1));
            $xc1++; if($xc1>57 && $xc1<90) $xc1=52;
            if($xc1>95) $xc1-=49;
            $xc2=substr($pv,6,26);          
            $pwd = $xc2.$xc1.$xc0;                                        
            $this->db->update("t_users",array('fc_usr_pwd'=>$pwd),"f_code = ".$this->f_code);
        }        
        
    }
    
    
    public function fc_usr_group_level()
    {
        $select_gl = new Zend_Db_Select($this->db);
        if($this->params['t_wares_parent_16']['f_code'][0] == 0 || empty($this->params['t_wares_parent_16']['f_code'])) {
            //get child of this group and sum group level
            $res_child = $select_gl->from(array("t1"=>"t_users"), array("f_code"))
                ->join(array("t2"=>"t_wares_parent"),"t1.f_code = t2.f_code",[])
                ->where("t2.f_active = 1")->where("t2.f_parent_code = ?", $this->f_code)
                ->where("fc_usr_group_level = -1")->query()->fetchAll();
            $tot_a = count($res_child);
            for($a = 0;$a < $tot_a;++$a) {
                $group_level = $params['fc_usr_group_level'];
                $line_child = $res_child[$a];		
                //check if this user have other father and add group level
                $select_gl->reset();
                $res_father = $select_gl->from(array("t1"=>"t_users"),array('fc_usr_group_level'))
                    ->join(array("t2"=>"t_wares_parent"),"t1.f_code = t2.f_code",[])
                    ->where("t2.f_active = 1")
                    ->where("t2.f_code = ?",$line_child['f_code'])
                    ->where("t2.f_parent_code != ?",$this->f_code)->query()->fetchAll();
                foreach($res_father as $father_gl) {
                    $group = (int)$father_gl['fc_usr_group_level'];
                    $group_level+=$group;
                }
                $this->db->update("t_users",array("fc_usr_group_level"=>$group_level),"f_code = {$line_child['f_code']}");
            }
        }
        else {
            $group_level = $this->params['fc_usr_group_level'];
            if(is_array($this->params['t_wares_parent_16']['f_code']) && ($this->params['fc_usr_group_level'] == -1 || $this->params['fc_usr_group_level'] == '' || $params['fc_usr_group_level'] == 0)) {
                $group_level = 0;                
                foreach($this->params['t_wares_parent_16']['f_code'] as $f_code_parent_group) {
                    $select_gl->reset();
                    $res_child = $select_gl->from(array("t1"=>"t_users"),array('fc_usr_group_level'))                                    
                    ->where("t1.f_code = ?",$f_code_parent_group)
                    ->query()->fetchAll();
                    if(!empty($res_child)) {
                        $group = (int)$res_child[0]['fc_usr_group_level'];
                        $group_level+=$group;
                    }
                }
            }
            if(!$group_level)$group_level = -1;            
            $this->db->update("t_users",array("fc_usr_group_level"=>$group_level),"f_code = {$this->f_code}");
        }        
    }
    
//    public function fc_level_level()
//    {
//        
//        if(empty($this->params['fc_level_level'])) {
//            $q = new Zend_Db_Select($this->db);
//            
//            $res = $q->from(array("cd" => "t_creation_date"), [])->join(array("cf" => "t_custom_fields"), "cd.f_id = cf.f_code", array("tot" => "count(*)"))
//                ->where("cd.f_type = 'SYSTEMS'")->where("cd.f_category = 'LEVEL' and fc_level_level = -1")->query()->fetchAll();
//            if($res[0]['tot'] == 0){
//                $level = -1;
//            }
//            else{
//                $q->reset();
//                $res = $q->from(array("cd" => "t_creation_date"), [])->join(array("cf" => "t_custom_fields"), "cd.f_id = cf.f_code", array("level" => "MAX(fc_level_level)"))
//                        ->where("cd.f_type = 'SYSTEMS'")->where("cd.f_category = 'LEVEL'")->query()->fetchAll();
//                if($res[0]["level"] == 0) $level = 2;else if($res[0]["level"] >= 2) $level = $res[0]["level"]*2;
//            }
//            $this->db->update("t_custom_fields", array('fc_level_level' => $level), "f_code = ".$this->f_code);                
//        }
//    }
    
    public function fc_wo_ending_date(){
        
        if(in_array('fc_wo_ending_date', $this->skipScript))return;
        
        $q = new Zend_Db_Select($this->db);
        $res_date = $q->from(array("cd" => "t_workorders"), array('fc_wo_starting_date','fc_wo_ending_date','f_end_date','f_start_date'))
                ->where("cd.f_code = " . $this->f_code)->query()->fetch();
        
        if($res_date['fc_wo_ending_date'] > 0 && $res_date['fc_wo_starting_date']>0)
        if($res_date['fc_wo_starting_date'] >= $res_date['fc_wo_ending_date']) throw new Exception ("Ending date before Starting date!");

        if($res_date['f_end_date'] > 0 && $res_date['f_start_date']>0)
        if($res_date['f_start_date'] >= $res_date['f_end_date']) throw new Exception ("Planned ending date before Planned Starting date!");
    }
    
    public function fc_lvl_level()
    {
        if(empty($this->params['fc_lvl_level'])) {
            $q = new Zend_Db_Select($this->db);
            $res = $q->from(array("cd" => "t_creation_date"), [])->join(array("s" => "t_systems"), "cd.f_id = s.f_code", array("fc_lvl_level"))
                    ->where("s.f_type_id = 4")->where("cd.f_phase_id = 1")
                    ->order("s.fc_lvl_level DESC")->query()->fetchAll();
            if(count($res) == 0) $lvl = -1;
            else $lvl = intval($res[0]["fc_lvl_level"]) * 2;
            $this->db->update("t_systems", array('fc_lvl_level' => $lvl), "f_code = ".$this->f_code);
        }
    }
        
    public function __fc_material_moved_quantity()
    {           
        //do this operation only for generated passed from module and not generator
        if($this->table == "t_workorders" && (!in_array($this->params['f_type_id'],array(3,9) ) ) && !isset($this->params['is_generator']) && false )  {            
            $sel = new Zend_Db_Select($this->db);
            //check if moved quantity are set for class material and not instance
            $tot_a = count($this->params['t_wares_3']['f_code']);
            for($a = 0;$a < $tot_a;++$a) {
                $sel->reset();
                $res_check = $sel->from("t_wares",array("num"=>"count(*)"))
                        ->where("f_code = ?",$this->params['t_wares_3']['f_code'][$a])
                        ->where("fc_material_type = 'class'")->query()->fetch();
                if($res_check['num'] > 0)
                    throw new Exception ("You cannot set a movimentation for a material of type 'Class' ");
            }
            $sel->reset();
            $res_old_pair = $sel->from(array("t1"=> "t_pair_cross"),array("f_code_main","f_code_cross","fc_material_moved_quantity"))
                            ->join(array("t2"=>"t_wares"),"t1.f_code_cross = t2.f_code",[])
                            ->where("f_code_main = ?",$this->params['t_wares_3']['f_code_main'])                                                                                                                        
                            ->where("t2.f_type_id = ?",$this->params['t_wares_3']['f_type'])
                    ->query()->fetchAll();                
            $old_warehouse = [];    
            foreach($res_old_pair as $old_pair) {                                
                    if(!array_key_exists($old_pair['f_code_cross'],$old_warehouse)) {
                            $old_warehouse[$old_pair['f_code_cross']] = 0;
                    }                                
                    $old_warehouse[$old_pair['f_code_cross']] += (int)$old_pair['fc_material_moved_quantity'];
            }                      
            //rimetto in ordine i vecchi magazzini                                
            foreach($old_warehouse as $key_warehouse => $value_warehouse) {
                    $sel->reset();                    
                    $res_custom = $sel->from("t_wares",array("fc_material_quantity"))->where("f_code = ?",$key_warehouse)->query()->fetch();	                    
                    $res_custom['fc_material_quantity'] += $value_warehouse;	
                    //passo a old il vecchio custom e aggiorno                                    
                    $this->db->update("t_wares",$res_custom,"f_code = {$key_warehouse}");                                    
            }
            if(is_array($this->params['t_wares_3']['f_pair'])){
                //adesso inserisco le nuove giacenze       
                $wo = new Mainsim_Model_Workorders($this->db);
                $time_material = time();
                foreach($this->params['t_wares_3']['f_pair'] as $f_pair) {
                    $fc_material_moved_quantity = 0;
                    if(is_null($f_pair)) continue;		
                    $fc_material_moved_quantity = (int)$f_pair['fc_material_moved_quantity'];		                                        
                    //recupero anche il campo wares per aggiornareil ts
                    $sel->reset();
                    $res_wares = $sel->from("t_wares",array('fc_material_quantity','fc_material_min_stock','fc_material_spare_number','f_priority','fc_material_stock_type','fc_material_standard_price'))
                            ->join("t_creation_date","t_wares.f_code = t_creation_date.f_id",array("f_title","f_description"))
                            ->where("f_code = ?",$f_pair['f_code_cross'])->query()->fetch();
                    $res_wares['f_timestamp'] = $time_material;
                    $min_stock = !empty($res_wares['fc_material_min_stock'])?(int)$res_wares['fc_material_min_stock']:0;
                    $purchase_num = !empty($res_wares['fc_material_spare_number'])?(int)$res_wares['fc_material_spare_number']:0;
                    $res_wares['fc_material_quantity'] -= $fc_material_moved_quantity;                          
                    if($res_wares['fc_material_quantity'] <= $min_stock) {                        
                        //check if some one already open a wo
                        $sel->reset();
                        $res_check = $sel->from("t_workorders",[])
                            ->join("t_ware_wo","t_workorders.f_code = t_ware_wo.f_wo_id",[])
                            ->join("t_creation_date","t_creation_date.f_id = t_workorders.f_code",array("f_creation_date"))
                            ->join("t_wf_phases","t_creation_date.f_wf_id = t_wf_phases.f_wf_id  and t_creation_date.f_phase_id = t_wf_phases.f_number",[])
                            ->join("t_wf_groups","t_wf_phases.f_group_id = t_wf_groups.f_id",[])
                            ->where("t_wf_groups.f_visibility != 0")
                            ->where("f_type_id = 2")->where("f_ware_id = ?",$f_pair['f_code_cross'])
                            ->query()->fetchAll();                        
                        // if finish material and this is a finite warehouse, declare this wo without spare                        
                        if($res_wares['fc_material_quantity'] < 0 && $res_wares['fc_material_stock_type'] == 1) {                             
                            if(empty($res_check)){
                                $this->db->update("t_workorders",array("fc_wo_empty_spare"=>1),"f_code = {$this->f_code}");
                            }
                            else { // check if po hase been opened after my material request
                                $found = false;
                                if($f_pair['fc_material_moving_date'] == 0){
                                    $found = true;
                                }
                                else {                                    
                                    foreach($res_check as $line_check) {                                        
                                        if($line_check['f_creation_date'] < $f_pair['fc_material_moving_date']){
                                            $found = true;break;
                                        }
                                    }                                    
                                }
                                if($found) {                                    
                                    //check if in history there is already a fc_wo_empty_spare, if true don't assign empty spare
                                    $sel->reset();
                                    $res_wo_spare_check = $sel->from("t_workorders_history",array("num"=>"count(*)"))
                                            ->where("f_code = ?",$this->f_code)->where("fc_wo_empty_spare = 1")
                                            ->query()->fetch();                                    
                                    if($res_wo_spare_check['num'] == 0) {
                                        $this->db->update("t_workorders",array("fc_wo_empty_spare"=>1),"f_code = {$this->f_code}");
                                    }
                                    else {
                                        $this->db->update("t_workorders",array("fc_wo_empty_spare"=>0),"f_code = {$this->f_code}");
                                    }
                                }
                                else {
                                    $this->db->update("t_workorders",array("fc_wo_empty_spare"=>0),"f_code = {$this->f_code}");
                                }
                            }
                        }                        
                        
                        if(empty($res_check)){
                            //get f_code of vendor associate to wo.
                            $sel->reset();
                            $res_vendor = $sel->from("t_wares_relations",array("f_code_ware_slave"))
                                    ->where("f_code_ware_master = ?",$f_pair['f_code_cross'])
                                    ->where("f_type_id_slave = 6")->query()->fetchAll();                            
                            $codes_vendor = [];
                            foreach($res_vendor as $vend) {
                                $codes_vendor[] = $vend['f_code_ware_slave'];
                            }
                            //open a purchase order                        
                            $param_po = array(
                                'f_type_id'=>2,
                                'f_code'=>0,
                                'f_title'=>'Purchase order of '.$res_wares['f_title'],
                                'f_priority'=>!empty($res_wares['f_priority'])?$res_wares['f_priority']:0,
                                'f_description'=>$res_wares['f_description'],                                
                                'f_module_name'=>'mdl_po_tg',
                                't_wares_3'=>array(
                                    'f_code'=>array($f_pair['f_code_cross']),
                                    "f_pair"=>array(
                                        array(
                                            'f_code_cross'=>$f_pair['f_code_cross'],
                                            'f_code_main'=>0,                                        
                                            'fc_po_spare_ordered'=>$purchase_num,
                                            'f_group_code'=>"0",
                                            "f_title"=>"Purchase Order",
                                            "fc_po_material_price"=>$res_wares['fc_material_standard_price']
                                        )
                                    ),
                                    "f_code_main"=>0,
                                    "f_type"=> 3,
                                    "Ttable"=> "t_wares",
                                    "pairCross"=> 0,
                                    "f_module_name"=> "mdl_po_material_tg"                            
                                ),
                                't_wares_6'=>array(
                                    'f_code'=>$codes_vendor,
                                    'f_pair'=>[],
                                    "f_code_main"=>0,
                                    "f_type"=> 6,
                                    "Ttable"=> "t_wares",
                                    "pairCross"=> 0,
                                    "f_module_name"=> "mdl_po_vendor_tg"                                    
                                )
                            );
                            $resp = $wo->newWorkOrder($param_po, false);                          
                            if(isset($resp['message'])) throw new Exception ($resp['message']);
                        }
                    }
                    unset($res_wares['f_title']);
                    unset($res_wares['f_description']);
                    unset($res_wares['f_priority']);
                    Mainsim_Model_Utilities::newRecord($this->db,$f_pair['f_code_cross'],"t_creation_date",array("f_timestamp"=>$time_material));
                    Mainsim_Model_Utilities::newRecord($this->db,$f_pair['f_code_cross'],"t_wares",$res_wares);				
                    //passo a old il vecchio custom e aggiorno                     
                    $custom['f_timestamp'] = $time_material;				
                    Mainsim_Model_Utilities::newRecord($this->db,$f_pair['f_code_cross'],"t_custom_fields",$custom);                                                                           
                }
            }
        }
//        elseif(isset($this->params['is_generator']) ) {
//            //use generator
//            $res = $sel->from(array("t1"=>"t_pair_cross"),array('fc_material_moved_quantity','f_code_cross'))
//                    ->join(array("t2"=>"t_wares"),"t1.f_code_cross = t2.f_code",[])
//                    ->where("f_code_main = ?",$this->f_code)                    
//                    ->where("t2.f_type_id = 3")->query()->fetchAll();            
//            $time_material = time();
//            $wo = new Mainsim_Model_Workorders($this->db);
//            foreach($res as $f_pair) {
//                $fc_material_moved_quantity = 0;                
//                $fc_material_moved_quantity = (int)$f_pair['fc_material_moved_quantity'];		                                
//                //recupero anche il campo wares per aggiornareil ts
//                $sel->reset();
//                $res_wares = $sel->from("t_wares",array('fc_material_quantity','fc_material_min_stock','fc_material_spare_number','f_priority',"fc_material_stock_type",'fc_material_standard_price'))
//                        ->join("t_creation_date","t_wares.f_code = t_creation_date.f_id",array("f_title","f_description"))
//                        ->where("f_code = ?",$f_pair['f_code_cross'])->query()->fetch();
//                $res_wares['f_timestamp'] = $time_material;
//                $min_stock = !empty($res_wares['fc_material_min_stock'])?(int)$res_wares['fc_material_min_stock']:0;
//                $purchase_num = !empty($res_wares['fc_material_spare_number'])?(int)$res_wares['fc_material_spare_number']:0;
//                $res_wares['fc_material_quantity'] -= $fc_material_moved_quantity;  
//                
//                // if finish material and this is a finite warehouse, declare this wo without spare
//                if($res_wares['fc_material_quantity'] < 0 && $res_wares['fc_material_stock_type'] == 1) { 
//                    $this->db->update("t_workorders",array("fc_wo_empty_spare"=>1),"f_code = {$this->f_code}");
//                }
//                else {
//                    $this->db->update("t_workorders",array("fc_wo_empty_spare"=>0),"f_code = {$this->f_code}");
//                }
//                
//                if($res_wares['fc_material_quantity'] <= $min_stock) {
//                    //check if some one already open a wo
//                    $sel->reset();                    
//                    $res_check = $sel->from("t_workorders",[])
//                        ->join("t_ware_wo","t_workorders.f_code = t_ware_wo.f_wo_id",[])
//                        ->join("t_creation_date","t_creation_date.f_id = t_workorders.f_code",array("f_creation_date"))
//                        ->join("t_wf_phases","t_creation_date.f_wf_id = t_wf_phases.f_wf_id  and t_creation_date.f_phase_id = t_wf_phases.f_number",[])
//                        ->join("t_wf_groups","t_wf_phases.f_group_id = t_wf_groups.f_id",[])
//                        ->where("t_wf_groups.f_visibility != 0")
//                        ->where("f_type_id = 2")->where("f_ware_id = ?",$f_pair['f_code_cross'])
//                        ->query()->fetchAll();                        
//                    // if finish material and this is a finite warehouse, declare this wo without spare                        
//                    if($res_wares['fc_material_quantity'] < 0 && $res_wares['fc_material_stock_type'] == 1) {                             
//                        if(empty($res_check)){
//                            $this->db->update("t_workorders",array("fc_wo_empty_spare"=>1),"f_code = {$this->f_code}");
//                        }
//                        else { // check if po hase been opened after my material request
//                            $found = false;
//                            if($f_pair['fc_material_moving_date'] == 0){
//                                $found = true;
//                            }
//                            else {                                    
//                                foreach($res_check as $line_check) {                                        
//                                    if($line_check['f_creation_date'] < $f_pair['fc_material_moving_date']){
//                                        $found = true;break;
//                                    }
//                                }                                    
//                            }
//                            if($found) {
//                                $this->db->update("t_workorders",array("fc_wo_empty_spare"=>1),"f_code = {$this->f_code}");                                
//                            }                            
//                        }
//                    }
//                    if(empty($res_check)){
//                        
//                        //get f_code of vendor associate to wo.
//                        $sel->reset();
//                        $res_vendor = $sel->from("t_wares_relations",array("f_code_ware_slave"))
//                                ->where("f_code_ware_master = ?",$f_pair['f_code_cross'])
//                                ->where("f_type_id_slave = 6")->query()->fetchAll();
//                        $codes_vendor = [];
//                        foreach($res_vendor as $vend) {
//                            $codes_vendor[] = $vend['f_code_ware_slave'];
//                        }
//                        
//                        //open a purchase order                        
//                        $param_po = array(
//                            'f_type_id'=>2,
//                            'f_code'=>0,
//                            'f_title'=>'Purchase order of '.$res_wares['f_title'],
//                            'f_priority'=>!empty($res_wares['f_priority'])?$res_wares['f_priority']:0,
//                            'f_description'=>$res_wares['f_description'],                                
//                            'f_module_name'=>'mdl_po_tg',
//                            't_wares_3'=>array(
//                                'f_code'=>array($f_pair['f_code_cross']),
//                                "f_pair"=>array(
//                                    array(
//                                        'f_code_cross'=>$f_pair['f_code_cross'],
//                                        'f_code_main'=>0,                                        
//                                        'fc_po_spare_ordered'=>$purchase_num,
//                                        'f_group_code'=>"0",
//                                        "f_title"=>"Purchase Order",
//                                        "fc_po_material_price"=>$res_wares['fc_material_standard_price']
//                                        )
//                                ),
//                                "f_code_main"=>0,
//                                "f_type"=> 3,
//                                "Ttable"=> "t_wares",
//                                "pairCross"=> 0,
//                                "f_module_name"=> "mdl_po_material_tg"                            
//                            ),
//                            't_wares_6'=>array(
//                                'f_code'=>$codes_vendor,
//                                'f_pair'=>[],
//                                "f_code_main"=>0,
//                                "f_type"=> 6,
//                                "Ttable"=> "t_wares",
//                                "pairCross"=> 0,
//                                "f_module_name"=> "mdl_po_vendor_tg"                                    
//                            )
//                        );                        
//                        $resp = $wo->newWorkOrder($param_po, false);                          
//                        if(isset($resp['message'])) throw new Exception ($resp['message']);
//                    }
//                }
//                unset($res_wares['f_title']);
//                unset($res_wares['f_description']);unset($res_wares['f_priority']);            
//                Mainsim_Model_Utilities::newRecord($this->db,$f_pair['f_code_cross'],"t_wares",$res_wares);	
//                Mainsim_Model_Utilities::newRecord($this->db,$f_pair['f_code_cross'],"t_creation_date",array("f_timestamp"=>$time_material));
//                //passo a old il vecchio custom e aggiorno                     
//                $custom['f_timestamp'] = $time_material;				
//                Mainsim_Model_Utilities::newRecord($this->db,$f_pair['f_code_cross'],"t_custom_fields",$custom);                                                                           
//            }
//        }
    }
    
    public function fc_selector()
    {
        $select_sel = new Zend_Db_Select($this->db);
        if($this->table == 't_wares') {
            $cross_table = 't_selector_ware';    
            $field = 'f_ware_id';
        } elseif($this->table == 't_workorders') {
            $cross_table = 't_selector_wo';
            $field = 'f_wo_id';
        } elseif($this->table == 't_systems') {
            $cross_table = 't_selector_system';
            $field = 'f_system_id';
        }
        else {
            return;
        }
        $res = $select_sel->from(array("t1"=>"t_selectors"),[])
            ->join(array("t4"=>"t_creation_date"),"t1.f_code = t4.f_id",array("f_title"))
            ->join(array("t2"=>$cross_table),"t1.f_code = t2.f_selector_id",[])                
            ->where("t2.$field = ?",$this->f_code)->where("t2.f_nesting_level = 1")
            ->query()->fetchAll();
        $tot_a = count($res);        
        $hy = [];
        for($a = 0; $a < $tot_a;++$a) {
            $hy[] = $res[$a]['f_title'];
        }        
        $this->db->update("t_custom_fields",array('fc_selector'=>implode(', ',$hy)),'f_code = '.$this->f_code);
    }
        
    /**
     * update mtbf (Mean Time Between Failure) 
     */
    public function wo_fc_asset_mtbf($f_code_asset)
    {
        $select = new Zend_Db_Select($this->db);        
        if($this->params['f_type_id'] == 1 || $this->params['f_type_id'] == 10 || $this->params['f_type_id'] == 13) {                  
            //count number of failure with other request and update MTBF
            $select->reset();
            $resAsset = $select->from(array("t1"=>"t_pair_cross"),array("tot"=>"sum(t1.fc_asset_downtime_hours)"))
                ->join(array("t2"=>"t_workorders"),"t1.f_code_main = t2.f_code",[])					
                ->join(array("t3"=>"t_creation_date"),"t2.f_code = t3.f_id",[])					
                ->join(array("t4"=>"t_wf_phases"),"t3.f_phase_id = t4.f_number and t3.f_wf_id = t4.f_wf_id",[])                            
                ->where("t1.f_code_cross = ?",$f_code_asset)->where('t4.f_group_id = 6')
                ->where("t2.f_type_id IN (1,10,13)")->query()->fetch();  

            $select->reset();
            $resNumDt = $select->from(array("t1"=>"t_pair_cross"),array("num"=>"count(*)"))
                ->join(array("t2"=>"t_workorders"),"t1.f_code_main = t2.f_code",[])
                ->join(array("t3"=>"t_creation_date"),"t2.f_code = t3.f_id",[])
                ->join(array("t4"=>"t_wf_phases"),"t3.f_phase_id = t4.f_number and t3.f_wf_id = t4.f_wf_id",[])   				
                ->where("t1.f_code_cross = ?",$f_code_asset)->where('t4.f_group_id = 6')->where("t2.f_type_id IN (1,10,13)")
                ->query()->fetch();    						                
            $select->reset();
            $res_aot = $select->from("t_wares","fc_asset_operational_time")->where("f_code = ?",$f_code_asset)->query()->fetch();                                        
            if($resNumDt['num'] > 0 && $res_aot['fc_asset_operational_time'] > 0) {                    
                $mtbf = number_format(($res_aot['fc_asset_operational_time'] -$resAsset['tot']) /$resNumDt['num'],4,',','');   
                $this->db->update("t_wares",array("fc_asset_mtbf"=>floatval($mtbf)),"f_code = {$f_code_asset}");
            }                
        }                
    }
    
    
    /**
     * update mttr (Mean Time To Repair) 
     */
    public function wo_fc_asset_mttr($f_code_asset)
    {
        $select = new Zend_Db_Select($this->db);
        if($this->table == 't_workorders') {
            if($this->params['f_type_id'] == 1 || $this->params['f_type_id'] == 10 || $this->params['f_type_id'] == 13) {                                                
                //count number of failure with other request and update MTTR
                $select->reset();
                $res = $select->from(array("t1"=>"t_pair_cross"),array("fc_asset_downtime_since","fc_asset_downtime_till"))
                        ->join(array("t2"=>"t_workorders"),"t1.f_code_main = t2.f_code",[])
                        ->join(array("t3"=>"t_creation_date"),"t2.f_code = t3.f_id",[])
                        ->join(array("t4"=>"t_wf_phases"),"t3.f_phase_id = t4.f_number and t3.f_wf_id = t4.f_wf_id",[])
                        ->where("t4.f_group_id = 6")->where("t1.f_code_cross = ?",$f_code_asset)
                        ->where("t2.f_type_id IN (1,10,13)")
                        ->query()->fetchAll();                        
                if(!empty($res)) {
                    $nof = count($res);
                    $tot_hof = 0;
                    foreach($res as $line) {
                        $since = (int)$line['fc_asset_downtime_since'];
                        $till = (int)$line['fc_asset_downtime_till'];
                        $tot_hof+= ($till - $since);
                    }                            
                    $mttr = number_format((($tot_hof/3600)/$nof),4);                            
                    $this->db->update("t_wares",array("fc_asset_mttr"=>floatval($mttr)),"f_code = {$f_code_asset}");
                }                        
                
            }
        }
    }
    
    /**
     * update mtbr (Mean Time Between Repair) 
     */
    public function wo_fc_asset_mtbr($f_code_asset)
    {
        $select = new Zend_Db_Select($this->db);
        if($this->table == 't_workorders') {
            if($this->params['f_type_id'] == 1 || $this->params['f_type_id'] == 10 || $this->params['f_type_id'] == 13) {                                                
                //count number of failure with other request and update MTBR
                $select->reset();
                $res = $select->from(array("t1"=>"t_wares"),array("fc_asset_mtbf","fc_asset_mttr"))                                
                        ->where("t1.f_code = ?",$f_code_asset)->query()->fetch();                        
                if(!empty($res)) {                            
                    $mtbr = $res['fc_asset_mtbf'] - $res['fc_asset_mttr'];
                    $this->db->update("t_wares",array("fc_asset_mtbr"=>floatval($mtbr)),"f_code = {$f_code_asset}");
                }                
            }
        }
    }
        
    public function __fc_material_warehouse()
    {
        if($this->params['f_type_id'] == 3 && $this->table = 't_wares') {
            $sel = new Zend_Db_Select($this->db); 
            $whs = [];        
            if(!empty($this->params['t_wares_21']['f_code'])) {
                foreach($this->params['t_wares_21']['f_code'] as $code_wh) {
                    $res = $sel->from("t_creation_date",array("f_title","fc_hierarchy"))->where("f_id = ?",$code_wh)->query()->fetch();
                    if(!empty($res)){
                        $hy = $res['fc_hierarchy'];
                        $material_wh = $res['f_title'];
                        $material_wh .= !empty($hy)?" ($hy)":'';
                        $whs[] = $material_wh;            
                    }
                }
            }
            $whs = implode(', ',$whs);
            $this->db->update("t_wares",array('fc_material_warehouse'=>$whs),"f_code = {$this->f_code}");
        }
    }
    
    /**
     * declare if an asset could be bookable or not
     */
    /*
    public function fc_asset_bookable()
    {        
        if($this->table != 't_wares' || $this->params['f_type_id'] != 1) return [];
        //check if there is a change of reservation
        if(!isset($this->olds['old_table']['fc_asset_bookable']) || $this->olds['old_table']['fc_asset_bookable'] != $this->params['fc_asset_bookable']) {
            $sel_rsv = new Zend_Db_Select($this->db);
            if(!empty($this->params['fc_asset_bookable'])) { //check if add new reservation
                $wo = new Mainsim_Model_Workorders($this->db);
                $params_rsv = array(
                    "f_title"=>$this->params['f_title'],
                    "f_description"=>$this->params['f_description'],
                    "f_type_id"=>8,
                    "f_priority"=>0,
                    "t_wares_1"=>array("f_code"=>array($this->f_code)),
                    'f_module_name'=>"mdl_rsv_tg",
                    'fc_rsv_minimum_hour'=>$this->params['fc_asset_bookable_minimum_hour'],
                    'fc_rsv_maximum_hour'=>$this->params['fc_asset_bookable_maximum_hour'],
                    'fc_rsv_interval'=>$this->params['fc_asset_bookable_interval']
                );
                $wo->newWorkOrder($params_rsv, false);                
            }
            else { //check if remove reservation
                $exist = $sel_rsv->from(array("t1"=>"t_workorders"),"f_code")
                    ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_wo_id",[])
                    ->where("f_ware_id = ?",$this->f_code)->where("t1.f_type_id = 8")
                    ->query()->fetch();
                if(empty($exist)) return;
                //check if there are children and if this have expire reservation
                $sel_rsv->reset();
                $exist_children = $sel_rsv->from(array("t1"=>"t_workorders_parent"),array("num"=>"count(*)"))
                    ->join(array("t2"=>"t_periodics"),"t1.f_code = t2.f_code",[])
                    ->join(array("t3"=>"t_creation_date"),"t1.f_code = t3.f_id",[])
                    ->join(array("t4"=>"t_wf_phases"),"t3.f_phase_id = t4.f_number and t3.f_wf_id = t4.f_wf_id",[])
                    ->join(array("t5"=>"t_wf_groups"),"t4.f_group_id = t5.f_id",[])
                    ->where("t1.f_parent_code = ?",$exist['f_code'])
                    ->where("((t2.f_start_date - t2.f_forewarning) > ".time()." or t5.f_visibility != 0)")
                    ->query()->fetch();
                if($exist_children['num'] > 0){ 
                    throw new Exception("Cannot declare this asset not bookable because some reservations are still open");
                }
                else { //declare every reservation (father and sons) closed
                    $change_codes = Mainsim_Model_Utilities::getChildCode("t_workorders_parent",$exist['f_code']);
                    $sel_rsv->reset();
                    $phase = $sel_rsv->from(array("t1"=>"t_workorders_types"),[])                            
                            ->join(array("t2"=>"t_wf_phases"),"t1.f_wf_id = t2.f_wf_id",array("f_number"))                            
                            ->where("t2.f_group_id = 7")->where("t1.f_id = 8")->query()->fetch();
                    $time = time();
                    $custom['f_timestamp'] = $time;
                    $wo['f_timestamp'] = $time;
                    $creation['f_timestamp'] = $time;
                    $creation['f_phase_id'] = $phase['f_number'];
                    foreach($change_codes as $line_code) {
                        Mainsim_Model_Utilities::newRecord($this->db, $line_code, "t_creation_date", $creation);
                        Mainsim_Model_Utilities::newRecord($this->db, $line_code, "t_workorders", $wo);
                        Mainsim_Model_Utilities::newRecord($this->db, $line_code, "t_custom_fields", $custom);
                    }
                }                
            }
        }
    }
    */
        
    /**
     * Create asset hierarchy from wizard
     */
    public function t_wares()         
    {
        $select_hy = new Zend_Db_Select($this->db);
        $asset_hierarchies = [];
        $f_code_wares_1 = $this->params['t_wares']; 
        //get f_title and hierarchy of assets
        $select_hy->reset();
        $res_title = $select_hy->from("t_creation_date",array('f_title','fc_hierarchy'))
            ->where("f_id = ?",$f_code_wares_1)->query()->fetch();
        if(empty($res_title)) return [];
        $asset_hierarchies[$res_title['f_title']] = $res_title['fc_hierarchy'];                
        
        $new_fc_asset_hy = [];
        foreach ($asset_hierarchies as $key_hy =>$value_hy) {                            
            $new_fc_asset_hy[] = !empty($value_hy)?"$key_hy ($value_hy)":"$key_hy";
        }
        $fc_hierarchy = implode(", ",$new_fc_asset_hy);            
        $this->db->update("t_workorders",array("fc_wo_asset"=>$fc_hierarchy),"f_code = {$this->f_code}");
    }
    
    /**
     * manage ui component add,remove and edit from 
     * module
     */
    public function fc_ui_label()
    {
        $select = new Zend_Db_Select($this->db);
        //get type of UI 
        $select->reset();
        $res = $select->from("t_selector_system","f_selector_id")->where("f_system_id = ?",$this->f_code)
                ->query()->fetch();
        $selector = array(27,28,30,32);
        if(!empty($res) && in_array($res['f_selector_id'],$selector) ) {
            //moduleComponent of this element.
            $exp_name = explode('_',$this->params['f_title']);
            $exp_name[0] = 'mdl';
            $pos = array_search("edit", $exp_name);
            $exp_name = array_slice($exp_name, 0, $pos +1 );
            $exp_name[] = 'c1';
            $mdl = implode('_',$exp_name);  
            $select->reset();            
            $res_mdl_ui = $select->from(array("t1"=>"t_creation_date"),[])
                    ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code",array("fc_ui_fields","f_code"))
                    ->where("t1.f_type = 'SYSTEMS'")->where("t1.f_title = ?",$mdl)
                    ->where("t2.fc_ui_type = 'moModuleComponent'")->query()->fetch();
            if(!empty($res_mdl_ui)) {
                $fields = explode(',',$res_mdl_ui['fc_ui_fields']);
                //if instance_name is changed remove old and instance new.                
                if($this->olds['old_creation']['f_title'] != $this->params['f_title']) { //remove old and insert new                    
                    $pos = array_search($this->olds['old_creation']['f_title'],$fields);                                        
                    if($pos !== false) {                        
                        unset($fields[$pos]);
                    }
                    $fields[] = $this->params['f_title'];
                }
                elseif(!in_array($this->params['f_title'],$fields)) { //just add new UI
                    $fields[] = $this->params['f_title'];
                }
                elseif(($this->olds['old_creation']['f_phase_id'] != $this->params['f_phase_id']) && $this->params['f_phase_id'] == 3) { //remove old
                    $pos = array_search($this->olds['old_creation']['f_title'],$fields);
                    if($pos !== false) {
                        unset($fields[$pos]);
                    }
                }
                $fields = implode(',',$fields);
                $this->db->update("t_systems",array("fc_ui_fields"=>$fields),"f_code = {$res_mdl_ui['f_code']}");
            }
        }        
    }
    
    /**
     * Check if value reference or meter is changed and oc generator
     * is every.
     * If true, recalculate next due dates
     */
    public function fc_cond_value_reference()
    {
        $select = new Zend_Db_Select($this->db);
        $resCond = $select->from(['t_workorders'],['f_code','fc_cond_value_reference','fc_cond_start_value','fc_cond_meter_code'])
            ->where('f_code = ?',$this->f_code)->where("fc_cond_list_function = 'EVERY'")->query()->fetchAll(); 
        if(empty($resCond[0]['fc_cond_meter_code'])){ return; }    
        $select->reset();
        $resCondH = $select->from(array('t2'=>'t_workorders_history'),['fc_cond_value_reference','fc_cond_meter_code'])
            ->where('f_code = ?',$this->f_code)->order("f_id desc")->where("fc_cond_list_function = 'EVERY'")->query()->fetch();
        if((!empty($resCondH[0]['fc_cond_value_reference']) && $resCond[0]['fc_cond_value_reference'] == $resCondH[0]['fc_cond_value_reference'])
            && (!empty($resCondH[0]['fc_cond_meter_code']) && $resCond[0]['fc_cond_meter_code'] == $resCondH[0]['fc_cond_meter_code'])   ){ return; }
        $this->setOcMeterValues($resCond[0]['fc_cond_meter_code']);
        $this->calculateNextDueDateOc($resCond[0]['fc_cond_meter_code'], $resCond);
    }
        
    /**
     * Insert new meter reading history value 
     * directly instead of using meter request
     */
    public function fc_meter_add_value($f_code_meter = 0)
    {        
        if(!$f_code_meter &&
          !($this->params['f_type_id'] && in_array($this->params['f_type_id'], array(7,11)) )
        )
        {
            return;
        }
		$f_code_meter = !$f_code_meter?$this->f_code:$f_code_meter;
        $this->updateOcUM($f_code_meter);
        $addValueSetted = isset($this->params['fc_meter_add_value']) 
            && (strlen($this->params['fc_meter_add_value']))?true:false;         
        if(!$addValueSetted && $this->params['f_type_id'] == 11) { return; }
        //if( !$addValueSetted && ($this->params['f_type_id'] != 11 && $f_code_meter != $this->f_code)){ return; } die('lol');
        
        if(!isset($this->params['fc_meter_add_value'])){
            $this->params['fc_meter_add_value'] = null;
        }
        
        if(!isset($this->params['fc_meter_value'])){
            $this->params['fc_meter_value'] = null;
        }
        
        $value = $addValueSetted?$this->params['fc_meter_add_value']:$this->params['fc_meter_value'];
        //get parent_code from f_code_periodic           
        $time = !empty($this->params['fc_meter_add_value_time']) ?$this->params['fc_meter_add_value_time']:time();        
        $history = [
            'f_code'=>$f_code_meter,
            'f_timestamp'=>$time,
            'f_user_id'=>$this->userinfo->f_id,
            'f_value'=>$value,
			'f_checked'=>0, 'f_in_work'=>0
        ];
        $this->db->insert("t_meters_history",$history);
        //change value with last insert        
        $this->db->update("t_workorders",['fc_meter_value'=>$value,'fc_meter_read_time'=>$time],"f_code = {$f_code_meter}");                
        //set values for oc crossed
        $this->setOcMeterValues($f_code_meter);         
        $this->calculateNextDueDateOc($f_code_meter);
    }
    
    private function updateOcUM($meterCode)
    {
        $select = new Zend_Db_Select($this->db);
        $resOc = $select->from(['t1'=>'t_workorders'],['fc_meter_um'])
            ->join(['t2'=>'t_wo_relations'],'t1.f_code = t2.f_code_wo_master',['f_code_wo_slave'])
            ->where("f_code_wo_master = ?",$meterCode)->where("f_type_id_wo_slave = 9")
            ->query()->fetchAll();
        $tot = count($resOc);
        for($i = 0;$i < $tot;++$i) {
            $this->db->update("t_workorders",['fc_meter_um'=>$resOc[$i]['fc_meter_um']],"f_code = {$resOc[$i]['f_code_wo_slave']}");
        }
    }
    
    
    /**
     * Set fc_meter_value, fc_meter_um, fc_meter_read_time
     * for oc crossed with meter code
     * @param int $meterCode meter f_code
     */
    private function setOcMeterValues($meterCode)
    {
        $select = new Zend_Db_Select($this->db);
        $resMeterInfo = $select->from("t_workorders",['fc_meter_value', 'fc_meter_um', 'fc_meter_read_time'])
            ->where("f_code = ?",$meterCode)->query()->fetch();
        $select->reset();
        $resOcCodes = $select->from(["t_wo_relations"],['fCodeOc'=>'f_code_wo_slave'])            
            ->where("f_code_wo_master = ?",$meterCode)->where("f_type_id_wo_slave = 9")->query()->fetchAll();
        $tot = count($resOcCodes);
        for($i = 0;$i < $tot;++$i) {
            $this->db->update('t_workorders',$resMeterInfo,"f_code = {$resOcCodes[$i]['fCodeOc']}");            
        }
    }
    
    private function getPrevReads($codeMeter)
    {
        $select = new Zend_Db_Select($this->db);        
        $resNum = $select->from('t_meters_history',['f_value','f_timestamp','f_id'])->where('f_code = ?',$codeMeter)
            ->order('f_timestamp asc')->query()->fetchAll(); 
        $tot = count($resNum);
        if($tot <= 1) { return []; }
        $slice = $tot <= 20?$tot:($tot<=1000?(int)($tot*0.8):800);       
        return array_slice($resNum, $tot - $slice, $slice);
    }
    
    /**
     * Calculate for condition of type 'EVERY' the next due date and subsequent due date
     * @param int $codeMeter meter code
     * @param array [$resCond] oc info to calculate next due date
     */
	/*
    private function calculateNextDueDateOc($codeMeter,$resCond = [])
    {
        $select = new Zend_Db_Select($this->db);
        if(empty($resCond)){
            $resCond = $select->from(['t1'=>'t_wo_relations'],[])
                ->join(array('t2'=>'t_workorders'),'t1.f_code_wo_slave = t2.f_code',['fc_cond_meter_code','f_code','fc_cond_value_reference','fc_cond_start_value'])
                ->where('f_code_wo_master = ?',$codeMeter)->where('f_type_id = 9')->where("fc_cond_list_function = 'EVERY'")
                ->query()->fetchAll();        
        }
        $prevReads = $this->getPrevReads($codeMeter);        
        if(empty($resCond) || empty($prevReads) ){ return;}        
        //convert timestamp reading from seconds to days
        $t = []; $ft = [];
        $tot = count($prevReads);
        $dayFirst = (int)($prevReads[0]['f_timestamp']/86400);
        $firstDayTs = $prevReads[0]['f_timestamp'];         
        for($a = 0;$a <$tot;++$a) {                    
            $days = ((int)($prevReads[$a]['f_timestamp']/86400)) - $dayFirst; 
            if($days == 0){ continue;}
            $t[] = $days;  $ft[] = $prevReads[$a]['f_value'];
        }               
        if($tot == 0){ return; }//if there aren't previous reading, stop
        $totB = count($resCond);
        for($b = 0;$b < $totB;++$b) {
            $nextValue = $this->getEveryValue($resCond[$b]['fc_cond_meter_code'], $resCond[$b]['fc_cond_value_reference'],$resCond[$b]['fc_cond_start_value']);                
            $this->db->update('t_workorders',['fc_cond_next_threshold'=>$nextValue],"f_code = {$resCond[$b]['f_code']}");//set next threshold value in oc
            $this->fillOcPeriodics($resCond[$b]['f_code'],$resCond[$b]['fc_cond_value_reference'],
                ['t'=>$t,'ft'=>$this->normalizeReadingSeries($ft,$resCond[$b]['fc_cond_value_reference']),'nextValue'=>$nextValue,'firstTimestamp'=>$firstDayTs]);                        
        }         
    }*/
    
    // calcola nuovo valore di soglia se ci sono almeno due letture
	private function calculateNextDueDateOc($codeMeter,$resCond = [])
    {
        /*
         * $resCond[$b]['fc_cond_value_reference'] = valore di soglia
         */
        
        $select = new Zend_Db_Select($this->db);
        if(empty($resCond)){
            $resCond = $select->from(['t1'=>'t_wo_relations'],[])
                ->join(array('t2'=>'t_workorders'),'t1.f_code_wo_slave = t2.f_code',['fc_cond_meter_code','f_code','fc_cond_value_reference','fc_cond_start_value'])
                ->where('f_code_wo_master = ?',$codeMeter)->where('f_type_id = 9')->where("fc_cond_list_function = 'EVERY'")
                ->query()->fetchAll();        
        }
        $prevReads = $this->getPrevReads($codeMeter); 
		//print_r($resCond); print_r($prevReads); 
        if(empty($resCond) || empty($prevReads) ){ return;}        
        //convert timestamp reading from seconds to days
        $t = []; $ft = [];
        $tot = count($prevReads);
        $dayFirst = (int)($prevReads[0]['f_timestamp']/86400);
        $firstDayTs = $prevReads[0]['f_timestamp'];         
        for($a = 0;$a <$tot;++$a) {                    
            $days = ((int)($prevReads[$a]['f_timestamp']/86400)) - $dayFirst; 
            //if($days == 0){ continue;}
            $t[$days] = $days;  $ft[$days] = $prevReads[$a]['f_value'];
        }    
		$tot_t=count($t);
        //if($tot == 0){ return; }//if there aren't previous reading, stop
		if($tot_t < 1){ return; }//if there is only one red, or less, then stop. Se c'è una sola lettura non si può calcolare la retta di regressione
		sort($ft);
		sort($t);
		
        $totB = count($resCond);
		for($b = 0;$b < $totB;++$b) {
                   $resCond[$b]['fc_cond_meter_code']=isset($resCond[$b]['fc_cond_meter_code'])?$resCond[$b]['fc_cond_meter_code']:$codeMeter;
            // calcolo nuovo valore di soglia per ogni on condition generator (meter code, valore di soglia, valore iniziale on condition)
            $nextValue = $this->getEveryValue($resCond[$b]['fc_cond_meter_code'], $resCond[$b]['fc_cond_value_reference'],$resCond[$b]['fc_cond_start_value']);                
			
			$this->db->update('t_workorders',['fc_cond_next_threshold'=>$nextValue],"f_code = {$resCond[$b]['f_code']}");//set next threshold value in oc
            $this->fillOcPeriodics($resCond[$b]['f_code'],$resCond[$b]['fc_cond_value_reference'],
                ['t'=>$t,'ft'=>$this->normalizeReadingSeries($ft,$resCond[$b]['fc_cond_value_reference']),'nextValue'=>$nextValue,'firstTimestamp'=>$firstDayTs]);                        
        }         
    }
    
    /**
     * Normalize meter reading to create a ordered ascending series
     * @param array $series list of meter reading values
     * @param mixed $refVal reference valu of on condition
     */
    private function normalizeReadingSeries($series,$refVal)
    {
        if(empty($series)){ return []; }
        $correctList = [$series[0]];        
        $tot = count($series);
        for($i = 1;$i < $tot;++$i) {
            if($series[$i] >= max($correctList)){
                $correctList[] = $series[$i];
            }
            else {        
                $correctVal = $series[$i];
                do{
                    $correctVal +=$refVal;
                }while($correctVal<=max($correctList));        
                $correctList[] = $correctVal;        
            }
        }
        return $correctList;
    }
    
    /**
     * Calculate next generation days (teorically) using regression function
     * @param int $fCodeOc oc generator code
     * @param mixed $valRef oc generatore value reference
     * @param array $options array with the values t (array of days from first reading),
     *  ft (array with reading values), nextValue (next value where system will generate),
     *  firstTimestamp (timestamp of first reading
     */
    private function fillOcPeriodics($fCodeOc,$valRef,$options)
    {
        $endDate = mktime(null,null,null,null,null,date('Y')+2);        
        $wo = new Mainsim_Model_Workorders($this->db);
        $nextValue = $options['nextValue'];
        $nextGeneration = 0;            
        $r2 = 0;
        $this->db->delete("t_periodics","f_code = $fCodeOc");           
        $this->db->update("t_workorders",['fc_pm_next_due_date'=>null,'fc_pm_subsequent_due_date'=>null],"f_code = $fCodeOc");           
        $i = 0;        
        while($nextGeneration < $endDate) {                
            $regression = $this->regression($options['t'], $options['ft'],$nextValue,$r2);   
            $r2 = $regression['r2'];
            if(!$regression) {break;}
            $nextGeneration = $options['firstTimestamp'] + ($regression['out'] * 86400);                        
            $i++;
            if($nextGeneration > $endDate){continue;}
            $this->db->insert("t_periodics",['f_code'=>$fCodeOc,
                'f_forewarning'=>0,'f_start_date'=>$nextGeneration,
                'f_end_date'=>$nextGeneration+86400,
                'f_duration'=>86400,'f_timestamp'=>time(),
                'f_mode'=>0,'f_available_type'=>0,'f_executed'=>0
            ]);
            $nextValue+=$valRef;
        }
        $this->db->update("t_workorders",['fc_cond_r2'=>$r2],"f_code = {$fCodeOc}");
        $wo->addNextDueDate($fCodeOc);     
    }
    
    private function regression($X,$Y,$nextVal,$r2 = 0)
    {        
      if (!is_array($X) && !is_array($Y)) return false;
      if (count($X) <> count($Y) || count($X) == 1) return false;
      if (empty($X) || empty($Y)) return false;

      $regres = [];$sst = 0;$ssr = 0;$sse = 0;
      $n = count($X);      
      $mx = array_sum($X)/$n; // media delle x
      $my = array_sum($Y)/$n; // media delle y
      $sxy = 0;
      $sxsqr = 0;      
      for ($i=0;$i<$n;$i++){
        $sxy += ($X[$i] - $mx) * ($Y[$i] - $my);
        $sxsqr += pow(($X[$i] - $mx),2); // somma degli scarti quadratici medi
      }

      $m = $sxy / $sxsqr; // coefficiente angolare
      $q = $my - $m * $mx; // termine noto     
                  
      $out = $m == 0?0:($nextVal - $q)/$m;
      if(!$r2 && $m) {
          for ($i=0;$i<$n;$i++){
            $sst += pow($Y[$i] - $my,2);            
            $regres[$i] = $m * $X[$i] + $q;
            $ssr += pow($regres[$i] - $my,2);
            $sse += pow($Y[$i] - $regres[$i],2);
          }
          $r2 = number_format(($ssr/$sst), 2);
      }
      return ['out'=>(int)$out,'r2'=>$r2];
    }
    
    // calcolo nuovo valore di soglia (meter code, valore di soglia, valore iniziale della oncondition) 
    private function getEveryValue($f_code,$times,$startValue)
    {
        // times = valore di soglia
        $select = new Zend_Db_Select($this->db);
        $resValue = $select->from("t_meters_history","f_value")
          ->where("f_code = ?",$f_code)->order("f_id desc")->limit(1)
          ->query()->fetch();                
        $prevVal = empty($resValue)?0:$resValue['f_value'] - $startValue;     
        //$prevVal = $prevVal < $startValue?$startValue:$f_code
        //if($prevVal < $startValue){ return $startValue; }        
        $count = ((int)($prevVal/$times)) + 1;
        // $newThreesholdValue = nuovo valore di soglia 
        $newThreesholdValue = ($times * $count)+$startValue;                
        return $newThreesholdValue;        
    }
    
    
    /**
     * Script to insert new meter reading value  
     */
    /*public function performedReading($userId = null)
    {   
        $select_rem = new Zend_Db_Select($this->db);
        //get parent_code from f_code_periodic and write history log for meter generator
        $res_meter_value = $select_rem->from("t_workorders",array("f_code_periodic","fc_meter_value","f_type_id"))                
                ->where("f_code = ?",$this->f_code)->query()->fetch();        
        if(!empty($res_meter_value['f_type_id']) && $res_meter_value['f_type_id'] == 7) { return; }
        $history = array(
            'f_code'=>$res_meter_value['f_code_periodic'],
            'f_active'=>1,
            'f_timestamp'=>time(),
            'f_user_id'=>$userId ? $userId : $this->userinfo->f_id,
            'f_value'=>$res_meter_value['fc_meter_value'],
            'f_checked'=>0,
            'f_in_work'=>0
        );        
        $this->db->insert("t_meters_history",$history);        
        //change value with last insert        
        $this->db->update("t_workorders",['fc_meter_value'=>$res_meter_value['fc_meter_value'],'fc_meter_read_time'=>time()],"f_code = {$res_meter_value['f_code_periodic']}");        
        $select_rem->reset();
        $resNum = $select_rem->from('t_meters_history',['f_value','f_timestamp','f_id'])->where('f_code = ?',$res_meter_value['f_code_periodic'])
            ->order('f_timestamp asc')->query()->fetchAll();        
        $tot = count($resNum);
        if($tot <= 1) { return; }
        $slice = $tot <= 20?$tot:($tot<=1000?(int)($tot*0.8):800);        
        $this->calculateNextDueDateOc(array_slice($resNum, $tot - $slice, $slice),$res_meter_value['f_code_periodic']);
        /*$this->db->update("t_workorders",array('fc_meter_value'=>$res_meter_value['fc_meter_value'],'fc_meter_read_time'=>time()),
            "f_code = {$res_meter_value['f_code_periodic']}");         
        $select_rem->reset();
        $resNum = $select_rem->from('t_meters_history',array('f_value','f_timestamp','f_id'))->where('f_code = ?',$this->f_code)
            ->order('f_timestamp desc')->query()->fetchAll();        
        $tot = count($resNum);
        if($tot > 1) {
            $this->calculateNextDueDateOc(array_slice($resNum, 0, 10), $this->f_code);
        }
    }*/
        
    
    public function deletePhase()
    {        
        /*18/01/2015 - Lara: Funzione per il controllo di figli associati ad un elemento*/
        if($this->table == 't_workorders' && $this->thereAreChildren($this->table))
            throw new Exception('you cannot close this element because its has got one or more children');
        /************************/
        
        $select = new Zend_db_Select($this->db);   
        $resTypeId = $select->from("{$this->table}",array('f_type_id'))
            ->where("f_code = ?",$this->f_code)->query()->fetch();
        
        //if selectors check if try to remove root and forbid this
        if($this->table == 't_selectors'){
            if($this->f_code >= 10 && $this->f_code <= 19){
                throw new Exception ("This selector cannot be removed.");
            }        
        }
        
        $select->reset();
        $resOO = $select->from('t_workorders','fc_wo_out_of_order')
            ->where('f_code = ?',$this->f_code)->query()->fetch();
        if($resOO['fc_wo_out_of_order'] == 1) {
            throw new Exception('You cannot close or delete a request with Out of order asset');
        }
        
        if($resTypeId['f_type_id'] == 1 && $this->table == 't_wares') {
            //if try delete an asset, check if is linked in any wo
            $select->reset();
            $res_check = $select->from(array("t1"=>"t_ware_wo"),array('num'=>'count(*)'))
                ->join(array("t2"=>"t_workorders"),"t1.f_wo_id = t2.f_code",[])
                ->join(array('t5'=>"t_creation_date"),"t2.f_code = t5.f_id",[])
                ->join(array("t3"=>"t_wf_phases"),"t3.f_number = t5.f_phase_id and t3.f_wf_id = t5.f_wf_id",[])
                ->join(array("t4"=>"t_wf_groups"),"t4.f_id = t3.f_group_id",[])                            
                ->where("t1.f_ware_id = ?",$this->f_code)
                ->where("t4.f_visibility != 0")->query()->fetch();
            if($res_check['num'] > 0) {                    
                throw new Exception ("Cannot delete asset. Remove all linked workorders before.");
            }
        }
    }
    /* 31 Marzo 2016 Baldini// openCorrective che richiami approveWorkrequest*/
    public function openCorrective()
    {
        $this->approveWorkrequest(1);
    }
    
    /* 31 Marzo 2016 Baldini// aggiunta la funzione approveWorkrequest che permette di creare qualsiasi tipo di wo da WR.*/
    public function approveWorkrequest($type=1)
    {
        $obj = new Mainsim_Model_MainsimObject($this->db);        
        $obj->type = 't_workorders';
        $resType = $this->db->query("select f_wf_id,f_wf_phase,f_module_name,f_type from t_workorders_types where f_id = (?)",$type)->fetch();        
        $res = $obj->cloneObjects($this->f_code, 1, $resType['f_module_name'], false);
        if(isset($res['error'])){
            throw new Exception($res['error']);
        }
        $woCode = $res['codes_cloned'][$this->f_code];        
        $fc_progress = Mainsim_Model_Utilities::getFcProgress("t_workorders", $type, $this->db);
        $this->db->update('t_workorders',array('f_type_id'=>$type),"f_code = $woCode");
        $this->db->update('t_creation_date',array('f_category'=>$resType['f_type'],
            'f_wf_id'=>$resType['f_wf_id'],
            'f_phase_id'=>$resType['f_wf_phase'],'fc_progress'=>$fc_progress),"f_id = $woCode");     
        
         /* 31 Marzo 2016 Baldini// BUG FIX: update dello storico del nuovo wo.*/
        $resWr = $this->db->query("select fc_progress,f_phase_id from t_creation_date where f_id = (?)",$this->f_code)->fetch();  
        
        $this->db->update('t_creation_date_history',array('f_phase_id'=>$resWr["f_phase_id"],'fc_progress'=>$resWr["fc_progress"]),"f_code = $woCode");  
        
    }
    
    /*18/01/2015 - Lara: Funzione per il controllo di figli associati ad un elemento*/
    public function thereAreChildren($type){
        $select = new Zend_Db_Select($this->db);
        $select->reset();
        $resChildren = $select->from(array('t1'=>$type.'_parent'),array('f_code'))
            ->join(array('t5'=>"t_creation_date"),"t1.f_code = t5.f_id",[])
            ->join(array('t6'=>"t_wf_phases"),"t5.f_phase_id = t6.f_number AND t5.f_wf_id = t6.f_wf_id",[])
            ->where('f_parent_code = ?',$this->f_code)
            ->where('f_group_id not in (6,7)')
            ->query()->fetchAll();
        
        if(count($resChildren) > 0) return true;
        return false;
    }
    
    public function closeRequest()
    {
        /*18/01/2015 - Lara: Funzione per il controllo di figli associati ad un elemento*/
        if($this->table == 't_workorders' && $this->thereAreChildren($this->table))
            throw new Exception('you cannot close this workorders because its has got one or more children');
        /************************/
        
        $select = new Zend_Db_Select($this->db);
        /*//check if user close a father periodic and, if is true, check if every child are close or delete
        $select = new Zend_Db_Select($this->db);
        $res_wr = $select->from(array("t1"=>"t_workorders_parent"),[])
            ->join(array("t2"=>"t_workorders"),"t1.f_code = t2.f_code",[])
            ->join(array("t5"=>"t_creation_date"),"t2.f_code = t5.f_id",array('num'=>'count(*)'))
            ->join(array("t3"=>"t_wf_phases"),"t3.f_number = t5.f_phase_id and t3.f_wf_id = t5.f_wf_id",[])
            ->join(array("t4"=>"t_wf_groups"),"t4.f_id = t3.f_group_id",[])
            ->where("t1.f_active = 1")->where("f_parent_code = ?",$this->f_code)->where('t4.f_visibility <> 0')
            ->query()->fetch();
        
        if($res_wr['num'] > 0) {                            
            throw new Exception("You cannot close or delete this request till you don't close child request");
        }
        $select->reset();*/
        $resOO = $select->from('t_workorders',['fc_wo_out_of_order','f_type_id','f_code_periodic'])
            ->where('f_code = ?',$this->f_code)->query()->fetch();
        if($resOO['fc_wo_out_of_order'] == 1) {
            throw new Exception('you cannot close or delete a work order with the field Out of order set true.');
        }
        //calculate costs
        $this->calculateCosts();   
        //periodic shift 
        $this->recalculatePmOc();
        //add meter reading
        if($resOO['f_type_id'] == 7) { $this->fc_meter_add_value($resOO['f_code_periodic']); }
        
        $select->reset();
        $resAsset = $select->from(array('t1'=>'t_ware_wo'),[])
            ->join(array('t2'=>'t_wares'),'t1.f_ware_id = t2.f_code',array('f_code'))
            ->where('f_wo_id = ?',$this->f_code)->where('f_type_id = 1')
            ->query()->fetchAll();
        $tot = count($resAsset);        
        for($i = 0;$i < $tot;++$i) {
            //execute script for update asset values        
            $this->wo_fc_asset_downtime_total_hours($resAsset[$i]['f_code']);
            $this->fc_asset_available($resAsset[$i]['f_code']);
            $this->wo_fc_assetno_failures_wo($resAsset[$i]['f_code']);
            $this->wo_fc_asset_mtbf($resAsset[$i]['f_code']);
            $this->wo_fc_asset_mttr($resAsset[$i]['f_code']);
            $this->wo_fc_asset_mtbr($resAsset[$i]['f_code']);            
            $this->fc_asset_last_wo_closed($resAsset[$i]['f_code']);
            $this->fc_condition($resAsset[$i]['f_code']);        
        }
    }
    
    /**
     * Recalculate for pm/oc in double condition the next threshold
     * or next due date for pm     
     */
    private function recalculatePmOc()
    {
        $select = new Zend_Db_Select($this->db);
        $resWoClosed = $select->from("t_workorders",['f_type_id','fc_wo_ending_date','f_code_periodic'])
            ->where("f_code = ?",$this->f_code)->query()->fetch();               
        
        // type id 6 = OC
        // type id 9 = OC GEN
        // type id 3 = PM GEN
        // type id 4 = PM
        
        if($resWoClosed['f_type_id'] != 6 && $resWoClosed['f_type_id'] != 4) { return; }
        $type_id_crossed = $resWoClosed['f_type_id'] == 6?3:9;
        $wo = new Mainsim_Model_Workorders($this->db);        
        $select->reset();
        $select->from(["t1"=>"t_wo_relations"],"f_code_wo_slave AS f_code_crossed")
            ->join(["t2"=>"t_workorders"],"f_code_wo_slave = f_code",["fc_pm_fixed","fc_cond_meter_code"])
            ->where("t1.f_code_wo_master = ?",$resWoClosed['f_code_periodic'])
            ->where("t1.f_type_id_wo_slave = ?",$type_id_crossed);
        $resPm = $select->query()->fetch();    
        // se non esiste relazione master slave ricerca a ruoli inveriti
        if(!$resPm){
            $select->reset();
            $select->from(["t1"=>"t_wo_relations"],"f_code_wo_master AS f_code_crossed")
                    ->join(["t2"=>"t_workorders"],"f_code_wo_master = f_code",["fc_pm_fixed","fc_cond_meter_code"])
                    ->where("t1.f_code_wo_slave = ?",$resWoClosed['f_code_periodic'])
                    ->where("t1.f_type_id_wo_master = ?",$type_id_crossed);
            $resPm = $select->query()->fetch();   
        }
        if($resWoClosed['f_type_id'] == 6 && $resPm){ //on condition shift next pm
            if(empty($resPm['fc_pm_fixed'])){ return; }//not exists or is fixed                        
            $wo->createPeriodics($resPm['f_code_crossed'], $resWoClosed['fc_wo_ending_date']);//recalculate
        }
        elseif($resWoClosed['f_type_id'] == 4 && $resPm) {            
            //if(!isset($this->params['fc_meter_add_value']) || is_null($this->params['fc_meter_add_value'])) {throw new Exception("Missing the last reading");}
            if(!isset($this->params['fc_meter_value']) || is_null($this->params['fc_meter_value'])) {throw new Exception("Missing the last reading");}
            //$this->db->update("t_workorders",['fc_cond_start_value'=>$this->params['fc_meter_add_value']],"f_code = {$resPm['f_code_crossed']}");
            $this->db->update("t_workorders",['fc_cond_start_value'=>$this->params['fc_meter_value']],"f_code = {$resPm['f_code_crossed']}");
            $this->fc_meter_add_value($resPm['fc_cond_meter_code']);
        }
    }
    
    /**
     * Write fc_condition from closing wo to releated asset
     * @param int $f_code_asset : f_code of releated asset to wo
     */
    private function fc_condition($f_code_asset)
    {
        $select = new Zend_Db_Select($this->db);
        $resCondition = $select->from("t_creation_date","fc_condition")
            ->where("f_id = ?",$this->f_code)->query()->fetch();
        if(is_null($resCondition['fc_condition'])){ return;}                
        $this->db->update("t_creation_date",['fc_condition'=>$resCondition['fc_condition']],
            "f_id = {$f_code_asset}");
    }
    
    /**
     * write information about last completed corrective,corrective task,emergency 
     * in asset
     */
    private function fc_asset_last_wo_closed($f_code_asset)
    {
        if(!empty($this->params['f_type_id']) && in_array($this->params['f_type_id'],array(1,10,13))){
            $select = new Zend_Db_Select($this->db);
            $trsl = new Mainsim_Model_Translate($this->userinfo->f_language,$this->db);
            $resEndDate = $select->from(array("t1"=>"t_workorders"),"fc_wo_ending_date")
                ->join(array("t2"=>"t_creation_date"),"t1.f_code = t2.f_id",array("fc_progress","f_category"))
                ->where("f_code = ?",$this->f_code)->query()->fetch();            
            if(empty($resEndDate['fc_wo_ending_date'])) return;
            $time = date('d/m/Y',$resEndDate['fc_wo_ending_date']);
            $num = sprintf("%05s",$resEndDate['fc_progress']);
            //$string = $trsl->_('Type').': '.$trsl->_($resEndDate['f_category']).' - '.$trsl->_('Ending date').': '.$time.' - WO ID:'.$num;            
            $string = "WO ID: $num - ".$trsl->_('Category').': '.$trsl->_($resEndDate['f_category']).' - '.$trsl->_('Ending date').': '.$time;            
            $this->db->update("t_wares",array("fc_asset_last_wo_closed"=>$string),"f_code = {$f_code_asset}");
        }
    }
    
    
    /**
     *update total downtime hours per asset 
     */
    private function wo_fc_asset_downtime_total_hours($f_code_asset)
    {
        $query = "SELECT SUM( pc.fc_asset_downtime_hours ) AS hours
            FROM t_pair_cross as pc 
                JOIN t_workorders as wo ON pc.f_code_main = wo.f_code
                JOIN t_creation_date as tcd ON tcd.f_id = wo.f_code
                JOIN t_wf_phases as ph ON ( tcd.f_wf_id = ph.f_wf_id AND tcd.f_phase_id = ph.f_number ) 
            WHERE
                pc.f_code_cross = {$f_code_asset}
                and ph.f_group_id = 6
                AND pc.fc_asset_downtime_hours >0
            GROUP BY pc.f_code_cross";
        $resValue = $this->db->query($query)->fetch();             
        $this->db->update('t_wares',array('fc_asset_downtime_total_hours'=>$resValue['hours']),"f_code = {$f_code_asset}");                            
    }
    
    public function applyMovimentation()
    {
        $sel_mat = new Zend_Db_Select($this->db);
        $wa = new Mainsim_Model_Wares($this->db);
        $error = [];
        $res_pair = $sel_mat->from(array("t1"=>"t_pair_cross"))
                ->join(array("t2"=>"t_wares"),"t1.f_code_cross = t2.f_code",[])
                ->where("t1.f_code_main = ?",$this->f_code)->where("t2.f_type_id = 3")
                ->query()->fetchAll();
        //----------------------------------------------------------------------
        $sel_mat->reset();
        $res_cross_mat = $sel_mat->from(array("t1"=>"t_wares"),[])
                ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id")
                ->where("t2.f_wo_id = ?",$this->f_code)->where("t1.f_type_id = 3")
                ->query()->fetchAll();
        $codes = [];
        $tot_ci = count($res_cross_mat);
        for($i = 0;$i < $tot_ci;++$i) {
            $codes[] = $res_cross_mat[$i]['f_ware_id'];
        }
        $codes_mat = implode(',',$codes);
        //----------------------------------------------------------------------        
        $sel_mat->reset();
        $params = $sel_mat->from(array("t1"=>"t_creation_date"))
                ->join(array("t2"=>"t_workorders"),"t1.f_id = t2.f_code")
                ->join(array("t3"=>"t_custom_fields"),"t1.f_id = t3.f_code")
                ->where("t1.f_id = ?",$this->f_code)->query()->fetch();        
        if(empty($res_pair)) return;
        $total_moved = 0;                    
        $tot_a = count($res_pair);      
        $class_list = [];
        for($a = 0;$a < $tot_a;++$a) {
            $pair_mat = $res_pair[$a];
            if(empty($pair_mat["fc_mov_moved_type"])){
                throw new Exception('Please declare which operation do with materials inserted.');
            }
            if(empty($pair_mat["fc_mov_moved_type"])){
                throw new Exception('Please declare which operation do with materials inserted.');
            }
            if($pair_mat['fc_mov_moved_type'] == 'movein') {
                $total_moved += $pair_mat['fc_mov_moved_quantity'];                                    
            }
            elseif($pair_mat['fc_mov_moved_type']  == 'moveout') {
                $total_moved -= $pair_mat['fc_mov_moved_quantity'];                                    
            } 
            $sel_mat->reset();
            $res_matclass = $sel_mat->from(array("t1"=>"t_wares_relations"),"f_code_ware_slave")
                    ->where("f_code_ware_master = ?",$pair_mat['f_code_cross'])
                    ->where("f_type_id_slave = 21")->query()->fetch();
            if(!empty($res_matclass)) {
                $class_list[$res_matclass['f_code_ware_slave']] = $res_matclass['f_code_ware_slave'];
            }
        }
        //apply check for every type of movimentation        
        switch($params['fc_mov_type']) {
            case 1: // purchase order
                if(!empty($params['fc_mov_po_moved_quantity']) && $total_moved != $params['fc_mov_po_moved_quantity']) {
                    throw new Exception('During a movimentation from purchase order quantity stocked must be the same of quantity arrived.');
                }
                break;
            case 2: // movimentation between warehouse
                if(count($res_cross_mat) < 2) {
                    throw new Exception ("You cannot apply a warehouse balance if materials are less then two");
                }
                $sel_mat->reset();
                //check if come from same father and different from 0.                
                $res_check_class = $sel_mat->from("t_wares_relations","f_code_ware_slave")
                        ->where("f_code_ware_master IN ($codes_mat)")->where("f_type_id_slave = 21")
                        ->group("f_code_ware_slave")->query()->fetchAll();
                if(count($res_check_class) == 0 || count($res_check_class) > 1 ) {
                    throw new Exception ("You cannot apply a warehouse balance if materials are not under the same class");
                }
                //apply movimentation (if possible)
                $tot_sa = count($res_pair);                
                $sum_zero_list = [];
                for($sa = 0;$sa < $tot_sa;++$sa) {
                    if(!isset($sum_zero_list[$res_pair[$sa]['f_code_cross']])) {
                        $sel_mat->reset();
                        $res_material = $sel_mat->from("t_wares",array("fc_material_tot_avbl_qty","fc_material_tot_stored_qty","fc_material_type","fc_material_stock_type"))
                            ->where("f_code = ?",$res_pair[$sa]['f_code_cross'])
                            ->query()->fetch();
                        $sum_zero_list[$res_pair[$sa]['f_code_cross']]['fc_material_tot_avbl_qty'] = $res_material['fc_material_tot_avbl_qty'];
                        $sum_zero_list[$res_pair[$sa]['f_code_cross']]['sum_zero'] = 0;
                    }
                    //check if moveout is over stocks
                    if($res_pair[$sa]['fc_mov_moved_type'] == 'moveout' && $res_material['fc_material_stock_type'] == 2 &&
                            ($sum_zero_list[$res_pair[$sa]['f_code_cross']]['fc_material_tot_avbl_qty'] - $res_pair[$sa]['fc_mov_moved_quantity'] < 0)  ) {
                        throw new Exception("You cannot withdraw materials if you have not enough stocks");
                    }

                    if($res_pair[$sa]['fc_mov_moved_type'] == 'moveout') { //moveout
                        $sum_zero_list[$res_pair[$sa]['f_code_cross']]['sum_zero']-= $res_pair[$sa]['fc_mov_moved_quantity'];                        
                    }
                    else { //movein
                        $sum_zero_list[$res_pair[$sa]['f_code_cross']]['sum_zero'] += $res_pair[$sa]['fc_mov_moved_quantity'];                        
                    }
                }                
                foreach($sum_zero_list as $line_zero_list) {
                    if($line_zero_list['sum_zero'] != 0) {
                        throw new Exception("Warehouse balance cannot leave out materials");
                    }
                }                
                break;            
        }
        foreach($res_pair as $pair_mat) {   
            $params_edit_ware = array(
                'f_module_name'=>"mdl_material_tg",
                'f_type_id'=>3,
                'f_code'=>$pair_mat['f_code_cross']
            );
            $sel_mat->reset();
            $mat = $sel_mat->from("t_wares",array("fc_material_tot_avbl_qty","fc_material_tot_stored_qty"))
                            ->where("f_code = ?",$pair_mat['f_code_cross'])->query()->fetch();
            $time = time();
            $creation['f_timestamp'] = $time;
            $mat['f_timestamp'] = $time;
            $custom['f_timestamp'] = $time;                            
            if($pair_mat['fc_mov_moved_type'] == 'movein') {
                $params_edit_ware['fc_material_last_rcvd_qty'] = $pair_mat['fc_mov_moved_quantity'];
                $params_edit_ware['fc_material_last_rcvd_date'] = time();
                $mat['fc_material_tot_avbl_qty'] += $pair_mat['fc_mov_moved_quantity'];
                $mat['fc_material_tot_stored_qty'] += $pair_mat['fc_mov_moved_quantity'];
                if( $mat['fc_material_tot_avbl_qty'] > 0) {
                    $sel_mat->reset();
                    $res_wo_ko = $sel_mat->from(array("t1"=>"t_pair_cross"),array('fc_material_moving_date','fc_material_moved_quantity'))
                        ->join(array("t2"=>"t_workorders"),"t1.f_code_main = t2.f_code",array("f_code"))
                        ->join(array("t3"=>"t_creation_date"),"t3.f_id = t2.f_code",[])
                        ->join(array("t4"=>"t_wf_phases"),"t4.f_number = t3.f_phase_id and t4.f_wf_id = t3.f_wf_id",[])										
                        ->join(array("t5"=>"t_wf_groups"),"t4.f_group_id = t5.f_id",[])
                        ->where("t2.fc_wo_empty_spare = 1")->where("t5.f_visibility != 0")->where("t1.f_code_cross = ?",$pair_mat['f_code_cross'])->query()->fetchAll();
                    foreach($res_wo_ko as $line_wo_ko) {
                        $wo = array('f_timestamp'=> $time);
                        if( $mat['fc_material_tot_avbl_qty'] > 0) {														
                                $wo['fc_wo_empty_spare'] = 0;										
                        }
                        //if quantity available continue to be under 0, remove empty spare only for who request material before			
                        elseif( $line_wo_ko['fc_material_moving_date'] > 0 && ($qty_arr - $line_wo_ko['fc_material_moved_quantity']) >= 0 ) { 					
                            $qty_arr -= $line_wo_ko['fc_material_moved_quantity'];
                            $wo['fc_wo_empty_spare'] = 0;															
                        }				
                        //update wo
                        if(!isset($wo['fc_wo_empty_spare'])) continue;				
                        Mainsim_Model_Utilities::newRecord($this->db, $line_wo_ko['f_code'],'t_creation_date',$creation);
                        Mainsim_Model_Utilities::newRecord($this->db, $line_wo_ko['f_code'],'t_workorders',$wo);
                        Mainsim_Model_Utilities::newRecord($this->db, $line_wo_ko['f_code'],'t_custom_fields',$custom);
                    }
                }
            }
            elseif($pair_mat['fc_mov_moved_type']  == 'moveout') {
                $mat['fc_material_tot_avbl_qty'] -= $pair_mat['fc_mov_moved_quantity'];
                $mat['fc_material_tot_stored_qty'] -= $pair_mat['fc_mov_moved_quantity'];
            }    
            
            $params_edit_ware['fc_material_tot_avbl_qty'] = $mat['fc_material_tot_avbl_qty'];
            $params_edit_ware['fc_material_tot_stored_qty'] = $mat['fc_material_tot_stored_qty'];
            $res_wa = $wa->editWares($params_edit_ware, false, false);
            if(!empty($res_wa)) {
                throw new Exception($res_wa['message']);
            }
        }
        //reset information about material class
        foreach($class_list as $class) {
            $params_edit_matclass = array(
                'f_module_name'=>"mdl_matclass_tg",
                'f_type_id'=>21,
                'f_code'=>$class
            );
            //get class info
            $sel_mat->reset();
            $res_matclass_avg = $sel_mat->from("t_wares",array("fc_material_avg_price"))
                    ->where("f_code = ?",$class)->query()->fetch();
            $avg_price = $res_matclass_avg['fc_material_avg_price'] > 0?$res_matclass_avg['fc_material_avg_price']:0;
            //get instance available and stored quantity updated
            $sel_mat->reset();
            $res_instance_qty = $sel_mat->from(array("t1"=>"t_wares"),array(
                "fc_material_tot_avbl_qty","fc_material_tot_stored_qty")
            )                    
            ->join(array("t2"=>"t_wares_relations"),"t1.f_code = t2.f_code_ware_slave",[])        
            ->where("t2.f_type_id_slave = 3")
            ->where("f_code_ware_master = ?",$class)->query()->fetchAll();
            $tot_stored = 0;
            $tot_stored_price = 0;
            $tot_avbl = 0;
            $tot_avbl_price = 0;
            $tot_b = count($res_instance_qty);
            for($b = 0;$b < $tot_b;++$b) {
                $tot_stored+=$res_instance_qty[$b]['fc_material_tot_stored_qty'];
                $tot_avbl+=$res_instance_qty[$b]['fc_material_tot_avbl_qty'];
            }
            $tot_stored_price = $tot_stored*$avg_price;
            $tot_avbl_price = $tot_avbl*$avg_price;
            $params_edit_matclass['fc_material_tot_stored_qty'] = $tot_stored;
            $params_edit_matclass['fc_material_tot_avbl_qty'] = $tot_avbl;
            $params_edit_matclass['fc_material_tot_stored_value'] = $tot_stored_price;
            $params_edit_matclass['fc_material_tot_avbl_value'] = $tot_avbl_price;
            $res_edit = $wa->editWares($params_edit_matclass, false, false);
            if(!empty($res_edit)) {
                throw new Exception($res_edit['message']);
            }
        }        
        if(isset($error['message'])) throw new Exception($error['message']);
    }
    
    /**
     * List of method if saved a material 
     */
    public function t_wares_21()
    {
        if($this->table == 't_wares' && $this->params['f_type_id'] == 3) {
            if(count($this->params['t_wares_21']['f_code']) > 1 ) throw new Exception("Every material can be associated to one material class");
            $this->fc_material_matclass_cross();
            //update price and last received date
            $this->updatePriceAndDate($this->params['t_wares_21']['f_code'][0]);
        }
    }
    
    /*
     * Update last price and last rcvd of material class
     */
    private function updatePriceAndDate($f_code_class)
    {
        $select = new Zend_Db_Select($this->db);
        $res_last = $select->from(array("t1"=>"t_wares"),array("fc_material_last_price","fc_material_last_rcvd_date"))
            ->join(array("t2"=>"t_wares_relations"),"t2.f_code_ware_slave = t1.f_code",[])
            ->where("t2.f_code_ware_master = ?",$f_code_class)->where("t1.f_type_id = 3")
            ->order("fc_material_last_rcvd_date DESC")->limit(1)
            ->query()->fetch();
        if(!empty($res_last)) {
            $this->db->update("t_wares",array(
                "fc_material_last_rcvd_date"=>$res_last['fc_material_last_rcvd_date'],
                "fc_material_last_price"=>$res_last['fc_material_last_price'],
            ),"f_code = {$f_code_class}");
        }
    }
    
        
    /**
     * Script to close purchase order     
     */
    public function completePo()
    {
        $select = new Zend_Db_Select($this->db); 
        //get material information
        $res = $select->from(array("t1"=>"t_pair_cross")) 
            ->join(array("t2"=>"t_wares"),"t1.f_code_cross = t2.f_code",[])
            ->where("f_type_id = 3")->where("f_code_main = ?",$this->f_code)
            ->query()->fetchAll();
        //get general PO info
        $select->reset();
        $po_info = $select->from(array("t1"=>"t_creation_date"),array("f_title","f_description"))
                ->join(array("t2"=>"t_workorders"),"t1.f_id = t2.f_code",array("fc_po_invoice_date","fc_po_invoice_number","fc_po_direct_store"))
                ->where("f_code = ?",$this->f_code)->query()->fetch();
        if(empty($po_info)) return;
        
        $tot = count($res);        
        $time = time();
        $wa = new Mainsim_Model_Wares($this->db);
        $wo = new Mainsim_Model_Workorders($this->db);        
	//foreach($params['t_wares_3']['f_pair'] as $inv_po) {
        for($a = 0;$a < $tot;++$a) {            
            $inv_po = $res[$a];            
            $real_spare_arrived = ($inv_po['fc_po_real_spares_arrived'] - $inv_po['fc_po_spare_returned']) > 0 ? $inv_po['fc_po_real_spares_arrived'] - $inv_po['fc_po_spare_returned']: 0;
            //if there are not material, continue.
            if($real_spare_arrived == 0) continue;
            
            //extract material info
            $select->reset();
            $material_info = $select->from("t_wares",array(
                'fc_material_tot_avbl_qty',
	 	'fc_material_tot_stored_qty',
	 	'fc_material_last_rcvd_qty',
	 	'fc_material_last_rcvd_date',
	 	'fc_material_avg_price',
	 	'fc_material_last_price',
	 	'fc_material_tot_stored_value',
	 	'fc_material_tot_avbl_value',
	 	'fc_material_matclass_cross',
	 	'fc_material_stock_type',
	 	'fc_matclass_code',
	 	'fc_material_standard_price',
		'fc_material_package_type',
	 	'fc_material_package_capacity'                
            ))->where("f_code = ?",$inv_po['f_code_cross'])->query()->fetch();
            
            $params = array(
                'f_type_id'=>3,
                'f_code'=>$inv_po['f_code_cross'],
                'f_module_name'=>'mdl_material_tg',
                'fc_material_last_price'=>$inv_po['fc_po_material_price']
            );
            
            //calculate average cost
            $select->reset();
            $res_history_po = $select->from(array("t1"=>"t_creation_date"),[])
                    ->join(array("t2"=>"t_pair_cross"),"t1.f_id = t2.f_code_main",array("fc_po_real_spares_arrived","fc_po_material_price"))
                    ->join(array("t3"=>"t_wf_phases"),"t1.f_phase_id = t3.f_number and t1.f_wf_id = t3.f_wf_id",[])
                    ->join(array("t4"=>"t_wf_groups"),"t3.f_group_id = t4.f_id",[])
                    ->where("t4.f_id = 6")->where("f_category = 'PURCHASE ORDER'")
                    ->where("t2.f_code_cross = ?",$inv_po['f_code_cross'])->query()->fetchAll();
            $tot_b = count($res_history_po);
            if($tot_b) {
                $tot_mat = 0;
                for($b = 0;$b <$tot_b;++$b) {
                    
                    
                }
            }
            
            if($po_info['fc_po_direct_store'] == 1) { // store directly without movimentation request
                $params = array(
                    'fc_material_tot_avbl_qty'=>$material_info['fc_material_tot_avbl_qty'] + $real_spare_arrived,
                    'fc_material_tot_stored_qty'=>$material_info['fc_material_tot_stored_qty'] + $real_spare_arrived,
                    'fc_material_last_rcvd_qty'=>$real_spare_arrived,
                    'fc_material_last_rcvd_date'=>$inv_po['fc_po_delivery_time'] > 0 ? $inv_po['fc_po_delivery_time']:$time                    
                );
            }
            else { // open movimentation request
                $mov_params = array(
                    'f_title'=>'Movimentation : '.$po_info['f_title'],
                    'f_description'=>$po_info['f_description'],
                    'f_module_name'=>'mdl_mov_tg',
                    'f_type_id'=>16,                    
                    'fc_mov_po_moved_quantity'=>$inv_po['fc_po_real_spares_arrived'],
                    'f_code'=>0,
                    'fc_mov_type'=>1,
                    't_wares_3'=>array(
                        'f_code'=>array( $inv_po['f_code_cross']),
                        'f_pair'=>array(
                            'f_code'=>-1,
                            "f_code_main"=> 0,
                            "f_code_cross"=> $inv_po['f_code_cross'],
                            "fc_mov_moved_quantity"=> $real_spare_arrived,
                            "fc_mov_moved_type"=> "movein"
                        ),
                        'f_code_main'=>0,
                        'f_type'=>3,
                        'Ttable'=>'t_wares',
                        'pairCross'=>0,
                        'f_module_name'=>'mdl_mov_material_tg'
                    )
                );
                $wo->newWorkOrder($mov_params, false);
            }
            //delete every lock for this request to edit material quantity
            $this->db->delete("t_locked","f_code = {$inv_po['f_code_cross']}");
            $this->db->insert("t_locked",array(
                'f_code'=>$inv_po['f_code_cross'],
                'f_user_id'=>$this->userinfo->f_id,
                'f_timestamp'=>time()
            ));
            $res = $wa->editWares($params, false, false);
            $this->db->delete("t_locked","f_code = {$inv_po['f_code_cross']}");
            if(isset($res['message'])) {
                throw new Exception($res['message']);
            }
        }
    }
    
    /**
     *  Convert and sum days,hours and minutes of forewarning in seconds and update row in t_workorders 
     */
    public function fc_pm_forewarning_days()
    {
        $days = empty($this->params['fc_pm_forewarning_days'])?0:$this->params['fc_pm_forewarning_days'];
        $hours = empty($this->params['fc_pm_forewarning_hours'])?0:$this->params['fc_pm_forewarning_hours'];
        $minutes = empty($this->params['fc_pm_forewarning_minutes'])?0:$this->params['fc_pm_forewarning_minutes'];
        $days_in_sec = ((int)$days * 24*60*60);
        $hours_in_sec = ((int)$hours * 60 * 60  );
        $minutes_in_sec = ((int)$minutes * 60);
        $tot = $days_in_sec + $hours_in_sec + $minutes_in_sec;        
        $this->db->update("t_workorders",array('fc_pm_forewarning'=>$tot),"f_code = {$this->f_code}");        
    }
    
    /**
     *  Convert and sum days,hours and minutes of duration in seconds and update row in t_workorders 
     */
    public function fc_pm_duration_days()
    {
        $days = empty($this->params['fc_pm_duration_days'])?0:$this->params['fc_pm_duration_days'];
        $hours = empty($this->params['fc_pm_duration_hours'])?0:$this->params['fc_pm_duration_hours'];
        $minutes = empty($this->params['fc_pm_duration_minutes'])?0:$this->params['fc_pm_duration_minutes'];
        $days_in_sec = ((int)$days * 24*60*60);
        $hours_in_sec = ((int)$hours * 60 * 60  );
        $minutes_in_sec = ((int)$minutes * 60);
        $tot = $days_in_sec + $hours_in_sec + $minutes_in_sec;             
        $this->db->update("t_workorders",array('fc_pm_duration'=>$tot),"f_code = {$this->f_code}");        
    }
    
    /**
     *  Convert and sum hours and minutes of 'at time hour' in seconds and update row in t_workorders 
     */
    public function fc_pm_attime_hours()
    {
        $hours = empty($this->params['fc_pm_attime_hours'])?0:$this->params['fc_pm_attime_hours'];
        $minutes = empty($this->params['fc_pm_attime_minutes'])?0:$this->params['fc_pm_attime_minutes'];
        $hours_in_sec = ((int)$hours * 60 * 60);
        $minutes_in_sec = ((int)$minutes * 60);
        $tot = $hours_in_sec + $minutes_in_sec;                
        $this->db->update("t_workorders",array('fc_pm_at_time'=>$tot),"f_code = {$this->f_code}");        
    }
    
    /**
     * Put last comment in t_creation_date's f_last_comment fields
     */
    public function f_last_comment()
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from('t_pair_cross','fc_comment_description')
            ->where('f_code_main = ?',$this->f_code)->where('f_code_cross = -2')
            ->order('f_id DESC')->limit(1)->query()->fetch();
        $last_comment = null;
        if(!empty($res['fc_comment_description'])) {
            $last_comment = $res['fc_comment_description'];
        }else {return; }        
        $last_comment = $this->userinfo->fc_usr_firstname.' '.$this->userinfo->fc_usr_lastname.' - '.date('d/m/Y \a\t H:i').': '.$last_comment;        
        $this->db->update('t_creation_date',array('f_last_comment'=>$last_comment),"f_id = ".$this->f_code);                
    }    
    
    /**
     * Propagate out of order phase for assets
     * and all fathers if fc_asset_out_order_propagation is setted
     * @return type
     * @throws Exception
     */
    public function fc_wo_out_of_order()
    {
        if(in_array('fc_wo_out_of_order', $this->skipScript))return;
        
        $select = new Zend_Db_Select($this->db);
        $resOOO = $select->from('t_workorders',array('fc_wo_out_of_order'))
            ->where("f_code = ?",$this->f_code)->query()->fetch();                        
        $select->reset();        
        $resAsset = $select->from(array('t1'=>'t_ware_wo'),[])
            ->join(array('t2'=>'t_wares'),'t1.f_ware_id = t2.f_code',array('f_code','fc_asset_out_order_propagation','fc_asset_no_out_of_order'))
            ->join(array('t3'=>'t_creation_date'),'t2.f_code = t3.f_id',array('f_phase_id'))
            ->where("t2.f_type_id = 1")->where('t1.f_wo_id = ?',$this->f_code)
            ->query()->fetchAll();            
        if(!empty($this->olds['old_ware_wo_cross' ]) && ($resOOO['fc_wo_out_of_order'] == 1 &&
                !empty($this->olds['old_table']['fc_wo_out_of_order']))){ 
            //if flagged, check if any asset has been removed (not allow)
            $waresList = array_map(function($el) { return $el['f_ware_id']; },$this->olds['old_ware_wo_cross' ]);
            $select->reset();
            $resOldAsset = $select->from("t_wares","f_code")->where("f_code in (?)",$waresList)
                ->where("f_type_id = 1")->query()->fetchAll();
            $oldAssets = array_map(function($el) { return $el['f_code']; },$resOldAsset);                        
            $newAssets = array_map(function($el) { return $el['f_code']; },$resAsset);            
            $checkAsset = array_intersect($oldAssets, $newAssets);
            if(count($checkAsset) != count($oldAssets)) {
                throw new Exception("You cannot remove asset if Out of order is flagged.");
            }
        }
        $tot = count($resAsset);         
        $waParams = array(
            'f_type_id'=>1,
            'f_module_name'=>'mdl_asset_tg'
        );
        
        for($a = 0;$a < $tot;++$a) {
            $f_code_asset = $resAsset[$a]['f_code'];            
            $waParams['f_code'] = $f_code_asset;
            $outOfOrder = (int)$resAsset[$a]['fc_asset_no_out_of_order'];            
            $waParams['fc_asset_no_out_of_order'] = $resOOO['fc_wo_out_of_order'] == 1?
                ($outOfOrder + 1) :($outOfOrder - 1);
            
            $waParams['f_phase_id'] = $waParams['fc_asset_no_out_of_order'] <= 0?1:3;                          
            if($waParams['f_phase_id'] == $resAsset[$a]['f_phase_id']) {//if phase in waParams is the same of asset,skip this edit                
                continue;
            }                       
            
            $resChange = $this->editWares($waParams, false, false);                        
            if(isset($resChange['message'])) {
                throw new Exception($resChange['message']);
            }            
            
            if($resAsset[$a]['fc_asset_out_order_propagation'] != 1) {
                continue;
            }   
            $select->reset();
            $resParent = $select->from(array('t1'=>'t_wares'),[])
                ->join(array('t2'=>'t_wares_parent'),'t1.f_code = t2.f_code',array('f_code'=>'f_parent_code'))
                ->join(array('t3'=>'t_wares'),'t2.f_parent_code = t3.f_code',array('fc_asset_no_out_of_order'))
                ->where('t2.f_code = ?',$f_code_asset)->where('t1.fc_asset_out_order_propagation = 1')
                ->query()->fetchAll();            
            do {                
                if(empty($resParent)) {
                    break;
                }                                
                $totB = count($resParent);
                $parentCodes = [];
                for($b = 0;$b < $totB;++$b) {
                    $waParams['f_code'] = $resParent[$b]['f_code'];
                    $parentCodes[] = $resParent[$b]['f_code'];                    
                    $outOfOrder = (int)$resParent[$b]['fc_asset_no_out_of_order'];            
                    $waParams['fc_asset_no_out_of_order'] = $resOOO['fc_wo_out_of_order'] == 1?
                        $outOfOrder + 1 :$outOfOrder - 1;
                    $waParams['f_phase_id'] = $waParams['fc_asset_no_out_of_order'] == 0?1:3;                    
                    $resChange = $this->editWares($waParams, false, false);
                    if(isset($resChange['message'])) {
                        throw new Exception($resChange['message']);
                    }                    
                }
                $select->reset(Zend_Db_Select::WHERE);
                $resParent = $select->where('t2.f_code IN (?)',$parentCodes)
                    ->where('t1.fc_asset_out_order_propagation = 1')->query()->fetchAll();
            }while(true);
            
        }        
    }
    
    /**
     * Edit wares 
     * @param array $params
     * @param bool $isBatch if true is an editBatch
     * @param bool $commit if true, system commit
     */
    private function editWares($params,$isBatch = false,$commit = false)
    {
        $wa = new Mainsim_Model_Wares($this->db);
        $this->db->insert('t_locked',array(
            'f_code'=>$params['f_code'],
            'f_user_id'=>$this->userinfo->f_id,
            'f_timestamp'=>time()
        ));
        $res = $wa->editWares($params, $isBatch, $commit);
        $this->db->delete('t_locked',"f_code = {$params['f_code']} and f_user_id = {$this->userinfo->f_id}");
        return $res;
    }
    
    public function f_priority()
    {
        if(in_array('f_priority', $this->skipScript))return;
        
        if($this->table == 't_workorders' && in_array($this->params['f_type_id'],array(1,12))             
                && empty($this->params['f_start_date']) && !empty($this->params['f_priority'])  ) {
            $select = new Zend_Db_Select($this->db);
            $res = $select->from('t_wares',array('fc_action_priority_taking_charge'))
                ->where('f_type_id = 11')->where('fc_action_priority_value = ?',$this->params['f_priority'])
                ->query()->fetch();
            if(empty($res['fc_action_priority_taking_charge'])) {
                return;
            }
            $taking_charge = $res['fc_action_priority_taking_charge'] * 3600;
            $delay = 0;            
            $time = time() + $taking_charge;
            if(date('N',$time) > 5){
                $delay = date('N',$time) == 6?172800:86400;
            }
            $time+=$delay;
            $this->db->update('t_workorders',array('f_start_date'=>$time),"f_code = {$this->f_code}");            
        }
    }
    
    /*
     * calculate assets costs related to workorder closed
     * 
     * $params : workorders data
     */
    private function calculateCosts(/*$params $f_code_wo*/) 
    {        
        $select = new Zend_Db_Select($this->db);
        $params = $select->from('t_custom_fields',array(
                'fc_costs_for_downtime','fc_costs_for_material','fc_costs_for_labor',
                'fc_costs_for_tool','fc_costs_for_service','fc_total_costs'
            ))->where('f_code = ?',$this->f_code)->query()->fetch();        
        $select->reset();
        $resultAssets = $select->distinct()->from(array('wawo' => 't_ware_wo'), array('f_ware_id'))
            ->join(array('wa' => 't_wares'), 'wa.f_code = wawo.f_ware_id', [])
            ->where('wawo.f_wo_id = ?', $this->f_code)->where('wa.f_type_id = 1')
            ->query()->fetchAll();
        // loop assets        
        $assetsNumber = count($resultAssets); 
        $tot = count($resultAssets);
        $timestamp = time();
        for($i = 0; $i < $tot; $i++){            
            // get current t_custom_fields row 
            $select->reset();
            $resultCurrentWare = $selectCurrentWare = $select->from('t_wares', array('fc_asset_direct_costs_ytd', 'fc_asset_indirect_costs_ytd', 'fc_asset_budget_ytd', 'fc_asset_total_costs_ytd', 'fc_asset_maintenance_costs','fc_asset_replacement_costs'))
              ->join('t_custom_fields', 't_wares.f_code = t_custom_fields.f_code', array('fc_costs_for_downtime', 'fc_costs_for_material', 'fc_costs_for_labor', 'fc_costs_for_tool', 'fc_costs_for_service'))
              ->where('t_wares.f_code = ?', $resultAssets[$i]['f_ware_id'])->query()->fetch();            
            // insert new t_custom_fields row
            $newWareData = array(
                'f_timestamp'=>$timestamp,
                'fc_asset_direct_costs_ytd' => $resultCurrentWare['fc_asset_direct_costs_ytd'], 
                'fc_asset_indirect_costs_ytd' => $resultCurrentWare['fc_asset_indirect_costs_ytd'], 
                'fc_asset_budget_ytd' => $resultCurrentWare['fc_asset_budget_ytd'],
                'fc_asset_total_costs_ytd' => $resultCurrentWare['fc_asset_total_costs_ytd'],
                'fc_asset_maintenance_costs' => $resultCurrentWare['fc_asset_maintenance_costs'],
                'fc_asset_replacement_costs' => $resultCurrentWare['fc_asset_replacement_costs']
            );
            $newCustomWareData = array(
                'f_timestamp'=>$timestamp,
                'fc_costs_for_downtime' => $resultCurrentWare['fc_costs_for_downtime'], 
                'fc_costs_for_material' => $resultCurrentWare['fc_costs_for_material'], 
                'fc_costs_for_labor' => $resultCurrentWare['fc_costs_for_labor'],
                'fc_costs_for_tool' => $resultCurrentWare['fc_costs_for_tool'], 
                'fc_costs_for_service' => $resultCurrentWare['fc_costs_for_service']
            );            
            $newCustomWareData['fc_costs_for_downtime'] += $params['fc_costs_for_downtime'] / $assetsNumber;
            // if a day someone will tell me about a material problem cost ask baldazzo 30-05-2014
            $newCustomWareData['fc_costs_for_material'] += $params['fc_costs_for_material'] / $assetsNumber;
            $newCustomWareData['fc_costs_for_labor'] += $params['fc_costs_for_labor'] / $assetsNumber;
            $newCustomWareData['fc_costs_for_tool'] += $params['fc_costs_for_tool'] / $assetsNumber;
            $newCustomWareData['fc_costs_for_service'] += $params['fc_costs_for_service'] / $assetsNumber;
            //$newWareData['f_timestamp'] = $timestamp;
            $newWareData['fc_asset_direct_costs_ytd'] += $params['fc_total_costs'] / $assetsNumber;
            $newWareData['fc_asset_total_costs_ytd'] = $newWareData['fc_asset_direct_costs_ytd'] + $resultCurrentWare['fc_asset_indirect_costs_ytd'];
            $newWareData['fc_asset_maintenance_costs'] += $newWareData['fc_asset_total_costs_ytd'] - $resultCurrentWare['fc_asset_total_costs_ytd'];
            $newWareData['fc_asset_balance'] = $resultCurrentWare['fc_asset_budget_ytd'] - $newWareData['fc_asset_total_costs_ytd'];
            $newWareData['fc_asset_m_cost_on_rep_value'] = $newWareData['fc_asset_replacement_costs'] > 0 ? round($newWareData['fc_asset_maintenance_costs'] / $newWareData['fc_asset_replacement_costs'], 2) : '';
            // make history only if there are active costs for the workorder
            if($params['fc_total_costs'] > 0){
                $this->db->update('t_custom_fields', $newCustomWareData, 'f_code = ' . $resultAssets[$i]['f_ware_id']);
                $this->db->update('t_wares', $newWareData, 'f_code = ' . $resultAssets[$i]['f_ware_id']);
                $this->db->update('t_creation_date', array('f_timestamp' => $timestamp), 'f_id = ' . $resultAssets[$i]['f_ware_id']);               
                // update indirect parent costs                
                $visitedArray = [];
                $this->updateParentsCosts($resultAssets[$i]['f_ware_id'], $params['fc_total_costs'] / $assetsNumber,$visitedArray);
            }
        }        
    }
        
    private function updateParentsCosts($f_code, $cost, &$visitedArray){
        
        // get parents
        $selectParents = $this->db->select()
            ->from('t_wares_parent', array('f_parent_code'))
            ->join('t_wares', 't_wares_parent.f_parent_code = t_wares.f_code', 
                array('fc_asset_replacement_costs', 'fc_asset_maintenance_costs','fc_asset_indirect_costs_ytd',
                'fc_asset_total_costs_ytd','fc_asset_budget_ytd'))
            ->where('t_wares_parent.f_code = ?', $f_code);
        $resultParents = $this->db->query($selectParents)->fetchAll();
        $timestamp = time();
        // loop parents
        $totParents = count($resultParents);
        for($i = 0; $i < $totParents; $i++){            
            // this parent has not yet visited
            if(!in_array($resultParents[$i]['f_parent_code'], $visitedArray)){
                foreach($resultParents[$i] as &$line) {
                    $line = is_null($line)?0:$line;
                }
                // insert into visited list
                $visitedArray[] = $resultParents[$i]['f_parent_code'];
                // update costs
                $data['fc_asset_indirect_costs_ytd'] = $resultParents[$i]['fc_asset_indirect_costs_ytd'] + ($cost/$totParents);
                $data['fc_asset_total_costs_ytd'] = $resultParents[$i]['fc_asset_total_costs_ytd']+ ($cost/$totParents);
                $data['fc_asset_balance'] = $resultParents[$i]['fc_asset_budget_ytd'] - $resultParents[$i]['fc_asset_total_costs_ytd'];
                $data['fc_asset_maintenance_costs'] =  $resultParents[$i]['fc_asset_maintenance_costs'] + ($cost/$totParents);
                $data['fc_asset_m_cost_on_rep_value'] = $resultParents[$i]['fc_asset_replacement_costs'] > 0 ? round($data['fc_asset_maintenance_costs'] / $resultParents[$i]['fc_asset_replacement_costs'], 2) : '';
                $this->db->update('t_wares', $data, 'f_code = ' . $resultParents[$i]['f_parent_code']);
                Mainsim_Model_Utilities::newRecord($this->db,$resultParents[$i]['f_parent_code'],'t_creation_date', array('f_timestamp' => $timestamp +1),"Update indirect costs");
                Mainsim_Model_Utilities::newRecord($this->db,$resultParents[$i]['f_parent_code'],'t_wares', array('f_timestamp' => $timestamp + 1),"Update indirect costs");
                Mainsim_Model_Utilities::newRecord($this->db,$resultParents[$i]['f_parent_code'],'t_custom_fields', array('f_timestamp' => $timestamp + 1),"Update indirect costs");                
                // update parents cost
                $this->updateParentsCosts($resultParents[$i]['f_parent_code'], $cost, $visitedArray);  
            }
        }
    }
    
    /**
     * Propagate change phase from wo to 
     * his children (if possible)
     */
    public function changePhasePropagator()
    {
        $children = [];        
        $mObj = new Mainsim_Model_MainsimObject($this->db);
        $mObj->setTable($this->table);
        $children = Mainsim_Model_Utilities::getChildCode($this->table.'_parent', $this->f_code, $children, $this->db);        
        if(!empty($children)) {
            $res = $mObj->changePhase(implode(',',$children), $this->params['f_wf_id'], $this->params['f_phase_id'], $this->params['f_module_name'],false);
            if(isset($res['message'])) {
                throw new Exception($res['message']);
            }
        }
    }
    
    /**
     * Create new installation from mainsim host
     */
    public function fc_prj_url()
    {
        if($this->params['f_type_id'] == 8 && $this->params['f_code'] == 0 ) {                                    
            $this->validatePrjData();
            //get db params
            $dbParams = $this->getDbParams($this->params['fc_prj_host'],$this->params['fc_prj_username'],$this->params['fc_prj_password']);
            
            /** create database and populate */
            $this->createDatabase($dbParams,$this->params['fc_prj_dbname']);
            
            /** set attachment folder */    
            $projectPath = $this->createFolderProject($this->params['fc_prj_attachment_path'],$this->params['f_title']);
            
            /** create dns */
            $this->createDns($this->params['fc_prj_url']);            
            /** create cron project VEDERE REACT PHP*/
            //$this->createCron($this->params['fc_prj_url']);
            
            $customization = !empty($this->params['fc_prj_customization'])?$this->params['fc_prj_customization']:'';
            $template = !empty($this->params['fc_prj_template'])?$this->params['fc_prj_template']:'';
            
            //customize supervisor name with project name 
            $supervisorName = substr($this->params['fc_prj_url'],0, strpos($this->params['fc_prj_url'],".mainsim"));
            $dbConn = Zend_Db::factory($this->params['fc_prj_adapter'],array(
                'host'=>$dbParams['DEFAULT_DB_HOST'],
                'dbname'=>$this->params['fc_prj_dbname'],
                'username'=>$dbParams['DEFAULT_DB_USERNAME'],
                'password'=>$dbParams['DEFAULT_DB_PASSWORD'],
            ));
            $dbConn->update("t_users",['fc_usr_usn'=>"{$supervisorName}.supervisor"],"fc_usr_usn like '%supervisor'");
            $this->manageSVLevel($this->params['fc_prj_installation_type'],$this->params['fc_prj_adapter'],[
                'dbname'=>$this->params['fc_prj_dbname'],'host'=>$dbParams['DEFAULT_DB_HOST'],
                'username'=>$dbParams['DEFAULT_DB_USERNAME'],'password'=>$dbParams['DEFAULT_DB_PASSWORD']]);
                    
            /** write record in database list */            
            $this->db->insert("database_lists",array(
                'f_code'=>$this->f_code,
                'f_url'=>$this->params['fc_prj_url'],
                'f_adapter'=>$this->params['fc_prj_adapter'],
                'f_dbname'=>$this->params['fc_prj_dbname'],
                'f_host'=>$dbParams['DEFAULT_DB_HOST'],
                'f_username'=>$dbParams['DEFAULT_DB_USERNAME'],
                'f_password'=>$dbParams['DEFAULT_DB_PASSWORD'],
                'f_attachment_path'=>$projectPath,
                'f_customization'=>$customization,
                'f_template'=>$template,
                'f_installation_type'=>$this->params['fc_prj_installation_type']
            ));
        }      
        elseif($this->params['f_type_id'] == 8){ //update data
             $this->validatePrjData('edit');             
            //get db params
            $dbParams = $this->getDbParams($this->params['fc_prj_host'],$this->params['fc_prj_username'],$this->params['fc_prj_password']);
            $this->manageSVLevel($this->params['fc_prj_installation_type'],$this->params['fc_prj_adapter'],[
                'dbname'=>$this->params['fc_prj_dbname'],'host'=>$dbParams['DEFAULT_DB_HOST'],
                'username'=>$dbParams['DEFAULT_DB_USERNAME'],'password'=>$dbParams['DEFAULT_DB_PASSWORD']]);
            $customization = !empty($this->params['fc_prj_customization'])?$this->params['fc_prj_customization']:'';
            $template = !empty($this->params['fc_prj_template'])?$this->params['fc_prj_template']:'';
            $this->db->update("database_lists",array(                
                'f_url'=>$this->params['fc_prj_url'],
                'f_adapter'=>$this->params['fc_prj_adapter'],
                'f_dbname'=>$this->params['fc_prj_dbname'],
                'f_host'=>$dbParams['DEFAULT_DB_HOST'],
                'f_username'=>$dbParams['DEFAULT_DB_USERNAME'],
                'f_password'=>$dbParams['DEFAULT_DB_PASSWORD'],
                'f_attachment_path'=>$projectPath,
                'f_customization'=>$customization,
                'f_template'=>$template,
                'f_installation_type'=>$this->params['fc_prj_installation_type']
            ),"f_code = {$this->f_code}");
        }
    }
    
    /**
     * Set level to supervisor checking installation type
     * @param string $type installation type
     */
    private function manageSVLevel($type,$adapter,$params)
    {
        $dbConn = Zend_Db::factory($adapter,$params);
        $select = new Zend_Db_Select($dbConn);
        $resUser = $select->from("t_users",['f_code'])->where("fc_usr_usn like '%supervisor'")
            ->query()->fetch();
        $levels = [
            'free'=>['lvl'=>128,'name'=>'Supervisor FREE'],
            'pro'=>['lvl'=>64,'name'=>'Supervisor PRO'],
            'enterprise'=>['lvl'=>4,'name'=>'Supervisor'],
        ];        
        if($resUser['fc_usr_level_text'] == $levels[$type]['name']){ return; }
        $select->reset();
        $resLvlCode = $select->from("t_creation_date","f_id")
            ->where("f_title = ?",$levels[$type]['name'])->where("f_category = 'LEVEL'")->query()->fetch();
        $dbConn->delete("t_wares_systems","f_ware_id = {$resUser['f_code']} and f_system_id IN (select f_code from t_systems where f_type_id = 4)");
        $dbConn->insert("t_wares_systems",['f_ware_id'=>$resUser['f_code'],'f_system_id'=>$resLvlCode['f_id'],'f_timestamp'=>time()]);
        $dbConn->update("t_users",['fc_usr_level_text'=>$levels[$type]['name'],'fc_usr_level'=>$levels[$type]['lvl']],"fc_usr_usn like '%supervisor'");
    }
    
    /**
     * Validate new project data (url and db name)
     */
    private function validatePrjData($type = 'new')
    {
        $checkNum = $type == 'new'?0:1;
        /* check if fc_prj_dbname is correct */
        if(preg_match('/[^A-Za-z0-9\-\_]/',$this->params['fc_prj_dbname'])) {
            throw new Exception("Invalid database name. Special chars and space are not allow.");
        }
        $resCheck = $this->db->query("select count(*) as num from database_lists where f_url = '{$this->params['fc_prj_url']}'")->fetch();
        if($resCheck['num'] > $checkNum) { throw new Exception("Url {$this->params['fc_prj_url']} already exists."); }


        /** check if fc_prj_url is valid*/
        $parts = parse_url($this->params['fc_prj_url']);
        $checkUrl = $this->params['fc_prj_url'];
        if(!isset($parts['scheme'])) {
            $checkUrl = 'http://'.$checkUrl;
        }
        if(!filter_var($checkUrl,FILTER_VALIDATE_URL)) {
            throw new Exception('Invalid URL');
        }
    }
    
    /**
     * Create path where mainsim save attachments
     * @param string $mainPath main path where create folder
     * @param string $folderName name of new folder
     * @throws Exception
     */
    private function createFolderProject($mainPath,$folderName)
    {
        $folders = array(
            'temp','doc','img',
            'data manager','data manager/importer',
            'data manager/exporter','data manager/module importer'
        );        
        if(!$mainPath) {
            $mainPath = Zend_Registry::get('attachmentsFolder');
        }
        $projectPath = realpath($mainPath.'/'.$folderName);
        if(!$projectPath) {
            mkdir($mainPath.'/'.$folderName);
            $projectPath = realpath($mainPath.'/'.$folderName);
        }
        $this->db->update("t_systems",['fc_prj_attachment_path'=>$projectPath],"f_code = {$this->f_code}");
        $totF = count($folders);
        for($i = 0;$i < $totF;++$i) {
            if(!is_dir($projectPath.'/'.$folders[$i])) {
                mkdir($projectPath.'/'.$folders[$i]);
            }
        }        
        return $projectPath;
    }
    
    /**
     * Create new database for new project if database does not exists
     * WARNING: it works only with MySQL
     * @param string $dbname database name
     * @throws Exception if database params are incorrect
     */
    private function createDatabase($dbParams,$dbname)
    {
        //get database params
        $select = new Zend_Db_Select($this->db);                
        $this->db->update("t_systems",array('fc_prj_host'=>$dbParams['DEFAULT_DB_HOST'],
            'fc_prj_username'=>$dbParams['DEFAULT_DB_USERNAME'],'fc_prj_password'=>$dbParams['DEFAULT_DB_PASSWORD']),
            "f_code = {$this->f_code}");        
        $command = 'C:\wamp\bin\mysql\mysql5.6.17\bin\mysql.exe -h '.$dbParams['DEFAULT_DB_HOST'].' -u '.$dbParams['DEFAULT_DB_USERNAME'].' -p'.$dbParams['DEFAULT_DB_PASSWORD'];
        $show = $command.' -e "show create database `'.$dbname.'`"';    	        
        $res = shell_exec($show);            
        if(is_null($res) || strpos($res,"Unknown database") !== false){            
            $select->reset();
            $resDbType = $select->from('t_systems',array('fc_prj_installation_type'))->where('f_code = ?',$this->f_code)
                ->query()->fetch();
            $sourceDb = 'mainsim3.sql';            
            $library = realpath(APPLICATION_PATH.'/../library').DIRECTORY_SEPARATOR.'Mainsim'.DIRECTORY_SEPARATOR.$sourceDb;     
            $library = strpos($library, " ") !== false ? '"'.$library.'"':$library;
            $resCreate = shell_exec($command.' -e "create database if not exists `'.$dbname.'`"');            
            if(!is_null($resCreate)) {
                throw new Exception($resCreate);
            }                       
            $resPopulate = shell_exec($command." $dbname < $library ");   				
            /** set tabbar for specific project */
            $this->fc_prj_installation_type();
            if(!is_null($resPopulate)) {
                throw new Exception($resPopulate);
            }
        }
    }
    
    private function getDbParams($host,$user,$pass)
    {
        $select = new Zend_Db_Select($this->db);
        if($host == 'default'){
            $resDbParams = $select->from("t_creation_date",array('f_title','f_description'))
                ->where("f_title IN ('DEFAULT_DB_HOST','DEFAULT_DB_USERNAME','DEFAULT_DB_PASSWORD')")                
                ->query()->fetchAll();            
        }
        else {
            $resDbParams = array(array('f_title'=>'DEFAULT_DB_HOST','f_description'=>$host),
                array('f_title'=>'DEFAULT_DB_USERNAME','f_description'=>$user),array('f_title'=>'DEFAULT_DB_PASSWORD','f_description'=>$pass));
        }        
        $dbParams = [];
        array_walk($resDbParams, function($el) use(&$dbParams){
            $dbParams[$el['f_title']] = empty($el['f_description'])?'':$el['f_description'];
        });
        return $dbParams;
    }
    
    /**
     * Set mainsim type changing tabbar
     */
    public function fc_prj_installation_type()
    {
        return;
        if($this->params['f_type_id'] == 8){
            $select = new Zend_Db_Select($this->db);
            $configFiles = array('free','pro','enterprise');
            $resDb = $select->from("t_systems",array(
                'fc_prj_adapter','fc_prj_host','fc_prj_dbname','fc_prj_username','fc_prj_password','fc_prj_installation_type'
            ))->where("f_code = ?",$this->f_code)->query()->fetch();
            if($resDb['fc_prj_installation_type'] == 'demo'){return;}         
            $dbConn = Zend_Db::factory($resDb['fc_prj_adapter'],array(
                'host'=>$resDb['fc_prj_host'],
                'dbname'=>$resDb['fc_prj_dbname'],
                'username'=>$resDb['fc_prj_username'],
                'password'=>$resDb['fc_prj_password'],
            ));
            $file = 'config_'.$resDb['fc_prj_installation_type'].'.sql';
            if(!in_array($resDb['fc_prj_installation_type'],$configFiles)){ throw new Exception($this->trsl->_("Unknown configuration")." ".$resDb['fc_prj_installation_type']);}
            $query = file_get_contents(APPLICATION_PATH.'/../library/Mainsim/'.$file);        
            $dbConn->delete("t_ui_object_instances","f_instance_name like 'tbbr_%'");
            $dbConn->query($query);
        }
    }
    
    /**
     * Create cron for linux or task scheduler for windows OR for church
     
    private function createCron($url)
    {
        if(strpos(PHP_OS,'WIN') === false) {// cron for linux
            $cron = shell_exec("crontab -l");
            $cronExp = explode(PHP_EOL, $cron);
            $totC = count($cronExp);
            $found = false;    
            for($c = 0;$c < $totC;++$c) {
                if(preg_match("@{$url}@",$cronExp[$c])) {
                    $found = true;
                    break;
                }
            }
            if(!$found) {
                $cron.='* * * * * php /var/www/test/mainsim3_multi/crontab.php "'.$url.'"';
            }    
            file_put_contents('/tmp/crontab.txt', $cron.PHP_EOL);
            exec('crontab /tmp/crontab.txt');
        }
        else{ // task scheduler for windows
            
        }
    }*/
    
    /**
     * Create dns for new installation
     * @param string $url
     */
    private function createDns($url)
    {
        $select = new Zend_Db_Select($this->db);
        $mail = new Mainsim_Model_Mail($this->db);        
        $resMailParts = $select->from("t_creation_date",['f_title','f_description'])
            ->where("f_category = 'SETTING'")->where("f_title IN ('IP_MULTI_INSTALLATION','IT_MAIL','DNS_MAIL_BODY')")
            ->query()->fetchAll();
        $mailParams = [];
        array_walk($resMailParts, function($el) use(&$mailParams){
            $mailParams[$el['f_title']] = empty($el['f_description'])?'':$el['f_description'];
        });        
        $sendTo = explode('|',$mailParams['IT_MAIL']);
        if(empty($mailParams['IP_MULTI_INSTALLATION']) || empty($mailParams['IT_MAIL'])) return;
        $to = ['To'=>$sendTo,'Cc'=>['quintino@mainsim.com',$this->userinfo->fc_usr_mail]];        
        $subject = "DNS request application from mainsim";
        $body = "DNS:".PHP_EOL."{$url}:{$mailParams['IP_MULTI_INSTALLATION']}";        
        //$mail->sendMail($to, $subject, $body,[],['content-type'=>'text/plain']); [TENERE COMMENTO SOLO PER TEST]
    }
    
    public function fc_wo_task_sequence()
    {
        if($this->params['f_code'] != 0){
            return;
        }
        $select  = new Zend_Db_Select($this->db);
        $res = $select->from(array('t1'=>'t_pair_cross'),[])
            ->join(array('t2'=>'t_wares'),'t1.f_code_cross = t2.f_code',array('fc_task_sequence'))
            ->where("f_code_main = ?",$this->f_code)->where("f_type_id = 10")
            ->order('fc_task_sequence asc')->group("fc_task_sequence")->query()->fetchAll();        
        $tasks = array_map(function($el){return $el['fc_task_sequence'];},$res);
        if(!empty($tasks)){
            $this->db->update("t_workorders",array('fc_wo_task_sequence'=>implode(',',$tasks)),"f_code = {$this->f_code}");
        }
    }    
    
    /**
     * Add wares (default: contracts and documents)
     * linked in assetto wo
     */
    //private function attachDocFromAsset()
    private function linkWaresFromAsset()
    {
        if(in_array('attachDocFromAsset', $this->skipScript))return;
        if(in_array('linkWaresFromAsset', $this->skipScript))return;
        
        $select = new Zend_Db_Select($this->db); 
        
        $setting_lwfa = $select->from(["t_creation_date"], ["f_title", "f_description"])
            ->where("f_category = 'SETTING' AND f_title = 'LINK_WARES_FROM_ASSET'")
            ->where("f_phase_id = 1")
            ->query()->fetch();
        $select->reset();
        
        //controllo che esista il setting e che 'f_description' sia valorizzato come json
        if(!empty($setting_lwfa) && !empty($setting_lwfa['f_description']) && !is_null(json_decode($setting_lwfa['f_description'])))
        {
            $lwfa = json_decode($setting_lwfa['f_description'],1);
            
            /*ESEMPIO DI JSON VALIDO
             *[{"f_type_id":5,"fc_doc_type":["contract","manual"],"fc_doc_tag":["#666"]},{"f_type_id":7,"fc_contract_type":["Warranty","Service","Purchase"]}]
             */
            
            $t_wares_columns = Mainsim_Model_Utilities::get_tab_cols("t_wares");
            $where_by_types = [];
            
            if(is_array($lwfa) && !empty($lwfa)){
                //accetto solo tipi integer
                $lwfa = array_filter($lwfa, function($val) {
                    $f_type_id = isset($val["f_type_id"])?$val["f_type_id"]:false;
                    $flag1 = !empty($f_type_id) && is_numeric($f_type_id);
                    $flag2 = $f_type_id > 0 && $f_type_id == round($f_type_id);

                    return ($flag1 && $flag2); 
                });


                foreach($lwfa as $val){
                        $tipo = $val["f_type_id"];
                        unset($val["f_type_id"]);
                        $where_by_types[$tipo] = "t3.f_type_id_slave = ".$tipo;
                        $colums = array_keys($val);
                        if(empty(array_diff($colums,$t_wares_columns))){ //verifico che i binds siano colonne di 't_wares'
                                foreach($colums as $col){
                                        $values = $val[$col];
                                        if(!empty($values)){
                                                if(!is_array($values)) $values = array($values);
                                                $values = "('".implode("','",$values)."')";
                                                $where_by_types[$tipo].= " AND t4." .$col. " IN ". $values;
                                        }
                                }
                        }
                }

            }
            
            // Default: se il setting non è valorizzato, o non è valorizzato correttamente
            if(empty($where_by_types)){
                $where_by_types['5'] = "t3.f_type_id_slave = 5";
                $where_by_types['7'] = "t3.f_type_id_slave = 7";
            }
            
            $where = "(".implode(") OR (",array_values($where_by_types)).")";
            
            if(!empty($where)){
                    $select->reset();
                    $resDoc = $select->from(array("t1"=>"t_ware_wo"),[])
                            ->join(array("t2"=>"t_wares"),"t1.f_ware_id = t2.f_code",[])
                            ->join(array("t3"=>"t_wares_relations"),"t2.f_code = t3.f_code_ware_master",array('f_code_ware_slave'))
                            ->join(array("t4"=>"t_wares"),"t4.f_code = t3.f_code_ware_slave",[])   
                            ->where("t2.f_type_id = 1")->where("f_wo_id = ?",$this->f_code)
                            ->where($where)
                            ->where("t3.f_code_ware_slave not in (select f_ware_id from t_ware_wo where f_wo_id = ?)",$this->f_code)
                            ->query()->fetchAll();
            }
        }        
        else
        {
            $include = [];
            $setting = $select->from(["t_creation_date"], ["f_title", "f_description"])
                                ->where("f_category = 'SETTING' AND f_title = 'WARES_TO_INCLUDE'")
                                ->where("f_phase_id = 1")
                                ->query()->fetch();
            if (empty($setting)) {
                // Default: se non ho il setting non metto niente
                $include[] = '5';
                $include[] = '7';
            } else {
                if (empty($setting['f_description'])) {
                    // Se ho il setting ma è vuoto
                    $include[] = '-1';
                } else {
                    $tipi = explode(',', $setting['f_description']);
                    foreach ($tipi as $tipo) {
                        $include[] = $tipo;
                    }
                }
            }

            $select->reset();
            $resDoc = $select->from(array("t1"=>"t_ware_wo"),[])
                ->join(array("t2"=>"t_wares"),"t1.f_ware_id = t2.f_code",[])
                ->join(array("t3"=>"t_wares_relations"),"t2.f_code = t3.f_code_ware_master",array('f_code_ware_slave'))                
                ->where("t2.f_type_id = 1")->where("f_wo_id = ?",$this->f_code)
                ->where("t3.f_type_id_slave IN (?)",$include)
                ->where("t3.f_code_ware_slave not in (select f_ware_id from t_ware_wo where f_wo_id = ?)",$this->f_code)
                ->query()->fetchAll();
        }
        
        
        $codes_docs = [];
        foreach($resDoc as $kkey=>$vvalue) {
            $codes_docs[] = $vvalue['f_code_ware_slave'];
        }
        $resDoc = array_unique($codes_docs);
        //die(var_dump($params));
        foreach ($resDoc as $kkey=>$vvalue) {
            $this->db->insert("t_ware_wo",array(
                "f_ware_id"=>$vvalue,
                "f_wo_id"=>$this->f_code,
                "f_timestamp"=>time()
            ));
        }
        
        //print('attachDocFromAsset');
        $this ->fc_documents();
    }

	//FUNZIONE CHE DATA UNA DATA E UN NUMERO DI GIORNI LAVORATIVI CALCOLA L'ULTIMO GIORNO LEVANDO LE FESTIVITA'// 
	function workday($tsData,$lavorativi,$timestamp){
		// Calcolo del giorno di Pasqua fino all'ultimo anno valido
		for ($i=2006; $i<=2037; $i++)
		{
			$pasqua = date("Y-m-d", easter_date($i));
			$array_pasqua[] = $pasqua;
		}

		// Calcolo le rispettive pasquette
		foreach($array_pasqua as $pasqua)
		{
			list ($anno,$mese,$giorno) = explode("-",$pasqua);
			$pasquetta = mktime (0,0,0,date($mese),date($giorno)+1,date($anno));
			$array_pasquetta[] = $pasquetta;
		}

		// questi giorni son sempre festivi a prescindere dall'anno
		$giorniFestivi = array("01-01",
			"01-06",
			"04-25",
			"05-01",
			"06-02",
			"08-15",
			"11-01",
			"12-08",
			"12-25",
			"12-26",
		);

		$i = 0;
		while ($i<=$lavorativi)
		{
			$giorno_data = date("w",$tsData); //verifico il giorno: da 0 (dom) a 6 (sab)
			$mese_giorno = date('m-d',$tsData); // confronto con gg sempre festivi
			// Infine verifico che il giorno non sia sabato,domenica,festivo fisso o festivo variabile (pasquetta);	
			if ($giorno_data !=0 && $giorno_data != 6 && !in_array($mese_giorno,$giorniFestivi) && !in_array($tsData,$array_pasquetta) )
			{
				$i++;
			}
			$tsData=$tsData+86400;
		}
		$tsData=$tsData-86400;
		if($timestamp) $giorno_conclusivo = $tsData;
		else $giorno_conclusivo = date('d-m-Y H:i',$tsData);
		return $giorno_conclusivo;
	}
	
        //Funzione che date due date calcola i giorni feriali tra di esse
	function get_days_interval($data_inizio, $data_fine){
		$giorni_effettivi = ($data_fine - $data_inizio)/86400;
		
		// Calcolo del giorno di Pasqua fino all'ultimo anno valido
		for ($i=2006; $i<=2037; $i++)
		{
			$pasqua = date("Y-m-d", easter_date($i));
			$array_pasqua[] = $pasqua;
		}

		// Calcolo le rispettive pasquette
		foreach($array_pasqua as $pasqua)
		{
			list ($anno,$mese,$giorno) = explode("-",$pasqua);
			$pasquetta = mktime (0,0,0,date($mese),date($giorno)+1,date($anno));
			$array_pasquetta[] = $pasquetta;
		}

		// questi giorni son sempre festivi a prescindere dall'anno
		$giorniFestivi = array("01-01",
			"01-06",
			"04-25",
			"05-01",
			"06-02",
			"08-15",
			"11-01",
			"12-08",
			"12-25",
			"12-26",
		);
		
		$i = 0;
		while ($data_inizio < $data_fine)
		{
			$giorno_data = date("w",$data_inizio); //verifico il giorno: da 0 (dom) a 6 (sab)
			$mese_giorno = date('m-d',$data_inizio); // confronto con gg sempre festivi
			// Infine verifico che il giorno non sia sabato,domenica,festivo fisso o festivo variabile (pasquetta);	
			if ($giorno_data !=0 && $giorno_data != 6 && !in_array($mese_giorno,$giorniFestivi) && !in_array($data_inizio,$array_pasquetta) )
			{
				$i++;
				
			}
			$data_inizio = $data_inizio + 86400;
		}
		
		return $i;
	}

        function description_of_task_in_wo(){
            if(!empty($this->params['t_wares_10']['f_pair']))
                for($u = 0; $u < count($this->params['t_wares_10']['f_pair']); $u++)
                        $this->db->update('t_pair_cross', array('fc_task_notes' => $this->params['t_wares_10']['f_pair'][$u]['f_description']), 'f_code_cross =' . $this->params['t_wares_10']['f_pair'][$u]['f_group_code'] . ' and ' . 'f_code_main = ' . $this->f_code);
        }
        
            /**
     * Create function to check on condition clause
     * @return string 
     */
    public function fc_cond_condition()
    {
        return;
        $function = 'if(';
        $error = array();
        if($this->params['fc_cond_list_function'] == 'Function' || !isset($this->params['fc_cond_list_function'])) {
            $function = $value;
            $functions = array("IN_RANGE"=>"/IN_RANGE\(\d+,\d+,\d+\)/",
                    "EVERY"=>"/EVERY\(\d+,\w+\)/",
                    "FCODE"=>"/FCODE\(\d+\)/",
                    "IN_LIST"=>"/IN_LIST\(\d+,.*\)/"
                    ,"METER_VALUE"=>"/METER_VALUE\(\d+\)/");                    
            $f_codes_meter = array();                    
            foreach($functions as $key => $reg) {        
                $preg_match = array();
                if(preg_match_all($reg, $value,$preg_match)) {            
                    foreach($preg_match as $line) {
                        foreach($line as $val) {
                            //elimino il nome funzione
                            $raw_code = str_replace($key,"",$val);
                            //elimino le parentesi
                            $raw_code = str_replace("(","",$raw_code);
                            $raw_code = str_replace(")","",$raw_code);
                            //esplodo per le virgole e prendo il primo code
                            $exp_val = explode(",",$raw_code);
                            if(!in_array($exp_val[0],$f_codes_meter))
                                    $f_codes_meter[] = $exp_val[0];
                        }                
                    }            
                }
            }                                
        }
        else {
            $function = 'if(';
            $f_codes_meter = array($this->params['fc_cond_code_meter']);
            $code_meter = $this->params['fc_cond_code_meter'];
            $val_ref = addslashes($this->params['fc_cond_value_reference']);
            $operation = utf8_decode($this->params['fc_cond_run_operation']);
            if($code_meter == '' || !ctype_digit($code_meter) || $code_meter == 0) {                                                    
                $function = '';
                $f_codes_meter = array();
            }                                
            elseif($val_ref == '') {                                    
                $error['message'] = 'You cannot create an on condition function without reference value';
                $function = '';
                $f_codes_meter = array();
            }
            else {
                switch($this->params['fc_cond_list_function']) {
                    case 'METER_VALUE' :                                         
                        $function.="METER_VALUE('$code_meter') {$this->params['fc_cond_operator']} '$val_ref'){".$operation."}";
                        break;
                    case 'AVG' :
                        $avg_num = (int)($this->params['fc_cond_average_number_value']);                                        
                        if($avg_num == 0) {
                                $error['message'] = 'You cannot create an on condition average function without the number of meter to do the operation';
                                $function = '';$f_codes_meter = array();
                        }
                        else {
                                $function.="AVG($code_meter,'$avg_num') {$this->params['fc_cond_operator']} '$val_ref'){".$operation."}";                                        
                        }
                        break;
                    case 'IN_LIST' :
                        $function.="IN_LIST($code_meter,\"$val_ref\")){".$operation."}";
                        break;
                    case 'IN_RANGE' :
                        $function.="IN_RANGE($code_meter,$val_ref)){".$operation."}";
                        break;
                    case 'EVERY' :
                        $function.="EVERY($code_meter,$val_ref)){".$operation."}";
                        break;                                    
                    default : 
                        $error['message'] = 'Unknown on condition function';
                        $function = '';$f_codes_meter = array();
                        break;
                }
                if($function != '') {
                    $this->db->update("t_workorders",array("fc_cond_condition"=>$function),"f_code = $this->f_code");
                }
            }
        }
        if($function != '') {
            $this->db->delete("t_wo_relations","f_code_wo_slave = $this->f_code");
            $select_cond = new Zend_Db_Select($this->db);
            foreach($f_codes_meter as $code_meter) {
                $select_cond->reset();
                $res_check = $select_cond->from("t_workorders",array('f_code'))
                    ->where("f_type_id = 11")
                    ->where("f_code = ?",$code_meter)
                    ->query()->fetchAll();
                if(empty($res_check)) continue;
                $this->db->insert("t_wo_relations",array(
                    'f_code_wo_master'=>$code_meter,
                    'f_type_id_wo_master'=>11,
                    'f_code_wo_slave'=>$this->f_code,
                    'f_type_id_wo_slave'=>9
                ));
            }
            unset($select_cond);
        }        
        return $error;
    }

  /**
   * Verifico al salvataggio se è cambiato la installation date.
   * Se si, modifico la schedulazione per tutte le pm associate
   */
  public function fc_asset_installation_date()
  {
 
      $select = new Zend_Db_Select($this->db);
      //controllo che sia un asset e che non sia stato chiuso/sospeso/cancellato
      $group_id_asset = $select->from(["t1" => "t_creation_date"], [])
        ->join(["t2" => "t_wf_phases"], "t1.f_phase_id = t2.f_number and t1.f_wf_id = t2.f_wf_id", ['f_group_id'])
        ->where("t1.f_id = ?", $this->f_code)->where("f_category = 'ASSET'")->query()->fetchAll();

      if(!$group_id_asset[0]['f_group_id'] || in_array($group_id_asset[0]['f_group_id'], [5, 6, 7])){ return; }
     
      $select->reset();
      $res_installation_date = $select->from("t_wares","fc_asset_installation_date")
        ->where("f_code = ?", $this->f_code)->query()->fetch();

      $installation_date = $res_installation_date["fc_asset_installation_date"];
      $select->reset();
      $res_old_installation_date = $select->from("t_wares_history","fc_asset_installation_date")
        ->order("f_id DESC")->limit(1)->query()->fetch();
      $old_installation_date = $res_old_installation_date["fc_asset_installation_date"];  
      if($installation_date == $old_installation_date) return; //stessa data, nulla da fare
      if($installation_date == null || $installation_date == 0){ //rimuovo la ciclicità da tutte le PM associate a questo asset
          $this->db->delete("t_periodics","f_executed = 0 and f_asset_code = ".$this->f_code);
          return;
      }
      $wo = new Mainsim_Model_Workorders($this->db);
      $select->reset();
      $res_pm = $select->from(['t1' => "t_workorders"],['f_code'])
        ->join(['t2' => 't_ware_wo'], "t1.f_code = t2.f_wo_id", [])
        ->where("f_type_id = 3")->where("fc_pm_type = 'pm-per-asset'")
        ->where("f_ware_id = ?",$this->f_code)
        ->query()->fetchAll();
      $asset_list = [$this->f_code => $installation_date];
      foreach($res_pm as $pm) {
          $wo->createPeriodics($pm['f_code'], null, $asset_list);
      }
  }

    /**
     * Verifica se l'asset è in fase sospesa, chiusa o cancellata.
     * Se così, rimuove le periodicità dai wo associati
     */
  public function manageAssetPeriodic()
  { 
      $select = new Zend_Db_Select($this->db);
      $group_id_asset = $select->from(["t1" => "t_creation_date"], [])
        ->join(["t2" => "t_wf_phases"], "t1.f_phase_id = t2.f_number and t1.f_wf_id = t2.f_wf_id", ['f_group_id'])
        ->where("t1.f_id = ?", $this->f_code)->where("f_category = 'ASSET'")->query()->fetch();
        $group_id_asset = array_values($group_id_asset);
      if(!$group_id_asset || !in_array($group_id_asset, [5, 6, 7])){ return; }
      //l'asset è in una fase di sospensione/chiusura/cancellazione quindi cancello tutte le sue future generazioni
      $this->db->delete("t_periodics", "f_asset_code = ".$this->f_code." and f_executed = 0");
  }

  public function popupForm($params) {
    $data = [];
    $data['title'] = $params['title'];
    $data['width'] = $params['width'];
    $data['height'] = $params['height'];
    $data['callback'] = $params['callback'];
    
    //modificato da elCarbo in data 10/10/2018
    //controllo se ci siano parametri extra(separati da virgola), e nel caso li passo all'url
    if(!empty($params['extra'])){
        $extra_params = str_replace(",", "&", $params['extra']);
    }
    
    $src = preg_replace('/^.*scripts/', 'scripts', SCRIPTS_PATH.$params["file"]).'.php';
    if($extra_params) $src .= "?". $extra_params;
    
    $content = '<iframe scrolling="auto" frameborder="0" style="width: 500px; position: absolute; height: 700px; overflow: visible" src="'.$src.'"></iframe>';
    $data['content'] = base64_encode($content);
    return $data;
  }
}
 