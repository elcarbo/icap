<?php

 class Mainsim_Model_Menu {

     private $db, $q, $uid;

     public function __construct() {
         $this->db = Zend_Db::factory(Zend_Registry::get('db'));
         $this->q = new Zend_Db_Select($this->db);
         $this->uid = Zend_Auth::getInstance()->getIdentity()->f_id;
     }

     public function __destruct() {
         $this->db->closeConnection();
     }

     public function createInspection($params) {
         $module = $params["tg"];
         $db_module = str_replace("pp_", "", $module);
         $name = $params["menu"];
         
         // security fix (match number,number,...)
         if(!preg_match("/^[0-9]{1,}(,[0-9]{1,})*$/", $params["type"])){ 
             die('params type input not valid');
         }

         // Shared
         $select = new Zend_Db_Select($this->db);
         $shared = $select->from(array("t1" => "t_creation_date"), array("f_name" => "f_title"))
                         ->join(array("t2" => "t_systems"), "t1.f_id = t2.f_code", array("fc_bkm_module"))
                         ->where("f_phase_id = 1")
                         ->where("fc_bkm_module = ?", $db_module)
                         ->where("fc_bkm_user_id = 0")->where("f_type_id = 3")->query()->fetchAll();

         //Personal
         $select->reset();
         $personal = $select->from(array("t1" => "t_creation_date"), array("f_name" => "f_title"))
                         ->join(array("t2" => "t_systems"), "t1.f_id = t2.f_code", array("fc_bkm_module"))
                         ->where("fc_bkm_module = ?", $db_module)
                         ->where("f_phase_id = 1")
                         ->where("fc_bkm_user_id = ?", $this->uid)->where("f_type_id = 3")
                         ->query()->fetchAll();

         $menu = array(
             array("Txt" => "Shared", "Type" => (count($shared) > 0 ? 1 : 0), "Disable" => (count($shared) > 0 ? 0 : 1), "Sub" => array()),
             array("Txt" => "Personal", "Type" => (count($personal) > 0 ? 1 : 0), "Disable" => (count($personal) > 0 ? 0 : 1), "Sub" => array()),
             array("Txt" => "Save as...", "Type" => 0, "Fnc" => "$module.createInspection('', 0, '$name');", "Icon" => array("inspection_add.png")),
             array("Txt" => "Delete displayed", "Type" => 0, "Fnc" => "$module.deleteInspection();")
         );
         $tot_a = count($shared);
         for ($a = 0; $a < $tot_a; ++$a) {
             $menu[0]["Sub"][] = array(
                 "Txt" => stripslashes(Mainsim_Model_Utilities::chg($shared[$a]["f_name"])),
                 "Type" => 0,
                 "Status" => 1,
                 "Group" => 1,
                 "Icon" => "",
                 "Fnc" => "$module.selectInspection('" . stripslashes(Mainsim_Model_Utilities::chg($shared[$a]["f_name"])) . "', 1);"
             );
         }
         $tot_a = count($personal);
         for ($a = 0; $a < $tot_a;  ++$a) {
             $menu[1]["Sub"][] = array(
                 "Txt" => stripslashes(Mainsim_Model_Utilities::chg($personal[$a]["f_name"])),
                 "Type" => 0,
                 "Status" => 1,
                 "Group" => 1,
                 "Icon" => "",
                 "Fnc" => "$module.selectInspection('" . stripslashes(Mainsim_Model_Utilities::chg($personal[$a]["f_name"])) . "');"
             );
         }
         return $menu;
     }

     public function createExtras($params) {
         $table = $params["table"];
         $category = $params["type"];
         // security fix
         if(!preg_match("/^[0-9]{1,}(,[0-9]{1,})*$/", $category)){
             die('params type not valid');
         }
         
        // security fix (match mdl)
        if(!preg_match("/^mdl_([a-z]){2,}\d*_tg$/", $params['tg'])){ 
            die('param tg not valid');
        }
        else{
            $module = $params['tg'];
        }
        
        // security fix (match mn)
        if(!preg_match("/^mn_([a-z]){2,}\d*_extras$/", $params['menu'])){ 
            die('param menu not valid');
        }

         $this->q->reset();
         if (!isset($module) || !isset($table) || !isset($category))
             return array();
         //get wfs        
         $res = $this->q->from(array("t1" => $table . '_types'), array('f_wf_id'))
                         ->join(array("t2" => "t_workflows"), "t1.f_wf_id = t2.f_id", array("f_name"))
                         ->where("t1.f_id IN ({$category})")->group("f_wf_id")->group('f_name')->query()->fetchAll();
         $tot_i = count($res);
         $menu = array();
         for ($i = 0; $i < $tot_i;  ++$i) {
             $this->q->reset();
             $line = $res[$i];
             $res_wf = $this->q->from(array("t3" => "t_wf_phases"), array("Txt" => "f_name", "f_number"))
                             ->where("f_wf_id = ({$line['f_wf_id']})")->where("f_group_id <> 8")
                             ->group("t3.f_order")->group('t3.f_number')->group('t3.f_name')
                             ->order("t3.f_order ASC")->order("t3.f_name ASC")->query()->fetchAll();

             $tot_a = count($res_wf);
             $phases = array();
             for ($a = 0; $a < $tot_a; ++$a) {
                 $phases[] = array("Txt" => $res_wf[$a]["Txt"], "Type" => 0, "Fnc" => "$module.setPhaseBatch({$line["f_wf_id"]}, {$res_wf[$a]['f_number']});");
             }
             if ($tot_a > 0) {
                 $phases = array_values($phases);
                 $menu[$line['f_wf_id']] = array("Txt" => $line["f_name"], "Type" => 1, "Sub" => $phases);
             }
         }
         return array_values($menu);
     }

//    public function createChgph($params) 
//    {//var_dump($params);
//        $this->q->reset();
//        $same_wf = $this->q->from(array("t1"=>'t_creation_date'), array('f_wf_id'))
//                ->where("t1.f_id IN ({$params['code']})")->query()->fetchAll();     
//
//                foreach($same_wf as $sw){
//                    if($sw['f_wf_id'] != $same_wf[0]['f_wf_id']) return array_values(array());
//                }
//        $this->q->reset();
//
//        $list_codes = explode(',', $params['code']);
//        
//        $query = "select f_phase_id, t_wf_exits.f_description, f_exit, COUNT(f_exit) as ex
//        from t_creation_date join t_wf_exits on t_creation_date.f_wf_id = t_wf_exits.f_wf_id 
//        and t_creation_date.f_phase_id = t_wf_exits.f_phase_number 
//        where t_creation_date.f_id IN (" . $params['code'] . ") and t_wf_exits.f_visibility <> 1 group by f_exit";
//        //var_dump($query);
//    
//        $res = $this->db->query($query)->fetchAll();     
//        
//        $arr_ex = array();
//        foreach ($res as $value) {
//            if($value['ex'] == count($list_codes)) array_push ($arr_ex, $value);
//        }
//       
//        $module = $params['tg'];
//        $arr = array();
//        foreach($arr_ex as $r){
//            $txt = $r['f_description'];
//            $code = $r['f_exit'];
//            $a = array("Txt" => $txt, "Type" => 0, "Fnc" => "$module.setPhaseBatch({$same_wf[0]['f_wf_id']}, {$code});");
//            array_push($arr, $a);
//        }
//		if(empty($arr)){
//            array_push($arr, array("Txt" => "No phases"));
//        }
//        return array_values($arr);
//    }
//    
     public function createChgph($params) 
    {
         
        // security fix
        if(!preg_match("/^[0-9]{1,}(,[0-9]{1,})*$/", $params['code'])){
            die('params code not valid');
        }
         
        $this->q->reset();
        $same_wf = $this->q->from(array("t1"=>'t_creation_date'), array('f_wf_id'))->distinct()
                ->where("t1.f_id IN ({$params['code']})")->query()->fetchAll();     

        if(count($same_wf)>1) return array_values(array());

        $q_wj = '';           
        try{
            //aggiunto da elCarbo in data 16/01/2019
            $excludewfphases = empty($params['excludewfphases'])?[]:json_decode($params['excludewfphases'],true);  
            if(isset($excludewfphases[$same_wf[0]['f_wf_id']])){
                $awj = explode(",",$excludewfphases[$same_wf[0]['f_wf_id']]);

                $arwp=array();

                for($r=0;$r<count($awj);$r++){
                    $aw=explode("_",$awj[$r]);

                    $f_phase_n=$aw[0]; $f_exit=$aw[1];

                    if(!isset($arwp[$f_phase_n]) ) $arwp[$f_phase_n]=array();  
                    if(!in_array($f_exit, $arwp[$f_phase_n]) ) $arwp[$f_phase_n][]=$f_exit;
                }

                $query_restr_arr = array();
                // build query restrinction
                foreach($arwp as $k=>$v){  
                    if(!empty($v))$query_restr_arr [] = "(t_wf_exits.f_phase_number != $k OR t_wf_exits.f_exit NOT IN(".implode(",",$v)."))"; 
                }

                if(!empty($query_restr_arr))
                    $q_wj = " AND (".implode(" AND ",$query_restr_arr).")";
            }
            /////////////////
        }
        catch (Exception $e){}
        
        $this->q->reset();
        
        

        $list_codes = explode(',', $params['code']);
        
        $query = "select  t_wf_phases.f_name,  t_wf_phases.f_number, COUNT(f_exit) as ex, t_wf_exits.f_visibility as lvl,
             t_creation_date.f_editability as edit
        from t_creation_date join t_wf_exits on t_creation_date.f_wf_id = t_wf_exits.f_wf_id 
        and t_creation_date.f_phase_id = t_wf_exits.f_phase_number 
        join t_wf_phases on t_wf_phases.f_number=t_wf_exits.f_exit and t_wf_phases.f_wf_id=t_creation_date.f_wf_id 
        where t_creation_date.f_id IN (" . $params['code'] . ")".$q_wj."  group by t_wf_phases.f_number, f_name, t_wf_exits.f_visibility, t_creation_date.f_editability ";

        $res = $this->db->query($query)->fetchAll();     

        $arr_ex = array();
        foreach ($res as $value) {
            if($value['ex'] == count($list_codes)) array_push ($arr_ex, $value);
        }
        
        // security fix (match number,number,...)
        if(!preg_match("/^mdl_([a-z]){2,}_tg$/", $params['tg'])){ 
            die('param tg input not valid');
        }
        else{
            $module = $params['tg'];
        }
        
        $arr = array();
        foreach($arr_ex as $r){
            $txt = $r['f_name'];
            $code = $r['f_number'];
            $lvl=(int)$r['lvl'];
            $edit=(int)$r['edit'];

            $a = array("Txt" => $txt, "Type" => 0, "Fnc" => "$module.setPhaseBatch({$same_wf[0]['f_wf_id']}, {$code});","Level"=>$lvl&$edit);
            array_push($arr, $a);
        }
		if(empty($arr)){
            array_push($arr, array("Txt" => "Nessuna fase in comune"));
        }
        return array_values($arr);
    }

}