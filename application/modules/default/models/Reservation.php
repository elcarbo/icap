<?php

class Mainsim_Model_Reservation
{
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function getReservationWorkorder($params){
        
        $startDate = mktime (0,0,0, $params['daterange']["from"][1]+1, $params['daterange']["from"][2], $params['daterange']["from"][0]);
        $endDate = mktime (23,59,59, $params['daterange']["to"][1]+1, $params['daterange']["to"][2], $params['daterange']["to"][0]);
        
        $select = new Zend_Db_Select($this->db);
        $select->from('t_creation_date')
                ->join('t_workorders', 't_creation_date.f_id = t_workorders.f_code')
                ->joinLeft('t_ware_wo', 't_workorders.f_code = t_ware_wo.f_wo_id', array())
                ->joinLeft(array('cd2' => 't_creation_date'), 't_ware_wo.f_ware_id = cd2.f_id', array('ware_id' => 'f_id', 'ware_title' => 'f_title'))
                ->where('!(t_workorders.f_start_date > ' . $endDate . ' OR t_workorders.f_end_date < ' . $startDate . ')')
                ->where('t_workorders.f_type_id = 8');
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $result[] = array(
                $res[$i]['f_code'],
                $res[$i]['fc_progress'],
                $res[$i]['f_user_id'],
                $res[$i]['f_title'],
                isset($res[$i]['f_description']) ? $res[$i]['f_description'] : "",
                $res[$i]['f_start_date'],
                $res[$i]['f_end_date'],
                $res[$i]['ware_id'],
                $res[$i]['ware_title'],
                $res[$i]['fc_rsv_id']
            );
        }
        return $result;
    }
    
    public function newReservation($postData){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        
        $objWo = new Mainsim_Model_Workorders();
        // prepare data for workorder creation
        $params['f_type_id'] = 8;
        $params['f_module_name'] = "mdl_wo_tg";
        $params['f_description'] = $postData->description;
        $params['f_title'] = $postData->title;
        $params['t_wares_1']['f_code'] = array($postData->asset_code);
        $params['t_wares_1']['f_pair'] = array();
        $params['t_wares_1']['f_code_main'] = 0;
        $params['t_wares_1']['f_type'] = 1;
        $params['t_wares_1']['Ttable'] = 't_wares';
        $params['t_wares_1']['pairCross'] = 0;
        $params['t_wares_1']['f_module_name'] = 'mdl_wo_asset_tg';
        $params['f_start_date'] = $postData->datefrom;
        $params['f_end_date'] = $postData->dateto == 0 ? $postData->datefrom + (($postData->timeto - $postData->timefrom) * 1800) : $postData->dateto;
        $params['fc_rsv_id'] = $userData->f_id . '_' . time();
        $params['fc_rsv_fieldset'] = json_encode($postData->f_reservation_fieldset);
        // check ware availability
        $availability = $this->checkWareAvailability($params['f_start_date'], $params['f_end_date'], $postData->asset_code);
        if($availability){
            $resNewWo = $objWo->newWorkOrder($params);
            $response['result'] = 1;
            $response['tablist'][] = array($params['f_start_date'], $params['f_end_date'], 1);
            $response['f_reservation_id'] = $params['fc_rsv_id'];
            $response['f_code'] = $resNewWo['f_code'];
        }
        else{
            $response['result'] = 0;
            $response['tablist'][] = array($params['f_start_date'], $params['f_end_date'], 0);   
        }
        return $response;
    }
    
    public function editReservation($postData){
        $userData = Zend_Auth::getInstance()->getIdentity();

        // edit title, description
        $objWo = new Mainsim_Model_Workorders();
        $params['f_title'] = $postData->title;
        $params['f_description'] = $postData->description;
        $params['f_code'] = $postData->f_code;
        $params['f_type_id'] = 8;
        $params['f_module_name'] = "mdl_wo_tg";
        // lock wo
        $this->db->insert('t_locked', array('f_code' => $postData->f_code, 'f_user_id' => $userData->f_id, 'f_timestamp' => time()));
        $id = $this->db->lastInsertId();
        $objWo->editWorkOrder($params);
        // unlock wo
        $this->db->delete('t_locked', 'f_id = ' . $id);
        
        // check if there are workorders to be deleted
        // retrieve database workorders
        $select = new Zend_Db_select($this->db);
        $select->from('t_workorders')
               ->where("fc_rsv_id ='" . $postData->f_reservation_id . "'");
        $res = $select->query()->fetchAll();
        // for each wo database control if it has been deleted from the client
        for($i = 0; $i < count($res[$i]); $i++){
            $found = false;
            for($j = 0; $j < count($postData->l_tablist); $j++){
                if($res[$i]['f_start_date'] == $postData->l_tablist[$j][0] && $res[$i]['f_end_date'] == $postData->l_tablist[$j][1]){
                    $found = true;
                    break;
                }
            }
            if(!$found){
                // delete t_ware_wo
                $this->db->delete('t_ware_wo', 'f_wo_id = ' . $res[$i]['f_code']);
                // delete t_workorders_parent
                $this->db->delete('t_workorders_parent', 'f_code = ' . $res[$i]['f_code']);
                // delete t_custom_fields_history
                $this->db->delete('t_custom_fields_history', 'f_code = ' . $res[$i]['f_code']);
                // delete t_custom_fields
                $this->db->delete('t_custom_fields', 'f_code = ' . $res[$i]['f_code']);
                // delete t_workorders_history
                $this->db->delete('t_workorders_history', 'f_code = ' . $res[$i]['f_code']);
                // delete t_workorders
                $this->db->delete('t_workorders', 'f_code = ' . $res[$i]['f_code']);
                // delete t_creation_date_history
                $this->db->delete('t_creation_date_history', 'f_id = ' . $res[$i]['f_code']);
                // delete t_creation_date
                $this->db->delete('t_creation_date', 'f_id = ' . $res[$i]['f_code']);
            }
        }
        
        $response['result'] = 1;
        return $response;
    }
    
    public function checkWareAvailability($from, $to, $wareId){
        
        $select = new Zend_Db_Select($this->db);
        $select->from('t_workorders', array('count(*) as tot'))
               ->join('t_ware_wo', 't_workorders.f_code = t_ware_wo.f_wo_id', array())
               ->where('!(f_end_date <= ' . $from . ' OR f_start_date >= ' . $to . ')')
               ->where('t_ware_wo.f_ware_id =' . $wareId)
               ->where('t_workorders.f_type_id = 8');
        $res = $select->query()->fetchAll();
        if($res[0]['tot'] > 0){
            return false;
        }
        else{
            return true;
        }
    }
    
    public function getReservationByCode($f_code){
        $select = new Zend_Db_Select($this->db);
        
        
    }
    
    public function getReservationEdit($params){
        
        // get bookable wares
        $select = new Zend_Db_Select($this->db);
        $select->from('t_wares')
               ->join('t_creation_date', 't_creation_date.f_id = t_wares.f_code')
               ->where('fc_asset_bookable = 1');
        $res = $select->query()->fetchAll();
        
        $assets = array();
        $visited = array();
        for($i = 0; $i < count($res); $i++){
            // get hierachy for each bookable ware
            $parentsCodes = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $res[$i]['f_code']);
            array_unshift($parentsCodes, $res[$i]['f_code']);
            
            $select->reset();
            $select->from('t_wares')
                ->join('t_creation_date', 't_creation_date.f_id = t_wares.f_code')
                ->joinLeft('t_wares_parent', 't_creation_date.f_id = t_wares_parent.f_code')
                ->where('t_wares.f_code IN (' . implode(",", $parentsCodes) . ')');
            $resParents = $select->query()->fetchAll();
            for($j = 0; $j < count($resParents); $j++){
                // element not yet visited => add it to result array
                if(!in_array($resParents[$j]['f_code'], $visited)){
                    $assets[] = array($resParents[$j]['f_code'], Mainsim_Model_Utilities::chg($resParents[$j]['f_title']), $resParents[$j]['fc_asset_bookable_minimum_hour'], $resParents[$j]['fc_asset_bookable_maximum_hour'], $resParents[$j]['f_parent_code'], $resParents[$j]['fc_asset_bookable']);
                    $visited[] = $resParents[$j]['f_code'];
                }
            }
        }
        $result['assets'] = $assets;
        
        // edit old reservation
        if($params['edit'] == 1){
            
            // get all instances of this reservation
            $select->reset();
            $select->from('t_workorders')
                   ->where("fc_rsv_id ='" . $params['f_reservation_id'] . "'");
            $res = $select->query()->fetchAll();
            $tablist = array();
            for($i = 0; $i < count($res); $i++){
                $tablist[] = array($res[$i]['f_start_date'], $res[$i]['f_end_date'], 1);
                
            }
            $result['edit']['tablist'] = $tablist;
            
            // get fieldset
            $select->reset();
            $select->from('t_workorders', array('fc_rsv_fieldset'))
                   ->where("f_code ='" . $params['f_code'] . "'");
            $res = $select->query()->fetchAll();
            $result['edit']['f_reservation_fieldset'] = json_decode($res[0]['fc_rsv_fieldset']);
            
        }
        return $result;
    }
} 