<?php

class Mainsim_Model_OCReader
{
    private $db;
    
    public function __construct($db = null) {
        $this->db = !is_null($db)?$db:Zend_Db::factory(Zend_Registry::get('db'));
    }
        
    /**
     * 
     * @param mixed $value value to be checked
     * @param string $listOfValues list of values used for the check. Values are comma-separeted value
     * @return boolean true if value is in list or false
     */
    private function inList($value,$listOfValues) 
    {
        if(is_null($value) || is_null($listOfValues)){ return false;}
        $valuesList = explode(',',$listOfValues);
        return in_array($value, $valuesList);
    }
    
    /**
     * Calculate if last value reach generation value or its multiple
     * @param int $f_code meter code
     * @param int|float $value last insert value 
     * @param int|float $refValue reference value
     * @return boolean true if condition is correct, false if not
     */
    private function every($f_code,$value,$refValue,$startValue) 
    {
        if(is_null($f_code) || is_null($value) || is_null($refValue)){ return false;}
        $select = new Zend_Db_Select($this->db);
        $resValue = $select->from("t_meters_history","f_value")
          ->where("f_code = ?",$f_code)->where("f_checked = 1")->order("f_id desc")->limit(1)
          ->query()->fetch();
        $prevVal = empty($resValue)?0:$resValue['f_value'] - $startValue;        
        $count = ((int)($prevVal/$refValue)) + 1;
        $max = ($refValue * $count) + $startValue;        
        return $value >= $max;
    }
    
    /**
     * Check if value is between the passed range
     * @param mixed $value
     * @param string $rangeValues range is a string with values comma-separated
     * @return boolean true if in range or false if not
     */
    private function inRange($value,$rangeValues) 
    {
        if(is_null($value) || is_null($rangeValues)){ return false;}
        list($min,$max) = explode(",",$rangeValues);
        return $min <= $value && $value <=$max;
    }
    
    /**
     * Check if last reading is over the reference value
     * @param int|float $value last meter value
     * @param int|float $refValue reference value
     * @param string $operator logical operator (<,>,<=,>=,!=)
     * @return boolean true if condition is correct or false
     */
    private function meterValue($value = null,$refValue = null,$operator=null) 
    {
        if(is_null($operator) || is_null($value) || is_null($refValue)){ return false;}
        $checkFnc = "\$check = ($value $operator $refValue);";
        eval($checkFnc);
        return $check;        
    }
    
    /**
     * checks if last readings violate the condition
     * @param int $f_code meter code
     * @param int $avgNum number of reading to be checked
     * @param int|float $refValue reference value to check the condition
     * @param string $operator logical operator (<,>,<=,>=,!=)
     * @return boolean true if condition is correct or false
     */
    private function average($f_code,$avgNum,$refValue,$operator) 
    {
        if(is_null($f_code) ||is_null($operator) || empty($avgNum) || is_null($refValue)){ return false;}        
        $select = new Zend_Db_Select($this->db);
        $resReadings = $select->from("t_meters_history","f_value")
            ->where("f_code = ?",$f_code)->order("f_id desc")
            ->limit($avgNum)->query()->fetchAll();                
        $tot = count($resReadings);
        $totValue = 0;
        for($i = 0;$i < $tot;++$i) {
            $totValue+=is_null($resReadings[$i]['f_value'])?0:$resReadings[$i]['f_value'];
        }        
        $avgValue = number_format( ($totValue / $avgNum),4,".","");
        $checkFnc = "\$check = ($avgValue $operator $refValue);";
        eval($checkFnc);
        return $check;
    }
    
    /**
     * read pending meter value and if condition is triggered, 
     * execute associated operations
     */
    public function onConditions() 
    {
        $select = new Zend_Db_Select($this->db);
        $meterList = $select->from("t_meters_history",['f_id','f_code','f_value'])
            ->where("f_checked = 0")->where("f_in_work = 0")->order("f_timestamp ASC")->limit(10)
            ->query()->fetchAll();        
        //$this->setAsInWork($meterList); 
        $tot = count($meterList);
        for($i = 0;$i < $tot;++$i) {
            $meterValue = $meterList[$i]['f_value'];
            $temp_meter_log = [];
            $select->reset();
            $resOc = $select->from(["t1"=>"t_creation_date"],[])
                ->join(["t2"=>"t_workorders"],"t1.f_id = t2.f_code",['f_code','f_id','f_type_id','fc_cond_list_function',
                    'fc_cond_average_number_value','fc_cond_value_reference','fc_cond_run_operation','fc_cond_operator',
                    'fc_cond_start_value'])->join(["t3"=>"t_wf_phases"],"t1.f_phase_id = t3.f_number and t1.f_wf_id = t3.f_wf_id",[])                
                ->join(["t5"=>"t_wo_relations"],"t5.f_code_wo_slave = t1.f_id",[])                    
                ->where("t2.f_type_id = 9")->where("t3.f_group_id < 6")
                ->where("t5.f_code_wo_master = ?",$meterList[$i]['f_code'])                
                ->query()->fetchAll();            
            $totB = count($resOc);
            for($b = 0;$b < $totB;++$b){
                $conditionCheck = false;
                $line = $resOc[$b];
                switch($line['fc_cond_list_function']) {
                    case 'METER_VALUE':
                        $conditionCheck = $this->meterValue($meterValue,$line['fc_cond_value_reference'],$line['fc_cond_operator']);
                        break;
                    case 'AVG':
                        $conditionCheck = $this->average($meterList[$i]['f_code'],$line['fc_cond_average_number_value'],$line['fc_cond_value_reference'],$line['fc_cond_operator']);                        
                        break;
                    case 'IN_LIST':
                        $conditionCheck = $this->inList($meterValue,$line['fc_cond_value_reference']);                        
                        break;
                    case 'IN_RANGE':
                        $conditionCheck = $this->inRange($meterValue,$line['fc_cond_value_reference']);                        
                        break;
                    case 'EVERY':                        
                        $startValue = !is_null($line['fc_cond_start_value'])?$line['fc_cond_start_value']:0;
                        $conditionCheck = $this->every($meterList[$i]['f_code'],$meterValue,$line['fc_cond_value_reference'],$startValue);                        
                        break;
                }
                if($conditionCheck) {  
                    $temp_meter_log[] = "OC CODE: " . $line['f_code'] . " CONDITION: " . $line['fc_cond_list_function'] . " VALUE REFERENCE: " . $line['fc_cond_value_reference'];                 
                    $this->executeCondition($resOc[$b]['f_code'],$meterList[$i]['f_value'],$resOc[$b]['fc_cond_run_operation'],$resOc[$b]['f_type_id'],$resOc[$b]['f_id']);
                }
            }
             //check this reading as complete (checked)
             if ($temp_meter_log)
                 $meter_log = count($temp_meter_log) . " OC GENERATED --- " . implode(" --- ", $temp_meter_log);
             else
                 $meter_log = NULL;
             
             try {
                 $this->db->update("t_meters_history", ['f_checked' => 1, "f_meter_log" => $meter_log], "f_id = {$meterList[$i]['f_id']}");
             } catch (Exception $e) {
                 $this->db->update("t_meters_history", ['f_checked' => 1], "f_id = {$meterList[$i]['f_id']}");
                 $this->db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>"Error in ".__METHOD__." : ".Mainsim_Model_Utilities::chg($e->getMessage()),'f_type_log'=>2));
           
            
             }
        }        
    }
    
    /**
     * set passed meters in "in work" phase, so other 
     * cron cannot take these
     * @param array $meters
     */
    private function setAsInWork($meters)
    {
        $tot = count($meters);
        for($i = 0;$i < $tot;++$i) {
            $this->db->update("t_meters_history", ["f_in_work"=>1],"f_id = {$meters[$i]['f_id']}");
        }
    }
    
    /**
     * Open new workorder
     * @param int $f_code f_code of oc generator    
     * @param mixed $value value raised oc
     */
    private function openWorkorder($f_code,$value) 
    {
        $select = new Zend_Db_Select($this->db);            
        //check: check if father is an oc generator. if true, skip this generation
        $res_father_no_oc = $select->from(array("t1"=>"t_workorders"))
            ->join(array("t2"=>"t_workorders_parent"),"t1.f_code = t2.f_parent_code")
            ->where("t2.f_active = 1")->where("t2.f_code = ?",$f_code)->query()->fetch();    
        if(!empty($res_father_no_oc) && (isset($res_father_no_oc['fc_cond_meter']) && strlen($res_father_no_oc['fc_cond_meter']) > 0)) {        
            return;
        }
        $wo = new Mainsim_Model_Workorders($this->db);    
        //get default user info    
        $admin_info = Mainsim_Model_Login::getUSerInfo(1, $this->db);        
        $row = array('f_code'=>$f_code,'f_start_date'=>time(),'fc_meter_value'=>$value, 'f_type_id' => 9);
        $wo->initializeWoGeneration($row, $admin_info);
    }
    
    private function sendMail(){} // wait implementation action email
    
    /**
     * Follow instructions of condition raiseds
     * @param int $f_code
     * @param mixed $value trigger value
     * @param string $operation operation type : open workorder or send mail
     */
    private function executeCondition($f_code,$value,$operation)
    {
        if($operation == 'OPENWORKORDER();') {
            $this->openWorkorder($f_code,$value);
        }
        else {
            //first or late i'll send an email
        }
    }
    
}