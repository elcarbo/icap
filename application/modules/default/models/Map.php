<?php

class Mainsim_Model_Map
{
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function getLocations()
    {   
        $q = new Zend_Db_Select($this->db);
        $locations = $q->from("t_creation_date", array("f_code" => "f_id", "loc" => "f_title", "lat" => "fc_latitude", "lon" => "fc_longitude"))->where("f_type = 'WARES'")->where("f_category = 'ASSET'")
            ->where("fc_latitude IS NOT NULL")->where("fc_latitude != ''")
            ->where("fc_longitude IS NOT NULL")->where("fc_longitude != ''")->query()->fetchAll();
        return $locations;
    }
    
    public function getAssetsByLocation($location) {
        return Mainsim_Model_Utilities::getChildCode("t_wares_parent", $location);
    }
    
    public function getWorkordersByAssets($assets, $type = '1,4,5,6,7,10,13', $priority = '', $owner = '') {
        $q = new Zend_Db_Select($this->db);
        $q->from(array("ww" => "t_ware_wo"), array())->join(array("wo" => "t_workorders"), "ww.f_wo_id = wo.f_code", array("f_type_id", "max_priority" => "MAX(wo.f_priority)", "count" => "COUNT(wo.f_type_id)"))
            ->join(array("cd" => "t_creation_date"), "wo.f_code = cd.f_id", array(""))
            ->join(array("p" => "t_wf_phases"), "cd.f_wf_id = p.f_wf_id AND cd.f_phase_id = p.f_number", array())
            ->join(array("g" => "t_wf_groups"), "p.f_group_id = g.f_id", array());
        if($owner) $q->join(array("wwo" => "t_ware_wo"), "cd.f_id = wwo.f_wo_id", array());
        $q->where("ww.f_ware_id IN (".implode(",", $assets).")")->where("wo.f_type_id IN ($type)")
        ->where("g.f_visibility != 0");
        if($priority) $q->where("wo.f_priority = $priority");
        if($owner) $q->where("wwo.f_ware_id = $owner");
        $res = $q->group("wo.f_type_id")->query()->fetchAll();
        return $res;
    }
    
    public function getTypeForFilter(){
        $q = new Zend_Db_Select($this->db);
        $data = $q->from(array("oi" => "t_ui_object_instances"), array('f_properties'))->where("oi.f_instance_name like 'mdl_wo_tg'")->query()->fetch();
        $data_res = json_decode($data['f_properties'], TRUE);
        $q->reset();
        $type = $q->from(array("wt" => "t_workorders_types"), array('f_id','f_type'))->where("wt.f_id in (" . $data_res['ftype'] . ")")->query()->fetchAll();
        
        return $type;
    }
            
        public function getPriorityForFilter(){
        $q = new Zend_Db_Select($this->db);
        $data = $q->from(array("oi" => "t_ui_object_instances"), array('f_properties'))->where("oi.f_instance_name like 'slct_wo_edit_priority'")->query()->fetch();
        $data_res = json_decode($data['f_properties'], TRUE);
        //$q->reset();
        //$type = $q->from(array("wt" => "t_workorders_types"), array('f_type'))->where("wt.f_id in (" . $data_res['ftype'] . ")")->query()->fetchAll();
        
        return $data_res;
    }
    
    public function getOwnerForFilter(){
        $q = new Zend_Db_Select($this->db);
        $data = $q->distinct()->from(array("oi" => "t_workorders"), array('fc_owner_name'))->where("oi.fc_owner_name is not NULL")->query()->fetchAll();
        $data_res = json_decode($data['fc_owner_name'], TRUE);
        //$q->reset();
        //$type = $q->from(array("wt" => "t_workorders_types"), array('f_type'))->where("wt.f_id in (" . $data_res['ftype'] . ")")->query()->fetchAll();
        
        return $data_res;
    }
}