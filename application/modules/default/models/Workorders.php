<?php

class Mainsim_Model_Workorders {

    private $db, $table;

    public function __construct($db = null) {
        $this->db = is_null($db) ? Zend_Db::factory(Zend_Registry::get('db')) : $db;
        $this->table = 't_workorders';
        $this->obj = new Mainsim_Model_MainsimObject();
        $this->obj->type = $this->table;
    }

    public function editWorkOrder($params, $edit_batch = false, $commit = true, $userinfo = array()) {
        $error = array();
        unset($params['f_id']);
        $time = time();
        if (Zend_Registry::isRegistered('time')) {
            $time = Zend_Registry::get('time');
        } else {
            Zend_Registry::set('time', $time);
        }
        try {
            //if is a normal new wo, start transaction
            if ($commit) {
                $this->db->beginTransaction();
            }
            Zend_Registry::set('db_connection_editor', $this->db);
            $sel = new Zend_Db_Select($this->db);
            if (empty($userinfo)) {
                $userinfo = Zend_Auth::getInstance()->getIdentity();
            }

            //check if this ware is your or not
            $check_lock = $sel->from("t_locked")->where("f_code = ?", $params['f_code'])
                            ->where("f_user_id = ?", $userinfo->f_id)->query()->fetch();
//            if(empty($check_lock)) {
//                throw new Exception("Element locked by another user.");
//            }   
            //AGGIUNTO 30-01-18 BALDINI: feedback riguardo a utente che blocca l'oggetto
            if (empty($check_lock)) {
                $sel->reset();
                $res_lock = $sel->from("t_locked")->join("t_creation_date", "t_creation_date.f_id=t_locked.f_user_id", array("f_title"))
                                ->where("t_locked.f_code = ?", $params['f_code'])->where("t_locked.f_user_id != ?", $userinfo->f_id)->query()->fetch();

                if (!empty($res_lock))
                    throw new Exception("You cannot modify this object because " . $res_lock["f_title"] . " is working on it");
                else
                    throw new Exception("You don't have the permssion to edit this object now. Try to repeat the operation");
            }

            // mod 04/02/13 AC
            $params['fc_editor_user_name'] = $userinfo->fc_usr_firstname . ' ' . $userinfo->fc_usr_lastname;
            $params['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_editor_user_gender'] = $userinfo->f_gender;
            $params['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_editor_user_mail'] = $userinfo->fc_usr_mail;

            // end mod 04/02/13 AC
            $columns = Mainsim_Model_Utilities::get_tab_cols("t_workorders");
            $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
            $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");

            //get old wo
            $old_wo = $this->getWO($params['f_code']);
            //get old wo's custom (used from t_scripts)
            $old_custom = $this->getWO($params['f_code'], 't_custom_fields');
            $old_creation = $this->getWO($params['f_code'], 't_creation_date', 'f_id');
            $old_cross_wa_wo = Mainsim_Model_Utilities::getCross('t_ware_wo', $params['f_code'], 'f_wo_id');
            $old_paircross = Mainsim_Model_Utilities::getPairCross($params['f_code']);
            //olds array for script
            $olds = array(
                'old_table' => $old_wo,
                'old_custom' => $old_custom,
                'old_creation' => $old_creation,
                'old_ware_wo_cross' => $old_cross_wa_wo,
                'old_paircross' => $old_paircross
            );
            
            //remove f_phase_id if = 0        
            if (empty($params['f_phase_id'])) {
                unset($params['f_phase_id']);
            }
            $custom_fields = array();
            $creation_fields = array();
            $new_wo = array();
            $module_name = $params['f_module_name'];
            $params = Mainsim_Model_Utilities::clearChars($params);
            $params = Mainsim_Model_Utilities::clearEmptyValuesParams($params);

            $config = Zend_Registry::get('db');
            $owner = false;
            if (strtolower($config->adapter) == 'sqlsrv') {
                $queryo1 = $this->db->fetchAll("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='t_workorders' AND COLUMN_NAME='fc_owner_id'");
            } else {
                $queryo1 = $this->db->fetchAll("SHOW COLUMNS FROM `t_workorders` LIKE 'fc_owner_id'");
            }
            if (sizeof($queryo1) > 0) {
                if (strtolower($config->adapter) == 'sqlsrv') {
                    $queryo1 = $this->db->fetchAll("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='t_workorders' AND COLUMN_NAME='fc_owner_avatar'");
                } else {
                    $queryo2 = $this->db->fetchAll("SHOW COLUMNS FROM `t_workorders` LIKE 'fc_owner_avatar'");
                }
                if (sizeof($queryo2) > 0) {
                    $owner = true;
                }
            }
            foreach ($params as $key => $param) {
                if (!$owner && ($key == 'fc_owner_id' || $key == 'fc_owner_avatar')) {
                    continue;
                }
                if (in_array($key, $creation_columns)) {
                    $creation_fields[$key] = $param;
                } elseif (in_array($key, $columns)) {
                    $new_wo[$key] = $param;
                } elseif (in_array($key, $custom_columns)) {
                    $custom_fields[$key] = $param;
                }
            }
            //check if there are any unique to check
            if ((!isset($params['multiEdit'])) && (isset($params['unique']) && !empty($params['unique']))) {
                foreach ($params['unique'] as $line_unique) {/* var_dump($line_unique);die(); */
                    Mainsim_Model_Utilities::isUnique($line_unique, $params, $params['f_module_name'], $this->db);
                }
            }

            $new_wo['f_timestamp'] = $time;
            $f_code = $params['f_code'];

            $creation_fields['f_timestamp'] = $time;
            //update creation date	            
            $this->db->update("t_creation_date", $creation_fields, "f_id = $f_code");
            $new_wo['f_user_id'] = $userinfo->f_id;
            //update t_workorders             
            $this->db->update("t_workorders", $new_wo, "f_code = $f_code");

            //insert uid of modifier
            $custom_fields['f_code'] = $params['f_code'];
            $custom_fields['f_timestamp'] = $time;

            //update t_custom_fields            
            $this->db->update("t_custom_fields", $custom_fields, "f_code = $f_code");

            /*
             * saved wo --> continue insert of wo and pair cross
             */
            $find = false;
            $scriptModule = new Mainsim_Model_Script($this->db, "t_workorders", $f_code, $params, $olds, $edit_batch, $userinfo);

            // check if overlapWare control is set
            $overlap = $scriptModule->overlapLabors();
            if ($overlap != null) {
                $error['message'] = $overlap;
            }
                  
            $this->saveCrossAndPair($f_code, $params, $scriptModule, $edit_batch);
            
            //manage attachments            
            Mainsim_Model_Utilities::saveAttachments($f_code, 't_workorders', $params, $scriptModule);
            if (isset($creation_fields['f_phase_id'])) {
                //check a change phase and some script to run.
                if ($creation_fields['f_phase_id'] != $old_creation['f_phase_id']) {
                    $sel->reset();
                    $res_phase = $sel->from(['t1' => "t_wf_exits"], 'f_script')
                                    ->join(['t2' => 't_wf_phases'], "t1.f_wf_id = t2.f_wf_id and t1.f_exit = t2.f_number", ['f_group_id'])
                                    ->where("f_phase_number = ?", $old_creation['f_phase_id'])
                                    ->where("f_exit = ?", $creation_fields['f_phase_id'])->where("t1.f_wf_id = ?", $creation_fields['f_wf_id'])->query()->fetch();
                    if (in_array($res_phase['f_group_id'], [6, 7])) {
                        $activeChildren = Mainsim_Model_Utilities::checkActiveChildren("t_workorders", $f_code, $this->db);
                        if ($activeChildren) {
                            throw new Exception("Close or delete all children elements before");
                        }
                    }
                    if (!empty($res_phase['f_script'])) {
                        eval($res_phase['f_script']);
                    }
                }
            }
            //----- check if there is one or more script linked to field ----- 
            $res_bm = Mainsim_Model_Utilities::getBookmark($module_name);
            $bm = json_decode(Mainsim_Model_Utilities::chg($res_bm['f_properties']), true);
            array_walk($bm, function($el) use(&$params) {
                if (!array_key_exists($el[0], $params)) {
                    $params[$line_bm[0]] = '';
                }
            });

            foreach ($params as $column => $value) {
                $sel->reset();
                $res_script_exist = $sel->from("t_scripts")->where("f_name = ?", $column . '-t_workorders')->where("f_type = 'php'")->query()->fetch();
                if (!empty($res_script_exist['f_script'])) {
                    eval($res_script_exist['f_script']);
                } elseif (method_exists($scriptModule, $column) && is_callable(array($scriptModule, $column))) {//if not exist try to check if is a custom script in script model
                    $scriptModule->$column();
                }
            }

            //manage periodics 
            $this->checkPeriodicShift($f_code);

            //if this is a pm or meter, re-create periodics                        
            if ($params['f_type_id'] == 3 || $params['f_type_id'] == 11 || $params['f_type_id'] == 19) {
                $maunaul_counter = (isset($params['fc_pm_manual_counter']) && $params['fc_pm_manual_counter'] > 0) ? true : false;
                $res_pm = $this->createPeriodics($f_code, 0, NULL, FALSE, $maunaul_counter);
                if (!$res_pm) {
                    throw new Exception("Invalid frequency settings");
                }
            }

            if (!empty($params['attachments']) && !($edit_batch)) {
                Mainsim_Model_Utilities::clearTempAttachments($params, $this->db);
            }

            // save pricelist if set
            if (isset($params['t_pricelist_25'])) {
                $objPrl = new Mainsim_Model_Pricelist($this->db);
                $objPrl->savePricelist($params['t_pricelist_25']);
            }

            // update auxiliary fields for selectors
            $selectors = [];
            foreach ($params as $key => $value) {
                if (strpos($key, 't_selectors_') !== false) {
                    $selectors = array_merge($selectors, $params[$key]['f_code']);
                }
            }
            
            // NICO 22/06/2018
            $exclude = ["USER"]; //Escludo i types dei quali non voglio andare ad associare i selettori in fc_hierarchy_selectors
            $sel->reset();
            $ids = $sel->from("t_wares_types", "f_id")
                        ->where("f_type not in (?)", $exclude)
                        ->query()->fetchAll(Zend_Db::FETCH_COLUMN);
            $codes = [];
            /*foreach ($params as $key=>$value) {
                if (strpos($key, 't_wares_') !== false) {
                    $type = explode('_', $key)[2]; // guardo il tipo
                    if (in_array($type,$ids)) {
                        foreach ($value['f_code'] as $value2) {
                            $codes[] = intval($value2);
                        }
                    }
                }
            }*/
            // NICO: recupero da DB invece che da params per il multiedit
            $sel->reset();
            $codes = $sel->from(["t1"=>"t_ware_wo"], "f_ware_id")
                    ->join(["t2"=>"t_wares"], "t1.f_ware_id = t2.f_code", [])
                    ->join(["t3"=>"t_wares_types"], "t2.f_type_id = t3.f_id", [])
                    ->where ("t3.f_type not in (?)", $exclude)
                    ->where("t1.f_wo_id = ".$f_code)
                    ->query()->fetchAll(Zend_db::FETCH_COLUMN);
            
            
            // NICO: ricreo $params[t_selectors_1,2,3,4,5,6,7,8,9,10] in caso non ci sia
            // Non c'è se il tab dei selettori non è visibile
            if (!isset($params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']) && !isset($params['t_selectors_1,2,3,4,5,6,7,8,9,10'])) {
                $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'] = [];
                $s = new Zend_Db_Select($this->db);
                $s->reset();
                $results = $s->from(["t1"=>"t_selector_wo"])
                        ->join(["t2"=>"t_selectors"],"t1.f_selector_id = t2.f_code", ["f_type_id"])
                        ->where("f_wo_id = ".$f_code)
                        ->query()->fetchAll();   
                $selettori = [];
                foreach ($results as $indice=>$selettore) {
                    $selettori[$selettore['f_type_id']][] = $selettore['f_selector_id'];
                }  
                foreach ($params as $indice=>$par) {
                    if (strpos($indice, 't_selectors_') !== false) {
                        if($indice == 't_selectors_1,2,3,4,5,6,7,8,9,10,11')
                            continue;
                        $selettori[substr($indice,12 )] = $par['f_code'];
                    }
                }
                foreach ($selettori as $k=>$arr) {
                    foreach ($arr as $k2=>$code) {
                        $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'][] = $code;
                    }
                }
                
                $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'] = array_unique($params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code']);
                $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code_main'] = $f_code;
            }
            
            
            //die(var_dump($selectors,$codes));
            Mainsim_Model_Utilities::updateAuxSelectorFields('workorders', $f_code, $this->db, array_values(array_unique($selectors)), isset($codes) ? $codes : null);
            //Mainsim_Model_Utilities::updateAuxSelectorFields($params, 'workorders', $f_code, $this->db, isset($selectorKey) ? $params[$selectorKey]['f_code'] : null, isset($params['t_wares_1']['f_code']) ? $params['t_wares_1']['f_code'] : null);
            //FINE NICO 22/06/2018
            
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", ["f_timestamp" => $time], "Edit");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_workorders", ["f_timestamp" => $time], "Edit");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", ["f_timestamp" => $time], "Edit");

            if ($commit) {
                //if some script raise an error, return it
                if (!empty($error['message'])) {
                    $this->db->rollBack();
                    return $error;
                }
                $this->db->commit();
            }
        } catch (Zend_Db_Exception $e) { //Catching exceptions on DB
            // elcarbo 01/01/2019
            if(strpos($e->getMessage(), 'Deadlock') !== false){
                die("Errore: modifiche concorrenti. Riprovare il salvataggio");
            }
            // LUCA 12/09/2017
            // Get if setting debug is on 
            $sel->reset();
            $debug = $sel->from("t_creation_date", array("f_title", "f_description"))
                            ->where("f_type = 'SYSTEMS'")->where("f_category = 'SETTING'")->where("f_title = 'DEBUG_ON'")->query()->fetch();

            // Basically return error only if debug is on and die() anyway
            // But only if the setting is actually there, otherwise normal behaviour
            if (isset($debug['f_description']) && $debug['f_description'] == 1) {
                return $e->getMessage();
                die();
            } else if (isset($debug['f_description'])) {
                die();
            }
        } catch (Exception $e) {
            if ($commit) {
                $this->db->rollBack();
            }
            $trsl = new Mainsim_Model_Translate(null, $this->db);
            $trsl->setLang($trsl->getUserLang($userinfo->f_language));
            $this->db->insert("t_logs", array('f_timestamp' => time(), 'f_log' => "Error in " . __METHOD__ . " : " . Mainsim_Model_Utilities::chg($e->getMessage()), 'f_type_log' => 0));
            $error['message'] = $trsl->_("Error") . ": " . $trsl->_($e->getMessage());
            print_r($e->getMessage());
            return $error;
        }
        Mainsim_Model_Utilities::saveReverseAjax("t_workorders_parent", $f_code, [], "upd", $module_name, $this->db);
        return $error;
    }

    private function saveCrossAndPair($f_code, &$params, $scriptModule, $editBatch = false) {
        $select = new Zend_Db_Select($this->db);
        $time = Zend_Registry::get('time');
        $pairCols = Mainsim_Model_Utilities::get_tab_cols('t_pair_cross');
        $f_type_id = $params['f_type_id'];
        $find = false; 
        //die(var_dump($params));
        foreach ($params as $key => $value) {
            if (!is_array($value) || (empty($value['f_code']) && $editBatch)) {
                // NICO: lascio eseguire per i selettori soltanto o per chi ha pck = true
                if (!(strpos($key, "t_selectors_") !== false && $editBatch)) {
                    if ((strpos($key, "t_wares_") !== false && $editBatch)) {
                        if(is_array($value)&&(!isset($value['pck'])||!$value['pck'])) {
                                continue;
                        }
                    } else {
                        continue;
                    }
                }
            }
            $ftype = $value['f_type'];
            //parent array
            if (strpos($key, "t_workorders_parent") !== false) {
                $parentCodes = empty($value['f_code']) ? [0] : $value['f_code'];
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($parentCodes, 't_workorders_parent', $f_code, 'f_code', [], 'f_parent_code');
                if (!$checkIdenticalCross) {
                    $this->db->delete("t_workorders_parent", "f_code = $f_code");
                    Mainsim_Model_Utilities::createParents('t_workorders_parent', $f_code, $value['f_code'], $this->db);
                }
                $find = true;
            } elseif (strpos($key, "t_workorders") !== false && !empty($value)) {
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($value['f_code'], 't_wo_relations', $f_code, 'f_code_wo_master', [], 'f_code_wo_slave', 't_workorders', $ftype);
                if (!$checkIdenticalCross) {
                    $this->db->delete("t_wo_relations", "f_code_wo_master = $f_code and f_type_id_wo_slave IN ($ftype)");
                    $this->db->delete("t_wo_relations", "f_code_wo_slave = $f_code and f_type_id_wo_master IN ($ftype)");
                    array_walk($value['f_code'], function($el) use($f_code, $ftype, $f_type_id) {
                        $this->createWoCross($f_code, $el, $f_type_id, $ftype);
                    });
                }
            }

            if (strpos($key, "t_wares_") !== false && !empty($value)) {
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($value['f_code'], 't_ware_wo', $f_code, 'f_wo_id', array(), 'f_ware_id', 't_wares', $ftype);
                /* NICO 31/10/2018: Alla creazione il ware non deve piu prendere la cross diretta col selettore dell'asset
                 
                if ($key == "t_wares_1") {
                    //connect wares selector with wo     
                    $params_keys = array_keys($params);
                    $not_found = true;
                    foreach ($params_keys as $key_searched) {
                        if (strpos($key_searched, "t_selectors_") !== false && !empty($params[$key_searched]['f_code'])) {
                            $not_found = false;
                            break;
                        }
                    }
                    if ($not_found && !$checkIdenticalCross) { // if no selector has been inserted and asset has been modified, assign the selector of the asset
                        $this->db->delete("t_selector_wo", "f_wo_id = $f_code"); //remove old selector ware cross
                        $this->createSelectorCross($f_code, $value['f_code']);
                    }
                 
                }
                 */
                if (!empty($value['f_module_name'])) {
                    $get_pair_book = Mainsim_Model_Utilities::getBookmark($value['f_module_name']);
                    if (!empty($get_pair_book)) {
                        $pair_book = json_decode(Mainsim_Model_Utilities::chg($get_pair_book['f_properties']), true);
                        foreach ($pair_book as $line_book) {
                            $select->reset();
                            $field = $line_book[0];
                            if (!in_array($field, $pairCols))
                                continue;
                            $res_search = $select->from("t_scripts")->where("f_name = ?", $field)->where("f_type = 'php'")->query()->fetch();
                            if (!empty($res_search['f_script'])) {
                                eval($res_search['f_script']);
                            } elseif (method_exists($scriptModule, $field) && is_callable(array($scriptModule, $field))) {//if not exist try to check if is a custom script in script model
                                $err_script = $scriptModule->$field();
                                if (isset($err_script['message']))
                                    throw new Exception($err_script['message']);
                            }
                        }
                    }
                }
                if (!empty($ftype) && !$checkIdenticalCross) { //delete previouse record and change with new                        
                    $this->db->delete("t_ware_wo", "f_wo_id = {$f_code} AND f_ware_id in (select f_code from t_wares where f_type_id IN ({$ftype}) ) ");
                }

                if (!$checkIdenticalCross && (($ftype != 10 && $ftype > 0) || !$value['pairCross'])) {
                    Mainsim_Model_Utilities::createCross($this->db, 't_ware_wo', $f_code, 'f_wo_id', $value['f_code'], 'f_ware_id');
                } elseif (!$checkIdenticalCross && $ftype == 10) {
                    $group_codes = array_map(function($v) {
                        return $v['f_group_code'];
                    }, $value['f_pair']);
                    Mainsim_Model_Utilities::createCross($this->db, 't_ware_wo', $f_code, 'f_wo_id', $group_codes, 'f_ware_id');
                }

                //delete olds pair cross
                if ($ftype > 0) {
                    $this->db->delete("t_pair_cross", "f_code_main = {$f_code} and f_code_cross IN (SELECT f_code from t_wares where f_type_id = {$value['f_type']})");
                } else { // delete pair cross for periodic maintenance
                    $this->db->delete("t_pair_cross", "f_code_main = {$f_code} and f_code_cross = {$value['f_type']}");
                }
                if (isset($value['f_pair'])) {
                    foreach ($value['f_pair'] as $f_pair) {
                        if (is_null($f_pair))
                            continue;
                        $f_pair_params = array();
                        foreach ($f_pair as $key_pair => $line_pair) {
                            if ($key == '' || !in_array($key_pair, $pairCols))
                                continue;
                            $f_pair_params[$key_pair] = $line_pair;
                        }
                        unset($f_pair_params['f_id']);
                        $f_pair_params['f_code_main'] = $f_code;

                        $f_pair_params = Mainsim_Model_Utilities::clearChars($f_pair_params);
                        $f_pair_params = Mainsim_Model_Utilities::clearEmptyValuesParams($f_pair_params);
                        
                        //aggiunto da elCarbo il 14/09/2018
                        if(isset($value['f_module_name']) && isset($value['pairCross']) && $value['f_module_name'] == 'mdl_wo_rsc_tg' && $value['pairCross'] == 0 && $value['f_type'] == 2 && empty($f_pair['fc_rsc_cost']))  
                            $f_pair_params['fc_rsc_cost'] = round($f_pair['fc_rsc_hours']*Mainsim_Model_Utilities::getLaborFee($f_pair['f_code_cross'],$f_pair['fc_rsc_fee_type'],$this->db),2);
                        
                        $this->newPairCross($f_pair_params, $value['pairCross']);
                    }
                    
                    //aggiunto da elCarbo il 14/01/2019
                    //aggiorno i campi 'fc_total_costs','fc_costs_for_downtime', 'fc_costs_for_labor','fc_costs_for_service'
                    if(isset($value['f_module_name']) && isset($value['pairCross']) && $value['pairCross'] == 0 && isset($value['f_type']) && in_array($value['f_type'], [1,2,24]))
                    {
                        $res_pcs = Mainsim_Model_Utilities::getPairCrossByType($f_code,$this->db);
                        //print_r($res_pcs); die("respcs");
                        $pcs_assets = (empty($res_pcs)||empty($res_pcs['1']))?array():$res_pcs['1'];
                        $pcs_labors = (empty($res_pcs)||empty($res_pcs['2']))?array():$res_pcs['2'];
                        $pcs_supply = (empty($res_pcs)||empty($res_pcs['24']))?array():$res_pcs['24'];
                        $fc_total_costs = 0;
                        $fc_costs_for_downtime = (empty($pcs_assets))?0:array_sum(array_column($pcs_assets, 'fc_asset_downtime_cost'));
                        $fc_costs_for_labor = (empty($pcs_labors))?0:array_sum(array_column($pcs_labors, 'fc_rsc_cost'));
                        $fc_costs_for_service = (empty($pcs_supply))?0:array_sum(array_column($pcs_supply, 'fc_supply_total_cost'));

                        $fc_total_costs = $fc_costs_for_downtime+$fc_costs_for_labor+$fc_costs_for_service;
                        $vtoupdate = ['fc_costs_for_downtime' => $fc_costs_for_downtime,'fc_costs_for_labor' => $fc_costs_for_labor,
                            'fc_costs_for_service' => $fc_costs_for_service,'fc_total_costs' => $fc_total_costs];
                        //if($value['f_type'] == 2) {print_r($vtoupdate); die("zioporcello");};
                        $this->db->update('t_custom_fields',$vtoupdate, 'f_code = '.$f_code);
                    }  

                }
            }

            if (strpos($key, "t_selectors_") !== false) {
                $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($value['f_code'], 't_selector_wo', $f_code, 'f_wo_id', array(), 'f_selector_id', 't_selectors', $ftype);
                if (!$checkIdenticalCross) {
                    if (in_array($params['f_type_id'], [3, 9, 11, 15, 19])) {
                        $this->manageMaintenancePlanSelectors($f_code, $value['f_code']);
                    }
                    $this->db->delete("t_selector_wo", "f_wo_id = $f_code and f_selector_id in (select f_code from t_selectors where f_type_id IN ({$value['f_type']}) )"); //remove old selector wo cross
                    if (!empty($value['f_code'])) { //create (if passed) new cross
                        Mainsim_Model_Utilities::createSelectorCross($this->db, $f_code, $value['f_code'], "t_selector_wo", "f_wo_id");
                    }
                }
            }
        }

        //if not found, create workorders_parent array list        
        if (!$find) {
            if ($params['f_code'] == 0) {
                $this->db->insert('t_workorders_parent', ['f_code' => $f_code, 'f_parent_code' => 0, 'f_active' => 1, 'f_timestamp' => $time]);
            }
            $select->reset();
            $res_parent = $select->from("t_workorders_parent", 'f_parent_code')->where("f_active = 1")
                            ->where("f_code = ?", $f_code)->query()->fetchAll();
            $params['t_workorders_parent_' . $params['f_type_id']]['f_code'] = array();
            foreach ($res_parent as $line_parent) {
                $params['t_workorders_parent_' . $params['f_type_id']]['f_code'][] = $line_parent['f_parent_code'];
            }
        }
    }

    public function manageMaintenancePlanSelectors($code, $selectors_code) {
        $select = new Zend_Db_Select($this->db);
        $current_selectors = $select->from(["t1" => "t_selector_wo"], [])
                        ->join(["t2" => "t_selectors"], "t1.f_selector_id = t2.f_code", ['f_code'])
                        ->where("f_type_id = 11")->where("f_wo_id = ?", $code)
                        ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

        $select->reset();
        $new_selectors = !$selectors_code ? [] : $select->from("t_selectors", "f_code")
                        ->where("f_type_id = 11")->where("f_code IN (?)", $selectors_code)
                        ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

        //aggiorniamo eventuali selettori rimossi
        $selectors_to_remove = array_diff($current_selectors, $new_selectors);
        $select->reset();
        $select->from(["t1" => "t_wares"], "f_code")
                ->join(["t2" => "t_selector_ware"], "t1.f_code = t2.f_ware_id", []);
        foreach ($selectors_to_remove as $selector) {
            $select->reset(Zend_Db_Select::WHERE);
            $res_asset = $select->where("f_selector_id = ?", $selector)
                            ->where("f_type_id = 1")
                            ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

            if ($res_asset) {
                $asset_codes = implode(', ', $res_asset);
                $this->db->delete("t_ware_wo", sprintf("f_wo_id = %d and f_ware_id IN (%s)", $code, $asset_codes));
            }
        }

        //aggiungiamo eventuali nuovi selettori
        $selectors_to_add = array_diff($new_selectors, $current_selectors);
        if (!$selectors_to_add)
            return;

        $select->reset();
        $select->from(["t1" => "t_wares"], "f_code")
                ->join(["t2" => "t_selector_ware"], "t1.f_code = t2.f_ware_id", []);
        foreach ($selectors_to_add as $selector) {
            $select->reset(Zend_Db_Select::WHERE);
            $res_asset = $select->where("f_selector_id = ?", $selector)
                            ->where("f_type_id = 1")
                            ->where("t1.f_code not IN (select f_ware_id from t_ware_wo where f_wo_id = ?)", $code)
                            ->query()->fetchAll(Zend_Db::FETCH_COLUMN);
            if ($res_asset) {
                Mainsim_Model_Utilities::createCross($this->db, 't_ware_wo', $code, 'f_wo_id', $res_asset, 'f_ware_id');
            }
        }
    }

    /** Salvataggio pairCross */
    public function newPairCross($params, $pairCross) {
        if ($pairCross == 1) {
            $f_group_code = $params['f_code_cross'];
            $params['f_code_cross'] = $params['f_group_code'];
            $params['f_group_code'] = $f_group_code;
        }
        $cross = array();
        $cross = $params;

        unset($cross['f_parent_code']);
        unset($cross['f_code']);
        $cross['f_timestamp'] = time();
        $this->db->insert("t_pair_cross", $cross);
    }

    /* ADDED BY ALESSANDRA 13/03/2012 */

    public function newWorkOrder($params, $commit = true, $userinfo = array()) {
        $error = array();
        $time = time();
        if (Zend_Registry::isRegistered('time')) {
            $time = Zend_Registry::get('time');
        } else {
            Zend_Registry::set('time', $time);
        }

        try {
            //if is a normal new wo, start transaction
            if ($commit) {
                $this->db->beginTransaction();
            }
            Zend_Registry::set('db_connection_editor', $this->db);
            if (empty($userinfo)) {
                $userinfo = Zend_Auth::getInstance()->getIdentity();
            }
            // mod 04/02/13 AC
            //editor info
            $params['fc_editor_user_name'] = $userinfo->fc_usr_firstname . ' ' . $userinfo->fc_usr_lastname;
            $params['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_editor_user_gender'] = $userinfo->f_gender;
            $params['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_editor_user_mail'] = $userinfo->fc_usr_mail;
            //creator info
            $params['fc_creation_user_name'] = $userinfo->fc_usr_firstname . ' ' . $userinfo->fc_usr_lastname;
            $params['fc_creation_user_avatar'] = $userinfo->fc_usr_avatar;
            $params['fc_creation_user_gender'] = $userinfo->f_gender;
            $params['fc_creation_user_phone'] = $userinfo->fc_usr_phone;
            $params['fc_creation_user_mail'] = $userinfo->fc_usr_mail;
            $params['fc_creation_date'] = $time;
            //var_dump($params); die("fc_progress");
            
            if(isset($params['fc_progress']) && !empty($params['fc_progress']) && !in_array($params['f_type_id'],array(4,5,6,7,11,20))){
                $fc_progress = $params['fc_progress'];
            }
            else{
                $fc_progress = Mainsim_Model_Utilities::getFcProgress("t_workorders", $params['f_type_id'], $this->db);
            }
            // end mod 04/02/13 AC
            $columns = Mainsim_Model_Utilities::get_tab_cols("t_workorders");
            $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
            $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
            $pair_cols = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
            //recupero il nome del tipo di wo da inserire nel creation_date
            $select = new Zend_Db_Select($this->db);
            $result_wo = $select->from("t_workorders_types")->where("f_id = ?", $params['f_type_id'])->query()->fetch();

            // Insert into t_creation_date
            $new_cd = array();
            $select->reset();
            $res_order = $select->from("t_creation_date", array("f_order" => "MAX(f_order)"))
                            ->where("f_type = 'WORKORDERS'")->where("f_category = '{$result_wo['f_type']}'")
                            ->query()->fetch();
            $f_order = !empty($res_order) && !empty($res_order['f_order']) ? (int) $res_order['f_order'] : 0;

            // Insert into t_workorders        
            $custom_fields = array();
            $new_wo = array();
            $params = Mainsim_Model_Utilities::clearChars($params);
            $params = Mainsim_Model_Utilities::clearEmptyValuesParams($params);

            $owner = false;
            $config = Zend_Registry::get('db');
            if (strtolower($config->adapter) == 'sqlsrv') {
                $queryo1 = $this->db->fetchAll("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='t_workorders' AND COLUMN_NAME='fc_owner_id'");
            } else {
                $queryo1 = $this->db->fetchAll("SHOW COLUMNS FROM `t_workorders` LIKE 'fc_owner_id'");
            }
            if (sizeof($queryo1) > 0) {
                if (strtolower($config->adapter) == 'sqlsrv') {
                    $queryo1 = $this->db->fetchAll("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='t_workorders' AND COLUMN_NAME='fc_owner_avatar'");
                } else {
                    $queryo2 = $this->db->fetchAll("SHOW COLUMNS FROM `t_workorders` LIKE 'fc_owner_avatar'");
                }
                if (sizeof($queryo2) > 0) {
                    $owner = true;
                }
            }
            foreach ($params as $key => $value) {
                if (!$owner && ($key == 'fc_owner_id' || $key == 'fc_owner_avatar')) {
                    continue;
                }
                if (in_array($key, $creation_columns)) {
                    $new_cd[$key] = $value;
                } elseif (in_array($key, $columns)) {
                    $new_wo[$key] = $value;
                } elseif (in_array($key, $custom_columns)) {
                    $custom_fields[$key] = $value;
                }
            }
            $custom_fields['f_main_table'] = 't_workorders';
            $custom_fields['f_main_table_type'] = $params['f_type_id'];
            $new_cd["f_type"] = "WORKORDERS";
            $new_cd["f_category"] = $result_wo['f_type'];
            $new_cd["f_order"] = $f_order;
            $new_cd["f_timestamp"] = $time;
            $new_cd["f_creation_date"] = $time;
            $new_cd["f_creation_user"] = $userinfo->f_id;
            $new_cd['f_phase_id'] = isset($params['f_phase_id']) && !empty($params['f_phase_id']) ? $params['f_phase_id'] : $result_wo['f_wf_phase'];
            $new_cd['f_wf_id'] = isset($params['f_wf_id']) ? $params['f_wf_id'] : $result_wo['f_wf_id'];
            $new_cd['f_visibility'] = isset($params['f_visibility']) ? $params['f_visibility'] : -1;
            $new_cd['f_editability'] = isset($params['f_editability']) ? $params['f_editability'] : -1;
            $new_cd['fc_progress'] = $fc_progress;
            $module_name = $params['f_module_name'];
            $this->db->insert("t_creation_date", $new_cd);
            $f_code = $this->db->lastInsertId();


            //check if there are any unique to check
            if (isset($params['unique']) && !empty($params['unique'])) {
                foreach ($params['unique'] as $line_unique) {
                    Mainsim_Model_Utilities::isUnique($line_unique, $params, $params['f_module_name'], $this->db);
                }
            }

            $new_wo['f_user_id'] = $userinfo->f_id;
            $new_wo['f_code'] = $f_code;
            $new_wo['f_counter'] = !isset($params['f_counter']) ? 1 : $params['f_counter'];
            $new_wo['f_code_periodic'] = isset($params['f_code_periodic']) ? $params['f_code_periodic'] : 0;

            $new_wo['f_timestamp'] = $time;

            $new_wo['f_order'] = $f_order;
            $f_order++;

            $params['f_phase_id'] = $new_cd['f_phase_id'];
            if (!isset($new_wo['f_priority'])) {
                $new_wo['f_priority'] = 1;
            }

            $this->db->insert("t_workorders", $new_wo);

            // Insert custom fields        
            $custom_fields['f_code'] = $f_code;
            $custom_fields['f_timestamp'] = $time;
            $this->db->insert("t_custom_fields", $custom_fields);
            //var_dump($f_code); die("ccccc");
            $scriptModule = new Mainsim_Model_Script($this->db, "t_workorders", $f_code, $params, [], false, $userinfo);
            $this->saveCrossAndPair($f_code, $params, $scriptModule);
            
            //check if there are attachments and add into the system
            Mainsim_Model_Utilities::saveAttachments($f_code, 't_workorders', $params, $scriptModule);

            //execute change phase script
            $select->reset();
            $res_phase = $select->from("t_wf_exits")->where("f_phase_number = 0")
                            ->where("f_exit = 1")->where("f_wf_id = ?", $new_cd['f_wf_id'])->query()->fetch();
            //var_dump($res_phase); 
            if (!empty($res_phase) && $res_phase['f_script'] != "") {
                eval($res_phase['f_script']);
            }


            //----- check if there is one or more script linked to field -----
            $res_bm = Mainsim_Model_Utilities::getBookmark($module_name, "view", $userinfo->f_id);
            $bm = json_decode(Mainsim_Model_Utilities::chg($res_bm['f_properties']), true);
            foreach ($bm as $line_bm) {
                if (!array_key_exists($line_bm[0], $params)) {
                    $params[$line_bm[0]] = '';
                }
            }

            foreach ($params as $column => $value) {
                $select->reset();
                $res_script_exist = $select->from("t_scripts")->where("f_name = ?", $column . '-t_workorders')->where("f_type = 'php'")->query()->fetch();
                if (!empty($res_script_exist['f_script'])) {
                    eval($res_script_exist['f_script']);
                } elseif (method_exists($scriptModule, $column) && is_callable(array($scriptModule, $column))) {//if not exist try to check if is a custom script in script model                                                                                
                    $scriptModule->$column();
                }
            }

            //if this is a pm or meter, re-create periodics            
            if ($params['f_type_id'] == 3 || $params['f_type_id'] == 11 || $params['f_type_id'] == 19) {
                $new_wo['f_title'] = $params['f_title'];
                $res_pm = $this->createPeriodics($f_code);
                if (!$res_pm) {
                    throw new Exception("There is an error in frequency settings");
                }
            }

            // update auxiliary fields for selectors 
            $selectors = [];    
            foreach ($params as $key => $value) {
                if (strpos($key, 't_selectors_') !== false) {
                    $selectors = array_merge($selectors, $params[$key]['f_code']);
                }
            }
            
            // NICO 22/06/2018
            $exclude = ["USER"]; //Escludo i types dei quali non voglio andare ad associare i selettori in fc_hierarchy_selectors
            $select->reset();
            $ids = $select->from("t_wares_types", "f_id")
                        ->where("f_type not in (?)", $exclude)
                        ->query()->fetchAll(Zend_Db::FETCH_COLUMN);
            $codes = [];
            foreach ($params as $key=>$value) {
                if (strpos($key, 't_wares_') !== false) {
                    $type = explode('_', $key)[2]; // guardo il tipo
                    if (in_array($type,$ids)) {
                        foreach ($value['f_code'] as $value2) {
                            $codes[] = intval($value2);
                        }
                    }
                }
            }
            Mainsim_Model_Utilities::updateAuxSelectorFields('workorders', $f_code, $this->db, array_values(array_unique($selectors)), isset($codes) ? $codes : null);
            //Mainsim_Model_Utilities::updateAuxSelectorFields($params, 'workorders', $f_code, $this->db, isset($selectorKey) ? $params[$selectorKey]['f_code'] : null, isset($params['t_wares_1']['f_code']) ? $params['t_wares_1']['f_code'] : null);
            //FINE NICO 22/06/2018
            
            Mainsim_Model_Utilities::saveReverseAjax("t_workorders_parent", $f_code, [0], "add", $module_name, $this->db);
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", ["f_timestamp" => $time], "New Record");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_workorders", ["f_timestamp" => $time], "New Record");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", ["f_timestamp" => $time], "New Record");
            if ($commit) {
                if (!isset($error['message']))
                    $this->db->commit();
                else {
                    $this->db->rollBack();
                    return $error;
                }
            }
            if (!empty($params['attachments'])) {
                Mainsim_Model_Utilities::clearTempAttachments($params, $this->db);
            }
        } catch (Zend_Db_Exception $e) { //Catching exceptions on DB
            // LUCA 12/09/2017
            // Get if setting debug is on 
            $select = new Zend_Db_Select($this->db);
            $debug = $select->from("t_creation_date", array("f_title", "f_description"))
                            ->where("f_type = 'SYSTEMS'")->where("f_category = 'SETTING'")->where("f_title = 'DEBUG_ON'")->query()->fetch();

            // Basically return error only if debug is on and die() anyway
            // But only if the setting is actually there, otherwise normal behaviour
            if (isset($debug['f_description']) && $debug['f_description'] == 1) {
                return $e->getMessage();
                die();
            } else if (isset($debug['f_description'])) {
                die();
            }
        } catch (Exception $e) {

            $prefix = "";
            $trsl = new Mainsim_Model_Translate(null, $this->db);
            $trsl->setLang($trsl->getUserLang($userinfo->f_language));
            if ($commit) {
                $prefix = $trsl->_("Error") . ":";
                $this->db->rollBack();
            }
            $this->db->insert("t_logs", array('f_timestamp' => time(), 'f_log' => "Error in " . __METHOD__ . " : " . $e->getMessage(), 'f_type_log' => 0));
            $error['message'] = $prefix . Mainsim_Model_Utilities::chg($e->getMessage());
            return $error;
        }
        return array('f_code' => $f_code);
    }

    public function newWoFromWizard($params) {
        try {
            $this->db->beginTransaction();
            $sel = new Zend_db_Select($this->db);

            // get user info for old wizard
            if ($params['wiz_old']) {
                $userinfo = $sel->from('t_creation_date', array('f_displayedname' => 'f_title', "creation_account" => "f_creation_date"))
                                ->join('t_users', 't_creation_date.f_id = t_users.f_code', array(
                                    'f_id' => 'f_code', 'f_level' => 'fc_usr_level',
                                    'fc_usr_level_text' => 'fc_usr_level_text',
                                    'f_group_level' => 'fc_usr_group_level', 'f_status' => 'fc_usr_status',
                                    'fc_usr_usn', 'fc_usr_pwd', 'fc_usr_firstname', 'fc_usr_lastname', 'fc_usr_mail',
                                    'f_gender' => 'fc_usr_gender', 'f_language' => 'fc_usr_language', 'fc_usr_status',
                                    'fc_usr_usn_registration', 'fc_usr_pwd_registration', 'fc_usr_usn_expiration',
                                    'fc_usr_pwd_expiration', 'fc_usr_avatar', 'fc_usr_address', 'fc_usr_phone', 'fc_usr_mobile_autoswitch_desktop'))
                                ->where('t_users.fc_usr_usn="utenza"')->query()->fetchObject();
            }

            if (empty($userinfo)) {
                $userinfo = Zend_Auth::getInstance()->getIdentity();
            }

            $userData = Zend_Auth::getInstance()->getIdentity();
            // transform userinfo array to object for mobile  users
            if (is_array($userData)) {
                $aux = $userData;
                $userData = new stdClass();
                foreach ($aux as $key => $value) {
                    if ($key == 'f_code') {
                        $userData->f_id = $value;
                    }
                    if ($key == 'fc_usr_level') {
                        $userData->f_level = $value;
                    } else {
                        $userData->{$key} = $value;
                    }
                }
            } else {
                $userData = Zend_Auth::getInstance()->getIdentity();
            }
            $userinfo = $userData;
            if (!isset($params['f_type_id'])) {
                $params['f_type_id'] = 1;
            }
            if (!isset($params['f_module_name'])) {
                $params['f_module_name'] = 'mdl_wo_tg';
            }

            //Aggiunta la possibilità di associare un selettore direttamente da wizard passando il parametro t_selectors:f_code
            foreach ($params as $key => $value) {

                if (strpos($key, "t_wares") !== false) {
                    $t_wares_f_codes = explode(',', $value);
                    foreach ($t_wares_f_codes as $k => $v) {
                        $sel->reset();
                        $resType = $sel->from('t_wares', 'f_type_id')->where('f_code = ?', $v)->query()->fetch();
                        if (empty($resType)) {
                            continue;
                        }
                        //if (!isset($params["t_wares_{$resType['f_type_id']}"]) || !is_array($params["t_wares_{$resType['f_type_id']}"])) {
                        if (!isset($params["t_wares_{$resType['f_type_id']}"])) {
                            $params["t_wares_{$resType['f_type_id']}"] = array(
                                "f_code" => array(),
                                "f_pair" => array(),
                                "f_code_main" => 0,
                                "f_type" => $resType['f_type_id'],
                                "Ttable" => "t_wares",
                                "pairCross" => 0,
                            );
                        }
                        $params["t_wares_{$resType['f_type_id']}"]['f_code'][] = $v;
                    }
                } else if (strpos($key, "t_selectors") !== false) {
                    $sel->reset();
                    $resType = $sel->from('t_selectors', 'f_type_id')->where('f_code = ?', $value)->query()->fetch();
                    if (empty($resType)) {
                        continue;
                    }
                    if (!isset($params["t_selectors_{$resType['f_type_id']}"]) || !is_array($params["t_selectors_{$resType['f_type_id']}"])) {
                        $params["t_selectors_{$resType['f_type_id']}"] = array(
                            "f_code" => array(),
                            "f_pair" => array(),
                            "f_code_main" => 0,
                            "f_type" => $resType['f_type_id'],
                            "Ttable" => "t_selectors",
                            "pairCross" => 0,
                        );
                    }
                    $params["t_selectors_{$resType['f_type_id']}"]['f_code'][] = $value;
                }
            }
            //save attacched docs (if exists)
            if (!empty($params['wiz_attach'])) {
                $params['t_wares_5'] = [
                    "f_code" => $this->saveNewDocFromWiz($params['wiz_attach']),
                    "f_pair" => [],
                    "f_code_main" => 0,
                    "f_type" => 5,
                    "Ttable" => "t_wares",
                    "pairCross" => 0,
                ];
            }

            // cross docs
            if (!empty($params['set_attach'])) {
                $afc = explode(",", $params['set_attach']);

                if (count($afc)) {
                    if (!isset($params['t_wares_5'])) {
                        $params['t_wares_5'] = [
                            "f_code" => $afc,
                            "f_pair" => [],
                            "f_code_main" => 0,
                            "f_type" => 5,
                            "Ttable" => "t_wares",
                            "pairCross" => 0,
                        ];
                    } else {

                        $params['t_wares_5']["f_code"] = array_merge($params['t_wares_5']["f_code"], $afc);
                    }
                }
            }

            $res = $this->newWorkOrder($params, false, $userinfo);
            if (isset($res['message'])) {
                throw new Exception($res['message']);
            }
            $f_code = $res['f_code'];

            if ($params['ware_cross']) {
                $this->db->query("INSERT INTO t_ware_wo VALUES ('', " . $params['ware_cross'] . ", " . $f_code . ", " . time() . ")");
            }

            // add documents
            $objWare = new Mainsim_Model_Wares($this->db);
            foreach ($params as $key => $value) {
                if (strpos($key, 'picture_') !== false) {
                    $objWizard = new Mainsim_Model_Wizard();
                    $aux = explode("|", $value);
                    $aux2 = explode(".", $aux[0]);
                    $documentTimestamp = time();
                    $params = array(
                        "f_type_id" => 5,
                        "f_wf_id" => 1,
                        "f_code" => 0,
                        "f_title" => $aux2[0],
                        "fc_doc_attach" => $documentTimestamp . "_" . $userinfo->f_id . "|" . $aux[0],
                        "f_module_name" => "mdl_doc_tg"
                    );
                    $newWare = $objWare->newWares($params, $userinfo, false);
                    // create association workorder-ware
                    $this->db->insert('t_ware_wo', array('f_ware_id' => $newWare['f_code'], 'f_wo_id' => $f_code, 'f_timestamp' => $documentTimestamp));
                    $objWizard->saveAttachment($aux[1], $newWare['f_code'], $documentTimestamp, $aux[0]);
                    // update fc_documents
                    $this->db->update('t_creation_date', array('fc_documents' => new Zend_Db_Expr('fc_documents + 1')), 'f_id = ' . $f_code);
                }
            }
            // add paircross
            if (isset($params['paircross'])) {
                $objWizard = new Mainsim_Model_Wizard($this->db);
                $objWizard->addPairCross(json_decode($params['paircross'], true), $f_code);
            }

            $this->db->commit();
        } catch (Exception $e) {
            $this->db->rollBack();
            $this->db->insert("t_logs", array('f_timestamp' => time(), 'f_log' => "Error in " . __METHOD__ . " : " . $e->getMessage(), 'f_type_log' => 0));
            return array('message' => $e->getMessage());
        }
        return array();
    }

    /**
     * 
     * @param type $f_attach
     */
    private function saveNewDocFromWiz($f_attach) {
        $wa = new Mainsim_Model_Wares($this->db);
        $f_attach = str_replace("”", '"', $f_attach);
        $list = json_decode($f_attach, true);
        $codes = [];
        $tot = count($list);
        $params = ['f_code' => 0, 'f_type_id' => 5, 'f_module_name' => 'mdl_doc_tg', 'attachments' => ['fc_doc_attach']];
        for ($i = 0; $i < $tot; ++$i) {
            $params['f_title'] = $list[$i]['name'];
            $params['fc_doc_attach'] = $list[$i]['url'];
            $res = $wa->newWares($params, [], false);
            if (!isset($res['f_code'])) {
                die('error');
            }
            $codes[] = $res['f_code'];
        }
        return $codes;
    }

    private function getWO($f_code, $table = 't_workorders', $field = 'f_code') {
        $select = new Zend_Db_Select($this->db);
        $select->from($table)->where("$field = ?", $f_code);
        $result = $select->query()->fetchAll();
        return $result[0];
    }

    /**
     * Cross workorders with selectors using selectors linked with asset
     * @params $f_code int wo's f_code
     * @params $assetCodes array list of assets
     */
    private function createSelectorCross($f_code, $assetCodes) {
        $select = new Zend_Db_Select($this->db);
        $tot = count($assetCodes);
        for ($i = 0; $i < $tot; ++$i) {
            $select->reset();
            $assetCodeList = array($assetCodes[$i]);
            $assetCodeList = Mainsim_Model_Utilities::getParentCode("t_wares_parent", $assetCodes[$i], $assetCodeList, $this->db);
            $res = $select->from("t_selector_ware", "f_selector_id")->where("f_ware_id IN (?)", $assetCodeList)
                            ->join(['t2' => "t_selectors"], "f_selector_id = t2.f_code", array())
                            ->where("f_type_id != 11") // escludo i selettori per i piani manutentivi
                            ->group("f_selector_id")->query()->fetchAll();
            foreach ($res as &$cross) {
                $cross['f_wo_id'] = $f_code;
                $cross['f_timestamp'] = time();
                $cross['f_nesting_level'] = 1;
                $insertData = array(
                    'f_selector_id' => $cross['f_selector_id'],
                    'f_wo_id' => $f_code,
                    'f_timestamp' => time(),
                    'f_nesting_level' => 1
                );
                $this->db->insert('t_selector_wo', $insertData);
            }
        }
    }

    /**
     * changePhaseBatch
     * @param type $f_codes
     * @param type $f_wf_id
     * @param type $f_exit
     * @param type $f_module_name
     * @return type
     */
    public function changePhaseBatch($f_codes, $f_wf_id, $f_exit, $f_module_name) {
        return $this->obj->changePhase($f_codes, $f_wf_id, $f_exit, $f_module_name);
    }

    /**
     * Create periodics
     * @param int $f_code : f_code periodic
     * @param int $start_date : [optional]custom start date
     * @param bool $force_recreate : [optional] force periodics regeneration
     */
    public function createPeriodics($f_code, $start_date = 0, $asset_list = null, $force_recreate = false, $manual_counter = false, $floating_shift = false) {
        //create periodics
        $sch = new Mainsim_Model_Scheduler($this->db);
        $select = new Zend_Db_Select($this->db);
        $engagement = $select->from("t_workorders", array(
                    'fc_pm_at_time', 'fc_pm_duration', 'fc_pm_forewarning',
                    'fc_pm_start_date', 'fc_pm_end_date',
                    'fc_pm_cyclic_number', 'fc_pm_sequence',
                    'fc_pm_cyclic_type', 'fc_pm_periodic_type', 'fc_pm_fixed', 'fc_pm_type'
                ))->where("f_code = ?", $f_code)->query()->fetch();
        if ($start_date) {
            //setto a "letto" tutte le periodiche fino alla nuova start date.
            if ($asset_list != null) {
                $this->db->update("t_periodics", array("f_executed" => 1), "f_code = " . $f_code . " and f_start_date <= " . $start_date . " and f_executed = 0 AND f_asset_code = " . array_shift(array_keys($asset_list)));
            } else {
                $this->db->update("t_periodics", array("f_executed" => 1), "f_code = " . $f_code . " and f_start_date <= " . $start_date . " and f_executed = 0");
            }
            $engagement['fc_pm_start_date'] = $start_date;
        }
        //rigenero se è cambiata la ciclità, se qualcuno ha forzato la generazione oppure è un wo multiplo
        if ($force_recreate || $engagement['fc_pm_type'] == 'pm-per-asset' || $this->checkPreviousEngagement($f_code, $engagement)) {
            $this->db->update("t_workorders", array(
                "fc_pm_next_due_date" => null,
                "fc_pm_subsequent_due_date" => null), "f_code  = {$f_code}");

            if ($engagement['fc_pm_type'] == 'pm-per-asset' && !$asset_list) { //l'attività va schedulata su ogni asset
                $select->reset();
                $asset_list = array();
                $res_assets = $select->from(array("t1" => "t_ware_wo"), array('f_ware_id'))
                                ->join(array('t2' => 't_wares'), 't1.f_ware_id = t2.f_code', array('fc_asset_installation_date'))
                                ->where('f_wo_id = ?', $f_code)->where("f_type_id = 1")
                                ->where('t2.fc_asset_installation_date > 0')
                                ->query()->fetchAll();
                foreach ($res_assets as $asset) {
                    $asset_list[$asset['f_ware_id']] = $asset['fc_asset_installation_date'];
                }

                //cancello tutti le periodiche con asset non più crossati alla pm
                $this->db->delete('t_periodics', sprintf("f_code = %d and f_executed = 0", $f_code, empty($asset_list) ? '' : ' and f_asset_code NOT IN (' . implode(', ', array_keys($asset_list)))
                );
            }

            $asset_list = $asset_list ?: [null]; //se non ho nulla, creo un array nullo per effettuare la ciclicità base

            foreach ($asset_list as $asset_code => $asset_timestamp) {
                //rimuovo tutte le periodics che non ho ancora eseguito
                $sch->clearPeriodicRows();
                $this->db->delete("t_periodics", "f_code  = {$f_code} and f_executed = 0 " . ($asset_code ? "and f_asset_code = " . $asset_code : ""));
                if ($asset_timestamp !== null) { // se è una pm per asset cambio la start date
                    $engagement['fc_pm_start_date'] = $asset_timestamp;
                    $sch->setAssetCode($asset_code);
                } elseif ($engagement['fc_pm_type'] == 'pm-per-asset') { // se è una pm per asset senza installation_date, vado avanti
                    continue;
                }

                switch ($engagement['fc_pm_periodic_type']) {
                    case '0':
                        $res = $sch->weekly($engagement, $f_code); //WEEKLY OK (TESTARE PER BENE)
                        break;
                    case '1':
                        $res = $sch->monthly($engagement, $f_code); //monthly
                        break;
                    case '2':
                        $res = $sch->periodic($engagement, $f_code, $floating_shift); //periodic
                        break;
                    case '3':
                        $res = $sch->dayofthemonth($engagement, $f_code); //MONTHLY OK (TESTARE PER BENE)
                        break;
                    case '4':
                        $res = $sch->ontimescheduled($engagement, $f_code); // ON TIME SCHEDULED
                        break;
                }
                if (isset($res['message'])) {
                    throw new Exception($res['message']);
                }
            }
            $this->addNextDueDate($f_code);
            //calculate new f_counter se il la PM non è impostata su MANUAL COUNTER
            $select->reset();
            if (!$manual_counter) {
                //AGGIUNTO 8/10/17 BALDINI: controllo che il conto di f_counter venga eseguito solamente per l'ultima modifica di 
                //engagement che modificato t_periodics
                $maxTmp = $select->from("t_periodics", array('maxTmp' => 'max(f_timestamp)'))
                                ->where("f_code = ?", $f_code)
                                ->query()->fetch();
                if ((int) ($maxTmp['maxTmp']) > 0) {
                    $select->reset();
                    $resCounter = $select->from("t_periodics", array('num' => 'count(*)'))
                                    ->where("f_code = ?", $f_code)->where("f_start_date - f_forewarning < ?", time())
                                    ->where("f_timestamp = ?", $maxTmp['maxTmp'])
                                    ->query()->fetch();
                }
                $counter = $resCounter['num'] > 0 ? (int) $resCounter['num'] : 1;
                $this->db->update("t_workorders", array('f_counter' => $counter), "f_code = $f_code");
            }

            $this->updateFcPmFrequency($f_code);
        }
        return true;
    }

    /**
     * Check if passed engagement is different from last version saved
     * @param int $f_code f_code of generatore
     * @param array $engagement engagement array
     */
    private function checkPreviousEngagement($f_code, $engagement) {
        $keys = array_keys($engagement);
        $select = new Zend_Db_Select($this->db);
        $res = $select->from('t_workorders_history', $keys
                        )->where('f_code = ?', $f_code)
                        ->order('f_id DESC')->limit(1)->query()->fetch();
        if (empty($res)) {
            return true;
        }
        $diff = $engagement == $res;
        return !$diff;
    }

    
    /**
     * Get the last periodic generator's scheduled pm betweeen two dates
     * @param int $f_code f_code of generatore
     * @param array $asset
     * @param int $fromDate 
     * @param int $toDate 
     * aggiunta da ElCarbo il 16/09/2019
     */
    public function getLastPm($f_code, $asset = null , $fromDate = null, $toDate = null, $order = 'desc') {
//       var_dump($f_code, $asset  , $fromDate , $toDate ); die;
        $select = new Zend_Db_Select($this->db);
        $res = $select->from(array("t1"=>"t_periodics"))
                ->join(array("t2"=>"t_workorders"), 't2.f_code = t1.f_code', array('f_type_id','fc_pm_type'))
                ->join(array('t3'=>"t_creation_date"), 't3.f_id = t2.f_code', [])
                ->join('t_wf_phases', 't3.f_wf_id = t_wf_phases.f_wf_id AND t3.f_phase_id = t_wf_phases.f_number', array())
                ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                ->where(sprintf("t1.f_code = %d", $f_code)
                        .(empty($asset) ? '' : ' and t1.f_asset_code = ' . $asset)
                        .(empty($fromDate) ? '' : ' and t1.f_start_date >= ' . $fromDate)
                        .(empty($toDate) ? '' : ' and t1.f_start_date <= ' . $toDate))
                ->where('t_wf_groups.f_id NOT IN (6,7)')
                ->order("t1.f_start_date $order")
                ->query()->fetchAll();
//        die($select->__toString());
        return $res;

    }
    
    
    /**
     * Set next due date and subsequent due date on generators fields
     * @param type $f_code f_code generator
     */
    public function addNextDueDate($f_code) {
        $select = new Zend_Db_Select($this->db);
        //search the first due date and subsequent due date
        $res_due = $select->from("t_periodics", array("f_start_date"))->where("f_code = ?", $f_code)
                        ->where("f_executed = 0")->where("f_start_date - f_forewarning > ?", time())
                        ->order("f_start_date ASC")->limit(2)->query()->fetchAll();

        $custom = array();
        if (empty($res_due)) {
            return;
        }
        $custom['fc_pm_next_due_date'] = $res_due[0]['f_start_date'];
        if (isset($res_due[1]['f_start_date'])) {
            $custom['fc_pm_subsequent_due_date'] = $res_due[1]['f_start_date'];
        }
        $this->db->update("t_workorders", $custom, "f_code = {$f_code}");
    }

    /**
     * Create custom frequency
     * @param type $f_code
     */
    //private function _fcPmFrequency($f_code)
    public function updateFcPmFrequency($f_code) {
        $select = new Zend_Db_Select($this->db);
        $res_pm = $select->from(array("t1" => "t_workorders"), array("f_code", "fc_pm_periodic_type", "fc_pm_sequence", "fc_pm_cyclic_number", "fc_pm_cyclic_type"))
                        ->where("t1.f_code = ?", $f_code)->query()->fetch();
        $line = $res_pm;
        $frequency = '';
        switch ($line['fc_pm_periodic_type']) {
            case 0: //weekly
                $seq = explode(',', $line['fc_pm_sequence']);
                $tot_day = count($seq);
                if ($tot_day == 1)
                    $frequency = 'WEEKLY';
                elseif ($tot_day == 2)
                    $frequency = 'BIWEEKLY';
                else
                    $frequency = $tot_day . ' TIMES A WEEK';
                break;
            case 1: //monthly
                switch ($line['fc_pm_cyclic_type']) {
                    case 4 : //year
                        if ($line['fc_pm_cyclic_number'] == 1)
                            $frequency = 'ANNUAL';
                        elseif ($line['fc_pm_cyclic_number'] == 2)
                            $frequency = 'BIANNUAL';
                        else
                            $frequency = $line['fc_pm_cyclic_number'] . ' YEARS';
                        break;
                }
                break;
            case 2: //periodic
                switch ($line['fc_pm_cyclic_type']) {
                    case 1: //day
                        if ($line['fc_pm_cyclic_number'] == 1)
                            $frequency = 'DAILY';
                        else
                            $frequency = "EVERY " . $line['fc_pm_cyclic_number'] . ' DAYS';
                        break;
                    case 2: //week
                        $frequency = $line['fc_pm_cyclic_number'] . ' WEEK';
                        break;
                    case 3: //month CONTROLLARE
                        if ($line['fc_pm_cyclic_number'] == 1)
                            $frequency = 'MONTHLY';
                        elseif ($line['fc_pm_cyclic_number'] == 1)
                            $frequency = 'BIMESTRAL';
                        elseif ($line['fc_pm_cyclic_number'] == 1)
                            $frequency = 'TRIMESTRAL';
                        elseif ($line['fc_pm_cyclic_number'] == 3)
                            $frequency = 'QUATERLY';
                        elseif ($line['fc_pm_cyclic_number'] == 6)
                            $frequency = 'HALF-YEARLY';
                        else
                            $frequency = $line['fc_pm_cyclic_number'] . ' MONTHS';
                        break;
                    case 4: //year                            
                        if ($line['fc_pm_cyclic_number'] == 1)
                            $frequency = 'ANNUAL';
                        elseif ($line['fc_pm_cyclic_number'] == 2)
                            $frequency = 'BIANNUAL';
                        else
                            $frequency = $line['fc_pm_cyclic_number'] . ' YEARS';
                        break;
                }//alfredo sei un viscido di merda
                break;
            case 3: $frequency = 'MONTHLY';
                break;
        }
        $this->db->update("t_workorders", array("fc_pm_frequency" => $frequency), "f_code = {$f_code}");
    }

    /**
     * generate wo from stw or oc
     * @param type $f_code
     * @return string 
     */
    public function createWo($f_code) {
        $select = new Zend_Db_Select($this->db);
        //check if is an oc without condition
        $resValid = $select->from("t_workorders", array("f_type_id", "fc_cond_condition"))
                        ->where("f_code = ?", $f_code)->query()->fetch();
        /* 	
          if($resValid['f_type_id'] == 9 && empty($resValid['fc_cond_condition']) ){
          return 0;
          } */
        $periodic = array(
            'f_code' => $f_code,
            'f_start_date' => time()
        );
        return $this->initializeWoGeneration($periodic);
    }

    public function reopenedClose($codes = array(), $module_name = '') {
        $changed = 0;
        $select = new Zend_Db_Select($this->db);
        $this->db->beginTransaction();
        $not_changed = array();
        foreach ($codes as $code) {
            $select->reset();
            $res_closed = $select->from(array("t1" => "t_workorders"), array('f_code'))
                            ->join(array("t2" => "t_wf_phases"), "t1.f_wf_id = t2.f_wf_id and t2.f_number = t1.f_phase_id", array())
                            ->join(array("t3" => "t_wf_groups"), "t2.f_group_id = t3.f_id", array())
                            ->where("t1.f_code = ?", $code)->where("t3.f_id = 6")
                            ->query()->fetch();
            if (empty($res_closed)) {
                $not_changed[] = $code;
                continue;
            }

            try {
                $this->db->update("t_workorders", array("f_phase_id" => 1), "f_code = $code and f_active = 1");
            } catch (Exception $e) {
                $this->db->rollBack();
                return array('message' => $e->getMessage());
            }
            $changed++;
        }
        $this->db->commit();

        foreach ($codes as $code) {
            if (in_array($code, $not_changed))
                continue;
            Mainsim_Model_Utilities::saveReverseAjax('t_workorders_parent', $code, array(), "add", $module_name);
        }
        return $changed;
    }

    /**
     * Create Purchase Order
     * from material codes
     */
    public function openPo($f_codes, $f_type, $singlePO) {
        $select = new Zend_Db_Select($this->db);
        $codes = explode(',', $f_codes);
        $tot = count($codes);
        $this->db->beginTransaction();
        $params = array(
            'f_code' => 0,
            'f_module_name' => 'mdl_po_tg',
            'f_type_id' => 2,
            't_workorders_parent_2' => array(
                'f_code' => array(),
                'f_code_main' => 0,
                'f_type' => 2,
                'Ttable' => 't_workorders_parent',
                'f_module_name' => 'mdl_po_parent_tg'
            ),
            't_wares_3' => array(
                'f_code' => $codes,
                'f_code_main' => 0,
                'Ttable' => 't_wares',
                'pairCross' => 0,
                'f_module_name' => "mdl_po_material_tg",
                'f_pair' => array()
            )
        );
        $select->reset();

        $material_list = array();
        $res_info = $select->from(array("t1" => "t_creation_date"), array("f_title", "f_id"))
                        ->join(array("t2" => "t_wares"), "t1.f_id = t2.f_code", array(
                            'fc_material_avg_price', 'fc_material_package_type', 'fc_material_package_capacity'
                        ))->where("f_code IN ({$f_codes})")->query()->fetchAll();
        //if noone is a material, return
        if (empty($res_info))
            return array("Unknown materials passed");
        foreach ($res_info as $line_info) {
            $material_list[$line_info['f_id']] = $line_info;
        }

        if ($singlePO) {
            for ($i = 0; $i < $tot; ++$i) {
                $params['f_title'] = "Purchase Order for " . $material_list[$codes[$i]]['f_title'];
                $params['t_wares_3']['f_code'] = array($codes[$i]);
                $params['t_wares_3']['f_pair'] = array(
                    array(
                        'f_code' => -2,
                        'f_title' => 'Purcxxxhase Order',
                        'f_code_main' => 0,
                        'f_code_cross' => $codes[$i],
                        'fc_po_spare_ordered' => 1,
                        'fc_po_material_price' => !is_null($material_list[$codes[$i]]['fc_material_avg_price']) ? $material_list[$codes[$i]]['fc_material_avg_price'] : 0
                    )
                );
                $new = $this->newWorkOrder($params, false);
                if (isset($new['message'])) {
                    $this->db->rollBack();
                    return $new['message'];
                }
            }
        } else {
            $titles = array();
            for ($i = 0; $i < $tot; ++$i) {
                $titles[] = $material_list[$codes[$i]]['f_title'];
                $params['t_wares_3']['f_pair'][] = array(
                    'f_code' => -2,
                    'f_title' => 'Purchase Order',
                    'f_code_main' => 0,
                    'f_code_cross' => $codes[$i],
                    'fc_po_spare_ordered' => 1,
                    'fc_po_material_price' => !is_null($material_list[$codes[$i]]['fc_material_avg_price']) ? $material_list[$codes[$i]]['fc_material_avg_price'] : 0
                );
            }
            $params['f_title'] = "Purchase Order for " . implode(', ', $titles);
            $new = $this->newWorkOrder($params, false);
            if (isset($new['message'])) {
                $this->db->rollBack();
                return $new['message'];
            }
        }
        $this->db->commit();
        return array();
    }

    /**
     * check if passed wo is a wo generated and if generator is float.
     * If true, shift all periodic maintenance
     */
    public function checkPeriodicShift($f_code) {
        $select = new Zend_Db_Select($this->db);
        $objUtils = new Mainsim_Model_Utilities();
        $resWo = $select->from(['t1' => 't_workorders'], [
                            'f_code_periodic', 'f_start_date', 'f_end_date', 'fc_wo_ending_date', 'fc_wo_starting_date'])
                        ->join(['t2' => 't_creation_date'], 't1.f_code = t2.f_id', [])
                        ->join(['t3' => 't_wf_phases'], 't2.f_wf_id = t3.f_wf_id and t2.f_phase_id = t3.f_number', ['f_group_id'])
                        ->join(['t4' => 't_workorders'], 't4.f_code = t1.f_code_periodic', ['fc_pm_fixed', 'f_counter', 'fc_pm_type', 'fc_pm_at_time', 'fc_pm_next_due_date'])
                        ->join(['t5' => 't_creation_date'], 't5.f_id = t4.f_code ', ['f_phase_id as generator_phase_id'])
                        ->where('t1.f_code = ?', $f_code)->query()->fetch();
        
        //recupero i gruppi di fase per i quali fare il ricalcolo delle floating (6 di default)
        $select->reset();
        $floatingsGroups = [6];
        $select->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
            ->join(array("s" => "t_systems"), "c.f_id = s.f_code", [])
            ->where("c.f_type = 'SYSTEMS'")
            ->where("c.f_category = 'SETTING'")
            ->where("c.f_title = 'periodics_floating_groups' ")->where("c.f_phase_id = 1");
        $result = $select->query()->fetch();
        $select->reset();
        
        if(!empty($result['f_description']))
        {
            $addingGroups = explode(",", $result['f_description']);
            $floatingsGroups = array_unique(array_merge($floatingsGroups,$addingGroups));
        }
    
        //check if is a closed periodic generated and if the periodic generator is floating, re-create the new timeline
        if (!empty($resWo) && in_array($resWo['f_group_id'], $floatingsGroups)) {
            $deltaEnd = abs($resWo['fc_wo_ending_date'] - $resWo['f_end_date']);
            if ($resWo['fc_pm_fixed'] == 1 && $deltaEnd > 86400) {//if the difference between delta and original_delta  > 86400 sec (1 day)
                //recalculate periodic using as start date fc_wo_ending_date
                $asset_list = [];
                if ($resWo['fc_pm_type'] == 'pm-per-asset') {
                    $select->reset();
                    $assets = $select->from(['t1' => 't_wares'], ['f_code'])
                                    ->join(['t2' => 't_ware_wo'], "t1.f_code = t2.f_ware_id", [])
                                    ->join(['t3' => 't_ware_wo'], "t3.f_ware_id = t2.f_ware_id", [])
                                    ->where("t1.f_type_id = 1")->where("t2.f_wo_id = ?", $f_code)->where("t3.f_wo_id = ?", $resWo['f_code_periodic'])
                                    ->query()->fetchAll();
                    foreach ($assets as $asset) {
                        $asset_list[$asset['f_code']] = $resWo['fc_wo_ending_date']; // rischedulo usando la data di chiusura
                    }
                }
                $this->createPeriodics($resWo['f_code_periodic'], $resWo['fc_wo_ending_date'], $asset_list, null, null, true);

                // recupero nuova next due dati per aggiornare il generatore
                $select->reset();
                $select->from('t_workorders', ['fc_pm_next_due_date', 'fc_pm_at_time'])
                        ->where('f_code =' . $resWo['f_code_periodic']);
                $resGen = $select->query()->fetch();

                //re imposto l'f_counter perché viene modificato di base dal metodo createPeriodics
                // aggiorno la data di inizio generazione del piano manutentivo impostandola uguale alla data di chiusura dell'ultima periodica
                $this->db->update('t_workorders', ['f_counter' => $resWo['f_counter'], 'fc_pm_start_date' => strtotime('midnight', $resGen['fc_pm_next_due_date']) + $resGen['fc_pm_at_time']], "f_code = {$resWo['f_code_periodic']}");
            }
            
            if($resWo['generator_phase_id'] == 5 || $resWo['generator_phase_id'] == 6){
                $select->reset();
                //aggiungo il gruppo Deleting(7) 
                $close_groups = array_merge([7],$floatingsGroups);
                $resNotClosedWo = $select->from(['t1' => 't_workorders'], ['f_code'])
                    ->join(['t2' => 't_creation_date'], 't2.f_id = t1.f_code ', [])
                    ->join(['t3' => 't_wf_phases'], 't3.f_wf_id = t2.f_wf_id and t3.f_number = t2.f_phase_id', [])
                    ->where('t1.f_code_periodic = ?', $resWo['f_code_periodic'])
                    ->where('t1.f_type_id IN (4,5,7)')
                    ->where('t3.f_group_id NOT IN ('. implode(',',$close_groups).')')
                    ->query()->fetch();
                
                if(empty($resNotClosedWo)){
                    $admusr = Mainsim_Model_Login::getUSerInfo(1, $this->db);
                    $admusrinfo = array(
                        "fc_editor_user_name"=>$admusr->fc_usr_firstname . ' ' . $admusr->fc_usr_lastname
                        ,"fc_editor_user_avatar"=>$admusr->fc_usr_avatar,"fc_editor_user_gender" => $admusr->f_gender
                        ,"fc_editor_user_phone" => $admusr->fc_usr_phone,"fc_editor_user_mail" => $admusr->fc_usr_mail
                    );
                    
                    //riporta la fase del generatore in 'Attivo' e inserisco il log
                    $timestamp = time();
                    $updt = array_merge($admusrinfo,["f_timestamp" => $timestamp, "f_phase_id" => 1]);
                    $this->db->update('t_creation_date', $updt, "f_id = {$resWo['f_code_periodic']}");
                    $objUtils->newRecord($this->db, $resWo['f_code_periodic'], "t_creation_date", array("f_timestamp" => $timestamp),"PeriodicShift",$timestamp);
                    $objUtils->newRecord($this->db, $resWo['f_code_periodic'], "t_custom_fields", array("f_timestamp" => $timestamp),"PeriodicShift",$timestamp);
                    $objUtils->newRecord($this->db, $resWo['f_code_periodic'], "t_workorders", array("f_timestamp" => $timestamp),"PeriodicShift",$timestamp);

                    //riporta la fase del generatore in 'Attivo'
                    //$this->db->update('t_creation_date', ['f_phase_id' =>1], "f_id = {$resWo['f_code_periodic']}");
                    
                    //recupero le righe in 't_periodics' tra la data di fc_wo_ending_date e adesso, in ordine decrescente di f_start_date
                    $reslpm = $this->getLastPm($resWo['f_code_periodic'], $asset_list[0] , $resWo['fc_wo_ending_date'], time());
                    
                    //rimuovo periodiche con f_executed = 1 non generate
                    $reslpm = array_filter($reslpm, function ($v){ 
                         return $v['f_executed'] != 1 || !empty($v['f_generated']);
                    }); 
                    
                    $reslpm = array_values($reslpm);
                    //eventualmente setto come da generare l'ultima periodica bloccata,se esiste,tra la data di fc_wo_ending_date e adesso
                    if(!empty($reslpm[0]) && $reslpm[0]['f_executed'] == 2)
                        $this->db->update("t_periodics",array("f_executed"=>4),"f_id = '{$reslpm[0]['f_id']}'");
                }
            }
        }
        
    }

    /**
     * take wo generator ready to generate and prepare it 
     */
    public function initializeWoGeneration($periodic, $user_info = array()) {
        if (!isset($periodic['f_type_id']))
            $periodic['f_type_id'] = '';
        $res_asset = array(); //asset list        
        $f_code_master = $periodic['f_code'];
        $f_parent_code = 0;
        $isFather = true;
        $select = new Zend_Db_Select($this->db);
        $generated = 0;
        //check if father exist and is only a folder        
        $res_father = $select->from(array("t1" => "t_workorders_parent"), array("f_parent_code"))
                        ->where("t1.f_code = ?", $periodic['f_code'])->query()->fetch();
        if ($res_father['f_parent_code'] != 0) {
            $f_code_master = $res_father['f_parent_code'];
            // se non è una pm-per-asset, mantieni il comportamento base
            if ($periodic['f_type_id'] != 3 || $periodic['fc_pm_type'] == 'classic' || $periodic['fc_pm_type'] == '' || !$periodic['fc_pm_type']) {
                //get asset from asset chain (if associated)
                $res_asset = Mainsim_Model_Utilities::getAssetChain($f_code_master, $periodic['f_start_date'], $res_asset, $this->db);
                //get asset from pm
                $res_asset = Mainsim_Model_Utilities::getAsset($f_code_master, $periodic['f_start_date'], $res_asset, $this->db);
            }
            $select->reset();
            $resFatherGenerated = $select->from(array('t1' => "t_workorders"), "f_code")
                            ->join(array('t2' => 't_creation_date'), 't1.f_code = t2.f_id', array())
                            ->join(array('t3' => 't_wf_phases'), 't2.f_phase_id = t3.f_number and t2.f_wf_id = t3.f_wf_id', array())
                            ->where('f_code_periodic = ?', $f_code_master)->where('f_start_date = ?', $periodic['f_start_date'])
                            ->where('f_group_id < 6')->query()->fetch();
            if (empty($resFatherGenerated['f_code'])) {
                //parent folder not exist, create new 
                $periodic_line = array("f_code" => $f_code_master, "f_start_date" => $periodic['f_start_date']);
                $select->reset();
                $res_wo_fath = $select->from(array("t1" => "t_workorders"))
                                ->join("t_creation_date", "t1.f_code = t_creation_date.f_id")
                                ->join(array("t2" => "t_custom_fields"), "t_creation_date.f_id = t2.f_code")
                                ->where("t1.f_code = ?", $f_code_master)->query()->fetch();
                foreach ($res_wo_fath as &$line_wo) {
                    $line_wo = Mainsim_Model_Utilities::chg($line_wo);
                }
                $resFatherGenerated['f_code'] = $this->createWorkorders($periodic_line, $res_wo_fath, $res_asset, $user_info);
            }
            $f_parent_code = $resFatherGenerated['f_code'];
            $isFather = false;
        } else { //this is a father. take Asset and asset chain
            // se non è una pm-per-asset, mantieni il comportamento base
            if ($periodic['f_type_id'] != 3 || $periodic['fc_pm_type'] == 'classic' || $periodic['fc_pm_type'] == '' || !$periodic['fc_pm_type']) {
                $res_asset = Mainsim_Model_Utilities::getAssetChain($periodic['f_code'], $periodic['f_start_date'], $res_asset, $this->db);
                $res_asset = Mainsim_Model_Utilities::getAsset($periodic['f_code'], $periodic['f_start_date'], $res_asset, $this->db);
            } else { //assegno l'asset che era in t_periodics
                $select->reset();
                $res_asset_single = $select->from(["t1" => "t_wares"], ['f_code', 'f_type_id'])
                                ->join("t_creation_date", "t1.f_code = t_creation_date.f_id", ["f_title"])
                                ->join(["t3" => "t_wf_phases"], "t_creation_date.f_phase_id = t3.f_number and t_creation_date.f_wf_id = t3.f_wf_id", [])
                                ->where("t1.f_code = ?", $periodic['f_asset_code'])->where("t3.f_group_id not in(6,7,8)")
                                ->where("t1.f_start_date IS NULL OR (t1.f_start_date <= ?)", $periodic['f_start_date'])
                                ->query()->fetch();
                //change keys of asset with f_code to simplify the check
                if ($res_asset_single) {
                    $res_asset[$res_asset_single['f_code']] = $res_asset_single;
                }
            }
        }

        //third check : if parent of other generator, generate all
        $select->reset();
        $res_child = $select->from("t_workorders_parent", array("f_code"))
                        ->where("f_parent_code = ?", $periodic['f_code'])->query()->fetchAll();
        $f_codes = array($periodic['f_code']);
        foreach ($res_child as $child) {
            $f_codes[] = $child['f_code'];
        }

        $select->reset();
        $select->from(array("t1" => "t_workorders"))
                ->join("t_creation_date", "t1.f_code = t_creation_date.f_id")
                ->join(array("t2" => "t_custom_fields"), "t_creation_date.f_id = t2.f_code");
        foreach ($f_codes as $f_code) {
            $resAssetChildren = [];
            $periodic['f_code'] = $f_code;
            $select->reset(Zend_Db_Select::WHERE);
            $res_wo = $select->where("t1.f_code = ?", $periodic['f_code'])->query()->fetch();
            foreach ($res_wo as &$line_wo) {
                $line_wo = Mainsim_Model_Utilities::chg($line_wo);
            }
            //ADD ASSETS FOR EVERY GENERATOR
            if ($periodic['f_type_id'] != 3 || $periodic['fc_pm_type'] == 'classic' || $periodic['fc_pm_type'] == '' || !$periodic['fc_pm_type']) {
                $resAssetChildren = $res_asset;
                $resAssetChildren = Mainsim_Model_Utilities::getAssetChain($periodic['f_code'], $periodic['f_start_date'], $resAssetChildren, $this->db);
                $resAssetChildren = Mainsim_Model_Utilities::getAsset($periodic['f_code'], $periodic['f_start_date'], $resAssetChildren, $this->db);
            } else {
                $select->reset();
                $res_asset_single = $select->from(["t1" => "t_wares"], ['f_code', 'f_type_id'])
                                ->join("t_creation_date", "t1.f_code = t_creation_date.f_id", ["f_title"])
                                ->join(["t3" => "t_wf_phases"], "t_creation_date.f_phase_id = t3.f_number and t_creation_date.f_wf_id = t3.f_wf_id", [])
                                ->where("t1.f_code = ?", $periodic['f_asset_code'])->where("t3.f_group_id not in(6,7,8)")
                                // 24/04/17 : tolto controllo su f_start_date (data acquisto asset) per associazione a wo
                                //->where("t1.f_start_date IS NULL OR (t1.f_start_date < ?)",$periodic['f_start_date'])
                                ->query()->fetch();
                //change keys of asset with f_code to simplify the check
                if ($res_asset_single) {
                    $resAssetChildren[$res_asset_single['f_code']] = $res_asset_single;
                }
            }

            $f_code_generated = $this->createWorkorders($periodic, $res_wo, $resAssetChildren, $user_info);
            if (is_array($f_code_generated)) {
                $this->db->insert('t_logs', array('f_timestamp' => time(), 'f_log' => $f_code_generated['message'], 'f_type_log' => 0));
                return $f_code_generated;
            }
            $generated++;
            if ($isFather) {
                $f_parent_code = $f_code_generated;
                $isFather = false;
            } else {
                $this->db->update("t_workorders_parent", array("f_parent_code" => $f_parent_code), "f_code = $f_code_generated");
            }
        }
        return $generated;
    }

    public function createWorkorders($line, $res_wo, $res_asset = array(), $user_info = array()) {
        #  print_r($line);exit;
        $select = new Zend_Db_Select($this->db);
        $f_code_generator = $line['f_code'];
        $f_counter = $res_wo['f_counter'];
        if (!isset($res_wo['fc_pm_duration']) || is_null($res_wo['fc_pm_duration'])) { //if no duration, set 1 hour
            $res_wo['fc_pm_duration'] = 3600;
        }
        $res_task = array();
        $res_ware_wo = array();
        $res_pair = array();
        $res_sel_wo = array();
        $f_type_id = 0;
        switch ($res_wo['f_type_id']) {
            case 3 : $f_type_id = 4;
                break;
            case 11 : $f_type_id = 7;
                break;
            case 19 : $f_type_id = 5;
                break;
            case 9 : $f_type_id = 6;
                break;
            case 15 : $f_type_id = 1;
                break;
            default : throw new Exception("Unknown generator type");
        }

        $select->reset();
        $res_task_list = $select->from(array("t1" => "t_wares"), array('f_code', 'f_type_id', 'fc_task_sequence', 'fc_task_exception_allow', 'fc_task_exception_deny'))
                        ->join(array("t2" => "t_ware_wo"), "t1.f_code = t2.f_ware_id", array())
                        ->join("t_creation_date", "t1.f_code = t_creation_date.f_id", array("f_title", 'f_description'))
                        ->where("t2.f_wo_id = ?", $line['f_code'])
                        ->where("t1.f_type_id = 23")->where("t_creation_date.f_phase_id = 1")
                        ->order("t1.f_order ASC")->query()->fetchAll();

        //change keys of task with f_code to simplify the check            
        $tot_ta = count($res_task_list);
        for ($ta = 0; $ta < $tot_ta; ++$ta) {
            $line_tl = $res_task_list[$ta];
            if (Mainsim_Model_Utilities::checkTaskSequence($f_counter, $line_tl['fc_task_sequence'], $line_tl['fc_task_exception_allow'], $line_tl['fc_task_exception_deny'])) {
                $res_task = Mainsim_Model_Utilities::getTaskFromTaskList($line_tl['f_code'], $res_task, $this->db, $f_counter, $line_tl['fc_task_sequence']);
            }
        }

        //add task
        $select->reset();
        $result_task = $select->from(array("t1" => "t_wares"), array('f_code', 'f_type_id', 'fc_task_sequence', 'fc_task_exception_allow', 'fc_task_exception_deny'))
                        ->join(array("t2" => "t_ware_wo"), "t1.f_code = t2.f_ware_id", array())
                        ->join("t_creation_date", "t1.f_code = t_creation_date.f_id", array("f_title", "f_description"))
                        ->where("t2.f_wo_id = ?", $line['f_code'])
                        ->where("t1.f_type_id = 10")->where("t_creation_date.f_phase_id = 1")->order("t1.f_order ASC")->query()->fetchAll();
        //change keys of task with f_code to simplify the check            
        $tot_ta = count($result_task);
        for ($ta = 0; $ta < $tot_ta; ++$ta) {
            $line_task = $result_task[$ta];
            if (Mainsim_Model_Utilities::checkTaskSequence($f_counter, $line_task['fc_task_sequence'], $line_task['fc_task_exception_allow'], $line_task['fc_task_exception_deny'])) {
                $res_task[$result_task[$ta]['f_code']] = $result_task[$ta];
            }
        }

        $select->reset();
        //insert other ware_wo cross
        $res_ware_wo_per = $select->from("t_ware_wo")
                        ->join("t_wares", "f_code = f_ware_id", array('f_type_id'))
                        ->where("f_wo_id = ?", $f_code_generator)->where("f_type_id NOT IN (1,10,13,23)")
                        ->query()->fetchAll();
        $tot_wwo = count($res_ware_wo_per);
        for ($wwo = 0; $wwo < $tot_wwo; ++$wwo) {
            $res_ware_wo[$res_ware_wo_per[$wwo]['f_ware_id']] = $res_ware_wo_per[$wwo];
        }

        //aggiunto da elcarbo in data 06/07/2018
        if ($res_wo['f_type_id'] == 3) { //multi
            $tot_assets = Mainsim_Model_Utilities::getAssetChain($f_code_generator, $line['f_start_date'], array(), $this->db);
            $tot_assets = Mainsim_Model_Utilities::getAsset($f_code_generator, $line['f_start_date'], $tot_assets, $this->db);
            if(count($tot_assets) > 0){
                $tot_assets = array_keys($tot_assets);

                $select->reset();
                $filter_assets = $select->from(array('wr' => "t_wares_relations"), array('f_code_ware_slave'))
                                ->where("wr.f_code_ware_master IN (" . implode(",", $tot_assets) . ")")
                                ->query()->fetchAll();

                $filter_assets = array_column($filter_assets, 'f_code_ware_slave');
                foreach ($filter_assets as $key_code)
                    unset($res_ware_wo[$key_code]);
            }
        }
        
        
        //insert other ware_wo cross
        $asset_list = array_keys($res_asset);
        $select->reset();
        $select->from("t_selector_wo")
                ->join("t_selectors", "f_code = f_selector_id", array('f_type_id'))
                ->where("f_wo_id = ?", $f_code_generator)
                ->where("f_type_id != 11"); //escludo il selettore dei piani manutentivi
        if (!empty($asset_list)) {
            $select->where("f_selector_id not in (select f_selector_id from t_selector_ware where f_ware_id in(?) ) ", $asset_list);
        }
        $res_sel_wo_per = $select->query()->fetchAll();
        $tot_swo = count($res_sel_wo_per);
        for ($swo = 0; $swo < $tot_swo; ++$swo) {
            $res_sel_wo[$res_sel_wo_per[$swo]['f_selector_id']] = $res_sel_wo_per[$swo];
        }

        //Find all pair cross (excluding engagement) and add to system
        $select->reset();
        $res_pair_per = $select->from("t_pair_cross")
                        ->join(array("t2" => "t_wares"), "f_code_cross = t2.f_code", array("f_type_id"))
                        ->where("f_code_main = ?", $f_code_generator)->query()->fetchAll();
        $tot_pp = count($res_pair_per);
        for ($pp = 0; $pp < $tot_pp; ++$pp) {
            foreach ($res_pair_per[$pp] as &$linePP) {
                $linePP = Mainsim_Model_Utilities::chg($linePP);
            }
            $res_pair[$res_pair_per[$pp]['f_id']] = $res_pair_per[$pp];
        }

        $select->reset();
        $res_type = $select->from("t_workorders_types", "f_module_name")
                        ->where("f_id = ?", $res_wo['f_type_id'])->query()->fetch();
        $module_name = !empty($res_type['f_module_name']) ? $res_type['f_module_name'] : 'mdl_wo_tg';

        $params = [
            'f_code' => 0,
            'f_type_id' => $f_type_id,
            'f_code_periodic' => $line['f_code'],
            'f_user_id' => $res_wo['f_user_id'],
            'f_start_date' => $line['f_start_date'], //data inizio
            'f_end_date' => $line['f_start_date'] + (int) $res_wo['fc_pm_duration'] + 60,
            'f_priority' => $res_wo['f_priority'],
            'f_counter' => $f_counter,
            'f_title' => $res_wo['f_title'],
            'f_description' => $res_wo['f_description'],
            'f_visibility' => -1,
            'f_editability' => -1,
            'f_module_name' => $module_name,
            'fc_wo_generator_id' => "ID: " . sprintf("%05s", $res_wo['fc_progress']) . ' - ' . $res_wo['f_category'],
            'fc_work_instructions' => Mainsim_Model_Utilities::chg($this->createWorkInstruction($res_asset)),
            'fc_wo_linked_pm_oc' => $this->pmLinkedOc($line['f_code'])
        ];

        if ($f_type_id == 6) { // add meter value if exists
            $params['fc_meter_value'] = $line['fc_meter_value'];
        } else {
            unset($res_wo['fc_meter_value']);
            unset($res_wo['fc_meter_add_value']);
        }
        unset($res_wo['f_id']);
        unset($res_wo['f_timestamp']);
        unset($res_wo['f_wf_id']);
        unset($res_wo['f_phase_id']);
        unset($res_wo['f_category']);
        unset($res_wo['f_type']);
        unset($res_wo['f_creation_date']); //unset($res_wo['fc_meter_value']);
        $params = $params + $res_wo; // merge default params with info from generator
        //Create provvisory parent
        $params['t_workorders_parent'] = array(
            'f_code' => array(0),
            'f_code_main' => 0,
            'f_type' => $f_type_id,
            'Ttable' => 't_workorders_parent',
            "f_module_name" => "mdl_wo_parent_tg"
        );

        //Inserisco gli asset se sono presenti.        
        $select->reset();
        $keys_ra = array_keys($res_asset);
        $tot_ra = count($res_asset);
        for ($ra = 0; $ra < $tot_ra; ++$ra) {
            $line_asset = $res_asset[$keys_ra[$ra]];
            $params["t_wares_1"]['f_code'][] = (int) $line_asset['f_code'];
            $params["t_wares_1"]['f_type'] = 1;
            $params["t_wares_1"]['Ttable'] = "t_wares";
            $params["t_wares_1"]["f_code_main"] = 0;
            $params["t_wares_1"]["pairCross"] = 0;
        }

        $tot_wwo = count($res_ware_wo);
        $wwo_keys = array_keys($res_ware_wo);

        for ($wwo = 0; $wwo < $tot_wwo; ++$wwo) {
            $line_wwo = $res_ware_wo[$wwo_keys[$wwo]];
            $params["t_wares_{$line_wwo['f_type_id']}"]['f_code'][] = (int) $line_wwo['f_ware_id'];
            $params["t_wares_{$line_wwo['f_type_id']}"]['f_type'] = $line_wwo['f_type_id'];
            $params["t_wares_{$line_wwo['f_type_id']}"]['Ttable'] = "t_wares";
            $params["t_wares_{$line_wwo['f_type_id']}"]["f_code_main"] = 0;
            $params["t_wares_{$line_wwo['f_type_id']}"]["pairCross"] = 0;
        }

        $tot_swo = count($res_sel_wo);
        $swo_keys = array_keys($res_sel_wo);
        for ($swo = 0; $swo < $tot_swo; ++$swo) {
            $line_swo = $res_sel_wo[$swo_keys[$swo]];
            $params["t_selectors_1,2,3,4,5,6,7,8,9,10,11"]['f_code'][] = (int) $line_swo['f_selector_id'];
            $params["t_selectors_1,2,3,4,5,6,7,8,9,10,11"]['f_type'] = $line_swo['f_type_id'];
            $params["t_selectors_1,2,3,4,5,6,7,8,9,10,11"]['Ttable'] = "t_selectors";
            $params["t_selectors_1,2,3,4,5,6,7,8,9,10,11"]["f_code_main"] = 0;
            $params["t_selectors_1,2,3,4,5,6,7,8,9,10,11"]["pairCross"] = 0;
        }

        $params['t_wares_10'] = array('f_code' => !empty($res_asset) ? array_keys($res_asset) : array(), 'f_pair' => array());
        $params["t_wares_10"]["f_code_main"] = 0;
        $params["t_wares_10"]["f_type"] = "10";
        $params["t_wares_10"]["Ttable"] = "t_wares";
        $params["t_wares_10"]["pairCross"] = "1";

        $this->createTaskParams($res_task, $res_asset, $params);

        //insert specific task inserted before in asset                
        for ($ra = 0; $ra < $tot_ra; ++$ra) {
            $line_asset = $res_asset[$keys_ra[$ra]];
            $res_task_asset = array();
            //extract task from associated tasklist
            $select->reset();
            $res_tasklist_asset = $select->from(array("t1" => "t_wares"), array('f_code', 'f_type_id', 'fc_task_sequence', 'fc_task_exception_allow', 'fc_task_exception_deny'))
                            ->join(array("t2" => "t_wares_relations"), "t1.f_code = t2.f_code_ware_slave", array())
                            ->join(array("t3" => "t_creation_date"), "t1.f_code = t3.f_id", array("f_title", "f_description"))
                            ->where("t2.f_code_ware_master = ?", $line_asset['f_code'])
                            ->where("t1.f_type_id = 23")->where("t3.f_phase_id = 1")
                            ->order("t1.f_order ASC")->query()->fetchAll();
            //change keys of task with f_code to simplify the check            
            $tot_ta_a = count($res_tasklist_asset);
            for ($ta_a = 0; $ta_a < $tot_ta_a; ++$ta_a) {
                $line_tl_a = $res_tasklist_asset[$ta_a];
                if (Mainsim_Model_Utilities::checkTaskSequence($f_counter, $line_tl_a['fc_task_sequence'], $line_tl_a['fc_task_exception_allow'], $line_tl_a['fc_task_exception_deny'])) {
                    $res_task_asset = Mainsim_Model_Utilities::getTaskFromTaskList($line_tl_a['f_code'], $res_task_asset, $this->db);
                }
            }
            //extract task from associated asset
            $select->reset();
            $res_asset_task = $select->from(array("t1" => "t_wares_relations"), array())
                            ->join(array("t2" => "t_wares"), "t1.f_code_ware_slave = t2.f_code", array('f_code', 'f_type_id', 'fc_task_sequence', 'fc_task_exception_allow', 'fc_task_exception_deny'))
                            ->join(array("t3" => "t_creation_date"), "t2.f_code = t3.f_id", array('f_title'))
                            ->where("t1.f_code_ware_master = ?", $line_asset['f_code'])->where("t2.f_type_id = 10")->where("t3.f_phase_id = 1")
                            ->query()->fetchAll();
            $tot_at = count($res_asset_task);
            for ($at = 0; $at < $tot_at; ++$at) {
                if (Mainsim_Model_Utilities::checkTaskSequence($f_counter, $res_asset_task[$at]['fc_task_sequence'], $res_asset_task[$at]['fc_task_exception_allow'], $res_asset_task[$at]['fc_task_exception_deny'])) {
                    $res_task_asset[$res_asset_task[$at]['f_code']] = $res_asset_task[$at];
                }
            }
            $this->createTaskParams($res_task_asset, array(array('f_code' => $line_asset['f_code'])), $params);
        }

        //Find all pair cross (excluding engagement) and add to system
        $tot_p = count($res_pair);
        $keys_p = array_keys($res_pair);
        for ($p = 0; $p < $tot_p; ++$p) {
            $per = $res_pair[$keys_p[$p]];
            $f_type_id_pair = $per['f_type_id'];
            $per["f_code_main"] = 0;
            unset($per['f_id']);
            unset($per['f_type_id']);
            $params["t_wares_{$f_type_id_pair}"]['f_pair'][] = $per;
        }
        $res_code = $this->newWorkOrder($params, true, $user_info);
        
        if (isset($res_code['message'])) {
            return array('message' => $res_code['message']);
        }
        $f_code = $res_code['f_code'];

        if ($res_wo['f_type_id'] != 9 && $res_wo['f_type_id'] != 15) {
            // update with generated workorder code
            $this->db->update("t_periodics", array("f_generated_code" => $f_code), "f_id = '{$line['f_id']}'");
        }

        $this->createWoCross($f_code_generator, $f_code, $res_wo['f_type_id'], $f_type_id);
        //finished to create the periodic, increment my counter for task sub cyclic
        $this->incrementCounter($f_code_generator);
        return $f_code;
    }

    /**
     * Check if the pm have oc generator crossed
     * @param int $f_code pm generator
     * @return int 0 if no oc are linked to pm, 1 if true
     */
    private function pmLinkedOc($f_code) {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from("t_wo_relations", ['num' => 'count(*)'])
                        ->where("f_type_id_wo_master = 3")->where("f_code_wo_master = ?", $f_code)
                        ->where("f_type_id_wo_slave = 9")->query()->fetch();
        return (int) ($res['num'] > 0);
    }

    /**
     * Write asset's work instruction 
     * into generated wo
     * @param array $resAsset list of asset to get work instructions
     * @return string work instruction generated
     */
    private function createWorkInstruction($resAsset) {
        $wi = '';
        if (empty($resAsset)) {
            return $wi;
        }
        $select = new Zend_Db_Select($this->db);
        $codes = array_keys($resAsset);
        $resAssetInfo = $select->from(['t1' => 't_creation_date'], ['f_id', 'fc_progress', 'fc_work_instructions'])
                        ->where("f_id IN (?)", $codes)->query()->fetchAll();
        $tot = count($resAssetInfo);
        for ($i = 0; $i < $tot; ++$i) {
            $wi .= '------------------------------------' . PHP_EOL;
            $wi .= 'Asset ID: ' . sprintf("%05s", $resAssetInfo[$i]['fc_progress']) . ' - ' . $resAsset[$resAssetInfo[$i]['f_id']]['f_title'] . PHP_EOL;
            $wi .= '------------------------------------' . PHP_EOL;
            $wi .= ' - ' . $resAssetInfo[$i]['fc_work_instructions'] . PHP_EOL . PHP_EOL;
        }
        return $wi;
    }

    private function incrementCounter($f_code) {
        $select = new Zend_Db_Select($this->db);
        $res_wo = $select->from("t_workorders", "f_counter")->where("f_code = $f_code")->query()->fetchAll();
        $new_wo = $res_wo[0];
        $new_wo['f_counter'] = $new_wo['f_counter'] + 1;
        //increment counter
        $this->db->update("t_workorders", array("f_counter" => $new_wo['f_counter']), "f_code = $f_code");
    }

    /**
     * Create cross between workorders
     * @param int $generator f_code generator wo 
     * @param int $generated f_code generated wo 
     * @param int $type_id_generator f_type_id generator wo
     * @param int $type_id_generated f_type_id generated wo
     */
    private function createWoCross($generator, $generated, $type_id_generator, $type_id_generated) {
        $this->db->insert("t_wo_relations", array(
            "f_code_wo_master" => $generator,
            "f_type_id_wo_master" => $type_id_generator,
            "f_code_wo_slave" => $generated,
            "f_type_id_wo_slave" => $type_id_generated,
            "f_timestamp" => time()
        ));
        $this->db->insert("t_wo_relations", array(
            "f_code_wo_master" => $generated,
            "f_type_id_wo_master" => $type_id_generated,
            "f_code_wo_slave" => $generator,
            "f_type_id_wo_slave" => $type_id_generator,
            "f_timestamp" => time()
        ));
    }

    /**
     * create params array about task (t_wares_10) 
     */
    private function createTaskParams($res_task, $res_asset, &$params) {
        $tot_rt = count($res_task);
        $task_keys = array_keys($res_task);
        for ($rt = 0; $rt < $tot_rt; ++$rt) {
            $line_task = $res_task[$task_keys[$rt]];

            //hook task for every asset
            foreach ($res_asset as $line_asset) {
                $params["t_wares_10"]['f_pair'][] = array(
                    'f_code_main' => 0,
                    'f_code_cross' => (int) $line_asset['f_code'],
                    'f_group_code' => (int) $line_task['f_code'],
                    'f_title' => Mainsim_Model_Utilities::chg($line_task['f_title']),
                    'f_description' => (isset($line_task['f_description']) ? Mainsim_Model_Utilities::chg($line_task['f_description']) : NULL)
                );
            }
        }
    }

    public function forceMultiPm($ware, $pmList) {

        // no pm selected
        if ($pmList == "") {
            $result = array(
                'codes' => array(),
                'message' => "Nessuna pm selezionata"
            );
            print(json_encode($result));
            die;
        }
        // for each pm get first wo to generate (if exists) from t_periodics
        $aux = explode(",", $pmList);
        $select = new Zend_Db_Select($this->db);
        // counter 
        $toGenerate = array();
        $pm = new Mainsim_Model_PMReader();
        for ($i = 0; $i < count($aux); $i++) {
            $select->reset();
            $select->from('t_periodics', array('f_id'))
                    ->where('f_code = ' . $aux[$i])
                    ->where('f_asset_code = ' . $ware)
                    ->where('f_executed = 0')
                    ->limit(1);
            //echo $select;
            $res = $select->query()->fetchAll();
            if (count($res) > 0) {
                $generated += $pm->readPeriodics(time(), 0, 1, $aux[$i], 0, $res[0]['f_id']);
            }
        }

        // retrieve last wo generated
        $select->reset();
        $select->from('t_periodics', 'max(f_generated_code) AS code')
                ->where('f_executed = 1')
                ->where('f_code IN (' . $pmList . ')')
                ->where('f_asset_code = ' . $ware)
                ->group('f_code');
        $resCodes = $select->query()->fetchAll();

        $result = array(
            'codes' => array(),
            'message' => "Sono state generate " . (int) $generated . "/" . count($aux) . " OdL periodico/i (i piani manutentivi di tipo <b>\"Un job per tutti gli asset\"</b> vanno forzati dal modulo \"Attività programmate\")");
        for ($i = 0; $i < count($resCodes); $i++) {
            if ($resCodes[$i]['code'] != null) {
                $result['codes'][] = $resCodes[$i]['code'];
            }
        }

        print(json_encode($result));
        die;
    }

}
