<?php


class Mainsim_Model_Wizard{
    private $db;
    
    public function __construct($db = null) {
        if($db){
            $this->db = $db;
        }
        else{
            $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        }
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function cacheWizard(){
         $select = new Zend_Db_Select($this->db);
        $select->from('t_creation_date', array('f_description'))
                ->where("f_title ='CACHE_WIZARD'");
        $res = $select->query()->fetchAll();
        return $res[0]['f_description'];
    }
    
    public function readSelect(){
        
        $select = new Zend_Db_Select($this->db);
        
        // get priorities
        $priorities = array(
            "none" => 'none'
        );
        $select->from('t_creation_date', array('f_title'))
               ->join('t_wares', 't_creation_date.f_id = t_wares.f_code', array('fc_action_priority_color', 'fc_action_priority_value'))
               ->where("t_creation_date.f_category = 'ACTION'")
               ->where("t_wares.fc_action_type = 'priority'");
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $priorities[$res[$i]['fc_action_priority_value']] = $res[$i]['f_title'];
        }
        unset($priorities["a"]);
        
        // get owners
        $owners = array(
            '0' => 'none'
        );
        $select->reset();
        $select->from('t_users', array('f_code', 'fc_usr_firstname', 'fc_usr_lastname', 'fc_usr_usn'));
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            if(($res[$i]['fc_usr_firstname'] . ' ' . $res[$i]['fc_usr_lastname']) != ' '){
              $owners[$res[$i]['f_code']] = utf8_encode($res[$i]['fc_usr_firstname'] . ' ' . $res[$i]['fc_usr_lastname']);
            }
            else{
              $owners[$res[$i]['f_code']] = utf8_encode($res[$i]['fc_usr_usn']);
            }
        }
        
        // get anchornames
        $anchors = array(
            'none',
            'root'
        );
        $select->reset();
        $select->from('t_wares', array('fc_asset_wizard'))
               ->where("fc_asset_wizard IS NOT null AND fc_asset_wizard <> ''");
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $anchors[] = $res[$i]['fc_asset_wizard'];
        }
        
        return array(
            'wo_priority' => $priorities,
            'wo_owner' => $owners,
            'anchor_name' => $anchors
        );
    }
    
    public function addPairCross($paircross, $f_code){
        
        //print_r($paircross); die();
        $time = time();
        for($i = 0; $i < count($paircross); $i++){
            // insert into t_pair_cross
            $insertData = array('f_code_main' => $f_code, 'f_code_cross' => $paircross[$i]['f_code_cross'], 'f_title' => 'Job');
            for($j = 0; $j < count($paircross[$i]['fieldlist']); $j++){
                $insertData[$paircross[$i]['fieldlist'][$j]] = $paircross[$i][$paircross[$i]['fieldlist'][$j]];
            }
            $this->db->insert('t_pair_cross', $insertData);
            // insert into t_ware_wo
            $this->db->insert('t_ware_wo', array('f_ware_id' => $paircross[$i]['f_code_cross'], 'f_wo_id' => $f_code, 'f_timestamp' => $time));
        }
    }
    
    public function getWizard()
    {
        $userlevel = Zend_Auth::getInstance()->getIdentity()->f_level; 
        $select = new Zend_Db_Select($this->db);
                
        // campi in ware
        $dbfield=array("f_code"=>"idf","f_parent_code"=>"idp",
            "f_title"=>"title","f_description"=>"subtitle",
            "fc_wizard_linktitle"=>"linktitle","fc_wizard_visibility"=>"level",
            "fc_wizard_color"=>"col","fc_wizard_color_box"=>"colbox",
            "fc_wizard_htm"=>"htm","fc_wizard_srcbox"=>"srcbox",
            "fc_wizard_imgbox"=>"imgbox","fc_wizard_ink"=>"ink",
            "fc_wizard_valfield"=>"valfield","fc_wizard_field"=>"field",
            "fc_wizard_exit"=>"exit"
        );
        
        $wiz = array(); 
        $n = 0;
        $select->from(array("t1"=>"t_wares"),array("f_code"))
                ->join(array("t4"=>"t_creation_date"),"t4.f_id = t1.f_code",array("f_title","f_description","f_visibility"))                
                ->join(array("t2"=>"t_wares_parent"),"t1.f_code = t2.f_code",array("f_parent_code"))
                ->join(array("t3"=>"t_wizard"), "t3.f_code = t2.f_code",array("fc_wizard_linktitle","fc_wizard_color","fc_wizard_color_box","fc_wizard_htm","fc_wizard_srcbox","fc_wizard_imgbox","fc_wizard_ink","fc_wizard_valfield","fc_wizard_field","fc_wizard_exit"))
        ->where("t1.f_type_id = 9")
        ->where("(t4.f_visibility & $userlevel) != 0");
        $res = $select->query()->fetchAll();
        
        $f_codes = array();
        foreach ($res as $line){
            if(!in_array($line['f_code'], $f_codes)) {
                $f_codes[$n] = $line['f_code'];
                
                foreach($line as $key =>$value) {
                    if($key == "f_parent_code") {
                        $wiz[$n][$dbfield[$key]][] = $value;
                    }
                    else {
                        $v = stripslashes($value);
                        $wiz[$n][$dbfield[$key]] = Mainsim_Model_Utilities::chg($v);
                    }
                }                             
                $n++;
            }
            else {
                $index = array_search($line['f_code'], $f_codes);
                $wiz[$index]['idp'][] = $line['f_parent_code'];
            }
        }
        $risp=array("userlevel"=>$userlevel, "data"=>$wiz);        
        return  $risp;
    }
    
    /**
     * read wizard if version is different from the first row of t_wizard
     * @param type $version 
     */
    public function readWizard($version)
    {        
        // security fix (match number)
        if(!preg_match("/^(-?[0-9]{1,})$/", $version)){ 
            die('param version not valid');
        }
        
        
        $data = array();         
        $select = new Zend_Db_Select($this->db);

        $cols="#496670,#6E8D97,#4ECDC4,#AED138,#FF6B6B,#C44D58,#AAAAAA";

        $select->reset();
        $select->from('t_creation_date', 'f_description')
               ->where("f_title = 'wizard_color_set'")
               ->where("f_category = 'SETTING'");
        $res = $select->query()->fetchAll();
        $tot_a = count($res);
        if($tot_a) $cols = $res[0]['f_description'];
        
        $select->reset();
        $select->from('t_creation_date', 'f_description')
               ->where("f_title = 'wizard_upload_max_file'")
               ->where("f_category = 'SETTING'");
        $res = $select->query()->fetchAll();
        $tot_a = count($res);
        if($tot_a) $upload_max = $res[0]['f_description'];
        
        $select->reset();
        $res_first_row = $select->from("t_wizard","f_order")->order("f_code ASC")->limit(1)
                ->query()->fetch();                        
        if($res_first_row['f_order']!=$version || true) {            
            $select->reset();
            $res = $select->from("t_wizard")->query()->fetchAll();
            $tot_a = count($res);
            if($tot_a > 0) {
                $keys = array_keys($res[0]);
                $tot_b = count($keys);
            }
            for($a = 0;$a < $tot_a;++$a) {        
                for($b = 0;$b < $tot_b;++$b) {
                    $data[$a][$b] = in_array($res[$a][$keys[$b]], array(null, "null")) ? "" : $res[$a][$keys[$b]];    // $keys[$b]
                }
            }
        }

        // get asset actions from setting if it exists
        $select->reset();
        $select->from("t_creation_date", ['f_description'])
            ->where("f_title = 'WIZARD_TILE_ACTIONS'");
        $res = json_decode($select->query()->fetch()['f_description']);
        $actions = [];
        if($res != null){
            foreach($res as $key => $value){
                $actions[] = $key;
            }
        }
        
        return array("version"=>$res_first_row['f_order'], "colors"=>$cols, "data"=>$data, "wizard_upload_max_file" => $upload_max, "asset_actions" => $actions);
    }
    
    /**
     * save all wizard
     * @param type $data 
     */
    public function saveWizard($data, $cols)
    {
        try {
            $this->db->beginTransaction();
            if(!empty($data)) {
                $this->db->delete("t_wizard");       
                $is_mysql = $this->isMysql();                
                if($is_mysql) {
                    $this->db->query("ALTER TABLE t_wizard AUTO_INCREMENT = 1");
                }
                else {
                    try {$this->db->query("DBCC CHECKIDENT (t_wizard, RESEED, 0)");}catch(Exception $e){}
                }
                  
                for($r=0;$r<count($data);$r++){
                    $this->db->insert("t_wizard",array(
                    'f_code'=>$data[$r][1],
                    'f_parent_code'=>$data[$r][2],
                    'f_title'=>$data[$r][3],
                    'f_subtitle'=>$data[$r][4] ,
                    'f_imgtile'=>$data[$r][5] ,
                    'f_linktitle'=>$data[$r][6], 
                    'f_visibility'=>$data[$r][7] ,
                    'f_order'=>$data[$r][8] ,
                    'f_field'=>$data[$r][9],
                    'f_color'=>$data[$r][10], 
                    'f_tilewidth'=>$data[$r][11] ,
                    'f_srcbox'=>$data[$r][12],
                    'f_ware_start'=>$data[$r][13],
                    ));
                }
            }
            $this->db->commit();
        }catch(Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        } 
        
        
            // colors
        if($cols){
            $select = new Zend_Db_Select($this->db);
            
            $select->reset();
            $select->from('t_creation_date', 'f_id')
                   ->where("f_title = 'wizard_color_set'")
                   ->where("f_category = 'SETTING'");
            $res = $select->query()->fetchAll();
            $tot_a = count($res);
            if($tot_a) {
              $f_code = $res[0]['f_id'];
              $this->db->update("t_creation_date",  array('f_description'=>$cols), "f_id = $f_code");
            }
        }
        
    }
    
    public function getWaresPlus($ware_start, $pdr, $waresByUserSelectors = null){
        
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $ul = $userData['fc_usr_level'];
        }
        else{
            $ul = Zend_Auth::getInstance()->getIdentity()->f_level;
        }

        // get selectors
        $selectors = explode("_ADD_", $ware_start);
        // get wares associated to selectors
        $select = new Zend_Db_Select($this->db);
        $select->from('t_selector_ware')
               ->where('f_selector_id IN (' . $selectors[1] . ')');
        $resSelectors = $select->query()->fetchAll();
        $visited = array(); 
        // loop through wares, for each of them get his parents
        for($i = 0; $i < count($resSelectors); $i++){
            $parentsCodes = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $resSelectors[$i]['f_ware_id']);
            $parentsCodes[] = $resSelectors[$i]['f_ware_id'];
            $toVisitCodes = array();
            // remove elements already visited
            for($k = 0; $k < count($parentsCodes); $k++){
                if(!in_array($parentsCodes[$k], $visited)){
                    $toVisitCodes[] = $parentsCodes[$k];
                    $visited[] = $parentsCodes[$k];
                }
            }
            
            // if a parent ware has hidden property set, delete tree branch from result
            if(count($toVisitCodes) > 0){
                $select->reset();
                $select->from('t_wares', array('count(*) AS tot'))
                       ->where('f_code IN (' . implode(",", $toVisitCodes) . ')')
                       ->where('fc_asset_wizard_end = 2');
                $res = $select->query()->fetchAll();

                if($res[0]['tot'] == 0){

                    //array_unshift($toVisitCodes, $resSelectors[$i]['f_ware_id']);
                    $select->reset();
                    $select->from(array("t1"=>"t_wares"),array("f_code","fc_asset_wizard_end", "f_subtitle_wiz", "f_imgtile_wiz", "f_linktitle_wiz", "f_order_wiz", "f_field_wiz", "f_color_wiz", "f_tilewidth_wiz", "f_srcbox_wiz"))
                        ->join(array("t2"=>"t_creation_date"),"t2.f_id = t1.f_code",array("f_title","f_visibility"))
                        ->join(array("t3"=>"t_wf_phases"),"t2.f_wf_id = t3.f_wf_id and t2.f_phase_id = t3.f_number",array())
                        ->join(array("t4"=>"t_wf_groups"),"t3.f_group_id = t4.f_id",array())
                        ->join(array("t5"=>"t_wares_parent"),"t1.f_code = t5.f_code",array("f_parent_code"))
                        ->where("t1.f_code IN (" . implode(",", $toVisitCodes) . ")")
                        ->where("(t2.f_visibility & $ul) != 0")
                        ->where("(t3.f_visibility & $ul) != 0")
                        ->where("(t4.f_visibility & $ul) != 0");  
                    if(is_array($waresByUserSelectors)){
                        $select->where("t1.f_code IN (" . implode(",", $waresByUserSelectors) . ")");
                    }
                    $resParents = $select->query()->fetchAll();

                    for($j = 0; $j < count($resParents); $j++){
                        //if(!in_array($resParents[$j]['f_code'], $visited)){
                            $result[] = array(
                                'f_code'=>'w_'. $resParents[$j]['f_code'],
                                'f_parent_code'=> $resParents[$j]['f_parent_code'] == 0 ? $pdr : 'w_'. $resParents[$j]['f_parent_code'],
                                'f_title'=>  Mainsim_Model_Utilities::chg($resParents[$j]['f_title']),
                                'f_visibility'=> $resParents[$j]['f_visibility'],
                                'f_subtitle' => $resParents[$j]['f_subtitle_wiz'] ? Mainsim_Model_Utilities::chg($resParents[$j]['f_subtitle_wiz']) : '',
                                'f_imgtile' => $resParents[$j]['f_imgtile_wiz'] ? $resParents[$j]['f_imgtile_wiz'] : '',
                                'f_linktitle' => $resParents[$j]['f_linktitle_wiz'] ? $resParents[$j]['f_linktitle_wiz'] : '',
                                'f_order' => $resParents[$j]['f_order_wiz'] ? $resParents[$j]['f_order_wiz'] : 0,
                                'f_field' => $resParents[$j]['f_field_wiz'] ? $resParents[$j]['f_field_wiz'] : '',
                                'f_color' => $resParents[$j]['f_color_wiz'] ? $resParents[$j]['f_color_wiz'] : '',
                                'f_tilewidth' => $resParents[$j]['f_tilewidth_wiz'] ? $resParents[$j]['f_tilewidth_wiz'] : 0,
                                'f_srcbox' => $resParents[$j]['f_srcbox_wiz'] ? $resParents[$j]['f_srcbox_wiz'] : ''
                            );
                            //$visited[] = $resParents[$j]['f_code'];
                        //}
                    } 
                }
            }
        }
        return $result;
    }
    
    public function getWaresMinusData($wares , $pdr){
        
        $select = new Zend_Db_Select($this->db);
        $select->from(array("t1"=>"t_wares"),array("f_code","fc_asset_wizard_end", "f_subtitle_wiz", "f_imgtile_wiz", "f_linktitle_wiz", "f_order_wiz", "f_field_wiz", "f_color_wiz", "f_tilewidth_wiz", "f_srcbox_wiz"))
                ->join(array("t2"=>"t_creation_date"),"t2.f_id = t1.f_code",array("f_title","f_visibility"))
                ->join(array("t5"=>"t_wares_parent"),"t1.f_code = t5.f_code",array("f_parent_code"))
                ->where("t1.f_code IN (" . implode(',', $wares) . ")");
        $resWares = $select->query()->fetchAll();
        
        for($i = 0; $i < count($resWares); $i++){
            $result[] = array(
                'f_code'=>'w_'. $resWares[$i]['f_code'],
                'f_parent_code'=> $resWares[$i]['f_parent_code'] == 0 ? $pdr : 'w_'. $resWares[$i]['f_parent_code'],
                'f_title'=>  Mainsim_Model_Utilities::chg($resWares[$i]['f_title']),
                'f_visibility'=> $resWares[$i]['f_visibility'],
                'f_subtitle' => $resWares[$i]['f_subtitle_wiz'] ? Mainsim_Model_Utilities::chg($resWares[$i]['f_subtitle_wiz']) : '',
                'f_imgtile' => $resWares[$i]['f_imgtile_wiz'] ? $resWares[$i]['f_imgtile_wiz'] : '',
                'f_linktitle' => $resWares[$i]['f_linktitle_wiz'] ? $resWares[$i]['f_linktitle_wiz'] : '',
                'f_order' => $resWares[$i]['f_order_wiz'] ? $resWares[$i]['f_order_wiz'] : 0,
                'f_field' => $resWares[$i]['f_field_wiz'] ? $resWares[$i]['f_field_wiz'] : '',
                'f_color' => $resWares[$i]['f_color_wiz'] ? $resWares[$i]['f_color_wiz'] : '',
                'f_tilewidth' => $resWares[$i]['f_tilewidth_wiz'] ? $resWares[$i]['f_tilewidth_wiz'] : 0,
                'f_srcbox' => $resWares[$i]['f_srcbox_wiz'] ? $resWares[$i]['f_srcbox_wiz'] : ''
            );
        }
        
        return $result;
    }
    
    public function getWaresMinus($ware_start, &$result, &$visited, $wares_to_avoid, $pdr, $waresByUserSelectors = null){

        $select = new Zend_Db_Select($this->db);
        
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $ul = $userData['fc_usr_level'];
        }
        else{
            $ul = Zend_Auth::getInstance()->getIdentity()->f_level;
        }
        
        // get ware 
        if($ware_start != 0){
            $visited[] = $ware_start;
        }
        else{
            $visited = array(0);
        }

        // get children 
        $select->from('t_wares_parent', array('f_code'))
               ->join('t_creation_date', 't_wares_parent.f_code = t_creation_date.f_id', array())
               ->join('t_wares', 't_wares.f_code = t_creation_date.f_id', array())
               ->join("t_wf_phases","t_creation_date.f_wf_id = t_wf_phases.f_wf_id and t_creation_date.f_phase_id = t_wf_phases.f_number",array())
               ->join("t_wf_groups" ,"t_wf_phases.f_group_id = t_wf_groups.f_id",array())
               ->where("t_wares_parent.f_parent_code = " . $ware_start)
               ->where("(t_creation_date.f_visibility & $ul) != 0")
               ->where("(t_wf_phases.f_visibility & $ul) != 0")
               ->where("(t_wf_groups.f_visibility & $ul) != 0")
               ->where("t_creation_date.f_id NOT IN (" . $wares_to_avoid . ")") 
               ->where("t_wares.fc_asset_wizard_end != 2 OR t_wares.fc_asset_wizard_end IS NULL")
               ->where("t_creation_date.f_id NOT IN (" . implode(",", $visited) . ")")
               ->where("t_creation_date.f_category = 'ASSET'");
        if(is_array($waresByUserSelectors)){
            $select->where("t_creation_date.f_id IN (" . implode(",", $waresByUserSelectors) . ")");
        }
        $resChildren = $select->query()->fetchAll();
        for($i = 0; $i < count($resChildren); $i++){
            $this->getWaresMinus($resChildren[$i]['f_code'], $result, $visited, $wares_to_avoid, $pdr);
        }
    }
    
    public function getWaresListMinus($ware_start, &$result, &$visited, $waresToAvoid, $pdr, $waresByUserSelectors = null){
        
        $select = new Zend_Db_Select($this->db);
        // get f_code from alias
        if(is_array($ware_start)){
            
            $select->from('t_selector_ware', array('f_ware_id'))
                   ->where('f_selector_id IN (' . $ware_start[1] . ')');
            $resSelectors = $select->query()->fetchAll();
            for($i = 0; $i < count($resSelectors); $i++){
                $waresToAvoid[] = $resSelectors[$i]['f_ware_id'];
            }
            if($ware_start[0] != 'root'){
                
                $select->reset();
                $select->from('t_wares', 'f_code')
                        ->where("fc_asset_wizard = '" . $ware_start[0] . "'");
                $res = $select->query()->fetchAll();
                $ware_start = $res[0]['f_code'];
            }
            else{
                $ware_start = 0;
            }
        }
        
        
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $ul = $userData['fc_usr_level'];
        }
        else{
            $ul = Zend_Auth::getInstance()->getIdentity()->f_level;
        }
        
        // get ware 
        if($ware_start != 0){
            $visited[] = $ware_start;
        }
        else{
            $visited = array(0);
        }

        // get children 
        $select->reset();
        $select->from('t_wares_parent', array('f_code'))
               ->join('t_creation_date', 't_wares_parent.f_code = t_creation_date.f_id', array())
               ->join('t_wares', 't_wares.f_code = t_creation_date.f_id', array())
               ->join("t_wf_phases","t_creation_date.f_wf_id = t_wf_phases.f_wf_id and t_creation_date.f_phase_id = t_wf_phases.f_number",array())
               ->join("t_wf_groups" ,"t_wf_phases.f_group_id = t_wf_groups.f_id",array())
               ->where("t_wares_parent.f_parent_code = " . $ware_start)
               ->where("(t_creation_date.f_visibility & $ul) != 0")
               ->where("(t_wf_phases.f_visibility & $ul) != 0")
               ->where("(t_wf_groups.f_visibility & $ul) != 0")
               ->where("t_creation_date.f_id NOT IN (" . implode(",", $waresToAvoid) . ")") 
               ->where("t_wares.fc_asset_wizard_end != 2 OR t_wares.fc_asset_wizard_end IS NULL")
               ->where("t_creation_date.f_id NOT IN (" . implode(",", $visited) . ")")
               ->where("t_creation_date.f_category = 'ASSET'")
               ->where('t_wf_groups.f_id NOT IN (6,7)');
        $resChildren = $select->query()->fetchAll();
        for($i = 0; $i < count($resChildren); $i++){
            $this->getWaresListMinus($resChildren[$i]['f_code'], $result, $visited, $waresToAvoid, $pdr, $waresByUserSelectors);
        }
    }
	
	public function getWaresListStartOld($ware_start, &$result, &$visited, $waresToAvoid, $pdr, $waresByUserSelectors = null, $fc_asset_wizard_end = null){
        
        $select = new Zend_Db_Select($this->db);
        if(!is_numeric($ware_start)){
                $select->from('t_wares', array('f_code'))
                        ->where('fc_asset_wizard ="' . $ware_start . '"');
                $res = $select->query()->fetchAll();
                $ware_start = $res[0]['f_code'];
        }
        else{
                // get ware 
                if($ware_start != 0){
                        $visited[] = $ware_start;
                }
                else{
                        $visited = array(0);
                }
        }
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $ul = $userData['fc_usr_level'];
        }
        else{
            $ul = Zend_Auth::getInstance()->getIdentity()->f_level;
        }
        
        if($fc_asset_wizard_end == 1){
            return;
        }
        
        
        // get children 
        $select->reset();
        $select->from('t_wares_parent', array('f_code'))
               ->join('t_creation_date', 't_wares_parent.f_code = t_creation_date.f_id', array())
               ->join('t_wares', 't_wares.f_code = t_creation_date.f_id', array('fc_asset_wizard_end'))
			   ->join('t_custom_fields', 't_wares.f_code = t_custom_fields.f_code', array())
               ->join("t_wf_phases","t_creation_date.f_wf_id = t_wf_phases.f_wf_id and t_creation_date.f_phase_id = t_wf_phases.f_number",array())
               ->join("t_wf_groups" ,"t_wf_phases.f_group_id = t_wf_groups.f_id",array())
               ->where("t_wares_parent.f_parent_code = " . $ware_start)
               ->where("(t_creation_date.f_visibility & $ul) != 0")
               ->where("(t_wf_phases.f_visibility & $ul) != 0")
               ->where("(t_wf_groups.f_visibility & $ul) != 0")
               ->where("t_wares.fc_asset_wizard_end != 2 OR t_wares.fc_asset_wizard_end IS NULL")
               ->where("t_creation_date.f_category = 'ASSET'")
               ->where('t_wf_groups.f_id NOT IN (6,7)');
		if(count($waresToAvoid) > 0){
			$select->where("t_creation_date.f_id NOT IN (" . implode(",", $waresToAvoid) . ")");
		}
		
		if(is_array($visited)){
			$select->where("t_creation_date.f_id NOT IN (" . implode(",", $visited) . ")");
		}
		$resChildren = $select->query()->fetchAll();
		
		
        for($i = 0; $i < count($resChildren); $i++){
            $this->getWaresListStart($resChildren[$i]['f_code'], $result, $visited, $waresToAvoid, $pdr, $waresByUserSelectors, $resChildren[$i]['fc_asset_wizard_end']);
        }
    }
	
    public function getWaresListStart($ware_start, &$result, &$visited, $waresToAvoid, $pdr, $waresByUserSelectors = null, $fc_asset_wizard_end = null){
		$select = new Zend_Db_Select($this->db);
        $includeWareStart = true;
		if(!is_numeric($ware_start) && !is_array($ware_start)){
                $select->from('t_wares', array('f_code', 'fc_asset_wizard_end'))
                        ->where("fc_asset_wizard ='" . $ware_start . "'");
                if(!empty($waresByUserSelectors)){
                    $select->where('f_code IN (' . implode(',', $waresByUserSelectors) . ')');
                }
                $res = $select->query()->fetchAll();
                if(count($res) > 1){
                    $ware_start = array();
                    for($i = 0; $i < count($res); $i++){
                        $ware_start[] = $res[$i]['f_code'];
                        
                    }
                }
                else{
                    $ware_start = $res[0]['f_code'];
                    
                }
                                
        }
        
        if(empty($ware_start)) return;
        
        if(!is_array($ware_start)){
            $visited[] = $ware_start;
        }
        else{
            $visited = array_merge($visited, $ware_start);
        }
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $ul = $userData['fc_usr_level'];
        }
        else{
            $ul = Zend_Auth::getInstance()->getIdentity()->f_level;
        }
		
        
		/*
        if($fc_asset_wizard_end == 1){
            return;
        }*/
		
        
        // get children 
        $select->reset();
        $select->from('t_wares_parent', array('f_code'))
               ->join('t_creation_date', 't_wares_parent.f_code = t_creation_date.f_id', array())
               ->join('t_wares', 't_wares.f_code = t_creation_date.f_id', array('fc_asset_wizard_end'))
			   ->join('t_custom_fields', 't_wares.f_code = t_custom_fields.f_code', array())
               ->join("t_wf_phases","t_creation_date.f_wf_id = t_wf_phases.f_wf_id and t_creation_date.f_phase_id = t_wf_phases.f_number",array())
               ->join("t_wf_groups" ,"t_wf_phases.f_group_id = t_wf_groups.f_id",array())
               ->where("t_wares_parent.f_parent_code IN (" . (is_array($ware_start) ? implode(',', $ware_start) : $ware_start) . ")")
               ->where("(t_creation_date.f_visibility & $ul) != 0")
               ->where("(t_wf_phases.f_visibility & $ul) != 0")
               ->where("(t_wf_groups.f_visibility & $ul) != 0")
               ->where("t_wares.fc_asset_wizard_end != 2 OR t_wares.fc_asset_wizard_end IS NULL")
               ->where("t_creation_date.f_category = 'ASSET'")
               ->where('t_wf_groups.f_id NOT IN (6,7)');
               if(!empty($waresByUserSelectors)){
                $select->where('t_creation_date.f_id IN (' . implode(',', $waresByUserSelectors) . ')');
               }
		if(count($waresToAvoid) > 0){
			$select->where("t_creation_date.f_id NOT IN (" . implode(",", $waresToAvoid) . ")");
		}

		if(is_array($visited)){
			$select->where("t_creation_date.f_id NOT IN (" . (is_array($visited) ? implode(",", $visited) : $visited) . ")");
		}
		try{
		$resChildren = $select->query()->fetchAll();
		}
		catch(Exception $e){
			echo $select;
			var_dump($visited);
			var_dump($ware_start);
		}
		for($i = 0; $i < count($resChildren); $i++){
			
			if($resChildren[$i]['fc_asset_wizard_end'] != 1){
                            $toVisit[] = $resChildren[$i]['f_code'];
                            $visited[] = $resChildren[$i]['f_code'];
			}
		}

		if(is_array($toVisit)){
                    $this->getWaresListStart($toVisit, $result, $visited, $waresToAvoid, $pdr, $waresByUserSelectors);
		}
		else{return;}
    }
	
	public function getWaresListNotHierarchy($ware_start, &$visited){
		
		$aux = explode('_', $ware_start);
		
		// get selectors
		$selectors = $aux[2];
		
		// get wares from selectors
		$select = new Zend_Db_Select($this->db);
		$select->from('t_selector_ware', array('f_ware_id'))
			->where('f_selector_id IN (' . $selectors . ')');
        $res = $select->query()->fetchAll();
		for($i = 0; $i < count($res); $i++){
			$visited[] = $res[$i]['f_ware_id'];
		}
		
    }
    
    public function getWaresListPlus($ware_start, $pdr, $waresByUserSelectors = null){
        
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $ul = $userData['fc_usr_level'];
        }
        else{
            $ul = Zend_Auth::getInstance()->getIdentity()->f_level;
        }

        // get selectors
        $selectors = explode("_ADD_", $ware_start);
        // get wares associated to selectors
        $select = new Zend_Db_Select($this->db);
        $select->from('t_selector_ware')
               ->where('f_selector_id IN (' . $selectors[1] . ')');
        $resSelectors = $select->query()->fetchAll();
        $visited = array(); 
        // loop through wares, for each of them get his parents
        for($i = 0; $i < count($resSelectors); $i++){
            $parentsCodes = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $resSelectors[$i]['f_ware_id']);
            $parentsCodes[] = $resSelectors[$i]['f_ware_id'];
            $toVisitCodes = array();
            // remove elements already visited
            for($k = 0; $k < count($parentsCodes); $k++){
                if(!in_array($parentsCodes[$k], $visited)){
                    $toVisitCodes[] = $parentsCodes[$k];
                    $visited[] = $parentsCodes[$k];
                }
            }
            
            // if a parent ware has hidden property set, delete tree branch from result
            if(count($toVisitCodes) > 0){
                $select->reset();
                $select->from('t_wares', array('count(*) AS tot'))
                       ->where('f_code IN (' . implode(",", $toVisitCodes) . ')')
                       ->where('fc_asset_wizard_end = 2');
                $res = $select->query()->fetchAll();

                if($res[0]['tot'] == 0){

                    //array_unshift($toVisitCodes, $resSelectors[$i]['f_ware_id']);
                    $select->reset();
                    $select->from(array("t1"=>"t_wares"),array("f_code","fc_asset_wizard_end", "f_subtitle_wiz", "f_imgtile_wiz", "f_linktitle_wiz", "f_order_wiz", "f_field_wiz", "f_color_wiz", "f_tilewidth_wiz", "f_srcbox_wiz"))
                        ->join(array("t2"=>"t_creation_date"),"t2.f_id = t1.f_code",array("f_title","f_visibility"))
                        ->join(array("t3"=>"t_wf_phases"),"t2.f_wf_id = t3.f_wf_id and t2.f_phase_id = t3.f_number",array())
                        ->join(array("t4"=>"t_wf_groups"),"t3.f_group_id = t4.f_id",array())
                        ->join(array("t5"=>"t_wares_parent"),"t1.f_code = t5.f_code",array("f_parent_code"))
                        ->where("t1.f_code IN (" . implode(",", $toVisitCodes) . ")")
                        ->where("(t2.f_visibility & $ul) != 0")
                        ->where("(t3.f_visibility & $ul) != 0")
                        ->where("(t4.f_visibility & $ul) != 0");  
                    if(is_array($waresByUserSelectors)){
                        $select->where("t1.f_code IN (" . implode(",", $waresByUserSelectors) . ")");
                    }
                    $resParents = $select->query()->fetchAll();

                    for($j = 0; $j < count($resParents); $j++){
                        $result[] = $resParents[$j]['f_code'];
                    } 
                }
            }
        }
        return $result;
    }
    
    /*
    private function getUserSelectors(){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $userCode = $userData['f_code'];
        }
        else{
            $userCode = Zend_Auth::getInstance()->getIdentity()->f_id;
        }
        
        $select = new Zend_Db_Select($this->db);
        $select->from('t_selector_ware', array('f_selector_id'))
               ->join('t_selectors', 't_selector_ware.f_selector_id = t_selectors.f_code', array('f_type_id'))
               ->where('f_ware_id = ' . $userCode);
        $res = $this->db->query($select)->fetchAll();
        if(count($res) > 0){
            for($i = 0; $i < count($res); $i++){
                $selectors[$res[$i]['f_type_id']][] = $res[$i]['f_selector_id'];
            }
            
            return $selectors;
        }
        else{
            return null;
        }
    }
    
    private function getWaresBySelectors($selectors, $locked = array()){  
        $userData = Zend_Auth::getInstance()->getIdentity();
        $result = array();
        $selectWare = new Zend_Db_Select($this->db);
        if(count($selectors) > 0){
            
            $all = array();
            foreach($selectors as $key => $value){
                $selectWare->reset();
                // get ware by selector
                $selectWare->from('t_wares', array('f_code'))
                         ->join('t_creation_date', "t_wares.f_code = t_creation_date.f_id", array())
                         ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                         ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                         ->join('t_selector_ware', 't_wares.f_code = t_selector_ware.f_ware_id', array())
                         ->where('t_wf_groups.f_id NOT IN (6,7)')
                         ->where('t_selector_ware.f_selector_id IN (' . implode(',', $value) . ')')
                         ->where('t_wares.f_type_id = 1');
                // if locked wares are passed in remove them from result
                if(count($locked)){
                    $selectWare->where('t_wares.f_code NOT IN (' . implode(',', $locked) . ')');
                }
                $resWare = $selectWare->query()->fetchAll();
                $aux = array();
                for($i = 0; $i < count($resWare); $i++){
                    $aux[] = $resWare[$i]['f_code'];
                }
                if(!$multiple){
                    $multiple = $aux;
                }
                else{
                    $multiple = array_intersect($multiple, $aux);
                }
                
                for($i = 0; $i < count($aux); $i++){
                    if(!in_array($aux[$i], $all)){
                        $all[] = $aux[$i];
                    }
                }
                $selectorTypes[] = $key;
            }
            $result = $multiple;
            
            $single = array_diff($all, $multiple);
            if(is_array($single) && count($single) > 0){
                // check that all wares with only one selector (id, not type) don't have other selectors of type of users selectors
                $selectWare->reset();
                $selectWare->from('t_wares', array('f_code'))
                        ->join('t_selector_ware', 't_wares.f_code = t_selector_ware.f_ware_id', array())
                        ->join('t_selectors', 't_selector_ware.f_selector_id = t_selectors.f_code', array())
                        ->where('t_selectors.f_type_id IN (' . implode(',', $selectorTypes) . ')')
                        ->where('t_wares.f_code IN (' . implode(',', $single) . ')')
                        ->group('t_wares.f_code')
                        ->having('count(*) = 1');
                $resWare = $selectWare->query()->fetchAll();
                for($i = 0; $i < count($resWare); $i++){
                    $result[] = $resWare[$i]['f_code'];
                }
            }
            
            // get ware related to selector's types not contained in user selectors
            $selectWare->reset();
            $selectWare->from('t_wares', array('f_code'))
                     ->join('t_creation_date', "t_wares.f_code = t_creation_date.f_id", array())
                     ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->join('t_selector_ware', 't_wares.f_code = t_selector_ware.f_ware_id', array())
                     ->join('t_selectors', 't_selectors.f_code = t_selector_ware.f_selector_id', array('f_type_id'))
                     ->where('t_wf_groups.f_id NOT IN (6,7)')
                     ->where('t_selectors.f_type_id NOT IN (' . implode(',', $selectorTypes) . ')')
                     ->where('t_wares.f_type_id = 1');
            // if locked warerkorders are passed in remove them from result
            if(count($locked)){
                $selectWare->where('t_wares.f_code NOT IN (' . implode(',', $locked) . ')');
            }
            
            $resWare = $selectWare->query()->fetchAll();
            for($i = 0; $i < count($resWare); $i++){
                $result[] = $resWare[$i]['f_code'];
            }

            // get ware without selector relation
            $selectWare->reset();
            $selectWare->from('t_wares', array('f_code'))
                     ->join('t_creation_date', "t_wares.f_code = t_creation_date.f_id", array())
                     ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->where('t_wf_groups.f_id NOT IN (6,7)')
                     ->where('t_wares.f_code NOT IN (SELECT f_ware_id FROM t_selector_ware)')
                     ->where('t_wares.f_type_id = 1');
            //echo $selectWare;
            // if locked ware are passed in remove them from result
            if(count($locked)){
                $selectWare->where('t_wares.f_code NOT IN (' . implode(',', $locked) . ')');
            }
            $resWare = $selectWare->query()->fetchAll();
            for($i = 0; $i < count($resWare); $i++){
                $result[] = $resWare[$i]['f_code'];
            }
            return $result;
        }
        else{
            return null;
        }
    }*/
    
    public function getWaresBySelectors($selectors){  
        $userData = Zend_Auth::getInstance()->getIdentity();
        $result = array();
        $selectWare = new Zend_Db_Select($this->db);
        // loop through selector types
        $singleWa = array();
        
        $selectWare->from('t_creation_date')
                   ->where("f_title ='WIZARD_ASSET_PHASES'");
        $resSettings = $selectWare->query()->fetchAll();
        $phases = $resSettings[0]['f_description'];
        
        if(count($selectors) > 0){
        
            foreach($selectors as $key => $value){

                $selectorTypes[] = $key; 

                $selectWare->reset();
                // get wo by selector
                $selectWare->from('t_wares', array('f_code'))
                         ->join('t_creation_date', "t_wares.f_code = t_creation_date.f_id", array())
                         ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                         ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                         ->join('t_selector_ware', 't_wares.f_code = t_selector_ware.f_ware_id', array())
                         ->join('t_selectors', 't_selectors.f_code = t_selector_ware.f_selector_id', array('f_type_id'))
                         ->where('t_wf_groups.f_id NOT IN (6,7)')
                         ->where('t_selector_ware.f_selector_id IN (' . implode(",", $value) . ')')
                         ->where('t_wares.f_type_id = 1')
                         ->order(array('t_wares.f_priority DESC', 't_creation_date.f_creation_date'));
                
                if($phases != null){
                    $selectWare->where('t_creation_date.f_phase_id IN (' . $phases . ')');
                }
                
                //echo $selectWare; die();
                $resWare = $selectWare->query()->fetchAll();
                for($i = 0; $i < count($resWare); $i++){
                    $aux[] = $resWare[$i]['f_code'];
                }
                if(count($result) > 0){
                    $result = array_intersect($aux, $result);

                }
                else{
                    $result = $aux;
                }
                $wareBySelectorType[$key] = $aux;
                
                // get wo of the same type of user selector but not included
            }
            foreach($wareBySelectorType as $key => $value){
                for($i = 0; $i < count($value); $i++){
                    if(in_array($value[$i], $result)){
                        unset($value[$i]);
                    }
                }
                if(is_array($value)){
                    $singleWare[$key] = array_values($value);
                }
            }
            // check if wares not related with all user's selectors have been associated with selectors of same type of user selectors types
            if(count($singleWare) > 0 && count($selectorTypes) > 1){

                foreach($singleWare as $key => $value){

                    if(count($value) > 0){
                        $auxSelectorTypes = $selectorTypes;
                        for($k = 0; $k < count($auxSelectorTypes); $k++){
                            if($auxSelectorTypes[$k] == $key){
                                unset($auxSelectorTypes[$k]);
                            }
                        }
                        $selectWare->reset();
                        $selectWare->from('t_wares', array('f_code'))
                                 ->join('t_creation_date', "t_wares.f_code = t_creation_date.f_id", array())
                                 ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                                 ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                                 ->join('t_selector_ware', 't_wares.f_code = t_selector_ware.f_ware_id', array())
                                 ->join('t_selectors', 't_selectors.f_code = t_selector_ware.f_selector_id', array('f_type_id'))
                                 ->where('t_wf_groups.f_id NOT IN (6,7)')
                                 ->where('t_creation_date.f_id IN (' . implode(',', $value) . ')')
                                 ->where('t_selectors.f_type_id IN (' . implode(',', $auxSelectorTypes) . ')');
                        
                        if($phases != null){
                            $selectWare->where('t_creation_date.f_phase_id IN (' . $phases . ')');
                        }
                        
                        $resWare = $selectWare->query()->fetchAll();
                        //print_r($resWo);
                        if(count($resWare) > 0){
                            for($i = 0; $i < count($resWare); $i++){
                                $aux[] = $resWare[$i]['f_code'];
                            }
                        }
                        $result = array_merge($result, array_diff($value, $aux));
                    }
                }
            }
            $aux = $result;
            for($i = 0; $i < count($aux); $i++){
                $children = Mainsim_Model_Utilities::getChildCode('t_wares_parent', $aux[$i]);
                foreach($children as $key => $value){
                    $result[] = $value;
                }
            }
        }
        else{
            // get wares not related with user's selector types

            $selectWare->reset();
            if(count($result) > 0){
                $selectWare->from('t_wares', array('f_code'))
                     ->join('t_creation_date', "t_wares.f_code = t_creation_date.f_id", array())
                     ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->where('t_wf_groups.f_id NOT IN (6,7)')
                     ->where('t_wares.f_type_id = 1')
                     ->where('t_creation_date.f_id NOT IN (' . implode(",", $result) . ')')
                     ->where('t_creation_date.f_id NOT IN (SELECT t_selector_ware.f_ware_id FROM t_selector_ware INNER JOIN t_selectors ON t_selectors.f_code = t_selector_ware.f_selector_id WHERE t_selectors.f_type_id IN (' . implode(',', $selectorTypes) . ') AND (t_selector_ware.f_ware_id NOT IN (' . implode(",", $result) . ')))');
                
                     if($phases != null){
                        $selectWare->where('t_creation_date.f_phase_id IN (' . $phases . ')');
                    }
            }
            else{
                $selectWare->from('t_wares', array('f_code'))
                     ->join('t_creation_date', "t_wares.f_code = t_creation_date.f_id", array())
                                     ->join('t_custom_fields', "t_custom_fields.f_code = t_wares.f_code", array())
                                     ->join('t_wares_parent', "t_wares_parent.f_code = t_custom_fields.f_code", array())
                     ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->where('t_wf_groups.f_id NOT IN (6,7)')
                     ->where('t_wares.f_type_id = 1');
                     if($phases != null){
                    $selectWare->where('t_creation_date.f_phase_id IN (' . $phases . ')');
                }
                if(is_array($selectorTypes)){
                    $selectWare->where('t_creation_date.f_id NOT IN (SELECT t_selector_ware.f_ware_id FROM t_selector_ware INNER JOIN t_selectors ON t_selectors.f_code = t_selector_ware.f_selector_id WHERE t_selectors.f_type_id IN (' . implode(',', $selectorTypes) . '))');
                }     
            }
            // if locked warerkorders are passed in remove them from result
            if(count($locked)){
                $selectWare->where('t_wares.f_code NOT IN (' . implode(',', $locked) . ')');
            }
            $resWare = $selectWare->query()->fetchAll();
            if(count($resWare) > 0){
                for($i = 0; $i < count($resWare); $i++){
                    $result[] = $resWare[$i]['f_code'];
                }
            }
        }
        return $result;
    }
    
    public function getUserSelectors(){
        
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $f_code = $userData['f_code'];
        }
        else{
            $f_code = Zend_Auth::getInstance()->getIdentity()->f_id;
        }

        $select = new Zend_Db_Select($this->db);
        $select->from('t_selector_ware', array('f_selector_id'))
               ->join('t_selectors', 't_selector_ware.f_selector_id = t_selectors.f_code', array('f_type_id'))
               ->where('f_ware_id = ' . $f_code);
        $res = $select->query()->fetchAll();
        if(count($res) > 0){
            for($i = 0; $i < count($res); $i++){
                $selectors[$res[$i]['f_type_id']][] = $res[$i]['f_selector_id'];
            }
            
            return $selectors;
        }
        else{
            return null;
        }
    }
    
    /*
     * Read 
     */
    public function waresWizard($ware_start, $pdr)
    {        
        // get users selectors
        $userSelectors = $this->getUserSelectors();
        if($userSelectors){
            // get wares codes by selectors
            $waresByUserSelectors = $this->getWaresBySelectors($userSelectors);
        }
        else{
            $waresByUserSelectors = null;
        }
        
		
        // + wares
        if(strpos($ware_start, "_ADD_") > 0){
            return $this->getWaresPlus($ware_start, $pdr, $waresByUserSelectors);
        }
        // - wares
        else if(strpos($ware_start, "_SUB_") > 0){
            
            // get selectors and root ware
            $aux = explode("_SUB_", $ware_start);
            // get wares associated to selectors
            $select = new Zend_Db_Select($this->db);
            $select->from('t_selector_ware', array('f_ware_id'))
                   ->where('f_selector_id IN (' . $aux[1] . ')');
            $resSelectors = $select->query()->fetchAll();
            for($i = 0; $i < count($resSelectors); $i++){
                $waresToAvoid[] = $resSelectors[$i]['f_ware_id'];
            }
            $visited = array();
            
            // get f_code from alias
            if($aux[0] != 'root'){
                $select->reset();
                $select->from('t_wares', 'f_code')
                        ->where("fc_asset_wizard = '" . $aux[0] . "'");
                $res = $select->query()->fetchAll();
                $f_code = $res[0]['f_code'];
            }
            else{
                $f_code = 0;
            }
            
            $this->getWaresMinus($f_code, $result, $visited, implode(",", $waresToAvoid), $pdr, $waresByUserSelectors);
            $result = $this->getWaresMinusData($visited, $pdr);
            return $result;
        }
        else{
			$select = new Zend_Db_Select($this->db);
            $type_id = 0;
            if($ware_start != 'root'){
                $arr=array();    
				
                //get start element 
                $select->from("t_wares",array("f_code"))
                       ->join("t_creation_date","t_wares.f_code = t_creation_date.f_id",array())
                       ->join("t_wf_phases","t_creation_date.f_wf_id = t_wf_phases.f_wf_id and t_creation_date.f_phase_id = t_wf_phases.f_number",array())
                       ->join("t_wf_groups","t_wf_phases.f_group_id = t_wf_groups.f_id",array())
					->where('t_wf_groups.f_id NOT IN (6,7)')
						
                     ->where("fc_asset_wizard = ?",$ware_start)
                     ->where("t_wares.f_type_id = 1");
                if(is_array($waresByUserSelectors)){
                    $select->where("t_wares.f_code IN (" . implode(",", $waresByUserSelectors) . ")");
                }
                $res = $select->query()->fetch();        
                if(empty($res)) return $arr;        
                $f_code=(int) $res['f_code'];
            }
            else{
                $f_code = 0;
                $type_id = 1;
            }
            $children = array($f_code);
            $res = $this->getChildCodeAsset($f_code, $children,array(),$pdr, $type_id, $waresByUserSelectors);
        }
        return $res;
    }
    
    public function getChildCodeAsset($f_code,&$child_codes = array(),$result = array(),$pdr = 0, $type_id = 0, $waresByUserSelectors = null)
    {           
        
        
        $custom = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
        $wiz_custom = array('subtitle','imgtile','order','field','color','tilewidth');
        $check = array_diff($wiz_custom,$custom);
        if($_GET['ns']){
            $userData = Zend_Auth::getInstance()->getIdentity();
            $ul = $userData['fc_usr_level'];
        }
        else{ 
            $ul = Zend_Auth::getInstance()->getIdentity()->f_level;
        }
        $select = new Zend_Db_Select($this->db);
        if(!is_array($f_code)) {                        
            $f_code = array($f_code);
        }
        $f_parent_codes = implode(',',$f_code);        
        $new_parent = array();
        $select->reset();
        $select->from(array("t1"=>"t_wares"),array("f_code","fc_asset_wizard_end", "f_subtitle_wiz", "f_imgtile_wiz", "f_linktitle_wiz", "f_order_wiz", "f_field_wiz", "f_color_wiz", "f_tilewidth_wiz", "f_srcbox_wiz"))
                ->join(array("t2"=>"t_creation_date"),"t2.f_id = t1.f_code",array("f_title","f_visibility"))
                ->join(array("t3"=>"t_wf_phases"),"t2.f_wf_id = t3.f_wf_id and t2.f_phase_id = t3.f_number",array())
                ->join(array("t4"=>"t_wf_groups"),"t3.f_group_id = t4.f_id",array())
                ->join(array("t5"=>"t_wares_parent"),"t1.f_code = t5.f_code",array("f_parent_code"))
                ->where("t5.f_parent_code IN ($f_parent_codes)")->where("t5.f_active = 1")
                ->where("(t2.f_visibility & $ul) != 0")
                ->where("(t3.f_visibility & $ul) != 0")
                ->where("(t4.f_visibility & $ul) != 0");    
        if(is_array($waresByUserSelectors)){
            $select->where("t1.f_code IN (" . implode(",", $waresByUserSelectors) . ")");
        }
        if($type_id){
            $select->where("t1.f_type_id = " . $type_id);
        }
        if(!empty($child_codes) && count($result) != 0) {
            $not_in = implode(',',$child_codes);
            //$select->where("t5.f_code not in ($not_in)");
        }        
        if(count($check) == 0) {
            $select->join(array("t6"=>"t_custom_fields"),"t6.f_code = t2.f_id", array('subtitle','img_tile','order_field','color','tile_width'));
        }
        /*
        if($counter == 1){
            echo $select;die('ddd' . $counter);
        }*/
        

        $res = $select->query()->fetchAll(); 
        $pos = count($result);
        
        $first_tot = $pos;
        foreach($res as $line) {            
            $child_codes[] = (int)$line['f_code'];
            if($line['fc_asset_wizard_end'] != 1 && $line['fc_asset_wizard_end'] != 2) {
                $new_parent[] = (int)$line['f_code'];
            }
            
            if($line['fc_asset_wizard_end'] != 2){
                $result[$pos] = array(
                    'f_code'=>'w_'.$line['f_code'],
                    'f_parent_code'=>$first_tot == 0?$pdr:'w_'.$line['f_parent_code'],
                    'f_title'=>  Mainsim_Model_Utilities::chg($line['f_title']),
                    'f_visibility'=>$line['f_visibility'],
                    'f_subtitle' => $line['f_subtitle_wiz'] ? Mainsim_Model_Utilities::chg($line['f_subtitle_wiz']) : '',
                    'f_imgtile' => $line['f_imgtile_wiz'] ? $line['f_imgtile_wiz'] : '',
                    'f_linktitle' => $line['f_linktitle_wiz'] ? $line['f_linktitle_wiz'] : '',
                    'f_order' => $line['f_order_wiz'] ? $line['f_order_wiz'] : 0,
                    'f_field' => $line['f_field_wiz'] ? $line['f_field_wiz'] : '',
                    'f_color' => $line['f_color_wiz'] ? $line['f_color_wiz'] : '',
                    'f_tilewidth' => $line['f_tilewidth_wiz'] ? $line['f_tilewidth_wiz'] : 0,
                    'f_srcbox' => $line['f_srcbox_wiz'] ? $line['f_srcbox_wiz'] : ''
                );
                $pos++;
            }
            /*
            if(isset($line['color'])) {
                $result[$pos]['subtitle'] = $line['subtitle'];
                $result[$pos]['img_tile'] = $line['imgtile'];
                $result[$pos]['order'] = $line['order'];
                $result[$pos]['field'] = $line['field'];
                $result[$pos]['color'] = $line['color'];
                $result[$pos]['tile_width'] = $line['tilewidth'];
            }*/
            
        } 
        if(!empty($new_parent)) {             
            $result = $this->getChildCodeAsset($new_parent, $child_codes,$result, $pdr, $type_id, null);
        }        
        return $result;
    }
    
    public function saveAttachment($base64, $code, $timestamp, $title = 'document', $type = 'jpg'){
       
        $attachmentFolder = Zend_Registry::get('attachmentsFolder');
        // create destination dir if not exists
        if(!file_exists($attachmentFolder . '/doc/' . $code)){
            mkdir($attachmentFolder . '/doc/' . $code);
        }
        if($type == 'jpg'){
        
            try{
                $aux = explode(",", $base64);
                $imageString = base64_decode($aux[1]);
                $imageResource = imagecreatefromstring($imageString);
                // save image
                $userData = Zend_Auth::getInstance()->getIdentity();
                // transform userinfo array to object for mobile  users
                if(is_array($userData)){
                    $aux = $userData;
                    $userData = new stdClass();
                    foreach($aux as $key => $value){
                            if($key == 'f_code'){
                                    $userData->f_id = $value;
                            }
                            else{
                                    $userData->{$key} = $value;
                            }
                    }
                }
                
                $aux = explode('.', $title);
                
                $filename = $title;
                
                imagejpeg($imageResource, $attachmentFolder . '/doc/' . $code . '/' . $timestamp . '_' . $userData->f_id . "_doc.jpg");
            }
            catch(Exception $e){
                print($e->getMessage());
            }
        }
        
        // insert into t_attachments
        
        $data = array(
            'f_code' => $code,
            'f_session_id' => $timestamp . '_' . $userData->f_id,
            'f_timestamp' => $timestamp,
            'f_active' => 1,
            'f_fieldname' => 'fieldname',
            'f_file_name' => $filename,
            'f_file_ext' => $type,
            'f_type' => 'doc',
            'f_mime' => 'image/jpeg',
            'f_path' => $attachmentFolder . DIRECTORY_SEPARATOR.'doc'.DIRECTORY_SEPARATOR . $code . DIRECTORY_SEPARATOR . $timestamp . '_' . $userData->f_id . "_doc.jpg"
        );
        
        $this->db->insert('t_attachments', $data);
        
        // update t_wares
        $data2 = array(
            'fc_doc_attach' => $timestamp . '_' . $userData->f_id . '|' . $filename
        );
        
        $update = "UPDATE t_wares "
                . "SET fc_doc_attach = '" . $timestamp . '_' . $userData->f_id . '|' . $filename . "' "
                . "WHERE f_code = " . $code;
        //$this->db->query($update);
    }
    
    public function isMysql()
    {
        $config = Zend_Registry::get('db');
        $dbConf = $config->toArray();
        return stripos($dbConf['adapter'],'PDO_MYSQL') !== false ? true:false;   
    }
    
    public function boxPage($script){
        try{
            include(APPLICATION_PATH . "/../new_wizard/boxpage/scripts/" . $script . ".php");
        } catch (Exception $ex) {
          //  print($ex->getMessage());
        }
    }
    
    public function getWaresParentRelations2($wareList){
        $parentRelations = array();
            $aux = $wareList;
            while(count($aux) > 0){
                    $select = new Zend_Db_Select($this->db);
                    $select->from('t_wares_parent')
                                    ->where('f_code IN (' . implode(',', $aux) . ')');
                    $resAux = $select->query()->fetchAll();
                    $aux = array();
                    $aux2 = array();
                    for($i = 0; $i < count($resAux); $i++){   
                        if(in_array($resAux[$i]['f_parent_code'], $wareList) || $resAux[$i]['f_parent_code'] == 0){
                                if(in_array($resAux[$i]['f_code'], $aux2)){
                                            if(!isset($parentRelations[$aux2[$resAux[$i]['f_code']]])){
                                                    $parentRelations[$aux2[$resAux[$i]['f_code']]] = array($resAux[$i]['f_parent_code']);
                                            }
                                            else{
                                                    $parentRelations[$aux2[$resAux[$i]['f_code']]][] = $resAux[$i]['f_parent_code'];
                                            }
                                            unset($aux2[$resAux[$i]['f_code']]);
                                    }
                                    else{
                                            if(!isset($parentRelations[$resAux[$i]['f_parent_code']])){
                                                    $parentRelations[$resAux[$i]['f_parent_code']] = array($resAux[$i]['f_code']);
                                            }
                                            else{
                                                    $parentRelations[$resAux[$i]['f_parent_code']][] = ($resAux[$i]['f_code']);
                                            }
                                    }

                        }
                        else{
                                $aux[] = $resAux[$i]['f_parent_code'];
                                $aux2[$resAux[$i]['f_parent_code']] = $resAux[$i]['f_code'];
                        }
                    }
            }
        return $parentRelations;
    }

    public function getWaresParentRelations3($wareList){
        $parentRelations = array();
            $aux = $wareList;
            while(count($aux) > 0){
                    $select = new Zend_Db_Select($this->db);
                    $select->from('t_wares_parent')
                                    ->where('f_code IN (' . implode(',', $aux) . ')');
                    $resAux = $select->query()->fetchAll();
                    $aux = array();
                    $aux2 = array();
                    for($i = 0; $i < count($resAux); $i++){   
                        if(in_array($resAux[$i]['f_parent_code'], $wareList) || $resAux[$i]['f_parent_code'] == 0){
                                if(in_array($resAux[$i]['f_code'], $aux2)){
                                            if(!isset($parentRelations[$aux2[$resAux[$i]['f_code']]])){
                                                    $parentRelations[$aux2[$resAux[$i]['f_code']]] = array($resAux[$i]['f_parent_code']);
                                            }
                                            else{
                                                    $parentRelations[$aux2[$resAux[$i]['f_code']]][] = $resAux[$i]['f_parent_code'];
                                            }
                                            unset($aux2[$resAux[$i]['f_code']]);
                                    }
                                    else{
                                            if(!isset($parentRelations[$resAux[$i]['f_parent_code']])){
                                                    $parentRelations[$resAux[$i]['f_parent_code']] = array($resAux[$i]['f_code']);
                                            }
                                            else{
                                                    $parentRelations[$resAux[$i]['f_parent_code']][] = ($resAux[$i]['f_code']);
                                            }
                                    }

                        }
                        else{
                            if(!isset($parentRelations[0])){
                                    $parentRelations[0] = array($resAux[$i]['f_code']);
                                     unset($aux2[$resAux[$i]['f_code']]);
                            }
                            else{
                                    $parentRelations[0][] = ($resAux[$i]['f_code']);
                            }
                        }
                    }
            }
        return $parentRelations;
    }
    
    public function getWaresParentRelations($wareList){
        $parentRelations = array();
        // loop through ware list
        for($i = 0; $i < count($wareList); $i++){
            // climb the tree until we find a ware that the user can see (included in $wareList)
            $parent = $this->getFirstVisibleParent($wareList[$i], $wareList);
            if(!isset($parentRelations[$parent])){
                $parentRelations[$parent] = array($wareList[$i]);
            }
            else{
                $parentRelations[$parent][] = $wareList[$i];
            }
        }
        return $parentRelations;
    }
    
    // get single ware data suitable for the wizard
    public function getWizardData($parentRelations, $pdr, $ware_start = null, $wareList = null){
        if(empty($wareList)) return [];
        
        $select = new Zend_Db_Select($this->db);
        if(!is_numeric($ware_start) && !is_array($ware_start)){
            if($ware_start != 'root' && strpos($ware_start, "_ADD") == 0 && strpos($ware_start, "_SUB")== 0){
                //unset($parentRelations[0]);
                $select->from('t_wares', array('f_code'))
                    ->where('fc_asset_wizard = ?', $ware_start);
                if($wareList != null){
                    $select->where('f_code IN (' . implode(",", $wareList) . ')');
                }
                $res = $select->query()->fetchAll();
            }
            else{
                 //$select->from('t_wares', array('f_code'))
                    //->where('f_code IN (' . implode(",", $wareList) . ')');
                 $res = array('f_code' => 0);
            }
            
            
        }
        else{
            $res[] = $ware_start;
        }
        for($i = 0; $i < count($res); $i++){
            $wareStartArray[] = $res[$i]['f_code'];
            if(isset($res[$i]['f_code'])){
                $parentRelations[0][] = $res[$i]['f_code'];
            }
        }

        $t_wares_fields = array("f_code","fc_asset_wizard_end", "f_subtitle_wiz", "f_imgtile_wiz", "f_linktitle_wiz", "f_order_wiz", "f_field_wiz", "f_color_wiz", "f_tilewidth_wiz", "f_srcbox_wiz",  "fc_asset_wo", "fc_asset_opened_wo", "fc_asset_opened_pm", "fc_asset_opened_on_condition");
        

       
        foreach($parentRelations as $key => $value){
            if($key == 0){
                $key = 'root';
            }
            $select->reset();
            $select->from('t_wares', $t_wares_fields)
                ->join('t_creation_date','t_creation_date.f_id = t_wares.f_code',array("f_title","f_visibility","fc_documents"))
                ->where('t_wares.f_code IN (' . implode(',', $value) . ')');
            $res2 = $select->query()->fetchAll();
            for($j = 0; $j < count($res2); $j++){
                //if(!in_array($res2[$j]['f_code'], $wareStartArray)){
                    $wizardData[] = array(
                        'f_code'=>'w_'. $res2[$j]['f_code'],
                        'f_parent_code'=>($key == 'root' /*|| (is_array($wareStartArray) && in_array($key,$wareStartArray))*/ || $key == '_pdr_') ? $pdr : 'w_' . $key,
                        'f_title'=>  Mainsim_Model_Utilities::chg($res2[$j]['f_title']),
                        'f_visibility'=>$res2[$j]['f_visibility'],
                        'f_subtitle' => $res2[$j]['f_subtitle_wiz'] ? Mainsim_Model_Utilities::chg($res2[$j]['f_subtitle_wiz']) : '',
                        'f_imgtile' => $res2[$j]['f_imgtile_wiz'] ? $res2[$j]['f_imgtile_wiz'] : '',
                        'f_linktitle' => $res2[$j]['f_linktitle_wiz'] ? $res2[$j]['f_linktitle_wiz'] : '',
                        'f_order' => $res2[$j]['f_order_wiz'] ? $res2[$j]['f_order_wiz'] : 0,
                        'f_field' => $res2[$j]['f_field_wiz'] ? $res2[$j]['f_field_wiz'] : '',
                        'f_color' => $res2[$j]['f_color_wiz'] ? $res2[$j]['f_color_wiz'] : '',
                        'f_tilewidth' => $res2[$j]['f_tilewidth_wiz'] ? $res2[$j]['f_tilewidth_wiz'] : 0,
                        'f_srcbox' => $res2[$j]['f_srcbox_wiz'] ? $res2[$j]['f_srcbox_wiz'] : '',
                        'f_asset_action' => $res2[$j]['f_action_wiz'] ? $res2[$j]['f_action_wiz'] : '',
                        'fc_documents' => $res2[$j]['fc_documents'] ? $res2[$j]['fc_documents'] : '',
                        'fc_asset_wo' => $res2[$j]['fc_asset_wo'] ? $res2[$j]['fc_asset_wo'] : '',
                        'fc_asset_opened_wo' => $res2[$j]['fc_asset_opened_wo'] ? $res2[$j]['fc_asset_opened_wo'] : '',
                        'fc_asset_opened_pm' => $res2[$j]['fc_asset_opened_pm'] ? $res2[$j]['fc_asset_opened_pm'] : '',
                        'fc_asset_opened_on_condition' => $res2[$j]['fc_asset_opened_on_condition'] ? $res2[$j]['fc_asset_opened_on_condition'] : '',
                    );
                //}
            }
        }
        
        /*
        if($ware_start == 'root'){
            foreach($parentRelations as $key => $value){
                if($key == 0){
                    $key = 'root';
                }
                $select->reset();
                $select->from('t_wares',array("f_code","fc_asset_wizard_end", "f_subtitle_wiz", "f_imgtile_wiz", "f_linktitle_wiz", "f_order_wiz", "f_field_wiz", "f_color_wiz", "f_tilewidth_wiz", "f_srcbox_wiz"))
                    ->join('t_creation_date','t_creation_date.f_id = t_wares.f_code',array("f_title","f_visibility"))
                    ->where('t_wares.f_code IN (' . implode(',', $value) . ')');
                $res2 = $select->query()->fetchAll();
                for($j = 0; $j < count($res2); $j++){
                    if($ware_start != $res2[$j]['f_code']){
                        $wizardData[] = array(
                            'f_code'=>'w_'. $res2[$j]['f_code'],
                            'f_parent_code'=>($key == 'root' || $key == $ware_start || $key == '_pdr_') ? $pdr : 'w_' . $key,
                            'f_title'=>  Mainsim_Model_Utilities::chg($res2[$j]['f_title']),
                            'f_visibility'=>$res2[$j]['f_visibility'],
                            'f_subtitle' => $res2[$j]['f_subtitle_wiz'] ? Mainsim_Model_Utilities::chg($res2[$j]['f_subtitle_wiz']) : '',
                            'f_imgtile' => $res2[$j]['f_imgtile_wiz'] ? $res2[$j]['f_imgtile_wiz'] : '',
                            'f_linktitle' => $res2[$j]['f_linktitle_wiz'] ? $res2[$j]['f_linktitle_wiz'] : '',
                            'f_order' => $res2[$j]['f_order_wiz'] ? $res2[$j]['f_order_wiz'] : 0,
                            'f_field' => $res2[$j]['f_field_wiz'] ? $res2[$j]['f_field_wiz'] : '',
                            'f_color' => $res2[$j]['f_color_wiz'] ? $res2[$j]['f_color_wiz'] : '',
                            'f_tilewidth' => $res2[$j]['f_tilewidth_wiz'] ? $res2[$j]['f_tilewidth_wiz'] : 0,
                            'f_srcbox' => $res2[$j]['f_srcbox_wiz'] ? $res2[$j]['f_srcbox_wiz'] : ''
                        );
                    }
                }
            }
        }
        else{
            $wizardData = array();
            for($i = 0; $i < count($res); $i++){
                $ware_start = $res[$i]['f_code'];
                //foreach($parentRelations as $key => $value){
                $value = $parentRelations[$ware_start];
                if($key == 0){
                        $key = 'root';
                }
                $select->reset();
                $select->from('t_wares',array("f_code","fc_asset_wizard_end", "f_subtitle_wiz", "f_imgtile_wiz", "f_linktitle_wiz", "f_order_wiz", "f_field_wiz", "f_color_wiz", "f_tilewidth_wiz", "f_srcbox_wiz"))
                    ->join('t_creation_date','t_creation_date.f_id = t_wares.f_code',array("f_title","f_visibility"))
                    ->where('t_wares.f_code IN (' . implode(',', $value) . ')');
                $res2 = $select->query()->fetchAll();
                for($j = 0; $j < count($res2); $j++){
                    if($ware_start != $res2[$j]['f_code']){
                        $wizardData[] = array(
                            'f_code'=>'w_'. $res2[$j]['f_code'],
                            'f_parent_code'=>($key == 'root' || $key == $ware_start || $key == '_pdr_') ? $pdr : 'w_' . $key,
                            'f_title'=>  Mainsim_Model_Utilities::chg($res2[$j]['f_title']),
                            'f_visibility'=>$res2[$j]['f_visibility'],
                            'f_subtitle' => $res2[$j]['f_subtitle_wiz'] ? Mainsim_Model_Utilities::chg($res2[$j]['f_subtitle_wiz']) : '',
                            'f_imgtile' => $res2[$j]['f_imgtile_wiz'] ? $res2[$j]['f_imgtile_wiz'] : '',
                            'f_linktitle' => $res2[$j]['f_linktitle_wiz'] ? $res2[$j]['f_linktitle_wiz'] : '',
                            'f_order' => $res2[$j]['f_order_wiz'] ? $res2[$j]['f_order_wiz'] : 0,
                            'f_field' => $res2[$j]['f_field_wiz'] ? $res2[$j]['f_field_wiz'] : '',
                            'f_color' => $res2[$j]['f_color_wiz'] ? $res2[$j]['f_color_wiz'] : '',
                            'f_tilewidth' => $res2[$j]['f_tilewidth_wiz'] ? $res2[$j]['f_tilewidth_wiz'] : 0,
                            'f_srcbox' => $res2[$j]['f_srcbox_wiz'] ? $res2[$j]['f_srcbox_wiz'] : ''
                        );
                    }
                }
            }
        }*/
        
		
        
        return $wizardData;
    }
    
    private function getFirstVisibleParent($ware, $wareList){
        
        $found = false;
        $select = new Zend_Db_Select($this->db);
        while(true){
            $select->reset();
            $select->from('t_wares_parent')
                   ->where('f_code = ' . $ware);
            //print("<br>$select</br>");
            $res = $select->query()->fetch();
            if($res['f_parent_code'] == 0){
                return 'root';
            }
            else if(in_array($res['f_parent_code'], $wareList)){
                return $res['f_parent_code'];
            }
            else{
                $ware = $res['f_parent_code'];
            }
        }
    }
    
    public function getAssetDocs($f_code){

        $select = new Zend_Db_Select($this->db);
        $select->from('t_wares_relations', [])
                ->join('t_creation_date', 't_wares_relations.f_code_ware_slave = t_creation_date.f_id', ['f_id', 'f_title', 'f_creation_date', 'fc_progress'])
                ->join('t_attachments', 't_attachments.f_code = t_creation_date.f_id', ['f_file_ext', 'f_session_id'])
                ->join(['tcd2' => 't_creation_date'], 'tcd2.f_id = t_wares_relations.f_code_ware_master', ['tcd2.f_title AS ware'])
                ->join('t_wares', 't_creation_date.f_id = t_wares.f_code', ['fc_doc_type'])
                ->where('t_wares_relations.f_code_ware_master = ' . str_replace("w_", "", $f_code))
                ->where('t_wares_relations.f_type_id_slave = 5')
                ->where('t_attachments.f_active != -1')
                ->order('f_id DESC');

        $res = $select->query()->fetchAll();
        $userFilter = $this->getUserFilters('docs');

        // apply user filter if it exists
        for($i = 0; $i < count($res); $i++){
            if($userFilter != null && !in_array($res[$i]['fc_doc_type'], $userFilter)){
                $res[$i]['visible'] = false;
            }
            else{
                $res[$i]['visible'] = true;
            }
        }

        return $res;
    }

    public function getAssetWorkorders($f_code){

        $select = new Zend_Db_Select($this->db);
        $select->from('t_ware_wo', [])
            ->join('t_creation_date', 't_ware_wo.f_wo_id = t_creation_date.f_id', ['f_id', 'f_title', 'fc_progress', 'f_creation_date', 'fc_creation_user_name', 'f_description'])
            ->join('t_workorders', 't_workorders.f_code = t_creation_date.f_id', ['f_type_id'])
            ->join('t_workorders_types', 't_workorders.f_type_id = t_workorders_types.f_id', ['f_summary_name'])
            ->join('t_custom_fields', 't_workorders.f_code = t_custom_fields.f_code', [])
            ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', ['f_name', 'f_percentage'])
            ->join('t_wares', 't_workorders.f_priority = t_wares.fc_action_priority_value', ['fc_action_priority_color'])
            ->join(['tcd2' => 't_creation_date'], 'tcd2.f_id = t_ware_wo.f_ware_id', ['tcd2.f_title AS ware'])
            ->where('t_ware_wo.f_ware_id = ' . str_replace("w_", "", $f_code))
            ->where('t_workorders.f_type_id IN (' . implode(',', [1,2,4,5,6,7,8,10,12,13]) . ')')
            ->where('t_wf_phases.f_group_id NOT IN (6,7,8)')
            ->order('f_id DESC');


        $res = $select->query()->fetchAll();

        $userFilter = $this->getUserFilters('workorders');

        // translate phase name and eventually apply filter
        $trsl = new Mainsim_Model_Translate(null, $this->db);
        for($i = 0; $i < count($res); $i++){
            $res[$i]['f_name'] = $trsl->_($res[$i]['f_name']);
            if($userFilter != null && !in_array($res[$i]['f_type_id'], $userFilter)){
                $res[$i]['visible'] = false;
            }
            else{
                $res[$i]['visible'] = true;
            }
        }
        return $res;
    }

    public function getUserFilters($action){

        // get user level
        $userData = Zend_Auth::getInstance()->getIdentity();
        
        // get asset actions from setting if it exists
        $select = new Zend_Db_Select($this->db);
        $select->from("t_creation_date", ['f_description'])
            ->where("f_title = 'WIZARD_TILE_ACTIONS'");
        $res = json_decode($select->query()->fetch()['f_description']);
        $levelFilters = $res->{$action};

        // search filter for user level
        for($i = 0; $i < count($levelFilters); $i++){
            if($levelFilters[$i]->level == $userData->f_level){
                switch($action){
                    case 'workorders':
                        $filter = $levelFilters[$i]->f_type_id;
                        break;
                    case 'docs':
                        $filter = $levelFilters[$i]->fc_doc_type;
                        break;
                }                                               
            }
        }

        return $filter; 
    }
    
}