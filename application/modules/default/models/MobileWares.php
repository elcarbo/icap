<?php
class Mainsim_Model_MobileWares extends Mainsim_Model_MobileBase{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function checkDocument($params){
        if(file_exists(APPLICATION_PATH . '/' . $params['path'])){
            return "ok";
        }
        else{
            return "";
        }
    }
    
    public function searchWares($params){
        $params1 = $params;
        $params1['columns'] = array(
            'wa.f_code',
            'cd.f_title',
            'cd.f_description'
        );
        $wares = $this->getWares($params1);
        for($i = 0; $i < count($wares['t_wares']); $i++){
            
            $result['asset_list'][] = array(
                $wares['t_wares'][$i]['f_code'],
                $wares['t_wares'][$i]['f_title'],
                $wares['t_wares'][$i]['f_description'],
                count(Mainsim_Model_MobileUtilities::getChildren($wares['t_wares'][$i]['f_code'], 'wares'))
            );
        }
        
        // get wares total count
        $params2 = $params;
        unset($params2['limit']);
        unset($params2['offset']);
        $params2['count'] = true;
        $result['total_count'] = $this->getWares($params2);
        return $result;
    }
    
    public function getChildrenWares($params){
        // get children codes
        $aux = Mainsim_Model_MobileUtilities::getChildren($params['search'], 'wares');
        unset($params['search']);
        // get children data by code
        $params1 = $params;
        $params1['columns'] = array(
            'wa.f_code',
            'cd.f_title',
            'cd.f_description'
        );
        $params1['in'] = $aux;
        $wares = $this->getWares($params1);
        for($i = 0; $i < count($wares['t_wares']); $i++){
            $result['asset_list'][] = array(
                $wares['t_wares'][$i]['f_code'],
                $wares['t_wares'][$i]['f_title'],
                $wares['t_wares'][$i]['f_description'],
                count(Mainsim_Model_MobileUtilities::getChildren($wares['t_wares'][$i]['f_code'], 'wares'))
            );
        }
        
        // get wares total count
        $params2 = $params;
        unset($params2['limit']);
        unset($params2['offset']);
        $params2['count'] = true;
        $params2['in'] = $aux;
        $result['total_count'] = $this->getWares($params2);
        return $result;
    }
    
    public function getWares($params){
        // **********************************
        // table wares
        // **********************************
        
        // get user selectors
        $userData = Zend_Auth::getInstance()->getIdentity();
        //$session = new Zend_Session_Namespace('session');
        //$userData = $session->data;
        //$selectors = Mainsim_Model_MobileUtilities::getSelectors($userData['f_code'], 'wares');
        // group selector by type
        $selectorsList = array();
        for($i = 0; $i < count($selectors); $i++){
            if(!$selectorsList[$selectors[$i]['f_type_id']]){
                $selectorsList[$selectors[$i]['f_type_id']] = $selectors[$i]['f_selector_id'];
                $selectorsTypeList[] = $selectors[$i]['f_type_id'];
            }
            else{
                $selectorsList[$selectors[$i]['f_type_id']] .= ',' . $selectors[$i]['f_selector_id'];
            }
        }
        //wa query
        $select = new Zend_Db_Select($this->db);
        //$select->distinct();
        // count query
        if($params['count']){
            $select->from(array("wa" => "t_wares"), new Zend_Db_Expr('COUNT(*) as tot'));
        }
        // data query
        else{
            if($params['columns']){
                $select->from(array("wa" => "t_wares"), array());
            }
            else{
                $fields = Mainsim_Model_MobileUtilities::getMobileFields('t_wares', unserialize(MOBILE_MODULES));
                $select->from(array("wa" => "t_wares"), $fields['list']);
            }
            
        }
        
        $select->join(array("cd" => "t_creation_date"), "wa.f_code = cd.f_id", array())
                ->join(array("wp" => "t_wf_phases"), "cd.f_phase_id = wp.f_number AND cd.f_wf_id = wp.f_wf_id", array())
                ->join(array("wg" => "t_wf_groups"), "wp.f_group_id = wg.f_id", array())      
                ->joinLeft(array("sw" => "t_selector_ware"), 'wa.f_code = sw.f_ware_id', array())
                ->joinLeft(array("st" => "t_selectors_types"), 'sw.f_selector_id = st.f_id', array())
                ->joinLeft(array("l" => "t_locked"), 'wa.f_code = l.f_code', array());
        
        // filter by visibility
        $select->where("cd.f_visibility & " . $userData['fc_usr_level'] . " != 0")
               ->where("cd.f_visibility & " . $userData['fc_usr_group_level'] . " != 0")
               ->where("wp.f_visibility & " . $userData['fc_usr_level'] . " != 0")
               ->where("wg.f_visibility & " . $userData['fc_usr_level'] . "!= 0"); 
        
        // filter by selectors
        foreach($selectorsList as $key => $value){
            $where[] =  'sw.f_selector_id IN (' . $value. ')';
        }

        if($where){
            $select->where('(' . implode(' AND ', $where) . ' OR sw.f_selector_id IS NULL)');
            //$select->where('(' . implode(' AND ', $where) . ')');
        }
        /*
        else{
            $select->where('sw.f_selector_id IS NULL');
        }*/
        
        // where clause for workorders not locked/locked
        if(isset($params['locked'])){
            if($params['locked'] == true){
                $select->where('l.f_code IS NOT NULL AND l.f_user_id =' . $userData['f_code']);
            }
            else{
                $select->where('l.f_code IS NULL');
            }
        }
        
        if(isset($params['in'])){
            $select->where('wa.f_code IN (' . implode(',', $params['in']) . ')');
        } 
        
        if(isset($params['not in'])){
            $select->where('wa.f_code NOT IN (' . implode(',', $params['not in']) . ')');
        }
        
        
        // filter by wares type
        if($params['type_id']){
            $select->where("wa.f_type_id IN (" . $params['type_id'] . ")");
        }
        
        // filter by group phases
        if($params['group_phases']){
            $select->where("wg.f_id IN (" . $params['group_phases'] . ")");
        }
        
        // filter by text search 
        if($params['filter'] && $params['search']){
            for($i = 0; $i < count($params['filter']); $i++){
                $whereSearch[] = ($params['filter'][$i] == 'f_code' ? 'wa.f_code' : $params['filter'][$i]) . " like '%" . $params['search'] . "%'" ;
            }
            $select->where(implode(" OR ", $whereSearch));
        }
        
        // filter columns to get
        if($params['columns']){
            $select->columns($params['columns']);
        }
        
        
        // limit result set 
        if($params['limit'] && $params['offset']){
            $select->limit($params['offset'], $params['limit']);
        }
        else if($params['offset']){
            $select->limit($params['offset']);
        }
        
        $result['t_wares'] = $this->dbSelect($select, $params['fetch'], false);
        
        if($params['count']){
            return isset($result['t_wares'][0]['tot']) ? $result['t_wares'][0]['tot'] : $result['t_wares'][0][0];
            die();
        }
        
        if(count($result['t_wares']) > 0){
            // loop wares to get code list
            // associative array
            if(Mainsim_Model_MobileUtilities::isAssociativeArray($result['t_wares'][0])){
                foreach($result['t_wares'] as $value){
                    $codeList[] = $value['f_code'];
                }
            }
            // numeric array
            else{
                for($i = 0; $i < count($result['t_wares']); $i++){
                    $codeList[] = $result['t_wares'][$i][1];
                }
            }
            if($params['onlyCodes']){
                return count($codeList) > 0 ? $codeList : array();
            }
        }
        
        // **********************************
        // additional wares data table
        // **********************************
        
        // get parents wares
        if(count($codeList) > 0){
            $parentList = Mainsim_Model_MobileUtilities::getParents($codeList, 'wares', true);
            $additionalDataConfig = array(
                array(
                    'table' => 't_custom_fields',
                    'field' => 'f_code',
                    'value' => $codeList
                ),
                array(
                    'table' => 't_creation_date',
                    'field' => 'f_id',
                    'value' => $codeList
                ),
                array(
                    'table' => 't_wares_parent',
                    'field' => 'f_code',
                    'join' => array(
                        'table' => 't_creation_date',
                        'on' => 't_wares_parent.f_code = t_creation_date.f_id'
                    ),
                    'filter' => 'f_active = 1',
                    'value' => array_merge($codeList, $parentList)
                )
            );
        }
        else{
            $parentList = array();
            $codeList = array();
            $additionalDataConfig = array();
        }
        
        
        foreach($additionalDataConfig as $config){
            $result[$config['table']] = Mainsim_Model_MobileUtilities::getAdditionalData($config, $params['fetch']);
        }
        // **********************************
        // get workorders linked to wares
        // **********************************
        if($params['workorders'] == true){
            if(is_array($codeList)){
                $config = array(
                    'table' => 't_ware_wo',
                    'field' => 'f_ware_id',
                    'value' => $codeList
                );
                $result['t_ware_wo'] = Mainsim_Model_MobileUtilities::getAdditionalData($config, $params['fetch']);
                $workorderList = array();
                for($k = 0; $k < count($result['t_ware_wo']); $k++){
                    if(!in_array($result['t_ware_wo'][$k][1], $workorderList)){
                        $workorderList[] = $result['t_ware_wo'][$k][1];
                    }
                }

                $objWo = new Mainsim_Model_MobileWorkorders();
                    $woParams = array(
                        'in' => $workorderList
                    );
                $woResult = $objWare->getWares($woParams);
                $result['t_custom_fields'] = array_merge($result['t_custom_fields'], $woResult['t_custom_fields']);
                $result['t_creation_date'] = array_merge($result['t_creation_date'], $woResult['t_creation_date']);
                $result['t_workorders'] = $woResult['t_workorders'];
                $result['t_workorders_parent'] = $woResult['t_workorders_parent'];
            }
            else{
                $result['t_workorders'] = array();
                $result['t_workorders_parent'] = array();
                $result['t_ware_wo'] = array();
            }
        }
        
        return $result;
    }
    
    public function getCodeByNFC($nfc){
        $selectAsset = new Zend_Db_Select($this->db);
        $selectAsset->from('t_creation_date', array('f_id', 'f_title', 'f_description'))
                    ->where('f_nfc_code =' . $this->db->quote($nfc));
        $resAsset = $this->dbSelect($selectAsset);
        if($resAsset[0]['f_id']){
            return $resAsset;
        }
        else{
            return null;
        }
    }
    
    public function newWare($params){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        
        try{
            // if type is 'document' -> create a stub dataset to insert into db
            /*
            if($params['document']){
                $params['data'] = Mainsim_Model_MobileUtilities::createStubData('ware', 'document', $data);
            }*/
            
            // insert into t_creation_date table
            $insertData = array_combine($params['data']['t_creation_date'][0], $params['data']['t_creation_date'][1]);
            if($params['document']){
                $aux = explode(".", $insertData['f_title']);
                $userData = Zend_Auth::getInstance()->getIdentity();
                $documentTitle = $insertData['f_title']; // str_replace("picture_", "", $aux[0]) . "_" . $userData['f_code'];
                $aux = explode(".", $documentTitle);
                $ext = $aux[1];
                $documentTimestamp = str_replace("picture_", "", $aux[0]); //$insertData['f_timestamp'];
                $documentTimestamp = str_replace("signature_", "", $documentTimestamp); //$insertData['f_timestamp'];
            }
            
            $fakeId = $insertData['f_id'];
            $insertData['f_id'] = null;
            
            $wareId = $this->dbInsert('t_creation_date', $insertData);
            // update wares/workorder counter (fc_asset_wo in t_wares)
            $newWares = array();
            for($i = 1; $i < count($params['data']['t_ware_wo']); $i++){
                $newWares[] = $params['data']['t_ware_wo'][$i][1];
            }
            //$this->waresWorkorderIconAssociation($newWares, $workorderId);
            // loop tables
            unset($params['data']['t_creation_date']);
            $tables['t_creation_date_history'] = $params['data']['t_creation_date_history'];
            $tables['t_custom_fields'] = $params['data']['t_custom_fields'];
            $tables['t_custom_fields_history'] = $params['data']['t_custom_fields_history'];
            $tables['t_wares'] = $params['data']['t_wares'];
            $tables['t_wares_history'] = $params['data']['t_wares_history'];
            $tables['t_ware_wo'] = $params['data']['t_ware_wo'];
            $tables['t_wares_parent'] = $params['data']['t_wares_parent'];
            
            foreach($tables as $table => $rows){
                if(count($rows) > 1){
                    for($i = 1; $i < count($rows); $i++){
                        $insertData = array_combine($rows[0], $rows[$i]);
                        if($table == 't_creation_date_history'){
                            unset($insertData['f_id']);
                            unset($insertData['f_wo_id']);
                            // replace fake code with real db code
                            $insertData['f_code'] = $wareId;
                        }
                        else if($table == 't_ware_wo'){
                            unset($insertData['f_id']);
                            unset($insertData['f_code']);
                            // replace fake code with real db code
                            $insertData['f_wo_id'] = $wareId;
                        }
                        else{
                            unset($insertData['f_id']);
                            unset($insertData['f_wo_id']);
                            // replace fake code with real db code
                            $insertData['f_code'] = $wareId;
                        }
                        $this->dbInsert($table, $insertData);
                    }
                }
            }
            
            // document ware --> save file and create entry in t_attachments
            if($params['document']){
                $this->saveAttachment($params['document'], $wareId, $documentTimestamp, $documentTitle, $ext);
            }

            
            
            // reverse ajax
            Mainsim_Model_Utilities::saveReverseAjax("t_wares_parent", $wareId, array(0), "add", 'mdl_asset_tg');

            return array(
                'f_code_old' => $fakeId,
                'f_code_new' => $wareId,
            );
        }
        catch(Exception $e){
            print("\n" . $e->getMessage() . "\n");
        }
    }
    
    /*
     * save attachment into filesystem and create a entry in t_attachments table
     */
    private function saveAttachment($base64, $code, $timestamp, $title = 'document', $type = 'jpg'){
        $attachmentFolder = Zend_Registry::get('attachmentsFolder');
         $objMobile = new Mainsim_Model_MobileBase();

        // create destination dir if not exists
        if(!file_exists($attachmentFolder . '/doc/' . $code)){
            mkdir($attachmentFolder . '/doc/' . $code);
        }
        if($type == 'jpg'){
        
            try{
                $imageString = base64_decode($base64);
                $imageResource = imagecreatefromstring($imageString);
                imagesavealpha($imageResource, true);
                // save image
                $userData = Zend_Auth::getInstance()->getIdentity();
                $aux = explode('.', $title);
                
                $filename = $title;
                imagejpeg($imageResource, $attachmentFolder . '/doc/' . $code . '/' . $timestamp . '_' . $userData['f_code'] . "_doc.jpg", 100);
                $mime = 'image/jpeg';
            }
            catch(Exception $e){
                print($e->getMessage());
            }
        }
        else if($type == 'png'){
            try{
                $imageString = base64_decode($base64);
                $imageResource = imagecreatefromstring($imageString);
                imagesavealpha($imageResource, true);
                // save image
                $userData = Zend_Auth::getInstance()->getIdentity();
                $aux = explode('.', $title);
                
                $filename = $title;
                imagepng($imageResource, $attachmentFolder . '/doc/' . $code . '/' . $timestamp . '_' . $userData['f_code'] . "_doc.png");
                $mime = 'image/png';
            }
            catch(Exception $e){
                print($e->getMessage());
            }
            
        }
        
        // insert into t_attachments
        
        $data = array(
            'f_code' => $code,
            'f_session_id' => $timestamp . '_' . $userData['f_code'],
            'f_timestamp' => $timestamp,
            'f_active' => 1,
            'f_fieldname' => 'fieldname',
            'f_file_name' => $filename,
            'f_file_ext' => $type,
            'f_type' => 'doc',
            'f_mime' => $mime,
            'f_path' => $attachmentFolder . '/doc/' . $code . '/' . $timestamp . '_' . $userData['f_code'] . "_doc." . $type
        );
        $objMobile->dbInsert('t_attachments', $data);
        
        // update t_wares
        $data2 = array(
            'fc_doc_attach' => $timestamp . '_' . $userData['f_code'] . '|' . $filename
        );
        $where2 = array(
            'f_code' => $code
        );
        $objMobile->dbUpdate('t_wares', $data2, $where2);
       
    }
    
    
    public function getAttachements($params){
    
      $fc=$params["f_code"];
      
      // find crossed file & link
      $select = new Zend_Db_Select($this->db);
      
      $select->from('t_wares_relations', array('f_code_ware_slave'))
             ->join('t_creation_date', 't_creation_date.f_id = t_wares_relations.f_code_ware_slave', array('f_title'))
             ->join('t_wares', 't_wares.f_code = t_wares_relations.f_code_ware_slave', array('fc_doc_attach'))
             ->where('t_wares_relations.f_code_ware_master = ?',$fc )
             ->where('t_creation_date.f_category = ?','FILES AND LINKS' );
      

      $res = $select->query()->fetchAll();
 
      
      $resp=array( "attach"=>$res  );
    
    
      return $resp;
    
    }
    
    
}
