<?php

class Mainsim_Model_Update
{
    private $db;
    
    public function __construct() 
    {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function uiUpdateVersion($version)
    {
        try {
            $this->db->update("t_system_information", array("f_version_num" => $version), "f_id = 1");                
        } catch(Exception $ex) {
            return array("status" => "error", "message" => "Error in updating mainsim version<br/>".$ex->getMessage());
        }
        return array("status" => "ok", "message" => "Version updated successfully!");
    }
        
    public function uiConvertProperties($table)
    {
        $result = array(); $fields = array('f_properties');
        if($table) $fields[] = 'f_instance_id'; else $fields[] = "f_code";
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from('t_'.$table.'ui_object_instances', $fields)->where('f_type_id IN (6, 7, 8, 13, 14)')->query()->fetchAll();
        foreach($uis as $ui) {
            $props = json_decode($ui['f_properties'], true);
            unset($props['bkm_type']); unset($props['db_type']); unset($props['t_workflows_1']);
            unset($props['t_selectors_1']); unset($props['t_users_1']);
            foreach($props as $k => $v) {
                if($k == 'mandatory') { $props['m_on'] = $v; unset($props['mandatory']); }
                else if($k == 'visible') { $props['v_on'] = $v; unset($props['visible']); }
                else if($k == 'readonly') { $props['r_on'] = $v; unset($props['readonly']); }
                else if($k == 'disabled') { $props['d_on'] = $v; unset($props['disabled']); }
                else if($k == 'level_edit') { $props['r_level'] = $v; unset($props['level_edit']); }
                else if($k == 'types') { $props['v_type'] = $v; unset($props['types']); }
                else if($k == 't_selectors') { $props['v_selector'] = $v; unset($props['t_selectors']); }
            }
            $c_ui = array( 'f_properties' => json_encode($props) );
            if($table) {
                $c_ui['f_instance_id'] = $ui['f_instance_id'];
                $this->db->update("t_admin_ui_object_instances", $c_ui, "f_instance_id = {$c_ui["f_instance_id"]}");                
            } else {
                $c_ui['f_code'] = $ui['f_code'];
                $this->db->update("t_ui_object_instances", $c_ui, "f_code = {$c_ui["f_code"]}");
            }
            $result[] = $c_ui;
        }
        return $result;
    }
    
    /**
     * Get translations from file and put them into mainsim
     * Query to extract translations
     * select CONCAT('"', cd.f_title, '" => "', s.fc_lang_it_IT, '",') as translation from t_creation_date cd join t_systems s on cd.f_id = s.f_code where s.f_type_id = 7 order by cd.f_id
     * execute in phpmyadmin and extract as php array, then put translations in the it_IT file and run this method
     */
    public function langArrayToSystemSettings()
    {
        $res = array("#total_entries" => count($it_IT), "#ok_entries" => 0);
        
        // Creating entry in t_systems_type
        //$this->db->insert("t_systems_types", array("f_id" => 7, "f_type" => "LANGUAGE", "f_wf_id" => 9, "f_wf_phase" => 1, "f_short_name" => "lang", "f_module_name" => "mdl_lang_tg"));
        
        // Creating columns for languages in t_systems and t_systems_history
//        $this->db->query("ALTER TABLE t_systems ADD fc_lang_it_IT VARCHAR(255) NULL, ADD fc_lang_fr_FR VARCHAR(255) NULL, ADD fc_lang_es_ES VARCHAR(255) NULL");
//        $this->db->query("ALTER TABLE t_systems_history ADD fc_lang_it_IT VARCHAR(255) NULL, ADD fc_lang_fr_FR VARCHAR(255) NULL, ADD fc_lang_es_ES VARCHAR(255) NULL");
        
        include APPLICATION_PATH.'/configs/lang/it_IT.php'; // Italian
        include APPLICATION_PATH.'/configs/lang/fr_FR.php'; // French
        include APPLICATION_PATH.'/configs/lang/es_ES.php'; // Spanish
        
        foreach($it_IT as $k => $v) {
            $r = array();
            // t_creation_date
            try {
                $this->db->insert('t_creation_date', array("f_type" => "SYSTEMS", "f_category" => "LANGUAGE", "f_order" => 1, "f_creation_date" => time(), "f_creation_user" => 1, "f_title" => $k, "f_wf_id" => 9, "f_phase_id" => 1, "f_visibility" => -1, "f_editability" => -1, "f_timestamp" => time()));                
            } catch(Exception $ex) { $res[$k] = array("error" => "Error in creating entry in t_creation_date: ".$ex->getMessage()); break; }
            $f_code = $this->db->lastInsertId();
            
            // t_systems
            try {
                $this->db->insert('t_systems', array("f_code" => $f_code, "f_type_id" => 7, "f_timestamp" => time(), "f_user_id" => 1, "fc_lang_it_IT" => utf8_decode($v), "fc_lang_fr_FR" => (isset($fr_FR[$k])?utf8_decode($fr_FR[$k]):""), "fc_lang_es_ES" => (isset($es_ES[$k])?utf8_decode($es_ES[$k]):"")));
            } catch(Exception $ex) { $res[$k] = array("error" => "Error in creating entry in t_systems: ".$ex->getMessage()); break; }
                        
            // t_systems_parent            
            try {
                $this->db->insert('t_systems_parent', array("f_code" => $f_code, "f_parent_code" => 0, "f_active" => 1, "f_timestamp" => time()));
            } catch(Exception $ex) { $res[$k] = array("error" => "Error in creating entry in t_systems_parent: ".$ex->getMessage()); break; }            
            
            // t_custom_fields            
            try {
                $this->db->insert('t_custom_fields', array("f_code" => $f_code, "f_timestamp" => time()));
            } catch(Exception $ex) { $res[$k] = array("error" => "Error in creating entry in t_custom_fields: ".$ex->getMessage()); break; }
                        
            $res[$k] = array("message" => "Entry for key $k created successfully");
            $res["#ok_entries"]++;
        }
        return $res;
    }
    
    public function convertEditModuleToCb($isadmin = false) {
        $table = 't_'.($isadmin?'admin_':'').'ui_object_instances';
        $edits = array(); $cbs = array();
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from($table)->where("f_instance_name LIKE 'mdl%edit_cb_bl'")->orWhere("f_properties LIKE '%moModuleEdit%'")->query()->fetchAll();
        foreach($uis as $ui) {
            if(strpos($ui["f_instance_name"], '_edit_cb_bl') > 0) $cbs[$ui["f_instance_name"]] = json_decode($ui["f_properties"], true);
            else $edits[$ui["f_instance_name"]] = array(
                "f_code" => $ui["f_code"],
                "f_properties" => json_decode($ui["f_properties"], true)
            );
        }
        foreach($edits as $k => $v) {
            unset($v["f_properties"]["f_instance_name"]); unset($v["f_properties"]["tabbar"]); unset($v["f_properties"]["commandsl"]); unset($v["f_properties"]["phaseFieldId"]);
            unset($v["f_properties"]["table"]); unset($v["f_properties"]["tgType"]); unset($v["f_properties"]["order"]); unset($v["f_properties"]["pairCross"]);
            if(!isset($v["f_properties"]["commands"])) {
                $v["f_properties"]["commands"] = str_replace('_edit_cb', '', $cbs["{$k}_cb_bl"]["elements"]);
                try {
                    $this->db->update($table, array("f_properties" => json_encode($v["f_properties"])), "f_code = {$v["f_code"]}");
                    $msg[] = array("message" => "$k updated successfully!");
                } catch(Exception $ex) {
                    $msg[] = array("error" => "Error in converting $k: ".$ex->getMessage());
                    return $msg;
                }
            }
        }
        try { // Deleting old layouts and modules
            $this->db->query("delete from $table where (f_type_id = 10 AND f_instance_name LIKE '%edit_cb') OR (f_type_id = 9 AND f_instance_name LIKE '%edit_cb_bl')");
            $msg[] = array("message" => "Old layouts and modules deleted successfully");
        } catch(Exception $ex) {
            $msg[] = array("error" => "Error in deleting old layouts and modules: ".$ex->getMessage());
            return $msg;
        }
        try { // Converting buttons
            $this->db->query("update $table set f_instance_name = REPLACE(f_instance_name, '_edit_cb', '') where f_type_id = 5 AND f_instance_name LIKE '%edit_cb%'");
            $msg[] = array("message" => "Buttons converted successfully");
        } catch(Exception $ex) {
            $msg[] = array("error" => "Error in converting buttons: ".$ex->getMessage());
            return $msg;
        }
        return $msg;
    }
    
    public function convertUiTruthValuesToBoolean() {
        $msg = array();
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from("t_ui_object_instances")->where("f_type_id IN (6, 7, 8, 9, 13, 14)")->query()->fetchAll();
        foreach($uis as $ui) {
            $ui["f_properties"] = json_decode($ui["f_properties"], true);
            foreach($ui["f_properties"] as $k => $v) {
                if($k != "ftype" && $k != "stype" && $k != "v_type" && $k != "m_type" && $k != "r_type" && $k != "d_type" &&
                $k != "v_wf" && $k != "r_wf" && $k != "m_wf" && $k != "d_wf" &&
                $k != "v_selector" && $k != "r_selector" && $k != "m_selector" && $k != "d_selector" &&
                $k != "v_level" && $k != "r_level" && $k != "m_level" && $k != "d_level") {
                    if($v === true || $v === "true" || $v === 1 || $v === "1") $ui["f_properties"][$k] = 1;
                    elseif($v === false || $v === "false" || $v === 0 || $v === "0") $ui["f_properties"][$k] = 0;
                }                
            }
            $ui["f_properties"] = json_encode($ui["f_properties"]);
            try {
                $this->db->update("t_ui_object_instances", $ui, "f_code = ".$ui["f_code"]);
                $msg[] = array("message" => "{$ui["f_instance_name"]} converted successfully");
            } catch(Exception $ex) {
                $msg[] = array("error" => "Error in converting values for {$ui["f_instance_name"]}: ".$ex->getMessage());
                return $msg;
            }            
        }
        return $msg;
    }
    
    public function cleanUi() {
        $msg = "";
        $propsToRemove = array("f_instance_name", "f_type_id", "bkm_type", "db_type", "db_table", "db_generate", "checkmode", "error", "parentSelection");
        $propsTgToRemove = array("table", "tgType", "pairCross", "order");
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from("t_ui_object_instances", array("f_code", "f_instance_name", "f_properties"))->where("f_type_id IN (5, 6, 7, 8, 9, 13, 14)")->query()->fetchAll();
        foreach($uis as $ui) {
            $ui["f_properties"] = json_decode($ui["f_properties"], true);
            foreach($ui["f_properties"] as $k => $v) {
                if(array_search($k, $propsToRemove) !== FALSE) unset($ui["f_properties"][$k]);
                if($ui["f_properties"]["type"] != "moModuleTreeGrid" && array_search($k, $propsTgToRemove) !== FALSE) unset($ui["f_properties"][$k]);
            }
            $ui["f_properties"] = json_encode($ui["f_properties"]);
            try {
                $this->db->update("t_ui_object_instances", $ui, "f_code = ".$ui["f_code"]);
                $msg[] = array("message" => $ui["f_instance_name"]." cleaned successfully");
            } catch(Exception $ex) {
                $msg[] = array("error" => "Error in cleaning {$ui["f_instance_name"]}: ".$ex->getMessage());
                return $msg;
            }            
        }
        return $msg;
    }
    
    public function getUiComponentList() {
        $uis = array();
        include APPLICATION_PATH.'/configs/lang/it_IT.php'; // Italian
        $fields = array();
        $q = new Zend_Db_Select($this->db);
        $comps = $q->from("t_ui_object_instances", array("f_instance_name", "f_properties"))->where("f_instance_name LIKE 'mdl_wo_edit_c_'")->query()->fetchAll();		
        foreach($comps as $ck => $cv) {
            $props = json_decode(Mainsim_Model_Utilities::chg($cv["f_properties"]), true);
            //$fields = array_merge($fields, explode(",", $props["fields"]));
			$fields = explode(",", str_replace('|', ',', $props["fields"]));
            $q->reset();
            $c_uis = $q->from("t_ui_object_instances", array("f_instance_name", "f_properties"))
                ->where("f_instance_name IN ('".implode("','", $fields)."')")->query()->fetchAll();
            foreach($c_uis as $k => $v) {
                $props = json_decode(Mainsim_Model_Utilities::chg($v["f_properties"]), true);
                $c_uis[$k]["bind"] = $props["bind"];
                $c_uis[$k]["label"] = $props["label"];
                $c_uis[$k]["label_it"] = utf8_decode($it_IT[$props["label"]]);
                $c_uis[$k]["info"] = $props["info"];
                $c_uis[$k]["info_it"] = utf8_decode($it_IT[$props["info"]]);
                unset($c_uis[$k]["f_properties"]);
            }
            $uis[$cv["f_instance_name"]] = $c_uis;
        }        
        return $uis;
    }
    
    public function setUiProperties($instance_name, $addprops) {
        $q = new Zend_Db_Select($this->db);
        $res = $q->from("t_ui_object_instances", array("f_properties"))->where("f_instance_name LIKE '$instance_name'")->query()->fetch();
        $props = json_decode(Mainsim_Model_Utilities::chg($res["f_properties"]), true);
        foreach($addprops as $p => $v) { $props[$p] = $v; }
        try {
            $this->db->update("t_ui_object_instances", array("f_properties" => json_encode($props)), "f_instance_name = '$instance_name'");
            return array("error" => false, "message" => "$instance_name updated successfully!");
        } catch(Exception $ex) {
            return array("error" => true, "message" => "Problems updating $instance_name: ".$ex->getMessage());
        }
    }
    
    public function mainsimToSql($table = '', $ftype = '') {
        if($table && $ftype) {
            $types = array('t_workorders' => 'WORKORDER', 't_wares' => 'WARES', 't_selectors' => 'SELECTOR', 't_systems' => 'SYSTEMS');
            $type = $categories[$table];
            $q = new Zend_Db_Select($this->db);
            // Get codes
            $res = $q->from(array("c" => "t_creation_date"), array("f_id"))->join(array("t" => $table."_types"), "c.f_category = t.f_type", array())
            ->where("t.f_id = ?", $ftype)->query()->fetchAll(); $q->reset();
            foreach($res as $r) $codes[] = $r["f_id"];
            $codes_list = implode(",", $codes);
            $t_creation_date = $q->from("t_creation_date")->where("f_id IN ($codes_list)")->order("f_id ASC")->query()->fetchAll(); $q->reset();            
            $t_table = $q->from($table)->where("f_code IN ($codes_list)")->order("f_code ASC")->query()->fetchAll(); $q->reset();
            $t_custom_fields = $q->from("t_custom_fields")->where("f_code IN ($codes_list)")->order("f_code ASC")->query()->fetchAll(); $q->reset();
            $t_table_parent = $q->from($table."_parent")->where("f_code IN ($codes_list)")->order("f_code ASC")->query()->fetchAll(); $q->reset();
            if($table == 't_systems' && $ftype == 3) { // Bookmarks
                $t_pair_cross = $q->from("t_bookmarks")->where("f_code IN ($codes_list)")->order("f_code ASC")->query()->fetchAll(); $q->reset();
                $t_pair_cross_fields = Mainsim_Model_Utilities::get_tab_cols("t_bookmarks");
            } else {
                $t_pair_cross = $q->from("t_pair_cross")->where("f_code_cross < 0")->where("f_code_main IN ($codes_list)")->order("f_code_main ASC")->query()->fetchAll(); $q->reset();
                $t_pair_cross_fields = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
            }
            // Creating file
            return array(
                "t_creation_date" => array("count" => count($t_creation_date), "fields" => Mainsim_Model_Utilities::get_tab_cols("t_creation_date"), "data" => $t_creation_date ),
                $table => array( "count" => count($t_table), "fields" => Mainsim_Model_Utilities::get_tab_cols($table), "data" => $t_table ),
                "t_custom_fields" => array( "count" => count($t_custom_fields), "fields" => Mainsim_Model_Utilities::get_tab_cols("t_custom_fields"), "data" => $t_custom_fields ),
                $table."_parent" => array( "count" => count($t_table_parent), "fields" => Mainsim_Model_Utilities::get_tab_cols($table."_parent"), "data" => $t_table_parent ),
                "t_pair_cross" => array( "count" => count($t_pair_cross), "fields" => $t_pair_cross_fields, "data" => $t_pair_cross )
            );
            
        } else {
            $wo = new Zend_Db_Select($this->db); $wa = new Zend_Db_Select($this->db); $sel = new Zend_Db_Select($this->db); $ss = new Zend_Db_Select($this->db);
            return array(
                "t_workorders" => array(
                    "label" => "Workorders",
                    "categories" => $wo->from("t_workorders_types", array("value" => "f_id", "label" => "f_type"))->query()->fetchAll()
                ),
                "t_wares" => array(
                    "label" => "Wares",
                    "categories" => $wa->from("t_wares_types", array("value" => "f_id", "label" => "f_type"))->query()->fetchAll()
                ),
                "t_selectors" => array(
                    "label" => "Selectors",
                    "categories" => $sel->from("t_selectors_types", array("value" => "f_id", "label" => "f_type"))->query()->fetchAll()
                ),
                "t_systems" => array(
                    "label" => "System settings",
                    "categories" => $ss->from("t_systems_types", array("value" => "f_id", "label" => "f_type"))->query()->fetchAll()
                )
            );
        }                   
    }
    
    public function updateOpenOverlayButtons() {
        $q = new Zend_Db_Select($this->db);
        $res = $q->from("t_ui_object_instances", array("f_code", "f_instance_name", "f_properties"))->where("f_properties LIKE '%moLibrary.openOverlay%'")
            ->query()->fetchAll();
        foreach($res as $r) {
            $mod = str_replace(array("btn_", "_add"), array("", ""), $r["f_instance_name"]);
            $props = json_decode(Mainsim_Model_Utilities::chg($r["f_properties"]), true);
            $props["action"] = "mdl_{$mod}_tg.addElements()";
            try {
                $this->db->update("t_ui_object_instances", array("f_properties" => json_encode($props)), "f_instance_name = '{$r["f_instance_name"]}'");
                echo "<div style='color: green;'>{$r["f_instance_name"]} updated successfully!</div>\n";
            } catch(Exception $ex) {
                echo "<div style='color: red;'>Problems updating {$r["f_instance_name"]}: ".$ex->getMessage()."</div>";
            }
        }
    }
    
    public function setDefaultValuesToUi($addDefault = array(), $overwrite = false) {
        $q = new Zend_Db_Select($this->db);
        $res = $q->from("t_ui_object_instances")->where("f_type_id IN (".implode(", ", array_keys($addDefault)).")")->query()->fetchAll();        
        foreach($res as $r) {
            $props = json_decode(Mainsim_Model_Utilities::chg($r["f_properties"]), true);
            if($r["f_type_id"] == 9) {
                if(is_array($addDefault[$r["f_type_id"]][$props["type"]])) {
                    foreach($addDefault[$r["f_type_id"]][$props["type"]] as $p => $v) {
                        if($overwrite || !isset($props[$p])) $props[$p] = $v;
                        //$props[$p] = $v;
                    }
                }
            } else if($r["f_type_id"] == 10) {
                for($i=0; $i<count($props["layout"]); $i++) {
                    foreach($addDefault[$r["f_type_id"]] as $p => $v) {
                        if($overwrite || !isset($props["layout"][$i][$p])) $props["layout"][$i][$p] = $v;
                        if(intval($props["layout"][$i]['wh']) == 36) $props["layout"][$i]['wh'] = 38;
                    }
                }
            } else foreach($addDefault[$r["f_type_id"]] as $p => $v) {
                if($overwrite || !isset($props[$p])) $props[$p] = $v;
            }
            unset($props["f_instance_name"]); unset($props["f_type_id"]);
            try {
                $this->db->update("t_ui_object_instances", array("f_properties" => json_encode($props)), "f_code = {$r["f_code"]}");
                echo "<div style='color: green;'>{$r["f_instance_name"]} updated successfully!</div>\n";
            } catch(Exception $ex) {
                echo "<div style='color: red;'>Problems updating {$r["f_instance_name"]}: ".$ex->getMessage()."</div>";
            }
        }        
    }
    
    public function createDbSchema($table = '') {
        $tables = array();
        if(!$table) {
            $config = $this->db->getConfig();
            $res = $this->db->query("SHOW TABLES FROM {$config["dbname"]}")->fetchAll();
            foreach($res as $r) $tables[] = ($r["Tables_in_{$config["dbname"]}"]);            
        } else $tables[] = $table;
        $data = array();
        foreach($tables as $tbl) {
            $data[$tbl] = array();
            $fields = $this->db->describeTable($tbl);
            foreach($fields as $k => $fld) {
                $key = false;
                if($tbl == 't_creation_date' && $k == 'f_id') $key = true;
                $data[$tbl][$fld["COLUMN_NAME"]] = array( "key" => $key, "type" => $fld["DATA_TYPE"] );
            }
        }
        return $data;
    }
    
    public function convertCategoryToOptions() {
        $result = array();
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from("t_ui_object_instances", array("f_instance_name", "f_properties"))->where("f_type_id = 7")->where("f_instance_name not like '%phases'")->query()->fetchAll();        
        foreach($uis as $k => $ui) {
            $q->reset();
            $props = json_decode($ui['f_properties'], true);
            if(isset($props["category"])) {
                if(!isset($props["options"])) {
                    $options = $q->from("t_ui_object_instances", array("f_properties"))->where("f_instance_name = ?", $props["category"])->query()->fetchAll();
                    $props = json_decode(Mainsim_Model_Utilities::chg($ui["f_properties"]), true);
                    unset($props["category"]);
                    try {
                        $this->db->update("t_ui_object_instances", array("f_properties" => json_encode($props)), "f_instance_name = '{$ui["f_instance_name"]}'");
                        $result[] = array("message" => "{$ui["f_instance_name"]} updated succesfully!");
                    } catch(Exception $ex) {
                        $result[] = array("error" => "Problems in updating {$ui["f_instance_name"]}: ".$ex->getMessage());
                    }
                } else {
                    $result[] = array("warning" => "{$ui["f_instance_name"]} has both category and options properties, please check");
                }
            } else {
                if(!isset($props["options"])) {
                    $result[] = array("error" => "{$ui["f_instance_name"]} hasn't a category nor options");
                } else {
                    $result[] = array("warning" => "{$ui["f_instance_name"]} hasn't a category but it has options");
                }
            }
        }
        return $result;
    }
    
    public function layoutHideCb() {
        $layouts = array(
            "crud" => '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":1,"wh":1219,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":0,"ov":1,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_edit","acc":1,"lock":0,"minv":36},{"idp":1,"ov":0,"tp":1,"wh":36,"level":-1,"module":"mdl_::module::_cb","acc":0,"lock":1,"minv":36},{"idp":1,"ov":0,"tp":1,"wh":399,"level":-1,"module":"mdl_::module::_tg","acc":0,"lock":0,"minv":36}]}',
            "timeline" => '{"layout":[{"idp":-1,"ov":0,"tp":0,"wh":100,"level":-1,"module":"","lock":0,"minv":36,"acc":0},{"idp":0,"ov":1,"tp":1,"wh":1183,"level":-1,"module":"","acc":0,"lock":0,"minv":36},{"idp":0,"ov":1,"module":"mdl_::module::_edit","tp":0,"wh":36,"level":-1,"acc":1,"lock":0,"minv":36},{"idp":0,"ov":1,"tp":0,"wh":36,"level":-1,"module":"mdl_::module::_tl","acc":0,"lock":0,"minv":36},{"idp":1,"ov":0,"tp":1,"wh":36,"level":-1,"module":"mdl_::module::_cb","acc":0,"lock":1,"minv":36},{"idp":1,"ov":0,"tp":0,"wh":393,"level":-1,"module":"mdl_::module::_tg","acc":0,"lock":0,"minv":36}]}'
        );        
        $result = array();
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from("t_ui_object_instances", array("f_instance_name", "f_properties"))->where("f_type_id = 10")->where("f_instance_name like 'lyt%crud'")->query()->fetchAll();
        foreach($uis as $ui) {
            $name = explode('_', $ui["f_instance_name"]);
            if($name[2] == 'crud') { // main crud only, no cross and pair cross
                $mod = $name[1];
                $props = (str_replace("::module::", $mod, $layouts[(stripos($ui["f_properties"], "mdl_{$mod}_tl") > 0 ? 'timeline' : 'crud')]));
                try {
                    $this->db->update("t_ui_object_instances", array("f_properties" => $props), "f_instance_name = '{$ui["f_instance_name"]}'");
                    $result[] = array("message" => "{$ui["f_instance_name"]} updated succesfully!");
                } catch(Exception $ex) {
                    $result[] = array("error" => "Problems in updating {$ui["f_instance_name"]}: ".$ex->getMessage());
                }
            }
        }
        return $result;
    }
	
    public function setCustomProgress() {
        $result = array();
        $q = new Zend_Db_Select($this->db);
        $res = $q->from(array("cd" => "t_creation_date"), array("f_id", "f_category", "fc_progress"))
            ->join(array("cf" => "t_custom_fields"), "cd.f_id = cf.f_code", array())
            ->where("cf.custom_progress IS NULL OR ")->where("cd.f_category IN ('CORRECTIVE', 'WORK REQUEST', 'EMERGENCY', 'PIS')")
            ->query()->fetchAll();
        foreach($res as $r) {
            $progress = $r["fc_progress"];
            $length = strlen($progress);
            for($i=$length; $i<5; $i++) $progress = '0'.$progress;
            $progress = ($r["f_category"] == 'CORRECTIVE' || $r["f_category"] == 'WORK REQUEST' ? 'TK' : 'PIS').$progress;				
            try {
                $this->db->update("t_custom_fields", array("custom_progress" => $progress), "f_code = ".$r["f_id"]);
                $result[] = array("message" => "{$r["f_id"]} {$r["f_category"]} -  {$r["fc_progress"]} => $progress");
            } catch(Exception $ex) {
                $result[] = array("error" => "Problems in updating {$r["f_id"]}: ".$ex->getMessage());
            }
        }
        return $result;
    }
}