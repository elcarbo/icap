<?php

class Mainsim_Model_UI
{
    private $type_id;
    /**
     *
     * @var Zend_Db 
     */
    private $db,$shortname,$uiShort;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        
        $res_shortname = $this->db->query("SELECT f_short_name from t_selectors_types")->fetchAll();
        $tot_a = count($res_shortname);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_shortname[$a]['f_short_name']) && !is_null($res_shortname[$a]['f_short_name']))
                $this->shortname[] = $res_shortname[$a]['f_short_name'];
        }

        $res_shortname = $this->db->query("SELECT f_short_name from t_wares_types")->fetchAll();
        $tot_a = count($res_shortname);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_shortname[$a]['f_short_name']) && !is_null($res_shortname[$a]['f_short_name']))
                $this->shortname[] = $res_shortname[$a]['f_short_name'];
        }

        $res_shortname = $this->db->query("SELECT f_short_name from t_workorders_types")->fetchAll();
        $tot_a = count($res_shortname);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_shortname[$a]['f_short_name']) && !is_null($res_shortname[$a]['f_short_name']))
                $this->shortname[] = $res_shortname[$a]['f_short_name'];
        }
        
        $res_shortname = $this->db->query("SELECT f_short_name from t_systems_types")->fetchAll();
        $tot_a = count($res_shortname);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_shortname[$a]['f_short_name']) && !is_null($res_shortname[$a]['f_short_name']))
                $this->shortname[] = $res_shortname[$a]['f_short_name'];
        }
        $this->uiShort = array('mn','btn','txtfld','slct','chkbx','mdl','lyt','tbbr','img','pck');
    }
    
    public function getLastUIEdited() {
        $res = $this->db->query("SELECT MAX(f_timestamp) as f_timestamp from t_ui_object_instances")->fetch();
        return $res['f_timestamp'];
    }
            
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function getUI($custom_search = array())
    {   
        $select = new Zend_Db_Select($this->db);
        $select->from('t_ui_object_instances')->where('f_type_id = ?', $this->type_id);
        foreach($custom_search as $line) {
            $select->where($line);
        }
        
        $result = $select->query()->fetchAll();
        $tot = count($result);
        for($i = 0;$i < $tot;++$i) {                        
            $result[$i]['f_properties'] = Mainsim_Model_Utilities::chg($result[$i]['f_properties']);            
        }              
        //if($this->type_id == 2 || $this->type_id == 9 || $this->type_id == 10)
        $result = $this->createPopup($result);      
        $tot = count($result);
        for($i = 0;$i < $tot;++$i) {
            $result[$i]['f_properties'] = Mainsim_Model_Utilities::chg($result[$i]['f_properties']);            
        }
        return $result;        
    }
    
    public function setSearch($search) {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_ui_object_types",'f_type_id');
        $select->where("f_type_name = ?", $search);
        $result = $select->query()->fetchAll();
        if(count($result)) {
            $this->type_id = $result[0]['f_type_id'];
            return true;
        }
        return false;        
    }
    
    private function duplicateUi($ui)
    {
        $changeModule = function (&$row,$key,$main) {
            $row = $row.'_'.$main;
        };
        $exp_name = explode('_',$ui['f_instance_name']);        
        if((count($exp_name) == 2 && $ui['f_instance_name'] != 'tbbr_main') || !in_array($exp_name[2],$this->shortname)) {            
            $main = $exp_name[1];
            $searchArray = $this->uiShort;            
            array_walk($searchArray,$changeModule,$main);            
            $replaceArray = $searchArray;
            array_walk($replaceArray,$changeModule,'pp');                                    
            return $this->uiToArray(array(
                "f_instance_name" => str_replace($searchArray,$replaceArray,$ui['f_instance_name']),
                "f_properties" => str_replace($searchArray,$replaceArray,$ui['f_properties'])
            ));
        }
        return false;        
    }
    
    public function uiToArray($ui)
    {
        $result['name'] = $ui['f_instance_name'];                
        $properties = json_decode($ui['f_properties'],true);      
        if(!is_array($properties) && (is_null($properties) || !$properties)) {
            Zend_Debug::dump($ui['f_properties']);die($ui['f_instance_name']);
        }
        $keys = array_keys($properties);
        $tot_a = count($keys);
        for($a = 0;$a < $tot_a;++$a) {
            $result[$keys[$a]] = $properties[$keys[$a]];
        }
        if($this->type_id == 11 && strpos($result['name'],"_pp") !== false) {             
            $tabs = array();
            $old_prop = json_decode($ui['f_properties'],true);            
            foreach($old_prop['tabs'] as $line_pr){                            
                if(strpos($line_pr['layout'],'_edit_') !== false) {                                
                    $tabs[] = $line_pr;
                }
            }
            $result['tabs'] = $tabs;
        }
        return $result;   
    }
    
    public function createPopup($response = array())
    {        
        $tot_a = count($response);                
        $final_result = array();
        for($a = 0;$a < $tot_a;++$a) {
            //$response[$a]["f_instance_name"] = utf8_decode($response[$a]["f_instance_name"]);
            $response[$a]["f_properties"] = $response[$a]["f_properties"];
            $final_result[] = $this->uiToArray($response[$a]);
//            $duplicate_ui = $this->duplicateUi($response[$a]);
//            if(is_array($duplicate_ui)) {
//                $final_result[] = $duplicate_ui;
//            }
        }                
        return $final_result;
    }
    
    /**
     * Recupero il result e creo l'array che serve per il 
     * sistema
     * @param type $result 
     */
    private function customizeArray($result = array(),$response = array(),$i = 0,$search = '',$sublist = array()) 
    {           
        $select = new Zend_Db_Select($this->db);
        
        $start_name = '';
        switch($this->type_id) {            
            case 2 : $start_name = 'mn';
                break;            
            case 5 : $start_name = 'btn';
                break;
            case 6 : $start_name = 'txtfld';
                break;
            case 7 : $start_name = 'slct';
                break;
            case 8 : $start_name = 'chkbx';
                break;
            case 9 : $start_name = 'mdl';
                break;
            case 10 : $start_name = 'lyt';
                break;
            case 11 : $start_name = 'tbbr';
                break;            
            case 13 : $start_name = 'img';
                break;
        }
        
        $replace = array('mn','btn','txtfld','slct','chkbx','mdl','lyt','tbbr','img','pck');
        $shortname_list = Zend_Registry::isRegistered("shortname_list")?Zend_Registry::get('shortname_list'):array();        
        if(empty($result)) {            
            //get from any element the popup if necessary
            $tot_a = count($sublist);                        
            for($a = 0;$a < $tot_a;++$a) {
                $line_list = $sublist[$a];                
                $select->reset();
                if($line_list == 'parent') {
                    $select->from("t_ui_object_instances")->where("f_instance_name like '{$start_name}_{$search}%'");                                
                    $tot_b = count($shortname_list);                    
                    for($b = 0;$b < $tot_b;++$b){
                        $line_snl = $shortname_list[$b];
                        $select->where("f_instance_name not like '{$start_name}_{$search}_{$line_snl}%'");
                    }
                    
                }else {                                    
                    $res = $select->from("t_ui_object_instances")->where("f_instance_name like '{$start_name}_{$line_list}%'");                                
                    $tot_b = count($shortname_list);                    
                    for($b = 0;$b < $tot_b;++$b){
                        $line_snl = $shortname_list[$b];
                        $select->where("f_instance_name not like '{$start_name}_{$line_list}_{$line_snl}\_%'");
                    }                    
                }    
                
                $res = $select->query()->fetchAll();                                
                $tot_c = count($res);                
                for($c = 0;$c < $tot_c;++$c) {
                    $line = $res[$c];
                    if($line_list != 'parent') {
                        $instance_name = str_replace("{$start_name}_{$line_list}","{$start_name}_{$search}_{$line_list}_pp",$line['f_instance_name']);                                        
                    }
                    else {
                        $instance_name = str_replace("{$start_name}_{$search}","{$start_name}_{$search}_{$line_list}_pp",$line['f_instance_name']);                                        
                    }
                    $properties = $line['f_properties'];
                    
                    $tot_b = count($replace);
                    for($b = 0;$b < $tot_b;++$b) {        
                        if($line_list != 'parent') {
                            $properties = str_replace("{$replace[$b]}_{$line_list}_","{$replace[$b]}_{$search}_{$line_list}_pp_",$properties);                                        
                        }
                        else {
                            $properties = str_replace("{$replace[$b]}_{$search}_","{$replace[$b]}_{$search}_{$line_list}_pp_",$properties);                                        
                        }
                    }                      

                    $response[$i]['name'] = $instance_name;                
                    $response[$i]['f_instance_name'] = $instance_name;                
                    $properties = json_decode(utf8_encode($properties),true);      
                    if(!is_array($properties) && (is_null($properties) || !$properties)) {
                        var_dump($line['f_properties']);die($line['f_instance_name']);
                    }       
                    
                    $prop_keys = array_keys($properties) ;
                    $tot_d = count($prop_keys);
                    for($d = 0;$d < $tot_d;++$d) {
                        $key = $prop_keys[$d];
                        $pr = $properties[$key];                    
                        if($this->type_id == 11) { 
                            unset($response[$i]['name']);
                            unset($response[$i]['f_instance_name']);
                            $response[$i]['name'] = $instance_name;
                            $response[$i]['f_instance_name'] = $instance_name;
                            $tabs = array();
                            $old_prop = json_decode(utf8_encode($line['f_properties']),true);
                            foreach($old_prop['tabs'] as $key_pr => $line_pr){                            
                                $check_tab = str_replace("lyt_", "", $line_pr['layout']);                            
                                $check_tab = substr($check_tab,0,strpos($check_tab,'_edit'));                                                        
                                $check_exp = explode('_',$check_tab);                            
                                if(count($check_exp) == 1 && $check_exp[0] != '') {                                
                                    $tabs[] = $pr[$key_pr];
                                }
                            }                        
                            $response[$i][$key] = $tabs;
                        }
                        else{
                            $response[$i][$key] = $pr;
                        }
                    }
                    if(empty($response[$i])) unset($response[$i]);
                    $i++;
                }            
            }
        }
        else {
            if(is_array($result)) {
                $tot_a = count($result);
                for($a = 0;$a < $tot_a;++$a) {                 
                    $line = $result[$a];                
                    $response[$i]['name'] = $line['f_instance_name'];                
                    $properties = json_decode(utf8_encode($line['f_properties']),true);      
                    if(!is_array($properties) && (is_null($properties) || !$properties)) {
                        var_dump($line['f_properties']);die($line['f_instance_name']);
                    }

                    foreach($properties as $key => $pr) {                                        
                        $response[$i][$key] = $pr;                    
                    }
                    if(empty($response[$i])) unset($response[$i]);
                    $i++;
                }
            }
        }  
        
        return $response;
    }    
    
    public function getUserLang($lang_id)
    {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_localization",array("f_code"))->where("f_id = $lang_id");
        $res = $select->query()->fetchAll();
        return $res[0]['f_code'];
    }
    
    public function getLocalization($lang_id)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from("t_localization")->where("f_id = $lang_id")->query()->fetchAll();
        return $res[0];
    }
    
    public function getInfoSystem()
    {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_system_information");
        $res = $select->query()->fetchAll();
        return $res[0];
    }
    
    public function getSingleUI($name = '')
    {
        if(empty($name)) return array();
        $select = new Zend_Db_Select($this->db);
        return $select->from("t_ui_object_instances")->where("f_instance_name = ?",$name)->query()->fetch();         
    }
    
    public function getMenus() {
        $menus = array();
        if($this->setSearch('menu')) $menus = $this->getUI();
        return $menus;
    }
    
    public function getButtons() {
        $buttons = array();
        $type_id_search = $this->setSearch('button');
        if($type_id_search) {
            $buttons = $this->getUI();              
            /*foreach($this->shortname as $main ) {                
                    $buttons[] = array(
                        'name'=>"btn_{$main}_pp_select",
                        'f_instance_name'=>"btn_{$main}_pp_select",
                        'label'=>"Select",
                        'iconon'=>"select.png",
                        "action" =>"ov_{$main}.select()"
                    );
                    $buttons[] = array(
                        'name'=>"btn_{$main}_pp_close",
                        'f_instance_name'=>"btn_{$main}_pp_close",
                        'label'=>"Close",
                        'iconon'=>"dispose.png",
                        "action" =>"ov_{$main}.close()"
                    );
            }*/
        }
        return $buttons;
    }
    public function getTextfields() {
        $textfields = array();
        if($this->setSearch('textfield')) $textfields = $this->getUI();
        return $textfields;
    }
    public function getSelects() {
        $selects = array();
        if($this->setSearch('select')) $selects = $this->getUI();
        return $selects;
    }
    public function getCheckboxes() {
        $checkboxes = array();
        if($this->setSearch('checkbox')) $checkboxes = $this->getUI();
        return $checkboxes;
    }
    public function getModules() {        
        $modules = array();
        if($this->setSearch('module')) {
            $result_modules = $this->getUI();
            /*$tot_a = count($this->shortname);
            for($a=0; $a<$tot_a; ++$a) {
                $main = $this->shortname[$a];
                $result_modules[] = array(
                    'name'=>"mdl_{$main}_pp_cancel",
                    'f_instance_name'=>"mdl_{$main}_pp_cancel",
                    'type'=>"moModuleCommandBar",
                    'noshadow'=>"true",
                    'margin' => array(t => 2, l => 4, r => 4, b => 2),
                    'elements'=>"btn_{$main}_pp_close|btn_{$main}_pp_select"
                );
            }*/
            //----------
            $modules = $result_modules;
        }
        return $modules;
    }
    public function getLayouts() {
        $layouts = array();
        if($this->setSearch('layout')) {
            $layouts = $this->getUI();                        
            /*$tot_x = count($layouts);
            //foreach($layouts as &$lyt) {                
            for($x = 0;$x < $tot_x;++$x) {
                $lyt = $layouts[$x];
                if(strpos($lyt['name'],"_pp") + 3 == strlen($lyt['name'])) {
                    $name = str_replace('lyt_','',$lyt['name']);
                    $name = str_replace('_pp','',$name);
                    // Base popup layout
                    $new_layout = array(                        
                        array( "idp"=> -1, "ov"=> 1, "tp"=> 0, "wh"=> 100, "level"=> -1, "module"=> "", "lock"=> 0, "minv"=> 36, "acc"=> 0 ),
                        array( "idp"=> 0, "ov"=> 0, "tp"=> 0, "wh"=> 538, "level"=> -1, "module"=> "", "acc"=> 0, "lock"=> 0, "minv"=> 36 ),
                        array( "idp"=> 0, "ov"=> 0, "tp"=> 0, "wh"=> 36, "level"=> -1, "module"=> "mdl_{$name}_pp_cancel", "acc"=> 0, "lock"=> 1, "minv"=> 36 ),
                        array( "idp"=> 1, "ov"=> 1, "tp"=> 0, "wh"=> 258, "level"=> -1, "module"=> "", "acc"=> 0, "lock"=> 0, "minv"=> 36 )
                    );

                    $tot_a = count($lyt['layout']);
                    $vertical_cut = false;
                    for($a = 0;$a < $tot_a;$a++) {
                        $line_a = $lyt['layout'][$a];
                        if(strpos($line_a['module'],'_sel') !== false || strpos($line_a['module'],'summary') !== false){ 
                            $vertical_cut = true;
                            break;
                        }                        
                    }
                    if(!$vertical_cut) { 
                        $new_layout = array(
                            array(
                                "idp"=> -1,
                                "ov"=> 1,
                                "tp"=> 0,
                                "wh"=> 100,
                                "level"=> -1,
                                "module"=> "",
                                "lock"=> 0,
                                "minv"=> 36,
                                "acc"=> 0,
                            )
                        );                        
                    }                    
                    
                    for($a = 0;$a < $tot_a;$a++) {
                        $line_a = $lyt['layout'][$a];
                        if(($line_a['idp'] <=0 && strpos($line_a['module'],'_sel') === false && strpos($line_a['module'],'_summary') === false) && strpos($line_a['module'],'crud') === false) continue;
                        if($vertical_cut) {
                            $line_a['idp'] = strpos($line_a['module'],'crud') !== false?1:3;                        
                        }
                        elseif(strpos($line_a['module'],'crud') !== false) {
                            $line_a['idp'] = 0;                            
                        }
                        $new_layout[] = $line_a;                        
                    }
                    if(!$vertical_cut) {
                        $new_layout[] =  array(
                            "idp"=> 0,
                            "ov"=> 0,
                            "tp"=> 0,
                            "wh"=> 36,
                            "level"=> -1,
                            "module"=> "mdl_{$name}_pp_cancel",
                            "acc"=> 0,
                            "lock"=> 1,
                            "minv"=> 36
                        );
                    }                   
                    //$lyt['layout'] = $new_layout;                    
                    $layouts[$x]['layout'] = $new_layout;                    
                }
            }     */       
        }
        return $layouts;
    }
    public function getTabbars() {
        $tabbars = array();
        if($this->setSearch('tabbar')) $tabbars = $this->getUI();
        return $tabbars;
    }
    public function getOptions() {                
        $options = array();
        if($this->setSearch('options')) {
            $t_options = $this->getUI();
            for($i=0; $i<count($t_options); $i++) {
                $name = $t_options[$i]["name"]; unset($t_options[$i]["name"]);
                $options[$name] = $t_options[$i];
            }
        }
        return $options;
    }
    public function getImages() {
        $images = array();
        if($this->setSearch('image')) $images = $this->getUI();
        return $images;
    }
    public function getPicklists() {
        $picklists = array();
        if($this->setSearch('picklist')) $picklists = $this->getUI();
        return $picklists;
    }
}