<?php



class Mainsim_Model_Document
{
    private $db;
    public function __construct()
    {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    /**
     * Return row in t_attachements with requested file
     * @param string $column can be f_session_id or f_code
     * @param string $value session_id/f_code value
     * @param string $size can be thumbnail,medium,high(image) or doc (other)
     * @return array t_attachments row
     */
    public function getFile($column,$value,$size)
    {
        $query = "SELECT * FROM t_attachments WHERE f_active != -1 and ";
        $whereClause = $this->db->quoteInto("$column = ? ",$value);
        $whereClause .= $this->db->quoteInto("and f_type = ?",$size);
        $query.=$whereClause; 
        return $this->db->query($query)->fetch();
    }
}
