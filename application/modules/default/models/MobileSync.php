<?php
class Mainsim_Model_MobileSync extends Mainsim_Model_MobileBase{
    
    public function __construct() {
        
       parent::__construct();
    }
    
    public function getDbStructure($sessionId, $toTranslate = null){
        // 0 : table name
        // 1 : get data
        // 2 : where conditions
        $checkTables = array(
            array('t_creation_date', 0),
            array('t_creation_date_history', 0),
            array('t_custom_fields', 0),
            array('t_custom_fields_history', 0),            
            array('t_pair_cross', 0),
            //array('t_selectors', 0),
            //array('t_selectors_history', 0),
            //array('t_selectors_parent', 0),
            //array('t_selectors_types', 0),
            //array('t_selector_ware', 0),
            //array('t_selector_wo', 0),
            //array('t_bookmarks', 0),
            //array('t_ui_object_instances', 1, array('f_type_id IN (6,7,8,12,13)')), // filter fields
            array('t_wares', 0),
            array('t_wares_parent', 0),
            array('t_wares_relations', 0),
            array('t_wares_types', 1),
            array('t_ware_wo', 0),
            array('t_wf_exits', 1, array("f_script IS NOT NULL OR f_script <> ''"), array('f_script')),
            array('t_wf_groups', 1),
            array('t_wf_phases', 1),     
            //array('t_wizard', 1),
            array('t_workflows', 1),
            array('t_workorders', 0),
            array('t_workorders_history', 0),
            array('t_workorders_parent', 0),
            array('t_workorders_types', 1),
            array('t_wo_relations', 0),
            array('t_scripts', 1, array("f_script IS NOT NULL OR f_script <> ''"), array('f_script')),
            array('t_locked', 0),
            array('t_users', 1)
        );
        
        $userData = Zend_Auth::getInstance()->getIdentity();

        $mapTypes = array(
            'varchar' => 'string',
            'int' => 'int',
            'longtext' => 'string',
            'text' => 'string',
            'double' => 'real'
        );
        
        try{
            // loop tables
            for($i = 0; $i < count($checkTables); $i++){

                // get table info
                $tableFields = Mainsim_Model_MobileUtilities::getMobileFields($checkTables[$i][0], unserialize(MOBILE_MODULES));
                // save table command creation
                $commandCreation = "create_table " . $checkTables[$i][0];
                $jsonFields = array();
                foreach($tableFields['data'] as $f){
                    if($checkTables[$i][0] != 't_creation_date' && $f['COLUMN_NAME'] == 'f_id'){
                        $jsonFields[] = $f['COLUMN_NAME'] . " inc";
                    }
                    else{
                        $jsonFields[] = $f['COLUMN_NAME'] . " " . $mapTypes[$f['DATA_TYPE']] . ($f['DEFAULT'] ? ' ' . $f['DEFAULT'] : '');
                    }
                }
                
                if($checkTables[$i][0] == 't_wares_parent'){
                    $jsonFields[] = 'nfc string';
                }
                
                $commandCreation .= " (" . implode(',', $jsonFields) . ")";
                $aux['structure'] = $commandCreation;
                
                // get table data if requested
                if($checkTables[$i][1]){
                    $selectData = new Zend_Db_Select($this->db);
                    $selectData->from($checkTables[$i][0], $tableFields['list']);
                    // check if exists filter
                    if(isset($checkTables[$i][2])){
                        $where = implode(' AND ', $checkTables[$i][2]);
                        $selectData->where($where);
                    }
                    $aux['data'] = $this->dbSelect($selectData, Zend_Db::FETCH_NUM, false);
                    
                    // translate phases names
                    if($checkTables[$i][0] == 't_wf_phases'){
                        $objTranslate = new Mainsim_Model_Translate('it_IT');
                        $objTranslate->setLang($objTranslate->getLangNameFromCode($userData['fc_usr_language']));
                        for($t = 0; $t < count($aux['data']); $t++){
                            $aux['data'][$t][5] = $objTranslate->_($aux['data'][$t][5]);
                            $aux['data'][$t][6] = $objTranslate->_($aux['data'][$t][6]);
                        }
                    }
                    
                    // check if exists fields to empty
                    if(isset($checkTables[$i][3])){
                        for($k = 0; $k < count($checkTables[$i][3]); $k++){
                            for($z = 0; $z < count($jsonFields); $z++){
                                if(strpos($jsonFields[$z], $checkTables[$i][3][$k]) === 0){
                                    $indexToRemove[] = $z;
                                }
                            }
                        }
                        
                        for($m = 0; $m < count($aux['data']); $m++){
                            for($n = 0; $n < count($indexToRemove); $n++){
                                if($aux['data'][$m][$indexToRemove[$n]]){
                                    $aux['data'][$m][$indexToRemove[$n]] = '#rem#';
                                }
                            }
                        }
                    }
                }
                else{
                    $aux['data'] = null;
                }
                
                $result[] = $aux;
            }
            
            
            //$result[] = $this->getTaskReadOnlyTable();

            // get UI table
            $result[] = $this->getUITable(unserialize(MOBILE_MODULES));
            
            // get language translations
            $lng = Mainsim_Model_MobileUtilities::getLanguageTranslation($userData['fc_usr_language'], $toTranslate);
            // priority
            $select = new Zend_Db_Select($this->db);
            $select->from('t_creation_date', array('f_title'))
               ->join('t_wares', 't_creation_date.f_id = t_wares.f_code', array('fc_action_priority_color', 'fc_action_priority_value'))
               ->where("t_creation_date.f_category = 'ACTION'")
               ->where("t_wares.fc_action_type = 'priority'");
            $res = $this->dbSelect($select);
            for($i = 0; $i < count($res); $i++){
                $priority[$res[$i]['fc_action_priority_value']] = array($res[$i]['f_title'], $res[$i]['fc_action_priority_color']);
            }
       
            // get settings
            $settings = $this->getMobileSettings();
            return $this->createResponseObject(true, 'LOAD_STRUCTURE_DB', $sessionId, array('db' => $result, 'lang' => $lng, 'settings' => $settings, 'priority' => $priority));
        }
        catch(Exception $e){
            print($e->getMessage());
            return $this->createResponseObject(false, 'ERROR_RETRIEVING_DB_STRUCTURE', $sessionId);
        }
    }
	
	
	  // setting:   mobile_download_number 
	 
	  public function getMobileSettingSyncNumber(){
    
        $select = New Zend_Db_Select($this->db);
        $select->reset();
        
        $select->from('t_creation_date', array('f_title','f_description'))
                      ->where("f_title = 'mobile_download_number' ");
        $res = $select->query()->fetchAll();
                
        if(count($res)) {       
              $nsync=(int) $res[0]['f_description'];
              if($nsync<1) $nsync=10;
              return $nsync;     
        } else return 10;  
    
    }
	
	
    public function getMobileSettings(){
            $settings = array(
                    'mobile_to_desktop' => 0,
                    'mobile_no_task_options' => 1,
                    'mobile_no_task_options_quality' => 1,
                    'mobile_user_wizard_level' => 0,
                    'mobile_new_wo_button_level' => 0,
                    'mobile_signature_level' => 0,
                    'mobile_disable_types_multi_change_phase_wo' => 1,
                    'mobile_max_wizard_offline_wo' => 0,
                    'mobile_footer_home_asset' => 0,
                    'mobile_footer_home_wizard' => 0,
                    'mobile_date_wo_list' => 0,
                    'mobile_nphase_task_checked' => 0,
                    'mobile_taskview_method' => 0,
                    'mobile_main_url' => 0,
                    'mobile_scripts' => 2,
                    'mobile_wo_types' => 0
            );
            $select = New Zend_Db_Select($this->db);
            $select->from('t_creation_date', array('f_title','f_description'))
                    ->where("f_title IN ('" . implode("','", array_keys($settings)) . "')");
            $res = $select->query()->fetchAll();
            for($i = 0; $i < count($res); $i++){

                if($settings[$res[$i]['f_title']] == 1){
                        $result[$res[$i]['f_title']] = json_decode($res[$i]['f_description']);
                }
                else if($settings[$res[$i]['f_title']] == 2){
                        $result[$res[$i]['f_title']] = Mainsim_Model_Utilities::chg($res[$i]['f_description']);  // base64_encode()
                }
                else{
                        $result[$res[$i]['f_title']] = $res[$i]['f_description'];
                }
            }
            
            $userData = Zend_Auth::getInstance()->getIdentity();
            $select->reset();
            $select->from("t_users", array("fc_usr_mobile_autoswitch_desktop"))
                ->where("f_code = " . $userData['f_code']);
            $res = $select->query()->fetchAll();
            $result["fc_usr_mobile_autoswitch_desktop"] = $res[0]["fc_usr_mobile_autoswitch_desktop"];
            
            
            // attachment url
            $result['attachment_url'] = Zend_Registry::get('attachmentsFolder');
            
            return $result;
    }

    public function sync($params = null){
        
       $nsync = $this->getMobileSettingSyncNumber(); 
        
        try{
            $userData = Zend_Auth::getInstance()->getIdentity();
			$this->dbBeginTransaction();
            $objWo = new Mainsim_Model_MobileWorkorders();
            // get locked user wo
            $woListLocked = $objWo->getUserWorkordersLocked($userData['f_code']);
            // get unlocked user wo
            $params = array(
                'limit' => $nsync,   // default 10
                'noLocked' => true,
                'wares' => true
            );
            $woListUnlocked = $objWo->getUserWorkordersCodes($params);
            // lock unlocked user wo
            //$session = new Zend_Session_Namespace('session');
            //$userData = $session->data;
            // admin users don't lock workorders
            for($i = 0; $i < count($woListUnlocked); $i++){
                $objWo->lockWorkorder(array(
                    'f_code' => $woListUnlocked[$i],
                    'f_user_id' => $userData['f_code']
                ));
            };
            
            $woList = $objWo->getUserWorkordersData(array_merge($woListLocked, $woListUnlocked), $params);

            // get t_locked data for user
            $select = new Zend_Db_Select($this->db);
            $select->from('t_locked')
                   ->where('f_user_id = ' . $userData['f_code']);
            $woList['t_locked'] = $this->dbSelect($select, Zend_Db::FETCH_NUM);

            $this->dbEndTransaction();

            // get workorders relationships
            for($i = 0; $i < count($woList['t_creation_date']); $i++){
                $par_wo_relations[] = Mainsim_Model_MobileUtilities::getRelations($woList['t_creation_date'][$i][0], 'workorders', 'fc_progress');
            }
            
            $custom['par_wo_relations'] = $par_wo_relations;

            return $this->createResponseObject(true, 'SYNC', isset($params['sessionId']) ? $params['sessionId'] : null, $woList, null, null, null, $custom);
        }
        catch(Exception $e){
            print($e->getMessage());
            return $this->createResponseObject(false, 'ERROR_SYNC', isset($params['sessionId']) ? $params['sessionId'] : null);
        }
    }
    
    public function syncAdd($params){
        try{
            $this->dbBeginTransaction();
            $objWo = new Mainsim_Model_MobileWorkorders();
            // get unlocked user wo
            $params['locked'] = false;
            $woList['db'] = $objWo->getUserWorkorders($params);
            // lock unlocked user wo
            $userData = Zend_Auth::getInstance()->getIdentity();
            // lock workorders
            for($i = 0; $i < count($woList['db']['t_workorders']); $i++){
                $objWo->lockWorkorder(array(
                    'f_code' => $woList['db']['t_workorders'][$i][2],
                    'f_user_id' => $userData['f_code']
                ));
                $woList['wo_list'][] = $woList['db']['t_workorders'][$i][2];
            };
            // get t_locked data for user
            if(is_array($woList['wo_list'])){
                $select = new Zend_Db_Select($this->db);
                $select->from('t_locked')
                       ->where('f_user_id = ' . $userData['f_code'])
                       ->where('f_code in (' . implode(',', $woList['wo_list']) . ')');
                $woList['db']['t_locked'] = $this->dbSelect($select, Zend_Db::FETCH_NUM);
            }
            
            // get workorders relationships
            for($i = 0; $i < count($woList['t_creation_date']); $i++){
                $par_wo_relations[] = Mainsim_Model_MobileUtilities::getRelations($woList['t_creation_date'][$i][0], 'workorders', 'fc_progress');
            }
            
            $custom['par_wo_relations'] = $par_wo_relations;
            
            $this->dbEndTransaction();
            return $this->createResponseObject(true, 'SYNC_ADD', isset($params['sessionId']) ? $params['sessionId'] : null, $woList, null, null, null, $custom);
        }
        catch(Exception $e){
            return $this->createResponseObject(false, 'ERROR_SYNC_ADD', isset($params['sessionId']) ? $params['sessionId'] : null);
        }
    }
    
    public function syncAdmin($params = null){
        
        $nsync = $this->getMobileSettingSyncNumber();
        
        try{
            $this->dbBeginTransaction();
            $objWo = new Mainsim_Model_MobileWorkorders();
            // confirm/remove workorders already get
            $params['in'] = $params['code_list'] ? $params['code_list'] : array('-1');
            $woListAlreadyGet = $objWo->getUserWorkorders($params);         
            unset($params['in']);
            // get new workorders
            $params['limit'] = $nsync;  // 10
            $params['not in'] = $params['code_list'];
            $woListNew = $objWo->getUserWorkorders($params);
            // get locked wo
            $userData = Zend_Auth::getInstance()->getIdentity();
            $select = new Zend_Db_Select($this->db);
            $select->from('t_locked')
                   ->where('f_user_id = ' . $userData['f_code']);
            $woList['t_locked'] = $this->dbSelect($select, Zend_Db::FETCH_NUM);
            $this->dbEndTransaction();
            // merge already get workorders with new workorders
            if(is_array($woListAlreadyGet)){
                foreach($woListAlreadyGet as $key => $value){
                    if(is_array($woListAlreadyGet[$key]) && is_array($woListNew[$key])){
                        $woList[$key] = Mainsim_Model_MobileUtilities::removeDuplicate(array_merge($woListAlreadyGet[$key], $woListNew[$key]), $key);
                    }
                    else{
                        $woList[$key] = $woListAlreadyGet[$key];
                    }
                }
            }
            return $this->createResponseObject(true, 'SYNC_ADMIN', isset($params['sessionId']) ? $params['sessionId'] : null, $woList);   
        }
        catch(Exception $e){
            return $this->createResponseObject(false, 'ERROR_SYNC_ADMIN', isset($params['sessionId']) ? $params['sessionId'] : null);
        }
    }
    
    public function getTaskReadOnlyTable(){
        
        // get data for virtual table t_task_readonly
        /*
        $taskArray = array(
            'mdl_wo_task_tg' => array(
                'type' => 'TASKS'
            )
        );
        $tableFields = Mainsim_Model_MobileUtilities::getMobileFields('t_custom_fields', $taskArray);*/
        $fields = Mainsim_Model_MobileUtilities::getModuleFields('mdl_task_tg');
        
        
        // save table command creation
        $commandCreation = "create_table t_task_readonly";
        $jsonFields = array();
        $jsonFields[] = 'f_code int';
        foreach($fields as $f => $v){
            $jsonFields[] = str_replace(" ", "_", $v) . " string";
        }
        $result['structure'] = $commandCreation . " (" . implode(',', $jsonFields) . ")";
        return $result;
    }
    
    private function getUITypes($visibility, $readonly){
        $userData = Zend_Auth::getInstance()->getIdentity();
        if($visibility != null){
            
            $aux = explode(',', $visibility);
            for($i = 0; $i < count($aux); $i++){
                $result[] = array(
                    'type_id' => $aux[$i],
                    'readonly' => (strpos($readonly, $aux[$i]) !== FALSE) ? 1 : 0
                );
            }
        }
        else if($readonly != null){
            
            $aux = explode(',', $userData['workorder_types']);
            for($i = 0; $i < count($aux); $i++){
                $result[] = array(
                    'type_id' => $aux[$i],
                    'readonly' => (strpos($readonly, $aux[$i]) !== FALSE) ? 1 : 0
                );
            }
        }
        else{
            $result[] = array(
                'type_id' => 0,
                'read_only' => 0
            );
        }
        return $result;    
    }
    
    public function getUITable($modules){
        foreach($modules as $m => $n){
            // get module name
            $moduleName = str_replace(array('mdl_', '_tg'), array('', ''), $m);
            
			$selectUI = new Zend_Db_Select($this->db);
			
			// get field to show
			$selectUI->from('t_ui_object_instances')
                     ->where("f_instance_name LIKE 'mdl_" . $moduleName . "_edit%'")
					 ->where("f_properties LIKE '%moModuleComponent%'");
            $res = $this->dbSelect($selectUI);
			$properties = json_decode($res[0]['f_properties']);
			$fields = $properties->fields;
			$auxFields = explode(",", str_replace("|", ",", $fields));
			
			$selectUI->reset();
            $selectUI->from("t_ui_object_instances")
                         ->where('f_type_id IN (6,7,8,13,14)')
                         //->where("(f_instance_name LIKE '%txtfld_" . $moduleName . "_edit%' OR f_instance_name LIKE '%chkbx_" . $moduleName . "_edit%' OR f_instance_name LIKE '%slct_" . $moduleName . "_edit%')");
						 ->where("f_instance_name IN  ('" . str_replace(array(",", "|") , "','", $fields) .  "')");
            $res2 = $this->dbSelect($selectUI);
			for($i = 0; $i < count($res2); $i++){
                            $uis[$res2[$i]['f_instance_name']] = $res2[$i];
			}
			
            // get bookmark
            /*
            $selectModule = new Zend_Db_Select($this->db);
            $selectModule->from("t_bookmarks")
                         ->where("f_module_name='" . $m . "'")
                         ->where("f_name='Default'");
            $bookmark = $this->dbSelect($selectModule);
            
            // loop through bookmark fields
            $bookmarkFields = json_decode($bookmark[0]['f_properties']);
            $objSys = new Mainsim_Model_System();
            $bookmarks = $objSys->getBookmark($m, 'Default');
            $bookmarkFields = json_decode(utf8_encode($bookmarks['view']['f_properties']));*/
            // get language translations
            $userData = Zend_Auth::getInstance()->getIdentity();
            $lng = Mainsim_Model_MobileUtilities::getLanguageArray($userData['fc_usr_language']);
            
            $rowCounter = 0;
            for($i = 0; $i < count($auxFields); $i++){
                
                $ui = $uis[$auxFields[$i]];
                
                 
                 //$uiProps = json_decode(Mainsim_Model_Utilities::chg($ui['f_properties']));
                 $uiProps = json_decode($ui['f_properties']);
				 //$uiProps = json_decode(utf8_decode($ui['f_properties']));
                 if(!in_array($uiProps->bind, array('fc_task_issue', 'fc_wo_task_order')) && $uiProps->nomobile != 1){
                    
                    $types = $this->getUITypes($uiProps->v_type, $uiProps->r_type);
                    
                    
            //  print_r($uiProps); print_r($types);  die();  
                    
                    for($j = 0; $j < count($types); $j++){
                        
                        // create UI table row
                        $row = array();
                        
                        // f_id inc
                        $row[] = $rowCounter;
                        // f_type (workorder, ware, etc...)
                        $row[] = $n['type'];
                        // workorder/ware id types
                        $row[] = $types[$j]['type_id'];
                        // field_type according to ui_object_instances types
                        switch($ui['f_type_id']){
                            case 6:
                                // textarea
                                if($uiProps->text == true){
                                    $row[] = 'textarea';
                                }
                                // date
                                else if($uiProps->dtmpicker){
                                    $row[] = $uiProps->dtmpicker;
                                }
                                // attachment
                                else if($uiProps->attach === true){
                                    $row[] = $uiProps->attach;
                                }
                                else{
                                    $row[] = 'input';
                                }
                                break;
                            // radio checkbox
                            case 8:
                                if($uiProps->radio === true){
                                    $row[] = 'radio';
                                }
                                else{
                                    $row[] = 'checkbox';
                                }
                                break;
                            case 7:
                                $row[] = 'select';
                                break;
                            case 13:
                                $row[] = 'image';
                                break;
                            default:
                                $row[] = 'select';
                                break;
                        }                  
                        // field name
                        $row[] = $uiProps->bind;
                        // label
                        $row[] = $lng[$uiProps->label] ? $lng[$uiProps->label] : $uiProps->label;               
                        // select options
                        if($uiProps->bind == 'fc_owner_name'){
                            $options = null;
                            $selectUsers = new Zend_Db_Select($this->db);
                            $selectUsers->from('t_users');
                            $resUsers = $this->dbSelect($selectUsers);
                            $options[] = array(
                                    'value' => '',
                                    'label' => 'none',
                                    'selected' => 'false'
                            );
                            for($p = 0; $p < count($resUsers); $p++){
                                $options[] = array(
                                    'value' => $resUsers[$p]['fc_usr_firstname'] . " " . $resUsers[$p]['fc_usr_lastname'],
                                    'label' => $resUsers[$p]['fc_usr_firstname'] . " " . $resUsers[$p]['fc_usr_lastname'],
                                    'selected' => 'false'
                                );
                            }
                            $row[] = json_encode($options);
                        }
                        else if($uiProps->server){
                            $options = null;
                            if($uiProps->bind == 'f_priority'){
                                $objOptions = new Mainsim_Model_OptionSelect();
                                $aux = $objOptions->getWoPriorities();
                                for($p = 0; $p < count($aux); $p++){
                                    $options[] = array(
                                        'value' => $aux[$p]['value'],
                                        'label' => $aux[$p]['label'],
                                        'selected' => $aux[$p]['selected'] ? 'true' : 'false'
                                    );
                                }
                                $row[] = json_encode($options);
                            }
                        }
                        else if($uiProps->options){
                            $options = null;
                            for($p = 0; $p < count($uiProps->options); $p++){
                                $options[] = array(
                                    'value' => $uiProps->options[$p]->value,
                                    'label' => $uiProps->options[$p]->label,
                                    'selected' => $uiProps->options[$p]->selected ? 'true' : 'false'
                                );
                            }
                            $row[] = json_encode($options);
                        }
                        // select options from db
                        else if($ui['f_type_id'] == 7){
                            $selectOptions = new Zend_Db_Select($this->db);
                            $selectOptions->from('t_ui_object_instances')
                                          ->where("f_instance_name='" . $uiProps->category . "'")
                                          ->where("f_type_id = 12");
                            $options = $this->dbSelect($selectOptions, null, false);
                            $aux = json_decode($options[0]['f_properties']);
                            for($p = 0; $p < count($aux); $p++){
                                $aux[$p]->label = $lng[$aux[$p]->label] ? $lng[$aux[$p]->label] : $aux[$p]->label;
                            }
                            $row[] = json_encode($aux);
                        }
                        // checkbox options
                        else if($ui['f_type_id'] == 8){
                            $row[] = json_encode((array)$uiProps->buttons);
                        }
                        else{
                            $row[] = '';
                        }
                        
                        
                        
                        // visibility
                        $row[] = $uiProps->v_level ? $uiProps->v_level : -1;

                        // editability
                        
                        
                        $edty=-1;
                        
                        if($types[$j]['readonly'] == 1){
                            $edty=0;
                        }
                        else {
                        
                          if($uiProps->r_on){
                          
                            $edty=0;
                            if($uiProps->r_level) $edty=$uiProps->r_level;
                          
                          }
           
                        }
                        
                        $row[] = $edty;
                

                        // mandatory
                        $row[] = $uiProps->m_on == true ? 1 : 0;
                        // selector visibility
                        $row[] = '';
                        // table field
                        if($m == 'mdl_wo_tg'){
                            //$table = Mainsim_Model_MobileUtilities::getFieldTable($uiProps->bind, array('t_creation_date', 't_workorders', 't_custom_fields'));
							$table = $this->getFieldTable($uiProps->bind, array('t_creation_date', 't_workorders', 't_custom_fields'));
                        }
                        else if($m == 'mdl_asset_tg'){
                            //$table = Mainsim_Model_MobileUtilities::getFieldTable($uiProps->bind, array('t_creation_date', 't_wares', 't_custom_fields'));
							$table = $this->getFieldTable($uiProps->bind, array('t_creation_date', 't_wares', 't_custom_fields'));
                        }
                        else if($m == 'mdl_wo_task_tg'){
                            //$table = Mainsim_Model_MobileUtilities::getFieldTable($uiProps->bind, array('t_pair_cross'));
							$table = $this->getFieldTable($uiProps->bind, array('t_pair_cross'));
                        }
                        $row[] = $table;
                        // exp
                        $row[] = $uiProps->checkExp;
                        // visibility for workflow
                        $row[] = $uiProps->v_wf;
                        // editability for workflow
                        $row[] = $uiProps->r_wf;
                        // mandatory for workflow
                        $row[] = $uiProps->m_wf;
                        
                        // add field only if exist in database
                        if($table){
                            $result['data'][] = $row;
                        }
                        
                    }
               }
            }
        }
        $result['structure'] = 'CREATE_TABLE t_ui_fields (f_id inc, f_type string, f_type_id int, f_field_type string, f_field_name string, f_label string, f_options string, f_visibility int -1, f_editability int 0,f_mandatory int, f_selector_visibility string, f_table_ref string, f_check_exp string, f_visibility_wf string, f_editability_wf string, f_mandatory_wf string)';
        return $result;
    }
	
	public function getFieldTable($field, $tables){
        
        // get onte time tables definition from db if not exist  
        for($i = 0; $i < count($tables); $i++){
            if(!isset($this->tableDefinition[$tables[$i]])){
                
                $tableDefinition = $this->db->describeTable($tables[$i]);
                foreach($tableDefinition as $key => $value){
                    $this->tableDefinition[$tables[$i]][] = $value['COLUMN_NAME'];
                }
            }   
        }
        // serach fields in tables
        foreach($this->tableDefinition as $key => $value){
            if(in_array($field, $value)){
                return $key;
            }
        }
    }
 
}
?>
