<?php

class Mainsim_Model_Translate
{
    private $db;
    private $select;
    private $userLang;     
    private $alreadySearched;
    
    public function __construct($userLang = null,$db = null) {
        $this->db = is_null($db)?Zend_DB::factory(Zend_Registry::get('db')):$db;
        $this->select = new Zend_Db_Select($this->db);
        $this->alreadySearched = array();
        if(is_null($userLang) && Zend_Auth::getInstance()->hasIdentity()) {            
            $userLang = $this->getLangNameFromCode(Zend_Auth::getInstance()->getIdentity()->f_language);            
        }        
        $this->userLang = $userLang;        
    }
    
    /**
     * Set new Language for translation
     * @param type $lang
     */
    public function setLang($lang) 
    {
        if(!empty($lang)) {
            $query = $this->db->quoteInto("select count(*) as num from t_localization where f_code = ?",$lang);
            $res = $this->db->query($query)->fetch();
            if($res['num'] == 0) {
                throw new Exception($this->_("Unknown language")." ".$lang);
            }
            $this->userLang = $lang;
        }
    }
    
    
    public function getLangNameFromCode($langId = null)
    {
        if(empty($langId) || is_null($langId)){
            throw new Exception($this->_("langId param is empty"));
        }
        $select = new Zend_Db_Select($this->db);
        $select->from("t_localization",array("f_code"))->where("f_id = $langId");
        $res = $select->query()->fetch();
        return $res['f_code'];
    }
    
    public function getLang()
    {
        return $this->userLang;
    }
    
    /**
     * Translate passed value if possible
     * @param string $translate string to translate
     * @param string $lang other language
     */
    public function _($translate,$encode = true,$langSource = null,$langTranslate = null)
    {
        $languages = array('fc_lang_it_IT','fc_lang_fr_FR','fc_lang_es_ES');
        if(empty($translate) || is_null($translate)){
            return $translate;
        }
        if(is_null($langSource)) {
            $langSource = 'f_title';
        }            
        else {
            $langSource = "fc_lang_".$langSource;
        }
        if($langSource != 'f_title' && !in_array($langSource,$languages)){
            throw new Exception($this->_("Unknown language")." ".$langSource);
        }
        if(is_null($langTranslate)) {
            $langTranslate = $this->userLang;
        }
        $langTranslate = "fc_lang_".$langTranslate;        
        if(!isset($this->alreadySearched[$langSource][$translate])) {//check if this search has been made before
            $this->select->reset();
            $this->select->from(array("t1"=>"t_creation_date"),array('fc_lang_en_GB'=>'f_title','f_phase_id','f_id'))
                ->join(array('t2'=>'t_systems'),'t1.f_id = t2.f_code',$languages)
                ->where('f_phase_id = 1')->where("f_type_id = 7")->where("$langSource = ?",$translate);
            if(Mainsim_Model_Utilities::isMysql()) { $this->select->where("BINARY($langSource) = ?",$translate); }
            else {
                
                /*
                $resCollate = $this->db->query("SELECT cast(SERVERPROPERTY ('Collation') as varchar(50)) as coll")->fetch();
                $collation = str_replace("_CI_", "_CS_", $resCollate['coll']);
                $this->select->where("$f COLLATE {$collation}) = ?",$translate);*/
            }
            $resVal = $this->select->query()->fetch();              
            $translated = Mainsim_Model_Utilities::clearChars($translate);            
            if(!empty($resVal[$langTranslate])) {
                $this->alreadySearched[$langSource][$translate] = $resVal;//take a copy for next translation
                $translated =  $resVal[$langTranslate];
            }
        }
        else { //get previous research            
            $translated =  $this->alreadySearched[$langSource][$translate][$langTranslate];
        }
        if($encode) {
            $translated = Mainsim_Model_Utilities::chg($translated);
        }
        return $translated;
    }    
    
    /**
     * Get all translation for specific language
     * @param string $lang
     * @return array associative array english->choosen lang
     */
    public function getLanguage($lang = '',$encode = true) 
    {
        $result = array();
        if(!$lang) {
            $lang = $this->getUserLang(Zend_Auth::getInstance()->getIdentity()->f_language);
        }
        if($lang == "en_GB") return array();
        $q = new Zend_Db_Select($this->db);
        $res = $q->from(array("c" => "t_creation_date"), array("f_title"))
            ->join(array("s" => "t_systems"), "c.f_id = s.f_code", array("translation" => "fc_lang_{$lang}"))
            ->where("s.f_type_id = 7")->where("c.f_phase_id = 1")->query()->fetchAll(); 
        $tot = count($res);
        for($i = 0;$i < $tot;++$i) {
            $result[addslashes($res[$i]["f_title"])] = $encode?Mainsim_Model_Utilities::chg($res[$i]["translation"]):$res[$i]["translation"];
        }
        return $result;
    }
    
    public function getUserLang($lang_id)
    {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_localization",array("f_code"))->where("f_id = $lang_id");
        $res = $select->query()->fetchAll();
        return empty($res)?'':$res[0]['f_code'];
    }
}