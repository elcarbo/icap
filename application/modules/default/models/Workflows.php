<?php

class Mainsim_Model_Workflows
{
    
    /**
     *
     * @var Zend_Db 
     */
    private $db;
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function getWorkflows()
    {
        $select = new Zend_Db_Select($this->db);
        $select->from('t_workflows');
        $wfs = $select->query()->fetchAll();
        
        foreach($wfs as $wf) {
            $result[] = array(
                "f_id" => $wf['f_id'],
                "f_name" => $wf['f_name']
            );
        }
        
        return $result;
    }
    
    public function getWorkflowPhases($id = null)
    {
        if(!$id) return false;        
        $select = new Zend_Db_Select($this->db);
        $select->from('t_wf_phases')->where('f_wf_id = ?',$id);
        $result = $select->query()->fetchAll();
        return $result;
    }
    
    public function getWfPhase($id = null)
    {
        if(!$id) return false;        
        $select = new Zend_Db_Select($this->db);
        $select->from('t_wf_phases')->where('f_id = ?',$id);
        $result = $select->query()->fetchAll();
        return $result;
    }
    
    public function getWfExit($wf_phase_number, $wf_id)
    {
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        $select = new Zend_Db_Select($this->db);
        $select->from(array("e"=>"t_wf_exits"))
            ->join(array("p"=>"t_wf_phases"), "e.f_wf_id = p.f_wf_id AND e.f_phase_number = p.f_number")
            ->where("e.f_wf_id = ?", $wf_id) 
            ->where("e.f_phase_number = ?", $wf_phase_number) 
            ->where("(p.f_visibility & {$userinfo->f_level}) != 0")
            ->where("(e.f_visibility & {$userinfo->f_level}) != 0")
            ->order("e.f_exit ASC")->order("e.f_order ASC");
        
        $result = $select->query()->fetchAll();
        if($wf_phase_number) $response = array(array('label' => 'Change phase', 'value' => 0, 'selected' => true));
        foreach($result as $phase) {
            $response[] = array('label'=>$phase['f_description'], 'value'=>$phase['f_exit'], 'selected'=> false);            
        }
        
        return $response;
    }
}