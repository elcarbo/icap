<?php

class Mainsim_Model_Mainpage
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));        
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
        
    public function getSettings($local = 0) {
        $result = array();
        $q = new Zend_Db_Select($this->db);
        $q->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
            ->join(array("s" => "t_systems"), "c.f_id = s.f_code", array())
            ->where("c.f_type = 'SYSTEMS'")->where("c.f_category = 'SETTING'")->where("c.f_phase_id = 1");
            if($local) $q->where("s.fc_sys_server_only IS NULL OR s.fc_sys_server_only != 1");
        $res = $q->query()->fetchAll();
        $count = count($res);
        for($i=0; $i<$count; $i++) {
            $result[$res[$i]["f_title"]] = $res[$i]["f_description"];
        }
        return $result;
    }
    
    public function getPriorities() {/*perchè selected era commentato?*/
        $result = array();
        $q = new Zend_Db_Select($this->db);
        $res = $q->from(array("c" => "t_creation_date"), array("f_title"))
            //->join(array("w" => "t_wares"), "c.f_id = w.f_code", array("fc_action_priority_selected", "fc_action_priority_color", "fc_action_priority_value", "fc_action_priority_taking_charge"))
                ->join(array("w" => "t_wares"), "c.f_id = w.f_code", array("fc_action_priority_color", "fc_action_priority_value", "fc_action_priority_taking_charge","fc_action_priority_selected"))
            ->where("w.f_type_id = 11")->where("c.f_phase_id = 1")->where("w.fc_action_type = 'priority'")->order("w.fc_action_priority_value")->query()->fetchAll();
        $count = count($res);
        for($i=0; $i<$count; $i++) {
            $result[$res[$i]["fc_action_priority_value"]] = array(
                "label" => Mainsim_Model_Utilities::chg($res[$i]["f_title"]),
                "selected" => ($res[$i]["fc_action_priority_selected"] == 1 ? true : false),
                "color" => Mainsim_Model_Utilities::chg($res[$i]["fc_action_priority_color"]),
                "hours" => Mainsim_Model_Utilities::chg($res[$i]["fc_action_priority_taking_charge"])
            );
        }
        return $result;
    }
    
    public function getEasters() {
        $easters = array();
        for($i=1970; $i<2038; $i++) {
            $easter = easter_date($i); $m = intval(date('m', $easter))-1; $d = date('d', $easter);
            $easters[$i] = "$m-$d";
        }
        return $easters;
    }
}