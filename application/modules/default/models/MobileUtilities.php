<?php
class Mainsim_Model_MobileUtilities extends Mainsim_Model_MobileBase{
    
    public function __construct() {
        parent::__construct();
    }
    
    public static function checkSession($params){
        // check valid session
        try{
            $objMobile = new Mainsim_Model_MobileBase();
            if(!$params['sessionId']){
                $response = $objMobile->createResponseObject(1, 'SESSION_EXPIRED');
            }
            else{
                //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
                $select = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
                $select->from("t_access_registry")
                       ->where("f_user_id = " . $params['userId'])
                       ->where("f_session_id = '" . $params['sessionId'] . "'");
                $res = $objMobile->dbSelect($select);
                if(count($res) > 0){
                    $response = $objMobile->createResponseObject(1, 'SESSION_OK');
                    $userData = Zend_Auth::getInstance()->getIdentity();                             
                    // user coming after browser closure --> recreate session data
                    if(!$userData){
                        $select = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
                        $select->from(array("u" => "t_users"), array("f_code", "fc_usr_usn", "fc_usr_pwd", "fc_usr_firstname", "fc_usr_lastname", "fc_usr_level", "fc_usr_group_level"))
                                //->join(array("cd" => "t_creation_date"), "u.f_code = cd.f_id", array("fc_usr_level", "fc_usr_group_level"))
                                ->where("f_code = '" . $params['userId'] . "'");
                        $res = $objMobile->dbSelect($select);
                        $auth = Zend_Auth::getInstance();
                        $authStorage = $auth->getStorage(); 
                        $authStorage->write($res[0]);
                    }
                }
                else{
                    $response = $objMobile->createResponseObject(1, 'SESSION_EXPIRED');
                }
            }
        } 
        catch(Exception $ex){
            $response = $objMobile->createResponseObject(0, 'ERROR_CHECK_SESSION');
        }
        if($response['status'] == 'SESSION_EXPIRED'){
            print(json_encode($response));
            die();
        }
    }
    
    public static function convertToDesktopParams($mobileParams){
        
        $result = [];
        foreach($mobileParams['data'] as $key => $value){
            if(strpos($key, 'history') === FALSE){
                if($key == 't_wares_parent'){
                    
                }
                else if($key == 't_ware_wo'){
                    
                }
                else{
                    $result = array_merge($result,array_combine($value[0], $value[1]));
                }
            }
        }
        return $result;
    }
    
    public static function isAssociativeArray($array){
        return array_keys($array) !== range(0, count($array) - 1);
    }
    
    public static function getModuleFields($module){
        
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
	$selectUI = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
	$objMobile = new Mainsim_Model_MobileBase();
	// get field to show
	$moduleName = str_replace(array('mdl_', '_tg'), array('', ''), $module);
        $selectUI->from('t_ui_object_instances')
                ->where("f_instance_name LIKE 'mdl_" . $moduleName . "_edit%'")
                ->where("f_properties LIKE '%moModuleComponent%'");
        $res = $objMobile->dbSelect($selectUI);
        $properties = json_decode($res[0]['f_properties']);
        $fields = $properties->fields;
        $auxFields = explode(",", str_replace("|", ",", $fields));	
        $selectUI->reset();
        $selectUI->from("t_ui_object_instances")
                ->where('f_type_id IN (6,7,8,13,14)')
                ->where("f_instance_name IN  ('" . str_replace(array(",", "|") , "','", $fields) .  "')");
        $res2 = $objMobile->dbSelect($selectUI);
        for($i = 0; $i < count($res2); $i++){
            $uis[$res2[$i]['f_instance_name']] = $res2[$i];
        }
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        $lng = Mainsim_Model_MobileUtilities::getLanguageArray($userData['fc_usr_language']);
        
        for($i = 0; $i < count($auxFields); $i++){
            $ui = $uis[$auxFields[$i]];
            $uiProps = json_decode($ui['f_properties']);
            if(!in_array($uiProps->bind, array('fc_task_issue', 'fc_wo_task_order', 'fc_task_quality')) && $uiProps->nomobile != 1){
                $result[Mainsim_Model_MobileUtilities::getFieldTable($uiProps->bind, array('t_creation_date', 't_wares','t_pair_cross','t_custom_fields')). '.' . $uiProps->bind] = $lng[$uiProps->label] ? $lng[$uiProps->label] : $uiProps->label;
            }
        }
        return $result;
    }

    public static function getAdditionalData($params, $fetch = null){
        
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $select = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        
        // virtual table t_task_read_only
        if($params['table'] == 't_task_readonly'){
            
            $fields = Mainsim_Model_MobileUtilities::getModuleFields('mdl_wo_task$pm_tg');
			unset($fields['t_creation_date.f_title']);
			unset($fields['t_custom_fields.fc_task_notes']);
            $select->from('t_custom_fields', array())
                    ->join('t_pair_cross', 't_custom_fields.f_code = t_pair_cross.f_code_cross', $field['list'])
                    ->join('t_wares', 't_custom_fields.f_code = t_wares.f_code', array())
                    ->join('t_creation_date', 't_creation_date.f_id = t_wares.f_code', array());
            if(is_array($params['value'])){
                $select->where($params['field'] . " IN (" . implode(',', $params['value']) . ")");
            }
            else{
                $select->where($params['field'] . " = '" . $params['value'] . "'");
            }
            $auxArray = array_merge(array('t_wares.f_code'),array_keys(Mainsim_Model_MobileUtilities::getModuleFields('mdl_wo_task$pm_tg'))); 
			$select->columns($auxArray);
        }
        // real tables
        else{
            
            $fields = Mainsim_Model_MobileUtilities::getMobileFields($params['table'], unserialize(MOBILE_MODULES));
            if($params['table'] == 't_wares_parent'){
                $fields['list'][] = 't_creation_date.f_nfc_code';
            }
            $select->from($params['table'], $fields['list']);

            if($params['join']){
                $select->join($params['join']['table'], $params['join']['on'], array());
            }

            if($params['join2']){
                $select->join($params['join2']['table'], $params['join2']['on'], array());
            }

            if(is_array($params['value'])){
                $select->where($params['field'] . " IN (" . implode(',', $params['value']) . ")");
            }
            else{
                $select->where($params['field'] . " = '" . $params['value'] . "'");
            }

            if($params['filter']){
                $select->where($params['filter']);
            }

            if($params['order']){
                $select->order($params['order']);
            }
        }
		
        $objMobile = new Mainsim_Model_MobileBase();
        return $objMobile->dbSelect($select, $fetch, false);
    }
    
    public static function getChildren($f_code, $type, $recursive = false){
        switch($type){
            case 'wares':
                $table = 't_wares_parent';
                $field = 'f_parent_code';
                break;
            
            case 'workorders':
                $table = 't_workorders_parent';
                $field = 'f_parent_code';
                break;
        }
        
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $select = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        $select->from($table, array('f_code'))
               ->where($table . '.' . $field . '=' . $f_code)
               ->where('f_active = 1');
        $objMobile = new Mainsim_Model_MobileBase();
        $children = $objMobile->dbSelect($select, Zend_Db::FETCH_NUM);
        //print_r($children);
        $result = array();
        for($i = 0; $i < count($children); $i++){
            $result[] = $children[$i][0] ;
            if($recursive){
                $result = array_merge($result, Mainsim_Model_MobileUtilities::getChildren($children[$i][0], $type, $recursive));
            }
        }
        return $result;
    }
    
    public static function getParents($f_code, $type, $recursive = false){
        switch($type){
            case 'wares':
                $table = 't_wares_parent';
                $field = 'f_code';
                break;
            case 'workorders':
                $table = 't_workorders_parent';
                $field = 'f_code';
                break;
        }
        /*
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $select = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        $select->from($table, array('f_parent_code'))
               ->where($table . '.' . (is_array($f_code) ? $field . ' IN (' . implode(',', $f_code) . ')' : $field . '=' . $f_code))
               ->where('f_parent_code <> 0')
               ->where('f_active = 1');
        $objMobile = new Mainsim_Model_MobileBase($db);
       
        $parents = $objMobile->dbSelect($select, Zend_Db::FETCH_NUM, $debug);
         
        $result = array();
        for($i = 0; $i < count($parents); $i++){
            $result[] = $parents[$i][0] ;
            if($recursive){
                $result = array_merge($result, Mainsim_Model_MobileUtilities::getParents($parents[$i][0], $type, true));
            }
        }
        return array_unique($result);*/
        
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
	$db = Zend_Db::factory(Zend_Registry::get('db'));
        
        
        $objMobile = new Mainsim_Model_MobileBase($db);
        
        if(!is_array($f_code)){
            $aux = $f_code;
            $f_code = array($aux);
        }
        $result = array();
        $select = new Zend_Db_Select($db);

        while(($el = array_shift($f_code)) != null){
            //$el = array_shift($f_code);
            $select->reset();
            $select->distinct()
                    ->from($table, array('f_parent_code'))
                    ->where($table . '.' . $field . '=' . $el)
                    ->where('f_parent_code <> 0')
                    ->where('f_active = 1');
            $res = $objMobile->dbSelect($select);
            for($i = 0; $i < count($res); $i++){
                if(!in_array($res[$i]['f_parent_code'], $f_code) && !in_array($res[$i]['f_parent_code'], $result)){
                    $f_code[] = $res[$i]['f_parent_code'];
                }
            }
            if(!in_array($el, $result)){
                $result[] = $el;
            }
        }
        
        return $result;
    }
    
    public static function getBrothers($f_code, $type){
        
        switch($type){
            case 'wares':
                $table = 't_wares_parent';
                $field = 'f_code';
                break;
            case 'workorders':
                $table = 't_workorders_parent';
                $field = 'f_code';
                break;
        }
       
        $result = array();
        $parents = Mainsim_Model_MobileUtilities::getParents($f_code, $type);
        for($i = 0; $i < count($parents); $i++){
            $result = array_merge($result, Mainsim_Model_MobileUtilities::getChildren($parents[$i], $type));
        }
        
        return array_unique(array_diff($result, array($f_code)));
    }
    
    public static function getRelations($f_code, $type, $field = 'f_code'){

        switch($type){
            case 'wares':
                $table = 't_wares';
                break;
            case 'workorders':
                $table = 't_workorders';
                break;
        }
        
        $parents = Mainsim_Model_MobileUtilities::getParents($f_code, $type, true); 
        $children = Mainsim_Model_MobileUtilities::getChildren($f_code, $type, true);
        $brothers = Mainsim_Model_MobileUtilities::getBrothers($f_code, $type);

        if($field != 'f_code'){
            //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
            $select = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
            $objMobile = new Mainsim_Model_MobileBase();
            // parents
            if(is_array($parents) && count($parents) > 0){
                $select->from($table, array())
                       ->join('t_creation_date', 't_creation_date.f_id = ' . $table . '.f_code', array($field))
                       ->where('t_creation_date.f_id IN (' . implode(',', $parents) . ')');
                
                $res = $objMobile->dbSelect($select, Zend_Db::FETCH_NUM);
                $parents = array();
                for($i = 0; $i < count($res); $i++){
                    $parents[] = $res[$i][0];
                }
            }
            // children
            if(is_array($children) && count($children) > 0){
                $select->reset();
                $select->from($table, array())
                       ->join('t_creation_date', 't_creation_date.f_id = ' . $table . '.f_code', array($field))
                       ->where('t_creation_date.f_id IN (' . implode(',', $children) . ')');
                $res = $objMobile->dbSelect($select, Zend_Db::FETCH_NUM);
                $children = array();
                for($i = 0; $i < count($res); $i++){
                    $children[] = $res[$i][0];
                }
            }
            // brothers
            if(is_array($brothers) && count($brothers) > 0){
                $select->reset();
                $select->from($table, array())
                       ->join('t_creation_date', 't_creation_date.f_id = ' . $table . '.f_code', array($field))
                       ->where('t_creation_date.f_id IN (' . implode(',', $brothers) . ')');
                $res = $objMobile->dbSelect($select, Zend_Db::FETCH_NUM);
                $brothers = array();
                for($i = 0; $i < count($res); $i++){
                    $brothers[] = $res[$i][0];
                }
                
            }
        }

        return array(
            $f_code,
            implode(",", array_unique($parents)),
            implode(",", array_unique($children)),
            implode(",", array_unique($brothers))
        );
    }
    
    public static function getSelectors($f_code, $type){
        
        switch($type){
            case 'workorders':
                $table = 't_selector_wo';
                $table2 = 't_workorders';
                $field = 'f_wo_id';
                break;
            case 'wares':
                $table = 't_selector_ware';
                $table2 = 't_wares';
                $field = 'f_ware_id';
                break;
        }
        
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $select = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        $select->from($table, array('f_selector_id'))
               ->join($table2, $table . '.' . $field . ' = ' . $table2 . '.f_code', array('f_type_id'))
               ->where($table . '.' . $field . '=' . $f_code);
        $objMobile = new Mainsim_Model_MobileBase();
        return $objMobile->dbSelect($select);
    }
    
    public static function getCodeField($table, $type = null){
        
        switch($table){
            
            case 't_creation_date_history':
                $position = 1;
                $name = 'f_id';
                break;
            case 't_custom_fields_history':
                $position = 1;
                $name = 'f_code';
                break;
            case 't_custom_fields':
                $position = 1;
                $name = 'f_code';
                break;
            
           case 't_creation_date':
                $position = 0;
                $name = 'f_id';
                break;
            
           case 't_workorders_history':
               $position = 2;
               $name = 'f_code';
               break;
                
           case 't_workorders':
               $position = 2;
               $name = 'f_code';
               break; 
           
           case 't_workorders_parent':
               $position = 1;
               $name = 'f_code';
               break; 
           
           case 't_ware_wo':
               if($type == 'workorder'){
                    $position = 2;
                    $name = 'f_wo_id';
               }
               else{
                   $position = 1;
                   $name = 'f_ware_id';
               }
               break;
           
           case 't_pair_cross':
               if($type == 'workorder'){
                    $position = 2;
                    $name = 'f_code_main';
               }
               else if($type == 'groupcode'){
                   $position = 5;
                   $name = 'f_code_cross';
               }
               else{
                   $position = 3;
                   $name = 'f_code_cross';
               }
               break; 
           
           case 't_wares':
               $position = 1;
               $name = 'f_code';
               break;
           
           case 't_wares_parent':
               $position = 1;
               $name = 'f_code';
               break;
        }
        
        return array(
            'position' => $position,
            'name' => $name
        );
    }
    
    public static function getFieldTable($field, $tables){
        
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $db = Zend_Db::factory(Zend_Registry::get('db'));
        foreach($tables as $t){
            $fields = $db->describeTable($t);
            if(array_key_exists($field , $fields)){
                return $t;
            }
        }
    }
    
    public static function removeDuplicate($array, $table){
        $codeField = Mainsim_Model_MobileUtilities::getCodeField($table, 'workorder');
        
        if($table == 't_ware_wo'){
           $codeField2 =  Mainsim_Model_MobileUtilities::getCodeField($table, 'ware');
        }
        else if($table == 't_pair_cross'){
            $codeField2 =  Mainsim_Model_MobileUtilities::getCodeField($table, 'ware');
            $codeField3 =  Mainsim_Model_MobileUtilities::getCodeField($table, 'groupcode');
        }
        
        $aux = array();
        $result = array();
        for($i = 0; $i < count($array); $i++){
            if($table == 't_ware_wo'){
                if(!in_array($array[$i][$codeField['position']] . "_" . $array[$i][$codeField2['position']], $aux)){
                    $aux[] = $array[$i][$codeField['position']] . "_" . $array[$i][$codeField2['position']];
                    $result[] = $array[$i];
                }
            }
            else if($table == 't_pair_cross'){
                if(!in_array($array[$i][$codeField['position']] . "_" . $array[$i][$codeField2['position']] . "_" . $array[$i][$codeField3['position']], $aux)){
                    $aux[] = $array[$i][$codeField['position']] . "_" . $array[$i][$codeField2['position']] . "_" . $array[$i][$codeField3['position']];
                    $result[] = $array[$i];
                }
            }
            else{
                if(!in_array($array[$i][$codeField['position']], $aux)){
                    $aux[] = $array[$i][$codeField['position']];
                    $result[] = $array[$i];
                }
            }
        }
        return $result;
    }
    
    public static function createProgress($type, $typeId){
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $select = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        switch($type){
            case 'workorder':
                $table = 't_workorders';
                break;
        }
        $objMobile = new Mainsim_Model_MobileBase();
        $select->from('t_progress_lists',array('max'=>'max(fc_progress)'))
                ->where("f_table = ?", $table)->where("f_type_id = ?", $typeId);
        $res = $objMobile->dbSelect($select);
        $fc = !empty($res[0]['max'])?(int)$res[0]['max']:0;
        $fc++;
        while(true) {
            try {
                $objMobile->dbInsert('t_progress_lists', array(
                    'fc_progress'=> $fc,
                    'f_table'=> $table,
                    'f_type_id'=> $typeId
                ));
            }catch(Exception $e) {
                $fc++;
                continue;
            }
            break;
        }
        return $fc;
    }
    
    public static function getLanguageTranslation($language, $toTranslate){
        
        switch($language){
            case 1:
                $l = 'en_GB';
                break;
            case 2:
                $l = 'it_IT';
                break;
            case 3:
                $l = 'fr_FR';
                break;
            case 4:
                $l = 'es_ES';
                break;
        }
        require APPLICATION_PATH . '/configs/lang/' . $l . '.php';
        $ar = $$l;
        //print_r($toTranslate);

        for($i = 0; $i < count($toTranslate); $i++){
            $translated[$toTranslate[$i]] = $ar[$toTranslate[$i]] ? $ar[$toTranslate[$i]] : $toTranslate[$i];
        }
        //print_r($translated);
        return $translated;
    }
    
    public static function getLanguageArray($language){
        
        switch($language){
            case 1:
                $l = 'en_GB';
                break;
            case 2:
                $l = 'it_IT';
                break;
            case 3:
                $l = 'fr_FR';
                break;
            case 4:
                $l = 'es_ES';
                break;
        }
        require APPLICATION_PATH . '/configs/lang/' . $l . '.php';
        $ar = $$l;
        return $ar;
    }
    
    public static function getMobileFields($table, $modules){
        
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $db = Zend_Db::factory(Zend_Registry::get('db'));
        $objMobile = new Mainsim_Model_MobileBase();
        
        $mobileFields = array();
        foreach($modules as $key => $value){
            $moduleName = str_replace(array('mdl_', '_tg'), array('', ''), $key);
            $selectUI = new Zend_Db_Select($db);
            // get field to show
            $selectUI->from('t_ui_object_instances')
                     ->where("f_instance_name LIKE 'mdl_" . $moduleName . "_edit%'")
                    ->where("f_properties LIKE '%moModuleComponent%'");
            $res = $objMobile->dbSelect($selectUI);
            $properties = json_decode($res[0]['f_properties']);
            $fields = $properties->fields;
            $auxFields = explode(",", str_replace("|", ",", $fields));
            $selectUI->reset();
            $selectUI->from("t_ui_object_instances")
                        ->where('f_type_id IN (6,7,8,13,14)')
                         //->where("(f_instance_name LIKE '%txtfld_" . $moduleName . "_edit%' OR f_instance_name LIKE '%chkbx_" . $moduleName . "_edit%' OR f_instance_name LIKE '%slct_" . $moduleName . "_edit%')");
                        ->where("f_instance_name IN  ('" . str_replace(array(",", "|") , "','", $fields) .  "')");
            $uis = $objMobile->dbSelect($selectUI);
            for($i = 0; $i < count($uis); $i++){
                $ui = $uis[$i];
                $uiProps = json_decode(Mainsim_Model_Utilities::chg($ui['f_properties']));
                if(!in_array($uiProps->bind, array('fc_task_issue', 'fc_wo_task_order')) && $uiProps->nomobile != 1){
                    if(!in_array($uiProps->bind, $mobileFields)){
                        $mobileFields[] = $uiProps->bind;
                    }
                }
            }

			/*
			$selectUI = new Zend_Db_Select($db);
            $selectUI->from("t_ui_object_instances")
                         ->where('f_type_id IN (6,7,8,13,14)')
                         ->where("(f_instance_name LIKE '%txtfld_" . $moduleName . "_edit%' OR f_instance_name LIKE '%chkbx_" . $moduleName . "_edit%' OR f_instance_name LIKE '%slct_" . $moduleName . "_edit%')");
            $uis = $objMobile->dbSelect($selectUI);
            for($i = 0; $i < count($uis); $i++){
                $ui = $uis[$i];
                $uiProps = json_decode($ui['f_properties']);
                
                if(!in_array($uiProps->bind, array('fc_task_issue', 'fc_wo_task_order')) && $uiProps->nomobile != 1){
                    if(!in_array($uiProps->bind, $mobileFields)){
                        $mobileFields[] = $uiProps->bind;
                    }
                }
            }*/
        }
        $dbFields = $db->describeTable($table);
        foreach($dbFields as $key => $value){
            if(strpos($key, 'f_') !== FALSE || in_array($key, $mobileFields) || in_array($key, array('fc_doc_attach','fc_progress', 'fc_task_issue_not_performed', 'fc_task_issue_performed', 'fc_task_issue_not_executable', 'fc_task_quality_good', 'fc_task_quality_acceptable', 'fc_task_quality_not_acceptable', 'fc_usr_firstname', 'fc_usr_lastname', 'fc_usr_usn'))){
                $resultFields['data'][] = $value;
                $resultFields['list'][] = $key;
            }
        }
        return $resultFields;
    }
    
    public static function createStubData($type, $category, $data = null){
        //$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        $db = Zend_Db::factory(Zend_Registry::get('db'));
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        $timestamp = time();
        if($type == 'ware'){
            
            if($category == 'document'){
                $tables = array(
                    't_creation_date' => array(
                        'f_type' => 'WARES',
                        'f_category' => 'DOCUMENT',
                        'f_order' => 7,
                        'f_wf_id' => 9,
                        'f_phase_id' => 1,
                        'f_visibility' => -1,
                        'f_editability' => -1
                    ),
                    't_wares' => array(
                        'f_type_id' => 5,
                    ),
                    't_custom_fields' => array(),
                    't_wares_parent' => array()
                );
            }
            
            $tables['t_wares']['f_order'] = 1;
            $tables['t_wares']['f_timestamp'] = $timestamp;
            $tables['t_wares']['f_user_id'] = $userData['f_code'];
            $tables['t_custom_fields']['f_timestamp'] = $timestamp;
            $tables['t_wares_parent']['f_parent_code'] = 0;
            $tables['t_wares_parent']['f_active'] = 1;
            $tables['t_wares_parent']['f_timestamp'] = $timestamp;
        }
        else if($type == 'workorder'){
            if($category == 'corrective task'){
                $tables = array(
                    't_creation_date' => array(
                        'f_type' => 'WORKORDERS',
                        'f_category' => 'CORRECTIVE TASK',
                        'f_order' => 0,
                        'f_wf_id' => 9,
                        'f_phase_id' => 1,
                        'f_visibility' => -1,
                        'f_editability' => -1
                    ),
                    't_workorders' => array(
                        'f_type_id' => 10
                    ),
                    't_custom_fields' => array(),
                    't_wares_parent' => array()
                );
            }
            
            $tables['t_workorders']['f_order'] = 0;
            $tables['t_workorders']['f_timestamp'] = $timestamp;
            $tables['t_workorders']['f_user_id'] = $userData['f_code'];
            $tables['t_custom_fields']['f_timestamp'] = $timestamp;
            $tables['t_workorders_parent']['f_parent_code'] = 0;
            $tables['t_workorders_parent']['f_active'] = 1;
            $tables['t_workorders_parent']['f_timestamp'] = $timestamp;
            $tables['t_ware_wo'] = null;
        }
        
        
        // common fields
        $tables['t_creation_date']['f_creation_date'] = $timestamp;
        $tables['t_creation_date']['f_creation_user'] = $userData['f_code'];
        $tables['t_creation_date']['f_title'] = $category;

        foreach($tables as $table => $value){
            $tableDescription = $db->describeTable($table);
            $fields = null;
            $values = null;
            foreach($tableDescription as $field => $value2){
                $fields[] = $field;
                if(isset($data[$table][$field])){
                    $values[] = $data[$table][$field];
                }
                else if(isset($value[$field])){
                    $values[] = $value[$field]; 
                }
                else{
                    $values[] = $value2['default'];
                }
            }
            $result[$table][0] = $fields;
            $result[$table][1] = $values;
        }
        return $result;
    }
}

