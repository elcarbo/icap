<?php
class ExcelStreamer{
    
    private $handleFileData;
    private $handleWorkSheet;
    private $handleContextType;
    private $handleStyles;
    private $handleApp;
    private $handleRels;
    private $sheetCounter;
    private $outputFile;
    private $modelsPath; 
    private $tempPath;
    private $type;
    private $tempDir;
    
    private $stylesXLSX;
    private $mergedXLSX;
    private $sheetsXLSX;
    
    private $handleXML;
    private $sharedStringsXLSX;
    
    private $maxRow;
    private $maxColumn;
	
	private $encoding = 'iso-8859-1';
    
    public function __construct(){
       $this->modelsPath = APPLICATION_PATH . '/../export/models';
       $this->tempPath = APPLICATION_PATH . '/../export/temp';
    }
    
    public function createXLS($filename){
        
        $this->outputFile = $filename;

        // open file e get handler
        try{
            $this->handleFileData= fopen($this->tempPath . '/' . $filename, 'a');
            $this->sheetCounter = 0;
        }
        catch(Exception $e){
            print($e->getMessage());
            die();
        }
        // write file header
        $this->writeHeaderXLS();
    }
    
    public function createXLSX($filename){
        
        $this->outputFile = $filename;
        // create XLSX file structure
        $this->tempDir = $this->tempPath . '/' . $filename;
        mkdir($this->tempDir);
    }
    
    public function closeSheetXLSX(){
        
        if(count($this->mergedXLSX) > 0){
            $mergedXML = "<mergeCells count=\"" . count($this->mergedXLSX) . "\">\n";
            for($i = 0; $i < count($this->mergedXLSX); $i++){
                $mergedXML .= "\t<mergeCell ref=\"" . $this->mergedXLSX[$i] . "\"/>\n";
            }
            $mergedXML .= "</mergeCells>\n";
            $this->mergedXLSX = null;
        }

        fwrite($this->handleWorkSheet, '</sheetData>' . $mergedXML . $columnXML . '</worksheet>');
        fclose($this->handleWorkSheet);
    }
    
    public function addSheetXLSX($columns = null, $name = null){
        // increment sheet counter
        $this->sheetCounter++;
        
        if($this->sheetCounter > 1){
            // finalize current worksheet
            $this->closeSheetXLSX();
        }
        
        if(count($columns) > 0){
            $columnXML = "<cols>\n";
            for($i = 0; $i < count($columns); $i++){
                $columnXML .= "\t<col customWidth=\"1\" width=\"" . $columns[$i]['width'] . "\" max=\"" . ( $i + 1). "\" min=\"" . ($i + 1) . "\"/>\n";
            }
            $columnXML .= "</cols>\n";
        }
        
        // save current sheet information before create a new one (to finalize workbook.xml.rels and workbook.xml
        $this->sheetsXLSX[] = array(
            'id' => $this->sheetCounter,
            'name' => ($name ? $name : 'sheet ' . $this->sheetCounter),
            'file' => 'sheet' . $this->sheetCounter . ".xml" 
        );
        // create new worksheet file
        $this->handleWorkSheet = fopen($this->tempDir . '/sheet' . $this->sheetCounter . '.xml', 'w');
        fwrite($this->handleWorkSheet, '<?xml version="1.0" encoding="' . $this->encoding . '"?>' .
            '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">' .
            '<dimension ref="A1:L100" />' .
            '<sheetViews><sheetView tabSelected="1" workbookViewId="0"/></sheetViews>' .
            $columnXML .
            "<sheetData>\n");
    }
    
    public function closeXLSX(){  
        
        // finalize current worksheet
        $this->closeSheetXLSX();

        // write workbook.xml
        $stringXML = null;
        for($i = 0; $i < count($this->sheetsXLSX); $i++){
            $stringXML .= '<sheet r:id="rId' . $this->sheetsXLSX[$i]['id'] . '" sheetId="' . $this->sheetsXLSX[$i]['id'] . '" name="' . $this->sheetsXLSX[$i]['name'] . '" />';
        } 
        $handle = fopen($this->tempDir . '/workbook.xml', 'w');
        fwrite($handle, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
            '<workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">' .
            '<fileVersion rupBuild="9303" lowestEdited="5" lastEdited="5" appName="xl"/>' . 
            '<workbookPr defaultThemeVersion="124226"/>' . 
            '<bookViews><workbookView windowHeight="6855" windowWidth="14055" yWindow="600" xWindow="270"/></bookViews>' .
            '<sheets>' . $stringXML . '</sheets>' .
            '<calcPr calcId="0"/>' . 
            '<fileRecoveryPr repairLoad="1"/>' .
            '</workbook>');
        fclose($handle);

        // write workbook.xml.rels
        $stringXML = null;
        for($i = 0; $i < count($this->sheetsXLSX); $i++){
            $stringXML .= '<Relationship Id="rId' . $this->sheetsXLSX[$i]['id'] . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" Target="worksheets/' . $this->sheetsXLSX[$i]['file'] . '"/>';
        } 
        $relId = count($this->sheetsXLSX);
        $handle = fopen($this->tempDir . '/workbook.xml.rels', 'w');
        fwrite($handle, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
            '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">' .
                $stringXML . 
                '<Relationship Id="rId' . ++$relId . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" Target="theme/theme1.xml"/>' .
                '<Relationship Id="rId' . ++$relId . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>' .
            '</Relationships>');
        fclose($handle);
        
        // write [Content_Types].xml
        $stringXML = null;
        for($i = 0; $i < count($this->sheetsXLSX); $i++){
            $stringXML .= '<Override ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml" PartName="/xl/worksheets/' . $this->sheetsXLSX[$i]['file'] . '"/>';
        } 
        $handle = fopen($this->tempDir . '/[Content_Types].xml', 'w');
        fwrite($handle, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . 
            '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">' .
                '<Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>' .
                '<Default Extension="xml" ContentType="application/xml"/>' .
                '<Override PartName="/xl/workbook.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"/>' . 
                '<Override PartName="/xl/theme/theme1.xml" ContentType="application/vnd.openxmlformats-officedocument.theme+xml"/>' .
                '<Override PartName="/xl/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml"/>' .
                '<Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml"/>' .
                '<Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml"/>' .
                $stringXML .
            '</Types>');
        fclose($handle);
        
        // write app.xml
        $stringXML = null;
        for($i = 0; $i < count($this->sheetsXLSX); $i++){
            $stringXML .= '<vt:lpstr>' . $this->sheetsXLSX[$i]['name'] . '</vt:lpstr>';
        } 
        $handle = fopen($this->tempDir . '/app.xml', 'w');
        fwrite($handle, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
            '<Properties xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes" xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties">' .
            '<Application>Microsoft Excel</Application>' .
            '<DocSecurity>0</DocSecurity>' . 
            '<ScaleCrop>false</ScaleCrop>' .
            '<HeadingPairs>' .
                '<vt:vector baseType="variant" size="2">' .
                    '<vt:variant><vt:lpstr>Fogli di lavoro</vt:lpstr></vt:variant>' .
                    '<vt:variant><vt:i4>' . count($this->sheetsXLSX) . '</vt:i4></vt:variant>' .
                '</vt:vector>' .
            '</HeadingPairs>' . 
            '<TitlesOfParts>' .
                '<vt:vector baseType="lpstr" size="' . count($this->sheetsXLSX) . '">' .
                    $stringXML . 
                '</vt:vector>' .
            '</TitlesOfParts>' .
            '<Company>Unplugged</Company>' .
            '<LinksUpToDate>false</LinksUpToDate>' . 
            '<SharedDoc>false</SharedDoc>' . 
            '<HyperlinksChanged>false</HyperlinksChanged>' . 
            '<AppVersion>14.0300</AppVersion>' . 
            '</Properties>');
        fclose($handle);
   
        // create zip from model
        $this->zipXLSX();
        // remove temp dir
        $this->removeDir($this->tempDir);
    }
    
    
    public function writeHeaderXLS(){
        
        $header = "<?xml version='1.0'?>\n" . 
                   "<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet'\n" .
                     "xmlns:o='urn:schemas-microsoft-com:office:office'\n" .
                     "xmlns:x='urn:schemas-microsoft-com:office:excel'\n" .
                     "xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet'\n" .
                     "xmlns:html='http://www.w3.org/TR/REC-html40'>\n";
        fwrite($this->handleFileData, $header);
        //print($header);
    }
    
    public function addStylesXLS($styles){
        fwrite($this->handleFileData, "<Styles>\n");
        foreach($styles as $key => $value){
           $this->writeStyleXLS($value, $key);
        }
        fwrite($this->handleFileData, "</Styles>\n");
        //print("</Styles>\n");
    }
    
    public function addStylesXLSX($styles){
        foreach($styles as $key => $value){
            $this->addStyleXLSX($value, $key);
        }
        // Replico ogni stile per il tipo data
        
        foreach($styles as $key => $value){
            $this->addStyleXLSX($value, $key."-date");
        }

        // write style xml
        $this->handleStyles = fopen($this->tempDir . '/styles.xml', 'w');
        fwrite($this->handleStyles, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
                "<styleSheet xmlns:x14ac=\"http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac\" mc:Ignorable=\"x14ac\"\n" .
                "xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\"\n" .  
                "xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">\n");
        
        // NumFmt
        $dateXML = "<numFmts count=\"1\">\n";
        $dateXML.= "\t<numFmt numFmtId=\"170\" formatCode=\"d/m/yy\ h:mm;@\"/>\n";
        $dateXML.= "</numFmts>\n";
        fwrite($this->handleStyles, $dateXML);
        //die(var_dump($this->handleStyles, $dateXML));

        // fonts
        if($this->stylesXLSX['fonts']){
            $fontXML = "<fonts count=\"" . count($this->stylesXLSX['fonts']) . "\">\n";
            for($i = 0; $i < count($this->stylesXLSX['fonts']); $i++){
                $fontXML .= "\t<font>\n";
                    if($this->stylesXLSX['fonts'][$i]['b']){
                        $fontXML .= "\t\t<b />\n";
                    }
                    if($this->stylesXLSX['fonts'][$i]['sz val']){
                        $fontXML .= "\t\t<sz val=\"" . $this->stylesXLSX['fonts'][$i]['sz val'] . "\" />\n";
                    }
                    if($this->stylesXLSX['fonts'][$i]['color rgb']){
                        $fontXML .= "\t\t<color rgb=\"" . $this->stylesXLSX['fonts'][$i]['color rgb'] . "\" />\n";
                    }
                    //$fontXML .= "<color theme=\"1\"/><name val=\"Calibri\"/><family val=\"2\"/><scheme val=\"minor\"/>\n";
                    $fontXML .= "<name val=\"Calibri\"/><family val=\"2\"/><scheme val=\"minor\"/>\n";
                    $fontXML .= "\t</font>\n";
            }
            $fontXML .= "</fonts>\n";
            fwrite($this->handleStyles, $fontXML);
        }
        // fills
        if($this->stylesXLSX['fills']){
            $fillXML = "<fills count=\"" . (count($this->stylesXLSX['fills']) + 2) . "\">\n";
            $fillXML .= "\t<fill>\n" . 
                       "\t\t<patternFill patternType=\"none\"/>\n" . 
                       "\t</fill>\n" .
                       "\t<fill>\n" .
                       "\t\t<patternFill patternType=\"gray125\"/>\n" .
                       "\t</fill>\n";
            for($i = 0; $i < count($this->stylesXLSX['fills']); $i++){
                $fillXML .= "\t<fill>\n" .
                            "\t\t<patternFill patternType=\"solid\">\n" .
                            "\t\t\t<fgColor rgb=\"FF" . $this->stylesXLSX['fills'][$i] . "\"/>\n" .
                            "\t\t\t<bgColor indexed=\"64\" />" .
                            "\t\t</patternFill>\n" .
                            "\t</fill>\n";
            }
            $fillXML .= "</fills>\n";
            fwrite($this->handleStyles, $fillXML);
        }
        
        // borders
        $borderXML = "<borders count=\"2\">\n";
        $borderXML .= "\t<border>\n" .
                      "\t\t<left/>\n" .
                      "\t\t<right/>\n" .
                      "\t\t<top/>\n" .
                      "\t\t<bottom/>\n" .
                      "\t\t<diagonal/>\n" .
                      "\t</border>\n";
        $borderXML .= "\t<border>\n" .
                      "\t\t<left style=\"thin\">\n" .
                      "\t\t\t<color indexed=\"64\" />\n" .
                      "\t\t</left>\n" .
                      "\t\t<right style=\"thin\">\n" .
                      "\t\t\t<color indexed=\"64\" />\n".
                      "\t\t</right>\n" .
                      "\t\t<top style=\"thin\">\n" .
                      "\t\t\t<color indexed=\"64\" />\n".
                      "\t\t</top>\n" .
                      "\t\t<bottom style=\"thin\">\n" .
                      "\t\t\t<color indexed=\"64\" />\n" .
                      "\t\t</bottom>\n" .
                      "\t</border>\n";
        $borderXML .= "</borders>\n";
        fwrite($this->handleStyles, $borderXML); 
        
        // cell
        if($this->stylesXLSX['map']){
           $cellXML = "<cellXfs count=\"" . (count($this->stylesXLSX['map']) + 1) . "\">\n";
                  $cellXML .= "\t<xf borderId=\"0\" fillId=\"0\" fontId=\"0\" numFmtId=\"0\" xfId=\"0\"/>\n";          
            foreach($this->stylesXLSX['map'] as $key => $value){
                $cellXML .= "\t<xf";
                $cellXML .= " fontId=\"" . $value['fontId'] . "\"";
                $cellXML .= " fillId=\"" . ($value['fillId'] + 2) . "\"";

               $cellXML .= " borderId=\"1\"";
                if(strpos($key, "-date") !== false) {
                    $cellXML .= " applyBorder=\"1\" applyAlignment=\"1\" applyFont=\"1\" numFmtId=\"170\" xfId=\"0\" applyNumberFormat=\"1\"";
                } else {
                    $cellXML .= " applyBorder=\"1\" applyAlignment=\"1\" applyFont=\"1\" numFmtId=\"0\" xfId=\"0\"";
                }
                //$cellXML .= " numFmtId=\"" . $i . "\" xfId=\"" . $i . "\"";
                $cellXML .= ">\n";
				$cellXML .= "\t<alignment " . ($value['alignmentWrapText'] ? " wrapText=\"" . $value['alignmentWrapText'] . "\"" : " ") .  ($value['alignmentVertical'] ? " vertical=\"" . $value['alignmentVertical'] . "\"" : " ") . ($value['alignmentRotation'] ? " textRotation=\"" . $value['alignmentRotation'] . "\"" : " ") . "/>\n";
				$cellXML .= "</xf>";
            }
            $cellXML .= "</cellXfs>\n";
            $cellXML .= "<cellStyles count=\"1\">\n" .
                        "\t<cellStyle xfId=\"0\" builtinId=\"0\" name=\"Normale\"/>\n" .
                        "</cellStyles>";
            fwrite($this->handleStyles, $cellXML);
        }
        fwrite($this->handleStyles, "</styleSheet>\n");
        fclose($this->handleStyles);
    }
    
    public function addStyleXLSX($style, $name){   
        foreach($style as $key => $value){
            switch($key){
                // font
                case 'font':
                    $aux = array();
                    // bold
                    if($value['bold']){
                        $aux['b'] = true;
                    }
                    // size
                    if($value['size']){
                        $aux['sz val'] = $value['size'];
                    }
                    // color
                    if($value['color']){
                        $aux['color rgb'] = 'FF' . $value['color'];
                    }
                    $this->stylesXLSX['fonts'][] = $aux;
                    $this->stylesXLSX['map'][$name]['fontId'] = count($this->stylesXLSX['fonts']) - 1;
                    break;
                // interior
                case 'interior':
                    // color
                    if($value['color']){
                        $this->stylesXLSX['fills'][] = $value['color'];
                        $this->stylesXLSX['map'][$name]['fillId'] = count($this->stylesXLSX['fills']) - 1;
                    }
                    break;
                // borders
                case 'borders':
                    $aux = array();
                    $aux['left style'] = 'thin';
                    $aux['right style'] = 'thin';
                    $aux['top style'] = 'thin';
                    $aux['bottom style'] = 'thin';
                    $this->stylesXLSX['borders'][] = $aux;
                    $this->stylesXLSX['map'][$name]['borderId'] = count($this->stylesXLSX['borders']) - 1; 
                    break;
				// height 
				case 'height':
					$this->stylesXLSX['map'][$name]['height'] = $value['size'];
					break;
                // text
				case 'alignment':
                    if($value['rotation']){
                        $this->stylesXLSX['map'][$name]['alignmentRotation'] = $value['rotation'];
                    }
                    if($value['vertical']){
                        $this->stylesXLSX['map'][$name]['alignmentVertical'] = $value['vertical'];
                    }
                    if($value['wrapText']){
                        $this->stylesXLSX['map'][$name]['alignmentWrapText'] = $value['wrapText'];
                    }
					break;
            }
        }

    }
    
    public function writeStyleXLS($style, $name){
        $data ="\t<Style ss:ID='" . $name . "'>\n";
        foreach($style as $key => $value){
            switch($key){
                // font
                case 'font':
                    $data .= "\t\t<Font ";
                    // bold
                    if($value['bold']){
                        $data .= "ss:Bold='1' ";
                    }
                    // size
                    if($value['size']){
                        $data .= "ss:Size='" . $value['size'] . "' ";
                    }
                    $data .= "/>\n";
                    break;
                // interior
                case 'interior':
                    $data .="\t\t<Interior ";
                    // color
                    if($value['color']){
                        $data .= "ss:Color='#" . $value['color'] . "' ss:Pattern='Solid'";
                    }
                    $data .= "/>\n";
                    break;
                // borders
                case 'borders':
                    $data .="\t\t<Borders>\n";
                    $data .= "\t\t\t<Border ss:Position='Bottom' ss:LineStyle='Continuous' ss:Weight='1'/>\n";
                    $data .= "\t\t\t<Border ss:Position='Left' ss:LineStyle='Continuous' ss:Weight='1'/>\n";
                    $data .= "\t\t\t<Border ss:Position='Right' ss:LineStyle='Continuous' ss:Weight='1'/>\n";
                    $data .= "\t\t\t<Border ss:Position='Top' ss:LineStyle='Continuous' ss:Weight='1'/>\n";
                    $data .= "</Borders>";
                    break;
                // date format
                case 's22':
                    $data .= "\t\t<NumberFormat ss:Format='" . $value['format'] . "' />\n"; 
                    break;
            }
            
        }
        $data .= "\t</Style>\n";
        fwrite($this->handleFileData, $data);
        //print($data);
    }
    
    
    
    public function writeRowXLS($rowData, $rowOffset, $styleID = null){
		/*
        if($height){
            $heightAttr = "ss:Height='" . $height . "' ";
        }*/
		
		//($this->stylesXLSX['map'][$style]['height'] ? " customHeight=\"1\" ht=\"" . $this->stylesXLSX['map'][$style]['height'] . "\"" : "")
        
        if($styleID){
            $styleAttr = "ss:StyleID='" . $styleID . "' ";
        }
        else{
            $styleAttr = '';
        }
 
		for($i = 0; $i < count($rowData); $i++){
			$row .= $this->writeCellXLS($rowData[$i][0], $rowData[$i][1], $rowOffset, $columnOffset, $style, $rowData[$i][5], $rowData[$i][6]);
		}

		fwrite($this->handleFileData, "<Row " . $styleAttr . $heightAttr . ">" . $row . "</Row>\n");
    }
    
    public function writeRowXLSX($rowData, $rowOffset, $styleRow = null){
        /*
        if($styleRow){
            $styleRowId = array_search($style,array_keys($this->stylesXLSX['map']));
            $styleRow = $style;
        }*/
        for($i = 0; $i < count($rowData); $i++){
            
			if($rowData[$i][3]){
                $columnOffset = $rowData[$i][3];
            }
            else{
                $columnOffset = $i;
            }
            
			
            if($rowData[$i][2]){
                $rowOffset = $rowData[$i][2];
            }
			
			if($rowData[$i][3]){
				$columnOffset = $rowData[$i][3];
			}
            
            if($styleRow){
                $style = $styleRow;
            }
            // custom style for cell
            if($rowData[$i][4]){
                $style = $rowData[$i][4];
            }
            elseif(!$style){
                $style = 'data';
            }
            
            // set height row
            if($this->stylesXLSX['map'][$style]['height']){
                $height = " ht=\"" . $this->stylesXLSX['map'][$style]['height'] . "\" ";
            }
            
            $row .= $this->writeCellXLSX($rowData[$i][0], $rowData[$i][1], $rowOffset, $columnOffset, $style, $rowData[$i][5], $rowData[$i][6]);
            $style = null;
            
        }
        //fwrite($this->handleWorkSheet, "<row r=\"" . $rowOffset . "\"" . ($styleId || "$styleId" === '0' ? " s=\"" . $styleId . "\"" : "") . ($this->stylesXLSX['map'][$style]['height'] ? " customHeight=\"1\" ht=\"" . $this->stylesXLSX['map'][$style]['height'] . "\"" : "") . ">" . $row . "</row>\n");
        fwrite($this->handleWorkSheet, "<row r=\"" . $rowOffset . "\"" . $height  .  ">" . $row . "</row>\n");
    }
	
	public function writeRowsXLS($data, $rowOffset, $style){
		for($i = 0; $i < count($data); $i++){
            $this->writeRowXLS($data[$i], $rowOffset, $style);
			$rowOffset++;
        }
        $this->maxRow += count($data);
    }
    
    public function writeRowsXLSX($data, $rowOffset, $style){
		for($i = 0; $i < count($data); $i++){
            $this->writeRowXLSX($data[$i], $rowOffset, $style);
			$rowOffset++;
        }
        $this->maxRow += count($data);
    }
    
    public function getExcelColumn($index){
        $numeric = $index % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($index / 26);
        if ($num2 > 0) {
            return $this->getExcelColumn($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
    
    public function getExcelIndex($column){
        
        $match = array();
        preg_match("/^[a-zA-Z]+/", $column, $match);
        $column = $match[0];
        $sum = 0;
        $aux = str_split($column);
        for($i = 0; $i < count($aux); $i++)
        {
            $sum *= 26;
            $sum += (ord($aux[$i]) - ord('A') + 1);
        }
        return $sum;
    }
    
    public function writeCellXLSX($value = '', $type = 'inLineStr', $rowOffset, $columnOffset, $style = null, $mergeHorizontal = null, $mergeVertical = null){
		$excelColumnOffset = $this->getExcelColumn($columnOffset);
        if($style){
            $styleId = array_search($style,array_keys($this->stylesXLSX['map'])) + 1;
        }
        if($mergeHorizontal && !$mergeVertical){
            $this->mergedXLSX[] = $excelColumnOffset . $rowOffset . ':' . $this->getExcelColumn($columnOffset + $mergeHorizontal) . $rowOffset;
        }
        
        if($type == 'inlineStr' && $value){
          # $value = "<is><t>" .html_entity_decode(htmlspecialchars($value)).'</is><¸/t>';
      
           $value = "<is><t>" . str_replace(array("&", "'", "<", ">", "\"", "Ã", "€", '°', 'É', 'á', 'é', 'ú'), array("&amp;", "&apos;", "&lt;", "&gt;", "&quot;", "&Aacute;", "&#8364;", "&deg;", '&Eacute;', '&aacute;', '&eacute;', '&uacute;'), preg_replace('/(\|\)/', '\'', $value)) . "</t></is>";
        }
        else if($value || $value === '0'){
            $value = "<v>" . $value . "</v>";
        }
        if ($type == 'date') {
            $type = "n";
            $styleId = array_search($style."-date",array_keys($this->stylesXLSX['map'])) + 1;
            //$value = "<v>" . $value . "</v>";
            //die(var_dump("<c r='" . $excelColumnOffset . $rowOffset . "'" . ($styleId || "$styleId" === '0' ? " s=\"" . $styleId . "\" " : "") .  ($type ? " t='" . $type . "'" : "") . ">" . $value . "</c>"));
        }
        return "<c r='" . $excelColumnOffset . $rowOffset . "'" . ($styleId || "$styleId" === '0' ? " s=\"" . $styleId . "\" " : "") .  ($type ? " t='" . $type . "'" : "") . ">" . $value . "</c>";
    }
    
    public function writeCellXLS($value = '', $type = 'String', $rowOffset, $columnOffset, $style = null, $mergeHorizontal = null, $mergeVertical = null){
		if($mergeHorizontal && !$mergeVertical){
            $mergeAcrossAttr = "ss:MergeAcross='" . $mergeHorizontal . "' ";
        }
        
        if($index){
            $indexAttr = "ss:Index='" . $index . "' ";
        }
        /*
        if($style){
            $styleAttr = "ss:StyleID='" . $style . "' ";
            print_r($styleAttr);
        }*/
		
		if(!$type){
			$type = 'String';
		}
        
        if($type == 'String'){
			$value = str_replace(array("&", "'", "<", ">"), array("&amp;", "&apos;", "&lt;", "&gt;"), $value);
		}

        return "<Cell " . $mergeAcrossAttr . $indexAttr . $styleAttr . "><Data ss:Type='" . $type . "'>" . utf8_encode($value) . "</Data></Cell>";
    }
    
    public function closeXLS(){
        fwrite($this->handleFileData, "</Table></Worksheet></Workbook>");
        //print("</Table></Worksheet></Workbook>");
        fclose($this->handleFileData);
    }
    
    public function addSheetXLS($title = null, $styleID = null, $autoWidthColumns = null){
        
        if($styleID){
            $styleAttr = "ss:StyleID='" . $styleID . "' ";
        }
        else{
            $styleAttr = '';
        }
        
        if($autoWidthColumns){
            $autoWidthAttr = "<Column ss:AutoFitWidth='1' ss:Width='100' ss:Span='" . $autoWidthColumns . "'/>\n";
        }
        else{
            $autoWidthAttr = null;
        }
        
        // close last sheet opened before create new one
        if($this->sheetCounter > 0){
            fwrite($this->handleFileData, "</Table></Worksheet><Worksheet ss:Name='" . $title . "' ><Table " . $styleAttr . ">" . $autoWidthAttr);
            //print("</Table></Worksheet><Worksheet ss:Name='" . $title . "' ><Table " . $styleAttr . ">" . $autoWidthAttr);
        }
        // first sheet 
        else{
            fwrite($this->handleFileData, "<Worksheet ss:Name='" . $title . "' ><Table " . $styleAttr . ">" . $autoWidthAttr);
            //print("<Worksheet ss:Name='" . $title . "' ><Table " . $styleAttr . ">" . $autoWidthAttr);
        }
        $this->sheetCounter++;
    }
    
    public function setColumnsXLS($columns){
        for($i = 0; $i < count($columns); $i++){
            $data .= "<Column ";
            if($columns[$i]['width']){
                $data .= " ss:Width='" . $columns[$i]['width'] . "'";
            }
            $data .= " />";
        }
        fwrite($this->handleFileData, $data);
        //print($data);
    }

    public function zipXLSX($filename = null){
        if($filename){
            $this->outputFile = $filename;
        }
        copy(APPLICATION_PATH . '/../export/models/model.xlsx', $this->tempPath . '/' . $this->outputFile . '.xlsx');
        $zip = new ZipArchive();
        $zip->open($this->tempPath . '/' . $this->outputFile . '.xlsx');
        $zip->addEmptyDir('xl/worksheets');
        for($i = 0; $i < count($this->sheetsXLSX); $i++){
            $zip->addFile($this->tempDir . '/' . $this->sheetsXLSX[$i]['file'], 'xl/worksheets/'  . $this->sheetsXLSX[$i]['file']);
        }
        
        $zip->addFile($this->tempDir . '/styles.xml', 'xl/styles.xml');
        $zip->addFile($this->tempDir . '/workbook.xml', 'xl/workbook.xml');
        $zip->addFile($this->tempDir . '/workbook.xml.rels', 'xl/_rels/workbook.xml.rels');
        $zip->addFile($this->tempDir . '/[Content_Types].xml', '[Content_Types].xml');
        $zip->addFile($this->tempDir . '/app.xml', 'docProps/app.xml');
        $zip->close();
    }
    
    public function unzipXLSX(){
        
        try{
            $zip = new ZipArchive();
            $zip->open($this->inputFile);
            $zip->extractTo('temp/' . $this->tempDir);
            $zip->close();
        }
        catch(Exception $e){
            print($e->getMessage());
        }
    }
    
    public function openXLSX($file){
        $this->inputFile = $file;
        $this->tempDir = time(); 
        $this->unzipXLSX();
        
        // store strings xml
        $this->getSharedStringsXLSX(); 
    }
    
    public function getSharedStringsXLSX(){
        $this->handleXML = new XMLReader();
        $this->handleXML->open('temp/' . $this->tempDir . '/xl/sharedStrings.xml');
        
        while ($this->handleXML->read()) { 
            if($this->handleXML->name == "t"){
                $this->handleXML->read();
                $this->sharedStringsXLSX[] = $this->handleXML->value;
                $this->handleXML->read();
            }
            else if($this->handleXML->name == 'r'){
                $text = null;
                while($this->handleXML->read()){
                    if($this->handleXML->name == 'si'){
                        $this->sharedStringsXLSX[] = $text;
                        break;
                    }
                    elseif($this->handleXML->name == 't'){
                        $this->handleXML->read();
                        $text .= $this->handleXML->value;
                    }
                }
            }
        }
        
    }
    
    public function openSheetXLSX($sheetName){
        $this->handleXML = new XMLReader();
        $this->handleXML->open('temp/' . $this->tempDir . '/xl/workbook.xml');
        
        while ($this->handleXML->read()) { 
            if($this->handleXML->getAttribute('name') == $sheetName){
                //$sheetId = substr($this->handleXML->getAttribute('r:id'), 3);
                $sheetId = $this->handleXML->getAttribute('sheetId');
                break;
            }
        }
        $this->handleXML->close();
        $this->handleXML->open('temp/' . $this->tempDir . '/xl/workSheets/sheet' . $sheetId . '.xml');
        // point hanlde  to first data node
        while($this->handleXML->read()){
            if($this->handleXML->name == 'sheetData'){
                break;
            }
        } 
    }
    
    public function closeSheetXML(){
        $this->handleXML->close();
    }
    
    public function readRowXLSX($map = null){
        $tagCounter = 0;
        $read = true;
        $columnCounter = 0;
        $data = array();
        $tag = null;
        $tagData = false;
        $index = null;
        while(true){
                
            $this->handleXML->read();
            $prevTag = $tag;
            $tag = $this->handleXML->name;
            
            if($tag == 'sheetData'){
                return null;
            }
            else if($tag == 'row'){
                $tagCounter++;
                if($tagCounter == 2){
                    break;
                }
            }
            else if($tag == 'c'){
                if($index){
                    if($map){
                        $data[$map[$index]] = null;
                    }
                    else{
                        $data[$index] = null;
                    }
                    
                }
                $index = $this->getExcelIndex($this->handleXML->getAttribute('r'));
                $type = $this->handleXML->getAttribute('t');
            }
            else if($tag == 'v'){
                $this->handleXML->read();
                    if($type == 's'){
                        if($map){ 
                            $data[$map[$index]] = $this->sharedStringsXLSX[$this->handleXML->value];
                        }
                        else{
                            $data[$index] = $this->sharedStringsXLSX[$this->handleXML->value];
                        }
                    }
                    else{
                        if($map){
                            $data[$map[$index]] = $this->handleXML->value;
                        }
                        else{
                            $data[$index] = $this->handleXML->value;
                        }
                    }
                $this->handleXML->read();
                $this->handleXML->read();
                $index = null;
            }
            else if($tag == 'is'){
                $this->handleXML->read();
                $this->handleXML->read();
                if($map){
                    $data[$map[$index]] = $this->handleXML->value;
                }
                else{
                    $data[$index] = $this->handleXML->value;
                }
                if($this->handleXML->value){
                    $this->handleXML->read();
                }
                $this->handleXML->read();
                $this->handleXML->read();
                $index = null;
            }
            else{
                //return null;
            }
            
        }
        
        // format row data by map array
        if($map){
            for($i = 1; $i <= count($map); $i++){
                $aux[$map[$i]] = isset($data[$map[$i]]) ? $data[$map[$i]] : null;
            }
            $data = $aux;
        }
        
        return $data;
    }
    
    private function removeDir($dir){
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file)){
				removeDir($file);
			}
			else{
				unlink($file);
			}
		}
		rmdir($dir);
	}
    
    //************************************************************
    // multiscript version (one xml processed per script)
    //************************************************************
    public function ms_createSheetXLSX($id, $filename, $columns = null, $name = null){
       
        
        if(count($columns) > 0){
            $columnXML = "<cols>\n";
            for($i = 0; $i < count($columns); $i++){
                $columnXML .= "\t<col customWidth=\"1\" width=\"" . $columns[$i]['width'] . "\" max=\"" . ( $i + 1). "\" min=\"" . ($i + 1) . "\"/>\n";
            }
            $columnXML .= "</cols>\n";
        }
       
        // create new worksheet file
        $this->handleWorkSheet = fopen($filename . '/sheet' . $id . '.xml', 'w');
        fwrite($this->handleWorkSheet, '<?xml version="1.0" encoding="' . $this->encoding . '"?>' .
            '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">' .
            '<dimension ref="A1:L100" />' .
            '<sheetViews><sheetView tabSelected="1" workbookViewId="0"/></sheetViews>' .
            $columnXML .
            "<sheetData>\n");
    }
    
    public function ms_setTempDir($dir){
        $this->tempDir = $dir;
    }
    
    public function ms_finalizeXLSX($dirTemp, $sheets){
        
        $aux = explode("/", $dirTemp);
        $filename = $aux[count($aux) - 1];
        // create zip from model
        $this->ms_zipXLSX($sheets, $filename);
        // remove temp dir
        //$this->removeDir($this->tempDir);
        
        return $this->tempPath . '/' . $filename . '.xlsx';
    }
   
    public function ms_createConfigXML($configSheets, $configStyles){
        // write workbook.xml
        $stringXML = null;
        for($i = 0; $i < count($configSheets); $i++){
            $stringXML .= '<sheet r:id="rId' . ($i + 1)  . '" sheetId="' . ($i + 1) . '" name="' . $configSheets[$i] . '" />';
        } 
        $handle = fopen($this->tempDir . '/workbook.xml', 'w');
        fwrite($handle, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
            '<workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">' .
            '<fileVersion rupBuild="9303" lowestEdited="5" lastEdited="5" appName="xl"/>' . 
            '<workbookPr defaultThemeVersion="124226"/>' . 
            '<bookViews><workbookView windowHeight="6855" windowWidth="14055" yWindow="600" xWindow="270"/></bookViews>' .
            '<sheets>' . $stringXML . '</sheets>' .
            '<calcPr calcId="0"/>' . 
            '<fileRecoveryPr repairLoad="1"/>' .
            '</workbook>');
        fclose($handle);

        // write workbook.xml.rels
        $stringXML = null;
        for($i = 0; $i < count($configSheets); $i++){
            $stringXML .= '<Relationship Id="rId' . ($i + 1) . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" Target="worksheets/sheet' . ($i + 1) . '.xml"/>';
        } 
        $relId = count($configSheets);
        $handle = fopen($this->tempDir . '/workbook.xml.rels', 'w');
        fwrite($handle, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
            '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">' .
                $stringXML . 
                '<Relationship Id="rId' . ++$relId . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" Target="theme/theme1.xml"/>' .
                '<Relationship Id="rId' . ++$relId . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>' .
            '</Relationships>');
        fclose($handle);
        
        // write [Content_Types].xml
        $stringXML = null;
        for($i = 0; $i < count($configSheets); $i++){
            $stringXML .= '<Override ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml" PartName="/xl/worksheets/sheet' . ($i + 1) . '.xml"/>';
        } 
        $handle = fopen($this->tempDir . '/[Content_Types].xml', 'w');
        fwrite($handle, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . 
            '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">' .
                '<Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>' .
                '<Default Extension="xml" ContentType="application/xml"/>' .
                '<Override PartName="/xl/workbook.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"/>' . 
                '<Override PartName="/xl/theme/theme1.xml" ContentType="application/vnd.openxmlformats-officedocument.theme+xml"/>' .
                '<Override PartName="/xl/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml"/>' .
                '<Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml"/>' .
                '<Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml"/>' .
                $stringXML .
            '</Types>');
        fclose($handle);
        
        // write app.xml
        $stringXML = null;
        for($i = 0; $i < count($configSheets); $i++){
            $stringXML .= '<vt:lpstr>sheet' . ($i + 1) . '.xml</vt:lpstr>';
        } 
        $handle = fopen($this->tempDir . '/app.xml', 'w');
        fwrite($handle, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
            '<Properties xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes" xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties">' .
            '<Application>Microsoft Excel</Application>' .
            '<DocSecurity>0</DocSecurity>' . 
            '<ScaleCrop>false</ScaleCrop>' .
            '<HeadingPairs>' .
                '<vt:vector baseType="variant" size="2">' .
                    '<vt:variant><vt:lpstr>Fogli di lavoro</vt:lpstr></vt:variant>' .
                    '<vt:variant><vt:i4>' . count($configSheets) . '</vt:i4></vt:variant>' .
                '</vt:vector>' .
            '</HeadingPairs>' . 
            '<TitlesOfParts>' .
                '<vt:vector baseType="lpstr" size="' . count($configSheets) . '">' .
                    $stringXML . 
                '</vt:vector>' .
            '</TitlesOfParts>' .
            '<Company>Unplugged</Company>' .
            '<LinksUpToDate>false</LinksUpToDate>' . 
            '<SharedDoc>false</SharedDoc>' . 
            '<HyperlinksChanged>false</HyperlinksChanged>' . 
            '<AppVersion>14.0300</AppVersion>' . 
            '</Properties>');
        fclose($handle);
    }
    
    public function ms_zipXLSX($configSheets, $filename = null){
        //print($this->tempDir);
        if($filename){
            $this->outputFile = $filename;
        }
        copy(APPLICATION_PATH . '/../export/models/model.xlsx', $this->tempPath . '/' . $this->outputFile . '.xlsx');
        $zip = new ZipArchive();
        $zip->open($this->tempPath . '/' . $this->outputFile . '.xlsx');
        $zip->addEmptyDir('xl/worksheets');
        for($i = 0; $i < count($configSheets); $i++){
            $zip->addFile($this->tempDir . '/sheet' . ($i + 1) . '.xml', 'xl/worksheets/sheet'  . ($i + 1) . '.xml');
        }
        
        $zip->addFile($this->tempDir . '/styles.xml', 'xl/styles.xml');
        $zip->addFile($this->tempDir . '/workbook.xml', 'xl/workbook.xml');
        $zip->addFile($this->tempDir . '/workbook.xml.rels', 'xl/_rels/workbook.xml.rels');
        $zip->addFile($this->tempDir . '/[Content_Types].xml', '[Content_Types].xml');
        $zip->addFile($this->tempDir . '/app.xml', 'docProps/app.xml');
        $zip->close();
    }
    
    public function ms_addStylesXLSX($styles){
        foreach($styles as $key => $value){
            $this->addStyleXLSX($value, $key);
        }
    }
    
    
    
    
}

?>
