<?php

class Mainsim_Model_MainsimObject
{
    private $db;
    public $type = '';
    private $cross = array(
        't_wares'=>array(
            't_selector_ware'=>array('fields'=>array('f_ware_id')),
            't_ware_wo'=>array('fields'=>array('f_ware_id'),'condition'=>" and exists(select f_id from t_workorders where f_code = f_wo_id and f_type_id IN (3,9,11,15,19))"),
            't_wares_systems'=>array('fields'=>array('f_ware_id')),
            't_pair_cross'=>array('fields'=>array('f_code_main')),
            't_wares_relations'=>array('fields'=>array(array('f_code_ware_master','f_code_ware_slave'))),
            't_users'=>array('fields'=>array('f_code'))
        ),
        't_workorders'=>array(
            't_selector_wo'=>array('fields'=>array('f_wo_id')),
            't_ware_wo'=>array('fields'=>array('f_wo_id')),
            't_pair_cross'=>array('fields'=>array('f_code_main')),            
            't_periodics'=>array('fields'=>array('f_code')),
            't_wo_relations'=>array('fields'=>array(array('f_code_wo_master','f_code_wo_slave')))
        ),
        't_selectors'=>array(
            't_selector_ware'=>array('fields'=>array('f_selector_id')),
            't_selector_wo'=>array('fields'=>array('f_selector_id')),
            't_pair_cross'=>array('fields'=>array('f_code_main'))
        ),
        't_systems'=>array( 
            't_selector_system'=>array('fields'=>array('f_system_id')),
            't_wares_systems'=>array('fields'=>array('f_ware_id')),
            't_bookmarks'=>array('fields'=>array('f_code'))
        )
    );
    /** List of column per type must be empty after cloned */
    private $cleanCols = array(
        't_wares'=>array(
            '1'=>array('fc_asset_direct_costs_ytd','fc_costs_for_downtime','fc_costs_for_material','fc_costs_for_labor','fc_costs_for_tool',
                'fc_costs_for_service','fc_asset_indirect_costs_ytd','fc_asset_total_costs_ytd','fc_asset_budget_ytd','fc_asset_balance',
                'fc_asset_maintenance_costs','fc_asset_replacement_costs','fc_asset_m_cost_on_rep_value','fc_asset_downtime_hourly_cost',
                'fc_asset_daily_usage','fc_asset_wo','fc_asset_opened_wo','fc_asset_mtbf','fc_asset_mttr','fc_asset_mtbr','fc_asset_availability',
                'fc_asset_downtime_total_hours','fc_asset_no_failures','fc_asset_downtime','fc_asset_planned_downtime','fc_asset_operational_time',
                'fc_asset_last_wo_closed'
            )
        )        
    );
    
    private $defaultCols = array(
        't_wares'=>array(
            '1'=>array(
                'fc_asset_wo'=>0
            )
        )
    );    
    
    public function __construct($db = null) {
        if(is_null($db)){
            $this->db = Zend_Db::factory(Zend_Registry::get('db'));        
        }
        else {
            $this->db = $db;
        }
    }    
    
    /**
     * set table to work
     * @param string $table table name
     */
    public function setTable($table)
    {
        $this->type = $table;
    }
    
    /**
     * getObject
     * @param type $f_code
     * @return type
     */
    public function getObject($f_code) {
        $q = new Zend_Db_Select($this->db);
        return $q->from(array("cd" => "t_creation_date"))->join(array("it" => $this->type), "cd.f_id = it.f_code")
        ->join(array("cf" => "t_custom_fields"), "it.f_code = cf.f_code")
        ->join(array("itp" => $this->type."_parent"), "cf.f_code = itp.f_code", array("f_parent_code"))        
        ->where("cd.f_id = $f_code")->where("itp.f_active = 1")->query()->fetchAll();
    }    
    
    /**
     * getTable
     * @param type $table
     * @param type $where
     * @param type $cols
     * @return type
     */
    private function getTable($table, $where, $cols = '*') {
        $q = new Zend_Db_Select($this->db);        
        return $q->from($table, $cols)->where($where)->query()->fetchAll();
    }
    
    public function cloneObjects($codes,$cloneType,$module_name,$commit = true) 
    {
         // security fix (match number,number,...)
        if(!preg_match("/^[0-9]{1,}(,[0-9]{1,})*$/", $codes)){ 
            die('params type input not valid');
        }
        
        $codes = explode(",", $codes); 
        $f_codes_list = array();
        $time = time();
        $userinfo = Zend_Auth::getInstance()->getIdentity();  
        $creationUpd = array(
            'fc_editor_user_name' => utf8_decode($userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname),
            'fc_editor_user_avatar' => $userinfo->fc_usr_avatar,
            'fc_editor_user_gender' => $userinfo->f_gender,
            'fc_editor_user_phone' => $userinfo->fc_usr_phone,
            'fc_editor_user_mail' => $userinfo->fc_usr_mail,
            'fc_creation_user_name' => $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname,
            'fc_creation_user_avatar' => $userinfo->fc_usr_avatar,
            'fc_creation_user_gender' => $userinfo->f_gender,
            'fc_creation_user_phone' => $userinfo->fc_usr_phone,
            'fc_creation_user_mail' => $userinfo->fc_usr_mail
        );
        $tab = array(                    
            "f_user_id"=>$userinfo->f_id
        );                        
        Zend_Registry::set('time',$time);
        try {
            if($commit) {
                $this->db->beginTransaction();
            }
            $tot = count($codes);
            for($i = 0;$i < $tot;++$i) {
                $f_codes = array($codes[$i]);
                if($cloneType == 1) 
                {
                    $f_codes = Mainsim_Model_Utilities::getChildCode($this->type.'_parent', $f_codes, $f_codes, $this->db);
                    $array = array_values($f_codes);
                    $array = implode(",",$array);
                    $array = "(".$array.")";
                    $select = new Zend_Db_Select($this->db);
                    $select->from(array("cd"=>'t_creation_date'),array("cd.f_id"))
                    ->join(array('wp'=>'t_wf_phases'),"cd.f_wf_id = wp.f_wf_id AND cd.f_phase_id = wp.f_number AND wp.f_group_id NOT IN (7)",array())
                    ->where("cd.f_id IN ".$array);
                    $res = $select->query()->fetchAll();
                    $num = count($res);
                    $select -> reset();
                    $child_codes = array();
                    for($j = 0;$j < $num;++$j) {
                        $line = $res[$j];
                        $code = (int)$line['f_id'];
                        $child_codes[$code] = $code;
                    }
                    $f_codes = $child_codes;
                }
                $f_codes = array_values($f_codes);
                $tot2 = count($f_codes);
                for($i2 = 0;$i2 < $tot2;++$i2){   
                    $timeH = $time+1;
                    $f_codes_list[$f_codes[$i2]] = $this->cloneObject($f_codes[$i2], $module_name);
                    $this->db->update("t_creation_date", $creationUpd, "f_id = {$f_codes_list[$f_codes[$i2]]}");
                    $this->db->update($this->type, $tab, "f_code = {$f_codes_list[$f_codes[$i2]]}");
                    $this->cleanClonedCols($f_codes_list[$f_codes[$i2]],$this->type);                    
                    Mainsim_Model_Utilities::newRecord($this->db, $f_codes_list[$f_codes[$i2]], "t_creation_date", array("f_timestamp"=>$timeH), "Cloned");
                    Mainsim_Model_Utilities::newRecord($this->db, $f_codes_list[$f_codes[$i2]], $this->type, array("f_timestamp"=>$timeH), "Cloned");
                    Mainsim_Model_Utilities::newRecord($this->db, $f_codes_list[$f_codes[$i2]], "t_custom_fields", array("f_timestamp"=>$timeH), "Cloned");
                }                
            }
            if($commit) {
                $this->db->commit();
            }
        } catch (Exception $ex) {
            if($commit) { $this->db->rollBack(); }
            $this->db->insert('t_logs',array(
                "f_type_log"=>0,
                'f_timestamp'=>$time,
                'f_log'=>"Something went wrong during clone : ".$ex->getMessage().'<br /><br />'.
                $ex->getTraceAsString()                
            ));
            
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') === 1){
                return array('changed'=>0,'message'=>"Something went wrong during clone : ".$ex->getMessage());
            }
            else{
                return array('changed'=>0,'message'=>"Something went wrong during clone (debug mode off)");
            }
            
        }
        $this->setCloneParents($f_codes_list, $codes, $module_name);
        $this->duplicateAttachments($f_codes_list);
        return array('changed'=>$i,'codes_cloned'=>$f_codes_list);
    }
    
    /**
     * Create fathership after clone
     * @param type $f_codes list of cloned object
     * @param type $root_list original list of objet to clone
     * @param type $module_name 
     */
    private function setCloneParents($f_codes,$root_list,$module_name)
    {        
        $select = new Zend_Db_Select($this->db);
        $time = Zend_Registry::get('time');
        $new_codes = array();
        foreach($f_codes as $old => $new) {            
            $select->reset();
            $res_parent = $select->from($this->type."_parent",array("f_parent_code","f_active"))
                    ->where("f_code = ?",$old)->where("f_active = 1")
                    ->query()->fetchAll();                
            $tot_a = count($res_parent);
            array_push($new_codes, $new);
            for($a = 0;$a < $tot_a;++$a) {
                $wares_parent_copy = $res_parent[$a];                
                $wares_parent_copy['f_code'] = $new;
                $wares_parent_copy['f_parent_code'] = isset($f_codes[$wares_parent_copy['f_parent_code']])?$f_codes[$wares_parent_copy['f_parent_code']] : $wares_parent_copy['f_parent_code'];
                $wares_parent_copy['f_timestamp'] = $time;                
                $this->db->insert($this->type."_parent",$wares_parent_copy);
            }
            if(in_array($old, $root_list)) {
                Mainsim_Model_Utilities::saveReverseAjax($this->type."_parent",$new, array($wares_parent_copy['f_parent_code']), "add", $module_name,$this->db);
            }
        }
        
        foreach ($new_codes as $new) {
            $new_parents=Mainsim_Model_Utilities::getParentCode($this->type."_parent", $new);
            $str = implode(',', $new_parents);
            //print('NEW_PARENTS of '.$new-' = '.$str);
            $this->db->update("t_creation_date", array('fc_hierarchy_codes' => $str), "f_id = {$new}");
        }
    }
    
    /**
     * cloneObject
     * @param type $f_code
     * @param type $module_name
     * @param type $fields
     * @return type
     */
    public function cloneObject($f_code, $module_name, $fields = array()) 
    {
        $f_ncode = $this->cloneCreationDate($f_code, (isset($fields["t_creation_date"])?$fields["t_creation_date"]:array()));
        if(is_array($f_ncode)) return array("message" => "Method cloneCreationDate failed: ".$f_ncode["message"]); // Error
        $res = $this->cloneItem($f_code, (isset($fields["t_item"])?$fields["t_item"]:array()), $f_ncode, $this->type);
        if(is_array($res)) return array("message" => "Method cloneItemParent failed: ".$res["message"]); // Error        
        $res = $this->cloneCustomFields($f_code, (isset($fields["t_custom_fields"])?$fields["t_custom_fields"]:array()), $f_ncode);        
        if(is_array($res)) return array("message" => "Method cloneCustomFields failed: ".$res["message"]); // Error        
        $this->cloneCross($this->type,$f_code,$f_ncode,$fields);
        return $f_ncode;
    }
    
        
    
    /**
     * cloneCreationDate
     * @param type $f_code
     * @param type $fields
     * @return int f_code
     */
    private function cloneCreationDate($f_code,$fields = array()) 
    {
        $time = Zend_Registry::get('time');
        $user = Zend_Auth::getInstance()->getIdentity();        
        $res_t_item = $this->getTable($this->type, "f_code = $f_code");
        $t_item = $res_t_item[0];
        $res_creation_date = $this->getTable('t_creation_date', "f_id = $f_code");        
        $t_creation_date = $res_creation_date[0];
        $phase = $this->getClonedPhase($t_creation_date['f_wf_id']);
        $t_creation_date["f_creation_date"] = $time;
        $t_creation_date["f_creation_user"] = $user->f_id;
        $t_creation_date["f_phase_id"] = $phase;
        $t_creation_date["f_timestamp"] = $time;
        $t_creation_date["fc_progress"] = Mainsim_Model_Utilities::getFcProgress($this->type,$t_item['f_type_id'],$this->db);
        $t_creation_date["fc_editor_user_name"] = '';
        $t_creation_date["fc_editor_user_avatar"] = '';
        $t_creation_date["fc_editor_user_gender"] = '';
        $t_creation_date["fc_editor_user_phone"] = '';
        $t_creation_date["fc_editor_user_mail"] = '';
        $t_creation_date["fc_creation_user_name"] = $user->fc_usr_usn;
        $t_creation_date["fc_creation_user_avatar"] = $user->fc_usr_avatar;
        $t_creation_date["fc_creation_user_gender"] = $user->f_gender;
        $t_creation_date["fc_creation_user_phone"] = $user->fc_usr_phone;
        $t_creation_date["fc_creation_user_mail"] = $user->fc_usr_mail;     
        foreach($fields as $k => $v){
            $t_creation_date[$k] = $v; // Setting up category fields
        }
        return $this->cloneTable($t_creation_date, "t_creation_date");
    }
    
    /**
     * cloneItem
     * @param type $f_code
     * @param type $fields
     * @param type $f_ncode
     * @param type $table
     * @return type
     */
    private function cloneItem($f_code, $fields = array(), $f_ncode, $table) 
    {
        $user = Zend_Auth::getInstance()->getIdentity();        
        $res_t_item = $this->getTable($table, "f_code = $f_code");
        $t_item = $res_t_item[0];
        $t_item["f_code"] = $f_ncode; $t_item["f_timestamp"] = Zend_Registry::get('time'); $t_item["f_user_id"] = $user->f_id;
        foreach($fields as $k => $v) $t_item[$k] = $v; // Setting up category fields
        return $this->cloneTable($t_item, $table);
    }
    
    /**
     * get from passed workflows specific cloned phase number
     * @param int $wf
     */
    private function getClonedPhase($wf)
    {
        $select = new Zend_Db_Select($this->db);
        $resClonedPhase = $select->from('t_wf_phases','f_number')
            ->where('f_wf_id = ?',$wf)->where('f_group_id = 8')
            ->query()->fetch();
        if(empty($resClonedPhase)) {
            throw new Exception ("Cloned phase does not exists");
        }
        return $resClonedPhase['f_number'];
    }
    
    /**
     * cloneItemParent
     * @param type $f_code
     * @param type $f_ncode
     * @param type $table
     * @return type
     */
    private function cloneItemParent($f_code, $fields = array(), $f_ncode, $table) {
        $user = Zend_Auth::getInstance()->getIdentity(); $parents = array();
        $t_item_parent = $this->getTable($table, "f_code = $f_code");
        for($i=0; $i<count($t_item_parent); $i++) {
            $t_item_parent[$i]["f_code"] = $f_ncode; $t_item_parent[$i]["f_timestamp"] = time();
            foreach($fields as $k => $v) $t_item_parent[0][$k] = $v; // Setting up category fields
            $res = $this->cloneTable($t_item_parent[$i], $table);
            if(is_array($res)) return $res;
            $parents[] = $t_item_parent[$i]["f_parent_code"];
        }
        return $parents;
    }
    
    /**
     * cloneCustomFields
     * @param type $f_code
     * @param type $fields
     * @param type $f_ncode
     * @return type
     */
    private function cloneCustomFields($f_code, $fields = array(), $f_ncode) 
    {
        $res_t_custom = $this->getTable("t_custom_fields", "f_code = $f_code");
        $t_custom = $res_t_custom[0];
        $t_custom["f_code"] = $f_ncode; $t_custom["f_timestamp"] = Zend_Registry::get('time');
        foreach($fields as $k => $v){
            $t_custom[$k] = $v; // Setting up category fields
        }
        return $this->cloneTable($t_custom, "t_custom_fields");
    }
    
    /**
     * cloneCross clone cross of old f_code passed into new f_code
     * @param type $table
     * @param type f_code_new
     * @param type $fields
     * @return boolean
     */
    public function cloneCross($table, $f_code_old,$f_code_new, $fields = array()) 
    {
        $cross = $this->cross[$table];        
        foreach($cross as $tableCross => $columnCross){            
            $condition = "";        
            if(!empty($columnCross['condition'])) {$condition = $columnCross['condition']; }
            $lineCross = $columnCross['fields'];
            $tot = count($lineCross);
            for($i = 0;$i < $tot;++$i) {
                if(is_array($lineCross[$i])) {
                    $tot2 = count($lineCross[$i]);
                    for($i2 = 0;$i2 < $tot2;++$i2) {
                        $this->cloneCrossRow($tableCross,$this->getTable($tableCross,"{$lineCross[$i][$i2]} = $f_code_old $condition "),$lineCross[$i][$i2],$f_code_new,$fields);
                    }
                }
                else {
                    $this->cloneCrossRow($tableCross,$this->getTable($tableCross,"{$lineCross[$i]} = $f_code_old $condition"),$lineCross[$i],$f_code_new,$fields);
                }
            }            
        }        
        return true;
    }
    
    /**
     * Edit and clone rows passed for cloneCross Method
     * @param string $table
     * @param array $rows
     * @param string $column
     * @param int $f_code
     * @param array $fields
     */
    private function cloneCrossRow($table, $rows,$column,$f_code,$fields = array())
    {
        $time = Zend_Registry::get('time');
        $tot = count($rows);
        for($i = 0;$i < $tot;++$i) {
            $row = $rows[$i];
            unset($row['f_id']);
            if(isset($row['f_timestamp'])) {
                $row['f_timestamp'] = $time;
            }
            $row[$column] = $f_code;
            foreach($fields as $k => $v){
                $row[$k] = $v; // Setting up new f_code
            }            
            $this->cloneTable($row, $table);            
        }        
    }
    
    /**
     * cloneCross
     * @param type $table
     * @param type $cond
     * @param type $fields
     * @return boolean
     * @deprecated since version 3.0.5.1
     */
    public function _cloneCross($table, $cond, $fields = array()) {
        $t_cross = $this->getTable($table, $cond);
        for($i=0; $i<count($t_cross); $i++) {
            $t_cross[$i]["f_timestamp"] = time();
            foreach($fields as $k => $v) $t_cross[$i][$k] = $v; // Setting up new f_code
            $res = $this->cloneTable($t_cross[$i], $table);
            if(is_array($res)) return $res;            
        }
        return true;
    }
    
    /**
     * clonePairCross
     * @param type $f_code_main
     * @param type $fields
     * @param type $f_ncode
     * @return boolean
     */
    public function clonePairCross($f_code_main, $f_ncode, $fields = array(), $table = "t_pair_cross", $key = "f_code_main") {
        $t_pair_cross = $this->getTable($table, "$key = $f_code_main");
        for($i=0; $i<count($t_pair_cross); $i++) {
            $t_pair_cross[$i][$key] = $f_ncode;
            $t_pair_cross[$i]["f_timestamp"] = time();
            foreach($fields as $k => $v) $t_pair_cross[$i][$k] = $v; // Setting up new f_code
            $res = $this->cloneTable($t_pair_cross[$i], $table);
            if(is_array($res)) return $res;
        }
        return true;
    }
    
    public function cloneBookmarks($f_code, $f_ncode, $fields = array()) {
        return $this->clonePairCross($f_code, $f_ncode, $fields, "t_bookmarks", "f_code");
    }
    
    /**
     * cloneType
     * @param type $f_code_main
     * @param type $fields
     * @param type $f_ncode
     * @return boolean
     */
    public function cloneType($item) {
        return $this->cloneTable($item, "{$this->type}_types");
    }
    
    /**
     * cloneTable
     * @param type $item
     * @param type $table
     * @return type
     */
    private function cloneTable($item, $table) {
        unset($item["f_id"]);
        unset($item["f_generated_code"]);
        try {
            $this->db->insert($table, $item);
            $id = $this->db->lastInsertId();
            if(isset($item['f_field_wiz']) && !empty($item['f_field_wiz'])) {
                $sel = new Zend_DB_Select($this->db);
                $res = $sel->from('t_wares', ['f_code'])->where('f_id='.$id)->query()->fetch();
                $fw = "t_wares:'".$res['f_code']."'";
                $this->db->update('t_wares', ['f_field_wiz' => $fw], 'f_id='.$id);
            }
        } catch(Exception $ex) {
            return array("message" => $ex->getMessage());
        }        
        return $id;
    }
        
    
    /**
     * createType
     * @param type $data
     * @return type
     */
    public function createType($data) {        
        try {
            $this->db->insert("{$this->type}_types", $data);
        } catch(Exception $ex) {
            return array("message" => $ex->getMessage());
        }        
        return $this->db->lastInsertId();
    }
    
    /**
     * getChildren
     * @param type $f_code
     * @return type
     */
    public function getChildren($f_code) {
        $children = array(); $q = new Zend_Db_Select($this->db);
        $res = $q->from("{$this->type}_parent", array("f_code"))->where("f_parent_code = ?", $f_code)->query()->fetchAll();
        $count = count($res); for($i=0; $i<$count; $i++) $children[] = $res[$i]["f_code"];
        return $children;
    }
    
    /**
     * getProgeny
     * @param type $f_code
     * @todo
     */
    public function getProgeny($f_code) {
        
    }
    
    /**
     * changePhase
     * @param type $f_codes
     * @param type $f_wf_id
     * @param type $f_phase_id
     * @todo
     */
    public function changePhase($f_codes, $f_wf_id, $f_exit, $f_module_name,$commit=true,$skipscripts=false) 
    {
        try{
            
            // security fix (match number,number,...)
            if(!preg_match("/^[0-9]{1,}$/", $f_exit)){ 
                die('param f_exit not valid');
            }
            
            // security fix (match mdl_*_tg)
            if(!preg_match("/^mdl_([a-z]){2,}_tg$/", $f_module_name)){ 
                die('param tg not valid');
            }
            
            if($commit) {
                $this->db->beginTransaction();
            }
            $time = time();
            // get only codes that are able to change phase
            $q = new Zend_Db_Select($this->db);                    
            $userinfo = Zend_Auth::getInstance()->getIdentity();  
            
            $scriptModule = new Mainsim_Model_Script($this->db,$this->type);
//            $codes = $q->from(array('t1'=>"t_creation_date"), array("f_id", "script" => "(select f_script from t_wf_exits where f_phase_number = t1.f_phase_id and f_exit = $f_exit and f_wf_id = t1.f_wf_id)"))
//                ->where("t1.f_id in ($f_codes)")->where("f_wf_id = $f_wf_id")
//                ->join(array('t2'=>$this->type),'t1.f_id = t2.f_code',array('f_type_id'))
//            ->where("f_phase_id in (select f_phase_number from t_wf_exits where f_wf_id = $f_wf_id and f_exit = $f_exit)")->query()->fetchAll(); 
            $codes = $q->from(array('t1'=>'t_creation_date'),'f_id')
                ->join(array('t2'=>$this->type),'t1.f_id = t2.f_code',array('f_type_id'))
                ->join(array('t3'=>'t_wf_exits'),'t1.f_phase_id = t3.f_phase_number and t1.f_wf_id = t3.f_wf_id',array('f_script'))
                ->where("t1.f_id IN($f_codes)")->where('t3.f_wf_id = ?',$f_wf_id)->where("f_exit = ?",$f_exit)
                ->query()->fetchAll();                        
            $q->reset();
   
            
            $count = count($codes);     
            if($this->type == 't_workorders') {
                $woObj = new Mainsim_Model_Workorders($this->db);
                $utls = new Mainsim_Model_Utilities();
                $mcpbolds = $utls->getSettings("MULTICHANGEPHASEBATCHOLD");                
            }
            
            for($i=0; $i<$count; $i++) {
                $f_code = $codes[$i]["f_id"];
                $scriptModule->setFcode($f_code);
                $scriptModule->setParams(array('f_type_id'=>$codes[$i]['f_type_id'],'f_wf_id'=>$f_wf_id,'f_phase_id'=>$f_exit,'f_module_name'=>$f_module_name));    

                if(!empty($mcpbolds) && $mcpbolds == '1')
                {
                    //get old wo
                    $old_wo = $utls->getOld($this->db,$f_code,'t_workorders','f_code');
                    //get old wo's custom (used from t_scripts)
                    $old_custom = $utls->getOld($this->db,$f_code,'t_custom_fields','f_code');
                    $old_creation = $utls->getOld($this->db,$f_code, 't_creation_date', 'f_id');
                    $old_cross_wa_wo = $utls->getCross('t_ware_wo', $f_code, 'f_wo_id');
                    $old_paircross = $utls->getPairCross($f_code);
                    //olds array for script
                    $olds = array(
                        'old_table' => $old_wo,
                        'old_custom' => $old_custom,
                        'old_creation' => $old_creation,
                        'old_ware_wo_cross' => $old_cross_wa_wo,
                        'old_paircross' => $old_paircross
                    );
                    
                    //print_r($olds); die("olds");
                }
                
                $creationUpd = array(
                    'fc_editor_user_name' => utf8_decode($userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname),
                    'fc_editor_user_avatar' => $userinfo->fc_usr_avatar,
                    'fc_editor_user_gender' => $userinfo->f_gender,
                    'fc_editor_user_phone' => $userinfo->fc_usr_phone,
                    'fc_editor_user_mail' => $userinfo->fc_usr_mail,                    
                    "f_phase_id" => $f_exit
                );
                $tab = array(                    
                    "f_user_id"=>$userinfo->f_id
                ); 
                $this->db->update("t_creation_date", $creationUpd, "f_id = $f_code");
                $this->db->update($this->type, $tab, "f_code = $f_code");
                Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", array("f_timestamp"=>$time), "Change phase batch");
                Mainsim_Model_Utilities::newRecord($this->db, $f_code, $this->type, array("f_timestamp"=>$time), "Change phase batch");
                Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", array("f_timestamp"=>$time), "Change phase batch");                                
             
             if(!$skipscripts){    if($codes[$i]["f_script"]){ eval($codes[$i]["f_script"]);}    }   
                
                $q->reset();
                $parents = $q->from("{$this->type}_parent", array("f_parent_code"))->where("f_code = ?", $f_code)->query()->fetchAll();
                $parent_codes = array(); foreach($parents as $p) $parent_codes[] = $p["f_parent_code"]; 
                
                if($this->type == 't_workorders') {
                    $woObj->checkPeriodicShift($f_code);
                }
                
                /*Ciao Cristian, mi hai obbligato a mettere questo commento per poter in futuro, nel caso ci fossero dei problemi, dare la colpa a me u.u */
                if($codes[$i]['f_type_id'] == 11) $scriptModule->fc_action_email_ph();
                
                Mainsim_Model_Utilities::saveReverseAjax("{$this->type}_parent", $f_code, $parent_codes, "change phase batch", $f_module_name,$this->db); 
            }
            if($commit) {
                $this->db->commit();
            }            
        }catch(Exception $e) {
            $trsl = new Mainsim_Model_Translate(null,$this->db);            
            $trsl->setLang($trsl->getUserLang($userinfo->f_language)); 
            $message = $trsl->_("Error during multiple change phase : ").$e->getMessage();
            if($commit) {
                $this->db->rollBack();
            }
            $this->db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>"f_code: ".$f_code." ".$message,'f_type_log'=>0));
            return array("f_count" => count(explode(",", $f_codes)), "f_changed" => 0,'f_message'=>$message);
        }
        return array("f_count" => count(explode(",", $f_codes)), "f_changed" => $count);
    }
    
    /**
     * historicizeItem
     * @param type $f_code
     * @return type
     */
    public function historicizeItem($f_code) {
        $this->db->beginTransaction();        
        try {
            $t_creation_date = $this->getRecord("t_creation_date", "f_id = $f_code");
            $t_creation_date["f_code"] = $t_creation_date["f_id"]; unset($t_creation_date["f_id"]);
            $this->db->insert("t_creation_date_history", $t_creation_date);
            $this->db->update("t_creation_date", array("f_timestamp" => mktime()), "f_id = $f_code");

            $t_items = $this->getRecord($this->type, "f_code = $f_code", true);
            $this->db->insert("{$this->type}_history", $t_items);
            $this->db->update($this->type, array("f_timestamp" => mktime()), "f_code = $f_code");
            
            $t_custom_fields = $this->getRecord("t_custom_fields", "f_code = $f_code", true);
            $this->db->insert("t_custom_fields_history", $t_custom_fields);
            $this->db->update("t_custom_fields", array("f_timestamp" => mktime()), "f_code = $f_code");
        } catch(Exception $ex) {
            $this->db->rollBack();
            return array("message" => "Error in historicizing item with f_code $f_code: ".$ex->getMessage());
        }
        $this->db->commit();
    }
    
    /**
     * getRecord
     * @param type $table
     * @param type $cond
     * @param type $clean
     * @return type
     */
    public function getRecord($table, $cond, $clean = false) {
        $q = new Zend_Db_Select($this->db);
        $res = $q->from($table)->where($cond)->query()->fetchAll();
        if($clean) unset($res[0]["f_id"]);
        return $res[0];
    }
    
    /**
     * Duplicate attachment after clone
     * @param type $f_code_list
     */
    private function duplicateAttachments($f_code_list) 
    {
        $select = new Zend_Db_Select($this->db);
        $binds = array(
            $this->type =>Mainsim_Model_Utilities::get_tab_cols($this->type),
            "t_custom_fields"=>Mainsim_Model_Utilities::get_tab_cols("t_custom_fields"),
            "t_creation_date"=>Mainsim_Model_Utilities::get_tab_cols("t_creation_date"),  
            "t_users"=>Mainsim_Model_Utilities::get_tab_cols("t_users")
        ); 
        
        foreach($f_code_list as $old => $new) {
            $select->reset();
            $resAttachment = $select->from('t_attachments',array('f_fieldname','f_session_id','f_code'))
                ->where('f_code = ?',$old)->group('f_session_id')->group('f_code')->group('f_fieldname')->query()->fetchAll();            
            $tot = count($resAttachment);
            for($i = 0;$i < $tot;++$i) {
                $bind = $this->getBindFromUI($resAttachment[$i]['f_fieldname']);                
                if(!$bind){
                    continue;
                }
                $table = '';
                foreach($binds as $tab => $val) {
                    if(in_array($bind, $val)) {
                        $table = $tab;break;
                    }
                }
                if(empty($table)) {
                    continue;
                }
                $this->cloneAttach($new,$table,$bind,$resAttachment[$i]);
            }
        }        
    }
    
    private function getBindFromUI($ui)
    {
        $select = new Zend_Db_Select($this->db);
        $resUi = $select->from('t_ui_object_instances','f_properties')
            ->where("f_instance_name = ?",$ui)->query()->fetch();
        if(empty($resUi)) {
            return false;            
        }
        $prop = json_decode(Mainsim_Model_Utilities::chg($resUi['f_properties']),true);
        return $prop['bind'];        
    }
    
    private function cloneAttach($newCode,$table,$bind,$oldAttach)
    {
        $select = new Zend_Db_Select($this->db);
        $resAttach = $select->from('t_attachments')
            ->where('f_code = ?',$oldAttach['f_code'])
            ->where('f_session_id = ?',$oldAttach['f_session_id'])
            ->query()->fetchAll();        
        $tot = count($resAttach);
        $time = microtime(true) * 10000;
        for($i = 0;$i < $tot;++$i){
            $new_attach = $resAttach[$i];
            $defaultPath = realpath(substr($resAttach[$i]['f_path'], 0,strpos($resAttach[$i]['f_path'],$resAttach[$i]['f_code'])));
            $newPath = $defaultPath.'/'.$newCode;
            if(!is_dir($newPath)) {
                if(!mkdir($newPath)){
                    return 0; 
                }                    
            }
            $session_id = $time.'_'.Zend_Auth::getInstance()->getIdentity()->f_id.'_'.$newCode;
            $new_file_name = $session_id.'_'.$resAttach[$i]['f_type'].'.'.$new_attach['f_file_ext'];
            copy($resAttach[$i]['f_path'],$newPath.'/'.$new_file_name);
            $new_attach['f_timestamp'] = time();
            $new_attach['f_path'] = $newPath.'/'.$new_file_name;
            $new_attach['f_session_id'] = $session_id;
            $new_attach['f_code'] = $newCode;
            unset($new_attach['f_id']);
            $this->db->insert("t_attachments",$new_attach);
        }
        $column = $table == 't_creation_date'?'f_id':'f_code';
        $this->db->update($table,array($bind=>$session_id."|".$new_attach['f_file_name'].'.'.$new_attach['f_file_ext']),"$column = $newCode");
    }
    
    /**
     * 
     */
    private function cleanClonedCols($f_code,$table)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from($table,'f_type_id')->where('f_code = ?',$f_code)
            ->query()->fetch();        
        if(!empty($res['f_type_id']) && isset($this->cleanCols[$table][$res['f_type_id']])) {            
            $list = $this->cleanCols[$table][$res['f_type_id']];
            $tableList = array(
                't_creation_date'=>array('bind'=>'f_id','cols'=>  Mainsim_Model_Utilities::get_tab_cols('t_creation_date')),
                $table=>array('bind'=>'f_code','cols'=>  Mainsim_Model_Utilities::get_tab_cols($table)),
                't_custom_fields'=>array('bind'=>'f_code','cols'=>  Mainsim_Model_Utilities::get_tab_cols('t_custom_fields')),
                't_users'=>array('bind'=>'f_code','cols'=>  Mainsim_Model_Utilities::get_tab_cols('t_users'))
            );
            $tot = count($list);
            for($i = 0;$i < $tot;++$i) {
                foreach($tableList as $tableK => $value) {
                    if(in_array($list[$i],$value['cols'])) {
                        if(isset($this->defaultCols[$table][$res['f_type_id']][$list[$i]])){
                            $this->db->update($tableK,array($list[$i]=>$this->defaultCols[$table][$res['f_type_id']][$list[$i]]),"{$value['bind']} = $f_code");
                        }
                        else {
                            $this->db->update($tableK,array($list[$i]=>null),"{$value['bind']} = $f_code");
                        }
                    }
                }
            }
        }
    }
    
    public function editBatch($table,$params)
    {    
        $paramsObj = json_decode($params);
        
        // security fix 
        if($paramsObj == null){
            die('invalid json params');
        }
        
        $params = json_decode($params,true);
        $params['multiEdit'] = true;
        $f_codes = explode(',',$params['f_codes']);
        if(empty($params['f_codes']) && isset($params['f_filters'])) {
            $treeParams = $paramsObj->f_filters;            
            $treegrid = new Mainsim_Model_Treegrid();
            $t_sst = (microtime(true)*10000).'_'.Zend_Auth::getInstance()->getIdentity()->f_id.'_'.rand(0,9);
            $cols = array('mainTableCols'=>array('f_code'));
            $resParams= $treegrid->prepareArrayTreegrid($treeParams,$t_sst);        
            $treegridParams = $resParams['params'];        		            
            $g = $treegrid->treegridQuery($treeParams->Tab[0], $treeParams->Tab[1], $cols, $treegridParams);                                                		
            $f_codes = array_map(function($line){ return (int)$line['f_code'];}, $g);		            
            $this->db->delete("t_selectors_support","f_call like '{$t_sst}%'");
        }        
        $select = new Zend_Db_Select($this->db);
        $user_level = Zend_Auth::getInstance()->getIdentity()->f_level;
        $uid = Zend_Auth::getInstance()->getIdentity()->f_id;
        unset($params['f_codes']);
        $error = array();
        try {            
            //general error manager
            $this->db->beginTransaction();
            $obj = '';
            $method = '';
            $tipo = '';
            if($table == 't_wares') {
                $obj = new Mainsim_Model_Wares($this->db);
                $method = 'editWares';
                $tipo = "ware";
            }
            elseif($table == 't_workorders'){
                $obj = new Mainsim_Model_Workorders($this->db);
                $method = 'editWorkOrder';
                $tipo = "wo";
            }
            elseif($table == 't_selectors'){
                $obj = new Mainsim_Model_Selectors($this->db);
                $method = 'editSelector';
            }
            elseif($table == 't_systems'){
                $obj = new Mainsim_Model_System($this->db);
                $method = 'editSystem';
            }
            else{
                throw new Exception("Unknown table $table.");
            }
            //check if f_codes are more then 200
            if(count($f_codes) > 200) {
                throw new Exception("Are you trying to edit more than 200 rows.");
            }
            //check if selected rows are locked from other users
            if(!empty($f_codes)) {
                $resLocked = $select->from(array("t1"=>"t_locked"),array())
                    ->join(array('t2'=>'t_creation_date'),'t1.f_code = t2.f_id',array('fc_progress'))
                    ->where("f_code in (?)",$f_codes)->where("f_user_id <> ?",$uid)
                    ->query()->fetchAll();
                
                if(!empty($resLocked)) {
                    $progressList = array_map(function($el){return sprintf("%05s",$el['fc_progress']);},$resLocked);
                    $progressList = implode(',',$progressList);
                    throw new Exception("You cannot edit these elements because some of them are locked by someone : $progressList");
                }                
            }
            //check if selected codes are editable            
            $select->reset();
            $checkedit = $select->from(array("t1"=>"t_creation_date"),"f_id")
                ->join(array("t2"=>"t_wf_phases"),"t1.f_wf_id = t2.f_wf_id and t1.f_phase_id = t2.f_number",array())
                ->join(array("t3"=>"t_wf_groups"),"t2.f_group_id = t3.f_id",array())
                ->where("t3.f_visibility != 0")->where("(t1.f_editability & $user_level) != 0")
                ->where("(t2.f_editability & $user_level) != 0")->where("t1.f_id IN (?)",$f_codes)->query()->fetchAll();            
            if(count($checkedit) != count($f_codes)) {
                throw new Exception("One or more rows you want edit are not editable.");
            }
            //apply multi edit
            $select->reset();
            $select->from($table,'f_type_id');
            $s = new Zend_Db_Select($this->db);
            
            // Rimuovo t_selectors_1,2.. perchè lo ricalcolo
            unset($params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']);
            unset($params['t_selectors_1,2,3,4,5,6,7,8,9,10']);
            
            foreach($f_codes as $f_code) {
                // NICO: sistemo $params['t_selectors_1,2,3,4,5,6,7,8,9,10']
                if ($tipo) {
                    $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'] = [];
                    $s->reset();
                    $results = $s->from(["t1"=>"t_selector_".$tipo])
                            ->join(["t2"=>"t_selectors"],"t1.f_selector_id = t2.f_code", ["f_type_id"])
                            ->where("f_".$tipo."_id = ".$f_code)
                            ->query()->fetchAll();   
                    $selettori = [];
                    foreach ($results as $indice=>$selettore) {
                        $selettori[$selettore['f_type_id']][] = $selettore['f_selector_id'];
                    }  
                    foreach ($params as $indice=>$par) {
                        if (strpos($indice, 't_selectors_') !== false) {
                            if($indice == 't_selectors_1,2,3,4,5,6,7,8,9,10,11')
                                continue;
                            $selettori[substr($indice,12 )] = $par['f_code'];
                        }
                    }
                    foreach ($selettori as $k=>$arr) {
                        foreach ($arr as $k2=>$code) {
                            $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'][] = $code;
                        }
                    }
                    $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code'] = array_unique($params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code']);
                    $params['t_selectors_1,2,3,4,5,6,7,8,9,10,11']['f_code_main'] = $f_code;
                    
                }
                $params['f_code'] = $f_code;                                
                $select->reset(Zend_Db_Select::WHERE);
                $resTypeId = $select->where('f_code = ?',$f_code)->query()->fetch();                    
                $params['f_type_id'] = $resTypeId['f_type_id'];                
                $this->db->insert('t_locked',array('f_code'=>$f_code,'f_user_id'=>$uid,'f_timestamp'=>time()));
                $idLocked = $this->db->lastInsertId();
                $res = $obj->$method($params,true,false);
                $this->db->delete('t_locked',"f_id = $idLocked");
                if(isset($res['message'])) throw new Exception($res['message']);
            }   
            if(!empty($params['attachments'])) {
                Mainsim_Model_Utilities::clearTempAttachments($params,$this->db);
            }
            $this->db->commit();
        }catch(Exception $e) {
            $this->db->rollBack();
            $error['message'] = $e->getMessage();
        }
        return $error;
    }
}
