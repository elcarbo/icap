<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Mainsim_Model_Help
{
    private $db,$helpPath,$projectHelpPath;
    
    public function __construct() 
    {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        $this->helpPath = realpath(APPLICATION_PATH.'/../public/help');        
        $res = $this->db->query("select f_description from t_creation_date where f_category = 'SETTING' and f_title = 'PROJECT_HELP_FOLDER'")->fetch();
        if(!empty($res) && realpath($res['f_description'])) {
            $this->projectHelpPath = realpath($res['f_description']);
        }        
    }
    
    /**
     * Get xml help file and return in array format
     * @param string $lang user language
     * @param array $ptr params
     * @return array with xml
     */
    public function xmltojson($lang, $ptr) {   // $ptr =[ { module:"wizard" }, { argument:"Configurazione" } ]        
        $url = "";
        if(!empty($this->projectHelpPath)) {
            if(file_exists($this->projectHelpPath."/help_{$lang}.xml")){//check if exists project help file for specific lang
                $url = $this->projectHelpPath."/help_{$lang}.xml";
            }
            elseif(file_exists($this->projectHelpPath."/help_en_GB.xml")) {
                $url = $this->projectHelpPath."/help_en_GB.xml";
            }
        }
        if(empty($url)) {
            if(file_exists($this->helpPath."/help_{$lang}.xml")){//check if exists project help file for specific lang
                $url = $this->helpPath."/help_{$lang}.xml";
            }
            else {
                $url = $this->helpPath."/help_en_GB.xml";
            }
        }
        
        $xml=file_get_contents($url);
        $xml = str_replace(array("\n", "\r", "\t"), '', $xml);
        $xml = trim(str_replace('"', "'", $xml));

        $axml = json_decode(json_encode( (array) simplexml_load_string($xml) ),1);

        $page=1;        
        $n=count($ptr);    
        if($n) {
            $lastp=$ptr[$n-1];
            foreach($lastp as $k=>$v){ 
                if($k=="page") { $page=$v; array_pop($ptr); }
            }
        }            

        $x=$this->navXml($axml,$ptr); $y=array();
        if( isset($x["page"]) ) {
            if($this->isAssoc($x["page"])) $y[]=$x["page"]; else $y=$x["page"];
            return array( "data"=>$y, "page"=>$page, "tag"=>"page");         // page nodes
        } 
  
        foreach($x as $k=>$v){
            if($k!="@attributes"){

              if($this->isAssoc($x[$k])) { 
                $y[]=$x[$k]["@attributes"]["label"];
              } else {
                for($r=0;$r<count($x[$k]);$r++) {
                   $y[]=$x[$k][$r]["@attributes"]["label"];                             // list nodes
                }
              } 

                return array( "data"=>$y, "page"=>0, "tag"=>$k ); 
            }
        }
        return array("data"=>array(), "page"=>-1, "error"=>$x );                       // error
    }

    private function navXml($x,$p){                               
        $n=count($p);
        for($r=0;$r<$n;$r++) { $x=$this->GetSub($x,$p[$r]);  }
        return $x;
    }

    private function GetSub($x,$i){
        foreach($i as $k=>$v){ $key=$k; $val=$v; }

        if($this->isAssoc($x[$key])) return $x[$key];

        $n=count($x[$key]); 
        for($r=0;$r<$n;$r++){
           if( $x[$key][$r]["@attributes"]["label"]==$val ) return $x[$key][$r];
        }          
        return $x;  
    }

    private function isAssoc($a){
        return array_keys($a) !== range(0, count($a) - 1);
    }
    
}