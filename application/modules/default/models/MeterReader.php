<?php

class Mainsim_Model_MeterReader
{
    /**
     *
     * @var Zend_Db
     */
    private $db;
    private $create_user_info = array();
    
    public function __construct() {                
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');        
        $this->db = Zend_Db::factory($config->database);    
        //Zend_session::writeClose();
        $select = new Zend_Db_Select($this->db);
        //create default user        
        $admin_info = $select->from(array("c" => "t_users"),
            array('fc_usr_firstname','fc_usr_lastname',
                'fc_usr_mail','f_gender'=>'fc_usr_gender',                
                'fc_usr_avatar','fc_usr_phone'))->where("c.f_code = 1")->query()->fetch();      
        //creator info
        $this->create_user_info['fc_editor_user_name'] = $admin_info['fc_usr_firstname'].' '.$admin_info['fc_usr_lastname'];
        $this->create_user_info['fc_editor_user_avatar'] = $admin_info['fc_usr_avatar'];
        $this->create_user_info['fc_editor_user_gender'] = $admin_info['f_gender'];
        $this->create_user_info['fc_editor_user_phone'] = $admin_info['fc_usr_phone'];
        $this->create_user_info['fc_editor_user_mail'] = $admin_info['fc_usr_mail'];        
        $this->create_user_info['fc_creation_user_name'] = $admin_info['fc_usr_firstname'].' '.$admin_info['fc_usr_lastname'];
        $this->create_user_info['fc_creation_user_avatar'] = $admin_info['fc_usr_avatar'];
        $this->create_user_info['fc_creation_user_gender'] = $admin_info['f_gender'];
        $this->create_user_info['fc_creation_user_phone'] = $admin_info['fc_usr_phone'];
        $this->create_user_info['fc_creation_user_mail'] = $admin_info['fc_usr_mail'];             
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function readMeters($start = -1,$end = 0,$number = 0,$from_generator = false)
    {        
        $select = new Zend_Db_Select($this->db);
        $days_before = 1;
        //get delay (in days)
        try {
            $res_delay = $select->from("t_system_information","f_days_before_check")->query()->fetch();
            $days_before = !empty($res_delay) && $res_delay['f_days_before_check'] > 0?$res_delay['f_days_before_check']:1;
        }catch(Exception $e) {};
        //convert delay in second
        $days_before = $days_before*86400;
        //-----
        
        $start = $start == -1?time() - $days_before:$start; 
        $end = $end==0?time() + 400:$end;
        
        $select->reset();
        $select->from(array("t1"=>"t_periodics"))
                ->join(array("t2"=>"t_workorders"),"t1.f_code = t2.f_code",array())     
                ->join("t_creation_date","t_creation_date.f_id = t2.f_code",array())
                ->where("t2.f_type_id = 11")->where("t_creation_date.f_phase_id = 1")                
                ->where("f_executed = 0")
                ->where("t1.f_start_date - t1.f_forewarning > $start")
                ->where("t1.f_start_date - t1.f_forewarning < $end");
         if($number) {
            $select->limit($number);
        }
        
        $res = $select->query()->fetchAll();
        foreach($res as $line) {
            $this->db->query("UPDATE t_periodics SET f_executed = 1 where  f_id = '{$line['f_id']}'");
        }        
        foreach($res as $line) {
            //PRENDO TUTTE LE PERIODICHE TROVATE E LE TRASFORMO IN RICHIESTE
            $select->reset();
            $res_wo = $select->from(array("t1"=>"t_workorders"))
                    ->join(array("tc"=>"t_creation_date"),"t1.f_code = tc.f_id",array("f_title","f_description","f_visibility","f_editability","f_wf_id","f_phase_id"))
                    ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code")                    
                    ->where("t1.f_code = ?",$line['f_code'])->query()->fetchAll();              
            if(empty($res_wo)) continue;                        
            $this->createWorkorders($line,$res_wo[0]);                                    
        }
        if(!$from_generator) {
             echo count($res).' meter generated at '.date('d-m-Y H:i:s').PHP_EOL;
        }        
        $this->db->insert("t_logs",array("f_log"=>count($res).' meter generated at '.date('m-d-Y H:i:s'),'f_type_log'=>1,'f_timestamp'=>time()));
        return count($res);
    }
    
    private function createWorkorders($line,$res_wo,$res_asset = array())
    {
        $select = new Zend_Db_Select($this->db);
        $f_code_genereator =  $line['f_code'];                
        $data = array();        
        $time = time();
        //recupero il wf dal type_id
        $select->reset();
        $res_wf = $select->from("t_workorders_types",array('f_wf_id','f_wf_phase'))
                ->where("f_id = ?",$res_wo['f_type_id_periodic'])->query()->fetchAll();
        $wf = !empty($res_wf) && $res_wf[0]['f_wf_id']?$res_wf[0]['f_wf_id']:1;
        $wf_phase = !empty($res_wf) && $res_wf[0]['f_wf_phase']?$res_wf[0]['f_wf_phase']:1;
        
        $data['f_type_id'] = 7;        
        $data['f_timestamp'] = $time;
        $data['f_code_periodic'] = $line['f_code'];        
        $data['f_user_id'] = $res_wo['f_user_id'];
        $data['f_start_date'] = $line['f_start_date']; //data inizio
        $data['f_end_date'] = $line['f_start_date'] + $res_wo['fc_pm_duration'];
        $data['f_priority'] = $res_wo['f_priority'];        
        $data['f_counter'] = 0;
        
        //recupero il type_id da usare       
        $select->reset();
        $res_types = $select->from("t_workorders_types")->where("f_id = 7")->query()->fetchAll();
        
        if(empty($res_types)) return;
        $select->reset();
        $res_creation = $select->from("t_creation_date")->where("f_category = ?",$res_types[0]['f_type'])->order("f_order DESC")->limit(1)->query()->fetchAll();        
        $creation = array(
            'f_type'=>'WORKORDERS',
            'f_category'=>$res_types[0]['f_type'],
            'f_order'=>!empty($res_creation)?$res_creation[0]['f_order'] + 1:1,
            'f_creation_date'=>$time,
            'f_creation_user'=>1,
            'f_wf_id'=>$wf,
            'f_phase_id'=>$wf_phase,
            'f_title'=>$res_wo['f_title'],
            'f_description'=>$res_wo['f_description'],
            'f_visibility'=>-1,
            'f_editability'=>-1
        );                
        $creation = array_merge($creation,$this->create_user_info);
        $f_code = $this->creationDate($creation);            
        $data['f_code'] = $f_code;
        $select->reset();
        $res_wo_order = $select->from("t_workorders")->where("f_type_id = ?",$res_wo['f_type_id_periodic'])->order("f_order DESC")->limit(1)->query()->fetchAll();
        $data['f_order'] = !empty($res_wo_order)?$res_wo_order[0]['f_order'] + 1 : 1;
        //Inserisco il wo        
        $this->db->insert("t_workorders",$data);
        
        //Inserico i custom fields 
        $select->reset();
        $res_custom = $select->from("t_custom_fields")->where("f_code = ?",$f_code_genereator)
                ->query()->fetchAll();
                
        $custom = isset($res_custom[0])?$res_custom[0]:array();        
        
        //Inserico i custom fields 
        $custom["f_code"] = $f_code;
        $custom["f_timestamp"] = $time;
        //$custom = array_merge($custom,$this->create_user_info);
        unset($custom['f_id']);
                
        $this->db->insert("t_custom_fields", $custom);
        
        $select->reset();
        //script
        $res_script = $select->from("t_scripts")->where("f_type = 'generator'")->query()->fetchAll();
        foreach( $res_script as $script) {
            eval($script['f_script']);
        }
        //Controllo se la periodica ha un padre e se quel padre va generato
        //Primo caso : controllo se ha una paternitÃ 
        $select->reset();
        $res_parent = $select->from("t_workorders_parent")->where("f_code = ?",$line['f_code'])->query()->fetchAll();
        $parent = array(
            "f_active"=>1,
            "f_timestamp"=>$time,
            "f_code"=>$f_code
        );
        if($res_parent[0]['f_parent_code'] == 0) {
            $parent['f_parent_code'] = 0;
            $this->db->insert("t_workorders_parent", $parent);
        }
        else {
            //verifico se il parent ha una periodicitÃ  : se no lo metto a 0 il parent, else controllo se esiste giÃ  il parent, altrimenti lo creo on the fly
            $select->reset();
            $res_periodic = $select->from("t_periodics")->where("f_executed = 0")->where("f_code = {$res_parent[0]['f_parent_code']}")->query()->fetchAll();
            if(empty($res_periodic)) {
                $parent['f_parent_code'] = 0;
                $this->db->insert("t_workorders_parent", $parent);
            }
            else { //controllo se questo f_code ha una periodica che scatta oggi
                $select->reset();
                $res_periodic = $select->from("t_periodics")->where("f_executed = 0")
                        ->where("f_code = {$res_parent[0]['f_parent_code']}")
                        ->where("f_start_date - f_forewarning between ".$time." and ".($time + 60).")")
                        ->query()->fetchAll();
                if(empty($res_periodic)) {
                    $parent['f_parent_code'] = 0;
                    $this->db->insert("t_workorders_parent", $parent);
                }
                else {
                    $parent['f_parent_code'] = 0;
                    $this->db->insert("t_workorders_parent", $parent);
                }                
            }
        }
        $select->reset();
        $res_selector = $select->from("t_selector_wo",array("f_selector_id","f_timestamp","f_nesting_level"))
                ->where("f_wo_id = ?",$f_code_genereator)->query()->fetchAll();
        foreach($res_selector as $line_selector) {
            $wo_sel = $line_selector;
            $wo_sel['f_wo_id'] = $f_code;
            $this->db->insert("t_selector_wo",$wo_sel);    
        }
        
        //Inserisco gli asset se sono presenti.        
        $select->reset();
        
        $select->reset();
        $res_asset = $select->from(array("t1"=>"t_wares"),array('f_code'))
            ->join(array("tc"=>"t_creation_date"),"t1.f_code = tc.f_id",array())
            ->join(array("t2"=>"t_ware_wo"),"t1.f_code = t2.f_ware_id",array())
            ->join(array("t3"=>"t_wf_phases"),"tc.f_phase_id = t3.f_number and tc.f_wf_id = t3.f_wf_id",array())    
            ->join(array("t4"=>"t_wf_groups"),"t4.f_id = t3.f_group_id",array())    
            ->where("t4.f_visibility != 0")
            ->where("t2.f_wo_id = ?",$line['f_code'])
            ->where("t1.f_type_id = 1")->where("t1.f_start_date < ".$line['f_start_date'])
            ->where("tc.f_phase_id = 1")->query()->fetchAll();   
        
        foreach($res_asset as $line_asset) {
            $asset_edit = array(
                'f_ware_id'=>$line_asset['f_code'],
                'f_wo_id'=>$f_code,                
                'f_timestamp'=>$time                
            );            
            $this->db->insert("t_ware_wo",$asset_edit);                
        }          
    }
    
    private function creationDate($data)
    {
        $this->db->beginTransaction();
        $this->db->insert("t_creation_date", $data);
        $f_code = $this->db->lastInsertId();
        $this->db->commit();
        return $f_code;
    }    
    
    
}