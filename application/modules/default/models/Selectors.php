<?php

class Mainsim_Model_Selectors
{
    /**
     *
     * @var Zend_Db 
     */
    private $db, $table, $obj;
    
    public function __construct($db = null) {        
        $this->db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;
        $this->obj = new Mainsim_Model_MainsimObject();
        $this->table = 't_selectors';
        $this->obj->type = $this->table;
    }
    
    
    /**
     * Get user selectors and its fathers and childre
     */
    private function getUserSelectors($stype,$fathers = true,$children = true)
    {
        $q = new Zend_Db_Select($this->db);
        $userSels = Zend_Auth::getInstance()->getIdentity()->selectors;
        $selsAllow = [];
        if(!empty($userSels)){
            $resSelsAllow = $q->from('t_selectors','f_code')->where('f_type_id = ?',$stype)
                ->where("f_code IN ($userSels)")->query()->fetchAll();
            $selsAllow = array_map(function($el){ return (int)$el['f_code'];},$resSelsAllow);            
            $tot = count($selsAllow);
            for($i = 0;$i < $tot;++$i) {
                if($children) {
                    $selsAllow = Mainsim_Model_Utilities::getChildCode('t_selectors_parent', $selsAllow[$i], $selsAllow, $this->db);
                }
                if($fathers) {
                    $selsAllow = Mainsim_Model_Utilities::getParentCode('t_selectors_parent', $selsAllow[$i], $selsAllow, $this->db);
                }
                $selsAllow = array_values($selsAllow);
            }
        }
        return $selsAllow;
    }
    
    
    public function get($stype = 1, $parent = 0, $orderby = "f_title", $order = "ASC") {
        $data = [];
        $q = new Zend_Db_Select($this->db);
        $selsAllow = $this->getUserSelectors($stype);       
        $creationCols = Mainsim_Model_Utilities::get_tab_cols('t_creation_date');
        $selParentCols = Mainsim_Model_Utilities::get_tab_cols('t_selectors_parent');
        $orderCommand = 'cd.f_id ASC';
        if(in_array($orderby,$creationCols)) {
            $orderCommand = "cd.{$orderby} $order";
        }
        elseif(in_array($orderby,$selParentCols)){
            $orderCommand = "sp.{$orderby} $order";
        }        
        $user = Zend_Auth::getInstance()->getIdentity();        
        $q->from(array("cd" => "t_creation_date"), array("f_code" => "f_id", "f_title", "children" => "(SELECT COUNT(*) FROM t_selectors_parent JOIN t_creation_date ON t_selectors_parent.f_code = t_creation_date.f_id WHERE f_parent_code = cd.f_id AND t_creation_date.f_phase_id = 1)"))
            ->join(array("sp" => "t_selectors_parent", []), "cd.f_id = sp.f_code")
            ->where("cd.f_type = 'SELECTORS'")->where("cd.f_phase_id = 1")->where("sp.f_parent_code = $parent")->where("sp.f_active = 1")
            ->where("cd.f_category = (SELECT f_type FROM t_selectors_types WHERE f_id = $stype)")
            ->where("(cd.f_visibility & {$user->f_level}) != 0")->order($orderCommand);
        if(!empty($selsAllow) && $parent != 0) {
            $q->where("cd.f_id IN (?)",$selsAllow);
        }                    
        $res = $q->query()->fetchAll();
        $count = count($res);
        for($i=0; $i<$count; $i++) $data[] = array($res[$i]["f_code"], Mainsim_Model_Utilities::chg($res[$i]["f_title"]), intval($res[$i]["children"]));
        return $data;
    }
    
    /**
     * @deprecated
     * @param type $type
     * @return type
     */
    public function getSelectorsFromType($type)
    {           
        $select  = new Zend_Db_Select($this->db);        
        $response = [];
        $user = Zend_Auth::getInstance()->getIdentity();        
        //get selectors of this user for filter selector'structure
        $u_sels = isset($user->selectors)?$user->selectors:'';        
        //Cerco gli elementi di root
        $result_parent = $select->from(array("s" => "t_selectors"),array("f_code","f_order"))
            ->join("t_creation_date","s.f_code = t_creation_date.f_id",array('f_title'))
            ->join(array('sp' => 't_selectors_parent'),'s.f_code = sp.f_code')
            ->where("sp.f_parent_code = ?",0)
            ->where("s.f_type_id = ?",$type)
            ->where("(t_creation_date.f_visibility & ?) != 0",$user->f_level)                
            ->where("sp.f_active = 1")->where("t_creation_date.f_phase_id = 1")
            ->query()->fetchAll();
        if(count($result_parent)) {                        
            //carico la root            
            $line = $result_parent[0];
            //Father                
            $response[$line['f_code']]['pid'] = [];
            //Label
            $response[$line['f_code']]['label'] = Mainsim_Model_Utilities::chg($line['f_title']);
            //open e selected
            $response[$line['f_code']]['open'] = true;
            $response[$line['f_code']]['selected'] = false;
            //f_order
            $response[$line['f_code']]['order'] = $line['f_order'];
            //recupero tutti i figli di questo elemento
            $select->reset();            
            $select->from(array("sp" => "t_selectors_parent"),array('num'=>'COUNT(*)'))
                ->join(array("s" => 't_selectors'),'s.f_code = sp.f_code',[])
                ->join("t_creation_date","s.f_code = t_creation_date.f_id",[])
                ->where("sp.f_parent_code = ?",$line['f_code'])                    
                ->where("(t_creation_date.f_visibility & ?) != 0",$user->f_level)                
                ->where("sp.f_active = 1")->where("t_creation_date.f_phase_id = 1");
            $result_parent_child = $select->query()->fetchAll();
            
            if(isset($result_parent_child[0]['num']) && $result_parent_child[0]['num'] > 0) {                    
                $response[$line['f_code']]['leaf'] = false;
            }
            else {                    
                $response[$line['f_code']]['leaf'] = true;
            }                
            $response[$line['f_code']]['sub'] = [];    
            
            $sub = $this->getSelectorsFromId($line['f_code']);
            if(isset($sub[$line['f_code']])){
                $response[$line['f_code']]['sub'] = array_keys($sub[$line['f_code']]);
                foreach($sub[$line['f_code']] as $key => $value) {
                    $response[$key] = $value;
                }
            }
        }  
        
        return $response;
    }
    
    /**
     * @deprecated
     * @param type $id
     * @return array
     */
    public function getSelectorsFromId($id)
    {        
        $select  = new Zend_Db_Select($this->db);        
        $res_type_id = $select->from("t_selectors",array("f_type_id"))->where("f_code = ?",$id)->query()->fetchAll();
        $type_id = $res_type_id[0]['f_type_id'];
        $response = [];
        $user = Zend_Auth::getInstance()->getIdentity();                
        $u_sels_exp = isset($user->selectors)?explode(',',$user->selectors):[];
        $u_sel_types = [];
        if(!empty($u_sels_exp)) {            
            $tot_a = count($u_sels_exp);
            for($a = 0;$a < $tot_a;++$a) {
                $select->reset();
                $res_types = $select->from("t_selectors",array("f_type_id"))
                        ->where("f_code = ?",$u_sels_exp[$a])
                        ->query()->fetchAll();
                if(!empty($res_types))$u_sel_types[$res_types[0]['f_type_id']][] = $u_sels_exp[$a];
            }
        }        
        //Cerco gli elementi di root        
        $select->reset();
        $result_parent = $select->from("t_selectors_parent")
            ->join('t_selectors','t_selectors.f_code = t_selectors_parent.f_code',[])
            ->join("t_creation_date","t_selectors.f_code = t_creation_date.f_id",array('f_title'))
            ->where("f_parent_code = ?",$id)->where("t_selectors_parent.f_active = 1")
            ->where("(t_creation_date.f_visibility & ?) != 0",$user->f_level)->where("t_creation_date.f_phase_id = 1")->query()->fetchAll();
        //Carico i figli con i rispettivi padri e figli        
        $tot = count($result_parent);
        for($a = 0; $a < $tot; ++$a) {
            $line = $result_parent[$a];
            //check if this is element can be viewed by user            
            if(!empty($u_sel_types[$type_id]) && !in_array($line['f_code'], $u_sel_types[$type_id])){
                //if is cannot be viewed,i'm check if their child can be viewed,
                //and if it's true, create insert element in array, otherwise continue
                $ok = false;
                $parent = $line['f_code'];
                do {
                    $parent_array = [];
                    $not_end = true;
                    $select->reset();
                    $res_parent_check = $select->from("t_selectors_parent",array("f_code"))
                            ->where("f_active = 1")->where("f_parent_code IN ($parent)")->query()->fetchAll();                    
                    $tot_b = count($res_parent_check);                    
                    if(!$tot_b) break;
                    for($b = 0; $b < $tot_b;++$b) {
                        if(in_array($res_parent_check[$b]['f_code'],$u_sel_types[$type_id])) {
                            $not_end = false;
                            $ok = true;
                            break;
                        }
                        else {
                            $parent_array[] = $res_parent_check[$b]['f_code'];
                        }
                    }                    
                    $parent = implode(',',$parent_array);
                }while($not_end);
                if(!$ok) continue;
            }            
            
            //Father
            $select->reset();
            $select->from("t_selectors_parent")
                ->join('t_selectors','t_selectors.f_code = t_selectors_parent.f_code',[])
                ->join("t_creation_date","t_selectors.f_code = t_creation_date.f_id",array('f_title'))
                ->where("t_selectors_parent.f_code = ?",$line['f_code'])->where("t_selectors_parent.f_parent_code = ?",$id)
                ->where("t_selectors_parent.f_active = 1")->where("t_creation_date.f_phase_id = 1");
            $select->where("(t_creation_date.f_visibility & ?) != 0",$user->f_level);
            
            $result_parent_pid = $select->query()->fetchAll();
            
            $f_code_pid = [];
            foreach($result_parent_pid as $line_pid) {
                $f_code_pid[] = $line_pid['f_parent_code'];
            }
            $response[$id][$line['f_code']]['pid'] = $f_code_pid;
            //Label
            $response[$id][$line['f_code']]['label'] = Mainsim_Model_Utilities::chg($line['f_title']);
            //open e selected
            $response[$id][$line['f_code']]['open'] = false;
            $response[$id][$line['f_code']]['selected'] = false;
            //f_order
            $response[$id][$line['f_code']]['order'] = $line['f_order'];
            //recupero tutti i figli di questo elemento
            $select->reset();
            
            $select->from("t_selectors_parent",array('num'=>'COUNT(*)'))
                    ->join('t_selectors','t_selectors.f_code = t_selectors_parent.f_code',[])
                    ->join("t_creation_date","t_selectors.f_code = t_creation_date.f_id",[]);
            $select->where("t_selectors_parent.f_parent_code = ?",$line['f_code'])->where("t_selectors_parent.f_active = 1");
            $select->where("(t_creation_date.f_visibility & ?) != 0",$user->f_level)->where("t_creation_date.f_phase_id = 1");
            
            
            $result_parent_child = $select->query()->fetchAll();
            
            if(isset($result_parent_child[0]['num']) && $result_parent_child[0]['num'] > 0) {                    
                $response[$id][$line['f_code']]['leaf'] = false;
            }
            else {
                $response[$id][$line['f_code']]['leaf'] = true;
            }   
            $response[$id][$line['f_code']]['sub'] = [];
        }
                
        return $response;
    }
    
    /**
     * create a new selector
     * @param type $params 
     */
    public function newSelector($params, $commit = true)
    {
        $time = time();          
        $params = Mainsim_Model_Utilities::clearEmptyValuesParams($params);        
        if(Zend_Registry::isRegistered('time')) {
            $time = Zend_Registry::get('time');
        }
        else {
            Zend_Registry::set('time', $time);
        }        
        $error = [];
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        $params['fc_editor_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;
        $params['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
        $params['fc_editor_user_gender'] = $userinfo->f_gender;
        $params['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
        $params['fc_editor_user_mail'] = $userinfo->fc_usr_mail;
        //creator info
        $params['fc_creation_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;
        $params['fc_creation_user_avatar'] = $userinfo->fc_usr_avatar;
        $params['fc_creation_user_gender'] = $userinfo->f_gender;
        $params['fc_creation_user_phone'] = $userinfo->fc_usr_phone;
        $params['fc_creation_user_mail'] = $userinfo->fc_usr_mail;
        $params['f_creation_user'] = $userinfo->f_id;
        $params['fc_creation_date'] = $time;
        
        try {
            if($commit){
                $this->db->beginTransaction();   
            }
            
            $parent_key = array_filter(array_keys($params),function($v) {
                return (strpos($v,"t_selectors_parent") !== false);});            
            $parents = !empty($parent_key)?$params[$parent_key[0]]['f_code']:[];            
            
			if(empty($parents)) {
				throw new Exception("You cannot save without father.");
			}			
			$sel = new Zend_Db_Select($this->db);
			$resTypeId = $sel->from("t_selectors",'f_type_id')->where('f_code IN (?)',$parents)
				->group('f_type_id')->query()->fetchAll();			
            if(count($resTypeId) > 1){
				throw new Exception("You cannot have fathers with different type id.");
			}			
			$params['f_type_id'] = $resTypeId[0]['f_type_id'];            
            $fc_progress = Mainsim_Model_Utilities::getFcProgress("t_selectors",$params['f_type_id'],$this->db);            
            Zend_Registry::set('db_connection_editor',$this->db);
            
            $columns = Mainsim_Model_Utilities::get_tab_cols("t_selectors");
            $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
            $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");        
            $pair_cols = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
            $module_name = $params['f_module_name'];
                        
            $new_sel = [];
            $new_cd = [];
            $custom_fields = [];

            foreach($params as $key => $value) {
                if(in_array($key,$creation_columns)) {//check if this is a column of creation_date
                    $new_cd[$key] = Mainsim_Model_Utilities::clearChars ($value);
                }
                elseif(in_array($key,$columns)) { // check if this is a column of selector
                    $new_sel[$key] = Mainsim_Model_Utilities::clearChars ($value);
                }
                elseif(in_array($key,$custom_columns)) { // check if this is a column of custom
                    $custom_fields[$key] = Mainsim_Model_Utilities::clearChars ($value);
                }
            }
            
            //catch name to use as f_category in t_cretion_date
            $sel->reset();
            $sel_type = $sel->from("t_selectors_types")->where("f_id = ?",$params['f_type_id'])->query()->fetch();                    
            $sel->reset();
            $res_order = $sel->from("t_creation_date",array("f_order"=>"MAX(f_order)"))
                ->where("f_type = 'SELECTORS'")->where("f_category = ?",$sel_type['f_type'])                   
                ->query()->fetch();
            $sel->reset();
            $f_order = 1;
            if(!empty($res_order['f_order'])) {
                $f_order = $res_order['f_order'];
                $f_order+=1;
            }
            
            $new_cd["f_type"] = "SELECTORS";
            $new_cd["f_category"] = $sel_type['f_type'];
            $new_cd["f_order"] = $f_order;
            $new_cd["f_creation_date"] = $time;
            $new_cd['f_wf_id'] = $sel_type['f_wf_id'];
            $new_cd['f_phase_id'] = $sel_type['f_wf_phase'];
            $new_cd['f_visibility'] = -1;
            $new_cd['f_editability'] = -1;                    
            $new_cd['fc_progress'] = $fc_progress;                    
            $this->db->insert("t_creation_date", $new_cd);
            $f_code = $this->db->lastInsertId();
            if(empty($new_sel['f_priority'])){ $new_sel['f_priority'] = 0;}

            // Insert into t_selectors        
            $new_sel ['f_timestamp'] = $time;
            $new_sel['f_code'] = $f_code;
                   
            $new_sel['f_user_id'] = $userinfo->f_id;        
            $new_sel['f_order'] = $f_order;
            $this->db->insert("t_selectors", $new_sel);                            

            $custom_fields['f_code'] = $f_code;
            $custom_fields['f_timestamp'] = $time; 
            
            Mainsim_Model_Utilities::createParents('t_selectors_parent',$f_code,$parents,$this->db);            
            
            foreach($params as $key => $value) {  
                if(!is_array($value)) continue;
                $ftype = $value['f_type'];                
                if(isset($value['f_pair']) && is_array($value['f_pair'])) {                     
                    foreach($value['f_pair'] as $f_pair) {
                        if(is_null($f_pair)) continue;
                        $f_pair_params = [];
                        foreach($f_pair as $key_pair => $line_pair) {
                            if($key == '' || !in_array($key_pair, $pair_cols)) continue;
                            $f_pair_params[$key_pair] = Mainsim_Model_Utilities::clearChars($line_pair);
                        }                        
                        $f_pair_params['f_code_main'] = $f_code;                        
                        Mainsim_Model_Utilities::createPairCross($this->db,$f_pair_params,$value['pairCross']);                            
                    }                    
                }
            }
           
            $fields_params = array_keys($params);                
            //check if exist a custom script of mandatory_script in fields_script and script. If not exist, run default version
            $scriptModule = new Mainsim_Model_Script($this->db,"t_selectors",$f_code,$params);
            $scriptModule->t_selector_parent();            
            
            foreach($fields_params as $field) {                      
                $sel->reset();                       
                $res_scr = $sel->from("t_scripts")->where("f_name = '$field-t_selectors'")->where("f_type = 'php'")->query()->fetch();                
                if(!empty($res_scr['f_script'])) { eval($res_scr['f_script']); }
                elseif(method_exists($scriptModule,$field) && is_callable(array($scriptModule,$field))) {//if not exist try to check if is a custom script in script model
                    $err_script = $scriptModule->$field();
                    if(isset($err_script['message'])) throw new Exception($err_script['message']);
                }
            }
            // insert custom fields
            $this->db->insert("t_custom_fields", $custom_fields); 
            Mainsim_Model_Utilities::saveAttachments($f_code, 't_selectors', $params, $scriptModule);  
           
            if(!empty($params['attachments'])) {
                Mainsim_Model_Utilities::clearTempAttachments($params,$this->db);
            }
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", array("f_timestamp"=>$time), "New");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_selectors", array("f_timestamp"=>$time), "New");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", array("f_timestamp"=>$time), "New");              
            
                if(empty($error)) { 
                    if($commit){
                        $this->db->commit(); 
                    }
                    return $f_code;
                }
                else{ 
                    if($commit){
                        $this->db->rollBack(); 
                    }
                    return $error; 
                }
          
        }
        catch(Exception $e) { 
            if($commit){
                $this->db->rollBack();
            }
            $this->db->insert("t_logs",array("f_timestamp"=>$time,"f_log"=>"Something went wrong during creation of selector : ".Mainsim_Model_Utilities::clearChars($e->getMessage()),"f_type_log"=>0));
            $error['message'] = "Something went wrong during creation of selector : ".Mainsim_Model_Utilities::clearChars($e->getMessage());
        }
        //inform system about insert
        Mainsim_Model_Utilities::saveReverseAjax("t_selectors_parent",$f_code,[],"add", $module_name,$this->db);        
        return $error;
    }
    
    /**
     * Create hierarchy of selector
     * @param type $f_code f_code of selector
     * @param type $f_parent_codes : list of parent code (array)
     * 
     */
    private function createParent($f_code,$f_parent_codes)
    {           
        if(empty($f_parent_codes)) {
           $this->db->insert('t_selectors_parent', array(
                'f_code'=>$f_code,
                'f_parent_code'=>0,
                'f_active'=>1,
                'f_timestamp'=>time()                
            ));   
        }
        foreach($f_parent_codes as $parent) {            
            if($parent == $f_code) {
                continue;
            }            
            $this->db->insert('t_selectors_parent', array(
                'f_code'=>$f_code,
                'f_parent_code'=>$parent,
                'f_active'=>1,
                'f_timestamp'=>time()                
            ));            
        }
    }
    
    /**
     * Edit existing selector
     * @param type $params : list of params sent from edit module
     * @return array empty if is ok otherwise with a message
     * @throws Exception 
     */    
    public function editSelector($params,$edit_batch = false,$commit = true)
    {
        $time = time();  
        $params = Mainsim_Model_Utilities::clearEmptyValuesParams($params);        
        if(Zend_Registry::isRegistered('time')) {
            $time = Zend_Registry::get('time');
        }
        else {
            Zend_Registry::set('time', $time);
        }
        $error = [];
        $mandatory_script = array("t_selector_parent");
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        
        $params['fc_editor_user_name'] = $userinfo->fc_usr_firstname.' '.$userinfo->fc_usr_lastname;
        $params['fc_editor_user_avatar'] = $userinfo->fc_usr_avatar;
        $params['fc_editor_user_gender'] = $userinfo->f_gender;
        $params['fc_editor_user_phone'] = $userinfo->fc_usr_phone;
        $params['fc_editor_user_mail'] = $userinfo->fc_usr_mail;
        
        try {
             //if is a normal new wo, start transaction
            if($commit){
                $this->db->beginTransaction();                
            } 
            Zend_Registry::set('db_connection_editor',$this->db);
            $sel = new Zend_Db_Select($this->db);

            if($params['new_type'] != "" && $params['f_type_id'] == -1) {
                //create a new type
                $this->db->insert("t_selectors_types",array('f_type'=>  strtoupper($params['f_title'])));
                $params['f_type_id'] = $this->db->lastInsertId();
            }
            
            $columns = Mainsim_Model_Utilities::get_tab_cols("t_selectors");
            $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
            $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
            $pair_cols = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
            $module_name = $params['f_module_name'];
            
            //Recupero i dati del vecchio WO
            $old_sel = Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],"t_selectors","f_code");        
            $old_creation = Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],"t_creation_date","f_id");        
            $old_custom = Mainsim_Model_Utilities::getOld($this->db,$params['f_code'],"t_custom_fields","f_code");        
            
            $olds = array(
                'old_table'=>$old_sel,
                'old_custom'=>$old_custom,
                'old_creation'=>$old_creation,                
            );

            if(!$params['f_phase_id']) {
                unset($params['f_phase_id']);
            }
            unset($params['f_id']);
            $f_code = $params['f_code'];        

            $custom_fields = $creation_fields = $edit_sel = [];
                        
            foreach ($params as $key => $param) {
                if(in_array($key,$creation_columns)) {
                    $creation_fields[$key] = Mainsim_Model_Utilities::clearChars($param);
                }
                if(in_array($key, $columns)) {
                    $edit_sel[$key] = Mainsim_Model_Utilities::clearChars($param);
                }            
                elseif(in_array($key, $custom_columns)) {
                    $custom_fields[$key] = Mainsim_Model_Utilities::clearChars($param);
                }
            }        
            $edit_sel['f_timestamp'] = $time;                    
            $custom_fields['f_timestamp'] = $time;
            $creation_fields['f_timestamp'] = $time;
            
            $this->db->update("t_creation_date",$creation_fields,"f_id = $f_code");
            $this->db->update("t_selectors",$edit_sel,"f_code = $f_code");
            $this->db->update("t_custom_fields",$custom_fields,"f_code = $f_code");
            
            
            $find = false;
            
            foreach($params as $key => $value) {  
                if(!is_array($value)) continue;
                $ftype = $value['f_type'];
                if(strpos($key,"t_selectors_parent_") !== false) {
                    $parentCodes = empty($value['f_code'])?array(0):$value['f_code'];                    
                    $checkIdenticalCross = Mainsim_Model_Utilities::checkIdenticalCross($parentCodes,'t_selectors_parent',$f_code,'f_code',[],'f_parent_code');
                    if(!$checkIdenticalCross) {
                        $this->db->delete("t_selectors_parent", "f_code = $f_code");
                        Mainsim_Model_Utilities::createParents('t_selectors_parent',$f_code,$value['f_code'],$this->db);
                    }
                    $new_parent = $value['f_code'];                    
                    sort($new_parent);                                        
                    $find = true;                    
                }
                if(isset($value['f_pair']) && is_array($value['f_pair'])) { 
                    if($ftype > 0 || strpos($ftype,',')!==false) {                         
                        $this->db->delete("t_pair_cross","f_code_main = {$params['f_code']} and f_code_cross IN (SELECT f_code from {$value['Ttable']} where f_type_id IN({$value['f_type']}) )");                        
                    }
                    elseif($ftype < 0) { // delete pair cross for periodic maintenance                                    
                        $this->db->delete("t_pair_cross","f_code_main = {$params['f_code']} and f_code_cross = {$value['f_type']}");
                    }
                    foreach($value['f_pair'] as $f_pair) {
                        if(is_null($f_pair)) continue;
                        $f_pair_params = [];
                        foreach($f_pair as $key_pair => $line_pair) {
                            if($key == '' || !in_array($key_pair, $pair_cols)) continue;
                            $f_pair_params[$key_pair] = Mainsim_Model_Utilities::clearChars($line_pair);
                        }                        
                        $f_pair_params['f_code_main'] = $f_code;                        
                        Mainsim_Model_Utilities::createPairCross($this->db,$f_pair_params,$value['pairCross']);                            
                    }                    
                }
                
            }
            if(!$find) {                
                $new_parent = array(0=>0);                
            }
            
            $scriptModule = new Mainsim_Model_Script($this->db,"t_selectors",$f_code,$params,$olds);            
            if(isset($creation_fields['f_phase_id'])){
                //check a change phase and some script to run.
                /*if($creation_fields['f_phase_id'] != $old_creation['f_phase_id']) {    
                    $sel->reset();
                    $res_phase = $sel->from("t_wf_exits")->where("f_phase_number = ?",$old_creation['f_phase_id'])
                            ->where("f_exit = ?",$creation_fields['f_phase_id'])->where("f_wf_id = ?",$creation_fields['f_wf_id'])->query()->fetch();                                                    
                    if(!empty($res_phase) && $res_phase['f_script'] != ""){                          
                        eval($res_phase['f_script']);
                    }
                }*/
                if($creation_fields['f_phase_id'] != $old_creation['f_phase_id']) {    
                    $sel->reset();
                    $res_phase = $sel->from(['t1'=>"t_wf_exits"],'f_script')
                        ->join(['t2'=>'t_wf_phases'],"t1.f_wf_id = t2.f_wf_id and t1.f_exit = t2.f_number",['f_group_id'])    
                        ->where("f_phase_number = ?",$old_creation['f_phase_id'])->where("f_exit = ?",$creation_fields['f_phase_id'])
                        ->where("t1.f_wf_id = ?",$creation_fields['f_wf_id'])->query()->fetch();
                    if(in_array($res_phase['f_group_id'],[6,7])) {
                        $activeChildren = Mainsim_Model_Utilities::checkActiveChildren("t_selectors",$f_code,$this->db);
                        if($activeChildren) { throw new Exception("Close child before close or delete the selector"); }
                    }
                    if(!empty($res_phase['f_script'])){ eval($res_phase['f_script']); }
                }
            }
            
            $fields_params = array_keys($params);                
            
            $scriptModule->t_selector_parent();//check change parent type            
            
            foreach($fields_params as $field) {      
                if(in_array($field,$mandatory_script)) continue;                                
                
                $sel->reset();                       
                $res_scr = $sel->from("t_scripts")->where("f_name = '$field-t_selectors'")->where("f_type = 'php'")->query()->fetch();                
                if(!empty($res_scr['f_script'])) { eval($res_scr['f_script']); }
                elseif(method_exists($scriptModule,$field) && is_callable(array($scriptModule,$field))) {//if not exist try to check if is a custom script in script model
                    $err_script = $scriptModule->$field();                    
                    if(isset($err_script['message'])) throw new Exception($err_script['message']);
                } 
            }            
            Mainsim_Model_Utilities::saveAttachments($f_code, 't_selectors', $params, $scriptModule);  
            
            if(!empty($params['attachments'])) {
                Mainsim_Model_Utilities::clearTempAttachments($params,$this->db);
            }
                         
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", array("f_timestamp"=>$time), "Edit");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_selectors", array("f_timestamp"=>$time), "Edit");
            Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", array("f_timestamp"=>$time), "Edit");                        
            if($commit) {
                if(empty($error)) { $this->db->commit(); }
                else { $this->db->rollBack(); return $error; }
            }
        }catch(Exception $e) {
           if($commit) {
                $this->db->rollBack();
           }
           $this->db->insert("t_logs",array("f_timestamp"=>$time,"f_log"=>"Something went wrong during selector edit: ".Mainsim_Model_Utilities::clearChars($e->getMessage()),"f_type_log"=>0));
           $error['message'] = "Something went wrong: ".Mainsim_Model_Utilities::clearChars($e->getMessage());
           return $error;
        }
        //write reverse ajax
        Mainsim_Model_Utilities::saveReverseAjax("t_selectors_parent", $f_code, $new_parent, "upd", $module_name,$this->db);
        return $error;
    }    
    
    public function cloneSelectors($clone_type = 0, $f_codes, $module_name, $fields = "") {
        if(is_string($f_codes)) $codes = explode(",", $f_codes);
        else $codes = $f_codes; $count = count($codes); $res = "";
        for($i=0; $i<$count; $i++) {
            // If it is a root element, we have to clone type too            
            $q = new Zend_Db_Select($this->db);
            $parents = $q->from("t_selectors_parent", array("f_parent_code"))->where("f_code = ?", $codes[$i])->where("f_parent_code = 0")->query()->fetchAll();
            if(count($parents) == 1) {
                $f_type_id = $this->cloneType($codes[$i]);
                if(is_array($f_type_id)) return $f_type_id; // Error
                $fields = array(
                    "t_creation_date" => array("f_category" => "SELECTOR $f_type_id"),
                    "t_item" => array("f_type_id" => $f_type_id)
                );
            }
            
            // Cloning object
            $f_ncode = $this->obj->cloneObject($codes[$i], $module_name, $fields); if(is_array($f_ncode)) return $f_ncode; // Error
            $res = $this->obj->cloneCross("t_selector_ware", "f_selector_id = {$codes[$i]}", array("f_selector_id" => $f_ncode)); if(is_array($res)) return $res; // Error
            $res = $this->obj->cloneCross("t_selector_wo", "f_selector_id = {$codes[$i]}", array("f_selector_id" => $f_ncode)); if(is_array($res)) return $res; // Error
            $res = $this->obj->cloneCross("t_selector_system", "f_selector_id = {$codes[$i]}", array("f_selector_id" => $f_ncode)); if(is_array($res)) return $res; // Error
            $res = $this->obj->clonePairCross($codes[$i], $f_ncode); if(is_array($res)) return $res; // Error
            
            // Cloning children recursively
            if($clone_type > 0) {
                $children = $this->obj->getChildren($codes[$i]); $c_count = count($children);                
                $fields["t_item_parent"]["f_parent_code"] = $f_ncode;
                for($j=0; $j<$c_count; $j++) {
                    $res = $this->cloneSelectors($clone_type, $children[$j], $module_name, $fields);
                    if(is_array($res)) return $res; // Error
                }
            }
        }
        return $res;
    }
    
    /**
     * cloneType
     * @param type $f_code_main
     * @param type $fields
     * @param type $f_ncode
     * @return boolean
     */
    private function cloneType($f_code) {
        $q = new Zend_db_select($this->db);
        $t_type = $q->from(array("t" => "{$this->table}_types"))->join(array("i" => $this->table), "t.f_id = i.f_type_id", [])->where("i.f_code = ?", $f_code)->query()->fetchAll();
        $q->reset();
        $last_id = $q->from("{$this->table}_types", array("f_id" => "MAX(f_id)"))->query()->fetchAll(); $last_id = $last_id[0]["f_id"]+1;
        $t_type[0]["f_type"] = "SELECTOR $last_id";
        $t_type[0]["f_sheet_name"] = "t_selector_$last_id";
        return $this->obj->cloneType($t_type[0]);
    }
    
    public function changePhaseBatch($f_codes, $f_wf_id, $f_exit, $f_module_name) {
        // security fix (match number,number,...)
        if(!preg_match("/^[0-9]{1,}(,[0-9]{1,})*$/", $f_codes)){ 
            die('params type input not valid');
        }
        return $this->obj->changePhase($f_codes, $f_wf_id, $f_exit, $f_module_name);
    }
}