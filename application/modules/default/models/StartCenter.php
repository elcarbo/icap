<?php

class Mainsim_Model_StartCenter
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));        
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function kpi1($params) {
        $select = new Zend_Db_Select($this->db);
        $select->reset();

        $val=$_POST['val'];

        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }

        $res = $select->from("t_workorders",array('n' => 'COUNT(f_type_id)','f_type_id'))->where("f_timestamp $range")->group("f_type_id")->query()->fetchAll();

        $wo_type=array(1=> '',4=>'',5=>'',6=>'',7=>'',10=>'',12=>'');
        $blist_type=array(2,3,8,9);
        $wo_tot=0;
        foreach($res as $ris){
                if(!in_array($ris["f_type_id"],$blist_type)){
                        $wo_type[$ris['f_type_id']]=$ris['n']; 
                        $wo_tot=$wo_tot+$wo_type[$ris['f_type_id']];
                }
        }

        $d1	 =	(int)$wo_type[1];
        $d4	 =	(int)$wo_type[4];
        $d5	 =	(int)$wo_type[5];
        $d6	 =	(int)$wo_type[6];
        $d7	 = 	(int)$wo_type[7];
        $d10 = 	(int)$wo_type[10];
        $d12 =  (int)$wo_type[12];

        $arr=array($d1,$d4,$d5,$d6,$d7,$d10,$d12);
        for($i=0;$i<7;$i++) if($arr[$i]=='') $arr[$i]=0;

        return $arr;
    }
    
    public function kpi2($params) {        
        $select = new Zend_Db_Select($this->db);
        $select->reset();
        $val=$_POST['val'];

        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }
                
               /* $select->from('t_creation_date', array())
                        ->joinLeft('t_workorders','t_workorders.f_code = t_creation_date.f_id', array())
                        ->joinLeft('t_wf_phases', 't_wf_phases.f_wf_id = t_creation_date.f_wf_id AND t_creation_date.f_phase_id = t_wf_phases.f_number', array())
                        ->joinLeft('t_wf_groups', 't_wf_groups.f_id = t_wf_phases.f_group_id', array('f_group', 'COUNT(*)'))
                        ->group('t_wf_groups.f_id')
                        ->where('t_workorders.f_type_id NOT IN (14,18)')
                        ->where("t_workorders.f_timestamp " . $range);*/
                
                $select->from('t_wf_groups', array('f_group', 'COUNT(*) AS tot'))
                        ->join('t_wf_phases', 't_wf_groups.f_id = t_wf_phases.f_group_id', array())
                        ->join('t_creation_date', 't_wf_phases.f_wf_id = t_creation_date.f_wf_id AND t_creation_date.f_phase_id = t_wf_phases.f_number', array())
                        ->join('t_workorders','t_workorders.f_code = t_creation_date.f_id', array())
                        ->group('t_wf_groups.f_id')
                        ->where('t_workorders.f_type_id NOT IN (14,18,3,9,11,19)')
                        ->where("t_workorders.f_timestamp " . $range);
                
                $res = $select->query()->fetchAll();
                //print_r($res);
                
                $groups = [
                    "Requested" => 0,
                    "Approval" => 0,
                    "Execution" => 0,
                    "Completion" => 0,
                    "Suspention" => 0,
                    "Closing" => 0,
                    "Deleting" => 0
                ];
                
                for($i = 0; $i < count($res); $i++){
                    $groups[$res[$i]['f_group']] = (int)$res[$i]['tot'];
                }
                return array_values($groups);
               /* print_r($groups); die();
                

                $d1	=	(int)$wo_phase[1];
                $d2	=	(int)$wo_phase[2];
                $d3	=	(int)$wo_phase[3];
                $d4	=	(int)$wo_phase[4];
                $d5	= 	(int)$wo_phase[5];
                $d6= 	(int)$wo_phase[6];
                $d7= 	(int)$wo_phase[7];
	
	return array($d1,$d2,$d3,$d4,$d5,$d6,$d7);       */ 
    }
    
    public function kpi3($params) {
                
        $select = new Zend_Db_Select($this->db);
        $params = json_decode($params['form2'],true);
        
        $year = isset($params['yr']) && !empty($params['yr'])?$params['yr'] :date('Y') ;
        $month = isset($params['mnt']) && !empty($params['mnt'])?$params['mnt'] :date('m') ;

        $end_day = date('d',mktime(0,0,0,$month+1,0,$year));

        $arr = array();
        for($i = 1; $i <= $end_day; ++$i) {
            $arr[$i] = 0;
            $select->reset();
            $start_date = mktime(0,0,0,$month,$i,$year);
            $end_date = mktime(23,59,59,$month,$i,$year);
            try {
            $res = $select->from(array("t1"=>"t_workorders"))
                    ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array("punteggio"=>"SUM(punteggio)"))
                    ->join(array("t3"=>"t_wf_phases"),"t3.f_number = t1.f_phase_id and t1.f_wf_id = t3.f_wf_id",array())
                    ->join(array("t4"=>"t_wf_groups"),"t3.f_group_id = t4.f_id",array())                                
                    ->where("t1.f_type_id = 1")            
                    ->where("f_timestamp between $start_date and $end_date")
                    ->where("t4.f_id = 6")
                    ->query()->fetchAll();
            }catch(Exception $e) {};
            if(!empty($res)) $arr[$i] =(int)$res[0]['punteggio'];    

        }
        return $arr;
    }
    
    public function kpi4($params) {
        $select = new Zend_Db_Select($this->db);
        $tipologia = array(
            "Avarie",
            "Consumabili",
            "Miglioramento prodotto","Pulizie pitturazioni e vernici",
            "Comfort e arredi",
            "Scadenze di legge",
            "Acquisti vari"
        );
        $params = json_decode($params['form2'],true);
        
        $val = $params['val'];
        $start_date = mktime(0,0,0,1,1,$val);        
        $end_date = mktime(23,59,59,12,31,$val);        
        
        $pos = 0;        
        $limit = 0;
        $data = array();
        for($i = 1; $i < 54;++$i) {
            $limit += (int)(60*60*24*(7*$i));            
            foreach($tipologia as $tipo_rit) {
                $select->reset();
                $select->from(array("t1"=>"t_workorders"),array("num"=>'count(t1.f_code)'))
                    ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array())                    
                    ->join(array("t4"=>"t_wf_phases"),"t4.f_number = t1.f_phase_id and t1.f_wf_id = t4.f_wf_id",array())
                    ->join(array("t5"=>"t_wf_groups"),"t5.f_id = t4.f_group_id",array())                    
                    ->where("t1.f_type_id = 1")->where("t5.f_id = 6")->where("rit_tipologia_intervento = ?",$tipo_rit)
                    ->where("t2.rit_data_gi between $start_date and $end_date ");                
                if($i < 52) {
                    $select->where("(t2.rappit_data_gf - t2.rit_data) between $pos and $limit ");
                }    
                else {
                    $select->where("(t2.rappit_data_gf - t2.rit_data) > $pos ");
                }
                $res = $select->query()->fetchAll();
                $data[$i][] = $res[0]['num'];
            }
            $pos = $limit;
            $pos++;
        }
        return $data;
    }
    
    public function kpi5($params) {
        $select = new Zend_Db_Select($this->db);
        $tipologia = array(
            "Avarie",
            "Consumabili",
            "Miglioramento prodotto","Pulizie pitturazioni e vernici",
            "Comfort e arredi",
            "Scadenze di legge",
            "Acquisti vari"
        );

        $pos = 0;
        $data = array();
        for($i = 1; $i <= 16;++$i) {    
            $trim = $i.'T';
            foreach($tipologia as $tipo_rit) {
                $select->reset();
                $select->from(array("t1"=>"t_workorders"),array("num"=>'count(t1.f_code)'))
                    ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array())
                    ->join(array("t3"=>"t_creation_date"),"t3.f_id = t1.f_code",array())               
                    ->join(array("t4"=>"t_wf_phases"),"t4.f_number = t1.f_phase_id and t1.f_wf_id = t4.f_wf_id",array())
                    ->join(array("t5"=>"t_wf_groups"),"t5.f_id = t4.f_group_id",array())                    
                    ->where("t1.f_type_id = 1")
                    ->where("t5.f_id = 6")
                    ->where("rappit_trimestre = ?",$trim)->where("rit_tipologia_intervento = ?",$tipo_rit);                
                $res = $select->query()->fetchAll();
                $data[$trim][] = $res[0]['num'];
            }
            $pos = $limit;
            $pos++;
        }
        return $data;
    }
    
    public function kpi6($params) {
        $select = new Zend_Db_Select($this->db);
        $select->reset();

        $form=$_POST['form2'];

        $form2=json_decode($form);
        //var_dump($form2);
        $val=$form2->val;
        $phases=$form2->phase;

        switch($val) {
            case 1: //OGGI -- OK
                    $range = "between ".mktime(0,0,0)." and ".time();
            break;
            case 2: //IERI -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
            break;
            case 3: // QUESTA SETTIMANA -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
            break;
            case 4: // SETTIMANA PRECEDENTE -- OK
                $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
            break;
            case 5: //QUESTO MESE
                $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
            break;
            case 6: //MESE PRECEDENTE
                $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
            break;
            case 7: //ANNO CORRENTE
                $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
            break;
            case 8: //ANNO PRECEDENTE
                $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
            break;
        }

        switch($phases){
                case 0:
                                $phase="tw.f_phase_id>0";
                                break;
                case 1:
                                $phase="tw.f_phase_id=1";
                                break;
                case 2:
                                $phase="tw.f_phase_id=2";
                                break;
                case 3:
                                $phase="tw.f_phase_id=3";
                                break;
                case 4:
                                $phase="tw.f_phase_id=4";
                                break;
                case 5:
                                $phase="tw.f_phase_id=5";
                                break;
                case 6:
                                $phase="tw.f_phase_id=6";
                                break;
                case 7:
                                $phase="tw.f_phase_id=7";
                                break;
                case 8:
                                $phase="tw.f_phase_id=8";
                                break;
                case 9:
                                $phase="tw.f_phase_id=9";
                                break;
                case 10:
                                $phase="tw.f_phase_id=10";
                                break;
                case 11:
                                $phase="tw.f_phase_id=11";
                                break;
                }
                
                $tipologia = array(
                    "Avarie",
                    "Consumabili",
                    "Miglioramento prodotto","Pulizie pitturazioni e vernici",
                    "Comfort e arredi",
                    "Scadenze di legge",
                    "Acquisti vari"
                );
                $arr = array();
                
                foreach($tipologia as $tipo) {
                    $select->reset();
                    $res = $select->from(array("tw"=>"t_workorders"),array())
                        ->join(array('tc'=>'t_custom_fields'),"tw.f_code=tc.f_code",array('n' => "COUNT(tc.rit_tipologia_intervento)"))
                        ->join(array('t3' => 't_wf_phases'),"tw.f_phase_id=t3.f_number and tw.f_wf_id=t3.f_wf_id",array("f_phase_name" => "f_name"))
                        ->where("tc.rit_data_gi $range")                        
                        ->where("tw.f_type_id=1")
                        ->where("t3.f_wf_id = 2")
                        ->where("tc.rit_tipologia_intervento = ?",$tipo)
                        ->where("$phase")
                        ->group("tw.f_phase_id")
                        ->order('n')
                        ->query()
                        ->fetchAll();
                    
                    $arr[] = (int)$res[0]['n'];
                }
        
        return $arr;
    }
    
    public function kpi7($params) {
        $select = new Zend_Db_Select($this->db);
        $select->reset();

        $form=$_POST['form2'];

        $form2=json_decode($form);
        $val=$form2->val;



        switch($val) {
            case 1: //OGGI -- OK
                    $range = "between ".mktime(0,0,0)." and ".time();
            break;
            case 2: //IERI -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
            break;
            case 3: // QUESTA SETTIMANA -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
            break;
            case 4: // SETTIMANA PRECEDENTE -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
            break;
            case 5: //QUESTO MESE
                    $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
            break;
            case 6: //MESE PRECEDENTE
                    $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
            break;
            case 7: //ANNO CORRENTE
                    $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
            break;
            case 8: //ANNO PRECEDENTE
                    $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
            break;
            default : $range = "between ".mktime(0,0,0)." and ".time();
                break;
        }
        $tipologia = array(
            "Avarie",
            "Consumabili",
            "Miglioramento prodotto","Pulizie pitturazioni e vernici",
            "Comfort e arredi",
            "Scadenze di legge",
            "Acquisti vari"
        );
        $arr = array();
        for($i = 0; $i < 7;++$i) {
            $select->reset();
            try {
            $res = $select->from(array("tw"=>"t_workorders"),array())                
                ->join(array('tc'=>'t_custom_fields'),"tw.f_code=tc.f_code",array('n' => "COUNT(tc.eswbs_sap)")) 	//JOIN t_custom_fields on t_custom_fields.f_code=t_wares.f_code        
                ->join(array('t3'=>'t_wf_phases'),"tw.f_wf_id = t3.f_wf_id and t3.f_number = tw.f_phase_id",array())
                ->join(array('t4'=>'t_wf_groups'),"t3.f_group_id = t4.f_id",array())
                ->where("tw.f_timestamp $range")                
                ->where("tw.f_type_id=1")->where("t4.f_id = 6")
                ->where("tc.rit_tipologia_intervento = ?",$tipologia[$i])
                ->group("tw.f_phase_id")->order('n')
                ->query()->fetchAll();
            }catch(Exception $e) {
                echo $e->getMessage();die;
            }
            $arr[]=isset($res[0]['n'])?$res[0]['n']:0;
        }
        return $arr;
    }
    
    public function kpi8($params) {
        $select = new Zend_Db_Select($this->db);
        $select->reset();

        $form=$params['form2'];

        $form2=json_decode($form);
        $val=$form2->val;
        $phases=$form2->phase;



        switch($val) {
            case 1: //OGGI -- OK
                $range = "between ".mktime(0,0,0)." and ".time();
                break;
            case 2: //IERI -- OK
                $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
            case 3: // QUESTA SETTIMANA -- OK
                $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
            case 4: // SETTIMANA PRECEDENTE -- OK
                $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
            case 5: //QUESTO MESE
                $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
            case 6: //MESE PRECEDENTE
                $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
            case 7: //ANNO CORRENTE
                $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
            case 8: //ANNO PRECEDENTE
                $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
            default :  //default anno corrente
                $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
        }

        switch($phases){
            case 0:
                $phase="tw.f_phase_id>0";
                break;
            case 1:
                $phase="tw.f_phase_id=1";
                break;
            case 2:
                $phase="tw.f_phase_id=2";
                break;
            case 3:
                $phase="tw.f_phase_id=3";
                break;
            case 4:
                $phase="tw.f_phase_id=4";
                break;
            case 5:
                $phase="tw.f_phase_id=5";
                break;
            case 6:
                $phase="tw.f_phase_id=6";
                break;
            case 7:
                $phase="tw.f_phase_id=7";
                break;
            case 8:
                $phase="tw.f_phase_id=8";
                break;
            case 9:
                $phase="tw.f_phase_id=9";
                break;
            case 10:
                $phase="tw.f_phase_id=10";
                break;
            case 11:
                $phase="tw.f_phase_id=11";
                break;
            default : $phase="tw.f_phase_id>0"; // default tutti
                break;
        }
        $arr = array();
        for($i = 1;$i<=7;++$i) {
            $select->reset();
            $res = $select->from(array("tw"=>"t_workorders"),array())                
                        ->join(array('ww'=>'t_ware_wo'),"ww.f_wo_id=tw.f_code",array()) //JOIN t_ware_wo on t_workorders.f_code=t_ware_wo.f_wo_id
                        ->join(array('twa'=>'t_wares'),"ww.f_ware_id=twa.f_code",array()) 			//JOIN t_wares on t_wares.f_code=t_workorders.f_code
                        ->join(array('tc'=>'t_custom_fields'),"tw.f_code=tc.f_code",array('n'=>"COUNT(tc.eswbs_sap)")) 	//JOIN t_custom_fields on t_custom_fields.f_code=t_wares.f_code
                        ->where("tc.rit_data_gi $range")
                        ->where("tw.f_wf_id = 2")                        
                        ->where("tw.f_type_id=1")                        
                        ->where("twa.f_code IN (
                                select t1.f_code from t_wares as t1 
                                join t_custom_fields as t2 on t1.f_code = t2.f_code 
                                where t2.eswbs_sap like '006090-$i%' and t1.f_type_id = 1)"
                                )
                        ->where("$phase")
                        ->order('n')->query()->fetchAll();
            
            $arr[] = isset($res[0]['n'])?(int)$res[0]['n']:0;

        }

        return $arr;
    }
    
    public function kpi9($params) {
        $select = new Zend_Db_Select($this->db);
        $select->reset();

        $form=$params['form2'];

        $form2=json_decode($form);
        $val=$form2->val;
        $phases=$form2->phase;



        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }

        switch($phases){
                case 0:
                                $phase="tw.f_phase_id>0";
                                break;
                case 1:
                                $phase="tw.f_phase_id=1";
                                break;
                case 2:
                                $phase="tw.f_phase_id=2";
                                break;
                case 3:
                                $phase="tw.f_phase_id=3";
                                break;
                case 4:
                                $phase="tw.f_phase_id=4";
                                break;
                case 5:
                                $phase="tw.f_phase_id=5";
                                break;
                case 6:
                                $phase="tw.f_phase_id=6";
                                break;
                case 7:
                                $phase="tw.f_phase_id=7";
                                break;
                case 8:
                                $phase="tw.f_phase_id=8";
                                break;
                case 9:
                                $phase="tw.f_phase_id=9";
                                break;
                case 10:
                                $phase="tw.f_phase_id=10";
                                break;
                case 11:
                                $phase="tw.f_phase_id=11";
                                break;
                }

        $wo_type=array(0=>"",1=>"",2=>"",3=>"",4=>"");
        $res = $select->from(array("tw"=>"t_workorders"),array('n' => "COUNT(tc.rit_livello)"))
                        ->join(array('tc'=>'t_custom_fields'),"tw.f_code=tc.f_code",array('tip'=>'tc.rit_livello'))
                        ->where("tw.f_timestamp $range")                        
                        ->where("tw.f_type_id=1")
                        ->where("tw.f_wf_id = 2")   
                        ->where("$phase")                
                        ->group("tw.f_phase_id")
                        ->order('n')
                        ->query()
                        ->fetchAll();

        $i=0;
        $wo_tot=0;
        $wo_type=array();
        for($k=0;$k<5;$k++){

                        $wo_type[$k]=$res[$k]['n'];
                        if($wo_type[$k]=="")$wo_type[$k]=0;
                        $wo_tot+=$wo_type[$k];
                        }

        $d1	 =	intval($wo_type[0]);
        $d2a	 =	intval($wo_type[1]);
        $d2b	 =	intval($wo_type[2]);
        $d3	 =	intval($wo_type[3]);
        $d4	 =  intval($wo_type[4]);


        $arr=array($d1,$d2a,$d2b,$d3,$d4);

        for($i=0;$i<5;$i++) if($arr[$i]=='') $arr[$i]=0;
        return $arr;
    }
    
    public function kpi10($params) {
        $select = new Zend_Db_Select($this->db);
        $tipologia = array(
            "Avarie",
            "Consumabili",
            "Miglioramento prodotto","Pulizie pitturazioni e vernici",
            "Comfort e arredi",
            "Scadenze di legge",
            "Acquisti vari"
        );
        
        $form=$params['form2'];

        $form2=json_decode($form);
        $val=$form2->val;
        $phases=$form2->phase;



        switch($val) {
            case 1: //OGGI -- OK
                    $range = "between ".mktime(0,0,0)." and ".time();
            break;
            case 2: //IERI -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
            break;
            case 3: // QUESTA SETTIMANA -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
            break;
            case 4: // SETTIMANA PRECEDENTE -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
            break;
            case 5: //QUESTO MESE
                    $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
            break;
            case 6: //MESE PRECEDENTE
                    $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
            break;
            case 7: //ANNO CORRENTE
                    $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
            break;
            case 8: //ANNO PRECEDENTE
                    $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
            break;
        }

        switch($phases){
            case 0:
                            $phase="t1.f_phase_id>0";
                            break;
            case 1:
                            $phase="t1.f_phase_id=1";
                            break;
            case 2:
                            $phase="t1.f_phase_id=2";
                            break;
            case 3:
                            $phase="t1.f_phase_id=3";
                            break;
            case 4:
                            $phase="t1.f_phase_id=4";
                            break;
            case 5:
                            $phase="t1.f_phase_id=5";
                            break;
            case 6:
                            $phase="t1.f_phase_id=6";
                            break;
            case 7:
                            $phase="t1.f_phase_id=7";
                            break;
            case 8:
                            $phase="t1.f_phase_id=8";
                            break;
            case 9:
                            $phase="t1.f_phase_id=9";
                            break;
            case 10:
                            $phase="t1.f_phase_id=10";
                            break;
            case 11:
                            $phase="t1.f_phase_id=11";
                            break;
            }


        $data = array();
        foreach($tipologia as $tipo_rit) {
            $select->reset();
            $select->from(array("t1"=>"t_workorders"),array("num"=>'count(t1.f_code)'))
                ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array())                                
                ->where("t1.f_type_id = 1")->where("t1.f_wf_id = 2")      
                ->where("$phase")->where("t1.f_timestamp $range")
                ->where("rit_tipologia_intervento = ?",$tipo_rit);            
            $res = $select->query()->fetchAll();
            $data[0][] = $res[0]['num'];
        }
        $pos = $limit;
        $pos++;
        return $data;
    }
    
    public function getWoDataForPie($type) {
        $select = new Zend_Db_Select($this->db);
        
        $res = $select->from(array("t1"=>"t_workorders"), array('f_phase_id','f_start_date', 'f_end_date'))
        ->join(array("t2"=>"t_custom_fields"),"t1.f_code=t2.f_code",array("fc_wo_starting_date","fc_wo_ending_date"))
        ->join(array("t3"=>"t_creation_date"),"t1.f_code=t3.f_id",array("f_creation_date"))        
        ->where("t1.f_type_id=?", $type)        
        ->where("t1.f_wf_id=2")
        ->query()->fetchAll();

        $chiusi = 0;
        $ritardo= 0; //chiusi in ritardo
        $aperti = 0;

        foreach($res as $ris){
                $diff=($ris['fc_wo_ending_date']-$ris['f_creation_date'])/86400;
                if($diff<=3 and $ris['f_phase_id']==6)
                        $chiusi++;
                if($diff>3 and $ris['f_phase_id']==6 )
                        $ritardo++;
                $diffopen=time()-$ris['f_creation_date'];
                if($diffopen > 3 and ($ris['f_phase_id']!=6 and $ris['f_phase_id']!=7))
                        $aperti++;
        }
        $arr=array($chiusi,$ritardo,$aperti);
        
        return $arr;
    }
    
    public function getWoDataForBarChart($type) {
        $select = new Zend_Db_Select($this->db);
        
        $query = "select CAST((c.fc_wo_ending_date - d.f_creation_date)/(60*60*24) AS SIGNED) as closing_days, count(CAST((c.fc_wo_ending_date - d.f_creation_date)/(60*60*24) AS SIGNED)) as num
        from t_workorders w join t_custom_fields c on w.f_code = c.f_code join t_creation_date d on c.f_code = d.f_id
        where w.f_type_id = $type and w.f_phase_id = 6 and c.fc_wo_ending_date > 0
        group by closing_days";

        $res = $this->db->query($query)->fetchAll();
        $max=0;

        foreach($res as $ris) {
            if($max < $ris['closing_days'])
                $max=$ris['closing_days'];
            $val[$ris['closing_days']] = $ris['num'];
        }

        return array($max,$val);
    }
    
    public function kpi11($params) {
        return $this->getWoDataForPie(1);
    }
    
    public function kpi12($params) {
        return $this->getWoDataForBarChart(1);
    }

    public function kpi13($params) {
        return $this->getWoDataForPie(4);
    }
    
    public function kpi14($params) {
        return $this->getWoDataForBarChart(4);
    }
    
    public function kpi15($params) {
        
    }
    
    public function kpi16($params) {
        
    }
    
    public function getWoByType1($params) {
        $select = new Zend_Db_Select($db);
        $select->reset();

        $val = $params["val"];
        
        switch($val) {
            case 1: //OGGI -- OK
                    $range = "between ".mktime(0,0,0)." and ".time();
            break;
            case 2: //IERI -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
            break;
            case 3: // QUESTA SETTIMANA -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
            break;
            case 4: // SETTIMANA PRECEDENTE -- OK
                    $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
            break;
            case 5: //QUESTO MESE
                    $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
            break;
            case 6: //MESE PRECEDENTE
                    $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
            break;
            case 7: //ANNO CORRENTE
                    $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
            break;
            case 8: //ANNO PRECEDENTE
                    $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
            break;
        }

        $res = $select->from("t_workorders",array('n' => 'COUNT(f_type_id)','f_type_id'))->where("f_timestamp $range")->group("f_type_id")->query()->fetchAll();

        $wo_type=array(1=> '',4=>'',5=>'',6=>'',7=>'',10=>'',12=>'');
        $blist_type=array(2,3,8,9);
        $wo_tot=0;
        foreach($res as $ris){
                if(!in_array($ris["f_type_id"],$blist_type)){
                        $wo_type[$ris['f_type_id']]=$ris['n']; 
                        $wo_tot=$wo_tot+$wo_type[$ris['f_type_id']];
                }
        }

        $d1	 =	(int)$wo_type[1];
        $d4	 =	(int)$wo_type[4];
        $d5	 =	(int)$wo_type[5];
        $d6	 =	(int)$wo_type[6];
        $d7	 = 	(int)$wo_type[7];
        $d10 = 	(int)$wo_type[10];
        $d12 =  (int)$wo_type[12];

        $arr=array($d1,$d4,$d5,$d6,$d7,$d10,$d12);
        for($i=0;$i<7;$i++) if($arr[$i]=='') $arr[$i]=0;
        return $data;
    }
    
    public function getWoByType6($params) {
        $select = new Zend_Db_Select($db);
        $select->reset();

        $form = $params['form2'];
        $form2 = json_decode($form);
        //var_dump($form2);
        $val=$form2->val;
        $phases=$form2->phase;

        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }

        switch($phases){
                case 0:
                                $phase="tw.f_phase_id>0";
                                break;
                case 1:
                                $phase="tw.f_phase_id=1";
                                break;
                case 2:
                                $phase="tw.f_phase_id=2";
                                break;
                case 3:
                                $phase="tw.f_phase_id=3";
                                break;
                case 4:
                                $phase="tw.f_phase_id=4";
                                break;
                case 5:
                                $phase="tw.f_phase_id=5";
                                break;
                case 6:
                                $phase="tw.f_phase_id=6";
                                break;
                case 7:
                                $phase="tw.f_phase_id=7";
                                break;
                case 8:
                                $phase="tw.f_phase_id=8";
                                break;
                case 9:
                                $phase="tw.f_phase_id=9";
                                break;
                case 10:
                                $phase="tw.f_phase_id=10";
                                break;
                case 11:
                                $phase="tw.f_phase_id=11";
                                break;
                }

        $wo_type=array(0=>"",1=>"",2=>"",3=>"",4=>"",5=>"",6=>"",7=>"");
        $res = $select->from(array("tw"=>"t_workorders"),array('n' => "COUNT(tc.rit_tipologia_intervento)"))
                        ->join(array('tc'=>'t_custom_fields'),"tw.f_code=tc.f_code",array('tip'=>'tc.rit_tipologia_intervento'))
                        ->join(array('t3' => 't_wf_phases'),"tw.f_phase_id=t3.f_number and tw.f_wf_id=t3.f_wf_id",array("f_phase_name" => "f_name"))
                        ->where("tw.f_timestamp $range")                        
                        ->where("tw.f_type_id=1")
                        ->where("$phase")
                        ->group("tw.f_phase_id")
                        ->order('n')
                        ->query()
                        ->fetchAll();

        $i=0;
        $wo_tot=0;
        for($k=0;$k<8;$k++){

                        $wo_type[$k]=$res[$k]['n'];
                        if($wo_type[$k]=="")$wo_type[$k]=0;
                        $wo_tot+=$wo_type[$k];

                        }

        $d0	 =	intval($wo_type[0]);
        $d1	 =	intval($wo_type[1]);
        $d2	 =	intval($wo_type[2]);
        $d3	 =	intval($wo_type[3]);
        $d4	 =  intval($wo_type[4]);
        $d5 = 	intval($wo_type[5]);
        $d6 =   intval($wo_type[6]);
        $d7 =   intval($wo_type[7]);

        $arr=array($d0,$d1,$d2,$d3,$d4,$d5,$d6,$d7);

        for($i=0;$i<8;$i++) if($arr[$i]=='') $arr[$i]=0;
        return $arr;
    }
    
    public function getWoByStructure($params) {
        $select = new Zend_Db_Select($db);
        $select->reset();

        $form=$params['form2'];

        $form2=json_decode($form);
        $val=$form2->val;
        $phases=$form2->phase;



        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }

        switch($phases){
                case 0:
                                $phase="tw.f_phase_id>0";
                                break;
                case 1:
                                $phase="tw.f_phase_id=1";
                                break;
                case 2:
                                $phase="tw.f_phase_id=2";
                                break;
                case 3:
                                $phase="tw.f_phase_id=3";
                                break;
                case 4:
                                $phase="tw.f_phase_id=4";
                                break;
                case 5:
                                $phase="tw.f_phase_id=5";
                                break;
                case 6:
                                $phase="tw.f_phase_id=6";
                                break;
                case 7:
                                $phase="tw.f_phase_id=7";
                                break;
                case 8:
                                $phase="tw.f_phase_id=8";
                                break;
                case 9:
                                $phase="tw.f_phase_id=9";
                                break;
                case 10:
                                $phase="tw.f_phase_id=10";
                                break;
                case 11:
                                $phase="tw.f_phase_id=11";
                                break;
                }

        $res = $select->from(array("tw"=>"t_workorders"),array('n' => "COUNT(tc.eswbs_sap)"))
                        ->join(array('tc'=>'t_custom_fields'),"twa.f_code=tc.f_code") 	//JOIN t_custom_fields on t_custom_fields.f_code=t_wares.f_code
                        ->join(array('twa'=>'t_wares'),"tw.f_code=twa.f_code") 			//JOIN t_wares on t_wares.f_code=t_workorders.f_code
                        ->join(array('ww'=>'t_ware_wo'),"ww.f_wo_id=tw.f_code") 		//JOIN t_ware_wo on t_workorders.f_code=t_ware_wo.f_wo_id
                        ->where("tw.f_timestamp $range")                        
                        ->where("tw.f_type_id=1")
                        ->where("tc.eswbs_sap=1%") //Having eswbs_sap="006090-1%"
                        ->where("$phase")
                        ->group("tw.f_phase_id")
                        ->order('n')
                        ->query()
                        ->fetchAll();

        $i=0;
        $wo_tot=0;
        $wo_type=array();
        for($k=0;$k<6;$k++){

                        $wo_type[$k]=$res[$k]['n'];
                        if($wo_type[$k]=="")$wo_type[$k]=0;
                        $wo_tot+=$wo_type[$k];
                        }

        $d1	 =	intval($wo_type[0]);
        $d2	 =	intval($wo_type[1]);
        $d3	 =	intval($wo_type[2]);
        $d4	 =	intval($wo_type[3]);
        $d5	 =  intval($wo_type[4]);
        $d6	 =  intval($wo_type[5]);


        $arr=array($d1,$d2,$d3,$d4,$d5,$d6);

        for($i=0;$i<6;$i++) if($arr[$i]=='') $arr[$i]=0;
        return $arr;
    }
    
    public function getWoByPriority($params) {
        $select = new Zend_Db_Select($db);
        $select->reset();

        $val=$params['val'];
        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }

	$res = $select->from("t_workorders",array('COUNT(f_priority)','f_priority'))->where("f_timestamp $range")->group("f_priority")->query()->fetchAll();
	$wo_priority=array(1=> '',2=>'',3=>'');
	
	//echo "Valori da DB - priority<br>";
	foreach($res as $ris){
			$wo_priority[$ris['f_priority']]=$ris['COUNT(f_priority)']; 

		
	}
	
	$d1	=	(int)$wo_priority[1];
	$d2	=	(int)$wo_priority[2];
	$d3	=	(int)$wo_priority[3];

	return array($d1, $d2, $d3);
    }
    
    public function getWoByLevel($params) {
        $select = new Zend_Db_Select($db);
        $select->reset();

        $form=$params['form2'];

        $form2=json_decode($form);
        $val=$form2->val;
        $phases=$form2->phase;



        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }

        switch($phases){
                case 0:
                                $phase="tw.f_phase_id>0";
                                break;
                case 1:
                                $phase="tw.f_phase_id=1";
                                break;
                case 2:
                                $phase="tw.f_phase_id=2";
                                break;
                case 3:
                                $phase="tw.f_phase_id=3";
                                break;
                case 4:
                                $phase="tw.f_phase_id=4";
                                break;
                case 5:
                                $phase="tw.f_phase_id=5";
                                break;
                case 6:
                                $phase="tw.f_phase_id=6";
                                break;
                case 7:
                                $phase="tw.f_phase_id=7";
                                break;
                case 8:
                                $phase="tw.f_phase_id=8";
                                break;
                case 9:
                                $phase="tw.f_phase_id=9";
                                break;
                case 10:
                                $phase="tw.f_phase_id=10";
                                break;
                case 11:
                                $phase="tw.f_phase_id=11";
                                break;
                }

        $wo_type=array(0=>"",1=>"",2=>"",3=>"",4=>"");
        $res = $select->from(array("tw"=>"t_workorders"),array('n' => "COUNT(tc.rit_livello)"))
                        ->join(array('tc'=>'t_custom_fields'),"tw.f_code=tc.f_code",array('tip'=>'tc.rit_livello'))
                        ->where("tw.f_timestamp $range")                        
                        ->where("tw.f_type_id=1")
                        ->where("$phase")
                        ->group("tw.f_phase_id")
                        ->order('n')
                        ->query()
                        ->fetchAll();

        $i=0;
        $wo_tot=0;
        $wo_type=array();
        for($k=0;$k<5;$k++){

                        $wo_type[$k]=$res[$k]['n'];
                        if($wo_type[$k]=="")$wo_type[$k]=0;
                        $wo_tot+=$wo_type[$k];
                        }

        $d1	 =	intval($wo_type[0]);
        $d2a	 =	intval($wo_type[1]);
        $d2b	 =	intval($wo_type[2]);
        $d3	 =	intval($wo_type[3]);
        $d4	 =  intval($wo_type[4]);


        $arr=array($d1,$d2a,$d2b,$d3,$d4);

        for($i=0;$i<5;$i++) if($arr[$i]=='') $arr[$i]=0;
        return $arr;
    }
    
    public function getWoByCost($params) {
        $select = new Zend_Db_Select($db);
        $select->reset();

        $form=$params['form2'];

        $form2=json_decode($form);
        $val=$form2->val;
        $phases=$form2->phase;



        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }

        switch($phases){
                case 0:
                                $phase="tw.f_phase_id>0";
                                break;
                case 1:
                                $phase="tw.f_phase_id=1";
                                break;
                case 2:
                                $phase="tw.f_phase_id=2";
                                break;
                case 3:
                                $phase="tw.f_phase_id=3";
                                break;
                case 4:
                                $phase="tw.f_phase_id=4";
                                break;
                case 5:
                                $phase="tw.f_phase_id=5";
                                break;
                case 6:
                                $phase="tw.f_phase_id=6";
                                break;
                case 7:
                                $phase="tw.f_phase_id=7";
                                break;
                case 8:
                                $phase="tw.f_phase_id=8";
                                break;
                case 9:
                                $phase="tw.f_phase_id=9";
                                break;
                case 10:
                                $phase="tw.f_phase_id=10";
                                break;
                case 11:
                                $phase="tw.f_phase_id=11";
                                break;
                }

        $res = $select->from(array("tw"=>"t_workorders"),array('n' => "COUNT(tc.eswbs_sap)"))
                        ->join(array('tc'=>'t_custom_fields'),"twa.f_code=tc.f_code") 	//JOIN t_custom_fields on t_custom_fields.f_code=t_wares.f_code
                        ->join(array('twa'=>'t_wares'),"tw.f_code=twa.f_code") 			//JOIN t_wares on t_wares.f_code=t_workorders.f_code
                        ->join(array('ww'=>'t_ware_wo'),"ww.f_wo_id=tw.f_code") 		//JOIN t_ware_wo on t_workorders.f_code=t_ware_wo.f_wo_id
                        ->where("tw.f_timestamp $range")                        
                        ->where("tw.f_type_id=1")
                        ->where("tc.eswbs_sap=1%") //Having eswbs_sap="006090-1%"
                        ->where("$phase")
                        ->group("tw.f_phase_id")
                        ->order('n')
                        ->query()
                        ->fetchAll();

        $i=0;
        $wo_tot=0;
        $wo_type=array();
        for($k=0;$k<7;$k++){

                        $wo_type[$k]=$res[$k]['n'];
                        if($wo_type[$k]=="")$wo_type[$k]=0;
                        $wo_tot+=$wo_type[$k];
                        }

        $d1	 =	intval($wo_type[0]);
        $d2	 =	intval($wo_type[1]);
        $d3	 =	intval($wo_type[2]);
        $d4	 =	intval($wo_type[3]);
        $d5	 =  intval($wo_type[4]);
        $d6	 =  intval($wo_type[5]);
        $d7	 =	intval($wo_type[6]);


        $arr=array($d1,$d2,$d3,$d4,$d5,$d6,$d7);

        for($i=0;$i<7;$i++) if($arr[$i]=='') $arr[$i]=0;
        
        return $arr;
    }
    
	public function kpi17($params){
		$select = new Zend_Db_Select($this->db);
 
		$arr = array();

			$start_date = mktime(0,0,0);
			$end_date = mktime(23,59,59);
			$x=0;
			$query = "SELECT cd.f_type,cd.f_creation_date
			FROM t_workorders wo
			JOIN t_creation_date cd on wo.f_code=cd.f_id
			WHERE wo.f_type_id=1 and cd.f_creation_date between $start_date and $end_date";
			$res = $this->db->query($query)->fetchAll();
		   for($k=0;$k<24;$k++){
			for($i=0;$i<count($res);$i++){
				if($res[$i]['f_creation_date']>=mktime($k,0,0) && $res[$i]['f_creation_date'] <= mktime($k+1,0,0))
					$x++;
					
			}
			//echo date("H ",mktime($k,0,0))." : ".date("H ",mktime($k+1,0,0))." | ";
			//echo $x;
			$arr[$k]=$x;
			$x=0;
		   }
		   //var_dump($arr);die;
		//$arr[0]=1;
		//$arr[1]=0;
		//for($k=0;$k<24;$k++){ $arr[$k] = rand(1,50) ;}
		return $arr;
	}
/*****************************************************/
	public function kpi18($params){
		$select = new Zend_Db_Select($this->db);
		$select->reset();
		$val=$_POST['val'];
		$range= "between ".mktime(0,0,0)." and ".mktime(24,0,0);
		/*switch($val) {
			case 1: //OGGI -- OK
				$range = "between ".mktime(0,0,0)." and ".time();
			break;
			case 2: //IERI -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
			break;
			case 3: // QUESTA SETTIMANA -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
			break;
			case 4: // SETTIMANA PRECEDENTE -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
			break;
			case 5: //QUESTO MESE
				$range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
			break;
			case 6: //MESE PRECEDENTE
				$range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
			break;
			case 7: //ANNO CORRENTE
				$range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
			break;
			case 8: //ANNO PRECEDENTE
				$range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
			break;
		}
*/
		$res = $select->from(array("t1"=>"t_wf_groups"),array('t3.f_code','t1.f_group',"t3.f_phase_id","group"=>"t2.f_group_id"))
		->join(array("t2"=>"t_wf_phases"), "t1.f_id=t2.f_group_id",array(""))
		->join(array("t3"=>"t_workorders"),"t3.f_phase_id=t2.f_number and  t2.f_wf_id=t3.f_wf_id",array(""))
		->join(array("cd"=>"t_creation_date"),"t3.f_code=cd.f_id")
		->where("cd.f_creation_date $range")->where("t3.f_type_id=1")->query()->fetchAll();
		//echo $select;die;
		
		$phase=array();
		for($i=0;$i<8;$i++) {$d1=$d2=$d3=$d4=$d6=$d7=0;}
		
		for($i=0;$i<count($res);$i++){
			//echo $res[$i]['group'];
			if($res[$i]['group'] == 1) $d1++;
			if($res[$i]['group'] == 2) $d2++;
			if($res[$i]['group'] == 3) $d3++;
			if($res[$i]['group'] == 4) $d4++;
			//if($res[$i]['group'] == 5) $d5++;
			if($res[$i]['group'] == 6) $d6++;
			if($res[$i]['group'] == 7) $d7++;
			
			//echo $res[$i]['f_group_id']."-";
		}
		/*$d1	 =	(int) $phase[0]; //apertura
		$d2	 =	(int) $phase[1]; //irrisolte
		$d3	 =	(int) $phase[2]; //supervisione
		$d4	 =	(int) $phase[3]; //attesa materiale
		$d5	 = 	(int) $phase[4]; //chiusura
		$d6 = 	(int) $phase[5]; //cancellazione
		*/
		//$d1=$d2=$d3=$d4=$d5=$d6=1;
		$arr=array($d1,$d2,$d3,$d4,$d6,$d7);
		return $arr;
		}
/*****************************************************/
	 public function kpi19($params){$select = new Zend_Db_Select($this->db);
$select->reset();

$val=$_POST['val'];

$range= "between ".mktime(0,0,0)." and ".mktime(24,0,0);
		/*switch($val) {
			case 1: //OGGI -- OK
				$range = "between ".mktime(0,0,0)." and ".time();
			break;
			case 2: //IERI -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
			break;
			case 3: // QUESTA SETTIMANA -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
			break;
			case 4: // SETTIMANA PRECEDENTE -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
			break;
			case 5: //QUESTO MESE
				$range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
			break;
			case 6: //MESE PRECEDENTE
				$range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
			break;
			case 7: //ANNO CORRENTE
				$range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
			break;
			case 8: //ANNO PRECEDENTE
				$range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
			break;
		}
*/

$res = $select->from(array("wo"=>"t_workorders"),array('n' => 'f_phase_mobile_status'))->join(array("cd"=>"t_creation_date"),"cd.f_id=wo.f_code",array(""))->where("cd.f_creation_date $range")->where("f_wf_id = 2")->where("f_phase_id=2")->query()->fetchAll();
//echo $select;die;
$taken=0;
$not_taken=0;

//var_dump($res);
//die;
foreach($res as $ris){
	if($ris['n'] == 0) $not_taken++;
	else $taken++;
	}

$d1	 =	$taken;
$d2	 =	$not_taken;


$arr=array($d1,$d2);
return $arr;
}
	
/*****************************************************/
	
	public function kpi20($params){$select = new Zend_Db_Select($this->db);
$select->reset();

$val=$_POST['val'];

$range= "between ".mktime(0,0,0)." and ".mktime(24,0,0);
		/*switch($val) {
			case 1: //OGGI -- OK
				$range = "between ".mktime(0,0,0)." and ".time();
			break;
			case 2: //IERI -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
			break;
			case 3: // QUESTA SETTIMANA -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
			break;
			case 4: // SETTIMANA PRECEDENTE -- OK
				$range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
			break;
			case 5: //QUESTO MESE
				$range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
			break;
			case 6: //MESE PRECEDENTE
				$range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
			break;
			case 7: //ANNO CORRENTE
				$range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
			break;
			case 8: //ANNO PRECEDENTE
				$range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
			break;
		}
*/
$res = $select->from(array("t1"=>"t_workorders"),array('n' => 'COUNT(t2.divisione)','t2.divisione'))
->join(array("t2"=>"t_custom_fields"),"t1.f_code=t2.f_code",array(""))
->where("t1.f_timestamp $range")
->where("t1.f_type_id=1")
->group("divisione")->query()->fetchAll();



foreach($res as $ris){
if($ris['divisione'] != "" && $ris['divisione'] != "Selezionare")
	$divisione[]=$ris['n'];
}


$d1	 = (int)	$divisione[0];
$d2	 =	(int) $divisione[1];
$d3	 =	(int) $divisione[2];
$d4	 =	(int) $divisione[3];
$d5	 = 	(int) $divisione[4];
$d6 = 	(int) $divisione[5];
$d7 = (int) $divisione[6];

$arr=array($d1,$d2,$d3,$d4,$d5,$d6,$d7);

return $arr;
}
/*****************************************************/
public function kpi21($params)
{
    $select = new Zend_Db_Select($this->db);
    $albergo = array(
        "Hotel Castello",
        "Hotel Borgo",
        "Hotel Dune",
            "Hotel Le Palme",
        "Hotel Pineta",
        "Villa del Parco",
        "Hotel Villaggio",
            "Beach Comber",
            "Boat House",
            "Cala del Forte",
            "Asset generico"
    );
    $range= "between ".mktime(0,0,0)." and ".mktime(24,0,0);
    $data = array();
    foreach($albergo as $hotel) {
        $select->reset();
        $res = $select->from(array("wo"=>"t_workorders"),array("num"=>"count(wo.f_title)"))            
            ->join(array("cd"=>"t_creation_date"),"cd.f_id=wo.f_code",array())
            ->join(array("ww"=>"t_ware_wo"),"ww.f_wo_id=wo.f_code",array())
            ->join(array("w"=>"t_wares"),"ww.f_ware_id = w.f_code",array())
            ->where("wo.f_type_id=1")->where("cd.f_creation_date $range") 
            ->where("wo.f_title LIKE ?",'%'.$hotel.'%')->query()->fetchAll();
            $data[0][] = $res[0]['num'];
    }
    $pos = $limit;
    $pos++;

    return $data;
}
/*****************************************************/
public function kpi22($params){
$select = new Zend_Db_Select($this->db);

$tipologia = array(
    "Diga",
    "Elettricisti1",
    "Elettricisti2",
    "Falegnami",
    "Idraulici",
    "Imbianchini",
	"TV e Telefoni"
);
$diga=array(0,0,0,0);
$ele1=array(0,0,0,0);
$ele2=array(0,0,0,0);
$fal=array(0,0,0,0);
$idr=array(0,0,0,0);
$imb=array(0,0,0,0);
$tvtel=array(0,0,0,0);
$dep=array(0,0,0,0);
$mur=array(0,0,0,0);
$fal2=array(0,0,0,0);
$mag=array(0,0,0,0);

$arr=array();

for($i=0;$i<4;$i++){
$query = 
"SELECT divisione
FROM t_workorders w
JOIN t_custom_fields c on w.f_code=c.f_code
WHERE w.f_type_id=1 and w.f_priority=($i+1) and (w.f_phase_id = 2 or w.f_phase_id=10) and divisione not IN ('','Selezionare')";
$res = $this->db->query($query)->fetchAll();

foreach ($res as $ris){
 if($ris['divisione']=="DEP (Cois Robertino)") $dep[$i]++;
if($ris['divisione']=="Elettricisti1 (Mascia)") $ele1[$i]++;
 if($ris['divisione']=="Elettricisti2 (Turtas Cristian)") $ele2[$i]++;
 if($ris['divisione']=="Falegnami (Tonnellato Lazzaro)") $fal[$i]++;
 if($ris['divisione']=="Falegnami2 (Boi Guido)") $fal2[$i]++;
 if($ris['divisione']=="Idraulici (Garau Davide)") $idr[$i]++;
 if($ris['divisione']=="Imbianchini (Piredda Ignazio)") $imb[$i]++;
  if($ris['divisione']=="Muratori (Lai Antonio)") $mur[$i]++;
   if($ris['divisione']=="Magazzino (Onorato Marcello)") $mag[$i]++;
 if($ris['divisione']=="Diga (Fanunza Emanuele)" || $ris['divisione']=="Officina (Fanunza Emanuele)") $diga[$i]++;
 if($ris['divisione']=="TV e Telefoni (Pischedda Antonio)") $tvtel[$i]++;
 



}
$arr[$i]=array($dep[$i],$ele1[$i],$ele2[$i],$fal[$i],$fal2[$i],$idr[$i],$imb[$i],$mur[$i],$mag[$i],$diga[$i],$tvtel[$i]);
}

return $arr;
}
	
	
    public function getWoByStatus($params) {
        $select = new Zend_Db_Select($db);
        $select->reset();
        $val=$params['val'];

        switch($val) {
                case 1: //OGGI -- OK
                        $range = "between ".mktime(0,0,0)." and ".time();
                break;
                case 2: //IERI -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-1)." and ".mktime(23,59,59,date('n'),date('j')-1);
                break;
                case 3: // QUESTA SETTIMANA -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1)." and ".time();
                break;
                case 4: // SETTIMANA PRECEDENTE -- OK
                        $range = "between ".mktime(0,0,0,date('n'),date('j')-date('N')+1 -7)." and ".mktime(23,59,59,date('n'),date('j')-date('N'));
                break;
                case 5: //QUESTO MESE
                        $range = "between ".mktime(0,0,0,date('n'),1)." and ".time();
                break;
                case 6: //MESE PRECEDENTE
                        $range = "between ".mktime(0,0,0,date('n') - 1,1)." and ".mktime(23,59,59,date('n'),0);
                break;
                case 7: //ANNO CORRENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y'))." and ".mktime(23,59,59,12,31,date('Y'));
                break;
                case 8: //ANNO PRECEDENTE
                        $range = "between ".mktime(0,0,0,1,1,date('Y')-1)." and ".mktime(23,59,59,12,31,date('Y')-1);
                break;
        }
	$res = $select->from("t_workorders",array('COUNT(f_phase_id)','f_phase_id'))->where("f_timestamp $range")->group("f_phase_id")->query()->fetchAll();
	$wo_phase=array(1=> '',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'');

	foreach($res as $ris){
			if($ris['f_phase_id']>0 && $ris['f_phase_id']<8){
				$wo_phase[$ris['f_phase_id']]=$ris['COUNT(f_phase_id)']; 
		}
	}
	
	$d1	=	(int)$wo_phase[1];
	$d2	=	(int)$wo_phase[2];
	$d3	=	(int)$wo_phase[3];
	$d4	=	(int)$wo_phase[4];
	$d5	= 	(int)$wo_phase[5];
	$d6= 	(int)$wo_phase[6];
	$d7= 	(int)$wo_phase[7];
	
	return array($d1, $d2, $d3, $d4, $d5, $d6, $d7);
    }
    
    public function getBulletins(){
        $select = $this->db->select()
        ->from(array('wo' => 't_workorders'),array('f_priority'))
        ->join(array('cd' => 't_creation_date'),'wo.f_code = cd.f_id',array('f_title', 'f_description','f_creation_date','fc_editor_user_name'))
        ->join(array('wa' => 't_wares'),'wo.f_user_id = wa.f_code',array())
        ->join(array('cu' => 't_users'),'wa.f_code = cu.f_code',array('cu.fc_usr_usn', 'cu.fc_usr_avatar'))
        ->where('wo.f_type_id = 14')->where('cd.f_phase_id = 1')->order('cd.f_creation_date DESC')
        ->limit(10);
        $result = $this->db->fetchAll($select);
        return $result;
    }   
}