<?php

class Mainsim_Model_Pdf {

    private $db;

    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        $this->select = new Zend_Db_Select($this->db);
    }

    public function __destruct() {
        $this->db->closeConnection();
    }

    public function printPdf(&$params) {
        $this->f_code = $params['f_code'];
        //$this->select = new Zend_Db_Select($this->db);
        try {
            // Recupero dati utente loggato per visibilità
            $auth = Zend_Auth::getInstance()->getIdentity();
            $user_level = $auth->f_level;
            $group_level = $auth->f_group_level;

            $wmax = $pdf->wmax;
            $lmarg = 10;
            $pdf = new C('P', 'mm', 'A4');
            $pdf->AddFont('Arial', '', 'OpenSans-Light.php');
            $pdf->AddFont('Arial', 'B', 'OpenSans-Regular.php');

            // Traduzione
            $traductor = new Mainsim_Model_Translate();
            $GLOBALS['trad'] = $traductor;

            // Recupero il tipo dell'f_code passato come parametro
            $this->select->reset();
            $res = $this->select->from("t_creation_date", ["f_type", "f_category"])
                            ->where("f_id = " . $params['f_code'])
                            ->query()->fetch();
            $category = (!empty($res) && isset($res['f_category'])) ? $res['f_category'] : "";
            if (empty($category)) {
                throw new Exception("No category");
            }

            $type = (!empty($res) && isset($res['f_type'])) ? $res['f_type'] : "";
            if (empty($type)) {
                throw new Exception("No type");
            }
            if ($type == "WARES") {
                $type = "ware";
            } else if ($type == "WORKORDERS") {
                $type = "wo";
            } else if ($type == "SELECTORS") {
                $type = "selector";
            }
            // Recupero modulo da categoria/tipo
            $module_name = $this->getModName($type, $category);
            //Recupero json da FS
            $settings = "";
            //Controllo prima se è stato definito un json custom, poi passo al default.
            $dir_json = SCRIPTS_PATH . "pdf/";
            if (file_exists($dir_json . "default.json")) {
                $settings = json_decode(file_get_contents($dir_json . "default.json"));
                if (empty($settings->$module_name)) {
                    throw new Exception("No settings in json");
                }
                $settings = $settings->$module_name;
            } else {
                $dir_json = SCRIPTS_PATH_DEFAULT . "pdf/";
                if (file_exists($dir_json . "default.json")) {
                    $settings = json_decode(file_get_contents($dir_json . "default.json"));
                    if (empty($settings->$module_name)) {
                        throw new Exception("No settings in json");
                    }
                    $settings = $settings->$module_name;
                } else {
                    throw new Exception($dir_json);
                }
            }
            // Controllo se uso le nuove picklist o no
            $this->select->reset();
            $res = $this->select->from("t_creation_date", ["f_description"])->where("f_title = 'NEW_PICKLISTS'")->where("f_category = 'SETTING'")->query()->fetch();
            $this->new_picklist = (!empty($res) && $res['f_description'] == 1) ? true : false;

            $fields = $this->selectFields($module_name, $settings->fields, $type);

            $fields_end = $this->getData($params["f_code"], $fields, $settings->type);
            $title = strtoupper("report " . $settings->title);
            $this->printFields($pdf, $fields_end, $title, $settings->rowelements);
            if (!empty($settings->subItems)) {
                foreach ($settings->subItems as $item => $setting) {
                    $module_name = $item;
                    $fields = $this->selectFields($module_name, $setting->fields, $settings->type);
                    $res_wo = $this->getCodes($params['f_code'], $setting->id, $setting->type, $settings->type);
                    if (count($res_wo) > 0) {
                        $this->printFieldsSubitems($pdf, $fields, $res_wo, $setting->title, $setting->type, $setting->rowelements, $setting->elementname);
                    }
                }
            }
        } catch (Exception $e) {
            die("Exception " . $e->getMessage());
        }
        return $pdf;
    }

    function getCodes($f_code, $filter, $type1, $type2) {
        $this->select->reset();
        if ($type2 == "ware") {
            if ($type1 == "wo") {
                $res_wo = $this->select->from(['t1' => 't_ware_wo'], ["f_wo_id AS f_code"])
                                ->join(['t2' => 't_workorders'], 't1.f_wo_id = t2.f_code', [])
                                ->where('t2.f_type_id in (' . $filter . ')')
                                ->where('t1.f_ware_id = ?', $f_code)
                                ->query()->fetchAll();
            }
            if ($type1 == "ware") {
                $res_wo = $this->select->from(['t1' => 't_wares_relations'], ['f_code_ware_slave AS f_code'])
                                ->join(['t2' => 't_wares'], 't1.f_code_ware_slave = t2.f_code', [])
                                ->where('t1.f_type_id_slave in (' . $filter . ')')
                                ->where('t1.f_code_ware_master=?', $f_code)
                                ->query()->fetchAll();
            }
            if ($type1 == "selector") {
                $res_wo = $this->select->from(['t1' => 't_selector_ware'], ['f_selector_id AS f_code'])
                                ->join(['t2' => 't_selectors'], 't1.f_selector_id = t2.f_code', [])
                                ->where('t1.f_ware_id=?', $f_code)
                                ->where('t2.f_type_id in (' . $filter . ')')
                                ->query()->fetchAll();
            }
        }
        if ($type2 == "wo") {
            if ($type1 == "ware") {
                $res_wo = $this->select->from(['t1' => 't_ware_wo'], ["f_ware_id AS f_code"])
                                ->join(['t2' => 't_wares'], 't1.f_ware_id = t2.f_code', [])
                                ->where('t2.f_type_id in (' . $filter . ')')
                                ->where('t1.f_wo_id = ?', $f_code)
                                ->query()->fetchAll();
            }
            if ($type1 == "wo") {
                $res_wo = $this->select->from(['t1' => 't_wo_relations'], ['f_code_wo_slave AS f_code'])
                                ->join(['t2' => 't_wares'], 't1.f_code_wo_slave = t2.f_code', [])
                                ->where('t1.f_type_id_wo_slave in (' . $filter . ')')
                                ->where('t1.f_code_wo_master=?', $f_code)
                                ->query()->fetchAll();
            }
            if ($type1 == "selector") {
                $res_wo = $this->select->from(['t1' => 't_selector_wo'], ['f_selector_id AS f_code'])
                                ->join(['t2' => 't_selectors'], 't1.f_selector_id = t2.f_code', [])
                                ->where('t1.f_wo_id=?', $f_code)
                                ->where('t2.f_type_id in (' . $filter . ')')
                                ->query()->fetchAll();
            }
        }
        return $res_wo;
    }

    function selectFields($module_name, $value, $type = "") {
        $this->select->reset();
        $res_edit_c1 = $this->select->from(["t1" => "t_ui_object_instances"])
                        ->where("f_instance_name='" . $module_name . "'")->query()->fetch();
        // Lista completa dei campi
        $fields_temp = json_decode($res_edit_c1['f_properties'], true);
        $fields_temp['fields'] = str_replace("|", ",", $fields_temp['fields']);
        $fields = explode(",", $fields_temp['fields']);
        // Per ogni campo prendo le sue proprietà dal DB
        $tot_fields = count($fields);
        $res_field_values = [];
        $field_v = [];
        for ($index_fields = 0; $index_fields < $tot_fields; $index_fields++) {
            $this->select->reset();
            $res_field_values[$index_fields] = $this->select->from(array("t1" => "t_ui_object_instances"))->where("f_instance_name='" . $fields[$index_fields] . "'")->query()->fetch();
            $field_v[$index_fields] = json_decode($res_field_values[$index_fields]["f_properties"], true);
            $field_v[$index_fields]['instance_name'] = $fields[$index_fields];
            $field_v[$index_fields]['f_type_id'] = $res_field_values[$index_fields]['f_type_id'];
        }
        $field_groups = [];

        // Filtro per selettori
        if (!empty($type)) {
            $this->select->reset();
            $selectors = $this->select->from(["t1" => "t_selector_" . $type], ["f_selector_id"])
                            ->where("f_" . $type . "_id='" . $this->f_code . "'")->query()->fetchAll();
        }
        // Divisione per groupName
        for ($k = 0; $k < $tot_fields; $k++) {
            if (!empty($field_v[$k]["v_selector"])) {
                //die(var_dump($field_v[$k]));
                foreach ($selectors as $index => $selector) {
                    if (strrpos($field_v[$k]["v_selector"], $selector['f_selector_id']) !== false) {
                        $field_groups[$field_v[$k]["groupName"]][$field_v[$k]["bind"]] = $field_v[$k];
                    }
                }
            } else {
                $field_groups[$field_v[$k]["groupName"]][$field_v[$k]["bind"]] = $field_v[$k];
            }
        }
        //die(var_dump($field_groups));
        // Tengo solo i campi che voglio vedere (scritti nel json)

        $fields_end = [];
        foreach ($value as $k => $v) {
            if (isset($field_groups[$k])) {
                if (empty($v)) { // Nel caso in cui non siano specificati campi prendo tutti quelli associati al group name
                    $fields_end[$k] = $field_groups[$k];
                    if ($k == "msim-default") {
                        // Aggiungo campi non presenti nell'editC1 ma utili
                        // DEFAULT: aggiungo stato
                        $fields_end['msim-default']['f_name']["label"] = "Stato";
                        $fields_end['msim-default']['f_name']["groupName"] = "msim-default";
                        $fields_end['msim-default']['f_name']["bind"] = "f_name";
                        $fields_end['msim-default']['f_name']["f_type_id"] = 6;
                        // DEFAULT: aggiungo Tipo
                        $fields_end['msim-default']['f_type']["label"] = "Tipo";
                        $fields_end['msim-default']['f_type']["groupName"] = "msim-default";
                        $fields_end['msim-default']['f_type']["bind"] = "f_type";
                        $fields_end['msim-default']['f_type']["f_type_id"] = 6;
                    }
                } else { // Prendo solo i campi specificati
                    foreach ($v as $k2 => $v2) {
                        // Controllo campi extra editc1 necessari
                        if ($v2 == "f_name") {
                            $fields_end['msim-default']['f_name']["label"] = "Stato";
                            $fields_end['msim-default']['f_name']["groupName"] = "msim-default";
                            $fields_end['msim-default']['f_name']["bind"] = "f_name";
                            $fields_end['msim-default']['f_name']["f_type_id"] = 6;
                            continue;
                        }
                        if ($v2 == "f_type") {
                            $fields_end['msim-default']['f_type']["label"] = "Tipo";
                            $fields_end['msim-default']['f_type']["groupName"] = "msim-default";
                            $fields_end['msim-default']['f_type']["bind"] = "f_type";
                            $fields_end['msim-default']['f_type']["f_type_id"] = 6;
                            continue;
                        }
                        $fields_end[$k][$v2] = $field_groups[$k][$v2];
                    }
                }
            }
        }
        
        //die(var_dump($fields_end));
        return $fields_end;
    }

    function printFields($pdf, $fields_end, $name, $rowelements = 4) {
        $lmarg = 10;
        $wmax = $pdf->wmax;
        //PDF - Aggiungo pagina
        $pdf->SetLineWidth(0.1);
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(1, 10);
        // REPORT
        $pdf->SetFillColor(180, 180, 180);
        $pdf->Rect($lmarg, $pdf->GetY(), $pdf->wmax, 8, 'F');
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell($pdf->wmax, 8, utf8_decode($GLOBALS['trad']->_($name)), 0, 1, 'C');
        $pdf->Ln(4);
        foreach ($fields_end as $k => $fieldset) {
            // Calcolo dimensioni e align
            $cellWidth = round($pdf->wmax / $rowelements, 0, PHP_ROUND_HALF_DOWN);
            $cellsWidth = [];
            $cellsAlignTitle = [];
            $cellsAlignValues = [];
            for ($j = 0; $j < $rowelements; $j++) {
                $cellsWidth[] = $cellWidth;
                $cellsAlignTitle[] = "C";
                $cellsAlignValues[] = "L";
            }


            // Intestazione fieldset, scrivo diverso se msim-default
            if ($k == "msim-default") {
                $fancybinds = ["fc_progress", "f_priority", "fc_hierarchy", "f_name"];
                $fancytitles = [];
                $fancyvalues = [];
                foreach ($fieldset as $k3 => $v3) {
                    if (in_array($v3['bind'], $fancybinds)) {
                        $fancytitles[] = utf8_decode($GLOBALS['trad']->_($v3['label']));
                        $fancyvalues[] = $this->getValue($v3);
                        unset($fieldset[$k3]);
                    }
                }
                $pdf->SetAligns($cellsAlignValues);
                $pdf->fancyRow2($fancytitles, $fancyvalues, [220, 220, 220], [255, 255, 255]);
            } else {
                $pdf->SetFillColor(220, 220, 220);
                $pdf->Rect($pdf->GetX(), $pdf->GetY(), $pdf->wmax, 5, 'F');
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->Cell($pdf->wmax, 5, $GLOBALS['trad']->_($k), 0, 1, 'C');
            }
            $pdf->Ln(3);
            // Divido le celle
            $i = 0;
            $rowtitles = [];
            $rowvalues = [];
            $titles = [];
            $values = [];
            foreach ($fieldset as $k2 => $v2) {
                if ($i < $rowelements) {
                    $i++;
                    $titles[] = utf8_decode($GLOBALS['trad']->_($v2['label']));
                    $values[] = $this->getValue($v2);
                } else {
                    $i = 1;
                    $rowtitles[] = $titles;
                    $rowvalues[] = $values;
                    $titles = [];
                    $values = [];
                    $titles[] = utf8_decode($GLOBALS['trad']->_($v2['label']));
                    $values[] = $this->getValue($v2);
                }
            }
            if (!empty($titles)) {
                $rowtitles[] = $titles;
                $rowvalues[] = $values;
            }
            // dai due array con i titoli e i valori costruisco le celle
            $pdf->SetWidths($cellsWidth);
            for ($k = 0; $k < count($rowtitles); $k++) {
                $pdf->SetAligns($cellsAlignTitle);
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Row2($rowtitles[$k], [245, 245, 245]);
                $pdf->SetAligns($cellsAlignValues);
                $pdf->SetFont('Arial', '', 8);
                $pdf->Row2($rowvalues[$k], [251, 251, 251]);
                $pdf->Ln(1.5);
            }
            $pdf->Ln(3);
        }
    }

    function printFieldsSubitems($pdf, $fields, $res_wo, $name, $type, $rowelements = 5, $elementname = "") {
        $lmarg = 10;
        //PDF - Aggiungo pagina
        $pdf->SetLineWidth(0.1);
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(1, 10);
        // REPORT
        $pdf->SetFillColor(180, 180, 180);
        $pdf->Rect($lmarg, $pdf->GetY(), $pdf->wmax, 8, 'F');
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell($pdf->wmax, 8, utf8_decode($GLOBALS['trad']->_($name)), 0, 1, 'C');
        $pdf->Ln(4);
        foreach ($res_wo as $k3 => $v3) {
            $fields_end = $this->getData($v3["f_code"], $fields, $type);
            // Nome 
            $pdf->SetFillColor(220, 220, 220);
            $pdf->Rect($pdf->GetX(), $pdf->GetY(), $pdf->wmax, 5, 'F');
            $pdf->SetFont('Arial', 'B', 12);
            // Se è presente l'id OdL lo scrivo anche nel titolo
            if (isset($fields_end['msim-default']['fc_progress'])) {
                $pdf->Cell($pdf->wmax, 5, utf8_decode($GLOBALS['trad']->_($elementname)) . " - " . sprintf("%05d", $fields_end['msim-default']['fc_progress']['value']), 0, 1, 'C');
            } else {
                $pdf->Cell($pdf->wmax, 5, utf8_decode($GLOBALS['trad']->_($elementname)), 0, 1, 'C');
            }
            $pdf->Ln(2.5);
            $rowtitles = [];
            $rowvalues = [];
            $titles = [];
            $values = [];
            $i = 0;
            foreach ($fields_end as $k => $fieldset) {
                // Intestazione fieldset
                /*
                  $pdf->SetFillColor(210, 210, 210);
                  $pdf->Rect($pdf->GetX(), $pdf->GetY(), $pdf->wmax, 5, 'F');
                  $pdf->SetFont('Arial', 'B', 12);
                  if ($k == "msim-default") {
                  $pdf->Cell($pdf->wmax, 5, "Generali", 0, 1, 'C');
                  } else {
                  $pdf->Cell($pdf->wmax, 5, $GLOBALS['trad']->_($k), 0, 1, 'C');
                  } */
                foreach ($fieldset as $k2 => $v2) {
                    if ($i < $rowelements) {
                        $i++;
                        $titles[] = utf8_decode($GLOBALS['trad']->_($v2['label']));
                        $values[] = $this->getValue($v2);
                    } else {
                        $i = 1;
                        $rowtitles[] = $titles;
                        $rowvalues[] = $values;
                        $titles = [];
                        $values = [];
                        $titles[] = utf8_decode($GLOBALS['trad']->_($v2['label']));
                        $values[] = $this->getValue($v2);
                    }
                }
            }
            if (!empty($titles)) {
                $rowtitles[] = $titles;
                $rowvalues[] = $values;
            }
            // dai due array con i titoli e i valori costruisco le celle
            $cellWidth = round($pdf->wmax / $rowelements, 0, PHP_ROUND_HALF_DOWN);
            $cellsWidth = [];
            $cellsAlignTitle = [];
            $cellsAlignValues = [];
            for ($j = 0; $j < $rowelements; $j++) {
                $cellsWidth[] = $cellWidth;
                $cellsAlignTitle[] = "C";
                $cellsAlignValues[] = "L";
            }
            $pdf->SetWidths($cellsWidth);
            for ($k = 0; $k < count($rowtitles); $k++) {
                $pdf->SetAligns($cellsAlignTitle);
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Row2($rowtitles[$k], [240, 240, 240]);
                $pdf->SetAligns($cellsAlignValues);
                $pdf->SetFont('Arial', '', 8);
                $pdf->Row2($rowvalues[$k], [250, 250, 250]);
                $pdf->Ln(1.5);
            }
            $pdf->Ln(5);
        }
    }

    function getValue($value) {
        
        if ($value['bind'] == "fc_progress")
            return sprintf("%05d", $GLOBALS['trad']->_($value['value']));
        switch ($value["f_type_id"]) {
            case 6:
                if ($value["text"]) {
                    //return "TEXTAREA";
                    return utf8_decode($GLOBALS['trad']->_($value['value']));
                } else if ($value["dtmpicker"]) {
                    return $value['value'] > 0 ? date("d-m-Y H:i", $value['value']) : "";
                } else if ($value["autocomplete"]) {
                    //return "AUTOCOMPLETE";
                    return utf8_decode($GLOBALS['trad']->_($value['value']));
                } else if ($value["pwd"]) {
                    return utf8_decode($GLOBALS['trad']->_($value['value']));
                    //return "PWD";
                } else if (strpos($value['checkExp'], "decimal")!==false) {
                    $decimal = explode("_", $value['checkExp']);
                    //return "costs";
                    return sprintf("%.".$decimal[1]."f",$value['value']);
                } else {
                    return utf8_decode($GLOBALS['trad']->_($value['value']));
                }

                break;

            case 7:
                if ($value["server"]) {
                    if ($value['bind'] == "f_priority") {
                        $options = new Mainsim_Model_OptionSelect();
                        foreach ($options->getWoPriorities() as $k4 => $v4) {
                            if ($v4['value'] == $value['value']) {
                                return utf8_decode($GLOBALS['trad']->_($v4['label']));
                            }
                        }
                        if ($value['value'] == 0) {
                            return "Nessuna";
                        }
                        return $value['value'];
                    }
                    //return "Dynamic-slct";
                    return utf8_decode($GLOBALS['trad']->_($value['value']));
                } else
                    $val = utf8_decode($GLOBALS['trad']->_($value['options'][$value["value"]]['label']));
                if (empty($val)) {
                    $val = utf8_decode($GLOBALS['trad']->_($value["value"]));
                }
                return $val;
                break;
            case 8:
                if ($value["radio"]) {
                    foreach ($value['buttons'] as $k5 => $v5) {
                        if ($v5['value'] == $value['value']) {
                            return utf8_decode($GLOBALS['trad']->_($v5['label']));
                        }
                    }
                    return utf8_decode($GLOBALS['trad']->_($value['value']));
                    //return "Chkbx-Radio";
                } else {
                    //return "Chkbx-Multi";
                    $val = str_replace(",", ", ", utf8_decode($GLOBALS['trad']->_($value['value'])));
                    return $val;
                }
                break;
            case 13:
                /* TODO: sistemare
                 * VECCHIO CODICE:
                 * $attach_path = APPLICATION_PATH . "/../attachments/img/";
                  $ath_arr = explode('|', $v2['value'], 2);
                  if ($ath_arr != FALSE && sizeof($ath_arr) == 2) {
                  $tmp = explode('.', $ath_arr[1]);
                  $ext = end($tmp); // prendo l'estensione come ultimo elemento di tmp
                  $ext = ($ext != FALSE) ? '.' . $ext : ''; // ci aggiungo il punto prima
                  $dir = explode('_', $ath_arr[0], 3);
                  if ($dir != FALSE && sizeof($dir) == 3) {
                  $attach_path .= $dir[2] . '/';
                  $attach = $attach_path . $ath_arr[0] . "_high" . $ext;
                  $pdf->Image($attach, null, null, 25, 25);
                  }
                  }
                  $pdf->Ln();
                 */
                return "TODO image";
                break;
            case 14:
                if ($this->new_picklist) {
                    $labs = json_decode($value['value'], true);
                    $labs = $labs['labels'];
                    if (count($labs) == 0) {
                        return "";
                    }
                    return implode(", ", $labs);
                } else {
                    return $value['value'];
                }
                break;
            default:
                //return "TEXTAREA";
                return utf8_decode($GLOBALS['trad']->_($value['value']));
        }
    }

    function getData($f_code, $fields_end, $type) {
        $this->select->reset();
        switch ($type) {
            case "ware":
                //die(var_dump($fields_end));
                $values = $this->select->from(["t1" => "t_creation_date"])
                                ->join(["t2" => "t_custom_fields"], "t1.f_id = t2.f_code")
                                ->join(["t3" => "t_wares"], "t3.f_code = t2.f_code")
                                ->join(['t4' => 't_wf_phases'], "t1.f_phase_id = t4.f_number and t1.f_wf_id = t4.f_wf_id", ["f_name"])
                                ->where("t1.f_id = " . $f_code)
                                ->query()->fetch();
                foreach ($fields_end as $k => $v) {
                    foreach ($v as $k2 => $v2) {
                        $fields_end[$k][$k2]["value"] = $values[$fields_end[$k][$k2]["bind"]];
                    }
                }
                return $fields_end;
            case "wo":
                // Recupero dal DB i valori dei campi
                $values = $this->select->from(["t1" => "t_creation_date"])
                                ->join(["t2" => "t_custom_fields"], "t1.f_id = t2.f_code")
                                ->join(["t3" => "t_workorders"], "t3.f_code = t2.f_code")
                                ->join(['t4' => 't_wf_phases'], "t1.f_phase_id = t4.f_number and t1.f_wf_id = t4.f_wf_id", ["f_name"])
                                ->join(["t5" => "t_workorders_types"], "t3.f_type_id = t5.f_id", ["f_type"])
                                ->where("t1.f_id = " . $f_code)
                                ->query()->fetch();
                foreach ($fields_end as $k => $v) {
                    foreach ($v as $k2 => $v2) {
                        $fields_end[$k][$k2]["value"] = $values[$fields_end[$k][$k2]["bind"]];
                    }
                }

                //die(var_dump($fields_end));
                return $fields_end;

            case "selector":
                $values = $this->select->from(["t1" => "t_creation_date"])
                                ->join(["t2" => "t_custom_fields"], "t1.f_id = t2.f_code")
                                ->join(["t3" => "t_selectors"], "t3.f_code = t2.f_code")
                                ->where("t1.f_id = " . $f_code)
                                ->query()->fetch();
                foreach ($fields_end as $k => $v) {
                    foreach ($v as $k2 => $v2) {
                        $fields_end[$k][$k2]["value"] = $values[$fields_end[$k][$k2]["bind"]];
                    }
                }
                return $fields_end;
            default:
                return [];
        }
    }

    function getModName($type, $category) {
        switch ($type) {
            case "ware":
                switch ($category) {
                    case "ASSET":
                        return "mdl_asset_edit_c1";
                    default:
                        return "";
                }
                break;
            case "wo":
                return "mdl_wo_edit_c1";
            case "selector":
                return "mdl_slc_edit_c1";
            default:
                return "";
        }
    }

}

class C extends PDF_MC_Table {

    public $wmax = 190;

    function Header() {
        $installation = new Mainsim_Model_Mainpage();
        $settings = $installation->getSettings();
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 14);
        $this->Image(APPLICATION_PATH . "/../public/msim_images/default/_pdf/logo.png");
        $this->SetXY(100, 12);
        $this->MultiCell(100, 5, $settings['MAINSIM_PROJECT_NAME'], 0, 'R');
        $this->SetXY(10, 15);
        $this->SetXY(10, 6);
        $this->setfillcolor($this->R[0] = 74, $this->G[0] = 102, $this->B[0] = 113);
        $this->rect(10, 27, $this->wmax, 1.6, "F");
        $this->Ln(25);
    }

    function Footer() {
        $session_1 = $_SESSION['Zend_Auth'];
        $user_session = $session_1['storage']->f_displayedname;

        $this->SetXY(10, 285);
        $this->setfillcolor($this->R[0] = 74, $this->G[0] = 102, $this->B[0] = 113);
        $this->rect(10, 283, $this->wmax, 1.6, "F");
        $this->Ln(20);
        $this->SetXY(8.7, 285);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(100, 5, iconv('UTF-8', 'ASCII//TRANSLIT', "Mainsim™ - what maintenance can be - www.mainsim.com"));
        $this->SetXY(136, 285);
        $this->AliasNbPages();
        $this->Cell(63.5, 5, 'Pagina ' . $this->PageNo() . ' - Stampato da ' . $user_session . ' - ' . date('d-m-Y'), 0, '', 'R');
    }

    function Row2($data, $color = [255, 255, 255]) {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            $this->SetLineWidth(0.1);
            $this->SetDrawColor(200, 200, 200);
            $this->SetFillColor($color[0], $color[1], $color[2]);
            $this->Rect($x, $y, $w, $h, 'FD');
            //Print the text
            $this->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function fancyRow2($header, $data, $colorh = [255, 255, 255], $colorr = [255, 255, 255]) {
        $this->SetFont('Arial', 'B', 10);
        //Calculate the height of the row
        $nb = 0;
        $nc = 0;
        for ($i = 0; $i < count($data); $i++) {
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
            $nc = max($nc, $this->NbLines($this->widths[$i], $header[$i]));
        }
        $h = 5 * ($nb + $nc);
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        $cellWidths = [];
        $cellWidth = round($this->wmax / count($header), 0, PHP_ROUND_HALF_DOWN);
        for ($i = 0; $i < count($header); $i++) {
            $cellWidths[] = $cellWidth;
        }
        $this->SetWidths($cellWidths);
        //Draw the cells of the row
        $this->SetFont('Arial', 'B', 10);
        for ($i = 0; $i < count($header); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetFillColor($colorh[0], $colorh[1], $colorh[2]);
            $this->Rect($x, $y, $w, $nb * 5, 'F');
            //Print the text
            $this->MultiCell($w, 5, $header[$i], 0, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        $this->Ln($nb * 5);
        $this->SetFont('Arial', '', 10);
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetFillColor($colorr[0], $colorr[1], $colorr[2]);
            $this->Rect($x, $y, $w, $nc * 5, 'F');
            //Print the text
            $this->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

}
