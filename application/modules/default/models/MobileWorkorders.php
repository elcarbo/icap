<?php
class Mainsim_Model_MobileWorkorders extends Mainsim_Model_MobileBase{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getUserWorkordersLocked($userCode = null){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        $select = new Zend_Db_Select($this->db);
        $select->from('t_locked', array('f_code'))
               ->join('t_workorders', 't_locked.f_code = t_workorders.f_code', array());
		if($userCode != null){
            $select->where('t_locked.f_user_id = ' . $userCode);
		}
        $res = $this->dbSelect($select);
        if(count($res)){
            for($i = 0; $i < count($res); $i++){
                $result[] = $res[$i]['f_code'];
            }
            return $result;
        }
        else{
            return array();
        }
    }
    
    public function getUserWorkordersCodes($params = null){
        
        if($params['noLocked'] == true){
            $locked = $this->getUserWorkordersLocked();
        }
        $userLevels = $this->getUserLevels();
        $userSelectors = $this->getUserSelectors();
        // exist levels --> get workorders for each level
        $workordersByLevel = array();
        if(is_array($userLevels)){
            for($i = 0; $i < count($userLevels); $i++){
                $workordersByLevel = array_merge($workordersByLevel, $this->getWorkordersByLevel($userLevels[$i], $locked));
            }
        } 
        // exists selectors --> get workorders by selectors
        $workordersBySelectors = $this->getWorkordersBySelectors($userSelectors, $locked);
        
        //print_r($workordersBySelectors);
        //print_R($workordersByLevel);
        
        $workordersResult = array_intersect(array_unique($workordersByLevel), array_unique($workordersBySelectors));
        //print_r($workordersResult); die();
        if(isset($params['limit'])){
            return array_slice($workordersResult, 0, $params['limit']);
        }
        else{
            return $workordersResult;
        }
        
    }
    
    public function getUserWorkordersData($codes, $params){
		
        if(count($codes) == 0){
            return array(
                't_workorders' => array(),
                't_custom_fields' => array(),
                't_creation_date' => array(),
                't_workorders_parent' => array(),
                't_pair_cross' => array(),
                't_wares' => array(),
                't_task_readonly' => array()
            );
        }

        $fields = Mainsim_Model_MobileUtilities::getMobileFields('t_workorders', unserialize(MOBILE_MODULES));
        $select = new Zend_Db_Select($this->db);
        $select->from('t_creation_date')
               ->join('t_workorders', 't_creation_date.f_id = t_workorders.f_code')
               ->where('t_workorders.f_code IN ('. implode(',', $codes) . ')');
        $result['t_workorders'] = $this->dbSelect($select);
        
        // **********************************
        // additional workorders data table
        // **********************************
        $additionalDataConfig = array(
            array(
                'table' => 't_custom_fields',
                'field' => 'f_code',
                'value' => $codes
            ),
            array(
                'table' => 't_creation_date',
                'field' => 'f_id',
                'value' => $codes
            ),
            array(
                'table' => 't_workorders_parent',
                'field' => 'f_code',
                'filter' => 'f_active = 1',
                'value' => $codes
            ),
            array(
                'table' => 't_pair_cross',
                'field' => 'f_code_main',
                'value' => $codes,
                'join' => array(
                    'table' => 't_wares',
                    'on' => 't_wares.f_code = t_pair_cross.f_code_cross'
                ),
                'filter' => 't_wares.f_type_id = 10',
				'order'  => 't_wares.f_code'
            ),
            array(
                'table' => 't_task_readonly',
                'field' => 'f_code_main',
                'value' => $codes
            )
        );
        
        foreach($additionalDataConfig as $config){
            $result[$config['table']] = Mainsim_Model_MobileUtilities::getAdditionalData($config, Zend_Db::FETCH_NUM);
        }
        
        // **********************************
        // get wares linked to workorders
        // **********************************
        if($params['wares'] == true){
            if(is_array($codes)){
                $config = array(
                    'table' => 't_ware_wo',
                    'field' => 'f_wo_id',
                    'value' => $codes
                );
                $result['t_ware_wo'] = Mainsim_Model_MobileUtilities::getAdditionalData($config, Zend_Db::FETCH_NUM);
                $wareList = array();
                for($k = 0; $k < count($result['t_ware_wo']); $k++){
                    if(!in_array($result['t_ware_wo'][$k][1], $wareList)){
                        $wareList[] = $result['t_ware_wo'][$k][1];
                    }
                }
                $objWare = new Mainsim_Model_MobileWares();
                if(count($wareList) > 0){
                    $wareParams = array(
                        'in' => $wareList,
                        'fetch' => Zend_Db::FETCH_NUM
                    );
                    $wareResult = $objWare->getWares($wareParams);
                }
                else{
                    $wareResult['t_wares'] = array();
                    $wareResult['t_custom_fields'] = array();
                    $wareResult['t_creation_date'] = array();
                    $wareResult['t_wares_parent'] = array();
                }

                $result['t_custom_fields'] = array_merge($result['t_custom_fields'], $wareResult['t_custom_fields']);
                $result['t_creation_date'] = array_merge($result['t_creation_date'], $wareResult['t_creation_date']);
                $result['t_wares'] = $wareResult['t_wares'];
                $result['t_wares_parent'] = $wareResult['t_wares_parent'];
            }
            else{
                 $result['t_wares'] = array();
                 $result['t_wares_parent'] = array();
                 $result['t_ware_wo'] = array();
            }
        }
        return $result;
        
    }
    
    private function getWorkordersBySelectors($selectors, $locked = array()){  
        $userData = Zend_Auth::getInstance()->getIdentity();
        $result = array();
        $selectWo = new Zend_Db_Select($this->db);
        // loop through selector types
        $singleWo = array();
        
        if(count($selectors) > 0){
        
            foreach($selectors as $key => $value){

                $selectorTypes[] = $key; 

                $selectWo->reset();
                // get wo by selector
                $selectWo->from('t_workorders', array('f_code'))
                         ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                         ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                         ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                         ->join('t_selector_wo', 't_workorders.f_code = t_selector_wo.f_wo_id', array())
                         ->join('t_selectors', 't_selectors.f_code = t_selector_wo.f_selector_id', array('f_type_id'))
                         ->where('t_wf_groups.f_id NOT IN (6,7)')
                         ->where('t_selector_wo.f_selector_id IN (' . implode(",", $value) . ')')
                         ->where('t_workorders.f_type_id IN (' . $userData['workorder_types'] . ')')
                         ->order(array('t_workorders.f_priority DESC', 't_creation_date.f_creation_date'));
                // if locked warerkorders are passed in remove them from result
                if(count($locked)){
                    $selectWo->where('t_workorders.f_code NOT IN (' . implode(',', $locked) . ')');
                }
                $resWo = $selectWo->query()->fetchAll();
                $aux = array();
                for($i = 0; $i < count($resWo); $i++){
                    $aux[] = $resWo[$i]['f_code'];
                }
                if(count($result) > 0){
                    $result = array_intersect($aux, $result);

                }
                else{
                    $result = $aux;
                }
                $woBySelectorType[$key] = $aux;
            }
            foreach($woBySelectorType as $key => $value){
                for($i = 0; $i < count($value); $i++){
                    if(in_array($value[$i], $result)){
                        unset($value[$i]);
                    }
                }
                $singleWo[$key] = array_values($value);
            }
            // check if workorders not related with all user's selectors have been associated with selectors of same type of user selectors types
            if(count($singleWo) > 0 && count($selectorTypes) > 1){

                foreach($singleWo as $key => $value){

                    if(count($value) > 0){
                        $auxSelectorTypes = $selectorTypes;
                        for($k = 0; $k < count($auxSelectorTypes); $k++){
                            if($auxSelectorTypes[$k] == $key){
                                unset($auxSelectorTypes[$k]);
                            }
                        }
                        $selectWo->reset();
                        $selectWo->from('t_workorders', array('f_code'))
                                 ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                                 ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                                 ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                                 ->join('t_selector_wo', 't_workorders.f_code = t_selector_wo.f_wo_id', array())
                                 ->join('t_selectors', 't_selectors.f_code = t_selector_wo.f_selector_id', array('f_type_id'))
                                 ->where('t_wf_groups.f_id NOT IN (6,7)')
                                 ->where('t_creation_date.f_id IN (' . implode(',', $value) . ')')
                                 ->where('t_selectors.f_type_id IN (' . implode(',', $auxSelectorTypes) . ')');
                                
                        // if locked warerkorders are passed in remove them from result
                        if(count($locked)){
                            $selectWo->where('t_workorders.f_code NOT IN (' . implode(',', $locked) . ')');
                        }
                        $resWo = $selectWo->query()->fetchAll();
                        //print_r($resWo);
                        if(count($resWo) > 0){
                            for($i = 0; $i < count($resWo); $i++){
                                $aux[] = $resWo[$i]['f_code'];
                            }
                        }
                        $result = array_merge($result, array_diff($value, $aux));
                    }
                }
            }
        }
        // get workorders not related with user's selector types
        $selectWo->reset();
        if(count($result) > 0){
            $selectWo->from('t_workorders', array('f_code'))
                 ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                 ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                 ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                 ->where('t_wf_groups.f_id NOT IN (6,7)')
                 ->where('t_workorders.f_type_id IN (' . $userData['workorder_types'] . ')')
                 ->where('t_creation_date.f_id NOT IN (' . implode(",", $result) . ')')
                 ->where('t_creation_date.f_id NOT IN (SELECT t_selector_wo.f_wo_id FROM t_selector_wo INNER JOIN t_selectors ON t_selectors.f_code = t_selector_wo.f_selector_id WHERE t_selectors.f_type_id IN (' . implode(',', $selectorTypes) . ') AND (t_selector_wo.f_wo_id NOT IN (' . implode(",", $result) . ')))');
        }
        else{
            $selectWo->from('t_workorders', array('f_code'))
                 ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                 ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                 ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                 ->where('t_wf_groups.f_id NOT IN (6,7)')
                 ->where('t_workorders.f_type_id IN (' . $userData['workorder_types'] . ')');
            if(is_array($selectorTypes)){
                $selectWo->where('t_creation_date.f_id NOT IN (SELECT t_selector_wo.f_wo_id FROM t_selector_wo INNER JOIN t_selectors ON t_selectors.f_code = t_selector_wo.f_selector_id WHERE t_selectors.f_type_id IN (' . implode(',', $selectorTypes) . '))');
            }     
        }
        //$selectWo->where('t_creation_date.f_id = 34719');
        // if locked warerkorders are passed in remove them from result
        if(count($locked)){
            $selectWo->where('t_workorders.f_code NOT IN (' . implode(',', $locked) . ')');
        }
        $resWo = $selectWo->query()->fetchAll();
        if(count($resWo) > 0){
            for($i = 0; $i < count($resWo); $i++){
                $result[] = $resWo[$i]['f_code'];
            }
        }
        return $result;
        
    }
    /*
    private function getWorkordersBySelectors($selectors, $locked = array()){  
        $userData = Zend_Auth::getInstance()->getIdentity();
        $result = array();
        $selectWo = new Zend_Db_Select($this->db);
        if(count($selectors['id']) > 0){
            // get wo by selector
            $selectWo->from('t_workorders', array('f_code'))
                     ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                     ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->join('t_selector_wo', 't_workorders.f_code = t_selector_wo.f_wo_id', array())
                     ->where('t_wf_groups.f_id NOT IN (6,7)')
                     ->where('t_selector_wo.f_selector_id IN (' . $selectors['id'] . ')')
                     ->where('t_workorders.f_type_id IN (' . $userData['workorder_types'] . ')')
                     ->order(array('t_workorders.f_priority DESC', 't_creation_date.f_creation_date'));
            // if locked workorders are passed in remove them from result
            if(count($locked)){
                $selectWo->where('t_workorders.f_code NOT IN (' . implode(',', $locked) . ')');
            }
            $resWo = $selectWo->query()->fetchAll();  
            for($i = 0; $i < count($resWo); $i++){
                $result[] = $resWo[$i]['f_code'];
            }
        
            // get wo related to selector's types not contained in user selectors
            $selectWo->reset();
            $selectWo->from('t_workorders', array('f_code'))
                     ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                     ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->join('t_selector_wo', 't_workorders.f_code = t_selector_wo.f_wo_id', array())
                     ->join('t_selectors', 't_selectors.f_code = t_selector_wo.f_selector_id', array('f_type_id'))
                     ->where('t_wf_groups.f_id NOT IN (6,7)')
                     ->where('t_selectors.f_type_id NOT IN (' . $selectors['type'] . ')')
                     ->where('t_workorders.f_type_id IN (' . $userData['workorder_types'] . ')')
                     ->order(array('t_workorders.f_priority DESC', 't_creation_date.f_creation_date'));
            // if locked workorders are passed in remove them from result
            if(count($locked)){
                $selectWo->where('t_workorders.f_code NOT IN (' . implode(',', $locked) . ')');
            }
            
            $resWo = $selectWo->query()->fetchAll();
            for($i = 0; $i < count($resWo); $i++){
                $result[] = $resWo[$i]['f_code'];
            }

            // get wo without selector relation
            $selectWo->reset();
            $selectWo->from('t_workorders', array('f_code'))
                     ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                     ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->where('t_wf_groups.f_id NOT IN (6,7)')
                     ->where('t_workorders.f_code NOT IN (SELECT f_wo_id FROM t_selector_wo)')
                     ->where('t_workorders.f_type_id IN (' . $userData['workorder_types'] . ')')
                     ->order(array('t_workorders.f_priority DESC', 't_creation_date.f_creation_date'));
            //echo $selectWo;
            // if locked workorders are passed in remove them from result
            if(count($locked)){
                $selectWo->where('t_workorders.f_code NOT IN (' . implode(',', $locked) . ')');
            }
            $resWo = $selectWo->query()->fetchAll();
            for($i = 0; $i < count($resWo); $i++){
                $result[] = $resWo[$i]['f_code'];
            }
            print_r($result);
            return $result;
        }
        else{
            $selectWo->from('t_workorders', array('f_code'))
                     ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                     ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                     ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                     ->where('t_wf_groups.f_id NOT IN (6,7)')
                     ->where('t_workorders.f_type_id IN (' . $userData['workorder_types'] . ')')
                     ->order(array('t_workorders.f_priority DESC', 't_creation_date.f_creation_date'));
            // if locked workorders are passed in remove them from result
            if(count($locked)){
                $selectWo->where('t_workorders.f_code NOT IN (' . implode(',', $locked) . ')');
            }
            $resWo = $selectWo->query()->fetchAll();
            for($i = 0; $i < count($resWo); $i++){
                $result[] = $resWo[$i]['f_code'];
            }
            return $result;
        }
    }*/
    
    private function getWorkordersByLevel($level, $locked = array()){
        $userData = Zend_Auth::getInstance()->getIdentity();
        $selectWo = new Zend_Db_Select($this->db);
        
        switch($level){
            case 'Administrator':
            case 'Supervisor':
            case 'Internal Mantainer':
                break; 
            
            case 'Service Requestor':
                $selectWo->where('t_creation_date.f_creation_user = ' . $userData['f_code']);
                break;
            
            case 'External Mantainer':
                $selectWo->join('t_ware_wo', 't_ware_wo.f_wo_id = t_workorders.f_code', array());
                $selectWo->where('t_ware_wo.f_ware_id = ' . $userData['f_code']);
                break;
        }
        
        $selectWo->from('t_workorders', array('f_code'))
                 ->join('t_creation_date', "t_workorders.f_code = t_creation_date.f_id", array())
                 ->join('t_wf_phases', 't_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id', array())
                 ->join('t_wf_groups', 't_wf_phases.f_group_id = t_wf_groups.f_id', array())
                 ->where('t_wf_groups.f_id NOT IN (6,7)')
                 ->where("t_wf_phases.f_visibility & " . $userData['fc_usr_level'] . " != 0")
                 ->where("t_wf_groups.f_visibility & " . $userData['fc_usr_level'] . "!= 0")
                 ->where("t_creation_date.f_visibility & " . $userData['fc_usr_level'] . " != 0")
                 ->where("t_creation_date.f_visibility & " . $userData['fc_usr_group_level'] . " != 0")
                 ->where('t_workorders.f_type_id IN (' . $userData['workorder_types'] . ')')
                 ->order(array('t_workorders.f_priority DESC', 't_creation_date.f_creation_date'));
        
        // if locked workorders are passed in remove them from result
        if(count($locked)){
            $selectWo->where('t_workorders.f_code NOT IN (' . implode(',', $locked) . ')');
        }
        $result = array();
        $resWo = $selectWo->query()->fetchAll();
        for($i = 0; $i < count($resWo); $i++){
            $result[] = $resWo[$i]['f_code'];
        }
        return $result;
    }
    
    private function getUserLevels(){
        $userData = Zend_Auth::getInstance()->getIdentity();
        if($userData['fc_usr_level_text'] != ""){
            return explode(",", $userData['fc_usr_level_text']);
        }
        else{
            return null;
        }
    }
    /*
    private function getUserSelectors(){
        $userData = Zend_Auth::getInstance()->getIdentity();
        $select = new Zend_Db_Select($this->db);
        $select->from('t_selector_ware', array('f_selector_id'))
               ->join('t_selectors', 't_selector_ware.f_selector_id = t_selectors.f_code', array('f_type_id'))
               ->where('f_ware_id = ' . $userData['f_code']);
        $res = $this->dbSelect($select);
        $selectors = array(
            'id' => array(),
            'type' => array()
        );
        if(count($res) > 0){
            for($i = 0; $i < count($res); $i++){
                $selectors['id'][] = $res[$i]['f_selector_id'];
                if(!in_array($res[$i]['f_type_id'], $selectors['type'])){
                    $selectors['type'][] = $res[$i]['f_type_id'];
                }
            }
            
            return array(
                'id' => implode(',', $selectors['id']),
                'type' => implode(',', $selectors['type'])
            );
        }
        else{
            return null;
        }
    }
     * 
     */
    
    private function getUserSelectors(){
        $userData = Zend_Auth::getInstance()->getIdentity();
        $select = new Zend_Db_Select($this->db);
        $select->from('t_selector_ware', array('f_selector_id'))
               ->join('t_selectors', 't_selector_ware.f_selector_id = t_selectors.f_code', array('f_type_id'))
               ->where('f_ware_id = ' . $userData['f_code']);
        $res = $this->dbSelect($select);
        if(count($res) > 0){
            for($i = 0; $i < count($res); $i++){
                $selectors[$res[$i]['f_type_id']][] = $res[$i]['f_selector_id'];
            }
            
            return $selectors;
        }
        else{
            return null;
        }
    }
    
    // return workoroders not locked
    public function getUserWorkorders($params = null){
        
        // **********************************
        // table workorders
        // **********************************

        // get user selectors
        $userData = Zend_Auth::getInstance()->getIdentity();
        //$session = new Zend_Session_Namespace('session');
        //$userData = $session->data;
        $selectors = Mainsim_Model_MobileUtilities::getSelectors($userData['f_code'], 'workorders');
        // group selector by type
        $selectorsList = array();
        for($i = 0; $i < count($selectors); $i++){
            if(!$selectorsList[$selectors[$i]['f_type_id']]){
                $selectorsList[$selectors[$i]['f_type_id']] = $selectors[$i]['f_selector_id'];
                $selectorsTypeList[] = $selectors[$i]['f_type_id'];
            }
            else{
                $selectorsList[$selectors[$i]['f_type_id']] .= ',' . $selectors[$i]['f_selector_id'];
            }
        }
        //wo query
        $select = new Zend_Db_Select($this->db);
        $select->distinct();
        // count query
        if($params['count']){
            $select->from(array("wo" => "t_workorders"), new Zend_Db_Expr('COUNT(DISTINCT(wo.f_code)) as tot'))
            ->join(array("cd" => "t_creation_date"), "wo.f_code = cd.f_id", array());
        }
        // data query
        else{
            $fields = Mainsim_Model_MobileUtilities::getMobileFields('t_workorders', unserialize(MOBILE_MODULES));
            $select->from(array("wo" => "t_workorders"), $fields['list'])
            ->join(array("cd" => "t_creation_date"), "wo.f_code = cd.f_id", array('cd.f_creation_date'));
        }
        
        
                $select->join(array("wp" => "t_wf_phases"), "cd.f_phase_id = wp.f_number AND cd.f_wf_id = wp.f_wf_id", array())
                ->join(array("wg" => "t_wf_groups"), "wp.f_group_id = wg.f_id", array())      
                ->joinLeft(array("sw" => "t_selector_wo"), 'wo.f_code = sw.f_wo_id', array())
                ->joinLeft(array("st" => "t_selectors_types"), 'sw.f_selector_id = st.f_id', array())
                ->joinLeft(array("l" => "t_locked"), 'wo.f_code = l.f_code', array());
        
        // filter by visibility
        $select->where("cd.f_visibility & " . $userData['fc_usr_level'] . " != 0")
               ->where("cd.f_visibility & " . $userData['fc_usr_group_level'] . " != 0")
               ->where("wp.f_visibility & " . $userData['fc_usr_level'] . " != 0")
               ->where("wg.f_visibility & " . $userData['fc_usr_level'] . "!= 0"); 
        
        // filter by selectors
        foreach($selectorsList as $key => $value){
            $where[] =  'sw.f_selector_id IN (' . $value. ')';
        }

        if($where){
            $select->where('(' . implode(' AND ', $where) . ' OR sw.f_selector_id IS NULL)');
        }
        /*
        else{
            $select->where('sw.f_selector_id IS NULL');
        }*/
        
        // where clause for workorders not locked/locked
        if(isset($params['locked'])){
            if($params['locked'] == true){
                $select->where('l.f_code IS NOT NULL AND l.f_user_id =' . $userData['f_code']);
            }
            else{
                $select->where('l.f_code IS NULL');
            }
        }
        
        if(isset($params['in'])){
            $select->where('wo.f_code IN (' . implode(',', $params['in']) . ')');
        } 
        
        if(isset($params['not in'])){
            $select->where('wo.f_code NOT IN (' . implode(',', $params['not in']) . ')');
        }
        
        
        // filter by workorders type
        if($params['type_id']){
            $select->where("wo.f_type_id IN (" . $params['type_id'] . ")");
        }
        
        // filter by group phases
        if($params['group_phases']){
            $select->where("wg.f_id IN (" . $params['group_phases'] . ")");
        }
        
        // filter by text search 
        if($params['filter'] && $params['search']){
            for($i = 0; $i < count($params['filter']); $i++){
                $whereSearch[] = ($params['filter'][$i] == 'f_code' ? 'wo.f_code' : $params['filter'][$i]) . " like '%" . $params['search'] . "%'" ;
            }
            $select->where(implode(" OR ", $whereSearch));
        }
        
        // limit result set 
        if($params['limit'] && $params['offset']){
            $select->limit($params['offset'], $params['limit']);
        }
        else if($params['offset']){
            $select->limit($params['offset']);
        }
        
        if(!$params['count']){
            $select->order(array('wo.f_priority DESC', 'cd.f_creation_date'));
        }
        
        
        $result['t_workorders'] = $this->dbSelect($select, $params['fetch'], false);
        
        if($params['count']){
            return isset($result['t_workorders'][0]['tot']) ? $result['t_workorders'][0]['tot'] : $result['t_workorders'][0][0];
            die();
        }
        
        if(count($result['t_workorders']) > 0){
            // loop workorders to get code list
            // associative array
            if(Mainsim_Model_MobileUtilities::isAssociativeArray($result['t_workorders'][0])){
                foreach($result['t_workorders'] as $value){
                    $codeList[] = $value['f_code'];
                }
            }
            // numeric array
            else{
                for($i = 0; $i < count($result['t_workorders']); $i++){
                    $codeList[] = $result['t_workorders'][$i][2];
                }
            }

            if($params['onlyCodes']){
                return count($codeList) > 0 ? $codeList : array();
            }
        }
        

        // **********************************
        // additional workorders data table
        // **********************************
        $additionalDataConfig = array(
            array(
                'table' => 't_custom_fields',
                'field' => 'f_code',
                'value' => $codeList
            ),
            array(
                'table' => 't_creation_date',
                'field' => 'f_id',
                'value' => $codeList
            ),
            array(
                'table' => 't_workorders_parent',
                'field' => 'f_code',
                'filter' => 'f_active = 1',
                'value' => $codeList
            ),
            array(
                'table' => 't_pair_cross',
                'field' => 'f_code_main',
                'value' => $codeList,
                'join' => array(
                    'table' => 't_wares',
                    'on' => 't_wares.f_code = t_pair_cross.f_code_cross'
                ),
                'filter' => 't_wares.f_type_id = 10',
				'order'  => 't_wares.f_code'
            ),
            array(
                'table' => 't_task_readonly',
                'field' => 'f_code_main',
                'value' => $codeList
            )
        );
        
        foreach($additionalDataConfig as $config){
            $result[$config['table']] = Mainsim_Model_MobileUtilities::getAdditionalData($config, $params['fetch']);
        }
        
        // **********************************
        // get wares linked to workorders
        // **********************************
        if($params['wares'] == true){
            if(is_array($codeList)){
                $config = array(
                    'table' => 't_ware_wo',
                    'field' => 'f_wo_id',
                    'value' => $codeList,
                    'join' => array(
                        'table' => 't_wares',
                        'on' => 't_wares.f_code = t_ware_wo.f_ware_id'
                    ),
                    'filter' => 't_wares.f_type_id IN (1,5,10)',
                );
                $result['t_ware_wo'] = Mainsim_Model_MobileUtilities::getAdditionalData($config, $params['fetch']);
                $wareList = array();
                for($k = 0; $k < count($result['t_ware_wo']); $k++){
                    if(!in_array($result['t_ware_wo'][$k][1], $wareList)){
                        $wareList[] = $result['t_ware_wo'][$k][1];
                    }
                }

                $objWare = new Mainsim_Model_MobileWares();
                if(count($wareList) > 0){
                    $wareParams = array(
                        'in' => $wareList,
                        'fetch' => $params['fetch']
                    );
                    $wareResult = $objWare->getWares($wareParams);
                }
                else{
                    $wareResult['t_wares'] = array();
                    $wareResult['t_custom_fields'] = array();
                    $wareResult['t_creation_date'] = array();
                }

                $result['t_custom_fields'] = array_merge($result['t_custom_fields'], $wareResult['t_custom_fields']);
                $result['t_creation_date'] = array_merge($result['t_creation_date'], $wareResult['t_creation_date']);
                $result['t_wares'] = $wareResult['t_wares'];
                $result['t_wares_parent'] = $wareResult['t_wares_parent'];
            }
            else{
                 $result['t_wares'] = array();
                 $result['t_wares_parent'] = array();
                 $result['t_ware_wo'] = array();
            }
        }
        return $result;
    }
    
    public function lockWorkorder($params){
        try{
            // check if workorder is already locked
            $select = new Zend_Db_Select($this->db);
            $select->from('t_locked')
                   ->where('f_code =' . $params['f_code']);
            $res = $this->dbSelect($select);
            // workorder unlocked --> lock it!
            if(count($res) == 0){
                $this->dbInsert('t_locked', array('f_code' => $params['f_code'], 'f_user_id' => $params['f_user_id'], 'f_timestamp' => time())) ;
                return 1;
            }
            else{
                return 0;
            }
        }
        catch(Exception $e){
            print($e->getMessage());
        }
    }
    
    public function lockWorkorders($params){
        for($i = 0; $i < count($params['code_list']); $i++){
            $res = $this->lockWorkorder(array(
               'f_code' => $params['code_list'][$i],
               'f_user_id' => $params['userId']
            ));
            if($res){
                $result['locked'][] = $params['code_list'][$i];
            }
            else{
                $result['notLocked'][] = $params['code_list'][$i];
            }
        }
        return $result;
    }
    
    public function unlockWorkorders($params){
        try{
            $this->dbDelete('t_locked', 'f_code IN (' . implode(",", $params['code_list']) . ")");
            return true;
        }
        catch(Exception $e){
            return false;
        }
    }
    
    public function unlockWorkorder($params){
        
        try{
            // unlock workorders owned by user
            if($params['userId']){
                $this->dbDelete('t_locked', 'f_user_id =' . $params['userId']);
            }
            // unlock workorders list
            if($params['woList']){
                $this->dbDelete('t_locked', 'f_code IN (' . implode(",", $params['woList']) . ")");
            }
            return 1;
        }
        catch(Exception $e){
            return 0;
        }
    }
    
    // check if all workorders are locked by user
    public function checkUserLock($params){
        
        // exclude new wo from check
        for($i = 0; $i < count($params['woList']); $i++){
            if($params['woList'][$i] > 0){
                $woList[] = $params['woList'][$i];
            }
        }
        // there are old workorders edited --> check if they are properly locked by the user who made the changes
        if(count($woList) > 0){
            $select = new Zend_Db_Select($this->db);
            $select->distinct()->from(array('l' => 't_locked'))
                   ->where('f_user_id = ' . $params['userId'])
                   ->where('f_code IN (' . implode(',', $woList) . ')');
            $res = $this->dbSelect($select, null, false);
            if(count($res) == count($woList)){
                return true;
            }
            else{
                return false;
            }
        }
        // no old workorders has been modified --> check ok
        else{
            return true;
        }
    }
    
    public function editWorkorders($params){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        //$this->dbBeginTransaction();
        // loop wo to edit
        $newCodeList = array();
        for($i = 0; $i < count($params['woList']); $i++){
            $singleWoData = null;
            $wo = null;
            foreach($params['woData'] as $table => $rows){
                $singleWoData[$table][] = $rows[0];
                $codeField = Mainsim_Model_MobileUtilities::getCodeField($table, 'workorder');
                 
                for($j = 1; $j < count($rows); $j++){
                    if($rows[$j][$codeField['position']] == $params['woList'][$i]){
                        $singleWoData[$table][] = $rows[$j];
                    }
                }
            }
            
            // set documents to disassociate if they exist
            if($params['docs_deleted']){
                $wo['docs_deleted'] = $params['docs_deleted'][$params['woList'][$i]];
            }
           
            $wo['data'] = $singleWoData;
            $wo['f_code'] = $params['woList'][$i];
            
            // update history tables with current values
            $aux = array_diff($wo['data']['t_creation_date_history'][0], $wo['data']['t_creation_date'][0]);
            $aux2 = array_merge(array(0), $wo['data']['t_creation_date'][1]);
            foreach($aux as $k => $v){
                if($v != 'f_code'){
                    array_splice($aux2, $k, 0, '');
                }
            }
            $wo['data']['t_creation_date_history'][] = $aux2;
            $wo['data']['t_custom_fields_history'][] = $wo['data']['t_custom_fields'][1];
            $wo['data']['t_workorders_history'][] = $wo['data']['t_workorders'][1];
  
            // new workorder
            if($wo['f_code'] < 0){
                $newCodeList[] = $this->newWorkorder($wo);
            }
            // old workorder
            else{
                $this->editWorkorder($wo);
            }
        }
        
        
        // get new list of workorders user can see
        $params2 = array(
            'type' => $userData['workorder_types'],
            'group_phases' => GROUP_PHASES_WO,
            'fetch' => Zend_Db::FETCH_NUM,
            'onlyCodes' => true,
        );
        
        $woAvailableList = $this->getUserWorkorders($params2);
        // update params workorders list with real id
        for($i = 0; $i < count($params['woList']); $i++){
            if($params['woList'][$i] < 0){
                for($j = 0; $j < count($newCodeList); $j++){
                    if($newCodeList[$j]['f_code_old'] == $params['woList'][$i]){
                        $realCodeWoList[] = $newCodeList[$j]['f_code_new'];
                    }
                }
            }
            else{
                $realCodeWoList[] = $params['woList'][$i];
            }
        }
        
        $woToUnlock = array_diff($realCodeWoList, $woAvailableList);
        if(count($woToUnlock) > 0){
            $params3 = array(
                'woList' => $woToUnlock
            );
            $this->unlockWorkorder($params3);
        }
        //$this->dbEndTransaction();
        
        // create response object
        $resultParams = array(
            'code_list' => $woToUnlock,
            'new_code_list' => $newCodeList
        );
        return $this->createResponseObject(1, "WORKORDERS_EDIT_DONE", $userData['sessionId'], $resultParams);
    }
    
    private function changePhaseScripts($mobileParams, $olds){
        
        // convert user mobile params to desktop mobile params
        $userData = Zend_Auth::getInstance()->getIdentity();
        // transform userinfo array to object for mobile  users
        if(is_array($userData)){
            $aux = $userData;
            $userData = new stdClass();
            foreach($aux as $key => $value){
                if($key == 'f_code'){
                    $userData->f_id = $value;
                }
                elseif($key == 'fc_usr_level'){
                    $userData->f_level = $value;
                }
                elseif($key == 'fc_usr_language'){
                    $userData->f_language = $value;
                }
                else{
                    $userData->{$key} = $value;
                }
            }
        }
        
        // convert mobile params to desktop params
        $params = Mainsim_Model_MobileUtilities::convertToDesktopParams($mobileParams);
        $f_code = $params['f_code'];

        if(!isset($olds['old_creation']['f_phase_id'])){
            $currentPhaseId = 0;
        }
        else{
            $currentPhaseId = $olds['old_creation']['f_phase_id'];
        }
        // check if phase has changed 
        if($currentPhaseId != $params['f_phase_id']){    
            $select = new Zend_Db_Select($this->db);
            $select->from(['t1'=>"t_wf_exits"],'f_script')
                ->join(['t2'=>'t_wf_phases'],"t1.f_wf_id = t2.f_wf_id and t1.f_exit = t2.f_number",['f_group_id'])    
                ->where("f_phase_number = ?", $currentPhaseId)
                ->where("f_exit = ?", $params['f_phase_id'])
                ->where("t1.f_wf_id = ?",$params['f_wf_id'])
                ->query()->fetch();
            $res_phase = $select->query()->fetch();
            if(in_array($res_phase['f_group_id'],[6,7])) {
                $activeChildren = Mainsim_Model_Utilities::checkActiveChildren("t_workorders",$params['f_code'],$this->db);
                if($activeChildren){ 
                    throw new Exception("Close or delete all children elements before"); 
                }
            }
            if(!empty($res_phase['f_script'])){
                eval($res_phase['f_script']);
            }
        }
        
    }

    private function fieldSaveScripts($mobileParams, $olds){
        
        // convert user mobile params to desktop mobile params
        $userData = Zend_Auth::getInstance()->getIdentity();
        // transform userinfo array to object for mobile  users
        if(is_array($userData)){
            $aux = $userData;
            $userData = new stdClass();
            foreach($aux as $key => $value){
                if($key == 'f_code'){
                    $userData->f_id = $value;
                }
                elseif($key == 'fc_usr_level'){
                    $userData->f_level = $value;
                }
                elseif($key == 'fc_usr_language'){
                    $userData->f_language = $value;
                }
                else{
                    $userData->{$key} = $value;
                }
            }
        }
        
        // convert mobile params to desktop params
        $params = Mainsim_Model_MobileUtilities::convertToDesktopParams($mobileParams);
        $f_code = $params['f_code'];
        
        $scriptModule = new Mainsim_Model_Script($this->db,"t_workorders",$f_code,$params, null, null, $userData);
        
        // foreach field passed by client check if there are scripts to execute
        $sel = new Zend_Db_Select($this->db);
        foreach($params as $column => $value) {                
            $sel->reset();     
            $res_script_exist = $sel->from("t_scripts")->where("f_name = ?",$column.'-t_workorders')->where("f_type = 'php'")->query()->fetch();                                         
            if(!empty($res_script_exist['f_script'])) { eval($res_script_exist['f_script']); }  
            elseif(method_exists($scriptModule,$column) && is_callable(array($scriptModule,$column))) {//if not exist try to check if is a custom script in script model
                $scriptModule->$column();
            }                
        }
    }
    
    public function getOldData($f_code){
        
        // t_workorders
        $select = new Zend_Db_Select($this->db);
        $select->from('t_workorders')
                ->where('f_code = ?', $f_code);
        $res = $select->query()->fetch();    
        $olds['old_table'] = $res;
        
        // t_custom_fields
        $select->reset();
        $select->from('t_custom_fields')
                ->where('f_code = ?', $f_code);
        $res = $select->query()->fetch();    
        $olds['old_custom'] = $res;
        
        // t_creation_date
        $select->reset();
        $select->from('t_creation_date')
                ->where('f_id = ?', $f_code);
        $res = $select->query()->fetch();    
        $olds['old_creation'] = $res;
        
        return $olds;
    }
    
    public function editWorkorder($params){

        $timestamp = time();
        
        // update wares/workorder counter (fc_asset_wo in t_wares)
        for($i = 1; $i < count($params['data']['t_ware_wo']); $i++){
            $newWares[] = $params['data']['t_ware_wo'][$i][1];
        }

        // loop tables
        foreach($params['data'] as $table => $rows){
            
            // for t_ware_wo table delete old entries first
            if($table == 't_ware_wo'){
                $codeField = Mainsim_Model_MobileUtilities::getCodeField($table, 'workorder');
                //$this->dbDelete($table, $codeField['name'] . '=' . $params['f_code'] . " AND f_ware_id NOT IN (SELECT f_code FROM t_wares WHERE f_type_id IN (1, 10))");
                //$this->dbDelete($table, $codeField['name'] . '=' . $params['f_code']);
                $delete = "DELETE t_ware_wo FROM t_ware_wo JOIN t_wares ON t_ware_wo.f_ware_id = t_wares.f_code WHERE t_wares.f_type_id IN (1,5,10) AND t_ware_wo.f_wo_id = " . $params['f_code'];
                $this->db->query($delete);
            }
            else if($table == 't_workorders_parent'){
                $codeField = Mainsim_Model_MobileUtilities::getCodeField($table, 'workorder');
                $this->dbDelete($table, $codeField['name'] . '=' . $params['f_code']);
            }
            for($i = 1; $i < count($rows); $i++){
                // ware workorder association table --> delete old entries and insert new records
                if($table == 't_ware_wo' || $table == 't_workorders_parent'){
                    $insertData = array_combine($rows[0], $rows[$i]);
                    unset($insertData['f_id']);
                    $codeField = Mainsim_Model_MobileUtilities::getCodeField($table, 'workorder');
                    try{
                        $this->dbInsert($table, $insertData);
                    }
                    catch(Exception $e){
                        $e->getMessage();
                    }
                }
                // history table --> insert
                else if(strpos($table, '_history') > 0){
                    $insertData = array_combine($rows[0], $rows[$i]);
                    
                    // get from db data not included in $insertData
                    $sel = new Zend_Db_Select($this->db);
                    
                    if($table == 't_creation_date_history'){
                    
                        $userData = Zend_Auth::getInstance()->getIdentity();
                        if($userData['fc_usr_firstname'] != null && $userData['fc_usr_lastname'] != null){
                            $insertData['fc_editor_user_name'] = $userData['fc_usr_firstname'] . " " . $userData['fc_usr_lastname'];
                        }
                        else{
                            $insertData['fc_editor_user_name'] = $userData['fc_usr_usn'];
                        }
                    
                        $sel->from(str_replace('_history', '', $table))
                            ->where("f_id = " . $insertData['f_code']);
                        $res = $this->dbSelect($sel);
                    }
                    else if($table == 't_workorders_history' || $table == 't_custom_fields_history'){
                        $sel->from(str_replace('_history', '', $table))
                            ->where("f_code = " . $insertData['f_code']);
                        $res = $this->dbSelect($sel);
                    }
                    
                    unset($insertData['f_id']);
                    unset($res[0]['f_id']);
                    try{
                        $aux = array_merge($res[0], $insertData);
                        $aux['f_timestamp'] = $timestamp;
                        $this->dbInsert($table, $aux);
                    }
                    catch(Exception $e){
                        print("\n" . $e->getMessage() . "\n");
                    }
                }
                else if($table == 't_pair_cross'){
                    $updateData = array_combine($rows[0], $rows[$i]);
                    unset($updateData['f_id']);
                    if(isset($updateData['fc_task_issue_positive'])){
                        if($updateData['fc_task_issue_negative'] == 1){
                            $updateData['fc_task_issue'] = 'negative';
                        }
                        else if($updateData['fc_task_issue_not_executable'] == 1){
                            $updateData['fc_task_issue'] = 'not executable';
                        }
                        else if($updateData['fc_task_issue_not_performed'] == 1){
                            $updateData['fc_task_issue'] = 'not performed';
                        }
                        else if($updateData['fc_task_issue_positive'] == 1){
                            $updateData['fc_task_issue'] = 'positive';
                        }
                    }
                    $this->dbUpdate($table, $updateData, array('f_code_main' => $params['f_code'], 'f_code_cross' => $updateData['f_code_cross'], 'f_group_code' => $updateData['f_group_code']));
                    
                    // generate corrective after negative result
                    if($updateData['fc_task_issue_negative'] == 1 || $updateData['fc_task_issue_not_executable'] == 1){
                        
                        // check if the corrective task has already been generated 
                        $selectTask = new Zend_Db_Select($this->db);
                        $resTask = $selectTask->from('t_workorders')
                                        ->join('t_ware_wo', 't_workorders.f_code = t_ware_wo.f_wo_id', array())
                                        ->where('t_workorders.f_type_id = 10')
                                        ->where('t_ware_wo.f_ware_id = ' . $updateData['f_group_code'])
                                        ->where('t_workorders.f_code_periodic = ' . $updateData['f_code_main'])
                                        ->where('t_workorders.f_code_task = ' . $updateData['f_code_cross'])
                                        ->query()->fetchAll();
                        
                        // no corrective task has been generated --> create one
                        if(count($resTask) == 0){
                            if($updateData['fc_task_issue_negative'] == 1){
                                $resultTask = 'Negative';
                            }
                            else if($updateData['fc_task_not_executable'] == 1){
                                $resultTask = 'Not executable';
                            }
                            $auxData['t_creation_date'] = array(
                                'f_title' => 'Corrective Task raised from : ' . $params['data']['t_creation_date'][1][6] . '. Result Task : ' . $resultTask . '. Task Title : ' . $updateData['f_title'],
                                'f_wf_id' => 2,
                                'f_description' => $update['fc_task_notes']
                            );
                            $auxData['t_workorders'] = array(
                                'f_code_periodic' => $updateData['f_code_main'],
                                'f_code_task' => $updateData['f_code_cross']
                            );
                            $auxData['t_ware_wo'] = array(
                                'f_ware_id' => $updateData['f_group_code'],
                                'f_timestamp' => time()
                            );

                            $correctiveData['data'] = Mainsim_Model_MobileUtilities::createStubData('workorder', 'corrective task', $auxData);
                            $correctiveData['no_lock'] = true;
                            $this->newWorkorder($correctiveData);
                        }
                    }
                }
                // data table --> update
                else{
                    $updateData = array_combine($rows[0], $rows[$i]);
                    try{
                        $codeField = Mainsim_Model_MobileUtilities::getCodeField($table, 'workorder');
                        unset($updateData['f_id']);
                        if($table == 't_creation_date'){
                            $userData = Zend_Auth::getInstance()->getIdentity();
                            if($userData['fc_usr_firstname'] != null && $userData['fc_usr_lastname'] != null){
                                $updateData['fc_editor_user_name'] = $userData['fc_usr_firstname'] . " " . $userData['fc_usr_lastname'];
                            }
                            else{
                                $updateData['fc_editor_user_name'] = $userData['fc_usr_usn'];
                            }
                        }
                        $this->dbUpdate($table, $updateData, array($codeField['name'] => $params['f_code']));
                    } 
                    catch(Exception $e){
                        print($e->getMessage());
                    }
                }
            }  
            
            // update t_meters_history for meter readings
            /*
            if($table == 't_workorders' && $updateData['f_type_id'] == 7){
                if($params['data']['t_workorders'][1][1] == 7){
                    $userData = Zend_Auth::getInstance()->getIdentity();
                    $meterHistoryData = array(
                        'f_code'=> $updateData['f_code_periodic'],
                        'f_timestamp'=> time(),
                        'f_user_id'=> $userData['f_code'],
                        'f_value'=> $updateData['fc_meter_value'],
                        'f_checked'=> 0,
                        'f_in_work'=> 0
                    );
                    $this->dbInsert('t_meters_history', $meterHistoryData);
                }
            }*/
			
            // save data for server side scripts
            if($table == 't_workorders'){
                $type_id = $updateData['f_type_id'];
                $code = $updateData['f_code'];
                $code_periodic = $updateData['f_code_periodic'];
                $meter_value = $updateData['fc_meter_value'];
            }
			
            if($table == 't_creation_date'){
                $phase_id = $updateData['f_phase_id'];
            }
        }
        // update next date for floating pm
        // get 'closing' phase
        $select = new Zend_Db_Select($this->db);
        $select->from('t_workorders_types', array())
                ->join('t_wf_phases', 't_workorders_types.f_wf_id = t_wf_phases.f_wf_id', array('f_number'))
                ->where('t_wf_phases.f_group_id = 6')
                ->where("t_workorders_types.f_type = 'PERIODIC'");
        $res = $select->query()->fetchAll();
        if($type_id == 4 && $phase_id == $res[0]['f_number']){
                $objWo = new Mainsim_Model_Workorders();
                $objWo->checkPeriodicShift($code);
        }

        // update meter value in meter generator
        // get 'closing' phase
        $select = new Zend_Db_Select($this->db);
        $select->from('t_workorders_types', array())
                ->join('t_wf_phases', 't_workorders_types.f_wf_id = t_wf_phases.f_wf_id', array('f_number'))
                ->where('t_wf_phases.f_group_id = 6')
                ->where("t_workorders_types.f_type = 'METER READING'");
        $res = $select->query()->fetchAll();
        if($type_id == 7 && $phase_id == $res[0]['f_number']){
                /*
                $objScript = new Mainsim_Model_Script();
                $objScript->setFcode($code);
                $userData = Zend_Auth::getInstance()->getIdentity();
                $objScript->performedReading($userData['f_code*/
                $userData = Zend_Auth::getInstance()->getIdentity();
                $meterHistoryData = array(
                    'f_code'=> $code_periodic,
                    'f_timestamp'=> time(),
                    'f_user_id'=> $userData['f_code'],
                    'f_value'=> $meter_value,
                    'f_checked'=> 0,
                    'f_in_work'=> 0
                );
                $this->dbInsert('t_meters_history', $meterHistoryData);
        }
        
        
		
        
        // delete docs associations
        /*
        if($params['docs_deleted']){
            $this->dbDelete('t_ware_wo', 'f_wo_id = ' . $params['f_code'] . ' AND f_ware_id IN (' . implode($params['docs_deleted']) . ')');
        }*/
        
        // execute change phase scripts
        $this->changePhaseScripts($params, $olds);
        
        // execute field save scripts
        $this->fieldSaveScripts($params, $olds);
        
        Mainsim_Model_Utilities::saveReverseAjax("t_workorders_parent", $params['f_code'], array(0), "upd",'mdl_wo_tg');
    }
    
    public function newWorkorder($params){
        
        try{
            // insert into t_creation_date table
            $insertData = array_combine($params['data']['t_creation_date'][0], $params['data']['t_creation_date'][1]);
            $fakeId = $insertData['f_id'];
            $insertData['f_id'] = null;
            
            // create new progress
            $insertData['fc_progress'] = Mainsim_Model_MobileUtilities::createProgress('workorder', 1);
            $fc_progress = $insertData['fc_progress'];
            $workorderId = $this->dbInsert('t_creation_date', $insertData);
            // update wares/workorder counter (fc_asset_wo in t_wares)
            $newWares = array();
            for($i = 1; $i < count($params['data']['t_ware_wo']); $i++){
                $newWares[] = $params['data']['t_ware_wo'][$i][1];
            }
            $this->waresWorkorderIconAssociation($newWares, $workorderId, $insertData['f_phase_id']);
            // loop tables
            unset($params['data']['t_creation_date']);
            $tables['t_creation_date_history'] = $params['data']['t_creation_date_history'];
            $tables['t_custom_fields'] = $params['data']['t_custom_fields'];
            $tables['t_custom_fields_history'] = $params['data']['t_custom_fields_history'];
            $tables['t_workorders'] = $params['data']['t_workorders'];
            $tables['t_workorders_history'] = $params['data']['t_workorders_history'];
            $tables['t_ware_wo']= $params['data']['t_ware_wo'];
            $tables['t_workorders_parent'] = $params['data']['t_workorders_parent'];
            foreach($tables as $table => $rows){
                if(count($rows) > 1){
                    for($i = 1; $i < count($rows); $i++){
                        
                        $insertData = array_combine($rows[0], $rows[$i]);
                        if($table == 't_creation_date_history'){
                            unset($insertData['f_id']);
                            unset($insertData['f_wo_id']);
                            // replace fake code with real db code
                            $insertData['f_code'] = $workorderId;
                        }
                        else if($table == 't_ware_wo'){
                            unset($insertData['f_id']);
                            unset($insertData['f_code']);
                            // replace fake code with real db code
                            $insertData['f_wo_id'] = $workorderId;
                        }
                        else{
                            unset($insertData['f_id']);
                            unset($insertData['f_wo_id']);
                            // replace fake code with real db code
                            $insertData['f_code'] = $workorderId;
                        }
                        $this->dbInsert($table, $insertData, false);
                    }
                }
            }

            // lock new workorder
            $userData = Zend_Auth::getInstance()->getIdentity();
            if($params['no_lock'] !== true){
                $this->lockWorkorder(array(
                        'f_code' => $workorderId,
                        'f_user_id' => $userData['f_code']
                ));
            }
            
            // reverse ajax
            Mainsim_Model_Utilities::saveReverseAjax("t_workorders_parent", $workorderId, array(0), "add", 'mdl_wo_tg');
            return array(
                'f_code_old' => $fakeId,
                'f_code_new' => $workorderId,
                'fc_progress' => $fc_progress
            );
        }
        catch(Exception $e){
            print("\n" . $e->getMessage() . "\n");
        }
    }
    
    public function releaseWorkorders($params){
        try{
            if($params['userId']){
                $this->dbDelete('t_locked', 'f_code in (' . implode(',', $params['woList']) . ') AND f_user_id =' . $params['userId']);
            }
            else{
                $this->dbDelete('t_locked', 'f_code in (' . implode(',', $params['woList']) . ')');
            }
            
            return $this->createResponseObject(1, "WORKORDERS_RELEASE_DONE", $userData['sessionId'], null);
        } 
        catch(Exception $e){
            return $this->createResponseObject(0, "WORKORDERS_RELEASE_ERROR", $userData['sessionId'], null);
        }
    }
    
    public function waresWorkorderIconAssociation($newWares, $workorder, $phaseId = null){
        
        $selectWo = new Zend_Db_Select($this->db);
        $selectWo->from(array("wo" => "t_workorders"), array('f_type_id'))
                ->join(array("cd" => "t_creation_date"), "wo.f_code = cd.f_id", array())
                ->join(array("wp" => "t_wf_phases"), "cd.f_phase_id = wp.f_number AND cd.f_wf_id = wp.f_wf_id", array())
                ->join(array("wg" => "t_wf_groups"), "wp.f_group_id = wg.f_id", array('group' => 'f_id')) 
                ->where('cd.f_id =' . $workorder)
                ->query()->fetchAll();
        
        $resWo = $this->dbSelect($selectWo);

        // if workorder is closed or deleted decrease asset counter
        if(in_array($phaseId, array(6,7)) && !in_array($resWo[0]['f_id'], array(6,7))){
            

            $select = new Zend_Db_Select($this->db);
            $select->from('t_ware_wo', array('f_ware_id'))
                    ->where('f_wo_id =' . $workorder);
            $resWares = $this->dbSelect($select);
            $wares = array();
            for($i = 0; $i < count($resWares); $i++){
                $wares[] = $resWares[$i]['f_ware_id'];
            }
            // decrease wo counter in ware's parents
            for($i = 0; $i < count($wares); $i++){
                $parents = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $wares[$i]);
                unset($parents[count($parents) - 1]);
                $parents[] = $wares[$i];
                if(count($parents) > 0){
                    $this->db->update('t_wares', array('fc_asset_wo' => new Zend_Db_Expr('fc_asset_wo - 1')), 'f_code IN (' . implode(',', $parents) . ')');
                }
            }

        }
        else{
            // get old workorder asset association
            $select = new Zend_Db_Select($this->db);
            $resOldWares = $select->from('t_ware_wo', array('f_ware_id'))
                                   ->join('t_workorders', 't_ware_wo.f_wo_id = t_workorders.f_code')
                                   ->where('f_wo_id =' . $workorder)
                                   ->query()->fetchAll();
            $oldWares = array();
            for($i = 0; $i < count($resOldWares); $i++){
                $oldWares[] = $resOldWares[$i]['f_ware_id'];
            }
            // decrease wo counter in ware's parents
            if(!$newWares){
                $newWares = array();
            }
            $waresToRemove = array_diff($oldWares, $newWares);
            foreach($waresToRemove as $ware){
                $parents = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $ware);
                unset($parents[count($parents) - 1]);
                if(count($parents) > 0){
                    $this->db->update('t_wares', array('fc_asset_wo' => new Zend_Db_Expr('fc_asset_wo - 1')), 'f_code IN (' . implode(',', $parents) . ')');
                }
            }
            // increase wo counter in ware's parents
            $waresToAdd = array_diff($newWares, $oldWares);
            foreach($waresToAdd as $ware){
                $parents = Mainsim_Model_Utilities::getParentCode('t_wares_parent', $ware);
                unset($parents[count($parents) - 1]);
                if(count($parents) > 0){
                    try{
                        $this->db->update('t_wares', array('fc_asset_wo' => new Zend_Db_Expr('fc_asset_wo + 1')), 'f_code IN (' . implode(',', $parents) . ')');
                    }
                    catch(Exception $e){
                        print($e->getMessage());
                    }
                }
            }
        }   
    }
    
    public function searchNFCWorkorders($params){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
		
        // get user selectors
        $selectors = Mainsim_Model_MobileUtilities::getSelectors($userData['f_code'], 'wares');
		
        // group selector by type
        $selectorsList = array();
        for($i = 0; $i < count($selectors); $i++){
            if(!$selectorsList[$selectors[$i]['f_type_id']]){
                $selectorsList[$selectors[$i]['f_type_id']] = $selectors[$i]['f_selector_id'];
                $selectorsTypeList[] = $selectors[$i]['f_type_id'];
            }
            else{
                $selectorsList[$selectors[$i]['f_type_id']] .= ',' . $selectors[$i]['f_selector_id'];
            }
        }
		
        // get asset f_code associated to NFC code
        $objWares = new Mainsim_Model_MobileWares();
        $resWares = $objWares->getCodeByNFC($params['nfc_value']);
        $f_code = $resWares[0]['f_id'];
        $resultParams['code_list'] = array();
        if($f_code){
            $resultParams['asset_code'] = $resWares[0]['f_id'];
            // search children of ware associated to NFC
            $children = Mainsim_Model_MobileUtilities::getChildren($f_code, 'wares', true);
            $wares = array_merge(array($f_code), $children);
            // get workorders related to wares
            $select = new Zend_Db_Select($this->db);
            $select->from('t_ware_wo')
                ->join('t_creation_date', 't_ware_wo.f_wo_id = t_creation_date.f_id', array())
                ->join('t_workorders', 't_creation_date.f_id = t_workorders.f_code', array())
                ->join('t_wf_phases', "t_creation_date.f_phase_id = t_wf_phases.f_number AND t_creation_date.f_wf_id = t_wf_phases.f_wf_id", array())
                ->join('t_wf_groups', "t_wf_phases.f_group_id = t_wf_groups.f_id", array())      
                ->joinLeft('t_selector_wo', 't_workorders.f_code = t_selector_wo.f_wo_id', array())
                ->joinLeft('t_selectors_types', 't_selector_wo.f_selector_id = t_selectors_types.f_id', array())
                ->joinLeft('t_locked', 't_workorders.f_code = t_locked.f_code', array());
        
			// filter by visibility
			$select->where("t_creation_date.f_visibility & " . $userData['fc_usr_level'] . " != 0")
				   ->where("t_creation_date.f_visibility & " . $userData['fc_usr_group_level'] . " != 0")
				   ->where("t_wf_phases.f_visibility & " . $userData['fc_usr_level'] . " != 0")
				   ->where("t_wf_phases.f_visibility & " . $userData['fc_usr_level'] . "!= 0"); 
        
			// filter by selectors
			foreach($selectorsList as $key => $value){
				$where[] =  't_selector_wo.f_selector_id IN (' . $value. ')';
			}
			if($where){
				$select->where('(' . implode(' AND ', $where)  . ')');
			}
	
			// where clause for workorders not locked/locked
			if(isset($params['locked'])){
				if($params['locked'] == true){
					$select->where('t_locked.f_code IS NOT NULL AND t_locked.f_user_id =' . $userData['f_code']);
				}
				else{
					$select->where('t_locked.f_code IS NULL');
				}
			}
      

			$select->where("t_ware_wo.f_ware_id IN (" . implode(',', $wares) . ")")
				->where("t_workorders.f_type_id IN (" . $params['type_id'] . ")");

            $resWorkorders = $this->dbSelect($select);

         
			for($i = 0; $i < count($resWorkorders); $i++){
                $resultParams['code_list'][] = $resWorkorders[$i]['f_wo_id'];
            }
        }
        else{
            $resultParams['asset_code'] = 0;
        }
        if(count($resultParams['code_list']) > 0){
            return $this->createResponseObject(1, "WORKORDERS_NFC_FOUND", $userData['sessionId'], $resultParams);
        }
        else{
            return $this->createResponseObject(1, "WORKORDERS_NFC_NOT_FOUND", $userData['sessionId'], $resultParams);
        }
    }
}

