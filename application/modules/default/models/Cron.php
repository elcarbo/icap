<?php

 class Mainsim_Model_Cron {

     private $db, $timestart;

     public function __construct() {
         $this->db = Zend_Db::factory(Zend_Registry::get('db'));
         //Zend_session::writeClose();
         $this->timestart = time();
     }

     /**
      * Exec every cron 
      */
     public function executeCronSch() {
         try {
             $sel = new Zend_Db_Select($this->db);
             $res_cron = $sel->from(array("t1" => "t_wares"), array("f_code", "fc_sch_minute", "fc_sch_hour", "fc_sch_dom", "fc_sch_month", "fc_sch_dow", "fc_sch_n_times",
                                 "fc_sch_n_times_executed", "fc_sch_end_date"))->join("t_creation_date", "t_creation_date.f_id = t1.f_code")
                             ->where("t1.f_type_id = 14")->where("t_creation_date.f_phase_id = 1")->query()->fetchAll();

             foreach ($res_cron as $cron) {
                 //verify if now is timed out
                 $fc_sch_end_date = (int) $cron['fc_sch_end_date'];
                 if ($fc_sch_end_date > 0 && $this->timestart > $fc_sch_end_date) {
                     $this->changePhaseSchedule($cron['f_code']);
                     continue;
                 }

                 //control is time to run this script            
                 $min_exp = explode(',', $cron['fc_sch_minute']);
                 $hour_exp = explode(',', $cron['fc_sch_hour']);
                 $day_exp = explode(',', $cron['fc_sch_dom']);
                 $month_exp = explode(',', $cron['fc_sch_month']);
                 $dow_exp = explode(',', $cron['fc_sch_dow']);
                 if ($cron['fc_sch_minute'] != '*' && !in_array((int) date('i'), $min_exp))
                     continue;
                 if ($cron['fc_sch_hour'] != '*' && !in_array((int) date('G'), $hour_exp))
                     continue;
                 if ($cron['fc_sch_dom'] != '*' && !in_array((int) date('d'), $day_exp))
                     continue;
                 if ($cron['fc_sch_month'] != '*' && !in_array((int) date('n'), $month_exp))
                     continue;
                 if ($cron['fc_sch_dow'] != '*' && !in_array((int) date('N'), $dow_exp))
                     continue;

                 $sel->reset();
                 $actions = $sel->from("t_wares", array("fc_action_function"))
                                 ->join(array("t2" => "t_creation_date"), "t_wares.f_code = t2.f_id", array())
                                 ->join(array("t3" => "t_wares_relations"), "t_wares.f_code = t3.f_code_ware_slave", array())
                                 ->where("t2.f_phase_id = 1")->where("t3.f_type_id_slave = 11")
                                 ->where("t3.f_code_ware_master = ?", $cron['f_code'])
                                 ->query()->fetchAll();
                 foreach ($actions as $action) {
                     if (!empty($action['fc_action_function'])) {
                         eval($action['fc_action_function']);
                     }
                 }

                 $n_volte_eseguito = $cron['fc_sch_n_times_executed'] + 1;
                 //executed the eval,update new number of times
                 $this->db->update("t_wares", array("fc_sch_n_times_executed" => $n_volte_eseguito), "f_code = {$cron['f_code']}");

                 //if number of times is not undefined, check if is time to close this schedule
                 if ($cron['fc_sch_n_times']) {
                     if ($n_volte_eseguito >= $cron['fc_sch_n_times']) {
                         $this->changePhaseSchedule($cron['f_code']);
                     }
                 }
             }
         } catch (Exception $e) {
             $this->db->insert(
                     't_logs', array(
                 'f_log' => "Error Occured during scheduled cron script : " . $e->getMessage() . "<br /><br />" . $e->getTraceAsString(),
                 'f_type_log' => 0,
                 'f_timestamp' => $this->timestart
                     )
             );
         }
     }

     /**
      * Exec Cron inside system 
      */
     public function executeCron() {

         try {
             $sel = new Zend_Db_Select($this->db);
             $res_cron = $sel->from(array("t1" => "t_scripts"))->where("f_type = 'cron'")->query()->fetchAll();
             foreach ($res_cron as $cron) {
                 if (!empty($cron['f_script'])) {
                     eval($cron['f_script']);
                 }
             }
             //execute fixed cron                     
             $this->check_ending_contracts();
             $this->updateAssetData();
             $this->checkOverdueDate();
             $this->deleteLockedRecords();
             $this->deleteZombieAccess();
             $this->financialYearEnding();
             $this->deleteOldLogs();
             $this->deleteOldReverseAjax();
             $this->deleteFailAttachments();
             $this->expirationUser();
             $this->deleteOldSelectorSupports();
         } catch (Exception $e) {
             $this->db->insert(
                     't_logs', array(
                 'f_log' => "Error Occured during cron script : " . $e->getMessage() . "<br /><br />" . $e->getTraceAsString(),
                 'f_type_log' => 0,
                 'f_timestamp' => $this->timestart
                     )
             );
         }
         //$this->wo_countdown(); //Va aggiornato con le  priorità configurate come ACTION. 
     }

     /**
      * delete old rows in t_selectors_support
      */
     private function deleteOldSelectorSupports() {
         //ESEGUE LO SCRIPT OGNI DUE ORE, SOLAMENTE NELLE ORE DISPARI (ES.11.00, 13.00 etc..)
         $h = date('G', $this->timestart);
         if (($h % 2 == 1) && ($h != date('G', $this->timestart - 360))) { // delete row old then 1h           
             $this->db->delete("t_selectors_support", "f_timestamp < " . ($this->timestart - 3600));
             $resMax = $this->db->query("select max(f_timestamp) as f_timestamp from t_selectors_support")->fetch();
             if (($this->timestart - $resMax['f_timestamp']) > 7600) { // reset f_id
                 $this->db->query("truncate table t_selectors_support");
             }
         }
     }

     /*
      * delete old export (creation time > 1 hour
      */

     private function deleteOldExports() {
         if (file_exists(APPLICATION_PATH . '/../export/temp/')) {
             if ($handle = opendir(APPLICATION_PATH . '/../export/temp/')) {
                 while (false !== ($entry = readdir($handle))) {
                     if (strpos($entry, ".xlsx") > 0) {
                         $dir = str_replace(".xlsx", "", $entry);
                         if (time() - $dir > 3600) {
                             $objExporter = new Mainsim_Model_Exporter();
                             unlink(APPLICATION_PATH . '/../export/temp/' . $entry);
                             $objExporter->removeDir(APPLICATION_PATH . '/../export/temp/' . $dir);
                         }
                     }
                 }
                 closedir($handle);
             }
         }
     }

     /**
      * @deprecated since version 3.0.7.4
      * Check on condtions 
      */
     private function check_on_condition() {
         ONCONDITIONS();
     }

     /**
      * Update :
      * fc_asset_operational_time
      * fc_asset_availability
      * fc_asset_mtbf
      * fc_asset_mttr
      * fc_asset_mtbr
      * for every not closed,deleted or cloned
      */
     private function updateAssetData() {


         //ESEGUE LO SCRIPT OGNI DUE ORE, ALLA MEZZ'ORA, SOLAMENTE NELLE ORE PARI (ES. 10.30, 12.30, 14.30)
         $h = date('G', $this->timestart + 1800);
         if ($h % 2 == 0 && ($h != date('G', $this->timestart + 1400))) {
             //asset_operational_time
             $sel = new Zend_Db_Select($this->db);
             $res = $sel->from(array('t1' => "t_wares"), array("f_code", "f_start_date", "fc_asset_daily_usage"))
                             ->join(array('t2' => "t_creation_date"), 't1.f_code = t2.f_id', array())
                             ->join(array('t3' => "t_wf_phases"), 't2.f_phase_id = t3.f_number and t2.f_wf_id = t3.f_wf_id', array())
                             ->where("f_type_id = 1")->where("fc_asset_daily_usage != 0")->where('t3.f_group_id not in (6,7,8)')
                             ->query()->fetchAll();
             $scriptModule = new Mainsim_Model_Script($this->db, 't_workorders', 0, array('f_type_id' => 1));
             $now = $this->timestart;
             $tot = count($res);
             for ($i = 0; $i < $tot;  ++$i) {
                 $sd = (int) $res[$i]['f_start_date'];
                 if ($sd <= 0)
                     continue;
                 $daily_usage = (int) $res[$i]['fc_asset_daily_usage'];
                 $op_time_sec = (int) (($now - $sd) * $daily_usage);
                 $asset_operational_time = (int) (($op_time_sec) / (60 * 60));
                 $this->db->update("t_wares", array("fc_asset_operational_time" => $asset_operational_time), "f_code = {$res[$i]['f_code']}");
                 $scriptModule->fc_asset_available($res[$i]['f_code']);
                 $scriptModule->wo_fc_asset_mtbf($res[$i]['f_code']);
                 $scriptModule->wo_fc_asset_mttr($res[$i]['f_code']);
                 $scriptModule->wo_fc_asset_mtbr($res[$i]['f_code']);
             }
         }
     }

     /**
      * check ending contracts, if terminate or over forewarning
      * time, open a workorders     
      */
     private function check_ending_contracts() {
         $now = $this->timestart;
         /*
         if (date('j', $now) == date('j', $now - 400)) {
             return;
         }
         */
         $select = new Zend_Db_Select($this->db);
         $resContracts = $select->from("t_wares", array('f_code', 'fc_contract_ending_date', 'fc_contract_forewarning'))
                         ->join(array("t2" => "t_creation_date"), "f_code = t2.f_id", array("f_title", "f_description"))
                         ->where("f_type_id = 7")->query()->fetchAll();
         $tot = count($resContracts);
         $userinfo = Mainsim_Model_Login::getUSerInfo(1, $this->db);
         $wo = new Mainsim_Model_Workorders($this->db);
         $trsl = new Mainsim_Model_Translate();
         for ($i = 0; $i < $tot;  ++$i) {
             $line = $resContracts[$i];
             $params = array('f_code' => 0, 'f_type_id' => 20, 'f_module_name' => 'mdl_wo_tg');
             $fc_contract_expiring = null;
             $expDays = (int) $line['fc_contract_forewarning'] * 86400;
             if ($now > ($line['fc_contract_ending_date'] - $expDays)) {
                 $fc_contract_expiring = 1;
                 if ($now > $line['fc_contract_ending_date']) {
                     $fc_contract_expiring = 2;
                 }
                 $this->db->update("t_wares", array('fc_contract_expiring' => $fc_contract_expiring), "f_code = {$line['f_code']}");
                 //check if this wo has been already opened
                 $select->reset();
                 $resCeCheck = $select->from("t_workorders", array("num" => "count(f_code)"))->where("f_type_id = 20")
                                 ->join("t_ware_wo", "f_code = f_wo_id", array())
                                 ->join("t_creation_date", "f_code = t_creation_date.f_id", array())
                                 ->join("t_wf_phases", "t_creation_date.f_phase_id = f_number and "
                                         . "t_creation_date.f_wf_id = t_wf_phases.f_wf_id", array())
                                 ->where("f_group_id not in (6,7,8)")->where("f_ware_id = ?", $line['f_code'])
                                 ->query()->fetch();
                 if($resCeCheck['num'] > 0) {
                     continue;
                 }
                 $params['f_title'] = $trsl->_("Contract") . ' ' . $line['f_title'] . ' ' . $trsl->_('is expiring.');
                 $params['f_description'] = Mainsim_Model_Utilities::chg($line['f_description']);
                 $params['t_wares_7'] = array(
                     'f_code' => array($line['f_code']),
                     'f_code_main' => 0, 'Ttable' => 't_wares',
                     'f_type' => 7, 'f_module_name' => 'mdl_wo_contract_tg'
                 );
                 $wo->newWorkOrder($params, true, $userinfo);
             }
         }
     }

     private function changePhaseSchedule($f_code) {
         $time = $this->timestart;
         $creation['f_timestamp'] = $time;
         $custom['f_timestamp'] = $time;
         $ware['f_timestamp'] = $time;
         $creation['f_phase_id'] = 3;
         Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_creation_date", $creation);
         Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_wares", $ware);
         Mainsim_Model_Utilities::newRecord($this->db, $f_code, "t_custom_fields", $custom);
     }

     /*
      * check if are presents forgot periodic generate request
      * and put in fc_pm_overdue how long are there forgotten
      */

     private function checkOverdueDate() {
         $now = $this->timestart;
         if (date('j', $now) != date('j', $now - 400)) {
             $sel = new Zend_Db_Select($this->db);
             $res = $sel->from(array("t1" => "t_creation_date"))
                             ->join(array("t2" => "t_workorders"), "t1.f_id = t2.f_code", array("f_code", 'f_end_date', 'f_code_periodic'))
                             ->join(array("t3" => "t_wf_phases"), "t1.f_wf_id = t3.f_wf_id and t3.f_number = t1.f_phase_id", array())
                             ->join(array("t4" => "t_wf_groups"), "t3.f_group_id = t4.f_id", array())
                             ->where("f_end_date < $now and f_end_date > 0")->where("f_type_id = 4")->where("t4.f_id < 6")
                             ->query()->fetchAll();
             $tot_a = count($res);
             for ($a = 0; $a < $tot_a;  ++$a) {
                 $sel->reset();
                 $overdue_hour = number_format(($now - $res[$a]['f_end_date']) / 86400, 2);
                 if($res[$a]['f_code_periodic'])
                 $this->db->update("t_workorders", array("fc_pm_overdue" => $overdue_hour), "f_code = {$res[$a]['f_code_periodic']}");
             }
         }
     }

     private function wo_countdown() {
         $time = $this->timestart;
         if (date('h') != date('h', $this->timestart - 360)) {
             $select = new Zend_Db_Select($this->db);
             $res = $select->from(array("t1" => "t_workorders"), array('f_start_date', 'f_priority', 'f_code', 'fc_wo_countdown'))
                             ->join(array("t2" => "t_creation_date"), "t1.f_code = t2.f_id", array())
                             ->where("t1.f_type_id IN(1,4,6,7,10,13)")->where("f_phase_id not in (6,7,8,9,10)")
                             ->query()->fetchAll();
             for ($i = 0; $i < count($res); $i++) {
                 if (!empty($res[$i]['fc_wo_countdown']) && date('h') == date('h', $res[$i]['fc_wo_countdown'])) {
                     continue; // for this hour update has been made
                 }
                 $update['fc_wo_countdown'] = null;

                 if ($res[$i]['f_start_date'] != "") {

                     switch ($res[$i]['f_priority']) {

                         case '0':
                             break;
                         // priority 1 --> 5 days
                         case '1':
                             //echo ("code =" . $res[$i]['f_code'] . " ". ($this->timestart - $res[$i]['f_start_date'])) . "<br>";
                             $update['fc_wo_countdown'] = (int) (120 - (($this->timestart - $res[$i]['f_start_date']) / 3600));
                             break;
                         // priority 2 --> 2 hours
                         case '2':
                             // 2 ore
                             //echo ("code =" . $res[$i]['f_code'] . " ". ($this->timestart - $res[$i]['f_start_date'])) . "<br>";
                             $update['fc_wo_countdown'] = (int) (2 - (($this->timestart - $res[$i]['f_start_date']) / 3600));
                             break;
                         // priority 3 --> 1 hour
                         case '3':
                             // 1 ora
                             //echo "code =" . $res[$i]['f_code'] . " ". $this->timestart . " " . $res[$i]['f_start_date'] . "<br>";
                             $update['fc_wo_countdown'] = (int) (1 - (($this->timestart - $res[$i]['f_start_date']) / 3600));
                             break;
                     }
                 }
                 if (!empty($update)) {
                     $this->db->update("t_workorders", $update, "f_code = " . $res[$i]['f_code']);
                 }
             }
         }
     }

     private function mailToWo() {
         $mail = new Zend_Mail_Storage_Pop3(array(
             'host' => EMAIL_IN_ADDRESS,
             'user' => EMAIL_IN_USERNAME,
             'password' => EMAIL_IN_PASSWORD
                 )
         );
         $admin_info = Mainsim_Model_Login::getUSerInfo(1, $this->db);
         foreach ($mail as $id_mail => $message) {
             $i = 1;
             $multiMail = array();
             if ($message->isMultipart()) {
                 $i = 1;
                 while ($message->isMultipart()) {
                     try {
                         $part = $message->getPart($i);
                     } catch (Exception $e) {
                         break;
                     }
                     $exp_type = explode('; ', $part->contentType);
                     if ($exp_type[0] == 'text/plain') {
                         $multiMail['body'] = $part->getContent();
                     } else { //is an attachment
                         $type = $part->getHeaders();
                         $type = $type['content-disposition'];
                         if ($type != 'inline') {
                             $file = str_replace('"', '', $exp_type[1]);
                             $file = str_replace('name=', '', $file);
                             $multiMail['attachments'][] = array(
                                 'contentType' => $exp_type[0],
                                 'file' => $file,
                                 'extension' => substr($file, strrpos($file, ".") + 1),
                                 'content' => base64_decode($part->getContent())
                             );
                         }
                     }
                     $i++;
                 }
             } else {
                 $multiMail['body'] = $message->getContent();
             }

             //explode body of mail and get data
             $body_exp = explode(PHP_EOL, $multiMail['body']);
             $body_new = array();
             $tot_a = count($body_exp);
             for ($a = 0; $a < $tot_a;  ++$a) {
                 $value = trim($body_exp[$a]);
                 if (empty($value))
                     continue;
                 $exp_value = explode(' : ', $value);
                 $key_value = $exp_value[0];
                 unset($exp_value[0]);
                 $value_ok = implode(' : ', $exp_value);
                 $body_new[$key_value] = $value_ok;
             }

             //creo la wr
             $params = array(
                 'f_code' => 0,
                 'f_type_id' => 12,
                 'f_module_name' => 'mdl_wr_tg',
                 'f_title' => $body_new['Short Title'],
                 'f_description' => $body_new['Full Description of Failure'],
                 'priority_sa' => $body_new['Priority'],
                 'ship_department' => $body_new['Department'],
                 'ship_name' => $body_new['Ship Name'],
                 'failure_date' => strtotime(str_replace('/', '-', $body_new['Claim Date'])),
                 'list_material_needed' => $body_new['Parts of Material Needed'],
                 'gfr_number' => $body_new['Claim Number'],
                 'position' => $body_new['Claim Location']
             );

             if (isset($_GET['test'])) {
                 $wo = new Mainsim_Model_Workorders();
                 $wa = new Mainsim_Model_Wares();
                 $res = $wo->newWorkOrder($params, true, $admin_info);
                 if (isset($res['f_code'])) {
                     //create attachment
                     if (isset($multiMail['attachments'])) {
                         $tot_b = count($multiMail['attachments']);
                         $params = array(
                             'f_module_name' => 'mdl_doc_tg',
                             'f_code' => 0,
                             'f_type_id' => 5
                         );
                         for ($b = 0; $b < $tot_b;  ++$b) {
                             //inserisco il file in tmp
                             $data = array();
                             $path_temp = realpath(APPLICATION_PATH . '/../attachments/temp/');
                             $ext = $multiMail['attachments'][$b]['extension'];
                             $filename = substr($multiMail['attachments'][$b]['file'], 0, strrpos($multiMail['attachments'][$b]['file'], "."));
                             $session_id = $this->timestart . '_1';
                             if (file_put_contents(realpath($path_temp . '/') . '/' . $session_id . '.' . $ext, $multiMail['attachments'][$b]['content'])) {
                                 $data = array(
                                     "f_code" => 0,
                                     "f_session_id" => $session_id,
                                     "f_timestamp" => $this->timestart,
                                     "f_active" => 0,
                                     "f_fieldname" => "fc_doc_attach",
                                     "f_file_name" => $filename,
                                     "f_file_ext" => $ext,
                                     "f_type" => 'doc',
                                     "f_mime" => $multiMail['attachments'][$b]['contentType'], // TODO: add
                                     "f_path" => realpath($path_temp) . '/' . $session_id . '.' . $ext //temp path                        
                                 );
                                 $this->db->insert("t_attachments", $data);
                             }
                             $params['fc_doc_attach'] = $session_id . '|' . $multiMail['attachments'][$b]['file'];
                             $params['t_workorders_1,4,6,7,10,13'] = array(
                                 "f_code" => array($res['f_code']),
                                 "f_code_main" => 0,
                                 "f_type" => "1,4,6,7,10,13",
                                 "Ttable" => "t_workorders",
                                 "f_module_name" => "mdl_doc_wo_tg"
                             );
                             $wa->newWares($params, $admin_info);
                         }
                     }
                 }
             }
             //unset($mail[$id_mail]);            
         }
         die('sono qui');
     }

     /**
      * check if there are locked rows over 30 minutes 
      */
     private function deleteLockedRecords() {
         $select = new Zend_Db_Select($this->db);
         $res = $select->from("t_locked", "f_code")
                         ->join('t_users', "t_users.f_code = t_locked.f_user_id", array())
                         ->where($this->timestart . " - f_timestamp > 1800")
                         ->where('fc_usr_online <> 2')
                         ->query()->fetchAll();
         foreach ($res as $line) {
             $this->db->delete("t_locked", "f_code = " . $line['f_code']);
         }
     }

     /**
      * delete old logs two day before
      */
     private function deleteOldLogs() {
         $this->db->delete("t_logs", "f_type_log = 1 and f_timestamp < " . ($this->timestart - 172800));
     }

     private function deleteZombieAccess() {
         $select = new Zend_Db_Select($this->db);
         $logout = new Mainsim_Model_Login();
         $res = $select->from("t_access_registry", "f_user_id")
                         ->join("t_users", "f_user_id = f_code", array())
                         ->where($this->timestart . " - f_last_action_on > 300")
                         ->where("fc_usr_online <> 2")
                         ->query()->fetchAll();
         foreach ($res as $line) {
             $logout->logout($line['f_user_id']);
         }
     }

     private function financialYearEnding() {
         $select = new Zend_Db_Select($this->db);
         $res = $select->from('t_creation_date', array('FINANCIAL_DATE_END' => 'f_description'))
                         ->where("f_category = 'SETTING'")->where("f_title = 'FINANCIAL_DATE_END'")
                         ->query()->fetch();
         if (!empty($res['FINANCIAL_DATE_END']) && (date('d') != date('d', $this->timestart - 360)) && (date('m-d') == $res['FINANCIAL_DATE_END'])) {// check if this is a new day and if montth and day are the same of FINANCIAL_DATE_END            
             $select->reset();
             $time = $this->timestart;
             // get assets
             $res = $select->from("t_creation_date", array('f_id'))
                             ->join('t_wf_phases', 't_wf_phases.f_wf_id = t_creation_date.f_wf_id AND t_wf_phases.f_number = t_creation_date.f_phase_id', array())
                             ->where('f_category = "ASSET"')
                             ->where('f_group_id < 6')
                             ->query()->fetchAll();
             $wareData = array(
                 'fc_asset_direct_costs_ytd' => 0,
                 'fc_asset_indirect_costs_ytd' => 0,
                 'fc_asset_total_costs_ytd' => 0,
                 'fc_asset_balance' => 0,
                 'f_timestamp' => $time
             );
             $customData = array(
                 'fc_costs_for_downtime' => 0,
                 'fc_costs_for_material' => 0,
                 'fc_costs_for_labor' => 0,
                 'fc_costs_for_tool' => 0,
                 'fc_costs_for_service' => 0,
                 'f_timestamp' => $time
             );
             $creationData = array(
                 'f_timestamp' => $time
             );
             for ($i = 0; $i < count($res); $i++) {
                 Mainsim_Model_Utilities::newRecord($this->db, $res[$i]['f_id'], 't_creation_date', $creationData, 'Reset YTD Cost');
                 Mainsim_Model_Utilities::newRecord($this->db, $res[$i]['f_id'], 't_wares', $wareData, 'Reset YTD Cost');
                 Mainsim_Model_Utilities::newRecord($this->db, $res[$i]['f_id'], 't_custom_fields', $customData, 'Reset YTD Cost');
             }
         }
     }

     private function deleteFailAttachments() {
         $select = new Zend_Db_Select($this->db);
         $resAttach = $select->from('t_attachments')->where('f_code = 0')
                         ->where('f_timestamp < ' . ($this->timestart - 86400))
                         ->query()->fetchall();
         $tot = count($resAttach);
         for ($i = 0; $i < $tot;  ++$i) {
             $line = $resAttach[$i];
             @unlink($line['f_path']);
             $this->db->delete('t_attachments', "f_id = {$line['f_id']}");
         }
     }

     private function deleteOldReverseAjax() {

         //MODIFICATO DA MATTEO IL 3 MARZO 2017. QUERY OTTIMIZZATA.
         $tmp_to_check = $this->timestart - 86400;
         $this->db->delete('t_reverse_ajax', "f_timestamp < {$tmp_to_check}");

         /*
           $select = new Zend_Db_Select($this->db);
           $resRA = $select->from('t_reverse_ajax','f_id')
           ->where('f_timestamp < '.($this->timestart - 86400))
           ->query()->fetchall();
           $tot = count($resRA);
           for($i = 0;$i < $tot;++$i) {
           $this->db->delete('t_reverse_ajax',"f_id = {$resRA[$i]['f_id']}");
           } */
     }

     /**
       check if user is expired,if true, change phase in deactive
      */
     private function expirationUser() {
         if (date('d', $this->timestart) != date('d', $this->timestart - 360)) { //check date between and 5 minute ago
             $select = new Zend_Db_Select($this->db);
             $res = $select->from('t_users', array('f_code', 'fc_usr_usn_expiration'))
                             ->where('fc_usr_usn_expiration < ' . $this->timestart)->query()->fetchAll();
             $tot = count($res);
             for ($i = 0; $i < $tot;  ++$i) {
                 if (empty($res[$i]['fc_usr_usn_expiration'])) {
                     continue;
                 }
                 $this->db->update('t_creation_date', array('f_phase_id' => 2), "f_id = {$res[$i]['f_code']}");
             }
         }
     }

     public function log($message, $type, $time) {

         $this->db->insert(
                 't_logs', array(
             'f_log' => $message,
             'f_type_log' => $type,
             'f_timestamp' => $time
                 )
         );
     }

 }
 