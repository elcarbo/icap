<?php

class Mainsim_Model_Utilities
{
    private $db;
    private static $settings;
    
    public function __construct() 
    {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    /**
     * Get environment system settings from db
     * @param type $key define which data is require (optional). If empty all settings are returned
     * @return $settings or $value retrived from a non empty $key
     */
    public function getSettings($key = null){
        if (empty(self::$settings)) {
            $select = new Zend_Db_select($this->db);
            $select->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
                    ->join(array("s" => "t_systems"), "c.f_id = s.f_code")
                    ->where("c.f_type = 'SYSTEMS'")
                    ->where("c.f_category = 'SETTING'")
                    ->where("s.fc_sys_server_only IS NULL")
                    ->orWhere("s.fc_sys_server_only != 1");
            $res = $select->query()->fetchAll();
            for($i = 0; $i < count($res); $i++){
                $settings[$res[$i]['f_title']] = $res[$i]['f_description'];
            }
            self::$settings = $settings;
        }
        if ($key){
            if(isset(self::$settings[$key])){
                
                return self::$settings[$key];
            }else{
                return 0;
            }
        }else{
            return self::$settings;    
        }
    }
    
    public static function updateAuxSelectorFields($type, $f_code, $db, $newSelectors, $crossedElements = null){
        $select = new Zend_Db_Select($db);

        $propagateToChildren = false;
        $updateChildrenHierarchy = false;
        $updateChildrenStatements = [];
        $excludeSelFromParents = []; // per i wo non escludere nessun tipo di selettore dei parents
        
        // get selectors and hierarchy saved in database
        $select->from('t_creation_date', ['fc_hierarchy_selectors', 'fc_hierarchy_codes'])
            ->where('f_id = ' . $f_code);
        $resDatabaseData = $select->query()->fetchAll();

        // set tables to use by type
        switch($type){
            // wares
            case 'wares':
                $parentTable = 't_wares_parent';
                break;
            // workorders
            case 'workorders':
                $parentTable = 't_workorders_parent';
                $select->reset();
                $select->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
                    ->join(array("s" => "t_systems"), "c.f_id = s.f_code", [])
                    ->where("c.f_type = 'SYSTEMS'")
                    ->where("c.f_category = 'SETTING'")
                    ->where("c.f_title = 'WO_AUX_SEL_PROPAGATION_FROM_PARENTS' ")->where("c.f_phase_id = 1");
                $woAuxSelProp = $select->query()->fetch();
                $select->reset();
                //var_dump(!empty($woAuxSelProp),$woAuxSelProp['f_description']); die("debug_settings");

                if(!empty($woAuxSelProp))
                {
                    if(!empty($woAuxSelProp['f_description'])){ //esiste il setting ed è valorizzato
                        $excludeSelFromParents = explode(",", $woAuxSelProp['f_description']);
                        array_walk($excludeSelFromParents, function(&$value, &$key) {
                            $value = "SELECTOR ".$value;
                        });
                    }
                    else $excludeSelFromParents = -1; //se esiste il setting ed è vuoto escludi tutti i selettori dei parents
                }
                break;
        }
        //var_dump($excludeSelFromParents);die;
        // get selectors from crossed elements
        $crossedSelectors = [];
        if($crossedElements){
            $select->reset()->from('t_creation_date', ['fc_hierarchy_selectors'])
                ->where('f_id IN (' . implode(',', $crossedElements) . ')');
            $resCrossedSelectors = $select->query()->fetchAll();
            if(count($resCrossedSelectors) > 0){
                for($i = 0; $i < count($resCrossedSelectors); $i++){
                    if($resCrossedSelectors[$i]['fc_hierarchy_selectors'] != null){
                        $aux = explode(',', $resCrossedSelectors[$i]['fc_hierarchy_selectors']);
                        for($j = 0; $j < count($aux); $j++){
                            $aux2 = explode(':', $aux[$j]);
                            $crossedSelectors[] = $aux2[1];
                        }
                    }
                }
            }
        }
        //print_r($crossedSelectors);

        // get selectors from parent
        $parentSelectors = [];
        $select->reset()->from('t_creation_date', ['fc_hierarchy_selectors'])
            ->join($parentTable, 't_creation_date.f_id = ' . $parentTable . '.f_parent_code', [])
            ->where($parentTable . '.f_code =' . $f_code);
        $resParentSelectors = $select->query()->fetch();

        if(!empty($resParentSelectors['fc_hierarchy_selectors'])){
                $aux = explode(',', $resParentSelectors['fc_hierarchy_selectors']);
                for($j = 0; $j < count($aux); $j++){
                    $aux2 = explode(':', $aux[$j]);                    
                    if(is_array($excludeSelFromParents) && !in_array($aux2[0], $excludeSelFromParents))   $parentSelectors[] = $aux2[1];
                }
        }
       
        // get selectors (and selector's parents) directly related
        $directSelectors = [];
        if(count($newSelectors) > 0){
            for($i = 0; $i < count($newSelectors); $i++){
                $directSelectors[] = $newSelectors[$i];
                $directSelectors = array_merge($directSelectors, Mainsim_Model_Utilities::getParentCode('t_selectors_parent', $newSelectors[$i], array(), $db));
            }
        }
        //print_r($directSelectors);
        $selectors = array_unique(array_merge($crossedSelectors, $parentSelectors, $directSelectors));
        
        if(count($selectors) > 0){
            //var_dump($selectors); die("9999");
            // get selector categories
            $select->reset();
            $select->from('t_selectors', ['f_code', 'f_type_id'])
                ->where('f_code IN (' . implode(',', $selectors) . ')');
            $resSelectorsCategories = $select->query()->fetchAll();
            
            // re-initialize selectors array
            $selectors = [];
            
            // prepare associative array of selectors where key = code and value = category:code
            for($i = 0; $i < count($resSelectorsCategories); $i++){
                $selectors[] = 'SELECTOR ' . $resSelectorsCategories[$i]['f_type_id'] . ':' . $resSelectorsCategories[$i]['f_code'];
            }
        }    
            

        // if there are selectors saved in db make an associative array where key = code and value = category:code
        $databaseSelectors = [];
        if($resDatabaseData[0]['fc_hierarchy_selectors']){
            $databaseSelectors = explode(',', $resDatabaseData[0]['fc_hierarchy_selectors']);
        }
        // calculate selectors added and removed
        $addedSelectors = array_diff($selectors, $databaseSelectors);
        $removedSelectors = array_diff($databaseSelectors, $selectors);
        
        // update fc_hierachy_selectors
        if(count($addedSelectors) > 0 || count($removedSelectors) > 0){
            $db->update('t_creation_date', ['fc_hierarchy_selectors' => implode(',', $selectors)], 'f_id = ' . $f_code);

            //escludo nella propagazione ai figli le categorie di selettori specificati nel setting 'WO_AUX_SEL_PROPAGATION_FROM_PARENTS'
            $addedSelectors = array_filter($addedSelectors, function ($aux) use($excludeSelFromParents){ 
                $aux2 = explode(':', $aux);
                 return is_array($excludeSelFromParents) && !in_array($aux2[0], $excludeSelFromParents);
            });
            $removedSelectors = array_filter($removedSelectors, function ($aux) use($excludeSelFromParents){ 
                $aux2 = explode(':', $aux);
                 return is_array($excludeSelFromParents) && !in_array($aux2[0], $excludeSelFromParents);
            });
            $propagateToChildren = true;
        }

        // get hierarchy just saved in database 
        $databaseHierarchy = Mainsim_Model_Utilities::getParentCode($parentTable, $f_code, array(), $db);

        //check if hierarchy has been modified 
      
        //usare controllo sottostante se la funzione getParentCode non restituisce la gerarchia nell'ordine di 'fc_hierarchy_codes'
        //$dbhc = explode(",", $resDatabaseData[0]['fc_hierarchy_codes']);
        //$flagHc = ((count(array_diff($databaseHierarchy,$dbhc)) + count(array_diff($dbhc,$databaseHierarchy)))>0)?true:false;
        
        $databaseHierarchy = implode(',', $databaseHierarchy);
        
        // if hierarchy has been modified update element and his children
        if($databaseHierarchy != $resDatabaseData[0]['fc_hierarchy_codes']) {
                $db->update('t_creation_date', ['fc_hierarchy_codes' => $databaseHierarchy], 'f_id = ' . $f_code);
               // select from t_creation_date where fc_hierarchy_codes like '%f_code,' OR fc_hierarchy_codes like '%,f_code,%'
               $updateChildrenHierarchy = true; $propagateToChildren = true;
               $oldChildrenHierarchy = $f_code . ',' . $resDatabaseData[0]['fc_hierarchy_codes'];
               $newChildrenHierarchy = $f_code . ',' . $databaseHierarchy;
        }

        // propagation of changes to children
        if($propagateToChildren){
           
            $allUpdateStatements = "";
            //$children = Mainsim_Model_Utilities::getChildCode($parentTable, $f_code, $aux, $db);
            // get all children using fc_hierarchy_codes instead of Mainsim_Model_Utilities::getChildCode. faster?
            $select->reset()->from('t_creation_date as cd', ['f_id', 'fc_hierarchy_codes', 'fc_hierarchy_selectors'])
                ->joinLeft('t_selector_ware as sw','sw.f_ware_id = cd.f_id',["f_selector_id"])
                ->where("cd.fc_hierarchy_codes LIKE '" . $f_code . ",%' OR cd.fc_hierarchy_codes LIKE '%," . $f_code . ",%'")
                ->where('cd.f_phase_id = 1')
                ->order('cd.f_id ASC');
            
            $resChildren = $select->query()->fetchAll();
            
             // ricostruisco l'array dei figli settando per ognuno, come valore di 'cross_sw' , l'espressione regolare
             // che rappresenta l'elenco dei propri selettori crossati
            if(count($resChildren) > 0){
                $resChildren2 = [];
                $a = array_column($resChildren, 'f_id');
                $b = array_reverse($a,true);
                $ids =  array_unique($a);
                foreach($ids  as $ware_id){
                    $from = array_search($ware_id, $a);
                    $to = array_search($ware_id, $b);
                    $children = $resChildren[$from];
                    $temp = array_slice($resChildren, $from, $to-$from+1);
                    $children['cross_sw'] = implode('|:',array_column($temp, 'f_selector_id'));
                    array_push($resChildren2,$children);
                }
                $resChildren = $resChildren2;
            }
            
            //$resChildren = empty($resChildren[0]["f_id"])?[]:$resChildren;
            
            
            for($i = 0; $i < count($resChildren); $i++){
                $updateStatement = [
                    "fc_hierarchy_codes" => $resChildren[$i]["fc_hierarchy_codes"],
                    "fc_hierarchy_selectors" => (empty($resChildren[$i]['fc_hierarchy_selectors']))?[]:explode(',', $resChildren[$i]['fc_hierarchy_selectors'])
                ];

                // update hierarchy
                if($updateChildrenHierarchy == true){
                    $updateStatement["fc_hierarchy_codes"] = str_replace($oldChildrenHierarchy, $newChildrenHierarchy, $updateStatement["fc_hierarchy_codes"]);
                }

                // add selectors
                if(count($addedSelectors) > 0){
                    $updateStatement["fc_hierarchy_selectors"] = array_unique(array_merge($updateStatement["fc_hierarchy_selectors"], $addedSelectors));
                }
                
                $removedSelectors2 = $removedSelectors;
                
                //se $removedSelectors contiene selettori crossati con il figlio questi non verranno rimossi in "fc_hierarchy_selectors"
                if(!empty($removedSelectors) && !empty($resChildren[$i]['cross_sw'])){
                    $match = "/(:".$resChildren[$i]['cross_sw'].")/";
                    $removedSelectors2 = array_filter($removedSelectors, function($el) use ($match) {
                         return ( preg_match($match, $el) !== 1 );
                    });   
                }

                // remove selectors
                if(count($removedSelectors2) > 0){
                    $updateStatement["fc_hierarchy_selectors"] = array_diff($updateStatement["fc_hierarchy_selectors"], $removedSelectors2);
                }

                $updateStatement["fc_hierarchy_selectors"] = implode(',', $updateStatement["fc_hierarchy_selectors"]);
                
                // add element update query
                $allUpdateStatements .= "UPDATE t_creation_date SET fc_hierarchy_codes = '" . $updateStatement["fc_hierarchy_codes"] . "', fc_hierarchy_selectors = '" . $updateStatement["fc_hierarchy_selectors"] . "' WHERE f_id = " . $resChildren[$i]['f_id'] . ";\n";

            }

            if(count($resChildren) > 0){
                // update 
                try{
                    $db->query($allUpdateStatements);
                }
                catch(Exception $e){
                    print($e->GetMessage());
                }
            }
        }  
    }


    
    public static function get_tab_cols($tab) 
    {        
        $db = Zend_Db::factory(Zend_Registry::get('db'));
        Zend_Db_Table::setDefaultAdapter($db);
        $table = new Zend_Db_Table($tab);
        $columns = $table->info('cols'); 
      #  print '<pre>'; print_r($columns); print '</pre>';   
        $db->closeConnection();
     #   print 234;
        return $columns;
    }
    
    public static function chg($str) {
        $cp1252_map = array ("\xc2\x80" => "\xe2\x82\xac",
        "\xc2\x82" => "\xe2\x80\x9a",
        "\xc2\x83" => "\xc6\x92",    
        "\xc2\x84" => "\xe2\x80\x9e",
        "\xc2\x85" => "\xe2\x80\xa6",
        "\xc2\x86" => "\xe2\x80\xa0",
        "\xc2\x87" => "\xe2\x80\xa1",
        "\xc2\x88" => "\xcb\x86",
        "\xc2\x89" => "\xe2\x80\xb0",
        "\xc2\x8a" => "\xc5\xa0",
        "\xc2\x8b" => "\xe2\x80\xb9",
        "\xc2\x8c" => "\xc5\x92",
        "\xc2\x8e" => "\xc5\xbd",
        "\xc2\x91" => "\xe2\x80\x98",
        "\xc2\x92" => "\xe2\x80\x99",
        "\xc2\x93" => "\xe2\x80\x9c",
        "\xc2\x94" => "\xe2\x80\x9d",
        "\xc2\x95" => "\xe2\x80\xa2",
        "\xc2\x96" => "\xe2\x80\x93",
        "\xc2\x97" => "\xe2\x80\x94",
        "\xc2\x98" => "\xcb\x9c",
        "\xc2\x99" => "\xe2\x84\xa2",
        "\xc2\x9a" => "\xc5\xa1",
        "\xc2\x9b" => "\xe2\x80\xba",
        "\xc2\x9c" => "\xc5\x93",
        "\xc2\x9e" => "\xc5\xbe",
        "\xc2\x9f" => "\xc5\xb8");
        return strtr ( utf8_encode ( $str ), $cp1252_map );
    }
    
    /**
     * clear all passed string or array and return it has utf8 decoded
     */
    public static function clearChars($params)
    {
        if(is_string($params)){
            $old = $params;
            $clear = utf8_decode($params);
            if(strpos($clear,'?')!== false) {                
                $clear = iconv('UTF-8', 'CP1252', $old);                
            }
        }
        else if(is_array($params)){
            $clear = array();
            foreach ($params as $key => $value) {
                if(is_array($value) || $value==NULL){$clear[$key] = $value; continue;}
                $old = $value;
                $value = utf8_decode($value);
                if(strpos($value,'?')!== false) {
                    $value = iconv('UTF-8', 'CP1252', $old);
                }
                $clear[$key] = $value;
            }
        }
        else{
            $clear = $params;
        }
        return $clear;
    }
    
    public static function getOld($db,$f_code,$table = '',$field = 'f_code') 
    {
        if(empty($table)) return array();
        $select = new Zend_Db_Select($db);
        $select->from($table)->where("$field = ?",$f_code);        
        $result = $select->query()->fetch();  

        return $result;
    }
    
    /**
     * Disactive old record and insert the new
     * In case of old db version, follow old procedure with f_active
     */
    public static function newRecord($db,$f_code,$table = "t_selectors",$new_rec = array(),$history_type = "Script",$timestamp = null)
    {
        //Disactive record        
        $select = new Zend_Db_Select($db);
        $field = $table != 't_creation_date'?'f_code':'f_id';
        $rec = $select->from($table)->where("$field = ?",$f_code)->query()->fetch();
        if(empty($rec)) return;        
        if($field == 'f_id') {            
            $rec['f_code'] = $rec['f_id'];            	
            $rec['fc_type_history'] = $history_type;
        }               
        if($table == 't_workorders' || $table == 't_selectors' || $table == 't_wares') {            
            $rec = self::getHistoryCross($table, $f_code, $rec,$db);            
        }
        
        unset($rec['f_id']);
	$rec['f_timestamp'] = (empty($timestamp))?time():$timestamp;
        
        $db->insert("{$table}_history",$rec);                                    
        if(!empty($new_rec)) {
            unset($new_rec['f_id']);
            $db->update($table, $new_rec,"$field = $f_code");            
        }   
    }
    
    public static function getBookmark($module_name, $f_type = 'view', $uid = 0) 
    {        
        $db = Zend_Registry::isRegistered('db_connection_editor')?Zend_Registry::get('db_connection_editor'):Zend_Db::factory(Zend_Registry::get('db'));		
        $sys = new Mainsim_Model_System($db);        
        
        if($f_type == "bookmark") { // Looking for bookmark
            $res = $sys->getFavourite($module_name, $uid);
        } else {            
            $res = $sys->getBookmark($module_name, 'Default', 0);
        }        
        if($f_type == 'view' && empty($res)) {
            $mod = explode("_", $module_name);
            $submod = explode("$", $mod[1]);
            $res = $sys->getBookmark("mdl_".($submod[0])."_tg", 'default');            
            if(empty($res)) {
                $res = array(
                    "f_id" => 0,
                    "f_module_name" => $module_name,
                    "f_name" => "default",
                    "view" => array(
                        "f_id" => 0,
                        "f_module_name" => $module_name,
                        "f_name" => "default",
                        "f_properties" => '[["f_title","Title",0,1,1,280,[],[],[],""],["f_code","Code",0,0,1,80,[],[],[],""],["f_description","Description",1,0,1,180,[],[],[],""]]',
                        "f_user_id" => 0,
                        "f_type" => "view"),
                    "data" => array()
                );
            }
        }
        return $res[$f_type];
    }


    /**
     * Create cross between selectors and passed object
     * @param Zend_Db $db database connection
     * @param int $f_code f_code to cross with selectors (could be wo/wares/system)
     * @param array $f_code_selectors array with selectors to cross
     * @param string $table_name could be t_selector_ware/wo/systems
     * @param string $field field name (f_wo_id/f_ware_id/f_system_id)
     * @param int $nesting (old school) usually 1
     */
    public static function createSelectorCross($db, $f_code, $f_code_selectors, $table, $field, $nesting = 1)
    {
        if(!$f_code_selectors || !$f_code) { return; }

        $select = new Zend_Db_Select($db);
        $res_selectors = $select->from($table, 'f_selector_id')->where($field." = ?",$f_code)
          ->where("f_nesting_level = ?",$nesting)->query()->fetchAll(Zend_Db::FETCH_COLUMN);

        $f_code_selectors_cleaned = array_filter($f_code_selectors,function($v) use($res_selectors){
            return !in_array($v, $res_selectors);
        });

        $table_schema = ['f_selector_id', $field, 'f_nesting_level', 'f_timestamp'];
        $data = [];
        foreach($f_code_selectors_cleaned as $selector){
            $data[] = [
                'f_selector_id'=>$selector,
                $field=>$f_code,
                'f_nesting_level'=>$nesting,                
                'f_timestamp'=>time()
            ];
        }
        self::massiveCrossInsert($table,$table_schema, $data, $db);
    }
    
    public static function getSelectorFather($selector)
    {
        $db = Zend_Db::factory(Zend_Registry::get('db'));
        $select = new Zend_Db_Select($db);        
        $res_parent = $select->from("t_selectors_parent","f_parent_code")->where("f_active = 1")
                ->where("f_code = ?",$selector)->order("f_parent_code ASC")->query()->fetchAll();            
        $parent = array();
        $parent_hy = array();                 
        foreach($res_parent as $line_parent) {
            $parent[] = $line_parent['f_parent_code'];//to check different between parent
            $parent_hy[] = $line_parent['f_parent_code']; // to get all tree of the selector
            $parents = array($line_parent['f_parent_code']); // to itereate the selector's tree                
            do {
                $not_end = true;                    
                $select->reset();
                $res_tree = $select->from("t_selectors_parent","f_parent_code")->where("f_active = 1")
                        ->where("f_code IN (".implode(',',$parents).")")->query()->fetchAll();                          
                if(empty($res_tree)) { 
                    break;
                }                    
                $parents = array();
                foreach($res_tree as $line_tree) {
                    $parents[] = $line_tree['f_parent_code'];
                    if(!in_array($line_tree['f_parent_code'],$parent_hy)) {                            
                        $parent_hy[] = $line_tree['f_parent_code'];                        
                    }
                }                    
                if(empty($parents)) break;
            }while($not_end);
        }
        return $parent_hy;
    }
    
    public static function getChildCode($table,$f_code,&$child_codes = array(),$db = false,$only_father = false)
    {    
        if(!$db) {            
            $db = Zend_Db::factory(Zend_Registry::get('db'));
        }
        if(!is_array($f_code)){
            $f_code = array($f_code);
        }        
        $new_parent = array();
        $select = new Zend_Db_Select($db);
        $select->from(array("t1"=>$table),array("f_code","f_parent_code"))
                ->where("f_parent_code IN (?)",$f_code)
                ->where("f_active = 1");
        if($only_father) {
            $select->where("EXISTS(select * from $table where f_parent_code = t1.f_code)");
        }        
        $res = $select->query()->fetchAll();
        $tot = count($res);
        for($i = 0;$i < $tot;++$i) {
            $line = $res[$i];
            $code = (int)$line['f_code'];
            $child_codes[$code] = $code;
            if(!$only_father) {
                $select->reset();
                $res_is_father = $select->from($table,array("num"=>"count(*)"))
                        ->where("f_parent_code = ?",$code)->where("f_active = 1")
                        ->query()->fetch();
                if($res_is_father['num'] > 0) {
                    $new_parent[$code] = (int)$code;
                }
            }
            else {
                $new_parent[$code] = (int)$code;
            }
        }                
        if(!empty($new_parent)) {            
            self::getChildCode($table,$new_parent, $child_codes,$db,$only_father);
        }        
        return $child_codes;
    }
    
    /**
     *
     * @param type $f_code
     * @param type $parent_codes 
     */
    public static function saveReverseAjax($table,$f_code,$parent_codes,$action,$module_name,$db = null)
    {
        if(is_null($db)) {
            $db = Zend_Db::factory(Zend_Registry::get('db'));
        }        
        $old_parents = array();
        $parents_new = array();
        $time = Zend_Registry::isRegistered('time')?Zend_Registry::get('time'):time();        
        $sel = new Zend_Db_Select($db);        
        $res_logged_user = $sel->from("t_access_registry","f_user_id")->query()->fetchAll();
        $sel->reset();
        $resParent = $sel->from($table,'f_parent_code')->where('f_active = 1')
            ->where('f_code = ?',$f_code)->query()->fetchAll();                
        $totA = count($resParent);
        for($a = 0;$a < $totA;++$a){
            $parents_new[] = $resParent[$a]['f_parent_code'];
        }        
        if($action == 'add') {
            $old_parents[] = -1;            
        } 
        else{
            $sel->reset(Zend_Db_Select::WHERE);
            $resNewParents = $sel->where('f_code = ?',$f_code)->where('f_active = 1')
                ->where('f_timestamp = ?',$time)->query()->fetchAll();                        
            if(empty($resNewParents)){
                $old_parents = $parents_new;
            }            
        }        
        $old_parents_str = implode(",",$old_parents); 
        $parents_new = implode(",",$parents_new); 
       
        if($action == 'change phase batch') {
            $action = 'upd';
            $old_parents_str = $parents_new;
        }
        
        foreach($res_logged_user as $user) {
            $reverse = array(
                "f_user_id"=>$user['f_user_id'],
                "f_code"=>$f_code,
                "f_old_parent"=>$old_parents_str,
                "f_parent"=>$parents_new,
                "f_timestamp"=>time(),
                "f_action"=>$action,                
                "f_module_name"=>$module_name
            );
            $db->insert("t_reverse_ajax", $reverse);
        }
    }
    
    public function saveReverseAjax2($table,$f_code,$new_parent_codes,$action,$module_name,$db = null, $old_parent_codes = array())
    {
         if(is_null($db)) {
            
            $db = Zend_Db::factory(Zend_Registry::get('db'));
        }
        $sel = new Zend_Db_Select($db);        
        $res_logged_user = $sel->from("t_access_registry","f_user_id")->query()->fetchAll();
        $parents_new = $new_parent_codes;
        $old_parents = $old_parent_codes;        
        if($action == 'add') {
            $old_parents[] = -1;            
        } 
        
        $old_parents_str = implode(",",$old_parents);
        if(!count($parents_new)) $parents_new = 0;
        else $parents_new = implode(',',$parents_new);
        
        if($action == 'multiEdit'){
            $old_parents_str = $parents_new = '0';
            $action = 'upd';
        }
        
        foreach($res_logged_user as $user) {
            $reverse = array(
                "f_user_id"=>$user['f_user_id'],
                "f_code"=>$f_code,
                "f_old_parent"=>$old_parents_str,
                "f_parent"=>$parents_new,
                "f_timestamp"=>time(),
                "f_action"=>$action,                
                "f_module_name"=>$module_name
            );
            $db->insert("t_reverse_ajax", $reverse);
        }
    }
    
    
    /**
     * return cross between passed object and specified table
     * @param string $table table of cross (t_ware_wo,t_selector_wo,t_wares_relations,t_selector_ware)
     * @param int $f_code f_code of element we want the crosses
     * @param string $field name of the field inside passed table     
     * @return array element crossed 
     */        
    public static function getCross($table,$f_code,$field,$code_crosses = array(),$field_cross = '',$mainTableCross = '',$f_types = '',$onlyCode = false)
    {
        $codes = array();
        $db = Zend_Registry::isRegistered('db_connection_editor')?Zend_Registry::get('db_connection_editor'):Zend_Db::factory(Zend_Registry::get('db'));
        $select = new Zend_Db_Select($db);
        if(!empty($code_crosses) && !empty($field_cross)) {
            $select->where("$field_cross IN (?)",$code_crosses);
        }
        if(!empty($mainTableCross) && !empty($f_types) && !empty($field_cross)) {
            $select->join(array('t2'=>$mainTableCross),"t1.{$field_cross} = t2.f_code",array())
                ->where("t2.f_type_id IN ({$f_types})");
        }  
        $res = $select->from(array('t1'=>$table))->where("$field = ?",$f_code)->order("t1.f_id asc")->query()->fetchAll();
        if($onlyCode) {
            $tot = count($res);
            for($i = 0;$i < $tot;++$i) {
                $codes[] = (int)$res[$i][$field_cross];
            }
        }
        else {
            $codes = $res;
        }
        return $codes;
    }
    
    public static function checkIdenticalCross($checkList,$table,$codeMain,$fieldMain,$crossList = array(),$fieldCross = '',$mainTableCross = '',$crossType = '')
    {
        $checkList = array_map(function($el) { return (int)$el;}, $checkList);        
        $crossList = Mainsim_Model_Utilities::getCross($table,$codeMain,$fieldMain,$crossList,$fieldCross,$mainTableCross,$crossType,true);                
        if($crossList !== $checkList) {
            return false;
        }
        return true;
    }
    
    public static function createCross($db,$table,$codeMain,$fieldMain,$codesCross,$fieldCross)
    {
        $time = Zend_Registry::isRegistered('time')?Zend_Registry::get('time'):time();
        $table_schema = [$fieldMain, $fieldCross, 'f_timestamp'];
        $data = [];
        foreach($codesCross as $line)
        {
            $data[] = [
                $fieldMain => $codeMain,
                $fieldCross => $line,
                'f_timestamp' => $time
            ];
        }

        self::massiveCrossInsert($table, $table_schema, $data, $db);
    }


    /**
     * Massive insert of cross
     * @param string $table
     * @param array $table_schema
     * @param array $data
     * @param Zend_Db $db
     */
    public static function massiveCrossInsert($table, $table_schema, $data, $db = null)
    {
        if(!$db)
        {
            $db = Zend_Db::factory(Zend_Registry::get('db'));
        }
        $default_insert = "INSERT INTO ".$table." (".(implode(', ',$table_schema)).") VALUES ";
        foreach(array_chunk($data, 1000) as $chunck){
            $insert = $default_insert;
            $insert_data = [];
            foreach($chunck as $line) {
                $insert_data[] = "( ".implode(", ",$line)." )";
            }
            if($insert_data) {
                $insert.= (implode(", ",$insert_data));
                $db->query($insert);
            }
        }
    }
    
    /** Salvataggio pairCross*/
    public static function createPairCross($db,$params,$pairCross)
    {      
        $time = Zend_Registry::isRegistered('time')?Zend_Registry::get('time'):time();
        if($pairCross == 1) {            
            $f_group_code = $params['f_code_cross'];
            $params['f_code_cross'] = $params['f_group_code'];
            $params['f_group_code'] = $f_group_code;
        }                     
        unset($params['f_parent_code']); 
        unset($params['f_code']);
        $params['f_timestamp'] = $time;
        $db->insert("t_pair_cross",$params);
    }
     
    /**
     * return list of fathers 
     * @param type $f_code_sel
     * @param type $fathers
     * @return type 
     */
    public static function getParentCode($table = '',$f_code_sel = 0,$fathers = array(),$db = null)
    {         
        if(is_null($db)){
            
            $db = Zend_Db::factory(Zend_Registry::get('db'));                   
        }
        $select = new Zend_Db_Select($db);      
        //check parents
        $select->reset();            
        $res_parents = $select->from($table,"f_parent_code")->where("f_code = ?",$f_code_sel)
                ->where("f_active = 1")->query()->fetchAll();        
        foreach($res_parents as $par) {
            if(!in_array($par['f_parent_code'], $fathers) && $par['f_parent_code'] != $f_code_sel) {
                $fathers[] = (int)$par['f_parent_code'];
                $fathers = self::getParentCode($table,$par['f_parent_code'],$fathers,$db);
            }
        }            
        return $fathers;
    }
    
    /**
     * generate cross for history record
     * @param type $table
     * @param type $f_code_sel
     * @param type $fathers
     * @param type $db
     * @return type 
     */
    public static function getHistoryCross($table = '',$f_code = 0,$record = array(),$db = null)
    {         
        if($table != 't_wares' && $table != 't_workorders' && $table != 't_selectors') return $record;
        
        if(is_null($db)){
            $db = Zend_Db::factory(Zend_Registry::get('db'));                           
        }
        $select = new Zend_Db_Select($db);      
        $name = str_replace("t_","",$table);
        //get parents
        $select->reset();            
        $res_parents = $select->from($table.'_parent')->where("f_code = ?",$f_code)
                ->where("f_active = 1")->query()->fetchAll();        
        $record['fc_'.$name.'_parent'] = gzcompress(serialize($res_parents));
        if($table == 't_wares') {
            $select->reset();      // wares cross      
            $res_wares_relations = $select->from("t_wares_relations")
                    ->where("f_code_ware_master = ?",$f_code)->orWhere("f_code_ware_slave = ?",$f_code)
                    ->query()->fetchAll();
            $record['fc_ware_relations'] = gzcompress(serialize($res_wares_relations));
            
            $select->reset();  //ware wo cross
            $res_ware_wo = $select->from("t_ware_wo")
                    ->where("f_ware_id = ?",$f_code)->query()->fetchAll();
            $record['fc_ware_wo'] = gzcompress(serialize($res_ware_wo));
            
            $select->reset(); //selector ware cross                    
            $res_selector_ware = $select->from("t_selector_ware")
                    ->where("f_ware_id = ?",$f_code)->query()->fetchAll();
            $record['fc_selector_ware'] = gzcompress(serialize($res_selector_ware));
        }
        elseif($table == 't_workorders') {
            $select->reset();      // wos cross      
            $res_wo_relations = $select->from("t_wo_relations")
                    ->where("f_code_wo_master = ?",$f_code)->orWhere("f_code_wo_slave = ?",$f_code)
                    ->query()->fetchAll();
            $record['fc_wo_relations'] = gzcompress(serialize($res_wo_relations));
            
            $select->reset();  //ware wo cross
            $res_ware_wo = $select->from("t_ware_wo")
                    ->where("f_wo_id = ?",$f_code)->query()->fetchAll();
            $record['fc_ware_wo'] = gzcompress(serialize($res_ware_wo));
            
            $select->reset(); //selector wo cross                    
            $res_selector_wo = $select->from("t_selector_wo")
                    ->where("f_wo_id = ?",$f_code)->query()->fetchAll();
            $record['fc_selector_wo'] = gzcompress(serialize($res_selector_wo));
            
            $select->reset(); //pair cross                    
            $res_pair = $select->from("t_pair_cross")
                    ->where("f_code_main = ?",$f_code)->query()->fetchAll();
            $record['fc_pair_cross'] = gzcompress(serialize($res_pair));        
        }
        else{            
            $select->reset(); //selector wo cross                    
            $res_selector_wo = $select->from("t_selector_wo")
                    ->where("f_selector_id = ?",$f_code)->query()->fetchAll();
            $record['fc_selector_wo'] = gzcompress(serialize($res_selector_wo));        
            
            $select->reset(); //selector ware cross                    
            $res_selector_ware = $select->from("t_selector_ware")
                    ->where("f_selector_id = ?",$f_code)->query()->fetchAll();
            $record['fc_selector_ware'] = gzcompress(serialize($res_selector_ware));        
        }
            
                    
        return $record;
    }        
    
    public static function clearTempAttachments($params,$db)
    {
        $select = new Zend_Db_Select($db);
        $tot = count($params['attachments']);
        for($i = 0;$i < $tot;++$i) {
            if(empty($params[$params['attachments'][$i]])) continue;
            $files = explode("|",$params[$params['attachments'][$i]]);
            $select->reset();
            $res_temp = $select->from("t_attachments")->where("f_code = 0")->where("f_path like '%temp%'")
                ->where("f_session_id = ?",$files[0])->query()->fetchAll();                                                        
            if(!empty($res_temp)) {
                $tot_t = count($res_temp);
                for($t = 0;$t < $tot_t;++$t) {                         
                    unlink($res_temp[$t]['f_path']);                                
                    $db->delete("t_attachments","f_code = 0 and f_session_id = '{$res_temp[$t]['f_session_id']}' and f_type = '{$res_temp[$t]['f_type']}'");
                }   
            }
        }
    }
    
    /** Salvataggio pairCross*/
    public static function newPairCross($db,$params,$pairCross)
    {            
        if($pairCross == 1) {            
            $f_group_code = $params['f_code_cross'];
            $params['f_code_cross'] = $params['f_group_code'];
            $params['f_group_code'] = $f_group_code;
        }             
        $cross = array();
        $cross = $params;
        
        unset($cross['f_parent_code']); 
        unset($cross['f_code']);        
        $cross['f_timestamp'] = time();
        $db->insert("t_pair_cross",$cross);
    }
    
    /**
     * Read part of excel during import (from instant,cron or module importer)
     * @param type $filename
     * @param type $position
     * @param type $inc
     * @param type $sheet
     * @return type 
     */
    public static function IterateExcel($filename,$position,$inc,$sheet = false)
    {
        $ext = substr($filename,strrpos($filename,".") + 1 );
        
        if($ext == 'xls') {            
            $excelReader = new PHPExcel_Reader_Excel5();    
        }
        else {            
            $excelReader = new PHPExcel_Reader_Excel2007();
        }        
        $chunkFilter = new Mainsim_Model_ChunkReadFilter();
        $excelReader->setReadFilter($chunkFilter);    
        $chunkFilter->setRows($position, $inc);
        if($sheet !== false) {
            $excelReader->setLoadSheetsOnly($sheet);
        }
        $file = $excelReader->load($filename);
        return $file;
    }
    
    /**
     *
     * @param type $rowIterator
     * @param type $keys
     * @param type $get_keys
     * @return type 
     */
    public static function rowIteratorImport($rowIterator,&$keys,$get_keys = false)
    {
        $res = array();                
        foreach($rowIterator as $row){                
            $i = 0;
            $cellIterator = $row->getCellIterator();            
            $cellIterator->setIterateOnlyExistingCells(false);                         
            $rowIndex = $row->getRowIndex ();                
            
            $array = array();
            /**
             *@var PHPExcel_Cell $cell 
             */
            if($rowIndex == 1 && !$get_keys) continue;
            $emptyColumn = false;
            foreach ($cellIterator as  $cell) {              
                if($rowIndex == 1 && $get_keys) {                          
                    $key = trim($cell->getCalculatedValue());                    
                    if(strlen($key)){
                        if(in_array($key, $keys)) {
                            throw new Exception("Column $key appears more than once");
                        }
                        if($emptyColumn == true) {                            
                            throw new Exception("Column $column is nameless");
                        }
                        if(self::cleanColName($key)) {
                            $keys[] = $key;
                        }
                    }
                    else {
                        $column = $cell->getColumn().'1';
                        $emptyColumn = true;
                    }
                }                
                else {                    
                    if(isset($keys[$i])){                              
                        if($cell->getDataType() != 'f' && $cell->getDataType() != 'n') {
                            $cell->setDataType(PHPExcel_Cell_DataType::TYPE_STRING); 
                        }
                        
                        if($cell->getDataType() != 'n') {
                            $array[$keys[$i]] = $cell->getCalculatedValue();                            
                        }
                        else {                             
                            if(PHPExcel_Shared_Date::isDateTime($cell)) {     
                                 $array[$keys[$i]] = PHPExcel_Style_NumberFormat::toFormattedString($cell->getValue(), "M/D/YYYY");                                                                  
                            }
                            else {
                                $array[$keys[$i]] = $cell->getCalculatedValue();                                
                            }                            
                        }                        
                    }
                    $i++;
                    if($i > count($keys)) break;                    
                }
            }               
            //check if is the header
            if($rowIndex == 1){continue;}
            elseif($array['f_code'] == '' || is_null($array['f_code'])) continue;
            
            $res[] = $array;        
        }
        
        return $res;
    }
    
    /**
     * exec check inside ui object
     * @param type $v value
     * @param type $c condition
     * @return int 1 = ok, 0 = ko
     */
    public static function check($v,$c,$col = '',$table = '',$data = array(),$row = 0,$f_type_id = 0,$value_check = '')
    {           
        $risp=1; 
        $arrc=explode(" ",$c);  
        $na=count($arrc);   // ,cc,pp,app,ss,s,pi,pf,vl;

        $vl=strlen($v); 

        for($r=0;$r<$na;$r++){
            $cc=$arrc[$r]; 
            if($cc=="mail")  $risp=self::checkMail($v);
            if($cc=="int")   $risp=self::checkInt($v);
            if($cc=="unique")   $risp=self::checkUnique($v,$col,$table,$f_type_id);
            if($cc=="uniqueCode") return self::checkUniqueCode($v,$col,$table,$f_type_id);
            if($cc=="wf_exist") $risp=self::checkWf($v);
            if($cc=="phase_exist") $risp=self::checkWfPhase($v,$data[$row]['f_wf_id']);
            if($cc=='valid_date') $risp=self::checkValidDate($v);
            if($cc=='empty_field') $risp=self::checkEmpty($v);
            if($cc=='f_type') $risp=self::checkType($v);
            if($cc=='maxLength') $risp=self::maxLength($v,$value_check);
            if($cc=='minLength') $risp=self::minLength($v,$value_check);
            if($cc=='inList') $risp=self::inList($v,$value_check);
            if($cc=='notInList') $risp=self::notInList($v,$value_check);
            
            $app=explode("_",$cc); 
            //if value exist, check condition
            if($vl){
                if(substr($cc,0,5)=="nchar") {
                    if(count($app)>2) {
                        $pi=(int) ($app[1]);
                        $pf=(int) ($app[2]);
                        if($vl<$pi || $vl>$pf) $risp=0;
                    } else {
                        if($vl != ((int) ($app[1])) ) $risp=0;
                    } 
                }

                if(substr($cc,0,3)=="not") {
                    $ss=$app[1];
                    if(count($app)>2)  $ss.=$app[2]+"_";
                    for($n=0;$n<strlen($ss);$n++) {
                        $s=substr($ss,$n,1); 
                        if(strpos($v,$s)!==false) {
                            $risp=0;
                            break;
                        }
                    }
                }

                if(substr($cc,0,5)=="range") {
                    if(!is_numeric($v)) $risp=0;
                    else {
                        $v=(double) $v;

                        if($app[1]=="m" && $app[2]=="M") {  }
                        else {
                            $i=$app[1]; $f=$app[2]; $mM=1;

                            if($i=="m") { $f=(double) $f;  
                                if($v>$f) $risp=0;
                                $mM=0;
                            } else if($f=="M") { $i=(double) $i;
                                if($v<$i) $risp=0; 
                                $mM=0; 
                            }

                            if($mM) { $f=(double) $f; $i=(double) $i;
                                if($v<$i || $v>$f) $risp=0;  }
                        }
                    }
                }

                if( substr($cc,0,7)=="decimal") {   
                    if(!is_numeric($v)) $risp=0;
                    else { 
                        $pp=explode(".",$v);
                        $s=count($pp);                            
                        if($s && strlen($pp[1])>$app[1]) $risp=0; 
                    }
                }
                if(strpos($cc,"regexp")!== false) {
                    $risp = 0;
                    if(preg_match_all($app[1],$v)) {
                        $risp = 1;
                    }
                }
            } 
            if(!$risp) return 0;
        } //
        return 1;
    }


    /**
     * check if passed value is an int
     * @param type $v = value
     * @return int 1 = ok, 0= ko
     */
    private static function checkInt($v)
    {
        if($v=="" || is_long($v)) return 1;        
        for($r=0;$r<strlen($v);$r++){
            $c=substr($v,$r,1); $o=ord($c);
            if($o<48 || $o>57) return 0;
        }
        return 1;
    }
    
    /**
     * check if the value is unique inside the system
     * @param type $v value to check
     * @param type $col colum to check
     * @param type $table table to check
     * @param type $data other data passed from excel
     */
    private static function checkUnique($v,$col,$table,$f_type_id)
    {           
        //if $v is empty, return false;        
        if(empty($v) || is_null($v)) return false;
                
        $db = Zend_Registry::isRegistered("dbAdapter")?Zend_Registry::get('dbAdapter'):Zend_Db::factory(Zend_Registry::get('db'));
        $select = new Zend_Db_Select($db);                
        
        $tab_cols = self::get_tab_cols($table);
        $db_col = in_array($col,$tab_cols)?"t1.$col":"t2.$col";        
        //now check inside the system             
        try {            
            $res_unique = $select->from(array("t1"=>$table),array('num'=>'count(*)'))                    
                    ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",array())
                    ->where("$db_col = $v")->where("f_type_id = ?",$f_type_id)->query()->fetchAll();          
        }catch(Exception $e) {return false;}
        
        if($res_unique['num'] > 0) return false;
        
        return true;
    }
    
    
    /**
     * check if the value is unique inside the system
     * @param type $v value to check
     * @param type $col colum to check
     * @param type $table table to check
     * @param type $data other data passed from excel
     */
    private static function checkUniqueCode($codes_list,$col,$table,$f_type_id)
    {           
        //if $v is empty, return false;        
        if(empty($codes_list) || is_null($codes_list)) return false;        
        $db = Zend_Registry::isRegistered("dbAdapter")?Zend_Registry::get('dbAdapter'):Zend_Db::factory(Zend_Registry::get('db'));
        $select = new Zend_Db_Select($db);        
        //now check inside the system             
        try {            
            $res_unique = $select->from(array("t1"=>$table),array($col))
                ->where("$col IN ($codes_list)")->where("f_type_id = ?",$f_type_id)
                ->query()->fetchAll();                               
        }catch(Exception $e) {die($e->getMessage());return false;}        
        if(!empty($res_unique)) return $res_unique[0][$col];                
        return false;
    }

    /**
     * check if is a valid mail
     * @param type $email 
     * @return boolean 
     */
    private static function checkMail($email) {
        // First, we check that there's one @ symbol, and that the lengths are right
        if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
            // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
            return false;
        }
        // Split it into sections to make life easier
        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_{|}~-][A-Za-z0-9!#$%&'*+\/=?^_{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
                return false;
            }
        }
        if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
            $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
                if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static function checkWf($wf)
    {   
        if($wf == '') return true;
        $wf = (int)$wf; 
        
        if(!Zend_Registry::isRegistered("array_wf_phases")) {
            $db = Zend_Registry::isRegistered("dbAdapter")?Zend_Registry::get('dbAdapter'):Zend_Db::factory(Zend_Registry::get('db'));        
            $res = $db->query("select f_id from t_workflows where f_id = $wf")->fetch();
            return !empty($res);
        }
        else {
            $array_wf_phases = Zend_Registry::get("array_wf_phases");
            return (array_key_exists($wf, $array_wf_phases));
        }
    }
    
    public static function checkWfPhase($phase = '',$wf = '')
    {
        if($phase == '' && $wf == '') return true;
        $wf = (int)$wf;
        $phase = (int)$phase;
        if(!Zend_Registry::isRegistered("array_wf_phases")) {
            $db = Zend_Registry::isRegistered("dbAdapter")?Zend_Registry::get('dbAdapter'):Zend_Db::factory(Zend_Registry::get('db'));                
            $res = $db->query("select f_id from t_wf_phases where f_wf_id = $wf and f_number = $phase")->fetch();
            return !empty($res);        
        }
        else {
            $array_wf_phases = Zend_Registry::get("array_wf_phases");
            if(!array_key_exists($wf, $array_wf_phases)) return false;
            if(!in_array($phase, $array_wf_phases[$wf])) return false;
            return true; 
        }
    }
    
    public static function checkValidDate($v)
    {     
        if($v == '') return true;
        $ts = strtotime($v);
        return !$ts?false:true;
    }
    
    public static function checkEmpty($v) 
    {
        $v = trim($v);
        return  $v == '' || is_null($v)? false:true;
    }
    
    public static function checkType($v)
    {
        $res = false;
        $exp_v = explode(' : ',$v);
        if(count($exp_v) == 2) {
            if(ctype_digit($exp_v[0])) $res = true;
        }        
        return $res;
    }
    
    public static function cleanColName($name)
    {
        if(!ctype_alpha($name[0])) {
            throw new Exception("Wrong column name $name. First char must be a string and rest of can be chars or numbers.");
        }
        $name = str_replace('_', '', $name);
        if(!ctype_alnum($name)) {
            throw new Exception("Wrong column name $name. First char must be a string and rest of can be chars or numbers.");
        }
        return true;        
    }
    
    /**
     * check max length of passed value
     * @param string $value string to check
     * @param int $length max length allow
     * @return boolean true if is less than length passed, otherwise false
     */
    private function maxLength($value,$length)
    {
        if(empty($value) || empty($length)) return true; //don't apply check
        if(strlen($value) > $length) return false; // too long
        return true;// it's ok
    }
    
    /**
     * check min length of passed value
     * @param string $value string to check
     * @param int $length min length allow
     * @return boolean true if is over length passed, otherwise false
     */
    private function minLength($value,$length)
    {
        if(empty($value) || empty($length)) return true; //don't apply check
        if(strlen($value) < $length) return false; // too short
        return true;// it's ok
    }
    
    /**
     * check if passed value is inside passed list
     * @param string $value value to check
     * @param string $list list of allow values separated by comma
     * @return boolean true if ok, otherwise false
     */
    private static function inList($value,$list)  
    {
        if(empty($value) || empty($list)) return true; //don't apply check
        $list = explode(',',$list); //change string in array
        if(!in_array($value, $list)) return false; //not in list
        return true; // ok
    }
    
    /**
     * check if passed value is not inside passed list
     * @param string $value value to check
     * @param string $list list of values separated by comma
     * @return boolean true if not in list, otherwise false
     */
    private static function notInList($value,$list)  
    {
        if(empty($value) || empty($list)) return true; //don't apply check
        $list = explode(',',$list); //change string in array
        if(in_array($value, $list)) return false; //not in list
        return true; // ok
    }
    
     /**
     * check if passed task is valid this time or not
     * @param int $counter : field f_counter in t_workorders
     * @param int $sequence
     * @param int $exc_allow
     * @param int $exc_deny
     * @return boolean true if can generate otherwise false
     */
    public static function checkTaskSequence($counter,$sequence,$exc_allow,$exc_deny)
    {
        $allow_array = array();
        $deny_array = array();
        if(is_string($exc_allow)) {
            $allow_array = explode(",",$exc_allow);
            if(count($allow_array) == 1) {$allow_array = explode(".",$exc_allow);}
        }
        if(is_string($exc_deny)) {
            $deny_array = explode(",",$exc_deny);
            if(count($deny_array) == 1) {$deny_array = explode(".",$exc_deny);}
        }
        
        //if result not is 0, check if is allow
        $is_allow = false;
        $task_sequence = (int)$sequence?(int)$sequence:1;
        if(($counter%$task_sequence)) {
            //check if is allow
            foreach($allow_array as $allow) {
                if($allow && !($counter%$allow)){
                    $is_allow = true;
                    break;
                }
            }
        }            
        else { //if is an allow day, check if there are limitation for this task            
            if(!empty($deny_array)) {
                foreach ($deny_array as $deny) {
                    if($deny && !($counter%$deny)) {
                        $is_allow = false;
                        break;
                    }
                    else{
                        $is_allow = true;
                    }
                }
            }
            else {
                $is_allow = true;
            }                       
            if(!$is_allow) {
                foreach($allow_array as $allow) {
                    if($allow && !($counter%$allow)){
                        $is_allow = true;
                        break;
                    }
                }
            }            
        }                            
        return $is_allow;
    }
    
    public static function isUnique($uniqueFields,$params,$moduleName,$db = null) 
    {
        $db = is_null($db)?Zend_Db::factory(Zend_Registry::get('db')):$db;        
        $select = new Zend_Db_Select($db);
        
        $custom_columns = self::get_tab_cols("t_custom_fields");           
        $creation_columns = self::get_tab_cols("t_creation_date");           
        $users_columns = self::get_tab_cols("t_users");        
        $broke = false;        
        $unique_element = explode(',',$uniqueFields);
        if(empty($unique_element)){
            return;
        }
        
        /*if is only one field and value is empty, return */
        if(count($unique_element) == 1 && empty($params[$uniqueFields])){
            return;
        }
        /** get module name and type id */
        $res_tg = $select->from("t_ui_object_instances",array("f_properties"))
                ->where("f_instance_name = ?",$moduleName)->query()->fetch();        
        if(empty($res_tg)) {
            return;
        }
        $prop = json_decode(self::chg($res_tg['f_properties']),true);            
        $f_type_prog = $prop['ftype'];        
        $table = $prop['table'];         
        $columns = self::get_tab_cols($table);
        $select->reset();
        $select->from(array('t1'=>$table),array("num"=>"count(*)"))
                ->join(array('t2'=>'t_creation_date'),"t1.f_code = t2.f_id",array())
                ->join(array('t3'=>'t_custom_fields'),"t2.f_id = t3.f_code",array())
                ->join(array('t5'=>'t_wf_phases'),"t2.f_wf_id = t5.f_wf_id and t2.f_phase_id = t5.f_number",array())
                ->where("t5.f_group_id <> 7")->where("t1.f_code <> ?",$params['f_code']);
        if($table == 't_wares' && $f_type_prog == 16) {
            $select->join(array('t4'=>'t_users'),"t1.f_code = t4.f_code",array());
        }
        
        foreach($unique_element as &$line) {                        
            if(in_array($line, $columns)) {
                if(!isset($params[$line]) || (empty($params[$line]) &&  $params[$line] != 0) ) { $broke = true; break;}
                $select->where("t1.$line = ?",$params[$line]);
            }
            elseif(in_array($line, $creation_columns)) {
                if(!isset($params[$line]) || (empty($params[$line]) &&  $params[$line] != 0) ) { $broke = true; break;}
                $select->where("t2.$line = ?",$params[$line]);
            }
            elseif(in_array($line, $custom_columns)) {
                if(!isset($params[$line]) || (empty($params[$line]) &&  $params[$line] != 0) ) { $broke = true; break;}
                $select->where("t3.$line = ?",$params[$line]);
            }
            elseif(in_array($line, $users_columns)) {
                if(!isset($params[$line]) ||  (empty($params[$line]) &&  $params[$line] != 0)) { $broke = true; break;}                
                $select->where("t4.$line = ?",$params[$line]);
            }
            // i don't find this field in database
            else { $broke = true; break;}
        }      
                
        if($broke) {throw new Exception("$uniqueFields is not unique");}                
        $res_unique = $select->query()->fetch();        
        if($res_unique['num'] > 0){ 
            throw new Exception("$uniqueFields is not unique");
        }
    }
    
    public static function saveAttachments($f_code,$table,$params,$scriptModule)
    {
        $columns = Mainsim_Model_Utilities::get_tab_cols($table);
        $custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");        
        $creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");  
        $users_columns = Mainsim_Model_Utilities::get_tab_cols("t_users");  
        //check if there are attachments and add into the system        
        if(isset($params['attachments']) && !empty($params['attachments'])) {            
            for($af = 0;$af < count($params['attachments']);++$af) {                                
                $tableUpd = '';
                if(in_array($params['attachments'][$af], $columns)) {
                    $tableUpd = $table;
                }elseif(in_array($params['attachments'][$af], $creation_columns)) {
                    $tableUpd = 't_creation_date';
                }elseif(in_array($params['attachments'][$af], $custom_columns)) {
                    $tableUpd = 't_custom_fields';
                }elseif(in_array($params['attachments'][$af], $users_columns)) {
                    $tableUpd = 't_users';
                }else { continue; }                
                $special_folder = '';
                if($tableUpd == 't_systems') {
                    if($params['attachments'][$af] == 'fc_dmn_export_settings') {
                        $special_folder = 'data manager/exporter';
                    }
                    elseif($params['attachments'][$af] == 'fc_dmn_imported_file') {
                        $special_folder = 'data manager/importer';
                    }
                }
                $scriptModule->saveFile($f_code,$tableUpd,$params['attachments'][$af],$params[$params['attachments'][$af]],$special_folder);
            }
        }        
    }
    
    /**
     * Get new fc progress for specific table and type
     * @param string $table table name
     * @param int $type element type
     * @param Zend_Db $db db connection
     * @return type
     */
    public static function getFcProgress($table,$type,$db)
    {
        $select = new Zend_Db_Select($db);
        $res = $select->from('t_progress_lists',array('max'=>'max(fc_progress)'))
            ->where("f_table = ?",$table)->where("f_type_id = ?",$type)
            ->query()->fetch();
        $fc = !empty($res['max'])?(int)$res['max']:0;
        $fc++;
        while(true) {
            try {
                $db->insert("t_progress_lists",array(
                    'fc_progress'=>$fc,
                    'f_table'=>$table,
                    'f_type_id'=>$type
                ));
            }catch(Exception $e) {
                $fc++;
                continue;
            }
            break;
        }
        return $fc;
    }
    
    /**
     * Get Info about installation from mainsim3_database_list
     * @param type $dbConfig
     * @param type $host
     * @param type $field
     * @return type
     * @throws Exception
     */
    public static function getInstallationInfo($dbConfig,$host)
    {
		$db = Zend_Db::factory($dbConfig);
		$select = new Zend_Db_Select($db);
		$res = $select->from('database_lists')
            //->join(array('t2'=>'t_creation_date'),'f_code = t2.f_id',array('f_phase_id'))
                ->where("f_url = ?",$host)->query()->fetch();
		if(empty($res)) {
			throw new Exception("Unknown configuration for $host");
		}
//        elseif($res['f_phase_id'] == 2) { /*In future from here i'll send an email */
//            throw new Exception("Your installation has been deactivate");
//        }
//        elseif($res['f_phase_id'] == 3) { /*In future from here i'll send an email */
//            throw new Exception("Your installation has been removed");
//        }
		$dbParams = array(
			'adapter'=>$res['f_adapter'],
			'params'=>array(
				'host'=>$res['f_host'],
				'dbname'=>$res['f_dbname'],
				'username'=>$res['f_username'],
				'password'=>$res['f_password'],
			)
		);
		$paramsList['database'] = new Zend_Config($dbParams);
		$paramsList['attachment_path'] = $res['f_attachment_path'];		
		return $paramsList;		
	}
    
    /**
     * Create Parent Relations
     * @param type $table
     * @param type $f_code
     * @param type $f_parent_code
     */
    public static function createParents($table,$f_code,$f_parent_code,$db)
    {   
        $time = Zend_Registry::isRegistered('time')?Zend_Registry::get('time'):time();        
        $f_parent_code_array = empty($f_parent_code)?array(0):$f_parent_code;                
        foreach($f_parent_code_array as $parent) {            
            if($parent == $f_code) {
                continue;
            }                   
            $db->insert($table, array(
                'f_code'=>$f_code,
                'f_parent_code'=>$parent,
                'f_active'=>1,
                'f_timestamp'=>$time               
            ));                        
        }        
    }
    
    /**
     * Return Asset list from asset chain
     * @param type $woCode
     * @param type $start_date
     * @param type $res_asset
     * @return type
     */
    public static function getAssetChain($woCode,$start_date,$res_asset,$db)
    {
        $select = new Zend_Db_Select($db);
        //check if this pm have an asset chain
        $select->reset();
        $res_chain = $select->from(["t1"=>"t_wares"],['f_code'])
            ->join("t_creation_date","t1.f_code = t_creation_date.f_id",[])
            ->join(["t2"=>"t_ware_wo"],"t1.f_code = t2.f_ware_id",[])
            ->join(["t3"=>"t_wf_phases"],"t_creation_date.f_phase_id = t3.f_number and t_creation_date.f_wf_id = t3.f_wf_id",[])
            ->where("t3.f_group_id not in(6,7,8)")->where("t2.f_wo_id = ?",$woCode)
            ->where("t1.f_type_id = 13")->query()->fetchAll();      
        
        $tot = count($res_chain);        
        for($i = 0;$i < $tot;++$i) {
            $select->reset();
            $res_asset_chain = $select->from(["t1"=>"t_wares"],['f_code','f_type_id'])
                ->join("t_creation_date","t1.f_code = t_creation_date.f_id",["f_title"])
                ->join(["t2"=>"t_wares_relations"],"t1.f_code = t2.f_code_ware_slave",[])
                ->join(["t3"=>"t_wf_phases"],"t_creation_date.f_phase_id = t3.f_number and t_creation_date.f_wf_id = t3.f_wf_id",[])
                ->where("t3.f_group_id not in(6,7,8)")->where("t2.f_code_ware_master = ?",$res_chain[$i]['f_code'])->where("t2.f_type_id_slave = 1")
                ->where("t1.f_start_date IS NULL OR (t1.f_start_date < ?)",$start_date)->query()->fetchAll();
            $tot_ac = count($res_asset_chain);                
            for($ac = 0;$ac < $tot_ac; ++$ac ) {                    
                $res_asset[$res_asset_chain[$ac]['f_code']] = $res_asset_chain[$ac];                    
            }
        } 
        return $res_asset;
    }
    
    public static function getAsset($woCode,$start_date,$res_asset,$db) 
    {
        $select = new Zend_Db_Select($db);
        $res_asset_singles = $select->from(["t1"=>"t_wares"],['f_code','f_type_id'])
            ->join("t_creation_date","t1.f_code = t_creation_date.f_id",["f_title"])
            ->join(["t2"=>"t_ware_wo"],"t1.f_code = t2.f_ware_id",[])
            ->join(["t3"=>"t_wf_phases"],"t_creation_date.f_phase_id = t3.f_number and t_creation_date.f_wf_id = t3.f_wf_id",[])
            ->where("t3.f_group_id not in(6,7,8)")->where("t2.f_wo_id = ?",$woCode)
            ->where("t1.f_type_id = 1")->where("t1.f_start_date IS NULL OR (t1.f_start_date < ?)", $start_date)
            ->query()->fetchAll();           
        //change keys of asset with f_code to simplify the check            
        $tot_ra = count($res_asset_singles);
        for($ra = 0;$ra < $tot_ra;++$ra) {
            $res_asset[$res_asset_singles[$ra]['f_code']] = $res_asset_singles[$ra];
        }
        return $res_asset;
    }
    
    /**
     * extract task from tasklist
     * @param type $codeTaskList f_code tasklist
     * @param type $res_task list of task already inserted
     */
    public static function getTaskFromTaskList($codeTaskList,$res_task,$db,$f_counter=1,$tasklistSequence=1)
    {
        $select = new Zend_Db_Select($db);
        $res_task_fromlist = $select->from(["t1"=>"t_wares_relations"],[])
            ->join(["t2"=>"t_wares"],"t1.f_code_ware_slave = t2.f_code",["f_code","f_type_id",'fc_task_sequence','fc_task_exception_allow','fc_task_exception_deny'])
            ->join(["t3"=>"t_creation_date"],"t2.f_code = t3.f_id",['f_title'])->where("t1.f_code_ware_master = ?",$codeTaskList)
            ->where("t2.f_type_id = 10")->where("t3.f_phase_id = 1")->order("t1.f_id ASC")->query()->fetchAll();
        $tot_rtf = count($res_task_fromlist);
        for($rtf = 0;$rtf < $tot_rtf;++$rtf) {
           if($tasklistSequence!=1 || Mainsim_Model_Utilities::checkTaskSequence($f_counter,  $res_task_fromlist[$rtf]['fc_task_sequence'],  $res_task_fromlist[$rtf]['fc_task_exception_allow'],  $res_task_fromlist[$rtf]['fc_task_exception_deny']))
            $res_task[$res_task_fromlist[$rtf]['f_code']] = $res_task_fromlist[$rtf];
        } 
        return $res_task;
    }
    /**
     * Get Database type
     */
    public static function isMysql()
    {
        $config = Zend_Registry::get('db');
        $dbConf = $config->toArray();
        return stripos($dbConf['adapter'],'PDO_MYSQL') !== false ? true:false;   
    }
    
    
    /**
     * change all '' (empty string) in null
     * @param array $params
     * @return array $params cleared from empty values
     */
    public static function clearEmptyValuesParams($params)
    { 
        array_walk_recursive($params, function(&$el){
            $el = strlen($el)?$el:null;
        });
        return $params;
    }
    
    /**
     * 
     * @param string $table table to check (t_wares,t_workorders,t_selectors,t_systems)
     * @param int $f_code f_code to check
     * @param Zend_Db $db db connection
     * @return boolean true if there are active children otherwise false
     */
    public static function checkActiveChildren($table,$f_code,$db)
    {
        $children = [];
        $children = self::getChildCode($table.'_parent',$f_code,$children,$db,true);
        if(empty($children)){ return false;}        
        $select = new Zend_Db_Select($db);
        $res = $select->from(['t1'=>'t_creation_date'],['num'=>'count(t1.f_id)'])
            ->join(['t2'=>'t_wf_phases'],'t1.f_wf_id = t2.f_wf_id and t1.f_phase_id = t2.f_number',[])
            ->where("t1.f_id IN (?)",$children)->where("f_group_id not in (6,7)")->query()->fetch();
        return $res['num'] > 0;
    }

    public static function setParamsNull($adapter, $params) {
        if($adapter != 'sqlsrv') return $params;
        foreach($params as $k => $v) {
            if(is_array($v)) $params[$k] = Mainsim_Model_Utilities::setParamsNull($adapter, $v);
            if(empty($v)) $params[$k] = NULL;
        }
        return $params; 
    }
 
    public static function getLaborFee($f_code,$fee,$db) {
        $select = new Zend_Db_Select($db);
        $res = $select->from(['wa'=>'t_wares'],['fc_rsc_standard','fc_rsc_overtime','fc_rsc_holiday','fc_rsc_overnight'])
            ->join(['cf'=>'t_custom_fields'],'cf.f_code = wa.f_code')
            ->where("wa.f_code = (?)",$f_code)->query()->fetch();

        if(array_search($fee,['standard','overtime','holiday','overnight']) !== false) $fee = 'fc_rsc_'.$fee;

        return $res[$fee];
    }
    
    public static function getPairCrossByType($f_code,$db) {
        $select = new Zend_Db_Select($db);
        $select->from('t_pair_cross as pc')
             ->join(['wa'=>'t_wares'],'wa.f_code = pc.f_code_cross',['f_type_id'])   
            ->where('pc.f_code_main = ' . $f_code)
            ->where('wa.f_type_id >= 1 ');
        $temp = $select->query()->fetchAll();
        
        if(empty($temp)) return [];
        $result = array();
        //print_r(array_column($temp,'f_code_cross','f_type_id')); die("rtemp");
        foreach ($temp as $value) {
            if(empty($result[$value['f_type_id']])) $result[$value['f_type_id']] = array();
            $new = $value; unset($new['f_type_id']);
            $result[$value['f_type_id']] [] = $new;
        }
        
        return $result;
    }
    
    public static function mime_content_type($filename) {

            $mime_types = array(

                    'txt' => 'text/plain',
                    'htm' => 'text/html',
                    'html' => 'text/html',
                    'php' => 'text/html',
                    'css' => 'text/css',
                    'js' => 'application/javascript',
                    'json' => 'application/json',
                    'xml' => 'application/xml',
                    'swf' => 'application/x-shockwave-flash',
                    'flv' => 'video/x-flv',

                    // images
                    'png' => 'image/png',
                    'jpe' => 'image/jpeg',
                    'jpeg' => 'image/jpeg',
                    'jpg' => 'image/jpeg',
                    'gif' => 'image/gif',
                    'bmp' => 'image/bmp',
                    'ico' => 'image/vnd.microsoft.icon',
                    'tiff' => 'image/tiff',
                    'tif' => 'image/tiff',
                    'svg' => 'image/svg+xml',
                    'svgz' => 'image/svg+xml',

                    // archives
                    'zip' => 'application/zip',
                    'rar' => 'application/x-rar-compressed',
                    'exe' => 'application/x-msdownload',
                    'msi' => 'application/x-msdownload',
                    'cab' => 'application/vnd.ms-cab-compressed',

                    // audio/video
                    'mp3' => 'audio/mpeg',
                    'qt' => 'video/quicktime',
                    'mov' => 'video/quicktime',

                    // adobe
                    'pdf' => 'application/pdf',
                    'psd' => 'image/vnd.adobe.photoshop',
                    'ai' => 'application/postscript',
                    'eps' => 'application/postscript',
                    'ps' => 'application/postscript',

                    // ms office
                    'doc' => 'application/msword',
                    'rtf' => 'application/rtf',
                    'xls' => 'application/vnd.ms-excel',
                    'ppt' => 'application/vnd.ms-powerpoint',

                    // open office
                    'odt' => 'application/vnd.oasis.opendocument.text',
                    'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
            );

            $ext = strtolower(array_pop(explode('.',$filename)));
            if (array_key_exists($ext, $mime_types)) {
                    return $mime_types[$ext];
            }
            elseif (function_exists('finfo_open')) {
                    $finfo = finfo_open(FILEINFO_MIME);
                    $mimetype = finfo_file($finfo, $filename);
                    finfo_close($finfo);
                    return $mimetype;
            }
            else {
                    return 'application/octet-stream';
            }
    }

    public static function getPairCross($f_code){
        $db = Zend_Registry::isRegistered('db_connection_editor')?Zend_Registry::get('db_connection_editor'):Zend_Db::factory(Zend_Registry::get('db'));
        $select = new Zend_Db_Select($db);
        $select->from('t_pair_cross')
            ->where('f_code_main = ' . $f_code);
        $res = $select->query()->fetchAll();
        return $res;
    }
    
}
