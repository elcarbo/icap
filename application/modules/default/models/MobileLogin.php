<?php
class Mainsim_Model_MobileLogin extends Mainsim_Model_MobileBase{
    
    public function __construct() {
        
       parent::__construct();
    }

    public function login($username, $password, $deviceId = null, $sessionId = null){
        
        try{
            // check credentials
            $select = new Zend_Db_Select($this->db);
            $select->from(array("u" => "t_users"), array("f_code", "fc_usr_usn", "fc_usr_pwd", "fc_usr_mail","fc_usr_phone", "fc_usr_language", "fc_usr_firstname", "fc_usr_lastname", "fc_usr_level", "fc_usr_level_text", "fc_usr_group_level"))
                   ->join(array("cd" => "t_creation_date"), "u.f_code = cd.f_id", array())
                   ->where("u.fc_usr_usn = " . $this->db->quote($username))
                   ->where("u.fc_usr_pwd = " . $this->db->quote($this->crypt(md5($password))))
                   ->where("cd.f_phase_id = 1");             
            $res = $this->dbSelect($select);
            // user exists
            if(count($res) > 0){
                // check if application is in maintenance 
                if($res[0]['fc_usr_level'] != -1) {
                  /*  $selectMaintenance = "SELECT * FROM t_system_information";
                    $resMaintenance = $this->db->query($selectMaintenance)->fetchAll();
                    if($resMaintenance[0]['f_maintenance'] == 1){
                        return -100;
                    } 
                    */
                    $selectMaintenance = "SELECT * FROM t_creation_date WHERE f_category LIKE 'SETTING' AND f_title LIKE 'UNDER_MAINTENANCE'";
                    $resMaintenance = $this->db->query($selectMaintenance)->fetch();
                    if($resMaintenance['f_description'] == 1) {
                        return -100; 
                    } 
                }
                // set wo types to download
                $select->reset();
                $select->from('t_creation_date')
                       ->where("f_title = 'mobile_wo_types'");
                $res2 = $select->query()->fetchAll();
                if($res2[0]['f_description']){
                    $res[0]['workorder_types'] = $res2[0]['f_description'];
                }
                else{
                    $res[0]['workorder_types'] = WORKORDER_TYPES;
                }
                
                // save session user data
                $auth = Zend_Auth::getInstance();
                $authStorage = $auth->getStorage(); 
                //$session = new Zend_Session_Namespace('session');
                $res[0]['sessionId'] = session_id();
                $res[0]['f_timestamp'] = time();
                $authStorage->write($res[0]);
                //$session->data = $res[0];
                
                // if exists workorders locked in previously session --> unlock them
                
                $objWo = new Mainsim_Model_MobileWorkorders();
                $objWo->unlockWorkorder(array('userId' => $res[0]['f_code']));
                // set mobile admin
                if($res[0]['f_code'] == 1){
                    $res[0]['f_admin'] = 1;
                }
                
                // update user status 
                $this->dbUpdate('t_users', array('fc_usr_online' => 2), array('f_code' => $res[0]['f_code']));
                
                return $this->createResponseObject(1, 'USER_LOGGED', session_id(), $res[0], $this->translate('login effettuato'), $this->translate('header login effettuato'));
            }
            else{
                return $this->createResponseObject(0, 'USER_NOT_EXISTS', null, null, $this->translate('try to re-enter user and password'), $this->translate('account error'));
            }
        }
        catch(Exception $e){
            print($e->getMessage());
        }
    }
    
    public function editSettings($settings){

        $userData = Zend_Auth::getInstance()->getIdentity();
        
        foreach($settings as $key => $value){
            
            switch($key){
                
                case 'fc_usr_mobile_autoswitch_desktop':
                    $update[$key] = $value;
                    $this->dbUpdate('t_users', $update, array('f_code' => $userData['f_code']));
                    break;
            }
        }
    }
    
    public function getSettings(){
        
        $userData = Zend_Auth::getInstance()->getIdentity();
        
        $select = new Zend_Db_select($this->db);
        $select->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
                ->join(array("s" => "t_systems"), "c.f_id = s.f_code")
                ->where("c.f_type = 'SYSTEMS'")
                ->where("c.f_category = 'SETTING'")
                ->where("s.fc_sys_server_only IS NULL")
                ->orWhere("s.fc_sys_server_only != 1");
        $res = $select->query()->fetchAll();
        for($i = 0; $i < count($res); $i++){
            $settings[$res[$i]['f_title']] = $res[$i]['f_description'];
        }
        
        $select->reset();
        $select->from("t_users", array("fc_usr_mobile_autoswitch_desktop"))
                ->where("f_code = " . $userData['f_code']);
        $res = $select->query()->fetchAll();
        
        $settings = array_merge($settings, $res[0]);
        
        return $settings;
    }
    
    public function logout($params = null){
        
        try{
            $this->dbBeginTransaction();
            // unlock workorders
            $userData = Zend_Auth::getInstance()->getIdentity();
            //$session = new Zend_Session_Namespace('session');
            
            $this->dbDelete('t_locked', array('f_user_id = ' .  $userData['f_code']));
            //$this->dbDelete('t_locked', array('f_user_id' => $session->data['f_code']));
            // update access table
            //$this->dbUpdate('t_access_registry', array('f_active' => 0), array('f_user_id' => $userData['f_code']));
            
            // update t_users table
            $selectUser = new Zend_Db_Select($this->db);
            $selectUser->from('t_users')
                       ->join('t_access_registry', 't_users.f_code = t_access_registry.f_user_id')
                       ->where('t_users.f_code =' . $userData['f_code']);
            $resUser = $this->dbSelect($selectUser);
            $time = $resUser[0]['fc_usr_connection_time']  + time() - $resUser[0]['f_timestamp'];
            $this->dbUpdate('t_users', array('fc_usr_connection_time' => $time, 'fc_usr_online' => 0), array('f_code' => $userData['f_code']));
            
            $this->dbDelete("t_access_registry", "f_user_id = {$userData['f_code']}");
            $this->dbEndTransaction();
            // delete session
            Zend_Auth::getInstance()->clearIdentity();
            //Zend_Session::destroy();
            return $this->createResponseObject(1, 'USER_UNLOGGED', null, null, null, null);
        }
        catch(Exception $e){
            print($e->getMessage());
        }
    }
    
    public function userAlreadyLogged($userId){
        
        try{
            $select = new Zend_Db_Select($this->db);
            $select->from("t_access_registry", array('count(*) as logged'))
                   ->where("f_user_id = '" . $userId . "'");
            $res = $this->dbSelect($select);
            if($res[0]['logged']){
                return $this->createResponseObject(1, 'USER_ALREADY_LOGGED', null, null, $this->translate('overwrite session?'), $this->translate('user already logged'));
            }
            else{
                return null;
            }
        }
        catch(Exception $e){
            print($e->getMessage());
        }
    }
    
    public function saveLogin($userId, $sessionId, $deviceId = null){
        
        try{
            // disable old login if exists
            /*$this->dbUpdate(
                't_access_registry',
                array(
                    'f_active' => 0
                ),
                array(
                    'f_user_id' => $userId
                )
            );*/
            $this->dbDelete("t_access_registry", "f_user_id = $userId");

            // insert new login session
            $this->dbInsert(
                't_access_registry',
                array(
                    'f_user_id' => $userId,
                    'f_last_action_on' => time(),
                    'f_user_agent' => 0,
                    'f_ip' => 0,
                    'f_timestamp' => time(),
                    'f_session_id' => $sessionId,
                    'f_device_id' => $deviceId
                )
            );
        }
        catch(Exception $e){
             print($e->getMessage());
        }
    }
    
    private function crypt($pv)
    {
        $xc0=substr($pv,0,2);
        $xc1=substr($pv,2,6);
        $xc2=substr($pv,8,16);
        $xc3=substr($pv,24,8);
        $pv = $xc1.$xc3.$xc0.$xc2;
    
        $xc0=substr($pv,0,5);
        $xc1=ord(substr($pv,5,1));
        $xc1++; if($xc1>57 && $xc1<90) $xc1=52;
        if($xc1>95) $xc1-=49;
        $xc2=substr($pv,6,26);          
        return $xc2.$xc1.$xc0;
    }
}
?>
