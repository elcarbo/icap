<?php
class Default_Bootstrap extends Zend_Application_Module_Bootstrap 
{
  protected function _initAutoload()
  {      
      require_once APPLICATION_PATH.'/configs/constants.php';
  	$autoloader = new Zend_Application_Module_Autoloader(array(
         'namespace' => 'Mainsim_',
         'basePath'  => APPLICATION_PATH .'/modules/default',
         'resourceTypes' => array ()));
      return $autoloader;      
  }
}
