<?php

class SelectorsController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    /**
     * @deprecated
     */
    public function indexAction()
    {
        $response = array();
        $selector  = new Mainsim_Model_Selectors();
        $order_field = isset($_POST['orderby']) && !empty($_POST['orderby'])?$_POST['orderby']:'f_code';
        $order = isset($_POST['order']) && !empty($_POST['order'])?$_POST['order']:'ASC';
        $ignoreSel = isset($_POST['ignoreSelector']) && !empty($_POST['ignoreSelector'])?$_POST['ignoreSelector']:false;
        if(isset($_REQUEST['type'])) {            
            $response = $selector->getSelectorsFromType($_REQUEST['type'],$order_field,$order,$ignoreSel);
        }
        elseif(isset($_REQUEST['id']) && ctype_digit($_REQUEST['id'])) {
            $response = $selector->getSelectorsFromId($_REQUEST['id'],$order_field,$order,$ignoreSel);
        }
        echo json_encode($response);        
    }
    
    public function getAction() {        
        if(isset($_POST["param"])) $params = json_decode($_POST["param"], true);
        if(isset($params["stype"]) && $params["stype"]) $stype = $params["stype"]; else $stype = 1;
        if(isset($params["parent"]) && $params["parent"]) $parent = $params["parent"]; else $parent = 0;
        if(isset($params["orderby"]) && $params["orderby"]) $orderby = $params["orderby"]; else $orderby = 'f_title';
        if(isset($params["order"]) && $params["order"]) $order = $params["order"]; else $order = 'ASC';
        $model = new Mainsim_Model_Selectors();
        $data = $model->get($stype, $parent, $orderby, $order);
        echo json_encode(array( "parent" => $parent, "data" => $data));
    }
    
    public function editAction()
    {        
        $params = json_decode($_POST['params'],true);        
        $sel = new Mainsim_Model_Selectors();
        $error  = array();
        try {
            $error = $sel->editSelector($params);        
        }catch(Exception $e) {
            $error['message'] = utf8_decode($e->getMessage());
        }        
        echo json_encode($error);
    }
    
    /**
     * Edit batch 
     */
    public function editBatchAction()
    {
        $params = $_POST['params'];        
        $wares = new Mainsim_Model_MainsimObject();        
        $res = array();
        try {
            $res = $wares->editBatch('t_selectors',$params);              
        }catch (Exception $e) {
            $res['message'] = 'Something went wrong while editing your wares '.$e->getMessage();
        }
        echo json_encode($res);die;
    }
        
    public function newAction()
    {
        $params = json_decode($_POST['params'],true);        
        $sel = new Mainsim_Model_Selectors();
        $error  = array();
        try {
            $error = $sel->newSelector($params);        
        }catch(Exception $e)  {
            $error['message'] = utf8_decode($e->getMessage());
        }
        echo json_encode($error);
    }
    
    public function cloneSelectorsAction() {
        if(!isset($_POST["f_module_name"])) { echo json_encode(array("message" => "Missing module name")); die; }
        else if(!isset($_POST["f_code"])) { echo json_encode(array("message" => "No elements to clone")); die; }
        else if(!isset($_POST["clone_type"])) $_POST["clone_type"] = 0;
        $sel = new Mainsim_Model_Selectors();
        echo json_encode($sel->cloneSelectors($_POST["clone_type"], $_POST["f_code"], $_POST["f_module_name"]));
    }
    
    public function changePhaseBatchAction() {
        $selector = new Mainsim_Model_Selectors();
        echo json_encode($selector->changePhaseBatch($_POST["f_codes"], $_POST["f_wf_id"], $_POST["f_exit"], $_POST["f_module_name"]));
    }
}