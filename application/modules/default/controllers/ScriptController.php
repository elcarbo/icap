<?php

class ScriptController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    public function executeAction()
    {
        if(!empty($_POST)) {
            $script = new Mainsim_Model_Script();
            echo json_encode($script->execute($_POST));
        }
    }
	
	public function datadbactionemailAction()
    {
        if(!empty($_POST)) {
            $script = new Mainsim_Model_Script();
            print(json_encode($script->datadbactionemail($_POST)));
        }
    }

    public function popupAction() {
        if(!empty($_POST)) {
            $script = new Mainsim_Model_Script();
            print json_encode($script->popupForm($_POST));
        }
    }
}