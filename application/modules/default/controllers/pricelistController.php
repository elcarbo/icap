<?php
class PricelistController extends Zend_Controller_Action
{
    public function preDispatch() 
    {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function jaxReadAction(){
        
        $params = json_decode($_POST['param'], true);
        $objPrl = new Mainsim_Model_Pricelist();
        $response = $objPrl->getPricelistByWo($params['f_code'], $params['qof']);
        print($response);
        die();
    }
}
?>

