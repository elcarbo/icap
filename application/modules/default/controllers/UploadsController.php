<?php

class UploadsController extends Zend_Controller_Action
{
    public function preDispatch() 
    {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    public function uploadAction()
    {
        $upload = new Mainsim_Model_Uploads();        
        $res = $upload->uploads($_FILES, $_POST);      
        echo "<script> parent.moAttach.callback($res); </script>";  
    }
    
    /**
     * @deprecated since 3.0.7.4
     * remove attached file managed on save
     */
    public function removeAction() 
    {           
        $upload = new Mainsim_Model_Uploads(); 
        $params = json_decode($_POST['params'], true);
        $upload->removeAttach($params['f_code'], $params['f_fieldname']);
        echo 'ok';
    }
    
    public function getDocumentAction()
    {        
        $upload = new Mainsim_Model_Uploads();
        $res_img = $upload->getDocument(array(
            "session_id" => $_GET["session_id"],
            "size" => (isset($_GET["size"]) ? $_GET["size"] : "doc")
        ));        
        if(!empty($res_img)) {
            $mime = $res_img[0]['f_mime'];
            
            if($res_img[0]['f_type'] == "doc") {
                $name = $res_img[0]["f_file_name"].".".$res_img[0]["f_file_ext"];
                header("Content-Type: application/octet-stream");
                header('Content-Disposition: attachment; filename="'.$name.'"');
            }            
            $content = $res_img[0]['f_data'];            
            header("Content-Type: $mime");
        }
    }
    
    public function multipleUploadsAction()
    {
        $upload = new Mainsim_Model_Uploads();
        $res = $upload->multipleUploads();
        echo "<script>parent.moLibrary.multipleAttach({mod: '{$_GET["mod"]}'}, '$res'); </script>";        
    }
    
    /**
     * add files from wizard. When user finish the request
     * system will convert file in docs and attach this to the request
     */
    public function wizardDocUploadAction()
    {
  
		$upload = new Mainsim_Model_Uploads();
        $files = []; 
        if(is_array($_FILES['attachments']['name'])){
            $tot = count($_FILES['attachments']['name']);        
            for($i = 0;$i < $tot;++$i) {
                $files[] = [
                    'name'=>$_FILES['attachments']['name'][$i],
                    'type'=>$_FILES['attachments']['type'][$i],
                    'tmp_name'=>$_FILES['attachments']['tmp_name'][$i],
                    'error'=>$_FILES['attachments']['error'][$i],
                    'size'=>$_FILES['attachments']['size'][$i]
                ];
            }        
        }
        else { $files[] = $_FILES['attachments'];}

        $res = $upload->multipleUploadsFromWiz($files);
        $res = json_encode($res);
        if($_GET['mode'] == 1) {echo "<script> parent.mdlTab.ActAdd('".$res."');</script>"; }
        else { echo $res; }
        die;
    }
    
  
     /**
     * add files from treegrid file & links or treegrid_light_check.
     * convert tempfile in docs 
     * return file information for treegrid_light_check (props for updateChecked )     
     */
    public function dragdropDocUploadAction()
    {
        $upload = new Mainsim_Model_Uploads();
        $files = []; 

        if(is_array($_FILES['attachments']['name'])){
            $tot = count($_FILES['attachments']['name']);        
            for($i = 0;$i < $tot;++$i) {
                $files[] = [
                    'name'=>$_FILES['attachments']['name'][$i],
                    'type'=>$_FILES['attachments']['type'][$i],
                    'tmp_name'=>$_FILES['attachments']['tmp_name'][$i],
                    'error'=>$_FILES['attachments']['error'][$i],
                    'size'=>$_FILES['attachments']['size'][$i]
                ];
            }        
        }
        else { $files[] = $_FILES['attachments']; }  

        $res = $upload->multipleUploadsFromDragDrop($files);
        
        echo json_encode($res);
        die;
    }


    
    /**
     * remove added files from wizard.
     */
    public function removeWizardDocUploadedAction()
    {
        $upload = new Mainsim_Model_Uploads();
        $params = json_decode($_POST['param'],true);        
        $upload->removewizAttachment($params['url']);
        echo '[]';die;
    }
}
