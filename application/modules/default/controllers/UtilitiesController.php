<?php

class UtilitiesController extends Zend_Controller_Action
{
     public function preDispatch() {
         parent::preDispatch();
         $this->_helper->layout()->disableLayout();
         $this->_helper->viewRenderer->setNoRender(true);
     }
     
     
     /**
      *  Action per il passaggio in chiusura di una categoria di 
      *  wo
      */
     public function closeWoAction()
     {
         $utilities = new Mainsim_Model_Utilities();
         $type_id = $_REQUEST['type_id'];
         $phase_num = $_REQUEST['phase_num'];
         $utilities->changePhaseWo($type_id, $phase_num);
     }
}