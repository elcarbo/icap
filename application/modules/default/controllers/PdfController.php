<?php

class PdfController extends Zend_Controller_Action {

    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function pdfParamsAction() {
        $error = array();
        if (empty($_POST)) {
            $error['message'] = 'Empty filter';
        } else {
            $post = json_decode($_POST['filter']);
            $_SESSION['pdfParams'] = $post;
        }
        echo json_encode($error);
        die;
    }

    public function printPdfAction() {
        $params = $this->_getAllParams();
        $pdfCreator = new Mainsim_Model_Pdf();
        $pdf = $pdfCreator->printPdf($params);
        if (isset($params['pdf_name'])) {
            $name = $params['pdf_name'];
        } else {
            $name = 'MAINSIM_PDF_' . str_replace(',', '_', $params['f_code']) . '.pdf';
        }
        $pdf->Output($name, 'I');
    }
}
