<?php

class InstallerController extends Zend_Controller_Action
{     
    private $trsl;
    
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $langCode = Zend_Auth::getInstance()->hasIdentity()?Zend_Auth::getInstance()->getIdentity()->f_language:1;
        $this->trsl = new Mainsim_Model_Translate('en_GB');
        $this->trsl->setLang($this->trsl->getLangNameFromCode($langCode));
        //Zend_session::writeClose();
    } 
    

    public function newAction()
    {
    ////multi-installation.mainsim.com/attachments/testinstal
        //fc_prj_host => 10.0.97.7
        //"fc_prj_username"=> "host", "fc_prj_password"=> "23aGRpqZq7ud4vbK"
        //var_dump($_POST);die();
      // $params = array("f_id"=> 0,"f_code"=> 0,"f_type_id"=> 8,"f_timestamp"=> 1434034984, "f_user_id"=> 1, "fc_prj_url"=> "testinstal3.mainsim.com", "fc_prj_attachment_path"=> "C:/wamp/www/mainsim3/attachments", "fc_prj_adapter"=> "PDO_MYSQL", "fc_prj_host"=> "localhost", "fc_prj_dbname"=> "mainsim3_testinstal3", "fc_prj_username"=> "root", "fc_prj_password"=> "1881", "fc_prj_installation_type"=> "enterprise", "f_type"=> "WARES", "f_category"=> "ASSET", "f_order"=> 1, "f_creation_date"=> 1428484491, "f_creation_user"=> 1, "f_title"=> "testinstal3", "f_wf_id"=> 9, "f_phase_id"=> 1, "f_visibility"=> -1, "fc_progress"=> 1, "fc_documents"=> 0, "fc_contracts"=> 0, "f_edit"=> -1, "f_percentage"=> 30, "f_name"=> "Active", "prj_approved"=> "approved", "f_parent_code"=> "0", "f_module_name"=> "mdl_prj_tg", "unique"=> [ "fc_prj_url" ], "attachfields"=> [], "mandatory"=> [], "error"=> [], "attachments"=> []);
       $params = $_POST; 
       
        //$params = json_decode($data_js,true);  
       if(isset($_POST['pass_mainsim']) && count($params) > 1){
           $pass = $_POST['pass_mainsim'];
          
           $correct_pass = 'ae95d95ce5f491ca65c3940ebe2a819b';
           if($pass === $correct_pass){
           
                $wares = new Mainsim_Model_System();
                $res = array();

                try {
                    $res = $wares->newSystem($params);             
                }catch(Exception $e) {
                    $res['message'] = $this->trsl->_("Error").": ".$e->getMessage();
                }        
                echo json_encode($res);die;
           }
       }
    }
    
    public function controlAction(){
        $params = $_POST;
        $obj = new Mainsim_Model_Installer();
        $result = $obj->control_name_installation($_POST);
        return $result;
    }
    
      
}