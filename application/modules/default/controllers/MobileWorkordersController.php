<?php

class MobileWorkordersController extends Zend_Controller_Action{
    
    public function init(){
        $this->_helper->layout()->disableLayout();
        //Mainsim_Model_MobileUtilities::checkSession($_POST);
        //Zend_session::writeClose();
    }
    
    public function editAction(){
        // process attach documents --> create a ware for each document attached
        if($_POST['params']['doc_data_b64']){
            $k = 1;
            foreach($_POST['params']['doc_data_b64'] as $key => $value){
                $objWare = new Mainsim_Model_MobileWares();
                
                // set data for each ware that has to be inserted
                $wareParams = null;
                $wareParams['data']['t_creation_date'][] = $_POST['params']['doc_tables']['t_creation_date'][0];
                $wareParams['data']['t_creation_date'][] = $_POST['params']['doc_tables']['t_creation_date'][$k];
                $wareParams['data']['t_wares'][] = $_POST['params']['doc_tables']['t_wares'][0];
                $wareParams['data']['t_wares'][] = $_POST['params']['doc_tables']['t_wares'][$k];
                $wareParams['data']['t_custom_fields'][] = $_POST['params']['doc_tables']['t_custom_fields'][0];
                $wareParams['data']['t_custom_fields'][] = $_POST['params']['doc_tables']['t_custom_fields'][$k];
                unset($_POST['params']['doc_tables']['t_wares_parent'][0][5]);
                unset($_POST['params']['doc_tables']['t_wares_parent'][$k][5]);
                $wareParams['data']['t_wares_parent'][] = $_POST['params']['doc_tables']['t_wares_parent'][0];
                $wareParams['data']['t_wares_parent'][] = $_POST['params']['doc_tables']['t_wares_parent'][$k];
                $wareParams['document'] = $value;
                $ware = $objWare->newWare($wareParams);
                $doc_codes_list[$key] = $ware['f_code_new'];
                $k++;
                
                // update ware_wo data with documents newly created
                for($i = 1; $i < count($_POST['params']['doc_tables']['t_ware_wo']); $i++){
                    $row = $_POST['params']['doc_tables']['t_ware_wo'][$i];
                    if($row[1] == $key){
                        $row[1] = $ware['f_code_new'];
                        $_POST['params']['tables']['t_ware_wo'][] = $row;
                    }
                }
            } 
        }
        
        $userData = Zend_Auth::getInstance()->getIdentity();

        //$session = new Zend_Session_Namespace('session');
        //$userData = $session->data;
        $objWo = new Mainsim_Model_MobileWorkorders();
        // check if workorders are locked by user
        $params = array(
            'woList' => $_POST['params']['codes_list'],
            'userId' => $userData['f_code']
        );
        if($objWo->checkUserLock($params)){
            // edit workorders
            $params = array(
                'woData' => $_POST['params']['tables'],
                'woList' => $_POST['params']['codes_list'],
                'userId' => $userData['f_code']
            );
            
            // set documents to disassociate if they exist
            if($_POST['params']['doc_wo_deleted']){
                $params['docs_deleted'] = $_POST['params']['doc_wo_deleted'];
            }   
            
            $response = $objWo->editWorkorders($params);
            if(is_array($doc_codes_list)){
               $response['params']['doc_codes_list'] = $doc_codes_list; 
            }
            print(json_encode($response));
        }
        // wo sent by user are not consistent --> return KO
        else{
            print(json_encode($objWo->createResponseObject(1, 'SESSION_NOT_VALID')));
        }
        die();
    }
    
    public function lockAction(){
        $objWo = new Mainsim_Model_MobileWorkorders();
        $params = array(
            'userId' => $_POST['userId'],
            'code_list' => $_POST['params']['code_list']
        );
        $res = $objWo->lockWorkorders($params);
        if(!is_array($res['locked'])){
            $res['locked'] = array();
        }
        print(json_encode($objWo->createResponseObject(1, 'WORKORDERS_LOCK', $_POST['sessionId'], array('code_list' => $res['locked']))));
        die();
    }
    
    public function unlockAction(){
        $objWo = new Mainsim_Model_MobileWorkorders();
        $params = array(
            'userId' => $_POST['userId'],
            'code_list' => $_POST['params']['code_list']
        );
        $res = $objWo->unlockWorkorders($params);
        if($res){
            print(json_encode($objWo->createResponseObject(1, 'WORKORDERS_UNLOCK', $_POST['sessionId'], array('code_list' => $_POST['params']['code_list']))));
        }
        else{
            print(json_encode($objWo->createResponseObject(0, 'WORKORDERS_UNLOCK_ERROR', $_POST['sessionId'])));
        }
        die();
    }
    
    public function releaseAction(){
        $objWo = new Mainsim_Model_MobileWorkorders();
        $params['woList'] = $_POST['params']['f_codes'];
        $userData = Zend_Auth::getInstance()->getIdentity();
        $params['userId'] = $userData['f_code'];
        print(json_encode($objWo->releaseWorkorders($params)));
        die();
    }
    
    public function checkNfcAction(){
        $userData = Zend_Auth::getInstance()->getIdentity();
        $objWo = new Mainsim_Model_MobileWorkorders();
        $_POST['params']['type_id'] = $userData['workorder_types'];
        print(json_encode($objWo->searchNFCWorkorders($_POST['params'])));
        die();
    }
    
    public function testAction(){
        $objWo = new Mainsim_Model_MobileWorkorders();

        $codesLocked = $objWo->testGetUserWorkordersLocked();
        if(!is_array($codesLocked)){
            $codesLocked = array();
        }
        $params = array(
            'limit' => 10,
            'noLocked' => true,
            'wares' => true
        );
        $codesUnlocked = $objWo->getUserWorkordersCodes($params);
        
        $data = $objWo->getUserWorkordersData(array_merge($codesLocked, $codesUnlocked), $params);
        die();
    }

}

