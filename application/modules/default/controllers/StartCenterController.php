<?php

class StartCenterController extends Zend_Controller_Action
{
    public function preDispatch() 
    {        
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();        
        //Zend_session::writeClose();
    }
    
    public function personalQueriesAction() 
    {
       
    }
    
    public function kpi1Action() 
    {        
        
    }
    
    public function kpi2Action() 
    {
        
    }
    
    public function kpi3Action() 
    {        
        
    }
    
    public function kpi4Action() 
    {
        
    }
    
    public function kpi5Action() 
    {        
        
    }
    
    public function kpi6Action() 
    {
        
    }
    
    public function kpi7Action() 
    {        
        
    }
    
    public function kpi8Action() 
    {
        
    }
    
    public function kpi9Action() 
    {        
        
    }
    
    public function kpi10Action() 
    {
        
    }
    public function kpi11Action() 
    {
        
    }
    public function kpi12Action() 
    {
        
    }
    public function kpi13Action() 
    {
        
    }
    public function kpi14Action() 
    {
        
    }
    
    public function kpi15Action() 
    {
        
    }
    
    public function kpi16Action() 
    {
        
    }
    public function kpi17Action() 
    {
        
    }
    public function kpi18Action() 
    {
        
    }
    public function kpi19Action() 
    {
        
    }
    public function kpi20Action() 
    {
        
    }
    public function kpi21Action() 
    {
        
    }
    public function kpi22Action() 
    {
        
    }
    
    public function kpiscriptAction() 
    {
        
    }
 
    public function kpitrendAction() 
    {
        
    }
    
    public function kpitrendhpgAction() 
    {
        
    }
 
    public function getKpi1Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi1($_POST)); die;
    }
    
    public function getKpi2Action() {              
        ////Zend_session::writeClose();        
        $sc = new Mainsim_Model_StartCenter();        
        echo json_encode($sc->kpi2($_POST)); die;
    }
    
    public function getKpi3Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi3($_POST)); die;
    }
    
    public function getKpi4Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi4($_POST)); die;
    }
    
    public function getKpi5Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi5($_POST)); die;
    }
    
    public function getKpi6Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi6($_POST)); die;
    }
    
    public function getKpi7Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi7($_POST)); die;
    }
    
    public function getKpi8Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi8($_POST)); die;
    }
    
    public function getKpi9Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi9($_POST)); die;
    }
    
    public function getKpi10Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi10($_POST)); die;
    }
    
    public function getKpi11Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi11($_POST)); die;
    }
    
    public function getKpi12Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi12($_POST)); die;
    }
    
    public function getKpi13Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi13($_POST)); die;
    }
    
    public function getKpi14Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi14($_POST)); die;
    }
    
    public function getWoByType1Action() 
    {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->getgetWoByType1($_POST)); die;
    }
    
    public function getWoByType6Action() 
    {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->getgetWoByType6($_POST)); die;
    }
    public function getKpi17Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi17($_POST)); die;
    }
    public function getKpi18Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi18($_POST)); die;
    }
    public function getKpi19Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi19($_POST)); die;
    }
    public function getKpi20Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi20($_POST)); die;
    }
    public function getKpi21Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi21($_POST)); die;
    }
    public function getKpi22Action() {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->kpi22($_POST)); die;
    }
    
    public function getKpiscriptAction() {                              //  $_get contain  name of script in t_script   
    
   
        $sc = new Mainsim_Model_Script();
        
        $namescript=$_GET["script"];
        
        $params=$_POST;
        $params["script"]=$namescript;
 
        echo json_encode( $sc->execute($params) ); die;
    }
    
    public function getWoByStructureAction() 
    {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->getgetWoByStructure($_POST)); die;
    }
    
    public function getWoByPriorityAction() 
    {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->getgetWoByPriority($_POST)); die;
    }
    
    public function getWoByLevelAction() 
    {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->getgetWoByLevel($_POST)); die;
    }
    
    public function getWoByCostAction() 
    {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->getgetWoByCost($_POST)); die;
    }
    
    public function getWoByStatusAction() 
    {
        $sc = new Mainsim_Model_StartCenter();
        echo json_encode($sc->getgetWoByCost($_POST)); die;
    }
    
    public function bullettinBoardAction() 
    {
        $sc = new Mainsim_Model_StartCenter();
        $userInfo = Zend_Auth::getInstance()->getIdentity();
        
        $traductor = new Mainsim_Model_Translate();
       // $traduttore = $traductor->getLangNameFromCode($userInfo['f_language']);
        //$traductor->setLang($traduttore);
        $posted_by = $traductor->_("Posted by");
        $at = $traductor->_("at");
        $on = $traductor->_("on");
        
        $user = $userInfo->fc_usr_firstname . " " . $userInfo->fc_usr_lastname;
        
        $format_date = 'F d, Y';
        if($userInfo->f_language == 2) $format_date = 'd/m/Y';
        
        $this->view->posted_by = $posted_by;
        $this->view->at = $at;
        $this->view->on = $on;
        $this->view->user = $user;
        $this->view->format_date = $format_date;
        $this->view->bulletins = $sc->getBulletins();
    }
    
    public function locationsAction()
    {
        
    }
    
    public function indicatorsAction() 
    {
      
    }
    
    public function getWoByTypeAction()
    {
        
    }
    
    // trends KPI
    public function openWoAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trend('open-wo', $params));
    }
    
    public function workRequestAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trend('work-request', $params));
    }
    
    public function periodicAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trend('periodic', $params));
    }
    
    public function emergencyAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trend('emergency', $params));
    }
    
    public function priorityAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trend('priority', $params));
    }
    
    /*Hpg custom - Galanti - 03/05/2016*/
    public function openWoHpgAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trendhpg('open-wohpg', $params));
    }
    
    public function closeWoHpgAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trendhpg('closed-hpg', $params));
    }
    
    public function collaudoWoHpgAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trendhpg('collaudo-hpg', $params));
    }
    
    public function attesaWoHpgAction(){
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        
        if(isset($_POST['params'])){
            $params = json_decode($_POST['params']);
        }
        $obj = new Mainsim_Model_Kpi();
        echo json_encode($obj->trendhpg('attesa-hpg', $params));
    }
}

