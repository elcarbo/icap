<?php

class AutocompleteController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    /**
     * Deprecated
     */
    public function _getListAction()
    {        
        if(!empty($_POST)) {
            $field = $_POST['field'];
            $table = $_POST['table'];
            $condition = $_POST['cond'];
            $order = $_POST['order'];
            $groupby = $_POST['groupby'];
            $having = $_POST['having'];
            $list = new Mainsim_Model_Autocomplete();
            echo json_encode($list->getList($field, $table, $condition, $order, $groupby, $having));
        }        
    }
    
    public function getListAction()
    {        
        /*
        if(!empty($_POST)) {
            $field = $_POST['field'];
            $table = $_POST['table'];
            $value = $_POST['value'];
            $list = new Mainsim_Model_Autocomplete();
            echo json_encode($list->getList($field, $table, $value));
        }*/
        $list = new Mainsim_Model_Autocomplete();
        $list->getList($_POST['script'], $_POST['value']);
    }
    
    public function testAction()
    {        
        if(isset($_POST['value'])) $value = $_POST["value"]; else $value = "";
        $list = new Mainsim_Model_Autocomplete();
        echo json_encode($list->getList($value));
    }

    public function pickListAction(){

        $params = json_decode($_POST['params']);

        $treegridObj = new Mainsim_Model_Treegrid();

        if(isset($_POST['params']['Selector'])){
            $selectorFilterIds = array();
            foreach($_POST['params']['Selector'] as $key => $value){
                foreach($value as $key2 => $value2){
                    if($key2 == 's'){
                        $selectorFilterIds = array_merge($selectorFilterIds, $value2);
                    }
                } 
            }
        }
        $p['_fromPicklist'] = $params->fromPicklist;
        $p['_category'] = $treegridObj->getCategoriesByTypeId($params->Tab[1], $params->Tab[0]);                
        $p['_rtree'] = $params->rtree;
        $p['_selectors'] = is_array($selectorFilterIds) ? $selectorFilterIds : null;
        $p['_filters']['_and'] = $params->Ands;
        $p['_filters']['_order'] = $params->Sort;
        $p['_filters']['_search'] = $params->Search;
        $p['_filters']['_like'] = $params->Like;
        $p['_filters']['_filter'] = $params->Filter;
        $p['_hierarchy'] = $params->Hierarchy;
        $p['_self'] = $params->self;
        $p['_module'] = $params->module_name;
        $p['_ignoreSelector'] = $params->ignoreSelector;
        $p['_parent'] = $params->Prnt;
        
        $codes = $treegridObj->getCodes($p);
        
        if(count($codes) > 0){
            $autocompleteObj = new Mainsim_Model_Autocomplete();
            $data = $autocompleteObj->getPicklistData($codes, $params->picklistName, $params->picklistType, $params->Tab[0]);
        }
        else{
            $data = [];
        }
           
        $response = [
            'data' => $data,
            'picklistName' => $params->picklistName
        ];
        
        print(json_encode($response)); 
        die();

        //$result = $tree->formatData($fields, count($codes), $params);
    }
     
}