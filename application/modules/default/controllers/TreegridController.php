<?php

class TreegridController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        ////Zend_session::writeClose();
    }
       
    public function jaxReadChildAction()
    {        
        if (!isset($_POST["param"])) { exit("error0"); }
        $tree = new Mainsim_Model_Treegrid();
        
        if($tree->useNewSelectors()){
            $params=json_decode($_POST["param"]);
            // check if exists selector filter from request
            if(isset($params->Selector)){
                $selectorFilterIds = array();
                foreach($params->Selector as $key => $value){
                    foreach($value as $key2 => $value2){
                        if($key2 == 's'){
                            $selectorFilterIds = array_merge($selectorFilterIds, $value2);
                        }
                    } 
                }
            }
                
            try {
                $p['_fromPicklist'] = $params->fromPicklist;
                $p['_category'] = $tree->getCategoriesByTypeId($params->Tab[1], $params->Tab[0]);                
                //$p['_category'] = $tree->getCategoriesByModule($params->module_name, $params->Tab[0]);
                $p['_rtree'] = $params->rtree;
                $p['_selectors'] = is_array($selectorFilterIds) ? $selectorFilterIds : null;
                $p['_filters']['_and'] = $params->Ands;
                $p['_filters']['_order'] = $params->Sort;
                $p['_filters']['_search'] = $params->Search;
                $p['_filters']['_like'] = $params->Like;
                $p['_filters']['_filter'] = $params->Filter;
                $p['_hierarchy'] = $params->Hierarchy;
                $p['_self'] = $params->self;
                $p['_module'] = $params->module_name;
                $p['_ignoreSelector'] = $params->ignoreSelector;
//                $p['_parent'] = $params->Prnt;
                // If print negative, usually -1, client needs to know the count of objects to draw the treegrid
                if($params->Prnt < 0){ 
                    //$p['_parent'] = 0;
                    $p['_parent'] = $params->Prnt;
                   // $tcodes = new Zend_Session_Namespace('table_codes');
                    $codes = $tree->getCodes($p);
                    //$tcodes->data = $codes;
                    // format data for treegrid
                    $result = $tree->formatData($fields, count($codes), $params);
                } // Else collect data to be sent
                else { 
                    $p['_limit'] = array($params->Limit[0], $params->Limit[1]); // Limit used to send only data that would be displayed in view
                    $p['_parent'] = $params->Prnt > 0 ? $params->Prnt : -1; // Level of hierarchy to show

                    $codes = $tree->getCodes($p);
                 #   $codes = $tcodes->data;
                    //unset($tcodes->data); 
              
                    $p['_parent'] = $params->Prnt;
                    // get fields showed in treegrid

                    $fields = $tree->getFields($params->module_name, $params->Tab[0]);
                         
                    //print_r($fields); die();      
                    // get data
                    $data = $tree->getData($codes, $fields, $p,$params->Tab[0]);
                    for($k = 0; $k < count($data); $k++){
                        $data[$k] = array_map(utf8_encode, $data[$k]);
                    }
                    // format data for treegrid
                    $result = $tree->formatData($fields, $data, $params);

                }
            }catch(Exception $e) { //If something went wrong             
                $result['message'] = utf8_decode($e->getMessage()); 
            } // Send back JSON with data
            //echo json_encode($result, JSON_NUMERIC_CHECK);die;
            echo json_encode($result);die;
        }
        else{ // Not using NewSelectors               
            try {
                $in=json_decode($_POST["param"]);
                $result = $tree->jax_read_child($in);
            }catch(Exception $e) {
                $result['message'] = utf8_decode($e->getMessage());
            }
            echo json_encode($result);die;
        }
    }    
    
    public function jaxReadRootAction()
    {        
        try{
            $tree = new Mainsim_Model_Treegrid();       
            if(!isset($_POST["param"])) exit("error0"); 
            $in=json_decode($_POST["param"]);
            $result = $tree->jax_read_root($in);
            echo json_encode($result);die;
        }
        catch(Exception $e){
            
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') === 1){
                print($e->getMessage());
            }
            else{
                die("ERROR (debug mode off)");
            }
        }
    }
    
    public function jaxReadRootChkHistoryAction()
    {
        $tree = new Mainsim_Model_Treegrid();       
        if(!isset($_POST["param"])) exit("error0"); 
        
        $in=json_decode($_POST["param"]);        
        $result = $tree->jax_read_root_chk_history($in);
        echo json_encode($result);die;    
    }
    
    public function jaxReadRootChkAction()
    {
        try{
            $tree = new Mainsim_Model_Treegrid();       
            if(!isset($_POST["param"])) exit("error0"); 
            //Zend_session::writeClose();
            $in=json_decode($_POST["param"]);        
            $result = $tree->jax_read_root_chk($in);
            echo json_encode($result);die;    
        }
        catch(Exception $e){
            
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') === 1){
                print($e->getMessage());
            }
            else{
                die("ERROR (debug mode off)");
            }
        }
    }
    
    public function jaxReadCheckAction()
    {        
        $tree = new Mainsim_Model_Treegrid();
        if(!isset($_POST["param"])) exit("error0"); 
        $in=json_decode($_POST["param"]);
        $result = $tree->jax_read_check($in);
        echo json_encode($result);die;    
    }
    
    //-----------------------------------
    public function jaxReadRootChkPairAction()
    {        
        try{
            $tree = new Mainsim_Model_Treegrid();       
            if(!isset($_POST["param"])) exit("error0"); 

            $in=json_decode($_POST["param"]);

            if($in->Tab[0] == 't_systems' && $in->Tab[1] == -1) {
                $result = $tree->jax_read_root_chk_bm($in);
            }
            else {
                $result = $tree->jax_read_root_chk_pair($in);
            }
            echo json_encode($result);die; 
        }
        catch(Exception $e){
             
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') === 1){
                print($e->getMessage());
            }
            else{
                die("ERROR (debug mode off)");
            }
        }
    }
    
    public function jaxReadChkPairAction()
    {        
        $tree = new Mainsim_Model_Treegrid();
        if(!isset($_POST["param"])) exit("error0"); 
        $in=json_decode($_POST["param"]);
        $result = $tree->jax_read_chk_pair($in);
        echo json_encode($result);die;  
        
    }
    
    /**
     * @deprecated since version 3.0.5.1
     */
    public function jaxReadChkParentAction()
    {        
        $tree = new Mainsim_Model_Treegrid();
        if(!isset($_POST["param"])) exit("error0"); 
        $in=json_decode($_POST["param"]);
        $result = $tree->jax_read_chk_parent($in);
        echo json_encode($result);die;    
    }
    
    //------------------------------------------
    
    
    public function jaxLockAction()
    {
        $tree = new Mainsim_Model_Treegrid();
        $par=json_decode($_POST["param"]);
        $result = $tree->jax_lock($par);
        echo json_encode($result);die;
    }
    
    public function jaxLockOtherUserAction()
    {
        $result = array();        
        if(isset($_POST['param']) && !empty($_POST['param'])) {
            $par=json_decode($_POST["param"]);
            $tree = new Mainsim_Model_Treegrid();
            $result = $tree->jax_lockotheruser($par);            
        }
        echo json_encode($result);die;
    }
    
    /**
     *@ deprecated new action = inspection/save
     */
    public function __saveBookmarkAction() {
        if(isset($_POST["f_module_name"]) && isset($_POST["f_properties"])) {
            $data = array("f_module_name" => $_POST["f_module_name"],
                "f_name" => "bookmark",
                "f_user_id" => Zend_Auth::getInstance()->getIdentity()->f_id,
                "f_type" => "bookmark",
                "f_properties" => $_POST["f_properties"]
            );
            $tree = new Mainsim_Model_Treegrid();
            echo json_encode($tree->saveBookmark($data));            
        } 
    }
    
    public function jaxTimebarAction()
    {
        $tree = new Mainsim_Model_Treegrid();
        $result = array(array(),array());        
        if(isset($_POST['fcode']) && !empty($_POST['fcode']) && ctype_digit($_POST['fcode'])) {
            // labors timeline
            if($_POST['DataMode'] == 2){
                $result = $tree->jax_timebar_labors($_POST['fcode'], $_POST['year']);
                if(!$result){
                    $result = array(array(),array());
                }
            }
            // other timeline
            else{
                $par = $_POST["fcode"];
                $data_mode = 0;            
                if(isset($_POST['Module']) && strpos($_POST['Module'],"wo") !== false) {
                    $data_mode = 1;
                }
                $rsv = strpos($_POST['Module'],'rsv') !== false?true:false;
                $result = $tree->jax_timebar($par,$data_mode,$rsv);   
            }
        }
        else {
            $result = array("fcode"=>$_POST["fcode"], "data"=>array(array(),array()));
        }
        echo json_encode($result);die;
    }
    
    public function jaxTimebarChangeAction()
    {
        if(!isset($_POST["fid"])) exit("no param");
        
        $tree = new Mainsim_Model_Treegrid();
        $par = $_POST;
        $tree->editPeriodic($par);
        echo "ok";
    }
    
    public function reverseAjaxAction()
    {        
        //Zend_session::writeClose();
        $tree = new Mainsim_Model_Treegrid();    
        $res = $tree->getReverseAjax();
        echo json_encode($res);
    }
    
    /**
     * Dato un f_code restituisco le proprietà
     */
    public function getCodePropertiesAction()
    {        
        try{
            //Zend_session::writeClose();
            if(empty($_POST)) {echo json_encode(array());die;}

            $params = $_POST;
            $tree = new Mainsim_Model_Treegrid();
            $result = $tree->getCodeProperties($params);        
            echo Mainsim_Model_Utilities::chg(json_encode($result));
        }
        catch(Exception $e){
            
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') === 1){
                echo 'Something went wrong while editing your wares '.$e->getMessage();
            }
            else{
                echo 'Something went wrong while editing your wares (debug mode off)';
            }
        }
    }
    
    public function unlockRecordsAction()
    {
        //Zend_session::writeClose();
        if(empty($_POST)) {echo json_encode(array());die;}                
        if(isset($_POST['f_codes']) && is_array($_POST['f_codes'])) {
            $param = $_POST['f_codes'];
        }
        elseif(isset($_POST['f_module_name']) && !empty($_POST['f_module_name'])) {
            $param = $_POST['f_module_name'];
        }
        else {
            return;
        }
        
        $tree = new Mainsim_Model_Treegrid();
        $error = array();
        try {
            $tree->unlockRecords($param);        
        }catch(Exception $e){
            $error['message'] = $e->getMessage();
        }
        echo json_encode($error);
    }

    public function setRequestedFieldsAction() {
        $tree = new Mainsim_Model_Treegrid();  
 
        $t_custom_fields = (!empty($_POST['t_custom_fields']))?1:null;
        $res = $tree->setRequestedFields($_POST['f_code'],$t_custom_fields);
//        $res['f_title'] = utf8_encode($res['f_title']);
//        $res['f_description'] = utf8_encode($res['f_description']);
        print json_encode($res);
    }
}
