<?php

class CalendarController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }   
        
    public function calendarOptionsAction(){
        
        $objCal = new Mainsim_Model_Calendar();
        $res = $objCal->getCalendarOptions();
        $res['mode'] = $_POST['mode'];
        echo json_encode($res);
        die();
    }
    
    public function calendarWorkordersAction(){
        $objCal = new Mainsim_Model_Calendar();
        $res = $objCal->getCalendarWorkorders(json_decode($_POST['param'], true));
        echo json_encode(array("data"=>$res));
        die();
    }
    
    public function calendarReverseAction(){
        $objCal = new Mainsim_Model_Calendar();
        print($objCal->checkReverseAjax(json_decode($_POST['param'], true)));
        die();
    }
    
    public function calendarWorkorderAction(){
        $params = json_decode($_POST['param'], true);
        $objCal = new Mainsim_Model_Calendar();
        $res = $objCal->getCalendarWorkorder($params);
        echo json_encode(array( "data"=>$res, "fcode"=> $params['f_code']));
    }
    
    public function reservationWorkordersAction(){
        $objRes = new Mainsim_Model_Reservation();
        $res = $objRes->getReservationWorkorder(json_decode($_POST['param'], true));
        echo json_encode(array("data"=>$res));
        die();
    }
    
    public function reservationEditAction(){
        $objRes = new Mainsim_Model_Reservation();
        $res = $objRes->getReservationEdit($_POST);
        echo json_encode($res);
        die();
    }
 
    public function reservationSaveAction(){
        $postData = json_decode($_POST['val']);
        $objReservation = new Mainsim_Model_Reservation();
        if($postData->f_reservation_id == 0){
            print(json_encode($objReservation->newReservation($postData)));
        }
        else{
            print(json_encode($objReservation->editReservation($postData)));
        }
        die();
    }
}