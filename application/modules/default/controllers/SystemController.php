<?php

class SystemController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }   
        
    public function editAction()
    {        
        $params = json_decode($_POST['params'],true);
                // security fix 
        if($params == null){
            die('invalid json params');
        }
        $res = array();
        $sys = new Mainsim_Model_System();
        try {
            if($params['f_phase_id'] == 3) {
                $sys->deleteUi($params['f_code']);
            }
            else {
                $res = $sys->editSystem($params);                         
            }
        }catch(Exception $e) {
            $res['message'] = 'Something went wrong while editing your system element '.$e->getMessage();
        }     
        echo json_encode($res);die;
    }
    
    /* ADDED BY ALESSANDRA 13/03/2012 */
    public function newAction()
    {
        $params = json_decode($_POST['params'],true);
        // security fix 
        if($params == null){
            die('invalid json params');
        }        
        $wares = new Mainsim_Model_System();
        $res = array();
        
        try {
            $res = $wares->newSystem($params);             
        }catch(Exception $e) {
            $res['message'] = 'Something went wrong while inserting your system element: '.$e->getMessage();
        }        
        echo json_encode($res);die;
    }
    
    /**
     * Edit batch 
     */
    public function editBatchAction()
    {
        $params = $_POST['params'];        
        $wares = new Mainsim_Model_MainsimObject();        
        $res = array();
        try {
            $res = $wares->editBatch('t_systems',$params);              
        }catch (Exception $e) {
            $res['message'] = 'Something went wrong while editing your wares '.$e->getMessage();
        }
        echo json_encode($res);die;
    }
    
    /**
     * Action to Clone System elements
     * @deprecated since version 3.0.5.1
     */    
    public function cloneSystemAction_deprecated()
    {
        $f_code = explode(',',$_POST['f_code']);
        $tot = count($f_code);
        $clone_type = $_POST['clone_type'];
        $module_name = $_POST['f_module_name'];
        $wares = new Mainsim_Model_System();
        $message = array();
        $response = array('f_count'=>$tot);
        try {
            $message = $wares->cloneSystem($f_code,$clone_type,$module_name);
        }
        catch(Exception $e) {
            $message['message'] = "Something went wrong : ".utf8_decode($e->getMessage());
        }
        
        //callback
        if(isset($message['message'])) {
            echo json_encode($message);
        }
        else {
            $response['f_changed'] = $message['f_changed'];
            echo json_encode($response);
        }        
    }
    
    public function cloneSystemAction() {
        try {
            if(!isset($_POST["f_module_name"])) { echo json_encode(array("message" => "Missing module name")); die; }
            if(!isset($_POST["f_code"])) { echo json_encode(array("message" => "No elements to clone")); die; }
            if(!isset($_POST["clone_type"])) $_POST["clone_type"] = 0;
            /*$sel = new Mainsim_Model_System();
            echo json_encode($sel->cloneSystem($_POST["clone_type"], $_POST["f_code"], $_POST["f_module_name"]));*/
            $obj = new Mainsim_Model_MainsimObject();        
            $obj->type = 't_systems';
            $res = $obj->cloneObjects($_POST["f_code"],$_POST["clone_type"], $_POST["f_module_name"]);
            unset($res['codes_cloned']);
        }
        catch(Exception $e) {
            $res['message'] = "Something went wrong : ".Mainsim_Model_Utilities::clearChars($e->getMessage());
        }
        echo json_encode($res);
    }
    
    public function changePhaseBatchAction() {
        $system = new Mainsim_Model_System();
        echo json_encode($system->changePhaseBatch($_POST["f_codes"], $_POST["f_wf_id"], $_POST["f_exit"], $_POST["f_module_name"]));
    }
    
    /**
     * Return list of sheets to import 
     */
    public function getSheetsAction()
    {
        $sys = new Mainsim_Model_System();
        $sheets = $sys->getSheets();
        echo json_encode($sheets);
        die;
    }
    
//    public function getBookmarkAction() {
//        $sys = new Mainsim_Model_System();
//        var_dump($sys->getBookmark("mdl_wo_tg", "view", "aasa"));
//    }
}