<?php 

class ExportController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
    }

	// return the progress status HTML
	public function startAction(){
            $_SESSION['exportFilename'] = md5(time() . session_id());
            $_SESSION['exportParams'] = json_decode($_POST['params']);
            // security fix
            if($_SESSION['exportParams'] == null){
                die('invalid json params');
            }
            $_SESSION['exportFormat'] = $_SESSION['exportParams']->format;
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer('start');
	}
	
	// create in background export file
	public function createAction(){  
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            
            $_SESSION['exportFilename'] = md5(time() . session_id());
            ////Zend_session::writeClose();
            if($_SESSION['exportFormat'] == 'php'){
                $_SESSION['exportFormat'] = 'xlsx';
                $custom_php = true;
            }
            
            ob_end_clean();
            header("Connection: close");
            ignore_user_abort();
            ob_start();
            header("Content-Length: 0");
            ob_end_flush();
            flush();
            session_write_close();
           // echo "*";
            $objExcel = new Mainsim_Model_Exporter();
            if($custom_php){
                $objExcel->excelScript($_SESSION['exportFilename'], $_SESSION['exportParams'], 'xlsx');
            }
            else if($_SESSION['exportFormat'] == 'xls' || $_SESSION['exportFormat'] == 'xlsx'){ 
                $objExcel->excel2($_SESSION['exportFilename'], $_SESSION['exportParams'], $_SESSION['exportFormat']);
            }
            else if($_SESSION['exportFormat'] == 'tpl'){
                $objExcel->template();
            }
            // custom template
            else if(isset($_SESSION['exportCustomTemplate'])){
                $objExcel->custom($_SESSION['exportFilename'], $_SESSION['exportCustomTemplate']);
            }
            else{
                echo "export format not specified";
            }
	}
	
    // get the progress of export
    public function progressAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $objExcel = new Mainsim_Model_Exporter();
        print($objExcel->getProgress($_SESSION['exportFilename']));
    }
	
	// download the export file and remove file from server
    public function downloadAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment; filename=export.". $_SESSION['exportFormat']);
        ob_clean();
        flush();
        readfile(APPLICATION_PATH . '/../' . $_GET['file']);
        $objExcel = new Mainsim_Model_Exporter();
        $objExcel->delete($_SESSION['exportFilename'], $_SESSION['exportFormat']);
        exit;
    }
    
    //**************************************
    // exporter tool 
    //**************************************
    public function startTemplateAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer('start-template');
        
        if($_POST['template']){
            $_SESSION['template'] = Zend_Registry::get('attachmentsFolder') . '/temp/' . $_POST['template'] . '.xml';
        }
        else{
            unset($_SESSION['template']);
        }
        $_SESSION['template_code'] = $_POST['f_code'];
    }
    
    
    public function initTemplateAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $obj = new Mainsim_Model_Exporter(APPLICATION_PATH . '/../export/temp/', APPLICATION_PATH . '/../export/');  
        $dirTemp = $obj->createTempDirTool();
  
        if($_GET['customTemplate']){
            $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
            $_SESSION['customTemplate'] = true;
            
            // get id parameter
            if(isset($_GET['f_code'])){
                $_SESSION['customCode'] = $_GET['f_code'];
            }
            
            // custom export for multiproject
            if($config->multiDatabase == 'On'){
                $aux = Zend_Auth::getInstance()->getIdentity();
                if(file_exists(APPLICATION_PATH . '/../export/custom/' . $aux->project_name . '/' . $_GET['customTemplate'] . ".xml")){
                    $_SESSION['template'] = APPLICATION_PATH . '/../export/custom/' . $aux->project_name . '/' . $_GET['customTemplate'] . ".xml";
                }
            }
            // custom export for single installation
            else{
                if(file_exists($config->exportFolder . '/' . $_GET['customTemplate'] . ".xml")){
                    $_SESSION['template'] = $config->exportFolder . '/' . $_GET['customTemplate'] . ".xml";
                }
            }
        }else if ($_GET['defaultTemplate']) {
            $_SESSION['customTemplate'] = true;
            
            // get id parameter
            if(isset($_GET['f_code'])){
                $_SESSION['customCode'] = $_GET['f_code'];
            }
            
            // custom export
          
                $aux = Zend_Auth::getInstance()->getIdentity();
                if(file_exists(APPLICATION_PATH . '/../export/custom/default/' . $_GET['defaultTemplate'] . ".xml")){
                    $_SESSION['template'] = APPLICATION_PATH . '/../export/custom/default/' . $_GET['defaultTemplate'] . ".xml";
                }
            
            // custom export
            else{
                if(file_exists($config->exportFolder . '/' . $_GET['defaultTemplate'] . ".xml")){
                    $_SESSION['template'] = $config->exportFolder . '/' . $_GET['defaultTemplate'] . ".xml";
                }
            }
            
        }

        // create excel config xml
        $obj->createXMLConfigTool($dirTemp, $_SESSION['template'] ? $_SESSION['template'] : null);
        
        print($dirTemp);
    }
    
    public function getSheetsTemplateAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $obj = new Mainsim_Model_Exporter(APPLICATION_PATH . '/../export/temp/', APPLICATION_PATH . '/../export/');
        print(json_encode($obj->getSheetsTool($_SESSION['template'] ? $_SESSION['template'] : null)));
    }
    
    public function createSheetTemplateAction(){
        //Zend_session::writeClose();
        /*
        ob_end_clean();
        header("Connection: close");
        ignore_user_abort();
        ob_start();
        header("Content-Length: 0");
        ob_end_flush();
        flush();
        session_write_close();*/
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //shell_exec('php ' . APPLICATION_PATH . '/../export/createSheetTemplate.php ' . $_GET['filename'] . ' ' . str_replace(" ", "£$?", $_GET['sheet']) . ' ' . $_GET['sheetId']);
        $objExporter = new Mainsim_Model_Exporter(APPLICATION_PATH . '/../export/temp/', APPLICATION_PATH . '/../export/');
        $objExporter->createSheetTool(urldecode($_GET['filename']), urldecode($_GET['sheet']), $_GET['sheetId'], $_SESSION['template'], isset($_SESSION['customCode']) ? explode(",", $_SESSION['customCode']) : null);   
    }
    
    public function progressTemplateAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $sheets = json_decode($_POST['sheets']);
        $obj = new Mainsim_Model_Exporter(APPLICATION_PATH . '/../export/temp/', APPLICATION_PATH . '/../export/'); 
        print(json_encode($obj->getProgressTool($_GET['filename'], $sheets)));
    }
    
    public function finalizeExcelTemplateAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $sheets = json_decode($_POST['sheets']);
        $obj = new Mainsim_Model_Exporter(APPLICATION_PATH . '/../export/temp/', APPLICATION_PATH . '/../export/');
        print($obj->finalizeXLSXTool($_GET['filename'], $sheets));
    }
    
    // download the export file and remove file from server
    public function downloadExcelTemplateAction(){

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $obj = new Mainsim_Model_Exporter(APPLICATION_PATH . '/../export/temp/', APPLICATION_PATH . '/../export/');
        // save config file
        if(!isset($_SESSION['customTemplate'])){
            $obj->saveConfigFileTool($_SESSION['template_code'], $_SESSION['template']);
        }
        else{
            unset($_SESSION['customTemplate']);
        }
        
        // check if filename is customized
        if(isset($_SESSION['template'])){
            $filename = $obj->getFilename($_SESSION['template'], isset($_SESSION['customCode']) ? $_SESSION['customCode'] : null);
        }
        unset($_SESSION['customCode']);
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment; filename=" . ($filename != null ? $filename : "export") . ".xlsx");
        ob_clean();
        flush();
        $file = fopen($_GET['filename'],"rb"); 
        if($file){ 
            while(!feof($file)) { 
                print(fread($file, 1024*8)); 
                flush(); 
                if(connection_status()!=0) { 
                    fclose($file); 
                    break; 
                } 
            } 
            fclose($file); 
        }
        $obj->deleteTpl($_GET['filename']);
        exit;
    }
    
    public function testAction(){
        
    }
    
    
    
}
