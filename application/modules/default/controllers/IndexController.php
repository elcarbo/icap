<?php

class IndexController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->setLayout('login');
        //$this->_helper->viewRenderer->setNoRender(true);
    }

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
		$objUtils = new Mainsim_Model_Utilities();
		$adfsConf = json_decode($objUtils->getSettings('ADFS'));
		if(Zend_Auth::getInstance()->hasIdentity()) {
			if(Zend_Auth::getInstance()->getIdentity()->project_name == PROJECT_NAME)  {          
				$this->_redirect("mainpage");
			}
        }
		
		// check if adfs is enabled
		if($adfsConf != null && $adfsConf->enabled == 1){
			$this->_redirect("login/adfs");
		}
		else{
			session_unset();
                        $objUtils = new Mainsim_Model_Utilities();
			//$registration = new Mainsim_Model_Registration();
			//$this->view->registration = $registration->get_link();
                        //
                        // check registration enabled
                        $this->view->registration = $objUtils->getSettings('REGISTRATION_LINK');
                        
			// check password recovery enabled
			$this->view->forgot_password = $objUtils->getSettings('FORGOTPASSWORD_LINK');
		}

    }
    
    public function mainpageAction() 
    {
        
    }

    public function maintenanceAction() {

    }

}

