<?php

class MobileSyncController extends Zend_Controller_Action
{

    public function init()
    {
        Mainsim_Model_MobileUtilities::checkSession($_POST);
        //Zend_session::writeClose();
        //$userData = Zend_Auth::getInstance()->getIdentity();
        
    }
    
    public function checkDbStructureAction(){
        
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        $objSync = new Mainsim_Model_MobileSync();
        // first call --> return db structure
        if($_POST['params']['dbLoad'] == 1){
            print(str_replace(",null", ",\"\"", json_encode($objSync->getDbStructure($_POST['sessionId'], $_POST['params']['langwords']))));
        }
        else{
            print(json_encode($objSync->createResponseObject(1, 'DB_NOT_CHANGED')));
        }
        die();
    }
    
    public function syncAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $userData = ZEND_AUTH::getInstance()->getIdentity();
        // sync user
        if($_POST['userAdmin'] != 1){
            $objSync = new Mainsim_Model_MobileSync();
            $params['type_id'] = $userData['workorder_types'];
            $params['group_phases'] = '1,2,3,4,5,6';
            $params['fetch'] = Zend_Db::FETCH_NUM;
            $params['wares'] = true;
            $json = json_encode($objSync->sync($params));
        }
        // sync for admin
        else{
            $objSync = new Mainsim_Model_MobileSync();
            $params['type_id'] = $userData['workorder_types'];
            $params['group_phases'] = '1,2,3,4,5,6';
            $params['code_list'] = $_POST['params']['code_list'];
            $params['fetch'] = Zend_Db::FETCH_NUM;
            $params['wares'] = true;
            $json = json_encode($objSync->syncAdmin($params));
        }
        print(str_replace(",null", ",\"\"", $json));
        die();
    }
    
    public function syncAddAction(){
        $userData = ZEND_AUTH::getInstance()->getIdentity();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        // sync user
        if($_POST['userAdmin'] != 1){
            $objSync = new Mainsim_Model_MobileSync();
            $params['type_id'] = $userData['workorder_types'];
            $params['group_phases'] = '1,2,3,4,5,6';
            $params['fetch'] = Zend_Db::FETCH_NUM;
            $params['wares'] = true;
            $params['in'] = $_POST['params']['code_list'];
            $params['sessionId'] = $_POST['params']['sessionId'];
            $json = json_encode($objSync->syncAdd($params));
        }
        // sync for admin
        else{
            
        }
        print(str_replace(",null", ",\"\"", $json));
        die();
    }

    public function pollingAction(){
        $userData = ZEND_AUTH::getInstance()->getIdentity();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        if($_POST['userAdmin'] != 1){
            try{
                $objWo = new Mainsim_Model_MobileWorkorders();
                $params['noLocked'] = true;
                $params['userType'] = 'normal';
                $res = $objWo->getUserWorkordersCodes($params);
                $resultParams['counter'] = count($res);
                //print_r($res);
                print(json_encode($objWo->createResponseObject(1, 'POLLING_DONE', $_POST['sessionId'], $resultParams)));
            }
            catch(Exception $e){
                $resultParams['error'] = $e->getMessage();
                print(json_encode($objWo->createResponseObject(1, 'POLLING_ERROR', $_POST['sessionId'], $resultParams)));
            }
        }
        // 
        else{
            try{
                $objWo = new Mainsim_Model_MobileWorkorders();
                $params['notLocked'] = true;
                $resultParams['counter'] = count($objWo->getUserWorkordersCodes($params));
                print(json_encode($objWo->createResponseObject(1, 'POLLING_DONE', $_POST['sessionId'], $resultParams)));
            }
            catch(Exception $e){
                $resultParams['error'] = $e->getMessage();
                print(json_encode($objWo->createResponseObject(1, 'POLLING_ERROR', $_POST['sessionId'], $resultParams)));
            }
        }
    }
    
    public function testAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $objSync = new Mainsim_Model_MobileSync();
        
        $uiModules = array(
            'mdl_wo_tg' => array(
                'type' => 'WORKORDERS'
            ),
            'mdl_asset_tg' => array(
                'type' => 'WARES'
            ),
            'mdl_wo_task_tg' => array(
                'type' => 'TASKS'
            )
        );
        $res = $objSync->getMobileFields('t_pair_cross', $uiModules);
    }
    
}
?>
