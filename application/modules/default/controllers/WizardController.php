<?php

 class WizardController extends Zend_Controller_Action {

     public function preDispatch() {
         parent::preDispatch();
         if (!Zend_Auth::getInstance()->hasIdentity())
             exit;
         $this->_helper->layout()->disableLayout();
         $this->_helper->viewRenderer->setNoRender(true);
         //Zend_session::writeClose();
     }

     public function wizardDataAction() {
         $wiz = new Mainsim_Model_Wizard();
         $risp = $wiz->getWizard();
         echo json_encode($risp);
         exit;
     }

     public function exitAction() {
         $wiz = new Mainsim_Model_Wizard();
     }

     public function readWizardAction() {
         header("Content-type: text/html; charset=utf-8");
         $wiz = new Mainsim_Model_Wizard();
         $res = $wiz->readWizard($_POST['version']);
         echo json_encode($res);
         die;
     }

     public function saveWizardAction() {
         $wiz = new Mainsim_Model_Wizard();
         $params = json_decode($_POST['params'], true);
         try {
             $wiz->saveWizard($params["data"], $params["colors"]);
         } catch (Exception $e) {
             echo 'Error : ' . $e->getMessage();
             die;
         }
         echo 'ok';
         die;
     }

     public function waresWizardAction() {
         /*
           $wiz = new Mainsim_Model_Wizard();
           $res = array();
           if(!empty($_POST["f_ware_start"])) {
           $res = $wiz->waresWizard($_POST["f_ware_start"],$_POST["pdr"]);
           }
           $res = array_values($res);
           echo json_encode($res);
           die; */
         $wiz = new Mainsim_Model_Wizard();
         $cache = $wiz->cacheWizard();

         // check if exists a wares static file 
         $aux = explode(".", $_SERVER['HTTP_HOST']);
         $folder_name=(PROJECT_NAME!='')?PROJECT_NAME:$aux[0];
         $waresFile = implode("_", $_POST);
         
         // file exists -> load wares from file
         if ($cache && file_exists(APPLICATION_PATH . "/../new_wizard/wares/" . $folder_name . "/" . $waresFile . ".txt")) {
             print(file_get_contents(APPLICATION_PATH . "/../new_wizard/wares/" . $folder_name . "/" . $waresFile . ".txt"));
             die();
         }
         // file not exists -> load wares from database
         else {
             
             try{
                //CON getCodes si allinea il comportamento alla treegrid 14 marzo 2017 Baldini
                $tree = new Mainsim_Model_Treegrid();
                if ($tree->useNewSelectors()) {

                     /* add wizard asset phase filter  if 'WIZARD_PHASES' setting exists */
                       $objUtilities = new Mainsim_Model_Utilities();
                       $res = $objUtilities->getSettings('WIZARD_ASSET_PHASES');
                       if($res){
                           $like_filter = (object)[
                               'Text' => base64_encode(str_replace(",", "||", $res)),
                               'Fields' => ['f_phase_id']
                          ];
                       }

                    $p = array('_category' => array(0 => 'ASSET',), '_rtree' => 'root', '_selectors' => array(), '_filters' => array('_and' => array(), '_order' => array('f_code'), '_search' => array(), '_like' => isset($like_filter) ? $like_filter : array(), '_filter' => array(),), '_hierarchy' => 0, '_module' => 'mdl_asset_tg', '_parent' => -1, '_wizard' => true);
                    $wareList = $tree->getCodes($p);
                    //var_dump($wareList);die;
                } else {
                    // get users selectors
                    $userSelectors = $wiz->getUserSelectors();
                    if ($userSelectors) {
                        // get wares codes by selectors
                        $wareList = $wiz->getWaresBySelectors($userSelectors);
                    } else {
                        $wareList = $wiz->getWaresBySelectors(array());
                    }
                }

                if (strpos($_POST["f_ware_start"], "_SUB_") > 0) {

                    // get selectors and root ware
                    $aux = explode("_SUB_", $_POST["f_ware_start"]);
                    $waresToAvoid = array();
                    $wiz->getWaresListMinus($aux, $result, $visited, implode(",", $waresToAvoid), $_POST["pdr"], null);
                    $wareList = array_values(array_intersect($wareList, $visited));
                } else if (strpos($_POST["f_ware_start"], "_ADD_") > 0) {

                    // get selectors and root ware
                    $wareListPlus = $wiz->getWaresListPlus($_POST["f_ware_start"], $_POST["pdr"], $wareList);
                    $wareList = array_values(array_intersect($wareList, $wareListPlus));
                } else if (strpos($_POST["f_ware_start"], "_NHY_") > 0) {

                    $visited = array();
                    $wiz->getWaresListNotHierarchy($_POST["f_ware_start"], $visited);
                    $wareList = array_values(array_intersect($wareList, $visited));
                    $parentRelations["_pdr_"] = $wareList;
                } else {
                    $waresToAvoid = array();
                    $visited = array();
                    if ($_POST["f_ware_start"] != 'root') {
                        $wiz->getWaresListStart($_POST["f_ware_start"], $result, $visited, $waresToAvoid, $_POST["pdr"], $wareList);
                        $removeRootElements = true;
                    } else {
                        $starter_pack = $wiz->getWaresParentRelations($wareList);
                        $starter_pack = (!empty($starter_pack['root']))?$starter_pack['root']:0;
                        $wiz->getWaresListStart($starter_pack, $result, $visited, $waresToAvoid, $_POST["pdr"], $wareList);
//                        $wareList = array_values(array_intersect($wareList, $visited));
//                        $parentRelations = $wiz->getWaresParentRelations3($wareList);
                        //print_r($parentRelations); die;
                    }

                    $wareList = array_values(array_intersect($wareList, $visited));

                }
                if (!isset($parentRelations)) {
                    $parentRelations = $wiz->getWaresParentRelations3($wareList);
                    //print_r($parentRelations); die;
                    if ($removeRootElements) {
                        unset($parentRelations[0]);
                    }
                }
                //$parentRelationsOld = $wiz->getWaresParentRelations($wareList);
                $wizardData = $wiz->getWizardData($parentRelations, $_POST["pdr"], $_POST["f_ware_start"], $wareList);
                if ($cache) {
                    if (!is_dir(APPLICATION_PATH . "/../new_wizard/wares/" . $folder_name . "/")) {
                        mkdir(APPLICATION_PATH . "/../new_wizard/wares/" . $folder_name . "/");
                    }
                    file_put_contents(APPLICATION_PATH . "/../new_wizard/wares/" . $folder_name . "/" . $waresFile . ".txt", json_encode($wizardData));
                }
                print(json_encode($wizardData));
                die();
             }
             catch(Exception $e){

                // security fix
                if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') === 1 || true){
                    die('Something went wrong getting wizard wares: '.$e->getMessage());
                }
                else{
                    die('Something went wrong getting wizard wares: (debug mode off)');
                }
  
             }
         }
     }

     public function readSelectAction() {
         $wiz = new Mainsim_Model_Wizard();
         print(json_encode($wiz->readSelect()));
     }

     public function boxpageAction() {
         $wiz = new Mainsim_Model_Wizard();
         $wiz->boxPage($_GET['project']);
     }
     
     public function assetActionTemplateAction(){
         
         $this->_helper->viewRenderer->setNoRender(false);
         $wiz = new Mainsim_Model_Wizard();
        
         $this->view->div_height = $_POST['div_height'];

         switch($_POST['action']){
             case 'docs':
                 $this->view->elements = $wiz->getAssetDocs($_POST['asset_code']);
                 break;

             case 'workorders':
                 $this->view->elements = $wiz->getAssetWorkorders($_POST['asset_code']);
                 $this->view->type_icons = explode(",", $_POST['type_icons']);
                 break;

         }
         $this->_helper->viewRenderer('asset_' . $_POST['action']);
     }

     public function testAction() {

         $wiz = new Mainsim_Model_Wizard();

         // get users selectors
         $userSelectors = $wiz->getUserSelectors();
         if ($userSelectors) {
             // get wares codes by selectors
             $wareList = $wiz->getWaresBySelectors($userSelectors);
         } else {
             $wareList = null;
         }

         $parentRelations = $wiz->getWaresParentRelations($wareList);
         $wizardData = $wiz->getWizardData($parentRelations);
         print(json_encode($wizardData));
     }

 }
 