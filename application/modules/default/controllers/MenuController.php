<?php

class MenuController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    /*
    public function menuInspectionAction()
    {        
        if(!empty($_POST) && isset($_POST['f_category']) && isset($_POST['f_table'])) {
            //recupero il nome tabella ed il category code della vista.
            $category = $_POST['f_category'];
            $table = $_POST['f_table'];
            $module = $_POST['gridName'];
            $menu = new Mainsim_Model_Menu();
            $inspections = $menu->getInspections($category, $table, $module);
        }
        
    }
    */
    public function createInspectAction()
    {
        $tree = new Mainsim_Model_Menu();
        $result = $tree->createInspection($_POST);
        echo json_encode($result);die;
    }
    
    public function createExtrasAction()
    {
        try{
            $tree = new Mainsim_Model_Menu();
            $result = $tree->createExtras($_POST);
            echo json_encode($result);die;
        }
        catch(Exception $e){
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') === 1){
                echo 'Something went wrong while editing your wares '.$e->getMessage();
            }
            else{
                echo 'Something went wrong while editing your wares (debug mode off)';
            }
        }
    }
    
     public function createChgphAction()
    {
       $tree = new Mainsim_Model_Menu();
        $result = $tree->createChgph($_POST);
        echo json_encode($result);die;
    }
}