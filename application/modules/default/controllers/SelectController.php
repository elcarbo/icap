<?php

class SelectController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    public function getOptionsAction()
    {
        //Zend_session::writeClose();
        $select = new Mainsim_Model_OptionSelect();
        $select->getOptions($_POST["script"]);
        die;
    }
    
    public function getGroupsExceptionAction()
    {
        //Zend_session::writeClose();
        $excep = new Mainsim_Model_OptionSelect();
        $res = $excep->getGroupsExceptions();
        echo json_encode($res);die;
    }
    
    public function getDaysExceptionAction()
    {
        //Zend_session::writeClose();
        $excep = new Mainsim_Model_OptionSelect();
        if(!isset($_POST['f_id'])) die;
        $res = $excep->getDaysException($_POST['f_id']);
        echo json_encode($res);die;
    }
    
    public function getSelectorTypesAction()
    {
        //Zend_session::writeClose();
        $option = new Mainsim_Model_OptionSelect();        
        echo json_encode(array_merge(array(array("value" => "", "label" => "New", "selected" => true)), $option->getSelectorTypes()));
    }
    
    public function getWarehousesAction()
    {
        //Zend_session::writeClose();
        $excep = new Mainsim_Model_OptionSelect();
        if(!isset($_POST['wh_code'])){
            echo json_encode(array());
            die;
        }
        echo json_encode($excep->getWarehouses($_POST['wh_code']));
    }
    
    public function getDataByCategoryAction() {
        //Zend_session::writeClose();
        $excep = new Mainsim_Model_OptionSelect();
        if(isset($_POST['table'])) $table = $_POST['table']; else { echo json_encode(array("message" => "Category missing")); die; }
        if(isset($_POST['f_type'])) $ftype = $_POST['f_type']; else { echo json_encode(array("message" => "Type missing")); die; }        
        if(isset($_POST['cond'])) $cond = $_POST['cond']; else $cond = '';
        if(isset($_POST['field'])) $field = $_POST['field']; else $field = 'f_title';
        if(isset($_POST['orderby'])) $orderby = $_POST['orderby']; else $orderby = $field;
        if(isset($_POST['order'])) $order = $_POST['order']; else $order = 'ASC';
        $data = $excep->getDataByCategory($table, $ftype, $cond, $orderby, $order); $opts = array();
        foreach($data as $dt) $opts[] = array("label" => $dt[$field], "value" => $dt[$field], "seleected" => false);
        echo json_encode($opts);
    }
    
    public function getWoPrioritiesAction()
    {
        //Zend_session::writeClose();
        $option = new Mainsim_Model_OptionSelect();        
        echo json_encode($option->getWoPriorities());
    }
}