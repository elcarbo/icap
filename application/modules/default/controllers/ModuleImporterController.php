<?php

class ModuleImporterController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    public function uploadAction()
    {
        $module=$_GET["module"];
        $err=0; $risp="";
        $import = new Mainsim_Model_ModuleImporter();
        $res = $import->upload($_FILES,$module);
        $err = $res['err'];
        $risp = $res['risp'];        
        echo "<script> parent.".$_GET["fnc"]."('$risp',$err); </script>";
    }
    
    public function elaborateAction()
    {        
        $import = new Mainsim_Model_Moduleimporter();
        $res = $import->elaborate($_POST['param']);
        echo json_encode($res);
    }
    
   
}