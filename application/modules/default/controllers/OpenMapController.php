<?php

class OpenMapController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();        
    }
    
    public function showAction()
    {
        
    }
    
    public function getAssetsAction()
    {        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $map = new Mainsim_Model_OpenMap();
        echo json_encode($map->getAssets());
    }
    
    public function getWorkordersAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $map = new Mainsim_Model_OpenMap();
        echo json_encode($map->getWorkorders());
    }
    
    public function getWorkorderAssetsAction()
    {        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $map = new Mainsim_Model_OpenMap();
        echo json_encode($map->getWorkorderAssets($this->getRequest()->getParam('id')));
    }
    
    public function searchLocationAction()
    {        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $map = new Mainsim_Model_OpenMap();
        echo $map->searchLocation($this->getRequest()->getParam('location'));
    }
}