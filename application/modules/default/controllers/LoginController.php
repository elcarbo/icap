<?php

class LoginController extends Zend_Controller_Action
{
    private $trsl = null;
    
    public function preDispatch() {
        parent::preDispatch();        
        $this->_helper->layout()->setLayout('login');     
        $langCode = Zend_Auth::getInstance()->hasIdentity()?Zend_Auth::getInstance()->getIdentity()->f_language:1;
        $this->trsl = new Mainsim_Model_Translate('en_GB');
        $this->trsl->setLang($this->trsl->getLangNameFromCode($langCode));
    }
    
    public function indexAction(){
      $this->trsl = new Mainsim_Model_Translate();
    }
	
	public function adfsAction(){
		$objUtils = new Mainsim_Model_Utilities();
		$adfsConf = json_decode($objUtils->getSettings('ADFS'));
		$adfsConf->userProfile = (array) $adfsConf->userProfile;
		$adfsObj = new Mainsim_Model_Ldap($this->db);
		if($adfsObj->authenticateADFS($adfsConf)){
			$this->_redirect('mainpage');
		}
		else{
			$this->_helper->layout()->setLayout('login');
			$userinfo = Zend_Auth::getInstance()->getIdentity();
            Zend_Auth::getInstance()->clearIdentity();
			$this->_helper->layout()->setLayout('login');
			$this->view->message = "your user is not enabled to login in mainsim<br>click <b><a style=\"color:white\" href=\"/\">here</a></b> to retry";
			$this->render('adfs');
		}
	}
    
    public function loginAction()
    {
        // executes on submit        
        if ($_POST['usn'] != "" && $_POST['psw'] != "") {
            $login = new Mainsim_Model_Login();            
            $un = isset($_POST['usn']) ? $_POST['usn'] : "";
            $pw = isset($_POST['psw']) ? $_POST['psw'] : "";                
            $auth_res = $login->authenticate($un, $pw);
            if($auth_res === true) {
                if(isset($_POST['page_browser_support'])) {
                    $this->_redirect('page_browser_support/mainsim_wizard.php');                    
                }
                else {
                    $this->_redirect('mainpage');
                }
            } else switch ($auth_res) {
                case -3: 
                case -1: $this->view->errorMessage = "Incorrect username or password"; break;
                case 'DOUBLE_LOGIN':$this->view->errorMessage = "mainsim session already open"; break;
                // application maintenance
                case -100 :
                    $this->_redirect('index/maintenance');
                    break;
                default: $this->view->errorMessage = "Database error"; break;                                                
            }
        }
        else {
            $this->view->errorMessage = "Incorrect username or password";
        }
    }
	
	public function logoutAdfsAction(){
		$this->_helper->layout()->setLayout('login');
		$this->view->message = "Your session have been overwritten<br>click <b><a style=\"color:white\" href=\"/\">here</a></b> to login again";
		$this->render('adfs');
	}
    
    public function logoutAction()
    {    
		if(Zend_Auth::getInstance()->hasIdentity()) {

			$userinfo = Zend_Auth::getInstance()->getIdentity();
            $logout = new Mainsim_Model_Login();
            $logout->logout($userinfo->f_id);
            
            Zend_Auth::getInstance()->clearIdentity();
        }
		$objUtils = new Mainsim_Model_Utilities();
		$adfsConf = json_decode($objUtils->getSettings('ADFS'));
		if($adfsConf != null && $adfsConf->enabled == 1){
			print(json_encode(array('url' => 'login/logout-adfs')));
			die();
		}
		else{
			$this->_redirect('/');
		}
        
    }
    
    public function updateTimeAction()
    {
		// security fix (match number)
        if(!preg_match("/^[0-9]{1,}$/", $_POST['Timestamp'])){ 
            die('param Timestamp not valid');
        }
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $sc = new Mainsim_Model_StartCenter();            
        $onmaintenance = 0; $locked = array();
        $last_action = new Mainsim_Model_Login();
        if(Zend_Auth::getInstance()->hasIdentity()){
            $userinfo = Zend_Auth::getInstance()->getIdentity();            
            $locked = $last_action->getLocked();
            // logout for overwritten session
            $last_action->update($userinfo->f_id); 
            if($last_action->getOnSite() && $userinfo->f_level != -1){
				$onmaintenance = 1;
                Zend_Auth::getInstance ()->clearIdentity();            
            }
        }else {$onmaintenance = 1;}        
        echo json_encode(array(
            "online" => $last_action->getOnlineNum(),
            "bulletins" => count($sc->getBulletins()),
            "onmaintenance"=>$onmaintenance,
            "locked" => $locked
        ));
    }
    
    public function forgotPasswordAction()
    {
        $this->_helper->layout()->setLayout('external');
        $errors = array();
        $login = new Mainsim_Model_Login();
        if(!empty($_POST)) {
            $errors = $login->confirmRecovery($_POST['username'],$_POST['email']);
            if(empty($errors)) {
                $this->view->message = $this->trsl->_("An email has been sent to")." ".$_POST['email'];
            }            
        }
        $this->view->error = $errors;
    }
    
    public function recoverPasswordAction()
    {
        
        if(!isset($_GET['code']) || empty($_GET['code'])) {
            $this->redirect('mainpage');
        }
        $login = new Mainsim_Model_Login();
        $checkRec = $login->checkRecoveryCode($_GET['code']);        
        if(!$checkRec) {
            $this->redirect('mainpage');
        }        
        $this->view->code = $_GET['code'];
    }
    
    public function confirmRecoverAction()
    {
        $login = new Mainsim_Model_Login();        
        if(!empty($_POST)) {            
            $login->setNewPassword($_POST['code'],$_POST['password']);
            $this->redirect('mainpage');
        } 
        else{
            $this->redirect('mainpage');
        }        
        $this->view->code = $_POST['code'];
        $this->render('recover-password');
    }
}

