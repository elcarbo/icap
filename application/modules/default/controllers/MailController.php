<?php
//aggiungo questo commento nella speranza di poter fare un Git-commit di test in atom text editor
class MailController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }


    public function removeAttachAction()
    {
        $f_codes=explode(',',$_POST["f_code"]);
        $tot_a = count($f_codes);
        $mail = new Mainsim_Model_Mail();
        for($a = 0;$a < $tot_a;++$a) {
            $path = realpath(APPLICATION_PATH."/../attachments/temp")."/{$f_codes[$a]}";
            $mail->removeAttach($path);
        }
    }

    public function sendMailAction()
    {
        //$user = Zend_Auth::getInstance()->getIdentity();
        $params = json_decode(Mainsim_Model_Utilities::chg($_POST['params']),true);
        //Zend_Debug::dump($params);die;
        /*$user_name = $user->fc_usr_firstname.' '.$user->fc_usr_lastname;
        $user_mail = $user->fc_usr_mail;
        * Manage different ship mail
        */
        $mail = new Mainsim_Model_Mail();
        //get To
        $receivers = array();
        $receivers['To'] = explode(';',$params['To']);
        if(empty($receivers['To'])) {
            return array('message'=>'Cannot send mail without set receiver');
        }

        $object = $params['Object'];
        $body = base64_decode($params['Body']);
        $add_params = $params['add_params'];
        $receivers['Cc'] = explode(';',$params['Cc']);

        //applico check degli attchment per vedere se le dimensioni totali superano il limite
        $tot_a = count($params['Attach']);
        $total_size = 0;
        $attachments = array();
        $attachments_filename_list = array();
        $attachments_code_list = array();
        for($a = 0;$a < $tot_a;++$a) {
            $f_code_doc = $params["Attach"][$a]['f_code'];
            if($params["Attach"][$a]['type'] == 1) { //cerca nei tmp
                $attachments[$f_code_doc] = $mail->getAttachInfo($f_code_doc,"f_session_id");
                $attachments[$f_code_doc]['type'] = 1;
                $attachments_filename_list[] = $attachments[$f_code_doc]['filename'];
                $total_size += $attachments[$f_code_doc]['size'];
            }
            else { // cerca negli attachment
                $attachments[$f_code_doc] = $mail->getAttachInfo($f_code_doc);
                $attachments[$f_code_doc]['type'] = 0;
                $attachments_filename_list[] = $attachments[$f_code_doc]['filename'];
                $attachments_code_list[] = $f_code_doc;
                $total_size += $attachments[$f_code_doc]['size'];
            }
        }
        if($total_size > 5242880) { //5Mb
            return array('message'=>'Cannot send mail with attachments over 5Mb');
        }
        $f_code_wo = $add_params['f_code'];
        $user_mail = Zend_Auth::getInstance()->getIdentity()->fc_usr_mail;
        $username  = Zend_Auth::getInstance()->getIdentity()->fc_usr_firstname.' '.Zend_Auth::getInstance()->getIdentity()->fc_usr_lastname;
        if($add_params['callType'] == 'assignment' || $add_params['callType'] == 'enti-coinvolti') {
            $additional_params['from']['name'] = $username;
            $additional_params['from']['mail'] = $user_mail;
        }

        $message = $mail->sendMail($receivers,$object,$body,$attachments,$additional_params);
        if(isset($message['message'])){ echo json_encode ($message);die;}// in case of error
        $wares = new Mainsim_Model_Wares();
        for($a = 0;$a < $tot_a;++$a) {
            if($params["Attach"][$a]['type'] == 1) {
                $f_code_doc = $params["Attach"][$a]['f_code'];
                $params_doc = array(
                    'f_module_name'=>'mdl_doc_tg',
                    'f_code'=>0,
                    'f_type_id'=>5
                );

                //$params['fc_doc_attach'] = $session_id.'|'.$filename.'.'.$ext;
                $params_doc['fc_doc_attach'] = $attachments[$f_code_doc]['session_id'].'|'.$attachments[$f_code_doc]['filename'];
                $params_doc['t_selectors_6'] = array(
                        "f_code"=>array(129927),
                        "f_code_main"=> 0,
                        "f_type"=> "6",
                        "Ttable"=> "t_selectors",
                        "f_module_name"=> "mdl_doc_slc_tg"
                );
                $params_doc['f_title'] = $attachments[$f_code_doc]['filename'];
                $res_wa = $wares->newWares($params_doc);
                $attachments_code_list[] = $res_wa['f_code'];
            }
        }
        //-------------------
        $params_mail = array(
            'f_code'=>0,
            'f_type_id'=>22,
            'f_wf_id'=>9,
            'f_phase_id'=>1,
            'f_module_name'=>'mdl_mail_tg',
            'f_title'=>"Mail sent ".date('d/m/Y H:i'),
            'fc_mail_subject'=> utf8_encode($object),
            'fc_mail_body'=>utf8_encode($body),
            'fc_mail_attach'=>implode(',',$attachments_filename_list),
            'fc_mail_to'=>$params['To'],
            'fc_mail_cc'=>$params['Cc'],
            't_workorders_1,4,6,7,10,13'=>array(
                'f_code'=>array($f_code_wo),
                'f_code_main'=>0,
                "Ttable"=>'t_workorders',
                "f_module_name"=>"mdl_wo_mail_tg"
            ),
            't_wares_5'=>array(
                'f_code'=>$attachments_code_list,
                'f_code_main'=>0,
                "Ttable"=>'t_wares',
                "f_module_name"=>"mdl_doc_mail_tg"
            )
        );
        $message = $wares->newWares($params_mail);
        if(isset($message['message'])) {
            echo $message['message'];
        }
        else {
            echo 'ok';
        }
        die;
    }

    /**
     * Upload new doc
     */
    public function uploadsAction()
    {
        //uploads file(s) passed from browser
        $mail = new Mainsim_Model_Mail();
        $f_code = $_POST['f_code'];
        $jsn = $mail->addAttachment($f_code);
        echo "<script> parent.moSendMail.retUpldFiles('".$jsn."') </script>";die;
    }

    public function uploadsAttachAction()
    {
        //uploads file(s) passed from browser
        $mail = new Mainsim_Model_Mail();
        $f_code = $_POST['f_code'];
        $jsn = $mail->addAttachment($f_code,$_FILES);
        echo "<script> parent.moMultipleAttach.retUpldFiles('".$jsn."') </script>";
    }

    public function sendCustomMailAction() {

        $objMail = new Mainsim_Model_Mail();
        print(json_encode($objMail->sendCustomMail(json_decode($_POST['params']))));
    }

	public function operationLinkMailAction() {
        //var_dump($_GET['cd']); die;
        $objMail = new Mainsim_Model_Mail();
        $res = $objMail->setDataLink($_GET['cd'], (isset($_GET['frm']) ? $_GET['frm'] : ''));

        $this->_helper->layout()->enableLayout();
         $this->_helper->viewRenderer->setNoRender(false);

        $this->view->message = $res;
        //echo $res;

    }
}
