<?php

class MobileLoginController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        // action body
        if(!$_POST){
            $this->_redirect("mobile2/login.html");
        }
        else{
            // check credentials
            $objLogin = new Mainsim_Model_MobileLogin();
            $responseLogin = $objLogin->login($_POST['username'], $_POST['password'], $_POST['imei']);
            // credentials verified --> check if user is already logged
            if($responseLogin['result']){
                // mode = 0 --> check if user is already logged
                if($_POST['mode'] == 0){
                    $responseAlreadyLogged = $objLogin->userAlreadyLogged($responseLogin['params']['f_code']);
                    // user already logged --> ask to overwrite login
                    if($responseAlreadyLogged){
                        print(json_encode($responseAlreadyLogged));
                    }
                    // user not yet logged in
                    else{
                        $objLogin->saveLogin($responseLogin['params']['f_code'], $responseLogin['sessionId'], $_POST['imei']);
                        print(json_encode($responseLogin));
                    }
                }
                // overwrite user logged
                else{
                    $objLogin->saveLogin($responseLogin['params']['f_code'], $responseLogin['sessionId']);
                    print(json_encode($responseLogin));
                }
            }
            // user not recognized
            else{
                print(json_encode($responseLogin));
                die();
            }
            
        }
        
    }
    
    public function logoutAction(){
        // check session
        Mainsim_Model_MobileUtilities::checkSession($_POST);

        $this->_helper->viewRenderer->setNoRender(TRUE);
        $obj = new Mainsim_Model_MobileLogin();
        print(json_encode($obj->logout()));
        die();
    }
    
    public function switchDesktopAction(){
        try{
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
            $userData = Zend_Auth::getInstance()->getIdentity();
            // logout from mobile
            $obj = new Mainsim_Model_MobileLogin();
            $obj->logout();
            // login desktop
            $obj2 = new Mainsim_Model_Login();
            $res = $obj2->authenticate($userData['fc_usr_usn'], $userData['fc_usr_pwd'], false);
            if($res === true){
                $userData = Zend_Auth::getInstance()->getIdentity();
                $auth   = Zend_Auth::getInstance();
                $authStorage = $auth->getStorage();  
                $userData->switch_desktop = true;
                $authStorage->write($userData);
                echo 'ok';
            }
            else{
                echo 'ko';
            }
        }
        catch(Exception $e){
            echo 'ko';
        }
        die();
    }
    
    public function switchSettingAction(){
        try{
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
            $obj = new Mainsim_Model_MobileLogin();
            $obj->editSettings($_POST['params']);  
            echo 'ok';
        }
        catch(Exception $e){
            print($e->getMessage());
            echo 'ko';
        }
    }
    

}

