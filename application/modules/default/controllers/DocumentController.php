<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class DocumentController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    } 
    
    public function getFileAction()
    {
        $column = isset($_GET['session_id'])?'f_session_id':'f_code';
        $value = isset($_GET['session_id'])?$_GET['session_id']:$_GET['f_code'];        
        $size = isset($_GET["size"]) ? $_GET["size"] : "doc";
        if(empty($column) || empty($value) || empty($size)) {
            die('Wrong parameters');
        }
        $getDocs = new Mainsim_Model_Document();        
        $res = $getDocs->getFile($column,$value,$size);        
        if(empty($res)) {
            die('No file Found');
        }
        $content = $res['f_path'];
        $filename = $res['f_file_name'].'.'.$res['f_file_ext'];
        
        if(!file_exists($content)) {
            die('No file Found');
        }
        header('Content-Type: '.$res['f_mime']);
        header('Content-disposition: filename="'.$filename.'"');

        echo file_get_contents($content);
        die;
    }
}

