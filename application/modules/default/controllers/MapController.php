<?php
class MapController extends Zend_Controller_Action
{
    public function preDispatch() 
    {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $main = new Mainsim_Model_Mainpage();
        $this->view->priorities = $main->getPriorities();
    }
    
    public function filterWoAction() {
        if(isset($_POST["f_type_id"]) && !empty($_POST["f_type_id"])) $type = $_POST["f_type_id"]; else $type = '1,4,5,6,7,10,13';
        $priority = $_POST["f_priority"];
        $owner = $_POST["fc_owner_name"];
        $map = new Mainsim_Model_Map();
        $locations = $map->getLocations();
        $c_locations = count($locations);
        $results = array();
        for($i=0; $i<$c_locations; $i++) {
            $assets = $map->getAssetsByLocation($locations[$i]["f_code"]);
            $assets[] = $locations[$i]["f_code"];
            if($assets) {
                $types = $map->getWorkordersByAssets($assets, $type, $priority, $owner);                
                if(count($types) > 0) {
                    $result = $locations[$i];
                    $result["types"] = $types;
                    $result["text"] = 0;
                    $result["max_priority"] = 0;
                    foreach($types as $t) {
                        $result["text"] += $t["count"];
                        if($result["max_priority"] < $t["max_priority"]) $result["max_priority"] = $t["max_priority"];
                    }
                    $results[] = $result;
                }
            }
        }
        echo json_encode($results);
    }
    
    public function getTypeForFilterAction(){
       // print('babaluba');
        $map = new Mainsim_Model_Map();
        print(json_encode($map->getTypeForFilter()));
    }
    
     public function getPriorityForFilterAction(){
       // print('babaluba');
        $map = new Mainsim_Model_Map();
        print(json_encode($map->getPriorityForFilter()));
    }
    
    public function getOwnerForFilterAction(){
       // print('babaluba');
        $map = new Mainsim_Model_Map();
        print(json_encode($map->getOwnerForFilter()));
    }
//    public function getLocationsAction() {
//        $map = new Mainsim_Model_Map();
//        return $map->getLocations();
//    }
//    
//    public function getAssetsByLocationAction($location) {
//        $map = new Mainsim_Model_Map();
//        return $map->getAssetsByLocation($location);
//    }
}
?>