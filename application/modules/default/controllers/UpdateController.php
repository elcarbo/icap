<?php

class UpdateController extends Zend_Controller_Action
{
    public function preDispatch() 
    {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    /**
     * Updates db version of mainsim
     */
    public function uiUpdateVersionAction()
    {
        $update = new Mainsim_Model_Update();
        $res = $update->uiUpdateVersion(APP_VERSION);
        echo "<div style='color: ".($res["status"] == "ok" ? "green" : "red").";'>{$res["message"]}</div>"; die;
    }
    
    /**
     * Convert properties mandatory, readonly, visible and disabled and level_edit
     * of ui components in new properties m_on, r_on, v_on, d_on and r_level
     * WARNING!
     * Conversion for t_wf missing, do it by yourself! :)
     */
    public function uiConvertPropertiesAction()
    {
        $update = new Mainsim_Model_Update();
        var_dump($update->uiConvertProperties(isset($_GET["admin"])?'admin_':''));
    }
    
    /**
     * Import languages from file to database setting as new category
     * for system settings type
     */
    public function langArrayToSystemSettingsAction()
    {
        $update = new Mainsim_Model_Update();
        $res = $update->langArrayToSystemSettings();
        echo "<div style='color: green;'>Translations successfully imported: {$res["#ok_entries"]}/{$res["#total_entries"]}</div>\n";
        unset($res["#ok_entries"]); unset($res["#total_entries"]);
        foreach($res as $r) {
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div>\n";
            else for($i=0; $i<count($r["error"]); $i++) echo "<div style='color: red;'>{$r["error"][$i]}</div>\n";
        }
    }
    
    /**
     * Convert edit modules to support dynamic generation of bottom
     * command bars
     */
    public function convertEditModuleToCbAction() {
        $update = new Mainsim_Model_Update();
        $res = $update->convertEditModuleToCb(isset($_GET["isadmin"]));
        foreach($res as $r) {
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div><br/>\n";
            else if(isset($r["error"])) echo "<div style='color: red;'>{$r["error"]}</div><br/>\n";
        }
    }
    
    /**
     * Convert properties who contain a truth value in boolean, i.e.
     * 1 => true, "true" => true, 0 => false, "false" => false
     */
    public function convertUiTruthValuesToBooleanAction() {
        $update = new Mainsim_Model_Update();
        $res = $update->convertUiTruthValuesToBoolean();
        foreach($res as $r) {
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div><br/>\n";
            else if(isset($r["error"])) echo "<div style='color: red;'>{$r["error"]}</div><br/>\n";
        }
    }
    
    /**
     * Remove all unuseful properties from ui
     */
    public function cleanUiAction() {
        $update = new Mainsim_Model_Update();
        $res = $update->cleanUi();
        foreach($res as $r) {
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div><br/>\n";
            else if(isset($r["error"])) echo "<div style='color: red;'>{$r["error"]}</div><br/>\n";
        }
    }
    
    public function getUiComponentListAction() {
        $update = new Mainsim_Model_Update();
        $uis = $update->getUiComponentList();
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=fields.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "f_instance_name;bind;label;label italian;info;info italian\r\n";
        foreach($uis as $k => $comp) {
            echo "$k;;;;;\r\n";
            foreach($comp as $ui) {
                echo $ui["f_instance_name"].";".$ui["bind"].";".$ui["label"].";".$ui["label_it"].";".$ui["info"].";".$ui["info_it"]."\r\n";
            }
        }
    }
    
    public function setUiPropertiesAction() {
        $update = new Mainsim_Model_Update();        
        $uis = array(            
            'txtfld_wo_planning_edit_spare_notes'=>array('level' => 207,'level_edit' => 1)
        );
        $tot = 0; $ok = 0; $txt = '';
        foreach($uis as $instance_name => $props) {
            $res = $update->setUiProperties($instance_name, $props);
            $tot++; $ok += ($res["error"]?0:1);
            $txt .= "<div style='color: ".($res["error"]?"red":"green").";'>{$res["message"]}</div>\n";
        }
        echo "Completed $ok/$tot<br/>$txt";
    }
    
    public function mainsimToSqlAction() {
        $update = new Mainsim_Model_Update();            
        if(!isset($_POST["table"]) || (!isset($_POST["t_workorders"]) && !isset($_POST["t_wares"]) && !isset($_POST["t_selectors"]) && !isset($_POST["t_systems"]))) {
            $this->view->categories = $update->mainsimToSql($_POST["table"], $_POST["ftype"]);
            $this->_helper->layout()->disableLayout(false);
            $this->_helper->viewRenderer->setNoRender(false);
        } else {
            $items = $update->mainsimToSql($_POST["table"], $_POST[$_POST["table"]]);
            //$starting_index = $_POST["starting_index"];
            $starting_index = 1; $indexes = array(); $out = ""; $newline = "\r\n";
            foreach($items as $table => $data) {
                $out .= "INSERT INTO $table (".implode(', ', $data["fields"]).") VALUES";
                for($i=0; $i<$data["count"]; $i++) {
                    $item = $data["data"][$i]; $count = count($item); $j = 0;
                    if($table == 't_creation_date') $indexes[$item["f_id"]] = $starting_index+$i;
                    $out .= $newline."(";
                    foreach($item as $k => $v) {
                        if(($table == "t_creation_date" && $k == "f_id") || $k == "f_code") {
                            
                        }
                        unset($item["f_id"]);
                        if($k == 'f_user_id' || $k == 'f_creation_user') $v = 1;
                        $out .= "'$v'".($j < $count-1 ? ", ": ""); $j++;                        
                    }
                    $out .= ")".($i == $data["count"] -1 ? "" : ",");                    
                }
                $out .= ";".$newline.$newline;
            }            
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=export.sql");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $out;
        }
    }
    
    /**
     * Updates all add buttons to call the new popup function addElements
     */
    public function updateOpenOverlayButtonsAction() {
        $upd = new Mainsim_Model_Update();
        $upd->updateOpenOverlayButtons();
    }
    
    public function setDefaultValuesToUiAction() {
        $overwrite = true;
        $upd = new Mainsim_Model_Update();
        $upd->setDefaultValuesToUi(array(
//            9 => array(
//                "moModuleCommandBar" => array("minsz" => 25, "noheader" => 1, "noframe" => 1, "noshadow" => 0, "noscroll" => 1, "margin" => array("t" => 4, "l" => 4, "r" => 4, "b" => 0), "padding" => 0),
//                "moModuleComponent" => array("minsz" => 38),
//                "moModuleCrud" => array("minsz" => 38),            
//                "moModuleEdit" => array("minsz" => 38),
//                "moModuleFrame" => array("minsz" => 38),
//                "moModuleSelector" => array("minsz" => 38),
//                "moModuleSummary" => array("minsz" => 38),
//                "moModuleTimeLine" => array("minsz" => 38),
//                "moModuleTreeGrid" => array("minsz" => 38)
//            ),
//            5 => array( "visible" => 1, "level" => -1 ),
//            6 => array( "h" => 21 ),
//            7 => array( "h" => 27 ),
//            8 => array( "v_on" => 1, "v_level" => -1, "type" => "moCheckbox", "groupName" => "msim-default" ),
            10 => array( "minv" => 38 )
//            13 => array( "v_on" => 1, "v_level" => -1, "type" => "moImage", "groupName" => "msim-default" ),
//            14 => array( "h" => 21 )
        ), $overwrite);
    }
    
    public function createDbSchemaAction() {
        $update = new Mainsim_Model_Update();
        $data = $update->createDbSchema();
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=db-schema.json");
        header("Pragma: no-cache");
        header("Expires: 0");        
        echo json_encode($data);
    }
    
    public function convertCategoryToOptionsAction() {
        $colors = array("message" => "green", "error" => "red", "warning" => "orange");
        $update = new Mainsim_Model_Update();
        $res = $update->convertCategoryToOptions();        
        foreach($res as $r) {
            foreach($r as $k => $v) {
                echo "<div style='color: {$colors[$k]};'>$v</div>\n";
            }
        }
    }
    
    public function layoutHideCbAction() {
        $colors = array("message" => "green", "error" => "red", "warning" => "orange");
        $update = new Mainsim_Model_Update();
        $res = $update->layoutHideCb();
        echo "<div>Found ".count($res)." layouts</div>";
        foreach($res as $r) {
            foreach($r as $k => $v) {
                echo "<div style='color: {$colors[$k]};'>$v</div>\n";
            }
        }
    }
	
    public function setCustomProgressAction() {
        $update = new Mainsim_Model_Update();
        $res = $update->setCustomProgress();        
        foreach($res as $r) {            
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div>\n";
            else if(isset($r["error"])) echo "<div style='color: red;'>{$r["error"]}</div>\n";            
        }
    }
}
?>