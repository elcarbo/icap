<?php

class WorkflowsController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        //$this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function indexAction()
    {
        
    }
    
    public function getWorkflowsAction()
    {
        $wf = new Mainsim_Model_Workflows();
        $workflows = $wf->getWorkflows();
        $response = array();
        if(!$workflows || empty($workflows)) {
            echo json_encode($response);die;
        }
        for($i = 0; $i < count($workflows); $i++) {
            if(!count($response)) {
                $response[] = array_keys($workflows[$i]);
            }
            $response[] = array_values($workflows[$i]);
        }
        echo json_encode($response);die;        
    }
    
    public function getWorkflowPhasesAction()
    {
        $id = $this->_getParam('id');
        if(!isset($id) || !ctype_digit($id)) {
            echo json_encode(array());die;
        }
        $wf = new Mainsim_Model_Workflows();
        $result = $wf->getWorkflowPhases($id);
        $response = array();
        if(!$result || empty($result)) {
            echo json_encode($response);die;
        }
        for($i = 0; $i < count($result); $i++) {
            if(!count($response)) {
                $response[] = array_keys($result[$i]);
            }
            $response[] = array_values($result[$i]);
        }
        echo json_encode($response);die;  
    }
    
    public function wfPhaseExitAction()
    {
        $this->_helper->layout()->disableLayout();        
        if(!ctype_digit($_POST['f_phase_id']) && !ctype_digit($_POST['f_wf_id'])) {
            // echo json_encode(array());
            /* TODO: modificare */
            $wf_phase = 0;
            $wf_id = 1;
            /**/
        }
        else {
            $wf_id = $_POST['f_wf_id'];
            $wf_phase = $_POST['f_phase_id'];
        }
        $wf = new Mainsim_Model_Workflows();        
        $phase = $wf->getWfExit($wf_phase,$wf_id);        
        echo json_encode($phase);
    }    
}