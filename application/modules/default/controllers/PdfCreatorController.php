<?php

 class PdfCreatorController extends Zend_Controller_Action {

     public function preDispatch() {
         parent::preDispatch();
         $this->_helper->layout()->disableLayout();
         $this->_helper->viewRenderer->setNoRender(true);
         ////Zend_session::writeClose();
     }

    public function indexAction()
    {
         
     }

    public function pdfParamsAction()
    {
         $error = array();
        if(empty($_POST)) {
             $error['message'] = 'Empty filter';
         } else {
             $post = json_decode($_POST['filter']);
             $_SESSION['pdfParams'] = $post;
         }
        echo json_encode($error);die;
     }

    public function printPdfAction()
    {
         $params = $this->_getAllParams();
         //print_r($params);die();
         $pdfCreator = new Mainsim_Model_PdfCreator();
         $pdf = $pdfCreator->printPdf($params);
         if(isset($params['pdf_name'])) {
            $name = $params['pdf_name'];
         } else {
            $name = 'MAINSIM_PDF_'.str_replace(',', '_', $params['f_code']).'.pdf';
         }
         if ($params['pdf_from_wizard'] == true) {
             if ($params['pdf_download'] == true)
                 $pdf->Output($name, 'D');
             else
                 $pdf->Output($name, 'I');
             return;
         } else {
             $pdf->Output($name, 'I');
             return;
         }
     }
 }
 