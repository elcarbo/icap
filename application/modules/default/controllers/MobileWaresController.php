<?php

class MobileWaresController extends Zend_Controller_Action{
    
    public function init(){
        $this->_helper->layout()->disableLayout();
        Mainsim_Model_MobileUtilities::checkSession($_POST);
    }
    
    public function searchWareAction(){
        $objWares = new Mainsim_Model_MobileWares();
        $params['type'] = 1;
        $params['fetch'] = Zend_Db::FETCH_NUM;
        // mode = 0 --> wares search by string
        if(!$_POST['params']['mode']){
            $wares = $objWares->searchWares($_POST['params']);
        }
        else if($_POST['params']['mode'] == 1){
            $wares = $objWares->getChildrenWares($_POST['params']);
        }

        print(str_replace(",null", ",\"\"",json_encode($objWares->createResponseObject(1, 'SEARCH_DONE', $_POST['sessionId'], $wares))));
        die();
    }
    
    public function checkDocumentAction(){
        $objWares = new Mainsim_Model_MobileWares();
        print($objWares->checkDocument($_POST['params']));
        die();
    }
    
    public function readWaresAction(){
        $objWares = new Mainsim_Model_MobileWares();
        $params['type'] = 1;
        $params['fetch'] = Zend_Db::FETCH_NUM;
        $params['in'] = $_POST['params']['code_list'];
        if($params['in']){
            $wares = $objWares->getWares($params);
        }
        else{
            $wares = array();
        }
        print(str_replace(",null", ",\"\"",json_encode($objWares->createResponseObject(1, 'GET_WARES_DONE', $_POST['sessionId'], $wares))));
        die();
    }
    
    public function checkAssetNfcAction(){
        $objWares = new Mainsim_Model_MobileWares();
        $resWares = $objWares->getCodeByNFC($_POST['params']['nfc_value']);
        if($resWares){
            $resultParams['asset_code'] = $resWares[0]['f_id'] ? $resWares[0]['f_id'] : "";
            $resultParams['asset_title'] = $resWares[0]['f_title'] ? $resWares[0]['f_title'] : "";
            $resultParams['asset_description'] = $resWares[0]['f_description'] ? $resWares[0]['f_description'] : "";
            print(str_replace(",null", ",\"\"",json_encode($objWares->createResponseObject(1, 'NFC_ASSET_FOUND', $_POST['sessionId'], $resultParams))));
        }
        else{
            print(str_replace(",null", ",\"\"",json_encode($objWares->createResponseObject(1, 'NFC_ASSET_NOT_FOUND', $_POST['sessionId'], $resultParams))));
        }
        die();
    }
    
    public function readAttachAction(){
    
        $objWares = new Mainsim_Model_MobileWares();
        $risp=$objWares->getAttachements($_POST['params']);  

        print(str_replace(",null", ",\"\"",json_encode($risp)));
        die();
    }
    
}

