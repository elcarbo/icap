<?php

class WaresController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }   
        
    public function editAction()
    {        
        $params = json_decode($_POST['params'],true);
        if($params['f_type'] == "WARES" && $params['f_category'] == "USER"){
                $params['f_type_id'] = 16;
        }
        $res = array();
        $wares = new Mainsim_Model_Wares();
        try {
            $res = $wares->editWares($params);             
        }catch(Exception $e) {
           
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') == 1){
                $res['message'] = 'Something went wrong while editing your wares '.$e->getMessage();
            }
            else{
                $res['message'] = 'Something went wrong while editing your wares (debug mode off)';
            }
        }                
        echo json_encode($res);die;
    }
    
    public function changePhaseBatchAction()
    {
        $ware = new Mainsim_Model_Wares();
        echo json_encode($ware->changePhaseBatch($_POST["f_codes"], $_POST["f_wf_id"], $_POST["f_exit"], $_POST["f_module_name"]));
    }
    
    
    /* ADDED BY ALESSANDRA 13/03/2012 */   
    public function newAction()
    {
        $params = json_decode($_POST['params'],true);        
        $wares = new Mainsim_Model_Wares();
        $res = array();
        
        try {
            $res = $wares->newWares($params);             
        }catch(Exception $e) {
            
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') === 1){
                $res['message'] = 'Something went wrong while inserting your wares: '.$e->getMessage();
            }
            else{
                $res['message'] = 'Something went wrong while inserting your wares (debug mode off)';
            }
        }        
        echo json_encode($res);die;
    }
    
    /**
     * Edit batch 
     */
    public function editBatchAction()
    {
        $params = $_POST['params'];                
        $mObj = new Mainsim_Model_MainsimObject();
        $res = array();
        try {
            $res = $mObj->editBatch('t_wares',$params);              
        }catch (Exception $e) {
            $res['message'] = 'Something went wrong while editing your wares '.$e->getMessage();
        }        
        echo json_encode($res);die;
    }
        
    public function cloneWaresAction() 
    {
        //Zend_session::writeClose();
        if(!isset($_POST["f_module_name"])) { echo json_encode(array("message" => "Missing module name")); die; }
        else if(!isset($_POST["f_code"])) { echo json_encode(array("message" => "No elements to clone")); die; }
        else if(!isset($_POST["clone_type"])) $_POST["clone_type"] = 0;
        $obj = new Mainsim_Model_MainsimObject();        
        $obj->type = 't_wares';
        $res = $obj->cloneObjects($_POST["f_code"],$_POST["clone_type"], $_POST["f_module_name"]);
        //Zend_Debug::dump(memory_get_peak_usage()/1024);die; 
        unset($res['codes_cloned']);
        echo json_encode($res);
    }
}