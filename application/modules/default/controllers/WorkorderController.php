<?php

class WorkorderController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        //$this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    public function indexAction()
    {
        
    }
    
    public function createWoAction()
    {
        $message = array();
        if(empty($_POST['f_code'])) {
            $message['error'] = 'No workorder has been passed before';
            echo json_encode($message);die;
        }
        try {
            $wo = new Mainsim_Model_Workorders();
            $codes = explode(',',$_POST['f_code']);
            $tot = count($codes);
            $count = 0;
            for($i = 0;$i < $tot;++$i) {
                $res = $wo->createWo($codes[$i]);                
                if(is_array($res)) {
                    $message = $res;
                    break;
                }                
                $count+=$res;
            }
        }catch(Exception $e) {
            $message['message'] = $e->getMessage();            
        }
        if(empty($message)) {
            $message['message'] = $count == 1?Mainsim_Model_Utilities::chg("Workorder has been generated."):Mainsim_Model_Utilities::chg("$count Workorders have been generated.");
        }
        echo json_encode($message);die;
        
    }
    
    public function editAction()
    {
        $params = json_decode($_POST['params'],true);
        
        // security fix 
        if($params == null){
            die('invalid json params');
        }
        
        $wo = new Mainsim_Model_Workorders();
        $error = array();
        try {
            $result = $wo->editWorkOrder($params);
            $error = $result;            
        }catch(Exception $e) {            
            $error['message'] = "Something went wrong during the editing of your request: ".$e->getMessage();
        }                
        $this->_helper->layout()->disableLayout();
        echo json_encode($error);
    }
    
    public function editBatchAction()
    {        
        $params = $_POST['params'];
        $error = array();
        $wo = new Mainsim_Model_MainsimObject();
        try {
            $result = $wo->editBatch('t_workorders',$params);            
            $error = $result;
        }catch(Exception $e) {            
            $error['message'] = "Something went wrong during the editing of your request: ".$e->getMessage();
        }        
        $this->_helper->layout()->disableLayout();
        echo json_encode($error);
    }
    
    /* ADDED BY ALESSANDRA 13/03/2012 */
    public function newAction()
    {
        $wo = new Mainsim_Model_Workorders();
        $error = array();
        $wo = new Mainsim_Model_Workorders();
        try {
            $result = $wo->newWorkOrder(json_decode($_POST['params'],true));;
            $error = $result;
        }catch(Exception $e) {                   
            $error['message'] = "Something went wrong during insertion of your request ".$e->getMessage();
        }
        $this->_helper->layout()->disableLayout();
        echo json_encode($error);
    }
    /**/
    
   /*
     * generatore richieste da wizard
    */
    public function wizardAction()
    {   
        if(empty($_POST['params'])){
            return;
        }      
       
        $params = json_decode($_POST['params'],true);  
        $params['wiz_old'] = $_GET['wiz_old'];		
        $wo = new Mainsim_Model_Workorders();
        $res = $wo->newWoFromWizard($params);
        if(empty($res)) {
            echo 'OK';
        }
        else {
            echo json_encode($res);
        }
		die();
    }
    
    public function changePhaseBatchAction()
    {
        $this->_helper->layout()->disableLayout();
        $workorder = new Mainsim_Model_Workorders();
        echo json_encode($workorder->changePhaseBatch($_POST["f_codes"], $_POST["f_wf_id"], $_POST["f_exit"], $_POST["f_module_name"]));
    }
    
    /**
     * Action to clone check workorder
     * @return type 
     */
    public function cloneWorkorderAction()
    {
        //Zend_session::writeClose();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if(empty($_POST)) return;
        $wo = new Mainsim_Model_Workorders();
        $f_code = explode(',',$_POST['f_code']);        
        $tot = count($f_code);        
        $response = array('f_count'=>$tot);
        $message = array();        
        $obj = new Mainsim_Model_MainsimObject();        
        $obj->type = 't_workorders';                
        $res = $obj->cloneObjects($_POST["f_code"],$_POST["clone_type"], $_POST["f_module_name"]);
        if(!isset($_POST['return_codes'])){
            unset($res['codes_cloned']);
        }
        echo json_encode($res);
        die;
        
        //callback
        
        if(isset($message['message'])) {
            echo json_encode($message);
        }
        else {
            $response['f_changed'] = $message['f_changed'];
            echo json_encode($response);
        }
    }
    
    public function reopenedClose($codes = array(),$module_name = '') 
    {        
        $changed = 0;        
        $select = new Zend_Db_Select($this->db);        
        $this->db->beginTransaction();        
        $not_changed = array();
        foreach($codes as $code) {
            $select->reset();
            $res_closed = $select->from(array("t1"=>"t_workorders"),array('f_code'))
                    ->join(array("t2"=>"t_wf_phases"),"t1.f_wf_id = t2.f_wf_id and t2.f_number = t1.f_phase_id",array())
                    ->join(array("t3"=>"t_wf_groups"),"t2.f_group_id = t3.f_id",array())
                    ->where("t1.f_code = ?",$code)->where("t3.f_id IN (6,7)")
                    ->query()->fetch(); 
			
            if(empty($res_closed)){$not_changed[] = $code; continue; }

            try {                
                $this->db->update("t_workorders",array("f_phase_id"=>1),"f_code = $code and f_active = 1");                                
            }catch(Exception $e) {
                $this->db->rollBack();
                return array('message'=>$e->getMessage());
            }
            $changed++;
        }
        $this->db->commit();
        
        foreach($codes as $code) {   
            if(in_array($code,$not_changed)) continue;
            $select->reset();
            $res_parent = $select->from("t_workorders_parent",array("f_parent_code"))
                ->where("f_active = 1")->where("f_code = ?",$code)
                ->query()->fetchAll();
            $parent = array();
            foreach($res_parent as $line_parent) {
                $parent[] = $line_parent['f_parent_code'];
            }
            $this->saveReverseAjax($code, $parent,"add",$module_name);                
        }
        return $changed;
    }
    
    /**
     * Generate opportunistic workorder
     */
    public function opportunisticWoAction() 
    {
        try{
            if(empty($_POST['f_codes'])) return;
            
            // security fix (match number,number,...)
            if(!preg_match("/^[0-9]{1,}(,[0-9]{1,})*$/", $_POST["f_codes"])){ 
                die('param f_codes input not valid');
            }

            $pm = new Mainsim_Model_PMReader();
            $codes = explode(',',$_POST['f_codes']);

                    // allow wo generation only for 'a single wo for all asset' pm
                    $isMulti = $pm->isMulti($_POST['f_codes']);
                    foreach($isMulti as $key => $value){
                            if($value == 'pm-per-asset'){
                                    $toRemove[] = $key;
                            }
                    }
                    if(is_array($toRemove)){
                            $codes = array_diff($codes, $toRemove);
                    }

            $response = array();
            $number = 0;
            foreach($codes as $f_code) {
                $res = $pm->readPeriodics(time(), 0, 1, $f_code);            
                if(isset($res['message'])) {
                    $response['message'] = Mainsim_Model_Utilities::chg($res['message']);
                    break;
                }
                else {
                    Mainsim_Model_Utilities::saveReverseAjax("t_workorders_parent", $f_code, array(), "upd", "mdl_pm_tg");                                
                }
                $number+=$res;
            }
            if(!isset($response['message'])) {              
               //$response['message'] = $number == 1?Mainsim_Model_Utilities::chg("Periodic has been generated."):Mainsim_Model_Utilities::chg("$number periodics have been generated.");
                       $trsl = new Mainsim_Model_Translate();
                        if($number > 0){
                                    $response['message'] = "<center><b>" . $number .  "</b> " . $trsl->_("periodics generated");
                            }
                            if(($pmEndedCounter = count($isMulti) - $number - count($toRemove)) > 0){
                                    if(!isset($response['message'])){
                                            $response['message'] .= "<b>" . $pmEndedCounter . "</b> " . $trsl->_("periodics not generated (cycle finished)") . "</center>";
                                    }
                            }
                            if(count($toRemove) > 0){
                                    if($number > 0){
                                            $response['message'] .= "<br>";
                                    }
                                    $response['message'] .= "<b>" . count($toRemove) . "</b> " . $trsl->_("periodics not generated ('a job for every asset' periodic type)") . "</center>";
                            }
            }
            echo json_encode($response); die;
        }
        catch(Exception $e){
            $trsl = new Mainsim_Model_Translate();
            // security fix
            if((new Mainsim_Model_Utilities())->getSettings('DEBUG_ON') == 1){
				$response['message']=$trsl->_('Something went wrong while editing your wo').' '.$e->getMessage();
                echo json_encode($response); die;
            }
            else{
				$response['message']=$trsl->_('Something went wrong while editing your wo').' '.' (debug mode off)';
                echo json_encode($response); die;
            }
        }
    }
    
    public function openPoAction()
    {
        $wo = new Mainsim_Model_Workorders();
        if(empty($_POST['f_codes']) || empty($_POST['ftype'])) die;
        $res = $wo->openPo($_POST['f_codes'], $_POST['ftype'],$_POST['singlePo']);        
        echo json_encode($res);
    }
    
    public function forceMultiPmAction(){

        $objWo = new Mainsim_Model_Workorders();
        $res = $objWo->forceMultiPm($_POST['code_asset'], $_POST['codes_list']);
    }
        
}