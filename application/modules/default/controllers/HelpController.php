<?php

class HelpController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->setLayout('login');
        //$this->_helper->viewRenderer->setNoRender(true);
    }

    public function init()
    {
        /* Initialize action controller here */
    }

    public function xmlToJsnAction()
    {
        $res = array();
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $ptr = !empty($_POST["param"])?json_decode($_POST["param"],true):array();
            $translator = new Mainsim_Model_Translate();
            $help = new Mainsim_Model_Help();            
            $lang = $translator->getLang();
            if(empty($lang)) {
                $lang = 'en_GB';
            }
            $res = $help->xmltojson($lang, $ptr);
        }  
        echo json_encode($res);
        die;
    }   
    
    public function vimeoVideoAction()
    {
        
    }
}

