<?php

class MainsimObjectController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function getObjectAction()
    {   
        $res = array();
        if(!isset($_POST["f_code"])) $item = array("message" => "Missing f_code");
        else {
            $obj = new Mainsim_Model_MainsimObject();
            $obj->type = "workorders";
            $res = $obj->getObject($_POST["f_code"]);
            $item = $res[0];
            unset($item["f_id"]); unset($item["f_active"]); unset($item["f_parent_code"]);
            foreach($res as $k => $v) {
                $item["f_parent_codes"][] = $res[$k]["f_parent_code"];
            }
        }
        echo json_encode($item);
    }
}