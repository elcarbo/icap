<?php

    
class MainpageController extends Zend_Controller_Action
{
    
    /*public function preDispatch() {
        parent::preDispatch();
        //$this->_helper->layout()->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
    }*/
    
    public function indexAction()
    {
		if(!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->project_name != PROJECT_NAME) {            
            $this->redirect("/index");
        } 
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        $login = new Mainsim_Model_Login();
        if($login->checkLogin($userinfo->f_id) && false) { 
			Zend_Auth::getInstance()->clearIdentity();
            $this->redirect("/index");
        }
        $db = Zend_Db::factory(Zend_Registry::get('db'));  
        $db->query("DELETE from t_locked where f_user_id = {$userinfo->f_id}");

        $main = new Mainsim_Model_Mainpage();
        //get language info
        $translator = new Mainsim_Model_Translate();                
        $_SESSION['LANG'] = $translator->getLanguage($translator->getLang());
                
        $this->view->LANG = json_encode($_SESSION['LANG']);
        //$info_sys = $ui->getInfoSystem();
        $settings = $main->getSettings();
        foreach($settings as $k => $v) {
            $_SESSION[$k] = $v;
        }
		
        // get ADFS setting to enable/disable logout button
        $objUtils = new Mainsim_Model_Utilities();
        $adfsConf = json_decode($objUtils->getSettings('ADFS'));
        if($adfsConf != null && $adfsConf->enabled == 1){
                $this->view->adfs = true;
        }
        else{
                $this->view->adfs = false;
        }
		       
        // setting global search
        $setsearch=(!isset($_SESSION["treegrid_search_fields"]) ) ? "" : ($_SESSION["treegrid_search_fields"]); 
        $setsearch=str_replace("\n","",$setsearch); 
        $this->view->stgSearch = $setsearch;
        
        // setting summary_show_overall
        $setoverall=(!isset($_SESSION["summary_show_overall"]) ) ? 1 : ($_SESSION["summary_show_overall"]);    // default visible
        $setoverall=(int) (str_replace("\n","",$setoverall)); 
        $this->view->stgSummaryAll = $setoverall;
        
        
        $this->view->signature = $_SESSION['SIGNATURE'];
        $this->view->version = APP_VERSION;        
        $this->view->translate = new Mainsim_Model_Translate();
        
        $this->view->calendar = (isset($_SESSION["HEADER_CALENDAR"]) && (intval($_SESSION["HEADER_CALENDAR"]) & intval($userinfo->f_level)));
        $this->view->help = (isset($_SESSION["HEADER_HELP"]) && (intval($_SESSION["HEADER_HELP"]) & intval($userinfo->f_level)));
        $this->view->user_guide = (isset($_SESSION["HEADER_USER_GUIDE"]) && (intval($_SESSION["HEADER_USER_GUIDE"])));
        $this->view->bulletin = (isset($_SESSION["HEADER_BULLETIN"]) && (intval($_SESSION["HEADER_BULLETIN"]) & intval($userinfo->f_level)));
        $this->view->users_connected = (isset($_SESSION["HEADER_USERS_CONNECTED"]) && (intval($_SESSION["HEADER_USERS_CONNECTED"]) & intval($userinfo->f_level)));
        
        $summary = new Zend_Session_Namespace('summarydata');
        $summary->module = [];
        $summary->data = [];
        
        // load new/old picklist
        $newPicklist = $objUtils->getSettings('NEW_PICKLISTS');
        if($newPicklist != null && $newPicklist == 1){
            $this->view->newPicklist = true;
        }
        else{
            $this->view->newPicklist = false;
        } 
    }
    
    public function maininitAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $ui = new Mainsim_Model_UI();
        $lastEdit = $ui->getLastUIEdited();        
        $sessionTs = empty($_SESSION['ui'])?0:$_SESSION['ui']['ts'];
        
        if($sessionTs < $lastEdit){
            $_SESSION['ui']['data']['tabbars'] = $ui->getTabbars();
            $_SESSION['ui']['data']['textfields'] = $ui->getTextfields();
            $_SESSION['ui']['data']['picklists'] = $ui->getPicklists();
            $_SESSION['ui']['data']['checkboxes'] = $ui->getCheckboxes();
            $_SESSION['ui']['data']['menus'] = $ui->getMenus();
            $_SESSION['ui']['data']['buttons'] = $ui->getButtons();
            $_SESSION['ui']['data']['modules'] = $ui->getModules();
            $_SESSION['ui']['data']['layouts'] = $ui->getLayouts();
            $_SESSION['ui']['data']['selects'] = $ui->getSelects();
            $_SESSION['ui']['data']['images'] = $ui->getImages();
            $_SESSION['ui']['ts'] = time();
        }
        $data = $_SESSION['ui']['data'];        
        //BASEURL
        $data["global"] = array();
        $data["global"]["delegates"] = array();
        
        //---------Options
//        $data["global"]["options"] = $ui->getOptions();        
        //$data["global"]['baseUrl'] = BASEURL;
        
        $weekStartsOn = 1;
        $data["global"]['weekStartsOn'] = $weekStartsOn;
//        $info_sys = $ui->getInfoSystem(); 
//        $data["global"]["dateFormat"] = "{l}, {F} {d} {Y}";
//        $data["global"]["timeFormat"] = "{H}:{i}";
//        $data["global"]["dateTimeFormat"] = $info_sys['f_date_format_long']; // "{l}, {F} {d} {Y}, {H}:{i}"
//        $data["global"]["dateShortFormat"] =  $info_sys['f_date_format_short']; // "{m}/{d}/{Y}, {H}:{i}"        

        $login = new Mainsim_Model_Login();        
        $data["global"]['online'] = $login->getOnlineNum();        
        $data["global"]['user'] = $this->getUserData();
        $data["global"]['baseUrl'] = $this->view->baseUrl();
        $data["global"]['imgDefaultPath'] = IMAGE_DEFAULT_PATH;
        $data["global"]['imgPath'] = IMAGE_PATH;
        $data["global"]['attachPath'] = ATTACHMENT_PATH;
        $data["global"]['libraryPath'] = LIBRARY_PATH;    
        
        /**
         * Settings
         */
        $main = new Mainsim_Model_Mainpage();
        $settings = $main->getSettings(true);
        foreach($settings as $k => $v) {
            $data["global"][$k] = Mainsim_Model_Utilities::chg($v);
        }
        
        /**
         * Wo priorities
         */
        $data["global"]["priorities"] = $main->getPriorities();

        /**
         * Easter
         */
        $data["global"]['easters'] = $main->getEasters();
                
        /**
         * Localization
         */
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        $data["global"]["localization"] = $ui->getLocalization($userinfo->f_language);
        
        /**
         * Reverse Ajax Initialization
         */
        $data["global"]["revajax"] = array("t_workorders" => array(), "t_wares" => array(), "t_selectors" => array(), "t_systems" => array());
        $q = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        foreach($data["global"]["revajax"] as $k => $v) {
            $q->from($k."_types", array("f_id"));
            $res = $q->query()->fetchAll();
            $q->reset();
            foreach($res as $r) $data["global"]["revajax"][$k][$r["f_id"]] = array();
        }
        
        /**
         * Regular expressions
         */
        $data["regexp"] = array(
            "mail" => "/^(([^<>()[\\]\\\\.,;:\\s@\\\"] (\\.[^<>()[\\]\\\\.,;:\\s@\\\"] )*)|(\\ \". \\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA -Z\\-0-9] \\.) [a-zA-Z]{2,}))$/",
            "password" => "/^[a-zA-Z0-9\\@\\#\\-\\_\\.]{3,32}$/",
            "username" => "/^[a-zA-Z0-9\\@\\#\\-\\_\\.]{3,32}$/"
        );        
        echo json_encode($data);
    }
		
    public function getUserData() {
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        $sc = new Mainsim_Model_StartCenter();
		
		// check if 'change default password' setting is enabled
		$objUtils = new Mainsim_Model_Utilities();
		if($objUtils->getSettings('CHANGE_DEFAULT_PASSWORD')){
			$defaultPassword = $userinfo->fc_usr_pwd == '313ed0558152d04dc20036dbd850dc9bd';
		}
		else{
			$defaultPassword = false;
		}
		
        return array(
            "code" => $userinfo->f_id,
            "fullname" => Mainsim_Model_Utilities::chg($userinfo->f_displayedname),
            "firstname" => Mainsim_Model_Utilities::chg($userinfo->fc_usr_firstname),
            "lastname" => Mainsim_Model_Utilities::chg($userinfo->fc_usr_lastname),
            "gender" => $userinfo->f_gender == 0 ? "M" : "F",
            "avatar" => $userinfo->fc_usr_avatar,
            "level" => $userinfo->f_level,
            "level_text" => $userinfo->fc_usr_level_text,
            "groupLevel" => $userinfo->f_group_level,
            "status" => $userinfo->f_status,
            "address" => $userinfo->fc_usr_address,
            "phone" => $userinfo->fc_usr_phone,
            "username" => $userinfo->fc_usr_usn,
            "defaultPassword" => $defaultPassword,
            //"defaultProject" => $userinfo->fc_usr_default_project,          /* MULTI PROJECT */
            "accountExpiration" => $userinfo->fc_usr_usn_expiration,
            "pwdExpiration" => $userinfo->fc_usr_pwd_expiration,
            "accountCreation" => $userinfo->fc_usr_usn_registration,
            "email" => $userinfo->fc_usr_mail,
            "language" => $userinfo->f_language,
            "bulletins" => count($sc->getBulletins()),
            "fc_usr_mobile_autoswitch_desktop" => $userinfo->fc_usr_mobile_autoswitch_desktop
        );
    }

}

