<?php

class InspectionController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        //Zend_session::writeClose();
    }
    
    public function saveAction()
    {
        $data = json_decode($_POST["params"], true);           
        //check if is a shared view and if user can do
        
        if($data['shared'] == 1 && Zend_Auth::getInstance()->getIdentity()->f_level != -1){
            $trsl = new Mainsim_Model_Translate();
            $langName = $trsl->getLangNameFromCode(Zend_Auth::getInstance()->getIdentity()->f_language);                        
            $response = array('message'=>$trsl->_('You cannot save default shared because you are not an administrator.'));
            echo json_encode($response);die;
        }        
        $params = array(
            'f_code' => 0, 'fc_bkm_module' => $data['f_module_name'], 'fc_bkm_user_id' => ($data['shared'] == 1 ?0:Zend_Auth::getInstance()->getIdentity()->f_id),
            'fc_bkm_data' => $data['data'], 'f_title' => $data['f_name'], 'f_type_id' => 3, 'f_phase_id' => 1,
            'fc_bkm_bookmark'=>$data['bookmark'],
            'f_module_name' => 'mdl_bkm_tg',
            'unique' => array ( 'f_title,fc_bkm_user_id,fc_bkm_module', 'f_title,fc_bkm_user_id,fc_bkm_module' ),
            't_systems_-1' => array('f_code_main' => 0, 'f_type' => '-1', 'Ttable' => 't_systems', 'pairCross' => '2',
            'f_module_name' => 'mdl_bkm_eng_tg', "f_code" => array(), "f_pair" => array())
        );                  
        $props = json_decode($data["view"], true);        
        $n = count($props);
        for($i=0; $i<$n; $i++) {
            $params["t_systems_-1"]["f_code"][] = -$i-2;
            $params["t_systems_-1"]["f_pair"][] = array (
                'f_code_main' => 0, 'f_code' => -$i-2, 'f_code_cross' => -1, 'f_title' => 'Field', 'f_phase_id' => 1,
                'fc_bkm_label' => $props[$i][1], 'fc_bkm_bind' => $props[$i][0], 'fc_bkm_type' => $props[$i][2], 'fc_bkm_locked' => $props[$i][3],
                'fc_bkm_visible' => $props[$i][4], 'fc_bkm_width' => json_encode($props[$i][5]), 'fc_bkm_filter' => json_encode($props[$i][6]),
                'fc_bkm_range' => json_encode($props[$i][7]),'fc_bkm_prange' => json_encode($props[$i][8]), 'fc_bkm_script' => $props[$i][9],
                'fc_bkm_level' => $props[$i][10], 'fc_bkm_group' => $props[$i][11] );
        }        
        $inspection = new Mainsim_Model_Inspection();
        echo json_encode($inspection->create($params));
    }
    
    public function deleteAction()
    {
        $inspection = new Mainsim_Model_Inspection();
        
        // View
        if(isset($_POST['f_module_name'])) $module_name = $_POST['f_module_name'];
        if(isset($_POST['f_name'])) $name = $_POST['f_name'];
        $uid = ($_POST['shared'] == 1 ?0:Zend_Auth::getInstance()->getIdentity()->f_id);
        if($module_name && $name) echo json_encode($inspection->delete($module_name, $name, $uid));
        else { echo json_encode(array());}
    }
    
    public function getInspectionAction()
    {
        $res = array();
        if(isset($_POST['f_name']) && isset($_POST['f_module_name'])) {            
            $ins = new Mainsim_Model_Inspection();
            $res = $ins->getInspection($_POST['f_name'], $_POST['f_module_name'], $_POST["shared"]);            
        }        
        echo json_encode($res); exit;
    }
}