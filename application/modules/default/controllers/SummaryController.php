<?php

class SummaryController extends Zend_Controller_Action
{
    
    public function preDispatch() 
    {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
            
    public function getWfPhasesAction()
    {
        $summary = new Mainsim_Model_Summary();
        $f_type = 0;
        if(isset($_POST['f_type_id'])) {
            $f_type = $_POST['f_type_id'];
        }        
        $result = $summary->getWfPhase($f_type);
        echo json_encode($result);
        exit;
    }
    
    public function inboxAction()
    {
        $exec = new Mainsim_Model_Script();
        $data = json_decode($_POST["params"]);
        if(!$exec->execute(array('f_type' => $data->f_type, 'script' => 'inbox'))) {
            $summary = new Mainsim_Model_Summary();            
            $result = $summary->getInbox($data);
            echo json_encode($result);
        }
        exit;
    }
    
    public function woAllAction()
    {
        $exec = new Mainsim_Model_Script();
        $data = json_decode($_POST["params"]);
        if(!$exec->execute(array('f_type' => $data->f_type, 'script' => 'wo-all'))) {
            $summary = new Mainsim_Model_Summary();            
            $result = $summary->getSummary($data);
            echo json_encode($result);
        }
        exit;
    }
      public function wareAllAction()
    {
        $exec = new Mainsim_Model_Script();
        $data = json_decode($_POST["params"]);
        if(!$exec->execute(array('f_type' => $data->f_type, 'script' => 'ware-all'))) {
            $summary = new Mainsim_Model_Summary();            
            $result = $summary->getSummary($data);
            echo json_encode($result);
        }
        exit;
    }
    
    public function extraAction()
    {
        $resp = array();
        $summary = new Mainsim_Model_Summary();
        if(isset($_GET['summary']) && !empty($_GET['summary'])){
            $data = json_decode($_POST["params"]);
            $resp = $summary->extra($_GET['summary'],$data);
        }
        echo json_encode($resp);
    }
    
    public function wo1Action()
    {
        $summary = new Mainsim_Model_Summary();            
        echo json_encode($summary->getSummary(1));        
    }
    
    public function wo3Action()
    {
        $summary = new Mainsim_Model_Summary();            
        echo json_encode($summary->getSummary(3));        
    }
    
    public function wo4Action()
    {
        $summary = new Mainsim_Model_Summary();            
        echo json_encode($summary->getSummary(4));        
    }
    
    public function wo15Action()
    {
        $summary = new Mainsim_Model_Summary();            
        echo json_encode($summary->getSummary(15));        
    }
}