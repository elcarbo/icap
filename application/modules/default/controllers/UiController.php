<?php
class UiController extends Zend_Controller_Action
{
    public function preDispatch() 
    {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    public function getTabsAction() {
        $ui = new Mainsim_Model_UI();
        print_r($ui->getTabs());
    }
    
    public function getSubTabsAction() {
        $ui = new Mainsim_Model_UI();
        print_r($ui->getSubTabs("asset"));
    }
    
    public function createPopupUiAction() {
        $ui = new Mainsim_Model_UI();
        $ui->getTabbars();
    }
}
?>