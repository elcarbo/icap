<?php
class KpiController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);        
        //Zend_session::writeClose();
    }
    
    public function ui4Action(){
        $objKpi = new Mainsim_Model_Kpi('wos_trends');
        print(json_encode($objKpi->get_ui()));
        die();
    }
    
    public function data4Action(){
        $objKpi = new Mainsim_Model_Kpi('wos_trends');
        $params = json_decode($_POST['params']);
        foreach($params as $key => $value){
            if(strpos($key, '_nofilter') === FALSE){
                $filters[] = $key . '(' . $value . ')';
            }
            else{
                $time = $value;
            }
        }
        
        if(is_array($filters)){
            $key_filter = implode('_', $filters);
        }
        else{
            $key_filter = 'NO_FILTER';
        }
        $objKpi->initFilter($key_filter);
        $objKpi->update($params);
        
        // get time period
        if($params->year_nofilter == date('Y') || $params->year_nofilter == 0){
            $toDate = strtotime(date("Y-m-d"));
            $fromDate = strtotime(date("Y") . "-1-1");
        }
        else{
            $toDate = mktime(0, 0, 0, 12, 31, $params->year_nofilter);
            $fromDate = mktime(0, 0, 0, 1, 1, $params->year_nofilter);
        }
        //print($fromDate . " " . $toDate);
        $res = $objKpi->get_data($fromDate, $toDate);
        if($res){
            print(json_encode($res));
        }
        else{
                print("[]");
        }
    }
    
    public function wosTrendsAction(){
        $objKpi->update();
        $objKpi->read();
    }
    
    public function testAction(){

        $objKpi = new Mainsim_Model_Kpi();
        $objKpi->updateOpenedWo();
        
    }
    
    public function dataAction(){
        $objKpi = new Mainsim_Model_Kpi();
        print($objKpi->data($_POST['type'], $_POST['period']));
        die();
    }
    
    public function setupAction(){
        $aux = $this->getAllParams();
        $this->_helper->viewRenderer->setNoRender(false); 
        $this->render($aux['type']);
    }
    
    public function selectAction(){
        $objKpi = new Mainsim_Model_Kpi();
        print(json_encode($objKpi->selectOptions($_POST['type'])));
        die();
    }
    
    
}
