<?php

class RegistrationController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();        
        //$this->_helper->layout()->setLayout('login');        
        //$this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function indexAction()
    {
        $obj = new Mainsim_Model_Registration();
        $this->_helper->layout()->setLayout('external');
        $this->view->language = $obj->get_language();
        $this->view->dictionary = $obj->get_translations($this->view->language);
        // captcha definition
        $this->view->captchaDir = $this->getRequest()->getServer('HTTP_HOST');
        if($this->view->captchaDir == 'localhost' || in_array($this->view->captchaDir, array('192.168.0.5', '192.168.0.10'))){
            $this->view->captchaDir = 'http://' . $this->view->captchaDir . '/mainsim3/' . $obj->captchaDir;
        }
        else{
            $this->view->captchaDir = 'http://' . $this->view->captchaDir . '/' . $obj->captchaDir;
        }
        $this->view->captchaID = $obj->createCaptcha();
    
        // setting
        $this->view->wRegcf = $obj->Regcf; 
    }
    
    public function mobileAction(){
        $this->redirect('registration/index');
    }
    
    public function getdataAction()
    {
        //echo $_POST;
        $obj = new Mainsim_Model_Registration();
        $result = $obj->newUser($_POST);
        
        //echo "<script> alert('" . json_encode($result) . "'); </script>";  
        $this->view->error_message_code = $result['code'];
        if($result['code'] == 'OK'){
        
            $this->view->message = $result;
            
        }
        else{
        
            $this->view->wRegcf = $obj->Regcf;
            
           // print_r( $obj->Regcf ); die;
            
            $this->view->postData = $_POST;
            $this->view->language = $obj->get_language();
            $this->view->dictionary = $obj->get_translations($this->view->language);
            $this->view->message = $result['message'];
            $this->view->captchaID = $obj->createCaptcha();
            $this->view->captchaDir = '../' . $obj->captchaDir;
            $this->render('index');
            //$this->view->result = $result;     
        }  
        
        
    }
    
  /* public function getdatamailchimpAction($data)
    {
        $obj = new Mainsim_Model_Registration();
        $result = $obj->newUsermailchimp($data);
        if($result['code'] == 'OK'){
            $this->view->message = $result;
        }
        else{
            $this->view->postData = $data;
            $this->view->language = $obj->get_language();
            $this->view->dictionary = $obj->get_translations($this->view->language);
            $this->view->message = $result['message'];
            $this->view->captchaID = $obj->createCaptcha();
            $this->view->captchaDir = '../' . $obj->captchaDir;
            $this->render('index');
            //$this->view->result = $result;
        }   
    }*/
    
    public function autologinAction(){
        $objReg = new Mainsim_Model_Registration();
        
        $password = $objReg->decryptPassword($_GET['pwd']);
        $objLogin = new Mainsim_Model_Login();
        $auth_res = $objLogin->authenticate($_GET['usr'], $password);
        if($auth_res === true){
            $this->_redirect('mainpage');
        }
        else{
            switch ($auth_res) {
                case -3: 
                case -1: $this->view->errorMessage = "Incorrect username or password"; break;
                case 'DOUBLE_LOGIN':$this->view->errorMessage = "mainsim session already open"; break;
                // application maintenance
                case -100 :
                    $this->_redirect('index/maintenance');
                    break;
                default: $this->view->errorMessage = "Database error"; break;                                                
            }
            $this->_redirect('login/login');
        }
    }
    
    public function privacyAction(){
        
        $this->_helper->layout()->setLayout('void');
        $objReg = new Mainsim_Model_Registration();
        $this->view->privacyText = $objReg->get_privacy();
    }
    
    public function testAction(){  
    }
    
    
}

