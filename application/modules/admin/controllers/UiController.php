<?php

class Admin_UiController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function editAction()
    {
        $msg = array();
        try {
            $data = json_decode($_POST["params"], true);
            $data['f_instance_name'] = addslashes(strip_tags($data['f_instance_name']));
            $ui = new MainsimAdmin_Model_UI();
            unset($data['f_module_name']);
            $res = $ui->editUi(array(
                "f_code" => $data["f_code"],
                "f_instance_name" => $data["f_instance_name"],
                "f_type_id" => $data["f_type_id"],
                "f_properties" => $data["f_properties"]
            ));
            if(is_array($res)) {
                $msg = $res;
            }
            if(!$res) {
                $msg["message"] = "You cannot save this UI with the same name of another one";
            }
        }catch(Exception $e) {
            $msg = array("message"=>"SQL error : ".$e->getMessage());         
        }
        echo json_encode($msg); die;
    }
    
    public function cloneUiAction()
    {
        $msg = array();
        try {
            if(!empty($_POST)) {
                $data = $_POST;
                $ui = new MainsimAdmin_Model_UI();
                $ui->cloneUi($data);        
            }
        } catch(Exception $e) {
            $msg = array("message"=>"SQL error : ".$e->getMessage());         
        }
        echo json_encode($msg); die;
    }
    
    public function newAction()
    {
        //Check        
        if(empty($_POST) && !ctype_digit($_POST['f_code'])) { die; }
        $data = json_decode($_POST['params'],true);
        $data['f_instance_name'] = addslashes(strip_tags($data['f_instance_name']));
        $ui = new MainsimAdmin_Model_UI();
        unset($data['f_module_name']);
        try {
            $res = $ui->newUi($data);
            $msg = array("f_code" => $res);
        } catch(Exception $e) {
            $msg = array("message"=>"Error : ".  Mainsim_Model_Utilities::chg($e->getMessage()));
        }        
        echo json_encode($msg); die;
    }
    
    public function deleteAction()
    {
        //Controlli
        if(empty($_POST) && !ctype_digit($_POST['f_code'])) {
            die;
        }        
        $data = $_POST;        
        $ui = new MainsimAdmin_Model_UI();
        echo json_encode($ui->deleteUi($data));
    }
    
    public function cloneAllAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->view->cloned = 0;
        $msg = array();
        try {
            if(isset($_POST['search']) && isset($_POST['replace'])){            
                $ui = new MainsimAdmin_Model_UI();
                $ui->cloneAll($_POST['search'],$_POST['replace']);
                $this->view->cloned = 1;
            }    
        }catch(Exception $e) {
            $msg['message'] = array("message"=>"SQL error : ".$e->getMessage());         
        }
        echo json_encode($msg);die;
    }
    
    /**
     * @deprecated since version 3.0.6.4
     */
    public function layoutDesignAction() {
        $this->_helper->layout()->enableLayout();
        $this->_helper->viewRenderer->setNoRender(false);
    }
    
    /**
     * @deprecated since version 3.0.6.4
     */
    public function tabbarDesignAction() {
        $this->_helper->layout()->enableLayout();
        $this->_helper->viewRenderer->setNoRender(false);
    }
    
    /**
     * @deprecated since version 3.0.6.4
     */
    public function componentDesignAction() {
        $this->_helper->layout()->enableLayout();
        $this->_helper->viewRenderer->setNoRender(false);
    }
    
    /**
     * @deprecated since version 3.0.6.4
     */
    public function menuDesignAction() {
        $this->_helper->layout()->enableLayout();
        $this->_helper->viewRenderer->setNoRender(false);
    }
    
    public function getModuleNamesAction() {
        $ui = new MainsimAdmin_Model_UI();
        echo json_encode($ui->getModuleNames());
//        $modules = array(array("label" => "Select", "value" => "", "selected" => true, "level" => -1));var_dump($mods);
//        foreach($mods as $mod) {
//            $modules[] = array(
//                "label" => $mod["f_type"],
//                "value" => $mod["f_short_name"],
//                "selected" => false,
//                "level" => -1
//            );
//        }
//        echo json_encode($modules);
    }
    
    public function getSubmoduleNamesAction() {
        $ui = new MainsimAdmin_Model_UI();
        $mods = $ui->getModuleNames();
        $modules = array(array("label" => "Select", "value" => "", "selected" => true, "level" => -1));
        foreach($mods as $mod) {
            $modules[] = array(
                "label" => $mod["f_type"],
                "value" => $mod["f_short_name"],
                "selected" => false,
                "level" => -1
            );
        }
        $modules[] = array("label" => "PARENT", "value" => "parent", "selected" => false, "level" => -1);
        $modules[] = array("label" => "HISTORY", "value" => "history", "selected" => false, "level" => -1);
        echo json_encode($modules);
    }
    
    public function getSelectorTypesAction() {
        $ui = new MainsimAdmin_Model_UI();
        $sels = $ui->getSelectorTypes();
        foreach($sels as $sel) {
            $selectors[] = array(
                "label" => $sel["f_type"],
                "value" => $sel["f_id"],
                "selected" => false,
                "level" => -1
            );
        }
        echo json_encode($selectors);
    }
}