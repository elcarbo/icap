<?php

class Admin_TreegridController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function jaxReadRootChkAction()
    {        
        $special_tabs = array("t_workflows");        
        if(!isset($_POST["param"])) exit("error0"); 
        
        $in=json_decode($_POST["param"]);
        
        //$tree = new MainsimAdmin_Model_Treegrid();       
        if($in->Tab[0] == 't_ui_object_instances' || $in->Tab[0] == 't_selectors') $tree = new MainsimAdmin_Model_Treegrid();
        else $tree = new Mainsim_Model_Treegrid();
        
        $jax = "jax_read_root_chk";
        if(in_array($in->Tab[0],$special_tabs)) {
            $index = array_search($in->Tab[0], $special_tabs);
            $tab = str_replace("t_", "", $special_tabs[$index]);
            $jax.="_$tab";
        }elseif($in->Tab[0] == 't_selectors') {
            $jax = "jax_read_root_sel";
        }       
        
        $result = $tree->$jax($in);
        
        echo json_encode($result);die;    
    }
    
    public function jaxReadCheckAction()
    {      
        $special_tabs = array("t_workflows");
        $tree = new MainsimAdmin_Model_Treegrid();
        if(!isset($_POST["param"])) exit("error0"); 
        $in=json_decode($_POST["param"]);
        $jax = "jax_read_check";
        if(in_array($in->Tab[0],$special_tabs)) {
            $index = array_search($in->Tab[0], $special_tabs);
            $tab = str_replace("t_", "", $special_tabs[$index]);
            $jax.="_$tab";
        }        
        $result = $tree->$jax($in);        
        echo json_encode($result);die;    
    }
       
    
    public function jaxReadChildAction()
    {
        if(!isset($_POST["param"])) exit("error0"); 
        $in=json_decode($_POST["param"]);

        if($in->Tab[0] == 't_ui_object_instances') $tree = new MainsimAdmin_Model_Treegrid();
        else $tree = new Mainsim_Model_Treegrid();
        
        $result = $tree->jax_read_child($in);
        echo json_encode($result);die;       
    }
    
    public function jaxReadRootAction()
    {
        //$tree = new MainsimAdmin_Model_Treegrid();       
        if(!isset($_POST["param"])) exit("error0"); 
        $in=json_decode($_POST["param"]);
        
        if($in->Tab[0] == 't_ui_object_instances') $tree = new MainsimAdmin_Model_Treegrid();
        else $tree = new Mainsim_Model_Treegrid();
        
        $result = $tree->jax_read_root($in);
        echo json_encode($result);die;    
    }
    
    public function jaxLockAction()
    {
        $tree = new Mainsim_Model_Treegrid();
        $par=json_decode($_POST["param"]);
        $result = $tree->jax_lock($par);
        echo json_encode($result);die;
    }
    
    public function jaxLockOtherUserAction()
    {
        $result = array();        
        if(isset($_POST['param']) && !empty($_POST['param'])) {
            $par=json_decode($_POST["param"]);
            $tree = new Mainsim_Model_Treegrid();
            $result = $tree->jax_lockotheruser($par);            
        }
        echo json_encode($result);die;
    }    
}