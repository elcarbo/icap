<?php

class Admin_SelectController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function getOptionsAction()
    {
        $select = new MainsimAdmin_Model_OptionSelect();
        $categories = $select->getOptions($_POST["category"]);
        echo $categories[0]['f_properties'];die;
    }
    
    public function getCategoryTypesAction()
    {
        $excep = new MainsimAdmin_Model_OptionSelect();
        $res = $excep->getCategoryTypes($_POST["table"]);
        echo json_encode($res);die;
    }
}