<?php

class Admin_IndexController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->setLayout('login');
        //$this->_helper->viewRenderer->setNoRender(true);
    }

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        if(Zend_Auth::getInstance()->hasIdentity()) {            
            $this->redirect("/admin/mainpage");
        }
        else {
        $this->redirect('/index');
            
        }
    }
    
    public function mainpageAction() 
    {
        
    }
}

