<?php

class Admin_DatamanagerController extends Zend_Controller_Action
{
    public function preDispatch() 
    {
        parent::preDispatch();                
    }
    
    public function indexAction()
    {
        
    }
    
    public function getTypesAction()
    {
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
        $dmn = new MainsimAdmin_Model_Datamanager();
        $tableName = $_POST['table'];
        
        $res = $dmn->getTypes($tableName);
        echo $res;
        die;
        
    }
    
    
    public function deleteAction()
    {
        $this->_helper->layout()->disableLayout();                
        $dmn = new MainsimAdmin_Model_Datamanager();
        $db_name = $dmn->getDb();        
        $this->view->db_name = $db_name;       
        $strage = 0; $deleted = 0;
        if(!empty($_POST)) {
            /*****************************************************/
            $params = json_decode($_POST["params"], true);
            $this->_helper->viewRenderer->setNoRender(true);            
            /*****************************************************/                 
            if(isset($params['types']) && $params['strage'] != 1) {
                $resp = $dmn->clearObj($params['table'],$params['types'],$params['delete_ui'],$params['delete_bm']);
            } 
            elseif($params['strage'] == 1){
                $resp = $dmn->destroyAll();                
            }
            echo json_encode($resp);die;
        }
        $this->view->strage = $strage;
        $this->view->deleted = $deleted;        
    }
    
    public function getTypes()
    {
        $response = array();
        $dmn = new MainsimAdmin_Model_Datamanager();
        if(strpos($_POST['table'],"cross") === false ){
            $response = $dmn->getTypes($_POST['table']);
        }
        elseif($_POST['table'] == 'cross') {
            $response = array(
                't_ware_wo','t_wares_relations','t_wares_systems',
                't_selector_ware','t_selector_wo','t_selector_system',
                't_wo_relations','t_pair_cross','t_periodics'
            );
        }        
        echo json_encode($response);die;
    }
    
    public function instantDataLoaderAction()
    {
        $this->_helper->layout()->disableLayout();        
        $this->_helper->viewRenderer->setNoRender(true);
        if(isset($_POST['f_code']) && is_numeric($_POST['f_code'])) {
            $code = $_POST['f_code'];
            $pos = $_POST['pos'];
            $iF = $_POST['isFinish'];
            $sys = new MainsimAdmin_Model_Datamanager();
            $res = $sys->instantDataLoader($code, $pos,$iF);
            echo json_encode($res);die;
        }
    }
    
    public function woGeneratorAction()
    {
        $imported = false;
        if(!empty($_POST)) {
            $start_date = 0;
            $end_date = time();    
            $number = 0;
            $type = 0;
            if(!empty($_POST['start_date'])) {
                $start_date = date('Y',strtotime($_POST['start_date'])) != '1970' ?strtotime($_POST['start_date']):0;
            }    
            if(!empty($_POST['end_date'])) {
                $end_date = date('Y',strtotime($_POST['end_date'])) != '1970'?strtotime($_POST['end_date']):time();
            }    
            if($_POST['number_of_creation']) {
                $number = (int)$_POST['number_of_creation'];        
            }
            if($_POST['f_type_id']) {
                $type = (int)$_POST['f_type_id'];        
            }
            $reader = new Mainsim_Model_PMReader();
            $this->view->number_created = $reader->readPeriodics($start_date,$end_date,$number,false,$type);
            $imported = true;
        }
        $this->view->imported = $imported;
    }    
}