<?php

class Admin_PdfWriterController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function indexAction()
    {
        $pdf = new MainsimAdmin_Model_Pdf();
        $list = $pdf->getPdfList();
        $this->view->list = $list['f_name'];        
    }
    
    public function getPdfAction() 
    {        
        $this->_helper->viewRenderer->setNoRender(true);
        $pdf = new MainsimAdmin_Model_Pdf();
        $res = $pdf->getPdf($_POST['f_id']);
        echo $res;
        die;
    }
    
    public function importAction()
    {
        
    }
    
    public function savePdfAction() 
    {
        $this->_helper->viewRenderer->setNoRender(true);				
        $params = json_decode($_POST['params'],true);                
        $params['f_script'] = html_entity_decode(utf8_decode(base64_decode($params['f_script'])));        
        $pdf = new MainsimAdmin_Model_Pdf();
        $message = $pdf->save($params);        
        echo json_encode($message);die;
    }
    
    public function printPdfAction()
    {
        $params = $this->_getAllParams();
        $pdfCreator = new Mainsim_Model_PdfCreator();
        $pdf = $pdfCreator->printPdf($params);
        $pdf->Output();die;
    }
    
}