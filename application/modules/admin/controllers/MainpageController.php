<?php

    
class Admin_MainpageController extends Zend_Controller_Action
{
    
    /*public function preDispatch() {
        parent::preDispatch();
        //$this->_helper->layout()->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
    }*/
    
    public function indexAction()
    {   
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect("/index");
        }        
        $userinfo = Zend_Auth::getInstance()->getIdentity();

        $main = new Mainsim_Model_Mainpage();
        //get language info
        $translator = new Mainsim_Model_Translate();                
        $_SESSION['LANG'] = $translator->getLanguage($translator->getLang());        
      
        $this->view->LANG = json_encode($_SESSION['LANG']);
        //$info_sys = $ui->getInfoSystem();
        $settings = $main->getSettings();
        foreach($settings as $k => $v) {
            $_SESSION[$k] = $v;
        }
        $this->view->signature = $_SESSION['SIGNATURE'];
        $this->view->version = APP_VERSION;
        $this->view->translate = new Mainsim_Model_Translate();
        
        $this->view->calendar = (isset($_SESSION["HEADER_CALENDAR"]) && (intval($_SESSION["HEADER_CALENDAR"]) & intval($userinfo->f_level)));
        $this->view->help = (isset($_SESSION["HEADER_HELP"]) && (intval($_SESSION["HEADER_HELP"]) & intval($userinfo->f_level)));
        $this->view->user_guide = (isset($_SESSION["HEADER_USER_GUIDE"]) && (intval($_SESSION["HEADER_USER_GUIDE"]) & intval($userinfo->f_level)));
        $this->view->bulletin = (isset($_SESSION["HEADER_BULLETIN"]) && (intval($_SESSION["HEADER_BULLETIN"]) & intval($userinfo->f_level)));
        $this->view->users_connected = (isset($_SESSION["HEADER_USERS_CONNECTED"]) && (intval($_SESSION["HEADER_USERS_CONNECTED"]) & intval($userinfo->f_level)));
    }
    
    public function maininitAction()
    {
        $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $ui = new MainsimAdmin_Model_UI();
        $data = array();
                
        $data['tabbars'] = $ui->getTabbars($tabbar_edit_list);
        $data['textfields'] = $ui->getTextfields($tabbar_edit_list);
        $data['picklists'] = $ui->getPicklists($tabbar_edit_list);
        $data['checkboxes'] = $ui->getCheckboxes($tabbar_edit_list);
        $data['menus'] = $ui->getMenus($tabbar_edit_list);
        $data['buttons'] = $ui->getButtons($tabbar_edit_list);
        $data['modules'] = $ui->getModules($tabbar_edit_list);
        $data['layouts'] = $ui->getLayouts($tabbar_edit_list);
        $data['selects'] = $ui->getSelects($tabbar_edit_list);
        $data['images'] = $ui->getImages($tabbar_edit_list);
        
        //BASEURL
        $data["global"] = array();
        
        //---------Options
        $data["global"]["options"] = $ui->getOptions();
        
        $weekStartsOn = 1;
        $data["global"]['weekStartsOn'] = $weekStartsOn;
//        $info_sys = $ui->getInfoSystem(); 
//        $data["global"]["dateFormat"] = "{l}, {F} {d} {Y}";
//        $data["global"]["timeFormat"] = "{H}:{i}";
//        $data["global"]["dateTimeFormat"] = $info_sys['f_date_format_long']; // "{l}, {F} {d} {Y}, {H}:{i}"
//        $data["global"]["dateShortFormat"] =  $info_sys['f_date_format_short']; // "{m}/{d}/{Y}, {H}:{i}"        

        $login = new Mainsim_Model_Login();        
        $data["global"]['online'] = $login->getOnlineNum();        
        $data["global"]['user'] = $this->getUserData();
        $data["global"]['baseUrl'] = $baseUrl."/admin";
        $data["global"]['imgDefaultPath'] = str_replace("/admin", "", IMAGE_DEFAULT_PATH);
        $data["global"]['imgPath'] = $data["global"]['imgDefaultPath'];
        $data["global"]['attachPath'] = ATTACHMENT_PATH;
        $data["global"]['libraryPath'] = LIBRARY_PATH;
        
        /**
         * Settings
         */
        $main = new Mainsim_Model_Mainpage();
        $settings = $main->getSettings();
        foreach($settings as $k => $v) {
            $data["global"][$k] = Mainsim_Model_Utilities::chg($v);
        }

        /**
         * Delegates
         */
        $data["global"]["delegates"] = array();
        $q = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        $q->from("t_scripts")->where("f_type = 'js'");
        $res = $q->query()->fetchAll();
        foreach($res as $r) $data["global"]["delegates"][$r["f_name"]] = $r["f_script"];
        $q->reset();
        
        /**
         * Localization
         */
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        $data["global"]["localization"] = $ui->getLocalization($userinfo->f_language);
        
        /**
         * Reverse Ajax Initialization
         */
        $data["global"]["revajax"] = array("t_workorders" => array(), "t_wares" => array(), "t_selectors" => array());
        $q = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        foreach($data["global"]["revajax"] as $k => $v) {
            $q->from($k."_types", array("f_id"));
            $res = $q->query()->fetchAll();
            $q->reset();
            foreach($res as $r) $data["global"]["revajax"][$k][$r["f_id"]] = array();
        }
        
        echo json_encode($data);
    }
    
    public function getUserData() {
        $userinfo = Zend_Auth::getInstance()->getIdentity();
  
        $sc = new Mainsim_Model_StartCenter();
        return array(
            "code" => $userinfo->f_id,
            "fullname" => $userinfo->f_displayedname,
            "firstname" => $userinfo->fc_usr_firstname,
            "lastname" => $userinfo->fc_usr_lastname,
            "gender" => $userinfo->f_gender == 0 ? "M" : "F",
            "avatar" => $userinfo->fc_usr_avatar,
            "level" => $userinfo->f_level,
            "groupLevel" => $userinfo->f_group_level,
            "status" => $userinfo->f_status,
            "address" => $userinfo->fc_usr_address,
            "phone" => $userinfo->fc_usr_phone,
            "username" => $userinfo->fc_usr_usn,
            "defaultPassword" => ($userinfo->fc_usr_pwd == '313ed0558152d04dc20036dbd850dc9bd'),
            //"defaultProject" => $userinfo->fc_usr_default_project,          /* MULTI PROJECT */
            "accountExpiration" => $userinfo->fc_usr_usn_expiration,
            "pwdExpiration" => $userinfo->fc_usr_pwd_expiration,
            "accountCreation" => $userinfo->fc_usr_usn_registration,
            "email" => $userinfo->fc_usr_mail,
            "language" => $userinfo->f_language,
            "bulletins" => count($sc->getBulletins())
        );        
    }
    
    public function _maininitAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        $data = array();
        $ui = new MainsimAdmin_Model_UI();
        //--------Textfields
        $type_id_search = $ui->setSearch('textfield');
        if($type_id_search) {
            $result_textfields = $ui->getUI();
        }
        else {
            die('Type inesistente');
        }        
        $textfields = $result_textfields;               
        $data['textfields'] = $textfields;
        
        //--------Picklists
        $data['picklists'] = array();
        $type_id_search = $ui->setSearch('picklist');
        if($type_id_search) {
            $result_picklists = $ui->getUI();
            $picklists = $result_picklists;               
            $data['picklists'] = $picklists;
        }
        
        //-----------checkboxes
        $type_id_search = $ui->setSearch('checkbox');
        if($type_id_search) {
            $result_checkboxes = $ui->getUI();
        }
        else {
            die('Type inesistente');
        }                        
        $checkboxes = $result_checkboxes;        
        $data['checkboxes'] = $checkboxes;
        
        //-----------menus
        $type_id_search = $ui->setSearch('menu');
        if($type_id_search) {
            $result_checkboxes = $ui->getUI();
        }
        else {
            die('Type inesistente');
        }                        
        $checkboxes = $result_checkboxes;        
        $data['menus'] = $checkboxes;        

        //---------Buttons
        $type_id_search = $ui->setSearch('button');
        if($type_id_search) {
            $result_buttons = $ui->getUI();
        }
        else {
            die('Type inesistente');
        }                        
        $buttons = $result_buttons;        
        $data['buttons'] = $buttons;        
                
        //---------Modules
        $type_id_search = $ui->setSearch('module');
        if($type_id_search) {
            $result_modules = $ui->getUI();
        }
        else {
            die('Type inesistente');
        }
        $modules = $result_modules;                    
        $data['modules'] = $modules;
//        foreach($data['modules'] as &$line) {
//            if(isset($line['url'])) {
//                $line['url'] = str_replace("BASEURL", BASEURL, $line['url']);
//            }
//        }
        foreach($data['modules'] as &$line) {                
            if(strpos($line['src'],"BASEURL") !== false) {                
                $line['src'] = str_replace("BASEURL", BASEURL, $line['src']);                
            }            
        }
        //Zend_Debug::dump($modules);die;
        //---------Layouts
        $type_id_search = $ui->setSearch('layout');
        if($type_id_search) {
            $result_layouts = $ui->getUI();
        }
        else {
            die('Type inesistente');
        }                
        $layouts = $result_layouts;         
        $data['layouts'] = $layouts;        
        
        //---------Tabbars
        $type_id_search = $ui->setSearch('tabbar');
        if($type_id_search) {
            $result_tabbars = $ui->getUI();
        }
        else {
            die('Type inesistente');
        }        
        $tabbars = $result_tabbars;                        
        $data['tabbars'] = $tabbars;  
        
        //---------Images
        $type_id_search = $ui->setSearch('image');
        if($type_id_search) {
            $result_images = $ui->getUI();
        }
        else {
            die('Type inesistente');
        }        
        $images = $result_images;                        
        $data['images'] = $images;  
        
        //---------Selects
        $type_id_search = $ui->setSearch('select');
        if($type_id_search) {
            $result_selects = $ui->getUI();            
        }
        else {
            die('Type inesistente');
        }        
        $selects = $result_selects;
        $data['selects'] = $selects;
        
        //---------Options
        $type_id_search = $ui->setSearch('options');
        if($type_id_search) {
            $res = $ui->getUI();
            $rs = json_decode($res[0], true);
            foreach($res as $r) {
                $opt_name = $r["name"]; unset($r["name"]);
                $data['options'][$opt_name] = $r;
            }
        }
        
        $data['weekStartsOn'] = 1;
        $info_sys = $ui->getInfoSystem(); 
        $data["dateTimeFormat"] = $info_sys['f_date_format_long']; //"{l}, {d} {F} {Y}, {H}:{i}";
        $data["dateShortFormat"] =  $info_sys['f_date_format_short'];//"{d}/{m}/{Y}, {H}:{i}";        
//        $data['sformat'] = $sformat;
//        $data['format'] = $format;
        $login = new Mainsim_Model_Login();
        $data['online'] = $login->getOnlineNum();
        $login = new Mainsim_Model_Login();        
        $data['online'] = $login->getOnlineNum();
        $data['user'] = array(
            "code" => $userinfo->f_id,
            "fullname" => $userinfo->f_displayedname,
            "gender" => $userinfo->f_gender == 1 ? "M" : "F",
            "avatar" => $userinfo->fc_usr_avatar,
            "level" => $userinfo->f_level,
            "bulletins" => 3
        );
        $data['baseUrl'] = BASEURL."/admin";
        $data['imgDefaultPath'] = str_replace("/admin", "", IMAGE_DEFAULT_PATH);
        $data['imgPath'] = $data['imgDefaultPath'];
//        $data['imgPath'] = str_replace("/admin", "", IMAGE_PATH);
        $data['attachPath'] = ATTACHMENT_PATH;
        $data['libraryPath'] = LIBRARY_PATH;
        $data['debugOn'] = DEBUG_ON;
        
        /**
         * Delegates
         */
        $data["delegates"] = array();
        $q = new Zend_Db_Select(Zend_Db::factory(Zend_Registry::get('db')));
        $q->from("t_scripts")->where("f_type = 'js'");
        $res = $q->query()->fetchAll();
        foreach($res as $r) $data["delegates"][$r["f_name"]] = $r["f_script"];
        $q->reset();
        
        echo json_encode($data);
    }
}