<?php


class Admin_DbUpdaterController extends Zend_Controller_Action
{
    public function preDispatch() 
    {
        $this->_helper->layout()->disableLayout(); 
        parent::preDispatch();                
    }
    
    public function indexAction()
    {
        
    }
    
    public function readIniAction()
    {
        $dbupdater = new MainsimAdmin_Model_Dbupdater();
        echo $dbupdater->readIniFile($_GET["par"]);die;
    }
    
    public function saveIniAction()
    {
        $fname = $_GET["par"];
        $dbupdater = new MainsimAdmin_Model_Dbupdater();        
        $config=$_POST["params"];  
        if($config=="#delete#"){
            $dbupdater->deleteIniFile($fname);
        } else {  
            $dbupdater->saveIniFile($fname, $config);
        }
        die("ok");        
    }
    
    public function beginWorksAction()
    {
        if(empty($_POST['params'])) { echo "empty params";die;}
        $params = json_decode($_POST['params'],true);
        $dbupdater = new MainsimAdmin_Model_Dbupdater();
        $res = $dbupdater->applyAction($params['db_master'],$params['db_slave'],$params['action'],$params['limit']);
        echo json_encode($res);die;
    }
    
    public function getDbListAction()
    {
        if(empty($_POST)) { echo "empty params";die;}
        $dbupdater = new MainsimAdmin_Model_Dbupdater();
        $res = $dbupdater->getDbList($_POST['dbtype']);
        echo json_encode($res);die;
    }
}
