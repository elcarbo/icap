<?php

class Admin_WorkflowsController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        //$this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function indexAction()
    {
        
    }
    
    public function wfListAction()
    {
        $this->_helper->layout->disableLayout();
        $wf = new MainsimAdmin_Model_Workflows();
        echo json_encode($wf->wfList());
    }
    
    public function wfGroupsAction()
    {
        $this->_helper->layout->disableLayout();
        $wf = new MainsimAdmin_Model_Workflows();
        echo json_encode($wf->wfGroups());
    }
    
    public function getWfAction()
    {
        $this->_helper->layout->disableLayout();
        if(!isset($_POST["wfn"])) die(json_encode(array("error" => "Workflow id not found")));
        $wf = new MainsimAdmin_Model_Workflows();        
        echo json_encode($wf->getWf($_POST["wfn"]));
    }
    
    public function saveWfAction()
    {
        $this->_helper->layout->disableLayout();
        if(!isset($_POST["par"])) die(Mainsim_Model_Utilities::chg(json_encode(array("error" => "Missing parameter"))));        
        $wf = new MainsimAdmin_Model_Workflows();        
        $params = json_decode($_POST['par'],true);                
        echo json_encode($wf->saveWf($params));
    }
    
    public function printWfAction()
    {
        $this->_helper->layout->disableLayout();        
        $wf = new MainsimAdmin_Model_Workflows();        
        $params = $this->_getAllParams();              
        echo json_encode($wf->printWf($params));
    }
    
    public function createWfAction()
    {
        $this->_helper->layout->disableLayout();
        if(!isset($_POST["f_name"])) die(json_encode(array("error" => "Missing workflow name")));        
        $wf = new MainsimAdmin_Model_Workflows();
        echo json_encode($wf->createWf($_POST["f_name"]));
    }
    
    public function deleteWfAction()
    {
        $this->_helper->layout->disableLayout();
        if(!isset($_POST["wf"])) die(json_encode(array("error" => "Missing workflow id")));
        $wf = new MainsimAdmin_Model_Workflows();
        echo json_encode($wf->deleteWf($_POST["wf"]));
    }
    
    public function wfAction() {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->_helper->layout->disableLayout();
        $wf = new MainsimAdmin_Model_Workflows();
        $this->view->assign("wfg", json_encode(array($wf->wfList(), $wf->wfGroups())));
    }
}