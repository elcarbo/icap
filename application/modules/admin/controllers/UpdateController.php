<?php

class UpdateController extends Zend_Controller_Action
{
    public function preDispatch() 
    {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout(true);
        $this->_helper->viewRenderer->setNoRender(true);
        //Zend_session::writeClose();
    }
    
    /**
     * Convert properties mandatory, readonly, visible and disabled and level_edit
     * of ui components in new properties m_on, r_on, v_on, d_on and r_level
     * WARNING!
     * Conversion for t_wf missing, do it by yourself! :)
     */
    public function uiConvertPropertiesAction()
    {
        $update = new Mainsim_Model_Update();
        var_dump($update->uiConvertProperties(isset($_GET["admin"])?'admin_':''));
    }
    
    /**
     * Import languages from file to database setting as new category
     * for system settings type
     */
    public function langArrayToSystemSettingsAction()
    {
        $update = new Mainsim_Model_Update();
        $res = $update->langArrayToSystemSettings();
        foreach($res as $r) {
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div><br/>\n";
            else for($i=0; $i<count($r["error"]); $i++) echo "<div style='color: red;'>{$r["error"][$i]}</div><br/>\n";
        }
    }
    
    /**
     * Convert edit modules to support dynamic generation of bottom
     * command bars
     */
    public function convertEditModuleToCbAction() {
        $update = new Mainsim_Model_Update();
        $res = $update->convertEditModuleToCb(isset($_GET["isadmin"]));
        foreach($res as $r) {
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div><br/>\n";
            else if(isset($r["error"])) echo "<div style='color: red;'>{$r["error"]}</div><br/>\n";
        }
    }
    
    /**
     * Convert properties who contain a truth value in boolean, i.e.
     * 1 => true, "true" => true, 0 => false, "false" => false
     */
    public function convertUiTruthValuesToBooleanAction() {
        $update = new Mainsim_Model_Update();
        $res = $update->convertUiTruthValuesToBoolean();
        foreach($res as $r) {
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div><br/>\n";
            else if(isset($r["error"])) echo "<div style='color: red;'>{$r["error"]}</div><br/>\n";
        }
    }
    
    /**
     * Remove all unuseful properties from ui
     */
    public function cleanUiAction() {
        $update = new Mainsim_Model_Update();
        $res = $update->cleanUi();
        foreach($res as $r) {
            if(isset($r["message"])) echo "<div style='color: green;'>{$r["message"]}</div><br/>\n";
            else if(isset($r["error"])) echo "<div style='color: red;'>{$r["error"]}</div><br/>\n";
        }
    }
    
    public function getUiComponentListAction() {
        $update = new Mainsim_Model_Update();
        $uis = $update->getUiComponentList();
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=fields.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "f_instance_name;bind;label;label italian;info;info italian\r\n";
        foreach($uis as $k => $comp) {
            echo "$k;;;;;\r\n";
            foreach($comp as $ui) {
                echo $ui["f_instance_name"].";".$ui["bind"].";".$ui["label"].";".$ui["label_it"].";".$ui["info"].";".$ui["info_it"]."\r\n";
            }
        }
    }
    
    public function setUiPropertiesAction() {
        $update = new Mainsim_Model_Update();        
        $uis = array(
            'chkbx_wo_edit_documents'=>array('level' => 1,'level_edit' => -205),
            'slct_wo_edit_keyword'=>array('level' => 2,'level_edit' => -204),
            'slct_wo_edit_priority_fc'=>array('level' => 3,'level_edit' => -203),
            'txtfld_wo_edit_additional_notes'=>array('level' => 4,'level_edit' => -202),
            'txtfld_wo_edit_corrective_action_required'=>array('level' => 5,'level_edit' => -201),
            'txtfld_wo_edit_crew_hours'=>array('level' => 6,'level_edit' => -200),
            'txtfld_wo_edit_deck'=>array('level' => 7,'level_edit' => -199),
            'txtfld_wo_edit_description'=>array('level' => 8,'level_edit' => -198),
            'txtfld_wo_edit_details'=>array('level' => 9,'level_edit' => -197),
            'txtfld_wo_edit_failure_date'=>array('level' => 10,'level_edit' => -196),
            'txtfld_wo_edit_fc_responsible'=>array('level' => 11,'level_edit' => -195),
            'txtfld_wo_edit_fr_max'=>array('level' => 12,'level_edit' => -194),
            'txtfld_wo_edit_fr_min'=>array('level' => 13,'level_edit' => -193),
            'txtfld_wo_edit_gfr_number'=>array('level' => 14,'level_edit' => -192),
            'txtfld_wo_edit_hours_required_owner'=>array('level' => 15,'level_edit' => -191),
            'txtfld_wo_edit_hull_number'=>array('level' => 16,'level_edit' => -190),
            'txtfld_wo_edit_item_number'=>array('level' => 17,'level_edit' => -189),
            'txtfld_wo_edit_list_material_required'=>array('level' => 18,'level_edit' => -188),
            'txtfld_wo_edit_manufacturer'=>array('level' => 19,'level_edit' => -187),
            'txtfld_wo_edit_mvz'=>array('level' => 20,'level_edit' => -186),
            'txtfld_wo_edit_notification_date'=>array('level' => 21,'level_edit' => -185),
            'txtfld_wo_edit_other_estimeted_cost'=>array('level' => 22,'level_edit' => -184),
            'txtfld_wo_edit_part_number'=>array('level' => 23,'level_edit' => -183),
            'txtfld_wo_edit_position'=>array('level' => 24,'level_edit' => -182),
            'txtfld_wo_edit_priority_sa'=>array('level' => 25,'level_edit' => -181),
            'txtfld_wo_edit_running_hours'=>array('level' => 26,'level_edit' => -180),
            'txtfld_wo_edit_ship_department'=>array('level' => 27,'level_edit' => -179),
            'txtfld_wo_edit_ship_name'=>array('level' => 28,'level_edit' => -178),
            'txtfld_wo_edit_system_equipment'=>array('level' => 29,'level_edit' => -177),
            'txtfld_wo_edit_tag_number'=>array('level' => 30,'level_edit' => -176),
            'txtfld_wo_edit_tech_documentation'=>array('level' => 31,'level_edit' => -175),
            'txtfld_wo_edit_title'=>array('level' => 32,'level_edit' => -174),
            'txtfld_wo_edit_wbs'=>array('level' => 33,'level_edit' => -173),
            'btn_wo_edit_send_reject_communication_owner'=>array('level' => 35,'level_edit' => -171),
            'btn_wo_edit_status_attach'=>array('level' => 36,'level_edit' => -170),
            'chkbx_wo_edit_confirmed_closed_by_owner'=>array('level' => 37,'level_edit' => -169),
            'chkbx_wo_edit_dry_doc_status'=>array('level' => 38,'level_edit' => -168),
            'chkbx_wo_edit_reject'=>array('level' => 39,'level_edit' => -167),
            'chkbx_wo_edit_rejection_agree_owner'=>array('level' => 40,'level_edit' => -166),
            'chkbx_wo_edit_transato_senza_intervento'=>array('level' => 41,'level_edit' => -165),
            'slct_wo_edit_milestone'=>array('level' => 42,'level_edit' => -164),
            'txtfld_wo_edit_corrective_action_foreseen'=>array('level' => 43,'level_edit' => -163),
            'txtfld_wo_edit_criticita_status'=>array('level' => 44,'level_edit' => -162),
            'txtfld_wo_edit_data_chiusura_gfr_status'=>array('level' => 45,'level_edit' => -161),
            'txtfld_wo_edit_data_effettiva_chiusura_gfr'=>array('level' => 46,'level_edit' => -160),
            'txtfld_wo_edit_date_reject_communication'=>array('level' => 47,'level_edit' => -159),
            'txtfld_wo_edit_days_ship_not_available'=>array('level' => 48,'level_edit' => -158),
            'txtfld_wo_edit_effective_date_reject'=>array('level' => 49,'level_edit' => -157),
            'txtfld_wo_edit_failure_reoccurred_gfr_no'=>array('level' => 50,'level_edit' => -156),
            'txtfld_wo_edit_field1'=>array('level' => 51,'level_edit' => -155),
            'txtfld_wo_edit_field2'=>array('level' => 52,'level_edit' => -154),
            'txtfld_wo_edit_field3'=>array('level' => 53,'level_edit' => -153),
            'txtfld_wo_edit_field4'=>array('level' => 54,'level_edit' => -152),
            'txtfld_wo_edit_field5'=>array('level' => 55,'level_edit' => -151),
            'txtfld_wo_edit_gfr_phase'=>array('level' => 56,'level_edit' => -150),
            'txtfld_wo_edit_gfr_status'=>array('level' => 57,'level_edit' => -149),
            'txtfld_wo_edit_indisponibilita_nave'=>array('level' => 58,'level_edit' => -148),
            'txtfld_wo_edit_milestone_notes'=>array('level' => 59,'level_edit' => -147),
            'txtfld_wo_edit_next_step'=>array('level' => 60,'level_edit' => -146),
            'txtfld_wo_edit_priority_fc_status'=>array('level' => 61,'level_edit' => -145),
            'txtfld_wo_edit_reopens_failure_gfr_no'=>array('level' => 62,'level_edit' => -144),
            'txtfld_wo_edit_status_notes'=>array('level' => 63,'level_edit' => -143),
            'btn_wo_edit_sendmail'=>array('level' => 65,'level_edit' => -141),
            'chkbx_wo_edit_gfr_assignment'=>array('level' => 66,'level_edit' => -140),
            'chkbx_wo_edit_gfr_bond_consegnato'=>array('level' => 67,'level_edit' => -139),
            'chkbx_wo_edit_gfr_bond_richiesto'=>array('level' => 68,'level_edit' => -138),
            'chkbx_wo_edit_responsibility'=>array('level' => 69,'level_edit' => -137),
            'pck_wo_edit_other_supplier_involved_1'=>array('level' => 70,'level_edit' => -136),
            'pck_wo_edit_other_supplier_involved_2'=>array('level' => 71,'level_edit' => -135),
            'pck_wo_edit_other_supplier_involved_3'=>array('level' => 72,'level_edit' => -134),
            'pck_wo_edit_other_supplier_involved_4'=>array('level' => 73,'level_edit' => -133),
            'pck_wo_edit_other_supplier_involved_5'=>array('level' => 74,'level_edit' => -132),
            'pck_wo_edit_supplier'=>array('level' => 75,'level_edit' => -131),
            'txtfld_wo_edit_acceptance_date'=>array('level' => 76,'level_edit' => -130),
            'txtfld_wo_edit_acceptance_gfr_waiting_time'=>array('level' => 77,'level_edit' => -129),
            'txtfld_wo_edit_gfr_bond_fideiussione'=>array('level' => 78,'level_edit' => -128),
            'txtfld_wo_edit_gfr_bond_importo'=>array('level' => 79,'level_edit' => -127),
            'txtfld_wo_edit_gfr_bond_scadenza'=>array('level' => 80,'level_edit' => -126),
            'txtfld_wo_edit_gfr_oda_description'=>array('level' => 81,'level_edit' => -125),
            'txtfld_wo_edit_gfr_oda_position'=>array('level' => 82,'level_edit' => -124),
            'txtfld_wo_edit_notification_responsibility'=>array('level' => 83,'level_edit' => -123),
            'txtfld_wo_edit_oda'=>array('level' => 84,'level_edit' => -122),
            'txtfld_wo_edit_scadenza_garanzia'=>array('level' => 85,'level_edit' => -121),
            'chkbx_wo_edit_material_fc_charge'=>array('level' => 87,'level_edit' => -119),
            'btn_wo_edit_attach_criticita'=>array('level' => 89,'level_edit' => -117),
            'btn_wo_edit_sendmail_criticita1'=>array('level' => 90,'level_edit' => -116),
            'btn_wo_edit_sendmail_criticita2'=>array('level' => 91,'level_edit' => -115),
            'chkbx_wo_edit_applicabile_altre_navi'=>array('level' => 92,'level_edit' => -114),
            'chkbx_wo_edit_approvazione_responsabile'=>array('level' => 93,'level_edit' => -113),
            'chkbx_wo_edit_attesa_risposta'=>array('level' => 94,'level_edit' => -112),
            'chkbx_wo_edit_chiusura_ente_coinvolto'=>array('level' => 95,'level_edit' => -111),
            'chkbx_wo_edit_criticita'=>array('level' => 96,'level_edit' => -110),
            'chkbx_wo_edit_riaperto_ente'=>array('level' => 97,'level_edit' => -109),
            'chkbx_wo_edit_validazione_responsabile'=>array('level' => 98,'level_edit' => -108),
            'pck_wo_edit_ente_competente'=>array('level' => 99,'level_edit' => -107),
            'pck_wo_edit_ente_conoscenza_1'=>array('level' => 100,'level_edit' => -106),
            'pck_wo_edit_ente_conoscenza_2'=>array('level' => 101,'level_edit' => -105),
            'pck_wo_edit_ente_conoscenza_3'=>array('level' => 102,'level_edit' => -104),
            'pck_wo_edit_ente_conoscenza_4'=>array('level' => 103,'level_edit' => -103),
            'pck_wo_edit_ente_conoscenza_5'=>array('level' => 104,'level_edit' => -102),
            'pck_wo_edit_ente_conoscenza_6'=>array('level' => 105,'level_edit' => -101),
            'pck_wo_edit_ente_conoscenza_7'=>array('level' => 106,'level_edit' => -100),
            'txtfld_wo_edit_acceptance_gfr_waiting_time_ente'=>array('level' => 107,'level_edit' => -99),
            'txtfld_wo_edit_data_invio_gfr_enti'=>array('level' => 108,'level_edit' => -98),
            'txtfld_wo_edit_data_risposta_ente_competenza'=>array('level' => 109,'level_edit' => -97),
            'txtfld_wo_edit_dcm_n_1'=>array('level' => 110,'level_edit' => -96),
            'txtfld_wo_edit_dcm_n_2'=>array('level' => 111,'level_edit' => -95),
            'txtfld_wo_edit_dcm_n_3'=>array('level' => 112,'level_edit' => -94),
            'txtfld_wo_edit_dcm_n_4'=>array('level' => 113,'level_edit' => -93),
            'txtfld_wo_edit_dcm_n_5'=>array('level' => 114,'level_edit' => -92),
            'txtfld_wo_edit_note_riaperto'=>array('level' => 115,'level_edit' => -91),
            'txtfld_wo_edit_numero_costruzione_1'=>array('level' => 116,'level_edit' => -90),
            'txtfld_wo_edit_numero_costruzione_2'=>array('level' => 117,'level_edit' => -89),
            'txtfld_wo_edit_numero_costruzione_3'=>array('level' => 118,'level_edit' => -88),
            'txtfld_wo_edit_numero_costruzione_4'=>array('level' => 119,'level_edit' => -87),
            'txtfld_wo_edit_numero_costruzione_5'=>array('level' => 120,'level_edit' => -86),
            'txtfld_wo_edit_risposta_ente'=>array('level' => 121,'level_edit' => -85),
            'chkbx_wo_edit_cost_supplier'=>array('level' => 123,'level_edit' => -83),
            'chkbx_wo_edit_enable_consolidated_cost_po'=>array('level' => 124,'level_edit' => -82),
            'txtfld_wo_edit_altri_costi_interni'=>array('level' => 125,'level_edit' => -81),
            'txtfld_wo_edit_amount_effective_1'=>array('level' => 126,'level_edit' => -80),
            'txtfld_wo_edit_amount_effective_2'=>array('level' => 127,'level_edit' => -79),
            'txtfld_wo_edit_amount_effective_3'=>array('level' => 128,'level_edit' => -78),
            'txtfld_wo_edit_amount_requested_1'=>array('level' => 129,'level_edit' => -77),
            'txtfld_wo_edit_amount_requested_2'=>array('level' => 130,'level_edit' => -76),
            'txtfld_wo_edit_amount_requested_3'=>array('level' => 131,'level_edit' => -75),
            'txtfld_wo_edit_chargeback_1'=>array('level' => 132,'level_edit' => -74),
            'txtfld_wo_edit_chargeback_2'=>array('level' => 133,'level_edit' => -73),
            'txtfld_wo_edit_chargeback_3'=>array('level' => 134,'level_edit' => -72),
            'txtfld_wo_edit_consolidated_cost_crew'=>array('level' => 135,'level_edit' => -71),
            'txtfld_wo_edit_consolidated_cost_po'=>array('level' => 136,'level_edit' => -70),
            'txtfld_wo_edit_consolidated_cost_po_2'=>array('level' => 137,'level_edit' => -69),
            'txtfld_wo_edit_consolidated_cost_supplier'=>array('level' => 138,'level_edit' => -68),
            'txtfld_wo_edit_consolidated_crew_hours'=>array('level' => 139,'level_edit' => -67),
            'txtfld_wo_edit_consolidated_total_cost'=>array('level' => 140,'level_edit' => -66),
            'txtfld_wo_edit_costi_ore_interne'=>array('level' => 141,'level_edit' => -65),
            'txtfld_wo_edit_costi_spedizioni'=>array('level' => 142,'level_edit' => -64),
            'txtfld_wo_edit_crew_hours_rate'=>array('level' => 143,'level_edit' => -63),
            'txtfld_wo_edit_estimated_cost'=>array('level' => 144,'level_edit' => -62),
            'txtfld_wo_edit_estimated_cost_charged'=>array('level' => 145,'level_edit' => -61),
            'txtfld_wo_edit_item_total_cost'=>array('level' => 146,'level_edit' => -60),
            'txtfld_wo_edit_notes_cost'=>array('level' => 147,'level_edit' => -59),
            'txtfld_wo_edit_ore_interne'=>array('level' => 148,'level_edit' => -58),
            'txtfld_wo_edit_other_consolidated_cost'=>array('level' => 149,'level_edit' => -57),
            'txtfld_wo_edit_settlement'=>array('level' => 150,'level_edit' => -56),
            'txtfld_wo_edit_supplier_1'=>array('level' => 151,'level_edit' => -55),
            'txtfld_wo_edit_supplier_2'=>array('level' => 152,'level_edit' => -54),
            'txtfld_wo_edit_supplier_3'=>array('level' => 153,'level_edit' => -53),
            'txtfld_wo_edit_tariffa_fc'=>array('level' => 154,'level_edit' => -52),
            'btn_wo_planning_edit_load_gfr_solution'=>array('level' => 157,'level_edit' => -49),
            'btn_wo_planning_edit_load_gfr_solution_2'=>array('level' => 158,'level_edit' => -48),
            'btn_wo_planning_edit_load_other_doc'=>array('level' => 159,'level_edit' => -47),
            'btn_wo_planning_edit_sendmail_closing'=>array('level' => 160,'level_edit' => -46),
            'btn_wo_planning_edit_sendmail_service1'=>array('level' => 161,'level_edit' => -45),
            'btn_wo_planning_edit_sendmail_service2'=>array('level' => 162,'level_edit' => -44),
            'chkbx_wo_planning_edit_boarding_confirmed'=>array('level' => 163,'level_edit' => -43),
            'chkbx_wo_planning_edit_cabins_confirmed'=>array('level' => 164,'level_edit' => -42),
            'chkbx_wo_planning_edit_cabins_required'=>array('level' => 165,'level_edit' => -41),
            'chkbx_wo_planning_edit_defective_part_removed_from_ship'=>array('level' => 166,'level_edit' => -40),
            'chkbx_wo_planning_edit_document_availability'=>array('level' => 167,'level_edit' => -39),
            'chkbx_wo_planning_edit_hot_work_required'=>array('level' => 168,'level_edit' => -38),
            'chkbx_wo_planning_edit_inspection_required'=>array('level' => 169,'level_edit' => -37),
            'chkbx_wo_planning_edit_list_of_material'=>array('level' => 170,'level_edit' => -36),
            'chkbx_wo_planning_edit_materiali_inviato'=>array('level' => 171,'level_edit' => -35),
            'chkbx_wo_planning_edit_materiali_manodopera_disponibili'=>array('level' => 172,'level_edit' => -34),
            'chkbx_wo_planning_edit_service_dry_doc'=>array('level' => 173,'level_edit' => -33),
            'chkbx_wo_planning_edit_spare_list_of_material'=>array('level' => 174,'level_edit' => -32),
            'chkbx_wo_planning_edit_technical_shared_with_owner'=>array('level' => 175,'level_edit' => -31),
            'slct_wo_planning_edit_availability_of_results'=>array('level' => 176,'level_edit' => -30),
            'slct_wo_planning_edit_availability_supplier'=>array('level' => 177,'level_edit' => -29),
            'slct_wo_planning_edit_document_to_be_update_by'=>array('level' => 178,'level_edit' => -28),
            'slct_wo_planning_edit_documentation_required'=>array('level' => 179,'level_edit' => -27),
            'slct_wo_planning_edit_material_labor_fc'=>array('level' => 180,'level_edit' => -26),
            'slct_wo_planning_edit_material_labor_fc_2'=>array('level' => 181,'level_edit' => -25),
            'slct_wo_planning_edit_material_to_be_supplied_by'=>array('level' => 182,'level_edit' => -24),
            'txtfld_wo_planning_edit_attach'=>array('level' => 183,'level_edit' => -23),
            'txtfld_wo_planning_edit_bill_material'=>array('level' => 184,'level_edit' => -22),
            'txtfld_wo_planning_edit_closing_notes'=>array('level' => 185,'level_edit' => -21),
            'txtfld_wo_planning_edit_confirmed_date_service'=>array('level' => 186,'level_edit' => -20),
            'txtfld_wo_planning_edit_data_programmata_ispezione'=>array('level' => 187,'level_edit' => -19),
            'txtfld_wo_planning_edit_date_closing_communication'=>array('level' => 188,'level_edit' => -18),
            'txtfld_wo_planning_edit_date_sending_doc'=>array('level' => 189,'level_edit' => -17),
            'txtfld_wo_planning_edit_days_inspection'=>array('level' => 190,'level_edit' => -16),
            'txtfld_wo_planning_edit_days_of_service'=>array('level' => 191,'level_edit' => -15),
            'txtfld_wo_planning_edit_defective_part_sent_to_supplier'=>array('level' => 192,'level_edit' => -14),
            'txtfld_wo_planning_edit_doc_notes'=>array('level' => 193,'level_edit' => -13),
            'txtfld_wo_planning_edit_document_number'=>array('level' => 194,'level_edit' => -12),
            'txtfld_wo_planning_edit_effective_date_corrective_action'=>array('level' => 195,'level_edit' => -11),
            'txtfld_wo_planning_edit_effective_date_material_available'=>array('level' => 196,'level_edit' => -10),
            'txtfld_wo_planning_edit_effective_date_shipment'=>array('level' => 197,'level_edit' => -9),
            'txtfld_wo_planning_edit_email_to_owner_date'=>array('level' => 198,'level_edit' => -8),
            'txtfld_wo_planning_edit_exp_date_document_available'=>array('level' => 199,'level_edit' => -7),
            'txtfld_wo_planning_edit_exp_date_material_available'=>array('level' => 200,'level_edit' => -6),
            'txtfld_wo_planning_edit_numero_giorni_ispezione'=>array('level' => 201,'level_edit' => -5),
            'txtfld_wo_planning_edit_planning_type'=>array('level' => 202,'level_edit' => -4),
            'txtfld_wo_planning_edit_protocol_ddt'=>array('level' => 203,'level_edit' => -3),
            'txtfld_wo_planning_edit_scheduled_service_date'=>array('level' => 204,'level_edit' => -2),
            'txtfld_wo_planning_edit_service_notes'=>array('level' => 205,'level_edit' => -1),
            'txtfld_wo_planning_edit_short_description_material'=>array('level' => 206,'level_edit' => 0),
            'txtfld_wo_planning_edit_spare_notes'=>array('level' => 207,'level_edit' => 1)
        );
        $tot = 0; $ok = 0; $txt = '';
        foreach($uis as $instance_name => $props) {
            $res = $update->setUiProperties($instance_name, $props);
            $tot++; $ok += ($res["error"]?0:1);
            $txt .= "<div style='color: ".($res["error"]?"red":"green").";'>{$res["message"]}</div>\n";
        }
        echo "Completed $ok/$tot<br/>$txt";
    }
}
?>