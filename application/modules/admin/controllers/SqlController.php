<?php

class Admin_SqlController extends Zend_Controller_Action
{
    
    public function preDispatch() {
        parent::preDispatch();  
        $this->_helper->layout()->disableLayout();        
    }

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->view->result = array();
    }
    
    public function queryAction()
    {
        $res = array();
        if(isset($_POST['query']) &&  !empty($_POST['query'])) {
            $sql = new MainsimAdmin_Model_Db();
            $res = $sql->query($_POST["query"]);
            $res['query'] = $_POST["query"];
        }
        $this->view->result = $res;        
        $this->_helper->viewRenderer('index');
    }
}

