<?php

class Admin_ScriptsController extends Zend_Controller_Action
{
    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->layout()->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function indexAction()
    {
        $script = new MainsimAdmin_Model_Scripts();
        $list = $script->getScriptsList();
        $this->view->list = $list['f_name'];        
    }
    
    public function getScriptAction() 
    {        
        $this->_helper->viewRenderer->setNoRender(true);
        $script = new MainsimAdmin_Model_Scripts();
        $res = $script->getScript($_POST['f_id']);
        echo json_encode($res);
        die;
    }
        
    public function saveScriptAction() 
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = json_decode(utf8_decode(base64_decode($_POST['params'])),true);  
        $params['f_script'] = html_entity_decode($params['f_script']);                
        $script = new MainsimAdmin_Model_Scripts();
        $message = $script->save($params);        
        echo json_encode($message);die;
    }        
}