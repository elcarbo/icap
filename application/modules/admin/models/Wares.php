<?php

class Admin_Mainsim_Model_Wares
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function getFathers()
    {
        $select = new Zend_Db_Select($this->db);
        $select->from('t_wares','f_code');
        $select->limit(20,0);        
        $result = $select->query()->fetchAll();
        //verifico quanti di questi elementi abbiano la root sotto a 0
        $select->reset();
        $root = 0;
        $under_root = 0;
        foreach($result as $line) {
            $select->from('t_ware_cross',array('num'=>'COUNT(*)'))->where("f_parent_code = ?",$root)->where("f_code = ?", $line['f_code']);
            $result_num = $select->query()->fetchAll();            
            if(isset($result_num[0]['num'])) {
                $under_root+=$result_num[0]['num'];
            }
            $select->reset();
        }
        return array('n0'=>$under_root,'n1'=>count($result));
    }
    
    public function getAllBookTitle()
    {
        $select = new Zend_Db_Select($this->db);
        $titles = array();
        //recupero tutti i tipi
        $select->from('t_custom_field_types');
        $types = $select->query()->fetchAll();
        $select->reset();
        foreach($types as $line) {
            $select->from('t_custom_field_values_'.strtolower($line['f_custom_field_type']),'f_custom_field_label');
            $select->distinct("f_custom_field_label");
            $result_labels = $select->query()->fetchAll();
            foreach($result_labels as $label ) {
                if(!in_array($label['f_custom_field_label'], $titles)) {
                    $titles[] = $label['f_custom_field_label'];
                }
            }            
            $select->reset();
        }
        return $titles;
    }
}