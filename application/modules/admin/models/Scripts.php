<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class MainsimAdmin_Model_Scripts
{
    private $db;
    
    public function __construct()
    {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function getScriptsList()
    {
        $select = new Zend_Db_Select($this->db);
        $res_pdf = $select->from("t_scripts",array('f_id','f_name'))
            ->query()->fetchAll();
        $list = array();
        $names = array();        
        foreach($res_pdf as $line) {
            $names[$line['f_id']] = $line['f_name'];            
        }
        $list['f_name'] = $names;        
        return $list;
    }
    
    public function getScript($f_id = 0)
    {
        $select = new Zend_Db_Select($this->db);
        $res_script = $select->from("t_scripts",array('f_script','f_type'))
            ->where("f_id = $f_id")->query()->fetch();        
        if(!empty($res_script)) {            
            $res_script['f_script'] = Mainsim_Model_Utilities::chg($res_script['f_script']);
        }
        return $res_script;
    }
    
    public function save($params)
    {
        try{
            if($params['f_id']) {
                $f_id = $params['f_id'];
                unset($params['f_id']);
                $this->db->update("t_scripts",$params,"f_id=".$f_id);
            }
            else {
                unset($params['f_id']);                            
                $this->db->insert("t_scripts",$params);
            }
        }catch(Exception $e) {
            return array("message"=>$e->getMessage());            
        }
        return array();
    }
}
