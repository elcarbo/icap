<?php

class MainsimAdmin_Model_Treegrid
{
    /**
     *
     * @var Zend_Db 
     */
    private $db;  
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    private function getBookmark($f_module_name, $f_name,$table = 't_admin_bookmarks') 
    {
        $f_user_id = Zend_Auth::getInstance()->getIdentity()->f_id;
        $q = new Zend_Db_Select($this->db);        
        if($f_name) {   // L'utente ha richiesto una particolare vista          
            if($f_name == "default") {
                // Restituisco default
                $q->from($table)->where("f_module_name = ?", $f_module_name)->where("f_user_id = 0");
                $res = $q->query()->fetchAll();
                $q->reset();                
            } else {
                // Restituisco vista
                $q->from($table)->where("f_module_name = ?", $f_module_name)->where("f_user_id = $f_user_id")->where("f_name = ?", $f_name);
                $res = $q->query()->fetchAll();
                $q->reset();                
            }
        } else { // Cerco vista con f_active = 1                 
            $q->from($table)->where("f_module_name = ?", $f_module_name)->where("f_user_id = $f_user_id");            
            $res = $q->query()->fetchAll();
            $q->reset();
            if(empty($res)) {
                // Restituisco default
                $q->from($table)->where("f_module_name = ?", $f_module_name)->where("f_user_id = 0");                
                $res = $q->query()->fetchAll();
                $q->reset();
            }
        }        
        return $res[0];
    }
    
    public function jax_read_root($params)
    {
        //carico i parametri di visibilità dell'utente
        $qu = new Zend_Db_Select($this->db);        
        $tab = $params->Tab[0];           
        $f_type = $params->Tab[1];         
        $module_name = $params->module_name;
        $f_name = "";        
        //IN CASO DI PASSAGGIO DI GRUPPO WF DA SOMMARIO, DETERMINO LA VISUALIZZAZIONE IN BASE A QUELLI RICHIESTI 
        // E NON IN BASE ALLA VISIBILITA' DI BASE
        $qu->from(array('t1'=>$tab),array('num'=>'count(*)'))
                ->where("f_type_id IN ($f_type)");               
        $ris = $qu->query()->fetch();        
        $n0=(int) $ris['num']; 
        
        $n1=$n0; 
        $qu->reset();
        // se lista piatta conto elementi in $tab 
        if(!$n0) {
            $qu->from($tab,array('num'=>'count(*)'))
               ->where("f_type_id IN ($f_type)");                              
            $ris = $qu->query()->fetchAll();            
            $n1=(int) $ris[0]['num']; 
            $qu->reset();
        } 
        
        //Recupero la configurazione                 
        $book = $this->getBookmark($module_name, $f_name);        
        return array("n0"=>$n0, "n1"=>$n1, 'bookmark'=>json_decode($book['f_properties'],true),'bookmark_name'=>$book['f_name'],'bookmark_id'=>$book['f_id']);        
    }
    
    public function jax_read_child($params)
    {
        
        $user = Zend_Auth::getInstance()->getIdentity();
        //Zend_session::writeClose();        
        
        $tab = $params->Tab[0];  
        
        $f_type = $params->Tab[1];
        $rtree = $params->rtree;
        $module_name = $params->module_name;
        $q = new Zend_Db_Select($this->db);
        $oth=array(
            'timestamp'=>time(),
            'modify'=>0        
        );
        
        $filter="";
        if(count($params->Filter)) {
            $aflt=array();                         
            for($r=0;$r<count($params->Filter);$r++) {
                if($params->Filter[$r]->ty==4){
                    $f = $params->Filter[$r]->f;
                    if($f == 'f_title' || $f == 'f_code' || $f == 'f_description' || $f == "f_type_id") {
                        $aflt[]=$this->db->quoteInto("t1.$f = ?",$params->Filter[$r]->v);
                    }                    
                    else {
                        $aflt[] = $this->db->quoteInto("(t1.f_properties like ?) ",'"'.$f.'":"'.$params->Filter[$r]->v.'"');
                    }
                } else {
                    // filtra per data                            
                }
            }
            if(count($aflt)) $filter=$aflt;
        }
        
        if(count($params->Selector)) {
            
        }
        
        // And  
        $ands = array();                 
        if(count($params->Ands)) {             
            for($r=0;$r<count($params->Ands);$r++) 
                $ands[]="t1.".$params->Ands[$r]->f." ".$params->Ands[$r]->z;           
        }
         
        $search=array();  
        if(count($params->Search)) {
            $asrc=array();      
             for($r=0;$r<count($params->Search);$r++) {                  
                  $v0=$params->Search[$r]->v0;
                  $v1=$params->Search[$r]->v1;
                  $sel=$params->Search[$r]->Sel;
                  $typ=$params->Search[$r]->type;
                  $mn=$params->Search[$r]->Mm;
                  
                  $f=$params->Search[$r]->f;
                  if($f == 'f_instance_name' || $f == 'f_code' || $f == 'f_description') {
                      $f = 't1.'.$f;                  

                      if($mn) $f="BINARY(".$f.")";
                      // testo
                      if($typ==0){
                        if($sel==0) { $asrc[]="$f like '%$v0%' ";  } 
                        if($sel==1) { $asrc[]="$f not like '%$v0%' "; }
                        if($sel==2) { $asrc[]="$f = '$v0' ";  }
                        if($sel==3) { $asrc[]="$f != '$v0' ";  }
                        continue;
                      }
                      // numerico
                      if($typ==1){ 
                          if($sel==0) { $asrc[]="$f = $v0 ";  } 
                          if($sel==1) { $asrc[]="$f != $v0 "; }
                          if($sel==2) { $asrc[]="$f > $v0  ";  }
                          if($sel==3) { $asrc[]="$f < $v0 ";  }
                          if($sel==4) {   }    // espressione con placeholder
                          continue;  
                      }
                      // data
                      if($typ==3){ 
                          if($sel==0) { $asrc[]="$f = $v0 ";  } 
                          if($sel==1) { $asrc[]="$f != $v0 "; }
                          if($sel==2) { $asrc[]="$f > $v0  ";  }
                          if($sel==3) { $asrc[]="$f < $v0 ";  }
                          if($sel==4 && $v1) { $asrc[]="$f>$v0 and $f<$v1"; }                     // range data se esiste v1
                          continue;
                      }          
                  } else {                      
                      $asrc[] = $this->db->quoteInto("(t1.f_properties like ?) ","%\"$f\":\"%$v0%");
                  }
             }

            if(count($asrc)) $search=$asrc;//implode(" ",$asrc);
        } //
                
        $join_parent = "";        
        $hy = $params->Hierarchy;                         
        $fpdr = $params->Prnt;  // se hierarchy cerco figli altrimenti su tutti
        $and  = array();        
        if($f_type == 2) {
            $hy = 0;
        }
        
        // Sort        
        $sort="";
        if(count($params->Sort)) {            
            $asrt=array();                        
            foreach($params->Sort as $sortLine){
            //for($r=0;$r<count($params->Sort);$r++){                
                if(empty($sortLine->f)) break;                
                
                $prefix = "";
                if($sortLine->f == 'f_code' || $sortLine->f == 'f_instance_name') {
                    $prefix = "t1.";
                }                
                $asrt[]=$prefix.$sortLine->f." ".$sortLine->m;
            }
            if(count($asrt)) 
                $sort=implode(", ",$asrt);
        }
        
        if(empty($sort)) {
            $sort = "f_code DESC";
        }

        // limit
        $pos=$params->Limit[0];
        
        //------------------------------------------------------------------------------

        // leggere anzichè t1.*  i bookmark ed i components visibili
        $arr = array();
         // se non hierarchy o se padre==root restituisco elementi totali x scrollbar e nch padre implicito
        $ntot = $rtree;

        if($fpdr<0){
           $q->from(array('t1'=>$tab),array('num'=>'count(*)'))                
                ->where("f_type_id IN ($f_type)");           
           if(!empty ($ands)) {
               foreach($ands as $ands_line) {
                  $q->where($ands_line);
               }
           }
           if(!empty ($search)) {
               foreach($search as $ands_line) {
                  $q->where($ands_line);
               }
           }
           if(!empty($filter)) {
               foreach($filter as $filter_line) {
                   $q->where($filter_line);
               }
           }           
           $ris = $q->query()->fetchAll();//$this->db->query($q)->fetchAll();           
           $ntot=$ris[0]['num'];

        } else {
            //Recupero i t_custom_fields                        
            $q->reset();            
            $q->from(array('t1'=>$tab), 
                array(    
                    'f_code',
                    'f_properties'=>'f_properties',
                    'f_type_id'=>'f_type_id',
                    'f_instance_name'=>'f_instance_name'
                )
            );
                       
            $q->where("t1.f_type_id IN ($f_type)");                
            
            if(!empty ($ands)) {
                foreach($ands as $ands_line) {
                   $q->where($ands_line);
                }
            }
            if(!empty ($search)) {
               foreach($search as $ands_line) {
                  $q->where($ands_line);
               }
           }
            if(!empty ($and)) {
                foreach($and as $and_line) {
                   $q->where($and_line);
                }
            }    
            if(!empty($filter)) {
               foreach($filter as $filter_line) {
                   $q->where($filter_line);
               }
           }
            $q->order($sort)->limit($params->Limit[1],$params->Limit[0]);//($limit,$pos);                                    
            $ris = $q->query()->fetchAll(); 
            $i = 0; 
            $head = array("fpdr","nch","selected","fpos","rtree");           
            
            $q->reset();
            
            $result_col = $this->getBookmark($module_name,"");
            $result_col = json_decode($result_col['f_properties'],true);
            foreach($result_col as $line) {
                if($line[0] != 'f_code' && $line[0] != 'f_instance_name') {
                    $cols_name[] = $line[0];
                }
            }
            foreach($ris as $data){                
                $cmp = array($fpdr,0,0,$pos,$rtree);    
                foreach($data as $k => $v) {
                    if($k=="f_properties" && $f_type != 2 && $f_type != 10 && $f_type != 11 && $f_type != 12) {                        
                        $properties = json_decode(Mainsim_Model_Utilities::chg($v),true);
                        //$properties = Mainsim_Model_Utilities::chg($v,true);
                        $custom = array();
                        foreach($cols_name as $col) {
                            //echo 'miao';                            
                            $custom[$col] = isset($properties[$col])?$properties[$col]:'';
                        }         
                        if(isset($properties['icon'])) {
                            $custom['icon1'] = isset($properties['icon'][0])?$properties['icon'][0]:'';
                            $custom['icon2'] = isset($properties['icon'][1])?$properties['icon'][1]:'';
                        }
                        $v = $custom;                        
                    }                 
                    
                    if(!is_array($v)) {
                        $cmp[]=stripslashes(Mainsim_Model_Utilities::chg($v));                            
                    }
                    else {
                        $cmp = array_merge($cmp,array_values($v));
                    }
                    
                    if(!$i) {
                        if(!is_array($v)) {
                           $head[]=$k;                 
                        }
                        else {
                            $head = array_merge($head, array_keys($v));
                        }                     
                    }
                }   
                if(!$i) {
                    $head[] = 'f_editability';
                }
                $cmp[] = 1;
                if(!$i) 
                    $arr[]=$head;  
                $arr[]=$cmp;
                $pos++; $i++;
           }            
        }

        
        return array("ntot"=>$ntot, "data"=>$arr, "other"=>$oth);                
    }
    
    /** -------------------------------
     * Settore WF
     * @param type $params
     * @return type 
     */
    public function jax_read_root_chk_workflows($params) {
        //carico i parametri di visibilità  dell'utente
        $qu = new Zend_Db_Select($this->db);
        $res = $qu->from("t_ui_object_instances")->where("f_code = ?", $params->f_code)->query()->fetchAll();
        $properties = json_decode($res[0]['f_properties'], true);
        $n0 = 0;
        if (isset($properties['phases_id'])) {
            $n0 = count($res);
        }
        $n1 = $n0;
        $book = $this->getBookmark($params->module_name, $f_name, 'view');
        return array("n0" => $n0, "n1" => $n1, 'bookmark' => json_decode($book['f_properties'], true), 'bookmark_name' => $book['f_name'], 'bookmark_id' => $book['f_id'], 'checkcross' => "");
    }
    
    public function jax_read_check_workflows($params) {
        //carico i parametri di visibilità  dell'utente        
        $q = new Zend_Db_Select($this->db);
        //Zend_session::writeClose();        
        $res = $q->from("t_ui_object_instances")->where("f_code = ?", $params->f_code)->query()->fetchAll();
        $properties = json_decode($res[0]['f_properties'], true);
        $phases_array = explode(',',$properties['phases_id']);
        $arr = array();
        foreach($phases_array as $phase) {
            $q->reset();
            $phase_res = $q->from("t_wf_phases")->where("f_id = ?",$phase)->query()->fetchAll();
            if(isset($res[0]) && count($phase_res) == 1) {
                $phase_res[0]['f_name'] = "WF {$phase_res[0]['f_wf_id']} - {$phase_res[0]['f_name']}";
            }
            else {
                die('Qualcosa non va');
            }
            $arr[] = $phase_res[0];
        }
        return array("data"=>$arr, "other"=>array());
    }
    
    
    public function jax_read_root_sel($params)
    {
        //carico i parametri di visibilitÃ  dell'utente
        $qu = new Zend_Db_Select($this->db);        
        $user = Zend_Auth::getInstance()->getIdentity();                
        $f_name = "";                        
        $f_code = $params->f_code;         
        
        $res = $qu->from("t_ui_object_instances",array("f_properties"))
                ->where("f_code = ?",$f_code)
                ->query()->fetchAll();
        
        $f_codes = array();
        if(!empty($res)) {            
           $prop = json_decode($res[0]['f_properties'],true);           
           if(isset($prop['t_selectors'])) {
               $t_sel = $prop['t_selectors'];
               foreach($t_sel as $line) {
                   $f_codes[] = $line;
               }
           }
        }
        $n0 = count($f_codes);
        $n1 = $n0;
        if(!empty($f_codes)) {            
            $params_check = json_encode(array("Tab"=>$params->Tab,"Ands"=>implode(',',$f_codes),'module_name'=>$params->module_name));
            $res_check = $this->jax_read_check(json_decode($params_check));            
            $data = $res_check['data'];
            $oth = $res_check['oth'];
        }
        $book = $this->getBookmark($params->module_name, $f_name,'t_bookmarks');
        return array("n0"=>$n0, "n1"=>$n1, 'bookmark'=>json_decode($book['f_properties'],true),'bookmark_name'=>$book['f_name'],'bookmark_id'=>$book['f_id'], 'data'=>$data,'oth'=>$oth);        
    }
    
    
    private function get_tab_cols($tab) 
    {        
        Zend_Db_Table::setDefaultAdapter($this->db);
        $table = new Zend_Db_Table($tab);
        $columns = $table->info('cols');       print $columns;exit; 
        return $columns;
    }
    
    /** ------------------------------------------------ */  
    
    
    /**
     * @param mixed $params
     * @return type 
     */
    public function jax_read_root_chk($params)
    {
        
        //carico i parametri di visibilitÃ  dell'utente
        $qu = new Zend_Db_Select($this->db);      
        //Zend_session::writeClose();        
        $user = Zend_Auth::getInstance()->getIdentity();
        $user_level = $user->f_level;
        $uid = $user->f_id;
        $module_name = $params->module_name;
        
        $tab_explode = explode("_",$params->Tab[0]);
        $f_name = "";
        
        if(count($tab_explode) > 2) {
            $tab = $tab_explode[0].'_'.$tab_explode[1];            
            $tabp = $params->Tab[0];
        }
        else {
            $tab = $params->Tab[0];   
            $tabp = $tab."_parent";
        }
        
        $f_type = $params->Tab[1];         
        $f_code = $params->f_code;         
                
        $f_codes = array();
        if(count($tab_explode) < 3) {
            // Recupero asset per wo
            if($params->f_code > 0) {
                $qu->reset();
                //RECUPERO DAL F_CODE IL TIPO E LA CATEGORIA A CUI APPARTIENE                
                $result_f_code = $qu->from("t_creation_date")->where("f_id = ?",$f_code)->query()->fetchAll();
                $cross_table = "t_";
                $cross_id = "";
                $cross_id_join = "";                
                switch ($result_f_code[0]['f_type']) {
                    case "WARES" : 
                        switch ($tab) {
                            case "t_wares" : $cross_table.="wares_relations";                                   
                                break;
                            case "t_selectors" : $cross_table.="selector_ware";
                                $cross_id_join = "f_selector_id";
                                break;
                            case "t_workorders" : $cross_table.="ware_wo";
                                $cross_id_join = "f_wo_id";
                                break;
                        }  
                        $cross_id = "f_ware_id";
                        break;
                    case "SELECTORS" : 
                        switch ($tab) {
                            case "t_wares" :  $cross_table.="selector_ware";
                                $cross_id_join = "f_ware_id";
                                break;                        
                            case "t_workorders" : $cross_table.="selector_wo";
                                $cross_id_join = "f_wo_id";
                                break;
                        }     
                        $cross_id = "f_selector_id";
                        break;
                    case "WORKORDERS" : 
                        switch ($tab) {
                            case "t_wares" : $cross_table.="ware_wo";
                                $cross_id_join = "f_ware_id";
                                break;
                            case "t_selectors" : $cross_table.="selector_wo";
                                $cross_id_join = "f_selector_id";
                                break;                        
                        }                       
                        $cross_id = "f_wo_id";
                        break;                        
                }
                $qu->reset();           
                //ADESSO ESEGUO LA QUERY ESCLUDENDO LE CATEGORIE                            
                if($cross_table != "t_wares_relations") {                    
                    $qu->from($cross_table,array())
                       ->join($tab, "$cross_table.$cross_id_join = $tab.f_code",array('f_code'))
                       ->where("$cross_table.$cross_id = ?",$f_code)
                       ->where("$tab.f_active = 1");
                    if($cross_table == "t_selector_ware") $qu->where("f_nesting_level = 1"); //in caso di cross tra selector e ware cerco il primo livello
                    if($f_type) {                            
                        $qu->where("f_type_id IN ($f_type)");                                                
                    }                    
                    $qu->where("$cross_table.f_active = 1");
                    $data = $qu->query()->fetchAll();
                    foreach($data as $line) {                    
                        $f_codes[] = $cross_table == "t_wares_relations"?$line['f_code_ware_slave']:$line['f_code'];
                    }
                }
                else {                    
                    $qu->from($cross_table)                    
                       ->where("$cross_table.f_code_ware_master = ?",$f_code);                        
                    if($f_type) {                            
                        $qu->where("f_type_id_slave IN ($f_type)");                                                
                    }
                    /**/                       
                    $qu->where("$cross_table.f_active = 1");                       
                    $data = $qu->query()->fetchAll();
                    foreach($data as $line) {                    
                        $f_codes[] = $cross_table == "t_wares_relations"?$line['f_code_ware_slave']:$line['f_code'];
                    }
                }                
            }
        }
        else {      
            
            $result_codes = $qu->from($tabp)->where("f_code = ?",$f_code)->where("f_active = 1")->query()->fetchAll();                        
            foreach($result_codes as $line) {
                $f_codes[] = $line['f_parent_code'];                
            }
        }
        $data = array();
        $oth = array();
        if(!empty($f_codes)) {            
            $params_check = json_encode(array("Tab"=>$params->Tab,"Ands"=>implode(',',$f_codes),'module_name'=>$module_name));
            $res_check = $this->jax_read_check(json_decode($params_check));            
            $data = $res_check['data'];
            $oth = $res_check['oth'];
        }
        $qu->reset();        
        
        $book = $this->getBookmark($f_name, 'view',$module_name);
        
        return array('bookmark'=>json_decode(Mainsim_Model_Utilities::chg($book['f_properties']),true),'bookmark_name'=>$book['f_name'],'bookmark_id'=>$book['f_id'], 'data'=>$data,'oth'=>$oth);        
    }
    
    public function jax_read_check($params) {
        
        //carico i parametri di visibilità  dell'utente        
        $q = new Zend_Db_Select($this->db);
        //Zend_session::writeClose();        
        $tab = $params->Tab[0];   
        if(strpos($params->Tab[0],"_parent") === false ) {            
            $tab_columns = $this->get_tab_cols($tab);            
        }
        else {
            $exp = explode("_parent",$params->Tab[0]);
            $tab_columns = $this->get_tab_cols($exp[0]);
        }
        $an = $params->Ands;
        if($an=="") $an="0";
        $f_type = $params->Tab[1];
        
        $result_col = $this->getBookmark($params->module_name,$f_name,'t_bookmarks');
        $cols_name = array();
        $result_col = json_decode(Mainsim_Model_Utilities::chg($result_col['f_properties']),true);
        $black_list = array(
            'f_creation_date',
            'f_percentage',
            'f_phase_name',
            'f_creation_user',
            'f_editor_user_name',
            'f_creation_user_name'
        );
        foreach($result_col as $line) {
            $delegate = explode('__DELEGATE',$line[0]) ;            
                if(!in_array($delegate[0],$tab_columns)  && !in_array($delegate[0], $black_list)){
                    if(count($delegate) > 1) {
                        $cols_name[] = "REPLACE(t_custom_fields.{$delegate[0]},t_custom_fields.{$delegate[0]},(SELECT f_delegate from t_delegates where f_code = t1.f_code AND f_column = '{$delegate[0]}')) AS {$line[0]}";                    }    
                    else {
                        $cols_name[] = $line[0];
                    }
                }
                elseif(count($delegate) > 1  && !in_array($delegate[0], $black_list)) {                   
                    $cols_name[] = "REPLACE(t1.{$delegate[0]},t1.{$delegate[0]},(SELECT f_delegate from t_delegates where f_code = t1.f_code AND f_column = '{$delegate[0]}')) AS {$line[0]}";
                }            
        }
        
        $tab_explode = explode("_",$params->Tab[0]);        
        $q->reset();            
        if(strpos($tab,"_parent") === false) {
            $q->from(array('t1'=>$tab))            
                ->joinLeft("t_custom_fields","t_custom_fields.f_code = t1.f_code",$cols_name)
                ->where("t1.f_active = 1");
            if(count($tab_explode) < 3 && $f_type) {                                                            
                $q->where("f_type_id IN ($f_type)");                    
                
            }
            $q->where("t1.f_code in ($an)")->where("t_custom_fields.f_active = 1");               
        }          
        else {
            $q->from(array('t1'=>"{$tab_explode[0]}_{$tab_explode[1]}")) 
                ->joinLeft("t_custom_fields","t_custom_fields.f_code = t1.f_code",$cols_name)
                ->where("t1.f_active = 1")
                ->where("t_custom_fields.f_active = 1")
                ->where("t1.f_code in ($an)");            
        }
        
        $ris = $q->query()->fetchAll();             
        $i=0; 
        $head=array();           
        $arr = array();
        foreach($ris as $data){
            $cmp = array();    
            foreach($data as $k => $v) {
                $cmp[]=stripslashes(Mainsim_Model_Utilities::chg($v));                            
                if(!$i) $head[]=$k;                 
            }                
            if(!$i) 
                $arr[]=$head;  
            $arr[]=$cmp;
            $pos++; $i++;
        }
        if(strpos($tab,"_parent") !== false) {
            $array_code = explode(',',$an);
            $cmp = array();
            if(in_array(0, $array_code)) {
                $q->reset();
                //Recupero la configurazione                 
                if(empty($head)) {
                    $head = $this->getBookmark($f_name, 'view',$params->module_name);                    
                    $res_head = json_decode(Mainsim_Model_Utilities::chg($head['f_properties']),true); 
                    $head = array();
                    foreach($res_head as $value) {
                        $head[] = $value[0];
                    }
                }
                $head_json = array();
                foreach($head as $key) {
                    $head_json[] = $key;
                    if($key == 'f_title') {
                        $cmp[] = 'Root';
                    }
                    elseif($key == 'f_code') {
                        $cmp[] = 0;
                    }
                    else{
                        $cmp[] = "";
                    }
                }
                if(empty($arr)) {
                    $arr[] = $head_json;
                }
                //$head
                $arr[] = $cmp;
            }
        }        
        return array("data"=>$arr, "other"=>array());
    }
    
    public function jax_lock($params)
    {
        $latency=8;  // tempo di rilascio automatico uscita da mainsim forzata        
        
        $mode=addslashes($params->mode);  
        
        $afcode=$params->fcode;    // array di fcode        
        $user = Zend_Auth::getInstance()->getIdentity();        
        $uid = $user->f_id;
        $tt = time();
        // unlock        
        if(!$mode && !empty($afcode)) {    
            $query="delete from t_locked where f_code in (".implode(',',$afcode).")";            
            $this->db->query($query);            
            return "-";
        } 
        // lock sempre singolo
        if($mode==1){
            $risp = array();
            $this->db->beginTransaction();
            // verifico se non già bloccato
            
            foreach($afcode as $line) {
                $qu="SELECT count(*) as num FROM t_locked where f_code=".$line." and f_timestamp > ".($tt-$latency);            
                $ris = $this->db->query($qu)->fetchAll();            
                $nc=(int) $ris[0]['num'];
                if($nc) {                    
                    $risp=$afcode[0];
                }
            }
            /*$qu="SELECT count(*) as num FROM t_locked where f_code=".$afcode[0]." and f_timestamp > ".($tt-$latency);            
            $ris = $this->db->query($qu)->fetchAll();            
            $nc=(int) $ris[0]['num'];
            if($nc) 
                $risp=$afcode[0];*/
            //else {  // inserisco record
            if(empty($risp)){
                for($r=0;$r<count($afcode);$r++){
                    $query="insert into t_locked (f_code, f_user_id, f_timestamp) values (".$afcode[$r].", $uid, $tt)";                    
                    $this->db->query($query);
                }
            } 
            $this->db->commit(); //query("UNLOCK TABLES");            
            return $risp;
        }
        // update
        if($mode==2){               
            $que="update t_locked set f_timestamp=$tt where f_code in (".implode(',',$afcode).") and f_timestamp > ".($tt-$latency);                       
            $this->db->query($que);
            return array();
        }
    }
    
    public function jax_lockotheruser()
    {
        $latency = 8;

        $user = Zend_Auth::getInstance()->getIdentity();        
        $uid = $user->f_id;
                
        $tt = time();
        $arrlk=array();

        $qu="SELECT f_code FROM t_locked where f_user_id != $uid and f_timestamp > ".($tt-$latency);
        
        $ris = $this->db->query($qu)->fetchAll();
        foreach($ris as $line) 
            $arrlk[]=$line['f_code'];
        
        return $arrlk; 
    }    
}