<?php

class MainsimAdmin_Model_Db
{
    /**     
     * @var Zend_Db 
     */
    private $db;
    
    public function __construct() {        
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));                
    }   
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function showTables()
    {        
        $instance = Zend_Registry::getInstance();
        $db = Zend_Registry::get('db');       
        $tables = $this->db->query("SHOW TABLES FROM {$db->params->dbname}")->fetchAll();
        $response = array();
        foreach($tables as $line) {
            foreach($line as $table) {
                $response[] = $table;
            }            
        }
        return $response;
    }
    
    public function getTable($name,$pos = 0,$limit = 30) 
    {
        $select = new Zend_Db_Select($this->db);
        $select->from($name)->limit($limit, $pos);
        $result = $select->query()->fetchAll();
        return $result;
    }
    
    public function getStructureTable($name) 
    {
        $result = $this->db->query("DESCRIBE $name")->fetchAll();
        return $result;
    }
    
    public function query($query)
    {
        $res = array(
            "result"=>array(),
            "message"=>"",
            "isSelect"=>true
        );
        try {
            $this->db->beginTransaction();
            if(stripos($query, "select") === 0) {
                $res['result'] = $this->db->query($query)->fetchAll();
            }
            else {
                $this->db->query($query);
                $res['isSelect'] = false;
                $res['result'] = "Query executed";
            }
            $this->db->commit();        
        }catch(Exception $e) {
            $res['error'] = $e->getMessage();
            $this->db->rollBack();
        }
        return $res;
    }
}