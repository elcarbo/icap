<?php

class MainsimAdmin_Model_Datamanager
{
    private $db,$table,$f_type_id,$mdl,$custom_fields = array(),$validation = array(),$fields,$currentSheet;
    private $f_order,$timeFieldList = array();
    private $sheet_configuration = array();
    private $userData = array();
    private $default_check_list = array(    
        'f_code'=>array('check'=>'uniqueCode','message'=>'f_code must be unique and not empty'),
        'f_wf_id'=>array('check'=>'wf_exist','message'=>"f_wf_id must exist.If you don't know what workflow assign,leave the column f_wf_id empty."),
        'f_phase_id'=>array('check'=>'phase_exist','message'=>"f_phase_id must exist.If you don't know what workflow assign,leave the column f_wf_id empty."),        
        "f_title"=>array('check'=>'empty_field','message'=>'mainsim doesn\'t allow an empty f_title.'),        
        "f_type_id_wares"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
        "f_type_id_selector"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
        "f_type_id_wares_master"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
        "f_type_id_wares_slave"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"'),
        "f_type_id_workorder"=>array('check'=>'f_type','message'=>'Type id incorrect. Correct type id format is "type_id" : "name"')    
    );
    
    public function __construct()
    {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function getTypes($tab){       
        $select = new Zend_Db_Select($this->db);
        $response = array();
        if(strpos($tab,"cross") === false ){
            $res_types = $select->from("{$tab}_types")->query()->fetchAll();
            $response = array_map(function($line){ return $line['f_type'];}, $res_types);
            /*if(!empty($res_types)) {
                foreach($res_types as $line) {
                    $response[] = $line['f_type'];
                }    
            }*/
        }
        elseif($tab == 'cross') {
            $response = array(
                't_ware_wo','t_wares_relations',
                't_selector_ware','t_selector_wo',
                't_wo_relations','t_pair_cross','t_periodics'
            );
        }
        return json_encode($response);
    }


    /**
     * Get database name
     * @return string database name
     */
    public function getDb()
    {
        $config = Zend_Registry::get('db');
        $dbConf = $config->toArray();
        return $dbConf['params']['dbname'];        
    }
    
    /**
     * Get Database type
     */
    public function isMysql()
    {
        $config = Zend_Registry::get('db');
        $dbConf = $config->toArray();
        return stripos($dbConf['adapter'],'PDO_MYSQL') !== false ? true:false;   
    }
        
    /**
     * Import array from browser directly
     * @param type $f_code datamanager code to import
     * @param type $position position of array to import
     * @param type $isFinish value to complete import of a sheet
     * @return string resp json with response of operation
     */
    public function instantDataLoader($f_code,$position = 1,$isFinish = 0)
    {
        $select = new Zend_Db_Select($this->db);
        //publish f_in zend_registry to use it in any part of the system during import
        Zend_Registry::set("f_id",$f_code);        
        $importSessionId = "import".$f_code;
        try {    
            // get information about data to import
            $res = $select->from("t_systems",array("f_code","fc_dmn_objects","fc_dmn_objects_imported","fc_dmn_imported_file"))
                ->join("t_creation_date","t_systems.f_code = t_creation_date.f_id",array('f_phase_id'))
                ->where("f_code = ?",$f_code)->where("f_phase_id <= 2")->query()->fetch();
            if(empty($res)){
                return array('response'=>'ok');
            }
            $select->reset();
			//check if other import are running. if true, stop the operation
			$resCount = $select->from("t_creation_date",array("num"=>'count(*)'))
				->where('f_phase_id = 2')->where('f_id <> ?',$f_code)
				->where("f_type = 'SYSTEMS'")->where("f_category = 'DATA MANAGER'")
				->query()->fetch();
				
			if($resCount['num'] > 0) {
				$this->db->update("t_creation_date",array("f_phase_id"=>4),"f_id = $f_code");
				$this->db->update("t_systems",array("fc_dmn_log"=>"Another import process is running now. Please wait"),"f_code = $f_code");
				return array('response'=>'ko','message'=>'Another import process is running now. Please wait');
			}
            
            $sheets = explode(',',$res['fc_dmn_objects']);
            $imported_sheets = strlen($res['fc_dmn_objects_imported']) > 0?explode(',',$res['fc_dmn_objects_imported']):array();                   
            if($res['f_phase_id'] == 1) {
                $this->db->update('t_systems',array('fc_dmn_objects_imported'=>''),"f_code = {$f_code}");
                $imported_sheets = array();                
                unset($_SESSION["import".$f_code]);
            }
            $not_imported_list = array_values(array_diff($sheets, $imported_sheets));                        
            
            //check if excel exists and set its path
            $file = $this->getImportFile($f_code, 'instant');            
            if(is_array($file)){
                return $file;
            }
            if(empty($not_imported_list)) {
                //Close datamanager request as close
                $this->db->update("t_creation_date",array("f_phase_id"=>3),"f_id = $f_code");
                return array('response'=>'ok');
            }
            $fileEx = Mainsim_Model_Utilities::IterateExcel($file,1,1);            
            if(!isset($_SESSION[$importSessionId]['sheetNames'])) {
                $sheetsEx = $fileEx->getSheetNames();  
                $_SESSION[$importSessionId]['sheetNames'] = $sheetsEx;
            }
            else {
                $sheetsEx = $_SESSION[$importSessionId]['sheetNames'];
            }
            
            $sheet = $not_imported_list[0];    
            if($sheet == 't_wares_users'){
                // get max f_code for users
                $selectMaxUser = new Zend_Db_Select($this->db);
                $selectMaxUser->from("t_creation_date", array('MAX(f_id)AS max'))
                            ->where("f_category = 'USER'")
                            ->where("f_type = 'WARES'");
                $resMaxUser = $selectMaxUser->query()->fetchAll();
                $this->max_user_id = $resMaxUser[0]['max'];
            }
            $this->currentSheet = $sheet;
            $pos = array_search($sheet, $sheetsEx);    
            if(empty($imported_sheets) && $position == 1) { //check if this is the first step to create custom and validation array
                //--------------------------------------------------------------                
                $this->custom_fields = array();
                $this->validation = array();                                
                if(in_array("Custom",$sheetsEx)) {//check if there is Custom sheet                 
                    $this->custom_fields = $this->createCustomList($file,array_search("Custom", $sheetsEx));
                }                       
                //--------------------------------------------------------------
                //extract and parse validation tables             
                if(in_array("Validation Tables",$sheetsEx)) {
                    $this->validation = $this->createValidationList($file,array_search("Validation Tables", $sheetsEx));
                }
                $_SESSION[$importSessionId]['custom_fields'] = $this->custom_fields;
                $_SESSION[$importSessionId]['validation'] = $this->validation;
            }
            else {
                $this->custom_fields = $_SESSION[$importSessionId]['custom_fields'];
                $this->validation = $_SESSION[$importSessionId]['validation'];
            }
            
            if(strpos($sheet,"t_cross") === false && strpos($sheet,"t_pair") === false) {
                $this->setImportConfiguration($file,$sheet,'instant',$position);                    
            }

            $this->setTimeFieldList($sheet);
            if($position == 1) { // if new, prepare header of excel to import
                $key_sheet = array();
                $rowIterator = $fileEx->setActiveSheetIndex($pos)->getRowIterator();    
                $this->createRollBack($sheet,$this->table);
                Mainsim_Model_Utilities::rowIteratorImport($rowIterator,$key_sheet,true);                
                $this->fields = $key_sheet;
                $_SESSION[$importSessionId]['fields'] = $this->fields;                
            }
            else {                
                $this->fields = $_SESSION[$importSessionId]['fields'];                
            }
            
            //Zend_session::writeClose();
            if($this->table != '') {
                $this->setDefaultTableCols($this->table);
                $this->setCreationTableCols();
                $this->setCustomTableCols();
            }    
            if($this->f_type_id == 16) {
                $this->setUserTableCols();
            }
            
            //set the datamanager in process phase
            $this->db->update("t_creation_date",array("f_phase_id"=>2),"f_id = $f_code");            
            if($isFinish == 1) {                 
                if($sheet != 't_wares_wizard'){
                    $this->completeImport($sheet); 
                    $position = 1;
                    $resp['isFinish'] = 0;
                    $resp['message'] = "$sheet has been imported successfully";
                }
                $imported_sheets[] = $sheet;
                if(count($imported_sheets) == count($sheets)) {
                    //Close datamanager request as close
                    $this->db->update("t_creation_date",array("f_phase_id"=>3),"f_id = $f_code");
                    $this->db->update("t_systems",array("fc_dmn_log"=>""),"f_code = $f_code");
                    $resp['response']= 'ok';
                }
                else {
                    $resp['response']= 'continue';
                }
                $imported_sheets = implode(',',$imported_sheets);
                $this->db->update("t_systems",array("fc_dmn_objects_imported"=>$imported_sheets),"f_code = $f_code");
            }
            else {
                //set increment value (default 1500)                
                $inc = 1500;
                if(strpos($sheet,"t_cross_") !== false) {
                    $inc = 5000;
                }
                elseif($this->mdl == 'mdl_pm_tg' || $this->mdl == 'mdl_meter_tg') {
                    $inc = 50;
                }
                //set User info
                $this->setUserData();
                $resp = $this->import($file,$sheet,$position,$inc);                
                if($resp['response'] == 'ko'){// in case of error ALT                    
                    return $resp;
                }      
                else {
                    $resp['message'] = "Importing sheet $sheet. Loaded ".($position+$inc)." rows";
                }
                $position+=$inc;                                
            }
        }catch(Exception $e) {            
            $errorMessage = "Error on sheet $sheet";
            if(($position == ($position+$inc)) || $isFinish == 1 ) {
                $errorMessage.= " during the completion";
            }else {
                $errorMessage.= "on line between $position ".($position+$inc);
            }
            $errorMessage.=' : '.$e->getMessage();
            self::reportError($errorMessage.' '.$e->getTraceAsString());
            return array("response"=>"ko",'message'=>$errorMessage);
        }
        $resp['pos'] = $position;        
        $resp['f_code'] = $f_code;
        return $resp;
    }
    
    
    
    public function cronImport()
    {
        $select = new Zend_Db_Select($this->db);        
        //check if there is something to import
        $select->from(array("t1"=>"t_systems"),array("f_code","fc_dmn_objects","fc_dmn_objects_imported","fc_dmn_imported_file"))
            ->join(array("t2"=>"t_creation_date"),"t1.f_code = t2.f_id",array())
            ->where("f_phase_id = 1")->where("fc_dmn_import_type = 'cron'")
            ->where("f_type_id = 6")
            ->where("fc_dmn_cron_starting_date IS NULL OR fc_dmn_cron_starting_date = 0 OR fc_dmn_cron_starting_date <=".time());
        $res = $select->query()->fetch();    
        if(empty($res)) return;
        //check if other import are running now
        $select->reset();
        $res_check = $select->from(array("t1"=>"t_systems"),array("num"=>"count(*)"))
            ->join(array("t2"=>"t_creation_date"),"t1.f_code = t2.f_id",array())
            ->where("f_phase_id = 2 AND f_category = 'DATA MANAGER'");
        $res_check = $select->query()->fetch();        
        if($res_check['num'] > 0) return;
        
        $f_code = $res['f_code'];
        
        $file = $this->getImportFile($f_code, 'cron');            
        if(is_array($file)){
            return $file;
        }
        //set the datamanager in process phase
        $this->db->update("t_creation_date",array("f_phase_id"=>2),"f_id = $f_code");        
        $this->db->update("t_systems",array("fc_dmn_log"=>'',"fc_dmn_working_sheet"=>'','fc_dmn_working_pos'=>0),"f_id = $f_code");        
        
        //publish f_in zend_registry to use it in any part of the system during import
        Zend_Registry::set("f_id",$res['f_code']);        
        try {    
            //set User info
            $this->setUserData();        
            $sheets = explode(',',$res['fc_dmn_objects']);
            $tot = count($sheets);      
            //------------------------------------------------------------------
            $fileEx = Mainsim_Model_Utilities::IterateExcel($file,1,1);            
            $sheetsEx = $fileEx->getSheetNames();             
            if(in_array("Custom",$sheetsEx)) {//check if there is Custom sheet                 
                $this->custom_fields = $this->createCustomList($file,array_search("Custom", $sheetsEx));                                 
            }            
            //------------------------------------------------------------------
            //extract and parse validation tables             
            if(in_array("Validation Tables",$sheetsEx)) {        
                $this->validation = $this->createValidationList($file,array_search("Validation Tables", $sheetsEx));                 
            }     
            
            for($a = 0;$a < $tot;++$a) {                
                $sheet = $sheets[$a];                
                //get header of sheet
                $pos = array_search($sheet, $sheetsEx);    
                $key_sheet = array();
                $rowIterator = $fileEx->setActiveSheetIndex($pos)->getRowIterator();    
                Mainsim_Model_Utilities::rowIteratorImport($rowIterator,$key_sheet,true);            
                $this->fields = $key_sheet;

                //set sheet information to import
                if(strpos($sheet,"t_cross") === false && strpos($sheet,"t_pair") === false) {
                    $this->setImportConfiguration($file,$sheet,'cron',1); 
                }
                //set default wf and phase
                $this->setWfPhases();             
                //set default main table                
                if($this->table != '') {        
                    $this->setDefaultTableCols($this->table);
                    $this->setCreationTableCols();
                }    
                if($this->f_type_id == 16) {
                    $this->setUserTableCols();
                    // get max f_code for users
                    $selectMaxUser = new Zend_Db_Select($this->db);
                    $selectMaxUser->from("t_creation_date", array('MAX(f_id)AS max'))
                                ->where("f_category = 'USER'")
                                ->where("f_type = 'WARES'");
                    $resMaxUser = $selectMaxUser->query()->fetchAll();
                    $this->max_user_id = $resMaxUser[0]['max'];
                }
                //add to default controll list the time field list
                $this->setTimeFieldList($sheet);                   
                //set increment value (default 1500)
                $inc = 1500;
                $position = 1;
                if(strpos($sheet,"t_cross_") !== false) {
                    $inc = 5000;
                }
                elseif($this->mdl == 'mdl_pm_tg' || $this->mdl == 'mdl_meter_tg') {
                    $inc = 50;
                }           
                
                while(true) {// run till all row has been imported
                    $this->db->update("t_systems",array("fc_dmn_working_sheet"=>$sheet,'fc_dmn_working_pos'=>$position),"f_id = $f_code");        
                    $resp = $this->import($file,$sheet,$position,$inc);
                    if($resp['response'] == 'ko'){// in case of error ALT                        
                        self::reportError($resp['message']);
                        return array("response"=>"ko");
                    }
                    if($resp['response'] == 'continue' && $resp['isFinish'] == 1) {//import finished, start final part
                        break;                        
                    }
                    else {// continue the import
                        $position+=$inc;
                    }
                }
                                    
                
                if($sheet != 't_wares_wizard'){
                    $this->completeImport($sheet); 
                }
                //clear sheet configuration to next import
                $this->clearConfig();
            }
        }catch(Exception $e) {
            echo $e->getMessage(). PHP_EOL.$e->getTraceAsString();
            self::reportError("Uncatchable error on sheet {$sheet} : ".$e->getMessage()."<br /><br />".$e->getTraceAsString());
            return array("response"=>"ko");
        }           
        $this->db->update("t_creation_date",array("f_phase_id"=>3,"f_timestamp"=>time()),"f_id = $f_code");
    }
    
    
    /**
     *
     * @param type $filename
     * @param type $sheet
     * @param type $position
     * @param type $inc    
     * @return string 
     */
    
    public function import($filename,$sheet,$position,$inc)
    {
        //array for server response about import status
        $resp = array('response'=>'ok','message'=>'');   
        $engCreator = new Mainsim_Model_Workorders($this->db);
        try{                                        
            $file = Mainsim_Model_Utilities::IterateExcel($filename,1,1);            
            $sheets = $file->getSheetNames();
            //check if passed sheet exist inside the excel
            if(!in_array($sheet, $sheets)){                
                $resp['response'] = 'ko';
                $resp['message'] = "The sheet ".$sheet." is not in your excel.";
                self::reportError($resp['message']);
                return $resp;                
            }                                
            //--------------------------------------------------------------------------                        
            $check_list[$sheet] = $this->default_check_list;                
            //--------------------------------------------------------------------------            
            //add custom check rules
            foreach($this->fields as $line_key) {
                if(isset($this->custom_fields[$line_key]['f_check_rules']) && !empty($this->custom_fields[$line_key]['f_check_rules'])){
                    if(isset($check_list[$sheet][$line_key])) unset($check_list[$sheet][$line_key]);
                    $check_list[$sheet][$line_key]['check'] = $this->custom_fields[$line_key]['f_check_rules'];
                    $check_list[$sheet][$line_key]['check_value'] = $this->custom_fields[$line_key]['f_check_value'];
                }
            }     
            
            if($position == 1) {                 
                // we set f_id of start only for the first read            
                $this->createRollBack($sheet,$this->table);                    
                //create custom fields
                if(!empty($this->sheet_configuration) || strpos($sheet, "t_selectors") !== false) {
                    //get custom and system cols already exist 
                    $this->custom_fields_cols = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
                    $tab = strpos($sheet, "t_selectors") !== false?
                            Mainsim_Model_Utilities::get_tab_cols("t_selectors"):Mainsim_Model_Utilities::get_tab_cols($this->table);
                    $already_insert = array();
                    foreach($this->fields as $col){
                        //convert in lower case to avoid problem with mysql
                        $col_ok = strtolower($col);
                        if(!in_array($col_ok,$tab) && isset( $this->custom_fields[$col])) {
                            if(!in_array($col_ok,$this->custom_fields_cols) && $col != 'f_parent_code' && !in_array($col_ok,$already_insert)) {                
                                //check special chars and not permitted char for mysql col name                
                                $this->createCustomCol($col_ok, $this->custom_fields[$col]);
                                $already_insert[] = $col_ok;
                            }
                        }
                    }
                }
                if($this->table == 't_selectors') {
                    $this->setRootSelectors();
                }
                $this->setCustomTableCols();
            }
            
            //get part of excel                      
            $file = Mainsim_Model_Utilities::IterateExcel($filename,$position,$inc,$sheet);        
            $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
            $objects = Mainsim_Model_Utilities::rowIteratorImport($rowIterator,$this->fields,false);                    
            $file->disconnectWorksheets();
            unset($file);            
            Zend_Registry::set('dbAdapter',$this->db);
            //work extracted data                        
            //create array with unique field
            if(!in_array($sheet,array("t_cross_wares_selectors","t_cross_wares_wares","t_cross_workorders_wares","t_cross_workorders_selectors"))) {
                foreach($check_list[$sheet] as $key_check => $value_check) {                
                    if($value_check['check'] == 'unique' || $value_check['check'] == 'uniqueCode') {                      
                        self::checkUniquesArray($key_check,$objects,$position);
                    }
                }      
            }
            
            if($this->table != '') {                    
                //check unique codes
                $tot_b = count($objects);
                $codes_array_check = array();
                $col = $this->getImportColumn($this->table);
                
                for($b = 0;$b < $tot_b;++$b) {    
                    $v_quoted = $this->db->quote($objects[$b]['f_code']);
					if(is_numeric($objects[$b]['f_code'])) {
							$v_quoted = $this->db->quote((int)$objects[$b]['f_code']);
					}  
                    if(!in_array($v_quoted,$codes_array_check)) {
                        $codes_array_check[$b] = $v_quoted;
                    }
                    else {                        
                        $message = $check_list[$sheet]['f_code']['message'];                        
                        $resp['response'] = 'ko';
                        $resp['message'] = "Error occurred at line ".($position+$b).' : '.$message;
                        self::reportError($resp['message']);
                        return $resp;
                    }
                }
                
                $unique_check = implode(',',$codes_array_check);
                $duplicate = Mainsim_Model_Utilities::check($unique_check, 'uniqueCode', $col,$this->table , $objects,0,$this->f_type_id); //get code duplicate                                    
                if($duplicate !== false) {                
                    $b = array_search($duplicate, $codes_array_check);
                    $message = $check_list[$sheet]['f_code']['message'];                     
                    $resp['response'] = 'ko';
                    $resp['message'] = "Error occurred at line ".($position+$b).' : '.$message;
                    self::reportError($resp['message']);
                    return $resp;
                }
            }      
            if(($this->table == 't_wares' || $this->table == 't_workorders' || $this->table == 't_selectors') ) {
                $this->setCreationDateOrder();                
            }
            //check rules
            if(strpos($sheet,"t_cross") === false && ($sheet != 't_wares_wizard') ) {                
                $tot_b = count($objects);           
                for($b = 0; $b < $tot_b;++$b) {               
                    $line = $objects[$b];                    
                    foreach($line as $key_check => $value) {
                        if(isset($check_list[$sheet][$key_check]) && $key_check != 'f_code') {                    
                            $col = $key_check;                                                                                 
                            if($this->table != '' && $this->f_type_id !=0 ){                                 
                                $check_value = isset($check_list[$sheet][$key_check]['check_value'])?$check_list[$sheet][$key_check]['check_value']:'';                                
                                if(!Mainsim_Model_Utilities::check($value, $check_list[$sheet][$key_check]['check'], $col,$this->table , $objects,$b,$this->f_type_id,$check_value)) {                
                                    $message = isset($check_list[$sheet][$key_check]['message'])?$check_list[$sheet][$key_check]['message']:'error raised during the controll of the field '.$key_check;                        
                                    $resp['response'] = 'ko';
                                    $resp['message'] = "Error occurred at line ".($position+$b).' : '.$message;
                                    self::reportError($resp['message']);
                                    return $resp;
                                }                
                            }
                        }
                        if(isset($this->validation[$key_check])) {
                            if(!in_array($value,$this->validation[$key_check]['f_value_allow'])) {                
                                $message = "The value inside the column '$key_check' doesn't respect validation table.";                                
                                $resp['response'] = 'ko';
                                $resp['message'] = "Error occurred at line ".($position+$b).' : '.$message;
                                self::reportError($resp['message']);
                                return $resp;                                    
                            }
                        }
                    }                            
                    //insert checked element in specific table
                    if($this->table == "t_wares" || $this->table == "t_workorders" || $this->table == "t_selectors") {
                        $this->insertObj($objects[$b]);                                        
                    }
                    elseif($sheet == "t_pair_cross") {
                        $this->insertPairCross($objects[$b]);
                    }
                }
            }        
            if($sheet == 't_wares_wizard') {                
                $tot = count($objects);
                for($a = 0;$a < $tot;++$a) {
                    $row = $objects[$a];
                    $row['f_order'] = 1;            
                    $this->db->insert("t_wizard",$row);
                } 
            }                  
            elseif(strpos($sheet,"t_cross") !== false){
                switch($sheet) {
                    case 't_cross_wares_selectors' : //cross between wares and selectors                                                            
                        $this->crossSelectors($objects,'t_wares');
                        break;
                    case 't_cross_workorders_selectors' : //cross between wares and selectors                                    
                        $this->crossSelectors($objects,'t_workorders');
                        break;
                    case 't_cross_workorders_wares' ://cross between wares and workorders                         
                        $this->crossWaresWorkorders($objects);
                        break;
                    case 't_cross_wares_wares' : //cross between wares and wares
                        $this->crossAllWares($objects);
                        break;                    
                }
            }
            
            $resp['response'] = 'continue';
            $resp['isFinish'] = (count($objects)<$inc-1)?1:0;             
            return $resp;                                       
        }catch(Exception $e) {            
            $resp['response'] = 'ko';
            $resp['message'] = "Error occurred during row parse between $position and ".($position + $inc)." : "." ".$e->getMessage();
            self::reportError($resp['message'].'<br /><br />'.$e->getTraceAsString());
            return $resp;
        }
    }
    
    
    private function completeImport($sheet)
    {
        //create custom and bookmark        
        if(!empty($this->sheet_configuration)) {                
            $this->createFathers($this->table, $this->f_type_id);                                    
            $this->createParentField();                
            if($this->table == 't_wares' && $this->f_type_id == 16) {
                $this->cryptPwd();
            }
            if($this->table == "t_workorders") { //workorders                            
                if($this->f_type_id == 9) {                
                    $this->completeOnCondition();//if uploaded on condition change placeholder with f_code
                }
            }
            if(isset($this->sheet_configuration['short_name']) && isset($this->mdl)) {                            
                $this->createBookmark();
                $this->createUi();            
            }        
        }
        elseif($sheet == 't_cross_workorders_wares') {                
            $this->fillWoInformations();//fill information about asset cross with wo
            $this->selectorWoFromAsset();// if wo is without selector, assign selector of asset/s associate
            $this->writePmPerAsset(); //verifico se a delle pm di tipo 'pm-per-asset' sono stati aggiunti asset. Se vero, ne creo le sotto-cicilità (se possibile)
        }
        elseif($sheet == 't_cross_wares_selectors' || $sheet == 't_cross_workorders_selectors'){
            $this->crossPmAssetByMaintenancePlan();
            $this->writePmPerAsset(); //verifico se a delle pm di tipo 'pm-per-asset' sono stati aggiunti asset. Se vero, ne creo le sotto-cicilità (se possibile)
        }

    }


    protected function crossPmAssetByMaintenancePlan()
    {
        $select = new Zend_Db_Select($this->db);
        $res_generators = $select->from(['t1' => 't_creation_date'],[])
          ->join(['t2' => 't_wf_phases'], "t1.f_phase_id = t2.f_number and t1.f_wf_id = t2.f_wf_id", [])
          ->join(['t3' => 't_workorders'], "t1.f_id = t3.f_code", ['f_code'])
          ->join(['t4' => 't_selector_wo'], "t3.f_code = t4.f_wo_id", [])
          ->join(['t5' => 't_selectors'], "t4.f_selector_id = t5.f_code", ['sel_code' => 'f_code'])
          ->where("t3.f_type_id IN (3,9,11,15,19)")->where("t5.f_type_id = 11")
          ->where("t2.f_group_id NOT IN (5,6,7,8)")->where("t3.fc_pm_type = 'pm-per-asset'")
          ->query()->fetchAll();
        $generators = []; //trasformo il risultato per avere meno query da fare dopo
        array_walk($res_generators, function($v) use(&$generators){
            $generators[$v['f_code']]['selectors'][] =  $v['sel_code'];
        });

        $data = [];
        $table_schema = ['f_ware_id','f_wo_id','f_timestamp'];

        foreach($generators as $wo_code => $line)
        {
            //recupero la lista di asset associata ai selettori
            $select->reset();
            $res_assets = $select->from(['t1' => 't_creation_date'],[])
              ->join(['t2' => 't_wf_phases'], "t1.f_phase_id = t2.f_number and t1.f_wf_id = t2.f_wf_id", [])
              ->join(['t3' => 't_wares'], "t1.f_id = t3.f_code", ['f_code'])
              ->join(['t4' => 't_selector_ware'], "t3.f_code = t4.f_ware_id", [])
              ->where("t3.f_type_id = 1")->where("t2.f_group_id NOT IN (5,6,7,8)")
              ->where("t4.f_selector_id IN (?)", $line['selectors'])
              ->query()->fetchAll(Zend_Db::FETCH_COLUMN);
            if(!$res_assets) continue;

            $select->reset();
            $res_asset_already_inserted = $select->from(['t1'=>'t_ware_wo'],'f_ware_id')
              ->where('f_wo_id = ?',$wo_code)->where("f_ware_id IN (?)", $res_assets)
              ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

            $asset_to_insert = array_diff($res_assets, $res_asset_already_inserted); // rimuovo gli asset già inseriti
            if(!$asset_to_insert) continue;

            foreach($asset_to_insert as $asset)
            {
                $data[] = [
                    'f_ware_id' => $asset,
                    'f_wo_id' => $wo_code,
                    'f_timestamp' => time()
                ];
            }
        }

        if($data)
        {
            Mainsim_Model_Utilities::massiveCrossInsert('t_ware_wo', $table_schema, $data);
        }

        $script = new Mainsim_Model_Script($this->db);
        foreach(array_keys($generators) as $wo_code) // aggiorno i campi fc_wo_asset per tutti i wo modificati
        {
            $script->fc_wo_asset($wo_code);
        }
    }

    protected function writePmPerAsset()
    {
        $select = new Zend_Db_Select($this->db);
        $expr = new Zend_Db_Expr("(SELECT COUNT(*) FROM t_periodics where f_code = t3.f_code and f_asset_code = t5.f_code)");
        $res_pms = $select->from(['t1' => 't_creation_date'],['periodic_count' => $expr])
          ->join(['t2' => 't_wf_phases'], "t1.f_phase_id = t2.f_number and t1.f_wf_id = t2.f_wf_id", [])
          ->join(['t3' => 't_workorders'], "t1.f_id = t3.f_code", ['f_code'])
          ->join(['t4' => 't_ware_wo'], "t3.f_code = t4.f_wo_id", [])
          ->join(['t5' => 't_wares'], "t4.f_ware_id = t5.f_code", ['asset_code' => 'f_code', 'fc_asset_installation_date'])
          ->where("t3.f_type_id = 3")->where("t5.f_type_id = 1")->where("t2.f_group_id NOT IN (5,6,7,8)")
          ->where("t3.fc_pm_type = 'pm-per-asset'")
          ->group(array("t3.f_code","t5.f_code","t5.fc_asset_installation_date"))
          ->having($expr . " = 0")
          ->query()->fetchAll();
        $wo = new Mainsim_Model_Workorders($this->db);
        foreach($res_pms as $pm)
        {
            $select->reset();
            $asset_check = $select->from(['t1' => 't_creation_date'], ['num' => 'count(*)'])
              ->join(['t2' => 't_wf_phases'], 't1.f_wf_id = t2.f_wf_id and t1.f_phase_id = t2.f_number', [])
              ->where("t1.f_id = ?", $pm['asset_code'])
              ->where("t2.f_group_id NOT IN (5,6,7,8)")
//              ->query()->fetch(Zend_Db::FETCH_COLUMN);
              ->query()->fetch();
            array_values($asset_check);
            //se l'asset è in un gruppo non valido oppure non ha di installation date non proseguo
            if(!$asset_check || !$pm['fc_asset_installation_date']) continue;
            //schedulo l'attività
            $wo->createPeriodics($pm['f_code'], 0, [$pm['asset_code'] => $pm['fc_asset_installation_date']]);
			/* se non ho PM per asset pronte per
			* essere generate, gli faccio generare almeno il tipo
			* di frequenza (Fix del 18 luglio 2016
			*/
			if(empty($res_pms))
			{
				$select->reset();
				$res_pms =  $select->from(['t1' => 't_creation_date'],[])
					->join(['t2' => 't_wf_phases'], "t1.f_phase_id = t2.f_number and t1.f_wf_id = t2.f_wf_id", [])
					->join(['t3' => 't_workorders'], "t1.f_id = t3.f_code", ['f_code'])
					->where("t3.f_type_id = 3")->where("t2.f_group_id NOT IN (5,6,7,8)")
					->where("t3.fc_pm_type = 'pm-per-asset'")
					->query()->fetchAll();
				foreach($res_pms as $pm)
				{
				   $wo->updateFcPmFrequency($pm['f_code']);
				}
			}
        }
    }
    
    private function fillWoInformations()
    {
        $script = new Mainsim_Model_Script($this->db, "t_workorders",0,array(),array(),false,$this->userData);
        $res_wos = $this->db->query("select f_code from t_workorders where fc_imp_code_wo_excel != ''")
                ->fetchAll();
        $tot = count($res_wos);
        for($i = 0;$i < $tot;++$i) {
            $script->setFcode($res_wos[$i]['f_code']);
            //set resource,tool and inventory icon
            $script->fc_wo_inventory();
            $script->fc_wo_tool();
            $script->fc_wo_rsc();
            $script->fc_wo_asset();
        }
        
        $script = new Mainsim_Model_Script($this->db, "t_wares",0,array(),array(),false,$this->userData);
        $res_was = $this->db->query("select t1.f_code,t1.f_type_id from t_wares as t1 
            join t_ware_wo as t2 on t1.f_code = t2.f_ware_id
            join t_workorders as t3 on t2.f_wo_id = t3.f_code
            where t1.f_type_id = 1 and t3.f_type_id IN (1,3,9,15)
            and t1.fc_imp_code_wares_excel != '' ")->fetchAll();
        $tot = count($res_was);
        for($i = 0;$i < $tot;++$i) {
            $script->setFcode($res_was[$i]['f_code']);
            $script->setParams(['f_type_id'=>$res_was[$i]['f_type_id']]);
            //set resource,tool and inventory icon
            $script->fc_asset_opened_pm();
            $script->fc_asset_opened_on_condition();
            $script->fc_asset_opened_std();
            $script->fc_asset_opened_wo();
        }
        
        //set fc_asset_wo count
        $res_wo_count = $this->db->query("select t1.f_code,count(*) as num from t_wares as t1
            join t_ware_wo as t2 on t1.f_code = t2.f_ware_id
            join t_workorders as t3 on t2.f_wo_id = t3.f_code
            join t_creation_date as t4 on t3.f_code = t4.f_id
            join t_wf_phases as t5 on t4.f_wf_id = t5.f_wf_id and t4.f_phase_id = t5.f_number
            where t1.f_type_id = 1 and t3.f_type_id in (1,4,5,6,7,10,12,13,20)
            and t5.f_group_id not in (6,7,8)
            group by t1.f_code")->fetchAll();
        $tot = count($res_wo_count);
        for($i = 0;$i < $tot;++$i) {            
            $this->db->update("t_wares",['fc_asset_wo'=>$res_wo_count[$i]['num'] ],"f_code = {$res_wo_count[$i]['f_code']}");
        }
    }
    
    
    
    private function createFathers($table,$type_id)
    {   
        $select = new Zend_Db_Select($this->db);
        $timestamp = time();        
        $cols = array('f_code','fc_imp_parent_code');       
        
        $col_code = $this->getImportColumn($table);
                
        if($table == 't_wares' && $type_id == 9) {
            $cols[] = 'fc_imp_parent_code_wizard';            
        }
        
        //object without father
        $objects = $select->from(array("t1"=>"t_custom_fields"),$cols)
            ->join(array("t3"=>$table),"t1.f_code = t3.f_code",array($col_code))                                        
            ->where("t3.f_type_id = {$type_id}")->where("t3.f_code not in (select f_code from {$table}_parent)")->query()->fetchAll();
                
        $tot_a = count($objects);
        for($a = 0;$a < $tot_a;$a++){        
            $obj = $objects[$a];
            $f_code = $obj['f_code'];
            $f_parent_code_excel = $obj['fc_imp_parent_code'];                            
            //search original f_code in t_custom_fields
            $f_parent_code = 0;
            if($f_parent_code_excel != "" ) {                    
                $parent_array = array();
                if(strpos($f_parent_code_excel,",")!== false) {
                    $parent_array = explode(",",$f_parent_code_excel);
                }
                else {
                    $parent_array = explode(".",$f_parent_code_excel);
                }                
                foreach($parent_array as $line_array) {                    
                    if($line_array == '') continue;
                    
                    $f_parent_code = $this->getFather($col_code, $line_array, $f_code);
                    $parent = array(
                        'f_parent_code'=>$f_parent_code,
                        'f_code'=>$f_code,
                        'f_active'=>1,
                        'f_timestamp'=>$timestamp         
                    );                         
                    $this->db->insert("{$table}_parent",$parent);
                }
            }            
            else {
                $parent = array(
                    'f_parent_code'=>$f_parent_code,
                    'f_code'=>$f_code,
                    'f_active'=>1,
                    'f_timestamp'=>time()                
                );                       
                $this->db->insert("{$table}_parent",$parent);                
            }            
        }        
        
        //search check if parent code = 0 is truely 0 or a parent code not found        
        $select->reset();
        
        $res_parent = $select->from(array("t1"=>"{$table}_parent"),array())
            ->join(array("t3"=>$table),"t1.f_code = t3.f_code",array($col_code))
            ->join(array("t2"=>"t_custom_fields"),"t1.f_code = t2.f_code",$cols)
            ->where("t1.f_active = 1")->where("f_type_id = ?",$type_id)
            ->where("f_parent_code = 0")->where("fc_imp_parent_code != ''")
            ->query()->fetchAll();
        
        $tot_c = count($res_parent);
        for($c = 0; $c < $tot_c; ++$c) {
            $line = $res_parent[$c];            
            if(!$line['fc_imp_parent_code']) continue;

            //if fc_imp_parent_code != 0, search father
            $parent_array = array();
            if(strpos($line['fc_imp_parent_code'],",")!== false) {
                $parent_array = explode(",",$line['fc_imp_parent_code']);
            }
            else {
                $parent_array = explode(".",$line['fc_imp_parent_code']);
            }
            foreach($parent_array as $line_array) {
                if($line_array == '') continue;
                
                $f_parent_code = $this->getFather($col_code, $line_array, $line['f_code']);
                //change parent code
                $this->db->update("{$table}_parent",array("f_parent_code"=>$f_parent_code),"f_code = {$line['f_code']} and f_active = 1");                
            }
        }                
    }
    
    /**
     * Stop import and write general error
     * @param type $error 
     */
    public static function reportError($error,$rollback = true)
    {
        $db = Zend_Db::factory(Zend_Registry::get('db'));
        $code = Zend_Registry::get('f_id');
        $db->update("t_creation_date",array("f_phase_id"=>4),"f_id = $code");
        $db->update("t_systems",array("fc_dmn_log"=>$error),"f_code = $code");     
        $db->insert("t_logs",array(
            'f_timestamp'=>time(),
            'f_log'=>$error,
            'f_type_log'=>0
        ));
        if($rollback) {
            self::deleteDataInserted();
        }
    }
    
    /**
     * Report specific query error during import
     * @param type $table : table where verify error
     * @param type $error : string of error
     * @param type $id : id of request to import
     */
    public static function reportQueryError($table,$error,$query,$delete_all = true)
    {        
        $id = Zend_Registry::get('f_id');        
        $db = Zend_Db::factory(Zend_Registry::get('db'));
        //insert a message error in t_queue
        $message = "Something went wrong during execution of a query in $table . This is the db error generated : ".
                $error."<br />and this is the query : ".$query."";               
        if($delete_all) {            
            self::deleteDataInserted();                        
        }  
        //update t_queue and insert log row
        $db->update("t_creation_date",array("f_phase_id"=>4),"f_id = $id");
        $db->update("t_systems",array("fc_dmn_log"=>$message),"f_code = $id");     
        $db->insert("t_logs",array(
            'f_timestamp'=>time(),
            'f_log'=>$message,
            'f_type_log'=>0
        ));
    }
    
    /**
     *  get sheets from update id and delete every data insert
     */
    private static function deleteDataInserted()
    {        
        $db = Zend_Db::factory(Zend_Registry::get('db'));
        $code = Zend_Registry::get('f_id');        
        $check_rollback = $db->query("select fc_dmn_rollback from t_systems where f_code = $code")->fetch();                
        if(strlen($check_rollback['fc_dmn_rollback']) == 0) return;
        $roll_back_list = unserialize($check_rollback['fc_dmn_rollback']);        
        if(is_array($roll_back_list)) {
            try {
                foreach($roll_back_list as $table => $id) {                    
                    $db->delete($table,"f_id > $id");                    
                }        
            }catch(Exception $e) {                       
                $db->insert("t_logs",array(
                    'f_timestamp'=>time(),
                    'f_log'=>$e->getMessage(),
                    'f_type_log'=>0
                ));
            }        
        }        
    }
    
    private function createCustomList($path,$pos)
    {
        $key_sheet = array();        
        $position = 1;
        $inc = 1000;      
        $custom = array();
        $file = Mainsim_Model_Utilities::IterateExcel($path,$position,$inc);
        $rowIterator = $file->setActiveSheetIndex($pos)->getRowIterator();            
        Mainsim_Model_Utilities::rowIteratorImport($rowIterator,$key_sheet,true);        
        $keys["Custom"] = $key_sheet;
        //extract and parse all custom fields    
        do {    
            $not_end = true;
            $file = Mainsim_Model_Utilities::IterateExcel($path,$position,$inc,"Custom");
            $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
            $custom = Mainsim_Model_Utilities::rowIteratorImport($rowIterator,$keys["Custom"],false); 
            if(count($custom)<$inc-1) $not_end = false;                  
            $position+=$inc;    
            $file->disconnectWorksheets();
            unset($file);
        }while($not_end);

        //change key of array with the f_title inside
        $tot_a = count($custom);
        $custom_copy = array();
        for($a = 0;$a < $tot_a;++$a) {
            if(!isset($custom[$a]['f_title']) || empty($custom[$a]['f_title'])) continue;
            $k = strtolower($custom[$a]['f_title']);
            $custom_copy[$k] = $custom[$a];
            $custom_copy[$k]['f_title'] = strtolower($custom[$a]['f_title']);            
        }        
        return $custom_copy;
    }
    
    private function createValidationList($path,$pos)
    {
        $key_sheet = array();
        $position = 1;
        $inc = 1000;           
        $validation = array();
        $file = Mainsim_Model_Utilities::IterateExcel($path,1,1);
        $rowIterator = $file->setActiveSheetIndex($pos)->getRowIterator();    
        Mainsim_Model_Utilities::rowIteratorImport($rowIterator,$key_sheet,true);
        $keys["Validation Tables"] = $key_sheet;        
        do {    
            $not_end = true;
            $file = Mainsim_Model_Utilities::IterateExcel($path,$position,$inc,"Validation Tables");
            $rowIterator = $file->setActiveSheetIndex()->getRowIterator();        
            $validation = Mainsim_Model_Utilities::rowIteratorImport($rowIterator,$keys["Validation Tables"],false); 
            if(count($validation)<$inc-1) $not_end = false;                  
            $position+=$inc;    
            $file->disconnectWorksheets();
            unset($file);
        }while($not_end);

        //change key of array with the f_title inside
        $tot_a = count($validation);
        $validation_copy = array();
        for($a = 0;$a < $tot_a;++$a) {        
            if(!isset($validation[$a]['f_category']) || empty($validation[$a]['f_category'])) continue;        
            $validation_copy[$validation[$a]['f_category']]['f_value_allow'][] = $validation[$a]['f_value'];
            $validation_copy[$validation[$a]['f_category']]['f_ui_category'][] = array(
                'label'=>$validation[$a]['f_label'],
                'value'=>$validation[$a]['f_value'],
                'selected'=>substr($validation[$a]['f_selected'],0,1)
            );
        }    
        return $validation_copy;
    }
    
    private function createRollBack($sheet,$table) 
    {
        $id = Zend_Registry::get('f_id');                
        $select = new Zend_Db_Select($this->db);                
        //f_id of start in case of rollback
        $f_id_starts = array();
        //this condition are only to catch the max f_id in case of rollback
        if(strpos($sheet,"t_cross_") !== false){ // simple cross
            switch($sheet) {
                case 't_cross_wares_selectors' : 
                    $res_max_id = $select->from("t_selector_ware",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_selector_ware"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_wares_wares' : 
                    $res_max_id = $select->from("t_wares_relations",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_wares_relations"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_workorders_wares' : 
                    $res_max_id = $select->from("t_ware_wo",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_ware_wo"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
                case 't_cross_workorders_selectors' : 
                    $res_max_id = $select->from("t_selector_wo",array("f_id"=>"MAX(f_id)"))->query()->fetch();
                    $f_id_starts["t_selector_wo"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
                    break;
            }
        }
        //t_pair_cross are saved in t_pair_cross table
        elseif($sheet == 't_pair_cross') {
            $res_max_id = $select->from("t_pair_cross",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_pair_cross"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id'];             
        }

        //if not empty table, compile ids relative table,table_parent,creation_date and custom fields
        if(!empty($table)) {        
            //do the same operation for father and custom fields
            $select->reset();        
            $res_max_id = $select->from("{$table}_parent",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["{$table}_parent"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id'];             
            
            $select->reset();
            $res_max_id = $select->from("t_periodics",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_periodics"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
            
            $select->reset();
            $res_max_id = $select->from($table.'_history',array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts[$table.'_history'] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 

            $select->reset();
            $res_max_id = $select->from($table,array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts[$table] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 

            $select->reset();        
            $res_max_id = $select->from("t_custom_fields_history",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_custom_fields_history"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 

            $select->reset();        
            $res_max_id = $select->from("t_custom_fields",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_custom_fields"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 

            $select->reset();        
            $res_max_id = $select->from("t_creation_date_history",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_creation_date_history"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
            
            $select->reset();        
            $res_max_id = $select->from("t_creation_date",array("f_id"=>"MAX(f_id)"))->query()->fetch();
            $f_id_starts["t_creation_date"] = empty($res_max_id['f_id'])?1:$res_max_id['f_id']; 
        }
        //close existing connection (will be re-open when finish system)
        $compress_roolback = serialize($f_id_starts);
        $this->db->update("t_systems",array("fc_dmn_rollback"=>$compress_roolback),"f_code = $id");
    }
    
    /**
     * create new column in t_custom_fields and t_custom_fields history
     * @param string $col_name : name of column
     * @param array $col_data : array with information about column
     */
    private function createCustomCol($col_name,$col_data) 
    {
        //get adapter to create right command
        $config = Zend_Registry::get('db');
        $config = $config->toArray();
        $history = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields_history");
        $pdo = $config['adapter'];
        $null = stripos($pdo,"mysql")!== false?'DEFAULT NULL':'NULL';
        // don't create column start at 'f_' or 'fc_'
        if(strpos($col_name,"fc_") === 0 ||  strpos($col_name,"f_") === 0) return;        
        
        $type_column = "TEXT";                        
        $type_custom = substr(str_replace(" : ","",$col_data['f_type']),1);
        switch($type_custom) {
            case 'VARCHAR':
                if(!isset($col_data['f_column_length']) || empty($col_data['f_column_length'])) {
                    self::reportError("The length of field $col_name must be setted");
                }
                $length = (int)$col_data['f_column_length'];
                $type_column = "VARCHAR({$length}) ";
                if(stripos($pdo,"mysql")!== false && $length > 256 ) {
                    $type_column = "TEXT";
                }
                break;
            case 'TEXT' : 
                $type_column = stripos($pdo,"mysql")!== false?"TEXT":"VARCHAR(MAX)";
                break;
            case 'BOOLEAN' :                
            case 'INT' :                
            case 'DATE' :
                $type_column = "INT";
                break;
            case "DOUBLE" : 
                $type_column = stripos($pdo,"mysql")!== false?"DOUBLE":"FLOAT";
                break;
        }                            
        //create column in t_custom_fields and t_custom_fields history
        try {
            $this->db->query("ALTER TABLE t_custom_fields ADD $col_name $type_column  $null");
        }catch(Exception $e) {                
            self::reportQueryError("t_custom_fields",$e->getMessage(),"ALTER TABLE t_custom_fields ADD $col_name $type_column $null");            
        };
        if(!in_array($col_name, $history)) {
            try {
                $this->db->query("ALTER TABLE t_custom_fields_history ADD $col_name $type_column  $null");
            }catch(Exception $e) {
                self::reportQueryError("t_custom_fields",$e->getMessage(),"ALTER TABLE t_custom_fields_history ADD $col_name $type_column $null");            
            };        
        }
    }
    
    
    /**
     * Method to compile wf/phases
     * @param type $tab
     * @return type 
     */
    private function setWfPhases()
    {   
        $array_wf_phases = array();
        $res = $this->db->query("select f_wf_id,f_number from t_wf_phases order by f_wf_id,f_number")->fetchAll();
        foreach($res as $line) {            
            $array_wf_phases[$line['f_wf_id']][] = $line['f_number'];
        }        
        Zend_Registry::set('array_wf_phases',$array_wf_phases);
    }
    
    private function setDefaultTableCols($table = '') 
    {
        if($table != '') {
            Zend_Registry::set('default_table_cols',Mainsim_Model_Utilities::get_tab_cols($table));
        }
    }
    
    private function setCreationTableCols() 
    {        
        Zend_Registry::set('creation_table_cols',Mainsim_Model_Utilities::get_tab_cols("t_creation_date"));        
    }
    
    private function setCustomTableCols() 
    {     
        Zend_Registry::set('custom_table_cols',Mainsim_Model_Utilities::get_tab_cols("t_custom_fields"));        
    }
    
    /**
     * Set default columns of table t_users 
     */
    private function setUserTableCols() 
    {
        Zend_Registry::set('user_table_cols',Mainsim_Model_Utilities::get_tab_cols("t_users"));        
    }  
    
    
    public function insertObj($row) 
    {        
        $select = new Zend_Db_Select($this->db);             
        $time = time();
        $f_type_id = $this->f_type_id;                
        $excel_code = $this->getImportColumn($this->table);
                
        $table_cols = Zend_Registry::get('default_table_cols');   
        $creation_cols = Zend_Registry::get('creation_table_cols');
        $custom_cols = Zend_Registry::get('custom_table_cols');        
        $special_cols = array();
        if(empty($row['f_type_id']) && !$f_type_id) return;       
        if(!empty($row['f_type_id'])) {
            $exp_type = explode(' : ',$row['f_type_id']);
            $f_type_id = $exp_type[0];
        }

        if($f_type_id == 16 && $this->table == 't_wares') {
            $special_cols = Zend_Registry::get('user_table_cols');
            if(!isset($row['fc_usr_language']) || empty($row['fc_usr_language'])) {
                $row['fc_usr_language'] = 1;
            }
        }
        
        $type_id_info = array();                        
        if(empty($row)) return;
        
        //get fc_progress
        $fc_progress = Mainsim_Model_Utilities::getFcProgress($this->table, $f_type_id, $this->db);
                
        if($this->table == 't_workorders' && !empty($row['fc_pm_start_date'])) { // prepare fields for scheduler
            $row['fc_pm_fixed'] = substr($row['fc_pm_fixed'], 0,1);
            $row['fc_pm_periodic_type'] = substr($row['fc_pm_periodic_type'], 0,1);
            $row['fc_pm_cyclic_type'] = substr($row['fc_pm_cyclic_type'], 0,1);
            $days_in_sec = ((int)$row['fc_pm_forewarning_days'] * 24*60*60);
            $hours_in_sec = ((int)$row['fc_pm_forewarning_hours'] * 60 * 60  );
            $minutes_in_sec = ((int)$row['fc_pm_forewarning_minutes'] * 60);
            $tot = $days_in_sec + $hours_in_sec + $minutes_in_sec;        
            $row['fc_pm_forewarning'] =  $tot;
                    
            $days_in_sec = ((int)$row['fc_pm_duration_days'] * 24*60*60);
            $hours_in_sec = ((int)$row['fc_pm_duration_hours'] * 60 * 60  );
            $minutes_in_sec = ((int)$row['fc_pm_duration_minutes'] * 60);
            $tot = $days_in_sec + $hours_in_sec + $minutes_in_sec;
            $row['fc_pm_duration'] =  $tot;
        
            $hours_in_sec = ((int)$row['fc_pm_attime_hours'] * 60 * 60  );
            $minutes_in_sec = ((int)$row['fc_pm_attime_minutes'] * 60);
            $tot = $hours_in_sec + $minutes_in_sec;                
            $row['fc_pm_at_time'] =  $tot;
        }
        
        $type_id_info = $select->from($this->table."_types")->where("f_id = ?",$f_type_id)->query()->fetch();

        //create f_code
        $creation = array(
            'f_creation_date'=>$time,
            'f_order'=>$this->f_order,
            'f_type'=>$this->table != "t_system_information"?strtoupper(str_replace('t_','',$this->table)):"SYSTEM",
            'f_category'=>  strtoupper($type_id_info['f_type']),
            'f_creation_user'=>1,                
            'f_visibility'=>-1,
            'f_editability'=>-1,
            'f_wf_id'=>$type_id_info['f_wf_id'],
            'f_phase_id'=>$type_id_info['f_wf_phase'],
            'f_timestamp'=>$time,
            'fc_progress'=>  $fc_progress
        );
        
        if(!empty($this->userData)) {
            $creation['fc_editor_user_name'] = $this->userData['fc_usr_firstname'].' '.$this->userData['fc_usr_lastname'];            
            $creation['fc_editor_user_avatar'] = $this->userData['fc_usr_avatar'];
            $creation['fc_editor_user_gender'] = $this->userData['f_gender'];
            $creation['fc_editor_user_phone'] = $this->userData['fc_usr_phone'];
            $creation['fc_editor_user_mail'] = $this->userData['fc_usr_mail'];
            // -- creator info
            $creation['fc_creation_user_name'] = $this->userData['fc_usr_firstname'].' '.$this->userData['fc_usr_lastname'];
            $creation['fc_creation_user_avatar'] = $this->userData['fc_usr_avatar'];
            $creation['fc_creation_user_gender'] = $this->userData['f_gender'];
            $creation['fc_creation_user_phone'] = $this->userData['fc_usr_phone'];
            $creation['fc_creation_user_mail'] = $this->userData['fc_usr_mail'];
        }

        $table_values = array(
            'f_timestamp'=>$time
        );
        
        if($this->table == 't_wares' && $this->f_type_id == 1) {
            $table_values['f_start_date'] = $time;
        }
        
        if($this->table == 't_wares') {
            $table_values['fc_asset_wo'] = 0;
        }
        if($this->table == 't_workorders') {
            $table_values['f_counter'] = 1;
        }

        $custom_values = array(
            'f_timestamp'=>$time,
            'f_main_table' => $this->table,
            'f_main_table_type' => $f_type_id,
        );

        $special_values = array();

        foreach($row as $field => $value) {
            $v = utf8_decode($value);
            $f = strtolower($field);
            if(in_array($f,$this->timeFieldList)) {
                $v = strtotime($value);
            }
            if(in_array($field,$creation_cols)) {                    
                $creation[$field] = $v;
            }
            elseif(in_array($f,$table_cols)) {                    
                $table_values[$f] = $v;
            }
            elseif(in_array($f,$custom_cols) || $f == 'f_parent_code') {      
                $field = $f == 'f_parent_code'? 'fc_imp_parent_code':$f;                    
                if(array_key_exists($field, $this->custom_fields)) {
                    $type_custom = substr(str_replace(" : ","",$this->custom_fields[$field]['f_type']),1);
                    switch($type_custom) {
                        case 'BOOLEAN' :$custom_values[$f] = (int)(bool)$v;                                
                            break;
                        case 'INT' : $custom_values[$field] = (int)$v;                                
                            break;
                        case 'DATE' : 
                            if(is_int($type_custom)){
                                $custom_values[$field] = (int)$v;
                            }
                            else {
                                $custom_values[$field] = (int)strtotime($v);
                            }
                            break;  
                        case 'VARCHAR' :
                            $max_lenght = ( (int)$this->custom_fields[$field]['f_column_length']);
                            if(strlen($v) > $max_lenght) {
                                self::reportError("Error : string $v for field $field is too long. Maximum characters allow: $max_lenght");
                            }
                            $custom_values[$field] = $v;
                            break;
                        default : $custom_values[$field] = $v;
                            break;
                    }
                }
                else {
                    $custom_values[$field] = $v;
                }
            }
            elseif(in_array($field,$special_cols)) {                    
                $special_values[$field] = $v;
            }
        }                
        $this->f_order++;       
        if(empty($creation['f_wf_id'])) {
            $creation['f_wf_id']= $type_id_info['f_wf_id'];
        }
        if(empty($creation['f_phase_id'])) {                
            $creation['f_phase_id'] = $type_id_info['f_wf_phase'];
        }
        if(empty($creation['f_visibility'])) {
            $creation['f_visibility']= -1;
        }
        if(empty($creation['f_editability'])) {                
            $creation['f_editability'] = -1;
        }
        
        $this->db->insert("t_creation_date",$creation);

        $f_code = $this->db->lastInsertId();


        $table_values['f_code'] = $f_code;    
        $custom_values['f_code'] = $f_code;

        $table_values['f_order'] = isset($row['f_order']) && $row['f_order']!=''?$row['f_order']:$this->f_order;
        $table_values['f_type_id'] = $f_type_id;                        
        $table_values['f_user_id'] = $this->userData['f_id'];            
        $table_values['f_priority'] = isset($row['f_priority'])  && $row['f_priority']!=''?$row['f_priority']:0;   
        if(!empty($excel_code)) {
            $table_values[$excel_code] = $row['f_code'];
        }       
        // transform date string to timestamp only for ontimescheduled (type 4)
        if(isset($table_values['fc_pm_periodic_type']) && $table_values['fc_pm_periodic_type'] == 4){
            $table_values['fc_pm_sequence'] = strtotime($table_values['fc_pm_sequence']);
        }
        //save table, custom and special table

        $this->db->insert($this->table,$table_values);                             
        $this->db->insert("t_custom_fields",$custom_values);
        //for special table                
        if($f_type_id == 16 && $this->table == 't_wares') {
            $special_values['f_code'] = $f_code;            
            $this->db->insert("t_users",$special_values);
        }

        //if fc_imp_parent_code = 0 or empty create directly record in parent table
        if(empty($custom_values['fc_imp_parent_code'])) {
            if($this->table != 't_selectors') {
                $this->db->insert($this->table.'_parent',array(
                    'f_code'=>$f_code,
                    'f_parent_code'=>0,
                    'f_active'=>1,
                    'f_timestamp'=>time()                    
                ));
            }
            else {
                $select->reset();
                $res_parent = $select->from(array("t1"=>"t_selectors"),"f_code")
                    ->join(array("t2"=>"t_selectors_parent"),"t1.f_code = t2.f_code",array())
                    ->where("f_type_id = ?",$this->f_type_id)
                    ->where("f_parent_code = 0")
                    ->query()->fetch();

                $this->db->insert('t_selectors_parent',array(
                    'f_code'=>$f_code,
                    'f_parent_code'=>$res_parent['f_code'],
                    'f_active'=>1,
                    'f_timestamp'=>time()                    
                ));

            }
        }  
        else {
            $exp_parents = explode(',',$custom_values['fc_imp_parent_code']);
            if(count($exp_parents) == 1) {
                $f_parent_code = $this->getFather($excel_code,$custom_values['fc_imp_parent_code'],$f_code);                    
                if($f_parent_code != 0) {
                    $this->db->insert($this->table.'_parent',array(
                        'f_code'=>$f_code,
                        'f_parent_code'=>$f_parent_code,
                        'f_active'=>1,
                        'f_timestamp'=>time()                    
                    ));
                }
            }
        }
        //check if has been passed a generator (pm or meter) if true, generate periodics
        //UPDATE: verifico se passato il campo fc_pm_type che sia di tipo classic, altrimenti non posso generare
        // la ciclicità
        if($this->table == 't_workorders'
          && !empty($table_values['fc_pm_start_date'])
          //&& (empty($table_values['fc_pm_type']))
        ){
            $engCreator = new Mainsim_Model_Workorders($this->db);
            $engCreator->createPeriodics($f_code);
        }
        Mainsim_Model_Utilities::newRecord($this->db, $f_code, 't_creation_date', array('f_timestamp'=>$time), "Import");
        Mainsim_Model_Utilities::newRecord($this->db, $f_code, $this->table, array('f_timestamp'=>$time), "Import");
        Mainsim_Model_Utilities::newRecord($this->db, $f_code, 't_custom_fields', array('f_timestamp'=>$time), "Import");
    }
    
    private function getFather($column,$codeImp,$f_code)
    {
        $select = new Zend_Db_Select($this->db);
        $res_parent = $select->from($this->table,"f_code")
            ->where("f_type_id = ?",$this->f_type_id)
            ->where("$column = ?",$codeImp)
            ->query()->fetch();                
        $f_parent_code = 0;
        if(!empty($res_parent)) {
            $f_parent_code = $res_parent['f_code'];
            if($res_parent['f_code'] == $f_code ) {
                throw new Exception("f_code and f_parent_code are the same. Wrong code : $codeImp");
            } 
            $codes = array();
            $codes = Mainsim_Model_Utilities::getChildCode($this->table.'_parent', $f_code, $codes, $this->db);
            if(in_array($f_parent_code, $codes)) {
                throw new Exception("You cannot use as father children element. Wrong code : $codeImp ");
            }
        }
        return $f_parent_code;
    }
    
    private function checkUniquesArray($field,$data = array(),$position = 0) 
    {   
        if(empty($data) || empty($field)) return;     
        $array_uniques = array();
        $tot_a = count($data);
        for($a = 0;$a < $tot_a;++$a) {            
            $val = $data[$a][$field];
            if($field == 'f_code' && is_float($val)) {
                $val = "'$val'";
            }
            if(in_array($val,$array_uniques)){
                self::reportError("Error occurred at line ".($position+$a)." : $field must be unique and not empty");
            }
            $array_uniques[] = $val;
        }
    }
    
    /**
     *Set order to insert in mainsim for specific type
     * @param type $table : table interested (t_wares,t_workorders or t_selectors)
     * @param type $f_type_id : type id for the order
     */
    private function setCreationDateOrder() 
    {        
        $select = new Zend_Db_Select($this->db);
        //get name of type
        $res_name_type = $select->from("{$this->table}_types","f_type")->where("f_id = ?",$this->f_type_id)->query()->fetch();
        $type = strtoupper($res_name_type['f_type']);
        $select->reset();
        $table = $this->table != 't_system_information'?str_replace('t_','',$this->table):"SYSTEM";
        $res_f_order = $select->from("t_creation_date",array("f_order"=>"MAX(f_order)"))
                ->where("f_type = ?",  strtoupper($table))->where("f_category = ?",$type)->query()->fetch();        
        $order = (int)$res_f_order['f_order'];           
        $order = $order?$order:1;
        $this->f_order = $order;        
    }
        
    private function setTimeFieldList($sheet)
    {
        $check = array('check'=>'valid_date','message'=>'Invalid date format . ');        
        if(!empty($this->mdl)){            
            $prop = Mainsim_Model_Utilities::getBookmark($this->mdl);                
            $properties = json_decode(Mainsim_Model_Utilities::chg($prop['f_properties']));            
            $tot_a = count($properties);
            for($a = 0;$a < $tot_a;++$a) {
                if($properties[$a][2] == 3) {                    
                    $this->timeFieldList[] = $properties[$a][0];
                    $this->default_check_list[$properties[$a][0]] = $check;
                }
            }                       
        }
        elseif($sheet == 't_pair_cross') {
            $select = new Zend_Db_Select($this->db);
            $res = $select->from("t_ui_object_instances",array("f_instance_name"))
                    ->where("f_type_id = 9")->where("f_properties like '%moTreeGrid_light_check_pair%'")
                    ->query()->fetchAll();
            $tot = count($res);
            for($i = 0;$i < $tot;++$i) {
                $prop = Mainsim_Model_Utilities::getBookmark($res[$i]['f_instance_name']);                
                $properties = json_decode(Mainsim_Model_Utilities::chg($prop['f_properties']));
                $tot_a = count($properties);
                for($a = 0;$a < $tot_a;++$a) {
                    if($properties[$a][2] == 3) {
                        $this->timeFieldList[$properties[$a][0]] = $properties[$a][0];
                        $this->default_check_list[$properties[$a][0]] = $check;
                    }
                }
            }
        }
    }
    
    /**
     * Set User information about who import 
     * File 
     */
    private function setUserData()
    {
        $code = Zend_Registry::get('f_id');
        $sel = new Zend_Db_Select($this->db);
        $res_uid = $sel->from("t_creation_date","f_creation_user")->where("f_id = ?",$code)->query()->fetch();
        $sel->reset();
        $this->userData = $sel->from(array("t1"=>"t_users"),array(
            'f_id'=>'f_code',                        
            'fc_usr_usn',            
            'fc_usr_firstname',
            'fc_usr_lastname',
            'fc_usr_mail',
            'f_gender'=>'fc_usr_gender',            
            'fc_usr_avatar',
            'fc_usr_address',
            'fc_usr_phone'
            )
        )
        ->join(array("t2"=>"t_creation_date"),"t1.f_code = t2.f_id",array('f_displayedname'=>'f_title', "creation_account" => "f_creation_date"))
        ->where("t2.f_id = ?",$res_uid['f_creation_user'])->query()->fetch();             
    }
            
    
    /**
     * set sheet configuration array, table,f_type_id ,mdl objects,default and creation table 
     * @param type $sheet 
     */
    private function setImportConfiguration($file,$sheet,$importType,$position)
    {
        $select = new Zend_Db_Select($this->db);
        /*if($importType == 'instant') {
            $_SESSION[$importSessionId][' = new Zend_Session_Namespace('import'.Zend_Registry::get('f_id'));
        }*/
        $importSessionId = 'import'.Zend_Registry::get('f_id');
        if($position > 1 && $importType == 'instant') {//use configuration stored in session                                    
            $this->table = $_SESSION[$importSessionId]['table'];
            $this->f_type_id = $_SESSION[$importSessionId]['f_type_id'];
            $this->mdl = $_SESSION[$importSessionId]['mdl'];
            $this->sheet_configuration = $_SESSION[$importSessionId]['sheet_configuration'];
            return;
        }
        if(strpos($sheet,"t_wares") === 0) {
            $res_conf = $select->from("t_wares_types",array('f_id','f_short_name','f_sheet_name','f_module_name'))
                    ->where("f_sheet_name = ?",$sheet)->query()->fetch();    
            $table = "t_wares";
        }
        elseif(strpos($sheet,"t_workorders") === 0) {
            $res_conf = $select->from("t_workorders_types",array('f_id','f_short_name','f_sheet_name','f_module_name'))
                    ->where("f_sheet_name = ?",$sheet)->query()->fetch();
            $table = "t_workorders";
        }elseif(strpos($sheet,"t_selector") === 0) {
            $res_conf = $select->from("t_selectors_types",array('f_id','f_short_name','f_sheet_name','f_module_name'))
                    ->where("f_sheet_name = ?",$sheet)->query()->fetch();
            $table = "t_selectors";
        }
        
        $select->reset();            
        if(!empty($res_conf)) {
            $f_type_id = $res_conf['f_id'];
            $this->sheet_configuration['table'] = $table;
            $this->sheet_configuration['f_type'] = $res_conf['f_id'];
            if(!is_null($res_conf['f_module_name'])) {
                $this->sheet_configuration['mdl'] = $res_conf['f_module_name'];
            }
            if(!is_null($res_conf['f_short_name'])) {
                $this->sheet_configuration['short_name'] = $res_conf['f_short_name'];
            }
        }
        elseif(strpos($sheet,"t_selector") !== false){            
            $f_type_id = (int)str_replace("t_selector_","",$sheet);    
            $table = 't_selectors';        
            $this->sheet_configuration['table'] = $table;
            $this->sheet_configuration['f_type'] = $f_type_id;
        }
        $this->table = $table;
        $this->f_type_id = $f_type_id;
        $this->mdl = isset($res_conf['f_module_name'])?$res_conf['f_module_name']:'';
        if($importType == 'instant') { //if instant, store setted variable in session            
            $_SESSION[$importSessionId]['f_type_id'] = $f_type_id;
            $_SESSION[$importSessionId]['mdl'] = $this->mdl;
            $_SESSION[$importSessionId]['sheet_configuration'] =  $this->sheet_configuration;
            $_SESSION[$importSessionId]['table'] = $table;
        }
    }
    
    /**
     * 
     */
    public function setRootSelectors()
    {
        $select = new Zend_Db_Select($this->db);
        //check if father already exists
        $res_check =$select->from(array("t1"=>"t_selectors"),"f_code")
                ->join(array("t2"=>"t_selectors_parent"),"t1.f_code = t2.f_code",array())
                ->where("f_parent_code = 0")->where("f_type_id = ?",  $this->f_type_id)
                ->query()->fetch();
        
        if(!empty($res_check)) {
            return;
        }
        $select->reset();
        $res = $select->from("t_selectors_types")->where("f_id = ?",$this->f_type_id)
                ->query()->fetch();
        
        if(empty($res)) {
            return;
        }
        //get fc_progress
        $fc_progress = Mainsim_Model_Utilities::getFcProgress("t_selectors", $this->f_type_id, $this->db);
        
        $time = time();
        $cd = array(
            'f_type'=>'SELECTORS',
            'f_category'=>$res['f_type'],
            'f_title'=>$res['f_type'],
            'f_wf_id'=>!empty($res['f_wf_id'])?$res['f_wf_id']:9,
            'f_phase_id'=>!empty($res['f_wf_phase'])?$res['f_wf_phase']:1,
            'f_visibility'=>-1,
            'f_editability'=>-1,
            'f_creation_date'=>$time,
            'f_timestamp'=>$time,
            'f_creation_user'=>$this->userData['f_id'],
            'fc_editor_user_name'=>$this->userData['fc_usr_firstname'].' '.$this->userData['fc_usr_lastname'],
            'fc_editor_user_avatar'=>$this->userData['fc_usr_avatar'],
            'fc_editor_user_gender'=>$this->userData['f_gender'],
            'fc_editor_user_phone'=>$this->userData['fc_usr_phone'],
            'fc_editor_user_mail'=>$this->userData['fc_usr_mail'],
            'fc_creation_user_name'=>$this->userData['fc_usr_firstname'].' '.$this->userData['fc_usr_lastname'],
            'fc_creation_user_avatar'=>$this->userData['fc_usr_avatar'],
            'fc_creation_user_gender'=>$this->userData['f_gender'],
            'fc_creation_user_phone'=>$this->userData['fc_usr_phone'],
            'fc_creation_user_mail'=>$this->userData['fc_usr_mail'],
            'fc_progress'=>$fc_progress,
            'f_order'=>1            
        );
        $this->db->insert("t_creation_date",$cd);
        $f_code = $this->db->lastInsertId();
        
        $this->db->insert("t_selectors",array(
            "f_code"=>$f_code,
            "f_type_id"=>$this->f_type_id,
            "f_timestamp"=>$time,
            "f_order"=>1,
            "f_user_id"=>$this->userData['f_id'],
            "f_priority"=>1
        ));
        
        $this->db->insert("t_selectors_parent",array(
            'f_code'=>$f_code,
            'f_parent_code'=>0,
            "f_timestamp"=>$time,
            "f_active"=>1
        ));
        
        $this->db->insert("t_custom_fields",array(
            'f_code'=>$f_code,
            'f_timestamp'=>$time,
            'f_main_table'=>'t_selectors',
            'f_main_table_type'=>$this->f_type_id
        ));
    }
    
    /**
     *  Create Hierarchy
     */
    private function createParentField()
    {           
        $select = new Zend_Db_Select($this->db);   
        $code = Zend_Registry::get('f_id'); 
        $f_start = $select->from("t_systems","f_timestamp")
                ->where("f_code = ?",$code)->query()->fetch();
        if(empty($f_start)) {
            $f_start['f_timestamp'] = mktime(0,0,1);
        }
        $parent_table = $this->table."_parent";
        //searching elements with parent       
        $select->reset();
        $res_f_codes = $select->from(array('t1'=>$this->table),array('f_code'))                    
            ->join(array("t2"=>$parent_table),"t1.f_code = t2.f_code",array())
            ->where("f_type_id = ?",$this->f_type_id)->where("f_parent_code != 0")->where("f_active = 1")       
            ->where("t2.f_timestamp between {$f_start['f_timestamp']} and ".time())
            ->query()->fetchAll();       
        $tot = count($res_f_codes);
        $scriptModule = new Mainsim_Model_Script($this->db,$this->table);
        for($i = 0;$i < $tot;++$i){//foreach($res_f_codes as $f_code) {
            $scriptModule->fc_hierarchy($res_f_codes[$i]['f_code']);
        }                
    }    
    
    /**
     * Crypt password for users 
     */
    public function cryptPwd() 
    {        
        //$this->db = Zend_Registry::get('db');
        $login = new Mainsim_Model_Login();        
        $select = new Zend_Db_Select($this->db);
        if(isset($this->max_user_id)){
            $res = $select->from("t_users",array("f_code","fc_usr_pwd"))
                    ->join("t_wares","t_users.f_code = t_wares.f_code",array())                
                    ->where("t_wares.f_type_id = 16")
                    ->where("t_wares.f_code > " . $this->max_user_id)
                    ->query()->fetchAll();     
        }
        else{
            $res = $select->from("t_users",array("f_code","fc_usr_pwd"))
                    ->join("t_wares","t_users.f_code = t_wares.f_code",array())                
                    ->where("t_wares.f_type_id = 16")
                    ->where("t_wares.f_code > 5")
                    ->query()->fetchAll();     
        }
        $tot = count($res);
        for($a = 0;$a < $tot;++$a) {
            $line = $res[$a];
            $pwd = $login->crypt(md5($line['fc_usr_pwd']));            
            $this->db->update("t_users",array("fc_usr_pwd"=>$pwd),"f_code = {$line['f_code']}");            
        }
    }
    
    
    private function completeOnCondition()
    {
        $select = new Zend_Db_Select($this->db);                
        $res_oc = $select->from(array("t1"=>"t_workorders"),array(
            'f_code',
            'fc_cond_condition',
            'fc_cond_list_function',
            'fc_cond_code_meter',
            'fc_cond_value_reference',
            'fc_cond_run_operation',
            'fc_cond_operator',
            'fc_cond_average_number_value',
            'fc_cond_action_code'
        ))->where("t1.f_type_id = 9")->query()->fetchAll();        
                
        $script = new Mainsim_Model_Script($this->db,  $this->table,0,array(),array(),false,$this->userData);
        foreach($res_oc as $line) {
            if(empty($line['fc_cond_code_meter'])) continue;
            $select->reset();
            $res_code_meter = $select->from("t_workorders","f_code")
                    ->where("fc_imp_code_wo_excel = ?",$line['fc_cond_code_meter'])
                    ->where("f_type_id = 11")->query()->fetch();
            if(empty($res_code_meter)) continue;
            $this->db->update("t_workorders",array("fc_cond_code_meter"=>$res_code_meter['f_code']),"f_code = {$line['f_code']}");
            $line['fc_cond_code_meter'] = $res_code_meter['f_code'];
            try {
                $script->setFcode($line['f_code']);
                $script->setParams($line);                
                $script->fc_cond_condition();
            }catch(Exception $e) {
                
            }
        }        
    }
    
    /**
     * Create cross between selectors (and parent) and passed element (wares/workorders)
     * @param type $line cross to insert
     * @param type $table table to extract system code (wares/workorders)
     */    
    private function crossSelectors($crosses = array(),$table = 't_wares') 
    {           
        $time = time();
        $select = new Zend_Db_Select($this->db);                            
        //field of excel code in table
        $field = $table == 't_wares'?'fc_imp_code_wares_excel':'fc_imp_code_wo_excel';
        // fields of excel 
        $field_code_excel = $table == 't_wares'?'f_code_wares':'f_code_workorders'; // field used as code in excel
        $field_type_excel = $table == 't_wares'?'f_type_id_wares':'f_type_id_workorders'; // field used as type in excel
        // fields to insert data in cross table
        $table_cross = $table == 't_wares'?'t_selector_ware':'t_selector_wo';
        $field_cross = $table == 't_wares'?'f_ware_id':'f_wo_id';

        $sel = $oth = [];
        array_walk($crosses, function(&$cross) use(&$sel, &$oth, $field_code_excel, $field_type_excel){
            if($cross['f_code_selector'] == '' || $cross['f_type_id_selector'] == ''
              || $cross[$field_code_excel] == '' || $cross[$field_type_excel] == ''){
                $cross = null;
            }
            
            $cross[$field_type_excel] = (int)substr($cross[$field_type_excel], 0,  strpos($cross[$field_type_excel], " :"));
            $cross['f_type_id_selector'] = (int)substr($cross['f_type_id_selector'], 0,  strpos($cross['f_type_id_selector'], " :"));
            $sel[$cross['f_type_id_selector']][] = $cross['f_code_selector'];
            $oth[$cross[$field_type_excel]][] = $cross[$field_code_excel];
        });
        //rimuovo i null dall'elenco
        $crosses = array_filter($crosses);
        //get true codes of selectors

        foreach(array_keys($sel) as $type_id){
            $sel[$type_id] = $this->getMainsimCodes("t_selectors", $type_id, $sel[$type_id]);
        }
        //get true codes of wos
        foreach(array_keys($oth) as $type_id) {
            $oth[$type_id] = $this->getMainsimCodes($table, $type_id, $oth[$type_id]);
        }

        //insert cross
        $table_schema = ['f_selector_id', $field_cross, 'f_timestamp', 'f_nesting_level'];
        $data = [];

        foreach($crosses as $line){
            $type_oth = $line[$field_type_excel];
            $type_sel = $line['f_type_id_selector'];
            if(!isset($sel[$type_sel][$line['f_code_selector']]) || !isset($oth[$type_oth][$line[$field_code_excel]])) continue;
            $fcodesel = $sel[$type_sel][$line['f_code_selector']];
            $fcodeoth = $oth[$type_oth][$line[$field_code_excel]];
            $select->reset();
            $check = $select->from($table_cross,["num"=>"count(*)"])->where("f_selector_id = ?",$fcodesel)
              ->where($field_cross." = ?",$fcodeoth)
              ->query()->fetch();
            if($check['num'] > 0) continue;
            //insert cross
            $data[] = [
              'f_selector_id'=>$fcodesel,
              $field_cross=>$fcodeoth,
              'f_timestamp'=>  $time,
              'f_nesting_level'=>1
            ];
        }
        if($data){
            Mainsim_Model_Utilities::massiveCrossInsert($table_cross, $table_schema, $data);
        }
    }
    
    /*
     * Cross all wares with other wares
     * in t_wares_relations table
     */    			        
    private function crossAllWares($crosses = array())
    {
        $time = time();        
        $select = new Zend_Db_Select($this->db);        
        $wa_master = [];
        $wa_slave = [];

        array_walk($crosses, function(&$cross) use(&$wa_master, &$wa_slave){
            if($cross['f_code_wares_master'] == '' || $cross['f_type_id_wares_master'] == ''
              || $cross['f_code_wares_slave'] == '' || $cross['f_type_id_wares_slave'] == ''){
                $cross = null;
                return;
            }
            $cross['f_type_id_wares_master'] = (int)substr($cross['f_type_id_wares_master'], 0,  strpos($cross['f_type_id_wares_master'], " :"));
            $cross['f_type_id_wares_slave'] = (int)substr($cross['f_type_id_wares_slave'], 0,  strpos($cross['f_type_id_wares_slave'], " :"));
            $wa_master[$cross['f_type_id_wares_master']][] = $cross['f_code_wares_master'];
            $wa_slave[$cross['f_type_id_wares_slave']][] = $cross['f_code_wares_slave'];
        });

        //rimuovo i null dall'elenco
        $crosses = array_filter($crosses);

        //get true codes of wares
        foreach(array_keys($wa_master) as $type_id){
            $wa_master[$type_id] = $this->getMainsimCodes("t_wares", $type_id, $wa_master[$type_id]);
        }

        //get true codes of wos
        foreach(array_keys($wa_slave) as $type_id) {
            $wa_slave[$type_id] = $this->getMainsimCodes("t_wares", $type_id, $wa_slave[$type_id]);
        }

        //insert cross
        $table_schema = [
          'f_code_ware_master', 'f_type_id_master',
          'f_code_ware_slave', 'f_type_id_slave', 'f_timestamp'
        ];
        $data = [];

        foreach($crosses as $line){
            $type_master = $line['f_type_id_wares_master'];
            $type_slave = $line['f_type_id_wares_slave'];

            if(!isset($wa_master[$type_master][$line['f_code_wares_master']]) ||
               !isset($wa_slave[$type_slave][$line['f_code_wares_slave']])) continue;
            $fcodemaster = $wa_master[$type_master][$line['f_code_wares_master']];
            $fcodeslave = $wa_slave[$type_slave][$line['f_code_wares_slave']];
            $select->reset();
            $check = $select->from("t_wares_relations",["num"=>"count(*)"])
              ->where("f_code_ware_master = ?",$fcodemaster)
              ->where("f_code_ware_slave = ?",$fcodeslave)->query()->fetch();
            if(!$check['num']){
                //insert cross
                $data[] = [
                  'f_code_ware_master'=>$fcodemaster,
                  'f_type_id_master' => $type_master,
                  'f_code_ware_slave'=>$fcodeslave,
                  'f_type_id_slave' => $type_slave,
                  'f_timestamp'=>  $time
                ];
            }

            //verifico anche la cross opposta
            $select->reset();
            $check = $select->from("t_wares_relations",["num"=>"count(*)"])
              ->where("f_code_ware_master = ?",$fcodeslave)
              ->where("f_code_ware_slave = ?",$fcodemaster)->query()->fetch();
            if(!$check['num']){
                //insert cross
                $data[] = [
                  'f_code_ware_master'=>$fcodeslave,
                  'f_type_id_master' => $type_slave,
                  'f_code_ware_slave'=>$fcodemaster,
                  'f_type_id_slave' => $type_master,
                  'f_timestamp'=>  $time
                ];
            }
        }

        if($data){
            Mainsim_Model_Utilities::massiveCrossInsert("t_wares_relations", $table_schema, $data);
        }
    }
    
    /**
     * system to cross wares and workorders     
     */    
    private function crossWaresWorkorders($crosses = array())
    {           
        $select = new Zend_Db_Select($this->db);    
        $time = time();
        $wa = $wo = [];

        array_walk($crosses, function(&$cross) use(&$wa, &$wo){
            if($cross['f_code_wares'] == '' || $cross['f_type_id_wares'] == ''
              || $cross['f_code_workorder'] == '' || $cross['f_type_id_workorder'] == ''){
                $cross = null;
            }
            $cross['f_type_id_workorder'] = (int)substr($cross['f_type_id_workorder'], 0,  strpos($cross['f_type_id_workorder'], " :"));
            $cross['f_type_id_wares'] = (int)substr($cross['f_type_id_wares'], 0,  strpos($cross['f_type_id_wares'], " :"));
            $wa[$cross['f_type_id_wares']][] = $cross['f_code_wares'];
            $wo[$cross['f_type_id_workorder']][] = $cross['f_code_workorder'];
        });

        //rimuovo i null dall'elenco
        $crosses = array_filter($crosses);

        //get true codes of wares
        foreach(array_keys($wa) as $type_id){
            $wa[$type_id] = $this->getMainsimCodes("t_wares", $type_id, $wa[$type_id]);
        }
        
        //get true codes of wos
        foreach(array_keys($wo) as $type_id) {
            $wo[$type_id] = $this->getMainsimCodes("t_workorders", $type_id, $wo[$type_id]);
        }
        
        //insert cross
        $table_schema = ['f_ware_id', 'f_wo_id', 'f_timestamp'];
        $data = [];

        foreach($crosses as $line){
            $type_wo = $line['f_type_id_workorder'];
            $type_wa = $line['f_type_id_wares'];

            if(!isset($wa[$type_wa][$line['f_code_wares']]) || !isset($wo[$type_wo][$line['f_code_workorder']])) continue;
            $fcodewa = $wa[$type_wa][$line['f_code_wares']];
            $fcodewo = $wo[$type_wo][$line['f_code_workorder']];
            $select->reset();
            $check = $select->from("t_ware_wo",array("num"=>"count(*)"))->where("f_ware_id = ?",$fcodewa)
                    ->where("f_wo_id = ?",$fcodewo)->query()->fetch();
            if($check['num'] > 0) continue;
            //insert cross
            $data[] = [
                'f_ware_id'=>$fcodewa,
                'f_wo_id'=>$fcodewo,            
                'f_timestamp'=>  $time
            ];
        }

        if($data){
            Mainsim_Model_Utilities::massiveCrossInsert("t_ware_wo", $table_schema, $data);
        }
    }

    //restituisce un elenco di f_code in base al codice di importazione passato
    protected function getMainsimCodes($table, $type_id, $data)
    {
        if(!$table || !$type_id ||  !$data) {
            return [];
        }
        $result = [];
        $field = $this->getImportColumn($table);
        $select = new Zend_Db_Select($this->db);
        $res_codes = $select->from($table, [$field, "f_code"])
          ->where("f_type_id = ?", $type_id)
          ->where($field." IN ('" . implode("','", $data)  . "')")
          ->query()->fetchAll();
        foreach($res_codes as $code) {
            $result[$code[$field]] = (int)$code['f_code'];
        }

        return $result;
    }

    /**
     *Set empty workorders with wo of asset 
     */
    private function selectorWoFromAsset()
    {
        $select = new Zend_Db_Select($this->db);                
        $res_wo = $select->from(array("t1"=>"t_ware_wo"),"f_wo_id")
                ->join(array("t2"=>"t_wares"),"f_code = f_ware_id",array('f_code'))                
                ->where("t2.f_type_id = 1")->where("not exists(select * from t_selector_wo where f_wo_id = t1.f_wo_id)")
                ->query()->fetchAll();
        $tot_a = count($res_wo);
        for($a = 0;$a < $tot_a;++$a) {            
            $wares_list = implode(',',  Mainsim_Model_Utilities::getParentCode("t_wares_parent",$res_wo[$a]['f_code'],array($res_wo[$a]['f_code']),$this->db));
            $select->reset();
            $res_sel = $select->from("t_selector_ware",array("f_selector_id","f_timestamp","f_nesting_level"))->where("f_ware_id IN ($wares_list)")
                    ->query()->fetchAll();
            $tot_b = count($res_sel);
            $data = [];
            $table_schema = ['f_wo_id', 'f_selector_id', 'f_timestamp', 'f_nesting_level'];
            foreach($res_sel as $sel_line){
                $data[] = [
                    'f_wo_id'=>$res_wo[$a]['f_wo_id'],
                    'f_selector_id'=>$sel_line['f_selector_id'],
                    'f_timestamp'=>$sel_line['f_timestamp'],
                    'f_nesting_level'=>$sel_line['f_nesting_level']
                ];
            }
            Mainsim_Model_Utilities::massiveCrossInsert('t_selector_wo', $table_schema, $data);
        }
    }
    
    /**
     * Create a pair cross 
     * between workorders and wares
     * @param type $data
     * @param type $custom_fields 
     */
    public function insertPairCross($line) 
    {       
        $time = time();        
        $select = new Zend_Db_Select($this->db);           
        $tables = array(            
            "t_workorders_types"=>array('table'=>'t_workorders','column'=>'fc_imp_code_wo_excel'),
            "t_wares_types"=>array('table'=>'t_wares','column'=>'fc_imp_code_wares_excel'),
            "t_selectors_types"=>array('table'=>'t_selectors','column'=>'fc_imp_code_sel_excel')            
        );                
        
        //table with list of possible cross between main table and cross table
        $tableCross = array(
            't_workorders'=>array(
                't_wares'=>array(
                    array('table'=>'t_ware_wo','f_code_main'=>'f_wo_id','f_code_cross'=>'f_ware_id')
                ),
                't_workorders'=>array(
                    array('table'=>'t_wo_relations','f_code_main'=>'f_code_wo_master','f_code_cross'=>'f_code_wo_slave','f_type_main'=>'f_type_id_wo_master','f_type_cross'=>'f_type_id_wo_slave'),
                    array('table'=>'t_wo_relations','f_code_cross'=>'f_code_wo_master','f_code_main'=>'f_code_wo_slave','f_type_main'=>'f_type_id_wo_slave','f_type_cross'=>'f_type_id_wo_master')
                ),
                't_selectors'=>array(
                    array('table'=>'t_selector_wo','f_code_main'=>'f_wo_id','f_code_cross'=>'f_selector_id')
                )
            ),
            't_wares'=>array(
                't_wares'=>array(
                    array('table'=>'t_wares_relations','f_code_main'=>'f_code_ware_master','f_code_cross'=>'f_code_ware_slave','f_type_main'=>'f_type_id_master','f_type_cross'=>'f_type_id_slave'),
                    array('table'=>'t_wares_relations','f_code_cross'=>'f_code_ware_master','f_code_main'=>'f_code_ware_slave','f_type_main'=>'f_type_id_slave','f_type_cross'=>'f_type_id_master')
                ),
                't_selectors'=>array(
                    array('table'=>'t_selector_wo','f_code_main'=>'f_wo_id','f_code_cross'=>'f_selector_id')
                ),
                't_workorders'=>array(
                    array('table'=>'t_ware_wo','f_code_main'=>'f_ware_id','f_code_cross'=>'f_wo_id')
                )
            ),
            't_selectors'=>array(
                't_wares'=>array(
                    array('table'=>'t_selector_ware','f_code_main'=>'f_selector_id','f_code_cross'=>'f_ware_id')
                ),
                't_workorders'=>array(
                    array('table'=>'t_selector_wo','f_code_main'=>'f_selector_id','f_code_cross'=>'f_wo_id')
                )
            )
        );
        
        if(empty($line['f_type_main']) || empty($line['f_code_main']) || empty($line['f_code_cross']) || empty($line['f_type_cross'])){                             
            return;
        }
        
        //get main table and field
        $expMainType = explode(' : ',$line['f_type_main']);
        $typeMain = $expMainType[0];
        foreach($tables as $tableType => $tableVal){
            $select->reset();
            $resExist = $select->from($tableType,"f_id")->where("f_id = ?",$expMainType[0])
                    ->where("f_type = ?",  strtoupper($expMainType[1]))
                    ->query()->fetch();
            if(!empty($resExist)) {
                $table_main = $tableVal['table'];
                $field_main = $tableVal['column'];
                break;
            }
        }
        
        //get cross table and field
        $expCrossType = explode(' : ',$line['f_type_cross']);   
        $typeCross = $expCrossType[0];
        foreach($tables as $tableType => $tableVal){
            $select->reset();
            $resExist = $select->from($tableType,"f_id")->where("f_id = ?",$expCrossType[0])
                    ->where("f_type = ?",strtoupper($expCrossType[1]))
                    ->query()->fetch();
            if(!empty($resExist)) {
                $table_cross = $tableVal['table'];
                $field_cross = $tableVal['column'];
                break;
            }
        }
        
        if(!isset($table_main) || !isset($table_cross)) {
            return;
        }
        
        $select->reset();        
        $resCodeMain = $select->from($table_main,array("f_code",$field_main))
            ->where("f_type_id = ?",$typeMain)
            ->where("$field_main = ?",$line['f_code_main'])->query()->fetch();
        if(empty($resCodeMain)) {
            return;
        }
        $fCodeMain = $resCodeMain['f_code'];
        
        $select->reset();        
        $resCodeCross = $select->from($table_cross,array("f_code",$field_cross))
            ->where("f_type_id = ?",$typeCross)
            ->where("$field_cross = ?",$line['f_code_cross'])
            ->query()->fetch();        
        if(empty($resCodeCross)) {
            return;
        }
        $fCodeCross = $resCodeCross['f_code'];
        
        if(!empty($line['f_group_code'])) {
            $select->reset();
            $resGroupCode = $select->from("t_wares",array("f_code","fc_imp_code_wares_excel"))
                ->where("f_type_id = 1")->where("fc_imp_code_wares_excel = ?",$line['f_group_code'])
                ->query()->fetch();
            if(empty($resGroupCode)) {
                return;
            }
            $fGroupCode = $resGroupCode['f_code'];
        }

        $pair_cross = array();
        
        if(isset($fGroupCode)) {
            //check if already exists cross between main wo and asset
            $select->reset();
            $res_wa_wo = $select->from("t_ware_wo",array('num'=>'count(*)'))
                ->where("f_ware_id = ?",$fGroupCode)
                ->where("f_wo_id = ?",$fCodeMain)
                ->query()->fetch();
            if($res_wa_wo['num'] == 0) {//not exists, create cross                
                $ware_wo = array(
                    "f_ware_id"=>$fGroupCode,
                    "f_wo_id"=>$fCodeMain,                    
                    "f_timestamp"=>$time
                );
                $this->db->insert("t_ware_wo",$ware_wo);
            }
        }            

        $select->reset();
        if(!isset($tableCross[$table_main][$table_cross])) {
            return;
        }
        //check if insert Cross
        foreach($tableCross[$table_main][$table_cross] as $checkCross) {
            $select->reset();
            $resCheckCross = $select->from($checkCross['table'],'f_id')
                ->where("{$checkCross['f_code_main']} = ?",$fCodeMain)
                ->where("{$checkCross['f_code_cross']} = ?",$fCodeCross)
                ->query()->fetch();
            if(empty($resCheckCross)) {
                $insertCross = array(
                    'f_timestamp'=>$time,
                    $checkCross['f_code_main']=>$fCodeMain,
                    $checkCross['f_code_cross']=>$fCodeCross
                );
                if(isset($checkCross['f_type_main'])) {
                    $insertCross[$checkCross['f_type_main']] = $typeMain;
                    $insertCross[$checkCross['f_type_cross']] = $typeCross;
                }
                $this->db->insert($checkCross['table'],$insertCross);
            }
        }
        
        $black_list = array('f_type_main','f_type_cross','f_code');

        foreach($line as $key => &$value) {
            if(!in_array($key,$black_list)) {
                if(is_null($value)){ 
                    $value = null;
                }
                if(in_array($key,$this->timeFieldList)){
                    $value = strtotime ($value);
                }
                $pair_cross[$key] = utf8_decode($value);
            }
        }              
        $pair_cross['f_timestamp']  = $time;
        $pair_cross['f_code_main']  = $fCodeMain;
        $pair_cross['f_code_cross'] = $fCodeCross;
        $pair_cross['f_group_code'] = isset($fGroupCode)?$fGroupCode:null;
        $this->db->insert("t_pair_cross",$pair_cross);                    
    }
    
    private function createBookmark()
    {
        $select = new Zend_Db_Select($this->db);          
        //keep fields present in custom_fields sheet
        $bookmark = array();
        foreach($this->fields as $value) {
            if(!array_key_exists($value, $this->custom_fields)) continue;            
            $bookmark[$value] = $this->custom_fields[$value];
        }        
        if(empty($this->mdl)) return;
        //extract default bookmarks                        
        $res_bm = $select->from(array("t1"=>"t_creation_date"),"f_id")
            ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code",array())    
            ->where("fc_bkm_user_id = 0")->where("f_category = 'BOOKMARK'")
            ->where("fc_bkm_module = ?",$this->mdl)->query()->fetchAll();        
        $tot = count($res_bm);             
        //add missing custom
        for($a = 0;$a <$tot;++$a){ 
            try {
                $default_bm = array();
                $select->reset();
                $res_bm_pair = $select->from("t_bookmarks","fc_bkm_bind")
                        ->where("f_code = ?",$res_bm[$a]['f_id'])
                        ->query()->fetchAll();
                $tot_bp = count($res_bm_pair);
                for($bp = 0;$bp < $tot_bp;++$bp) {
                    $default_bm[] = $res_bm_pair[$bp]['fc_bkm_bind'];
                }                
                foreach($bookmark as $key => $value) {                
                    if(in_array($key,$default_bm)) continue;
                    $type = 0; //TESTO
                    $explode_type = explode(" : ",$value['f_type']);
                    $explode_bkm = array();
                    if(isset($value['f_bkm_visibility']) && !empty($value['f_bkm_visibility'])) {
                        $explode_bkm = explode(' : ',$value['f_bkm_visibility']);
                    }
                    switch($explode_type[0]) {
                        case 0 : //NUMERO                    
                        case 1 : $type = 1;
                            break;
                        case 5 : //DATE
                            $type = 3;
                            break;
                        case 6 : //BOOLEAN
                            $type = 2;
                            break;
                        case 4 : //ENUM
                            $type = 4;
                            break;
                    }
                    $new_pair = array(
                        'f_code'=>$res_bm[$a]['f_id'],                                                
                        'f_timestamp'=>time(),
                        'fc_bkm_bind'=>$value['f_title'],
                        'fc_bkm_label'=>trim(preg_replace('/\r|\n/',' ', $value['f_label'])),
                        'fc_bkm_type'=>$type,
                        'fc_bkm_locked'=>0,
                        'fc_bkm_visible'=>!empty($explode_bkm)?(int)$explode_bkm[0]:1,
                        'fc_bkm_width'=>110,
                        'fc_bkm_filter'=>$type == 4?json_encode($value['f_value']):"[]",
                        'fc_bkm_range'=>"[]",
                        'fc_bkm_prange'=>"[]",
                        'fc_bkm_script'=>is_null($value['f_conversion'])?'':$value['f_conversion'],
                        'fc_bkm_level'=>-1,
                        'fc_bkm_group' => ''
                    );
                    $this->db->insert("t_bookmarks",$new_pair);
                }
            }catch(Exception $e) {                       
                $this->db->insert("t_logs",array(
                    'f_timestamp'=>time(),
                    'f_type_log'=>0,
                    'f_log'=>$e->getMessage()
                ));
            }
        }        
    }
    
    public function createUi()
    {
        if(!isset($this->sheet_configuration['short_name'])) return;        
        $uiCreator = new MainsimAdmin_Model_UI();
        $short_name = $this->sheet_configuration['short_name'];
        $select = new Zend_Db_Select($this->db);
        //verifico se i campi all'interno corrispondono         
        $custom_fields = array();               
        foreach($this->fields as $line) {                        
            if(array_key_exists($line, $this->custom_fields)){
                $custom_fields[] = $this->custom_fields[$line];                
            }            
        }        
        
        //search every module linked to this table and create ui        
        $res_module = $select->from("t_ui_object_instances",array("f_instance_name","f_properties"))
            ->where("f_type_id = 9")->where("(f_instance_name like '%{$short_name}_edit%')")
            ->where("f_properties like '%moModuleComponent%'")->query()->fetchAll();        
        foreach($res_module as $module ) {            
            //now check if this module is an edit of pair cross
            $select->reset();
            $create_tg = substr($module['f_instance_name'],0,strpos($module['f_instance_name'],"edit_")).'tg';            
            $res_pair = $select->from("t_ui_object_instances",array('f_code'))
                ->where("f_type_id = 9")->where("f_instance_name = ?",$create_tg)
                ->where("f_properties like '%moTreeGrid_light_check_pair%'")->query()->fetchAll();
            
            //if this is a pair edit module, continue
            if(!empty($res_pair)){
                continue;
            }            
            //now create a part of the name used inside the ui
            $create_tg = substr($module['f_instance_name'],0,strpos($module['f_instance_name'],"edit_")+5).'tg';            
            $ui_name = str_replace("_tg","",str_replace("mdl_", "", $create_tg));            
            //find the fields create customs
            foreach($custom_fields as $line) {                
                $type = substr(str_replace(" : ","",$line['f_ui_type']),1);            
                $select->reset();
                switch($type) {
                    case "TEXTAREA":
                    case "TEXTFIELD" : 
                        $ui_check = "txtfld_{$ui_name}_{$line['f_title']}";
                        $res_check = $select->from("t_ui_object_instances",array("f_code"))
                                ->where("f_instance_name = ?",$ui_check)->query()->fetchAll();
                        if(!empty($res_check)) continue;                        
                        $uiCreator->createTxt($line,$ui_name);                        
                        break;
                    case "CHECKBOX" : 
                        $ui_check = "chkbx_{$ui_name}_{$line['f_title']}";
                        $res_check = $select->from("t_ui_object_instances",array("f_code"))
                                ->where("f_instance_name = ?",$ui_check)->query()->fetchAll();
                        if(!empty($res_check)) continue;
                        $uiCreator->createChk($line,$ui_name);                        
                        break;
                    case "SELECT" : 
                        $ui_check = "slct_{$ui_name}_{$line['f_title']}";
                        $res_check = $select->from("t_ui_object_instances",array("f_code"))
                                ->where("f_instance_name = ?",$ui_check)->query()->fetchAll();
                        if(!empty($res_check)) continue;
                        $uiCreator->createSelect($line,$ui_name,$this->validation[$line['f_title']]['f_ui_category']);                        
                        break;
                }
            }            
        }          
    }
    
    private function clearConfig()
    {
        $this->table = '';
        $this->f_type_id = '';
        $this->mdl = '';
        $this->sheet_configuration = array();
        $this->fields = array();
        $this->f_order = 0;        
        $this->timeFieldList = array();        
    }
    
    private function getImportColumn($table)
    {
        $col_code = '';
        if($table == 't_wares') {
            $col_code = 'fc_imp_code_wares_excel';            
        }
        elseif($table == 't_workorders') {
            $col_code = 'fc_imp_code_wo_excel';            
        }
        else{
            $col_code = 'fc_imp_code_sel_excel';            
        }
        return $col_code;
    }
    
    private function getImportFile($f_code,$importType)
    {
        $size = $importType == 'instant' ? 5242880:15728640;
        $select = new Zend_Db_Select($this->db);
        $res_file = $select->from("t_attachments",array("f_path"))->where("f_code = ?",$f_code)->query()->fetch();
        $errorFileMessage = "";

        if(empty($res_file) || !file_exists($res_file['f_path'])) {
            $errorFileMessage = "System didn't found excel to import";
        }
        elseif(filesize($res_file['f_path']) > $size ){
            $errorFileMessage = "System cannot import file over 5MB in $importType mode";
        }

        if(!empty($errorFileMessage)) {
            self::reportError($errorFileMessage);            
            return array('response'=>'ko','message'=>$errorFileMessage);
        }
        return $res_file['f_path'];            
    }

    
    /**
     * @param type $path
     * @param type $dir_code
     */
    public function removeFileDir($path,$dir_code = '')
    {
        $path = realpath($path);        
        $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);        
        $files = iterator_to_array($objects);
        foreach($files as $name => $obj){
            if(is_file($name)) {            
                @unlink($name);            
            }
        }        
        if(empty($dir_code)){            
            $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);            
            $files = iterator_to_array($objects);
            foreach($files as $name => $obj){
                if( is_dir($name) && strpos($name,'.') === false) {                    
                    @rmdir($name);            
                }
            }            
        }
        else{
            @rmdir($dir_code);
        }
    }
    
    public function getSubTypes($type) {
        $q = new Zend_Db_select($this->db);
        $q->from($type, array())->where("");
    }
    
    
    private function getTypeInfo($table,$type)
    {
        $select = new Zend_Db_Select($this->db);
        $resType = $select->from("$table")->where("f_type = ?",$type)
            ->query()->fetch();
        if(empty($resType)) {
            throw new Exception("Unkown type '$type' in $table");
        }
        return $resType;
    }
    
    
    public function clearObj($table,$category,$delete_ui = false,$delete_bm = false)
    {
        $type = strtoupper(str_replace("t_", "", $table));        
        $response = array(
            'message'=>"Operation finish successfully",
            'status'=>'ok'
        );
        
        $MS4_tables = [
            'WARES'=>[
              't_ms4_wizard_wa'=>'f_ware_id',
              't_ms4_hierarchy'=>'f_code'
            ],
            'WORKORDERS'=>[
              't_ms4_wizard_wo'=>'f_workorder_id',
              't_ms4_hierarchy'=>'f_code'
            ],
            'SELECTORS' => [
                't_ms4_selectors_cache' => 'f_code'
            ]
        ];

        $tables = [
            'WARES'=>[
              't_selector_ware'=>'f_ware_id','t_ware_wo'=>'f_ware_id',"t_wares_systems"=>"f_ware_id",
              't_pair_cross'=>['f_code_main','f_code_cross'],
              't_wares_relations'=>['f_code_ware_master','f_code_ware_slave'],
              't_selector_ware'=>'f_ware_id','t_users'=>'f_code','t_periodics' => 'f_asset_code'
            ],
            'WORKORDERS'=>[
              't_selector_wo'=>'f_wo_id','t_ware_wo'=>'f_wo_id','t_meters_history'=>'f_code',
              't_wo_relations'=>['f_code_wo_master','f_code_wo_slave'],
              't_periodics'=>'f_code','t_pair_cross'=>['f_code_main','f_code_cross']
            ],
            'SELECTORS'=>['t_selector_ware'=>'f_selector_id','t_selector_wo'=>'f_selector_id','t_selector_system'=>'f_selector_id'],
            'SYSTEMS'=>["t_selector_system"=>"f_system_id","t_wares_systems"=>"f_system_id"],
        ];
        try {              
            $this->db->beginTransaction();     
            if($table == 'cross') {                
                $this->db->delete($category);
                $this->db->commit();
                return $response;
                
            }
            $resType = $this->getTypeInfo($table."_types",$category);                        
            $this->db->delete('t_progress_lists',"f_table = '{$table}' and f_type_id = {$resType['f_id']}");                    
            //delete ui 
            if($delete_ui == 1 && !empty($resType['f_short_name'])) {                   
                $this->clearUi($resType['f_short_name']);
            }
            //delete bm
            if($delete_bm == 1) {
                $this->clearBm($resType['f_short_name']);    
            }
            $filter = '';
            if($category == 'USER') { $filter = ' AND f_id > 37'; }            
            if($type == 'SELECTORS') { $filter = ' AND f_id > 19'; }            
            $this->clearAttachments($type, $category);
            
            try{
                foreach($MS4_tables[$type] as $MS4_tableName => $MS4_field){
                    if(!is_array($MS4_field)){
                        $this->db->delete($MS4_tableName,"$MS4_field IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");                     
                    }
                    else {
                        foreach($MS4_field as $MS4_lineField) {
                            $this->db->delete($MS4_tableName,"$MS4_lineField IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");
                        }
                    } 
                } 
                
                } catch (Exception $ex) {
                   
                    
            }
            
            foreach($tables[$type] as $tableName => $field) {                
                if(!is_array($field)){
                    $this->db->delete($tableName,"$field IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");                     
                }
                else {
                    foreach($field as $lineField) {
                        $this->db->delete($tableName,"$lineField IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");
                    }
                }
            }
            $this->clearCommonTables($type, $category,$filter);
            $this->db->commit();            
            
        }catch(Exception $e) {
            die(var_dump($e->getMessage()));
            $this->db->rollBack();
            $response['message'] = $e->getMessage();
            $response['status'] = "ko";
        }
        return $response;
    }
    
    private function clearCommonTables($type,$category,$filter = '')
    {
        $table = "t_".strtolower($type);        
        $this->db->delete("t_custom_fields_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");   
        $this->db->delete("t_custom_fields","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");            
        $this->db->delete($table."_parent","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");
        $this->db->delete($table."_history","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");                                 
        $this->db->delete("$table","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category' $filter)");                                     
        $this->db->delete("t_attachments","f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = '$type' AND f_category = '$category')");
        $this->db->delete("t_creation_date_history","f_type = '$type' AND f_category = '$category' $filter");            
        $this->db->delete("t_creation_date","f_type = '$type' AND f_category = '$category' $filter");            
    }
    
    private function clearAttachments($type,$category)
    {
        $user = '';
        if($category == 'USER' && $type == 'WARES') {
            $user = ' AND f_id > 5';
        }
        $res_attach = $this->db->query("SELECT distinct(f_code) as f_code from t_attachments where f_code IN (SELECT f_id FROM t_creation_date WHERE f_type = 'WARES' AND f_category = '$category' $user)")->fetchAll();
        $tot_at = count($res_attach);
        $folder = Zend_Registry::get('attachmentsFolder');
        for($at = 0;$at < $tot_at;++$at) {
            $path_doc = realpath($folder."/doc/{$res_attach[$at]['f_code']}");
            if(!empty($path_doc)) {
                $this->removeFileDir($path_doc,$path_doc);
            }
            $path_img = realpath($folder."/img/{$res_attach[$at]['f_code']}");
            if(!empty($path_img)) {
                $this->removeFileDir($path_img,$path_img);
            }
        }
    }
    
    private function clearUi($searched)
    {           
        $sel = new Zend_Db_Select($this->db);        
        //find every module for remove ui                
        $res_modules = $sel->from("t_ui_object_instances",array("f_code","f_instance_name","f_properties"))
            ->where("f_type_id = 9")->where("f_instance_name like '%{$searched}_edit%'")
            ->where("f_properties like '%moModuleComponent%'")->query()->fetchAll();           
        $tot = count($res_modules);
        for($a = 0;$a < $tot;++$a) {        
            $line_mod = $res_modules[$a];
            $sel->reset();
            $create_tg = substr($line_mod['f_instance_name'],0,strpos($line_mod['f_instance_name'],"edit_")).'tg';                        
            $res_pair = $sel->from("t_ui_object_instances",array('num'=>'count(f_code)'))
                ->where("f_type_id = 9")->where("f_instance_name = ?",$create_tg)
                ->where("f_properties like '%moTreeGrid_light_check_pair%'")->query()->fetch();                        
            if($res_pair['num'] > 0) continue;            
            //decode the properties and get the list of component
            $mod_prop = json_decode($line_mod['f_properties'],true);
            $fields = explode(',',$mod_prop['fields']);
            
            $accep_fields = array();
            $tot_b = count($fields);
            $sel->reset();
            $sel->from("t_ui_object_instances",array("f_code","f_instance_name","f_properties"));
            $f_code_delete = array();
            for($b = 0;$b < $tot_b;++$b) {
                $expFields = explode('|',$fields[$b]);//check if field is pipe separeted
                $tot_c = count($expFields);
                $field = array();
                for($c = 0;$c < $tot_c;++$c){
                    $sel->reset(Zend_Db_Select::WHERE);
                    $res_ui = $sel->where("f_instance_name = ?",$expFields[$c])->query()->fetch();                
                    if(empty($res_ui)) continue;
                    //check if ui is binded with system column or custom
                    $ui_prop = json_decode($res_ui['f_properties'],true);
                    if(strpos($ui_prop['bind'],'f_',0) !== 0 &&  strpos($ui_prop['bind'],'fc_',0) !== 0) {                        
                        $f_code_delete[] = $res_ui['f_code'];
                    }
                    else {
                        $field[] = $expFields[$c];
                    }
                }
                if(!empty($field)){
                    $accep_fields[] = implode('|',$field);
                }
            }                
            if(!empty($f_code_delete)) { 
                $quoteCode = $this->db->quoteInto("f_code IN (?)", $f_code_delete);
                $this->db->delete("t_ui_object_instances",$quoteCode);                    
            }
            $mod_prop['fields'] = implode(',',$accep_fields);                        
            $enc = json_encode($mod_prop);
            $this->db->update("t_ui_object_instances",array("f_properties"=>$enc,"f_timestamp"=>time()),"f_code = '{$line_mod['f_code']}'");
        }                
    }
    
    /**
     * delete custom columns from bookmark
     * @param type $searched
     */
    private function clearBm($searched)
    {   
        $sel = new Zend_Db_Select($this->db);
        $module = "mdl_{$searched}_tg";
        $res_bm_name = $sel->from(array("t1"=>"t_creation_date"),array("f_id"))
            ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code",array())                                
            ->where("f_type_id = 3")->where("fc_bkm_module = ?",$module)
            ->query()->fetchAll();                
        $tot_bm = count($res_bm_name);        
        for($bm = 0;$bm < $tot_bm;++$bm) {
            //find true bm
            $sel->reset();
            $res_bm_fields = $sel->from("t_bookmarks",array("f_id","fc_bkm_bind"))
                ->where("f_code = ?",$res_bm_name[$bm]['f_id'])
                ->where("(fc_bkm_bind not like 'fc_%' AND fc_bkm_bind not like 'f_%')")
                ->query()->fetchAll();  
            $tot_fields = count($res_bm_fields);
            for($b = 0;$b < $tot_fields;++$b) {                
                $this->db->delete("t_bookmarks","f_id = {$res_bm_fields[$b]['f_id']}");
            }              
        }           
    }
    
    /**
     * Remove custom view
     * @param type $module
     */
    private function deleteBookmarks($module = '') 
    {
        $select = new Zend_Db_Select($this->db);        
        $delete_condition = "";
        $delete_condition_creation = "";
        $delete_condition_creation_history = "";        
        $select->from(array("t1"=>"t_creation_date"),"f_id")
                ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code",array())
                ->where("fc_bkm_user_id != 0")                
                ->where("f_category = 'BOOKMARK'");
        
        if($module != '') {
            $select->where("fc_bkm_module = ?",$module);                        
        }        
        $res_code = $select->query()->fetchAll();          
        $codes = array();
        $tot_code = count($res_code);
        for($c = 0;$c < $tot_code;++$c) {
            $codes[] = $res_code[$c]['f_id'];
        }
        if(!empty($codes)) {
            $codes = implode(',',$codes);                    
            $delete_condition = $codes;
            $delete_condition_creation = "f_id IN ($codes)";
            $delete_condition_creation_history = "f_code IN ($codes)";
        }
        else return;
        $this->db->delete("t_systems_parent","f_code IN ($delete_condition)");                                
        $this->db->delete("t_custom_fields_history","f_code IN ($delete_condition)");            
        $this->db->delete("t_custom_fields","f_code IN ($delete_condition)");
        $this->db->delete("t_selector_system","f_system_id IN ($delete_condition)");
        $this->db->delete("t_wares_systems","f_system_id IN ($delete_condition)");            
        $this->db->delete("t_bookmarks","f_code IN ($delete_condition)");        
        $this->db->delete("t_systems_history","f_code IN ($delete_condition)");
        $this->db->delete("t_systems","f_code IN ($delete_condition)");        
        $this->db->delete("t_locked","f_code IN ($delete_condition)");        
        $this->db->delete("t_creation_date_history","f_type = 'SYSTEM' AND $delete_condition_creation_history");      
        $this->db->delete("t_creation_date","f_type = 'SYSTEM' AND $delete_condition_creation");
    }
    
        
    public function clearCustom() 
    {           
        $custom_cols = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
        foreach($custom_cols as $line) {
            if((strpos($line,"fc_") === false && strpos($line,"f_") === false)
               || (strpos($line,"fc_") != 0 || strpos($line,"f_") != 0) ) {
                $this->db->query("alter table t_custom_fields drop column {$line}");
                $this->db->query("alter table t_custom_fields_history drop column {$line}");
            }
        }        
    }
    
    /**
     * destroy all data and recover default installation
     */
    public function destroyAll()
    {
        try {
            $this->db->beginTransaction();
            $select = new Zend_Db_Select($this->db);
            $folder = Zend_Registry::get('attachmentsFolder');
            $time = time();
            $tableTypes = array('t_wares_types','t_workorders_types','t_selectors_types');            
            $checked = array();
            for($i = 0;$i < 3;++$i){
                $select->reset();
                $res = $select->from($tableTypes[$i],"f_short_name")->query()->fetchAll();
                //delete all ui and custom       
                $tot_a = count($res);                
                for($a = 0;$a < $tot_a;++$a) {
                    if(!in_array($res[$a]['f_short_name'], $checked)){                        
                        $this->clearUi($res[$a]['f_short_name']);                             
                        $this->clearBm($res[$a]['f_short_name']); // remove custom fields from default view
                        $checked[] = $res[$a]['f_short_name'];
                    }
                }        
            }      
            
            $this->deleteBookmarks(); //delete custom view            
            $this->clearCustom(); // delete custom cols                        
            $restoreArray = $this->createRestoreFile(); // create array to restore configuration                                    
            $truncateTables = array(
                't_wares_parent','t_wares_relations','t_ware_wo','t_selectors_support',
                't_access_registry_history','t_users','t_bookmarks',
                't_wares_systems','t_progress_lists','t_selectors_parent','t_selector_wo',
                't_selector_ware','t_selector_system','t_workorders_parent','t_systems_parent',
                't_wo_relations','t_pair_cross','t_periodics','t_custom_fields_history',
                't_custom_fields','t_workorders_history','t_wares_history','t_selectors_history',
                't_creation_date_history','t_logs','t_reverse_ajax','t_locked','t_attachments','t_queue','t_meters_history',
                "t_wares","t_workorders","t_selectors","t_systems","t_creation_date"
            );
                        
            $tot_b = count($truncateTables);
            $is_mysql = $this->isMysql();
            for($b = 0;$b< $tot_b;++$b){                
                $this->db->delete($truncateTables[$b]);
                if($is_mysql) {
                    $this->db->query("ALTER TABLE {$truncateTables[$b]} AUTO_INCREMENT = 1");
                }
                else {
                    try {$this->db->query("DBCC CHECKIDENT ({$truncateTables[$b]}, RESEED, 0)");}catch(Exception $e){}
                }
            }            
            
            $sqlStart = file_get_contents(APPLICATION_PATH."/../library/Mainsim/start.sql");            
            if(!$is_mysql)  {            
                $this->db->query("SET IDENTITY_INSERT t_creation_date ON");                        
            }                
            $sqlStart = str_replace('UNIX_TIMESTAMP()',time(),$sqlStart);
            $this->db->query($sqlStart);  
            
            foreach($restoreArray as $table => $rows) {
                $tot = count($rows);                                    
                for($i = 0; $i < $tot; ++$i) {
                    if($table == 't_creation_date') {
                        $tableObj = "t_".strtolower($rows[$i]['f_type']);
                        $select->reset();
                        $resType = $select->from($tableObj.'_types','f_id')->where("f_type = ?",$rows[$i]['f_category'])
                            ->query()->fetch();
                        if(!empty($resType['f_id'])) {
                            $rows[$i]['fc_progress'] = Mainsim_Model_Utilities::getFcProgress($tableObj, $resType['f_id'], $this->db);
                        }
                    }
                    $this->db->insert($table,$rows[$i]);
                }                    
            }                
            
            if(!$is_mysql)  {            
                $this->db->query("SET IDENTITY_INSERT t_creation_date OFF");                        
            }
            $response['message'] = "strage";
            $response['status'] = "ok";
            
            //clear all file attachments
            $this->removeFileDir($folder."/temp");
            $this->removeFileDir($folder."/doc");
            $this->removeFileDir($folder."/img");    
            $this->removeFileDir($folder."/data manager/importer");    
            $this->removeFileDir($folder."/data manager/exporter");    
            $this->removeFileDir($folder."/data manager/module importer");  
            //remove session
            Zend_Auth::getInstance()->clearIdentity();
            $this->db->commit();
            $this->db->query("TRUNCATE TABLE t_access_registry");
        }catch(Exception $e) {            
            @unlink($folder.'/temp/restore.sql');
            $this->db->rollBack();
            $response['message'] = $e->getMessage();
            $response['status'] = "ko";
        }
        return $response;
    }
    
    private function createRestoreFile()
    {
        $insert = array();
        //prepare file with query to creation date, system,parent and custom fields
        $select = new Zend_Db_Select($this->db);        
        $creationTable = Mainsim_Model_Utilities::get_tab_cols('t_creation_date');
        $systemTable = Mainsim_Model_Utilities::get_tab_cols('t_systems');
        $parentTable = Mainsim_Model_Utilities::get_tab_cols('t_systems_parent');
        $customTable = Mainsim_Model_Utilities::get_tab_cols('t_custom_fields');        
        unset($creationTable[0]);unset($systemTable[0]);unset($parentTable[0]);unset($customTable[0]);        
        $f_code_start = 1000; //new start f_code
        //get system settings excluding level
        $res_system_cd = $select->from(array("t1"=>"t_creation_date"))
            ->join(array("t2"=>"t_systems"),"t1.f_id = t2.f_code")                                
            ->join(array("t3"=>"t_systems_parent"),"t3.f_code = t2.f_code")                                
            ->join(array("t4"=>"t_custom_fields"),"t1.f_id = t4.f_code")                                
            ->where("t2.f_type_id NOT IN (4,6)")->query()->fetchAll(); 

        $tot_a = count($res_system_cd);    
        $time = time();
        for($a = 0;$a < $tot_a;++$a) {            
            $creationDate = array();
            $system = array();
            $systemParent = array();
            $custom = array();           
            $line = $res_system_cd[$a];                        
            $f_code = $line['f_code'];        
            unset($line['f_id']);unset($line['f_timestamp']);unset($line['f_code']);
            $tot_b = count($creationTable);            
            for($b = 1;$b < $tot_b;++$b) {                
                $creationDate[$creationTable[$b]] = $line[$creationTable[$b]];
            }
            $creationDate['f_id'] = $f_code_start;$creationDate['f_timestamp'] = $time;
            $creationDate['f_creation_date'] = $time;
            $insert['t_creation_date'][] = $creationDate;
            
            $tot_c = count($systemTable);            
            for($c = 1;$c < $tot_c;++$c) {
                $system[$systemTable[$c]] = $line[$systemTable[$c]];
            }
            $system['f_code'] = $f_code_start;$system['f_timestamp'] = $time;
            $insert['t_systems'][] = $system;
            
            $tot_d = count($parentTable);
            for($d = 1;$d < $tot_d;++$d) {
                $systemParent[$parentTable[$d]] = $line[$parentTable[$d]];           
            }
            $systemParent['f_code'] = $f_code_start;$systemParent['f_timestamp'] = $time;
            $insert['t_systems_parent'][] = $systemParent;
            
            $tot_e = count($customTable);
            for($e = 1;$e < $tot_e;++$e) {
                $custom[$customTable[$e]] = $line[$customTable[$e]];
            }
            $custom['f_code'] = $f_code_start;$custom['f_timestamp'] = $time;
            $insert['t_custom_fields'][] = $custom;
                                    
            $select->reset();
            //get bookmark column        
            $res_pair_cross = $select->from("t_bookmarks")->where("f_code = ?",$f_code)->query()->fetchAll();
            $tot_pc = count($res_pair_cross);            
            for($pc = 0;$pc < $tot_pc;++$pc) {
                $line = $res_pair_cross[$pc];
                $line['f_code'] = $f_code_start;            
                $line['f_timestamp'] = $time;                        
                unset($line['f_id']);                
                $insert['t_bookmarks'][] = $line;
            }
            $f_code_start++;            
        }        
        return $insert;
    }
}