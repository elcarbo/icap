<?php

class MainsimAdmin_Model_Dbupdater
{
    /**
     *
     * @var Zend_Db 
     */
    private $db,$masterConn,$slaveConn;
    private $configPath,$fileList;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        $this->configPath = APPLICATION_PATH."/configs/dbupdate";
        $this->fileList = $this->configPath."/select_ini.jsn";
    }
    
    /**
     * read ini file
     * @param string $filename
     */
    public function readIniFile($filename)
    {
        $content = "[]";
        if(file_exists($this->configPath."/$filename")){
            $content = file_get_contents($this->configPath."/$filename");
        }
        return $content;
    }
    
    /**
     * Add new config file
     * @param string $filename filename
     * @param string $content json content for new file
     */
    public function saveIniFile($filename,$content)
    {
        if(!file_exists($this->configPath."/$filename")) {
            $this->updateIniList($filename, "add");              
        }  
        file_put_contents($this->configPath."/$filename",$content);
    }
    
    /**
     * remove config file
     * @param string $filename filename
     */
    public function deleteIniFile($filename)
    {
        @unlink($this->configPath."/$filename");
        $this->updateIniList($filename, "delete");
    }
    
    /**
     * update select_ini.jsn
     * @param string $filename filename 
     * @param string $action operation request(add/remove)
     */
    public function updateIniList($filename,$action)
    {
        $list = json_decode(file_get_contents($this->fileList));
        if($action == "add") { $list[] = $filename; }
        elseif($action == "delete") {
            $pos = array_search($filename, $list);
            if($pos!== false) {unset($list[$pos]); }
        }
        $list = array_values($list);
        file_put_contents($this->fileList,  json_encode($list));        
    }
    
    /**
     * Apply action to database slave 
     * @param array $master db master information
     * @param array $slave db slave information
     * @param array $action action to apply
     */
    public function applyAction($master,$slave,$action,$limit)
    {
        $config = Zend_Registry::get('db')->toArray();
        $config['params']['dbname'] = $master['db_name'];
        $this->masterConn = Zend_Db::factory($config['adapter'],$config['params']);
        $config['params']['dbname'] = $slave['db_name'];        
        $this->slaveConn = Zend_Db::factory($config['adapter'],$config['params']);
        $res = [];        
        switch($action['act_name']){
            case "update_language": $res = $this->updateSystemSettings(["f_code"=>0,"f_type_id"=>"7",'f_module_name'=>'mdl_lang_tg'] ,
                    ['f_title'],['fc_lang_it_IT','fc_lang_fr_FR','fc_lang_es_ES'],$limit[0], $limit[1]);                
                $res['status'] = 'progress';
                $total = empty($limit[2])?$this->getTotal("t_systems", "f_type_id = 7"):$limit[2];
                break;
            case "update_settings":
                $res = $this->updateSystemSettings(["f_code"=>0,"f_type_id"=>2,'f_module_name'=>'mdl_sys_tg'] ,
                    ['f_title','f_description','f_visibility'],['fc_sys_server_only','fc_sys_note'],$limit[0], $limit[1]);                
                $res['status'] = 'progress';
                $total = empty($limit[2])?$this->getTotal("t_systems", "f_type_id = 2"):$limit[2];
                break;
            case "update_ui":
                $res = $this->updateUI($limit[0], $limit[1],$slave['type']);
                $res['status'] = 'progress';
                $total = empty($limit[2])?$this->getTotal("t_ui_object_instances ", ""):$limit[2];
                break;
            case "update_bkm": 
                $res = $this->updateBkm($limit[0], $limit[1],$slave['type']);
                $res['status'] = 'progress';
                $total = empty($limit[2])?$this->getTotal("t_systems", "f_type_id = 3"):$limit[2];
                break;
            case "run_query": $res = $this->executeQuery($action['option']);
                $total = 50;
                $res['status'] = 'next';
                break;
            default : 
                break;
        }
        $res['error'] = [];
        if(isset($res['message'])) {
            $res['error'][] = [$limit[0],$limit[0]+$limit[1],$res['message']];
            unset($res['message']);
        }
        $res['params']['total'] = $total;
        return $res;
    }
    
    /**
     * Update bookmarks from master to slave
     * @param int $offset offset chunck
     * @param int $count chunck length
     * @param string $installation_type installation type (free,pro,enterprise)
     */
    private function updateBkm($offset,$count,$installation_type)            
    {
        $selMaster = new Zend_Db_Select($this->masterConn);
        $selSlave = new Zend_Db_Select($this->slaveConn);
        $sys = new Mainsim_Model_System($this->db);        
        $resBkmMaster = $selMaster->from(["t1"=>"t_creation_date"],"f_title")
            ->join(['t2'=>'t_systems'],"t1.f_id = t2.f_code",['f_code','fc_bkm_user_id','fc_bkm_module'])
            ->where("f_type_id = 3")->limit($count,$offset)->query()->fetchAll();
        $tot = count($resBkmMaster);  $res = [];
        for($i = 0;$i < $tot;++$i) {
            $selSlave->reset();
            $resBkmSlave = $selSlave->from(["t1"=>"t_creation_date"],[])
                ->join(['t2'=>'t_systems'],"t1.f_id = t2.f_code",['f_code'])
                ->where("f_type_id = 3")->where("f_title like ?",$resBkmMaster[$i]['f_title'])
                ->where("fc_bkm_user_id = ?",$resBkmMaster[$i]['fc_bkm_user_id'])
                ->where("fc_bkm_module = ?",$resBkmMaster[$i]['fc_bkm_module'])->query()->fetch();
            $newBkmParams = $this->createBkmParams($resBkmMaster[$i]['f_code']);
            if(empty($resBkmSlave)) {
                $newBkmParams['f_code'] = 0;
                $res = $sys->newSystem($newBkmParams);
            }elseif($installation_type != 'enterprise'){
                $res = $sys->editSystem($newBkmParams);
            }            
            unset($res['f_code']);
            if(isset($res['message'])) {return $res;}
        }
        return $res;
    }
    
    /**
     * create params array for new edit bookmarks
     * @param int $f_code bkm f_code from master
     * @return array $params array filled
     */
    private function createBkmParams($f_code)
    {
        $selMaster = new Zend_Db_Select($this->masterConn);
        $params = $selMaster->from(["t1"=>"t_creation_date"],["f_title","f_description","f_visibility","f_editability","f_phase_id"])
            ->join(['t2'=>'t_systems'],"t1.f_id = t2.f_code",['f_code','f_type_id','fc_bkm_user_id','fc_bkm_module','fc_bkm_data','fc_bkm_bookmark','fc_bkm_fields_bind'])            
            ->where("f_code = ?",$f_code)->query()->fetch();
        $params['f_module_name'] = 'mdl_bkm_tg';
        $params['unique'][] = "f_title,fc_bkm_user_id,fc_bkm_module";
        $params['t_systems_-1'] = [
            'f_code'=>[],"f_pair"=>[],
            "f_code_main"=> $params['f_code'],
            "f_type"=> "-1","Ttable"=> "t_systems",
            "pairCross"=> "2","f_module_name"=> "mdl_bkm_eng_tg"
        ];
        $selMaster->reset();
        $resPair = $selMaster->from("t_bookmarks",["f_code"=>"f_id","f_code_main"=> "f_code",
            new Zend_Db_Expr("'-1' as f_code_cross"),new Zend_Db_Expr("'Field' as f_title"),                
            new Zend_Db_Expr("'0' as f_group_code"),"f_timestamp",
            "fc_bkm_bind","fc_bkm_label","fc_bkm_type","fc_bkm_locked",
            "fc_bkm_visible","fc_bkm_width","fc_bkm_filter","fc_bkm_range",
            "fc_bkm_prange","fc_bkm_script","fc_bkm_level","fc_bkm_group"])
            ->where("f_code = ?",$f_code)->query()->fetchAll();
        $tot = count($resPair);
        for($i = 0;$i < $tot;++$i) {
            $params['t_systems_-1']['f_code'][] = $resPair[$i]['f_code'];
            $params['t_systems_-1']['f_pair'][] = $resPair[$i];
        }        
        return $params;
    }
    
    /**
     * return total amount from master db
     * @param string $table table name
     * @param string $condition where clauses
     */
    private function getTotal($table,$condition)
    {
        $where = !empty($condition)?" where $condition":"";
        $res = $this->masterConn->query("select count(*) as num from $table $where")->fetch();
        return $res['num'];
    }
    
    /**
     * Execute sql command to db
     * @param string $query
     * @return array empty if everything work fine, or a message
     */
    private function executeQuery($query)
    {
        $res = [];
        try{
            $this->slaveConn->beginTransaction();
            $this->slaveConn->query($query);                        
            $this->slaveConn->commit();
        } catch (Exception $ex) {
            $this->slaveConn->rollBack();
            $res['message'] = $ex->getMessage();            
        }
        return $res;
    }
    
    /**
     * update system settings (SETTING and LANGUAGE)
     */
    private function updateSystemSettings($params,$creationCols,$systemCols,$pos,$limit)
    {
        $f_type_id = $params['f_type_id'];
        $selMaster = new Zend_Db_Select($this->masterConn);
        $selSlave = new Zend_Db_Select($this->slaveConn);
        $resLangMaster = $selMaster->from(['t1'=>'t_creation_date'],$creationCols)
            ->join(["t2"=>'t_systems'],"t1.f_id = t2.f_code",$systemCols)
            ->where("f_type_id = ?",$params['f_type_id'])->where("f_phase_id = 1")->limit($limit,$pos)->query()->fetchAll();        
        //remove lang already installed        
        $resLangMaster = array_filter($resLangMaster, function($el) use($selSlave,$f_type_id){
            $selSlave->reset();
            $resExists = $selSlave->from(["t1"=>"t_creation_date"],['num'=>'count(t1.f_id)'])
                ->join(["t2"=>'t_systems'],"t1.f_id = t2.f_code")->where("f_type_id = ?",$f_type_id)
                ->where("f_phase_id = 1")->where("f_title = ?",$el['f_title'])->query()->fetch();
            return $resExists['num'] == 0; 
        });
        $sys = new Mainsim_Model_System($this->slaveConn);
        $userinfo = Mainsim_Model_Login::getUSerInfo(1, $this->slaveConn);
        foreach($resLangMaster as $lang) {
            $newSys = $params + $lang;            
            $res = $sys->newSystem($newSys,$userinfo);
            if(isset($res['message'])) { return $res; }
        }
        return [];
    }
    
    /**
     * get db list from database host
     * @param string $dbtype
     */
    public function getDbList($dbtype)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from(["t1"=>"t_creation_date"],"")
            ->join(['t2'=>'database_lists'],"t1.f_id = t2.f_code",['db_name'=>'f_dbname',
                'type'=>'f_installation_type',new Zend_Db_Expr("'0' AS disable")])
            ->where("t1.f_phase_id = 1")->where("f_installation_type = ?",$dbtype)
            ->query()->fetchAll();        
        return $res;
    }
    
    /**
     * Update ui from master to slave
     * @param int $offset start point
     * @param int $count chunck length
     * @param string $installation_type
     * @return array empty if everything is ok, otherwise a message
     */
    private function updateUI($offset,$count,$installation_type)
    {
        $res = [];
        try {
            $selMaster = new Zend_Db_Select($this->masterConn);
            $selSlave = new Zend_Db_Select($this->slaveConn);
            $resUiMaster = $selMaster->from("t_ui_object_instances",['f_instance_name','f_properties','f_type_id'])
                ->limit($count,$offset)->query()->fetchAll();
            $tot = count($resUiMaster);
            $this->slaveConn->beginTransaction();
            for($i = 0;$i < $tot; ++$i) {
                $new = $resUiMaster[$i];
                $new['f_timestamp'] = time();
                $selSlave->reset();
                $resUiSlave = $selSlave->from("t_ui_object_instances",['f_code'])
                    ->where("f_instance_name = ?",$resUiMaster[$i]['f_instance_name'])
                    ->query()->fetch();
                if(empty($resUiSlave)) {                 
                    $this->slaveConn->insert("t_ui_object_instances",$new);
                }
                elseif($installation_type != 'enterprise') {
                    $this->slaveConn->update("t_ui_object_instances",$new,"f_code = {$resUiSlave['f_code']}");
                }
            }
            $this->slaveConn->commit();
        }catch(Exception $e) {
            $this->slaveConn->rollBack();            
            $res['message'] = $e->getMessage();
        }
        return $res;
    }
}
