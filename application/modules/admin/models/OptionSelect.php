<?php

class MainsimAdmin_Model_OptionSelect
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));        
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function getOptions($category)
    {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_admin_ui_object_instances",array('f_properties'))->where('f_type_id = ?',12)->where('f_instance_name = ?',$category);
        return $select->query()->fetchAll();
    }
    
    public function getCategoryTypes($table)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from($table."_types", array('value'=>'f_id', 'text'=>'f_type'))->order("f_id ASC")->query()->fetchAll();
        if(count($res) == 1) $res[0]['selected'] = true;
        return $res;
    }
}