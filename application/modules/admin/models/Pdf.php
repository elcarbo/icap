<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class MainsimAdmin_Model_Pdf 
{
    private $db;
    
    public function __construct()
    {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function getPdfList()
    {
        $select = new Zend_Db_Select($this->db);
        $res_pdf = $select->from("t_scripts",array('f_id','f_name'))->where("f_type = 'php'")->where("f_script like '%{$pdf}%'")->query()->fetchAll();
        $list = array();
        $names = array();        
        foreach($res_pdf as $line) {
            $names[$line['f_id']] = $line['f_name'];            
        }
        $list['f_name'] = $names;        
        return $list;
    }
    
    public function getPdf($f_id = 0)
    {
        $select = new Zend_Db_Select($this->db);
        $res_pdf = $select->from("t_scripts",array('f_script'))->where("f_id = $f_id")->query()->fetchAll();
        
        $pdf = "";
        if(!empty($res_pdf)) {            
            $pdf = Mainsim_Model_Utilities::chg($res_pdf[0]['f_script']);
        }
        return $pdf;
    }
    
    public function save($params)
    {
		try {
			$this->db->beginTransaction();
            $where = $this->db->quoteInto("f_name = ? and f_type = 'php'", $params['f_name']);            
            $this->db->delete("t_scripts","f_id=".$params['f_id']);
			if($params['f_id']) {
				$this->db->delete("t_scripts","f_id=".$params['f_id']);
			}
			else {
				unset($params['f_id']);            
			}
			$params['f_type'] = 'php';			
			$this->db->insert("t_scripts",$params);
			$this->db->commit();
		}catch(Exception $e) {
			$this->db->rollback();
			return array("message"=>$e->getMessage());
		}
        return array();
    }
}
