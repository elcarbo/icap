<?php

class Admin_Mainsim_Model_Workorders
{
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db')); 
    }
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    
    public function editWorkOrder($params)
    {
        $select = new Zend_Db_Select($this->db);
        //Recupero i dati del vecchio WO
        $old_wo = $this->getWO($params['f_code']);       
        $userinfo = Zend_Auth::getInstance()->getIdentity();
        //Disattivo il vecchio Wo
        $this->disactiveWO($params['f_code']);
        //die;
        $new_wo = $old_wo;        
        $new_wo['f_title'] = $params['f_title'];
        $new_wo['f_description'] = $params['f_description']; 
        $new_wo['f_user_id'] = $userinfo->f_id;
        unset($new_wo['f_id']);        
        $this->db->insert("t_workorders", $new_wo);
        $custom_fields = $params;
        unset($custom_fields['f_title']);
        unset($custom_fields['f_description']);
        unset($custom_fields['f_table']);
        unset($custom_fields['phase']);
        $custom_fields['f_active'] = 1;
        $custom_fields['f_timestamp'] = time();
        $this->db->insert("t_custom_fields", $custom_fields);        
    }
    
    private function disactiveWO($f_code)
    {
        //Disattivo il WO
        $this->db->update("t_workorders", array('f_active'=>0), "f_code = $f_code");
        //Disattivo i Custom_fields
        $this->db->update("t_custom_fields", array('f_active'=>0), "f_code = $f_code");        
        //Disattivo i parent
        //$this->db->update("t_workorders_parent", array('f_active'=>0), "f_code = $f_code");        
        //Disattivo ware/wo
        //$this->db->update("t_selector_wo", array('f_active'=>0), "f_code = $f_code");        
        //Disattivo selector/wo
        //$this->db->update("t_ware_wo", array('f_active'=>0), "f_code = $f_code");        
    }
    
    private function getWO($f_code) 
    {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_workorders")->where("f_code = ?",$f_code)->where("f_active = 1");        
        $result = $select->query()->fetchAll();        
        return $result[0];
    }
    
    private function getCategoryCode($f_code) 
    {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_creation_date")->where("f_id = ?",$f_code);        
        $result = $select->query()->fetchAll();        
        if(count($result)) {
            return $result[0]['f_category'];
        }
        return array();
    }
    
    private function getLastOrder($table,$where = array())
    {
        $select = new Zend_Db_Select($this->db);
        $select->from($table,array('f_order'))->order("f_order DESC")->limit(1, 0);
        foreach($where as $line) {
            $select->where($line);
        }
        $result = $select->query()->fetchAll();
        if(count($result)) {
            return $result[0]['f_order'] + 1;
        }
        return 1;
    }
}