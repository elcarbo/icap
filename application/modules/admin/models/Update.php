<?php

class Mainsim_Model_Update
{
    private $db;
    
    public function __construct() 
    {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
        
    public function uiConvertProperties($table)
    {
        $result = array(); $fields = array('f_properties');
        if($table) $fields[] = 'f_instance_id'; else $fields[] = "f_code";
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from('t_'.$table.'ui_object_instances', $fields)->where('f_type_id IN (6, 7, 8, 13, 14)')->query()->fetchAll();
        foreach($uis as $ui) {
            $props = json_decode($ui['f_properties'], true);
            unset($props['bkm_type']); unset($props['db_type']); unset($props['t_workflows_1']);
            unset($props['t_selectors_1']); unset($props['t_users_1']);
            foreach($props as $k => $v) {
                if($k == 'mandatory') { $props['m_on'] = $v; unset($props['mandatory']); }
                else if($k == 'visible') { $props['v_on'] = $v; unset($props['visible']); }
                else if($k == 'readonly') { $props['r_on'] = $v; unset($props['readonly']); }
                else if($k == 'disabled') { $props['d_on'] = $v; unset($props['disabled']); }
                else if($k == 'level_edit') { $props['r_level'] = $v; unset($props['level_edit']); }
                else if($k == 'types') { $props['v_type'] = $v; unset($props['types']); }
                else if($k == 't_selectors') { $props['v_selector'] = $v; unset($props['t_selectors']); }
            }
            $c_ui = array( 'f_properties' => json_encode($props) );
            if($table) {
                $c_ui['f_instance_id'] = $ui['f_instance_id'];
                $this->db->update("t_admin_ui_object_instances", $c_ui, "f_instance_id = {$c_ui["f_instance_id"]}");                
            } else {
                $c_ui['f_code'] = $ui['f_code'];
                $this->db->update("t_ui_object_instances", $c_ui, "f_code = {$c_ui["f_code"]}");
            }
            $result[] = $c_ui;
        }
        return $result;
    }
    
    public function langArrayToSystemSettings()
    {
        $res = array();
        
        // Creating entry in t_systems_type
        $this->db->insert("t_systems_types", array("f_id" => 7, "f_type" => "LANGUAGE", "f_wf_id" => 9, "f_wf_phase" => 1, "f_short_name" => "lang", "f_module_name" => "mdl_lang_tg"));
        
        // Creating columns for languages in t_systems and t_systems_history
        $this->db->query("ALTER TABLE t_systems ADD fc_lang_it_IT VARCHAR(255) NULL, ADD fc_lang_fr_FR VARCHAR(255) NULL, ADD fc_lang_es_ES VARCHAR(255) NULL");
        $this->db->query("ALTER TABLE t_systems_history ADD fc_lang_it_IT VARCHAR(255) NULL, ADD fc_lang_fr_FR VARCHAR(255) NULL, ADD fc_lang_es_ES VARCHAR(255) NULL");
        
        include APPLICATION_PATH.'/configs/lang/it_IT.php'; // Italian
        include APPLICATION_PATH.'/configs/lang/fr_FR.php'; // French
        include APPLICATION_PATH.'/configs/lang/es_ES.php'; // Spanish
        
        foreach($it_IT as $k => $v) {
            $r = array();
            // t_creation_date
            try { 
                $this->db->insert('t_creation_date', array("f_type" => "SYSTEM", "f_category" => "LANGUAGE", "f_order" => 1, "f_creation_date" => mktime(), "f_creation_user" => 1, "f_title" => $k, "f_wf_id" => 9, "f_phase_id" => 1, "f_visibility" => -1, "f_editability" => -1, "f_timestamp" => mktime()));
            } catch(Exception $ex) { $r["error"][] = "Error in creating entry in t_creation_date: ".$ex->getMessage(); break; }            
            $f_code = $this->db->lastInsertId();
            
            // t_systems
            try {
                $this->db->insert('t_systems', array("f_code" => $f_code, "f_type_id" => 7, "f_timestamp" => mktime(), "f_user_id" => 1, "fc_lang_it_IT" => utf8_decode($v), "fc_lang_fr_FR" => (isset($fr_FR[$k])?utf8_decode($fr_FR[$k]):""), "fc_lang_es_ES" => (isset($es_ES[$k])?utf8_decode($es_ES[$k]):"")));
            } catch(Exception $ex) { $r["error"][] = "Error in creating entry in t_systems: ".$ex->getMessage(); break; }            
            
            // t_systems_parent
            try {
                $this->db->insert('t_systems_parent', array("f_code" => $f_code, "f_parent_code" => 0, "f_active" => 1, "f_timestamp" => mktime()));
            } catch(Exception $ex) { $r["error"][] = "Error in creating entry in t_systems_parent: ".$ex->getMessage(); break; }            
            
            // t_custom_fields
            try {
                $this->db->insert('t_custom_fields', array("f_code" => $f_code, "f_timestamp" => mktime()));
            } catch(Exception $ex) { $r["error"][] = "Error in creating entry in t_custom_fields: ".$ex->getMessage(); break; }            
            
            if(!isset($r["error"])) $r["message"] = "Entry for key $k created successfully";
            $res[$k] = $r;
        }
        return $res;
    }
    
    public function convertEditModuleToCb($isadmin = false) {
        $table = 't_'.($isadmin?'admin_':'').'ui_object_instances';
        $edits = array(); $cbs = array();
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from($table)->where("f_instance_name LIKE 'mdl%edit_cb_bl'")->orWhere("f_properties LIKE '%moModuleEdit%'")->query()->fetchAll();
        foreach($uis as $ui) {
            if(strpos($ui["f_instance_name"], '_edit_cb_bl') > 0) $cbs[$ui["f_instance_name"]] = json_decode($ui["f_properties"], true);
            else $edits[$ui["f_instance_name"]] = array(
                "f_code" => $ui["f_code"],
                "f_properties" => json_decode($ui["f_properties"], true)
            );
        }
        foreach($edits as $k => $v) {
            unset($v["f_properties"]["f_instance_name"]); unset($v["f_properties"]["tabbar"]); unset($v["f_properties"]["commandsl"]); unset($v["f_properties"]["phaseFieldId"]);
            unset($v["f_properties"]["table"]); unset($v["f_properties"]["tgType"]); unset($v["f_properties"]["order"]); unset($v["f_properties"]["pairCross"]);
            if(!isset($v["f_properties"]["commands"])) {
                $v["f_properties"]["commands"] = str_replace('_edit_cb', '', $cbs["{$k}_cb_bl"]["elements"]);
                try {
                    $this->db->update($table, array("f_properties" => json_encode($v["f_properties"])), "f_code = {$v["f_code"]}");
                    $msg[] = array("message" => "$k updated successfully!");
                } catch(Exception $ex) {
                    $msg[] = array("error" => "Error in converting $k: ".$ex->getMessage());
                    return $msg;
                }
            }
        }
        try { // Deleting old layouts and modules
            $this->db->query("delete from $table where (f_type_id = 10 AND f_instance_name LIKE '%edit_cb') OR (f_type_id = 9 AND f_instance_name LIKE '%edit_cb_bl')");
            $msg[] = array("message" => "Old layouts and modules deleted successfully");
        } catch(Exception $ex) {
            $msg[] = array("error" => "Error in deleting old layouts and modules: ".$ex->getMessage());
            return $msg;
        }
        try { // Converting buttons
            $this->db->query("update $table set f_instance_name = REPLACE(f_instance_name, '_edit_cb', '') where f_type_id = 5 AND f_instance_name LIKE '%edit_cb%'");
            $msg[] = array("message" => "Buttons converted successfully");
        } catch(Exception $ex) {
            $msg[] = array("error" => "Error in converting buttons: ".$ex->getMessage());
            return $msg;
        }
        return $msg;
    }
    
    public function convertUiTruthValuesToBoolean() {
        $msg = array();
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from("t_ui_object_instances")->where("f_type_id IN (6, 7, 8, 9, 13, 14)")->query()->fetchAll();
        foreach($uis as $ui) {
            $ui["f_properties"] = json_decode($ui["f_properties"], true);
            foreach($ui["f_properties"] as $k => $v) {
                if($k != "ftype" && $k != "stype" && $k != "v_type" && $k != "m_type" && $k != "r_type" && $k != "d_type" &&
                $k != "v_wf" && $k != "r_wf" && $k != "m_wf" && $k != "d_wf" &&
                $k != "v_selector" && $k != "r_selector" && $k != "m_selector" && $k != "d_selector" &&
                $k != "v_level" && $k != "r_level" && $k != "m_level" && $k != "d_level") {
                    if($v === true || $v === "true" || $v === 1 || $v === "1") $ui["f_properties"][$k] = 1;
                    elseif($v === false || $v === "false" || $v === 0 || $v === "0") $ui["f_properties"][$k] = 0;
                }                
            }
            $ui["f_properties"] = json_encode($ui["f_properties"]);
            try {
                $this->db->update("t_ui_object_instances", $ui, "f_code = ".$ui["f_code"]);
                $msg[] = array("message" => "{$ui["f_instance_name"]} converted successfully");
            } catch(Exception $ex) {
                $msg[] = array("error" => "Error in converting values for {$ui["f_instance_name"]}: ".$ex->getMessage());
                return $msg;
            }            
        }
        return $msg;
    }
    
    public function cleanUi() {
        $msg = "";
        $propsToRemove = array("f_instance_name", "f_type_id", "bkm_type", "db_type", "db_table", "db_generate", "checkmode", "error", "parentSelection");
        $propsTgToRemove = array("table", "tgType", "pairCross", "order");
        $q = new Zend_Db_Select($this->db);
        $uis = $q->from("t_ui_object_instances", array("f_code", "f_instance_name", "f_properties"))->where("f_type_id IN (5, 6, 7, 8, 9, 13, 14)")->query()->fetchAll();
        foreach($uis as $ui) {
            $ui["f_properties"] = json_decode($ui["f_properties"], true);
            foreach($ui["f_properties"] as $k => $v) {
                if(array_search($k, $propsToRemove) !== FALSE) unset($ui["f_properties"][$k]);
                if($ui["f_properties"]["type"] != "moModuleTreeGrid" && array_search($k, $propsTgToRemove) !== FALSE) unset($ui["f_properties"][$k]);
            }
            $ui["f_properties"] = json_encode($ui["f_properties"]);
            try {
                $this->db->update("t_ui_object_instances", $ui, "f_code = ".$ui["f_code"]);
                $msg[] = array("message" => $ui["f_instance_name"]." cleaned successfully");
            } catch(Exception $ex) {
                $msg[] = array("error" => "Error in cleaning {$ui["f_instance_name"]}: ".$ex->getMessage());
                return $msg;
            }            
        }
        return $msg;
    }
    
    public function getUiComponentList() {
        $uis = array();
        include APPLICATION_PATH.'/configs/lang/it_IT.php'; // Italian
        $fields = array();
        $q = new Zend_Db_Select($this->db);
        $comps = $q->from("t_ui_object_instances", array("f_instance_name", "f_properties"))->where("f_instance_name LIKE 'mdl_wo_edit_c_'")->query()->fetchAll();		
        foreach($comps as $ck => $cv) {
            $props = json_decode(Mainsim_Model_Utilities::chg($cv["f_properties"]), true);
            //$fields = array_merge($fields, explode(",", $props["fields"]));
			$fields = explode(",", str_replace('|', ',', $props["fields"]));
            $q->reset();
            $c_uis = $q->from("t_ui_object_instances", array("f_instance_name", "f_properties"))
                ->where("f_instance_name IN ('".implode("','", $fields)."')")->query()->fetchAll();
            foreach($c_uis as $k => $v) {
                $props = json_decode(Mainsim_Model_Utilities::chg($v["f_properties"]), true);
                $c_uis[$k]["bind"] = $props["bind"];
                $c_uis[$k]["label"] = $props["label"];
                $c_uis[$k]["label_it"] = utf8_decode($it_IT[$props["label"]]);
                $c_uis[$k]["info"] = $props["info"];
                $c_uis[$k]["info_it"] = utf8_decode($it_IT[$props["info"]]);
                unset($c_uis[$k]["f_properties"]);
            }
            $uis[$cv["f_instance_name"]] = $c_uis;
        }        
        return $uis;
    }
    
    public function setUiProperties($instance_name, $addprops) {
        $q = new Zend_Db_Select($this->db);
        $res = $q->from("t_ui_object_instances", array("f_properties"))->where("f_instance_name LIKE '$instance_name'")->query()->fetch();
        $props = json_decode(Mainsim_Model_Utilities::chg($res["f_properties"]), true);
        foreach($addprops as $p => $v) { $props[$p] = $v; }
        try {
            $this->db->update("t_ui_object_instances", array("f_properties" => json_encode($props)), "f_instance_name = '$instance_name'");
            return array("error" => false, "message" => "$instance_name updated successfully!");
        } catch(Exception $ex) {
            return array("error" => true, "message" => "Problems updating $instance_name: ".$ex->getMessage());
        }
    }
}