<?php

class Admin_Mainsim_Model_Selectors
{
    /**
     *
     * @var Zend_Db 
     */
    private $db;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function getSelectorsFromType($type)
    {
        $select  = new Zend_Db_Select($this->db);        
        $response = array();
        $user = Zend_Auth::getInstance()->getIdentity();        
        //Cerco gli elementi di root
        $select->from("t_selectors_parent")->joinRight('t_selectors','t_selectors.f_code = t_selectors_parent.f_code');
        $select->where("f_parent_code = ?",0);        
        $select->where("f_type_id = ?",$type);
        $select->where("(f_visibility & ?) != 0",$user->f_level);
        $select->where("NOT EXISTS(SELECT * FROM t_user_restrictions where f_code = t_selectors.f_code AND f_user_id = {$user->f_id})");
        //echo $select;die;
        $result_parent = $select->query()->fetchAll();
        if(count($result_parent)) {
            $f_code_figli = array();
            foreach($result_parent as $line) {
                $f_code_figli[] = $line['f_code'];
            }
            $response[0] = array(
                'pid'=>array(),
                'label'=>'',
                'open'=>true,
                "selected"=> false,
                "order"=> 0,
                "sub" => $f_code_figli                
            );
            //Carico gli altri figli con i rispettivi padri e figli
            foreach($result_parent as $line) {
                //Father
                $select->reset();
                $select->from("t_selectors_parent")->joinLeft('t_selectors','t_selectors.f_code = t_selectors_parent.f_code');
                $select->where("t_selectors_parent.f_code = ?",$line['f_code']);
                $select->where("(f_visibility & ?) != 0",$user->f_level);
                $select->where("NOT EXISTS(SELECT * FROM t_user_restrictions where f_code = t_selectors_parent.f_code AND f_user_id = {$user->f_id})");                
                $result_parent_pid = $select->query()->fetchAll();
                $f_code_pid = array();
                foreach($result_parent_pid as $line_pid) {
                    $f_code_pid[] = $line_pid['f_parent_code'];
                }
                $response[$line['f_code']]['pid'] = $f_code_pid;
                //Label
                $response[$line['f_code']]['label'] = Mainsim_Model_Utilities::chg($line['f_title']);
                //open e selected
                $response[$line['f_code']]['open'] = false;
                $response[$line['f_code']]['selected'] = false;
                //f_order
                $response[$line['f_code']]['order'] = $line['f_order'];
                //recupero tutti i figli di questo elemento
                $select->reset();
                $select->from("t_selectors_parent",array('num'=>'COUNT(*)'))->joinLeft('t_selectors','t_selectors.f_code = t_selectors_parent.f_code',array());
                $select->where("t_selectors_parent.f_parent_code = ?",$line['f_code']);
                $select->where("(f_visibility & ?) != 0",$user->f_level);
                $select->where("NOT EXISTS(SELECT * FROM t_user_restrictions where f_code = t_selectors_parent.f_parent_code AND f_user_id = {$user->f_id})");                                
                $result_parent_child = $select->query()->fetchAll();
                
                if(isset($result_parent_child[0]['num']) && $result_parent_child[0]['num'] > 0) {                    
                    $response[$line['f_code']]['leaf'] = false;
                }
                else {                    
                    $response[$line['f_code']]['leaf'] = true;
                }                
                $response[$line['f_code']]['sub'] = array();
            }
        }        
        return $response;
    }
    
    
    public function getSelectorsFromId($id)
    {
        $select  = new Zend_Db_Select($this->db);        
        $response = array();
        $user = Zend_Auth::getInstance()->getIdentity();        
        //Cerco gli elementi di root
        $select->from("t_selectors_parent")->joinRight('t_selectors','t_selectors.f_code = t_selectors_parent.f_code');
        $select->where("f_parent_code = ?",$id);                
        $select->where("(f_visibility & ?) != 0",$user->f_level);
        $select->where("NOT EXISTS(SELECT * FROM t_user_restrictions where f_code = t_selectors.f_code AND f_user_id = {$user->f_id})");                
        $result_parent = $select->query()->fetchAll();
        if(count($result_parent)) {
            //Carico i figli con i rispettivi padri e figli
            foreach($result_parent as $line) {
                //Father
                $select->reset();
                $select->from("t_selectors_parent")->joinLeft('t_selectors','t_selectors.f_code = t_selectors_parent.f_code');
                $select->where("t_selectors_parent.f_code = ?",$line['f_code']);
                $select->where("f_visibility & ?",$user->f_level);
                $select->where("NOT EXISTS(SELECT * FROM t_user_restrictions where f_code = t_selectors_parent.f_code & f_user_id = {$user->f_id})");                                
                $result_parent_pid = $select->query()->fetchAll();
                $f_code_pid = array();
                foreach($result_parent_pid as $line_pid) {
                    $f_code_pid[] = $line_pid['f_parent_code'];
                }
                $response[$id][$line['f_code']]['pid'] = $f_code_pid;
                //Label
                $response[$id][$line['f_code']]['label'] = Mainsim_Model_Utilities::chg($line['f_title']);
                //open e selected
                $response[$id][$line['f_code']]['open'] = false;
                $response[$id][$line['f_code']]['selected'] = false;
                //f_order
                $response[$id][$line['f_code']]['order'] = $line['f_order'];
                //recupero tutti i figli di questo elemento
                $select->reset();
                
                $select->from("t_selectors_parent",array('num'=>'COUNT(*)'))->joinLeft('t_selectors','t_selectors.f_code = t_selectors_parent.f_code',array());
                $select->where("t_selectors_parent.f_parent_code = ?",$line['f_code']);
                $select->where("f_visibility & ?",$user->f_level);
                $select->where("NOT EXISTS(SELECT * FROM t_user_restrictions where f_code = t_selectors_parent.f_parent_code & f_user_id = {$user->f_id})");                
                $result_parent_child = $select->query()->fetchAll();
                
                if(isset($result_parent_child[0]['num']) && $result_parent_child[0]['num'] > 0) {                    
                    $response[$id][$line['f_code']]['leaf'] = false;
                }
                else {
                    $response[$id][$line['f_code']]['leaf'] = true;
                }   
                $response[$id][$line['f_code']]['sub'] = array();
            }
        }        
        return $response;
    }
}