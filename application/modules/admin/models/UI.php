<?php

class MainsimAdmin_Model_UI
{
    private $type_id;
    private $db,$shortname,$uiShort;
    
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
        
        $res_shortname = $this->db->query("SELECT f_short_name from t_selectors_types")->fetchAll();
        $tot_a = count($res_shortname);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_shortname[$a]['f_short_name']) && !is_null($res_shortname[$a]['f_short_name']))
                $this->shortname[] = $res_shortname[$a]['f_short_name'];
        }

        $res_shortname = $this->db->query("SELECT f_short_name from t_wares_types")->fetchAll();
        $tot_a = count($res_shortname);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_shortname[$a]['f_short_name']) && !is_null($res_shortname[$a]['f_short_name']))
                $this->shortname[] = $res_shortname[$a]['f_short_name'];
        }

        $res_shortname = $this->db->query("SELECT f_short_name from t_workorders_types")->fetchAll();
        $tot_a = count($res_shortname);
        for($a = 0;$a < $tot_a; ++$a) {
            if(!empty($res_shortname[$a]['f_short_name']) && !is_null($res_shortname[$a]['f_short_name']))
                $this->shortname[] = $res_shortname[$a]['f_short_name'];
        }
        $this->uiShort = array('mn','btn','txtfld','slct','chkbx','mdl','lyt','tbbr','img','pck');
    }
    
    public function cloneAll($search, $replace)
    {
        $select = new Zend_Db_Select($this->db);
        if(empty($search) ||  empty ($replace)) return;
            
        $res = $select->from("t_ui_object_instances",array("f_instance_name",'f_type_id','f_properties'/*,'f_code'*/))
                ->where("f_instance_name like '%_{$search}%'")
                ->query()->fetchAll();        
        $time = time();        
        $tot_short = count($this->uiShort);
        foreach($res as $line) {
            $new_rec = $line;
            $new_rec['f_instance_name'] = str_replace("_{$search}","_{$replace}",$line['f_instance_name']);
            $new_rec['f_properties'] = $line['f_properties'];      
            $new_rec['f_timestamp'] = time();      
            //$f_code = $new_rec['f_code'];
            //unset($new_rec['f_code']);
            for($i = 0;$i < $tot_short;++$i) {
                $new_rec['f_properties'] = str_replace($this->uiShort[$i]."_{$search}",$this->uiShort[$i]."_{$replace}",$new_rec['f_properties']);
            }
            try {
                //$this->db->update("t_ui_object_instances",$new_rec,"f_code = {$f_code}");
                $this->db->insert("t_ui_object_instances",$new_rec);
                $code = $this->db->lastInsertId();                
            }catch(Exception $e) {}
        }
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function editUi($data)
    {           
        $select = new Zend_Db_Select($this->db);
        $res_chek = $select->from("t_ui_object_instances")->where("f_code = ?",$data['f_code'])
                ->query()->fetchAll();        
        $prop = json_decode($data['f_properties'],true);
        
        if(!empty($res_chek)) {                        
            $where = $this->db->quoteInto("f_code = ?", $data['f_code']);
            foreach($data as &$line) {
                $line = Mainsim_Model_Utilities::clearChars($line);                
            }
            unset($data['f_code']);
            $data['f_timestamp'] = time();
            $this->db->update("t_ui_object_instances", $data, $where);
        }
        else 
            return false;
        
        //check if create custom field inside db        
        $right_type = array(6,7,8,13,14);        
        if(isset($prop['db_table']) && !empty($prop['db_table']) && in_array($data['f_type_id'],$right_type)) {            
            $this->createBindOnDb($prop["db_table"], $prop["db_type"], $prop["bind"], $prop['db_column_length']);
        }        
        return true;
    }   
    
    public function newUi($data,$import = false)
    {        
        $select = new Zend_Db_Select($this->db);         
        $res_chek = $select->from("t_ui_object_instances")->where("f_instance_name = ?",$data['f_instance_name'])
                ->query()->fetchAll();        
        $prop = json_decode(Mainsim_Model_Utilities::chg($data['f_properties']),true);                
        if(empty($res_chek)) {            
            foreach($data as &$line) {
                if(is_string($line)) {
                    $old = $line;
                    $line = utf8_decode($line);
                    if(strpos($line,'?')!== false) {
                        $line = iconv('UTF-8', 'CP1252', $old);
                    }
                }
            }
            $data['f_timestamp'] = time();
            unset($data['f_code']);
            $this->db->insert("t_ui_object_instances", $data);               
            $f_code = $this->db->lastInsertId();
        }
        else {            
            throw new Exception("You cannot save this UI with the same name of another one");
        }        
        
        
        $right_type = array(6,7,8,13,14);
        
        if(!$import && isset($prop['db_table']) && !empty($prop['db_table']) && in_array($data['f_type_id'],$right_type)) {            
            $this->createBindOnDb($prop["db_table"], $prop["db_type"], $prop["bind"], $prop['db_column_length']);
        }
        if($import) {            
            //insert ui in right module component and pop-up                    
            if(in_array($data['f_type_id'],$right_type)) {            
                $this->editUiModule($data['f_instance_name']);                
            }
        }
        return $f_code;
    }
    
    /**
     * Create bind into database
     * @param string $table
     * @param string $type
     * @param string $field_name
     * @param string $column_length
     * @return array with error message or empty array 
     */
    public function createBindOnDb($table, $type, $field_name,$column_length = '') 
    {
        $resp = array();
        $cols = Mainsim_Model_Utilities::get_tab_cols($table);
        //check if create field
        if(!in_array($field_name,$cols) && !empty($table) && !empty($type) ){
            try {                
                $this->createCustomCol($table, $field_name, $type,$column_length);
            } catch (Exception $ex) {
                $resp['message'] =  "Cannot create db column : $field_name in $table: ".$ex->getMessage();                
            }
        }
        return $resp;
    }
    
    /**
     * create new column in t_custom_fields and t_custom_fields history
     * @param string $col_name : name of column
     * @param array $col_data : array with information about column
     */
    private function createCustomCol($table, $name, $type,$column_length) 
    {
        //get adapter to create right command
        $config = Zend_Registry::get('db')->toArray();        
        $pdo = $config['adapter'];
        $isMysql = stripos($pdo,"mysql")!== false?true:false;
        $null = $isMysql?'DEFAULT NULL':'NULL';        
        switch($type) {
            case 'VARCHAR':
                //varchar check
                if(empty($column_length) && $type == 'VARCHAR') {
                    throw new Exception("The length of field $name must be setted");
                }
                break;
            case 'TEXT' : 
                $type = stripos($pdo,"mysql")!== false?"TEXT":"VARCHAR(MAX)";
                break;
            case 'BOOLEAN' :                
            case 'INT' :                
            case 'DATE' :
                $type = "INT";
                break;
        }
        $column_length = !empty($column_length)?"($column_length)":'';        
        //create column in t_custom_fields and t_custom_fields history        
        $this->db->query("ALTER TABLE $table ADD $name {$type}{$column_length} $null");
        $this->db->query("ALTER TABLE {$table}_history ADD $name {$type}{$column_length} $null");
    }
    
    /** insert o remove ui component from modules */
    
    private function editUiModule($ui_name,$old_ui_name = '',$action = 'add') 
    {
        //get name of moduleComponent using ui name
        $exp_name = explode('_',$ui_name);
        $exp_name[0] = 'mdl';
        $pos = array_search("edit", $exp_name);
        $exp_name = array_slice($exp_name, 0, $pos +1 );
        $exp_name[] = 'c1';
        $mdl = implode('_',$exp_name);        
        $select = new Zend_Db_Select($this->db);
        $res_mdl = $select->from("t_ui_object_instances",array("f_properties"))
                ->where("f_type_id = 9")->where("f_instance_name like ?",$mdl)
                ->where("f_properties like '%moModuleComponent%'")->query()->fetch();
        if(!empty($res_mdl)) {
            $prop = json_decode($res_mdl['f_properties'],true);
            $fields = explode(',',$prop['fields']);            
            if($action == 'add') {
                if(!in_array($ui_name,$fields)) {
                    $fields[] = $ui_name;
                }
            }
            elseif($action == 'edit') {
                $pos = array_search($old_ui_name,$fields);                
                if($pos === false) {
                    $fields[] = $ui_name;
                }
                else {
                    $fields[$pos] = $ui_name;
                }
            }
            else {
                $pos = array_search($ui_name,$fields);                
                if($pos !== false) unset($fields[$pos]);
            }
            $prop['fields'] = implode(',',$fields);            
            $json_prop = json_encode($prop);
            $this->db->update("t_ui_object_instances",array("f_properties"=>$json_prop,'f_timestamp'=>time()),"f_instance_name = '{$mdl}' ");            
        }
        return $mdl;
    }
    
    public function deleteUi($data)
    {
        try {
            $this->db->delete("t_ui_object_instances", "f_code IN ({$data['f_code']})");
            return array();
        } catch(Exception $ex) {
            return array("message" => "Problems in deleting ui: ".$ex->getMessage());
        }
    }
    
    public function cloneUi($data)
    {
    	$select = new Zend_db_select($this->db);
        $codes = explode(',',$data['f_code']);
        $tot = count($codes);
        for($a = 0; $a < $tot; ++$a) {
            $select->reset();
            $result = $select->from('t_ui_object_instances')->where("f_code = ?",$codes[$a])->query()->fetchAll();
            $tot_b = count($result);
            for($i=0; $i<$tot_b; $i++) {
                $ui_object = $result[$i];
                $ui_object['f_instance_name'] = $ui_object['f_instance_name'].'_'.time();
                unset($ui_object['f_code']);
                // Modifico il properties in modo da non interferire con gli altri
                $pattern_mdl = "/mdl\d+/";
                $pattern_lyt = "/lyt\d+/";
                $ui_object['f_properties'] = preg_replace($pattern_mdl, "mdl0", $ui_object['f_properties']);
                $ui_object['f_properties'] = preg_replace($pattern_lyt, "mdl0", $ui_object['f_properties']);
                $ui_object['f_timestamp'] = time();
                $this->db->insert('t_ui_object_instances',$ui_object); 
                $f_code = $this->db->lastInsertId();                                
            }            
        }        
    }
    
    public function getUI($custom_search = array())
    {   
        $select = new Zend_Db_Select($this->db);
        $select->from('t_admin_ui_object_instances')->where('f_type_id = ?', $this->type_id);                        
        foreach($custom_search as $line) {
            $select->where($line);
        }        
        
        $result = $select->query()->fetchAll();         
        $result = $this->createPopup($result);
        return $result;        
    }
    
    public function setSearch($search) {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_admin_ui_object_types", 'f_type_id');        
        $select->where("f_type_name = ?", $search);
        $result = $select->query()->fetchAll();
        if(count($result)) {
            $this->type_id = $result[0]['f_type_id'];
            return true;
        }
        return false;        
    }
    
    public function uiToArray($ui)
    {
        $result['name'] = $ui['f_instance_name'];                
        $properties = json_decode(utf8_encode($ui['f_properties']),true);      
        if(!is_array($properties) && (is_null($properties) || !$properties)) {
            Zend_Debug::dump($ui['f_properties']);die($ui['f_instance_name']);
        }
        $keys = array_keys($properties);
        $tot_a = count($keys);
        for($a = 0;$a < $tot_a;++$a) {
            $result[$keys[$a]] = $properties[$keys[$a]];
        }
        if($this->type_id == 11 && strpos($result['name'],"_pp") !== false) {             
            $tabs = array();
            $old_prop = json_decode(utf8_encode($ui['f_properties']),true);            
            foreach($old_prop['tabs'] as $line_pr){                            
                if(strpos($line_pr['layout'],'_edit_') !== false) {                                
                    $tabs[] = $line_pr;
                }
            }
            $result['tabs'] = $tabs;
        }
        return $result;   
    }
    
    public function createPopup($response = array())
    {        
        $tot_a = count($response);                
        $final_result = array();
        for($a = 0;$a < $tot_a;++$a) {
            $final_result[] = $this->uiToArray($response[$a]);            
        }                
        return $final_result;
    }
    
    
    
    public function getUserLang($lang_id)
    {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_localization",array("f_code"))->where("f_id = $lang_id");
        $res = $select->query()->fetchAll();
        return $res[0]['f_code'];
    }
    
    public function getLocalization($lang_id)
    {
        $select = new Zend_Db_Select($this->db);
        $res = $select->from("t_localization")->where("f_id = $lang_id")->query()->fetchAll();
        return $res[0];
    }
    
    public function getInfoSystem()
    {
        $select = new Zend_Db_Select($this->db);
        $select->from("t_system_information");
        $res = $select->query()->fetchAll();
        return $res[0];
    }
    
    public function getSingleUI($name = '')
    {
        if(empty($name)) return array();
        $select = new Zend_Db_Select($this->db);
        return $select->from("t_admin_ui_object_instances")->where("f_instance_name = ?",$name)->query()->fetch();         
    }
    
    public function getMenus() {
        $menus = array();
        if($this->setSearch('menu')) $menus = $this->getUI();
        return $menus;
    }
    public function getButtons() {
        $buttons = array();
        $type_id_search = $this->setSearch('button');
        if($type_id_search) {
            $buttons = $this->getUI();              
            foreach($this->shortname as $main ) {                      
                $buttons[] = array(
                    'name'=>"btn_{$main}_pp_select",
                    'f_instance_name'=>"btn_{$main}_pp_select",
                    'label'=>"Select",
                    'iconon'=>"select.png"
                );
                $buttons[] = array(
                    'name'=>"btn_{$main}_pp_cancel",
                    'f_instance_name'=>"btn_{$main}_pp_cancel",
                    'label'=>"Cancel",
                    'iconon'=>"cancel.png"
                );
            }
        }
        return $buttons;
    }
    public function getTextfields() {
        $textfields = array();
        if($this->setSearch('textfield')) $textfields = $this->getUI();
        return $textfields;
    }
    public function getSelects() {
        $selects = array();
        if($this->setSearch('select')) $selects = $this->getUI();
        return $selects;
    }
    public function getCheckboxes() {
        $checkboxes = array();
        if($this->setSearch('checkbox')) $checkboxes = $this->getUI();
        return $checkboxes;
    }
    public function getModules() {        
        $modules = array();
        if($this->setSearch('module')) {            
            $result_modules = $this->getUI();                                     
            $tot_a = count($this->shortname);
            for($a=0; $a<$tot_a; ++$a) {
                $main = $this->shortname[$a];
                $result_modules[] = array(
                    'name'=>"mdl_{$main}_pp_cancel",
                    'f_instance_name'=>"mdl_{$main}_pp_cancel",
                    'type'=>"moModuleCommandBar",
                    'noshadow'=>"true",
                    'elements'=>"btn_{$main}_pp_cancel|btn_{$main}_pp_select"
                );
            }
            //----------
            $modules = $result_modules;        
//            foreach($modules as &$line) {                
//                if(strpos($line['src'], "BASEURL") !== false) {                
//                    $line['src'] = str_replace("BASEURL", $baseurl, $line['src']);                
//                }            
//            }
        }
        return $modules;
    }
    public function getLayouts() {
        $layouts = array();
        if($this->setSearch('layout')) {
            $layouts = $this->getUI();                        
            $tot_x = count($layouts);
            //foreach($layouts as &$lyt) {                
            for($x = 0;$x < $tot_x;++$x) {
                $lyt = $layouts[$x];
                if(strpos($lyt['name'],"_pp") + 3 == strlen($lyt['name'])) {
                    $name = str_replace('lyt_','',$lyt['name']);
                    $name = str_replace('_pp','',$name);
                    // Base popup layout
                    $new_layout = array(                        
                        array( "idp"=> -1, "ov"=> 1, "tp"=> 0, "wh"=> 100, "level"=> -1, "module"=> "", "lock"=> 0, "minv"=> 36, "acc"=> 0 ),
                        array( "idp"=> 0, "ov"=> 0, "tp"=> 0, "wh"=> 538, "level"=> -1, "module"=> "", "acc"=> 0, "lock"=> 0, "minv"=> 36 ),
                        array( "idp"=> 0, "ov"=> 0, "tp"=> 0, "wh"=> 36, "level"=> -1, "module"=> "mdl_{$name}_pp_cancel", "acc"=> 0, "lock"=> 1, "minv"=> 36 ),
                        array( "idp"=> 1, "ov"=> 1, "tp"=> 0, "wh"=> 258, "level"=> -1, "module"=> "", "acc"=> 0, "lock"=> 0, "minv"=> 36 )
                    );
                            
                    $tot_a = count($lyt['layout']);
                    $vertical_cut = false;
                    for($a = 0;$a < $tot_a;$a++) {
                        $line_a = $lyt['layout'][$a];
                        if(strpos($line_a['module'],'_sel') !== false || strpos($line_a['module'],'summary') !== false){ 
                            $vertical_cut = true;
                            break;
                        }                        
                    }
                    if(!$vertical_cut) { 
                        $new_layout = array(
                            array(
                                "idp"=> -1,
                                "ov"=> 1,
                                "tp"=> 0,
                                "wh"=> 100,
                                "level"=> -1,
                                "module"=> "",
                                "lock"=> 0,
                                "minv"=> 36,
                                "acc"=> 0,
                            )
                        );                        
                    }                    
                    
                    for($a = 0;$a < $tot_a;$a++) {
                        $line_a = $lyt['layout'][$a];
                        if(($line_a['idp'] <=0 && strpos($line_a['module'],'_sel') === false) && strpos($line_a['module'],'crud') === false) continue;
                        if($vertical_cut) {
                            $line_a['idp'] = strpos($line_a['module'],'crud') !== false?1:3;                        
                        }
                        elseif(strpos($line_a['module'],'crud') !== false) {
                            $line_a['idp'] = 0;                            
                        }
                        $new_layout[] = $line_a;                        
                    }
                    if(!$vertical_cut) {
                        $new_layout[] =  array(
                            "idp"=> 0,
                            "ov"=> 0,
                            "tp"=> 0,
                            "wh"=> 36,
                            "level"=> -1,
                            "module"=> "mdl_{$name}_pp_cancel",
                            "acc"=> 0,
                            "lock"=> 1,
                            "minv"=> 36
                        );
                    }                   
                    //$lyt['layout'] = $new_layout;                    
                    $layouts[$x]['layout'] = $new_layout;                    
                }
            }            
        }
        return $layouts;
    }
    public function getTabbars() {
        $tabbars = array();
        if($this->setSearch('tabbar')) $tabbars = $this->getUI();
        return $tabbars;
    }
    public function getOptions() {                
        $options = array();
        if($this->setSearch('options')) {
            $t_options = $this->getUI();
            for($i=0; $i<count($t_options); $i++) {
                $name = $t_options[$i]["name"]; unset($t_options[$i]["name"]);
                $options[$name] = $t_options[$i];
            }
        }
        return $options;
    }
    public function getImages() {
        $images = array();
        if($this->setSearch('image')) $images = $this->getUI();
        return $images;
    }
    public function getPicklists() {
        $picklists = array();
        if($this->setSearch('picklist')) $picklists = $this->getUI();
        return $picklists;
    }    
    
    
    public function createTxt($data,$ui_name)
    { 
        $textarea = "false";
        if(strpos($data['f_ui_type'],"TEXTAREA") !== false) {
            $textarea = "true";
        }        
        $txtfld = array(
            'label'=>$data['f_label'],
            'info'=>$data['f_info'],            
            'bind'=>$data['f_title'],                      
            'text'=>$textarea,
            'autocomplete'=>"false"
        );
        $txtfld = $this->setAttributesUi($txtfld, $data);
        if(strpos($data['f_type'],"DATE") !== false) {
            $txtfld['dtmpicker'] = 'datetime';
        }
        
        if($data['f_fieldset'] != "" && !is_null($data['f_fieldset'])) {
            $txtfld['groupName'] = $data['f_fieldset'];
        }        
        // search every connection to selectors and add it on textfields                
        $properties = json_encode($txtfld);
        $insert = array(
            "f_instance_name"=>"txtfld_{$ui_name}_{$data['f_title']}",
            "f_properties"=>$properties,
            "f_type_id"=>6
        );        
        $this->newUi($insert,true);
    }
    
    public function createChk($data,$ui_name) 
    {
        $mandatory = (bool)strtolower(substr(str_replace(" : ", "", $data['f_mandatory']),0,1));
        $readonly = (bool)strtolower(substr(str_replace(" : ", "", $data['f_read_only']),0,1));        

        $radio = "false";
        if(strpos($data['f_ui_type'],"RADIO") !== false) {
            $radio = "true";
        }
        
        $array_vals = explode(',',$data['f_value']);
        if(count($array_vals) == 1) {
            $array_vals = explode('.',$data['f_value']);
        }
        $buttons = array(array('value'=>true,'level'=>1,'selected'=>false,'label'=>''));
        
        if(!empty($array_vals)) {
            $buttons = array();
            foreach($array_vals as $line_vals) {
                $exp_vals = explode(' : ',$line_vals);
                $value = $exp_vals[0];
                $text = isset($exp_vals[1])?$exp_vals[1]:$value;
                $buttons[] = array('value'=>$value,'level'=>1,'selected'=>false,'label'=>$text);
            }
        }                
        
        $chkbx = array(
            'buttons'=>$buttons,
            'label'=>$data['f_label'],
            'info'=>$data['f_info'],
            'mandatory'=>$mandatory,
            'readonly'=>$readonly,
            'bind'=>$data['f_title'],
            'radio'=>$radio, //f_radio
            'f_wo_types_visibility'=>isset($data['f_wo_types_visibility']) && !empty($data['f_wo_types_visibility'])?$data['f_wo_types_visibility']:'',
            'f_wares_types_visibility'=>isset($data['f_wares_types_visibility']) && !empty($data['f_wares_types_visibility'])?$data['f_wares_types_visibility']:''
        );
        if($data['f_fieldset'] != "" && !is_null($data['f_fieldset'])) {
            $chkbx['groupName'] = $data['f_fieldset'];
        }
        
        // search every connection to selectors and add it on checkbox        
        $chkbx['t_selectors'] = $this->getSelectorsUI($data);
        
        $properties = json_encode($chkbx);
        $insert = array(
            "f_instance_name"=>"chkbx_{$ui_name}_{$data['f_title']}",
            "f_properties"=>$properties,
            "f_type_id"=>8
        );
        $this->newUi($insert,true);        
    }
    
    public function getModuleNames() {
        $res = array();
        $q = new Zend_Db_Select($this->db);
        $res["workorders"] = $q->from("t_workorders_types", array("label" => "f_type", "value" => "f_short_name", "ftype" => "f_id", "table"=>"(select 't_workorders')"))->where("f_short_name IS NOT NULL")
            ->where("f_short_name != ''")->order("f_type")->query()->fetchAll();
        $q->reset();
        $res["wares"] = $q->from("t_wares_types", array("label" => "f_type", "value" => "f_short_name", "ftype" => "f_id", "table"=>"(select 't_wares')"))->where("f_short_name IS NOT NULL")
            ->where("f_short_name != ''")->order("f_type")->query()->fetchAll();
        $q->reset();
        $res["selectors"] = $q->from("t_selectors_types", array("label" => "f_type", "value" => "f_short_name", "ftype" => "f_id", "table"=>"(select 't_selectors')"))->where("f_short_name IS NOT NULL")
            ->where("f_short_name != ''")/*->group("f_short_name")*/->order("f_type")->query()->fetchAll();
        $q->reset();
        $res["systems"] = $q->from("t_systems_types", array("label" => "f_type", "value" => "f_short_name", "ftype" => "f_id", "table"=>"(select 't_systems')"))->where("f_short_name IS NOT NULL")
            ->where("f_short_name != ''")->order("f_type")->query()->fetchAll();
        $q->reset();
        return array_merge(
            array(array("label" => "------- Workorders --------", "f_short_name" => "", "table" => "", "ftype" => "")), $res["workorders"],
            array(array("label" => "---------- Wares ----------", "f_short_name" => "", "table" => "", "ftype" => "")), $res["wares"],
            array(array("label" => "-------- Selectors --------", "f_short_name" => "", "table" => "", "ftype" => "")), $res["selectors"],
            array(array("label" => "----- System settings -----", "f_short_name" => "", "table" => "", "ftype" => "")), $res["systems"]
        );
    }
    
    public function getSelectorTypes() {
        $res = array();
        $q = new Zend_Db_Select($this->db);
        return $q->from("t_selectors_types", array("f_id", "f_type"))->order("f_id")->query()->fetchAll();
    }   
    
    /**
     * creazione select options e select
     * @param type $data
     * @param type $table
     * @return type 
     */
    public function createSelect($data,$ui_name,$option_class) 
    {
        /** Return if option is empty */
        if(empty($option_class)){
            return '';
        }               
        $slct = array(
            'options'=>$option_class,
            'label'=>$data['f_label'],
            'info'=>$data['f_info'],            
            'bind'=>$data['f_title'],
            'radio'=>false,            
        );
        $slct = $this->setAttributesUi($slct, $data);
        if($data['f_fieldset'] != "" && !is_null($data['f_fieldset'])) {
            $slct['groupName'] = $data['f_fieldset'];
        }        
        $properties = json_encode($slct);        
        $insert = array(
            "f_instance_name"=>"slct_{$ui_name}_{$data['f_title']}",
            "f_properties"=>utf8_decode($properties),
            "f_type_id"=>7
        );
        $this->newUI($insert,true);
    }
    
    private function getSelectorsUI($data)
    {
        // search every connection to selectors and add it on textfields
        $select = new Zend_Db_Select($this->db);
        $selectors = array();
        foreach($data as $key => $val) {
            if(strpos($key,"fc_imp_cross_selector_") !== false && !empty($val)) { // is a selector                
                $explode_sel = explode(',',$val);
                if(count($explode_sel) == 1) { $explode_sel = explode('.',$val); }
                $type = str_replace("fc_imp_cross_selector_", "", $key);
                foreach($explode_sel as $code_sel) {
                    $select->reset();
                    try {
                        $res_sel = $select->from("t_selectors",array("f_code"))->where("fc_imp_code_sel_excel = ?",$code_sel)
                            ->where("f_type_id = ?",$type)->query()->fetch();
                    }catch(Exception $e) {                        
                        utilities::reportQueryError("t_selectors",$e->getMessage(),$select->__toString());die;
                    };
                    if(!empty($res_sel)) { $selectors[] = $res_sel['f_code']; }
                }
            }
        }
        return implode(',',$selectors);
    }
    
    private function setAttributesUi($ui,$data)
    {
        $types = array(
            'm'=>(int)strtolower(substr(str_replace(" : ", "", $data['f_mandatory']),0,1)),
            'r'=>(int)strtolower(substr(str_replace(" : ", "", $data['f_read_only']),0,1)),
            'v'=>1,
            'd'=>(int)strtolower(substr(str_replace(" : ", "", $data['f_disabled']),0,1))
        );
        $values = array("fields","wf","type","value","selector");
        foreach($types as $type => $val) {
            $ui["{$type}_on"] = $val;
            $ui["{$type}_level"] = $val==1?-1:"";
            $tot = count($values);
            for($i = 0;$i < $tot;++$i) {                
                $ui["{$type}_{$values[$i]}"] = "";
                if(isset($data["{$type}_{$values[$i]}"])) { $ui["{$type}_{$values[$i]}"] = $data["{$type}_{$values[$i]}"]; }
                if($values[$i] == "selector") { $ui["{$type}_{$values[$i]}"] = $this->getSelectorsUI($data);}
            }
        }
        if(isset($data['attach'])){
            $ui["attach"] = (int)strtolower(substr(str_replace(" : ", "", $data['attach']),0,1));
        }
        else{
            $ui["attach"] = 0;
        }
        return $ui;
    }
}