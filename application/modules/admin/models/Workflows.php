<?php

class MainsimAdmin_Model_Workflows
{
    
    /**
     *
     * @var Zend_Db 
     */
    private $db;
    public function __construct() {
        $this->db = Zend_Db::factory(Zend_Registry::get('db'));
    }
    
    public function __destruct() {
        $this->db->closeConnection();
    }
    
    public function wfList() {
        $t_workflows = array();
        $q = new Zend_Db_Select($this->db);
        $wfs = $q->from("t_workflows")->order("f_id")->query()->fetchAll();
        foreach($wfs as $wf) {
            $t_workflows[] = array(
                "f_id" => $wf['f_id'],
                "f_name" => $wf['f_name']
            );
        }
        return $t_workflows;
    }
    
    public function wfGroups() {
        $t_wf_groups = array();
        $q = new Zend_Db_Select($this->db);
        $groups = $q->from("t_wf_groups")->order("f_id")->query()->fetchAll();
        foreach($groups as $group) {
            $t_wf_groups[] = array(
                "f_id" => $group['f_id'],
                "f_group" => utf8_encode($group['f_group']),
                "f_visibility" => $group['f_visibility']
            );
        }
        return $t_wf_groups;
    }
    
    public function getPhases($wf) {
        $t_wf_phases = array();
        $q = new Zend_Db_Select($this->db);
        $phases = $q->from("t_wf_phases")->where("f_wf_id = $wf")->order("f_number")->query()->fetchAll();
        foreach($phases as $phase) {
            $t_wf_phases[] = array(
                "f_number" => $phase["f_number"],
                "f_percentage" => $phase["f_percentage"],
                "f_wf_id" => $phase["f_wf_id"],
                "f_group_id" => $phase["f_group_id"],
                "f_name" => Mainsim_Model_Utilities::chg(stripslashes($phase["f_name"])),
                "f_visibility" => $phase["f_visibility"],
                "f_editability" => $phase["f_editability"],
                "f_exits" => $this->getExits($wf, $phase["f_number"]),
                "f_summary_name"=>Mainsim_Model_Utilities::chg($phase["f_summary_name"]),
                "f_order"=>$phase["f_order"]
            );
        }                
        return $t_wf_phases;
    }
    
    public function getExits($wf, $phase) {
        $t_wf_exits = array();
        $q = new Zend_Db_Select($this->db);
        $exits = $q->from("t_wf_exits")->where("f_wf_id = $wf")->where("f_phase_number = ?", $phase)->order("f_exit")->query()->fetchAll();
        foreach($exits as $exit) {
            $t_wf_exits[] = array(
                "f_wf_id" => $exit["f_wf_id"],
                "f_phase_number" => $exit["f_phase_number"],
                "f_description" => $exit["f_description"],
                "f_exit" => $exit["f_exit"],
                "f_script" => base64_encode(/*Mainsim_Model_Utilities::chg(*/$exit["f_script"]/*)*/),
                "f_visibility" => $exit["f_visibility"]
            );
        }
        return $t_wf_exits;
    }
    
    public function getWf($wf) {
        $q = new Zend_Db_Select($this->db);
        $opening = $q->from("t_wf_exits", array("f_script"))->where("f_wf_id = $wf")->where("f_phase_number = 0")->query()->fetch();        
        $q->reset();
        $resUsed = $q->from("t_creation_date_history", array("max"=>"max(f_phase_id)"))->where("f_wf_id = ?", $wf)->query()->fetch();
        $inUse = is_null($resUsed['max'])?0:$resUsed['max'];        
        return array($this->getPhases($wf), base64_encode($opening["f_script"]),$inUse);
    }
    
    public function createWf($name) {
        try {  
            $this->db->beginTransaction();
            $select = new Zend_Db_Select($this->db);
            $resExists = $select->from('t_workflows')->where('f_name = ?',$name)
                ->query()->fetch();
            if(!empty($resExists)) {
                throw new Exception("Workflow's name must be unique.");
            }
            $this->db->insert("t_workflows", array("f_name" => $name));
            $wf_id = $this->db->lastInsertId();
            
            // Creating first phases
            $phases = array(
                array( "f_number" => 1, "f_percentage" => 0, "f_wf_id" => $wf_id, "f_group_id" => 1, "f_name" => "Opening","f_summary_name"=>"Opening",
                    "f_visibility" => -1,"f_editability" => -1, "f_exits" => array(
                        array("f_wf_id" => $wf_id, "f_phase_number" => 1, "f_description" => "Close", "f_exit" => 2, "f_script" => "", "f_visibility" => -1),
                        array("f_wf_id" => $wf_id, "f_phase_number" => 1, "f_description" => "Delete", "f_exit" => 3, "f_script" => "", "f_visibility" => -1)
                    )
                ),
                array( "f_number" => 2, "f_percentage" => 100, "f_wf_id" => $wf_id, "f_group_id" => 6, "f_name" => "Closing","f_summary_name"=>"Closing",
                    "f_visibility" => -1,"f_editability" => -1, "f_exits" => array()
                ),
                array( "f_number" => 3, "f_percentage" => 0, "f_wf_id" => $wf_id, "f_group_id" => 7, "f_name" => "Deleting","f_summary_name"=>"Deleting",
                    "f_visibility" => -1,"f_editability" => -1, "f_exits" => array()
                ),
                array( "f_number" => 4, "f_percentage" => 0, "f_wf_id" => $wf_id, "f_group_id" => 8, "f_name" => "Cloned","f_summary_name"=>"",
                    "f_visibility" => -1,"f_editability" => -1, "f_exits" => array(
                        array("f_wf_id" => $wf_id, "f_phase_number" => 4, "f_description" => "Open", "f_exit" => 1, "f_script" => '$scriptModule->changePhasePropagator();', "f_visibility" => -1),
                        array("f_wf_id" => $wf_id, "f_phase_number" => 4, "f_description" => "Delete", "f_exit" => 3, "f_script" => '$scriptModule->changePhasePropagator();', "f_visibility" => -1)                        
                    )
                ),
            );
            
            foreach($phases as $phase) {
                $res = $this->savePhase($phase);
                if(is_array($res)) return $res;
            }
            
            // Creating opening exit for opening script
            $this->db->insert("t_wf_exits", array(
                "f_wf_id" => $wf_id,
                "f_phase_number" => 0,
                "f_description" => "Creation",
                "f_exit" => 1,
                "f_script" => "",
                "f_visibility" => -1
            ));
            $this->db->commit();
        } catch(Exception $ex) {
            $this->db->rollBack();
            return array("message" => "Problems in creating wf: ".$ex->getMessage());
        }
        return array("action" => "add", "label" => $wf_id.") ".$name, "value" => $wf_id);
    }
    
    public function saveWf($wf) {        
        try {
            $this->db->beginTransaction(); 
            $select = new Zend_Db_Select($this->db);
            $phases = array_map(function($el){ return (int)$el["f_number"];},$wf["phases"]);
            $phases[] = 0;
            $resDeletedPhases = $select->from("t_wf_phases",array("f_id","f_number"))
                ->where("f_number not in (?)",$phases)->where("f_wf_id = ?",$wf["wfn"])->query()->fetchAll();
            $totA = count($resDeletedPhases);
            for($a = 0;$a < $totA;++$a) {
                $this->db->delete("t_wf_exits","f_phase_number = {$resDeletedPhases[$a]['f_number']} and f_wf_id = {$wf["wfn"]}");
                $this->db->delete("t_wf_phases","f_id = {$resDeletedPhases[$a]['f_id']}");
            }
            // Updating opening script
            $this->db->update("t_wf_exits", array("f_script" => base64_decode($wf["ext0"])), "f_wf_id = {$wf["wfn"]} AND f_phase_number = 0 AND f_exit = 1");
            foreach($wf["phases"] as $phase) {                                
                foreach($phase['f_exits'] as &$f_exit){
                    $f_exit['f_script'] = base64_decode($f_exit['f_script']);
                    if($f_exit['f_script'] == 'undefined') {
                        $f_exit['f_script'] = '';
                    }
                }                    
                $this->savePhase($phase);                
            }
            $this->db->commit();
        } catch (Exception $ex) {
            $this->db->rollBack();
            return array("error" => "Problems in saving wf: ".$ex->getMessage());
        }        
        return array("message" => "Workflow saved successfully!");
    }
    
    public function printWf($wf_params) { 
        extract($wf_params);
        try {
            if (file_exists(SCRIPTS_PATH . 'print_wf.php')) {
                      include(SCRIPTS_PATH . 'print_wf.php');
                  }else if (file_exists(SCRIPTS_PATH_DEFAULT . 'print_wf.php')){
                       include(SCRIPTS_PATH_DEFAULT . 'print_wf.php');
                  }
        } catch (Exception $ex) {            
//            return array("error" => "Problems in printing wf: ".$ex->getMessage());
        }        
        return array("message" => "Workflow printed successfully!");
    }
    
    /**
     * Save wf phase in t_wf_phases
     * @param array $phase list of phases
     * @throws Exception raise an exception if something is wrong
     */
    public function savePhase($phase) {  
        $exits = $phase["f_exits"]; unset($phase["f_exits"]);   // Getting exits and removing them from the phase array            
        $phase["f_name"] = $phase["f_name"];
        if(empty($phase['f_order'])) {
            $phase['f_order'] = $phase['f_number'];
        }
        // Checking if the phase already exists        
        $q = new Zend_Db_Select($this->db);
        $res = $q->from("t_wf_phases",'f_id')->where("f_wf_id = {$phase["f_wf_id"]}")->where("f_number = {$phase["f_number"]}")
            ->query()->fetch();        
        
        try {
            if(empty($res)) {
                $this->db->insert("t_wf_phases", $phase);
            }
            else {
                $this->db->update("t_wf_phases", $phase,"f_id = {$res['f_id']}");
            }
        } catch(Exception $e) {            
            throw new Exception ("Problems in creating phases: ".$e->getMessage());
        }

        /******** Aggiunte e modifiche del 30/11/2018  *******/
        $f_exits = array_map(function($el){ return (int)$el["f_exit"];},$exits);
        if(empty($f_exits))  $f_exits = [-1]; //elimina tutte le uscite
        $q->reset();
        $resDeletedExits = $q->from("t_wf_exits",array("f_exit"))
            ->where("f_phase_number = ? ",$phase['f_number'])->where("f_exit not in (?)",$f_exits)->where("f_wf_id = ?",$phase["f_wf_id"])->query()->fetchAll();
        $totA = count($resDeletedExits);
        
        for($a = 0;$a < $totA;++$a) {
            $this->db->delete("t_wf_exits","f_phase_number = {$phase['f_number']} and f_wf_id = {$phase["f_wf_id"]} and f_exit = {$resDeletedExits[$a]['f_exit']}");
        }
        //$this->db->delete("t_wf_exits","f_wf_id = {$phase['f_wf_id']} and f_phase_number = {$phase['f_number']}");
        /****************************************************/
        
        foreach($exits as $exit) {
            $this->saveExit($phase["f_wf_id"], $phase["f_number"], $exit);            
        }                
    }
    
    public function saveExit($wf, $phase, $exit) {        
        $exit["f_wf_id"] = $wf;
        $exit["f_phase_number"] = $phase;
        $exit["f_description"] = $exit["f_description"];
        $exit["f_script"] = $exit["f_script"];
        // Checking if the exit already exists        
        $q = new Zend_Db_Select($this->db);
        $res = $q->from("t_wf_exits",'f_id')->where("f_wf_id = $wf")->where("f_phase_number = $phase")
            ->where("f_exit = {$exit["f_exit"]}")->query()->fetch();        
        try {
            if(empty($res)) {
                $this->db->insert("t_wf_exits", $exit);
            }
            else {
                $this->db->update("t_wf_exits", $exit,"f_id = {$res['f_id']}");
            }
        } catch(Exception $e) {            
            throw new Exception("Problems in creating exits: ".$e->getMessage());
        }                
    }
    
    public function deleteWf($wfn) {
        try {
            $this->db->delete("t_wf_exits", "f_wf_id = $wfn");
            try {
                $this->db->delete("t_wf_phases", "f_wf_id = $wfn");
                try {
                    $this->db->delete("t_workflows", "f_id = $wfn");
                } catch(Exception $e) {
                    return array("message" => "Error in deleting workflow: ".$e->getMessage());
                }
            } catch(Exception $e) {
                return array("message" => "Error in deleting workflow phases: ".$e->getMessage());
            }
        } catch(Exception $e) {
            return array("message" => "Error in deleting workflow exits: ".$e->getMessage());
        }
        return array("action" => "del", "wf" => $wfn);
    }
}