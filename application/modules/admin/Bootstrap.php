<?php
class Admin_Bootstrap extends Zend_Application_Module_Bootstrap 
{
  protected function _initAutoload()
  {         
  	$autoloader = new Zend_Application_Module_Autoloader(array(
         'namespace' => 'MainsimAdmin_',
         'basePath'  => APPLICATION_PATH .'/modules/admin',
         'resourceTypes' => array ()));
      return $autoloader;
  } 
   
}
