<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions());        
        Zend_Registry::set('config', $config);
        return $config;
    }
    
    protected function _initDatabaseRegistry()
    {
        $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');
        Zend_Registry::set('db', $config->database); 
        Zend_Registry::set('db_connection',Zend_Db::factory($config->database));
        return $config->database;
    }
    
    protected function _initDefaultSettings()
    {
		$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');        
		Zend_Registry::set('attachmentsFolder',$config->attachmentsFolder);				
        Zend_Registry::set('multiInstallation',"Off");			
        $php_self =  str_replace(array("/index.php","/wizard/wizard.php","/new_wizard/wiz.php"),"",$_SERVER['PHP_SELF']);
        if($_SERVER['HTTP_HOST'] == 'test.mainsim.com'){
            $expRU = explode('/',$_SERVER['REQUEST_URI']);    
            $php_self = '/'.$expRU[1];            
        }
        define('BASEURL',$php_self);
	}
    
    protected function _initMultiDatabase()
    {
        try {
            $config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production');

            if($config->multiDatabase == 'On'){
                if($config->staging == 'On'){
                    //$host = (explode("/",$_SERVER['REQUEST_URI'])[1]) . ".mainsim.com";
                    $host = (explode(".",$_SERVER['SERVER_NAME'])[0]) . ".mainsim.com";
                }
                else{
                    $host = $_SERVER['HTTP_HOST'];
                }
                include APPLICATION_PATH.'/modules/default/models/Utilities.php';
                $info = Mainsim_Model_Utilities::getInstallationInfo($config->database,$host,'f_url');                                                    
                Zend_Registry::set('db', $info['database']); 
                Zend_Registry::set('db_connection',Zend_Db::factory($info['database']));
                Zend_Registry::set('attachmentsFolder',$info['attachment_path']);			            
                Zend_Registry::set('multiInstallation',"On");			
            } 
        }catch(Exception $e) {
            echo '
                <style type="text/css">
                    div#error {background-color: #6f8d97;height: 100%;position: absolute;width: 100%;}
                    div#error > * {font: 16px opensansL;color: #ffffff;}
                    div#error .header .logo {
                        background: url("public/msim_images/default/mainsim_logo_large.png") no-repeat scroll center center;
                        height: 70px;margin-top: 10px;}
                    div#error .header .title {font-size: 26px;text-align: center;}                    
                    div#error .header .title {margin-top: 100px;display: none;}
                    div#error .header .title { color: #A30A17; font-weight: bold; }
                    div#error .header .message {font-size: 16px;text-align: center;}                    
                    div#error .header .message {margin-top: 140px;font-size: 24px;}
                    div#error .footer {font-size: 12px;text-align: center;position: absolute;bottom: 10px; left: 0px; right: 0px;}
                </style><div id="error"><div class="header"><div class="logo">&nbsp;</div><div class="title">Mainsim Error</div><div class="message">
                        '.$e->getMessage().'</div></div><div class="footer">mainsim&trade; - what maintenance can be - Copyright &copy; 2015 mainsim. All rights reserved.</div></div>';
            die;
        }
	}
    
    protected function _initDefineElements() 
    {
        $db = Zend_Registry::get('db_connection');
        $q = new Zend_Db_Select($db);        
        $resMain = $q->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
            ->join(array("s" => "t_systems"), "c.f_id = s.f_code",array())
            ->where("c.f_type = 'SYSTEMS'")->where("c.f_category = 'SETTING'")->query()->fetchAll();
            //->where("f_title IN ('THEME','PROJECT_NAME')")
            //->where("s.fc_sys_server_only != 1")                
        $main = array();
        array_walk($resMain, function($el) use(&$main){
            $main[$el['f_title']] = $el['f_description'];
        });        
        /*foreach($resMain as $valueMain) {
            $main[$valueMain['f_title']] = $valueMain['f_description'];
        } */       
        Zend_Registry::set('settings',$main);
        $php_self =  str_replace(array("/index.php","/wizard/wizard.php","/new_wizard/wiz.php"),"",$_SERVER['PHP_SELF']);
        define('THEME', isset($main['THEME'])?$main['THEME']:'' );
        define('PROJECT_NAME',  isset($main['PROJECT_NAME'])?$main['PROJECT_NAME']:'');
        define('IMAGE_DEFAULT_PATH', $php_self."/public/msim_images/default/");
        define('IMAGE_PATH', $php_self."/public/msim_images/".THEME."/");
        define('ATTACHMENT_PATH', $php_self."/attachments/");
        define('JS_PATH', $php_self."/public/msim_jslib/");
        define('CSS_PATH', $php_self."/public/msim_css/");
        define('SVG_PATH', $php_self."/public/msim_svg/");
        define('LIBRARY_PATH', $php_self."/library/");

        define('SCRIPTS_PATH', APPLICATION_PATH . "/../scripts/" . PROJECT_NAME . '/');
        define('SCRIPTS_PATH_DEFAULT',  APPLICATION_PATH . "/../scripts/default/");
        
        define('IMAGE_DEFAULT_PATH_PHP', APPLICATION_PATH . "/../public/msim_images/default/");
        define('IMAGE_PATH_PHP', APPLICATION_PATH . "/../public/msim_images/".THEME."/");
    }

}