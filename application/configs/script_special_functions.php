<?php
$f_code_condition = 0;
$f_code_meter = 0;

/**
 * Function to send mail 
 * @param type $name name of the addressee
 * @param type $mail_addr mail of addressee
 * @param type $subject subject of mail
 * @param type $body  body of mail
 */
function SEND_MAIL($name,$mail_addr,$subject,$body)
{
    $mail = new Mainsim_Model_Mail();
    $mail->sendMail(array('To'=>array($name=>$mail_addr)), $subject, $body);
    unset($mail);    
}

function STOP($message = '')
{
    $db = Zend_Db::factory(Zend_Registry::get('db'));
    if(!empty($message)) {
        $db->insert("t_logs",array(
            'f_timestamp'=>time(),
            'f_log'=>$message,
            'f_type_log'=>0
        ));
        throw new Exception($message);
    }
}

/**
 * special function to get value or action from a f_code insert
 * @param int $f_code 
 */
function FCODE($f_code = 0)
{
    global $f_code_meter;    
    $db = Zend_Db::factory(Zend_Registry::get('db'));    
    //try to understand what table of code is it
    $select = new Zend_Db_Select($db);
    $res_id = $select->from("t_creation_date")->where("f_id = ?",$f_code)->query()->fetchAll();
    //if f_code does not exist, go out; 
    if(empty($res_id)) return;
    
    $tab = "t_".strtolower($res_id[0]['f_type']);
    //now search what type of code is it
    $select->reset();
    switch($res_id[0]['f_category']) {
        case 'METER' : //DEPRECATED
            $f_code_meter = $f_code;
            $res_value = $select->from("t_custom_fields","fc_meter_value")
                ->where("f_code = ?",$f_code)
                ->query()->fetchAll();            
            return $res_value[0]['fc_meter_value'];
            break;
        case 'ACTION' :             
            $res_value = $select->from("t_custom_fields","fc_action_function")
                ->where("f_code = ?",$f_code)->query()->fetchAll();
            eval($res_value[0]['fc_action_function']);
            break;
    }
}

/**
    * check if passed value in meter reading
    * is in value list
    * @param type $f_code f_code meter generator
    * @param type $value value of meter reading
    */

function IN_LIST($f_code,$list) 
{
    global $f_code_meter;  
    $f_code_meter = $f_code;
    $db = Zend_Db::factory(Zend_Registry::get('db'));
    $select = new Zend_Db_Select($db);
    $exp_list = explode(',',$list);    
    $res_value = $select->from("t_meters_history",array("f_value"))->where("f_active = 1")
            ->where("f_checked = 0")->where("f_code = ?",$f_code)
            ->order("f_id DESC")->limit(1)->query()->fetchAll();    
    $in_list = false;
    $db->closeConnection();
    foreach($exp_list as $line) {
        if($res_value[0]['fc_meter_value'] ==  $line) {
            $in_list = true;            
        }
    }
    return $in_list;
}

/**
 * check if total meter insert is a multiple of a setted value
 * @param type $f_code 
 */
function EVERY($f_code,$times = 0) 
{
    $times = (int)$times;
    if(!$times) return false;    
    global $f_code_meter;  
    $f_code_meter = $f_code;
    
    $db = Zend_Db::factory(Zend_Registry::get('db'));        
    $select = new Zend_Db_Select($db);
    $res_last_val = $select->from("t_meters_history",array("f_value"))
        ->where("f_code = ?",$f_code)->where("f_checked = 0")->order("f_id DESC")->query()->fetch();    
    $value = $res_last_val['f_value'];
    if(ctype_alpha($value)) {
        $value = (int)$value;
    }
    
    $select->reset();
    $res_values = $select->from("t_meters_history",array("max"=>"MAX(f_value)"))
        ->where("f_code = ?",$f_code)->where("f_checked = 1")->query()->fetch();      
    $last_value = !empty($res_values['max'])?$res_values['max']:$times; 
    
    $inc = 0;
    $inc_sup = $times;
    for(;;) {
        if($inc >= $last_value) break;
        $inc+=$times;
        $inc_sup+=$times;
    }    
    
    if($value >= $inc) return true;        
    return false;    
}

/**
 * Check if last value inserted is inside a range.
 * @param type $f_code f_code meter
 * @param type $lower
 * @param type $upper
 * @return boolean true is inside,false outside
 */
function IN_RANGE($f_code,$lower,$upper)
{
    global $f_code_meter;  
    $f_code_meter = $f_code;
    $db = Zend_Db::factory(Zend_Registry::get('db'));        
    $select = new Zend_Db_Select($db);
    
    $res_val = $select->from("t_meters_history",array("f_value"))->where("f_active = 1")
            ->where("f_checked = 1")->where("f_code = ?",$f_code)
            ->order("f_id DESC")->limit(1)->query()->fetch();    
    $db->closeConnection();    
    if(empty($res_val)) return false;        
    $in_range = false;
    if($res_val['f_value'] >= $lower && $res_val['f_value'] < $upper) {
        $in_range = true;
    }    
    return $in_range;
}

/**
 * get back last value inserted
 * @param type $f_code
 * @return boolean 
 */
function METER_VALUE($f_code) 
{       
    global $f_code_meter;  
    $f_code_meter = $f_code;
    $db = Zend_Db::factory(Zend_Registry::get('db'));        
    $select = new Zend_Db_Select($db);
    
    $res_val = $select->from("t_meters_history",array("f_value"))->where("f_active = 1")
            ->where("f_checked = 0")->where("f_code = ?",$f_code)
            ->order("f_id DESC")->limit(1)->query()->fetchAll();    
    $db->closeConnection();    
    if(empty($res_val)) return false;        
    return $res_val[0]['f_value'];
}

function ONCONDITIONS()
{    
    
    global $f_code_condition;
    $db = Zend_Db::factory(Zend_Registry::get('db'));
    $select = new Zend_Db_Select($db);    
    //get how many meter values insert are to check
    $res_meter = $select->from("t_meters_history",array("f_id","f_code"))->where("f_active = 1")->where("f_checked = 0")
            ->where("f_in_work = 0")->query()->fetchAll();    
    foreach($res_meter as $line_meter){
        $db->update("t_meters_history", array("f_in_work"=>1),"f_id = {$line_meter['f_id']}");
        try {
            $select->reset();
            $code_meter = $line_meter['f_code'];
            $res_oc = $select
                ->from(array("t1"=>"t_creation_date"),array())
                ->join(array("t2"=>"t_workorders"),"t1.f_id = t2.f_code",array('f_code','fc_cond_condition'))
                ->join(array("t3"=>"t_wf_phases"),"t1.f_phase_id = t3.f_number and t1.f_wf_id = t3.f_wf_id",array())                
                ->join(array("t4"=>"t_wf_groups"),"t3.f_group_id = t4.f_id",array())                
                ->join(array("t5"=>"t_wo_relations"),"t5.f_code_wo_slave = t1.f_id",array())                    
                ->where("t2.f_type_id = 9")
                ->where("t5.f_code_wo_master = ?",$code_meter)      
                ->where("t4.f_visibility != 0")
                ->query()->fetchAll();    
            foreach($res_oc as $line) {        
                if(!empty($line['fc_cond_condition'])){            
                    $f_code_condition = $line['f_code'];                          
                    eval($line['fc_cond_condition']);                
                }
            }
        }catch(Exception $e) {
            echo $e->getMessage();
            $db->insert("t_logs",array("f_timestamp"=>time(),"f_log"=>"Error during on condition generation : ".$e->getMessage(),"f_type_log"=>0));
        }
        //update meter read f_checked = 1        
        $db->update("t_meters_history", array("f_checked"=>1),"f_id = {$line_meter['f_id']}");
    }
    $db->closeConnection();
}

/**
 * Return the avarage of relevations did with a passed meter 
 * @param type $f_code code of meter
 * @param type $num_rilevazioni number of relevation before
 * @return double avarage of relevation
 */
function AVG($f_code = 0,$num_rilevations = 0) 
{
    if(!$f_code || $num_rilevations) return 0;
    global $f_code_meter;
    $db = Zend_Db::factory(Zend_Registry::get('db'));
    $select = new Zend_Db_Select($db);    
    
    $f_code_meter = $f_code;
    $res_val = $select->from("t_meters_history",array("f_value"))->where("f_active = 1")
            ->where("f_code = ?",$f_code)
            ->order("f_id DESC")->limit($num_rilevations)->query()->fetchAll();    
    $db->closeConnection();    
    $val = 0;
    foreach($res_val as $line_val) {
        $val+=$line_val;
    }
    $val = ($val/count($res_val));    
        
    return $val;   
}

/**
 *  @global int $f_code_condition
 * @global Zend_Config_ini $config
 * @global int $f_code_meter
 * @return type 
 */

function OPENWORKORDER()
{    
    global $f_code_condition,$f_code_meter;        
    $db = Zend_Db::factory(Zend_Registry::get('db'));  
    $select = new Zend_Db_Select($db);
    //$conditions_code = array($f_code_condition);
    //$f_codes_assoc = array();
    //include APPLICATION_PATH.'/modules/default/models/Workorders.php';
    //Like PM, manage hierarchy in two different ways : 
    // 1) father without condition 
    //first check : 
    $res_father_no_oc = $select->from(array("t1"=>"t_workorders"),array("fc_cond_condition"))
        ->join(array("t2"=>"t_workorders_parent"),"t1.f_code = t2.f_parent_code",array())
        ->where("t2.f_active = 1")->where("t2.f_code = ?",$f_code_condition)->query()->fetch();    
    if(!empty($res_father_no_oc) && strlen($res_father_no_oc['fc_cond_condition'])) {        
        return;
    }
    
    $wo = new Mainsim_Model_Workorders();    
    //get default user info    
    $admin_info = Mainsim_Model_Login::getUSerInfo(1, $db);
    $select->reset();
    $res_meter = $select->from("t_meters_history",array("f_value"))->where("f_active = 1")
            ->where("f_code = ?",$f_code_meter)->where("f_checked = 0")->query()->fetch();
    $row = array('f_code'=>$f_code_condition,'f_start_date'=>time(),'fc_meter_value'=>$res_meter['f_value']);
    $wo->initializeWoGeneration($row, $admin_info);
}