<?php
define('SIGNATURE',"mainsim&#8482; vers.3.0 - Web Based Maintenance Management Software | Info at mainsim.it | Copyright &#169; 2007 - ".@date("Y")." UNPlugged | unp.it");
define('app_name', "mainsim&#8482;");
define('APP_VERSION', '3.0.7.1 Beta');
define('FRMT_APP_NAME', "<b>main</b>sim&#8482;");

// mobile
define("WORKORDER_TYPES", "1,4,7,13");
define("MOBILE_MODULES", serialize(array(
    'mdl_wo_tg' => array(
        'type' => 'WORKORDERS'
    ),
    'mdl_asset_tg' => array(
        'type' => 'WARES'
    ),
    'mdl_wo_task_tg' => array(
        'type' => 'TASKS'
    )
)));
define("CHG_MOBILE", true);
define("GROUP_PHASES_WO", "1,2,3,4,5,6");

define("SEND_MAIL", true);