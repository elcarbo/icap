<?php
$es_ES = array(

"Search"=>"Busqueda",
"Help"=>"Ayuda",
"Users Connected"=>"Usuarios Conectados",
"All rights reserved"=>"Todos los derechos reservados",
"Welcome"=>"Bienvenido",

/******************************/
/**     Loading Messages     **/
/******************************/

"Loading images, progress" => "Cargando imagenes,en progreso", 
"0 Record found."=>"0 resultados encontrados.",
"Opening document: please wait..." => "Abriendo documento:por favor espere...",


/******************************/
/**         Tree Grid        **/
/******************************/

//General + Work Request
"Code"=>"Codigo",
"Category" => "Categoria",
"Priority" => "Prioridad",
"Icon" => "Icono",
"Workflow Phase" => "Fase de flujo de trabajo",
"Phase Name" => "Nombre Fase",
"Title" => "Titulo",
"Description" => "Descripcion",
"System Code" => "Codigo del Sistema",
"Requestor" => "Solicitante",
"Requestor Name" => "Nombre del solicitante",
"Opening Date" => "Fecha de Apertura",
"Opening date" => "Fecha de apertura",
"Requestor e-mail" => "E-mail Solicitante",
"Requestor Phone" => "Telefono Solicitante",
"Phone" => "Telefono",
"Modified by" => "Modificado por",
"Last Modified" => "Ultima Modificacion",
"No match found." => "Ningun resultado encontrado.",
//Work Orders
"Docs" => "Docs",
"Starting Date" => "Fecha de Inicio",
"Ending Date" => "Fecha de Fin",
"Planned Starting Date" => "Fecha de Inicio planificada ",
"Planned Ending Date" => "Fecha de Finalizacion planificada",
"Downtime Costs" => "Coste Inactividad",
"Downtime costs" => "Coste inactividad",
"Material Costs" => "Coste de Materiales",
"Labor Costs" => "Coste de Mano Obra", 
"Tool Costs" => "Coste de Herramientas",
"Services Costs" => "Coste de Servicios",
"Total Costs" => "Coste Total",
"Meter Value" => "Valor Contador",
"Response Time (in days)" => "Tiempo de Respuesta (dias)",

"Add viewed"=>"Añadir vista",
"Sub viewed"=>"Sub vista",
"Uncheck all"=>"Eliminar toda seleccion",

"Sort Ascending"=>"Orden Ascendente",
"Sort Descending"=>"Orden Descendente ",
"Add Filter"=>"Añadir filtro",
"Add Sort"=>"Añadir Orden",
"Add Search"=>"Añadir Busqueda",
"Add Filter by"=>"Añadir Filtro por",
"Clear all Filter"=>"Limpiar todos los filtros",
"Search"=>"Buscar",
"Filter by"=>"Filtrar por",
"Lock / Unlock"=>"Bloquear / Desbloquear",
"Hidden"=>"Oculto",


/******************************/
/**        Upload File       **/
/******************************/

"File upload" => "Cargar Archivo",
"Choose file to attach or insert a link" => "Elegir archivo para adjuntar o insertar un link",
"Attach" => "Adjuntar", 

/******************************/
/**     Calendar/Timeline    **/
/******************************/

"Starting Date" => "Fecha de Inicio",
"Ending Date" => "Fecha de Fin",
"Planned Starting Date" => "Fecha Planificada de Inicio",
"Planned Ending Date" => "Fecha Planificada de Fin",
"Jan" => "Ener",
"Feb" => "Feb",
"Mar" => "Mar",
"Apr" => "Abr",
"May" => "May",
"Jun" =>"Jun",
"Jul" => "Jul",
"Aug" => "Ago",
"Sep" =>"Sept",
"Oct" => "Oct",
"Nov" =>"Nov",
"Dec" =>"Dic",

"January"=>"Enero",    
"February"=>"Febrero",
"March"=>"Marzo",
"April"=>"Abril",
"May"=>"Mayo",
"June"=>"Junio",	 
"July"=>"Julio",	 
"August"=>"Agosto",	 
"September"=>"Septiembre",
"October"=>"Octubre",
"November"=>"Noviembre",
"December"=>"Diciembre",

"Mon"=>"Lun",
"Tue"=>"Mar",
"Wed"=>"Mier",
"Thu" =>"Jue",
"Fri"=>"Vier",
"Sat"=>"Sab",
"Sun"=>"Dom",

"Mo"=>"Lu",
"Tu"=>"Ma",
"We"=>"Mi",
"Th" =>"Ju",
"Fr"=>"Vi",
"Sa"=>"Sa",
"Su"=>"Do",

"Monday"=>"Lunes",
"Tuesday"=>"Martes",
"Wednesday"=>"Miercoles",
"Thursday" =>"Jueves",
"Friday"=>"Viernes",
"Saturday"=>"Sabado" ,
"Sunday"=>"Domingo",
"Year" => "Año",
" Quarter"=>"° Trimestre",
"Week"=>"Semana",
"Hours"=>"Horas",

"Availability"=>"Disponibilidad",
"Grid"=>"Cuadro",
"Final"=>"Final",
"Average"=>"Media",
"Scale up"=>"Escala Superior",
"Scale down"=>"Escala Inferior",
"Auto refresh"=>"Actualizacion automatica",
"Reload Timeline"=>"Actualizacion linea del tiempo",
"Fast view"=>"Vista Rapida",
"Delete"=>"Eliminar",
"Undo"=>"Deshacer",
"Loading..."=>"Cargando...",
"no Name TreeGrid inizialized"=>"Ningun nombre TreeGrid inicializado",
"Waiting..."=>"Esperando...",
"Operation in progress. Please wait." => "Operacion en progreso. Espere por favor.",


/******************************/
/**    Command Bar/Buttons   **/
/******************************/

"All"=>"Todo",
"New"=>"Nuevo",
"Add"=>"Añadir",
"Remove"=>"Quitar",
"Import"=>"Importar",
"Cancel"=>"Cancelar",
"Save"=>"Guardar",
"Select"=>"Seleccionar",
"Corrective" => "Correctiva",
"Emergency" => "Emergencia",
"Clone"=>"Duplicar",
"Element only" => "Unico elemento",
"Hierarchy"=>"Jerarquia",
"Print Work Order" => "Imprimir Orden de Trabajo",
"Print Work Request" => "Imprimir Peticion de Trabajo",
"Extras"=>"Extras",
//***********MultiEdit
"Select items to edit first" => "Selecciona primero los elementos para editar",
//***********Clone
"Select items to clone first" => "Selecciona primero los elementos para duplicar",
//***********Inspection
"Inspection" => "Inspeccion",
"Shared" => "Compartido",
"Personal" => "Personal",
"Save as..." => "Guardar como...",
"Delete displayed" => "Borrar visualizacion",
"Set a name for the new inspection" => "Configurar un nombre para la nueva inspeccion",
"set as shared inspection" => "Configurar como inspeccion compartida",
"The name 'default' is reserved, choose another one" => "El nombre 'por defecto'esta reservado,elija otro",
"The name is already in use" => "El nombre ya esta en uso",
"You cannot set the name default for your own inspections" => "No puede configurar el nombre por defecto debido a sus propias inspecciones",       
"You cannot delete shared inspections" => "No puede borrar las inspecciones compartidas",
"You cannot delete default inspections" => "No puede borrar las inpecciones por defecto",
"Inspection deleted successfully!" => "Inspeccion cancelada satisfactoriamente",
"Saving inspection, please wait..." => "Guardando inspeccion,por favor espere...",
"Setting up inspection, please wait..." => "Configurando inspeccion,por favor espere...",
"Trying to delete inspection" => "Intentando borrar inspeccion",
//***********Phase
"Change phase"=>"Cambio de fase",
"Periodic" => "Periodico",
"Meter Reading" => "Lectura de Contador",
"Report" => "Informe", 
"Normal" => "Normal", 
"Detailed" => "Detallado",
"Active" => "Activo",
"Inactive" => "Inactivo",
"Deletion" => "Cancelacion",
"There are" => "Hay",
"elements modified" => "elementos modificados",
"documents associated to this asset" => "",
"contracts associated to this asset" => "",
"parts associated to this workorder" => "",
"resources associated to this workorder" => "",
"tools associated to this workorder" => "",
"workorders associated to this asset" => "",
"standard workorders associated to this asset" => "",
"periodic maintenances associated to this asset" => "",
"on condition workorders associated to this asset" => "",
"This asset has a unplanned downtime right now" => "",
"This asset has a planned downtime right now" => "",
"Treegrid icon image not found" => "",
//***********Bookmark
"Bookmark"=>"Marcador",
"None"=>"Ninguno",   
"Red"=>"Rojo",
"Yellow"=>"Amarillo",
"Blue"=>"Azul",
"Select items to highlight" => "Seleccionar elementos para resaltar",
//***********Standard Work Orders
"Create WO"=>"Crear ODT",

/******************************/
/** 	Other Alert

/******************************/
/**       Modules Name       **/
/******************************/

"Instrument Panel" => "Panel de Instrumento",
"Work Requests" => "Peticiones de Trabajo", 
"Work Orders" => "Ordenes de Trabajo",
"Standard Work Orders" => "ODT Estandard",
"Assets" => "Activos",
"Periodic Maintenances" => "Manutencion Periodica",
"On condition" => " Con la condicion",
"On Condition" => "Con la Condicion",
"Meters" => "Medidores",
"Tasks" => "Tareas",
"Inventory" => "Inventario",
"Labors" => "Trabajadores",
"Tools" => "Herramientas",
"Vendors" => "Proveedores",
"Offers" => "Propuestas",
"Purchase Orders" => "Ordenes de Compra",
"Contracts" => "Contratos",
"Failure Codes" => "Codigos de Fallo",
"Failure Code" => "Codigo de Fallo",
"Documents" => "Documentos", 
"Schedule" => "Programa",
"Actions" => "Acciones",
"Reservations" => "Reservas",
"User Profile" => "Perfil de Usuario", 
"Users" => "Usuarios",
"Selectors" => "Seleccionadores",
"Bulletin" => "Boletin",
"Importer" => "Importador",
"Plan"=> "Plan",


/******************************/
/**        Instr.Panel       **/
/******************************/

"Bulletin Board" => "Tablon de Anuncios",
"Locations" => "Ubicaciones",
"Posted by" => "Publicado por",
"at" => "En",
"on" => "Encendido",

/******************************/
/**       Work Request       **/
/******************************/

"History" => "Historial",



/******************************/
/**       Work Orders        **/
/******************************/

"Work Orders Summary" => "Sumario Ordenes de Trabajo",
"Inbox" => "Bandeja de Entrada",
"My work orders" => "Mis Ordenes de Trabajo",
"All Work Orders" => "Todas la Ordenes de Trabajo",
"Opening" => "Apertura",
"Approval" => "Aprovado",                                
"Cancellation" => "Cancelacion", 
"Completion" => "Completado",
"Closing" => "Cerrado", 
"Execution" => "Ejecucion",
"Deleting" => "Borrado",
"Material costs" => "Costo de Materiales",
"Tool costs" => "Costo de Herramientas", 
"Service Costs" => "Costo de Servicios",
"Dates" => "Fechas",
"Date" => "Fecha",
"Final balance" => "Balance Final", 
"none" => "ninguna",
"high" => "alta",
"immediate" => "immediato",



/******************************/
/**           Asset          **/
/******************************/

"Latitude" => "Latitud",
"Longitude" => "Longitud",
"Class" => "Clase",
"Direct Costs (YTD)" => "Costes directos (año econ.)",
"Downtime Costs (YTD)" => "Costes de Inactividad (año econ.)",
"Material Costs (YTD)" => "Costes de Materiales (año econ.)",
"Labor Costs (YTD)" => "Costes Mano de Obra (año econ.)",
"Tool Costs (YTD)" => "Costes de Herramientas (año econ.)",
"Service Costs (YTD)" => "Costes de Servicio (año econ.)",
"Indirect Costs (YTD)" => "Costes Indirectos (año econ.)",
"Total Costs (YTD)" => "Costes Totales (año econ.)",
"Total costs (YTD)" => "Costes totales (año econ.)", 
"Budget (YTD)" => "Presupuesto anual",
"Balance" => "Balance",
"Maintenance Costs" => "Costo de Mantenimiento",
"Replacement Cost" => "Costo de Sustitucion",
"Replacement cost" => "Costo de Sustitucion",
"Replacement Value" => "Valor de Sustitucion",
"Maintenance Cost/Replacement Value" => "Costo de Mantenimiento/Valor de Sustitucion",
"Downtime Hourly Cost" => "Costo por hora de Inactividad",
"Reservation Properties" => "Reserva Propiedades ",
"Bookable" => "Reservable",
"this asset is bookable" => "Este activo es reservable",
"Minimum hours" => "Horas minimas",
"Maximum hours" => "Horas maximas",
"Bookable interval" => "Intervalo Reservable (hora)",
"Parents" => "Padres",
"Status" => "Estado",
"Status name" => "Nombre estado",
"Status Name" => "Nombre Estado",



"Loading selector..." => "Cargando selector...",


/******************************/
/**   Periodic Maintenances  **/
/******************************/

"Periodic Manteinances" => "Manutencion Periodica",//Maintenances
"Master" => "Principal",
"Operation" => "Operacion",
"Ending Date" => "Fecha final",
"Fixed or floating" => "Fijo o flotante",
"fixed" => "Fijo",
"floating" => "Flotante",
"Start date type" => "Tipo fecha inicio",
"Duration" => "Duracion",
"Hours" => "Horas",
"Days"=>"Dias",
"Minutes"=>"Minutos",
"Forewarning"=>"Preaviso",
"At time" => "En hora",
"Weekly Exception"=>"Excepcion semanal",
"Rules type" => "Tipo de normas",
"Holiday Exception" => "Excepcion Vacaciones",
"Rules exception group" => "Grupo Normas de excepcion",
"Rules type exception group"=>"Grupo tipo de normas de excepcion",
"postpone" => "pospone",
"predate" => "anticipa",
"exclude" => "excluye",
"normal" => "normal",
"multiple" => "multiple",


/******************************/
/**       On Condition       **/
/******************************/

"Conditions" => "Condiciones",
"Code meter" => "Codigo Medidor",
"Value Reference" => "Valor de Referencia",
"Run operation" => "Operacion en curso",
"List of operation available" => "Lista de operaciones disponibles",
"List" => "Lista",
"Media" => "Media",
"Range" => "Intervalo",
"Every" => "Todo",
"Function" => "Funcion",
"Operator" => "Operador",
"Equal" => "Igual",
"Not Equal" => "Distinto",
"Greater" => "Mayor que",
"Greater than or equal to" => "Mayor que o igual a",
"Less" => "Menor",
"Less than or equal to" => "Menor que o igual a",
"Number of meter reading for average"=> "Media del numero de lectura del contador",

/******************************/
/**          Meters          **/
/******************************/

"Reading Date" => "Fecha de Lectura",
"Reading history" => "Historial de Lectura",
"Value" => "Valor",
"Meter Type" => "Tipo de Contador",

/******************************/
/**           Tasks          **/
/******************************/

"Sequence" => "Secuencia",
"Exception Allow" => "Excepcion permitida",
"Exception Deny" => "Excepcion denegada",
"Workorders" => "Ordenes de Trabajo",

/******************************/
/**       Inventory          **/
/******************************/

"Quantity" => "Cantidad",
"Unit Price" => "Precio Unitario",
"Lead Time (in days)" => "Tiempo de ejecucion (en dias)",
"Prices" => "Precios",
"Unit price" => "Precio unitario",
"Average Price"=>"Precio Medio",
"Standard Price" => "Precio Estandar",
"Last Price" => "Precio Ultimo",

/******************************/
/**          Labors          **/
/******************************/

"Standard Fee" => "Tarifa Estandar",
"Overtime fee" => "Tarifa Extraordinaria",
"Holiday fee" => "Tarifa Vacacional",
"Overnight fee" => "Tarifa nocturna",
"Overtime" => "Extraordinaria",
"Holiday" => "Festivo",
"Overnight" => "Nocturno",
"Properties" => "Propiedades",


/******************************/
/**          Vendors         **/
/******************************/

"Company" => "Compañia",
"Address" => "Direccion",
"Contact Information" => "Informacion de Contacto",


/******************************/
/**      Purchase Orders     **/
/******************************/

"Workflow phase" => "Fase Flujo de Trabajo",
"Phase name" => "Nombre Fase",


/******************************/
/**         Contracts        **/
/******************************/

"Expiring" => "Expirado",
"Forewarning" => "Preaviso",
"Forewarning Days" => "Dias de Preaviso",
"Terms and Conditions" => "Terminos y Condiciones",
"Contract Type" => "Tipo de Contrato",
"Type" => "Tipo",
"Warranty" => "Garantia",
"Lease" => "Alquiler",
"Rental" => "Renta",
"Purchase" => "Compra",
"Service" => "Servicio",
"Installment" => "Cuota",
"Licence Contract" => "Contrato de Licencia",
"Total Amount" => "Cantidad Total",
"yes" => "sì",

/******************************/
/**       Failure Code       **/
/******************************/

"Mode" => "Modo",
"Occurence (1-10)"=>"Incidente (1-10)",
"Detection"=> "Deteccion",
"Severity" => "Grave",
"Occurrence"=>"Incidente",
"Detection"=> "Deteccion",
"Cause (Failure Mechanism)" => "Causa (Fallo de Mecanismo)",
"Failure Effects" => "Efectos del fallo",
"Remarks/Recommended Actions" => "Observaciones/Soluciones Recomendadas",

/******************************/
/**         Documenti        **/
/******************************/

"Author" => "Autor",
"Version" => "Version",
"Attach name" => "Nombre Adjunto",

/******************************/
/**       Schedulazione      **/
/******************************/

"Minute"=> "Minuto",
"Hour"=>"Hora",
"Month"=> "Mes",
"N° Times"=>"N° Veces",
"N° Times Executed" => "N° Veces Ejecutado",
"Day of Month" => "Dia del Mes",
"Properties" => "Propiedades",

/******************************/
/**          Actions         **/
/******************************/

"Action" => "Accion",

/******************************/
/**        Reservations      **/
/******************************/

"Ciclicality" => "Ciclicidad",
"Cyclicality" => "Ciclicidad",
"Alarm" => "Alarma",
"Reserve till" => "Reservado hasta",
"Alarm E-mail" => "Alarma E-mail",
"Min Hrs" => "Min Horas",
"Max Hrs" => "Max Horas",
"Interval" => "Intervalo",
"Since" => "Desde" ,
"Till" => "Hasta",
"Alarm in advance" => "Anticipo de Alarma",
"one hour" => "una hora",
"one day" => "un dia",
"two days" => "dos dias",
"Constraints" => "Vinculos",
"Minimum reservation hours" => "Horas minimas reservadas",
"Maximum reservation hours" => "Horas maximas reservadas",
"Reservation interval" => "Intervalo de Reserva",
"Guests" => "Invitados",
"First and Last Name" => "Nombre y Apellido",

/******************************/
/**        User Profile      **/
/******************************/

"First name" => "Nombre",
"Last name" => "Apellido",
"Gender" => "Sexo",
"Language" => "Idioma",
"Italian" => "Italiano",
"English" => "Ingles",
"French" => "Frances",
"Spanish" => "Español",
"Change Password" => "Cambia Contraseña",
"Old Password" => "Antigua Contraseña",
"Repeat Password" => "Repetir Contraseña",

/******************************/
/**            Users         **/
/******************************/

"First and Last name" => "Nombre y Apellido",
"User registration" => "Registro de Usuario",
"Pwd registration" => "Registro de Contraseña",
"Username expiration" => "Nombre de usuario expirado",
"Password expiration" => "Contraseña expirada",
"User Information" => "Informacion Usuario",
"Displayed name" => "Nombre visualizado",
"System Information" => "Informacion del Sistema",
"User level" => "Nivel de Usuario",
"Registration Data" => "Datos de Registro",
"Username registration" => "Registro de Usuario",
"Password registration" => "Registro de Contraseña",

/******************************/
/**        Selectors         **/
/******************************/

"Label" => "Etiqueta",
"New type" => "Nuevo tipo",
"use as a new type" => "usar como nuevo tipo",

/******************************/
/**           News           **/
/******************************/

"Creation Date" => "Fecha de Creacion"
);