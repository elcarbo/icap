<?php

defined('BASE_PATH_LIB')
    || define('BASE_PATH_LIB', realpath(dirname(__FILE__) . '/'));
ini_set("display_errors","On");

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/./application'));
error_reporting(E_ALL);

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

defined('SCRIPTS_PATH_DEFAULT')
    || define('SCRIPTS_PATH_DEFAULT', APPLICATION_PATH . "/../defaultScripts");


/*
 * File che richiamato da crontab, 
 * avvia i file previsti
 */

date_default_timezone_set('Europe/Paris');
set_include_path(implode(PATH_SEPARATOR, 
        array(
            realpath(BASE_PATH_LIB.'/library'),
            get_include_path()
        )
    )
);

/** Zend_Application */

require_once 'Zend/Application.php';
require_once('library/phpExcel/PHPExcel.php');

// Create application, bootstrap, and run
new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

new Zend_Application_Module_Autoloader(array(
    'namespace' => 'Mainsim_',
    'basePath'  => APPLICATION_PATH .'/modules/default',
    'resourceTypes' => array ()));

new Zend_Application_Module_Autoloader(array(
    'namespace' => 'MainsimAdmin_',
    'basePath'  => APPLICATION_PATH .'/modules/admin',
    'resourceTypes' => array ()));

//database configuration
$config = new Zend_Config_ini(APPLICATION_PATH.'/configs/application.ini','production'); 
if($config->multiDatabase == 'On' && !empty($argv[1])){        
	$host = $argv[1];
	$info = Mainsim_Model_Utilities::getInstallationInfo($config->database,$host);

	Zend_Registry::set('db', $info['database']);     
	Zend_Registry::set('attachmentsFolder',$info['attachment_path']);			
}
else {
    Zend_Registry::set('attachmentsFolder',$config->attachmentsFolder);			
    Zend_Registry::set('db', $config->database); 
}       

        $db = Zend_Db::factory(Zend_Registry::get('db'));
        $q = new Zend_Db_Select($db);        
        $resMain = $q->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
            ->join(array("s" => "t_systems"), "c.f_id = s.f_code",array())
            ->where("c.f_type = 'SYSTEMS'")->where("c.f_category = 'SETTING'")->query()->fetchAll();               
        $main = array();
        array_walk($resMain, function($el) use(&$main){
            $main[$el['f_title']] = $el['f_description'];
        });              
        Zend_Registry::set('settings',$main);
        
        
// Define scripts path
        
define('PROJECT_NAME',  isset($main['PROJECT_NAME'])?$main['PROJECT_NAME']:'');        
defined('SCRIPTS_PATH')
    || define('SCRIPTS_PATH', APPLICATION_PATH . "/../scripts/" . PROJECT_NAME . '/');        
        
//include BASE_PATH_LIB.'/importer/import.php';
require_once APPLICATION_PATH.'/configs/constants.php';
require_once APPLICATION_PATH.'/configs/script_special_functions.php';
require_once APPLICATION_PATH.'/configs/errorHandler.php';

set_error_handler('mainsimErrorHandler');

$read_class = new Mainsim_Model_PMReader(true);
$read_oc = new Mainsim_Model_OCReader();
$cron = new Mainsim_Model_Cron();
$dataManager = new MainsimAdmin_Model_Datamanager();

$read_oc->onConditions();
$read_class->readPeriodics();
$cron->executeCron();
$cron->executeCronSch();

// custom cron scripts for multiproject
if($config->multiDatabase == 'On'){
    $aux = explode(".", $argv[1]);
    if(file_exists(APPLICATION_PATH . '/../scripts/' . $aux[0] . '/cron.php')){
		$db = Zend_Db::factory(Zend_Registry::get('db'));
        include(APPLICATION_PATH . '/../scripts/' . $aux[0] . '/cron.php'); 
    }
}
// custom cron scripts for single installation
else{
    if(file_exists($config->scriptsFolder .'/cron.php')){
        $db = Zend_Db::factory($config->database);
        include($config->scriptsFolder .'/cron.php');
    }
}

unset($read_class);
unset($cron);
$dataManager->cronImport();




