<?php
// init
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));
/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);


$dbParams = array( //PRODUZIONE
        'adapter'=>'PDO_MYSQL',
        'params'=>array(
                'host'=>'95.110.175.149',
                'dbname'=>'mainsim3_icapsirabdm',
                'username'=>'root',
                'password'=>'vxpUVjPnHuZriwV3',
        )
);

//$dbParams = array(  //STAGING
//        'adapter'=>'PDO_MYSQL',
//        'params'=>array(
//                'host'=>'10.0.245.220',
//                'dbname'=>'mainsim3_icapsirabdm',
//                'username'=>'host',
//                'password'=>'SetRu7res+3b',
//        )
//);

$cd = new Zend_Config($dbParams);
Zend_Registry::set('db', $cd);
$db = Zend_Db::factory($cd);

$userInfo = Zend_Auth::getInstance()->getIdentity();
$userId = $userInfo->f_id;
$sel = new Zend_DB_Select($db);


$userSel = $sel->from(array('sw' => 't_selector_ware'), ['f_selector_id'])
        ->join(array('sel' => 't_selectors'),"sel.f_code = sw.f_selector_id",[])
        ->where("sw.f_ware_id = $userId")
        ->where("sel.f_type_id = 3")
        ->query()->fetchAll();

$vSelectors = array_column($userSel, 'f_selector_id');
$sel->reset();
if(empty($vSelectors)){
    $res = $sel->from('t_creation_date', ['f_id', 'f_title'])
        ->where("f_category LIKE 'VENDOR' AND f_phase_id = 1")->query()->fetchAll();
}else{
    $res = $sel->from(array('cd' => 't_creation_date'), ['f_id', 'f_title']) //vendor
        ->join(array('sw' => 't_selector_ware'),"sw.f_ware_id = cd.f_id",[]) //selector  - vendor
        ->join(array('sel' => 't_selectors'),"sel.f_code = sw.f_selector_id",[]) //selector 3
        ->where("cd.f_category LIKE 'VENDOR' AND cd.f_phase_id = 1")
        ->where("sel.f_type_id = 3")
        ->where("sw.f_selector_id IN (". implode(',',$vSelectors).")")
        ->query()->fetchAll();    
}

$val = json_encode($res);

$content = <<<EOD
	<html>
		<head>
                    <style>
                            body {
                                    font-size: 12px;
                                    font-family: arial;
                            }
                            select, input {
                                    padding: 3px 10px 3px 10px;	
                                    height: 30px;
                                    width: 167px;
                            }
                            input[type="button"] {
                                background-color: #a1c24d;
                                color:white;
                                font-size: 14px;
                                width: 60px;
                                margin-left: 5px;
                            }
                            .container {
                                display: inline-block;
                            } 
                            table {
                                border-spacing: 5px 5px;
                                border-collapse: separate;
                                border:"0"
                            }
                    </style>
        	<script>
        		window.onload = function() {
        			let f = document.querySelector('#fornitori')
        			let flist = {$val}
        			for(let i in flist) {
        				let opf = new Option()
        				opf.text = flist[i].f_title	
        				opf.value = flist[i].f_id
        				f.options[f.options.length] = opf
        				if(flist[i].f_title.match(/BRT/)) {
        					opf.selected = true
        				}
        			}
                                
                                var date = new Date();
                                var firstDayOfMonth = new Date(date.getFullYear(), date.getMonth(), 1);
                                var day = firstDayOfMonth.getDate();
                                var month = firstDayOfMonth.getMonth() + 1;
                                var year = firstDayOfMonth.getFullYear();
                                if (month < 10) month = "0" + month;
                                if (day < 10) day = "0" + day;
                                var since = year + "-" + month + "-" + day;       
                                document.getElementById("datesince").value = since;                                
                                
                                var day = date.getDate();
                                var month = date.getMonth() + 1;
                                var year = date.getFullYear();
                                if (month < 10) month = "0" + month;
                                if (day < 10) day = "0" + day;
                                var today = year + "-" + month + "-" + day;       
                                document.getElementById("dateto").value = today;
        		}

                        
        		let viewPdf = function() {
                                var extra = getIframeUrlParams();
                                if(extra != '') extra = "&"+extra;
        			var win = window.open("../../pdf-creator/print-pdf/f_code/0/mockup/1/script_name/report_manodopera?datesince=" 
                                    + document.querySelector('#datesince').value + "&dateto=" + document.querySelector('#dateto').value + '&fornitore=' + document.querySelector('#fornitori').options[document.querySelector('#fornitori').selectedIndex].value+extra, "_blank");
  						win.focus();
        		}
                                
                        let getIframeUrlParams = function() {
                                var re = document.URL.match(/^(.*)\?(.*)/);
                                if(re && re.length == 3)
                                    return re[2];
                                return "";
                        }
        	</script>
		</head>
		<body>	
                    <div class="container">
                        <table>
                            <tr>
                                        <td>Da</td>
                                </tr>
                                <tr>
                                        <td><input type="date" id="datesince"></td>
                                </tr>
                                <tr>
                            <tr>
                                        <td>A</td>
                                </tr>
                                <tr>
                                        <td><input type="date" id="dateto"></td>
                                </tr>
                                <tr>
                            <tr>
                                        <td>Fornitore</td>
                                </tr>
                                <tr>
                                        <td ><select id="fornitori"></select></td>
                                        <td ><input type="button" onclick="viewPdf()" value="Crea"></td>
                                </tr>
                            <tr>
                            </tr>
                        </table>
                    </div>
		</body>
	</htmL>
EOD;

print $content;

?> 
