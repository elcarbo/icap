<?php
class C extends PDF_MC_Table{
	function Header(){
		$installation = new Mainsim_Model_Mainpage();
		$settings = $installation->getSettings();
		$this->SetTextColor(0,0,0);
		$this->SetFont('Arial','B',20);
                $this->SetXY(5,10);
                $this->Image(APPLICATION_PATH."/../public/msim_images/icapsirabdm/_pdf/logo.png",159,9,45,17);
		$this->SetXY(5,6);
		$this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
		$this->rect(5, 27, 198, 1.6, "F");
		$this->SetY(30);
	}

	function Footer(){
            $session_1 = $_SESSION['Zend_Auth'];
            $user_session = $session_1['storage']->f_displayedname;
            $this->SetXY(5,285);
            $this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
            $this->rect(5, 283, 198, 1.6, "F");
            $this->Ln(20);
            $this->SetXY(5,285);
            $this->SetFont('Arial','B',6);
            $this->Cell(100,5,"mainsim"." - what maintenance can be - www.mainsim.com ");
            $this->SetXY(100,285);
            $this->AliasNbPages();
            $this->Cell(102,5,$GLOBALS['trad2']->_('Page') . ' '.$this->PageNo().' - ' . $GLOBALS['trad2']->_('Printed by') . ' ' . $user_session . ' - ' . date('d-m-Y'),0,0,'R');
	}
}
class translator
{
	private $traductor;
	private $voc;
	public function __construct() 
	{
		$this->traductor = new Mainsim_Model_Translate();
		$this->voc = $this->traductor->getLanguage($this->traductor->getLang());
	}	
	public function translate($string){
		$res = $string;
		if($this->traductor->_($string)!=null) $res = $this->traductor->_($string);
		return $res;
	}
}

 $pdf = new C('P','mm','A4');
 /*per conoscere la lingua*/

$currency_sign = $_SESSION["CURRENCY_SIGN"];
 /*OTTENERE IL TRADUTTORE*/
 
 $traductor = new Mainsim_Model_Translate();
 $voc = $traductor->getLanguage($traductor->getLang());

$GLOBALS['trad'] = $voc;
$GLOBALS['trad2'] = $traductor;

$mo_t = new translator();
 
$pdf->AddFont('Arial', '', 'OpenSans-Light.php');
$pdf->AddFont('Arial', 'B', 'OpenSans-Regular.php');
$session = $_SESSION['Zend_Auth'];
$language_session = $session['storage']->f_language;


$select->reset();
$language_query=$select->from(array('t1' => 't_localization'),array("f_datetime"))->where('t1.f_id=?',$language_session)->query()->fetch();
$correct_sint_lang = str_replace("{", "", $language_query);
$correct_sint_lang_final = str_replace("}", "", $correct_sint_lang);
$date_string_lang = $correct_sint_lang_final['f_datetime'];
$select->reset();


$f_transact_id= $_GET['f_transact_id'];

$data = $select->from(array('t_excel_import'))
 ->where("t_excel_import.f_transact_id = ".$f_transact_id)->query()->fetch();
$select->reset();

$logs = json_decode($data['f_logs'],1);
setlocale(LC_TIME, 'ita');

 $pdf->SetLineWidth(0.1);
 $pdf->AddPage();
 $pdf->SetAutoPageBreak(1,12);

  //VARIABILI DI RIFERIMENTO
 $marginX = 5;
 $first_width = 25;
 $X = $marginX;
 
 //TITOLO
 $Y=$pdf->GetY()+2;
 $pdf->SetXY(5,$Y);
 $pdf->SetFillColor(204,229,255);
 $pdf->SetLineWidth(0.3);
 $pdf->Rect(5,$Y,198,6,'F');
 $pdf->SetFont('Arial','B',12);	
 
 $pdf->Cell(198,6,"DETTAGLI IMPORT DDT di ".strftime("%A %d/%m/%Y %H:%M",time()),1,'','C');
 $Y=$pdf->GetY()+6;

 $pdf->SetFont('Arial','I',8);
$pdf->SetAligns(array('C','C','C','C'));
$pdf->SetWidths(array(49.5,49.5,49.5,49.5)); //195?
$pdf->SetXY(5,$Y);
//$pdf->Row(array('OdL Aggiornati','OdL Non trovati','OdL Bloccati'));
$pdf->SetX(5);
//$pdf->SetFont('Arial','B',8);

//$max = max([count($logs['updated']),count($logs['not_found']),count($logs['locked'])]);
$max = count($logs['all']);
$slice = 48;
for($i=0; $i < $max; $i+=$slice){
    $pdf->SetAligns(array('C','C','C','C'));
    $a = array_slice($logs['all'], $i, $slice); $b = array_slice($logs['updated'], $i, $slice);  $c = array_slice($logs['not_found'], $i, $slice); $d = array_slice($logs['locked'], $i, $slice); 
    $a = (empty($a))?"":implode("\r\n",$a); $b = (empty($b))?"":implode("\r\n",$b); $c = (empty($c))?"":implode("\r\n",$c); $d = (empty($d))?"":implode("\r\n",$d);
    $pdf->SetFont('Arial','I',8);
    $pdf->SetX(5);
    $pdf->Row(array('OdL Analizzati','OdL Aggiornati','OdL Non trovati','OdL Bloccati'));
    $pdf->SetFont('Arial','B',8);
    $pdf->SetX(5);
    $pdf->Row(array($a,$b,$c,$d));
    if($i+$slice < $max) $pdf->AddPage();
}


