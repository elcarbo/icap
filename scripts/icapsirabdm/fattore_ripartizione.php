<?php

if (!function_exists("sortByOrder")) {
    function sortByOrder($a, $b) {
        return $b["c"] - $a["c"];
    }
}

try{
if (isset($params['f_type_id']) && (in_array($params['f_type_id'],[3,19,1,4]))) {
    //controlo che sia associato un asset( e quindi di conseguenza un CDC!! ) o una COMMESSA!!.
    $check_asset = Mainsim_Model_Utilities::getCross('t_ware_wo',$f_code,'f_wo_id',array(),'f_ware_id','t_wares',"1",true);
    $check_commessa = Mainsim_Model_Utilities::getCross('t_ware_wo',$f_code,'f_wo_id',array(),'f_ware_id','t_wares',"26",true);
    if(empty($check_asset) && empty($check_commessa)){
        die("E' obbligatorio associare un asset o una commessa");
    }
    else if(!empty($check_asset) && !empty($check_commessa)){
        die("Non è possibile associare sia asset che commesse");
    }
    
    $selOp = new Zend_Db_Select($this->db);
    $selOp->reset();
    $resOp = $selOp->from(["t1" => "t_ware_wo"], ["f_ware_id"])
                    ->join(["t2a" => "t_creation_date"], "t1.f_ware_id = t2a.f_id", ["f_id as f_code_asset", "f_title as f_title_asset", "fc_hierarchy_codes"])
                    ->where("t1.f_wo_id = ?", $f_code)
                    ->where("t2a.f_category ='ASSET'")
                    ->query()->fetchAll();
    $total_assets = count($resOp);
    $this->db->update("t_custom_fields", ["num_asset" => $total_assets], "f_code = " . $f_code);
    $codes_asset = [];
    $count_codes = [];
    $count_codes2 = [];
    foreach ($resOp as $key => $value) {
        $explosion = explode(",", $value["fc_hierarchy_codes"]);
        if (count($explosion) == 1) { // è root, prendo il suo f_code
            $codes_asset[] = $value["f_code_asset"];
            $count_codes[$value["f_code_asset"]] = 0;
        } else { // Ho gerarchia, il penultimo valore è il padre
            $codes_asset[] = $explosion[count($explosion) - 2];
            $count_codes[$explosion[count($explosion) - 2]] = 0;
        }
    }

    foreach ($codes_asset as $key => $value) {
        $count_codes[$value]++;
        $count_codes2[$value] = $count_codes[$value] / count($codes_asset);
    }

//    foreach ($count_codes as $key => $value) {
//        $count_codes2[$key] = $value / count($codes_asset);
//    }
    if (!empty($codes_asset)) {
        $selOp->reset();
        $resOp = $selOp->from(["t1" => "t_wares_relations"], ["f_code_ware_slave as f", "f_code_ware_master as x"])
                        ->join(["t2" => "t_creation_date"], "t1.f_code_ware_slave = t2.f_id", ["f_title as t"])
                        ->where("t1.f_code_ware_master in (?)", array_keys($count_codes2))
                        ->where("t2.f_category = 'CDC'")
                        ->query()->fetchAll();
    }
    $tot = 0;
    foreach ($resOp as $key => $value) {
        //$resOp[$key]["cdc_value"] = $count_codes2[$value["f_code_cdc"]];
        $resOp[$key]["v"] = floor($count_codes2[$value["x"]]*100)/100;
        $resOp[$key]["c"] = $count_codes[$value["x"]];
        $tot += $resOp[$key]["v"];
    }
    usort($resOp, 'sortByOrder');
    // Arrotondamenti e rimuovo cdcc da array
    $tot = (1-$tot);
    foreach ($resOp as $key=>$value) {
        if ($tot > 0.001) {
            $resOp[$key]['v'] += 0.01;
            $tot-=0.01;
        }
        unset($resOp[$key]['c']);
    }
    $this->db->update("t_custom_fields", ["fattore_ripartizione" => json_encode($resOp)], "f_code = " . $f_code);
    // NICO 09/07/19: controllo che dopo tutti gli arrotondamenti sia ancora giusto il conto e che faccia 1
    $fatt_ripartiz = json_decode(json_encode($resOp),1);
    $somma = 0;
    foreach ($fatt_ripartiz as $key=>$value) {
        $somma+=$value["v"];
    }
    if($total_assets > 0 && abs($somma-1)>0.001) {
        //Scrivo nei log che c'è un errore
        $this->db->insert("t_logs", ["f_timestamp"=>$time, "f_log"=>"Errore nel calcolo del fattore di ripartizione per odl ".$f_code, "f_type_log"=>"679"]);
    }

    // Aggiusto CDC
    $codes = [];
    $labels = [];
    foreach($resOp as $key=>$value) {
        $codes[]=$value['f'];
        $labels[]=$value['t'];
    }
    $cdc = [];
    $cdc["codes"] = $codes;
    $cdc["labels"] = $labels;   
    $this->db->update("t_custom_fields", ["cdc" => json_encode($cdc)], "f_code = " . $f_code);
    //die(var_dump(json_encode($f_code)));
    $selOp->reset();
    $cross_cdc = $selOp->from(["t1"=>"t_ware_wo"], ["f_id"])
                        ->join (["t2"=>"t_creation_date"], "t1.f_ware_id = t2.f_id", [])
                        ->where("t1.f_wo_id = ?", $f_code)
                        ->where("t2.f_category = 'CDC'")
                        ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

    if (!empty($cross_cdc)) {
        $this->db->delete("t_ware_wo", "f_id in (".implode(",",$cross_cdc).")");
    }
    
    // Inserisco cross con cdc
    foreach ($cdc['codes'] as $key=>$value) {
       // die(var_dump($value));
        $this->db->insert("t_ware_wo", ["f_ware_id"=>$value, "f_wo_id"=>$f_code, "f_timestamp"=>$time]);
    }
}

} catch (Exception $e) {
    die($e->getMessage());
}