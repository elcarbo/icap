<?php
if(!function_exists("evalScript")){
function evalScript($db,$f_code,$script_name)
{
    try {
        require(APPLICATION_PATH . '/../scripts/icapsirabdm/'.$script_name);
    } catch (Exception $ex) {
        $message = '[changePhaseChildren][evalScript('.$f_code.','.$script_name.'): '.$ex->getMessage();
        $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>$message,'f_type_log'=>88));
       return false;
    }

    return true;
}

function getWoChildren($select,$father_code){
    $select -> reset();
    $result = $select->from(array("wp"=>"t_workorders_parent"),[])
        ->join(array("cd"=>"t_creation_date"),"cd.f_id = wp.f_parent_code",['f_phase_id']) // wo padre
        ->join(array("wfph"=>"t_wf_phases"),"wfph.f_wf_id = cd.f_wf_id AND wfph.f_number = cd.f_phase_id",['f_name']) // f_name fase
        ->join(array("wo"=>"t_workorders"),"wo.f_code = wp.f_code",['f_code as child_f_code']) // wo figli
        ->join(array("cd2"=>"t_creation_date"),"cd2.f_id = wo.f_code",['f_phase_id as child_f_phase_id']) // wo figli
        ->where("wp.f_parent_code = $father_code") // wo padre
        ->where("wo.f_type_id IN (8,13)") // wo figli emergency o reservation
        ->where("cd2.f_phase_id NOT IN (12)") // wo figli non cancellati
        ->query()->fetchAll();
    //        $sql = $selOp->__toString();
    //        die($sql);
	return $result;		
}

}

$selOp = new Zend_Db_Select($this->db);

$multi_phase_batch = (!empty($_POST['f_codes']) && !empty($f_code))?true:false;

if($multi_phase_batch){ // se multi phase batch
    if(isset($olds)){
        $params = array();
        $params['f_type_id'] = $olds['old_table']['f_type_id'];
        $params['fc_wo_ending_date'] = $olds['old_table']['fc_wo_ending_date'];
        //$params['fc_editor_user_name'] = $creationUpd['fc_editor_user_name'];
        $params['f_phase_id'] = $creationUpd['f_phase_id'];
        //$params['multiphasebatch'] = true;
    }
}

$from_to['Trasmesso a ditta esterna'] = ["1_3","2_3"];
$from_to['Lavoro terminato'] = ["5_6"];

//Verifica correttezza passaggio di fase, e propagazione ad eventuali figli
if(empty($params['not_execute_exit_123']) && in_array($params['f_type_id'], [1,4])){
    $result = getWoChildren($selOp,$f_code);

    $f_number = (!empty($olds['old_creation']['f_phase_id']))?$olds['old_creation']['f_phase_id']:-1;
    $phase_exit = (!empty($result))?$result[0]['f_phase_id']:$params['f_phase_id'];
    $wcf_phase_id = (!empty($result))?array_column($result,"child_f_phase_id"):[-1];
    
    //var_dump($result,$f_number,$phase_exit,$wcf_phase_id); die;
    //se il wo non ha figli
    if(empty($result)){
        if(in_array($f_number."_".$phase_exit, $from_to['Lavoro terminato'])){ //se passaggio di fase: 'Lavoro terminato'
            if(empty($params['fc_wo_ending_date'])){
                    $params['fc_wo_ending_date'] = time();
                    $this->db->update('t_workorders',['fc_wo_ending_date'=> $params['fc_wo_ending_date']], 'f_code = ' . $f_code);
            }
        }		
    }
    else if($f_number !== $phase_exit){
        //se il wo ha dei figli, ha effettuato un passaggio di fase e tutti i suoi figli hanno la sua fase di partenza 
        //oppure il passaggio di fase è 'Trasmesso a ditta esterna' e i figli sono tutti in fase 1 porto anch' essi alla nuova fase
        if(empty(array_diff($wcf_phase_id,[$f_number])) || 
                (in_array($f_number."_".$phase_exit, $from_to['Trasmesso a ditta esterna']) && empty(array_diff($wcf_phase_id,[1]))) )
        {	
            //$wcfcodes = "(".implode(",", array_column($result,"child_f_code")).")";
            $children_codes = array_column($result,"child_f_code");
            $wcfcodes = implode(",", $children_codes);

            $mmwo = new Mainsim_Model_MainsimObject($this->db);
            $mmwo->type = 't_workorders';
            $workflow_id = ($params['f_type_id'] == 1)?2:34;
            //$saltascripts = ($phase_exit != 3) ?true:false;
            $saltascripts = true;
            //print_r([$wcfcodes, $workflow_id, $phase_exit,$wcf_phase_id,$f_number,$saltascripts]); die("wwww");
            $changeresult = $mmwo->changePhase($wcfcodes, $workflow_id, $phase_exit, 'mdl_wo_tg',false,$saltascripts);

            
            if(in_array($f_number."_".$phase_exit, $from_to['Trasmesso a ditta esterna'])){ //se passaggio di fase: 'Trasmesso a ditta esterna'
                foreach ($children_codes as $value) {
                    evalScript($this->db,$value,'email_ditta_esterna.php');
                }
            }
            else if(in_array($f_number."_".$phase_exit, $from_to['Lavoro terminato'])){ //se passaggio di fase: 'Lavoro terminato'
                if(empty($params['fc_wo_ending_date'])){
                    $params['fc_wo_ending_date'] = time();
                    $this->db->update('t_workorders',['fc_wo_ending_date'=> $params['fc_wo_ending_date']], 'f_code = ' . $f_code);
                }

                foreach ($children_codes as $value) {
                    $this->db->update('t_workorders',['fc_wo_ending_date'=> $params['fc_wo_ending_date']], 'f_code = ' . $value);
                }
            }
        }
        else {
            $msg = "Impossibile effettuare il passaggio di fase in '".$result[0]['f_name']."'.";
            $msg .= "<br>Portare prima tutti gli ordini di lavoro figli nella stessa fase del padre";
            die($msg);
        }
		
		
    }
    
    evalScript($this->db,$f_code,'clearLaborCross.php');
}
//Aggiornamento fase di eventuale wo padre
//se il wo di tipo 8 o 13 ha un padre, aggiorno la fase del padre alla minore tra la sua e quella dei suoi fratelli
else if(empty($params['not_execute_exit_123']) && in_array($params['f_type_id'], [8,13])){    
    $selOp->reset();
    $result = $selOp->from(array("wp"=>"t_workorders_parent"),['f_parent_code'])
        ->join(array("wp2"=>"t_workorders_parent"),"wp2.f_parent_code = wp.f_parent_code",[]) 
        ->join(array("cd"=>"t_creation_date"),"cd.f_id = wp2.f_code",['f_id as child_f_id','f_phase_id as child_f_phase_id']) //fasi figli
        ->join(array("wfph"=>"t_wf_phases"),"wfph.f_wf_id = cd.f_wf_id AND wfph.f_number = cd.f_phase_id",['f_group_id as child_f_group_id']) // 
        ->where("wp.f_code = $f_code") // 
        ->where("wfph.f_group_id NOT IN (7)") // wo figli non cancellati
        ->query()->fetchAll();  
    
    if(!empty($result)){
        $f_number = $olds['old_creation']['f_phase_id'];
        $phase_exit = $params['f_phase_id'];
        if(in_array($f_number."_".$phase_exit, $from_to['Lavoro terminato'])){ //se passaggio di fase: 'Lavoro Terminato'
            if(empty($params['fc_wo_ending_date'])){
                $params['fc_wo_ending_date'] = time();
                $this->db->update('t_workorders',['fc_wo_ending_date'=> $params['fc_wo_ending_date']], 'f_code = ' . $f_code);
            }
            $this->db->update('t_workorders',['fc_wo_ending_date'=> $params['fc_wo_ending_date']], 'f_code = ' . $result[0]['f_parent_code']);
        }
    
        
        //recupero la fase minore tra i figli del wo padre 
        $arr1 = array_column($result,'child_f_phase_id','child_f_id');   
        $phase_exit = min($arr1);  $f_child_id = array_keys($arr1, $phase_exit);
        $arr2 = array_column($result,'child_f_group_id','child_f_id');   $f_group_id = $arr2[$f_child_id[0]];
        $this->db->update('t_creation_date',['f_phase_id'=>$phase_exit], 'f_id = ' . $result[0]['f_parent_code']);
        if(in_array($f_group_id, [6]) && $params['f_type_id'] == 8){
            $woCreator = new Mainsim_Model_Workorders($this->db);
            //var_dump($result[0]['f_parent_code']); die;
            $woCreator->checkPeriodicShift($result[0]['f_parent_code']);
        }
        evalScript($this->db,$result[0]['f_parent_code'],'clearLaborCross.php');
    }
}