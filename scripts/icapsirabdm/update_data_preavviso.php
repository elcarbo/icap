<?php
function checkIfExecute($scriptName,$db,&$log)
{
    // controllo che lo script non sia già stato eseguito nell'ultima ora
    $executeScript = false;
    $selectAux = new Zend_Db_Select($db);
    $selectAux->from('t_scripts', array('f_aux'))
                    ->where("f_name = '".$scriptName."'");
    $resAux = $selectAux->query()->fetchAll();

    $aux = json_decode($resAux[0]['f_aux']);
    $time = date('d-m-Y H:i');
    $timestamp = strtotime($time);
    if($aux == null || strtotime($time) > strtotime($aux->next)){
            $executeScript = true;
            $NewDate = strtotime("+15 minutes");
            $log = array('next' => date("d-m-Y H:i", $NewDate));
    }
 
    return $executeScript; 
}


$execute = checkIfExecute("update_data_preavviso",$this->db,$log);

if($execute){
    try {
        $selOp = new Zend_Db_Select($this->db);
        $selOp->reset();
        $resOp = $selOp->from(["t1" => "t_creation_date"], ["f_id"]) 
                        ->join(["t2"=> "t_workorders"], "t1.f_id = t2.f_code", ["fc_pm_next_due_date", "fc_pm_forewarning"])
                        ->where("t2.f_type_id in ('3','19')")
                        ->where("t1.f_phase_id in ('1','2','5','6')")
                        ->order("t1.f_id desc")
                        ->query()->fetchAll();
        //die(json_encode($resOp));
        $data_preavviso = 0;
        foreach($resOp as $key=>$value) {
            $data_preavviso = (intval($value["fc_pm_next_due_date"])-intval($value["fc_pm_forewarning"]));
            $data_preavviso = ($data_preavviso>0)?$data_preavviso:null;
            $this->db->update("t_custom_fields", ["data_preavviso"=>$data_preavviso], "f_code = ".$value["f_id"]);
        }
    } catch (Exception $e) {
        $this->db->insert("t_logs", ["f_timestamp"=>time(), "f_log"=>"Cron error: ".$e->getMessage(), "f_type_log"=>"679"]);
    }
    $this->db->update('t_scripts', array('f_aux' => json_encode($log)), array("f_name = 'update_data_preavviso'"));  
}
