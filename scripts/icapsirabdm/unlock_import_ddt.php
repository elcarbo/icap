<?php
class C extends PDF_MC_Table{
	function Header(){
		$installation = new Mainsim_Model_Mainpage();
		$settings = $installation->getSettings();
		$this->SetTextColor(0,0,0);
		$this->SetFont('Arial','B',20);
                $this->SetXY(5,10);
                $this->Image(APPLICATION_PATH."/../public/msim_images/icapsirabdm/_pdf/logo.png",159,9,45,17);
		$this->SetXY(5,6);
		$this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
		$this->rect(5, 27, 198, 1.6, "F");
		$this->SetY(30);
	}

	function Footer(){
            $session_1 = $_SESSION['Zend_Auth'];
            $user_session = $session_1['storage']->f_displayedname;
            $this->SetXY(5,285);
            $this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
            $this->rect(5, 283, 198, 1.6, "F");
            $this->Ln(20);
            $this->SetXY(5,285);
            $this->SetFont('Arial','B',6);
            $this->Cell(100,5,"mainsim"." - what maintenance can be - www.mainsim.com ");
            $this->SetXY(100,285);
            $this->AliasNbPages();
            $this->Cell(102,5,$GLOBALS['trad2']->_('Page') . ' '.$this->PageNo().' - ' . $GLOBALS['trad2']->_('Printed by') . ' ' . $user_session . ' - ' . date('d-m-Y'),0,0,'R');
	}
}
class translator
{
	private $traductor;
	private $voc;
	public function __construct() 
	{
		$this->traductor = new Mainsim_Model_Translate();
		$this->voc = $this->traductor->getLanguage($this->traductor->getLang());
	}	
	public function translate($string){
		$res = $string;
		if($this->traductor->_($string)!=null) $res = $this->traductor->_($string);
		return $res;
	}
}


$select = new Zend_Db_Select($this->db);
$result = $select->from(array("t1"=>"t_excel_import"),['f_percent','f_timestamp'])
    ->join(array("cd"=>"t_creation_date"),"cd.f_id = t1.f_user_id",['f_title as user'])
    ->where("t1.f_status = 1")
    ->query()->fetchAll();

$this->db->query("UPDATE t_excel_import SET f_status = -1 WHERE f_status = 1");

 $pdf = new C('P','mm','A4');
 /*per conoscere la lingua*/

$currency_sign = $_SESSION["CURRENCY_SIGN"];
 /*OTTENERE IL TRADUTTORE*/
 
 $traductor = new Mainsim_Model_Translate();
 $voc = $traductor->getLanguage($traductor->getLang());

$GLOBALS['trad'] = $voc;
$GLOBALS['trad2'] = $traductor;

$mo_t = new translator();
 
$pdf->AddFont('Arial', '', 'OpenSans-Light.php');
$pdf->AddFont('Arial', 'B', 'OpenSans-Regular.php');
$session = $_SESSION['Zend_Auth'];
$language_session = $session['storage']->f_language;


$select->reset();
$language_query=$select->from(array('t1' => 't_localization'),array("f_datetime"))->where('t1.f_id=?',$language_session)->query()->fetch();
$correct_sint_lang = str_replace("{", "", $language_query);
$correct_sint_lang_final = str_replace("}", "", $correct_sint_lang);
$date_string_lang = $correct_sint_lang_final['f_datetime'];
$select->reset();

$logs = json_decode($data['f_logs'],1);
setlocale(LC_TIME, 'ita');

 $pdf->SetLineWidth(0.1);
 $pdf->AddPage();
 $pdf->SetAutoPageBreak(1,12);

  //VARIABILI DI RIFERIMENTO
 $marginX = 5;
 $first_width = 25;
 $X = $marginX;
 
 //TITOLO
 $Y=$pdf->GetY()+2;
 $pdf->SetXY(5,$Y);
 //$pdf->SetFillColor(204,229,255);
 $pdf->SetLineWidth(0.3);
 //$pdf->Rect(5,$Y,198,5,'F');
 $pdf->SetFont('Arial','I',12);	
 
 if(empty($result)) $pdf->Cell(198,5,"NON CI SONO IMPORT DTT IN CORSO o BLOCCATI",0,'','C');
 else{
    $pdf->Cell(198,5,"IMPORT DDT SBLOCCATI",0,'','C');
    $Y=$pdf->GetY()+8;

    $pdf->SetAligns(array('C','C','C',));
    $widths = array(10,49.5,49.5,49.5);
    $pdf->SetWidths($widths); //195?
    $X = $marginX +(195 - array_sum($widths))/2;
    $pdf->SetXY($X,$Y);
    $pdf->SetFont('Arial','I',9);
    $pdf->Row(array('#','Ultimo Aggiornamento','% Progresso','Utente'));
    $pdf->SetFont('Arial','B',8);
    for($i=0; $i < count($result); $i++){    
    $pdf->SetX($X);
    $pdf->Row(array($i+1,date("d/m/Y, H:i",$result[$i]['f_timestamp']),$result[$i]['f_percent'],$result[$i]['user']));
    }

 }
