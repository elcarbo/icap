<?php
if(!class_exists('C')){
class C extends PDF_MC_Table{
	function Header(){
		$installation = new Mainsim_Model_Mainpage();
		$settings = $installation->getSettings();
		$this->SetTextColor(0,0,0);
		$this->SetFont('Arial','B',20);
		$this->Image(APPLICATION_PATH."/../public/msim_images/default/_pdf/logo.png");
		$this->SetXY(180,13);
		//$this->Cell(100,7,$settings['MAINSIM_PROJECT_NAME'],0,0,'R');
                $this->Image(APPLICATION_PATH."/../public/msim_images/icapsirabdm/_pdf/logo.png",242,9,45,17);
		$this->SetXY(10,15); 
		$this->SetXY(10,6);
		$this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
		$this->rect(10, 27, 276, 1.6, "F");
		$this->SetY(30);
		
	}

	function Footer(){
            if(isset($_SESSION['Zend_Auth'])){
		$session_1 = $_SESSION['Zend_Auth'];
		$user_session = $session_1['storage']->f_displayedname;
            }
            else $user_session = "Mainsim";
            
            $this->SetXY(10,200);
            $this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
            $this->rect(10, 200, 276, 1.6, "F");
            $this->Ln(20);
            $this->SetXY(8.7,203);
            $this->SetFont('Arial','B',6);
            $this->Cell(100,5,"mainsim"." - what maintenance can be - www.mainsim.com ");
            $this->SetXY(185,203);
            $this->AliasNbPages();
	    $this->Cell(100,5,$GLOBALS['trad2']->_('Page') . ' '.$this->PageNo().' - ' . $GLOBALS['trad2']->_('Printed by') . ' ' . $user_session . ' - ' . date('d-m-Y'),0,0,'R');
	}
}

class translator
{
	private $traductor;
	private $voc;
	public function __construct() 
	{
		$this->traductor = new Mainsim_Model_Translate();
		$this->voc = $this->traductor->getLanguage($this->traductor->getLang());
	}	
	public function translate($string){
		$res = $string;
		if($this->traductor->_($string)!=null) $res = $this->traductor->_($string);
		return $res;
	}
}

function getSelectorsInfo($db,$sel_code){
    $select = new Zend_Db_Select($db);
    
    $result = $select->from(array('cd' => 't_creation_date'),['f_title']) //mp - planned generator
    ->where("cd.f_id = $sel_code") 
    ->query()->fetch();  
    
    return $result;
}

function getMPscaduti($db,$selector){
    $select = new Zend_Db_Select($db);
    $now = time();
    $result = $select->from(array('wo' => 't_workorders'),["f_code","data_pianificata","f_end_date","fc_wo_asset","fc_pm_frequency"]) //mp
    ->join(array('sw' => 't_selector_wo'),"sw.f_wo_id = wo.f_code",[]) //mp - selector
    ->join(array('cd' => 't_creation_date'),"cd.f_id = wo.f_code",['f_description']) //mp
    ->join(array('wp' => 't_wf_phases'),"wp.f_number = cd.f_phase_id and wp.f_wf_id = cd.f_wf_id",['f_name as phase_name'])     //mp
    ->join(array('cf' => 't_custom_fields'),"cf.f_code = cd.f_id",['progress_custom','ditta_esterna','cdc','rif_scheda_as400']) //mp
    ->join(array('wr' => 't_wo_relations'),"wr.f_code_wo_slave = wo.f_code",[]) //mp - generatore
    ->join(array('cd2' => 't_creation_date'),"cd2.f_id = wr.f_code_wo_master",['fc_progress as pg_progress']) //generatore
    ->joinLeft(array('ww' => 't_ware_wo'),"ww.f_wo_id = cd.f_id",[]) //mp - asset
    ->joinLeft(array('cd3' => 't_creation_date'),"cd3.f_id = ww.f_ware_id AND cd3.f_category = 'ASSET' AND cd3.f_phase_id = 1",["GROUP_CONCAT(cd3.f_title SEPARATOR ', ') as assets_title"])    //assets
    ->where('wo.f_type_id = 4') //planned
    ->where('cd.f_phase_id IN (1,2,3,4,5)') //fasi
    ->where("cd.fc_hierarchy_selectors LIKE '%" . $selector . "%'") //fc_hierarchy_selectors
    ->where("wo.f_end_date < $now") //data pianificata fine < now
    ->where('wr.f_type_id_wo_master = 3') // generatore
    ->order("f_end_date")
    ->group("wo.f_code")
    ->query()->fetchAll();  
    
    //print_r($result); die;
    $result = array_map(function ($v) { 
        $ditta_esterna = json_decode($v['ditta_esterna'], true);
        $cdc = json_decode($v['cdc'], true);
        
        if(!empty($ditta_esterna)){
            $v['ditta_esterna'] = implode(", ", $ditta_esterna['labels']);
        }
        else $v['ditta_esterna'] = '';
        
        if(!empty($cdc)){
            $v['cdc'] = implode(", ", $cdc['labels']);
        }
        else $v['cdc'] = '';
        
        $v['f_end_date'] = strftime("%a %d/%m/%Y",$v['f_end_date']);

        return $v; 
    }, $result);
    
    return $result;
}

}

if(empty($db) && isset($params['mp_selector'])){
    $db = $this->db;
    $mp_selector = $params['mp_selector'];
    //$mp_selector = 78127;
}

setlocale(LC_TIME, 'ita');
$sel_info = getSelectorsInfo($db,$mp_selector);
$mps = getMPscaduti($db,$mp_selector);


 /*OTTENERE IL TRADUTTORE*/

 $traductor = new Mainsim_Model_Translate();
 
 if(is_null($traductor->getLang())) $voc = $traductor->getLanguage("it_IT");
 else $voc = $traductor->getLanguage($traductor->getLang());

$GLOBALS['trad'] = $voc;
$GLOBALS['trad2'] = $traductor;

try{
    $pdf = new C('L','mm','A4');
}
catch (Exception $e){
    try{
        $unreg = stream_wrapper_unregister('var'); 
        $pdf = new C('L','mm','A4');
    } catch (Exception $ex) {}
}
    
 $pdf->AddFont('Arial', '', 'OpenSans-Light.php');
 $pdf->AddFont('Arial', 'B', 'OpenSans-Regular.php');
  $pdf->SetLineWidth(0.1);
 $pdf->AddPage();
 $pdf->SetAutoPageBreak(1,12);

 //TITOLO
 $Y=$pdf->GetY()+2;
 $pdf->SetXY(10,$Y);
 $pdf->SetFillColor(204,229,255);
 $pdf->SetLineWidth(0.3);
 $pdf->Rect(10,$Y,275,8,'F');
 $pdf->SetFont('Arial','B',12);	
 
 $pdf->Cell(275,8,"PIANI MANUTENTIVI (".$sel_info['f_title'].") SCADUTI IN DATA ".strftime("%A %d/%m/%Y %H:%M",time()),1,'','C');
 $Y=$pdf->GetY()+12;

 $pdf->SetFont('Arial','B',8);
// $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C'));
// $pdf->SetWidths(array(15,30,65,25,30,30,30,25,25)); //276
 $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C'));
 $pdf->SetWidths(array(15,15,35,70,25,30,30,25,30)); //276
 $pdf->SetXY(10,$Y);
 //$pdf->Row(array('N. PM','Titolo Piano Manutentivo','Asset','Data pianificata fine','Ditta esecutrice','Cdc','Frequenza','Fase','Fase Ultima Generata'));
 $pdf->Row(array('N. PM','N. OdL','Titolo Piano Manutentivo','Asset','Scadenza','Ditta esecutrice','Cdc','Frequenza','Fase'));

 $pdf->SetFont('Arial','',8);

 $pageNo = 1;
 $pdf->PageNo();
 
 foreach($mps as $mp){
     //var_dump($ricambio);
    if($pdf->getY() + 15 >  200) {
        $pdf->SetFont('Arial','B',8);
        //$pdf->Row(array('N. PM','Titolo Piano Manutentivo','Asset','Data pianificata fine','Ditta esecutrice','Cdc','Frequenza','Fase','Fase Ultima Generata'));
        $pdf->Row(array('N. PM','N. OdL','Titolo Piano Manutentivo','Asset','Scadenza','Ditta esecutrice','Cdc','Frequenza','Fase'));
        $pageNo++;
        $pdf->SetFont('Arial','',8);
    }

    $pdf->Row(
        array(sprintf("%05d", $mp['pg_progress']),$mp['progress_custom'],$mp['f_description'],$mp['assets_title'],$mp['f_end_date'],$mp['ditta_esterna'],
            $mp['cdc'],$mp['fc_pm_frequency'],$mp['phase_name'])
    );

 }


