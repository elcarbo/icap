<?php

function checkStartScript($scriptName,$db,&$log)
{
    // controllo che lo script sia da eseguire
    $executeScript = false;
    $selectAux = new Zend_Db_Select($db);
    $selectAux->from('t_scripts', array('f_aux'))
        ->where("f_name = '".$scriptName."'");
    $resAux = $selectAux->query()->fetch();

    $aux = json_decode($resAux['f_aux']);
    $timestamp = (empty($aux))?-1:strtotime($aux->next);
    
    if(empty($aux)){
    //if(empty($aux) && date("H") >= 23){
        $log = array('next' => date("d-m-Y H:i", mktime(date("H")+12,0,0,date("m"),date("d"),date("Y"))));
        $executeScript = true;
    }
    else if(!empty($aux) && time() > $timestamp){
            $executeScript = true;
            $log = array('next' => date("d-m-Y H:i", mktime(date("H")+12,0,0,date("m"),date("d"),date("Y"))));
//            $NewDate = strtotime("+14 days");
//            $log = array('next' => date("Y-m-d H:i", $NewDate));
    }
 
    return $executeScript; 
}

function getWos($db) 
{ 
    $select = new Zend_Db_select($db);
    $select->from(array("wo" => "t_workorders"), ['f_code'])
        ->join(array("tp" => "t_pair_cross"), "tp.f_code_main = wo.f_code",[])
        ->join(array("wa" => "t_wares"), "wa.f_code = tp.f_code_cross",[])
        ->where("wo.f_type_id IN (1,4,8,13)")
        ->where("wa.f_type_id IN (1,2,24)");
    $res = $select->query()->fetchAll();
    
    $res = (empty($res))?[]: array_unique(array_column($res, 'f_code'));
//    $sql = $select->__toString();
//    die($sql);
    return $res;
 }
 
function getPairCrossByType($f_code,$db) {
   $select = new Zend_Db_Select($db);
   $select->from('t_pair_cross as pc')
        ->join(['wa'=>'t_wares'],'wa.f_code = pc.f_code_cross',['f_type_id'])   
       ->where('pc.f_code_main = ' . $f_code)
       ->where('wa.f_type_id >= 1 ');
   $temp = $select->query()->fetchAll();

   if(empty($temp)) return [];
   $result = array();
   //print_r(array_column($temp,'f_code_cross','f_type_id')); die("rtemp");
   foreach ($temp as $value) {
       if(empty($result[$value['f_type_id']])) $result[$value['f_type_id']] = array();
       $new = $value; unset($new['f_type_id']);
       $result[$value['f_type_id']] [] = $new;
   }

   return $result;
}
    
 
 function updateWoPaircrossFields($f_code,$db)
 {
    $res_pcs = getPairCrossByType($f_code,$db);

    $pcs_assets = (empty($res_pcs)||empty($res_pcs['1']))?array():$res_pcs['1'];
    $pcs_labors = (empty($res_pcs)||empty($res_pcs['2']))?array():$res_pcs['2'];
    $pcs_supply = (empty($res_pcs)||empty($res_pcs['24']))?array():$res_pcs['24'];
    $fc_total_costs = 0;
    $fc_costs_for_downtime = (empty($pcs_assets))?0:array_sum(array_column($pcs_assets, 'fc_asset_downtime_cost'));
    $fc_costs_for_labor = (empty($pcs_labors))?0:array_sum(array_column($pcs_labors, 'fc_rsc_cost'));
    $fc_costs_for_service = (empty($pcs_supply))?0:array_sum(array_column($pcs_supply, 'fc_supply_total_cost'));

    $fc_total_costs = $fc_costs_for_downtime+$fc_costs_for_labor+$fc_costs_for_service;
    $vtoupdate = ['fc_costs_for_downtime' => $fc_costs_for_downtime,'fc_costs_for_labor' => $fc_costs_for_labor,
        'fc_costs_for_service' => $fc_costs_for_service,'fc_total_costs' => $fc_total_costs];

    $db->update('t_custom_fields',$vtoupdate, 'f_code = '.$f_code);
}

$execute = checkStartScript("total_cost_cron",$this->db,$log);
//if($execute || true){
if($execute){
    $wos = getWos($this->db);
    //print_r([$wos,count($wos)]); die("wos");
    foreach($wos as $f_code) {
        updateWoPaircrossFields($f_code,$this->db);
    }
	$this->db->update('t_scripts', array('f_aux' => json_encode($log)), array("f_name = 'total_cost_cron'")); 
}