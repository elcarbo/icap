<?php
if(!function_exists("updateChildrenCross")){

function updateChildrenCross($children,$father_code,$db){
    
    $f_wares_types = "1,5,25,26"; //ASSET,FILES AND LINKS,CDC,COMMESSA
    $remove_types = "1,25,26"; //ASSET,CDC,COMMESSA
    $father_crossed_wares = Mainsim_Model_Utilities::getCross('t_ware_wo',$father_code,'f_wo_id',array(),'f_ware_id','t_wares',$f_wares_types,true);
    
    foreach($children as $child_code) {
        $child_cross = Mainsim_Model_Utilities::getCross('t_ware_wo',$child_code,'f_wo_id',array(),'f_ware_id','t_wares',$remove_types,false);
        if(!empty($child_cross)){
            $child_cross = array_column($child_cross, 'f_id');
            $db->delete("t_ware_wo", "f_id IN (". implode(",",$child_cross).")" );
        }

        if(!empty($father_crossed_wares)){
            Mainsim_Model_Utilities::createCross($db,'t_ware_wo',$child_code,'f_wo_id',$father_crossed_wares,'f_ware_id');
        }
    }
   
    $updateAjax = true;
}

function updateChildrenFields($children,$father_code,$db){
    $select = new Zend_Db_Select($db);

    $custom_fields = ['note_wo','cdc','lista_controllo','permesso_n','commessa','lista_controllo_2','lavoro_impianti_elettrici',
        'uri_delegante','ri_delegato','pl','ora_imp_elettrici','lavoro_edifici_reverse_charge','prod_mov','disp_imp','permesso_attach',
        'note_prima_int','note_durante_int','note_dopo_int','fattore_ripartizione','fattore_ripartizione_decode'];

    $creation_fields = ['f_title','f_description'];
    
    $workorder_fields = ['f_priority','f_start_date','f_end_date','data_pianificata'];
    
    //colonne di 't_custom_fields'
    $t_custom_columns = Mainsim_Model_Utilities::get_tab_cols("t_custom_fields");
    $custom_fields = array_intersect($custom_fields, $t_custom_columns);

    //colonne di 't_creation_date'
    $t_creation_columns = Mainsim_Model_Utilities::get_tab_cols("t_creation_date");
    $creation_fields = array_intersect($creation_fields, $t_creation_columns);
    
    //update 't_custom_fields'
    $custom_res = $select->from(array("t_custom_fields"),$custom_fields)
        ->where("f_code = $father_code") // wo padre
        ->query()->fetch();  

    if(!empty($custom_res))
        $db->update('t_custom_fields', $custom_res, "f_code IN (". implode(",",$children).")");
    
     //update 't_creation_date'
    $select->reset();
    $creation_res = $select->from(array("t_creation_date"),$creation_fields)
        ->where("f_id = $father_code") // wo padre
        ->query()->fetch();  
    
    if(!empty($creation_res))
        $db->update('t_creation_date', $creation_res, "f_id IN (". implode(",",$children).")");
    
     //update 't_workorders'
    $select->reset();
    $workorder_res = $select->from(array("t_workorders"),$workorder_fields)
        ->where("f_code = $father_code") // wo padre
        ->query()->fetch();  
    
    if(!empty($workorder_res))
        $db->update('t_workorders', $workorder_res, "f_code IN (". implode(",",$children).")");  
    
    //print_r(["custom"=> $custom_res,"creation"=> $creation_res,"workorder"=> $workorder_res]); die();
}

}
try {

$children = [];
$children = Mainsim_Model_Utilities::getChildCode('t_workorders_parent',$f_code,$children,$this->db);
$children = array_values($children);

//script da eseguire solo per wo di tipo 1,4 che hanno dei figli
if(empty($params['not_execute_script_123']) && in_array($params['f_type_id'], [1,4]) && !empty($children)){ 
$updateAjax = true;
updateChildrenFields($children,$f_code,$this->db);
updateChildrenCross($children,$f_code,$this->db);

//aggiorno reverse ajax
if($updateAjax){
    foreach ($children as $child_wo) {
        Mainsim_Model_Utilities::saveReverseAjax("t_workorders_parent", $child_wo, [], "upd", "mdl_wo_tg",$this->db);
    }
}

}
} catch (Exception $exc) {
    die($exc->getMessage());
}



