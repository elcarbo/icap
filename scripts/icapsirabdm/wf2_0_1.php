<?php
//ICAP Sira - wf2 - creazione OdL [f_type 1]

if($params['f_type_id'] == 1){
    $select = new Zend_Db_Select($this->db);
    $data_wo_priority = $select->from(array('wo' => 't_workorders'),['f_priority','f_start_date','data_pianificata'])
        ->join(array('cd' => 't_creation_date'), 'cd.f_id = wo.f_code', ['f_creation_date'])
        ->join(array('wa' => 't_wares'), 'wa.fc_action_priority_value = wo.f_priority', ['fc_action_priority_taking_charge'])
        ->join(array('cd2' => 't_creation_date'), 'cd2.f_id = wa.f_code', [])
        ->where("wo.f_code = ".$f_code)
//        ->where("cd2.f_category = 'ACTION' ")
        ->query()->fetch();
    $select->reset();

    if(empty($data_wo_priority['data_pianificata'])){
        $data_pianificata = $data_wo_priority['f_creation_date'] + ($data_wo_priority['fc_action_priority_taking_charge'] *3600);
        $this->db->update('t_workorders', array('data_pianificata' => $data_pianificata), 'f_code = ' . $f_code);
    }
//    var_dump($data_wo_priority); die;
    //aggiorno il campo f_visibility e custom_progress
    $this->db->update('t_creation_date', array('f_visibility' => 254), 'f_id = ' . $f_code);
    $this->db->update('t_custom_fields', array('progress_custom' => sprintf("%05d", $fc_progress)), 'f_code = ' . $f_code);
    
    $time = date('Y-m-d H:i');
    $timestamp = strtotime($time);
    
    if(!empty($params['t_wares']))
    {
        $data_asset = $select->from('t_creation_date as cd',array('fc_hierarchy_codes','f_title as asset_name', 'fc_hierarchy'))
                        ->where('cd.f_id = '.$params['t_wares'])
                        ->query()->fetch();

        if($data_asset['fc_hierarchy_codes'] === '0') $avo = $params['t_wares'];
        else{	
            $avo = explode(",",$data_asset['fc_hierarchy_codes']);
            $avo = $avo[count($avo)-2];
        }

        $select->reset();
        $data_asset_cdc = $select->from('t_wares_relations as wr',[])
            ->join(array('tcds' => 't_creation_date'), 'tcds.f_id = wr.f_code_ware_slave', array('f_id as f_code_cdc','f_title as cdc'))
            ->where('wr.f_code_ware_master = '.$avo)
            ->where('wr.f_type_id_slave = 25')
            ->query()->fetch();

        $cdc_json = ["codes"=>[$data_asset_cdc['f_code_cdc']],"labels"=>[$data_asset_cdc['cdc']]];

        $this->db->update('t_custom_fields',array('cdc' => json_encode($cdc_json)), 'f_code = ' . $f_code);			
        $this->db->query("INSERT INTO t_ware_wo(f_ware_id, f_wo_id, f_timestamp) VALUES (".$data_asset_cdc['f_code_cdc'].",".$f_code.",".$timestamp.")");

    }
    require(SCRIPTS_PATH.'email_wf2_0_1.php');

}