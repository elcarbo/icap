<?php

function getLaborInfo($select,$labor_code){
    $select -> reset();
    $result = $select->from(array("cd" => "t_creation_date"), ['f_phase_id'])
        ->joinLeft(array("wr" => "t_wares_relations"), "wr.f_code_ware_slave = cd.f_id AND wr.f_type_id_master = 6",['f_code_ware_master as vendor_code'])
        ->where("cd.f_id = $labor_code")
        ->query()->fetch();
    
//    $sql = $select->__toString();
//    die($sql);
    
    return $result;		
}

function getWo($select,$vendor_code,$labor_code){
    $select -> reset();
    $select->from(array("wo" => "t_workorders"), ['f_code'])
        ->join(array("cd" => "t_creation_date"), "cd.f_id = wo.f_code",[])
        ->join(array("wp" => "t_wf_phases"), "wp.f_wf_id = cd.f_wf_id AND wp.f_number = cd.f_phase_id",[])
        ->join(array("wwo" => "t_ware_wo"), "wwo.f_wo_id = wo.f_code",[]) //vendors crossati con il wo
        ->joinLeft(array("wwo2" => "t_ware_wo"), "wwo2.f_wo_id = wo.f_code",[]) //labors crossati con il wo
        ->joinLeft(array("wa" => "t_wares"), "wa.f_code = wwo2.f_ware_id AND wa.f_type_id = 2",["GROUP_CONCAT(wa.f_code SEPARATOR ',') as labors_id"])
        ->where("wo.f_type_id IN (8,13)")
        ->where("wp.f_group_id NOT IN (6,7)")
        ->where("wwo.f_ware_id = $vendor_code")
        ->group("wo.f_code");
//    $sql = $select->__toString();
//    die($sql);
    $result = $select->query()->fetchAll();

    
    //filtro i wo non crossati con il labor
    $result = array_filter($result,function($el) use($labor_code) {
        $ids = (empty($el['labors_id']))?[]:explode(",", $el['labors_id']);
        return !in_array($labor_code, $ids);
    });    
    
    $result = array_map(function ($v) { 
        return $v['f_code']; 
    }, $result);
        
    return $result;		
}

$vendor_code = (empty($params['t_wares_6']['f_code'][0]))?false:$params['t_wares_6']['f_code'][0];

//se edit di un labor in creazione o fase 1, crossato con un vendor
if($params['f_type_id'] == 2 ){
    $select = new Zend_Db_Select($this->db);
    $laborInfo = getLaborInfo($select,$f_code);
    if(!empty($laborInfo['vendor_code']) && $laborInfo['f_phase_id'] == 1 ){
        $wos = getWo($select,$laborInfo['vendor_code'],$f_code);
//        var_dump($wos); die;
        foreach ($wos as $wo_code) {
            Mainsim_Model_Utilities::createCross($this->db,'t_ware_wo',$wo_code,'f_wo_id',[$f_code],'f_ware_id');
        }
        
    }
}
