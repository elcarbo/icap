<?php
// init application and db instance
include('../init.php');

if (!function_exists("sortByOrder")) {
    function sortByOrder($a, $b) {
        return $b["c"] - $a["c"];
    }
}

$selOp = new Zend_Db_Select($db);
$selOp->reset();
$resOp = $selOp->from(["t1" => "t_creation_date"], ["f_id"]) 
                ->join(["t2"=> "t_workorders"], "t1.f_id = t2.f_code", [])
                ->where("t2.f_type_id in ('3','19')")
                ->where("t1.f_phase_id in ('1','2')")
                ->order("t1.f_id desc")
                ->query()->fetchAll(Zend_Db::FETCH_COLUMN);
$totale = count($resOp);
print("\nFound ".$totale." PM \n\n");
$count = 0;
$nocount = 0;
$errors = [];
foreach ($resOp as $keye=>$valuee) {
    print("Updating ".(intval($count)+1)."/".$totale."\r");
    try {
        $selOp->reset();
        $resOp = $selOp->from(["t1" => "t_ware_wo"], ["f_ware_id"])
                        ->join(["t2a" => "t_creation_date"], "t1.f_ware_id = t2a.f_id", ["f_id as f_code_asset", "f_title as f_title_asset", "fc_hierarchy_codes"])
                        ->where("t1.f_wo_id = ?", $valuee)
                        ->where("t2a.f_category ='ASSET'")
                        ->query()->fetchAll();
        $total_assets = count($resOp);

        $db->update("t_custom_fields", ["num_asset" => $total_assets], "f_code = " . $valuee);

        $codes_asset = [];
        $count_codes = [];
        $count_codes2 = [];
        foreach ($resOp as $key => $value) {
            $explosion = explode(",", $value["fc_hierarchy_codes"]);
            if (count($explosion) == 1) { // è root, prendo il suo f_code
                $codes_asset[] = $value["f_code_asset"];
                $count_codes[$value["f_code_asset"]] = 0;
            } else { // Ho gerarchia, il penultimo valore è il padre
                $codes_asset[] = $explosion[count($explosion) - 2];
                $count_codes[$explosion[count($explosion) - 2]] = 0;
            }
        }

        foreach ($codes_asset as $key => $value) {
            $count_codes[$value]++;
            $count_codes2[$value] = $count_codes[$value] / count($codes_asset);
        }

    //    foreach ($count_codes as $key => $value) {
    //        $count_codes2[$key] = $value / count($codes_asset);
    //    }
        if (!empty($codes_asset)) {
            $selOp->reset();
            $resOp = $selOp->from(["t1" => "t_wares_relations"], ["f_code_ware_slave as f", "f_code_ware_master as x"])
                            ->join(["t2" => "t_creation_date"], "t1.f_code_ware_slave = t2.f_id", ["f_title as t"])
                            ->where("t1.f_code_ware_master in (?)", array_keys($count_codes2))
                            ->where("t2.f_category = 'CDC'")
                            ->query()->fetchAll();
        }
        $tot = 0;
        foreach ($resOp as $key => $value) {
            //$resOp[$key]["cdc_value"] = $count_codes2[$value["f_code_cdc"]];
            $resOp[$key]["v"] = round($count_codes2[$value["x"]],2,PHP_ROUND_HALF_DOWN);
            $resOp[$key]["c"] = $count_codes[$value["x"]];
            $tot += $resOp[$key]["v"];
        }
        usort($resOp, 'sortByOrder');
        // Arrotondamenti e rimuovo cdcc da array
        $tot = intval((1-$tot)*100);
        foreach ($resOp as $key=>$value) {
            if ($tot > 0) {
                $resOp[$key]['v'] += 0.01;
                --$tot;
            }
            unset($resOp[$key]['c']);
        }
        $db->update("t_custom_fields", ["fattore_ripartizione" => json_encode($resOp)], "f_code = " . $valuee);

        // Aggiusto CDC
        $codes = [];
        $labels = [];
        foreach($resOp as $key=>$value) {
            $codes[]=$value['f'];
            $labels[]=$value['t'];
        }
        $cdc = [];
        $cdc["codes"] = $codes;
        $cdc["labels"] = $labels;   
        $db->update("t_custom_fields", ["cdc" => json_encode($cdc)], "f_code = " . $valuee);
        //die(var_dump(json_encode($f_code)));
        $selOp->reset();
        $cross_cdc = $selOp->from(["t1"=>"t_ware_wo"], ["f_id"])
                            ->join (["t2"=>"t_creation_date"], "t1.f_ware_id = t2.f_id", [])
                            ->where("t1.f_wo_id = ?", $valuee)
                            ->where("t2.f_category = 'CDC'")
                            ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

        if (!empty($cross_cdc)) {
            $db->delete("t_ware_wo", "f_id in (".implode(",",$cross_cdc).")");
        }

        // Inserisco cross con cdc
        foreach ($cdc['codes'] as $key=>$value) {
           // die(var_dump($value));
            $db->insert("t_ware_wo", ["f_ware_id"=>$value, "f_wo_id"=>$valuee, "f_timestamp"=>time()]);
        }
        
    } catch (Exception $e) {
        $nocount++;
        $errors[] = ["f_code"=>$valuee, "message"=>$e->getMessage()];
    }  
    $count++;
}
if ($nocount > 0) {
    print_r("\nErrors in:\n");
    print_r($errors);
}
print_r("\n\nUpdated ".(intval($totale)-intval($nocount)). " PM\n");