<?php
require(APPLICATION_PATH . '/../library/fpdf/fpdf.php');
require(APPLICATION_PATH . '/../library/fpdf/mc_table.php');

function selectorsInfo($db,$sel_code){
    $select = new Zend_Db_Select($db);
    
    $result = $select->from(array('cd' => 't_creation_date'),['f_id','f_title']) //mp - planned generator
    ->where("cd.f_id = $sel_code") 
    ->query()->fetch();  
    
    return $result;
}

function getSettings($db,$key = null)
{ 
     $select = new Zend_Db_select($db);
     $select->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
             ->join(array("s" => "t_systems"), "c.f_id = s.f_code")
             ->where("c.f_type = 'SYSTEMS'")
             ->where("c.f_category = 'SETTING'")
              ->where("c.f_title = '".$key."'");

     $res = $select->query()->fetch();
     return $res['f_description'];
 }
 
function checkExecuteScript($scriptName,$db,&$log)
{
    // controllo che lo script sia da eseguire
    $executeScript = false;
    $selectAux = new Zend_Db_Select($db);
    $selectAux->from('t_scripts', array('f_aux'))
        ->where("f_name = '".$scriptName."'");
    $resAux = $selectAux->query()->fetch();

    $aux = json_decode($resAux['f_aux']);
    $timestamp = (empty($aux))?-1:strtotime($aux->next);
    
    if(empty($aux)){
    //if(empty($aux) && date("H") >= 23){
        $log = array('next' => date("d-m-Y H:i", mktime(23,0,0,date("m"),date("d")+7,date("Y"))));
        $executeScript = true;
    }
    else if(!empty($aux) && time() > $timestamp){
            $executeScript = true;
            $log = array('next' => date("d-m-Y H:i", mktime(23,0,0,date("m"),date("d")+7,date("Y"))));
//            $NewDate = strtotime("+14 days");
//            $log = array('next' => date("Y-m-d H:i", $NewDate));
    }
 
    return $executeScript; 
}

 function generatePdf($db,$sel_code)
 {
    try{
        $selector_info = selectorsInfo($db,$sel_code);   
        $mp_selector = $selector_info['f_id'];
        require(APPLICATION_PATH . '/../scripts/icapsirabdm/mp_scaduti.php');
        $strfile = $pdf->Output("doc.pdf", "S");
        //var_dump($strfile); die;
        
        //$pdf_attach_title = "PIANI MANUTENTIVI SCADUTI (".$selector_info['f_title'].") ".strftime("%d/%m/%Y",time());
        $pdf_attach_title = "PIANI MANUTENTIVI (".$sel_info['f_title'].") SCADUTI IN DATA ".strftime("%A %d/%m/%Y",time());
        $attachment = array(
            "path" => "",
            "filename" => $pdf_attach_title. ".pdf",
            "content" => $strfile,
            "mime" => "application/pdf"
        );
    }
    catch(Exception $e){
        $message = "[Icapsirabdm (mp_scaduti_cron.php)]Non e' stato possibile creare al cron il report mp scaduti".$e->getMessage();
        $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>$message,'f_type_log'=>88));		
    }
    
    return $attachment;
 }
 
function sendEmail($db,$attachments)
{
    try{
        //MAIL
        $to = array();
        $bcc = array();
        $cc = array();

        $mail_dest = getSettings($db,"MAIL_MP_SCADUTI");
        $mail_dest = (!empty($mail_dest))?json_decode($mail_dest, true):null;
        if(empty($mail_dest)) return;
        
        $to = (!empty($mail_dest['to']))?$mail_dest['to']:[];
        $bcc = (!empty($mail_dest['bcc']))?$mail_dest['bcc']:[];
        $cc = (!empty($mail_dest['cc']))?$mail_dest['cc']:[];
        
        //var_dump($to,$bcc,$cc); die;
        //MAIL TEST
//        $to = array('carbonell@mainsim.com');
//        $bcc = array('chioccioli@mainsim.com');
//        $cc = array('n.michelis@mainsim.com');       

        $date = strftime("%d/%m/%Y",time());
        $obj = "[ICAP] REPORT PIANI MANUTENTIVI SCADUTI IN DATA ".$date;
        
        $testo = "In allegato i report dei piani manutentivi scaduti in data ".$date;
        $mail = new Mainsim_Model_Mail();
        //var_dump($attachments); die;
        $mail->sendMail(array('To' => $to, 'Bcc' => $bcc, 'Cc' => $cc), $obj, $testo,$attachments, array('content-type' => 'text/html'));
        //INVIO MAIL RDA CON PDF-------------------------------------------------------------------------------------------

    } catch (Exception $e) {
        $message = "[Icapsirabdm (mp_scaduti_cron.php)]Non e' stato possibile inviare al cron la mail dei mp scaduti".$e->getMessage();
        $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>$message,'f_type_log'=>88));
       return false;
    }
    
    return true;
 }

 $db = $this->db;
 $execute = checkExecuteScript("mp_scaduti_cron",$db,$log);
 
 if($execute){
    $selectors = ["35710","78127"];
    $attachments = [];
    

    foreach ($selectors as $sel_code) {
        $selector_info = selectorsInfo($db,$sel_code);   
        $mp_selector = $selector_info['f_id'];
        require(APPLICATION_PATH . '/../scripts/icapsirabdm/mp_scaduti.php');
        $strfile = $pdf->Output("doc.pdf", "S");
        $pdf_attach_title = "PIANI MANUTENTIVI (".$sel_info['f_title'].") SCADUTI IN DATA ".strftime("%A %d/%m/%Y",time());
        $attach = array(
            "path" => "",
            "filename" => $pdf_attach_title. ".pdf",
            "content" => $strfile,
            "mime" => "application/pdf"
        );
        //var_dump($attach); die;
       if(!empty($attach)) array_push($attachments, $attach);
   }
   //var_dump($attachments); die;
    sendEmail($db,$attachments);
    $db->update('t_scripts', array('f_aux' => json_encode($log)), array("f_name = 'mp_scaduti_cron'")); 
 }