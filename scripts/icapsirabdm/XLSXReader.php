<?php
class XLSXReader{
    
    private $tempDir;
    private $sheets;
    private $sharedStrings;
    private $styles;
    private $xmlReader;
    private $rowOffset;
    private $colOffset;
    private $sheetIndex;
    private $currentRowTag;
    private $currentColTag;
    private $ghostRows;
    private $numberFormats = array(
        0 => 'General',
        1 => '0',
        2 => '0.00',
        3 => '#,##0',
        4 => '#,##0.00',
        9 => '0%',
        10 => '0.00%',
        11 => '0.00E+00',
        12 => '# ?/?',
        13 => '# ??/??',
        14 => 'm-d-y', //mm-dd-yy
        15 => 'd-mmm-yy',
        16 => 'd-mmm',
        17 => 'mmm-yy',
        18 => 'h:mm AM/PM',
        19 => 'h:mm:ss AM/PM',
        20 => 'h:mm',
        21 => 'h:mm:ss',
        22 => 'm/d/yy h:mm',
        37 => '#,##0 ;(#,##0)',
        38 => '#,##0 ;[Red](#,##0)',
        39 => '#,##0.00;(#,##0.00)',
        40 => '#,##0.00;[Red](#,##0.00)',
        44 => '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)',
        45 => 'mm:ss',
        46 => '[h]:mm:ss',
        47 => 'mmss.0',
        48 => '##0.0E+0',
        49 => '@',
        27 => '[$-404]e/m/d',
        30 => 'm/d/yy',
        36 => '[$-404]e/m/d',
        50 => '[$-404]e/m/d',
        57 => '[$-404]e/m/d',
        59 => 't0',
        60 => 't0.00',
        61 => 't#,##0',
        62 => 't#,##0.00',
        67 => 't0%',
        68 => 't0.00%',
        69 => 't# ?/?',
        70 => 't# ??/??',
    );
    
    public function __construct(){   
    }
    
    public function open($filename, $tempDir){
        
        $this->tempDir = $tempDir;
        $this->sheets = null;
        $this->sharedStrings = array();
        $this->xmlReader = new XMLReader();
        
        // try to unzip the excel into temp dir
        try{
            $zip = new ZipArchive();
            $zip->open($filename);
            $zip->extractTo($this->tempDir);
            $zip->close();
        } 
        catch(Exception $ex){
            $this->log($ex->getMessage());
        }
        
        // get styles
        $this->getStyles();
        // get shared strings
        $this->getSharedStrings();
        // get sheets
        $this->getSheetsList();
    }
    
    public function close(){
        
        // close xmlReader
        if($this->xmlReader){
            $this->xmlReader->close();
        }
        // delete temporary folder
        $this->delTree($this->tempDir);
    }
    
    public function delTree($dir) { 
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file"); 
        } 
        return rmdir($dir); 
    } 
    
    public function getSheetsList(){
        
        // first request --> read sheets list from xml
        if(!$this->sheets){
            
            if($this->xmlReader){
                $this->xmlReader->close();
            }
            $this->xmlReader = null;
            $this->xmlReader = new XMLReader();
            if(file_exists($this->tempDir . '/xl/workbook.xml')){
                $this->xmlReader->open($this->tempDir . '/xl/workbook.xml');
                // move to the sheets tag
                while($this->xmlReader->read() != null){
                    if($this->xmlReader->name === 'sheets'){
                        break;
                    }
                }
                // loop through sheets
                while($this->xmlReader->read() != null){
                    if($this->xmlReader->name == 'sheet'){
                        $this->sheets[$this->xmlReader->getAttribute('name')] = array(
                            'name' => $this->xmlReader->getAttribute('name'),
                            'id' => $this->xmlReader->getAttribute('r:id')
                        );
                    }
                    else{
                        break;
                    }
                }
            }
            
            // for each sheet get his file path
            $this->xmlReader->close();
            $this->xmlReader->open($this->tempDir . '/xl/_rels/workbook.xml.rels');
            while($this->xmlReader->read() != null){
                switch($this->xmlReader->name){
                    
                    case 'Relationship':
                        $id = $this->xmlReader->getAttribute('Id');
                        foreach($this->sheets as $sheet => $s){
                            if($this->sheets[$sheet]['id'] == $id){
                                $aux = explode("/", $this->xmlReader->getAttribute('Target'));
                                $this->sheets[$sheet]['file'] = $aux[1]; 
                            }
                        }
                        
                }
            }
            // for each sheets get dimensions
            $this->xmlReader->close();
            foreach($this->sheets as $sheet => $s){
                $this->xmlReader->open($this->tempDir . '/xl/worksheets/' . $this->sheets[$sheet]['file']);
                while($this->xmlReader->name != 'dimension'){
                    $this->xmlReader->read();
                }
                $aux = $this->xmlReader->getAttribute('ref');
                $aux2 = explode(":", $aux);
                $sheetRows = null;
                $sheetColumns = null;
                if(isset($aux2[1])){
                    for($j = 0; $j < strlen($aux2[1]); $j++){
                        $char = substr($aux2[1], $j, 1);
                        if(is_numeric( $char )){ 
                            $sheetRows .= $char;
                        }
                        else{
                            $sheetColumns .= $char;
                        }
                    }
                    $this->sheets[$sheet]['rows'] = $sheetRows;
                    $this->sheets[$sheet]['columns'] = $this->getExcelIndex($sheetColumns);
                }
                else{
                    $this->sheets[$sheet]['rows'] = 0;
                    $this->sheets[$sheet]['columns'] = 0;
                }
                
            }
        }
        return $this->sheets; 
    }
    
    private function getSharedStrings(){
        
        if($this->xmlReader){
            $this->xmlReader->close();
        }
        $this->xmlReader = null;
        $this->xmlReader = new XMLReader();
        if(file_exists($this->tempDir . '/xl/sharedStrings.xml')){
            $this->xmlReader->open($this->tempDir . '/xl/sharedStrings.xml');

            // move to the sst tag
            while($this->xmlReader->read() != null){
                if($this->xmlReader->name === 'sst'){
                    break;
                }
            }
            // loop through si tags
            $openTagSi = false;
            $string = null;
            while($this->xmlReader->read() != null){
                switch($this->xmlReader->name){
                    // tag 'si'
                    case 'si':
                        if($openTagSi){
                            $openTagSi = false;
                            $this->sharedStrings[] = $string;
                            $string = null;
                        }
                        else{
                            $openTagSi = true;
                        }
                        break;
                    // tag 't'
                    case 't':
                        $string .= $this->xmlReader->readString();
                        break;
                }
            }
        }
    }
    
    private function getStyles(){

        if($this->xmlReader){
            $this->xmlReader->close();
        }
        $this->xmlReader = null;
        $this->xmlReader = new XMLReader();

        if(file_exists($this->tempDir . '/xl/styles.xml')){
            
            $this->xmlReader->open($this->tempDir . '/xl/styles.xml');
            
            // move to cellxfs tag
            while($this->xmlReader->read() != null){
                if($this->xmlReader->name === 'cellXfs'){
                    break;
                }
            }
            // loop through si tags
            $openTagXf = false;
            $string = null;
            while($this->xmlReader->read() != null){
                switch($this->xmlReader->name){
                    // tag 'xf'
                    case 'xf':
                        if($this->xmlReader->nodeType != XMLReader::END_ELEMENT){
                            $this->styles['xf'][] = array(
                                'numFmtId' => $this->xmlReader->getAttribute('numFmtId')
                            );
                        }
                        break;
                }
            }
        }
    }
    
    public function openSheet($name){
        
        if($this->xmlReader){
            $this->xmlReader->close();
        }
        foreach($this->sheets as $key => $value){
            
            if($this->sheets[$key]['name'] == $name){
                
                if($this->xmlReader){
                    $this->xmlReader->close();
                }
                $this->xmlReader = null;
                $this->xmlReader = new XMLReader();
                $this->xmlReader->open($this->tempDir . '/xl/worksheets/' . $this->sheets[$key]['file']);
                $this->sheetIndex = $key;
                $this->rowOffset = 0;
                $this->colOffset = 0;
                $this->currentColTag = null;
                $this->currentRowTag = null;
                $this->ghostRows = array();
                return true;
            }
        }
        return false;
    }
    
    private function log($data){
        $this->writeFile($this->tempDir . '/log.txt', "-------------------------------------\n" . $data . "\n");
    }
    
    private function writeFile($filename, $data, $mode = FILE_APPEND){

        file_put_contents($filename, $data, $mode);
    }
    
    private function resetPosition(){
        $this->xmlReader->open($this->tempDir . '/xl/worksheets/' . $this->sheets[$this->sheetIndex]['file']);
        $this->rowOffset = 0;
        $this->colOffset = 0;
        $this->currentColTag = null;
        $this->currentRowTag = null;
    }
    
    public function moveTo($rowOffset = 0, $colOffset = 0){
        if(!$this->xmlReader){
            return false;
        }
        
        // if the cursor has passed the required position is shown at the beginning of the file
        if($this->rowOffset > $rowOffset ||
           ($this->rowOffset == $rowOffset && $this->colOffset > $colOffset)){
            // restart from the beginning of the file
            $this->resetPosition();
        }

        
        // seek row position
        while($this->rowOffset < $rowOffset){
            $this->xmlReader->read();
            switch($this->xmlReader->name){
                
                case 'row':
                    // go to new row
                    if($this->xmlReader->getAttribute('r') != $this->currentRowTag){
                        
                        // check ghost row --> mark it
                        if($this->xmlReader->getAttribute('r') - $this->currentRowTag > 1){
                            for($i = $this->currentRowTag + 1; $i < $this->xmlReader->getAttribute('r'); $i++){
                                
                                if(!in_array($i, $this->ghostRows)){
                                    $this->ghostRows[] = $i;
                                }
                                $this->rowOffset++;
                            }
                        }
                        $this->currentRowTag = $this->xmlReader->getAttribute('r');
                        $this->rowOffset++;
                        $this->colOffset = 0;
                    }
                    break;
                
                // end of data --> row not exists
                case 'sheetData':
                    if($this->xmlReader->nodeType == XMLReader::END_ELEMENT){
                        $this->resetPosition();
                        return false;
                    }
                    break;
            }
        }
        
        // seek column position
        while($this->colOffset < $colOffset){
            $this->xmlReader->read();
            switch($this->xmlReader->name){
                
                case 'c':
                    if($this->xmlReader->getAttribute('r') != $this->currentColTag){
                        $this->currentColTag = $this->xmlReader->getAttribute('r');
                        $this->colOffset++;
                    }
                    break;
                
                // column not exists --> reset position
                case 'row':
                    $this->resetPosition();
                    return false;
                    break;
            }
        }
        
        return true;
    }
    
    public function readCell($rowOffset, $colOffset){
        // move to the required position
        if(!$this->moveTo($rowOffset, $colOffset)){
            return null;
        }
        
        return $this->readCellTag();
    }
    
    public function readRow($rowOffset){
        if(!$this->moveTo($rowOffset)){
            return null;
        }
        
        // if it's a ghost row return empty array
        if(in_array($rowOffset, $this->ghostRows)){
            return array_fill(0, $this->sheets[$this->sheetIndex]['columns'], null);
        }
        
        $endRow = false;
        $endCell = false;
        $expectedColumn = 1;
        while(!$endRow){
            
            $this->xmlReader->read();
            switch($this->xmlReader->name){
                
                case 'c':
                    $column = null;
                    $aux = $this->xmlReader->getAttribute('r');
                    for($j = 0; $j < strlen($aux); $j++){
                        $char = substr($aux, $j, 1);
                        if(!is_numeric($char)){ 
                            $column .= $char;
                        }
                    }
                    
                    if($this->xmlReader->isEmptyElement){
                        $result[$this->getExcelIndex($column)] = null;
                    }
                    else{
                        if(!$endCell){
                            $result[$this->getExcelIndex($column)] = $this->readCellTag();
                            $endCell = true;
                        }
                        else{
                            $endCell = false;
                            $this->colOffset++;
                        }
                    }
                    break;
                    
                case 'row':
                    $endRow = true;
                    break;
            }
        }
        // fill the empty elements of rows if necessary
        for($i = 0; $i < $this->sheets[$this->sheetIndex]['columns']; $i++){
            $result2[] = isset($result[$i + 1]) ? $result[$i + 1] : null;
        }
        return $result2;
    }
    
    public function readRows($rowOffset = 0, $num = 1){
        for($i = 0; $i < $num; $i++){
            $result[] = $this->readRow($rowOffset + $i);
        }
        return $result;
    }
    
    private function readCellTag(){
        if(($this->xmlReader->getAttribute('t')) == 's'){
            $value = $this->sharedStrings[$this->xmlReader->readString()];
        }
        else{
            $styleId = $this->xmlReader->getAttribute('s');
            if($styleId != null){
                $value = $this->getNumberFormat($this->styles['xf'][$styleId]['numFmtId'], $this->xmlReader->readString());
            }
            else{
                $value = $this->xmlReader->readString();
            }
        }
        
        return $value;
    }
    
    private function getNumberFormat($type, $value){
        
        switch($this->numberFormats[$type]){
            
            case 'm-d-y':
                $value = date('m-d-Y', ($value - 25569) * 86400);
                break;
        }
        
        return $value;
    }
    
    private function getExcelColumn($index){
        
        $numeric = $index % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($index / 26);
        if($num2 > 0){
            return $this->getExcelColumn($num2 - 1) . $letter;
        }
        else{
            return $letter;
        }
    }
    
    private function getExcelIndex($column){
        
        $match = array();
        preg_match("/^[a-zA-Z]+/", $column, $match);
        $column = $match[0];
        $sum = 0;
        $aux = str_split($column);
        for($i = 0; $i < count($aux); $i++)
        {
            $sum *= 26;
            $sum += (ord($aux[$i]) - ord('A') + 1);
        }
        return $sum;
    }
}


