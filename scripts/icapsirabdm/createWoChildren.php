<?php
//require(SCRIPTS_PATH.'email_ditta_esterna.php');
if(!function_exists("clear")){
    
function clear(&$var,$kfind,$vreplace){
    if(!is_array($var)) return;

    foreach($var as $key => &$values){
            if($key === $kfind) $var[$key] = $vreplace;
            else if(is_array($values)) clear($values,$kfind,$vreplace);
    }
}


function unsetKey(&$var,$kfind,$strpos = false){
    if(!is_array($var)) return;
    $params = [];
    
    foreach($var as $key => &$values){
            if($key !== $kfind && (!$strpos ||  strpos($key, $kfind) === false)) $params[$key] = $values;
            else if(is_array($values))  unsetKey($values,$kfind,$strpos);
    }
    $var = $params;
}

function getWoParamsMultiPhaseBatch($f_code,$db){
    $params = array();
    $select = new Zend_Db_Select($db);
    $select->from('t_creation_date as cd',['f_id as f_code'])
             ->join(array("wo"=>"t_workorders"),"wo.f_code = cd.f_id",['f_type_id'])
            ->where('cd.f_id = ' . $f_code);
    $params = $select->query()->fetch();
    return $params;
}

function getFornitoriAbilitati($db){
    $select = new Zend_Db_Select($db);
    $fornitori = 
        $select->distinct()->from(array("wa"=>"t_wares"),['f_code']) //fornitori
          ->join(array("swf"=>"t_selector_ware"),"swf.f_ware_id = wa.f_code",[]) // selettori fornitori
          ->join(array("swu"=>"t_selector_ware"),"swu.f_selector_id = swf.f_selector_id",[]) // selettori utenti ( = selettori fornitori)
          ->join(array("us"=>"t_users"),"us.f_code = swu.f_ware_id",[]) // utenti
          ->join(array("cd"=>"t_creation_date"),"cd.f_id = us.f_code",[]) // utenti
        ->where("wa.f_type_id = 6") // fornitori
        ->where("cd.f_phase_id = 1") //utenti attivi
        ->query()->fetchAll();  
    
//    $sql = $select->__toString();
//    die($sql);
    if(empty($fornitori)) return false;
    
    return array_column($fornitori, 'f_code');
}

function getWoChildFornitori($db,$fcode){
    $select = new Zend_Db_Select($db);
    
    $result = $select->from(array("wp"=>"t_workorders_parent"),[])
            ->join(array("wo"=>"t_workorders"),"wo.f_code = wp.f_code",[]) // wo figli
            ->join(array("cd"=>"t_creation_date"),"cd.f_id = wo.f_code",[]) // wo figli
            ->join(array("ww"=>"t_ware_wo"),"ww.f_wo_id = wo.f_code",['f_ware_id']) // ware crossati ai wo figli
            ->join(array("wa"=>"t_wares"),"wa.f_code = ww.f_ware_id",[]) // ware crossati ai wo figli
            ->where("wp.f_parent_code = $fcode") // wo padre
            ->where("wo.f_type_id IN (8,13)") // wo figli emergency
            ->where("cd.f_phase_id NOT IN (12,13)") // wo figli non cancellati e non clonati
            ->where("wa.f_type_id = 6") //fornitori crossati con i wo figli
            ->query()->fetchAll();  
    
//    $sql = $select->__toString();
//    die($sql);
    if(empty($result)) return [];
    
    
    return array_column($result,'f_ware_id');
}

function getCustomProgress($father_code,$father_fc_progress,$child_wo_type_id,$db){
    $select = new Zend_Db_Select($db);
    $resChilds = 
            $select->from(array("wt"=>"t_workorders_types"),['f_id as wo_type_id','f_summary_name'])
              ->joinLeft(array("wo"=>"t_workorders"),"wo.f_type_id = wt.f_id",['COUNT(wo.f_code) as num'])
              ->joinLeft(array("wp"=>"t_workorders_parent"),"wp.f_code = wo.f_code",[])
            ->where("wt.f_id = $child_wo_type_id")
            ->where("wp.f_parent_code = $father_code")
            ->query()->fetch();  
    
    $num = $resChilds['num']+1;
    //$sname =($child_wo_type_id != 13)? $resChilds['f_summary_name']." ":'';
    $custom_progress = $father_fc_progress." - ".$num;
    //$custom_progress = $father_fc_progress." ".$sname."- ".$num;
    //$sql = $select->__toString();

    //var_dump($resChilds); die($sql);
    return $custom_progress;
}

function getChildParams($params,$fornitore,$db){
    $father = $params;
    $params['f_code'] = 0;
    $params['f_visibility'] = -1;
    
    //f_type_id e parentela
    $params['f_parent_code'] = $father['f_code'];
    $params['fc_wo_parent_code'] = $father['f_code'];
    if($params['f_type_id'] == 1) {
        $params['f_type_id'] = 13; //EMERGENCY
        $params['t_workorders_parent_1']['f_code'] [0] = $father['f_code'];
    }
    else {
        $params['f_type_id'] = 8; //RESERVATION
        $params['t_workorders_parent_4']['f_code'] [0] = $father['f_code'];
//        $unset = ['fc_wo_generator_id','fc_wo_out_of_order','fc_wo_resolution_days','fc_wo_task_sequence','fc_pm_duration_days'
//        ,'fc_pm_forewarning_days','fc_pm_attime_hours','fc_cond_condition','fc_cond_value_reference','fc_pm_start_date','t_wares_10'
//        ,'fc_work_instructions'];
        $unset = ['t_wares_10','f_code_periodic','fc_wo_generator_id','fc_wo_out_of_order','fc_wo_resolution_days','fc_wo_task_sequence','fc_work_instructions','fc_cond_condition','fc_cond_value_reference'];
        
        foreach ($unset as $key) {
            if(isset($params[$key]))  unset($params[$key]);
        }
        
        unsetKey($params,'fc_pm_',true);
    }
    
    $params['progress_custom'] = getCustomProgress($father['f_code'],$father['progress_custom'],$params['f_type_id'],$db);
    unset($params['fc_imp_code_wo_excel']);
    $params['fc_progress'] = '';
    $params['fc_hierarchy_selectors']= '';
    $params['fc_hierarchy_codes'] = '';
    $params['fc_hierarchy'] = '';
    $params['f_phase_id'] = 1; // passaggio di fase da 0 a 1
    if(isset($params['t_wares_1'])) $params['t_wares_1']['f_pair'] = []; //svuoto eventuali pair cross del padre
    if(isset($params['t_wares_2'])) $params['t_wares_2']['f_pair'] = []; //svuoto eventuali pair cross del padre
    if(isset($params['t_wares_2'])) $params['t_wares_2']['f_code'] = []; //svuoto eventuali cross
    if(isset($params['t_wares_24'])) $params['t_wares_24']['f_pair'] = []; //svuoto eventuali pair cross del padre
    if(isset($params['t_wares_24'])) $params['t_wares_24']['f_code'] = []; //svuoto eventuali cross
    $params['ore_mdo_tot'] = 0;
    //cancello riferimenti al f_code del padre sostituendolo con 0
    clear($params,'f_code_main',0);

    //selettori 
    $params['t_selectors_1,2,3,4,5,6,7,8,9,10']['f_code'] = array();

    //picklist 'Ditta esecutrice'
    $params['ditta_esterna'] = json_encode($fornitore);
    $params['t_wares_6']['f_code'] = array($fornitore['codes'][0]); //cross ditta esecutrice

    //manodopera
    $codes_man = Mainsim_Model_Utilities::getCross('t_wares_relations',$fornitore['codes'][0],'f_code_ware_master',array(),'f_code_ware_slave','t_wares', 2,true);

    if(!empty($codes_man)){
        //$manodopera = $params['t_wares_2']['f_code'];
        //f_code della manodopera crossata al fornitore
        //$manodopera = array_intersect($manodopera,$codes);
        $params['t_wares_2']['f_code'] = $codes_man;
    }
    else $params['t_wares_2']['f_code'] = [];

    //per scripts in fase 'Creation' (0->1)
    //$params['t_wares'] = $params['t_wares_1']['f_code'][0];
    
    //per NON ESEGUIRE script 'fc_progress-t_workorders',''
    $params['not_execute_script_123'] = true;
    $params['not_execute_exit_123'] = true;
    return $params;
}

function getMDOInterna($db,$flag = false)
{
    $select = new Zend_Db_Select($db);
    $resFornitore = $select->from(array("cd"=>"t_creation_date"),['f_id as codes','f_title as labels','fc_hierarchy_selectors'])
            ->join(array("wa"=>"t_wares"),"wa.f_code = cd.f_id",[])
            ->where("cd.f_title = 'MDO Interna'")
            ->where("wa.f_type_id = 6")
            ->query()->fetch();  

    if(empty($resFornitore)) return false;

    $resFornitore['codes'] = array($resFornitore['codes']);
    $resFornitore['labels'] = array($resFornitore['labels']);
    
    if($flag === false) unset($resFornitore['fc_hierarchy_selectors']);
    return $resFornitore;
}

function editFatherBefore($params,$db)
{
    $f_code = $params['f_code'];
    
    //elimino le cross con i fornitori affinchè il campo 'fc_hierarchy_selectors' dei figli non venga valorizzato erroneamente
    $fornitori = Mainsim_Model_Utilities::getCross('t_ware_wo',$f_code,'f_wo_id',array(),'f_ware_id','t_wares', 6,true);
    if(!empty($fornitori)){
        $fornitori = "(".implode(",", $fornitori).")";
        $db->delete("t_ware_wo", "f_wo_id = $f_code AND f_ware_id IN ".$fornitori ); //remove old selector ware cross
    }
    
    $params['ditta_esterna'] = null;

    //aggiorno il campo fc_hierarchy_selector escludendo i selettori dei fornitori
    $selectors = explode(",", $params['fc_hierarchy_selectors']);
    $fc_selectors = array();
    foreach ($selectors as $selector) {
        if(strpos($selector, 'SELECTOR 3')  === false) array_push($fc_selectors, $selector);
    }
    
    if(!empty($fc_selectors))   $params['fc_hierarchy_selectors'] = implode(",", $fc_selectors);
    else    $params['fc_hierarchy_selectors'] = null;

    $db->update('t_creation_date', array('fc_hierarchy_selectors' => $params['fc_hierarchy_selectors']), 'f_id = ' . $f_code);

//    //aggiorno progress_custom
//    $params['progress_custom'] = $params['fc_progress'];
//    $db->update('t_custom_fields', array('progress_custom' => $params['fc_progress']), 'f_code = ' . $f_code);

    return $params;
//   '{"codes":["77685"],"labels":["MDO Interna"]}';
}

function editFatherAfter($params,$olds_params,$db)
{
    $f_code = $params['f_code'];

    //aggiorno il campo f_visibility
    $db->update('t_creation_date', array('f_visibility' => 254), 'f_id = ' . $f_code);
    $params['f_visibility'] = 254;
    
    $params['ditta_esterna'] = null;
    
    if(!empty($olds_params['t_wares_6']['f_code'])) {
        //riassocio i fornitori abilitati scelti da picklist
        $params['t_wares_6']['f_code'] = $olds_params['t_wares_6']['f_code'];
 
        foreach($params['t_wares_6']['f_code'] as $vendor_code)
            $db->insert('t_ware_wo', ['f_wo_id' => $f_code, 'f_ware_id' => $vendor_code,'f_timestamp' => time()]); //cross wo - fornitore
        
        //$params['ditta_esterna'] = json_encode($mdoin); //picklist
        $params['ditta_esterna'] = $olds_params['ditta_esterna']; //picklist
        $db->update('t_custom_fields', array('ditta_esterna' => $params['ditta_esterna']), 'f_code = ' . $f_code);//picklist
    }

//    $params['not_execute_script_123'] = true;
    return $params;
}

function createChildren($father,$fornitori,$userinfo,$old_father_phase_id,$db)
{
    //ottengo il fornitore manodopera interna
    $mdoin = getMDOInterna($db);
    $mdoin = (empty($mdoin))?-1:$mdoin['codes'][0];
    
    if(($index = array_search($mdoin, $fornitori['f_code'])) !== false) {
        unset($fornitori['f_code'][$index]);
    }

    if(empty($fornitori['f_code']) || count($fornitori['f_code']) < 1) return;

    $select = new Zend_Db_Select($db);
    $resFornitori = $select->from(array("cd"=>"t_creation_date"),['f_id as codes','f_title as labels'])
        ->join(array("wa"=>"t_wares"),"wa.f_code = cd.f_id",[])
        ->where("cd.f_id IN (". implode(",", $fornitori['f_code']).")")
        ->where("cd.f_phase_id = 1")
        ->where("wa.f_type_id = 6")
        ->query()->fetchAll(); 
    
    $wo = new Mainsim_Model_Workorders($db);
    $count = count($resFornitori);

    //creo un wo figlio per ogni fornitore
    foreach($resFornitori as $fornitore){
        try {
            $fornitore['codes'] = array($fornitore['codes']);
            $fornitore['labels'] = array($fornitore['labels']);
            
            //ottengo i params adatti per creare il figlio
            $child = getChildParams($father,$fornitore,$db);
            
            //utf8_encode di stringhe
            foreach ($child as $key => $value) {
                if(!is_array($child[$key]) && is_string($value)) $child[$key] = utf8_encode($value);
            }
            
//            die(json_encode($child)); 
            //print_r($child); die("ch_params");
            
            $result = $wo->newWorkOrder($child,false,$userinfo);
            //print_r($result); die("ch_result");
            if(empty($result['f_code'])){
                 throw new Exception($result);
            }
            else if(!empty($old_father_phase_id)){
                //allineo il figlio alla fase del padre 
                $db->update('t_creation_date',['f_phase_id'=>$old_father_phase_id], 'f_id = ' . $result['f_code']);
                //Mainsim_Model_Utilities::saveReverseAjax("t_workorders_parent", $result['f_code'], [0], "add", "mdl_wo_tg", $db);
            }
        }catch(Exception $e) { 
            $msg = 'Qualcosa è andato storto durante la creazione dei workorders figli(1)';
            $msg.= ':\n'.$e->getMessage();
            $db->insert("t_logs", array('f_timestamp'=>time(), 'f_log'=>$msg, 'f_type_log'=>71));
            throw new Exception($msg);		
            //$error['message'] = "Something went wrong during insertion of your request ".$e->getMessage();
        }finally{
            $msg = 'Qualcosa è andato storto durante la creazione dei workorders figli(2)';
            if(!empty($result['message'])) {
                $msg.= ':\n'.$result['message'];
                $db->insert("t_logs", array('f_timestamp'=>time(), 'f_log'=>$msg, 'f_type_log'=>71));
                throw new Exception($msg);
            }
        }
    }
}
}
$execute = true;
$multi_phase_batch = (!empty($_POST['f_codes']))?true:false;
//se sto eseguendo un multipassaggio di fase non eseguire lo script. Se il wo è di di tipo 1 o 4 avvisa l'utente
if($multi_phase_batch){
    $execute = false;
    if(!empty($f_code)){
        $datiWo = getWoParamsMultiPhaseBatch($f_code,$db);
        if(in_array($datiWo['f_type_id'], [1,4])) 
            die("Errore: Per questo tipo di ordine di lavoro eseguire singolarmente il passaggio di fase 'Trasmesso a ditta esterna'");
    }
}

//se sono una corettiva o planned e ho selezionato ditte esterne esegui lo script
if($execute && in_array($params['f_type_id'], [1,4]) && !empty($params['t_wares_6']['f_code'])) {
    $olds_params = $params;
    $fornitori_childs = $params['t_wares_6']; //fornitori scelti da picklist
    $abilitati = getFornitoriAbilitati($this->db); //fornitori attivi ( esiste almeno un utente con il selettore di un fornitore)

    //escludo fornitori non abilitati
    $fornitori_childs['f_code'] = array_intersect($fornitori_childs['f_code'], $abilitati);
    //escludo i fornitori crossati con eventuali wo figli 
    $excludefor = getWoChildFornitori($this->db,$params['f_code']);
    $fornitori_childs['f_code'] = array_diff($fornitori_childs['f_code'], $excludefor);

    if(empty($fornitori_childs['f_code'])) return;

    //modifiche sul wo padre e sui params affinchè vengano creati i figli correttamente
    $params0 = editFatherBefore($params,$this->db);
    $old_phase_id = (empty($olds))?'':$olds['old_creation']['f_phase_id'];
    //creo tanti wo figli quanti sono i fornitori abilitati scelti dalla picklist non crossati con eventuali figli
    createChildren($params0,$fornitori_childs,$userinfo,$old_phase_id,$this->db);

    $params = editFatherAfter($params0,$olds_params,$this->db);
}