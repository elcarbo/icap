<?php
// init application and db instance
include('../init.php');

if (!function_exists("sortByOrder")) {
    function sortByOrder($a, $b) {
        return $b["c"] - $a["c"];
    }
}

$selOp = new Zend_Db_Select($db);
$selOp->reset();
$resOp1 = $selOp->from(["t1" => "t_creation_date"], ["f_id"]) 
                ->join(["t2"=> "t_workorders"], "t1.f_id = t2.f_code", [])
                ->join(["t3"=> "t_custom_fields"], "t2.f_code=t3.f_code", ["fattore_ripartizione"])
                ->where("t2.f_type_id in ('1','3','4','5','8','13','19')")
                ->order("t1.f_id desc")
                ->query()->fetchAll();
$totale = count($resOp1);
print("\nFound ".$totale." PM \n");
$count = 0;
$nocount = 0;
$errors = [];
$i = 0;
foreach ($resOp1 as $keye=>$valuee) {
    if(empty($valuee['fattore_ripartizione'])) continue;
    try {
       $fatt_ripartiz = json_decode($valuee["fattore_ripartizione"],1); 
       if ($fatt_ripartiz == null) {
            print("Errore JSON in ".$valuee["f_id"]. "\n");
            continue;
       }
    } catch (Exception $e) {
        print("Errore JSON in ".$valuee["f_id"]. "\n");
        continue;
    }
    $somma = 0;
    foreach ($fatt_ripartiz as $key=>$value) {
        $somma+=$value["v"];
    }
    if(abs($somma-1)>0.001) {
        //Scrivo nei log che c'è un errore
        print("Errore in ".$valuee["f_id"]. "\n");
        $i++;
    }

}

print("\nTot errori: ".$i);