<?php
//var_dump($_POST); die;
$vendors = $_POST['vendors'];
//$vendors = str_replace("|", "," ,$vendors);

$data = array();
$select = new Zend_Db_Select($this->db);
$res= $select->from(array('wa' => 't_wares'),array('fc_rsc_skill'))
->join(array('cd' => 't_creation_date'),"cd.f_id = wa.f_code",array('f_title','f_id'))
->join(array('wp' => 't_wf_phases'),"wp.f_wf_id = cd.f_wf_id AND wp.f_number = cd.f_phase_id",array('f_group_id'))
->join(array('wr' => 't_wares_relations'),"wr.f_code_ware_master = cd.f_id",array())
->join(array('cd2' => 't_creation_date'),"cd2.f_id = wr.f_code_ware_slave",array())
->where("wa.f_type_id = 2")
->where("wp.f_group_id NOT IN (5,6,7,8)")
->where("wr.f_type_id_slave = 6")
//->where("cd2.f_title REGEXP '".$vendors."'")
->where("wr.f_code_ware_slave IN (".$vendors.")")
 ->query()->fetchAll();
$select->reset();

$data[] = array(
    "label" => "Selezionare",
    "value" => "", 
	"selected" => false,
	"code"=>""
);
for($i = 0; $i < count($res); $i++){
    $data[] = array(
        'label' => Mainsim_Model_Utilities::chg($res[$i]['f_title'])." - ".Mainsim_Model_Utilities::chg($res[$i]['fc_rsc_skill']),    
        'value' => Mainsim_Model_Utilities::chg($res[$i]['f_title']),
        'code' => Mainsim_Model_Utilities::chg($res[$i]['f_id']),
    );
}
return print(json_encode($data));
