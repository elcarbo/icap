<?php
class C extends PDF_MC_Table{
	function Header(){
		$installation = new Mainsim_Model_Mainpage();
		$settings = $installation->getSettings();
		$this->SetTextColor(0,0,0);
		$this->SetFont('Arial','B',20);
		$this->Image(APPLICATION_PATH."/../public/msim_images/default/_pdf/logo.png");
		$this->SetXY(221,5);
                $this->Image(APPLICATION_PATH."/../public/msim_images/icapsirabdm/_pdf/logo.png",246,9,45,17);
		$this->SetXY(5,6);
		$this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
		$this->rect(5, 27, 285, 1.6, "F");
		$this->SetY(30);
	}

	function Footer(){
            $session_1 = $_SESSION['Zend_Auth'];
            $user_session = $session_1['storage']->f_displayedname;
            $this->SetXY(5,200);
            $this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
            $this->rect(5, 200, 285, 1.6, "F");
            $this->Ln(20);
            $this->SetXY(5,203);
            $this->SetFont('Arial','B',6);
            $this->Cell(100,5,"mainsim"." - what maintenance can be - www.mainsim.com ");
            $this->SetXY(190,203);
            $this->AliasNbPages();
            $this->Cell(100,5,$GLOBALS['trad2']->_('Page') . ' '.$this->PageNo().' - ' . $GLOBALS['trad2']->_('Printed by') . ' ' . $user_session . ' - ' . date('d-m-Y'),0,0,'R');
	}
}
class translator
{
	private $traductor;
	private $voc;
	public function __construct() 
	{
		$this->traductor = new Mainsim_Model_Translate();
		$this->voc = $this->traductor->getLanguage($this->traductor->getLang());
	}	
	public function translate($string){
		$res = $string;
		if($this->traductor->_($string)!=null) $res = $this->traductor->_($string);
		return $res;
	}
}

function cdc_wo($cdcs,$select,$vendor_fcode,$reverse_charge = false,$contabile = false)
{
    /******* Query per Recupere i wo associati ad ogni cdc/commessa *******/
    $cdc_wo = array();
    foreach($cdcs as $cdc){
            $select->from(array('cd' => 't_creation_date'),array('f_id','f_description'))
            ->join(array('cf' => 't_custom_fields'),"cf.f_code = cd.f_id ",['progress_custom as fc_progress','lavoro_edifici_reverse_charge','fattore_ripartizione','ditta_esterna'])
            ->join(array('wwo' => 't_ware_wo'),"wwo.f_wo_id = cd.f_id AND wwo.f_ware_id = ".$cdc['f_id'],[])
            ->join(array('wp' => 't_wf_phases'),"wp.f_wf_id = cd.f_wf_id AND wp.f_number = cd.f_phase_id",[])
            ->where("cd.f_category IN ('CORRECTIVE','PLANNED')")
            ->where("wp.f_group_id NOT IN (7,8)");

            if($contabile) $select->where("wp.f_number = 9");
            else $select->where("wp.f_number NOT IN (9)");

            if($reverse_charge) $select->where("cf.lavoro_edifici_reverse_charge = '1'");
            else $select->where("cf.lavoro_edifici_reverse_charge IS NULL");

//            $sql = $select->__toString();
//            die($sql);
            $result = $select->query()->fetchAll();
            
//            //filtro solo i wo crossati con il vendor
            $result = array_filter($result, function ($v) use ($vendor_fcode){ 
                $ditte = json_decode($v['ditta_esterna'], true);
                 return !empty($ditte['codes']) && in_array($vendor_fcode, $ditte['codes']);
            });

            $result = array_map(function ($v) { 
                unset($v['ditta_esterna']);
                return $v; 
            }, $result);
    
            if(count($result)>0)$cdc_wo[$cdc['f_id']] = $result;
            $select->reset();
    }
    //var_dump($cdc_wo); die;
    return  $cdc_wo;
}

function getFormatRes($result,$hours,$cdc,$skills,$json_ripartizione)
{
//    if(count($result) == 0) return false;
    
    //$skills = ['Op. Specializzato','Capo Cantiere','Tecnico'];
    $jobs = [];
    $jobs['ore'] = array_sum(array_column($result, 'ore'));
    $jobs['ore'] = fatt_ripartizione($json_ripartizione,$cdc,$jobs['ore']);
    $jobs['costo'] = array_sum(array_column($result, 'costo')); 
    $jobs['costo'] = fatt_ripartizione($json_ripartizione,$cdc,$jobs['costo']);

        
    if($jobs['ore'] == 0) return $jobs;
    
    //print_r($result); die;
    foreach ($skills as $skill) {
        $temp = [];
        //ritorna jobs fatti da manodopera di tipo $skill
        $temp = array_filter($result,function($job) use($skill) {
            
            $clean_a = strtolower(preg_replace('/\s*/', '', $skill));
            $clean_b = strtolower(preg_replace('/\s*/', '', $job['fc_rsc_skill']));
            
            return $clean_a == $clean_b;
        });   
        
        //if($skill == 'Tecnico') print_r($temp);
        $jobs[$skill]['costo'] = array_sum(array_column($temp,'costo'));
        $jobs[$skill]['costo'] = fatt_ripartizione($json_ripartizione,$cdc,$jobs[$skill]['costo']);

        //dividi per tipo di ore
        foreach($hours as $key) {
            $jobs[$skill][$key] = array_sum(array_column($temp, $key));
            $jobs[$skill][$key] = fatt_ripartizione($json_ripartizione,$cdc,$jobs[$skill][$key]);
        }
    }

    return $jobs;
}

function wo_Labor(&$cdc_wo,$since,$till,$select,$vendor,$skills)
{
    $select->reset();
    $tariffe = ['standard','overtime','holiday','overnight','ritiro_materiale','chiamata_feriale_prima_ora','chiamata_feriale_seconda_ora','chiamata_nott_prima_ora','chiamata_nott_seconda_ora','lavoro_sabato'];
    $slcstr = "IF(fc_rsc_fee_type = 'TARIFFA', fc_rsc_total_hours, 0) as TARIFFA";
    $cols = ["fc_rsc_since","fc_rsc_total_hours as ore","fc_rsc_cost as costo"];
    foreach ($tariffe as $value) $cols [] = str_replace("TARIFFA",$value ,$slcstr);
    //var_dump($cols); die("Ddd");
    /******* Associazione wo - ore Manodopera *******/
    foreach($cdc_wo as $cdc => &$wos){
        foreach($wos as &$wo){
            $select->from(array('pc' => 't_pair_cross'),$cols)
            ->join(array('wa' => 't_wares'),"wa.f_code = pc.f_code_cross AND wa.f_type_id = 2",array('fc_rsc_skill'))
            ->join(array('wr' => 't_wares_relations'),"wr.f_code_ware_master = wa.f_code AND wr.f_code_ware_slave = ".$vendor,array())
            ->where("pc.f_title = 'Job'")
            ->where('pc.f_code_main = ?',$wo['f_id'])
            ->where('pc.fc_rsc_since >= ?',$since)
            ->where('pc.fc_rsc_since < ?',$till);
            $result = $select->query()->fetchAll();
            //print_r($result); die();
            if(($wo['jobs'] = getFormatRes($result,$tariffe,$cdc,$skills,$wo['fattore_ripartizione'])) == false){
//                    if (($key = array_search($wo, $wos)) !== false) {
//                            unset($wos[$key]);
//                    }                
            }
            //print_r($wo); die();
            $select->reset();
        }
        if(count($cdc_wo[$cdc]) == 0) unset($cdc_wo[$cdc]);
    } 
    //print_r($cdc_wo); die();
/**********************************************************/
}

function wo_asset(&$cdc_wo,$select)
{
    /******* Associazione wo - asset *******/
    foreach($cdc_wo as $cdc => &$wos){
        foreach($wos as &$wo){
            $select->from(array('cd' => 't_creation_date'),array('f_title'))
            ->join(array('wa_wo' => 't_ware_wo'),"wa_wo.f_wo_id = ".$wo['f_id'],array())
            ->join(array('wa' => 't_wares'),"wa.f_code = wa_wo.f_ware_id AND wa.f_type_id = 1",array())
            ->where('cd.f_id = wa.f_code');
            $result = $select->query()->fetchAll();

            $select->reset();
            $titles = array();
            foreach($result as $asset){
                if(!empty($asset)){
                    array_push($titles,$asset['f_title']);
                }
            }
            $asset_str = implode(", ",$titles);
            $wo['asset'] = $asset_str;
        }
    } 
    /**********************************************************/	
}

function wo_materials(&$cdc_wo,$vendor,$anno, $mese, $select)
{
    $mesi = ["gennaio","febbraio","marzo","aprile","maggio","giugno","luglio","agosto","settembre","ottobre","novembre","dicembre"];
    $mese_str = $mesi[$mese-1];
    
    /******* Associazione wo - materials *******/
    foreach($cdc_wo as $cdc => &$wos){
        foreach($wos as &$wo){
            //wo con figli
            $select->from(array('wp' => 't_workorders_parent'),[])
                ->join(array('cd' => 't_creation_date'),"cd.f_id = wp.f_code",[])
                ->join(array('wf' => 't_wf_phases'),"wf.f_wf_id = cd.f_wf_id AND wf.f_number = cd.f_phase_id",[])
                ->join(array('ww' => 't_ware_wo'),"ww.f_wo_id = cd.f_id",[])
                ->join(array('ddt' => 't_ddt_logs'),"ddt.f_code = cd.f_id",['rif_ddt','costs_for_ddt'])
                ->where('wp.f_parent_code = '.$wo['f_id'])
                ->where('wf.f_group_id NOT IN (7,8)')
                ->where('ww.f_ware_id = '.$vendor)
                ->where("ddt.anno_ddt = $anno AND ddt.mese_ddt like '%".$mese_str."%'")
                ->order("ddt.f_timestamp DESC");
            $result = $select->query()->fetch();
            $select->reset();

            //wo senza figli
            if(empty($result)){
                $select->from(array('ddt' => 't_ddt_logs'),['rif_ddt','costs_for_ddt'])
                    ->where("ddt.f_code = ".$wo['f_id']." AND ddt.anno_ddt = $anno AND ddt.mese_ddt like '%".$mese_str."%'")
                    ->order("ddt.f_timestamp DESC");
                $result = $select->query()->fetch();
                $select->reset();
            } 
            
            if(!empty($result)){
                $wo['rif_ddt'] = $result['rif_ddt'];
                $wo['costs_for_ddt'] = fatt_ripartizione($wo['fattore_ripartizione'],$cdc,$result['costs_for_ddt']);
//                $wo['costs_for_ddt'] = $result['costs_for_ddt'];
            }
        }
    } 
    /**********************************************************/	
}

function checkIfAddPage($pdf,$wo,$i,$widths){
//    $pdf->SetDrawColor(255,133,102);
//    $pdf->Line(0,$pdf->GetY(), 185, $pdf->GetY());
    
    $rows = max($pdf->NbLines($widths[1]+$widths[2],$wo['asset']),$pdf->NbLines($widths[3],$wo['f_description']),$pdf->NbLines($widths[5],$wo['rif_ddt']));
    $dY = 5* $rows;
    if($i == 0) $dY+= 3*5;

//    $pdf->SetDrawColor(153,230,1);
//    $pdf->Line(185,$pdf->GetY()+ $dY, 295, $pdf->GetY()+ $dY);
//    $pdf->SetDrawColor(10,7,237);
//    $pdf->Line(0,200, 295, 200);
    
    if($pdf->GetY()+ $dY > 200)
    {
        $pdf->SetLineWidth(0.3);
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(1,1);
        $pdf->SetXY(5,30);
    }
    else $pdf->SetLineWidth(0);
    
//    $pdf->SetDrawColor(0,0,0);
    return $dY;
}

function fatt_ripartizione($json,$cdc,$value){
//[{"f":"69749","x":"73009","t":"B121","v":0.5},{"f":"83792","x":"84341","t":"B009","v":0.5}]
    $fatt = json_decode($json,true);
    if(empty($fatt) || count($fatt) == 1)   return $value;
    
    $cdcs = array_column($fatt, 'v','f');
    $cdcs = array_map(function ($v) use ($value){ 
        return floor($v*$value*100)/100; 
    }, $cdcs);

    // Arrotondamenti
    $tot = ($value-array_sum($cdcs));
    foreach ($cdcs as $f => &$v) {
        if ($tot > 0.001) {
            $v+= 0.01;
            $tot-=0.01;
        }
    }
    
    return (isset($cdcs[$cdc]))?$cdcs[$cdc]:$value;
}

function check_fatt_ripartizione($json){
    if(empty(json_decode($json,true)) )return false;
    $fatt = json_decode($json,true);
    
    return (count($fatt) > 1);
}

function getSkills($select,$fornitore)
{
    $select->from(array('wa' => 't_wares'),['fc_rsc_skill'])
    ->join(array('wr' => 't_wares_relations'),"wr.f_code_ware_slave = wa.f_code",[])
    ->join(array('cd' => 't_creation_date'),"cd.f_id = wa.f_code",[])
    ->where('wa.f_type_id = 2') //labor
    ->where('cd.f_phase_id = 1') //fase attivo
    ->where("wr.f_code_ware_master = $fornitore");
    $result = $select->query()->fetchAll();
    $select->reset();
    
    $skills = [];     $unique = [];
    
    $result = (empty($result))?[]:array_unique(array_column($result, 'fc_rsc_skill'));
    
    //array di valori univoci
    foreach ($result as $value) {
        $value_clean = preg_replace('/\s*/', '', $value);
        $value_clean = strtolower($value_clean);
        if(!in_array($value_clean, $unique)) {
            array_push ($unique, $value_clean);
            array_push ($skills, $value);
        }
    }
    
    return $skills;
}

 $pdf = new C('L','mm','A4');
 /*per conoscere la lingua*/

$currency_sign = $_SESSION["CURRENCY_SIGN"];
 /*OTTENERE IL TRADUTTORE*/
 
 $traductor = new Mainsim_Model_Translate();
 $voc = $traductor->getLanguage($traductor->getLang());

$GLOBALS['trad'] = $voc;
$GLOBALS['trad2'] = $traductor;

$mo_t = new translator();
 
 $pdf->AddFont('Arial', '', 'OpenSans-Light.php');
 $pdf->AddFont('Arial', 'B', 'OpenSans-Regular.php');
 $session = $_SESSION['Zend_Auth'];
 $language_session = $session['storage']->f_language;


 $select->reset();
 $language_query=$select->from(array('t1' => 't_localization'),array("f_datetime"))->where('t1.f_id=?',$language_session)->query()->fetch();
 $correct_sint_lang = str_replace("{", "", $language_query);
 $correct_sint_lang_final = str_replace("}", "", $correct_sint_lang);
 $date_string_lang = $correct_sint_lang_final['f_datetime'];
$select->reset();


$period = $_GET['anno_contabile'].'-'.$_GET['mese_contabile'];
$since = new DateTime('first day of '.$period); 
$since = $since->getTimestamp();
$till = new DateTime('last day of '.$period); 
$till = $till->modify("first day of next month");
//print_r(array($till,$till->getTimestamp())); die();
$till = $till->getTimestamp();
$vendor_fcode = $_GET['fornitore'];
$skills = getSkills($select,$vendor_fcode);
//recupero f_title del vendor
$vendor = $select->from(array('cd' => 't_creation_date'),array('f_title'))
 ->where("cd.f_id = ".$vendor_fcode)->query()->fetch();
$select->reset();
$vendor = $vendor['f_title'];

//print_r(array($since,$till,$vendor)); die();

$cdcs_codes = !empty($_GET['cdcs'])?explode(",", $_GET['cdcs']):[];

$cdcs_codes = array_filter($cdcs_codes, function ($v){ //elimino valori < 0
     return $v > 0;
});

$cdcs = [];
/******* Query per Recupere i CDC *******/
if(count($cdcs_codes) > 0){
    $select->from(array('wa' => 't_wares'),array())
    ->join(array('cd' => 't_creation_date'),"cd.f_id=wa.f_code",array('f_title','f_id'))
    ->where("wa.f_code IN (".implode(",",$cdcs_codes).")");
    $select->where("wa.f_type_id = 25");
    $cdcs = $select->query()->fetchAll();
    $select->reset();
}

/****************************************/

//print_r($cdcs); die("cdcs");
foreach($cdcs as $cdc){
    $cdc_title[$cdc['f_id']] = $cdc['f_title'];
}

/******* Query per Recupere i wo associati ad ogni cdc *******/ 
$cdc_wo = cdc_wo($cdcs,$select,$vendor_fcode,!empty($_GET['reverse']),!empty($_GET['contabile']));
/****************************************/

/******* Associazione wo cdc - ore Manodopera *******/
wo_Labor($cdc_wo,$since,$till,$select,$vendor_fcode,$skills);
/**********************************************************/

/******* Associazione wo cdc - asset *******/
wo_asset($cdc_wo,$select);
/******************************************/

/******* Associazione wo cdc - materials  *******/
wo_materials($cdc_wo,$vendor_fcode,$_GET['anno_contabile'],$_GET['mese_contabile'],$select);
/***********************************************/

//FILTRO CDC CON ALMENO UN ODL CON ORE LAVORATIVE O COSTI MANODOPERA
foreach ($cdc_wo as $key => $wos) {
    $wos = array_filter($wos, function ($v){ //elimino valori < 0
         return (!empty($v['jobs']['ore']) || !empty($v['costs_for_ddt']));
    });

    $cdc_wo[$key] = $wos;
}

$cdc_wo = array_filter($cdc_wo, function ($v){ //elimino valori < 0
     return (!empty($v));
});

$commesse_codes = !empty($_GET['commesse'])?explode(",", $_GET['commesse']):[];

$commesse_codes = array_filter($commesse_codes, function ($v){ //elimino valori < 0
     return $v > 0;
});

$select->reset();

$commesse = [];
/******* Query per Recupere le COMMESSE *******/
if(count($commesse_codes) > 0){
    $select->from(array('wa' => 't_wares'),array())
    ->join(array('cd' => 't_creation_date'),"cd.f_id=wa.f_code",array('f_title','f_id'))
    ->where("wa.f_code IN (".implode(",",$commesse_codes).")");
    $select->where("wa.f_type_id = 26");
    $commesse = $select->query()->fetchAll();
    $select->reset();
}
/****************************************/

foreach($commesse as $commessa){
$commesse_title[$commessa['f_id']] = $commessa['f_title'];
}

/******* Query per Recupere i wo associati ad ogni commessa *******/ 
$commesse_wo = cdc_wo($commesse,$select,$vendor_fcode,!empty($_GET['reverse']),!empty($_GET['contabile']));
/****************************************/

//print_r($commesse_wo); die();

/******* Associazione wo commesse - ore Manodopera *******/
wo_Labor($commesse_wo,$since,$till,$select,$vendor_fcode,$skills);
/**********************************************************/

/******* Associazione wo commesse - asset *******/
wo_asset($commesse_wo,$select);
/**********************************************************/

/******* Associazione wo commesse - materials  *******/
wo_materials($commesse_wo,$vendor_fcode,$_GET['anno_contabile'],$_GET['mese_contabile'],$select);
/***********************************************/

//FILTRO COMMESSE CON ALMENO UN ODL CON ORE LAVORATIVE O COSTI MANODOPERA
foreach ($commesse_wo as $key => $wos) {
    $wos = array_filter($wos, function ($v){ //elimino valori < 0
         return (!empty($v['jobs']['ore']) || !empty($v['costs_for_ddt']));
    });
    
    $commesse_wo[$key] = $wos;
}

$commesse_wo = array_filter($commesse_wo, function ($v){ //elimino valori < 0
     return (!empty($v));
});

//die(json_encode($commesse_wo));
setlocale(LC_TIME, 'ita');
$cdc_ore = array();
$commesse_ore = array();
$totale_ore = 0;
//var_dump($skills); die;
//$skills = ['Op. Specializzato','Capo Cantiere','Tecnico'];
$hours = ['standard','overtime','holiday','overnight','ritiro_materiale','chiamata_feriale_prima_ora','chiamata_feriale_seconda_ora','chiamata_nott_prima_ora','chiamata_nott_seconda_ora','lavoro_sabato'];
$totalHours = array_combine($skills,array_fill(0, count($skills),array_combine($hours,array_fill(0, count($hours), 0))));
$totalCosts = array_combine($skills,array_fill(0, count($skills), 0));
$totalCostsMaterials = 0;
//print_r($totalHours); die;
//print_r(array_values($cdc_wo)); die;
foreach($cdc_wo as $cdc => $wos){
    $cfddt = array_sum(array_column($wos, 'costs_for_ddt'));
    $cdc_ore[$cdc]['costs_for_ddt'] = $cfddt;
    $totalCostsMaterials += $cfddt;
    $jobs = array_column($wos, 'jobs');
    $cdc_ore[$cdc]['ore'] = array_sum(array_column($jobs, 'ore'));
    $cdc_ore[$cdc]['costo'] = array_sum(array_column($jobs, 'costo'));
    //print_r([$cdc,"jobs"=>$jobs]); die;
    foreach($skills as $skill) {
        $temp = [];
        //ritorna le ore fatte da manodopera di tipo $skill
        $temp = array_column($jobs,$skill);  
        $totalCosts[$skill] += array_sum(array_column($temp,'costo'));
//        if($skill == 'Tecnico') {var_dump($temp); die();}
        foreach($hours as $type) {
            $totalHours[$skill][$type] += array_sum(array_column($temp, $type));
        }
    }
}
//print_r($totalHours); die;
foreach($commesse_wo as $commessa => $wos){    
    $cfddt = array_sum(array_column($wos, 'costs_for_ddt'));
    $commesse_ore[$commessa]['costs_for_ddt'] = $cfddt;
    $totalCostsMaterials += $cfddt;
    $jobs = array_column($wos, 'jobs');
    $commesse_ore[$commessa]['ore'] = array_sum(array_column($jobs, 'ore'));
    $commesse_ore[$commessa]['costo'] = array_sum(array_column($jobs, 'costo'));
    foreach($skills as $skill) {
        $temp = [];
        //ritorna le ore fatte da manodopera di tipo $skill
        $temp = array_column($jobs,$skill);  
        $totalCosts[$skill] += array_sum(array_column($temp,'costo'));
//        if($skill == 'Tecnico') {var_dump($temp); die();}
        foreach($hours as $type) {
            $totalHours[$skill][$type] += array_sum(array_column($temp, $type));
        }
    }
}

 $pdf->SetLineWidth(0.1);
 $pdf->AddPage();
 $pdf->SetAutoPageBreak(1,1);

  //VARIABILI DI RIFERIMENTO
 $marginX = 5;
 $first_width = 25;
 $X = $marginX;
 

$title = (empty($_GET['reverse']))?'REPORT CDC/COMMESSE - NO REVERSE CHARGE':'REPORT CDC/COMMESSE - REVERSE CHARGE';
$title .= (empty($_GET['contabile']))?'':' - CHIUSURA CONTABILE';

$pdf->SetXY($marginX,$pdf->GetY());
$pdf->SetFont('Arial','B',10);
$pdf->SetTextColor(255, 0, 0);
$pdf->Cell(285,7,$title,0,'','C');
$pdf->SetTextColor(0, 0, 0);

 $Y=$pdf->GetY()+9;
 $pdf->SetXY(5,$Y);
 $pdf->SetFillColor(204,229,255);
 $pdf->SetLineWidth(0.3);
 //$pdf->Rect(5,$Y,285,8,'F');
 $pdf->Rect(5,$Y,285,10,'F');
 $pdf->SetFont('Arial','B',12);	
 //$pdf->Cell(285,8,$vendor." ".strftime("%B %Y",$since),1,'','C');
 $pdf->Cell(285,10,$vendor." ".strftime("%B %Y",$since),1,'','C');
 
  /**************************/
 /****** COSTO TOTALE ******/
/**************************/
$Y = $pdf->GetY()+10;
// $pdf->SetXY($marginX,$Y);
 $pdf->SetXY($marginX+265,$Y);
 
 $pdf->SetFillColor(255,133,102);
 $pdf->Rect($marginX+265,$Y,20,5,'F');
 $pdf->SetFont('Arial','B',8); 
 $pdf->Cell(20,5,'Costo Totale',1,'','C');
 $pdf->SetFont('Arial','BI',8);
 $pdf->SetXY($marginX+265,$Y+5);
 $pdf->Cell(20,5,number_format($totalCostsMaterials+array_sum(array_values($totalCosts)), 2, ",", ".")." ".chr(128),1,'','C');
 
  /**************************/
 /******* MANODOPERA *******/
/**************************/
 $Y=$pdf->GetY()+7;
 $pdf->SetXY($X,$Y);
 $pdf->SetFillColor(245, 248, 236);
 $pdf->SetLineWidth(0.3);
 $pdf->Rect(5,$Y,285,5,'F');
 $pdf->SetFont('Arial','I',10);	
 $pdf->Cell(285,5,'Manodopera',1,'','C');
 $Y=$pdf->GetY()+5;
 
 $pdf->SetXY($X,$Y);
 $pdf->SetFont('Arial','B',8);
 $pdf->Cell($first_width,10,'ORE MESE',1,'','C');

 //SOMMARIO ORE E COSTI TOTALI PER TIPO QUALIFICA MANODOPERA
 $TotaliRecapLabels = ['Standard','Straordinario','Festivo','Notturno','Ritiro materiale','Ch. feriale (1° ora)',
    'Ch. feriale (2° ora e succ.)','Ch. Notturna/Festiva (1° ora)','Ch. Notturna/Festiva (2° ora e succ.)','Lavoro Sabato','Ore Totali','Costo Totale'];
 foreach($TotaliRecapLabels as $key => $value) $TotaliRecapLabels[$key] = iconv('UTF-8', 'windows-1252', $TotaliRecapLabels[$key]);
 $Widths = array(17,20,16,16,25,20,30,30,30,16,20,20); 
 $Colors = array([178,255,102],[255,178,102],[255,255,128],[153,221,255],[217,177,140],[236,198,217],[230,179,204],[173,235,173],[153,230,153],[209,179,255],[255,153,128],[255,153,128]);
 $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C','C'));
 $pdf->SetWidths($Widths); 

 //print_r($totalHours); die("dd");

 //COLORAZIONE SOMMARIO
 $X = $marginX+$first_width;
 $flag = false;
foreach($Colors as $key => $value) {
  //var_dump($value); die("colors");
  $pdf->SetFillColor($value[0],$value[1],$value[2]);
  $width = $Widths[$key];
  $pdf->SetXY($X,$Y);
  $sum = array_sum(array_column($totalHours, $hours[$key]));
  if($sum > 0) $flag = true;
  
  if($key < count($Colors) -2 ) $fill = ($sum > 0)?'F':'';
  else $fill = ($flag)?'F':'';
  
  $pdf->Rect($X,$Y,$width,10,$fill);
  $X += $width;
}

 $pdf->SetFont('Arial','B',8);
 $pdf->SetXY($marginX+$first_width,$Y);
 $pdf->Row($TotaliRecapLabels);
 $pdf->SetXY($marginX,$Y+10);
 
$pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C','C','C'));
$pdf->SetWidths(array_merge([$first_width],$Widths)); 
$somma = array_fill(0, count($hours), 0.0);
 //print_r($totalHours); die("dd");
 foreach($totalHours as $skill => $hours){
    $values = array_merge(array_values($hours),[array_sum(array_values($hours))],[$totalCosts[$skill]]);
    foreach ($values as $index => $value) {
        $somma[$index] += $value;
    }
    $values[count($values)-1] = number_format($values[count($values)-1], 2, ",", ".")." ".chr(128);
    $pdf->Row(array_merge([$skill],$values));
    $pdf->SetX($marginX);
 }
 $somma[count($somma)-1] = number_format($somma[count($somma)-1], 2, ",", ".")." ".chr(128);

$pdf->SetWidths($Widths); 
$pdf->SetXY($marginX+$first_width,$pdf->GetY());
 $pdf->SetFont('Arial','BI',8);
$pdf->Row($somma);

  /**************************/
 /******* MATERIALI ********/
/**************************/
 $Y = $pdf->GetY()+2;
 $pdf->SetXY($marginX,$Y);
 $pdf->SetFillColor(245, 248, 236);
 $pdf->SetLineWidth(0.3);
 $pdf->Rect(5,$Y,285,5,'F');
 $pdf->SetFont('Arial','I',10);	
 $pdf->SetX($marginX);
 $pdf->Cell(285,5,'Materiali - DDT',1,'','C');
 
 $X = $marginX+$first_width+array_sum(array_slice($Widths, 0, count($Widths)-1));
 $Y = $pdf->GetY()+5;
 $pdf->SetXY($X,$Y);
 
 $tmp = end($Colors);
 $pdf->SetFillColor($tmp[0],$tmp[1],$tmp[2]);
 $pdf->Rect($X,$Y,end($Widths),5,$fill);
 $pdf->SetFont('Arial','B',8); 
 $pdf->Cell(end($Widths),5,'Costo Totale',1,'','C');
 $pdf->SetFont('Arial','BI',8);
 $pdf->SetXY($X,$Y+5);
 $pdf->Cell(end($Widths),5,number_format($totalCostsMaterials, 2, ",", ".")." ".chr(128),1,'','C');
 
 
$Widths2 = [25,24,29,91,20,20,28,24,24];

 /**************************/
  /********* CDC *********/
 /**************************/
 $X = $marginX;
 $Y = $pdf->GetY()+7;
 $pdf->SetXY($X,$Y);
 if(!empty($cdc_wo)){
    $pdf->SetFillColor(245, 248, 236);
    $pdf->SetLineWidth(0.3);
    
    $pdf->Rect(5,$Y,$first_width+array_sum(array_slice($Widths, 0, 7)),5,'F');
    $pdf->SetFont('Arial','I',10);	
    $pdf->SetX($marginX);
    $pdf->Cell($first_width+array_sum(array_slice($Widths, 0, 7)),5,'Dettaglio per Cdc',1,'','C');
    $pdf->SetXY($X ,$Y+5);
 }
 
foreach($cdc_wo as $cdc => $wos)
{
    $i=0;
    foreach($wos as $wo){
        $dY = checkIfAddPage($pdf,$wo,$i,$Widths2);

        if($i == 0)
        {
            $X = $marginX;
            $Y = $pdf->GetY();
            $pdf->SetXY($X,$Y);
            $pdf->SetFont('Arial','BI',8);

            $pdf->Cell($Widths2[0],5,$mo_t->translate('Cdc'),1,'','C');
            $pdf->SetXY($X,$Y+5);
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell($Widths2[0],5,$cdc_title[$cdc],1,'','C');

            $X += $Widths2[0];
            //ORE CDC
            $pdf->SetXY($X,$Y);

            $pdf->SetTextColor(0,0,0);
             $pdf->SetFont('Arial','BI',8);
            $pdf->Cell($Widths2[1],5,$mo_t->translate('Ore Totali MDO'),1,'','C');
             $pdf->SetFont('Arial','B',8);
            $pdf->SetXY($X,$Y+5);
            $pdf->Cell($Widths2[1],5,$cdc_ore[$cdc]['ore'],1,'','C');

            $X += $Widths2[1];
            //COSTO MDO CDC
            $pdf->SetXY($X,$Y);
            $pdf->SetFont('Arial','BI',8);
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell($Widths2[2],5,$mo_t->translate('Costo Totale MDO'),1,'','C');
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY($X,$Y+5);
            $pdf->Cell($Widths2[2],5,number_format($cdc_ore[$cdc]['costo'], 2, ",", ".")." ".chr(128),1,'','C');

            $X += $Widths2[2];
            //COSTO MATERIALI CDC
            $pdf->SetXY($X,$Y);
            $pdf->SetFont('Arial','BI',8);
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(array_sum(array_slice($Widths, 3, 2)),5,$mo_t->translate('Costo Totale Materiali'),1,'','C');
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY($X,$Y+5);
            $pdf->Cell(array_sum(array_slice($Widths, 3, 2)),5,number_format($cdc_ore[$cdc]['costs_for_ddt'], 2, ",", ".")." ".chr(128),1,'','C');

            $X += array_sum(array_slice($Widths, 3, 2));
            //COSTO TOTALE
            $pdf->SetXY($X,$Y);
            $pdf->SetFont('Arial','BI',8);
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(array_sum(array_slice($Widths, 5, 2)),5,$mo_t->translate('Costo Totale'),1,'','C');
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY($X,$Y+5);
            $pdf->Cell(array_sum(array_slice($Widths, 5, 2)),5,number_format($cdc_ore[$cdc]['costo']+$cdc_ore[$cdc]['costs_for_ddt'], 2, ",", ".")." ".chr(128),1,'','C');
            
            $pdf->SetTextColor(0,0,0);
            $pdf->SetXY($marginX,$pdf->GetY()+5);
            $pdf->SetFont('Arial','B',8);
            $pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
            $pdf->SetWidths(array($Widths2[0],$Widths2[1]+$Widths2[2],$Widths2[3],$Widths2[4],$Widths2[5],$Widths2[6],$Widths2[7],$Widths2[8])); 
            $pdf->Row(array($mo_t->translate('N. OdL'),$mo_t->translate('Asset'),$mo_t->translate('Description'),$mo_t->translate('Ore MDO'),$mo_t->translate('Costo MDO'),$mo_t->translate('Rif. DDT'),$mo_t->translate('Costo Materiali'),$mo_t->translate('Costo Totale')));
        }
        $X = $marginX;
        $Y = $pdf->GetY();
        $pdf->SetXY($X,$Y);
        $pdf->SetFont('Arial','',8);
        $pdf->SetLineWidth(0.1);
        
        $fc_progress = $wo['fc_progress'];
        $ore = number_format($wo['jobs']['ore'], 2, ",", ".");
        $costo = number_format($wo['jobs']['costo'], 2, ",", ".")." ".chr(128);
        $costs_for_ddt = number_format($wo['costs_for_ddt'], 2, ",", ".")." ".chr(128);
        $cost_total = number_format($wo['jobs']['costo']+$wo['costs_for_ddt'], 2, ",", ".")." ".chr(128);
        if(check_fatt_ripartizione($wo['fattore_ripartizione'])) {
            $fc_progress .= "*";    $ore .= "*";    $costo .= "*";  $costs_for_ddt .="*";   $cost_total .="*";
            if($ore == '*') $ore = '0 *';
        }

        $pdf->Row(array($fc_progress,$wo['asset'],$wo['f_description'],$ore,$costo,$wo['rif_ddt'],$costs_for_ddt,$cost_total));

        $i++;
    }
    $pdf->SetY($pdf->GetY()+5);
}


 /**************************/
  /********* COMMESSE *********/
 /**************************/
// $X = $marginX;
// $Y = $pdf->GetY()+7;
// $pdf->SetXY($X,$Y);
if(!empty($commesse_wo)){
 $Y = $pdf->GetY();
 $pdf->SetXY($X ,$Y);
 $pdf->SetFillColor(245, 248, 236);
 $pdf->SetLineWidth(0.3);
 $pdf->Rect(5,$Y,$first_width+array_sum(array_slice($Widths, 0, 7)),5,'F');
 $pdf->SetFont('Arial','I',10);	
 $pdf->SetX($marginX);
 $pdf->Cell($first_width+array_sum(array_slice($Widths, 0, 7)),5,'Dettaglio per Commessa',1,'','C');
 $pdf->SetXY($X ,$Y+5);
}

foreach($commesse_wo as $commessa => $wos)
{
    $i=0;
    foreach($wos as $wo){
        $dY = checkIfAddPage($pdf,$wo,$i,$Widths2);

        if($i == 0)
        {
            $X = $marginX;
            $Y = $pdf->GetY();
            $pdf->SetXY($X,$Y);
            $pdf->SetFont('Arial','BI',8);

            $pdf->Cell($Widths2[0],5,$mo_t->translate('Commessa'),1,'','C');
            $pdf->SetXY($X,$Y+5);
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell($Widths2[0],5,$commesse_title[$commessa],1,'','C');

            $X += $Widths2[0];
            //ORE COMMESSA MDO
            $pdf->SetXY($X,$Y);

            $pdf->SetTextColor(0,0,0);
             $pdf->SetFont('Arial','BI',8);
            $pdf->Cell($Widths2[1],5,$mo_t->translate('Ore Totali MDO'),1,'','C');
             $pdf->SetFont('Arial','B',8);
            $pdf->SetXY($X,$Y+5);
            $pdf->Cell($Widths2[1],5,$commesse_ore[$commessa]['ore'],1,'','C');

            $X += $Widths2[1];
            //COSTO COMMESSA MDO
            $pdf->SetXY($X,$Y);
	
            $pdf->SetFont('Arial','BI',8);
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell($Widths2[2],5,$mo_t->translate('Costo Totale MDO'),1,'','C');
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY($X,$Y+5);
            $pdf->Cell($Widths2[2],5,number_format($commesse_ore[$commessa]['costo'], 2, ",", ".")." ".chr(128),1,'','C');

            $X += $Widths2[2];
            //COSTO COMMESSA MATERIALI
            $pdf->SetXY($X,$Y);
            $pdf->SetFont('Arial','BI',8);
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(array_sum(array_slice($Widths, 3, 2)),5,$mo_t->translate('Costo Totale Materiali'),1,'','C');
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY($X,$Y+5);
            $pdf->Cell(array_sum(array_slice($Widths, 3, 2)),5,number_format($commesse_ore[$commessa]['costs_for_ddt'], 2, ",", ".")." ".chr(128),1,'','C');
            
            $X += array_sum(array_slice($Widths, 3, 2));
            //COSTO TOTALE
            $pdf->SetXY($X,$Y);
            $pdf->SetFont('Arial','BI',8);
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(array_sum(array_slice($Widths, 5, 2)),5,$mo_t->translate('Costo Totale'),1,'','C');
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY($X,$Y+5);
            $pdf->Cell(array_sum(array_slice($Widths, 5, 2)),5,number_format($commesse_ore[$commessa]['costo']+$commesse_ore[$commessa]['costs_for_ddt'], 2, ",", ".")." ".chr(128),1,'','C');

            
            $pdf->SetTextColor(0,0,0);
            $pdf->SetXY($marginX,$pdf->GetY()+5);
            $pdf->SetFont('Arial','B',8);
            $pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
            $pdf->SetWidths(array($Widths2[0],$Widths2[1]+$Widths2[2],$Widths2[3],$Widths2[4],$Widths2[5],$Widths2[6],$Widths2[7],$Widths2[8])); 
            $pdf->Row(array($mo_t->translate('N. OdL'),$mo_t->translate('Asset'),$mo_t->translate('Description'),$mo_t->translate('Ore MDO'),$mo_t->translate('Costo MDO'),$mo_t->translate('Rif. DDT'),$mo_t->translate('Costo Materiali'),$mo_t->translate('Costo Totale')));
        }
        $X = $marginX;
        $Y = $pdf->GetY();
        $pdf->SetXY($X,$Y);
        $pdf->SetFont('Arial','',8);
        $pdf->SetLineWidth(0.1);

        $fc_progress = $wo['fc_progress'];
        $ore = number_format($wo['jobs']['ore'], 2, ",", ".");
        $costo = number_format($wo['jobs']['costo'], 2, ",", ".")." ".chr(128);
        $costs_for_ddt = number_format($wo['costs_for_ddt'], 2, ",", ".")." ".chr(128);
        $cost_total = number_format($wo['jobs']['costo']+$wo['costs_for_ddt'], 2, ",", ".")." ".chr(128);

        if(check_fatt_ripartizione($wo['fattore_ripartizione'])) {
            $fc_progress .= "*";
            $ore .= "*";
            $costo .= "*";
            if($ore == '*') $ore = '0 *';
        }

        $pdf->Row(array($fc_progress,$wo['asset'],$wo['f_description'],$ore,$costo,$wo['rif_ddt'],$costs_for_ddt,$cost_total));

        $i++;
    }
    $pdf->SetY($pdf->GetY()+5);
}
