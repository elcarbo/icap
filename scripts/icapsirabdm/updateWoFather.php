<?php
if(!function_exists("checkDiffPairCross")){
function checkDiffPairCross($old,$new){
    $exclude = ['f_timestamp','f_id','rif_pair_cross_figlio']; //binds da non considerare
    
    $t_pc_columns = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
    $t_pc_columns = array_diff($t_pc_columns, $exclude);
    
    //controllo: considero solo bind esistenti in 't_pair_cross'
    $old = array_intersect_key($old ,array_flip($t_pc_columns));
    $new = array_intersect_key($new ,array_flip($t_pc_columns));
    
    //ottengo tutto ciò che contenuto in $new differisce da $olds (non considerando i campi in $exclude)
    $diff = array_diff_assoc($new,$old);
    
    return $diff;
}

function updatePairCrossFather($olds,$news,$father_pair_cross,$father_code,$db){
    getLockWo($father_code,'t_pair_cross','f_code_main','count(*) as num',$db);
    $toAdd = array();$toRemove = array();$toUpdate = array();  
    $updateAjax = false;
    $t_pc_columns = Mainsim_Model_Utilities::get_tab_cols("t_pair_cross");
    $fc_comments = 0;
//    $olds = array_filter($olds, function ($v){ //elimino valori < 0
//         return $v['f_code_cross'] > 0;
//    });

//    $news = array_filter($news, function ($v){ //elimino valori < 0
//         return $v['f_code_cross'] > 0;
//    });
    
    if(!empty($olds)) {
        $olds = array_combine(array_column($olds, 'f_id'), array_values($olds));
    }
    
    if(!empty($news)) {
        $news = array_combine(array_column($news, 'f_id'), array_values($news));
    }
    
    //pair-cross da aggiungere al wo padre
    $toAdd = array_diff(array_keys($news),array_keys($olds));
    $toAdd = (!empty($toAdd))?array_intersect_key($news,array_flip($toAdd)):[];
    
    //pair-cross da rimuovere dal wo padre
    $toRemove = array_diff(array_keys($olds),array_keys($news));
    $toRemove = (!empty($toRemove))?array_intersect_key($olds,array_flip($toRemove)):[];

    //pair-cross da aggiornare al wo padre
    $toUpdate = array_intersect(array_keys($news),array_keys($olds));
    $toUpdate = (!empty($toUpdate))?array_intersect_key($news,array_flip($toUpdate)):[];
    
    $toUpdate2 = array(); $toRemove2 = array();
    $select = new Zend_Db_Select($db);
    
    foreach($toRemove as $rif_pair_cross_figlio => $old) {
        $rif_pc_padre = (isset($father_pair_cross[$rif_pair_cross_figlio]))?$father_pair_cross[$rif_pair_cross_figlio]:false;
        if(!empty($rif_pc_padre)) {
            $toRemove2[] = $rif_pc_padre ; 
            if($old['f_title'] == 'Comment') $fc_comments -= 1;
        }
    }    
    $toRemove = $toRemove2; 
    
    foreach($toUpdate as $rif_pair_cross_figlio => $new) {
        $old = $olds[$rif_pair_cross_figlio];
        //ottengo l'array di campi da aggiornare
        $diff = checkDiffPairCross($old,$new);
        $rif_pc_padre = (isset($father_pair_cross[$rif_pair_cross_figlio]))?$father_pair_cross[$rif_pair_cross_figlio]:false;
        if(!empty($diff) && !empty($rif_pc_padre)) {
            $toUpdate2[$rif_pc_padre] = $diff; 
        }
    }
    $toUpdate = $toUpdate2;
        
    //remove paircross's father
    if(!empty($toRemove)){
        $ids = "(".implode(",", $toRemove).")";
        $db->delete("t_pair_cross", "f_id IN ".$ids );
        $updateAjax = true;
    }
    
    //update paircross's father
    if(!empty($toUpdate)){
        foreach ($toUpdate as $f_id => $values) {
            $db->update('t_pair_cross',$values, 'f_id = ' . $f_id);
        }
        $updateAjax = true;
    }

    //add paircross (and eventually cross) to wo father, update
    if(!empty($toAdd)){
        //mi ricavo le cross con il wo padre
        $father_cross = Mainsim_Model_Utilities::getCross('t_ware_wo',$father_code,'f_wo_id',array(),'f_ware_id','t_wares', '',true);
        //cross delle paircross da aggiungere
        $child_cross = array_unique(array_column($toAdd, 'f_code_cross'));
        $child_cross = array_filter($child_cross, function ($v){ //elimino valori < 0
             return $v > 0;
        });
        //ottengo le cross del figlio non crossate con il wo padre
        $crossToAdd = array_diff($child_cross,$father_cross);
        
        //aggiungo le cross manodopera-wo con il wo padre
        if(!empty($crossToAdd))
            Mainsim_Model_Utilities::createCross($db,'t_ware_wo',$father_code,'f_wo_id',$crossToAdd,'f_ware_id');
        
        //aggiungo le nuove paircross al wo padre
        foreach ($toAdd as $rif_pair_cross_figlio => $values) {
            if($values['f_title'] == 'Comment') $fc_comments += 1;
            unset($values['f_id']);
            $values['f_code_main'] = $father_code;
            $values['rif_pair_cross_figlio'] = $rif_pair_cross_figlio;
            $values['f_timestamp'] = time();
            //$values = array_intersect_key($values ,array_flip($t_pc_columns));
            //print_r($values); die("values");
            Mainsim_Model_Utilities::createPairCross($db,$values,0);
        }
        $updateAjax = true;
    }
    //print_r([$toRemove,$toUpdate,$toAdd]); die();
    //aggiorno il campo 'fc_comments' del wo padre
    $db->update('t_creation_date', array('fc_comments' => new Zend_Db_Expr("IFNULL(fc_comments,0) + $fc_comments")), "f_id = $father_code");
    
    //aggiorno reverse ajax
    if($updateAjax)
        Mainsim_Model_Utilities::saveReverseAjax("t_workorders_parent", $father_code, [], "upd", "mdl_wo_tg", $db);

    return $updateAjax;
}

function updateFatherFields($olds,$params,$father_code,$updateAjax,$db)
{
    $return = true;
    //condizioni per non fare l'update del wo padre
//    if($olds['old_table']['fc_wo_resolution'] !== $params['fc_wo_resolution'])$return = false;
    if($olds['old_table']['fc_wo_resolution'] !== $params['fc_wo_resolution'] || 
            $olds['old_custom']['rif_ddt'] !== $params['rif_ddt'] ||
            $olds['old_custom']['costs_for_ddt'] !== $params['costs_for_ddt'] || $updateAjax) $return = false;
    
    if($return) return;
    
    $f_code = $params['f_code'];
    $select = new Zend_Db_Select($db);
    $result = $select->from(array("wp"=>"t_workorders_parent"),[])
        ->join(array("cd"=>"t_creation_date"),"cd.f_id = wp.f_code",[]) // wo figli
        ->join(array("wo"=>"t_workorders"),"wo.f_code = cd.f_id",['f_code']) // wo figli
        ->join(array("cf"=>"t_custom_fields"),"cf.f_code = cd.f_id",['ditta_esterna']) // wo figli
        ->where("wp.f_parent_code = $father_code") // wo padre
        ->where("wo.f_type_id IN (8,13)") // wo figli emergency
        ->where("cd.f_phase_id NOT IN (12)") // wo figli non cancellati
        ->query()->fetchAll();  
    $select->reset();
    
    //if(empty($result)) return;
    
    $ditte_esterne = array_filter($result, function ($v){ 
         return preg_replace('/\s+/', '', $v['ditta_esterna']) !== '';
    });
    $ditte_esterne = array_map(function ($v) { 
        $ditta_esterna = json_decode($v['ditta_esterna'], true);
        $ditta_esterna = $ditta_esterna['labels'][0];
        $v['ditta_esterna'] = $ditta_esterna ;
        return $v; 
    }, $ditte_esterne);

    
    $ditte_esterne = array_column($ditte_esterne,'ditta_esterna','f_code');
    $brothers = array_keys($ditte_esterne);
    
    //importante non bloccare me stesso
    unset($brothers[$f_code]);
    
     /***********************************************************************************/
    /*************************Attività svolte****************************/
   /***********************************************************************************/
    
    //lock sulle righe dei altri figli
    $ris = $db->query("SELECT f_code,fc_wo_resolution FROM t_workorders where f_code IN (". implode(",",$brothers).") FOR UPDATE")->fetchAll(); 
    //lock sulla riga del padre
    $father_fc_wo_resolution = getLockWo($father_code,'t_workorders','f_code','fc_wo_resolution',$db);    
    
    //Attività svolte
    $ris = array_filter($ris, function ($v){ 
         return preg_replace('/\s+/', '', $v['fc_wo_resolution']) !== '';
    });
    $ris = array_column($ris,'fc_wo_resolution','f_code');

    $activities = [];
    foreach($ditte_esterne as $code => $ditta){
        $value = '';
        if($code == $f_code ) $value = $params['fc_wo_resolution'];
        else if(isset($ris[$code])) $value = $ris[$code];
        
        if(!empty($value)) array_push($activities, "[".$ditta."]: ".$value);
    }
    
    //var_dump($activities); die;
    $fc_wo_resolution = empty($activities)?'': implode(",\r\n", $activities);

    $db->update('t_workorders', array('fc_wo_resolution' => $fc_wo_resolution ), "f_code = $father_code");

    
     /***********************************************************************************/
    /*************************Riferimenti DDT, costs_for_ddt****************************/
   /***********************************************************************************/
    //lock sulle righe dei altri figli
    $ris = $db->query("SELECT f_code,rif_ddt,costs_for_ddt FROM t_custom_fields where f_code IN (". implode(",",$brothers).") FOR UPDATE")->fetchAll(); 
    //lock sulla riga del padre
    $father_riff_ddt = getLockWo($father_code,'t_custom_fields','f_code','rif_ddt,costs_for_ddt',$db);   
    
    $costs_for_ddt = array_sum(array_column($ris,'costs_for_ddt'));

    //Riferimenti DDT
    $ris = array_filter($ris, function ($v){ 
         return preg_replace('/\s+/', '', $v['rif_ddt']) !== '';
    });
    $ris = array_column($ris,'rif_ddt','f_code');

    $rifs = [];
    foreach($ditte_esterne as $code => $ditta){
        $value = '';
        if($code == $f_code ) $value = $params['rif_ddt'];
        else if(isset($ris[$code])) $value = $ris[$code];
        
        if(!empty($value)) array_push($rifs, "[".$ditta."]: ".$value);
    }

    $rif_ddt = empty($rifs)?'': implode(",\r\n", $rifs);

    $db->update('t_custom_fields', array('rif_ddt' => $rif_ddt ), "f_code = $father_code");

    //costs_for_ddt
    updateTotalCost($father_code,$costs_for_ddt,$db);
    
}

function updateTotalCost($f_code,$costs_for_ddt = 0,$db){
    $res_pcs = Mainsim_Model_Utilities::getPairCrossByType($f_code,$db);
    //print_r($res_pcs); die("respcs");
    $pcs_assets = (empty($res_pcs)||empty($res_pcs['1']))?array():$res_pcs['1'];
    $pcs_labors = (empty($res_pcs)||empty($res_pcs['2']))?array():$res_pcs['2'];
    $pcs_supply = (empty($res_pcs)||empty($res_pcs['24']))?array():$res_pcs['24'];
    $fc_total_costs = 0;
    $ore_mdo_tot = (empty($pcs_labors))?0:array_sum(array_column($pcs_labors, 'fc_rsc_total_hours'));
    $fc_costs_for_downtime = (empty($pcs_assets))?0:array_sum(array_column($pcs_assets, 'fc_asset_downtime_cost'));
    $fc_costs_for_labor = (empty($pcs_labors))?0:array_sum(array_column($pcs_labors, 'fc_rsc_cost'));
    $fc_costs_for_service = (empty($pcs_supply))?0:array_sum(array_column($pcs_supply, 'fc_supply_total_cost'));

    $fc_total_costs = $fc_costs_for_downtime+$fc_costs_for_labor+$fc_costs_for_service+$costs_for_ddt;
    $vtoupdate = ['fc_costs_for_downtime' => $fc_costs_for_downtime,'fc_costs_for_labor' => $fc_costs_for_labor,
        'fc_costs_for_service' => $fc_costs_for_service,'costs_for_ddt' => $costs_for_ddt
            ,'fc_total_costs' => $fc_total_costs,'ore_mdo_tot' => $ore_mdo_tot];
        //var_dump($vtoupdate); die;
    $db->update('t_custom_fields',$vtoupdate, 'f_code = '.$f_code);    
}

function getLockWo($wo_code,$table,$main_field,$columns,$db){
    $ris = $db->query("SELECT $columns FROM $table where $main_field = $wo_code FOR UPDATE")->fetch(); 
}


}

//script da eseguire per wo di tipo 8,13 (RESERVATION,EMERGENCY) aventi un padre e il param 'not_execute_script_123' vuoto 
$parents = Mainsim_Model_Utilities::getParentCode('t_workorders_parent',$params['f_code']);
if(empty($params['not_execute_script_123']) && in_array($params['f_type_id'], [8,13]) && !empty($parents) && $parents[0] !== 0){

$father_code = $parents[0];
$pair_cross = Mainsim_Model_Utilities::getPairCross($params['f_code']);
$father_pair_cross = Mainsim_Model_Utilities::getPairCross($father_code);
$father_pair_cross = array_column($father_pair_cross, 'f_id','rif_pair_cross_figlio');
if (empty($userinfo)) {
    $temp = Zend_Auth::getInstance()->getIdentity();
    $userId = $temp->f_id;
}
else $userId = $userinfo->f_id;

$updateAjax = updatePairCrossFather($olds['old_paircross'],$pair_cross,$father_pair_cross,$father_code,$this->db);
updateFatherFields($olds,$params,$father_code,$updateAjax,$this->db);

}

