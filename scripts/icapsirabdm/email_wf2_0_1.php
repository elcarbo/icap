<?php
//ICAP Sira - wf2 - creazione OdL [f_type 1]

if($params['f_type_id'] == 1){
try{
    $selOp = new Zend_Db_Select($this->db);
    
    $to = array();
    $cc = array();
    $bcc = array();

    /////////////////////////////////
    //		WO
    /////////////////////////////////
    $selOp->reset();
    $data_wo = $selOp->from('t_workorders',array('f_start_date','data_pianificata','f_end_date','f_priority','f_type_id','fc_wo_asset'))
                    ->join('t_creation_date', 't_creation_date.f_id = t_workorders.f_code',array('f_title','f_creation_date','f_description','fc_creation_user_name','fc_progress'))
                    ->join('t_custom_fields', 't_custom_fields.f_code = t_workorders.f_code', array('prod_mov','disp_imp'))
                    ->where('t_workorders.f_code = '.$f_code)
                    ->query()->fetch();


    $selOp->reset();
    if(!empty($params['t_wares']))
    {
        $data_asset = $selOp->from('t_creation_date as cd',array('fc_hierarchy_codes','f_title as asset_name', 'fc_hierarchy'))
                        ->where('cd.f_id = '.$params['t_wares'])
                        ->query()->fetch();

        if($data_asset['fc_hierarchy_codes'] === '0') $avo = $params['t_wares'];
        else{	
            $avo = explode(",",$data_asset['fc_hierarchy_codes']);
            $avo = $avo[count($avo)-2];
        }

        $selOp->reset();
        $data_asset_cdc = $selOp->from('t_wares_relations as wr',[])
            ->join(array('tcds' => 't_creation_date'), 'tcds.f_id = wr.f_code_ware_slave', array('f_id as f_code_cdc','f_title as cdc'))
            ->where('wr.f_code_ware_master = '.$avo)
            ->where('wr.f_type_id_slave = 25')
            ->query()->fetch();

    }
    /////////////////////////////////
    //		CC
    /////////////////////////////////
    //1) Recupero l'utente Richiedente 
    $userRichiedenteData = Zend_Auth::getInstance()->getIdentity();
    $userRichiedenteSelettore = $userRichiedenteData->selectors;
    $userRichiedenteId = $userRichiedenteData->f_id;
    $userSelector = explode(',', $userRichiedenteSelettore);
    $userExclude = null;

    if(array_search(69684, $userSelector) !== false ||  array_search(69685, $userSelector) !== false){
            $userExclude = $userRichiedenteId;
    }

    $gruppi_Cc = $params['ccReparti'];	
    //var_dump($gruppi_Cc); die;	
    if(!empty($gruppi_Cc)){
    //recupero le mail dei Cc in base al Selettore Gruppo (tramite l'f_title)	
    $selOp->reset();
    $data_mail_cc = $selOp->from("t_users",["fc_usr_mail"])
        ->join("t_selector_ware", "t_selector_ware.f_ware_id = t_users.f_code",[])
                //->join(array("tcds" => "t_creation_date"), "tcds.f_id = t_selector_ware.f_selector_id", [])
        ->join("t_creation_date", "t_selector_ware.f_ware_id = t_creation_date.f_id",[])
        ->where("t_selector_ware.f_selector_id IN (".$gruppi_Cc .")")
        ->where("t_creation_date.f_phase_id = 1")
        ->query()->fetchAll();

        foreach($data_mail_cc as $mail){
            array_push($cc,$mail["fc_usr_mail"]);
        }
    }
    //var_dump($cc); die;	
    //3) Recupero i destinatari
    //recupero Destinatari in base al Selettore Gruppo: Assistente STF 69683  escludendo ( Responsabile manutenzione 69684 - Coordinatore tecnico lavori 69685)
    $selOp->reset();
    $data_mail_to = $selOp->from("t_users",["fc_usr_mail", "f_code"])
        ->join("t_selector_ware", "t_selector_ware.f_ware_id = t_users.f_code",["f_selector_id"])
        ->join("t_creation_date", "t_selector_ware.f_ware_id = t_creation_date.f_id",[])
        ->where("t_selector_ware.f_selector_id IN (69684,69685,69683)")
        ->where("t_creation_date.f_phase_id = 1")
        ->query()->fetchAll();

    foreach($data_mail_to as $mail){
        if($mail["f_code"] != $userExclude){
            array_push($to,$mail["fc_usr_mail"]);
        }
    }

    //////////////////////////////////
    //		INFO WO
    /////////////////////////////////
    //Priorità
    $apriority = array("1"=>"1 - nel più breve tempo possibile","2"=>"2 - entro 5 giorni lav.","3"=>"3 - oltre 5 giorni lav.");
    $v = $data_wo['f_priority'];
    $textPriority = $apriority[$v];

    //Numero Progressivo WO
    $progress_wo = sprintf("%05d", $data_wo["fc_progress"]);

    //Date
    $data_creazione = (($data_wo['f_creation_date'] > 0) ? date('d/m/Y, H:i',$data_wo['f_creation_date']) : '');
    $disponibilita_da = (($data_wo['data_pianificata'] > 0) ? date('d/m/Y, H:i',$data_wo['data_pianificata']) : '');
    $disponibilita_da = "<i>DA</i> " .$disponibilita_da; 
    $disponibilita_a = (($data_wo['f_end_date'] > 0) ? date('d/m/Y, H:i',$data_wo['f_end_date']) : '');
    $disponibilita_a = " - <i>A</i> ".$disponibilita_a;

    if (!empty($data_wo['disp_imp'])) {
        $disponibilita_da = $data_wo['disp_imp'];
        $disponibilita_a = "";
    }

    //Destinatari
    //array_push($bcc,'n.michelis@mainsim.com');

    /////////////////////////////////
    //		Send mail
    /////////////////////////////////
    $mail_action = new Mainsim_Model_Mail($this->db);
    if($data_wo['f_type_id'] == 1){
        $mail_action->sendMail(array('To'=>$to, 'Cc'=>$cc, 'Bcc'=>$bcc),"[ICAP Sira BDM] Richiesta MTZ n. ".$progress_wo."",
            htmlentities("E' stata fatta una Richiesta di MTZ in data: ") .$data_creazione
            .".<br><br><fieldset><legend><i>Dettagli</i></legend><br><b>Apparecchiatura:</b> ".(($data_wo['f_title']=="")?((empty($data_asset['asset_name']))?'':$data_asset['asset_name']." - Gerarchia: ".$data_asset['fc_hierarchy']):$data_wo['f_title'])
            ."<br><br><b>CDC:</b> ".((empty($data_asset_cdc))?'':$data_asset_cdc['cdc'])
            ."<br><br><b>Descrizione:</b> ".$data_wo['f_description']
            ."<br><br><b>".htmlentities("Disponibilità impianto:")."</b>  ".$disponibilita_da.$disponibilita_a
            ."<br><br><b>".htmlentities("Ultimo prodotto movimentato:")."</b> ".$data_wo['prod_mov']
            ."<br><br><b>Richiedente:</b> ".$data_wo['fc_creation_user_name']
            ."<br><br><b>".htmlentities("Priorità").":</b> ".htmlentities($textPriority)."</fieldset>",null,array('content-type'=>'text/html'));
    }
	/////////////////////////////////
}catch(Exception $e){
	$message = "[Icapsirabdm (email)] ".$e->getMessage();
	$this->db->insert("t_logs", array('f_timestamp'=>time(), 'f_log'=>$message, 'f_type_log'=>88));
}
}