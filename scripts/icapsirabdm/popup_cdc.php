<?php
// init
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));
/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);


$dbParams = array( //PRODUZIONE
        'adapter'=>'PDO_MYSQL',
        'params'=>array(
                'host'=>'95.110.175.149',
                'dbname'=>'mainsim3_icapsirabdm',
                'username'=>'root',
                'password'=>'vxpUVjPnHuZriwV3',
        )
);

//$dbParams = array(  //STAGING
//        'adapter'=>'PDO_MYSQL',
//        'params'=>array(
//                'host'=>'10.0.245.220',
//                'dbname'=>'mainsim3_icapsirabdm',
//                'username'=>'host',
//                'password'=>'SetRu7res+3b',
//        )
//);


$cd = new Zend_Config($dbParams);
Zend_Registry::set('db', $cd);
$db = Zend_Db::factory($cd);

$userInfo = Zend_Auth::getInstance()->getIdentity();
$userId = $userInfo->f_id;
$sel = new Zend_DB_Select($db);

$userSel = $sel->from(array('sw' => 't_selector_ware'), ['f_selector_id'])
        ->join(array('sel' => 't_selectors'),"sel.f_code = sw.f_selector_id",[])
        ->where("sw.f_ware_id = $userId")
        ->where("sel.f_type_id = 3")
        ->query()->fetchAll();

$vSelectors = array_column($userSel, 'f_selector_id');
$sel->reset();
if(empty($vSelectors)){
    $res = $sel->from('t_creation_date', ['f_id', 'f_title'])
        ->where("f_category LIKE 'VENDOR' AND f_phase_id = 1")->query()->fetchAll();
}else{
    $res = $sel->from(array('cd' => 't_creation_date'), ['f_id', 'f_title']) //vendor
        ->join(array('sw' => 't_selector_ware'),"sw.f_ware_id = cd.f_id",[]) //selector  - vendor
        ->join(array('sel' => 't_selectors'),"sel.f_code = sw.f_selector_id",[]) //selector 3
        ->where("cd.f_category LIKE 'VENDOR' AND cd.f_phase_id = 1")
        ->where("sel.f_type_id = 3")
        ->where("sw.f_selector_id IN (". implode(',',$vSelectors).")")
        ->query()->fetchAll();    
}
$val = json_encode($res);

$sel->reset();
/******* Query per Recupere i CDC *******/
$cdcs_res = $sel->from(array('wa' => 't_wares'),array())
->join(array('cd' => 't_creation_date'),"cd.f_id=wa.f_code",array('f_id','f_title'))
 ->where("wa.f_type_id = 25")->order('f_title')->query()->fetchAll();
/****************************************/
$all = ["f_id"=>-1,"f_title"=>"ALL"];
array_unshift($cdcs_res, $all);
$cdcs = json_encode($cdcs_res);

$sel->reset();
/******* Query per Recupere le COMMESSE *******/
$commesse_res = $sel->from(array('wa' => 't_wares'),array())
->join(array('cd' => 't_creation_date'),"cd.f_id=wa.f_code",array('f_id','f_title'))
 ->where("wa.f_type_id = 26")->order('f_title')->query()->fetchAll();
/****************************************/
array_unshift($commesse_res, $all);
$commesse = json_encode($commesse_res);

$content = <<<EOD
	<html>
		<head>
                    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                    <style>
                            body {
                                    font-size: 12px;
                                    font-family: arial;
                            }
                            select, input {
                                    padding: 3px 10px 3px 10px;
                                    height: 30px;
                                    width: 167px;
                            }

                            input[type="button"] {
                                background-color: #a1c24d;
                                color:white;
                                font-size: 14px;
                                width: 60px;
                                margin-left: 5px;
                            }
                            table {
                                border-spacing: 5px 5px;
                                border-collapse: separate;
                                border:"0"
                            }
                    </style>
                    <script>
                        var dialog,form,dialog2,form2;
                        function mf$(l) {
                                return document.getElementById(l);
                        }
                        function checkAll(flag,bind){
                            var chkbs;
                            if(bind == 'cdcs')chkbs = dialog.find("[type=checkbox]");
                            else  chkbs = dialog2.find("[type=checkbox]");
                            [].forEach.call(chkbs, function (ckb) {
                                  ckb.checked = flag;
                            });                            
                        }
                        function listFilter(bind,text){
                            var chkbs;
                            var regx =  new RegExp(".*"+text+".*");
                            if(bind == 'cdcs')chkbs = dialog.find("[type=checkbox]");
                            else  chkbs = dialog2.find("[type=checkbox]");
                            [].forEach.call(chkbs, function (ckb) {
                                if(text == '' || regx.test(ckb.value)) {
                                        mf$("li_"+ckb.getAttribute("f_code")+"_"+bind).style.display = 'block';
                                }
                                else mf$("li_"+ckb.getAttribute("f_code")+"_"+bind).style.display = 'none';
                            });        
                        }
        
                        function createList(list,bind){
                            var ckbRc = [];
                            var ul = document.createElement('ul');
                            for(var cdc in list){
                                    if (list.hasOwnProperty(cdc)) {
                                            var li = document.createElement('li');
                                            li.id = "li_"+list[cdc]["f_id"]+"_"+bind;
                                            li.setAttribute("style", "margin-bottom:5px;");
                                            var row = document.createElement('div');
                                            row.setAttribute("style", "position:relative;");
                                            var checkbox = document.createElement('input');
                                            //if(list[cdc]["f_id"] == -1) checkbox.type = "radio";
                                            checkbox.type = "checkbox";
                                            checkbox.name = list[cdc]["f_title"];
                                            checkbox.value = list[cdc]["f_title"];
                                            checkbox.id = "ckb_"+list[cdc]["f_id"]+"_"+bind;
                                            if(list[cdc]["f_id"] == -1) checkbox.onchange = function () {checkAll(this.checked,bind);};
                                            checkbox.setAttribute("style", "border:2px double #00f;background:#ff0000;width:15px;height:15px;");
                                            checkbox.setAttribute("f_code", list[cdc]["f_id"]);
                                            checkbox.checked = true;
                                            ckbRc.push(list[cdc]["f_id"]);
                                            row.appendChild(checkbox);
                                            var label = document.createElement('label');
                                            label.setAttribute("style", "height:19px;margin-left:5px;position:absolute;top:50%;transform:translate(0,-40%);");
                                            label.htmlFor = "ckb_"+list[cdc]["f_id"]+"_"+bind;
                                            label.appendChild(document.createTextNode(list[cdc]["f_title"]));
                                            row.appendChild(label);
                                            li.appendChild(row);
                                            ul.appendChild(li);
                                    }
                            }
                            mf$("id_"+bind).value = ckbRc.join();
                            mf$(bind+"List").appendChild(ul);
                        }

                        function addElements(bind) {
                           var usrRc = []; var chkbs; var select; var str = ''; var dlg;
                           var count = 0; 
                            if(bind == 'cdcs'){
                                chkbs = dialog.find("[type=checkbox]");
                                var select = mf$("selectCdcs");
                                str = " Cdc selezionati";
                                dlg = dialog;
                            }
                            else{
                                chkbs = dialog2.find("[type=checkbox]");
                                var select = mf$("selectCommesse");
                                str = " Commesse selezionate";
                                dlg = dialog2;
                            }
        
                            [].forEach.call(chkbs, function (ckb) {
                              if(ckb.checked){
                                  usrRc.push(ckb.getAttribute("f_code"));
                                  if(ckb.getAttribute("f_code") > 0)  count++;
                              }
                            });

                            mf$("id_"+bind).value = usrRc.join();
                            if(count == chkbs.length -1)  select.value = "ALL";
                            else select.value = count + str;
                            dlg.dialog("close");
                        }
        
                        function setChecked(bind){
                            if(mf$("id_"+bind).value == '') return;
                            var checked = mf$("id_"+bind).value.split(",");
                            checked.forEach(function(el) {
                                 mf$("ckb_"+el+"_"+bind).checked = true;
                            });
                        }
        
        		window.onload = function() {
                            let ac = document.querySelector('#anno_contabile')
                            let d = new Date()
                            for(let i = 2017; i <= 2027; ++i) {
                                    let op = new Option()
                                    op.text = i
                                    op.value = i
                                    ac.options[ac.options.length] = op     
                                    if(d.getFullYear() == i) {
                                            op.selected = true
                                    }  		
                            }
                            let f = document.querySelector('#fornitori')
                            let flist = {$val}
                            for(let i in flist) {
                                    let opf = new Option()
                                    opf.text = flist[i].f_title	
                                    opf.value = flist[i].f_id
                                    f.options[f.options.length] = opf
                                    if(flist[i].f_title.match(/BRT/)) {
                                            opf.selected = true
                                    }
                            }
                            document.querySelector('#mese_contabile').options[d.getMonth()].selected = true
                            dialog = $("#dialog-form").dialog({
                              autoOpen: false,
                              height: 400,
                              width: 350,
                              modal: true,
                              buttons: {
                                Ok: function() {
                                    listFilter('cdcs','');
                                    mf$("cdcs_filter").value = '';
                                    addElements('cdcs');
                                },
                                Annulla: function() {
                                  dialog.dialog("close");
                                }
                              },
                              close: function() {
                                form[0].reset();
                              },
                              open:function() {
                              }
                            });

                            form = dialog.find( "form" ).on( "submit", function( event ) {
                              event.preventDefault();
                            });
                            
                            $("#selectCdcs").on("click", function() {
                                $.when(setChecked('cdcs')).then(dialog.dialog("open"));
                            }); 
                            
                            
                            dialog2 = $("#dialog2-form").dialog({
                              autoOpen: false,
                              height: 400,
                              width: 350,
                              modal: true,
                              buttons: {
                                Ok: function() {
                                    listFilter('commesse','');
                                    mf$("commesse_filter").value = '';
                                    addElements('commesse');
                                },
                                Annulla: function() {
                                  dialog2.dialog("close");
                                }
                              },
                              close: function() {
                                form[0].reset();
                              },
                              open:function() {
                              }
                            });

                            form2 = dialog2.find("form").on( "submit", function( event ) {
                              event.preventDefault();
                            });
                            
                            $("#selectCommesse").on("click", function() {
                                $.when(setChecked('commesse')).then(dialog2.dialog("open"));
                            }); 
                            
                            createList({$cdcs},'cdcs');
                            createList({$commesse},'commesse');
        		}

        		let viewPdf = function() {
                            var extra = getIframeUrlParams();
                            var cdcs = "&cdcs="+mf$("id_cdcs").value;
                            var commesse = "&commesse="+ mf$("id_commesse").value;
                            if(extra != '') extra = "&"+extra;
                            var win = window.open("../../pdf-creator/print-pdf/f_code/0/mockup/1/script_name/report_cdc?anno_contabile=" + document.querySelector('#anno_contabile').options[document.querySelector('#anno_contabile').selectedIndex].value + "&mese_contabile=" + document.querySelector('#mese_contabile').options[document.querySelector('#mese_contabile').selectedIndex].value + "&fornitore=" + document.querySelector('#fornitori').options[document.querySelector('#fornitori').selectedIndex].value+cdcs+commesse+extra, "_blank");
  						win.focus();
        		}
                                
                        let getIframeUrlParams = function() {
                                var re = document.URL.match(/^(.*)\?(.*)/);
                                if(re && re.length == 3)
                                    return re[2];
                                return "";
                        }
        	</script>
		</head>
		<body>	
			<div style="display: inline-block">
				<table>
					<tr>
                                            <td>Anno contabile</td>
					</tr>
					<tr>
                                            <td>					
                                                <select id="anno_contabile"></select>					
                                            </td>
					</tr>
					<tr>
                                            <td>Mese contabile</td>
					</tr>
					<tr>
                                            <td>
                                                <select id="mese_contabile">
                                                        <option value="1">Gennaio</option>
                                                        <option value="2">Febbraio</option>
                                                        <option value="3">Marzo</option>
                                                        <option value="4">Aprile</option>
                                                        <option value="5">Maggio</option>
                                                        <option value="6">Giugno</option>
                                                        <option value="7">Luglio</option>
                                                        <option value="8">Agosto</option>
                                                        <option value="9">Settembre</option>
                                                        <option value="10">Ottobre</option>
                                                        <option value="11">Novembre</option>
                                                        <option value="12">Dicembre</option>
                                                </select>
                                            </td>
					</tr>
                                        <tr>
                                            <td>Cdc</td>
					</tr>
                                        <tr>
                                            <td>
                                                <input id = "selectCdcs" type="text" name="selectCdcs" value="ALL" readonly style ="font-size:12px;">
                                            </td>
                                            <td>
                                                <div id="dialog-form" title="Cdc">
                                                  <form>
                                                        <h4>Seleziona Cdc</h4> 
                                                        <input type="text" id = "cdcs_filter" value = ''  placeholder="Filtra" onkeyup="listFilter('cdcs',this.value)">
                                                        <fieldset id="cdcsList" style ="padding:0; border:0;" ></fieldset>
                                                  </form>
                                                </div>
                                                <input type="hidden" id = "id_cdcs" value = ''>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Commesse</td>
					</tr>
                                        <tr>
                                            <td>
                                                <input id = "selectCommesse" type="text" name="selectCommesse" value="ALL" readonly  style ="font-size:12px;">
                                            </td>
                                            <td>
                                                <div id="dialog2-form" title="Commesse">
                                                  <form>
                                                        <h4>Seleziona Commesse</h4>
                                                        <input type="text" id = "commesse_filter" value = ''  placeholder="Filtra" onkeyup="listFilter('commesse',this.value)">
                                                        <fieldset id="commesseList" style ="padding:0; border:0;" ></fieldset>
                                                  </form>
                                                </div>
                                                <input type="hidden" id = "id_commesse" value = ''>
                                            </td>
                                        </tr>
					<tr>
                                            <td>Fornitore</td>
					</tr>
					<tr>
                                            <td>
                                                <select id="fornitori"></select>
                                            </td>
                                            <td ><input type="button" onclick="viewPdf()" value="Crea"></td>
					</tr>
				</table>
			</div>
		</body>
	</htmL>
EOD;

print $content;

?> 
