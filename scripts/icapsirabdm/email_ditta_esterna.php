<?php
if(!function_exists("generatePdf")){
function generatePdf($db,$f_code,$params)
{
    try {
        //-------------------------------------------------------------------------------------------
        $selOp = new Zend_Db_Select($db);
        //creazione_pdf
        $script_name = 'print_fromtemplate';
        $res_template = $selOp->from('t_scripts', ['f_script'])
                            ->where("f_name like '" . $script_name . "'")
                            ->query()->fetch();

        if ($res_template) {
           $params['attach_params']="77752|fc_doc_attach|t_wares";//PROD
           //$params['attach_params']="77752|fc_doc_attach|t_wares";//STAGING
           eval($res_template['f_script']);
        } else {
           return false;
        }

        $strfile = $pdf->Output("doc.pdf", "S");
        //var_dump($strfile); die;

        $attachment = array(
                "path" => "",
                "filename" => "odl_" . $params['progress_custom'] . ".pdf",
                "content" => $strfile,
                "mime" => "application/pdf"

        );

    } catch (Exception $ex) {
        $message = '[icapsirabdm - PdfCustomGenerate] '.$ex ->getMessage();
        $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>$message,'f_type_log'=>88));
       return false;
    }

    return $attachment;
}

function getAttachmentsParams($arrs,$db)
{
    $attachments = array();
    $i=0;
    $attach_path =APPLICATION_PATH."/../attachments/icapsirabdm/doc/DIR/ATCHNAME_doc";
    $atch_temp_path =APPLICATION_PATH."/../attachments/icapsirabdm/temp/ATCHNAME";
    foreach($arrs as $key => $atch)
    {
        $ath_arr = explode( '|', $atch, 2);        
        //print_r(array($ath_arr,explode('_', $ath_arr[0], 3))); die();
        if($ath_arr!= FALSE && sizeof($ath_arr) == 2)
        {
            $tmp = explode('.',$ath_arr[1]);
            $ext = end($tmp);
            $ext = ($ext != FALSE)?'.'.$ext:'';
            $dir = explode('_', $ath_arr[0], 3);
            
            if($dir != FALSE && sizeof($dir) == 3) // attachment salvato in attachments/doc
            {
                $path = str_replace("DIR",$dir[2] ,$attach_path);
                $path = str_replace("ATCHNAME",$ath_arr[0],$path).$ext;
            }
            else if($dir != FALSE && sizeof($dir) == 2)  //attachment salvato in attachments/temp
            {
                $path = str_replace("ATCHNAME",$ath_arr[0],$atch_temp_path).$ext;
            }
            try
            {
                $filename = $ath_arr[1];
                $mimetype = Mainsim_Model_Utilities::mime_content_type($filename);
                $content = file_get_contents($path);
                $attach = array(
                     "path" => "",
                     "filename" => $filename,
                     "content" => $content,
                     "mime" => $mimetype

                );

                $attachments[$key] = $attach;
                $i++;
            }
            catch(Exception $ex) {
                $message = '[icapsirabdm - email_ditta_esterna.php - getAttachmentsParams] '.$ex ->getMessage();
                $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>$message,'f_type_log'=>88));
            }
        }
    }

    return  $attachments;
}

function moveToFolderAndCrossToWo($woId,$file,$db){
    $folder_code = '78300'; //PROD
    //$folder_code = '78186'; //STAGING

    //cross WO Ware (DOC)
    $cross = array();
    $cross['f_wo_id'] = $woId;
    $cross['f_timestamp'] = time();
    $cross['f_ware_id'] = $file["f_code"];
    $db->insert('t_ware_wo', $cross);
    
    //update ware
    $db->update('t_wares', ['fc_doc_type'=>'permesso_lavoro'],'f_code = '.$file["f_code"]);

    //cambio parentela
    $parent = array('f_parent_code' => $folder_code,'f_timestamp'=>time());
    $db->update('t_wares_parent', $parent,'f_code = '.$file["f_code"]);
    $hie_codes = $folder_code.',0';
    $db->update('t_creation_date', ['fc_hierarchy_codes'=>$hie_codes],'f_id = '.$file["f_code"]);
    //var_dump([$woId,$file]); die(" moveTo");
}

function saveFile($db,$woId,$attach)
{
    $strfile = $attach['content'];
    $filename = $attach['filename'];
    $type = $attach['mime'];
    
    try 
    {
        $upload = array();
        $upload = new Mainsim_Model_Uploads($db);
        $data_pdf = array(
             array(
                 'name' => $filename,
                 'type' => $type,
                 'tmp_name' => $strfile,
                 'error' => "",
                 'size' => "100"
            )
         );
         $resp = $upload->multipleUploadsFromDragDrop($data_pdf, 1);
         if(!empty($resp))moveToFolderAndCrossToWo($woId,$resp[0],$db);

   } catch (Exception $ex) {
       $message = '[icapsirabdm - cross wo-allegato] '.$ex ->getMessage();
       $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>$message,'f_type_log'=>88));
       return false;
   }
    
    return true;    
}
 
//ICAP Sira email Trasmissione ditta Esterna
function sendEmail($db,$f_code,$params,$attachments,$userRichiedente)
{
    try{
        $selOp = new Zend_Db_Select($db);
        $to = array();
        $cc = array();
        $bcc = array();

        $time = date('Y-m-d H:i');
        $timestamp = strtotime($time);

        /////////////////////////////////
        //		WO
        /////////////////////////////////
        $selOp->reset();
        $data_wo = $selOp->from('t_workorders',array('f_start_date','data_pianificata','f_end_date','f_priority','f_type_id','fc_wo_asset'))
                        ->join('t_creation_date', 't_creation_date.f_id = t_workorders.f_code',array('f_title','f_creation_date','f_description','fc_editor_user_name','fc_progress'))
                        ->join('t_custom_fields', 't_custom_fields.f_code = t_workorders.f_code', array('prod_mov','disp_imp','progress_custom','cdc'))
                        ->where('t_workorders.f_code = '.$f_code)
                        ->query()->fetch();
        
        /////////////////////////////////
        // CDC
        /////////////////////////////////
        $cdc = $data_wo['cdc'];
        $cdc = json_decode($cdc,true);

        $data_cdc = (empty($cdc))?'':implode(",", $cdc['labels']);
        
       
        //3) Recupero i destinatari che sono associati alla ditta esecutrice
        $selOp->reset();
        $data_mail_to = $selOp->from("t_ware_wo as ww",[])
                            ->join("t_wares as w", "w.f_code = ww.f_ware_id",['fc_vendor_email','fc_vendor_resp_email'])
                            ->where("ww.f_wo_id = ".$f_code)
                            ->where("w.f_type_id = 6")
                            ->query()->fetchAll();
        
        foreach($data_mail_to as $mail){
            if(!empty($mail["fc_vendor_email"])) array_push($to,$mail["fc_vendor_email"]);
            if(!empty($mail["fc_vendor_resp_email"])) array_push($to,$mail["fc_vendor_resp_email"]);
        }
        
        //DATA ASSET
        $selOp->reset();
        $assets = $selOp->from('t_ware_wo as ww',[])
                        ->join("t_creation_date as cd", "cd.f_id = ww.f_ware_id",['f_title as asset_name'])
                        ->join("t_wares as wa", "wa.f_code = ww.f_ware_id AND wa.f_type_id = 1",["f_code as asset"])
                        ->where('ww.f_wo_id = '.$f_code)
                        ->query()->fetchAll();
        
        $data_asset = (empty($assets))?'':implode(",", array_column($assets,'asset_name'));
        
        //////////////////////////////////
        //		INFO WO
        /////////////////////////////////
        //Priorità
        $apriority = array("1"=>"1 - nel più breve tempo possibile","2"=>"2 - entro 5 giorni lav.","3"=>"3 - oltre 5 giorni lav.");
        $v = $data_wo['f_priority'];
        $textPriority = $apriority[$v];

        //Numero Progressivo WO
        //$progress_wo = sprintf("%05d", $data_wo["fc_progress"]);
        $progress_wo = $data_wo["progress_custom"];
        //Date
        $disponibilita_da = (($data_wo['data_pianificata'] > 0) ? date('d/m/Y, H:i',$data_wo['data_pianificata']) : '');
        $disponibilita_da = "<i>DA</i> " .$disponibilita_da; 
        $disponibilita_a = (($data_wo['f_end_date'] > 0) ? date('d/m/Y, H:i',$data_wo['f_end_date']) : '');
        $disponibilita_a = " - <i>A</i> ".$disponibilita_a;

        if (!empty($data_wo['disp_imp'])) {
                $disponibilita_da = $data_wo['disp_imp'];
                $disponibilita_a = "";
        }

        //Destinatari
        array_push($cc,"m.luppichini@icapsira.com");

        /////////////////////////////////
        //		Send mail
        /////////////////////////////////

        $mail_action = new Mainsim_Model_Mail($db);
        $oggetto = "[ICAP Sira BDM] MTZ n. ".$progress_wo;
        $text = "L'ordine di lavoro n. ".$progress_wo." e' stato assegnato in data: " .date('d/m/Y, H:i')
                .".<br><br><fieldset><legend><i>Dettagli</i></legend><br><b>Apparecchiatura:</b> ".(($data_wo['f_title']=="")?$data_asset:$data_wo['f_title'])
                ."<br><br><b>CDC:</b> ".$data_cdc
                ."<br><br><b>Descrizione:</b> ".$data_wo['f_description']
                ."<br><br><b>".htmlentities("Disponibilità")." impianto:</b>  ".$disponibilita_da.$disponibilita_a
                ."<br><br><b>Ultimo prodotto movimentato:</b> ".$data_wo['prod_mov']
                ."<br><br><b>Richiedente:</b> ".$userRichiedente
                ."<br><br><b>".htmlentities("Priorità").":</b> ".htmlentities($textPriority)."</fieldset>";
        
        //var_dump([$to,$oggetto,$text,$attachments]); die("mail");
        if(!empty($to)){
                $mail_action->sendMail(array('To'=>$to, 'Cc'=>$cc, 'Bcc'=>$bcc),$oggetto,$text,
                        $attachments,array('content-type'=>'text/html'));
        }

    } catch (Exception $ex) {
        $message = "[Icapsirabdm (email_ditta_esterna.php)] ".$ex->getMessage();
	$db->insert("t_logs", array('f_timestamp'=>time(), 'f_log'=>$message, 'f_type_log'=>88));
        return false;
    }

    return true;
}
}

$multi_phase_batch = (empty($params['f_type_id']) && !empty($f_code))?true:false;
$db = (empty($db))?$this->db:$db;
//se si è nel multipassaggio di fase recupero i params necessari che mancano
if($multi_phase_batch){
	$selOp = new Zend_Db_Select($db);
	$res = $selOp->from('t_creation_date as cd',['fc_progress'])
                                ->join("t_workorders as wo", "wo.f_code = cd.f_id",['f_type_id'])
				->join("t_custom_fields as cf", "cf.f_code = cd.f_id",['ditta_esterna','progress_custom','permesso_attach'])
				->joinLeft("t_ware_wo as ww", "ww.f_wo_id = cd.f_id",[])
				->joinLeft("t_wares as wa", "wa.f_code = ww.f_ware_id AND wa.f_type_id = 1",["f_code as asset"])
				->where("cd.f_id = ".$f_code)
				->query()->fetch();
        
        foreach ($res as $key => $value) {
            $params[$key] = $value;
        }
	//Numero Progressivo WO : aggiungo eventuali 0 mancanti(se il numero ha meno di 5 cifre)
	$params['fc_progress'] = sprintf("%05d", $res["fc_progress"]);

	//controllo campo obbligatorio
	if(empty($res['ditta_esterna']) || empty(json_decode($res['ditta_esterna']))) 
		die("Impossibile eseguire il passaggio di fase, [N. OdL ".$params['fc_progress']."]: Il campo 'Ditta esecutrice' non e' compilato");

	if(!empty($res['asset'])){
		$params['t_wares_1']['f_code'] = [$res['asset']]; 
	}
}

if($params['f_type_id']  == 13){
    try
    {
        //var_dump($params); die("email_ditta_esterna");
        $session_1 = $_SESSION['Zend_Auth'];
        $user = $session_1['storage']->f_displayedname;
        $db->update("t_custom_fields",array("ri_delegato" => $user),"f_code = $f_code");
        $attachments = [];
        $customPdf = generatePdf($db,$f_code,$params);
        
        if($customPdf !== false && !empty($customPdf)) array_push($attachments, $customPdf);
        $atch_arr = array("permesso_attach"=>$params['permesso_attach']);
        $res_atch = getAttachmentsParams($atch_arr,$db);
        //var_dump($res_atch); die("££££");
        $permesso_atch = $res_atch['permesso_attach']; 
        if(!empty($permesso_atch)){
          array_push($attachments, $permesso_atch);
          saveFile($db,$f_code,$permesso_atch);
        }

        if($customPdf !== false && !empty($customPdf)){
            sendEmail($db,$f_code,$params,$attachments,$user);
        }
    }
    catch (Exception $ex) {
        $message = "[Icapsirabdm (email_ditta_esterna.php)] ".$ex->getMessage();
        $db->insert("t_logs", array('f_timestamp'=>time(), 'f_log'=>$message, 'f_type_log'=>88));
    }
}