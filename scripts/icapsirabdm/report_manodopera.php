<?php
class C extends PDF_MC_Table{
	function Header(){
		$installation = new Mainsim_Model_Mainpage();
		$settings = $installation->getSettings();
		$this->SetTextColor(0,0,0);
		$this->SetFont('Arial','B',20);
		$this->Image(APPLICATION_PATH."/../public/msim_images/default/_pdf/logo.png");
		//$this->Cell(100,7,$settings['MAINSIM_PROJECT_NAME'],0,0,'R');
                $this->Image(APPLICATION_PATH."/../public/msim_images/icapsirabdm/_pdf/logo.png",241,9,45,17);
		$this->SetXY(10,6);
		$this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
		$this->rect(10, 27, 275, 1.6, "F");
		$this->SetY(30);
	}

	function Footer(){
            $session_1 = $_SESSION['Zend_Auth'];
            $user_session = $session_1['storage']->f_displayedname;
            $this->SetXY(10,200);
            $this->setfillcolor($this->R[0] = 74,$this->G[0] = 102,$this->B[0] = 113);
            $this->rect(10, 200, 275, 1.6, "F");
            $this->Ln(20);
            $this->SetXY(8.7,203);
            $this->SetFont('Arial','B',6);
            $this->Cell(100,5,"mainsim"." - what maintenance can be - www.mainsim.com ");
            $this->SetXY(185,203);
            $this->AliasNbPages();
            $this->Cell(100,5,$GLOBALS['trad2']->_('Page') . ' '.$this->PageNo().' - ' . $GLOBALS['trad2']->_('Printed by') . ' ' . $user_session . ' - ' . date('d-m-Y'),0,0,'R');
	}
}
class translator
{
    private $traductor;
    private $voc;
    public function __construct() 
    {
            $this->traductor = new Mainsim_Model_Translate();
            $this->voc = $this->traductor->getLanguage($this->traductor->getLang());
    }	
    public function translate($string){
            $res = $string;
            if($this->traductor->_($string)!=null) $res = $this->traductor->_($string);
            return $res;
    }
}

function labor_paircross($labors,$since,$till,$contabile = false,$select)
{
    /******* Associazione Manodopera - ore lavorative - wo by data *******/
    $tariffe = ['standard','overtime','holiday','overnight','ritiro_materiale','chiamata_feriale_prima_ora','chiamata_feriale_seconda_ora','chiamata_nott_prima_ora','chiamata_nott_seconda_ora','lavoro_sabato'];
    $slcstr = "SUM(IF(fc_rsc_fee_type = 'TARIFFA', fc_rsc_total_hours, 0)) as TARIFFA";
    $cols = ["pc.f_code_cross","FROM_UNIXTIME(pc.fc_rsc_since,'%d-%m-%Y') as date"];
    foreach ($tariffe as $value) $cols [] = str_replace("TARIFFA",$value ,$slcstr);
    
    $dates = array();
    
    $select->reset();
    $query = $select->from(array('pc' => 't_pair_cross'),$cols)
    ->join(array('cdl' => 't_creation_date'),"cdl.f_id = pc.f_code_cross",[])
    ->join(array('cd' => 't_creation_date'),"cd.f_id = pc.f_code_main",['f_id','f_title','f_description','fc_progress','f_category'])
    ->join(array('wp' => 't_wf_phases'),"wp.f_wf_id = cd.f_wf_id AND wp.f_number = cd.f_phase_id",[])
    ->join(array('cf' => 't_custom_fields'),"cf.f_code = cd.f_id",['cdc','commessa'])
    ->join(array('wo' => 't_workorders'),"wo.f_code = cd.f_id",[])
    ->order(['cdl.f_title','pc.fc_rsc_since','cd.fc_progress'])
    ->group(["pc.f_code_cross","FROM_UNIXTIME(pc.fc_rsc_since,'%d-%m-%Y')","pc.f_code_main"])
    ->where("pc.f_title = 'Job'")
    ->where('pc.f_code_cross IN ('.implode(",", $labors).')')
    ->where('pc.fc_rsc_since >= ?',$since)
    ->where('pc.fc_rsc_since <= ?',$till)
    ->where('wo.f_type_id  IN (1,4)') //'CORRECTIVE','PLANNED'
    ->where("wp.f_group_id NOT IN (7,8)"); 
    
    if($contabile) $select->where("wp.f_number = 9");
    else $select->where("wp.f_number NOT IN (9)");            
//    $sql = $select->__toString();
//    die($sql);
    $result = $select->query()->fetchAll();
    $data = [];
    $data['ore'] = array_combine($tariffe, array_fill(0, count($tariffe), 0));
    $data['labors'] = [];
    
    foreach($result as $value){
        $labor = $value['f_code_cross'];
        $date = $value['date'];
        unset($value['f_code_cross']); unset($value['date']);
        if(!isset($data['labors'][$labor])) $data['labors'][$labor] = [];
        if(!isset($data['labors'][$labor][$date])) {
            $data['labors'][$labor][$date]['ore'] = array_combine($tariffe, array_fill(0, count($tariffe), 0));
            $data['labors'][$labor][$date]['jobs'] = [];
        }
        
        $ore = array_intersect_key($value ,array_flip($tariffe));
        $wo = array_intersect_key($value ,array_flip(array_diff(array_keys($value), $tariffe)));
        $temp = ["ore"=>$ore,"wo"=>$wo];
        array_push($data['labors'][$labor][$date]['jobs'],$temp);
        
        foreach ($tariffe as $tariffa) {
            $data['labors'][$labor][$date]['ore'][$tariffa] += $ore[$tariffa];
            $data['ore'][$tariffa] += $ore[$tariffa];
        }
    } 

    return $data;
}

function setFontSizeByWidthSingleRow($pdf,$text,$text_width,$minSize,$maxSize,$font){
    $title_sizes = range($minSize,$maxSize);
    for($i=count($title_sizes)-1; $i>=0 ; $i--) 
    {
        $size = $title_sizes[$i];
        $pdf->SetFont($font['family'], $font['style'], $size);

        if($pdf->GetStringWidth($text) <= $text_width){
                break;
        }
    }

    return $size;
}

function checkIfAddPage($pdf,$jobs,$i){
//    $pdf->SetDrawColor(255,133,102);
//    $pdf->Line(0,$pdf->GetY(), 295, $pdf->GetY());
    
    $rows = ($i == 0)? 2 : 0; //(i==0)? conta le 2 righe di nome e cognome, qualifica
    $rows+=2; //data
    $dY = ($i == 0)? 1 : 5; //conta lo spazio iniziale
    
//    $pdf->SetDrawColor(0, 153, 51);
//    $pdf->Line(5,$pdf->GetY()+($rows*5+$dY), 290, $pdf->GetY()+($rows*5+$dY));
    
    //$coc = empty($wo['cdc'])?json_decode($wo['commessa'], 1):json_decode($wo['cdc'], 1);      
    //if(!empty($coc)) $coc = implode(',', $coc['labels']);        
    //$descr = ($wo['f_category'] == "CORRECTIVE")?$wo['f_description']:$wo['f_title'];
    
    //$rows += max($pdf->NbLines(40,$coc),$pdf->NbLines(145,$descr))+1; // (cdc o commesse,descrizione)+intestazione
    $rows += count($jobs) +3; // jobs(x) + righe intestazioni(2) + totali(1)
    $dY += 5* $rows;

//    $pdf->SetDrawColor(153,230,1);
//    $pdf->Line(10,$pdf->GetY()+ $dY, 275, $pdf->GetY()+ $dY);
//    $pdf->SetDrawColor(10,7,237);
//    $pdf->Line(5,200, 295, 200);
    
    if($pdf->GetY()+ $dY >= 200)
    {
        $pdf->SetLineWidth(0.3);
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(1,1);
        $pdf->SetXY(10,30);
        $X = 10; $Y = 30;
    }
//    else $pdf->SetLineWidth(0);
//    $pdf->SetDrawColor(0,0,0);
    return $dY;
}

 $pdf = new C('L','mm','A4');
 /*per conoscere la lingua*/

$currency_sign = $_SESSION["CURRENCY_SIGN"];
 /*OTTENERE IL TRADUTTORE*/
 
 $traductor = new Mainsim_Model_Translate();
 $voc = $traductor->getLanguage($traductor->getLang());

$GLOBALS['trad'] = $voc;
$GLOBALS['trad2'] = $traductor;

$mo_t = new translator();
 
 $pdf->AddFont('Arial', '', 'OpenSans-Light.php');
 $pdf->AddFont('Arial', 'B', 'OpenSans-Regular.php');
 $session = $_SESSION['Zend_Auth'];
 $language_session = $session['storage']->f_language;


 $select->reset();
 $language_query=$select->from(array('t1' => 't_localization'),array("f_datetime"))->where('t1.f_id=?',$language_session)->query()->fetch();
 $correct_sint_lang = str_replace("{", "", $language_query);
 $correct_sint_lang_final = str_replace("}", "", $correct_sint_lang);
 $date_string_lang = $correct_sint_lang_final['f_datetime'];
$select->reset();

$since =  $_GET['datesince']; $till =  $_GET['dateto'];
$since = DateTime::createFromFormat("Y-m-d", $since);
$since->setTime(0,0,0);
$since = $since->getTimestamp();
$till = DateTime::createFromFormat("Y-m-d", $till);
$till->setTime(23,59,59);
$till = $till->getTimestamp();
$vendor_fcode = $_GET['fornitore'];

//recupero f_title del vendor
$vendor = $select->from(array('cd' => 't_creation_date'),array('f_title'))
 ->where("cd.f_id = ".$vendor_fcode)->query()->fetch();
$select->reset();
$vendor = $vendor['f_title'];

//print_r(array($since,$till,$vendor)); die();

/******* Query per Recupere le manodopere crossate al fornitore *******/
$labors = $select->from(array('wa' => 't_wares'),array('fc_rsc_skill'))
->join(array('wr' => 't_wares_relations'),"wr.f_code_ware_slave = wa.f_code",[])
->join(array('cd' => 't_creation_date'),"cd.f_id=wa.f_code",array('f_title','f_id'))
->order('cd.f_title')
->where("wa.f_type_id = 2")->where("wr.f_code_ware_master = $vendor_fcode")->query()->fetchAll();
$select->reset();
/****************************************/

if(empty($labors)) return;

foreach($labors as $labor){
    $labors_title[$labor['f_id']] = $labor['f_title'];
    $labors_skill[$labor['f_id']] = $labor['fc_rsc_skill'];
}

/******* Query per Recupere le ore di lavoro di ogni manodopera, ordinate per data e fc_progress dei wo referenti *******/ 
$data = labor_paircross(array_column($labors, "f_id"),$since,$till,!empty($_GET['contabile']),$select);
/****************************************/

//print_r($data); die;


$tariffe = ['standard','overtime','holiday','overnight','ritiro_materiale','chiamata_feriale_prima_ora','chiamata_feriale_seconda_ora','chiamata_nott_prima_ora','chiamata_nott_seconda_ora','lavoro_sabato'];
setlocale(LC_TIME, 'ita');
$cdc_ore = array();
$commesse_ore = array();
$TotaliRecapLabels = ['Standard','Straordinario','Festivo','Notturno','Ritiro materiale','Ch. feriale (1° ora)',
    'Ch. feriale (2° ora e succ.)','Ch. Notturna/Festiva (1° ora)','Ch. Notturna/Festiva (2° ora e succ.)','Lavoro Sabato','TOTALE'];
foreach($TotaliRecapLabels as $key => $value) $TotaliRecapLabels[$key] = iconv('UTF-8', 'windows-1252', $TotaliRecapLabels[$key]);
    

$TotaliRecap = $data['ore'];
$TotaliRecap['tot'] = array_sum(array_values($data['ore']));

//foreach($cdc_wo as $cdc => $wos){
//    foreach($tariffe as $tariffa){
//        $cdc_ore[$cdc][$tariffa] += array_sum(array_column($wos, $tariffa));
//        $TotaliRecap[$tariffa] +=  $cdc_ore[$cdc][$tariffa];
//    }
//    $cdc_ore[$cdc]['tot'] += array_sum(array_column($wos,'tot'));
//    $TotaliRecap['tot'] +=  $cdc_ore[$cdc]['tot'];
//}
    
 $pdf->SetLineWidth(0.1);
 $pdf->AddPage();
 $pdf->SetAutoPageBreak(1,1);

$pdf->SetXY(9,$pdf->GetY());
$pdf->SetFont('Arial','B',10);
$pdf->SetTextColor(255, 0, 0);
$title = (empty($_GET['contabile']))?'REPORT MANODOPERA':'REPORT MANODOPERA - CHIUSURA CONTABILE';
$pdf->Cell(275,7,$title,0,'','C');
$pdf->SetTextColor(0, 0, 0);

 $Y=$pdf->GetY()+9;
 $pdf->SetXY(10,$Y);
 $pdf->SetFillColor(204,229,255);
 $pdf->SetLineWidth(0.3);
 $pdf->Rect(10,$Y,275,8,'F');
 $pdf->SetFont('Arial','B',12);	
 $pdf->Cell(275,8,$vendor." [ ".strftime("%d %B %Y",$since)." - ".strftime("%d %B %Y",$till)." ]",1,'','C');
 $Y=$pdf->GetY()+8;
 
 $X = 10;
 $pdf->SetXY(10,$Y);
 $pdf->SetFont('Arial','B',8);
 $pdf->Cell(20,10,'ORE MESE',1,'','C');
 
 $Widths = array(20,20,20,20,25,20,30,30,30,20,20); 
 $Widths = array_combine(array_keys($TotaliRecap),$Widths);
 $Colors = array([178,255,102],[255,178,102],[255,255,128],[153,221,255],[217,177,140],[236,198,217],[230,179,204],[173,235,173],[153,230,153],[209,179,255],[255,133,102]);
 $Colors = array_combine(array_keys($TotaliRecap),$Colors);
 //var_dump($Colors); die("colors");
  $X = 30;
  
  foreach ($Colors as $key => $value) {
    //var_dump($value); die("colors");
    $pdf->SetFillColor($value[0],$value[1],$value[2]);
    $width = $Widths[$key];
    $pdf->SetXY($X,$Y);
    $fill = ($TotaliRecap[$key]>0)?'F':'';
    $pdf->Rect($X,$Y,$width,10,$fill);
    $X += $width;
  }

 $pdf->SetFont('Arial','B',8);
 $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C'));
 $pdf->SetWidths(array_values($Widths)); 
 $pdf->SetXY(30,$Y);
 $pdf->Row($TotaliRecapLabels);
 $pdf->SetXY(30,$Y+10);
 $pdf->Row(array_values($TotaliRecap));

 /**************************/
  /***** MANODOPERA *******/
 /**************************/
 $X = 10;
 $Y = $pdf->GetY()+7;
 $pdf->SetXY($X,$Y);

$labors_jobs= $data['labors'];
foreach($labors_jobs  as $labor_code =>$labor_dates)
{
    $i=0;
    foreach($labor_dates as $date => $info){
        $jobs = $info['jobs'];
        $dY = checkIfAddPage($pdf,$jobs,$i);
        if($i == 0)
        {
            $X = 10;
            $Y = $pdf->GetY();

            //NOME
            $pdf->SetXY($X,$Y);
            $pdf->SetFillColor(209,209,224);
            $pdf->Rect(10,$pdf->GetY(),60,5,'F');
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(60,5,$mo_t->translate('Nome e Cognome'),1,'','C');
            $pdf->SetXY($X,$Y+5);
            $pdf->SetFont('Arial','BI',8);
            $font = array("family"=>'Arial',"style"=>'BI');
            setFontSizeByWidthSingleRow($pdf,$labors_title[$labor_code],60,7,9,$font);
            $pdf->Cell(60,5,$labors_title[$labor_code],1,'','C');
            $pdf->SetFont('Arial','B',8);

            //QUALIFICA
            $pdf->SetXY(70,$Y);
            $pdf->Rect(70,$pdf->GetY(),40,5,'F');
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(40,5,$mo_t->translate('Qualifica'),1,'','C');
            $pdf->SetXY(70,$Y+5);
            $pdf->SetFont('Arial','BI',8);
            $font = array("family"=>'Arial',"style"=>'BI');
            setFontSizeByWidthSingleRow($pdf,$labors_skill[$labor_code],40,7,9,$font);
            $pdf->Cell(40,5,$labors_skill[$labor_code],1,'','C');
            $pdf->SetFont('Arial','B',8);
            $pdf->SetY($pdf->GetY()+1);
        }
        
        $pdf->SetXY(10,$pdf->GetY()+5);
        $pdf->SetFont('Arial','I',8);
        $pdf->SetAligns(array('C'));
        $pdf->SetWidths(array(20)); 
//        $pdf->SetAligns(array('C','C','C'));
//        $pdf->SetWidths(array(20,40,145)); 
//        $lbl = empty($wo['cdc'])?'':'Cdc';
//        $lbl = empty($wo['commessa'])?$lbl:'Commessa';
        
        //$pdf->Row(array($mo_t->translate('N. OdL'),$mo_t->translate($lbl),$mo_t->translate('Description')));
        $pdf->Row(array($mo_t->translate('Date')));

//        $coc = empty($wo['cdc'])?json_decode($wo['commessa'], 1):json_decode($wo['cdc'], 1);      
//        if(!empty($coc)) $coc = implode(',', $coc['labels']);
        
        $X = 10;
        $Y = $pdf->GetY();
        $pdf->SetXY($X,$Y);
        $pdf->SetFont('Arial','',8);
        $pdf->SetLineWidth(0.1);
        $pdf->Row(array($date));
//        $pdf->Row(array(sprintf("%05d", $wo['fc_progress']).( ($wo['f_category'] == "CORRECTIVE")?"":(($wo['f_category'] == "PLANNED")?" PM":" SORV"))
//            ,$coc,($wo['f_category'] == "CORRECTIVE")?$wo['f_description']:$wo['f_title']));

        //$pdf->SetY($pdf->GetY()+1);
        $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C','C'));
        $pdf->SetWidths(array_merge([20],array_values($Widths)));
        $pdf->SetFont('Arial','B',8);
        
        foreach ($jobs as $k => $job) {
            $wo = $job['wo'];
            $ore = $job['ore'];
            
            $fc_progress =
                    sprintf("%05d", $wo['fc_progress']).
                    ( ($wo['f_category'] == "CORRECTIVE")?"":(($wo['f_category'] == "PLANNED")?" PM":" SORV"));

            //$hrs [] = '';
            if($k == 0){
//                var_dump($job); die;

                $x = 30; $y = $pdf->GetY();
                for($j = 0;  $j < count($tariffe); $j++)
                {
                   $width = $Widths[$tariffe[$j]];
                    if($info['ore'][$tariffe[$j]] > 0){
                        $rgb = $Colors[$tariffe[$j]];
                        $pdf->SetFillColor($rgb[0],$rgb[1],$rgb[2]);
                        $pdf->Rect($x,$y,$width,10,'FD');
                    }
                    $x+=$width;
                }
                
                $rgb = $Colors['tot'];
                $pdf->SetFillColor($rgb[0],$rgb[1],$rgb[2]);
                $pdf->Rect($x,$y,20,10,'FD');
                $labels = $TotaliRecapLabels;
                array_unshift($labels,'N. OdL');
                //var_dump($labels); die("xxxx4");
                $pdf->Row($labels);
            }
            $pdf->SetFont('Arial','',8);
            $row = array($fc_progress);
            //$JobHours = array_map(function ($ore) { return (($ore > 0)?$ore:''); }, array_values($job));
            //$row = array_merge($row,array_values($job),[array_sum(array_values($job))]);
            $row = array_merge($row,array_values($ore),['']);
            $pdf->Row($row);
        }
        
        $pdf->SetWidths(array_values($Widths));
        $pdf->SetX(30);
         $pdf->SetFont('Arial','BI',8);
         $pdf->SetLineWidth(0.1);
         $JobsTot  = array_values($info['ore']);
         $JobsTot [] = array_sum($JobsTot);
         //print_r($JobsTot); die();
        $pdf->Row($JobsTot);

//        if($i == count($wos) -1 ){
//           $pdf->SetY($pdf->GetY()+5);
//        }
        $i++;
    }
    $pdf->SetY($pdf->GetY()+5);
}
