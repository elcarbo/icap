<?php

$select = new Zend_DB_Select($this->db);
function setCurlRobottinoNecuHvala($db, $url, $id, $userId) {
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL,$url);
	//curl_setopt($ch, CURLOPT_URL,urlencode($url));
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 1);
	$head = curl_exec($ch); 
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$head == false && $status != 200 &&
	    $db->update('t_excel_import', ['f_error' => $status.' '.curl_error($ch),'f_status' => -1,'f_user_id'=>$userId], 'f_transact_id='.$id);	
	curl_close($ch); 
	return;
}

function saveFile($fileInfo,$db){
try{
    $parenTitle = $fileInfo['parent_title'];
    $fc_doc_attach = $fileInfo['fc_doc_attach']; $fc_doc_author = '';
    $t_wares_parent_5 = array("f_code"=>[],"f_code_main"=>0,"f_type"=>5,"Ttable"=>"t_wares_parent","f_module_name"=>"mdl_doc_parent_tg");
    $select = new Zend_Db_Select($db);
    $res_folder = $select->from(["t1"=>"t_wares"],['f_code'])
        ->join("t_creation_date","t_creation_date.f_id = t1.f_code ",["f_title"])
        ->where("t1.f_type_id = 5")->where("f_title = '".$parenTitle."'")
        ->query()->fetch();  

    if(empty($fc_doc_attach) || empty($res_folder)) return;
    
    if(!empty($fileInfo['userId'])){
        $userId = $fileInfo['userId'];
        $select ->reset();
        $user_us_cols = ['fc_usr_firstname','fc_usr_lastname'];
        $user = $select->from('t_users',$user_us_cols)
        ->where("t_users.f_code = $userId")->query()->fetch();
        $fc_doc_author = $user['fc_usr_firstname'].' '.$user['fc_usr_lastname'];
    }
    
    $t_wares_parent_5["f_code"] = array($res_folder["f_code"]);
    $tmp = explode("|", $fc_doc_attach);
    $sid = $tmp[0]; $filename = $tmp[1];
    
    $f_title = "Import DDT - ".strftime("%d/%m/%Y %H:%M",time());
    $f_description = "";
    if(!empty($fileInfo['f_stats'])){
        $stats = json_decode($fileInfo['f_stats'],true);
        $f_description = "Elementi Totali: ".$stats["total"] .
                "\rElementi Aggiornati: ".$stats["update"] .
                "\rElementi Non Trovati: ".$stats["not_found"].
                "\rElementi Bloccati: ".$stats["locked"];
    }
    
    $newWare_params = array(
        "f_type_id"=>5,
        "f_code"=> "",
        "f_phase_id"=> "",
        "f_module_name"=> 'mdl_doc_tg',
        "fc_doc_type"=>'excel_ddt',
        "fc_doc_attach"=>$fc_doc_attach,
        "fc_doc_author" => $fc_doc_author,
        "attachments"=>"fc_doc_attach",
        "f_title"=>$f_title,
        "f_description" => $f_description,
        "transact_id" => $fileInfo['rid'],
        "t_wares_parent_5" => $t_wares_parent_5
    );

    $wares = new Mainsim_Model_Wares($db);
    $res = $wares->newWares($newWare_params,[],false,true);
} catch (Exception $e){
    $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>"[computeExcel - saveFile] =>\r".$e->getMessage(),'f_type_log'=>71));
    //die();
}
}

// inizio import
if(isset($params['curl']) && $params['curl'] == 1):	
    // controllo se c'e' già qualche import in corso
    $res = $this->db->query("select * from t_excel_import where f_status = 1")->fetchAll();
    if(count($res) > 0){
            print(json_encode(array('code' => 3, 'message' => "Import in corso", 'id' => $res[0]['f_transact_id'])));			
    }
    else{
        $id = $params['rid']; $fc_doc_attach = $params['fc_doc_attach'];
        $tmp = explode("|", $fc_doc_attach);$sid = $tmp[0]; $name = $tmp[1];
        $tmp2 = explode(".", $name);$ext = array_pop($tmp2);$filename = $sid.".".$ext;
        $userId = $params['userId'];
        
        $this->db->insert('t_excel_import', ['f_transact_id' => $id, 'f_status' => 1, 'f_percent' => "0",'f_user_id'=>$userId]);
        setCurlRobottinoNecuHvala($this->db, $_SERVER['SERVER_NAME'] .'/scripts/icapsirabdm/excel_costi_materiali.php?rid='.$id.'&filename='.$filename.'&userId='.$userId, $id , $userId);
        print(json_encode(array('code' => 1, 'message' => 0, 'id' => $id)));
    }	
    die;
endif;

// controllo import
if(isset($params['curl']) && $params['curl'] == 0):
	$res = $select->from('t_excel_import')->where('f_transact_id='.$params['rid'])->query()->fetch();
	if($res['f_status'] == 1){
		print(json_encode(array('code' => $res['f_status'], 'message' => $res['f_percent'])));			
	}
	elseif($res['f_status'] == -1){
		print(json_encode(array('code' =>$res['f_status'], 'message' => $res['f_error'])));
	}
	else{
            $id = $params['rid']; $fc_doc_attach = $params['fc_doc_attach'];
            $tmp = explode("|", $fc_doc_attach);$sid = $tmp[0]; $name = $tmp[1];
            $tmp2 = explode(".", $name);$ext = array_pop($tmp2);
            $filename = $sid.".".$ext;
            $userId = $params['userId'];
            $fileInfo = array();
            $fileInfo['fc_doc_attach'] = $fc_doc_attach;$fileInfo['rid'] = $id;$fileInfo['parent_title'] = "Gestione Materiali";$fileInfo["userId"]=$userId;
            $fileInfo["f_stats"] = $res['f_stats'];
            saveFile($fileInfo,$this->db);
            print(json_encode(array('code' =>$res['f_status'], 'f_transact_id' =>$res['f_transact_id'],'message' => $res['f_percent'], 'stats' => $res['f_stats'])));
	}

	die;
endif;


