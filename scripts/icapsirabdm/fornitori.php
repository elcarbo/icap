<?php
// init
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));
/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);


$dbParams = array(
        'adapter'=>'PDO_MYSQL',
        'params'=>array(
                'host'=>'95.110.175.149',
                'dbname'=>'mainsim3_icapsira',
                'username'=>'root',
                'password'=>'vxpUVjPnHuZriwV3',
        )
);
/*
$dbParams = array(
        'adapter'=>'sqlsrv',
        'params'=>array(
                'host'=>'172.17.210.18',
                'dbname'=>'mainsim3',
                'username'=>'sa',
                'password'=>'Ma1ns1m.',
        )
);
*/
// $dbParams = array(
        // 'adapter'=>'sqlsrv',
        // 'params'=>array(
                // 'host'=>'127.0.0.1',
                // 'dbname'=>$argv[1],
                // 'username'=>'sa',
                // 'password'=>'4#v$crePawu4',
        // )
// );

$cd = new Zend_Config($dbParams);
Zend_Registry::set('db', $cd);
$db = Zend_Db::factory($cd);
$sel = new Zend_DB_Select($db);

$res = $sel->from('t_creation_date', ['f_id', 'f_title'])->where("f_category LIKE 'VENDOR' AND f_phase_id = 1")->query()->fetchAll();

$val = json_encode($res);

$content = <<<EOD
	<html>
		<head>
			<style>
				body {
					font-size: 12px;
					font-family: arial;
				}
				select, input {
					padding: 3px 10px 3px 10px;					
				} 
			</style>
        	<script>
        		window.onload = function() {
        			let ac = document.querySelector('#anno_contabile')
        			let d = new Date()
        			for(let i = 2017; i <= 2027; ++i) {
        				let op = new Option()
        				op.text = i
        				op.value = i
        				ac.options[ac.options.length] = op     
        				if(d.getFullYear() == i) {
        					op.selected = true
        				}  		
        			}
        			let f = document.querySelector('#fornitori')
        			let flist = {$val}
        			for(let i in flist) {
        				let opf = new Option()
        				opf.text = flist[i].f_title	
        				opf.value = flist[i].f_id
        				f.options[f.options.length] = opf
        				if(flist[i].f_title.match(/Marus/)) {
        					opf.selected = true
        				}
        			}
        			document.querySelector('#mese_contabile').options[d.getMonth()].selected = true
        		}

        		let viewPdf = function() {
        			 var win = window.open("../../pdf-creator/print-pdf/f_code/0/mockup/1/script_name/print_report_mensile?anno_contabile=" + document.querySelector('#anno_contabile').options[document.querySelector('#anno_contabile').selectedIndex].value + "&mese_contabile=" + document.querySelector('#mese_contabile').options[document.querySelector('#mese_contabile').selectedIndex].value + '&fornitore=' + document.querySelector('#fornitori').options[document.querySelector('#fornitori').selectedIndex].value, "_blank");
  						win.focus();
        		}
        	</script>
		</head>
		<body>	
			<div style="display: inline-block">
				<table cellspacing="5" cellpadding="0" border="0">
					<tr>
						<td>Anno contabile:</td>
					</tr>
					<tr>
						<td>					
							<select id="anno_contabile"></select>					
						</td>
					</tr>
					<tr>
						<td>Mese contabile</td>
					</tr>
					<tr>
						<td>
							<select id="mese_contabile">
								<option value="1">Gennaio</option>
								<option value="2">Febbraio</option>
								<option value="3">Marzo</option>
								<option value="4">Aprile</option>
								<option value="5">Maggio</option>
								<option value="6">Giugno</option>
								<option value="7">Luglio</option>
								<option value="8">Agosto</option>
								<option value="9">Settembre</option>
								<option value="10">Ottobre</option>
								<option value="11">Novembre</option>
								<option value="12">Dicembre</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Fornitore</td>
					</tr>
					<tr>
						<td>
							<select id="fornitori"></select>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: right">
							<input style="font-size: 14px" type="button" onclick="viewPdf()" value="View">
						</td>
					</tr>
				</table>
			</div>
		</body>
	</htmL>
EOD;

print $content;

?> 
