<?php
function checkIfStartFGPScript($scriptName,$db,&$log)
{
    // controllo che lo script sia da eseguire
    $executeScript = false;
    $selectAux = new Zend_Db_Select($db);
    $selectAux->from('t_scripts', array('f_aux'))
        ->where("f_name = '".$scriptName."'");
    $resAux = $selectAux->query()->fetch();

    $aux = json_decode($resAux['f_aux']);
    $timestamp = (empty($aux))?-1:strtotime($aux->next);
    
    if(empty($aux) || (!empty($aux) && time() > $timestamp)){
        $log = array('next' => date("d-m-Y H:i", strtotime("+10 minutes")));
        $executeScript = true;
    }
 
    return $executeScript; 
}
    
function getWos($db) 
{ 
//la query recupera eventuali generatori (di periodiche) che devono essere portati dalla fase 1 alla 6 (attivo->in corso) o viceversa
// la regola è che i generatori devono essere in fase 1 se hanno tutte le periodiche "chiuse"
// mentre devono essere in fase 6 se hanno almeno una periodica "aperta"
    $querySelect = "
    SELECT t1.f_code,SUB.fc_progress,SUB.f_type_id,SUB.f_phase_id,SUB.gen_1_open,SUB.gen_6_open,SUB.odl_codes
    FROM t_workorders AS t1 
    JOIN(	
            SELECT wo.f_code,cd.fc_progress,cd.f_phase_id,wo.f_type_id
            ,COUNT(wp.f_group_id) AS gen_1_open,COUNT(wp2.f_group_id) AS gen_6_open
            ,GROUP_CONCAT(DISTINCT cd2.f_id SEPARATOR ', ') as odl_codes
            FROM t_workorders AS wo
            JOIN t_creation_date AS cd ON cd.f_id = wo.f_code
            JOIN t_wo_relations AS wr ON wr.f_code_wo_master = wo.f_code
            JOIN t_creation_date AS cd2 ON cd2.f_id = wr.f_code_wo_slave
            LEFT JOIN t_wf_phases AS wp ON wp.f_wf_id = cd2.f_wf_id AND wp.f_number = cd2.f_phase_id
                    AND cd.f_phase_id = 1 AND wp.f_group_id NOT IN (4,5,6,7)
            LEFT JOIN t_wf_phases AS wp2 ON wp2.f_wf_id = cd2.f_wf_id AND wp2.f_number = cd2.f_phase_id
                    AND cd.f_phase_id = 6 AND wp2.f_group_id NOT IN (4,5,6,7)
            WHERE wo.f_type_id IN (3,11,19) AND cd.f_phase_id IN(1,6)
            GROUP BY wo.f_code,cd.f_phase_id
    ) AS SUB
    ON t1.f_code = SUB.f_code
    WHERE ( ( SUB.f_phase_id = 1 AND SUB.gen_1_open > 0) OR ( SUB.f_phase_id = 6 AND SUB.gen_6_open = 0) )
    ";

    $res = $db->query($querySelect)->fetchAll();

//    die($select->__toString());
    return $res;
 }
 
 
 function updatePhase($value,$objUtils,$admusrinfo,$db)
 {
     $phase = $value['f_phase_id'];
     
     if($value['gen_1_open'] > 0)   $phase = 6;
     else if($value['gen_6_open'] == 0) $phase = 1;
     else return;

    //cambio fase + log
    $timestamp = time();
    $updt = array_merge($admusrinfo,["f_timestamp" => $timestamp, "f_phase_id" => $phase]);
    $db->update('t_creation_date', $updt, "f_id = {$value['f_code']}");
    $objUtils->newRecord($db, $value['f_code'], "t_creation_date", array("f_timestamp" => $timestamp),"UpdatePregresso");
    $objUtils->newRecord($db, $value['f_code'], "t_custom_fields", array("f_timestamp" => $timestamp),"UpdatePregresso");
    $objUtils->newRecord($db, $value['f_code'], "t_workorders", array("f_timestamp" => $timestamp),"UpdatePregresso");
}

$execute = checkIfStartFGPScript('update_generators_phase_cron',$this->db,$log);

if($execute){
    $wos = getWos($this->db);
    $objUtils = new Mainsim_Model_Utilities();
    $admusr = Mainsim_Model_Login::getUSerInfo(1, $this->db);
    $admusrinfo = array(
        "fc_editor_user_name"=>$admusr->fc_usr_firstname . ' ' . $admusr->fc_usr_lastname
        ,"fc_editor_user_avatar"=>$admusr->fc_usr_avatar,"fc_editor_user_gender" => $admusr->f_gender
        ,"fc_editor_user_phone" => $admusr->fc_usr_phone,"fc_editor_user_mail" => $admusr->fc_usr_mail
    );
    
    foreach($wos as $el) {
        updatePhase($el,$objUtils,$admusrinfo,$this->db);
    } 

    $this->db->update('t_scripts', array('f_aux' => json_encode($log)), array("f_name = 'update_generators_phase_cron'"));
    $msg = "update_generators_phase_cron : ".count($wos)." generatori cambiati di fase il ".date('d-m-Y H:i:s');
    $this->db->insert("t_logs",array("f_log"=>$msg,'f_type_log'=>123,'f_timestamp'=>time()));

}
