<?php
ini_set('memory_limit', '-1');
include('XLSXReader.php');
 
//ob_end_clean();
header("Connection: close");
ignore_user_abort();
ob_start();
header("Content-Length: 0");
ob_end_flush();
flush();
session_write_close(); 

defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));
error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));
include(APPLICATION_PATH.'/modules/default/models/Utilities.php');

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));
/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

// connessione a db
$config = parse_ini_file(APPLICATION_PATH . '/configs/application.ini');

//locale
//$config['attachmentsFolder'] = 'C:\inetpub\wwwroot\icapsirabdm\attachments';


$dbParams = array(
    'adapter'=>'PDO_MYSQL',
    'params'=>array(
            'host'=> $config['database.params.host'],
//            'dbname'=> $config['database.params.dbname'],
            'dbname'=> 'mainsim3_icapsirabdm',
            'username'=> $config['database.params.username'],
            'password'=> $config['database.params.password'],
    )
);
$cd = new Zend_Config($dbParams);
Zend_Registry::set('db', $cd);
$db = Zend_Db::factory($cd);

function update_status($id, $data, $db){
    $db->update('t_excel_import', $data, 'f_transact_id = ' . $id); 
}

// trasforma un array di dati da inserire in una tabella in una stringa per inserimenti multipli
function array_to_string_table_values($data){
    $string = "";
    for($i = 0; $i < count($data); $i++){
        $data[$i] = array_map(function ($v) { 
            return addslashes($v); 
        }, array_values($data[$i]));
        
        $string .= "('" . implode("','", $data[$i]) . "'),";
    }

    $string = rtrim($string, ",");
    return $string;
}

function filterElementsByUser($elements, $user ,$db){
    
    $select = new Zend_Db_Select($db);
    $select->from('t_selector_ware', ['f_selector_id'])
        ->join('t_creation_date', 't_creation_date.f_id = t_selector_ware.f_selector_id', ['f_category'])
        ->where("t_selector_ware.f_ware_id = ".$user);

    $result = $select->query()->fetchAll();
    
    //se l'utente non ha selettori crossati non filtrare gli elementi
    if(empty($result)) return $elements;
    
    $userSelectors = [];
    foreach ( $result as $value) {
        $f_type = $value['f_category'];
        if(!isset($userSelectors[$f_type])) $userSelectors[$f_type] = [];
        $userSelectors[$f_type] [] = $value['f_selector_id'];
    }
    
    $elements = array_filter($elements,function($el) use($userSelectors) {
        $hys = [] ; //selettori fc_hierarchy_selectors wo
        $tmp = (empty($el['fc_hierarchy_selectors']))?[]:explode(",", $el['fc_hierarchy_selectors']);
        for($j = 0; $j < count($tmp); $j++){
            $tmp2 = explode(':', $tmp[$j]);
            $f_type = $tmp2[0];
            if(!isset($hys[$f_type])) $hys[$f_type] = [];
            $hys[$f_type] [] = $tmp2[1];
        }
        
        $intersect = array_intersect_key ($userSelectors, $hys);

        //vero se l'utente non ha selettori crossati, o se il wo ha fc_hierarchy_selectors vuoto
        // o se utente e wo non hanno tipo di selettori in comune
        $flag = (empty($hys) || empty($userSelectors) || empty($intersect));
        
        if(!$flag){
            foreach ($intersect as $type => $values) {
                if(!empty(array_intersect($values, $hys[$type]))) {
                    $flag = true;
                    break;
                }
            }
        }

        return $flag;
    });  
    
    return $elements;
}

// trasforma un array di dati in uno statement di update
function array_to_string_table_update($table, $data, $id){
    foreach($data as $key => $value)
        $params[] = $key . "='" . addslashes($value) . "'";
    return 'UPDATE ' . $table . ' SET ' . implode(",", $params) . ' WHERE ' . ($table == 't_creation_date' ? 'f_id=' : 'f_code=') . $id;
}

// suddivide i cespiti excel in nuovi e vecchi
function check_tipo_cespiti($cespitiExcel, $user, $db){
    // per evitare non univocità e missing rendo la chiave standard
    // per esempio 123 MP => 00123mp
    $cespiti = [];
    foreach ($cespitiExcel as $key => $value) {
        preg_match('/^(\d+)(.*)/', $key, $output_array);
        $pre = (empty($output_array[1]))?'':sprintf("%05d", $output_array[1]);
        $suff = (empty($output_array[2]))?'':$output_array[2];
        $key2 = strtolower(preg_replace('/\s*/', '', $pre.$suff));  
        if(!empty($key2))  $cespiti[$key2] = $value;      
    }
    
    $keys = array_keys($cespiti);
    
    // recupero tutti i wo da db con progress_custom presi dall'excel non in gruppo 7 
    $select = new Zend_Db_Select($db);
    $select->from('t_creation_date', array('f_id','f_title','f_description','fc_hierarchy_selectors'))
        ->join('t_custom_fields', 't_custom_fields.f_code = t_creation_date.f_id', ["progress_custom","costs_for_ddt","rif_ddt","fc_costs_for_downtime","fc_costs_for_labor","fc_costs_for_service","LOWER(REPLACE(t_custom_fields.progress_custom, ' ', '')) as key"])
        ->join('t_wf_phases', 't_wf_phases.f_wf_id = t_creation_date.f_wf_id AND t_wf_phases.f_number = t_creation_date.f_phase_id', [])
//        ->joinLeft('t_workorders_parent', 't_workorders_parent.f_code = t_creation_date.f_id AND t_workorders_parent.f_parent_code != 0', [])
        ->where("t_wf_phases.f_group_id NOT IN (7)")
        ->where("LOWER(REPLACE(t_custom_fields.progress_custom, ' ', '')) IN ('" . implode("','", $keys) . "')");

    $resCespiti = $select->query()->fetchAll();
    //$sql = $select->__toString();

    $select->reset();
    
    $resCespiti = filterElementsByUser($resCespiti, $user ,$db);
    
    $existentCespiti = [];
    //recupero per ogni wo l'ultima riga(f_timestamp) in 'ddt_logs' per ogni anno e mese
    $querySelect = "SELECT * FROM t_ddt_logs AS ddt INNER JOIN (
    SELECT f_code,max(f_timestamp) AS max_timestamp FROM t_ddt_logs GROUP BY f_code,anno_ddt,mese_ddt) p2
    ON ddt.f_code = p2.f_code AND ddt.f_timestamp = p2.max_timestamp
    WHERE ddt.f_code = #TAG
    ORDER BY ddt.f_timestamp DESC";

    foreach($resCespiti as &$cespitedb){
//        $select->from('t_ddt_logs')
//        ->where("t_ddt_logs.f_code = ".$cespitedb['f_id'])->order("f_timestamp DESC");
//        $resLogs = $select->query()->fetchAll();
        $qr = str_replace('#TAG', $cespitedb['f_id'], $querySelect);
        $resLogs = $db->query($qr)->fetchAll();
        $cespitedb['ddt_logs'] = $resLogs;
        $existentCespiti[$cespitedb['key']] = $cespitedb;
        $select->reset();
    }
    

    // suddivisione tra cespiti nuovi(non esistenti) e vecchi
    $cespiteType = [
        'old' => [],
        'new' => []
    ];
    foreach($cespiti as $key => $value){
        // cespite vecchio
        if(isset($existentCespiti[$key])){
            $cespiteType['old'][$key]['excel_data'] = $value;
            $cespiteType['old'][$key]['code'] = $existentCespiti[$key]['f_id'];
            $cespiteType['old'][$key]['db_data'] = $existentCespiti[$key];
        }
        // cespite nuovo o non esistente
        else{
             $cespiteType['new'][$key] = $value;
        }
    }
    return $cespiteType;
}

function check_diff_cespite($cespite,$db){
    $result = null; 
    $wo_id = $cespite['code'];
    $ddt_logs = (empty($cespite['db_data']['ddt_logs']))?[]:$cespite['db_data']['ddt_logs'];
    $excel_rif_ddt = addslashes($cespite['excel_data'][1]);
    $excel_costs_for_ddt = number_format($cespite['excel_data'][2], 2, ".", "");
    $excel_mese_ddt = $cespite['excel_data'][3];
    $excel_anno_ddt = $cespite['excel_data'][4];
//    $whereClause = " WHERE ddt.f_code = $wo_id AND ddt.rif_ddt = '". $excel_rif_ddt."' AND CAST(ddt.costs_for_ddt AS DECIMAL(30, 2)) = CAST($excel_costs_for_ddt AS DECIMAL(30, 2)) AND ddt.anno_ddt = $excel_anno_ddt AND ddt.mese_ddt = '". $excel_mese_ddt."'";
//    $querySelect = "SELECT * FROM t_ddt_logs AS ddt".$whereClause;  
    
    //verifico se esiste già per il wo una riga di importazione precedente con gli stessi valori di rif_ddt,costs_for_ddt,anno_ddt,mese_ddt
//    if(!empty($ddt_logs)) $exist = $db->query($querySelect)->fetch();
        
    $logs_ddt_str = [];
    
//    if(empty($exist)){
        //t_ddt_logs
        $result['t_ddt_logs']['rif_ddt'] = $excel_rif_ddt;
        $result['t_ddt_logs']['costs_for_ddt'] = $excel_costs_for_ddt;
        $result['t_ddt_logs']['anno_ddt'] = $excel_anno_ddt;
        $result['t_ddt_logs']['mese_ddt'] = $excel_mese_ddt;
        $new_log = $result['t_ddt_logs']['anno_ddt'].' '.$result['t_ddt_logs']['mese_ddt'].' / '.$result['t_ddt_logs']['rif_ddt'].' / '.$result['t_ddt_logs']['costs_for_ddt']." ".chr(128);  

        //rimuovo l'eventuale precedente riga relativa allo stesso anno e mese di quella nuova
        $filter = array($excel_anno_ddt,$excel_mese_ddt);
        $ddt_logs = array_filter($ddt_logs,function($log) use($filter) {
            return ($log['anno_ddt'] != $filter[0] || strtolower($log['mese_ddt']) != strtolower($filter[1]) );
        });

        //custom_fields
        $logs_ddt_str [] = $new_log;
        
        foreach($ddt_logs as $log) {
             $logs_ddt_str [] = $log['anno_ddt'].' '.$log['mese_ddt'].' / '.$log['rif_ddt'].' / '.number_format($log['costs_for_ddt'], 2, ".", "")." ".chr(128);
             // 2019 Maggio / ddt 53 / 234.56
         }
        $result['t_custom_fields']['logs_ddt'] = implode("\r\n", $logs_ddt_str);  
        
        $result['t_custom_fields']['rif_ddt'] = $excel_rif_ddt;
        $total_costs_for_ddt = (empty($ddt_logs))?$excel_costs_for_ddt: $excel_costs_for_ddt + array_sum(array_column($ddt_logs,'costs_for_ddt'));
        $result['t_custom_fields']['costs_for_ddt'] = number_format($total_costs_for_ddt, 2, ".", "");
        $fc_total_costs = $cespite['db_data']['fc_costs_for_downtime']+$cespite['db_data']['fc_costs_for_labor']
                +$result['t_custom_fields']['fc_costs_for_service'] + $result['t_custom_fields']['costs_for_ddt'];
        $result['t_custom_fields']['fc_total_costs'] = number_format($fc_total_costs, 2, ".", "");

//    }
    
    return $result;
}

// updare vecchi cespiti
function update_cespiti($cespiti, $time, $db, &$counter,$uid,&$locked = []){
    
    if(count($cespiti) == 0) return;
    $toUnlock = [];
    
//    file_put_contents('log.txt',PHP_EOL.json_encode($cespiti), FILE_APPEND);
    // suddivisione dati cespite per tabella
    $changedCespiti = []; $parents = [];
    foreach($cespiti as $key => $value){
        // controllo se ci sono modifiche nel cespite
        $diff = check_diff_cespite($value,$db);
        
        if(is_array($diff)){
            $changes = false;
            //lock wo if unlocked
            if(empty(jax_lock(array($value['db_data']['f_id']),1,$uid,$db)))    $changes = true;
            else{ $locked[] = $value['excel_data'][0]; $changes = false;}    
            
            if($changes){
                if(is_array($diff['t_custom_fields'])){
                    $update['t_custom_fields'][] = array_to_string_table_update('t_custom_fields', $diff['t_custom_fields'], $value['db_data']['f_id']);
                    $temp = Mainsim_Model_Utilities::getParentCode('t_workorders_parent',$value['db_data']['f_id']);
                    if(!empty($temp) && !empty($temp[0]))   $parents [] = $temp[0]; 
                }
                if(is_array($diff['t_ddt_logs'])){
                    $diff['t_ddt_logs']['f_code'] = $value['db_data']['f_id'];
                    $diff['t_ddt_logs']['progress_custom'] = $value['db_data']['progress_custom'];
                    $diff['t_ddt_logs']['f_user_id'] = $uid;
                    $diff['t_ddt_logs']['f_timestamp'] = time();
                    $insert['t_ddt_logs'][] = $diff['t_ddt_logs']; 
                }
                
                $toUnlock [] = $value['db_data']['f_id'];
                $counter++;
                $changedCespiti[] = $value['excel_data'][0];
            }
        }
    }

    $last_statement = '';

    try{
        // update t_custom_fields
        if(is_array($update['t_custom_fields'])){
            $updateStatement = implode(';', $update['t_custom_fields']);
            $last_statement = $updateStatement;
            $db->query($updateStatement);

            //update parents
            if(!empty($parents)){
                $parents = array_unique($parents);
                $prts = getParentsUpdateStrings($parents,$db);
                $updateParentsStatement = implode(';', $prts);
                $last_statement = $updateParentsStatement;
                $db->query($updateParentsStatement);
            }
        }
        // insert t_ddt_logs
        if(is_array($insert['t_ddt_logs'])){
            $keys = array_keys($insert['t_ddt_logs'][0]);
            $insertStatement = "INSERT INTO t_ddt_logs(".implode(",", $keys).") VALUES " . array_to_string_table_values($insert['t_ddt_logs']);
            $last_statement = $insertStatement;
           $db->query($insertStatement);
        }            
    }
    catch(Exception $e){
//        print("update_cespiti error:" . $e->getMessage() . "\n");// . $insertStatement);
        $msgLogs = "update_cespiti error [".$_REQUEST['rid']."]: ".$e->getMessage()."\r\n".$last_statement;
        $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>$msgLogs,'f_type_log'=>71));
        $msg = Mainsim_Model_Utilities::chg("Si e' verificato un errore durante l'importazione");
        update_status($_REQUEST['rid'], array('f_status' => -1, 'f_error' => $msg,'f_timestamp' => time()), $db);
//        file_put_contents('log.txt', $last_statement, FILE_APPEND);
        die();
    } finally {
        //unlock
        if(!empty($toUnlock))   jax_lock($toUnlock,0,$uid,$db);            
    }

    
    return $changedCespiti;
}

function jax_lock($afcode,$mode,$uid,$db) {
    $tt = time();
    $sel = new Zend_Db_Select($db);

    //per propagare il lock del workorder ai parents del wo e ai suo discendenti
    $tolock = [];
    foreach ($afcode as $line) {
        $sel->reset();
        $temp = [];
        $hierarchy = $sel->from("t_creation_date as cd", ["fc_hierarchy_codes"])
            ->joinLeft("t_creation_date as cd2","cd2.fc_hierarchy_codes LIKE '".$line.",%' OR cd2.fc_hierarchy_codes LIKE '%,".$line.",%' ","f_id as child_code")
           ->where("cd.f_id = ".$line)->query()->fetchAll();

        $parents_hie = (!empty($hierarchy))?explode(",",$hierarchy[0]["fc_hierarchy_codes"]):[];
        $childs_hie = (!empty($hierarchy))?array_column($hierarchy, 'child_code'):[];
        $temp = array_unique(array_merge($parents_hie,$childs_hie));
        $temp = array_filter($temp, function ($v){ //elimino valori non numerici o minori di 0
            return $v > 0;
        });

        if(!empty($temp)) $tolock[$line] = implode(",",$temp);
   }

    // unlock        
    if (!$mode && !empty($afcode)) {
        //per propagare il lock del workorder ai parents del wo e ai suo discendenti  
        foreach ($afcode as $code) {
            $temp = [$code];
            if(!empty($tolock[$code])){
                $temp = array_merge($temp,explode(",",$tolock[$code]));
            }

           $sel->reset();
           $f_ids = $sel->from("t_locked as tl", ["f_id"])
              ->where( "tl.f_user_id = $uid". " and tl.f_code IN (". implode(",",$temp) .")")
               ->query()->fetchAll();

           if(!empty($f_ids)){
               $f_ids = implode(",",array_column($f_ids,'f_id'));
               $db->delete("t_locked", "f_id IN (" . $f_ids . ")");
           }
        }

        return array();
    }
    
    
    // lock sempre singolo
    if ($mode == 1) {
        $risp = array();
        $db->beginTransaction();
        //check if already locked   
        foreach ($afcode as $line) {
            $sel->reset();
            $ris = $sel->from("t_locked", array("num" => "count(*)"))
                            ->where("f_code = ?", $line)->query()->fetch();

           //$ris = $db->query("SELECT count(*) AS num FROM t_locked where f_code = $line FOR UPDATE")->fetch(); 
           $nc = (int) $ris['num'];

            if (!$nc) {
                $db->insert("t_locked", array(
                    "f_code" => $line,
                    "f_user_id" => $uid,
                    "f_timestamp" => $tt));

                //per propagare il lock del workorder ai parents del wo e ai suo discendenti 
                if(!empty($tolock[$line])){ 
                    $temp = explode(",",$tolock[$line]);
                    foreach ($temp as $wo_code) {
                        $db->insert("t_locked", array(
                            "f_code" => $wo_code,
                            "f_user_id" => $uid,
                            "f_timestamp" => $tt));                            
                    }
                }
            }
            else array_push($risp,$line);
        }
        $db->commit();
        return $risp;
    }
}

function getParentsUpdateStrings($parents,$db){

    $select = new Zend_Db_Select($db);
    $result = $select->from(array("wp"=>"t_workorders_parent"),['f_parent_code'])
        ->join(array("cd"=>"t_creation_date"),"cd.f_id = wp.f_code",[]) // wo figli
        ->join(array("ph"=>"t_wf_phases"), 'ph.f_wf_id = cd.f_wf_id AND ph.f_number = cd.f_phase_id', [])
        ->join(array("wo"=>"t_workorders"),"wo.f_code = cd.f_id",['f_code']) // wo figli
        ->join(array("cf"=>"t_custom_fields"),"cf.f_code = cd.f_id",['ditta_esterna','rif_ddt','costs_for_ddt']) // wo figli
        ->where("wp.f_parent_code IN (". implode(",", $parents).")") // wo padre
        ->where("wo.f_type_id IN (8,13)") // wo figli emergency o reservation
        ->where("ph.f_group_id NOT IN (7)") // wo figli non cancellati
        ->query()->fetchAll();  
    $select->reset();

    $db_data = [];
    foreach ($result as $value) {
        $f_parent_code = $value['f_parent_code'];
        unset($value['f_parent_code']);
        if(!isset($db_data[$f_parent_code]))$db_data[$f_parent_code] = array();
        array_push ($db_data[$f_parent_code], $value);
    }
    
    $update = [];
    foreach ($db_data as $f_parent_code => $children) {
        $sqlData = [];
        
        //costs_for_ddt, fc_total_costs
        $costs_for_ddt = array_sum(array_column($children,'costs_for_ddt'));
        $sqlData['costs_for_ddt'] = $costs_for_ddt;
                
        $res_pcs = Mainsim_Model_Utilities::getPairCrossByType($f_parent_code,$db);

        $pcs_assets = (empty($res_pcs)||empty($res_pcs['1']))?array():$res_pcs['1'];
        $pcs_labors = (empty($res_pcs)||empty($res_pcs['2']))?array():$res_pcs['2'];
        $pcs_supply = (empty($res_pcs)||empty($res_pcs['24']))?array():$res_pcs['24'];
        $fc_total_costs = 0;
        $fc_costs_for_downtime = (empty($pcs_assets))?0:array_sum(array_column($pcs_assets, 'fc_asset_downtime_cost'));
        $fc_costs_for_labor = (empty($pcs_labors))?0:array_sum(array_column($pcs_labors, 'fc_rsc_cost'));
        $fc_costs_for_service = (empty($pcs_supply))?0:array_sum(array_column($pcs_supply, 'fc_supply_total_cost'));

        $fc_total_costs = $fc_costs_for_downtime+$fc_costs_for_labor+$fc_costs_for_service+$costs_for_ddt;
        
        $sqlData['fc_total_costs'] = $fc_total_costs;
    
        
        //rif_ddt
        $ditte_esterne = array_filter($children, function ($v){ 
             return preg_replace('/\s+/', '', $v['ditta_esterna']) !== '';
        });
        $ditte_esterne = array_map(function ($v) { 
            $ditta_esterna = json_decode($v['ditta_esterna'], true);
            $ditta_esterna = $ditta_esterna['labels'][0];
            $v['ditta_esterna'] = $ditta_esterna ;
            return $v; 
        }, $ditte_esterne);


        $ditte_esterne = array_column($ditte_esterne,'ditta_esterna','f_code');
        $brothers = array_keys($ditte_esterne);


        //Riferimenti DDT
        $ris = array_filter($children, function ($v){ 
             return preg_replace('/\s+/', '', $v['rif_ddt']) !== '';
        });
        $ris = array_column($ris,'rif_ddt','f_code');

        $rifs = [];
        foreach($ditte_esterne as $code => $ditta){
            if(isset($ris[$code])) $value = $ris[$code];
            if(!empty($value)) array_push($rifs, addslashes("[".$ditta."]: ".$value));
        }

        $sqlData['rif_ddt'] = empty($rifs)?'': implode(",\r\n", $rifs);
        
        $update [] = array_to_string_table_update('t_custom_fields', $sqlData, $f_parent_code);
    }

    return $update;
    
}


$attachname = $_REQUEST['filename'];
$uid = $_REQUEST['userId'];
$filePath = $config['attachmentsFolder'].'\\temp\\'.$attachname;
$ext = explode(".", $attachname);
$ext = end($ext);
if(!in_array($ext, ['xlsx'])){
    update_status($_REQUEST['rid'], array('f_status' => -1, 'f_error' => "Formato File Non Compatibile",'f_timestamp' => time()), $db);
//    file_put_contents('log.txt', "Formato File Non Compatibile", FILE_APPEND);
    die();     
}

// recupero path file xlsx
$select = new Zend_Db_Select($db);

try{
if(file_exists($filePath)){
    
    $errorCounter = 0;
    $updateCounter = 0;
    $notFoundCounter = 0;
    $elements = [];
    $locked = [];    
    // set time
    $time = time();
    
    // open xlsx
    $objExcel = new XLSXReader();
    $objExcel->open($filePath, 'temp');
    if(!$objExcel->openSheet('Foglio1')){
        update_status($_REQUEST['rid'], array('f_status' => -1, 'f_error' => "foglio 'Foglio1' non trovato",'f_timestamp' => time()), $db);
//        file_put_contents('log.txt', "Foglio 'Foglio1' non trovato\n", FILE_APPEND);
        die();        
    }
    
    $resultLogs = array('all'=>[],'not_found'=>[],'updated'=>[],'locked'=>[]);
    $sheetsInfo = $objExcel->getSheetsList();
    // loop righe excel
    for($i = 2; $i <= $sheetsInfo['Foglio1']['rows']; $i++){
        $row = $objExcel->readRow($i);
        
        if(!empty($row[0])) {
            $elements[$row[0]] = $row;
        }
        // inserimento n righe alla volta
        if($i % 10 == 0 || $i == $sheetsInfo['Foglio1']['rows']){
            // suddivisione in cespiti nuovi/esistenti
            $tipo_cespiti = check_tipo_cespiti($elements, $uid, $db);

            $resultLogs['not_found'] = (empty($tipo_cespiti['new']))?$resultLogs['not_found']:array_merge($resultLogs['not_found'],array_keys($tipo_cespiti['new']));
            $resultLogs['all'] = (empty($tipo_cespiti['old']))?$resultLogs['all']:array_merge($resultLogs['all'],array_keys($tipo_cespiti['old']));
            
            $notFoundCounter+=count($tipo_cespiti['new']);
            // update vecchi cespiti
            $changedCespiti = update_cespiti($tipo_cespiti['old'], $time, $db, $updateCounter,$uid,$locked);
            $resultLogs['updated'] =(empty($changedCespiti))? $resultLogs['updated']:array_merge($resultLogs['updated'],$changedCespiti);            
            // rinizializzazione cespiti per il prossimo chunck di cespiti
            $elements = [];
            update_status($_REQUEST['rid'], array('f_status' => 1,'f_percent' => intval($i*100/$sheetsInfo['Foglio1']['rows']),'f_timestamp' => time()), $db);
        }
    }
    
    $resultLogs['locked'] = (empty($locked))?[]:$locked;
    $resultLogs['all'] = array_merge($resultLogs['not_found'], $resultLogs['all']);
    $jsonLogs = (!empty($resultLogs))?json_encode($resultLogs):null;
    //file_put_contents('log.txt','\r'.$jsonLogs, FILE_APPEND);
    // fine update
    update_status($_REQUEST['rid'], array('f_status' => 2, 'f_percent' => 100, 'f_stats' => json_encode(array('total' => count($resultLogs['all']), 'not_found' => $notFoundCounter, 'update' => $updateCounter, 'locked' => count($locked))),'f_logs'=>$jsonLogs,'f_timestamp' => time()), $db);
}    
else{
    update_status($_REQUEST['rid'], array('f_status' => -1, 'f_error' => 'File non trovato in '.$filePath ,'f_timestamp' => time()), $db);
}
} catch (Exception $e){
    update_status($_REQUEST['rid'], array('f_status' => -1, 'f_error' => $e->getMessage(), 'f_timestamp' => time()), $db);
    $db->insert("t_logs",array('f_timestamp'=>time(),'f_log'=>"[Excel Costi Materiali] =>\r".$e->getMessage(),'f_type_log'=>71));
    die();
} 
?>