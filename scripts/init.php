<?php

// init
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

   
// create db instance from application.ini
$config = parse_ini_file(APPLICATION_PATH . '/configs/application.ini');
$cd = new Zend_Config([
        'adapter'=> $config['database.adapter'],
        'params'=>array(
            'host'=> $config['database.params.host'],
            'dbname'=> $config['database.params.dbname'],
            'username'=> $config['database.params.username'],
            'password'=> $config['database.params.password']
        )
    ]);
Zend_Registry::set('db', $cd);
$db = Zend_Db::factory($cd);

// multidatabase ON --> search for db by url passed in argv
if($config['multiDatabase'] == 'On'){

    // get project db params
    $select = new Zend_Db_Select($db); 
    $select->from('database_lists')
            ->where("f_url = '" . $argv[1] . "'");
    $res = $select->query()->fetchAll();
    $cd = new Zend_Config([
        'adapter'=> $res[0]['f_adapter'],
        'params'=>array(
            'host'=> $res[0]['f_host'],
            'dbname'=> $res[0]['f_dbname'],
            'username'=> $res[0]['f_username'],
            'password'=> $res[0]['f_password']
        )
    ]);
    $db = Zend_Db::factory($cd);
}
    
