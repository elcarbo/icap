<?php
// init
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));
/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);


$dbParams = array(
        'adapter'=>'PDO_MYSQL',
        'params'=>array(
                'host'=>'95.110.175.149',
                'dbname'=>$argv[1],
                'username'=>'root',
                'password'=>'vxpUVjPnHuZriwV3',
        )
);
/*
$dbParams = array(
        'adapter'=>'sqlsrv',
        'params'=>array(
                'host'=>'10.0.245.220',
                'dbname'=>'mainsim3_hsr',
                'username'=>'sa',
                'password'=>'4#v$crePawu4',
        )
);
/*
$dbParams = array(
        'adapter'=>'sqlsrv',
        'params'=>array(
                'host'=>'10.0.245.220',
                'dbname'=>$argv[1],
                'username'=>'sa',
                'password'=>'4#v$crePawu4',
        )
);*/

$cd = new Zend_Config($dbParams);
Zend_Registry::set('db', $cd);
$db = Zend_Db::factory($cd);

//initialize database
/*$config = new Zend_Config_ini(APPLICATION_PATH . '/configs/application.ini', 'production'); 
Zend_Registry::set('db', $config->database);

$db = Zend_Db::factory($config->database);*/


function getParentCode($table = '',$f_code_sel = 0,$fathers = array(),$db = null){         
       
        $select = new Zend_Db_Select($db);      
        //check parents
        $select->reset();            
        $res_parents = $select->from($table,"f_parent_code")->where("f_code = ?",$f_code_sel)
                ->where("f_active = 1")->query()->fetchAll();        
        foreach($res_parents as $par) {
            if(!in_array($par['f_parent_code'], $fathers) && $par['f_parent_code'] != $f_code_sel) {
                $fathers[] = (int)$par['f_parent_code'];
                $fathers = getParentCode($table,$par['f_parent_code'],$fathers,$db);
            }
        }            
        return $fathers;
}
  
print("\n**************************************\n");
print("UPDATE HIERARCHY CODES SCRIPT");
print("\n**************************************\n");

/* wares */
$select = "SELECT f_id FROM t_creation_date WHERE f_type = 'WARES'";
$res = $db->query($select)->fetchAll();
$tot = count($res);

for($i = 0; $i < count($res); $i++){
    
    $parents = getParentCode('t_wares_parent', $res[$i]['f_id'], array(), $db);
    if(count($parents) > 0){
        $update = "UPDATE t_creation_date SET fc_hierarchy_codes = '" . implode(",", $parents) . "' WHERE f_id = " . $res[$i]['f_id'];
        $db->query($update);
    }
    print("\rWARES: " . $i . "/" . $tot);
}
print("\n\n");

/* wares */
$select = "SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS'";
$res = $db->query($select)->fetchAll();
$tot = count($res);

for($i = 0; $i < count($res); $i++){
    
    $parents = getParentCode('t_workorders_parent', $res[$i]['f_id'], array(), $db);
    if(count($parents) > 0){
        $update = "UPDATE t_creation_date SET fc_hierarchy_codes = '" . implode(",", $parents) . "' WHERE f_id = " . $res[$i]['f_id'];
        $db->query($update);
    }
    print("\rWORKORDERS: " . $i . "/" . $tot);
}
print("\n\n");
die();





        
