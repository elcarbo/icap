<?php

function init($argv){
    
    // create db instance from application.ini
    $config = parse_ini_file(APPLICATION_PATH . '/configs/application.ini');
    $cd = new Zend_Config([
            'adapter'=> $config['database.adapter'],
            'params'=>array(
                'host'=> $config['database.params.host'],
                'dbname'=> $config['database.params.dbname'],
                'username'=> $config['database.params.username'],
                'password'=> $config['database.params.password']
            )
        ]);
    Zend_Registry::set('db', $cd);
    $db = Zend_Db::factory($cd);
    
    // multidatabase ON --> search for db by url passed in argv
    if($config['multiDatabase'] == 'On'){

        // get project db params
        $select = new Zend_Db_Select($db); 
        $select->from('database_lists')
               ->where("f_url = '" . $argv[1] . "'");
        $res = $select->query()->fetchAll();
        $cd = new Zend_Config([
            'adapter'=> $res[0]['f_adapter'],
            'params'=>array(
                'host'=> $res[0]['f_host'],
                'dbname'=> $res[0]['f_dbname'],
                'username'=> $res[0]['f_username'],
                'password'=> $res[0]['f_password']
            )
        ]);
        $db = Zend_Db::factory($cd);
        $response['db'] = $db;
        $response['db_adapter'] = $res[0]['f_adapter'];
    }
    // multidatabase OFF
    else{
        $response['db'] = $db;
        $response['db_adapter'] = $config['database.adapter'];
    }
    
    // prepare query based on adapter type
    // MYSQL
    if($response['db_adapter'] == 'PDO_MYSQL'){
        // increase concat max length
		$db->query('SET group_concat_max_len = 1000000');
		
		// select ware
        $response['selectWares'] = "SELECT t1.f_id, t1.fc_hierarchy_codes, (SELECT GROUP_CONCAT(DISTINCT t2.f_selector_id)
                                                                            FROM t_selector_ware t2
                                                                            WHERE t1.f_id = t2.f_ware_id) AS selectors
                                    FROM t_creation_date t1
                                    WHERE t1.f_type = 'WARES'";
        // select workorders
        $response['selectWorkorders'] = "SELECT t1.f_id, t1.fc_hierarchy_codes, (SELECT GROUP_CONCAT(DISTINCT t2.f_selector_id) 
                                                                                    FROM t_selector_wo t2
                                                                                    WHERE t1.f_id = t2.f_wo_id) AS selectors,
                                                                                (SELECT GROUP_CONCAT(DISTINCT t3.f_ware_id)
                                                                                    FROM t_ware_wo t3
                                                                                    JOIN t_wares t4 ON t3.f_ware_id = t4.f_code 
                                                                                    WHERE t1.f_id = t3.f_wo_id
                                                                                        AND t4.f_type_id NOT IN (16)) AS wares
                                        FROM t_creation_date t1 
                                        WHERE t1.f_type = 'WORKORDERS'";
    }
    // SQLSERVER
    else{
        // select wares
        $response['selectWares'] = "SELECT t1.f_id, t1.fc_hierarchy_codes, 
                                        substring(
                                            (SELECT ',' + CAST(t2.f_selector_id as varchar)  AS [text()]
                                             FROM t_selector_ware t2
                                             WHERE t1.f_id = t2.f_ware_id
                                             FOR XML PATH ('')), 2, 1000) [selectors]
                                        FROM t_creation_date t1
                                        WHERE t1.f_type = 'WARES'";
        // select workorders
        $response['selectWorkorders'] = "SELECT t1.f_id, t1.fc_hierarchy_codes, 
                                            substring(
                                                (SELECT ',' + CAST(t2.f_selector_id as varchar)  AS [text()]
                                                 FROM t_selector_wo t2
                                                 WHERE t1.f_id = t2.f_wo_id
                                                 FOR XML PATH ('')), 2, 1000) [selectors],
                                            substring(
                                                (SELECT ',' + CAST(t3.f_ware_id as varchar)  AS [text()]
                                                 FROM t_ware_wo t3
                                                 JOIN t_wares t4 ON t3.f_ware_id = t4.f_code 
                                                 WHERE t1.f_id = t3.f_wo_id
                                                 AND t4.f_type_id NOT IN (16)
                                                 FOR XML PATH ('')), 2, 1000) [wares]
                                            FROM t_creation_date t1
                                            WHERE t1.f_type = 'WORKORDERS'";
    }
    
    return $response;
}


// init
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$config = init($argv);


print("\n**************************************\n");
print("UPDATE SELECTORS SCRIPT");
print("\n**************************************\n");

print("\ncreating aux selectors structure...");
$selectorsStructure = [];
function createSelectorsStructure($parent, $hierarchy, &$selectorsStructure, &$db){         
       
    $select = new Zend_Db_Select($db);      
    // get children selectors          
    $children = $select->from('t_selectors_parent',"f_code")
            ->join('t_creation_date', 't_selectors_parent.f_code = t_creation_date.f_id', array('f_category'))
            ->where("f_parent_code = ?", $parent)
            ->where("f_active = 1")
            ->where("f_category <> 'DATA MANAGER'")->query()->fetchAll();  
    for($i = 0; $i < count($children); $i++){
        $selectorsStructure[$children[$i]['f_code']] = array_merge(array($children[$i]['f_category'] . ":" . $children[$i]['f_code']), $hierarchy);
        createSelectorsStructure($children[$i]['f_code'], $selectorsStructure[$children[$i]['f_code']], $selectorsStructure, $db);
    }            
}
createSelectorsStructure(0, [], $selectorsStructure, $config['db']);
/************************************** WO_AUX_SEL_PROPAGATION_FROM_PARENTS **************************************
 il setting stabilisce per i workorders quali selettori non propagare ai figli nel campo 'fc_hierarchy_selectors'.
1) setting non esiste => vengono propagati tutti
2) vuoto => nessun selettore viene propagato
3) esempio: 3,5 => esclude tipi 3 e 5
 */
function getWoSelectorsTypeToExcludeFromParents(&$db){ 
    $select = new Zend_Db_Select($db); 
    $excludeSelFromParents = [];
    $select->from(array("c" => "t_creation_date"), array("f_title", "f_description"))
        ->join(array("s" => "t_systems"), "c.f_id = s.f_code", [])
        ->where("c.f_type = 'SYSTEMS'")
        ->where("c.f_category = 'SETTING'")
        ->where("c.f_title = 'WO_AUX_SEL_PROPAGATION_FROM_PARENTS' ")->where("c.f_phase_id = 1");
    $woAuxSelProp = $select->query()->fetch();
    $select->reset();

    if(!empty($woAuxSelProp))
    {
        if(!empty($woAuxSelProp['f_description'])){ //esiste il setting ed è valorizzato
            $excludeSelFromParents = explode(",", $woAuxSelProp['f_description']);
            array_walk($excludeSelFromParents, function(&$value, &$key) {
                $value = "SELECTOR ".$value;
            });
        }
        else $excludeSelFromParents = -1; //se esiste il setting ed è vuoto escludi tutti i selettori dei parents
    }
    
    return $excludeSelFromParents;
}
print("done\n\n");

/*************************************************/
/******************** wares **********************/
/*************************************************/
print("\n\n--------------------------------------\n");
print("WARES\n");
print("--------------------------------------\n");
// get wares and selectors directly associated
$resWares = $config['db']->query($config['selectWares'])->fetchAll();
$tot = count($resWares);
for($i = 0; $i < count($resWares); $i++){  
    $wares[$resWares[$i]['f_id']] = $resWares[$i];
    $wares[$resWares[$i]['f_id']]['fc_hierarchy_selectors'] = [];
    if($resWares[$i]['selectors'] != ''){
        $aux2 = explode(",", $resWares[$i]['selectors']);
        for($j = 0; $j < count($aux2); $j++){
			if(is_array($selectorsStructure[$aux2[$j]])){
				$wares[$resWares[$i]['f_id']]['fc_hierarchy_selectors'] = array_merge($wares[$resWares[$i]['f_id']]['fc_hierarchy_selectors'], $selectorsStructure[$aux2[$j]]);
			}
			else{
				print("\n\nERROR : unknown selector (ware : " . $resWares[$i]['f_id'] ." selector : " . $aux2[$j] . ") \n\n");
			}
        }
    }
    print("\rget selectors directly associated: " . ($i + 1) . "/" . $tot);
}
unset($resWares);

// loop through wares and get selectors inherited from parents
$i = 0;
print("\n\n");
foreach($wares as $key => &$value){
    $aux = explode(",", $value['fc_hierarchy_codes']);
    for($j = 0 ; $j < count($aux); $j++){
        if($aux[$j] != 0 && count($wares[$aux[$j]]['fc_hierarchy_selectors']) > 0 ){
            $value['fc_hierarchy_selectors'] = array_merge($value['fc_hierarchy_selectors'], $wares[$aux[$j]]['fc_hierarchy_selectors']);
        }
    }
    $value['fc_hierarchy_selectors'] = array_unique($value['fc_hierarchy_selectors']);
    print("\rget selectors inherited from parents: " . (++$i) . "/" . $tot);
}

// update database
print("\n\n");
$chunk = 100;
$counter = 0;
$wares_chunks = array_chunk($wares, $chunk);
for($i = 0; $i < count($wares_chunks); $i++){
    $update = [];
    for($j = 0; $j < count($wares_chunks[$i]); $j++){
        $update[] = "UPDATE t_creation_date SET fc_hierarchy_selectors = '" . implode(",", $wares_chunks[$i][$j]['fc_hierarchy_selectors']) . "' WHERE f_id = " . $wares_chunks[$i][$j]['f_id'];
    }
    $config['db']->query(implode(";", $update));
    $counter += count($wares_chunks[$i]);
    print("\rupdate database: " . $counter . "/" . $tot);  
}

/*************************************************/
/**************** workorders *********************/
/*************************************************/
print("\n\n--------------------------------------\n");
print("WORKORDERS\n");
print("--------------------------------------\n");
// get workorders and assets and selectors directly associated
$resWorkorders = $config['db']->query($config['selectWorkorders'])->fetchAll();
$tot = count($resWorkorders);

if($tot > 0){
	for($i = 0; $i < count($resWorkorders); $i++){
		
		$workorders[$resWorkorders[$i]['f_id']] = $resWorkorders[$i];
		$workorders[$resWorkorders[$i]['f_id']]['fc_hierarchy_selectors'] = [];
		// selectors
		if($resWorkorders[$i]['selectors'] != ''){
			$aux2 = explode(",", $resWorkorders[$i]['selectors']);
			for($j = 0; $j < count($aux2); $j++){
				if(is_array($selectorsStructure[$aux2[$j]])){
					$workorders[$resWorkorders[$i]['f_id']]['fc_hierarchy_selectors'] = array_merge($workorders[$resWorkorders[$i]['f_id']]['fc_hierarchy_selectors'], $selectorsStructure[$aux2[$j]]);
				}
				else{
					print("\n\nERROR : unknown selector (wo : " . $resWorkorders[$i]['f_id'] ." selector : " . $aux2[$j] . ") \n\n");
				}
			}
		}
		// selectors by wares
		if($resWorkorders[$i]['wares'] != ''){
			$aux2 = explode(",", $resWorkorders[$i]['wares']);
			for($j = 0; $j < count($aux2); $j++){
				if(is_array($wares[$aux2[$j]]['fc_hierarchy_selectors'])){
					$workorders[$resWorkorders[$i]['f_id']]['fc_hierarchy_selectors'] = array_merge($workorders[$resWorkorders[$i]['f_id']]['fc_hierarchy_selectors'], $wares[$aux2[$j]]['fc_hierarchy_selectors']);
				}
				else{
					print("\n\nERROR : unknown ware (wo : " . $resWorkorders[$i]['f_id'] ." wares : " . $aux2[$j] . ") \n\n");
				}
			}
		}
		print("\rget selectors by assets or directly associated: " . ($i + 1) . "/" . $tot);
	}
	unset($resWorkorders);
        
        $excludeSelFromParents = getWoSelectorsTypeToExcludeFromParents($config['db']);
	// loop through workorders and get selectors inherited from parents
	$i = 0;
	print("\n\n");
	foreach($workorders as $key => &$value){
		$aux = explode(",", $value['fc_hierarchy_codes']);
		for($j = 0 ; $j < count($aux); $j++){
                    if($aux[$j] != 0 && count($workorders[$aux[$j]]['fc_hierarchy_selectors']) > 0 ){
                        $parentSelectors = $workorders[$aux[$j]]['fc_hierarchy_selectors'];
                        //filtro i tipi di selettori da escludere indicati nel setting 'WO_AUX_SEL_PROPAGATION_FROM_PARENTS'
                        $parentSelectors = array_filter($parentSelectors,function($el) use($excludeSelFromParents) {
                            $tmp = explode(':', $el); 
                            return (is_array($excludeSelFromParents) && !in_array($tmp[0], $excludeSelFromParents));
                        });          
                        $value['fc_hierarchy_selectors'] = array_merge($value['fc_hierarchy_selectors'], $parentSelectors);
                    }
		}
		$value['fc_hierarchy_selectors'] = array_unique($value['fc_hierarchy_selectors']);
		print("\rget selectors inherited from parents: " . (++$i) . "/" . $tot);
	}
	
	// update database
	print("\n\n");
	$chunk = 100;
	$counter = 0;
	$workorders_chunks = array_chunk($workorders, $chunk);
	for($i = 0; $i < count($workorders_chunks); $i++){
		$update = [];
		for($j = 0; $j < count($workorders_chunks[$i]); $j++){
			$update[] = "UPDATE t_creation_date SET fc_hierarchy_selectors = '" . implode(",", $workorders_chunks[$i][$j]['fc_hierarchy_selectors']) . "' WHERE f_id = " . $workorders_chunks[$i][$j]['f_id'];
		}
                
//                $json = json_encode($update);
//                file_put_contents('log.txt',PHP_EOL.$json, FILE_APPEND); 

		$config['db']->query(implode(";", $update));
		$counter += count($workorders_chunks[$i]);
		print("\rupdate database: " . $counter . "/" . $tot);  
	}
}
else{
	print("\n\nno workorders found \n\n");
}








        
