<?php
// init
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/'));

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Paris');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);





//initialize database
/*
$dbParams = array(
        'adapter'=>'PDO_MYSQL',
        'params'=>array(
                'host'=>'10.0.245.220',
                'dbname'=>$argv[1],
                'username'=>'host',
                'password'=>'SetRu7res+3b',
        )
);*/
/*
$dbParams = array(
        'adapter'=>'sqlsrv',
        'params'=>array(
                'host'=>'10.0.245.220',
                'dbname'=>'mainsim3_hsr',
                'username'=>'sa',
                'password'=>'4#v$crePawu4',
        )
);
*/

$dbParams = array(
        'adapter'=>'sqlsrv',
        'params'=>array(
                'host'=>'127.0.0.1',
                'dbname'=>$argv[1],
                'username'=>'sa',
                'password'=>'4#v$crePawu4',
        )
);


$cd = new Zend_Config($dbParams);
Zend_Registry::set('db', $cd);
$db = Zend_Db::factory($cd);


function getParentCode($table = '',$f_code_sel = 0,$fathers = array(),$db = null){         
       
        $select = new Zend_Db_Select($db);      
        //check parents
        $select->reset();            
        $res_parents = $select->from($table,"f_parent_code")->where("f_code = ?",$f_code_sel)
                ->where("f_active = 1")->query()->fetchAll();        
        foreach($res_parents as $par) {
            if(!in_array($par['f_parent_code'], $fathers) && $par['f_parent_code'] != $f_code_sel) {
                $fathers[] = (int)$par['f_parent_code'];
                $fathers = getParentCode($table,$par['f_parent_code'],$fathers,$db);
            }
        }            
        return $fathers;
}
  
print("\n**************************************\n");
print("UPDATE SELECTORS SCRIPT");
print("\n**************************************\n");

/* wares */
$select = "SELECT f_id FROM t_creation_date WHERE f_type = 'WARES'";
$res = $db->query($select)->fetchAll();
$tot = count($res);

for($i = 0; $i < count($res); $i++){
    //if($res[$i]['f_id'] == 34866){
        $parents = getParentCode('t_wares_parent', $res[$i]['f_id'], array(), $db);
        $parents[] = $res[$i]['f_id'];

        // get parent selectors
        $select = " SELECT DISTINCT f_category, t_creation_date.f_id"
                . " FROM t_creation_date "
                . " JOIN t_selector_ware ON t_selector_ware.f_selector_id = t_creation_date.f_id"
                . " WHERE t_selector_ware.f_ware_id IN (" . implode(",", $parents) . ")";
        $resSelectors = $db->query($select)->fetchAll();
        if(count($resSelectors) > 0){

            $selectorsString = array();
            for($j = 0; $j < count($resSelectors); $j++){

                $selectorsString[] = $resSelectors[$j]['f_category'] . ":" . $resSelectors[$j]['f_id'];
                $selectorFathers = getParentCode('t_selectors_parent', $resSelectors[$j]['f_id'], array(), $db);
                /*
                $selectParentSelectors = " SELECT f_category, f_id"
                        . " FROM t_creation_date"
                        . " WHERE f_id IN (" . implode(",", $selectorFathers) . ")";
                $resParentSelectors = $db->query($selectParentSelectors)->fetchAll();
                for($k = 0; $k < count($resParentSelectors); $k++){
                    if(!in_array($resParentSelectors[$k]['f_category'] . ":" . $resParentSelectors[$k]['f_id'], $selectorsString) && $resParentSelectors[$k]['f_id'] > 19){
                        $selectorsString[] = $resParentSelectors[$k]['f_category'] . ":" . $resParentSelectors[$k]['f_id'];
                    }
                }*/
            }

            $update = "UPDATE t_creation_date SET fc_hierarchy_selectors = '" . implode(",", $selectorsString) . "' WHERE f_id = " . $res[$i]['f_id'];
            $db->query($update);
        }
    //}
    print("\rWARES: " . ($i + 1) . "/" . $tot);
}
print("\n\n");





/* workorders */
$select = "SELECT f_id FROM t_creation_date WHERE f_type = 'WORKORDERS'";
$res = $db->query($select)->fetchAll();
$tot = count($res);

for($i = 0; $i < count($res); $i++){
    //if($res[$i]['f_id'] == 35490){
        $selectorsString = array();
        $parents = getParentCode('t_workorders_parent', $res[$i]['f_id'], array(), $db);
        $parents[] = $res[$i]['f_id'];

        // get parent selectors
        $select = " SELECT DISTINCT f_category, t_creation_date.f_id"
                . " FROM t_creation_date "
                . " JOIN t_selector_wo ON t_selector_wo.f_selector_id = t_creation_date.f_id"
                . " WHERE t_selector_wo.f_wo_id IN (" . implode(",", $parents) . ")";
        $resSelectors = $db->query($select)->fetchAll();

        if(count($resSelectors) > 0){
            for($j = 0; $j < count($resSelectors); $j++){
                $selectorsString[] = $resSelectors[$j]['f_category'] . ":" . $resSelectors[$j]['f_id'];
            }
        } 

        // get wares selectors
        $select = " SELECT t_creation_date.fc_hierarchy_selectors "
                . " FROM t_ware_wo "
                . " JOIN t_creation_date ON t_ware_wo.f_ware_id = t_creation_date.f_id "
                . " WHERE f_wo_id = " . $res[$i]['f_id'];
                //. " AND t_creation_date.f_category = 'ASSET'";
        $resWareSelectors = $db->query($select)->fetchAll();
        if(count($resWareSelectors) > 0){
            for($j = 0; $j < count($resWareSelectors); $j++){
                if($resWareSelectors[$j]['fc_hierarchy_selectors'] != null){
                    $aux = explode(",", $resWareSelectors[$j]['fc_hierarchy_selectors']);
                    for($k = 0; $k < count($aux); $k++){
                        if(!in_array($aux[$k], $selectorsString)){
                            $selectorsString[] = $aux[$k];
                        }
                    }
                }
            }
        }


        if(count($selectorsString) > 0){
            $update = "UPDATE t_creation_date SET fc_hierarchy_selectors = '" . implode(",", $selectorsString) . "' WHERE f_id = " . $res[$i]['f_id'];
            $db->query($update);
        }
    
    //}
    
    print("\rWORKORDERS: " . ($i + 1) . "/" . $tot);
}
print("\n\n");
die();





        
