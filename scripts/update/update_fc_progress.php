<?php
// init application and db instance
include('../init.php');

include('../../application/modules/default/models/Utilities.php');

try{

    print("\n**************************************\n");
    print("UPDATE FC PROGRESS");
    print("\n**************************************\n");

    // delete progress codes from t_progress_lists
    $res = $db->query('SELECT * FROM t_creation_date where f_category IN ("PLANNED","INSPECTION","ON CONDITION","METER READING")  ORDER BY f_id')->fetchAll();

    for($i = 0; $i < count($res); $i++){
        switch($res[$i]['f_category']){
            case 'PLANNED':
                $progressToDelete['4-t_workorders'][] = $res[$i]['fc_progress'];
                $res[$i]['table'] = 't_workorders';
                $res[$i]['type_id'] = 4;
                break;
            case 'INSPECTION':
                $progressToDelete['5-t_workorders'][] = $res[$i]['fc_progress'];
                $res[$i]['table'] = 't_workorders';
                $res[$i]['type_id'] = 5;
                break;
            case 'ON CONDITION':
                $progressToDelete['6-t_workorders'][] = $res[$i]['fc_progress'];
                $res[$i]['table'] = 't_workorders';
                $res[$i]['type_id'] = 6;
                break;
            case 'METER READING':
                $progressToDelete['7-t_workorders'][] = $res[$i]['fc_progress'];
                $res[$i]['table'] = 't_workorders';
                $res[$i]['type_id'] = 7;
                break;
        }
    }
    
    foreach($progressToDelete as $key => $value){
        $aux = explode('-', $key);
        if(count($value) > 0){
            $db->query('DELETE FROM t_progress_lists WHERE f_table = "' . $aux[1] . '" AND f_type_id = ' . $aux[0] . ' AND fc_progress IN (' . implode(',', $value) . ')');
        }
    }


    // create progress and update element
    for($i = 0; $i < count($res); $i++){
        $fc_progress = Mainsim_Model_Utilities::getFcProgress($res[$i]['table'], $res[$i]['type_id'], $db);
        // update t_creation_date
        $db->update('t_creation_date', ['fc_progress' => $fc_progress], 'f_id = ' . $res[$i]['f_id']); 
        // update t_creation_date_history
        $db->update('t_creation_date_history', ['fc_progress' => $fc_progress], 'f_code = ' . $res[$i]['f_id']); 
    }
    
}
catch(Exception $e){
    print($e->GetMessage());
}