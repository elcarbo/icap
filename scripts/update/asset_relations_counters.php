<?php
// init application and db instance
include('../init.php');

include('../../application/modules/default/models/Utilities.php');

try{

    print("\n**************************************\n");
    print("UPDATE ASSET RELATIONS COUNTERS");
    print("\n**************************************\n");

    // reset counters
    $db->query('UPDATE t_creation_date SET fc_documents = 0 WHERE f_category = "ASSET"');
    $db->query('UPDATE t_wares SET fc_asset_wo = 0, fc_asset_opened_wo = 0, fc_asset_opened_pm = 0, fc_asset_opened_on_condition = 0 WHERE f_type_id = 1');


    // update documents counter
    $res = $db->query('SELECT t_creation_date.f_id, count(*) AS tot
                        FROM t_creation_date 
                        JOIN t_wares_relations ON t_creation_date.f_id = t_wares_relations.f_code_ware_master
                        WHERE t_wares_relations.f_type_id_slave = 5
                            AND f_category = "ASSET"
                        GROUP BY t_creation_date.f_id')->fetchAll();
    $tot = count($res); 
    for($i = 0; $i < count($res); $i++){
        $db->query('UPDATE t_creation_date SET fc_documents = ' . $res[$i]['tot'] . ' WHERE f_id = ' . $res[$i]['f_id']);
        print("\rupdate documents: " . ($i + 1) . "/" . $tot);
    }

    print("\n\n");

    // update all wo counter
    $res = $db->query('SELECT t_creation_date.f_id
                        FROM t_creation_date 
                        WHERE f_category = "ASSET"')->fetchAll();
    $tot = count($res);
    for($i = 0; $i < $tot; $i++){
        
        // get asset hierarchy
        $hierarchy_codes = [];
        Mainsim_Model_Utilities::getChildCode('t_wares_parent', $res[$i]['f_id'], $hierarchy_codes, $db);
        $hierarchy_codes[] = $res[$i]['f_id'];

        $res2 = $db->query('SELECT count(*) AS tot
                        FROM t_creation_date 
                        JOIN t_ware_wo ON t_creation_date.f_id = t_ware_wo.f_ware_id
                        JOIN t_creation_date cd2 ON t_ware_wo.f_wo_id = cd2.f_id
                        JOIN t_wf_phases ON cd2.f_phase_id = t_wf_phases.f_number AND cd2.f_wf_id = t_wf_phases.f_wf_id
                        JOIN t_wf_groups ON t_wf_phases.f_group_id = t_wf_groups.f_id
                        WHERE t_wf_groups.f_group NOT IN ("Closing","Deleting","Cloned")
                            AND cd2.f_category NOT IN ("PLANNED GENERATOR", "ON CONDITION GENERATOR", "METER GENERATOR", "INSPECTION GENERATOR")
                            AND t_creation_date.f_category = "ASSET"
                            AND t_creation_date.f_id IN (' . implode(',', $hierarchy_codes) . ')')->fetchAll();
        $db->query('UPDATE t_wares SET fc_asset_wo = ' . $res2[0]['tot'] . ' WHERE f_code = ' . $res[$i]['f_id']);
        print("\rupdate fc_asset_wo: " . ($i + 1) . "/" . $tot);
    }


    
    
    
    print("\n\n");
    
    // update fc_asset_opened_wo counter
    $res = $db->query('SELECT t_creation_date.f_id, count(*) AS tot
                        FROM t_creation_date 
                        JOIN t_ware_wo ON t_creation_date.f_id = t_ware_wo.f_ware_id
                        JOIN t_creation_date cd2 ON t_ware_wo.f_wo_id = cd2.f_id
                        JOIN t_wf_phases ON cd2.f_phase_id = t_wf_phases.f_number AND cd2.f_wf_id = t_wf_phases.f_wf_id
                        JOIN t_wf_groups ON t_wf_phases.f_group_id = t_wf_groups.f_id
                        WHERE t_wf_groups.f_group NOT IN ("Closing","Deleting","Cloned")
                            AND cd2.f_category IN ("CORRECTIVE", "PURCHASE ORDER", "RESERVATION", "CORRECTIVE TASK", "WORK REQUEST", "EMERGENCY", "STANDARD WORKORDER")
                            AND t_creation_date.f_category = "ASSET"
                        GROUP BY t_creation_date.f_id')->fetchAll();
    $tot = count($res); 
    for($i = 0; $i < count($res); $i++){
        $db->query('UPDATE t_wares SET fc_asset_opened_wo = ' . $res[$i]['tot'] . ' WHERE f_code = ' . $res[$i]['f_id']);
        print("\rupdate fc_asset_opened_wo: " . ($i + 1) . "/" . $tot);
    }

    print("\n\n");

    // update fc_asset_opened_pm counter
    $res = $db->query('SELECT t_creation_date.f_id, count(*) AS tot
                        FROM t_creation_date 
                        JOIN t_ware_wo ON t_creation_date.f_id = t_ware_wo.f_ware_id
                        JOIN t_creation_date cd2 ON t_ware_wo.f_wo_id = cd2.f_id
                        JOIN t_wf_phases ON cd2.f_phase_id = t_wf_phases.f_number AND cd2.f_wf_id = t_wf_phases.f_wf_id
                        JOIN t_wf_groups ON t_wf_phases.f_group_id = t_wf_groups.f_id
                        WHERE t_wf_groups.f_group NOT IN ("Closing","Deleting","Cloned")
                            AND cd2.f_category IN ("PLANNED", "INSPECTION", "METER READING")
                            AND t_creation_date.f_category = "ASSET"
                        GROUP BY t_creation_date.f_id')->fetchAll();
    $tot = count($res); 
    for($i = 0; $i < count($res); $i++){
        $db->query('UPDATE t_wares SET fc_asset_opened_pm = ' . $res[$i]['tot'] . ' WHERE f_code = ' . $res[$i]['f_id']);
        print("\rupdate fc_asset_opened_pm: " . ($i + 1) . "/" . $tot);
    }

    print("\n\n");

    // update fc_asset_opened_on_condition counter
    $res = $db->query('SELECT t_creation_date.f_id, count(*) AS tot
                        FROM t_creation_date 
                        JOIN t_ware_wo ON t_creation_date.f_id = t_ware_wo.f_ware_id
                        JOIN t_creation_date cd2 ON t_ware_wo.f_wo_id = cd2.f_id
                        JOIN t_wf_phases ON cd2.f_phase_id = t_wf_phases.f_number AND cd2.f_wf_id = t_wf_phases.f_wf_id
                        JOIN t_wf_groups ON t_wf_phases.f_group_id = t_wf_groups.f_id
                        WHERE t_wf_groups.f_group NOT IN ("Closing","Deleting","Cloned")
                            AND cd2.f_category IN ("ON CONDITION")
                            AND t_creation_date.f_category = "ASSET"
                        GROUP BY t_creation_date.f_id')->fetchAll();
    $tot = count($res); 
    for($i = 0; $i < count($res); $i++){
        $db->query('UPDATE t_wares SET fc_asset_opened_on_condition = ' . $res[$i]['tot'] . ' WHERE f_code = ' . $res[$i]['f_id']);
        print("\rupdate fc_asset_opened_on_condition: " . ($i + 1) . "/" . $tot);
    }  
}
catch(Exception $e){
    print($e->GetMessage());
}